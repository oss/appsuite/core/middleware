/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.soap.cxf.interceptor;

import org.apache.cxf.message.Message;

/**
 * {@link LoggingOutInterceptor} - Extends {@link org.apache.cxf.interceptor.LoggingOutInterceptor} by sanitizing possible passwords in
 * logged payloads.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class LoggingOutInterceptor extends org.apache.cxf.ext.logging.LoggingOutInterceptor {

    private final boolean filterLog;

    /**
     * Initializes a new {@link LoggingOutInterceptor}.
     *
     * @param filterLog Whether to filter passwords from log entries or not
     */
    public LoggingOutInterceptor(boolean filterLog) {
        super();
        this.filterLog = filterLog;
    }

    @Override
    protected String transform(Message message, String originalLogString) {
        return filterLog ? LoggingUtility.sanitizeMessage(originalLogString) : originalLogString;
    }

}
