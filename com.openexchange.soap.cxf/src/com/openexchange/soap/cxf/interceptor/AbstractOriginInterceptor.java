/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.soap.cxf.interceptor;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;

/**
 * Abstract class to provide header names for {@link OriginInInterceptor} and {@link OriginOutInterceptor}
 * 
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public abstract class AbstractOriginInterceptor extends AbstractPhaseInterceptor<Message> {

    protected static final String AUTH_USERNAME_HEADER = "X-OX-Auth-Username";
    protected static final String AUTH_BRANDNAME_HEADER = "X-OX-Auth-Brandname";
    protected static final String ORIGINAL_HOST_HEADER = "X-Original-Host"; // set by cloud-plugins
    protected static final String ORIGINAL_REMOTE_ADDRESS_HEADER = "X-Original-Remote-Address"; // set by cloud-plugins

    protected AbstractOriginInterceptor(String phase) {
        super(phase);
    }

}
