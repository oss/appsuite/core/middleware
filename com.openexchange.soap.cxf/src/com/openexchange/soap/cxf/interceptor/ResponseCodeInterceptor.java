/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.soap.cxf.interceptor;

import java.util.Map.Entry;
import javax.servlet.http.HttpServletResponse;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import com.openexchange.exception.AbstractResponseCodeAwareException;

/**
 * {@link ResponseCodeInterceptor} is an {@link AbstractPhaseInterceptor} which adjusts the response code
 * in case the exception chain contains an {@link AbstractResponseCodeAwareException}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class ResponseCodeInterceptor extends AbstractPhaseInterceptor<Message> {

    public ResponseCodeInterceptor() {
        super(Phase.PREPARE_SEND_ENDING);
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        Throwable ex = message.getContent(Exception.class);
        AbstractResponseCodeAwareException rca = findResponseCodeError(ex);
        if (null != rca) {
            HttpServletResponse response = (HttpServletResponse) message.get(AbstractHTTPDestination.HTTP_RESPONSE);
            response.setStatus(rca.getCode());
            for (Entry<String, String> header : rca.getAdditionalHeaders().entrySet()) {
                response.setHeader(header.getKey(), header.getValue());

            }
        }
    }

    // -------------------------- private methods ---------------------

    /**
     * Tries to find an {@link AbstractResponseCodeAwareException} with the exception chain
     *
     * @param ex The throwable to check
     * @return The first found {@link AbstractResponseCodeAwareException} or null
     */
    private AbstractResponseCodeAwareException findResponseCodeError(Throwable ex) {
        if (ex instanceof AbstractResponseCodeAwareException rca) {
            return rca;
        }
        Throwable cause = ex.getCause();
        if (cause == null) {
            return null;
        }
        return findResponseCodeError(cause);
    }

}
