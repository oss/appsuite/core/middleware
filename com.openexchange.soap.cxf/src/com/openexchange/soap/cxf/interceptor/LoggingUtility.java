/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.soap.cxf.interceptor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * {@link LoggingUtility} - Utility class for CXF logging.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class LoggingUtility {

    /**
     * Initializes a new {@link LoggingUtility}.
     */
    private LoggingUtility() {
        super();
    }

    /** The regular expression to discover possible password elements */
    private static final Pattern PATTERN_PASSWORD = Pattern.compile("(<[^<>]*?password[^<>]*?>)[^<>]+(</[^<>]*?password[^<>]*?>)");

    /**
     * Sanitizes possible user-sensitive data from given message.
     *
     * @param message The message to sanitize
     * @return The sanitized message
     */
    public static String sanitizeMessage(final String message) {
        if (null == message || message.indexOf("password") < 0) {
            // No input OR no occurrence possible since no such "password" substring
            return message;
        }

        // Initialize java.util.regex.Matcher instance
        Matcher m = PATTERN_PASSWORD.matcher(message);
        if (!m.find()) {
            // No occurrence
            return message;
        }

        // Replace possible passwords in message
        StringBuffer sb = new StringBuffer(message.length());
        do {
            m.appendReplacement(sb, "$1XXXX$2");
        } while (m.find()) ;
        m.appendTail(sb);
        return sb.toString();
    }

}
