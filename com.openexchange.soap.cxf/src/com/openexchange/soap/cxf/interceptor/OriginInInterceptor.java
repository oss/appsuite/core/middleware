/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.soap.cxf.interceptor;

import java.util.List;
import java.util.Map;
import org.apache.cxf.helpers.CastUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.Interceptor;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageUtils;
import org.apache.cxf.phase.Phase;
import com.openexchange.java.Strings;
import com.openexchange.log.LogProperties;
import com.openexchange.log.LogProperties.Name;

/**
 * SOAP {@link Interceptor} to pass the origin information of the request to the threads MDCs.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class OriginInInterceptor extends AbstractOriginInterceptor {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(OriginInInterceptor.class);
    private static final String LOG_MATCHED = "Matched '{}': {}.";

    public OriginInInterceptor() {
        super(Phase.READ);
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        if (MessageUtils.isOutbound(message)) {
            return;
        }
        Map<String, List<String>> headers = CastUtils.cast((Map<?, ?>) message.get(Message.PROTOCOL_HEADERS));
        if (headers == null || headers.isEmpty()) {
            LOG.debug("No headers to pass available.");
            return;
        }
        handle(headers, AUTH_USERNAME_HEADER, LogProperties.Name.SOAP_AUTH_USERNAME);
        handle(headers, AUTH_BRANDNAME_HEADER, LogProperties.Name.SOAP_AUTH_BRANDNAME);
        handle(headers, ORIGINAL_HOST_HEADER, LogProperties.Name.ORIGINAL_HOSTNAME);
        handle(headers, ORIGINAL_REMOTE_ADDRESS_HEADER, LogProperties.Name.GRIZZLY_ORIGINAL_REMOTE_ADDRESS);
    }

    private void handle(Map<String, List<String>> allHeaders, String headerToProcess, Name logProperty) {
        List<String> headers = allHeaders.get(headerToProcess);
        if (headers != null && !headers.isEmpty()) {
            for (String header : headers) {
                if (Strings.isNotEmpty(header)) {
                    LOG.debug(LOG_MATCHED, headerToProcess, header);
                    LogProperties.put(logProperty, header);
                    return;
                }
            }
        }
    }
}
