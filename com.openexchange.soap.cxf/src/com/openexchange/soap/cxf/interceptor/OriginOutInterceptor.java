/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.soap.cxf.interceptor;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.Interceptor;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import com.openexchange.log.LogProperties;

/**
 * SOAP {@link Interceptor} to remove the origin information of the request that were previously set through the {@link OriginInInterceptor}.
 * 
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class OriginOutInterceptor extends AbstractOriginInterceptor {

    public OriginOutInterceptor() {
        super(Phase.WRITE);
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        LogProperties.remove(LogProperties.Name.SOAP_AUTH_USERNAME);
        LogProperties.remove(LogProperties.Name.SOAP_AUTH_BRANDNAME);
        LogProperties.remove(LogProperties.Name.ORIGINAL_HOSTNAME);
        LogProperties.remove(LogProperties.Name.GRIZZLY_ORIGINAL_REMOTE_ADDRESS);
    }

}
