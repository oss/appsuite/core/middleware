/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.logback.json.extensions;

import java.util.Map;
import com.openexchange.log.LogProperties;
import com.openexchange.logback.extensions.json.BasicJsonLayout;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * {@link ExtendedJsonLayout} - Defines a JSON layout for {@link ILoggingEvent} to be logged
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 * @since v8.0.0
 */
public class ExtendedJsonLayout extends BasicJsonLayout {

    private static final String ATTR_NAME_REQUEST_ID = "requestId";
    private static final String ATTR_NAME_CONTEXTID = "contextId";
    private static final String ATTR_NAME_USERID = "userId";
    private static final String ATTR_NAME_BRAND = "brand";
    private static final String ATTR_NAME_AUDIT = "audit";

    private static final LogProperties.Name[] PROPERTIES_TO_FILTER = { LogProperties.Name.HOSTNAME, LogProperties.Name.REQUEST_TRACKING_ID };

    /**
     * Initializes a new {@link ExtendedJsonLayout}.
     */
    public ExtendedJsonLayout() {
        super();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Map<String, Object> toJsonMap(ILoggingEvent event) {
        Map<String, Object> map = super.toJsonMap(event);

        Map<String, String> mdc = event.getMDCPropertyMap();
        if (mdc != null) {
            addMdcPropertyIfPresent(LogProperties.Name.REQUEST_TRACKING_ID, mdc, ATTR_NAME_REQUEST_ID, map);
            addMdcPropertyIfPresent(LogProperties.Name.SESSION_CONTEXT_ID, mdc, ATTR_NAME_CONTEXTID, map);
            addMdcPropertyIfPresent(LogProperties.Name.SESSION_USER_ID, mdc, ATTR_NAME_USERID, map);
            addMdcPropertyIfPresent(LogProperties.Name.SESSION_BRAND, mdc, ATTR_NAME_BRAND, map);

            if ("true".equals(getPropertyFromMDC(LogProperties.Name.AUDIT, mdc))) {
                add(ATTR_NAME_AUDIT, true, true, map);
            }
        }

        Map<String, String> details = (Map<String, String>) map.get(ATTR_NAME_DETAILS);
        if (details != null) {
            Map<String, String> optFilteredDetails = filterMDC(details);
            if (optFilteredDetails == null) {
                // Remove "details"
                map.remove(ATTR_NAME_DETAILS);
            } else {
                // Replace "details"
                addMap(ATTR_NAME_DETAILS, true, optFilteredDetails, map);
            }
        }

        return map;
    }

    /**
     * Adds specified MDC property (if any) to superior map.
     *
     * @param property The name of the MDC property
     * @param mdc The MDC map to look-up
     * @param fieldName The desired name of that property's value in superior map
     * @param map The superior map to add to
     */
    private void addMdcPropertyIfPresent(LogProperties.Name property, Map<String, String> mdc, String fieldName, Map<String, Object> map) {
        add(fieldName, true, getPropertyFromMDC(property, mdc), map);
    }

    /**
     * Removes the filtered properties from MDC map
     *
     * @param mdcPropertyMap The MDC map to filter
     * @return A map without the filtered properties or <code>null</code> if MDC map is empty (possibly after filtering)
     */
    private static Map<String, String> filterMDC(Map<String, String> mdcPropertyMap) {
        if (null == mdcPropertyMap || mdcPropertyMap.isEmpty()) {
            return null;
        }
        for (LogProperties.Name prop : PROPERTIES_TO_FILTER) {
            mdcPropertyMap.remove(prop.getName());
        }
        return mdcPropertyMap.isEmpty() ? null : mdcPropertyMap;
    }

    /**
     * Gets the denoted property from given MDC properties.
     *
     * @param property The property to return (if available)
     * @param mdc The MDC properties to get from
     * @return The MDC property or <code>null</code>
     */
    private static String getPropertyFromMDC(LogProperties.Name property, Map<String, String> mdc) {
        return null == property || null == mdc ? null : mdc.get(property.getName());
    }

}
