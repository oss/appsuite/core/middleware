/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.contact.provider.composition.impl;

import static com.openexchange.contact.provider.ContactsProviders.getCapabilityName;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableMap;
import com.openexchange.capabilities.CapabilityChecker;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.contact.provider.ContactProviderProperty;
import com.openexchange.contact.provider.ContactsProvider;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.RankingAwareNearRegistryServiceTracker;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tools.session.ServerSessionAdapter;

/**
 * {@link ContactsProviderTracker}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @since v7.10.5
 */
public class ContactsProviderTracker extends RankingAwareNearRegistryServiceTracker<ContactsProvider> {

    private static final Logger LOG = LoggerFactory.getLogger(ContactsProviderTracker.class);

    private final ConcurrentMap<String, ServiceRegistration<CapabilityChecker>> checkerRegistrations;
    private final ConcurrentMap<String, CapabilityChecker> checkersByCapabilityName;
    private final ServiceLookup services;

    /**
     * Initializes a new {@link ContactsProviderTracker}.
     *
     * @param context The bundle context
     * @param services The service lookup reference
     */
    public ContactsProviderTracker(BundleContext context, ServiceLookup services) {
        super(context, ContactsProvider.class);
        this.services = services;
        this.checkerRegistrations = new ConcurrentHashMap<>();
        this.checkersByCapabilityName = new ConcurrentHashMap<>();
    }

    /**
     * Gets a value indicating whether the session's user has the required capability for a specific contacts provider or not.
     *
     * @param session The session to check
     * @param providerId The identifier of the contacts provider to check the capabilities for
     * @return <code>true</code> if the user has the required capability, <code>false</code>, otherwise
     */
    public boolean hasCapability(Session session, String providerId) throws OXException {
        String capabilityName = getCapabilityName(providerId);
        CapabilityChecker capabilityChecker = checkersByCapabilityName.get(capabilityName);
        if (null == capabilityChecker) {
            LOG.debug("No capability checker known for \"{}\", assuming absent.", capabilityName);
            return false;
        }
        return capabilityChecker.isEnabled(capabilityName, session);
    }

    @Override
    protected void onServiceAdded(ContactsProvider provider) {
        /*
         * declare capability for calendar provider
         */
        String capabilityName = getCapabilityName(provider);
        services.getService(CapabilityService.class).declareCapability(capabilityName);
        /*
         * register an appropriate capability checker
         */
        CapabilityChecker capabilityChecker = (capability, session) -> {
            if (capabilityName.equals(capability)) {
                ServerSession serverSession = ServerSessionAdapter.valueOf(session);
                if (serverSession.isAnonymous()) {
                    return false;
                }
                if (false == serverSession.getUserPermissionBits().hasContact()) {
                    return false;
                }
                return isProviderEnabled(provider) && provider.isAvailable(session);
            }
            return true;
        };
        Dictionary<String, Object> serviceProperties = new Hashtable<>(1);
        serviceProperties.put(CapabilityChecker.PROPERTY_CAPABILITIES, capabilityName);
        ServiceRegistration<CapabilityChecker> checkerRegistration = context.registerService(CapabilityChecker.class, capabilityChecker, serviceProperties);
        if (null != checkerRegistrations.putIfAbsent(provider.getId(), checkerRegistration)) {
            checkerRegistration.unregister();
        } else {
            checkersByCapabilityName.put(capabilityName, capabilityChecker);
        }
    }

    @Override
    protected void onServiceRemoved(ContactsProvider provider) {
        /*
         * unregister capability checker for calendar provider
         */
        String capabilityName = getCapabilityName(provider);
        ServiceRegistration<CapabilityChecker> checkerRegistration = checkerRegistrations.remove(provider.getId());
        if (null != checkerRegistration) {
            checkerRegistration.unregister();
            checkersByCapabilityName.remove(capabilityName);
        }
        /*
         * undeclare capability for calendar provider
         */
        services.getService(CapabilityService.class).undeclareCapability(capabilityName);
    }

    /**
     * Checks whether the specified {@link ContactsProvider} is enabled
     *
     * @param provider The provider to check
     * @return <code>true</code> if the provider is enabled; <code>false</code> otherwise
     * @throws OXException if the {@link LeanConfigurationService} is absent or any other error is occurred
     */
    private boolean isProviderEnabled(ContactsProvider provider) throws OXException {
        LeanConfigurationService service = services.getServiceSafe(LeanConfigurationService.class);
        return service.getBooleanProperty(ContactProviderProperty.ENABLED, ImmutableMap.of(ContactProviderProperty.OPTIONAL_NAME, provider.getId()));
    }
}
