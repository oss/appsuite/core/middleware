/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.context.id.impl;

import java.sql.Connection;
import java.sql.SQLException;
import com.openexchange.admin.autocontextid.rmi.exceptions.OXAutoCIDExceptionCodes;
import com.openexchange.admin.context.id.ContextIDGenerator;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.impl.IDGenerator;
import com.openexchange.groupware.impl.IDGenerator.Implementations;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * Implementation for the {@link ContextIDGenerator}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class ContextIDGeneratorImpl implements ContextIDGenerator {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextIDGeneratorImpl.class);
    private final ErrorAwareSupplier<? extends DatabaseService> dbServiceSupplier;
    private boolean initialized;

    /**
     *
     * Initializes a new {@link ContextIDGeneratorImpl}.
     *
     * @param dbServiceSupplier A supplier for the database service.
     */
    public ContextIDGeneratorImpl(ErrorAwareSupplier<? extends DatabaseService> dbServiceSupplier) {
        super();
        this.dbServiceSupplier = dbServiceSupplier;
    }

    /**
     * Initializes the {@link com.openexchange.groupware.impl.IDGenerator.Implementations} for the 'sequence_context' table.
     *
     * @throws SQLException In case of an error during initialization
     */
    public void init() throws SQLException {
        for (Implementations impl : Implementations.values()) {
            impl.getImpl().registerType("sequence_context", -2);
        }
        initialized = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int generateContextId() throws OXException {
        if (!initialized) {
            throw OXAutoCIDExceptionCodes.SERVICE_NOT_INITIALIZED.create("ContextIDGeneratorImpl");
        }
        Connection writeCon = this.dbServiceSupplier.get().getForUpdateTask();

        int rollback = 0;
        try {
            writeCon.setAutoCommit(false);
            rollback = 1;
            int id = IDGenerator.getId(writeCon, -2);
            writeCon.commit();
            rollback = 2;
            return id;
        } catch (SQLException e) {
            throw new OXException();
        } finally {
            if (rollback > 0) {
                if (rollback == 1) {
                    doRollback(writeCon);
                }
                Databases.autocommit(writeCon);
            }
            this.dbServiceSupplier.get().backWritable(writeCon);
        }
    }

    private static void doRollback(final Connection con) {
        if (null != con) {
            try {
                con.rollback();
            } catch (SQLException e2) {
                LOG.error("Error doing rollback", e2);
            }
        }
    }

}
