/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.deputy.provider.imap.osgi;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import com.openexchange.deputy.DeputyModuleProvider;
import com.openexchange.deputy.provider.imap.doveadm.DoveAdmProvider;
import com.openexchange.dovecot.doveadm.client.DoveAdmClient;
import com.openexchange.server.ServiceLookup;


/**
 * {@link DoveAdmClientTracker} - The tracker for DoveAdm service that registers a DoveAdm-backed
 * deputy module provider once such a service is available.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class DoveAdmClientTracker implements ServiceTrackerCustomizer<DoveAdmClient, DoveAdmClient> {

    private final BundleContext context;
    private final ServiceLookup services;

    private ServiceRegistration<DeputyModuleProvider> doveAdmProviderRegistration;

    /**
     * Initializes a new instance of {@link DoveAdmClientTracker}.
     *
     * @param services The service look-up to pass
     * @param context The bundle context
     */
    public DoveAdmClientTracker(ServiceLookup services, BundleContext context) {
        super();
        this.services = services;
        this.context = context;
    }

    @Override
    public synchronized DoveAdmClient addingService(ServiceReference<DoveAdmClient> reference) {
        DoveAdmClient doveAdmClient = context.getService(reference);
        doveAdmProviderRegistration = context.registerService(DeputyModuleProvider.class, new DoveAdmProvider(doveAdmClient, services), null);
        return doveAdmClient;
    }

    @Override
    public synchronized void modifiedService(ServiceReference<DoveAdmClient> reference, DoveAdmClient service) {
        // Nothing
    }

    @Override
    public synchronized void removedService(ServiceReference<DoveAdmClient> reference, DoveAdmClient service) {
        ServiceRegistration<DeputyModuleProvider> doveAdmProviderRegistration = this.doveAdmProviderRegistration;
        if (doveAdmProviderRegistration != null) {
            this.doveAdmProviderRegistration = null;
            doveAdmProviderRegistration.unregister();
        }
        context.ungetService(reference);
    }

}
