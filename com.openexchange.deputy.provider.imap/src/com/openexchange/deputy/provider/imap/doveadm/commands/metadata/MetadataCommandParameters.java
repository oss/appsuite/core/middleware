/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.deputy.provider.imap.doveadm.commands.metadata;

import java.util.HashMap;
import java.util.Map;
import com.openexchange.deputy.provider.imap.doveadm.commands.DoveAdmMailboxCommandParameters;
import com.openexchange.java.Strings;

/**
 *
 * {@link MetadataCommandParameters} - Simple container for parameters passed to METADATA-related DoveAdm commands.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class MetadataCommandParameters implements DoveAdmMailboxCommandParameters {

    /**
     * Creates a new builder for an instance of {@code MetadataCommandParameters}.
     *
     * @return The newly created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * The builder for an instance of {@code MetadataCommandParameters}.
     */
    public static final class Builder {

        private String user;
        private String mailbox;
        private String key;
        private String value;

        /**
         * Initializes a new {@link Builder}.
         */
        private Builder() {
            super();
        }

        /**
         * Sets the UID of user to apply to.
         *
         * @param user The user identifier
         * @return This builder
         */
        public Builder withUser(String user) {
            this.user = user;
            return this;
        }

        /**
         * Sets the mailbox name (aka full name).
         *
         * @param mailbox The mailbox name
         * @return This builder
         */
        public Builder withMailbox(String mailbox) {
            this.mailbox = mailbox;
            return this;
        }

        /**
         * Sets the METADATA key.
         *
         * @param key The METADATA key to set
         * @return This builder
         */
        public Builder withKey(String key) {
            this.key = key;
            return this;
        }

        /**
         * Sets the METADATA value
         *
         * @param right The METADATA value to set
         * @return This builder
         */
        public Builder withValue(String value) {
            this.value = value;
            return this;
        }

        /**
         * Builds the instance of {@code MetadataCommandParameters} from this builder's arguments.
         *
         * @return The parameters
         */
        public MetadataCommandParameters build() {
            return new MetadataCommandParameters(this);
        }
    }

    // ------------------------------------------------------------------------------------------------------------------

    private final String user;
    private final String mailbox;
    private final String key;
    private final String value;

    /**
     * Initializes a new instance of {@link MetadataCommandParameters}.
     *
     * @param builder The builder to get arguments from
     */
    private MetadataCommandParameters(Builder builder) {
        super();
        this.user = builder.user;
        this.mailbox = builder.mailbox;
        this.key = builder.key;
        this.value = builder.value;
    }

    @Override
    public String getUser() {
        return user;
    }

    @Override
    public String getMailbox() {
        return mailbox;
    }

    /**
     * Gets the METADATA key.
     *
     * @return The METADATA key
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets the METADATA value.
     *
     * @return The METADATA value
     */
    public String getValue() {
        return value;
    }

    @Override
    public Map<String, Object> toRequestBody() {
        Map<String, Object> params = new HashMap<>(4, 0.9F);
        if (null != user) {
            params.put("user", user);
        }
        if (Strings.isNotEmpty(mailbox)) {
            params.put("mailbox", mailbox);
        }
        if (null != key) {
            params.put("key", key);
        }
        if (null != value) {
            params.put("value", value);
        }
        return params;
    }

}
