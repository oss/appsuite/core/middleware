/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.deputy.provider.imap.doveadm.commands.mailbox;

import java.util.Map;
import com.openexchange.deputy.provider.imap.doveadm.commands.AbstractDoveAdmCommand;
import com.openexchange.dovecot.doveadm.client.DoveAdmClientExceptionCodes;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;

/**
 * {@link AbstractMailboxCommand} - The super class for ACL commands.
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public abstract class AbstractMailboxCommand extends AbstractDoveAdmCommand<MailboxCommandParameters> {

    /**
     * Initializes a new {@link AbstractMailboxCommand}.
     *
     * @param parameters The parameters to use
     * @throws OXException In case requires parameters are missing
     */
    protected AbstractMailboxCommand(MailboxCommandParameters parameters) throws OXException {
        super(parameters);
    }

    /**
     * Checks specified parameters.
     *
     * @param parameters The parameters to check
     * @throws OXException If parameters' check fails
     */
    @Override
    protected void checkParameters(MailboxCommandParameters parameters) throws OXException {
        if (Strings.isEmpty(parameters.getUser())) {
            throw DoveAdmClientExceptionCodes.MISSING_PARAMETER.create("user");
        }
    }

    @Override
    public Map<String, Object> getParameters() {
        return parameters.toRequestBody();
    }

}
