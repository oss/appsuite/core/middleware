/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.deputy.provider.imap.doveadm;

/**
 * {@link DoveAdmAclType} - An enumeration of known ACL types of the DoveAdm API.
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @see <a href="https://doc.dovecot.org/3.0/man/doveadm-acl.1/#arguments">Doveadm ACL Arguments</a>
 */
public enum DoveAdmAclType {

    /** Group-override identifier allows you to override users’ ACLs. Probably the most useful reason to do this is to temporarily disable access for some users. */
    GROUP_OVERRIDE("group-override"),

    /** Permission identifier for a single user */
    USER("user"),

    /** Permission identifier for the folder owner */
    OWNER("owner"),

    /** Permission identifier for a group */
    GROUP("group"),

    /** Permission identifier for authenticated */
    AUTHENTICATED("authenticated"),

    /** Permission identifier for anyone or anonymous */
    ANYONE("anyone"),
    ;

    private final String identifier;

    private DoveAdmAclType(String identifier) {
        this.identifier = identifier;
    }

    /**
     * Gets the identifier
     *
     * @return The identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Get the ACL identifier used by DoveAdm API.
     *
     * @param value The value to set, can be <code>null</code>
     * @return The ACL identifier; e.g. <code>"user=berta@context1.ox.test"</code>
     */
    public String toACLId(String value) {
        String key = getIdentifier();
        return switch (this) {
            case GROUP_OVERRIDE, USER, GROUP -> new StringBuilder(key).append('=').append(value).toString();
            case OWNER, AUTHENTICATED, ANYONE -> key;
            default -> throw new IllegalArgumentException("Unexpected value: " + key);
        };
    }

}
