/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.deputy.provider.imap.doveadm;

import java.util.Set;

/**
 * {@link DoveAdmACL} - Represents an ACL as returned by DoveAdm API:
 * <pre>
 *  {
 *    "id":"user=dora@context1.ox.test",
 *    "global":"",
 *    "rights":"insert lookup read write write-seen"
 *  }
 * </pre>
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class DoveAdmACL {

    private final String aclId;
    private final Set<DoveAdmAclRight> rights;

    /**
     * Initializes a new instance of {@link DoveAdmACL}.
     *
     * @param aclId The ACL entity identifier; e.g. <code>"user=berta@context1.ox.test"</code>
     * @param rights The rights associated with the ACL entity
     */
    public DoveAdmACL(String aclId, Set<DoveAdmAclRight> rights) {
        super();
        this.aclId = aclId;
        this.rights = rights;
    }

    /**
     * Gets the ACL entity identifier.
     *
     * @return The ACL entity identifier; e.g. <code>"user=berta@context1.ox.test"</code>
     */
    public String getAclId() {
        return aclId;
    }

    /**
     * Gets the rights
     *
     * @return The rights
     */
    public Set<DoveAdmAclRight> getRights() {
        return rights;
    }

}
