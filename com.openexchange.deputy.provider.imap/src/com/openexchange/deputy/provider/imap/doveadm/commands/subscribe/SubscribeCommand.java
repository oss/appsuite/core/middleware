/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.deputy.provider.imap.doveadm.commands.subscribe;

import com.openexchange.dovecot.doveadm.client.DoveAdmClientExceptionCodes;
import com.openexchange.exception.OXException;

/**
 * {@link SubscribeCommand} - The command to set subscription.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class SubscribeCommand extends AbstractSubscribeCommand {

    /**
     * Initializes a new {@link SubscribeCommand}.
     *
     * @param parameters The parameters to use
     * @throws OXException In case requires parameters are missing
     */
    public SubscribeCommand(SubscribeCommandParameters parameters) throws OXException {
        super(parameters);
    }

    @Override
    protected void checkParameters(SubscribeCommandParameters parameters) throws OXException {
        super.checkParameters(parameters);
        if (parameters.getMailbox() == null || parameters.getMailbox().isEmpty()) {
            throw DoveAdmClientExceptionCodes.MISSING_PARAMETER.create("mailbox");
        }
    }

    @Override
    public String getCommand() {
        return "mailboxSubscribe";
    }
}
