/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.deputy.provider.imap.doveadm.commands;

import java.util.concurrent.atomic.AtomicInteger;
import com.openexchange.dovecot.doveadm.client.DoveAdmCommand;
import com.openexchange.exception.OXException;

/**
 * {@link AbstractDoveAdmCommand} - The super class for DovveAdm commands.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public abstract class AbstractDoveAdmCommand<P extends DoveAdmCommandParameters> implements DoveAdmCommand {

    private static final AtomicInteger COMMAND_COUNTER = new AtomicInteger(0);

    /**
     * Create a new command identifier.
     *
     * @return The new command identifier
     */
    public static String createCommandIdentifier() {
        int current;
        int newValue;
        do {
            current = COMMAND_COUNTER.get();
            newValue = current < Integer.MAX_VALUE ? (current + 1) : 1;
        } while (!COMMAND_COUNTER.compareAndSet(current, newValue));
        return new StringBuilder("tag").append(newValue).toString();
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    /** The command identifier (or tag) */
    private final String optionalIdentifier;

    /** The parameters for this command */
    protected final P parameters;

    /**
     * Initializes a new {@link AbstractDoveAdmCommand}.
     *
     * @param parameters The parameters for this command
     * @throws OXException If parameters are invalid
     */
    protected AbstractDoveAdmCommand(P parameters) throws OXException {
        super();
        checkParameters(parameters); // NOSONARLINT
        this.parameters = parameters;
        optionalIdentifier = createCommandIdentifier();
    }

    /**
     * Checks specified parameters.
     *
     * @param parameters The parameters to check
     * @throws OXException If parameters' check fails
     */
    protected abstract void checkParameters(P parameters) throws OXException;

    @Override
    public String getOptionalIdentifier() {
        return optionalIdentifier;
    }

}
