/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.deputy.provider.imap.doveadm;

import static com.openexchange.deputy.Permission.CREATE_OBJECTS_IN_FOLDER;
import static com.openexchange.deputy.Permission.CREATE_SUB_FOLDERS;
import static com.openexchange.deputy.Permission.DELETE_ALL_OBJECTS;
import static com.openexchange.deputy.Permission.READ_ALL_OBJECTS;
import static com.openexchange.deputy.Permission.READ_FOLDER;
import static com.openexchange.deputy.Permission.WRITE_ALL_OBJECTS;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import com.openexchange.deputy.DefaultPermission;
import com.openexchange.deputy.DefaultPermission.Builder;
import com.openexchange.java.Strings;
import com.openexchange.deputy.Permission;

/**
 * {@link DoveAdmAclRight} - An enumeration of known ACL rights of the DoveAdm API.
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @see <a href="https://doc.dovecot.org/configuration_manual/acl/#acl-file-format">ACL right</a>
 */
public enum DoveAdmAclRight {

    /** Mailbox is visible in mailbox list. Mailbox can be subscribed to. */
    LOOKUP("lookup", 'l'),
    /** Mailbox can be opened for reading. */
    READ("read", 'r'),
    /** Message flags and keywords can be changed, except Seen and Deleted */
    WRITE("write", 'w'),
    /** Seen flag can be changed */
    WRITE_SEEN("write-seen", 's'),
    /** Deleted flag can be changed */
    WRITE_DELETED("write-deleted", 't'),
    /** Messages can be written or copied to the mailbox */
    INSERT("insert", 'i'),
    /** Messages can be posted to the mailbox by Dovecot LDA, e.g. from Pigeonhole Sieve Interpreter */
    POST("post", 'p'),
    /** Messages can be expunged */
    EXPUNGE("expunge", 'e'),
    /** Mailboxes can be created (or renamed) directly under this mailbox (but not necessarily under its children, see ACL Inheritance section above) (renaming also requires delete rights) */
    CREATE("create", 'k'),
    /** Mailbox can be deleted */
    DELETE("delete", 'x'),
    /** Administration rights to the mailbox (currently: ability to change ACLs for mailbox) */
    ADMIN("admin", 'a'),
    ;

    private final String identifier;
    private final char right;

    private DoveAdmAclRight(String identifier, char right) {
        this.identifier = identifier;
        this.right = right;
    }

    /**
     * Gets the right.
     *
     * @return The right
     */
    public char getRight() {
        return right;
    }

    /**
     * Gets the identifier.
     *
     * @return The identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Parses specified space-separated list of ACL rights; e.g.
     * <pre>
     *   "insert lookup read write write-seen"
     * </pre>
     *
     * @param rightsList The space-separated list of ACL rights
     * @return The ACL rights
     */
    public static Set<DoveAdmAclRight> aclRightsFor(String rightsList) {
        if (Strings.isEmpty(rightsList)) {
            return Collections.emptySet();
        }

        Set<DoveAdmAclRight> rights = EnumSet.noneOf(DoveAdmAclRight.class);
        for (String identifier : Strings.splitBy(rightsList, ' ', false)) {
            DoveAdmAclRight aclRight = aclRightFor(identifier);
            if (aclRight != null) {
                rights.add(aclRight);
            }
        }
        return rights;
    }

    /**
     * Gets the ACL right for given identifier.
     *
     * @param identifier The identifier to look-up
     * @return The associated ACL right or <code>null</code>
     */
    public static DoveAdmAclRight aclRightFor(String identifier) {
        if (Strings.isEmpty(identifier)) {
            return null;
        }

        String id = Strings.asciiLowerCase(identifier.trim());
        for (DoveAdmAclRight aclRight : values()) {
            if (id.equals(aclRight.getIdentifier())) {
                return aclRight;
            }
        }
        return null;
    }

    /**
     * Translates {@link Permission}s to dedicates {@link DoveAdmAclRight}s
     *
     * @param permission The permissions to translate
     * @return The rights to grant
     * @see com.openexchange.imap.acl.RFC4314ACLExtension
     * @see com.openexchange.deputy.Permission.DELETE_ALL_OBJECTS
     */
    public static Set<DoveAdmAclRight> getRights(Permission permission) {
        Set<DoveAdmAclRight> rights = EnumSet.noneOf(DoveAdmAclRight.class);
        if (permission.getFolderPermission() >= CREATE_SUB_FOLDERS) {
            rights.add(DoveAdmAclRight.CREATE);
            rights.add(DoveAdmAclRight.INSERT);
            rights.add(DoveAdmAclRight.LOOKUP);
        }
        if (permission.getFolderPermission() >= CREATE_OBJECTS_IN_FOLDER) {
            rights.add(DoveAdmAclRight.INSERT);
            rights.add(DoveAdmAclRight.LOOKUP);
        }
        if (permission.isVisible()) {
            rights.add(DoveAdmAclRight.LOOKUP);
        }
        if (permission.getReadPermission() >= READ_ALL_OBJECTS) {
            rights.add(DoveAdmAclRight.READ);
        }
        if (permission.getWritePermission() >= WRITE_ALL_OBJECTS) {
            rights.add(DoveAdmAclRight.WRITE);
        }
        if (permission.getDeletePermission() >= DELETE_ALL_OBJECTS) {
            rights.add(DELETE);
        }
        if (permission.isAdmin()) {
            rights.add(DoveAdmAclRight.ADMIN);
        }
        if (!rights.isEmpty()) {
            rights.add(POST);
        }
        return rights;
    }

    /**
     * Get permissions based in given ACL rights
     *
     * @param rights The rights
     * @return The permission
     */
    public static Permission getPermission(Set<DoveAdmAclRight> rights) {
        Builder builder = DefaultPermission.builder();
        if (null == rights || rights.isEmpty()) {
            return builder.build();
        }
        if (rights.contains(CREATE)) {
            builder.withFolderPermission(CREATE_SUB_FOLDERS);
        } else if (rights.contains(INSERT)) {
            builder.withFolderPermission(CREATE_OBJECTS_IN_FOLDER);
        } else if (rights.contains(LOOKUP)) {
            builder.withFolderPermission(READ_FOLDER);
        }
        if (rights.contains(READ)) {
            builder.withFolderPermission(READ_ALL_OBJECTS);
        }
        if (rights.contains(WRITE)) {
            builder.withFolderPermission(WRITE_ALL_OBJECTS);
        }
        if (rights.contains(DELETE)) {
            builder.withFolderPermission(DELETE_ALL_OBJECTS);
        }
        if (rights.contains(ADMIN)) {
            builder.withAdmin(true);
        }
        return builder.build();
    }

    /**
     * Gets the ACL rights from RFC 4314 (IMAP ACL extension) rights string; e.g <code>"lrwstipekxa"</code>.
     *
     * @param rfc4314Rights The RFC 4314 rights string
     * @return The rights
     */
    public static Set<DoveAdmAclRight> getRightsFromRFC4314(String rfc4314Rights) {
        Set<DoveAdmAclRight> rights = EnumSet.noneOf(DoveAdmAclRight.class);
        for (DoveAdmAclRight aclRight : values()) {
            if (rfc4314Rights.indexOf(aclRight.getRight()) >= 0) {
                rights.add(aclRight);
            }
        }
        return rights;
    }

    /**
     * Gets the rights string according to RFC 4314 (IMAP ACL extension); e.g <code>"lrwstipekxa"</code>.
     *
     * @param rights The ACL rights
     * @return The rights string
     */
    public static String getRFC4314Rights(Set<DoveAdmAclRight> rights) {
        if (rights == null || rights.isEmpty()) {
            return "";
        }

        StringBuilder sb = null;
        for (DoveAdmAclRight aclRight : values()) {
            if (rights.contains(aclRight)) {
                if (sb == null) {
                    sb = new StringBuilder(12);
                }
                sb.append(aclRight.getRight());
            }
        }
        return sb == null ? "" : sb.toString();
    }

}
