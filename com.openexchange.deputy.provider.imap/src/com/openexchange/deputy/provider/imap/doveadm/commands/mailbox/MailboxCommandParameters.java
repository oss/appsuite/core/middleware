/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.deputy.provider.imap.doveadm.commands.mailbox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import com.google.common.collect.ImmutableList;
import com.openexchange.deputy.provider.imap.doveadm.commands.DoveAdmCommandParameters;

/**
 *
 * {@link MailboxCommandParameters} - Simple container for parameters passed to mailbox-related DoveAdm commands.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class MailboxCommandParameters implements DoveAdmCommandParameters {

    /**
     * Creates a new builder for an instance of {@code MailboxCommandParameters}.
     *
     * @return The newly created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * The builder for an instance of {@code MailboxCommandParameters}.
     */
    public static final class Builder {

        private String user;
        private List<String> mailboxMask;

        /**
         * Initializes a new {@link Builder}.
         */
        private Builder() {
            super();
        }

        /**
         * Sets the UID of user to apply to.
         *
         * @param user The user identifier
         * @return This builder
         */
        public Builder withUser(String user) {
            this.user = user;
            return this;
        }

        /**
         * Sets the mailbox mask.
         *
         * @param mailboxMask The mailbox mask
         * @return This builder
         */
        public Builder withMailboxMask(List<String> mailboxMask) {
            this.mailboxMask = mailboxMask == null ? null : new ArrayList<>(mailboxMask);
            return this;
        }

        /**
         * Adds given mailbox mask entry; e.g. <code>"INBOX/*"</code>.
         *
         * @param mailboxMaskEntry The mailbox mask entry to add
         * @return This builder
         */
        public Builder addMailboxMaskEntry(String mailboxMaskEntry) {
            if (mailboxMaskEntry != null) {
                if (mailboxMask == null) {
                    mailboxMask = new ArrayList<>();
                }
                mailboxMask.add(mailboxMaskEntry);
            }
            return this;
        }

        /**
         * Builds the instance of {@code MailboxCommandParameters} from this builder's arguments.
         *
         * @return The parameters
         */
        public MailboxCommandParameters build() {
            return new MailboxCommandParameters(this);
        }
    }

    // ------------------------------------------------------------------------------------------------------------------

    private final String user;
    private final ImmutableList<String> mailboxMask;

    /**
     * Initializes a new instance of {@link MailboxCommandParameters}.
     *
     * @param builder The builder to get arguments from
     */
    private MailboxCommandParameters(Builder builder) {
        super();
        this.user = builder.user;
        this.mailboxMask = builder.mailboxMask == null ? null : ImmutableList.copyOf(builder.mailboxMask);
    }

    @Override
    public String getUser() {
        return user;
    }

    /**
     * Gets the mailbox mask; e.g. <code>["INBOX/*"]</code>.
     *
     * @return The mailbox mask
     */
    public ImmutableList<String> getMailboxMask() {
        return mailboxMask;
    }

    @Override
    public Map<String, Object> toRequestBody() {
        Map<String, Object> params = new HashMap<>(4, 0.9F);
        if (null != user) {
            params.put("user", user);
        }
        if (null != mailboxMask && !mailboxMask.isEmpty()) {
            // Translate to API rights parameter
            params.put("mailboxMask", JSONArray.jsonArrayFor(mailboxMask));
        }
        return params;
    }

}
