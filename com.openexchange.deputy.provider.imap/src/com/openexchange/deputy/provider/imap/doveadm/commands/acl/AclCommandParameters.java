/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.deputy.provider.imap.doveadm.commands.acl;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.openexchange.deputy.provider.imap.doveadm.DoveAdmAclRight;
import com.openexchange.deputy.provider.imap.doveadm.commands.DoveAdmMailboxCommandParameters;
import com.openexchange.java.Strings;

/**
 *
 * {@link AclCommandParameters} - Simple container for parameters passed to ACL-related DoveAdm commands.
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public final class AclCommandParameters implements DoveAdmMailboxCommandParameters {

    /**
     * Creates a new builder for an instance of {@code AclCommandParameters}.
     *
     * @return The newly created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * The builder for an instance of {@code AclCommandParameters}.
     */
    public static final class Builder {

        private String user;
        private String mailbox;
        private String aclId;
        private Set<DoveAdmAclRight> right;

        /**
         * Initializes a new {@link Builder}.
         */
        private Builder() {
            super();
        }

        /**
         * Sets the UID of user to apply to.
         *
         * @param user The user identifier
         * @return This builder
         */
        public Builder withUser(String user) {
            this.user = user;
            return this;
        }

        /**
         * Sets the mailbox name (aka full name).
         *
         * @param mailbox The mailbox name
         * @return This builder
         */
        public Builder withMailbox(String mailbox) {
            this.mailbox = mailbox;
            return this;
        }

        /**
         * Sets given ACL identifier.
         *
         * @param aclId The ACL identifier
         * @return This builder
         */
        public Builder withAclId(String aclId) {
            this.aclId = aclId;
            return this;
        }

        /**
         * Sets the given rights.
         *
         * @param right The rights to set
         * @return This builder
         */
        public Builder withRights(Set<DoveAdmAclRight> right) {
            this.right = right == null ? null : EnumSet.copyOf(right);
            return this;
        }

        /**
         * Adds given right to this builder
         *
         * @param right The right
         * @return This builder
         */
        public Builder addRight(DoveAdmAclRight right) {
            if (right != null) {
                if (this.right == null) {
                    this.right = EnumSet.of(right);
                } else {
                    this.right.add(right);
                }
            }
            return this;
        }

        /**
         * Builds the instance of {@code AclCommandParameters} from this builder's arguments.
         *
         * @return The parameters
         */
        public AclCommandParameters build() {
            return new AclCommandParameters(this);
        }
    }

    // ------------------------------------------------------------------------------------------------------------------

    private final String user;
    private final String mailbox;
    private final String aclId;
    private final ImmutableSet<DoveAdmAclRight> right;

    /**
     * Initializes a new instance of {@link AclCommandParameters}.
     *
     * @param builder The builder to get arguments from
     */
    private AclCommandParameters(Builder builder) {
        super();
        this.user = builder.user;
        this.mailbox = builder.mailbox;
        this.aclId = builder.aclId;
        this.right = builder.right == null ? null : Sets.immutableEnumSet(builder.right);
    }

    @Override
    public String getUser() {
        return user;
    }

    @Override
    public String getMailbox() {
        return mailbox;
    }

    /**
     * Gets the ACL identifier.
     *
     * @return The ACL identifier, e.g. <code>"user=anton@exmaple.org"</code>
     */
    public String getAclId() {
        return aclId;
    }

    /**
     * Gets the right(s) to set.
     *
     * @return The right(s)
     */
    public Set<DoveAdmAclRight> getRight() {
        return right;
    }

    @Override
    public Map<String, Object> toRequestBody() {
        Map<String, Object> params = new HashMap<>(4, 0.9F);
        if (null != user) {
            params.put("user", user);
        }
        if (Strings.isNotEmpty(mailbox)) {
            params.put("mailbox", mailbox);
        }
        if (null != aclId) {
            params.put("id", aclId);
        }
        if (null != right && !right.isEmpty()) {
            // Translate to API rights parameter
            params.put("right", new JSONArray(right.stream().map(r -> r.getIdentifier()).toList()));
        }
        return params;
    }

}
