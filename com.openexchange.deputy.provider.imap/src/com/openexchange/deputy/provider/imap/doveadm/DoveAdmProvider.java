/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.deputy.provider.imap.doveadm;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.context.ContextService;
import com.openexchange.deputy.DefaultPermission;
import com.openexchange.deputy.DeputyExceptionCode;
import com.openexchange.deputy.DeputyInfo;
import com.openexchange.deputy.ModulePermission;
import com.openexchange.deputy.Permission;
import com.openexchange.deputy.Permissions;
import com.openexchange.deputy.DefaultPermission.Builder;
import com.openexchange.deputy.provider.imap.AbstractMailProvider;
import com.openexchange.deputy.provider.imap.DeputyImapProviderExceptionCode;
import com.openexchange.deputy.provider.imap.doveadm.commands.DoveAdmCommands;
import com.openexchange.dovecot.doveadm.client.DoveAdmClient;
import com.openexchange.exception.OXException;
import com.openexchange.group.GroupService;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.imap.util.IMAPDeputyMetadataUtility;
import com.openexchange.java.Reference;
import com.openexchange.mail.api.MailConfig;
import com.openexchange.mail.utils.MailFolderUtility;
import com.openexchange.mailaccount.Account;
import com.openexchange.mailaccount.MailAccount;
import com.openexchange.mailaccount.MailAccountStorageService;
import com.openexchange.server.ServiceLookup;
import com.openexchange.server.impl.OCLPermission;
import com.openexchange.session.Session;
import com.openexchange.user.User;
import com.openexchange.user.UserService;
import com.sun.mail.imap.ACL;

/**
 * {@link DoveAdmProvider} - The deputy mail provider using DoveAdm REST API.
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class DoveAdmProvider extends AbstractMailProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(DoveAdmProvider.class);

    private final DoveAdmClient doveAdmClient;

    /**
     * Initializes a new {@link DoveAdmProvider}.
     *
     * @param doveAdmClient The DoveAdm client
     * @param services The service look-up
     */
    public DoveAdmProvider(DoveAdmClient doveAdmClient, ServiceLookup services) {
        super(false, services);
        this.doveAdmClient = doveAdmClient;
    }

    /**
     * Checks that account identifier refers to default mail account.
     *
     * @param accountId The account identifier
     * @param session The session providing user information
     * @return <code>true</code> if account identifier is okay; otherwise <code>false</code>
     */
    private static boolean checkAccountId(int accountId, Session session) {
        if (Account.DEFAULT_ID != accountId) {
            LOGGER.debug("Unable to perform deputy permission operation for account {} of user {} in context {}", I(accountId), I(session.getUserId()), I(session.getContextId()));
            return false;
        }
        return true;
    }

    @Override
    public boolean isApplicable(Optional<ModulePermission> optionalModulePermission, Session session) throws OXException {
        if (null != session && b(session.getParameterOrElse(com.openexchange.deputy.AdministrativeDeputyService.SESSION_PARAM_ADMINISTRATIVE_FLAG, Boolean.FALSE))) {
            return super.isApplicable(optionalModulePermission, session);
        }
        return false;
    }

    @Override
    protected void grantDeputyPermissionByFolders(int accountId, Collection<String> folderIds, DeputyInfo deputyInfo, Permission permission, Session session) throws OXException {
        if (!checkAccountId(accountId, session)) {
            return;
        }

        // Determine deputy's ACL name
        String aclName = getACLName(permission, session.getContextId());
        String optDeputyUser = permission.isGroup() ? null : doveAdmClient.checkUser(getMailLogin(permission.getEntity(), session.getContextId()), permission.getEntity(), session.getContextId());

        // Prepare to set ACL via DoveAdm
        String aclId = getAclId(permission.isGroup(), aclName);
        Set<DoveAdmAclRight> rights = DoveAdmAclRight.getRights(permission);
        String user = doveAdmClient.checkUser(getMailLogin(session.getUserId(), session.getContextId()), session.getUserId(), session.getContextId());

        List<String> foldersToClear = new ArrayList<String>(folderIds.size());
        try {
            // Apply to each folder
            for (String folderId : folderIds) {
                // Determine full name (aka mailbox)
                String fullName = MailFolderUtility.prepareMailFolderParamOrElseReturn(folderId);

                // Check possibly existent deputy meta-data in JSON format
                Optional<JSONObject> optionalMetadata = DoveAdmCommands.getDeputyMetadata(user, fullName, doveAdmClient);
                if (optionalMetadata.isPresent()) {
                    // Check existence of deputy meta-data
                    JSONObject jMetadata = optionalMetadata.get();
                    String removeKey = null;
                    for (Map.Entry<String, Object> e : jMetadata.entrySet()) {
                        JSONObject jDeputyMetadata = (JSONObject) e.getValue();
                        if (aclName.equals(jDeputyMetadata.optString("name", null)) || deputyInfo.getEntityId() == jDeputyMetadata.optInt("entity", -1)) {
                            if (VERSION == jDeputyMetadata.optInt("version", 0)) {
                                // Cannot grant deputy permission since already present
                                throw DeputyImapProviderExceptionCode.DUPLICATE_DEPUTY_PERMISSION.create(folderId);
                            }
                            // Version mismatch
                            removeKey = e.getKey();
                            break;
                        }
                    }
                    if (removeKey != null) {
                        jMetadata.remove(removeKey);
                    }
                }

                // Get possibly existent ACL for that deputy
                DoveAdmACL existentAcl = DoveAdmCommands.getAcl(user, fullName, doveAdmClient).get(aclId);

                JSONObject jDeputyMetadata;
                if (existentAcl == null) {
                    // Set ACL
                    DoveAdmCommands.setAcl(user, fullName, aclId, rights, doveAdmClient);
                    foldersToClear.add(folderId);
                    jDeputyMetadata = IMAPDeputyMetadataUtility.createDeputyMetadata(deputyInfo.getDeputyId(), deputyInfo.getEntityId(), Permissions.createPermissionBits(permission), aclName, VERSION, (ACL) null, optionalMetadata);
                } else {
                    DoveAdmCommands.deleteAcl(user, fullName, aclId, doveAdmClient);
                    foldersToClear.add(folderId);
                    DoveAdmCommands.setAcl(user, fullName, aclId, rights, doveAdmClient);
                    jDeputyMetadata = IMAPDeputyMetadataUtility.createDeputyMetadata(deputyInfo.getDeputyId(), deputyInfo.getEntityId(), Permissions.createPermissionBits(permission), aclName, VERSION, DoveAdmAclRight.getRFC4314Rights(existentAcl.getRights()), optionalMetadata);
                }
                DoveAdmCommands.setDeputyMetadata(user, fullName, jDeputyMetadata, doveAdmClient);
                if (optDeputyUser != null) {
                    try {
                        DoveAdmCommands.subscribeMailbox(optDeputyUser, fullName, doveAdmClient);
                    } catch (Exception e) {
                        LOGGER.error("Fail to subscribe to {} for deputy {} (user={}, context={})", fullName, optDeputyUser, I(permission.getEntity()), I(session.getContextId()), e);
                    }
                }
            }
        } finally {
            if (!foldersToClear.isEmpty()) {
                clearCaches(foldersToClear, accountId, deputyInfo, session);
            }
        }
    }

    @Override
    protected void updateDeputyPermissionByFolders(int accountId, Collection<String> folderIds, DeputyInfo deputyInfo, Permission permission, Session session) throws OXException {
        if (!checkAccountId(accountId, session)) {
            return;
        }

        // Determine deputy's ACL name
        String aclName = getACLName(permission, session.getContextId());
        String optDeputyUser = permission.isGroup() ? null : doveAdmClient.checkUser(getMailLogin(permission.getEntity(), session.getContextId()), permission.getEntity(), session.getContextId());

        // Prepare to update ACL via DoveAdm
        Context context = getContext(session);
        String aclId = getAclId(permission.isGroup(), aclName);
        Set<DoveAdmAclRight> rights = DoveAdmAclRight.getRights(permission);
        String user = doveAdmClient.checkUser(getMailLogin(session.getUserId(), session.getContextId()), session.getUserId(), session.getContextId());

        List<String> foldersToClear = new ArrayList<String>(folderIds.size());
        try {
            Boolean entityExists = null;
            for (String folderId : folderIds) {
                String fullName = MailFolderUtility.prepareMailFolderParamOrElseReturn(folderId);

                // Check existence of meta-data
                Optional<JSONObject> optionalMetadata = DoveAdmCommands.getDeputyMetadata(user, fullName, doveAdmClient);
                if (!optionalMetadata.isPresent()) {
                    // Cannot update deputy permission since not present
                    throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                }

                // Check existence of deputy meta-data
                JSONObject jMetadata = optionalMetadata.get();
                JSONObject jDeputyMetadata = jMetadata.optJSONObject(deputyInfo.getDeputyId());
                if (jDeputyMetadata == null || VERSION != jDeputyMetadata.optInt("version", 0)) {
                    // Version mismatch
                    throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                }

                // Check entity existence
                if (entityExists == null) {
                    entityExists = Boolean.valueOf(existsEntity(deputyInfo, context));
                }

                if (entityExists.booleanValue() == false) {
                    //  Entity does no more exist
                    jMetadata.remove(deputyInfo.getDeputyId());
                    if (jMetadata.isEmpty()) {
                        DoveAdmCommands.unsetDeputyMetadata(user, fullName, doveAdmClient);
                    } else {
                        DoveAdmCommands.setDeputyMetadata(user, fullName, jMetadata, doveAdmClient);
                    }
                } else {
                    if (!aclName.equals(jDeputyMetadata.optString("name", null))) {
                        // Deputy ACL name mismatch
                        throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                    }

                    // Get existent ACL for that deputy
                    DoveAdmACL existentAcl = DoveAdmCommands.getAcl(user, fullName, doveAdmClient).get(aclId);
                    if (existentAcl == null) {
                        throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                    }

                    DoveAdmCommands.deleteAcl(user, fullName, aclId, doveAdmClient);
                    foldersToClear.add(folderId);
                    DoveAdmCommands.setAcl(user, fullName, aclId, rights, doveAdmClient);

                    jDeputyMetadata.put("permission", Permissions.createPermissionBits(permission));
                    DoveAdmCommands.setDeputyMetadata(user, fullName, jMetadata, doveAdmClient);
                    if (optDeputyUser != null) {
                        try {
                            DoveAdmCommands.subscribeMailbox(optDeputyUser, fullName, doveAdmClient);
                        } catch (Exception e) {
                            LOGGER.error("Fail to subscribe to {} for deputy {} (user={}, context={})", fullName, optDeputyUser, I(permission.getEntity()), I(session.getContextId()), e);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            throw DeputyExceptionCode.JSON_ERROR.create(e, e.getMessage());
        } finally {
            if (!foldersToClear.isEmpty()) {
                clearCaches(foldersToClear, accountId, deputyInfo, session);
            }
        }
    }

    @Override
    protected void revokeDeputyPermissionByFolders(int accountId, Collection<String> folderIds, DeputyInfo deputyInfo, Session session) throws OXException {
        if (!checkAccountId(accountId, session)) {
            return;
        }

        // Determine deputy's ACL name
        String aclName = getACLName(deputyInfo, session.getContextId());
        String optDeputyUser = deputyInfo.isGroup() ? null : doveAdmClient.checkUser(getMailLogin(deputyInfo.getEntityId(), session.getContextId()), deputyInfo.getEntityId(), session.getContextId());

        // Prepare to update ACL via DoveAdm
        Context context = getContext(session);
        String aclId = getAclId(deputyInfo.isGroup(), aclName);
        String user = doveAdmClient.checkUser(getMailLogin(session.getUserId(), session.getContextId()), session.getUserId(), session.getContextId());

        List<String> foldersToClear = new ArrayList<String>(folderIds.size());
        try {
            Boolean entityExists = null;
            for (String folderId : folderIds) {
                String fullName = MailFolderUtility.prepareMailFolderParamOrElseReturn(folderId);

                // Check existence of meta-data
                Optional<JSONObject> optionalMetadata = DoveAdmCommands.getDeputyMetadata(user, fullName, doveAdmClient);
                if (!optionalMetadata.isPresent()) {
                    // Cannot update deputy permission since not present
                    throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                }

                // Check existence of deputy meta-data
                JSONObject jMetadata = optionalMetadata.get();
                JSONObject jDeputyMetadata = jMetadata.optJSONObject(deputyInfo.getDeputyId());
                if (jDeputyMetadata == null || VERSION != jDeputyMetadata.optInt("version", 0)) {
                    // Version mismatch
                    throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                }

                // Check entity existence
                if (entityExists == null) {
                    entityExists = Boolean.valueOf(existsEntity(deputyInfo, context));
                }

                if (entityExists.booleanValue() == false) {
                    //  Entity does no more exist
                    jMetadata.remove(deputyInfo.getDeputyId());
                    if (jMetadata.isEmpty()) {
                        DoveAdmCommands.unsetDeputyMetadata(user, fullName, doveAdmClient);
                    } else {
                        DoveAdmCommands.setDeputyMetadata(user, fullName, jMetadata, doveAdmClient);
                    }
                } else {
                    if (!aclName.equals(jDeputyMetadata.optString("name", null))) {
                        // Deputy ACL name mismatch
                        throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                    }

                    DoveAdmCommands.deleteAcl(user, fullName, aclId, doveAdmClient);
                    foldersToClear.add(folderId);

                    String previousRights = jDeputyMetadata.optString("previousRights", null);
                    if (previousRights != null) {
                        DoveAdmCommands.setAcl(user, fullName, aclId, DoveAdmAclRight.getRightsFromRFC4314(previousRights), doveAdmClient);
                    }

                    jMetadata.remove(deputyInfo.getDeputyId());
                    if (jMetadata.isEmpty()) {
                        DoveAdmCommands.unsetDeputyMetadata(user, fullName, doveAdmClient);
                    } else {
                        DoveAdmCommands.setDeputyMetadata(user, fullName, jMetadata, doveAdmClient);
                    }
                    if (optDeputyUser != null) {
                        try {
                            DoveAdmCommands.unsubscribeMailbox(optDeputyUser, fullName, doveAdmClient);
                        } catch (Exception e) {
                            LOGGER.error("Fail to unsubscribe from {} for deputy {} (user={}, context={})", fullName, optDeputyUser, I(deputyInfo.getEntityId()), I(session.getContextId()), e);
                        }
                    }
                }
            }
        } finally {
            if (!foldersToClear.isEmpty()) {
                clearCaches(foldersToClear, accountId, deputyInfo, session);
            }
        }
    }

    @Override
    protected void revokeDeputyPermissionForAccount(int accountId, DeputyInfo deputyInfo, Session session) throws OXException {
        if (!checkAccountId(accountId, session)) {
            return;
        }

        // Assume only for INBOX
        Collection<String> folderIds = Collections.singletonList(MailFolderUtility.prepareFullname(Account.DEFAULT_ID, DEFAULT_FOLDER_NAME));

        // Revoke...
        revokeDeputyPermissionByFolders(accountId, folderIds, deputyInfo, session);
    }

    @Override
    protected List<String> getDeputyPermissionFrom(int accountId, Reference<Permission> permissionReference, DeputyInfo deputyInfo, Session session) throws OXException {
        if (!checkAccountId(accountId, session)) {
            return List.of();
        }

        // Assume only for INBOX
        String folderId = MailFolderUtility.prepareFullname(Account.DEFAULT_ID, DEFAULT_FOLDER_NAME);
        String fullName = DEFAULT_FOLDER_NAME;

        // Prepare to get ACL via DoveAdm
        Context context = getContext(session);
        String user = doveAdmClient.checkUser(getMailLogin(session.getUserId(), session.getContextId()), session.getUserId(), session.getContextId());

        // Check existence of meta-data
        Optional<JSONObject> optionalMetadata = DoveAdmCommands.getDeputyMetadata(user, fullName, doveAdmClient);
        if (!optionalMetadata.isPresent()) {
            // Cannot get deputy permission since not present
            return Collections.emptyList();
        }

        // Check existence of deputy meta-data
        JSONObject jMetadata = optionalMetadata.get();
        JSONObject jDeputyMetadata = jMetadata.optJSONObject(deputyInfo.getDeputyId());
        if (jDeputyMetadata == null || VERSION != jDeputyMetadata.optInt("version", 0)) {
            // Version mismatch
            return Collections.emptyList();
        }

        // Check entity existence
        List<String> folderIds = new ArrayList<String>(1);
        if (!existsEntity(deputyInfo, context)) {
            //  Entity does no more exist
            jMetadata.remove(deputyInfo.getDeputyId());
            if (jMetadata.isEmpty()) {
                DoveAdmCommands.unsetDeputyMetadata(user, fullName, doveAdmClient);
            } else {
                DoveAdmCommands.setDeputyMetadata(user, fullName, jMetadata, doveAdmClient);
            }
        } else {
            folderIds.add(folderId);
            if (permissionReference.hasNoValue()) {
                int[] bits = Permissions.parsePermissionBits(jDeputyMetadata.optInt("permission", 0));
                Builder tmp = DefaultPermission.builder()
                                               .withFolderPermission(bits[0])
                                               .withReadPermission(bits[1])
                                               .withWritePermission(bits[2])
                                               .withDeletePermission(bits[3])
                                               .withAdmin(bits[4] > 0)
                                               .withEntity(deputyInfo.getEntityId())
                                               .withGroup(deputyInfo.isGroup());
                permissionReference.setValue(tmp.build());
            }
        }
        return folderIds;
    }

    /*
     * ============================== HELPERS ==============================
     */

    /**
     * Gets the name for the ACL.
     *
     * @param permission The permission to get the deputy name from
     * @param contextId The context identifier
     * @return The deputy's name; e.g. <code>"anton@example.org"</code>
     * @throws OXException If ACL name cannot be returned
     */
    private String getACLName(Permission permission, int contextId) throws OXException {
        return getACLName(contextId, permission.getEntity(), permission.isGroup());
    }

    /**
     * Gets the name for the ACL.
     *
     * @param deputyInfo The info
     * @param contextId The context identifier
     * @return The deputy's name; e.g. <code>"anton@example.org"</code>
     * @throws OXException If ACL name cannot be returned
     */
    private String getACLName(DeputyInfo deputyInfo, int contextId) throws OXException {
        return getACLName(contextId, deputyInfo.getEntityId(), deputyInfo.isGroup());
    }

    /**
     * Gets the name for the ACL.
     *
     * @param contextId The context identifier
     * @param entity The entity identifier
     * @param isGroup <code>true</code> if the entity is a group, <code>false</code> otherwise
     * @return The deputy's name; e.g. <code>"anton@example.org"</code>
     * @throws OXException If ACL name cannot be returned
     */
    private String getACLName(int contextId, int entity, boolean isGroup) throws OXException {
        if (entity == OCLPermission.ALL_GROUPS_AND_USERS) {
            return DoveAdmAclType.ANYONE.getIdentifier();
        }

        if (isGroup) {
            // Don't know better
            Context context = services.getServiceSafe(ContextService.class).getContext(contextId);
            return services.getServiceSafe(GroupService.class).getGroup(context, entity).getSimpleName();
        }

        return getMailLogin(entity, contextId);
    }

    /**
     * Gets the mail login for the given user.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The mail login for the user; e.g. <code>"anton@example.org"</code>
     * @throws OXException If mail login cannot be returned
     */
    private String getMailLogin(int userId, int contextId) throws OXException {
        MailAccount defaultMailAccount = services.getServiceSafe(MailAccountStorageService.class).getDefaultMailAccount(userId, contextId);
        User user = services.getServiceSafe(UserService.class).getUser(userId, contextId);
        return MailConfig.getMailLogin(defaultMailAccount, user.getLoginInfo(), userId, contextId);
    }

    /**
     * Get the ACL identifier to pass to DoveAdm.
     *
     * @param isGroup <code>true</code> if the ACL is generated for a group, <code>false</code> otherwise
     * @param aclName The ACL name
     * @return The ACL identifier; e.g. <code>"user=anton@example.org"</code>
     */
    private static String getAclId(boolean isGroup, String aclName) {
        return (isGroup ? DoveAdmAclType.GROUP : DoveAdmAclType.USER).toACLId(aclName);
    }

}
