/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.deputy.provider.imap.doveadm.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.json.JSONObject;
import org.json.JSONServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.deputy.provider.imap.doveadm.DoveAdmACL;
import com.openexchange.deputy.provider.imap.doveadm.DoveAdmAclRight;
import com.openexchange.deputy.provider.imap.doveadm.commands.acl.AclCommandParameters;
import com.openexchange.deputy.provider.imap.doveadm.commands.acl.AclDeleteCommand;
import com.openexchange.deputy.provider.imap.doveadm.commands.acl.AclGetCommand;
import com.openexchange.deputy.provider.imap.doveadm.commands.acl.AclSetCommand;
import com.openexchange.deputy.provider.imap.doveadm.commands.mailbox.MailboxCommandParameters;
import com.openexchange.deputy.provider.imap.doveadm.commands.mailbox.MailboxListCommand;
import com.openexchange.deputy.provider.imap.doveadm.commands.metadata.MetadataCommandParameters;
import com.openexchange.deputy.provider.imap.doveadm.commands.metadata.MetadataGetCommand;
import com.openexchange.deputy.provider.imap.doveadm.commands.metadata.MetadataSetCommand;
import com.openexchange.deputy.provider.imap.doveadm.commands.metadata.MetadataUnsetCommand;
import com.openexchange.deputy.provider.imap.doveadm.commands.subscribe.SubscribeCommand;
import com.openexchange.deputy.provider.imap.doveadm.commands.subscribe.SubscribeCommandParameters;
import com.openexchange.deputy.provider.imap.doveadm.commands.subscribe.UnsubscribeCommand;
import com.openexchange.dovecot.doveadm.client.DoveAdmClient;
import com.openexchange.dovecot.doveadm.client.DoveAdmCommand;
import com.openexchange.dovecot.doveadm.client.DoveAdmResponse;
import com.openexchange.dovecot.doveadm.client.Result;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;

/**
 * {@link DoveAdmCommands} - Utility class for DoveAdm commands.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class DoveAdmCommands {

    private static final Logger LOGGER = LoggerFactory.getLogger(DoveAdmCommands.class);

    /**
     * Initializes a new instance of {@link DoveAdmCommands}.
     */
    private DoveAdmCommands() {
        super();
    }

    /**
     * General check for a DoveAdm error response.
     *
     * @param response The response to check
     * @throws OXException If response is an error response from DoveAdm end-point
     */
    public static void checkForError(DoveAdmResponse response) throws OXException {
        if (response.isError()) {
            throw OXException.general("Received error for DoveAdm call: " + response.toString());
        }
    }

    /*
     * ============================== Doveadm ACLS calls ==============================
     */

    /**
     * Creates a new builder for an instance of {@code AclCommandParameters}.
     *
     * @return The newly created builder
     */
    private static AclCommandParameters.Builder aclParamsBuilder() {
        return AclCommandParameters.builder();
    }

    /**
     * Gets the ACL list for specified user.
     *
     * @param user The UID of the user
     * @param mailbox The mailbox (folder) name; e.g. <code>"INBOX"</code>
     * @param doveAdmClient The DoveAmd client to use
     * @return The ACL list mapped to ACL identifier
     * @throws OXException If command fails
     */
    public static Map<String, DoveAdmACL> getAcl(String user, String mailbox, DoveAdmClient doveAdmClient) throws OXException {
        DoveAdmResponse response = doveAdmClient.executeCommand(new AclGetCommand(aclParamsBuilder().withUser(user).withMailbox(mailbox).build()));
        checkForError(response);

        List<Result> results = response.asDataResponse().getResults();
        if (results == null || results.isEmpty()) {
            return Collections.emptyMap();
        }

        int numberOfResults = results.size();
        if (numberOfResults == 1) {
            Result aclGetResult = results.get(0);
            String aclId = aclGetResult.getValue("id");
            return Collections.singletonMap(aclId, new DoveAdmACL(aclId, DoveAdmAclRight.aclRightsFor(aclGetResult.getValue("rights"))));
        }

        Map<String, DoveAdmACL> acls = LinkedHashMap.newLinkedHashMap(numberOfResults);
        for (Result aclGetResult : results) {
            String aclId = aclGetResult.getValue("id");
            acls.put(aclId, new DoveAdmACL(aclId, DoveAdmAclRight.aclRightsFor(aclGetResult.getValue("rights"))));
        }
        return acls;
    }

    /**
     * Sets specified ACL for given user.
     *
     * @param user The UID of the user
     * @param mailbox The mailbox (folder) name; e.g. <code>"INBOX"</code>
     * @param aclId The identifier of the ACL; e.g. <code>"user=antone@example.org"</code>
     * @param rights The ACL rights to set
     * @param doveAdmClient The DoveAmd client to use
     * @throws OXException If command fails
     */
    public static void setAcl(String user, String mailbox, String aclId, Set<DoveAdmAclRight> rights, DoveAdmClient doveAdmClient) throws OXException {
        DoveAdmResponse response = doveAdmClient.executeCommand(new AclSetCommand(aclParamsBuilder().withUser(user).withMailbox(mailbox).withAclId(aclId).withRights(rights).build()));
        checkForError(response);
    }

    /**
     * Deletes specified ACL for given user.
     *
     * @param user The UID of the user
     * @param mailbox The mailbox (folder) name; e.g. <code>"INBOX"</code>
     * @param aclId The identifier of the ACL; e.g. <code>"user=antone@example.org"</code>
     * @param doveAdmClient The DoveAmd client to use
     * @throws OXException If command fails
     */
    public static void deleteAcl(String user, String mailbox, String aclId, DoveAdmClient doveAdmClient) throws OXException {
        DoveAdmResponse response = doveAdmClient.executeCommand(new AclDeleteCommand(aclParamsBuilder().withUser(user).withMailbox(mailbox).withAclId(aclId).build()));
        checkForError(response);
    }

    /*
     * ============================== Doveadm Subscribe calls ==============================
     */

    /**
     * Creates a new builder for an instance of {@code SubscribeCommandParameters}.
     *
     * @return The newly created builder
     */
    private static SubscribeCommandParameters.Builder subscribeParamsBuilder() {
        return SubscribeCommandParameters.builder();
    }

    /**
     * Subscribes specified mailbox to given user.
     *
     * @param user The UID of the user
     * @param mailbox The mailbox (folder) name; e.g. <code>"INBOX"</code>
     * @param doveAdmClient The DoveAmd client to use
     * @throws OXException If command fails
     */
    public static void subscribeMailbox(String user, String mailbox, DoveAdmClient doveAdmClient) throws OXException {
        DoveAdmResponse response = doveAdmClient.executeCommand(new SubscribeCommand(subscribeParamsBuilder().withUser(user).addMailboxEntry(mailbox).build()));
        checkForError(response);
    }

    /**
     * Unsubscribes specified mailbox from given user.
     *
     * @param user The UID of the user
     * @param mailbox The mailbox (folder) name; e.g. <code>"INBOX"</code>
     * @param doveAdmClient The DoveAmd client to use
     * @throws OXException If command fails
     */
    public static void unsubscribeMailbox(String user, String mailbox, DoveAdmClient doveAdmClient) throws OXException {
        DoveAdmResponse response = doveAdmClient.executeCommand(new UnsubscribeCommand(subscribeParamsBuilder().withUser(user).addMailboxEntry(mailbox).build()));
        checkForError(response);
    }

    /*
     * ============================== Doveadm mailbox calls ==============================
     */

    /**
     * Creates a new builder for an instance of {@code MailboxCommandParameters}.
     *
     * @return The newly created builder
     */
    private static MailboxCommandParameters.Builder mailboxParamsBuilder() {
        return MailboxCommandParameters.builder();
    }

    /**
     * Gets the mailbox list for specified user.
     *
     * @param user The UID of the user
     * @param doveAdmClient The DoveAmd client to use
     * @return The mailbox list
     * @throws OXException If command fails
     */
    public static List<String> getMailboxList(String user, DoveAdmClient doveAdmClient) throws OXException {
        DoveAdmResponse mailboxListResponse = doveAdmClient.executeCommand(new MailboxListCommand(mailboxParamsBuilder().withUser(user).build()));
        checkForError(mailboxListResponse);

        List<Result> results = mailboxListResponse.asDataResponse().getResults();
        if (results == null || results.isEmpty()) {
            // No folders...
            return Collections.emptyList();
        }

        List<String> mailboxNames = new ArrayList<>(results.size());
        for (Result result : results) {
            String mailbox = result.getValue("mailbox");
            if (mailbox != null) {
                mailboxNames.add(mailbox);
            }
        }
        return mailboxNames;
    }

    /*
     * ============================== Doveadm METADATA calls ==============================
     */

    /** The metadata key for deputy information */
    private static final String METADATA_KEY = "/shared/vendor/vendor.open-xchange/deputy";

    /**
     * Creates a new builder for an instance of {@code MetadataCommandParameters}.
     *
     * @return The newly created builder
     */
    private static MetadataCommandParameters.Builder metadataParamsBuilder() {
        return MetadataCommandParameters.builder();
    }

    /**
     * Gets the deputy METADATA for all mailboxes of given user.
     *
     * @param user The UID of the user
     * @param doveAdmClient The DoveAmd client to use
     * @return The deputy METADATA mapping
     * @throws OXException If operation fails
     */
    public static Map<String, JSONObject> getAllDeputyMetadata(String user, DoveAdmClient doveAdmClient) throws OXException {
        // Retrieve all folders
        List<String> mailboxNames = getMailboxList(user, doveAdmClient);

        // Compile "mailboxMetadataGet" command for each mailbox
        List<DoveAdmCommand> commands = new ArrayList<>(mailboxNames.size());
        for (String mailbox : mailboxNames) {
            commands.add(new MetadataGetCommand(metadataParamsBuilder().withUser(user).withMailbox(mailbox).withKey(METADATA_KEY).build()));
        }

        // Issue "mailboxMetadataGet" commands as batch
        List<DoveAdmResponse> metadataGetResponses = doveAdmClient.executeCommands(commands);

        // Handle responses
        Map<String, JSONObject> allDeputyMetadata = LinkedHashMap.newLinkedHashMap(metadataGetResponses.size());
        int i = 0;
        List<Result> results;
        for (DoveAdmResponse metadataGetResponse : metadataGetResponses) {
            checkForError(metadataGetResponse);
            results = metadataGetResponse.asDataResponse().getResults();
            if (results != null && !results.isEmpty()) {
                String value = results.get(0).getValue("value");
                if (value != null) {
                    try {
                        allDeputyMetadata.put(mailboxNames.get(i), JSONServices.parseObject(value));
                    } catch (Exception e) {
                        LOGGER.error("Invalid JSON deputy metadata: {}", value, e);
                    }
                }
            }
            i++;
        }
        return allDeputyMetadata;
    }

    /**
     * Gets the deputy METADATA associated with given mailbox for specified user.
     *
     * @param user The UID of the user
     * @param mailbox The mailbox (folder) name; e.g. <code>"INBOX"</code>
     * @param doveAdmClient The DoveAmd client to use
     * @return The optional deputy METADATA
     * @throws OXException If command fails
     */
    public static Optional<JSONObject> getDeputyMetadata(String user, String mailbox, DoveAdmClient doveAdmClient) throws OXException {
        DoveAdmResponse response = doveAdmClient.executeCommand(new MetadataGetCommand(metadataParamsBuilder().withUser(user).withMailbox(mailbox).withKey(METADATA_KEY).build()));
        checkForError(response);

        List<Result> results = response.asDataResponse().getResults();
        if (results == null || results.isEmpty()) {
            return Optional.empty();
        }

        String value = results.get(0).getValue("value");
        if (Strings.isNotEmpty(value)) {
            try {
                return Optional.of(JSONServices.parseObject(value));
            } catch (Exception e) {
                LOGGER.error("Invalid JSON deputy metadata: {}", value, e);
            }
        }
        return Optional.empty();
    }

    /**
     * Sets the deputy METADATA to given mailbox for specified user.
     *
     * @param user The UID of the user
     * @param mailbox The mailbox (folder) name; e.g. <code>"INBOX"</code>
     * @param jDeputyMetadata The deputy METADATA to set
     * @param doveAdmClient The DoveAmd client to use
     * @throws OXException If command fails
     */
    public static void setDeputyMetadata(String user, String mailbox, JSONObject jDeputyMetadata, DoveAdmClient doveAdmClient) throws OXException {
        DoveAdmResponse response = doveAdmClient.executeCommand(new MetadataSetCommand(metadataParamsBuilder().withUser(user).withMailbox(mailbox).withKey(METADATA_KEY).withValue(jDeputyMetadata.toString()).build()));
        checkForError(response);
    }

    /**
     * Unsets the deputy METADATA from given mailbox for specified user.
     *
     * @param user The UID of the user
     * @param mailbox The mailbox (folder) name; e.g. <code>"INBOX"</code>
     * @param doveAdmClient The DoveAmd client to use
     * @throws OXException If command fails
     */
    public static void unsetDeputyMetadata(String user, String mailbox, DoveAdmClient doveAdmClient) throws OXException {
        DoveAdmResponse response = doveAdmClient.executeCommand(new MetadataUnsetCommand(metadataParamsBuilder().withUser(user).withMailbox(mailbox).withKey(METADATA_KEY).build()));
        checkForError(response);
    }

}
