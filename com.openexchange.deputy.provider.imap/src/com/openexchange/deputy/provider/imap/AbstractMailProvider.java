/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.deputy.provider.imap;

import static com.openexchange.deputy.AdministrativeDeputyService.SESSION_PARAM_ADMINISTRATIVE_FLAG;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.b;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.slf4j.Logger;
import com.openexchange.deputy.DefaultModulePermission;
import com.openexchange.deputy.DeputyInfo;
import com.openexchange.deputy.DeputyModuleProvider;
import com.openexchange.deputy.KnownModuleId;
import com.openexchange.deputy.ModulePermission;
import com.openexchange.deputy.Permission;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.cache.CacheFolderStorage;
import com.openexchange.group.GroupService;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.contexts.impl.ContextStorage;
import com.openexchange.groupware.ldap.LdapExceptionCode;
import com.openexchange.groupware.userconfiguration.UserPermissionBits;
import com.openexchange.groupware.userconfiguration.UserPermissionBitsStorage;
import com.openexchange.imap.IMAPProvider;
import com.openexchange.imap.cache.ListLsubCache;
import com.openexchange.imap.config.IMAPConfig;
import com.openexchange.imap.entity2acl.Entity2ACLExceptionCode;
import com.openexchange.java.Functions;
import com.openexchange.java.Reference;
import com.openexchange.mail.FullnameArgument;
import com.openexchange.mail.MailSessionCache;
import com.openexchange.mail.Protocol;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.mail.api.MailCapabilities;
import com.openexchange.mail.service.MailService;
import com.openexchange.mail.utils.MailFolderUtility;
import com.openexchange.mailaccount.Account;
import com.openexchange.mailaccount.MailAccount;
import com.openexchange.mailaccount.MailAccountStorageService;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.UserService;
import com.sun.mail.imap.IMAPFolder;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

/**
 * {@link AbstractMailProvider} - The IMAP provider for deputy permission.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.6
 */
public abstract class AbstractMailProvider implements DeputyModuleProvider {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(AbstractMailProvider.class);

    /** The implementation version of the IMAP provider for deputy permission */
    public static final int VERSION = 1;

    /** The name of the default folder for <code>"mail"</code> module provider */
    protected static final String DEFAULT_FOLDER_NAME = "INBOX";

    /** The service look-up */
    protected final ServiceLookup services;

    /** Whether custom folder identifiers are allowed or not */
    protected final boolean allowSpecifiedFolders;

    /**
     * Initializes a new {@link AbstractMailProvider}.
     *
     * @param allowSpecifiedFolders <code>true</code> to allow the caller to specify the folders; otherwise <code>false</code> to stick to <code>"INBOX"</code>
     * @param services The service look-up
     */
    protected AbstractMailProvider(boolean allowSpecifiedFolders, ServiceLookup services) {
        super();
        this.allowSpecifiedFolders = allowSpecifiedFolders;
        this.services = services;
    }

    @Override
    public String getModuleId() {
        return KnownModuleId.MAIL.getId();
    }

    @Override
    public boolean isApplicable(Optional<ModulePermission> optionalModulePermission, Session session) throws OXException {
        if (getPermissionBits(session).hasWebMail() == false) {
            // No mail permission at all
            return false;
        }

        if (optionalModulePermission.isPresent()) {
            int[] accountCandidates = getAccountCandidates(session, true);

            Set<Integer> accountIds = new LinkedHashSet<Integer>(accountCandidates.length);
            for (String folderId : determineEffectiveFolderIds(optionalModulePermission.get(), allowSpecifiedFolders)) {
                Optional<FullnameArgument> optArg = MailFolderUtility.optPrepareMailFolderParam(folderId);
                int accountId = optArg.isPresent() ? optArg.get().getAccountId() : Account.DEFAULT_ID;
                if (Arrays.binarySearch(accountCandidates, accountId) < 0) {
                    // Not contained in account candidates
                    LOG.debug("Folder {} is not contained in account candidates. Deputy permission cannot be applied for user {} in context {}", folderId, I(session.getUserId()), I(session.getContextId()));
                    return false;
                }
                accountIds.add(I(accountId));
            }

            for (Integer accountId : accountIds) {
                if (checkAccountForApplicability(accountId.intValue(), session) == false) {
                    return false;
                }
            }
        } else {
            for (int accountId : getAccountCandidates(session, true)) {
                if (checkAccountForApplicability(accountId, session) == false) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean checkAccountForApplicability(int accountId, Session session) throws OXException {
        if (b(session.getParameterOrElse(SESSION_PARAM_ADMINISTRATIVE_FLAG, Boolean.FALSE))) {
            return true;
        }

        MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess = null;
        try {
            MailService mailService = services.getOptionalService(MailService.class);
            if (null == mailService) {
                throw ServiceExceptionCode.absentService(MailService.class);
            }
            mailAccess = mailService.getMailAccess(session, accountId);

            // Check protocol
            Protocol protocol = mailAccess.getProvider().getProtocol();
            if (null == protocol || (!Protocol.ALL.equals(protocol.getName()) && !IMAPProvider.PROTOCOL_IMAP.equals(protocol))) {
                // Account is not IMAP
                LOG.debug("Account {} is not an IMAP account. Deputy permission cannot be applied for user {} in context {}", I(accountId), I(session.getUserId()), I(session.getContextId()));
                return false;
            }

            // Check capabilities
            mailAccess.connect();
            MailCapabilities capabilities = mailAccess.getMailConfig().getCapabilities();
            if (!capabilities.hasPermissions()) {
                // Insufficient capabilities
                LOG.debug("Account {} does not have \"ACL\" capability. Deputy permission cannot be applied for user {} in context {}", I(accountId), I(session.getUserId()), I(session.getContextId()));
                return false;
            }
            if (!capabilities.hasMetadata()) {
                // Insufficient capabilities
                LOG.debug("Account {} does not have \"METADATA\" capability. Deputy permission cannot be applied for user {} in context {}", I(accountId), I(session.getUserId()), I(session.getContextId()));
                return false;
            }
            if (!((IMAPConfig) mailAccess.getMailConfig()).isSupportsACLs()) {
                // Insufficient capabilities
                LOG.debug("Account {} does not support ACLs (check property \"com.openexchange.imap.imapSupportsACL\"). Deputy permission cannot be applied for user {} in context {}", I(accountId), I(session.getUserId()), I(session.getContextId()));
                return false;
            }

            return true;
        } finally {
            if (mailAccess != null) {
                mailAccess.close(true);
            }
        }
    }

    @Override
    public void grantDeputyPermission(DeputyInfo deputyInfo, ModulePermission modulePermission, Session session) throws OXException {
        int[] accountCandidates = getAccountCandidates(session, true);
        for (Map.Entry<Integer, List<String>> entry : getGroupedFolderIds(modulePermission, allowSpecifiedFolders).entrySet()) {
            int accountId = entry.getKey().intValue();
            if (Arrays.binarySearch(accountCandidates, accountId) < 0) {
                // Not contained in account candidates
                throw DeputyImapProviderExceptionCode.MAIL_ACCOUNT_NOT_SUPPORTED.create(I(accountId));
            }
            grantDeputyPermissionByFolders(accountId, entry.getValue(), deputyInfo, modulePermission.getPermission(), session);
        }
    }

    /**
     * Grants specified deputy permission to given folders of specified mail account.
     *
     * @param accountId The account identifier
     * @param folderIds The folder identifiers
     * @param deputyInfo The deputy information providing the entity
     * @param permission The actual permission to grant
     * @param session The session providing user information
     * @throws OXException If granting deputy permission fails
     */
    protected abstract void grantDeputyPermissionByFolders(int accountId, Collection<String> folderIds, DeputyInfo deputyInfo, Permission permission, Session session) throws OXException;

    @Override
    public void updateDeputyPermission(DeputyInfo deputyInfo, ModulePermission modulePermission, Session session) throws OXException {
        int[] accountCandidates = getAccountCandidates(session, true);
        for (Map.Entry<Integer, List<String>> entry : getGroupedFolderIds(modulePermission, allowSpecifiedFolders).entrySet()) {
            int accountId = entry.getKey().intValue();
            if (Arrays.binarySearch(accountCandidates, accountId) < 0) {
                // Not contained in account candidates
                throw DeputyImapProviderExceptionCode.MAIL_ACCOUNT_NOT_SUPPORTED.create(I(accountId));
            }
            updateDeputyPermissionForAccount(accountId, entry.getValue(), deputyInfo, modulePermission.getPermission(), session);
        }
    }

    /**
     * Updates the deputy permission for given account's folders.
     *
     * @param accountId The account identifier
     * @param folderIds The folder identifiers
     * @param deputyInfo The deputy information providing the entity
     * @param permission The new permission to apply
     * @param session The session providing user information
     * @throws OXException If updating deputy permission fails
     */
    private void updateDeputyPermissionForAccount(int accountId, List<String> folderIds, DeputyInfo deputyInfo, Permission permission, Session session) throws OXException {
        Set<String> removeFolderIds;
        Set<String> newFolderIds;
        Set<String> updateFolderIds;
        {
            Optional<ModulePermission> existentModulePermission = getDeputyPermission(deputyInfo, session);
            if (existentModulePermission.isPresent()) {
                List<String> existentFolderIds = existentModulePermission.get().getOptionalFolderIds().get();
                removeFolderIds = new HashSet<String>(existentFolderIds);
                removeFolderIds.removeAll(folderIds);

                newFolderIds = new HashSet<String>(folderIds);
                newFolderIds.removeAll(existentFolderIds);

                updateFolderIds = new HashSet<String>(folderIds);
                updateFolderIds.retainAll(existentFolderIds);
            } else {
                removeFolderIds = Collections.emptySet();
                newFolderIds = Collections.emptySet();
                updateFolderIds = new HashSet<String>(folderIds);
            }
        }

        if (updateFolderIds.isEmpty() == false) {
            updateDeputyPermissionByFolders(accountId, updateFolderIds, deputyInfo, permission, session);
        }

        if (newFolderIds.isEmpty() == false) {
            grantDeputyPermissionByFolders(accountId, newFolderIds, deputyInfo, permission, session);
        }

        if (removeFolderIds.isEmpty() == false) {
            revokeDeputyPermissionByFolders(accountId, removeFolderIds, deputyInfo, session);
        }
    }

    /**
     * Updates existent deputy permission in given folders with specified permission.
     *
     * @param accountId The account identifier
     * @param updateFolderIds The identifiers of the folders for which to update the deputy permission
     * @param deputyInfo The deputy information providing the entity
     * @param permission The new permission to apply
     * @param session The session providing user information
     * @throws OXException If updating deputy permission fails
     */
    protected abstract void updateDeputyPermissionByFolders(int accountId, Collection<String> updateFolderIds, DeputyInfo deputyInfo, Permission permission, Session session) throws OXException;

    /**
     * Revokes existent deputy permission from given folders.
     *
     * @param accountId The account identifier
     * @param removeFolderIds The identifiers of the folders for which to revoke the deputy permission
     * @param deputyInfo The deputy information providing the entity
     * @param session The session providing user information
     * @throws OXException If revoking deputy permission fails
     */
    protected abstract void revokeDeputyPermissionByFolders(int accountId, Collection<String> removeFolderIds, DeputyInfo deputyInfo, Session session) throws OXException;

    @Override
    public void revokeDeputyPermission(DeputyInfo deputyInfo, Session session) throws OXException {
        for (int accountId : getAccountCandidates(session, false)) {
            revokeDeputyPermissionForAccount(accountId, deputyInfo, session);
        }
    }

    /**
     * Revokes existent deputy permission from given account.
     *
     * @param accountId The account identifier
     * @param deputyInfo The deputy information providing the entity
     * @param session The session providing user information
     * @throws OXException If revoking deputy permission fails
     */
    protected abstract void revokeDeputyPermissionForAccount(int accountId, DeputyInfo deputyInfo, Session session) throws OXException;

    @Override
    public Optional<ModulePermission> getDeputyPermission(DeputyInfo deputyInfo, Session session) throws OXException {
        Reference<Permission> permissionReference = new Reference<Permission>();
        List<String> folderIds = null;
        for (int accountId : getAccountCandidates(session, false)) {
            List<String> accountFolderIds = getDeputyPermissionFrom(accountId, permissionReference, deputyInfo, session);
            if (accountFolderIds != null && !accountFolderIds.isEmpty()) {
                if (folderIds == null) {
                    folderIds = accountFolderIds instanceof ArrayList ? accountFolderIds : new ArrayList<>(accountFolderIds);
                } else {
                    folderIds.addAll(accountFolderIds);
                }
            }
        }

        if (folderIds == null) {
            return Optional.empty();
        }

        return Optional.of(DefaultModulePermission.builder()
            .withModuleId(KnownModuleId.MAIL.getId())
            .withFolderIds(folderIds)
            .withPermission(permissionReference.getValue())
            .build());
    }

    /**
     * Gets the identifiers of those mail folders from given account that carry a permission for specified deputy entity.
     *
     * @param accountId The account identifier
     * @param permissionReference The reference to store permission to
     * @param deputyInfo The deputy information
     * @param session The session needed to access mail system
     * @return The folder identifiers
     * @throws OXException If folder identifiers cannot be returned
     */
    protected abstract List<String> getDeputyPermissionFrom(int accountId, Reference<Permission> permissionReference, DeputyInfo deputyInfo, Session session) throws OXException;

    // ------------------------------------------------- Utility methods -------------------------------------------------------------------

    /**
     * Gets the user permission bits for session-associated user.
     *
     * @param session The session providing user information
     * @return The user permission bits
     * @throws OXException If user permission bits cannot be returned
     */
    protected static UserPermissionBits getPermissionBits(Session session) throws OXException {
        if (session instanceof ServerSession) {
            return ((ServerSession) session).getUserPermissionBits();
        }
        return UserPermissionBitsStorage.getInstance().getUserPermissionBits(session.getUserId(), session.getContextId());
    }

    /**
     * Gets the context for session-associated user.
     *
     * @param session The session providing user information
     * @return The context
     * @throws OXException If context cannot be returned
     */
    protected static Context getContext(Session session) throws OXException {
        if (session instanceof ServerSession) {
            return ((ServerSession) session).getContext();
        }
        return ContextStorage.getInstance().getContext(session.getContextId());
    }

    /**
     * Checks if the deputy information's entity - either group or user - does exist.
     *
     * @param deputyInfo The deputy information providing the entity to check
     * @param context The context in which the entity resides
     * @return <code>true</code> if the entity exists; otherwise <code>false</code>
     * @throws OXException If existence check fails
     */
    protected boolean existsEntity(DeputyInfo deputyInfo, Context context) throws OXException {
        if (deputyInfo.isGroup()) {
            return services.getServiceSafe(GroupService.class).exists(context, deputyInfo.getEntityId());
        }
        return services.getServiceSafe(UserService.class).exists(deputyInfo.getEntityId(), context.getContextId());
    }

    /**
     * Checks if given Open-Xchange exception hints to an unknown entity error.
     *
     * @param e The Open-Xchange exception to check
     * @return <code>true</code> for unknown entity error; otherwise <code>false</code>
     */
    protected static boolean isUnknownEntityError(OXException e) {
        final int code = e.getCode();
        return (e.isPrefix("ACL") && (Entity2ACLExceptionCode.RESOLVE_USER_FAILED.getNumber() == code)) || (e.isPrefix("USR") && (LdapExceptionCode.USER_NOT_FOUND.getNumber() == code));
    }

    /**
     * Artificial flag since first implementation of deputy permission only covers default mail account's INBOX folder.
     * <p>
     * This flag can be removed later on to let other code become effective.
     */
    protected static final boolean ALLOW_PRIMARY_ONLY = true;

    /**
     * Gets the (sorted) identifiers of applicable mail accounts.
     *
     * @param session The session providing user information
     * @param sort <code>true</code> to sort the account identifier; otherwise <code>false</code>
     * @return The (sorted) mail account identifiers
     * @throws OXException If mail account identifiers cannot be returned
     */
    protected int[] getAccountCandidates(Session session, boolean sort) throws OXException {
        if (ALLOW_PRIMARY_ONLY) {
            return new int[] { Account.DEFAULT_ID };
        }

        TIntList accountIds = null;
        MailAccountStorageService mass = services.getServiceSafe(MailAccountStorageService.class);
        for (MailAccount mailAccount : mass.getUserMailAccounts(session.getUserId(), session.getContextId())) {
            if (mailAccount.isDefaultOrSecondaryAccount() && !mailAccount.isDeactivated() && "imap".equals(mailAccount.getMailProtocol())) {
                if (accountIds == null) {
                    accountIds = new TIntArrayList(4);
                }
                accountIds.add(mailAccount.getId());
            }
        }
        if (accountIds == null) {
            return new int[0];
        }
        if (sort) {
            accountIds.sort();
        }
        return accountIds.toArray();
    }

    /**
     * Clears all relevant caches for given arguments.
     *
     * @param folderIds The folder identifiers
     * @param accountId The account identifier
     * @param deputyInfo The deputy information providing the entity
     * @param session The session providing user information
     */
    protected static void clearCaches(Collection<String> folderIds, int accountId, DeputyInfo deputyInfo, Session session) {
        ListLsubCache.clearCache(accountId, session);
        ListLsubCache.dropFor(deputyInfo.getEntityId(), session.getContextId());
        MailSessionCache.clearFor(deputyInfo.getEntityId(), session.getContextId());

        CacheFolderStorage.getInstance().removeSingleFromCache(folderIds, CacheFolderStorage.REAL_TREE_ID, session.getUserId(), session, false);
        CacheFolderStorage.dropUserEntries(deputyInfo.getEntityId(), session.getContextId(), true);
    }

    /**
     * Gets the account-wise grouped identifiers of the effective folders for given module permission.
     *
     * @param modulePermission The module permission
     * @param allowSpecifiedFolders <code>true</code> to allow the caller to specify the folders; otherwise <code>false</code> to stick to <code>"INBOX"</code>
     * @return The account-wise grouped folder identifiers
     * @throws OXException If folder identifiers are illegal
     */
    protected static Map<Integer, List<String>> getGroupedFolderIds(ModulePermission modulePermission, boolean allowSpecifiedFolders) throws OXException {
        Map<Integer, List<String>> groupedFolderIds = LinkedHashMap.newLinkedHashMap(4);
        for (String folderId : determineEffectiveFolderIds(modulePermission, allowSpecifiedFolders)) {
            Optional<FullnameArgument> optArg = MailFolderUtility.optPrepareMailFolderParam(folderId);
            if (optArg.isPresent()) {
                // E.g. "default2/INBOX"
                Integer accountId = I(optArg.get().getAccountId());
                List<String> folderIds = groupedFolderIds.computeIfAbsent(accountId, Functions.getNewArrayListFuntion());
                folderIds.add(folderId);
            } else {
                // E.g. "INBOX"
                Integer accountId = I(Account.DEFAULT_ID);
                List<String> folderIds = groupedFolderIds.computeIfAbsent(accountId, Functions.getNewArrayListFuntion());
                folderIds.add(MailFolderUtility.prepareFullname(accountId.intValue(), folderId));
            }
        }
        return groupedFolderIds;
    }

    /**
     * Determines the identifiers of the effective folders to which given module refers is applicable.
     *
     * @param modulePermission The module permission
     * @param allowSpecifiedFolders <code>true</code> to allow the caller to specify the folders; otherwise <code>false</code> to stick to <code>"INBOX"</code>
     * @return The effective folders' identifiers
     * @throws OXException If folder identifiers are illegal
     */
    protected static List<String> determineEffectiveFolderIds(ModulePermission modulePermission, boolean allowSpecifiedFolders) throws OXException {
        Optional<List<String>> optionalFolderIds = modulePermission.getOptionalFolderIds();
        if (optionalFolderIds.isEmpty()) {
            return Collections.singletonList(MailFolderUtility.prepareFullname(Account.DEFAULT_ID, DEFAULT_FOLDER_NAME));
        }

        List<String> folderIds = optionalFolderIds.get();
        if (folderIds.isEmpty()) {
            // No folders specified. Assume default one.
            return Collections.singletonList(MailFolderUtility.prepareFullname(Account.DEFAULT_ID, DEFAULT_FOLDER_NAME));
        }

        if (allowSpecifiedFolders) {
            return folderIds;
        }

        String inboxFolderId = MailFolderUtility.prepareFullname(Account.DEFAULT_ID, DEFAULT_FOLDER_NAME);
        if (folderIds.size() == 1 && inboxFolderId.equals(folderIds.get(0))) {
            return folderIds;
        }
        throw DeputyImapProviderExceptionCode.NO_CUSTOM_FOLDERS.create();
    }

    /**
     * Safely {@link IMAPFolder#close(boolean) closes} given IMAP folder.
     *
     * @param imapFolder The IMAP folder to close
     */
    protected static void closeIMAPFolderSafe(IMAPFolder imapFolder) {
        if (imapFolder != null) {
            try {
                imapFolder.close(false);
            } catch (Exception e) {
                // Ignore
            }
        }
    }

}
