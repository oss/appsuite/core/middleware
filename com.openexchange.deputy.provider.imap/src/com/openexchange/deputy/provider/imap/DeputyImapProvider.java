/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.deputy.provider.imap;

import static com.openexchange.java.Autoboxing.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.mail.Folder;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import com.openexchange.deputy.DefaultPermission;
import com.openexchange.deputy.DefaultPermission.Builder;
import com.openexchange.deputy.DeputyExceptionCode;
import com.openexchange.deputy.DeputyInfo;
import com.openexchange.deputy.ModulePermission;
import com.openexchange.deputy.Permission;
import com.openexchange.deputy.Permissions;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.imap.ACLPermission;
import com.openexchange.imap.IMAPAccess;
import com.openexchange.imap.config.IMAPConfig;
import com.openexchange.imap.converters.IMAPFolderConverter;
import com.openexchange.imap.entity2acl.Entity2ACL;
import com.openexchange.imap.entity2acl.Entity2ACLArgs;
import com.openexchange.imap.util.IMAPDeputyMetadataUtility;
import com.openexchange.java.Reference;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.mail.mime.MimeMailException;
import com.openexchange.mail.service.MailService;
import com.openexchange.mail.utils.MailFolderUtility;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.sun.mail.imap.ACL;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;
import com.sun.mail.imap.Rights;

/**
 * {@link DeputyImapProvider} - The IMAP provider for deputy permission.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.6
 */
public class DeputyImapProvider extends AbstractMailProvider {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(DeputyImapProvider.class);

    /**
     * Initializes a new {@link DeputyImapProvider}.
     *
     * @param services The service look-up
     */
    public DeputyImapProvider(ServiceLookup services) {
        super(true, services);
    }

    @Override
    public int getRanking() {
        return 10000;
    }

    @Override
    public boolean isApplicable(Optional<ModulePermission> optionalModulePermission, Session session) throws OXException {
        if (null != session && b(session.getParameterOrElse(com.openexchange.deputy.AdministrativeDeputyService.SESSION_PARAM_ADMINISTRATIVE_FLAG, Boolean.FALSE))) {
            return false;
        }
        return super.isApplicable(optionalModulePermission, session);
    }

    @Override
    protected void grantDeputyPermissionByFolders(int accountId, Collection<String> folderIds, DeputyInfo deputyInfo, Permission permission, Session session) throws OXException {
        MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess = null;
        try {
            MailService mailService = services.getOptionalService(MailService.class);
            if (null == mailService) {
                throw ServiceExceptionCode.absentService(MailService.class);
            }

            mailAccess = mailService.getMailAccess(session, accountId);
            mailAccess.connect(false);

            Context context = getContext(session);
            IMAPConfig imapConfig = (IMAPConfig) mailAccess.getMailConfig();
            IMAPStore imapStore = IMAPAccess.getIMAPFolderStorageFrom(mailAccess).getImapStore();

            List<String> foldersToClear = new ArrayList<String>(folderIds.size());
            try {
                for (String folderId : folderIds) {
                    String fullName = MailFolderUtility.prepareMailFolderParamOrElseReturn(folderId);
                    IMAPFolder imapFolder = (IMAPFolder) imapStore.getFolder(fullName);

                    imapFolder.open(Folder.READ_WRITE);
                    try {
                        boolean selectable = (imapFolder.getType() & Folder.HOLDS_MESSAGES) > 0;
                        if (selectable == false) {
                            throw DeputyImapProviderExceptionCode.CANNOT_SELECT_FOLDER.create(folderId);
                        }

                        // Check rights
                        Rights ownRights = IMAPFolderConverter.getOwnRights(imapFolder, session, imapConfig);
                        if (ownRights == null || imapConfig.getACLExtension().canGetACL(ownRights) == false) {
                            throw DeputyImapProviderExceptionCode.CANNOT_GET_ACL.create(folderId);
                        }
                        if (imapConfig.getACLExtension().canSetACL(ownRights) == false) {
                            throw DeputyImapProviderExceptionCode.CANNOT_SET_ACL.create(folderId);
                        }

                        // Determine deputy's ACL name
                        Entity2ACLArgs args = IMAPFolderConverter.getEntity2AclArgs(session.getUserId(), session, imapFolder, imapConfig);
                        String aclName = Entity2ACL.getInstance(imapStore, imapConfig).getACLName(deputyInfo.getEntityId(), context, args);

                        // Check possibly existent deputy meta-data in JSON format
                        Optional<JSONObject> optionalMetadata = IMAPDeputyMetadataUtility.getDeputyMetadata(imapFolder);
                        if (optionalMetadata.isPresent()) {
                            // Check existence of deputy meta-data
                            JSONObject jMetadata = optionalMetadata.get();
                            String removeKey = null;
                            for (Map.Entry<String, Object> e : jMetadata.entrySet()) {
                                JSONObject jDeputyMetadata = (JSONObject) e.getValue();
                                if (aclName.equals(jDeputyMetadata.optString("name", null)) || deputyInfo.getEntityId() == jDeputyMetadata.optInt("entity", -1)) {
                                    if (VERSION == jDeputyMetadata.optInt("version", 0)) {
                                        // Cannot grant deputy permission since already present
                                        throw DeputyImapProviderExceptionCode.DUPLICATE_DEPUTY_PERMISSION.create(folderId);
                                    }
                                    // Version mismatch
                                    removeKey = e.getKey();
                                    break;
                                }
                            }
                            if (removeKey != null) {
                                jMetadata.remove(removeKey);
                            }
                        }

                        // Check existent ACLs
                        ACL[] acls = imapFolder.getACL();
                        ACLPermission existentPermission = null;
                        for (int i = acls.length; existentPermission == null && i-- > 0;) {
                            try {
                                ACLPermission aclPerm = new ACLPermission();
                                aclPerm.parseACL(acls[i], args, imapStore, imapConfig, context);
                                if (aclPerm.getEntity() == deputyInfo.getEntityId()) {
                                    existentPermission = aclPerm;
                                }
                            } catch (OXException e) {
                                if (!isUnknownEntityError(e)) {
                                    throw e;
                                }

                                LOG.debug("Cannot map ACL entity named \"{}\" to a system user", acls[i].getName());
                            }
                        }

                        // Apply permission & store deputy meta-data
                        ACLPermission newPermission = new ACLPermission();
                        newPermission.setEntity(deputyInfo.getEntityId());
                        newPermission.setDeleteObjectPermission(permission.getDeletePermission());
                        newPermission.setFolderAdmin(permission.isAdmin());
                        newPermission.setFolderPermission(permission.getFolderPermission());
                        newPermission.setGroupPermission(permission.isGroup());
                        newPermission.setReadObjectPermission(permission.getReadPermission());
                        newPermission.setSystem(0);
                        newPermission.setWriteObjectPermission(permission.getWritePermission());

                        JSONObject jDeputyMetadata;
                        if (existentPermission == null) {
                            // No existent permission
                            imapFolder.addACL(newPermission.getPermissionACL(args, imapConfig, imapStore, context));
                            foldersToClear.add(folderId);
                            jDeputyMetadata = IMAPDeputyMetadataUtility.createDeputyMetadata(deputyInfo.getDeputyId(), deputyInfo.getEntityId(), Permissions.createPermissionBits(permission), aclName, VERSION, (ACL) null, optionalMetadata);
                        } else {
                            // Deputy has already a permission on that IMAP folder
                            imapFolder.removeACL(aclName);
                            foldersToClear.add(folderId);
                            imapFolder.addACL(newPermission.getPermissionACL(args, imapConfig, imapStore, context));
                            jDeputyMetadata = IMAPDeputyMetadataUtility.createDeputyMetadata(deputyInfo.getDeputyId(), deputyInfo.getEntityId(), Permissions.createPermissionBits(permission), aclName, VERSION,
                                                                                             existentPermission.getPermissionACL(args, imapConfig, imapStore, context), optionalMetadata);
                        }
                        IMAPDeputyMetadataUtility.setDeputyMetadata(jDeputyMetadata, imapFolder);
                    } finally {
                        closeIMAPFolderSafe(imapFolder);
                    }
                }
            } catch (javax.mail.MessagingException e) {
                throw MimeMailException.handleMessagingException(e, imapConfig, session);
            } finally {
                if (foldersToClear.isEmpty() == false) {
                    clearCaches(foldersToClear, accountId, deputyInfo, session);
                }
            }
        } finally {
            if (mailAccess != null) {
                mailAccess.close(true);
            }
        }
    }

    @Override
    protected void updateDeputyPermissionByFolders(int accountId, Collection<String> folderIds, DeputyInfo deputyInfo, Permission permission, Session session) throws OXException {
        MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess = null;
        try {
            MailService mailService = services.getOptionalService(MailService.class);
            if (null == mailService) {
                throw ServiceExceptionCode.absentService(MailService.class);
            }

            mailAccess = mailService.getMailAccess(session, accountId);
            mailAccess.connect(false);

            Context context = getContext(session);
            IMAPConfig imapConfig = (IMAPConfig) mailAccess.getMailConfig();
            IMAPStore imapStore = IMAPAccess.getIMAPFolderStorageFrom(mailAccess).getImapStore();

            List<String> foldersToClear = new ArrayList<String>(folderIds.size());
            try {
                Boolean entityExists = null;
                for (String folderId : folderIds) {
                    String fullName = MailFolderUtility.prepareMailFolderParamOrElseReturn(folderId);
                    IMAPFolder imapFolder = (IMAPFolder) imapStore.getFolder(fullName);

                    imapFolder.open(Folder.READ_WRITE);
                    try {
                        boolean selectable = (imapFolder.getType() & Folder.HOLDS_MESSAGES) > 0;
                        if (selectable == false) {
                            throw DeputyImapProviderExceptionCode.CANNOT_SELECT_FOLDER.create(folderId);
                        }

                        // Check rights
                        Rights ownRights = IMAPFolderConverter.getOwnRights(imapFolder, session, imapConfig);
                        if (ownRights == null || imapConfig.getACLExtension().canGetACL(ownRights) == false) {
                            throw DeputyImapProviderExceptionCode.CANNOT_GET_ACL.create(folderId);
                        }

                        // Check existence of meta-data
                        Optional<JSONObject> optionalMetadata = IMAPDeputyMetadataUtility.getDeputyMetadata(imapFolder);
                        if (!optionalMetadata.isPresent()) {
                            // Cannot update deputy permission since not present
                            throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                        }

                        // Check existence of deputy meta-data
                        JSONObject jMetadata = optionalMetadata.get();
                        JSONObject jDeputyMetadata = jMetadata.optJSONObject(deputyInfo.getDeputyId());
                        if (jDeputyMetadata == null || VERSION != jDeputyMetadata.optInt("version", 0)) {
                            // Version mismatch
                            throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                        }

                        // Check entity existence
                        if (entityExists == null) {
                            entityExists = Boolean.valueOf(existsEntity(deputyInfo, context));
                        }
                        if (entityExists.booleanValue() == false) {
                            //  Entity does no more exist
                            jMetadata.remove(deputyInfo.getDeputyId());
                            if (jMetadata.isEmpty()) {
                                IMAPDeputyMetadataUtility.unsetDeputyMetadata(imapFolder);
                            } else {
                                IMAPDeputyMetadataUtility.setDeputyMetadata(jMetadata, imapFolder);
                            }
                        } else {
                            // Determine & validate deputy's ACL name
                            Entity2ACLArgs args = IMAPFolderConverter.getEntity2AclArgs(session.getUserId(), session, imapFolder, imapConfig);
                            String aclName = Entity2ACL.getInstance(imapStore, imapConfig).getACLName(deputyInfo.getEntityId(), context, args);

                            if (!aclName.equals(jDeputyMetadata.optString("name", null))) {
                                // Version mismatch
                                throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                            }

                            // Check existent ACLs
                            ACL[] acls = imapFolder.getACL();
                            ACLPermission existentPermission = null;
                            for (int i = acls.length; existentPermission == null && i-- > 0;) {
                                try {
                                    ACLPermission aclPerm = new ACLPermission();
                                    aclPerm.parseACL(acls[i], args, imapStore, imapConfig, context);
                                    if (aclPerm.getEntity() == deputyInfo.getEntityId()) {
                                        existentPermission = aclPerm;
                                    }
                                } catch (OXException e) {
                                    if (!isUnknownEntityError(e)) {
                                        throw e;
                                    }

                                    LOG.debug("Cannot map ACL entity named \"{}\" to a system user", acls[i].getName());
                                }
                            }

                            // Apply permission & store deputy meta-data
                            if (existentPermission == null) {
                                throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                            }

                            // Deputy has a permission on that IMAP folder
                            if (imapConfig.getACLExtension().canSetACL(ownRights) == false) {
                                throw DeputyImapProviderExceptionCode.CANNOT_SET_ACL.create(folderId);
                            }

                            ACLPermission newPermission = new ACLPermission();
                            newPermission.setEntity(deputyInfo.getEntityId());
                            newPermission.setDeleteObjectPermission(permission.getDeletePermission());
                            newPermission.setFolderAdmin(permission.isAdmin());
                            newPermission.setFolderPermission(permission.getFolderPermission());
                            newPermission.setGroupPermission(permission.isGroup());
                            newPermission.setReadObjectPermission(permission.getReadPermission());
                            newPermission.setSystem(0);
                            newPermission.setWriteObjectPermission(permission.getWritePermission());

                            imapFolder.removeACL(aclName);
                            foldersToClear.add(folderId);
                            imapFolder.addACL(newPermission.getPermissionACL(args, imapConfig, imapStore, context));

                            jDeputyMetadata.put("permission", Permissions.createPermissionBits(permission));
                            IMAPDeputyMetadataUtility.setDeputyMetadata(jMetadata, imapFolder);
                        }
                    } catch (JSONException e) {
                        throw DeputyExceptionCode.JSON_ERROR.create(e, e.getMessage());
                    } finally {
                        closeIMAPFolderSafe(imapFolder);
                    }
                }

            } catch (javax.mail.MessagingException e) {
                throw MimeMailException.handleMessagingException(e, imapConfig, session);
            } finally {
                if (foldersToClear.isEmpty() == false) {
                    clearCaches(folderIds, accountId, deputyInfo, session);
                }
            }
        } finally {
            if (mailAccess != null) {
                mailAccess.close(true);
            }
        }
    }

    @Override
    protected void revokeDeputyPermissionByFolders(int accountId, Collection<String> folderIds, DeputyInfo deputyInfo, Session session) throws OXException {
        MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess = null;
        try {
            MailService mailService = services.getOptionalService(MailService.class);
            if (null == mailService) {
                throw ServiceExceptionCode.absentService(MailService.class);
            }

            mailAccess = mailService.getMailAccess(session, accountId);
            mailAccess.connect(false);

            Context context = getContext(session);
            IMAPConfig imapConfig = (IMAPConfig) mailAccess.getMailConfig();
            IMAPStore imapStore = IMAPAccess.getIMAPFolderStorageFrom(mailAccess).getImapStore();

            List<String> foldersToClear = new ArrayList<String>(folderIds.size());
            try {
                Boolean entityExists = null;
                for (String folderId : folderIds) {
                    String fullName = MailFolderUtility.prepareMailFolderParamOrElseReturn(folderId);
                    IMAPFolder imapFolder = (IMAPFolder) imapStore.getFolder(fullName);

                    imapFolder.open(Folder.READ_WRITE);
                    try {
                        boolean selectable = (imapFolder.getType() & Folder.HOLDS_MESSAGES) > 0;
                        if (selectable == false) {
                            throw DeputyImapProviderExceptionCode.CANNOT_SELECT_FOLDER.create(folderId);
                        }

                        // Check rights
                        Rights ownRights = IMAPFolderConverter.getOwnRights(imapFolder, session, imapConfig);
                        if (ownRights == null || imapConfig.getACLExtension().canGetACL(ownRights) == false) {
                            throw DeputyImapProviderExceptionCode.CANNOT_GET_ACL.create(folderId);
                        }
                        if (imapConfig.getACLExtension().canSetACL(ownRights) == false) {
                            throw DeputyImapProviderExceptionCode.CANNOT_SET_ACL.create(folderId);
                        }

                        // Check existence of meta-data
                        Optional<JSONObject> optionalMetadata = IMAPDeputyMetadataUtility.getDeputyMetadata(imapFolder);
                        if (!optionalMetadata.isPresent()) {
                            // Cannot update deputy permission since not present
                            throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                        }

                        // Check existence of deputy meta-data
                        JSONObject jMetadata = optionalMetadata.get();
                        JSONObject jDeputyMetadata = jMetadata.optJSONObject(deputyInfo.getDeputyId());
                        if (jDeputyMetadata == null || VERSION != jDeputyMetadata.optInt("version", 0)) {
                            // Version mismatch
                            throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                        }

                        // Check entity existence
                        if (entityExists == null) {
                            entityExists = Boolean.valueOf(existsEntity(deputyInfo, context));
                        }
                        if (entityExists.booleanValue() == false) {
                            //  Entity does no more exist
                            jMetadata.remove(deputyInfo.getDeputyId());
                            if (jMetadata.isEmpty()) {
                                IMAPDeputyMetadataUtility.unsetDeputyMetadata(imapFolder);
                            } else {
                                IMAPDeputyMetadataUtility.setDeputyMetadata(jMetadata, imapFolder);
                            }
                        } else {
                            // Determine deputy's ACL name
                            Entity2ACLArgs args = IMAPFolderConverter.getEntity2AclArgs(session.getUserId(), session, imapFolder, imapConfig);
                            String aclName = Entity2ACL.getInstance(imapStore, imapConfig).getACLName(deputyInfo.getEntityId(), context, args);

                            if (!aclName.equals(jDeputyMetadata.optString("name", null))) {
                                // Version mismatch
                                throw DeputyImapProviderExceptionCode.MISSING_DEPUTY_PERMISSION.create(folderId);
                            }

                            imapFolder.removeACL(aclName);
                            foldersToClear.add(folderId);

                            String previousRights = jDeputyMetadata.optString("previousRights", null);
                            if (previousRights != null) {
                                imapFolder.addACL(new ACL(aclName, new Rights(previousRights)));
                            }

                            jMetadata.remove(deputyInfo.getDeputyId());
                            if (jMetadata.isEmpty()) {
                                IMAPDeputyMetadataUtility.unsetDeputyMetadata(imapFolder);
                            } else {
                                IMAPDeputyMetadataUtility.setDeputyMetadata(jMetadata, imapFolder);
                            }
                        }
                    } finally {
                        closeIMAPFolderSafe(imapFolder);
                    }
                }

                clearCaches(folderIds, accountId, deputyInfo, session);
            } catch (javax.mail.MessagingException e) {
                throw MimeMailException.handleMessagingException(e, imapConfig, session);
            } finally {
                if (foldersToClear.isEmpty() == false) {
                    clearCaches(folderIds, accountId, deputyInfo, session);
                }
            }
        } finally {
            if (mailAccess != null) {
                mailAccess.close(true);
            }
        }
    }

    @Override
    protected void revokeDeputyPermissionForAccount(int accountId, DeputyInfo deputyInfo, Session session) throws OXException {
        Context context = getContext(session);

        MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess = null;
        try {
            MailService mailService = services.getOptionalService(MailService.class);
            if (null == mailService) {
                throw ServiceExceptionCode.absentService(MailService.class);
            }

            mailAccess = mailService.getMailAccess(session, accountId);
            mailAccess.connect(false);

            IMAPConfig imapConfig = (IMAPConfig) mailAccess.getMailConfig();
            IMAPStore imapStore = IMAPAccess.getIMAPFolderStorageFrom(mailAccess).getImapStore();

            List<String> foldersToClear = null;
            try {
                Map<String, JSONObject> deputyMetadatas = IMAPDeputyMetadataUtility.getAllFoldersHavingDeputy(deputyInfo.getDeputyId(), imapStore);
                foldersToClear = new ArrayList<String>(deputyMetadatas.size());
                Boolean entityExists = null;
                for (Map.Entry<String, JSONObject> deputyMetadataEntry : deputyMetadatas.entrySet()) {
                    String fullName = deputyMetadataEntry.getKey();
                    JSONObject jMetadata = deputyMetadataEntry.getValue();

                    // Look-up deputy metadata for given deputy identifier
                    JSONObject jDeputyMetadata = jMetadata.optJSONObject(deputyInfo.getDeputyId());
                    if (jDeputyMetadata != null) {
                        String aclName = jDeputyMetadata.optString("name", null);
                        IMAPFolder imapFolder = (IMAPFolder) imapStore.getFolder(fullName);

                        imapFolder.removeACL(aclName);
                        foldersToClear.add(MailFolderUtility.prepareFullname(accountId, fullName));

                        // Check entity existence
                        if (entityExists == null) {
                            entityExists = Boolean.valueOf(existsEntity(deputyInfo, context));
                        }
                        if (entityExists.booleanValue() == false) {
                            //  Entity does no more exist
                            jMetadata.remove(deputyInfo.getDeputyId());
                            if (jMetadata.isEmpty()) {
                                IMAPDeputyMetadataUtility.unsetDeputyMetadata(imapFolder);
                            } else {
                                IMAPDeputyMetadataUtility.setDeputyMetadata(jMetadata, imapFolder);
                            }
                        } else {
                            String previousRights = jDeputyMetadata.optString("previousRights", null);
                            if (previousRights != null) {
                                imapFolder.addACL(new ACL(aclName, new Rights(previousRights)));
                            }

                            jMetadata.remove(deputyInfo.getDeputyId());
                            if (jMetadata.isEmpty()) {
                                IMAPDeputyMetadataUtility.unsetDeputyMetadata(imapFolder);
                            } else {
                                IMAPDeputyMetadataUtility.setDeputyMetadata(jMetadata, imapFolder);
                            }
                        }
                    }
                }
            } catch (javax.mail.MessagingException e) {
                throw MimeMailException.handleMessagingException(e, imapConfig, session);
            } finally {
                if (foldersToClear != null && foldersToClear.isEmpty() == false) {
                    clearCaches(foldersToClear, accountId, deputyInfo, session);
                }
            }
        } finally {
            if (mailAccess != null) {
                mailAccess.close(true);
            }
        }
    }

    @Override
    protected List<String> getDeputyPermissionFrom(int accountId, Reference<Permission> permissionReference, DeputyInfo deputyInfo, Session session) throws OXException {
        Context context = getContext(session);

        MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess = null;
        try {
            MailService mailService = services.getOptionalService(MailService.class);
            if (null == mailService) {
                throw ServiceExceptionCode.absentService(MailService.class);
            }

            mailAccess = mailService.getMailAccess(session, accountId);
            mailAccess.connect(false);

            IMAPConfig imapConfig = (IMAPConfig) mailAccess.getMailConfig();
            IMAPStore imapStore = IMAPAccess.getIMAPFolderStorageFrom(mailAccess).getImapStore();

            try {
                Map<String, JSONObject> fullName2Metadata = IMAPDeputyMetadataUtility.getAllFoldersHavingDeputy(deputyInfo.getDeputyId(), imapStore);
                int numberOfFolders = fullName2Metadata.size();
                if (numberOfFolders <= 0) {
                    return Collections.emptyList();
                }

                Boolean entityExists = null;
                List<String> folderIds = new ArrayList<String>(numberOfFolders);
                for (Map.Entry<String, JSONObject> deputyMetadataEntry : fullName2Metadata.entrySet()) {
                    String fullName = deputyMetadataEntry.getKey();
                    JSONObject jMetadata = deputyMetadataEntry.getValue();

                    // Look-up deputy metadata for given deputy identifier
                    JSONObject jDeputyMetadata = jMetadata.optJSONObject(deputyInfo.getDeputyId());
                    if (jDeputyMetadata != null) {
                        // Check entity existence
                        if (entityExists == null) {
                            entityExists = Boolean.valueOf(existsEntity(deputyInfo, context));
                        }
                        if (entityExists.booleanValue() == false) {
                            //  Entity does no more exist
                            IMAPFolder imapFolder = (IMAPFolder) imapStore.getFolder(fullName);
                            jMetadata.remove(deputyInfo.getDeputyId());
                            if (jMetadata.isEmpty()) {
                                IMAPDeputyMetadataUtility.unsetDeputyMetadata(imapFolder);
                            } else {
                                IMAPDeputyMetadataUtility.setDeputyMetadata(jMetadata, imapFolder);
                            }
                        } else {
                            folderIds.add(MailFolderUtility.prepareFullname(accountId, fullName));
                            if (permissionReference.hasNoValue()) {
                                int[] bits = Permissions.parsePermissionBits(jDeputyMetadata.optInt("permission", 0));
                                Builder tmp = DefaultPermission.builder()
                                                               .withFolderPermission(bits[0])
                                                               .withReadPermission(bits[1])
                                                               .withWritePermission(bits[2])
                                                               .withDeletePermission(bits[3])
                                                               .withAdmin(bits[4] > 0)
                                                               .withEntity(deputyInfo.getEntityId())
                                                               .withGroup(deputyInfo.isGroup());
                                permissionReference.setValue(tmp.build());
                            }
                        }
                    }
                }

                return folderIds;
            } catch (javax.mail.MessagingException e) {
                throw MimeMailException.handleMessagingException(e, imapConfig, session);
            }
        } finally {
            if (mailAccess != null) {
                mailAccess.close(true);
            }
        }
    }

}
