/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.health.impl.checks;

import java.util.List;
import java.util.Map;
import com.openexchange.health.DefaultMWHealthCheckResponse;
import com.openexchange.health.MWHealthCheck;
import com.openexchange.health.MWHealthCheckResponse;
import com.openexchange.java.CollectorUtils;
import com.openexchange.osgi.ServiceStateService;

/**
 * {@link NoServicesMissingCheck} is a healthcheck which checks that no services are missing
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class NoServicesMissingCheck implements MWHealthCheck {

    private final ServiceStateService service;

    /**
     * Initializes a new {@link NoServicesMissingCheck}.
     *
     * @param services The underlying service state service
     */
    public NoServicesMissingCheck(ServiceStateService service) {
        super();
        this.service = service;
    }

    @Override
    public String getName() {
        return "noServicesMissing";
    }

    @Override
    public MWHealthCheckResponse call() {
        Map<String, List<String>> missingServices = service.listMissingServices();
        boolean state = missingServices.isEmpty();
        return new DefaultMWHealthCheckResponse(getName(), toData(missingServices), state);
    }

    // ------------------ private methods ---------------

    /**
     * Converts the missing services map to a data map for the health response
     *
     * @param missingService The missing services map
     */
    private Map<String, Object> toData(Map<String, List<String>> missingService) {
        if (missingService.isEmpty()) {
            return null;
        }
        return missingService.entrySet()
                             .stream()
                             .collect(CollectorUtils.toMap(Map.Entry::getKey, Map.Entry::getValue, missingService.size()));
    }

}
