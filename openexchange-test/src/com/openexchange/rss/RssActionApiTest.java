package com.openexchange.rss;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.framework.AbstractAJAXSession;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.invoker.ApiResponse;
import com.openexchange.testing.httpclient.models.RssFeedElement;
import com.openexchange.testing.httpclient.models.RssResponse;
import com.openexchange.testing.httpclient.modules.RssApi;

public class RssActionApiTest extends AbstractAJAXSession{

    private RssApi rssApi;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        rssApi = new RssApi(testUser.getApiClient());
    }

    @Test
    public void testNewRssFeed() throws ApiException, URISyntaxException {
        List<URI> feedList = List.of(new URI("https://www.der-postillon.com/feeds/posts/default?alt=rss"));
        RssFeedElement element = new RssFeedElement().feedUrl(feedList);
        try {
            ApiResponse<RssResponse> response = rssApi.getRssFeedWithHttpInfo(element, 100, "desc", "date");
            assertEquals(200, response.getStatusCode());
        } catch (ApiException e) {
            fail(e.getMessage());
        }
    }
}
