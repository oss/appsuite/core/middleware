/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.rest.request.analyzer;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.ws.rs.core.Application;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.glassfish.jersey.server.ResourceConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.folder.actions.EnumAPI;
import com.openexchange.ajax.framework.UserValues;
import com.openexchange.ajax.framework.session.util.TestSessionReservationUtil;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.groupware.modules.Module;
import com.openexchange.java.Charsets;
import com.openexchange.java.Strings;
import com.openexchange.java.util.HttpStatusCode;
import com.openexchange.java.util.UUIDs;
import com.openexchange.request.analyzer.rest.RequestAnalyzerServlet;
import com.openexchange.rest.AbstractRestTest;
import com.openexchange.test.common.configuration.AJAXConfig;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.models.AcquireTokenResponse;
import com.openexchange.testing.httpclient.models.FolderData;
import com.openexchange.testing.httpclient.models.FolderPermission;
import com.openexchange.testing.httpclient.models.FolderResponse;
import com.openexchange.testing.httpclient.models.FolderUpdateResponse;
import com.openexchange.testing.httpclient.models.NewFolderBody;
import com.openexchange.testing.httpclient.models.NewFolderBodyFolder;
import com.openexchange.testing.httpclient.models.ShareLinkResponse;
import com.openexchange.testing.httpclient.models.ShareTargetData;
import com.openexchange.testing.httpclient.models.TokenLoginResponse;
import com.openexchange.testing.httpclient.models.UserData;
import com.openexchange.testing.httpclient.modules.FoldersApi;
import com.openexchange.testing.httpclient.modules.LoginApi;
import com.openexchange.testing.httpclient.modules.ShareManagementApi;
import com.openexchange.testing.httpclient.modules.TokenApi;
import com.openexchange.testing.httpclient.modules.UserApi;
import com.openexchange.testing.restclient.invoker.ApiException;
import com.openexchange.testing.restclient.invoker.ApiResponse;
import com.openexchange.testing.restclient.models.AnalysisResult;
import com.openexchange.testing.restclient.models.AnalysisResultHeaders;
import com.openexchange.testing.restclient.models.Header;
import com.openexchange.testing.restclient.models.RequestData;
import com.openexchange.testing.restclient.modules.RequestAnalysisApi;
import okhttp3.Cookie;
import okhttp3.HttpUrl;


/**
 * {@link RequestAnalyzerTest}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @author <a href="mailto:marcel.broecher@open-xchange.com">Marcel Broecher</a>
 */
public class RequestAnalyzerTest extends AbstractRestTest {

    String protocol = AJAXConfig.getProperty(AJAXConfig.Property.PROTOCOL);
    String host = AJAXConfig.getProperty(AJAXConfig.Property.SERVER_HOSTNAME);
    String port = AJAXConfig.getProperty(AJAXConfig.Property.SERVER_PORT);
    String basePath = AJAXConfig.getProperty(AJAXConfig.Property.BASE_PATH);

    private RequestAnalysisApi reqApi;

    @BeforeEach
    @Override
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        reqApi = new RequestAnalysisApi(getRestClient());
    }

    @Override
    protected Application configure() {
        return new ResourceConfig(RequestAnalyzerServlet.class);
    }

    /**
     * Tests that a session based request is properly mapped
     *
     * @throws OXException
     * @throws IOException
     * @throws JSONException
     */
    @Test
    public void testSessionRequest() throws OXException, IOException, JSONException {
        final String urlFormat = "%s://%s:%s%slogin?action=login&session=%s";
        final RequestData data = new RequestData();
        data.setMethod("GET");
        final List<Header> headerList = HeaderBuilder.builder()
                                               .add("someheader", "someValue")
                                               .build();
        data.setHeaders(headerList);
        data.setUrl(urlFormat.formatted(protocol, host, port, basePath, "nope"));
        data.setRemoteIP("127.0.0.1");
        try {
            reqApi.analyzeRequest(data);
            fail();
        } catch (final ApiException e) {
            assertEquals(404, e.getCode());
        }

        data.setUrl(urlFormat.formatted(protocol, host, port, basePath, getAjaxClient().getSession().getId()));
        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponse(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
        }
    }

    /**
     * Tests that a dav based request is properly mapped, using the <i>internal</i> URL.
     *
     * @throws OXException
     * @throws IOException
     * @throws JSONException
     */
    @Test
    public void testDavRequestInternalPath() throws OXException, IOException, JSONException {
        testDavRequest("http://localhost/servlet/dav/carddav/");
    }

    /**
     * Tests that a dav based request is properly mapped, using the <i>external</i> URL.
     *
     * @throws OXException
     * @throws IOException
     * @throws JSONException
     */
    @Test
    public void testDavRequestExternalPath() throws OXException, IOException, JSONException {
        testDavRequest("http://localhost/caldav/654");
    }

    private void testDavRequest(String url) throws OXException, IOException, JSONException {
        // Try invalid data first
        final RequestData data = new RequestData();
        data.setMethod("GET");
        data.setUrl("http://localhost/servlet/dav");
        List<Header> headerList = HeaderBuilder.builder()
                                               .add("Authorization", "basic YW50b25AMTpzZWNyZXQxMg==")
                                               .build();
        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        try {
            reqApi.analyzeRequest(data);
            fail();
        } catch (final ApiException e) {
            assertEquals(404, e.getCode());
        }

        // Try correct data
        final String base64Credentials = Base64.getEncoder()
                                         .encodeToString("%s:%s".formatted(testUser.getLogin(),
                                                                           testUser.getPassword())
                                                                .getBytes());
        headerList = HeaderBuilder.builder()
                                  .add("Authorization", "basic %s".formatted(base64Credentials))
                                  .build();
        data.setHeaders(headerList);
        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponse(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
        }
    }

    /**
     * Tests that a token login based request is properly mapped
     */
    @Test
    public void testRedeemTokenRequest() throws Exception {
        /*
         * acquire a valid token from a logged in client
         */
        AcquireTokenResponse acquireTokenResponse = new TokenApi(testUser.getApiClient()).acquireTokenBuilder().execute();
        assertNull(acquireTokenResponse.getError());
        assertNotNull(acquireTokenResponse.getData());
        String token = acquireTokenResponse.getData().getToken();
        assertNotNull(token);
        /*
         * build request URI and corresponding request data to analyze, using the previously retrieved token
         */
        String body = "token=" + URLEncoder.encode(token, Charsets.UTF_8) + "&client=myclient&secret=1234";
        RequestData requestData = prepareRequestData(
            "POST",
            "login",
            "redeemToken",
            Base64.getEncoder().encodeToString(body.getBytes(Charsets.UTF_8)),
            new Header[] { new Header().name(HttpHeaders.CONTENT_TYPE).value("application/x-www-form-urlencoded") },
            (NameValuePair[]) null)
        ;
        /*
         * attempt to analyze it & verify response
         */
        checkSuccessfulResponse(assertAnalyzeResponse(reqApi, requestData, HttpStatusCode.OK));
    }

    @Test
    public void testRedeemTokenRequestInvalidToken() throws Exception {
        /*
         * build request URI and corresponding request data to analyze, with an invalid/unknown token value specified
         */
        String body = "token=gibtsesnicht&client=myclient&secret=1234";
        RequestData requestData = prepareRequestData(
            "POST",
            "login",
            "redeemToken",
            Base64.getEncoder().encodeToString(body.getBytes(Charsets.UTF_8)),
            new Header[] { new Header().name(HttpHeaders.CONTENT_TYPE).value("application/x-www-form-urlencoded") },
            (NameValuePair[]) null)
        ;
        /*
         * attempt to analyze it, expecting HTTP 404
         */
        assertAnalyzeResponse(reqApi, requestData, HttpStatusCode.NOT_FOUND);
    }

    @Test
    public void testRedeemTokenRequestGarbledBody() throws Exception {
        /*
         * build request URI and corresponding request data to analyze, with an invalid/unknown token value specified
         */
        String body = "asclkjvclksdvkjnevwekncv";
        RequestData requestData = prepareRequestData(
            "POST",
            "login",
            "redeemToken",
            Base64.getEncoder().encodeToString(body.getBytes(Charsets.UTF_8)),
            new Header[] { new Header().name(HttpHeaders.CONTENT_TYPE).value("text/plain") },
            (NameValuePair[]) null)
        ;
        /*
         * attempt to analyze it, expecting HTTP 404
         */
        assertAnalyzeResponse(reqApi, requestData, HttpStatusCode.NOT_FOUND);
    }

    @Test
    public void testRedeemTokenRequestNoBody() throws Exception {
        /*
         * build request URI and corresponding request data to analyze, w/o body data
         */
        RequestData requestData = prepareRequestData(
            "POST",
            "login",
            "redeemToken",
            null,
            new Header[] { new Header().name(HttpHeaders.CONTENT_TYPE).value("application/x-www-form-urlencoded") },
            (NameValuePair[]) null)
        ;
        /*
         * attempt to analyze it, expecting HTTP 422
         */
        assertAnalyzeResponse(reqApi, requestData, HttpStatusCode.UNPROCESSABLE_ENTITY);
    }

    @Test
    public void testRegularLoginRequestNoBody() {
        RequestData data = new RequestData();
        data.setHeaders(HeaderBuilder.builder().add("someheader", "someValue").build());
        data.setRemoteIP("127.0.0.1");
        data.setUrl("%s://%s:%s%slogin?action=login".formatted(protocol, host, port, basePath));
        data.setMethod("POST");

        try {
            reqApi.analyzeRequest(data);
            fail("Expected ApiException with code 422, but no exception was thrown");
        } catch (ApiException e) {
            assertEquals(422, e.getCode());
        }
    }

    @Test
    public void testRegularLoginRequestInvalidCredentials() {
        RequestData data = new RequestData();
        data.setHeaders(HeaderBuilder.builder().add("someheader", "someValue").build());
        data.setRemoteIP("127.0.0.1");
        data.setUrl("%s://%s:%s%slogin?action=login".formatted(protocol, host, port, basePath));
        data.setMethod("POST");
        String encodedBody = Base64.getEncoder().encodeToString(("action=login&name=nouser&password=nosecret&locale=de_DE&client=open-xchange-appsuite&version=main&timeout=10000&rampup=false&staySignedIn=true").getBytes());
        data.setBody(encodedBody);

        try {
            reqApi.analyzeRequest(data);
            fail("Expected ApiException with code 404, but no exception was thrown");
        } catch (ApiException e) {
            assertEquals(404, e.getCode());
        }
    }

    @Test
    public void testRegularLoginRequestValidCredentials() throws OXException, IOException, JSONException  {
        RequestData data = new RequestData();
        data.setHeaders(HeaderBuilder.builder().add("someheader", "someValue").build());
        data.setRemoteIP("127.0.0.1");
        data.setUrl("%s://%s:%s%slogin?action=login".formatted(protocol, host, port, basePath));
        data.setMethod("POST");
        String encodedBody = Base64.getEncoder().encodeToString(("action=login&name=%s&password=secret&locale=de_DE&client=open-xchange-appsuite&version=main&timeout=10000&rampup=false&staySignedIn=true".formatted(testUser.getLogin())).getBytes());
        data.setBody(encodedBody);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponse(response);
        } catch (ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
        }
    }

    @Test
    public void testFormLoginRequestNoBody() {
        RequestData data = new RequestData();
        data.setHeaders(HeaderBuilder.builder().add("someheader", "someValue").build());
        data.setRemoteIP("127.0.0.1");
        data.setUrl("%s://%s:%s%slogin?action=formlogin&authId=123456789".formatted(protocol, host, port, basePath));
        data.setMethod("POST");

        try {
            reqApi.analyzeRequest(data);
            fail("Expected ApiException with code 422, but no exception was thrown");
        } catch (ApiException e) {
            assertEquals(422, e.getCode());
        }
    }

    @Test
    public void testFormLoginRequestInvalidCredentials() {
        RequestData data = new RequestData();
        data.setHeaders(HeaderBuilder.builder().add("someheader", "someValue").build());
        data.setRemoteIP("127.0.0.1");
        data.setUrl("%s://%s:%s%slogin?action=formlogin&authId=123456789".formatted(protocol, host, port, basePath));
        data.setMethod("POST");
        String encodedBody = Base64.getEncoder().encodeToString(("login=falselogin&password=falsepassword&client=open-xchange-appsuite&version=8.0.0&autologin=true").getBytes());
        data.setBody(encodedBody);

        try {
            reqApi.analyzeRequest(data);
            fail("Expected ApiException with code 404, but no exception was thrown");
        } catch (ApiException e) {
            assertEquals(404, e.getCode());
        }
    }

    @Test
    public void testFormLoginRequestValidCredentials() throws JSONException, OXException, IOException {
        RequestData data = new RequestData();
        data.setHeaders(HeaderBuilder.builder().add("someheader", "someValue").build());
        data.setRemoteIP("127.0.0.1");
        data.setUrl("%s://%s:%s%slogin?action=formlogin&authId=123456789".formatted(protocol, host, port, basePath));
        data.setMethod("POST");
        String encodedBody = Base64.getEncoder().encodeToString(("login=%s&password=%s&client=open-xchange-appsuite&version=8.0.0&autologin=true".formatted(testUser.getLogin(), testUser.getPassword())).getBytes());
        data.setBody(encodedBody);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponse(response);
        } catch (ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
        }
    }

    /**
     * Tests that an autologin request is properly mapped
     *
     *
     * @throws OXException
     * @throws IOException
     * @throws JSONException
     * @throws com.openexchange.testing.httpclient.invoker.ApiException
     */
    @Test
    public void testAutoLoginRequest() throws OXException, JSONException, IOException, com.openexchange.testing.httpclient.invoker.ApiException {
        final RequestData data = new RequestData();

        String url = "%s://%s:%s%slogin?action=login".formatted(protocol, host, port, basePath);

        final List<Cookie> cookies = testUser.getApiClient()
                                             .getHttpClient()
                                             .cookieJar()
                                             .loadForRequest(HttpUrl.get(url));

        final String cookieHeader = cookies.stream()
                                           .map(cookie -> "%s=%s".formatted(cookie.name(), cookie.value()))
                                           .collect(Collectors.joining(";"));

        url = "%s://%s:%s%slogin?action=autologin".formatted(protocol, host, port, basePath);

        final List<Header> headerList = HeaderBuilder.builder()
                                                     .add("Cookie", cookieHeader)
                                                     .add("User-Agent", testUser.getApiClient().getUserAgent())
                                                     .build();

        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        data.setUrl(url);
        data.setMethod("GET");

        try {
            ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponse(response);
        } catch (ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
        }
    }

    /**
     * Tests that a public session cookie based request is properly mapped
     *
     * @throws OXException
     * @throws IOException
     * @throws JSONException
     * @throws com.openexchange.testing.httpclient.invoker.ApiException
     */
    @Test
    public void testPublicSessionCookieRequest() throws OXException, IOException, JSONException, com.openexchange.testing.httpclient.invoker.ApiException {
        final RequestData data = new RequestData();
        data.setMethod("GET");

        String url = "%s://%s:%s%scontacts/picture?action=get".formatted(protocol, host, port, basePath);
        final List<Cookie> cookies = testUser.getApiClient().getHttpClient().cookieJar().loadForRequest(HttpUrl.get(url));
        final String cookieHeader = cookies.stream()
                                     .map(cookie -> "%s=%s".formatted(cookie.name(), cookie.value()))
                                     .collect(Collectors.joining(";"));

        // Apply cookies
        final List<Header> headerList = HeaderBuilder.builder()
                                               .add("Cookie", cookieHeader)
                                               .add("User-Agent", testUser.getApiClient().getUserAgent())
                                               .build();
        data.setHeaders(headerList);
        data.setUrl(url);
        data.setRemoteIP("127.0.0.1");

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponse(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
        }

        // Check wrong url
        url = "%s://%s:%s%scontacts/?action=get".formatted(protocol, host, port, basePath);
        data.setUrl(url);
        try {
            reqApi.analyzeRequest(data);
            fail();
        } catch (final ApiException e) {
            assertEquals(404, e.getCode());
        }
    }

    /**
     * Tests that a sharing link based request is properly mapped
     *
     * @throws Exception
     */
    @Test
    public void testSharingLinkRequest() throws Exception {

        // Create folder
        final FoldersApi foldersApi = new FoldersApi(testUser.getApiClient());
        final ShareManagementApi shareApi = new ShareManagementApi(testUser.getApiClient());

        final int parent = getAjaxClient().getValues().getPrivateInfostoreFolder();
        final FolderData folder = insertFolder(foldersApi, EnumAPI.OX_NEW, String.valueOf(parent));

        // Share folder
        final ShareTargetData shareTarget = new ShareTargetData();
        shareTarget.folder(folder.getFolderId());
        shareTarget.setModule(String.valueOf(FolderObject.INFOSTORE));
        final ShareLinkResponse resp = shareApi.getShareLinkBuilder()
                                         .withShareTargetData(shareTarget)
                                         .execute();

        // Analyze request with share url
        assertNull(resp.getError());
        assertNotNull(resp.getData());
        final String url = resp.getData().getUrl();

        final RequestData data = new RequestData();
        data.setMethod("GET");
        final List<Header> headerList = HeaderBuilder.builder()
                                               .add("someheader", "someValue")
                                               .build();
        data.setHeaders(headerList);
        data.setUrl(url);
        data.setRemoteIP("127.0.0.1");
        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);

            // Check response
            assertEquals(200, response.getStatusCode());
            assertNotNull(response.getData());
            final AnalysisResultHeaders headers = response.getData().getHeaders();
            assertNotNull(headers);
            final UserValues userValues = getAjaxClient().getValues();
            assertEquals(userValues.getContextId(), headers.getxOxContextId());
            assertNotNull(headers.getxOxUserId());
            assertNotEquals(userValues.getUserId(), headers.getxOxUserId());
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
        }
    }

    /**
     * Tests that a saml login request is properly mapped
     *
     * @throws Exception
     */
    @Test
    public void testSAMLRequest() throws Exception {
        // Reserve a session
        String token = TestSessionReservationUtil.reserveSession(host, testUser);
        String url = "%s://%s:%s%slogin?action=samlLogin&token=%s".formatted(protocol, host, port, basePath, token);

        RequestData data = new RequestData();
        data.setMethod("GET");
        List<Header> headerList = HeaderBuilder.builder()
                                               .add("someheader", "someValue")
                                               .build();
        data.setHeaders(headerList);
        data.setUrl(url);
        data.setRemoteIP("127.0.0.1");
        try {
            ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);

            // Check response
            assertEquals(200, response.getStatusCode());
            assertNotNull(response.getData());
            AnalysisResultHeaders headers = response.getData().getHeaders();
            assertNotNull(headers);
            UserValues userValues = getAjaxClient().getValues();
            assertEquals(userValues.getContextId(), headers.getxOxContextId());
            assertEquals(userValues.getUserId(), headers.getxOxUserId());
        } catch (ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
        }
    }

    /**
     * Tests that a free busy request is properly mapped
     *
     * @throws OXException
     * @throws IOException
     * @throws JSONException
     */
    @Test
    public void testFreeBusyRequest() {
        final String url = "%s://%s:%s/servlet/webdav.freebusy?contextId=%d&userName=%s&server=%s".formatted(protocol, host, port, Integer.valueOf(testUser.getContextId()), testUser.getUser(), testUser.getContext());

        final RequestData data = new RequestData();
        data.setMethod("POST");
        List<Header> headerList = HeaderBuilder.builder()
                .add("someheader", "someValue")
                .build();
        data.setHeaders(headerList);
        data.setUrl(url);
        data.setRemoteIP("127.0.0.1");

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            assertEquals(200, response.getStatusCode());
            assertNotNull(response.getData());
            final String marker = response.getData().getMarker();
            assertNotNull(marker);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
        }

        //check a string as cid
        data.setUrl("%s://%s:%s/servlet/webdav.freebusy?contextId=noRealContextID&userName=%s&server=%s".formatted(protocol, host, port, testUser.getUser(), testUser.getContext()));
        testInvalidRequest(data, 404);

        //check a non existing context as cid
        data.setUrl("%s://%s:%s/servlet/webdav.freebusy?contextId=2147483647&userName=%s&server=%s".formatted(protocol, host, port, testUser.getUser(), testUser.getContext()));
        testInvalidRequest(data, 404);
    }

    /**
     * Tests that the servlet responds with a 400er error in case at least one required field is missing.
     */
    @Test
    public void testMissingFields() {
        final RequestData data = new RequestData();
        try {
            reqApi.analyzeRequestWithHttpInfo(data);
            fail();
        } catch (final ApiException e) {
            assertEquals(400, e.getCode());
        }

        data.setUrl("someurl");
        try {
            reqApi.analyzeRequestWithHttpInfo(data);
            fail();
        } catch (final ApiException e) {
            assertEquals(400, e.getCode());
        }

        data.setMethod("method");
        try {
            reqApi.analyzeRequestWithHttpInfo(data);
            fail();
        } catch (final ApiException e) {
            assertEquals(400, e.getCode());
        }

        data.setRemoteIP("127.0.0.1");
        try {
            reqApi.analyzeRequestWithHttpInfo(data);
            fail();
        } catch (final ApiException e) {
            assertEquals(400, e.getCode());
        }
    }

    @Test
    public void testAdvertisementRequest() {
        final RequestData data = new RequestData();

        final String url = "%s://%s:%s/advertisement/v1/config/user?contextId=%s&userId=%s".formatted(protocol, host, port, Integer.valueOf(testUser.getContextId()), Integer.valueOf(testUser.getUserId()));
        final ArrayList<Header> headerList = new ArrayList<>();
        final Header header = new Header();

        header.setName("headername");
        header.setValue("headerValue");
        headerList.add(header);

        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        data.setUrl(url);
        data.setMethod("PUT");

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
        }
    }

    @Test
    public void testPreliminaryCapabilitiesRequest() {
        final RequestData data = new RequestData();

        final String url = "%s://%s:%s/preliminary/capabilities/v1/all/%s/%s".formatted(protocol, host, port, Integer.valueOf(testUser.getContextId()), Integer.valueOf(testUser.getUserId()));
        final ArrayList<Header> headerList = new ArrayList<>();
        final Header header = new Header();

        header.setName("headername");
        header.setValue("headerValue");
        headerList.add(header);

        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        data.setUrl(url);
        data.setMethod("GET");

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
        }
    }

    @Test
    public void testPreliminaryConfigurationRequest() {
        final RequestData data = new RequestData();

        final String url = "%s://%s:%s/preliminary/configuration/v1/property/testproperty/%s/%s".formatted(protocol, host, port, Integer.valueOf(testUser.getContextId()), Integer.valueOf(testUser.getUserId()));
        final ArrayList<Header> headerList = new ArrayList<>();
        final Header header = new Header();

        header.setName("headername");
        header.setValue("headerValue");
        headerList.add(header);

        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        data.setUrl(url);
        data.setMethod("GET");

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
        }
    }

    @Test
    public void testTokensLogin() throws Exception {
        String clientToken = UUIDs.getUnformattedStringFromRandom();
        String authId = "authId";
        String client = "open-xchange-appsuite";
        String version = "main";
        String userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/116.0";
        String ip = "127.0.1.1";
        String login = testUser.getLogin();
        String password = testUser.getPassword();
        String contextId = String.valueOf(testUser.getContextId());
        String userId = String.valueOf(testUser.getUserId());

        //Build url
        String url = "%s://%s:%s%slogin?action=tokens".formatted(protocol, host, port, basePath);
        RequestData data = new RequestData();
        data.setMethod("POST");
        List<Header> headerList = HeaderBuilder.builder()
                                               .add("User-Agent", userAgent)
                                               .add("Content-Type", "application/x-www-form-urlencoded")
                                               .build();
        data.setHeaders(headerList);
        data.setRemoteIP(ip);
        data.setUrl(url);

        // test missing body
        try {
            reqApi.analyzeRequestWithHttpInfo(data);
        } catch (ApiException e) {
            assertEquals(422, e.getCode());
        }

        // Generate tokens
        LoginApi loginApi = new LoginApi(testUser.getApiClient());
        TokenLoginResponse response = loginApi.doTokenLogin(authId, login, password, clientToken, client, version, null, Boolean.FALSE, null, ip, userAgent, Boolean.TRUE);
        assertNotNull(response);
        assertNull(response.getError());
        String serverToken = response.getServerToken();

        // Request body
        String body = "action=tokens&client=%s&version=%s&serverToken=%s&clientToken=%s&context_id=%s&user_id=%s".formatted(client, version, serverToken, clientToken, contextId, userId);
        final String base64 = Base64.getEncoder().encodeToString(body.getBytes());
        data.setBody(base64);

        ApiResponse<AnalysisResult> resp = reqApi.analyzeRequestWithHttpInfo(data);
        assertNotNull(resp);
        checkSuccessfulResponseWithoutUserInfo(resp);
    }

    @Test
    public void testTokenLogin() throws Exception {
        String clientToken = UUIDs.getUnformattedStringFromRandom();
        String authId = "authId";
        String client = "open-xchange-appsuite";
        String version = "main";
        String userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/116.0";
        String ip = "127.0.1.1";

        //Build url
        String url = "%s://%s:%s%slogin?action=tokenLogin&authId=%s".formatted(protocol, host, port, basePath, authId);
        RequestData data = new RequestData();
        data.setMethod("POST");
        List<Header> headerList = HeaderBuilder.builder()
                                               .add("User-Agent", userAgent)
                                               .add("Content-Type", "application/x-www-form-urlencoded")
                                               .build();
        data.setHeaders(headerList);
        data.setRemoteIP(ip);
        data.setUrl(url);

        // test missing body
        try {
            reqApi.analyzeRequestWithHttpInfo(data);
        } catch (ApiException e) {
            assertEquals(422, e.getCode());
        }

        // Request body
        String body = "login=%s&password=%s&clientToken=%s&client=%s&version=%s".formatted(testUser.getLogin(), testUser.getPassword(), clientToken, client, version);
        final String base64 = Base64.getEncoder().encodeToString(body.getBytes());
        data.setBody(base64);
        data.setUrl(url);

        ApiResponse<AnalysisResult> resp = reqApi.analyzeRequestWithHttpInfo(data);
        assertNotNull(resp);
        checkSuccessfulResponse(resp);
    }

    @Test
    public void testMultifactorRESTRequests() throws Exception {
        List<Header> headerList = HeaderBuilder.builder()
                                               .add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/116.0")
                                               .build();
        // List devices request
        {
            String url = "%s://%s:%s/admin/v1/contexts/%s/users/%s/multifactor/devices".formatted(protocol, host, port, I(testUser.getContextId()), I(testUser.getUserId()));
            RequestData data = new RequestData();
            data.setMethod("GET");
            data.setUrl(url);
            data.setHeaders(headerList);
            data.setRemoteIP("127.0.1.1");
            ApiResponse<AnalysisResult> resp = reqApi.analyzeRequestWithHttpInfo(data);
            assertNotNull(resp);
            checkSuccessfulResponseWithoutUserInfo(resp);
        }

        // Delete request
        {
            String url = "%s://%s:%s/admin/v1/contexts/%s/users/%s/multifactor/devices/totp/invalidDevice".formatted(protocol, host, port, I(testUser.getContextId()), I(testUser.getUserId()));
            RequestData data = new RequestData();
            data.setMethod("DELETE");
            data.setUrl(url);
            data.setHeaders(headerList);
            data.setRemoteIP("127.0.1.1");
            ApiResponse<AnalysisResult> resp = reqApi.analyzeRequestWithHttpInfo(data);
            assertNotNull(resp);
            checkSuccessfulResponseWithoutUserInfo(resp);
        }

        // Delete all devices request
        {
            String url = "%s://%s:%s/admin/v1/contexts/%s/users/%s/multifactor/devices".formatted(protocol, host, port, I(testUser.getContextId()), I(testUser.getUserId()));
            RequestData data = new RequestData();
            data.setMethod("DELETE");
            data.setUrl(url);
            data.setHeaders(headerList);
            data.setRemoteIP("127.0.1.1");
            ApiResponse<AnalysisResult> resp = reqApi.analyzeRequestWithHttpInfo(data);
            assertNotNull(resp);
            checkSuccessfulResponseWithoutUserInfo(resp);
        }

        // Use garbage data
        {
            String url = "%s://%s:%s/admin/v1/contexts/%s/users/%s/multifactor/devices".formatted(protocol, host, port, "no-integer", String.valueOf(10.9f));
            RequestData data = new RequestData();
            data.setMethod("DELETE");
            data.setUrl(url);
            data.setHeaders(headerList);
            data.setRemoteIP("127.0.1.1");
            try {
                reqApi.analyzeRequestWithHttpInfo(data);
            } catch (ApiException e) {
                assertEquals(404, e.getCode());
            }
        }
    }

    @Test
    public void testDovecotPushRESTRequest() throws Exception {
        String url = "%s://%s:%s/preliminary/http-notify/v1/notify".formatted(protocol, host, port);

        if (isServletAvailable(url)) {
            RequestData data = new RequestData();
            data.setMethod("PUT");
            List<Header> headerList = HeaderBuilder.builder()
                                                   .add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/116.0")
                                                   .build();
            data.setHeaders(headerList);
            data.setRemoteIP("127.0.1.1");

            // test missing body
            try {
                data.setUrl(url);
                reqApi.analyzeRequestWithHttpInfo(data);
            } catch (ApiException e) {
                assertEquals(422, e.getCode());
            }

            JSONObject body = new JSONObject();
            body.put("user", testUser.getUserId() + "@" + testUser.getContextId());
            body.put("imap-uidvalidity", "1234567890");
            body.put("imap-uid", "12345");
            body.put("folder", "INBOX");
            body.put("event", "messageNew");
            body.put("from", "tester@example.org");
            body.put("subject", "Request Analyzer Test");
            body.put("snippet", "Bla Bla Bla");
            data.body(Base64.getEncoder().encodeToString(body.getByteArray(false)));
            ApiResponse<AnalysisResult> resp = reqApi.analyzeRequestWithHttpInfo(data);
            assertNotNull(resp);
            checkSuccessfulResponseWithoutUserInfo(resp);
        }
    }

    public void testPasswordHistoryRESTRequest() throws Exception {
        String url = "%s://%s:%s/admin/v1/contexts/%s/users/%s/passwd-changes".formatted(protocol, host, port, I(testUser.getContextId()), I(testUser.getUserId()));
        RequestData data = new RequestData();
        data.setMethod("GET");
        data.setUrl(url);
        List<Header> headerList = HeaderBuilder.builder()
                                  .add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/116.0")
                                  .build();
        data.setHeaders(headerList);
        data.setRemoteIP("127.0.1.1");
        ApiResponse<AnalysisResult> resp = reqApi.analyzeRequestWithHttpInfo(data);
        assertNotNull(resp);
        checkSuccessfulResponseWithoutUserInfo(resp);
    }

    @Test
    public void testSessiondRESTRequests() throws Exception {
        String url = "%s://%s:%s/admin/v1/close-sessions/".formatted(protocol, host, port);
        if (isServletAvailable(url)) {
            RequestData data = new RequestData();
            data.setMethod("POST");
            List<Header> headerList = HeaderBuilder.builder()
                                                   .add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/116.0")
                                                   .build();
            data.setHeaders(headerList);
            data.setRemoteIP("127.0.1.1");

            // test missing body
            try {
                data.setUrl(url + "by-id");
                reqApi.analyzeRequestWithHttpInfo(data);
            } catch (ApiException e) {
                assertEquals(422, e.getCode());
            }

            // test with non-existent session identifier
            try {
                data.setUrl(url + "by-id");
                JSONArray array = new JSONArray(1);
                array.add(0, "nonexistent");
                JSONObject body = new JSONObject();
                body.put("sessionIds", array);
                data.body(Base64.getEncoder().encodeToString(body.getByteArray(false)));
                reqApi.analyzeRequestWithHttpInfo(data);
            } catch (ApiException e) {
                assertEquals(404, e.getCode());
            }

            // test with correct session identifier
            {
                data.setUrl(url + "by-id");
                JSONArray array = new JSONArray(1);
                array.add(0, getAjaxClient().getSession().getId());
                JSONObject body = new JSONObject();
                body.put("sessionIds", array);
                data.body(Base64.getEncoder().encodeToString(body.getByteArray(false)));
                ApiResponse<AnalysisResult> resp = reqApi.analyzeRequestWithHttpInfo(data);
                assertNotNull(resp);
                checkSuccessfulResponseWithoutUserInfo(resp);
            }

            // test with context identifier
            {
                data.setUrl(url + "by-context");
                JSONArray array = new JSONArray(1);
                array.add(0, I(testContext.getId()));
                JSONObject body = new JSONObject();
                body.put("contextIds", array);
                data.body(Base64.getEncoder().encodeToString(body.getByteArray(false)));
                ApiResponse<AnalysisResult> resp = reqApi.analyzeRequestWithHttpInfo(data);
                assertNotNull(resp);
                checkSuccessfulResponseWithoutUserInfo(resp);
            }

            // test with user
            {
                data.setUrl(url + "by-user");
                JSONArray array = new JSONArray(1);
                JSONObject user = new JSONObject();
                user.put("contextId", testUser.getContextId());
                user.put("userId", testUser.getUserId());
                array.add(0, user);
                JSONObject body = new JSONObject();
                body.put("users", array);
                data.body(Base64.getEncoder().encodeToString(body.getByteArray(false)));
                ApiResponse<AnalysisResult> resp = reqApi.analyzeRequestWithHttpInfo(data);
                assertNotNull(resp);
                checkSuccessfulResponseWithoutUserInfo(resp);
            }
        }
    }

    @Test
    public void testContextServiceRequestAnalyzer() {
        final RequestData data = new RequestData();

        String url = "%s://%s:%s/webservices/OXContextService".formatted(protocol, host, "8009");
        final ArrayList<Header> headerList = new ArrayList<>();
        final Header header = new Header();

        header.setName("headername");
        header.setValue("headerValue");
        headerList.add(header);

        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        data.setUrl(url);
        data.setMethod("POST");

        String xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:change>\n"
            + "         <soap:ctx>\n"
            + "            <xsd:id>" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "            <xsd:writeDatabase>\n"
            + "               <xsd:id>KEININTEGER</xsd:id>\n"
            + "            </xsd:writeDatabase>\n"
            + "         </soap:ctx>\n"
            + "         <soap:auth>\n"
            + "            <xsd1:login>oxadminmaster</xsd1:login>\n"
            + "            <xsd1:password>secret</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:change>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        String base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }

        //test that comments dont break everything
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <!-- Outer soap:change element -->\n"
            + "      <soap:change>\n"
            + "         <!-- soap:ctx element -->\n"
            + "         <soap:ctx>\n"
            + "            <!-- Context ID with a comment -->\n"
            + "            <xsd:id><!-- This is my comment about the ID -->" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "            <!-- soap:writeDatabase element -->\n"
            + "            <xsd:writeDatabase>\n"
            + "               <!-- Write Database ID -->\n"
            + "               <xsd:id>KEININTEGER</xsd:id>\n"
            + "            </xsd:writeDatabase>\n"
            + "         </soap:ctx>\n"
            + "         <!-- soap:auth element -->\n"
            + "         <soap:auth>\n"
            + "            <!-- xsd1:login and xsd1:password for authentication -->\n"
            + "            <xsd1:login>oxadminmaster</xsd1:login>\n"
            + "            <xsd1:password>secret</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:change>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }

        //test if request is still successfull even if theres another xsd:id which is not a direct child of the ctx element
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:change>\n"
            + "         <soap:ctx>\n"
            + "            <xsd:writeDatabase>\n"
            + "               <xsd:id>KEININTEGER</xsd:id>\n"
            + "            </xsd:writeDatabase>\n"
            + "            <xsd:id>" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "         </soap:ctx>\n"
            + "         <soap:auth>\n"
            + "            <xsd1:login>oxadminmaster</xsd1:login>\n"
            + "            <xsd1:password>secret</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:change>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }

        //test test fails fors wrong namespace
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.NOTADMIN.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:change>\n"
            + "         <soap:ctx>\n"
            + "            <xsd:writeDatabase>\n"
            + "               <xsd:id>KEININTEGER</xsd:id>\n"
            + "            </xsd:writeDatabase>\n"
            + "            <xsd:id>" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "         </soap:ctx>\n"
            + "         <soap:auth>\n"
            + "            <xsd1:login>oxadminmaster</xsd1:login>\n"
            + "            <xsd1:password>secret</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:change>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            reqApi.analyzeRequest(data);
            fail("Expected ApiException with code 404, but no exception was thrown");
        } catch (ApiException e) {
            assertEquals(404, e.getCode());
        }

        //test if request is successfull if theres a name element instead of id
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:change>\n"
            + "         <soap:ctx>\n"
            + "            <xsd:id>" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "            <xsd:writeDatabase>\n"
            + "               <xsd:id>KEININTEGER</xsd:id>\n"
            + "            </xsd:writeDatabase>\n"
            + "         </soap:ctx>\n"
            + "         <soap:auth>\n"
            + "            <xsd1:login>oxadminmaster</xsd1:login>\n"
            + "            <xsd1:password>secret</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:change>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }

        //test corrupted xml
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapensafv:Header/>\n"
            + "   <soapesadfnv:Body>\n"
            + "      <soap:change>\n"
            + "         </soafasp:fsdaauth>\n"
            + "      </soapfsa:change>\n"
            + "</soapenvfsad:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            reqApi.analyzeRequest(data);
            fail("Expected ApiException with code 404, but no exception was thrown");
        } catch (ApiException e) {
            assertEquals(404, e.getCode());
        }

        //test for non existing user
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:change>\n"
            + "         <soap:ctx>\n"
            + "            <xsd:writeDatabase>\n"
            + "               <xsd:id>KEININTEGER</xsd:id>\n"
            + "            </xsd:writeDatabase>\n"
            + "            <xsd:id>19041904190419041904</xsd:id>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "         </soap:ctx>\n"
            + "         <soap:auth>\n"
            + "            <xsd1:login>oxadminmaster</xsd1:login>\n"
            + "            <xsd1:password>secret</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:change>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";


        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            reqApi.analyzeRequest(data);
            fail("Expected ApiException with code 404, but no exception was thrown");
        } catch (ApiException e) {
            assertEquals(404, e.getCode());
        }

        //test different prefixes
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:othersoapprefix=\"http://soap.admin.openexchange.com\" xmlns:otherxsdprefix=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <othersoapprefix:change>\n"
            + "         <othersoapprefix:ctx>\n"
            + "            <otherxsdprefix:id>" + String.valueOf(testUser.getContextId()) + "</otherxsdprefix:id>\n"
            + "            <otherxsdprefix:name>?</otherxsdprefix:name>\n"
            + "            <otherxsdprefix:writeDatabase>\n"
            + "               <otherxsdprefix:id>KEININTEGER</otherxsdprefix:id>\n"
            + "            </otherxsdprefix:writeDatabase>\n"
            + "         </othersoapprefix:ctx>\n"
            + "         <othersoapprefix:auth>\n"
            + "            <xsd1:login>oxadminmaster</xsd1:login>\n"
            + "            <xsd1:password>secret</xsd1:password>\n"
            + "         </othersoapprefix:auth>\n"
            + "      </othersoapprefix:change>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
            reqApi.analyzeRequestWithHttpInfo(data);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }

        //test "create" gets ignored
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:create>\n"
            + "         <soap:ctx>\n"
            + "            <xsd:id>10</xsd:id>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "            <xsd:writeDatabase>\n"
            + "               <xsd:id>KEININTEGER</xsd:id>\n"
            + "            </xsd:writeDatabase>\n"
            + "         </soap:ctx>\n"
            + "         <soap:auth>\n"
            + "            <xsd1:login>oxadminmaster</xsd1:login>\n"
            + "            <xsd1:password>secret</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:create>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            reqApi.analyzeRequest(data);
            fail("Expected ApiException with code 404, but no exception was thrown");
        } catch (ApiException e) {
            assertEquals(404, e.getCode());
        }

        //test "listByDatabase" gets ignored
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "        <soapenv:Header/>\n"
            + "        <soapenv:Body>\n"
            + "           <soap:listByDatabase>\n"
            + "              <soap:db>\n"
            + "                 <xsd:name>oxuserdb_5</xsd:name>\n"
            + "              </soap:db>\n"
            + "              <soap:auth>\n"
            + "                 <xsd1:login>oxadminmaster</xsd1:login>\n"
            + "                 <xsd1:password>secret</xsd1:password>\n"
            + "              </soap:auth>\n"
            + "           </soap:listByDatabase>\n"
            + "        </soapenv:Body>\n"
            + "     </soapenv:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            reqApi.analyzeRequest(data);
            fail("Expected ApiException with code 404, but no exception was thrown");
        } catch (ApiException e) {
            assertEquals(404, e.getCode());
        }

        //test that the element has to be inside the body
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "           <soap:change>\n"
            + "         <!--Optional:-->\n"
            + "         <soap:ctx>\n"
            + "            <xsd:id>" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "            <xsd:writeDatabase>\n"
            + "               <xsd:id>KEININTEGER</xsd:id>\n"
            + "            </xsd:writeDatabase>\n"
            + "         </soap:ctx>\n"
            + "         <!--Optional:-->\n"
            + "         <soap:auth>\n"
            + "            <!--Optional:-->\n"
            + "            <xsd1:login>oxadminmaster</xsd1:login>\n"
            + "            <!--Optional:-->\n"
            + "            <xsd1:password>secret</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:change>\n"
            + "   <soapenv:Body>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            reqApi.analyzeRequest(data);
            fail("Expected ApiException with code 404, but no exception was thrown");
        } catch (ApiException e) {
            assertEquals(404, e.getCode());
        }
    }

    @Test
    public void testGroupServiceRequestAnalyzer() {
        final RequestData data = new RequestData();

        String url = "%s://%s:%s/webservices/OXGroupService".formatted(protocol, host, "8009");
        final ArrayList<Header> headerList = new ArrayList<>();
        final Header header = new Header();

        header.setName("headername");
        header.setValue("headerValue");
        headerList.add(header);

        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        data.setUrl(url);
        data.setMethod("POST");

        String xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:getMembers>\n"
            + "         <soap:ctx>\n"
            + "            <xsd:average_size>?</xsd:average_size>\n"
            + "            <xsd:enabled>?</xsd:enabled>\n"
            + "            <xsd:filestoreId>?</xsd:filestoreId>\n"
            + "            <xsd:filestore_name>?</xsd:filestore_name>\n"
            + "            <xsd:id>" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "            <xsd:loginMappings>?</xsd:loginMappings>\n"
            + "            <xsd:maxQuota>?</xsd:maxQuota>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "            <xsd:usedQuota>?</xsd:usedQuota>\n"
            + "            <xsd:userAttributes>\n"
            + "               <xsd:entries>\n"
            + "                  <xsd:key>?</xsd:key>\n"
            + "                  <xsd:value>\n"
            + "                     <xsd:entries>\n"
            + "                        <xsd:key>?</xsd:key>\n"
            + "                        <xsd:value>?</xsd:value>\n"
            + "                     </xsd:entries>\n"
            + "                  </xsd:value>\n"
            + "               </xsd:entries>\n"
            + "            </xsd:userAttributes>\n"
            + "            <xsd:gabMode>?</xsd:gabMode>\n"
            + "         </soap:ctx>\n"
            + "         <soap:grp>\n"
            + "            <xsd:displayname>?</xsd:displayname>\n"
            + "            <xsd:id>?</xsd:id>\n"
            + "            <xsd:members>?</xsd:members>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "         </soap:grp>\n"
            + "         <soap:auth>\n"
            + "            <xsd1:login>?</xsd1:login>\n"
            + "            <xsd1:password>?</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:getMembers>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        String base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }

        //test that create for other service than context service works
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:create>\n"
            + "         <soap:ctx>\n"
            + "            <xsd:readDatabase>\n"
            + "               <xsd:currentUnits>?</xsd:currentUnits>\n"
            + "               <xsd:driver>?</xsd:driver>\n"
            + "               <xsd:id>10</xsd:id>\n"
            + "            </xsd:readDatabase>\n"
            + "            <xsd:average_size>?</xsd:average_size>\n"
            + "            <xsd:enabled>?</xsd:enabled>\n"
            + "            <xsd:filestoreId>?</xsd:filestoreId>\n"
            + "            <xsd:filestore_name>?</xsd:filestore_name>\n"
            + "            <xsd:id>" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "            <xsd:loginMappings>?</xsd:loginMappings>\n"
            + "            <xsd:maxQuota>?</xsd:maxQuota>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "            <xsd:usedQuota>?</xsd:usedQuota>\n"
            + "            <xsd:userAttributes>\n"
            + "               <xsd:entries>\n"
            + "                  <xsd:key>?</xsd:key>\n"
            + "                  <xsd:value>\n"
            + "                     <xsd:entries>\n"
            + "                        <xsd:key>?</xsd:key>\n"
            + "                        <xsd:value>?</xsd:value>\n"
            + "                     </xsd:entries>\n"
            + "                  </xsd:value>\n"
            + "               </xsd:entries>\n"
            + "            </xsd:userAttributes>\n"
            + "            <xsd:gabMode>?</xsd:gabMode>\n"
            + "         </soap:ctx>\n"
            + "         <soap:grp>\n"
            + "            <xsd:displayname>?</xsd:displayname>\n"
            + "            <xsd:id>?</xsd:id>\n"
            + "            <xsd:members>?</xsd:members>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "         </soap:grp>\n"
            + "         <soap:auth>\n"
            + "            <xsd1:login>?</xsd1:login>\n"
            + "            <xsd1:password>?</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:create>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }
    }

    @Test
    public void testResourceServiceRequestAnalyzer() {
        final RequestData data = new RequestData();

        String url = "%s://%s:%s/webservices/OXResourceService".formatted(protocol, host, "8009");
        final ArrayList<Header> headerList = new ArrayList<>();
        final Header header = new Header();

        header.setName("headername");
        header.setValue("headerValue");
        headerList.add(header);

        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        data.setUrl(url);
        data.setMethod("POST");

        String xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:delete>\n"
            + "         <soap:ctx>\n"
            + "            <xsd:average_size>?</xsd:average_size>\n"
            + "            <xsd:enabled>?</xsd:enabled>\n"
            + "            <xsd:filestoreId>?</xsd:filestoreId>\n"
            + "            <xsd:filestore_name>?</xsd:filestore_name>\n"
            + "            <xsd:id>" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "            <xsd:loginMappings>?</xsd:loginMappings>\n"
            + "            <xsd:maxQuota>?</xsd:maxQuota>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "            <xsd:readDatabase>\n"
            + "               <xsd:currentUnits>?</xsd:currentUnits>\n"
            + "               <xsd:driver>?</xsd:driver>\n"
            + "               <xsd:id>?</xsd:id>\n"
            + "            </xsd:readDatabase>\n"
            + "            <xsd:usedQuota>?</xsd:usedQuota>\n"
            + "            <xsd:userAttributes>\n"
            + "               <xsd:entries>\n"
            + "                  <xsd:key>?</xsd:key>\n"
            + "                  <xsd:value>\n"
            + "                     <xsd:entries>\n"
            + "                        <xsd:key>?</xsd:key>\n"
            + "                        <xsd:value>?</xsd:value>\n"
            + "                     </xsd:entries>\n"
            + "                  </xsd:value>\n"
            + "               </xsd:entries>\n"
            + "            </xsd:userAttributes>\n"
            + "            <xsd:writeDatabase>\n"
            + "               <xsd:currentUnits>?</xsd:currentUnits>\n"
            + "               <xsd:driver>?</xsd:driver>\n"
            + "               <xsd:id>?</xsd:id>\n"
            + "            </xsd:writeDatabase>\n"
            + "            <xsd:gabMode>?</xsd:gabMode>\n"
            + "         </soap:ctx>\n"
            + "         <soap:auth>\n"
            + "            <xsd1:login>?</xsd1:login>\n"
            + "            <xsd1:password>?</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:delete>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        String base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }
    }

    @Test
    public void testSecondaryAccountServiceRequestAnalyzer() {
        final RequestData data = new RequestData();

        String url = "%s://%s:%s/webservices/OXSecondaryAccountService".formatted(protocol, host, "8009");
        final ArrayList<Header> headerList = new ArrayList<>();
        final Header header = new Header();

        header.setName("headername");
        header.setValue("headerValue");
        headerList.add(header);

        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        data.setUrl(url);
        data.setMethod("POST");

        String xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:delete>\n"
            + "         <soap:primaryAddress>?</soap:primaryAddress>\n"
            + "         <soap:context>\n"
            + "            <xsd:id>" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "         </soap:context>\n"
            + "         <soap:users>\n"
            + "            <xsd:id>?</xsd:id>\n"
            + "         </soap:users>\n"
            + "         <soap:groups>\n"
            + "            <xsd:id>?</xsd:id>\n"
            + "         </soap:groups>\n"
            + "         <soap:auth>\n"
            + "            <xsd1:login>?</xsd1:login>\n"
            + "            <xsd1:password>?</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:delete>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        String base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }
    }

    @Test
    public void testTaskMgmtServiceRequestAnalyzer() {
        final RequestData data = new RequestData();

        String url = "%s://%s:%s/webservices/OXTaskMgmtService".formatted(protocol, host, "8009");
        final ArrayList<Header> headerList = new ArrayList<>();
        final Header header = new Header();

        header.setName("headername");
        header.setValue("headerValue");
        headerList.add(header);

        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        data.setUrl(url);
        data.setMethod("POST");

        String xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:getJobList>\n"
            + "         <soap:ctx>\n"
            + "            <xsd:average_size>?</xsd:average_size>\n"
            + "            <xsd:enabled>?</xsd:enabled>\n"
            + "            <xsd:filestoreId>?</xsd:filestoreId>\n"
            + "            <xsd:filestore_name>?</xsd:filestore_name>\n"
            + "            <xsd:id>" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "            <xsd:loginMappings>?</xsd:loginMappings>\n"
            + "            <xsd:maxQuota>?</xsd:maxQuota>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "            <xsd:usedQuota>?</xsd:usedQuota>\n"
            + "            <xsd:userAttributes>\n"
            + "               <xsd:entries>\n"
            + "                  <xsd:key>?</xsd:key>\n"
            + "                  <xsd:value>\n"
            + "                     <xsd:entries>\n"
            + "                        <xsd:key>?</xsd:key>\n"
            + "                        <xsd:value>?</xsd:value>\n"
            + "                     </xsd:entries>\n"
            + "                  </xsd:value>\n"
            + "               </xsd:entries>\n"
            + "            </xsd:userAttributes>\n"
            + "         </soap:ctx>\n"
            + "         <soap:cred>\n"
            + "            <xsd1:login>?</xsd1:login>\n"
            + "            <xsd1:password>?</xsd1:password>\n"
            + "         </soap:cred>\n"
            + "      </soap:getJobList>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        String base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }
    }

    @Test
    public void testUserServiceRequestAnalyzer() {
        final RequestData data = new RequestData();

        String url = "%s://%s:%s/webservices/OXUserService".formatted(protocol, host, "8009");
        final ArrayList<Header> headerList = new ArrayList<>();
        final Header header = new Header();

        header.setName("headername");
        header.setValue("headerValue");
        headerList.add(header);

        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        data.setUrl(url);
        data.setMethod("POST");

        String xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:list>\n"
            + "         <soap:ctx>\n"
            + "            <xsd:id>" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "         </soap:ctx>\n"
            + "         <soap:search_pattern>?</soap:search_pattern>\n"
            + "         <soap:auth>\n"
            + "            <xsd1:login>oxadmin</xsd1:login>\n"
            + "            <xsd1:password>secret</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:list>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        String base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }
    }

    @Test
    public void testUserCopyServiceRequestAnalyzer() {
        final RequestData data = new RequestData();

        String url = "%s://%s:%s/webservices/OXUserCopyService".formatted(protocol, host, "8009");
        final ArrayList<Header> headerList = new ArrayList<>();
        final Header header = new Header();

        header.setName("headername");
        header.setValue("headerValue");
        headerList.add(header);

        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        data.setUrl(url);
        data.setMethod("POST");

        String xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.copy.user.admin.openexchange.com\" xmlns:xsd=\"http://dataobjects.soap.admin.openexchange.com/xsd\" xmlns:xsd1=\"http://dataobjects.rmi.admin.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:copyUser>\n"
            + "         <soap:user>\n"
            + "            <xsd:default_group>\n"
            + "               <xsd:displayname>?</xsd:displayname>\n"
            + "               <xsd:id>?</xsd:id>\n"
            + "               <xsd:members>?</xsd:members>\n"
            + "               <xsd:name>?</xsd:name>\n"
            + "            </xsd:default_group>\n"
            + "            <xsd:guiPreferencesForSoap>\n"
            + "               <xsd:entries>\n"
            + "                  <xsd:key>?</xsd:key>\n"
            + "                  <xsd:value>?</xsd:value>\n"
            + "               </xsd:entries>\n"
            + "            </xsd:guiPreferencesForSoap>\n"
            + "            <xsd:userAttributes>\n"
            + "               <xsd:entries>\n"
            + "                  <xsd:key>?</xsd:key>\n"
            + "                  <xsd:value>\n"
            + "                     <xsd:entries>\n"
            + "                        <xsd:key>?</xsd:key>\n"
            + "                        <xsd:value>?</xsd:value>\n"
            + "                     </xsd:entries>\n"
            + "                  </xsd:value>\n"
            + "               </xsd:entries>\n"
            + "            </xsd:userAttributes>\n"
            + "         </soap:user>\n"
            + "         <soap:src>\n"
            + "            <xsd:average_size>?</xsd:average_size>\n"
            + "            <xsd:enabled>?</xsd:enabled>\n"
            + "            <xsd:filestoreId>?</xsd:filestoreId>\n"
            + "            <xsd:filestore_name>?</xsd:filestore_name>\n"
            + "            <xsd:id>?</xsd:id>\n"
            + "            <xsd:loginMappings>?</xsd:loginMappings>\n"
            + "            <xsd:maxQuota>?</xsd:maxQuota>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "            <xsd:usedQuota>?</xsd:usedQuota>\n"
            + "            <xsd:userAttributes>\n"
            + "               <xsd:entries>\n"
            + "                  <xsd:key>?</xsd:key>\n"
            + "                  <xsd:value>\n"
            + "                     <xsd:entries>\n"
            + "                        <xsd:key>?</xsd:key>\n"
            + "                        <xsd:value>?</xsd:value>\n"
            + "                     </xsd:entries>\n"
            + "                  </xsd:value>\n"
            + "               </xsd:entries>\n"
            + "            </xsd:userAttributes>\n"
            + "            <xsd:gabMode>?</xsd:gabMode>\n"
            + "         </soap:src>\n"
            + "         <soap:dest>\n"
            + "            <xsd:average_size>?</xsd:average_size>\n"
            + "            <xsd:enabled>?</xsd:enabled>\n"
            + "            <xsd:filestoreId>?</xsd:filestoreId>\n"
            + "            <xsd:filestore_name>?</xsd:filestore_name>\n"
            + "            <xsd:id>" + String.valueOf(testUser.getContextId()) + "</xsd:id>\n"
            + "            <xsd:loginMappings>?</xsd:loginMappings>\n"
            + "            <xsd:maxQuota>?</xsd:maxQuota>\n"
            + "            <xsd:name>?</xsd:name>\n"
            + "            <xsd:usedQuota>?</xsd:usedQuota>\n"
            + "            <xsd:userAttributes>\n"
            + "               <xsd:entries>\n"
            + "                  <xsd:key>?</xsd:key>\n"
            + "                  <xsd:value>\n"
            + "                     <xsd:entries>\n"
            + "                        <xsd:key>?</xsd:key>\n"
            + "                        <xsd:value>?</xsd:value>\n"
            + "                     </xsd:entries>\n"
            + "                  </xsd:value>\n"
            + "               </xsd:entries>\n"
            + "            </xsd:userAttributes>\n"
            + "            <xsd:gabMode>?</xsd:gabMode>\n"
            + "         </soap:dest>\n"
            + "         <soap:auth>\n"
            + "            <xsd1:login>?</xsd1:login>\n"
            + "            <xsd1:password>?</xsd1:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:copyUser>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>\n"
            + "";

        String base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }
    }

    @Test
    public void testSessionServiceRequestAnalyzer() {
        final RequestData data = new RequestData();

        String url = "%s://%s:%s/webservices/OXSessionService".formatted(protocol, host, "8009");
        final ArrayList<Header> headerList = new ArrayList<>();
        final Header header = new Header();

        header.setName("headername");
        header.setValue("headerValue");
        headerList.add(header);

        data.setHeaders(headerList);
        data.setRemoteIP("127.0.0.1");
        data.setUrl(url);
        data.setMethod("POST");

        //test sessionId
        String xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.sessiond.openexchange.com\" xmlns:xsd=\"http://auth.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:clearUserSession>\n"
            + "            <soap:sessionId>" + String.valueOf(getAjaxClient().getSession().getId()) + "</soap:sessionId>\n"
            + "         <soap:auth>\n"
            + "            <xsd:login>?</xsd:login>\n"
            + "            <xsd:password>?</xsd:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:clearUserSession>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        String base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }

        //test contextId
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.sessiond.openexchange.com\" xmlns:xsd=\"http://auth.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:clearUserSessions>\n"
            + "         <soap:contextId>" + String.valueOf(testUser.getContextId()) + "</soap:contextId>\n"
            + "         <soap:userId>?</soap:userId>\n"
            + "         <soap:auth>\n"
            + "            <xsd:login>?</xsd:login>\n"
            + "            <xsd:password>?</xsd:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:clearUserSessions>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }

        //test that comments dont break the code
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.sessiond.openexchange.com\" xmlns:xsd=\"http://auth.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <!-- soap:clearUserSession element -->\n"
            + "      <soap:clearUserSession>\n"
            + "         <!-- sessionId element with dynamic content -->\n"
            + "         <soap:sessionId><!-- Dynamic content: Session ID -->" + String.valueOf(getAjaxClient().getSession().getId()) + "</soap:sessionId>\n"
            + "         <!-- soap:auth element -->\n"
            + "         <soap:auth>\n"
            + "            <!-- xsd:login and xsd:password for authentication -->\n"
            + "            <xsd:login>?</xsd:login>\n"
            + "            <xsd:password>?</xsd:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:clearUserSession>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            final ApiResponse<AnalysisResult> response = reqApi.analyzeRequestWithHttpInfo(data);
            checkSuccessfulResponseWithoutUserInfo(response);
        } catch (final ApiException e) {
            fail("Unable to get marker: %s".formatted(e.getMessage()));
            assertEquals(400, e.getCode());
        }

        //test forbidden operation clearContextsSessions
        xmlBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.sessiond.openexchange.com\" xmlns:xsd=\"http://auth.openexchange.com/xsd\">\n"
            + "   <soapenv:Header/>\n"
            + "   <soapenv:Body>\n"
            + "      <soap:clearContextsSessions>\n"
            + "         <soap:contextId>5</soap:contextId>\n"
            + "         <soap:userId>?</soap:userId>\n"
            + "         <soap:auth>\n"
            + "            <xsd:login>?</xsd:login>\n"
            + "            <xsd:password>?</xsd:password>\n"
            + "         </soap:auth>\n"
            + "      </soap:clearUserSessions>\n"
            + "   </soapenv:Body>\n"
            + "</soapenv:Envelope>";

        base64 = Base64.getEncoder().encodeToString(xmlBody.getBytes());
        data.setBody(base64);

        try {
            reqApi.analyzeRequest(data);
            fail("Expected ApiException with code 404, but no exception was thrown");
        } catch (ApiException e) {
            assertEquals(404, e.getCode());
        }
    }

    @Test
    public void testIMipPushRESTRequest() throws Exception {
        String url = "%s://%s:%s/chronos/v1/itip/pushmail".formatted(protocol, host, port);

        if (isServletAvailable(url)) {
            RequestData data = new RequestData();
            data.setMethod("PUT");
            List<Header> headerList = HeaderBuilder.builder()
                                                   .add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/116.0")
                                                   .build();
            data.setHeaders(headerList);
            data.setRemoteIP("127.0.1.1");
            data.setUrl(url);

            // test missing body
            try {
                reqApi.analyzeRequestWithHttpInfo(data);
            } catch (ApiException e) {
                assertEquals(422, e.getCode());
            }

            // generate proper iMIP message for analysis with body
            UserData userData = new UserApi(testUser.getApiClient()).getUser(String.valueOf(testUser.getUserId())).getData();
            String iTip = //@formatter:off
                "BEGIN:VCALENDAR\r\n" +
                "METHOD:REQUEST\r\n" +
                "VERSION:2.0\r\n" +
                "BEGIN:VEVENT\r\n" +
                "UID:2886db71-80e4-4b91-9aaf-6387d7f32016\r\n" +
                "DTSTAMP:202400101T00000Z\r\n" +
                "DTSTART:20240608T150000Z\r\n" +
                "DTEND:20240608T160000Z\r\n" +
                "SUMMARY:Hallo\r\n" +
                "ORGANIZER;CN=Organisator:mailto:organizer@example.com\r\n" +
                "ATTENDEE;PARTSTAT=NEEDS-ACTION:mailto:" + userData.getEmail1() + "\r\n" +
                "END:VEVENT\r\n" +
                "END:VCALENDAR"
            ; //@formatter:on

            String mimeMessage = com.openexchange.ajax.chronos.itip.AbstractITipTest.generateImip(
                "organizer@example.com", userData.getEmail1(), null, "Test", new Date(), SchedulingMethod.REQUEST, iTip);

            JSONObject body = new JSONObject();
            body.put("user", testUser.getUserId() + "@" + testUser.getContextId());
            body.put("imap-uidvalidity", "1234567890");
            body.put("imap-uid", "12345");
            body.put("folder", "INBOX");
            body.put("event", "messageNew");
            body.put("from", "tester@example.org");
            body.put("subject", "Request Analyzer Test for IMip Push");
            body.put("body", mimeMessage);
            data.setBody(Base64.getEncoder().encodeToString(body.getByteArray(false)));
            ApiResponse<AnalysisResult> resp = reqApi.analyzeRequestWithHttpInfo(data);
            assertNotNull(resp);
            checkSuccessfulResponseWithoutUserInfo(resp);
        }
    }

    // ----------------- private methods ---------------------

    private static final Integer BITS_ADMIN = Integer.valueOf(403710016);

    /**
     * Inserts a new folder
     *
     * @param foldersApi The {@link FoldersApi} to use
     * @param api The folder api to use
     * @param parent The parent folder
     * @return The {@link FolderData} of the new folder
     * @throws com.openexchange.testing.httpclient.invoker.ApiException
     */
    private FolderData insertFolder(FoldersApi foldersApi, EnumAPI api, String parent) throws com.openexchange.testing.httpclient.invoker.ApiException {
        final NewFolderBody body = new NewFolderBody();
        final NewFolderBodyFolder folder = new NewFolderBodyFolder();
        folder.setTitle(UUID.randomUUID().toString());
        folder.setFolderId(parent);
        folder.setModule(Module.INFOSTORE.getName());
        folder.setType(I(FolderObject.PRIVATE));
        folder.setPermissions(createPermissions(testUser));
        body.setFolder(folder);

        final String tree = String.valueOf(api.getTreeId());

        final FolderUpdateResponse resp = foldersApi.createFolderBuilder()
                                              .withTree(tree)
                                              .withFolderId(parent)
                                              .withNewFolderBody(body)
                                              .execute();
        assertNull(resp.getError(), resp.getErrorDesc());
        assertNotNull(resp.getData());
        final FolderResponse folderResponse = foldersApi.getFolderBuilder()
                                                  .withId(resp.getData())
                                                  .withTree(tree)
                                                  .execute();
        assertNull(folderResponse.getError(), folderResponse.getErrorDesc());
        assertNotNull(folderResponse.getData());
        return folderResponse.getData();
    }

    /**
     * Creates a list of permissions for a new folder
     *
     * @param user The user which creates the folder
     * @return The list of permissions
     */
    private List<FolderPermission> createPermissions(TestUser user) {
        final List<FolderPermission> result = new ArrayList<>();
        final FolderPermission adminPerm = createPermissionFor(I(user.getUserId()), BITS_ADMIN, Boolean.FALSE);
        result.add(adminPerm);
        return result;
    }

    /**
     * Creates a {@link FolderPermission} for a given entity
     *
     * @param entity The entity id
     * @param bits The permission bits
     * @param isGroup Whether the entity is a group or not
     * @return The {@link FolderPermission}
     */
    private FolderPermission createPermissionFor(Integer entity, Integer bits, Boolean isGroup) {
        final FolderPermission p = new FolderPermission();
        p.setEntity(entity);
        p.setGroup(isGroup);
        p.setBits(bits);
        return p;
    }

    /**
     * Checks that the response is successful and contains the correct data for user 1
     *
     * @param response The response to check
     * @throws OXException
     * @throws IOException
     * @throws JSONException
     */
    private void checkSuccessfulResponse(ApiResponse<AnalysisResult> response) throws OXException, IOException, JSONException {
        assertEquals(200, response.getStatusCode());
        assertNotNull(response.getData());
        final AnalysisResultHeaders headers = response.getData().getHeaders();
        assertNotNull(headers);
        final UserValues userValues = getAjaxClient().getValues();
        Optional<Integer> userId = Optional.ofNullable(headers.getxOxUserId());
        if (userId.isPresent()) {
            assertEquals(userValues.getUserId(), userId.get());
        }
        assertEquals(userValues.getContextId(), headers.getxOxContextId());
    }

    /**
     * Checks that the response is successful and contains the correct data for administrative requests
     *
     * @param response The response to check
     */
    private void checkSuccessfulResponseWithoutUserInfo(ApiResponse<AnalysisResult> response) {
        assertEquals(200, response.getStatusCode());
        assertNotNull(response.getData());
        final String marker = response.getData().getMarker();
        assertNotNull(marker);
    }

    /**
     *
     * Checks that an invalid request returns the right exception code
     *
     * @param data The request data
     * @param exceptionCode The expected exception code
     */
    private void testInvalidRequest(RequestData data, int exceptionCode) {
        try {
            reqApi.analyzeRequest(data);
            fail();
        } catch (final ApiException e) {
            assertEquals(exceptionCode, e.getCode());
        }
    }

    /**
     * Performs an <code>analyze</code> request to the given {@link RequestAnalysisApi} and checks the HTTP status code of the received response.
     * <p/>
     * For {@link HttpStatusCode#OK} responses, the analysis result is returned, otherwise an {@link ApiException} is expected whose
     * status code must match the expected one.
     *
     * @param analysisApi The request analysis API to use
     * @param requestData The request data to hand over to the analyzer
     * @param expectedStatusCode The expected status code
     * @return The analysis result for {@link HttpStatusCode#OK} responses, <code>null</code>, otherwise
     */
    private static ApiResponse<AnalysisResult> assertAnalyzeResponse(RequestAnalysisApi analysisApi, RequestData requestData, HttpStatusCode expectedStatusCode) {
        try {
            ApiResponse<AnalysisResult> analysisResult = analysisApi.analyzeRequestWithHttpInfo(requestData);
            if (HttpStatusCode.OK.equals(expectedStatusCode) && HttpStatusCode.OK.getCode() == analysisResult.getStatusCode()) {
                return analysisResult;
            }
            fail("Expected response with HTTP status " + expectedStatusCode);
        } catch (ApiException e) {
            assertEquals(expectedStatusCode.getCode(), e.getCode());
        }
        return null;

    }

    /**
     * Initializes and prepares a {@link RequestData} object for a typical Ajax request to the HTTP API.
     * <p/>
     * Typical header values (like <code>User-Agent</code>, <code>Accept</code> and <code>Host</code> are included implicitly for a
     * exemplary, browser-based client).
     *
     * @param httpMethod The HTTP Method to indicate, e.g. <code>PUT</code>
     * @param ajaxModule The Ajax module to append as path segment to the request URI
     * @param ajaxAction The Ajax action to append as <code>action</code> query parameter
     * @param body The request body to indicate (as encoded string), or <code>null</code> to omit
     * @param headers Additional headers to set in the request data, or <code>null</code> if not applicable
     * @param uriParameters Additional parameters to include in the request URI, or <code>null</code> if not applicable
     * @return The request data
     */
    private RequestData prepareRequestData(String httpMethod, String ajaxModule, String ajaxAction, String body, Header[] headers, NameValuePair... uriParameters) throws NumberFormatException, URISyntaxException {
        URIBuilder uriBuilder = new URIBuilder()
            .setScheme(protocol)
            .setPort(Integer.parseInt(port))
            .setHost(host)
            .setPathSegments(Strings.trimStart(Strings.trimEnd(basePath, '/'), '/'), Strings.trimStart(Strings.trimEnd(ajaxModule, '/'), '/'))
        ;
        if (null != ajaxAction) {
            uriBuilder.setParameter("action", "redeemToken");
        }
        if (null != uriParameters) {
            uriBuilder.addParameters(Arrays.asList(uriParameters));
        }
        RequestData requestData = new RequestData();
        requestData.setMethod(httpMethod);
        requestData.setRemoteIP("198.51.100.77");
        requestData.setUrl(uriBuilder.build().toASCIIString());
        requestData.setBody(body);
        requestData.addHeadersItem(new Header().name(HttpHeaders.USER_AGENT).value("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"));
        requestData.addHeadersItem(new Header().name(HttpHeaders.ACCEPT).value("application/json, text/javascript, */*; q=0.01"));
        requestData.addHeadersItem(new Header().name(HttpHeaders.HOST).value(host));
        if (null != headers && 0 < headers.length) {
            for (Header header : headers) {
                requestData.addHeadersItem(header);
            }
        }
        return requestData;
    }

    /** Checks if servlet is available for given URL
     *
     * @param url The URL to check
     * @return <code>true</code> if response for URL is not <code>NOT FOUND</code>, <code>false</code> otherwise
     */
    private boolean isServletAvailable(String url) {
        HttpGet request = new HttpGet(url);
        try  (CloseableHttpClient client = HttpClientBuilder.create().build(); CloseableHttpResponse response = client.execute(request);) {
            return 404 != response.getStatusLine().getStatusCode();
        } catch (Exception e) {
            return false;
        }
    }

}
