/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.context.preassembly;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.net.MalformedURLException;
import java.util.List;
import javax.ws.rs.core.Response.Status;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.admin.soap.context.dataobjects.Context;
import com.openexchange.admin.soap.context.soap.OXContextServicePortType;
import com.openexchange.ajax.chronos.scheduling.RESTUtilities;
import com.openexchange.ajax.framework.AbstractAPIClientSession;
import com.openexchange.test.common.test.pool.soap.SoapContextService;
import com.openexchange.testing.restclient.invoker.ApiException;
import com.openexchange.testing.restclient.models.PreAssembleRequest;
import com.openexchange.testing.restclient.models.PreAssemblyResponse;
import com.openexchange.testing.restclient.modules.AdminApi;

/**
 * {@link ContextPreAssemblyAPITests} - Tests for the pre-assemble REST API
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class ContextPreAssemblyAPITests extends AbstractAPIClientSession {

    private AdminApi adminApi;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        com.openexchange.testing.restclient.invoker.ApiClient adminRestClient = RESTUtilities.createRESTMasterClient(ContextPreAssemblyAPITests.class.toGenericString());
        this.adminApi = new AdminApi(adminRestClient);
    }

    @Test
    public void testPreAssemble_emptyRequest() {
        boolean exceptionThrown = false;
        try {
            this.adminApi.preAssemble(new PreAssembleRequest());
        } catch (ApiException e) {
            assertEquals(Status.BAD_REQUEST.getStatusCode(), e.getCode(), "Received wrong status: " + e.getResponseBody());
            exceptionThrown = true;
        }
        assertTrue(exceptionThrown);
    }

    @Test
    public void testPreAssemble_schemaNotSent() {
        boolean exceptionThrown = false;
        try {
            PreAssembleRequest preAssembleRequest = new PreAssembleRequest();
            preAssembleRequest.number(Long.valueOf(3));

            this.adminApi.preAssemble(preAssembleRequest);
        } catch (ApiException e) {
            assertEquals(Status.BAD_REQUEST.getStatusCode(), e.getCode(), "Received wrong status: " + e.getResponseBody());
            exceptionThrown = true;
        }
        assertTrue(exceptionThrown);
    }

    @Test
    public void testPreAssemble_umknownSchema() {
        boolean exceptionThrown = false;
        try {
            PreAssembleRequest preAssembleRequest = new PreAssembleRequest();
            preAssembleRequest.schema("unknownSchema");
            preAssembleRequest.number(Long.valueOf(3));

            this.adminApi.preAssemble(preAssembleRequest);
        } catch (ApiException e) {
            assertEquals(Status.BAD_REQUEST.getStatusCode(), e.getCode(), "Received wrong status: " + e.getResponseBody());
            exceptionThrown = true;
        }
        assertTrue(exceptionThrown);
    }

    @Test
    public void testPreAssemble_numberNotSent() {
        boolean exceptionThrown = false;
        try {
            PreAssembleRequest preAssembleRequest = new PreAssembleRequest();
            preAssembleRequest.schema(getValidSchema());

            this.adminApi.preAssemble(preAssembleRequest);
        } catch (ApiException e) {
            assertEquals(Status.BAD_REQUEST.getStatusCode(), e.getCode(), "Received wrong status: " + e.getResponseBody());
            exceptionThrown = true;
        }
        assertTrue(exceptionThrown);
    }

    @Test
    public void testPreAssemble_numberNotValid() {
        boolean exceptionThrown = false;
        try {
            PreAssembleRequest preAssembleRequest = new PreAssembleRequest();
            preAssembleRequest.schema(getValidSchema());
            preAssembleRequest.number(Long.valueOf(0));

            this.adminApi.preAssemble(preAssembleRequest);
        } catch (ApiException e) {
            assertEquals(Status.BAD_REQUEST.getStatusCode(), e.getCode(), "Received wrong status: " + e.getResponseBody());
            exceptionThrown = true;
        }
        assertTrue(exceptionThrown);
    }

    @Test
    public void testPreAssemble_numberTooLarge() {
        boolean exceptionThrown = false;
        try {
            PreAssembleRequest preAssembleRequest = new PreAssembleRequest();
            preAssembleRequest.schema(getValidSchema());
            preAssembleRequest.number(Long.valueOf(505));

            this.adminApi.preAssemble(preAssembleRequest);
        } catch (ApiException e) {
            assertEquals(Status.BAD_REQUEST.getStatusCode(), e.getCode(), "Received wrong status: " + e.getResponseBody());
            exceptionThrown = true;
        }
        assertTrue(exceptionThrown);
    }

    @Test
    public void testPreAssemble_preAssembledContextsReturned() throws Exception {
        PreAssembleRequest preAssembleRequest = new PreAssembleRequest();
        preAssembleRequest.schema(getValidSchema());

        Long numberOfContextsToPreAssemble = Long.valueOf(3);
        preAssembleRequest.number(numberOfContextsToPreAssemble);

        PreAssemblyResponse response = this.adminApi.preAssemble(preAssembleRequest);
        List<Integer> contextIds = response.getContextIds();

        assertEquals(numberOfContextsToPreAssemble.intValue(), contextIds.size());
        for (Integer contextId : contextIds) {
            assertTrue(contextId.intValue() > 0);
        }
    }

    private String getValidSchema() {
        try {
            SoapContextService instance = SoapContextService.getInstance();
            OXContextServicePortType contextPort = (OXContextServicePortType) instance.getPortType();
            List<Context> list = contextPort.list("*", Boolean.FALSE, instance.getSoapMasterCreds());
            assertTrue(list.size() > 0, "No context found.");
            return list.get(0).getReadDatabase().getScheme();
        } catch (MalformedURLException | com.openexchange.admin.soap.context.soap.StorageException_Exception | com.openexchange.admin.soap.context.soap.InvalidCredentialsException_Exception | com.openexchange.admin.soap.context.soap.InvalidDataException_Exception | com.openexchange.admin.soap.context.soap.RemoteException_Exception e) {
            fail(e);
        }
        fail("No valid schema found.");
        return "";
    }

}
