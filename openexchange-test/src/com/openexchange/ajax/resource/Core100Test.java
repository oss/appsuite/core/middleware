/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.framework.AbstractConfigAwareAPIClientSession;
import com.openexchange.java.util.UUIDs;
import com.openexchange.test.common.test.pool.ProvisioningUtils;
import com.openexchange.testing.httpclient.models.CommonResponse;
import com.openexchange.testing.httpclient.models.ResourceData;
import com.openexchange.testing.httpclient.models.ResourceResponse;
import com.openexchange.testing.httpclient.models.ResourceUpdateResponse;
import com.openexchange.testing.httpclient.modules.ResourcesApi;

/**
 * {@link Core100Test}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class Core100Test extends AbstractConfigAwareAPIClientSession {

    private ResourcesApi resourceApi;
    private Integer resourceId;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        resourceApi = new ResourcesApi(getApiClient());
        ResourceData resourceData = new ResourceData();
        String name = UUIDs.getUnformattedStringFromRandom();
        resourceData.setDisplayName(name);
        resourceData.setName(name);
        resourceData.setAvailability(Boolean.TRUE);
        resourceData.setDescription("description");
        resourceData.setMailaddress(ProvisioningUtils.getMailAddress(name, testContext.getId()));
        ResourceUpdateResponse response = resourceApi.createResource(resourceData);
        assertNull(response.getError(), response.getErrorDesc());
        resourceId = response.getData().getId();
    }

    @Test
    public void testChangeDisplayName() throws Exception {
        /*
         * get resource from server
         */
        ResourceResponse resourceResponse = resourceApi.getResource(resourceId);
        assertNull(resourceResponse.getError(), resourceResponse.getErrorDesc());
        ResourceData originalResourceData = resourceResponse.getData();
        assertEquals(Boolean.TRUE, originalResourceData.getAvailability());
        Long clientTimestamp = resourceResponse.getTimestamp();
        /*
         * update description
         */
        ResourceData resourceUpdate = new ResourceData().displayName("new display name");
        CommonResponse updateResponse = resourceApi.updateResource(resourceId, clientTimestamp, resourceUpdate);
        assertNull(updateResponse.getError(), updateResponse.getErrorDesc());
        /*
         * get & check updated resource from server (multiple times)
         */
        for (int i = 0; i < 5; i++) {
            resourceResponse = resourceApi.getResource(resourceId);
            assertNull(resourceResponse.getError(), resourceResponse.getErrorDesc());
            ResourceData updatedResourceData = resourceResponse.getData();
            assertEquals(Boolean.TRUE, updatedResourceData.getAvailability());
            assertEquals("new display name", updatedResourceData.getDisplayName());
        }
    }

}
