
package com.openexchange.ajax.infostore.apiclient;

import static com.openexchange.java.Autoboxing.L;
import static com.openexchange.java.Autoboxing.l;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.openexchange.java.util.Pair;
import com.openexchange.java.util.UUIDs;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.InfoItemData;
import com.openexchange.testing.httpclient.models.InfoItemUpdateResponse;
import com.openexchange.testing.httpclient.modules.DriveApi;

/**
 * {@link DownloadTests}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class DownloadTests extends InfostoreApiClientTest {

    @Test
    public void downloadAndCheckRanges() throws ApiException, IOException {
        String filename = UUIDs.getUnformattedStringFromRandom() + ".bin";
        byte[] data = getRandomData(1000 * 5);
        InfoItemUpdateResponse response = infostoreApi.uploadInfoItemBuilder().withBody(data).withFolderId(folderId).withFilename(filename).execute();
        String id = checkResponse(response.getError(), response.getErrorDesc(), response.getData());
        InfoItemData infoItem = getItem(id);
        String checksum = DigestUtils.md5Hex(data);
        DriveApi driveApi = new DriveApi(getApiClient());
        List<Pair<Long, Long>> offsetAndLengthPairs = new ArrayList<>();
        offsetAndLengthPairs.add(new Pair<Long, Long>(L(0L), L(12L)));
        offsetAndLengthPairs.add(new Pair<Long, Long>(L(data.length / 2), L(19)));
        offsetAndLengthPairs.add(new Pair<Long, Long>(L(data.length - 53), L(53)));
        offsetAndLengthPairs.add(new Pair<Long, Long>(L(0), L(data.length)));
        for (Pair<Long, Long> offsetAndLengthPair : offsetAndLengthPairs) {
            byte[] expectedRange = Arrays.copyOfRange(data, (int) l(offsetAndLengthPair.getFirst()), (int) (l(offsetAndLengthPair.getFirst()) + l(offsetAndLengthPair.getSecond())));
            downloadAndCheckPart(driveApi, folderId, infoItem.getFilename(), checksum, expectedRange, offsetAndLengthPair.getFirst(), offsetAndLengthPair.getSecond());
        }
    }

    private static void downloadAndCheckPart(DriveApi driveApi, String folderId, String filename, String checksum, byte[] expectedData, Long offset, Long length) throws ApiException, IOException {
        File downloadedFile = null;
        try {
            downloadedFile = driveApi.downloadFileBuilder().withRoot(folderId).withPath("/").withName(filename).withChecksum(checksum).withOffset(offset).withLength(length).execute();
            Assertions.assertArrayEquals(expectedData, Files.readAllBytes(downloadedFile.toPath()));
        } finally {
            if (null != downloadedFile) {
                try {
                    downloadedFile.delete();
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                }
            }
        }
    }

    private static byte[] getRandomData(int length) {
        Random random = new Random();
        byte[] data = new byte[length];
        random.nextBytes(data);
        return data;
    }

}
