
package com.openexchange.ajax.infostore.apiclient;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Random;
import java.util.function.Supplier;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.google.common.primitives.Bytes;
import com.openexchange.java.util.UUIDs;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.InfoItemData;
import com.openexchange.testing.httpclient.models.InfoItemUpdateResponse;

/**
 * {@link UploadTests}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class UploadTests extends InfostoreApiClientTest {

    @Test
    public void testAppendTwentyBytes() throws Exception {
        testAppend(() -> getRandomData(20));
    }

    @Test
    public void testAppendTenKilobytes() throws Exception {
        testAppend(() -> getRandomData(10 * 1000));
    }

    @Test
    public void testAppendThreeMegabytes() throws Exception {
        testAppend(() -> getRandomData(3 * 1000 * 1000));
    }

    @Test
    public void testAppendSixMegabytes() throws Exception {
        testAppend(() -> getRandomData(6 * 1000 * 1000));
    }

    public void testAppend(Supplier<byte[]> dataSupplier) throws Exception {
        String filename = UUIDs.getUnformattedStringFromRandom() + ".bin";
        byte[] data1 = dataSupplier.get();
        InfoItemUpdateResponse response = infostoreApi.uploadInfoItemBuilder().withBody(data1).withFolderId(folderId).withFilename(filename).execute();
        String id = checkResponse(response.getError(), response.getErrorDesc(), response.getData());
        downloadAndCheck(id, data1);
        InfoItemData infoItem = getItem(id);
        byte[] data2 = dataSupplier.get();
        response = infostoreApi.uploadInfoItemBuilder().withBody(data2).withFolderId(folderId).withId(id).withFilename(filename).withOffset(infoItem.getFileSize()).withTimestamp(response.getTimestamp()).execute();
        id = checkResponse(response.getError(), response.getErrorDesc(), response.getData());
        downloadAndCheck(id, Bytes.concat(data1, data2));
        infoItem = getItem(id);
        byte[] data3 = dataSupplier.get();
        response = infostoreApi.uploadInfoItemBuilder().withBody(data3).withFolderId(folderId).withId(id).withFilename(filename).withOffset(infoItem.getFileSize()).withTimestamp(response.getTimestamp()).execute();
        id = checkResponse(response.getError(), response.getErrorDesc(), response.getData());
        downloadAndCheck(id, Bytes.concat(data1, data2, data3));
    }

    private static byte[] getRandomData(int length) {
        Random random = new Random();
        byte[] data = new byte[length];
        random.nextBytes(data);
        return data;
    }

    private void downloadAndCheck(String id, byte[] expectedData) throws ApiException, IOException {
        File downloadedFile = null;
        try {
            downloadedFile = infostoreApi.getInfoItemDocumentBuilder().withFolder(folderId).withId(id).execute();
            Assertions.assertArrayEquals(expectedData, Files.readAllBytes(downloadedFile.toPath()));
        } finally {
            if (null != downloadedFile) {
                try {
                    downloadedFile.delete();
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                }
            }
        }
    }

}
