/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.mailcompose;

/**
 * {@link ScheduledMailTest}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ScheduledMailTest extends AbstractMailComposeTest {

//    @Test
//    public void testScheduleMail() throws Exception {
//        MailComposeResponseMessageModel model = createNewCompositionSpace();
//
//        MailComposeResponse loaded = api.getMailComposeById(model.getId());
//        check(loaded);
//        assertNotNull(loaded);
//        assertEquals(model.getId(), loaded.getData().getId(), "Wrong Composition Space loaded.");
//
//        model.setFrom(getSender());
//        model.setTo(getRecipient());
//        model.setSubject(UUID.randomUUID().toString());
//        model.setContent(UUID.randomUUID().toString());
//        model.setDateToSend(Long.valueOf(System.currentTimeMillis() + 86400000));
//        String json = new Gson().toJson(model);
//        MailComposeSendResponse postMailComposeSend = api.postMailComposeSend(model.getId(), json, null, null);
//        check(postMailComposeSend);
//        assertTrue(Strings.isEmpty(postMailComposeSend.getError()), postMailComposeSend.getErrorDesc());
//
//        MailaccountApi mailaccountApi = new MailaccountApi(getApiClient());
//        MailAccountResponse accountResponse = mailaccountApi.getAccount(0);
//        String scheduledFullname = accountResponse.getData().getScheduledFullname();
//        assertNotNull(scheduledFullname);
//
//        MailsResponse searchMailsResponse = mailApi.searchMails(scheduledFullname, "600,601", null, null, null, null, "610", "desc");
//        List<List<Object>> list = searchMailsResponse.getData();
//        assertNotNull(list);
//        assertTrue(list.isEmpty() == false, "");
//
//        String id = null;
//        for (List<Object> fields : list) {
//            id = fields.get(0).toString();
//        }
//
//        assertNotNull(id);
//    }

}
