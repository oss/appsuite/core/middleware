
package com.openexchange.ajax.mail.addresscollector;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import com.openexchange.ajax.config.actions.GetRequest;
import com.openexchange.ajax.config.actions.GetResponse;
import com.openexchange.ajax.config.actions.SetRequest;
import com.openexchange.ajax.config.actions.SetResponse;
import com.openexchange.ajax.config.actions.Tree;
import com.openexchange.ajax.framework.AJAXClient;
import com.openexchange.ajax.framework.AbstractAJAXSession;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;

@Disabled
public class ConfigurationTest extends AbstractAJAXSession {

    public ConfigurationTest() {
        super();
    }

    @Test
    public void testFolderId() throws Throwable {

        // since creating the contact collect folder for a user (triggered by the initial login)
        // will perform a 'config set' operation, we need to wait for it's completion.
        // otherwise a race condition can occur with subsequent 'set' calls

        RetryPolicy retryPolicy = new RetryPolicy()
            .withMaxRetries(5)
            .withDelay(1, TimeUnit.SECONDS)
            .retryOn(AssertionError.class);

        Failsafe.with(retryPolicy).get(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                GetRequest getRequest = new GetRequest(Tree.ContactCollectFolder);
                GetResponse getResponse = getClient().execute(getRequest);
                assertTrue(getResponse.hasInteger(), "No contact folder id available. Contact collection folder may not have been initialized yet?");
                return null;
            }
        });

        SetRequest setRequest = new SetRequest(Tree.ContactCollectFolder, I(100));
        SetResponse setResponse = getClient().execute(setRequest);
        assertFalse(setResponse.hasError());

        GetRequest getRequest = new GetRequest(Tree.ContactCollectFolder);
        GetResponse getResponse = getClient().execute(getRequest);
        assertEquals(100, getResponse.getInteger());

        setRequest = new SetRequest(Tree.ContactCollectFolder, I(123));
        setResponse = getClient().execute(setRequest);
        assertFalse(setResponse.hasError());

        getRequest = new GetRequest(Tree.ContactCollectFolder);
        getResponse = getClient().execute(getRequest);
        assertEquals(123, getResponse.getInteger());
    }

    @Override
    public AJAXClient getClient() {
        return super.getClient();
    }
}
