/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import com.openexchange.configuration.ConfigurationExceptionCodes;
import com.openexchange.exception.OXException;
import com.openexchange.test.common.configuration.AJAXConfig;
import com.openexchange.test.common.configuration.TestConfig;
import com.openexchange.test.common.test.pool.TestUser;

/**
 * {@link ProvisioningSetup}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v7.8.3
 */
public class ProvisioningSetup {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ProvisioningSetup.class);

    private static final TestConfig.Property KEY = TestConfig.Property.PROV_PROPS;

    private static AtomicInteger initialized = new AtomicInteger(0);

    private static final String MASTER_IDENTIFIER = "oxadminmaster";
    private static final String MASTER_PWD_IDENTIFIER = "oxadminmaster_password";
    private static final String REST_IDENTIFIER = "restUser";
    private static final String REST_PWD_IDENTIFIER = "restPwd";

    private static TestUser oxAdminMaster = null;

    private static TestUser restUser = null;

    private static final AtomicReference<Future<CloseableHttpClient>> CLIENT = new AtomicReference<>();

    /**
     * Initializes configuration etc.
     *
     * @throws OXException In case it fails
     */
    public static void init() throws OXException {
        synchronized (ProvisioningSetup.class) {
            if (initialized.get() <= 0) {
                AJAXConfig.init();
                Properties specialUsers = getSpecialUsers();
                createOXAdminMaster(specialUsers);
                createRestUser(specialUsers);
            } else {
                LOG.debug("Pool already initialized! Please do not try to remember users/pools multiple times as this will cause unexpected behavior within test execution.");
            }
            initialized.getAndIncrement();
        }
    }

    private static void createOXAdminMaster(Properties contextsAndUsers) {
        String user = contextsAndUsers.get(MASTER_IDENTIFIER).toString();
        String password = contextsAndUsers.get(MASTER_PWD_IDENTIFIER).toString();
        ProvisioningSetup.oxAdminMaster = new TestUser(user, "", password, ProvisioningSetup.class.getCanonicalName());
    }

    private static void createRestUser(Properties contextsAndUsers) {
        String user = contextsAndUsers.get(REST_IDENTIFIER).toString();
        String password = contextsAndUsers.get(REST_PWD_IDENTIFIER).toString();
        ProvisioningSetup.restUser = new TestUser(user, "", password, ProvisioningSetup.class.getCanonicalName());
    }

    public static TestUser getOxAdminMaster() {
        return oxAdminMaster;
    }

    public static String getRestAuth() {
        return restUser.getUser() + ":" + restUser.getPassword();
    }

    public static TestUser getRestUser() {
        return restUser;
    }

    /**
     * Gets the global HTTP client.
     *
     * @return The global HTTP client
     */
    public static CloseableHttpClient getGlobalClient() {
        Future<CloseableHttpClient> client = CLIENT.get();
        if (client == null) {
            FutureTask<CloseableHttpClient> ft = new FutureTask<CloseableHttpClient>(() -> createHttpClient());
            client = CLIENT.compareAndExchange(null, ft);
            if (client == null) {
                client = ft;
                ft.run();
            }
        }
        return getFrom(client);
    }

    /**
     * Gets the computation result from specified future; waiting if necessary for the computation to complete.
     *
     * @param <V> The type of the result value
     * @param f The future to get the computation result from
     * @return The computation result
     * @throws RuntimeException If computation raises an error
     */
    private static <V> V getFrom(Future<V> f) {
        if (f == null) {
            return null;
        }

        try {
            return f.get();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new IllegalStateException(e);
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof RuntimeException rte) {
                throw rte;
            }
            throw new IllegalStateException(cause == null ? e : cause);
        }
    }

    /*
     * ============================== HELPERS ==============================
     */

    /**
     * Returns the configured OXAdminMaster and OxAdmin
     *
     * @return
     * @throws OXException
     */
    private static Properties getSpecialUsers() throws OXException {
        File propFile = getPropFile();

        return readPropFile(propFile);
    }

    private static File getPropFile() throws OXException {
        String propertyFileName = getPropertyFileName();

        //Check for custom provisioning properties
        final File customPropFile = new File(propertyFileName.replace(".properties", "-custom.properties"));
        if (customPropFile.exists() && customPropFile.canRead()) {
            return customPropFile;
        }

        final File propFile = new File(propertyFileName);
        if (!propFile.exists()) {
            throw ConfigurationExceptionCodes.FILE_NOT_FOUND.create(propFile.getAbsoluteFile());
        }
        if (!propFile.canRead()) {
            throw ConfigurationExceptionCodes.NOT_READABLE.create(propFile.getAbsoluteFile());
        }
        return propFile;
    }

    private static String getPropertyFileName() throws OXException {
        final String fileName = TestConfig.getProperty(KEY);
        if (null == fileName) {
            throw ConfigurationExceptionCodes.PROPERTY_MISSING.create(KEY.getPropertyName());
        }
        return fileName;
    }

    private static Properties readPropFile(File propFile) throws OXException {
        Properties props = new Properties();
        try (FileInputStream fis = new FileInputStream(propFile)) {
            props.load(fis);
        } catch (FileNotFoundException e) {
            throw ConfigurationExceptionCodes.FILE_NOT_FOUND.create(e, propFile.getAbsolutePath());
        } catch (IOException e) {
            throw ConfigurationExceptionCodes.READ_ERROR.create(e, propFile.getAbsolutePath());
        }
        return props;
    }

    /**
     * Creates an {@link CloseableHttpClient}
     *
     * @return The closeableHttpClient
     * @throws OXException In case something went wrong
     */
    private static CloseableHttpClient createHttpClient() {
        SSLConnectionSocketFactory sslFactory = null;
        try {
            sslFactory = !AJAXConfig.getProperty(AJAXConfig.Property.PROTOCOL).equals("https") ? null : getSSLFactory();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        PoolingHttpClientConnectionManager connManager = null;
        try {
            connManager = new PoolingHttpClientConnectionManager();
            connManager.setMaxTotal(5);
            connManager.setDefaultMaxPerRoute(5);
            return HttpClients.custom().setSSLSocketFactory(sslFactory).setConnectionManager(connManager).build();
        } catch (Exception e) {
            if (null != connManager) {
                connManager.close();
            }
            LOG.error(e.getMessage(), e);
        }
        return HttpClients.custom().build();
    }

    private static SSLConnectionSocketFactory getSSLFactory() throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
        return new SSLConnectionSocketFactory(SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build(), NoopHostnameVerifier.INSTANCE);
    }

}
