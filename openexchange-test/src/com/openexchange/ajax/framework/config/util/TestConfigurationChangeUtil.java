/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.framework.config.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import org.junit.jupiter.api.Assertions;
import com.openexchange.ajax.framework.ClientCommons;
import com.openexchange.ajax.framework.ProvisioningSetup;
import com.openexchange.java.Strings;
import com.openexchange.test.Host;
import com.openexchange.test.common.configuration.AJAXConfig;
import com.openexchange.test.common.test.pool.TestUser;

/**
 *
 * {@link TestConfigurationChangeUtil}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @since v8.4
 */
public final class TestConfigurationChangeUtil {

    private TestConfigurationChangeUtil() {}

    /**
     * {@link Scopes} - The scopes that can be set for a configuration change
     *
     * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
     * @since v8.4
     */
    public enum Scopes {
        /** Changes will affect only one specific user */
        USER,
        /** Changes will affect the complete context */
        CONTEXT,
        /** The properties are set server-wide */
        SERVER,
        ;
    }

    /**
     * Changes the configuration of the given user
     *
     * @param user The user
     * @param config The new config
     * @throws ClientProtocolException In case the config can't be changes
     * @throws IOException In case the config can't be changes
     * @throws URISyntaxException In case the config can't be changes
     * @throws KeyStoreException In case the config can't be changes
     * @throws NoSuchAlgorithmException In case the config can't be changes
     * @throws KeyManagementException In case the config can't be changes
     */
    public static void changeConfigWithOwnClient(TestUser user, Map<String, String> config) throws ClientProtocolException, IOException, URISyntaxException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        changeConfigWithOwnClient(user, config, Scopes.USER, null);
    }

    /**
     * Changes the configuration of the given user
     *
     * @param user The user
     * @param config The new config
     * @param scope The scope the change effects
     * @throws ClientProtocolException In case the config can't be changes
     * @throws IOException In case the config can't be changes
     * @throws URISyntaxException In case the config can't be changes
     * @throws KeyStoreException In case the config can't be changes
     * @throws NoSuchAlgorithmException In case the config can't be changes
     * @throws KeyManagementException In case the config can't be changes
     */
    public static void changeConfigWithOwnClient(TestUser user, Map<String, String> config, String scope) throws ClientProtocolException, IOException, URISyntaxException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        changeConfigWithOwnClient(user, config, getScope(scope), null);
    }

    /**
     * Changes the configuration of the given user
     *
     * @param user The user
     * @param config The new config
     * @param scope The scope the change effects
     * @param reloadables The properties to reload, separated by a comma
     * @throws ClientProtocolException In case the config can't be changes
     * @throws IOException In case the config can't be changes
     * @throws URISyntaxException In case the config can't be changes
     * @throws KeyStoreException In case the config can't be changes
     * @throws NoSuchAlgorithmException In case the config can't be changes
     * @throws KeyManagementException In case the config can't be changes
     */
    public static void changeConfigWithOwnClient(TestUser user, Map<String, String> config, String scope, String reloadables) throws ClientProtocolException, IOException, URISyntaxException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        changeConfigWithOwnClient(user, config, getScope(scope), null == reloadables ? Collections.emptyList() : Arrays.asList(reloadables.split(",")));
    }

    private static Scopes getScope(String scope) {
        if (Strings.isNotEmpty(scope)) {
            for (Scopes s : Scopes.values()) {
                if (s.name().equalsIgnoreCase(scope)) {
                    return s;
                }
            }
        }
        return Scopes.USER;
    }

    /**
     * Changes the configuration of the given user
     *
     * @param user The user
     * @param config The new config
     * @param scope The scope the change effects
     * @throws ClientProtocolException In case the config can't be changes
     * @throws IOException In case the config can't be changes
     * @throws URISyntaxException In case the config can't be changes
     * @throws KeyStoreException In case the config can't be changes
     * @throws NoSuchAlgorithmException In case the config can't be changes
     * @throws KeyManagementException In case the config can't be changes
     */
    public static void changeConfigWithOwnClient(TestUser user, Map<String, String> config, Scopes scope) throws ClientProtocolException, IOException, URISyntaxException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        changeConfigWithOwnClient(user, config, scope, null);
    }

    /**
     * Changes the configuration of the given user
     *
     * @param user The user
     * @param config The new config
     * @param scope The scope the change effects
     * @param reloadables The properties to reload, separated by a comma
     * @throws ClientProtocolException In case the config can't be changes
     * @throws IOException In case the config can't be changes
     * @throws URISyntaxException In case the config can't be changes
     * @throws KeyStoreException In case the config can't be changes
     * @throws NoSuchAlgorithmException In case the config can't be changes
     * @throws KeyManagementException In case the config can't be changes
     */
    public static void changeConfigWithOwnClient(TestUser user, Map<String, String> config, Scopes scope, List<String> reloadables) throws ClientProtocolException, IOException, URISyntaxException {
        changeConfigWithOwnClient(Host.SERVER, user, config, scope, reloadables);
    }

    /**
     * Changes the configuration of the given user
     *
     * @param host The host to change the configuration on
     * @param user The user
     * @param config The new config
     * @param scope The scope the change effects
     * @param reloadables The properties to reload, separated by a comma
     * @throws ClientProtocolException In case the config can't be changes
     * @throws IOException In case the config can't be changes
     * @throws URISyntaxException In case the config can't be changes
     * @throws KeyStoreException In case the config can't be changes
     * @throws NoSuchAlgorithmException In case the config can't be changes
     * @throws KeyManagementException In case the config can't be changes
     */
    public static void changeConfigWithOwnClient(Host host, TestUser user, Map<String, String> config, Scopes scope, List<String> reloadables) throws ClientProtocolException, IOException, URISyntaxException {
        checkConfig(user, config);
        /*
         * Prepare call
         */
        String scheme = AJAXConfig.getProperty(AJAXConfig.Property.PROTOCOL);
        boolean useSSL = scheme.equals("https");
        URI uri = new URIBuilder().setScheme(scheme)
                                  .setHost(host.getHostname())
                                  .setPath(Host.getBasePath() + "/changeConfigForTest")
                                  .setPort(useSSL ? 443 : Integer.parseInt(AJAXConfig.getProperty(AJAXConfig.Property.SERVER_PORT)))
                                  .addParameter("userId", String.valueOf(user.getUserId()))
                                  .addParameter("contextId", String.valueOf(user.getContextId()))
                                  .addParameter("scope", null == scope ? Scopes.USER.name().toLowerCase() : scope.name().toLowerCase())
                                  .addParameter("reload", null == reloadables ? "" : reloadables.stream().collect(Collectors.joining(",")))
                                  .build();
        /*
         * Set test changing config
         */
        HttpPut httpPut = new HttpPut(uri);
        httpPut.setHeader(new BasicHeader(ClientCommons.X_OX_HTTP_TEST_HEADER_NAME, user.getCreatedBy()));
        StringEntity entity = new StringEntity(new JSONObject(config).toString(), ContentType.APPLICATION_JSON);
        httpPut.setEntity(entity);

        /*
         * Execute and check result, use global client with call specific context
         */
        try (CloseableHttpResponse response = ProvisioningSetup.getGlobalClient().execute(httpPut, new BasicHttpContext())) {
            assertEquals(200, response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
            parseBody(response.getEntity(), config);
        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    private static void parseBody(HttpEntity response, Map<String, String> config) throws JSONException, IOException {
        assertNotNull(response);
        String body = EntityUtils.toString(response);
        JSONObject jsonObject = JSONServices.parseObject(body);
        JSONObject data = jsonObject.getJSONObject("data");
        JSONObject updatedValues = data.getJSONObject("updatedValues");
        //JSONObject oldValues = data.getJSONObject("oldValues");
        //JSONArray reloaded = data.getJSONArray("reloaded");

        for (Entry<String, String> entry : config.entrySet()) {
            String value = updatedValues.getString(entry.getKey());
            assertNotNull(value);
            Assertions.assertEquals(value, entry.getValue());
        }

    }

    private static void checkConfig(TestUser user, Map<String, String> config) {
        assertTrue(user.getContextId() > 0);
        assertTrue(user.getUserId() > 0);
        assertThat("No configuration to change", config, is(not(nullValue())));
        assertFalse(config.isEmpty(), "No configuration to change");
    }

}
