/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.framework;

import java.util.Optional;
import com.openexchange.test.Host;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.test.common.tools.LoginTools;
import com.openexchange.testing.httpclient.invoker.ApiClient;
import com.openexchange.testing.httpclient.invoker.ApiException;

/**
 * 
 * {@link SerializationAware}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public interface SerializationAware {

    /**
     * Registers an {@link Serialiser} that intercepts the
     * {@link ApiClient#serialize(Object, String)} call. This enabled
     * the caller to overwrite the serialization of specific object(s).
     * This becomes in handy when explicitly setting <code>null</code> values
     * in a JSON request.
     * <p>
     * Example code:
     * //@formatter:off
     * <pre>
     * public Optional<String> serialize(Object obj, String contentType) throws ApiException {
     *      if (obj instanceof YourObject yo) {
     *          rerturn Optional.of(String.format("""
     *              {
     *              "yourJson": "%s",
     *              "someField": null
     *              }
     *              """, yo.getValue()));
     *          }
     *      return Optional.empty();
     * }
     * </pre>
     * //@formatter:on
     *
     * @param serialiser The serializer to register
     */
    void registerSerialiser(Serialiser serialiser);

    /**
     * {@link Serialiser}
     *
     * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
     */
    interface Serialiser {

        Optional<String> serialize(Object obj, String contentType) throws ApiException;
    }

    /**
     * Get or creates a {@link SerializationAware} API client
     *
     * @param user The user that uses the client
     * @param serialiser the serialiser to register on the client
     * @return A {@link SessionAwareClient} to use
     * @throws ApiException
     */
    default ApiClient createOrGetSerialiserAwareApiClient(TestUser user, Serialiser serialiser) throws ApiException {
        ApiClient client = user.getApiClient();
        if (client instanceof SerializationAware sa) {
            sa.registerSerialiser(serialiser);
            return client;
        }
        Optional<String> userAgent = client instanceof SessionAware sess ? Optional.ofNullable(sess.getUserAgent()) : Optional.empty();
        SessionAwareClient saClient = new SessionAwareClient(user, Optional.empty(), userAgent);
        saClient.registerSerialiser(serialiser);
        LoginTools.configureApiClient(saClient, Host.SERVER, userAgent);
        LoginTools.performLogin(saClient, userAgent);
        return saClient;
    }

}
