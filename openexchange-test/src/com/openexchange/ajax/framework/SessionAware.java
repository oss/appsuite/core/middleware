/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
* 
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.framework;

import com.openexchange.test.common.test.pool.TestUser;

/**
 * 
 * {@link SessionAware}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public interface SessionAware {

    /**
     * Gets the session id
     *
     * @return The session
     */
    String getSession();

    /**
     * Sets the session
     *
     * @param session The session to set
     */
    void setSession(String session);

    /**
     * Gets the user
     *
     * @return The user
     */
    TestUser getUser();

    /**
     * Get the user identifier
     *
     * @return The user identifier
     */
    Integer getUserId();

    /**
     * setUserId
     *
     * @param userId
     */
    void setUserId(Integer userId);

    /**
     * Sets the client's user agent
     *
     * @param userAgent The client's user agent
     */
    void setSessionUserAgent(String userAgent);

    /**
     * Gets the client identifier
     *
     * @return The client identifier
     */
    String getUserAgent();

    /**
     * Checks whether the client has the given user agent set
     *
     * @param userAgent The user agent to check
     * @return <code>True</code> if the given user agent matches the user agent of this {@link SessionAwareClient} instance
     */
    boolean hasUserAgent(String userAgent);

}
