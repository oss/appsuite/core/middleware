/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.framework;

import static com.openexchange.tools.arrays.Collections.isNotEmpty;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import com.openexchange.test.common.configuration.AJAXConfig;
import com.openexchange.test.common.test.TestUserConfig;
import com.openexchange.test.common.test.TestUserConfig.ClientTimeouts;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.invoker.ApiClient;
import com.openexchange.testing.httpclient.invoker.ApiException;
import okhttp3.CookieJar;
import okhttp3.JavaNetCookieJar;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * {@link SessionAwareClient}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v7.10.5
 */
public class SessionAwareClient extends ApiClient implements SessionAware, SerializationAware {

    /**
     * The {@link OkHttpClient} instance
     */
    protected static final OkHttpClient OKHTTPCLIENT = new OkHttpClient();

    private List<Serialiser> serialisers;

    private final TestUser user;
    private String session;
    private String userAgent;
    private Integer userId;

    /**
     * Initializes a new {@link SessionAwareClient}.
     *
     * @param user The {@link TestUser}
     */
    public SessionAwareClient(TestUser user) {
        this(user, Optional.empty(), Optional.empty());
    }

    /**
     * Initializes a new {@link SessionAwareClient}.
     *
     * @param user The {@link TestUser}
     * @param optConfig The optional user configuration
     * @param userAgent The client's userAgent to use
     */
    public SessionAwareClient(TestUser user, Optional<TestUserConfig> optConfig, Optional<String> userAgent) {
        this(user, optConfig, Optional.empty(), Optional.empty());
    }

    /**
     * Initializes a new {@link SessionAwareClient}.
     *
     * @param user The {@link TestUser}
     * @param optConfig The optional user configuration
     * @param userAgent The client's userAgent to use
     * @param optClientModifier An optional consumer allowing to adjust the HTTP client construction
     */
    public SessionAwareClient(TestUser user, Optional<TestUserConfig> optConfig, Optional<String> userAgent, Optional<Consumer<okhttp3.OkHttpClient.Builder>> optClientModifier) {
        super(prepareHttpClient(user, optConfig, optClientModifier));
        this.user = user;
        if (userAgent.isPresent()) {
            this.userAgent = userAgent.get();
            setUserAgent(userAgent.get());
        }
    }

    private static OkHttpClient prepareHttpClient(TestUser user, Optional<TestUserConfig> optConfig, Optional<Consumer<okhttp3.OkHttpClient.Builder>> optClientModifier) {
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieJar jar = new JavaNetCookieJar(cookieManager);

        long connectTimeout = Long.parseLong(AJAXConfig.getProperty(AJAXConfig.Property.CONNECTION_TIMOUT));
        long readTimeout = Long.parseLong(AJAXConfig.getProperty(AJAXConfig.Property.READ_TIMOUT));
        if (optConfig.isPresent() && optConfig.get().optTimeouts().isPresent()) {
            ClientTimeouts timeouts = optConfig.get().optTimeouts().get();

            if (timeouts.optConTimeout().isPresent()) {
                connectTimeout = timeouts.optConTimeout().get().getSeconds();
            }
            if (timeouts.optReadTimeout().isPresent()) {
                readTimeout = timeouts.optReadTimeout().get().getSeconds();
            }
        }
        Builder builder = OKHTTPCLIENT.newBuilder()
                                      .cookieJar(jar)
                                      .addInterceptor(chain -> {
                                          Request request = chain.request();
                                          Request newRequest = request.newBuilder()
                                                                      .addHeader(ClientCommons.X_OX_HTTP_TEST_HEADER_NAME, user.getCreatedBy())
                                                                      .build();
                                          return chain.proceed(newRequest);
                                      })
                                      .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
                                      .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
                                      .hostnameVerifier((hostname, session1) -> true);
        if (optClientModifier.isPresent()) {
            optClientModifier.get().accept(builder);
        }
        return builder.build();
    }

    /*
     * ============================== SessionAware ==============================
     */

    @Override
    public String getSession() {
        return session;
    }

    @Override
    public void setSession(String session) {
        this.session = session;
        this.setApiKey(session);
    }

    @Override
    public TestUser getUser() {
        return user;
    }

    @Override
    public Integer getUserId() {
        return userId;
    }

    @Override
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public void setSessionUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    @Override
    public String getUserAgent() {
        return this.userAgent;
    }

    @Override
    public boolean hasUserAgent(String userAgent) {
        return ((userAgent == null && this.userAgent == null) || (this.userAgent != null && this.userAgent.equals(userAgent)));
    }

    /*
     * ============================== SerializationAware ==============================
     */

    @Override
    public void registerSerialiser(Serialiser serialiser) {
        if (null == serialisers) {
            serialisers = new ArrayList<>(1);
        }
        serialisers.add(serialiser);
    }

    @Override
    public RequestBody serialize(Object obj, String contentType) throws ApiException {
        if (isNotEmpty(serialisers)) {
            for (Serialiser s : serialisers) {
                Optional<String> result = s.serialize(obj, contentType);
                if (result.isPresent()) {
                    return RequestBody.create(result.get(), MediaType.parse(contentType));
                }
            }
        }
        return super.serialize(obj, contentType);
    }
}
