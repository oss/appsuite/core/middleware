/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.apiclient.oauth;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.framework.AbstractConfigAwareAPIClientSession;
import com.openexchange.ajax.oauth.OAuthAccessTokenHelper;
import com.openexchange.exception.OXException;
import com.openexchange.test.Host;
import com.openexchange.test.common.configuration.AJAXConfig;
import com.openexchange.testing.httpclient.invoker.ApiClient;
import com.openexchange.testing.httpclient.invoker.ApiException;

/**
 * {@link AbstractOAuthAPIClient} is a subclass of the {@link AbstractConfigAwareAPIClientSession} which provides an oauth based api client
 *
 * This requires the following ajax properties to be configured: {@link AJAXConfig.Property#OAUTH_TOKEN_ENDPOINT}, {@link AJAXConfig.Property#OAUTH_CLIENT_ID} and {@link AJAXConfig.Property#OAUTH_CLIENT_PASSWORD}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v7.10.5
 */
public abstract class AbstractOAuthAPIClient extends AbstractConfigAwareAPIClientSession {

    protected OAuthApiClient oauthclient;
    private volatile String accessToken;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        oauthclient = generateOAuthClient();
    }

    /**
     * Generates an oauth client, which uses an provided oauth token to authenticate instead of login/password
     * Requires the "Resource Owner Password Credentials Grant" oauth flow to be enabled at the authorization server.
     *
     * @return The {@link OAuthApiClient}
     * @throws OXException in case something went wrong
     */
    protected final OAuthApiClient generateOAuthClient() throws OXException {
        OAuthApiClient newClient;
        try {
            newClient = new OAuthApiClient(() -> "Bearer " + getAccessToken());
            newClient.setBasePath(Host.SERVER.getPath());
            newClient.setUserAgent("HTTP API Testing Agent");
        } catch (Exception e) {
            throw new OXException(e);
        }
        return newClient;
    }

    /**
     * Gets an access token from the configured token endpoint.
     * Requires the "Resource Owner Password Credentials Grant" oauth flow to be enabled at the authorization server.
     *
     * @return The access token.
     * @throws ApiException if reading configuration fails or unable to get access token.
     */
    protected synchronized String getAccessToken() throws ApiException {
        if (accessToken == null) {
            accessToken = OAuthAccessTokenHelper.getAccessToken(testUser, getScopes());
        }
        return accessToken;
    }

    /**
     * Gets the oauth based client.
     *
     * @return The {@link ApiClient}
     */
    protected ApiClient getOAuthBasedClient() {
        return oauthclient;
    }

    /**
     * Gets the oauth scopes requested by the client.
     *
     * @return The oauth scopes.
     */
    protected abstract List<String> getScopes();
}
