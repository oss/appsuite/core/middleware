/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.apiclient.oauth;

import java.util.List;
import java.util.Map;
import com.openexchange.ajax.oauth.OAuthAccessTokenHelper;
import com.openexchange.exception.OXException;
import com.openexchange.test.common.configuration.AJAXConfig;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.invoker.ApiCallback;
import com.openexchange.testing.httpclient.invoker.ApiClient;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.invoker.Pair;
import okhttp3.Call;

/**
 * {@link OAuthApiClient}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v7.10.5
 */
public class OAuthApiClient extends ApiClient {

    private final ErrorAwareSupplier<String> tokenSupplier;

    /**
     * Initializes a new {@link OAuthApiClient}.
     *
     * @param tokenSupplier The error aware supplier with a String that contains the OAuth access token.
     */
    public OAuthApiClient(ErrorAwareSupplier<String> tokenSupplier) {
        super();
        this.tokenSupplier = tokenSupplier;
    }

    /**
     * Initializes a new {@link OAuthApiClient}.
     *
     * @param accessToken The OAuth access token.
     * @throws OXException if reading configuration fails.
     */
    public OAuthApiClient(String accessToken) throws OXException {
        this(() -> "Bearer " + accessToken);
        this.setBasePath(initBasePath());
        this.setUserAgent("HTTP API Testing Agent");
    }

    /**
     * Initializes a new {@link OAuthApiClient}.
     *
     * @param testUser The test user.
     * @param scopes The OAuth scopes.
     * @throws OXException if reading configuration fails.
     * @throws ApiException if reading configuration fails or unable to get access token.
     */
    public OAuthApiClient(TestUser testUser, List<String> scopes) throws OXException, ApiException {
        this(OAuthAccessTokenHelper.getAccessToken(testUser, scopes));
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Call buildCall(String basePath, String path, String method, List<Pair> queryParams, List<Pair> collectionQueryParams, Object body, Map<String, String> headerParams, Map<String, String> cookieParams, Map<String, Object> formParams, String[] authNames, ApiCallback callback) throws ApiException {
        try {
            headerParams.put("Authorization", tokenSupplier.get());
        } catch (Exception e) {
            throw new ApiException(e);
        }
        return super.buildCall(basePath, path, method, queryParams, collectionQueryParams, body, headerParams, cookieParams, formParams, authNames, callback);
    }

    static interface ErrorAwareSupplier<T> {

        T get() throws Exception;

    }

    /**
     * Gets the base path of the server and ensures that the ajax config is initialized before requesting the properties.
     *
     * @return The base path.
     * @throws OXException if reading configuration fails.
     *
     */
    private static String initBasePath() throws OXException {
        AJAXConfig.init();
        String protocol = AJAXConfig.getProperty(AJAXConfig.Property.PROTOCOL);
        String host = AJAXConfig.getProperty(AJAXConfig.Property.SERVER_HOSTNAME);
        String basePath = AJAXConfig.getProperty(AJAXConfig.Property.BASE_PATH);
        StringBuilder sb = new StringBuilder();
        sb.append(protocol).append("://");
        sb.append(host).append(basePath);
        // deletes the last "/" in the path, because the paths of the <modules> API start with a slash
        if (sb.charAt(sb.length() - 1) == '/') {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

}
