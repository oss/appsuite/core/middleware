/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.tokenloginV2;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.test.common.tools.LoginTools.HTTP_API_TESTING_AGENT;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.Boolean.valueOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import com.google.gson.reflect.TypeToken;
import com.openexchange.ajax.framework.AbstractConfigAwareAPIClientSession;
import com.openexchange.ajax.framework.config.util.TestConfigurationChangeUtil.Scopes;
import com.openexchange.testing.httpclient.invoker.ApiResponse;
import com.openexchange.testing.httpclient.models.AcquireTokenResponse;
import com.openexchange.testing.httpclient.models.AcquireTokenResponseData;
import com.openexchange.testing.httpclient.models.LoginResponse;
import com.openexchange.testing.httpclient.modules.LoginApi;
import com.openexchange.testing.httpclient.modules.TokenApi;
import okhttp3.Call;
import okhttp3.Cookie;
import okhttp3.HttpUrl;

/**
 * 
 * {@link PWATokenLoginTest}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class PWATokenLoginTest extends AbstractConfigAwareAPIClientSession {

    private TokenApi tokenApi;
    private LoginApi loginApi;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        this.tokenApi = new TokenApi(testUser.getApiClient());
        this.loginApi = new LoginApi(testUser.getApiClient());
    }

    @Override
    protected String getScope() {
        return Scopes.CONTEXT.toString();
    }

    private static Stream<Arguments> provideArguments() {
        return Stream.of(
                         Arguments.of(TRUE, TRUE),
                         Arguments.of(FALSE, TRUE),
                         Arguments.of(TRUE, FALSE),
                         Arguments.of(FALSE, FALSE));
    }

    @ParameterizedTest
    @MethodSource("provideArguments")
    public void testRedeemToken(boolean accessPassword, boolean staySignedIn) throws Exception {
        enableAllProperties(accessPassword, -1);

        String token = checkToken(tokenApi.acquireTokenBuilder().execute());

        String clientToken = UUID.randomUUID().toString();
        Call redeemTokenCall = loginApi.redeemTokenCall(clientToken, token, HTTP_API_TESTING_AGENT, "PWA", Boolean.valueOf(staySignedIn), null);
        String urlForCookies = redeemTokenCall.request().url().toString();
        Type localVarReturnType = new TypeToken<LoginResponse>() {}.getType();
        ApiResponse<LoginResponse> execute = loginApi.getApiClient().execute(redeemTokenCall, localVarReturnType);
        LoginResponse loginResponse = execute.getData();
        checkResponse(loginResponse.getError(), loginResponse.getErrorDesc());
        assertNotEquals(testUser.getApiClient().getSession(), loginResponse.getSession());
        if (accessPassword) {
            assertNotNull(loginResponse.getAdditionalProperties());
            assertTrue(loginResponse.getAdditionalProperties().keySet().contains("password"));
        }
        if (staySignedIn) {
            List<Cookie> cookies = testUser.getApiClient().getHttpClient().cookieJar().loadForRequest(HttpUrl.get(urlForCookies));
            assertTrue(cookies.stream().filter(c -> c.name().contains("open-xchange-session-") && c.value().equals(loginResponse.getSession())).findAny().isPresent());
        }
    }

    @Test
    public void testRedeemTokenTimeout() throws Exception {
        enableAllProperties(false, -1);
        /*
         * Set and let the token expire with 100 milliseconds
         */
        int expiry = 100;
        String token = checkToken(tokenApi.acquireTokenBuilder().withExpiry(I(expiry)).execute());
        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(expiry));

        String clientToken = UUID.randomUUID().toString();
        LoginResponse loginResponse = loginApi.redeemToken(clientToken, token, HTTP_API_TESTING_AGENT, "PWA", Boolean.FALSE);
        assertNotNull(loginResponse.getError(), loginResponse.getErrorDesc());

    }

    /*
     * ============================== HELPERS ==============================
     */

    private void enableAllProperties(boolean accessPassword, int expiry) throws Exception {
        changeConfigWithOwnClient(testUser, Map.of("com.openexchange.tokenlogin.maxIdleTime", expiry > 0 ? Integer.valueOf(expiry).toString() : "300000",
                                                   "com.openexchange.tokenlogin.applications", "PWA",
                                                   "com.openexchange.tokenlogin.PWA.accessPassword", valueOf(accessPassword).toString(),
                                                   "com.openexchange.tokenlogin.PWA.copyParameters", TRUE.toString(),
                                                   "com.openexchange.tokenlogin.PWA.announceId", TRUE.toString()));
    }

    private String checkToken(AcquireTokenResponse response) {
        AcquireTokenResponseData acquireTokenResponseData = checkResponse(response.getError(), response.getErrorDesc(), response.getData());
        String token = acquireTokenResponseData.getToken();
        assertNotNull(token);
        assertEquals(2, token.split("-").length);
        return token;
    }

}
