/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.ajax.multifactor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import com.google.gson.JsonObject;
import com.openexchange.testing.httpclient.models.CommonResponse;
import com.openexchange.testing.httpclient.models.MultifactorDevice;
import com.openexchange.testing.httpclient.models.MultifactorFinishAuthenticationData;
import com.openexchange.testing.httpclient.models.MultifactorFinishAuthenticationDataResponse;
import com.openexchange.testing.httpclient.models.MultifactorFinishRegistrationData;
import com.openexchange.testing.httpclient.models.MultifactorStartAuthenticationResponseData;
import com.openexchange.testing.httpclient.models.MultifactorStartAuthenticationResponseDataChallenge;
import com.openexchange.testing.httpclient.models.MultifactorStartAuthenticationResponseDataChallengeCredentialsGetJson;
import com.openexchange.testing.httpclient.models.MultifactorStartAuthenticationResponseDataChallengeCredentialsGetJsonPublicKeyAllowCredentialsInner;
import com.openexchange.testing.httpclient.models.MultifactorStartRegistrationResponseData;
import com.openexchange.testing.httpclient.models.MultifactorStartRegistrationResponseDataChallenge;
import com.openexchange.testing.httpclient.models.MultifactorStartRegistrationResponseDataChallengeData;
import com.yubico.webauthn.data.AuthenticatorAssertionResponse;
import com.yubico.webauthn.data.AuthenticatorAttestationResponse;
import com.yubico.webauthn.data.ByteArray;
import com.yubico.webauthn.data.ClientAssertionExtensionOutputs;
import com.yubico.webauthn.data.ClientRegistrationExtensionOutputs;
import com.yubico.webauthn.data.PublicKeyCredential;
import com.yubico.webauthn.data.PublicKeyCredentialCreationOptions;
import com.yubico.webauthn.data.PublicKeyCredentialDescriptor;
import com.yubico.webauthn.data.PublicKeyCredentialRequestOptions;
import com.yubico.webauthn.data.exception.Base64UrlException;
import de.adesso.softauthn.Authenticators;
import de.adesso.softauthn.CredentialsContainer;
import de.adesso.softauthn.Origin;
import de.adesso.softauthn.authenticator.WebAuthnAuthenticator;

/**
 * 
 */
public class WebAuthnTests extends AbstractMultifactorProviderTest {

    private static final String PROVIDER_NAME = "WEB_AUTHN";
    private WebAuthnAuthenticator authenticator;
    private CredentialsContainer credentials;

    private void createAuthenticator() {
        authenticator = Authenticators.yubikey5Nfc().build();
    }

    @Override
    protected Map<String, String> getNeededConfigurations() {
        HashMap<String, String> result = new HashMap<>();
        result.put("com.openexchange.webauthn.enabled", Boolean.TRUE.toString());
        result.put("com.openexchange.webauthn.relyingParty", "localhost");
        result.put("com.openexchange.webauthn.userVerificationRequired", "required");
        return result;
    }

    @Override
    protected String getProviderName() {
        return PROVIDER_NAME;
    }

    @Override
    protected boolean isBackupProvider() {
        return false;
    }

    @Override
    protected boolean isBackupOnlyProvider() {
        return false;
    }

    @Override
    protected MultifactorStartRegistrationResponseData doStartRegistration() throws Exception {
        return startRegistration(PROVIDER_NAME);
    }

    @Override
    protected void validateStartRegistrationResponse(MultifactorStartRegistrationResponseData startRegistrationData) throws Exception {
        MultifactorStartRegistrationResponseDataChallenge challenge = startRegistrationData.getChallenge();
        assertThat(challenge.getData(), is(not(nullValue())));

        MultifactorStartRegistrationResponseDataChallengeData challengeData = challenge.getData();
        assertThat(challengeData.getChallenge(), is(not(nullValue())));
        assertThat(challengeData.getRp().getId(), is(not(nullValue())));


    }

    @Override
    protected void validateStartAuthenticationResponse(MultifactorStartAuthenticationResponseData startAuthenticationData) {

        MultifactorStartAuthenticationResponseDataChallenge challenge = startAuthenticationData.getChallenge();

        MultifactorStartAuthenticationResponseDataChallengeCredentialsGetJson credJson = challenge.getCredentialsGetJson();

        assertThat(credJson.getPublicKey(), is(not(nullValue())));
        assertThat(credJson.getPublicKey().getChallenge(), is(not(nullValue())));
        assertThat(credJson.getPublicKey().getRpId(), is(not(nullValue())));
    }

    @Override
    protected MultifactorDevice doFinishRegistration(MultifactorStartRegistrationResponseData startRegistrationData) throws Exception {
        createAuthenticator();
        MultifactorStartRegistrationResponseDataChallenge challenge = startRegistrationData.getChallenge();
        MultifactorStartRegistrationResponseDataChallengeData challengeData = challenge.getData();
        var origin = new Origin("https", "localhost", -1, null);
        credentials = new CredentialsContainer(origin, List.of(authenticator));
        PublicKeyCredentialCreationOptions opts = PublicKeyCredentialCreationOptions.fromJson(challengeData.toJson());
        PublicKeyCredential<AuthenticatorAttestationResponse, ClientRegistrationExtensionOutputs> pkc = credentials.create(opts);

        MultifactorFinishRegistrationData data = new MultifactorFinishRegistrationData();
        data.setAttestationObject(pkc.getResponse().getAttestationObject().getBase64Url());
        data.setClientDataJSON(pkc.getResponse().getClientDataJSON().getBase64Url());
        data.setId(pkc.getId().getBase64Url());
        return finishRegistration(PROVIDER_NAME,
            startRegistrationData.getDeviceId(),
            data);
    }

    @Override
    protected void validateRegisteredDevice(MultifactorDevice device) throws Exception {
        assertThat(device.getEnabled(), is(Boolean.TRUE));
        assertThat(device.getProviderName(), is(PROVIDER_NAME));
    }

    /**
     * Generate a valid authentication
     *
     * @param startAuthenticationData
     * @return
     * @throws Base64UrlException
     */
    private MultifactorFinishAuthenticationData getAuth(MultifactorStartAuthenticationResponseData startAuthenticationData) throws Base64UrlException {
        MultifactorStartAuthenticationResponseDataChallenge challenge = startAuthenticationData.getChallenge();

        MultifactorStartAuthenticationResponseDataChallengeCredentialsGetJson credJson = challenge.getCredentialsGetJson();

        List<MultifactorStartAuthenticationResponseDataChallengeCredentialsGetJsonPublicKeyAllowCredentialsInner> allowed = credJson.getPublicKey().getAllowCredentials();
        ArrayList<PublicKeyCredentialDescriptor> allowedList = new ArrayList<PublicKeyCredentialDescriptor>();
        allowed.forEach(a -> {
            try {
                allowedList.add(PublicKeyCredentialDescriptor.builder().id(ByteArray.fromBase64Url(a.getId())).build());
            } catch (Base64UrlException e) {
                e.printStackTrace();
            }
        });

        PublicKeyCredentialRequestOptions opts = PublicKeyCredentialRequestOptions.builder()
            .challenge(ByteArray.fromBase64Url(credJson.getPublicKey().getChallenge()))
            .rpId(Optional.of((credJson.getPublicKey().getRpId())))
            .allowCredentials(allowedList)
            .build();

        PublicKeyCredential<AuthenticatorAssertionResponse, ClientAssertionExtensionOutputs> credential = credentials.get(opts);
        MultifactorFinishAuthenticationData data = new MultifactorFinishAuthenticationData();
        MultifactorFinishAuthenticationDataResponse resp = new MultifactorFinishAuthenticationDataResponse();
        resp.setAuthenticatorData(credential.getResponse().getAuthenticatorData().getBase64Url());
        resp.setClientDataJSON(credential.getResponse().getClientDataJSON().getBase64Url());
        resp.setSignature(credential.getResponse().getSignature().getBase64Url());
        data.setResponse(resp);
        data.setId(credential.getId().getBase64Url());
        data.setClientExtensionResults(new JsonObject());
        data.setType("public-key");
        return data;
    }

    @Override
    protected CommonResponse doAuthentication(MultifactorStartRegistrationResponseData startRegistrationData, MultifactorStartAuthenticationResponseData startAuthenticationData) throws Exception {
        MultifactorFinishAuthenticationData data = getAuth(startAuthenticationData);
        return MultifactorApi().multifactorDeviceActionfinishAuthentication(PROVIDER_NAME, startRegistrationData.getDeviceId(), data);
    }

    @Override
    protected CommonResponse doWrongAuthentication(MultifactorStartRegistrationResponseData startRegistrationData, MultifactorStartAuthenticationResponseData startAuthenticationData) throws Exception {
        MultifactorFinishAuthenticationData data = getAuth(startAuthenticationData);
        // Mess with the signature
        byte[] signature = data.getResponse().getSignature().getBytes();
        signature[10] = "a".getBytes()[0];
        data.getResponse().setSignature(new String(signature));
        return MultifactorApi().multifactorDeviceActionfinishAuthentication(PROVIDER_NAME, startRegistrationData.getDeviceId(), data);
    }

    @Test
    public void testsRegistrationSubsequentHasExclusionList() throws Exception {
        //Performing a complete registration of a new device
        MultifactorStartRegistrationResponseData registrationResponseData = registerNewDevice();
        // Check that the exclude list is empty
        assertThat(registrationResponseData.getChallenge().getData().getExcludeCredentials().isEmpty(), is(Boolean.TRUE));
        // Start to register new device
        MultifactorStartRegistrationResponseData startRegistrationData = doStartRegistration();
        // Confirm that exclude list is no longer empty
        assertThat(startRegistrationData.getChallenge().getData().getExcludeCredentials().isEmpty(), is(Boolean.FALSE));
        // Lets confirm that the exclude list will eclude the existing authenticator        
        MultifactorStartRegistrationResponseDataChallenge challenge = startRegistrationData.getChallenge();
        MultifactorStartRegistrationResponseDataChallengeData challengeData = challenge.getData();
        PublicKeyCredentialCreationOptions opts = PublicKeyCredentialCreationOptions.fromJson(challengeData.toJson());
        Boolean rejected = Boolean.FALSE;
        try {
            PublicKeyCredential<AuthenticatorAttestationResponse, ClientRegistrationExtensionOutputs> pkc = credentials.create(opts);
        } catch (Exception ex) {
            if (ex.getMessage().contains("this credential is excluded"))
                rejected = Boolean.TRUE;
        }
        assertThat(rejected, is(Boolean.TRUE));

    }
}
