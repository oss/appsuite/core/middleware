/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.chronos.itip;

import static com.openexchange.ajax.chronos.factory.AttendeeFactory.createIndividual;
import static com.openexchange.ajax.chronos.itip.ITipUtil.receiveNotification;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertContent;
import static com.openexchange.ajax.chronos.util.ChronosUtils.removeIfPresent;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import com.openexchange.ajax.chronos.UserApi;
import com.openexchange.ajax.chronos.factory.AttendeeFactory;
import com.openexchange.ajax.chronos.factory.EventFactory;
import com.openexchange.ajax.chronos.manager.EventManager;
import com.openexchange.ajax.chronos.util.DateTimeUtil;
import com.openexchange.ajax.folder.manager.FolderPermissionsBits;
import com.openexchange.chronos.Event;
import com.openexchange.chronos.ical.ImportedCalendar;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.EventForwardedResponse;
import com.openexchange.testing.httpclient.models.EventResponse;
import com.openexchange.testing.httpclient.models.FolderData;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.models.SchedulingMailResponseData.StatusEnum;
import com.openexchange.testing.httpclient.modules.ChronosApi;

/**
 * {@link ForwardEventTest} - Tests the forward HTTP action within the chronos module
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class ForwardEventTest extends AbstractForwardEventTest {

    @Test
    public void testEventForward_missingPermission() throws Exception {
        EventData eventData = EventFactory.createSingleTwoHourEvent(testUser.getUserId(), "Forward single event");
        eventData.setAttendees(AttendeeFactory.createAsExternals(testUserC2)); // Auto-add organizer
        EventData createdEvent = eventManager.createEvent(eventData);
        /*
         * Share folder with another user
         */
        FolderData data = folderManager.getFolder(defaultFolderId);
        folderManager.shareFolder(data, testUser3, FolderPermissionsBits.VIEW_FOLDER_ONLY);
        ChronosApi api = new ChronosApi(testUser3.getApiClient());
        /*
         * Forward non-accessible event, which shall fail
         */
        EventForwardedResponse response = forwardToUser(api, createdEvent, defaultFolderId, testUser2);
        assertThat(response.getError(), is(notNullValue()));
        /*
         * Share folder with enough permissions and forward again
         */
        data = folderManager.getFolder(defaultFolderId);
        folderManager.shareFolder(data, testUser3, FolderPermissionsBits.VIEWER);
        EventResponse eventResponse = api.getEventBuilder().withId(createdEvent.getId()).withFolder(defaultFolderId).execute();
        EventData secretaryEvent = checkResponse(eventResponse.getError(), eventResponse.getErrorDesc(), eventResponse.getData());
        response = forwardToUser(api, secretaryEvent, defaultFolderId, testUser2);
        checkResponse(response);
    }

    @ParameterizedTest
    @ValueSource(booleans =
    { true, false })
    public void testEventForward_failOnNonGroupScheduled(boolean crossContext) throws Exception {
        prepare(crossContext);
        EventData eventData = EventFactory.createSingleTwoHourEvent(testUser.getUserId(), "Forward single event");
        EventData createdEvent = eventManager.createEvent(eventData);
        /*
         * Forward non-group-scheduled event, which shall fail
         */
        EventForwardedResponse response = forwardToTargetUser(createdEvent);
        assertThat(response.getError(), is(notNullValue()));
    }

    @ParameterizedTest
    @ValueSource(booleans =
    { true, false })
    public void testEventForward_rejectOnAttendee(boolean crossContext) throws Exception {
        prepare(crossContext);
        EventData eventData = EventFactory.createSingleTwoHourEvent(testUser.getUserId(), "Forward single event");
        eventData.setAttendees(AttendeeFactory.createAsExternals(forwardTarget));
        EventData createdEvent = eventManager.createEvent(eventData);
        /*
         * Forward to recipient of event, which shall fail
         */
        EventForwardedResponse response = forwardToTargetUser(createdEvent);
        checkResponse(response, StatusEnum.REJECTED);
    }

    @ParameterizedTest
    @ValueSource(booleans =
    { true, false })
    public void testEventForward(boolean crossContext) throws Exception {
        prepare(crossContext);
        EventData eventData = EventFactory.createSingleTwoHourEvent(testUser.getUserId(), "Forward single event");
        eventData.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser3));
        EventData createdEvent = eventManager.createEvent(eventData);
        /*
         * Forward to recipient
         */
        EventForwardedResponse response = forwardToTargetUser(createdEvent);
        checkResponse(response);
        /*
         * Get mail and analyze static iCAL
         */
        IMipReceiver iMipReceiver = new IMipReceiver(forwardTarget.getApiClient());
        MailData iMIP = iMipReceiver.event(createdEvent).from(testUser.getLogin()).method(SchedulingMethod.REQUEST).receive();
        checkMailSubject(iMIP);
        ImportedCalendar calendar = ITipUtil.parseICalAttachment(forwardTarget.getApiClient(), iMIP, SchedulingMethod.REQUEST);
        Event event = assertCalendar(calendar);
        assertThat(event.getRecurrenceRule(), is(nullValue()));
        assertContent(iMIP, FORWARDED_INTRO, COMMENT);
    }

    @Test
    public void testEventForwardWithMultipleRecipients() throws Exception {
        EventData eventData = EventFactory.createSingleTwoHourEvent(testUser.getUserId(), "Forward single event");
        eventData.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser3));
        EventData createdEvent = eventManager.createEvent(eventData);
        /*
         * Forward to recipients
         */
        EventForwardedResponse response = chronosApi.forwardEventBuilder()
                                                    .withId(createdEvent.getId())
                                                    .withFolder(defaultFolderId)
                                                    .withRecurrenceId(createdEvent.getRecurrenceId())
                                                    .withEventForwardMailBody(createBody(testUser2, testUserC2))
                                                    .execute();
        checkResponse(response, StatusEnum.SENT, StatusEnum.SENT);

        for (TestUser user : List.of(testUser2, testUserC2)) {
            /*
             * Get mail and analyze static iCAL
             */
            IMipReceiver iMipReceiver = new IMipReceiver(user.getApiClient());
            MailData iMIP = iMipReceiver.event(createdEvent).from(testUser.getLogin()).method(SchedulingMethod.REQUEST).receive();
            checkMailSubject(iMIP);
            ImportedCalendar calendar = ITipUtil.parseICalAttachment(user.getApiClient(), iMIP, SchedulingMethod.REQUEST);
            Event event = assertCalendar(calendar);
            assertThat(event.getRecurrenceRule(), is(nullValue()));
            assertContent(iMIP, FORWARDED_INTRO, COMMENT);
        }
    }

    @ParameterizedTest
    @ValueSource(booleans =
    { true, false })
    public void testSeriesEventForward(boolean crossContext) throws Exception {
        prepare(crossContext);
        EventData eventData = EventFactory.createSeriesEvent(testUser.getUserId(), "Forward series event", 10, defaultFolderId);
        eventData.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser3));
        EventData createdEvent = eventManager.createEvent(eventData);
        /*
         * Forward to recipient
         */
        EventForwardedResponse response = forwardToTargetUser(createdEvent);
        checkResponse(response);
        /*
         * Get mail and analyze static iCAL
         */
        IMipReceiver iMipReceiver = new IMipReceiver(forwardTarget.getApiClient());
        MailData iMIP = iMipReceiver.event(createdEvent).from(testUser.getLogin()).method(SchedulingMethod.REQUEST).receive();
        checkMailSubject(iMIP);
        ImportedCalendar calendar = ITipUtil.parseICalAttachment(forwardTarget.getApiClient(), iMIP, SchedulingMethod.REQUEST);
        Event event = assertCalendar(calendar);
        assertThat(event.getRecurrenceRule(), is(notNullValue()));
        assertThat(event.getRecurrenceRule(), is(createdEvent.getRrule()));
        assertContent(iMIP, FORWARDED_INTRO, COMMENT);
    }

    @ParameterizedTest
    @ValueSource(booleans =
    { true, false })
    public void testSeriesEventWithExceptionsForward(boolean crossContext) throws Exception {
        prepare(crossContext);
        EventData eventData = EventFactory.createSeriesEvent(testUser.getUserId(), "Forward series event with excpetion", 10, defaultFolderId);
        eventData.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser3));
        EventData createdEvent = eventManager.createEvent(eventData);
        /*
         * Create a single change occurrence as organizer
         */
        List<EventData> allEvents = getAllEventsOfEvent(eventManager, createdEvent);
        String recurrenceId = allEvents.get(2).getRecurrenceId();
        EventData changeException = eventManager.createChangeException(createdEvent, null, recurrenceId, delta -> delta.setLocation("Olpe"));
        /*
         * Forward series with exception to recipient
         */
        EventForwardedResponse response = forwardToTargetUser(createdEvent);
        checkResponse(response);
        /*
         * Get mail and analyze static iCAL
         */
        IMipReceiver iMipReceiver = new IMipReceiver(forwardTarget.getApiClient());
        MailData iMIP = iMipReceiver.uid(createdEvent.getUid()).subject(createdEvent.getSummary()).from(testUser.getLogin()).method(SchedulingMethod.REQUEST).receive();
        checkMailSubject(iMIP);
        ImportedCalendar calendar = ITipUtil.parseICalAttachment(forwardTarget.getApiClient(), iMIP, SchedulingMethod.REQUEST);
        for (Event event : assertCalendar(calendar, 2)) {
            if (null != event.getRecurrenceId()) {
                // Change exception
                assertThat(event.getRecurrenceId().toString(), is(changeException.getRecurrenceId()));
            } else {
                assertThat(event.getRecurrenceRule(), is(notNullValue()));
                assertThat(event.getRecurrenceRule(), is(createdEvent.getRrule()));
            }
        }
        assertContent(iMIP, FORWARDED_INTRO, COMMENT);
    }

    @ParameterizedTest
    @ValueSource(booleans =
    { true, false })
    public void testSeriesOccurenceForward(boolean crossContext) throws Exception {
        prepare(crossContext);
        EventData eventData = EventFactory.createSeriesEvent(testUser.getUserId(), "Forward change exception", 10, defaultFolderId);
        eventData.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser3));
        EventData createdEvent = eventManager.createEvent(eventData);
        /*
         * Get a specific occurrence to forward
         */
        List<EventData> allEvents = getAllEventsOfEvent(eventManager, createdEvent);
        String recurrenceId = allEvents.get(2).getRecurrenceId();
        EventData occurrence = EventFactory.deltaOf(createdEvent);
        occurrence.setRecurrenceId(recurrenceId);
        /*
         * Forward the occurrence to recipient
         */
        EventForwardedResponse response = forwardToTargetUser(occurrence);
        checkResponse(response);
        /*
         * Get mail and analyze static iCAL
         */
        IMipReceiver iMipReceiver = new IMipReceiver(forwardTarget.getApiClient());
        MailData iMIP = iMipReceiver.uid(createdEvent.getUid()).subject(createdEvent.getSummary()).from(testUser.getLogin()).method(SchedulingMethod.REQUEST).receive();
        checkMailSubject(iMIP);
        ImportedCalendar calendar = ITipUtil.parseICalAttachment(forwardTarget.getApiClient(), iMIP, SchedulingMethod.REQUEST);
        Event event = assertCalendar(calendar);
        assertThat(event.getRecurrenceId(), is(notNullValue()));
        assertThat(event.getRecurrenceId().toString(), is(recurrenceId));
        assertContent(iMIP, FORWARDED_INSTANCE_INTRO, COMMENT);
    }

    @ParameterizedTest
    @ValueSource(booleans =
    { true, false })
    public void testSeriesOrphanedForward(boolean crossContext) throws Exception {
        prepare(crossContext);
        EventData eventData = EventFactory.createSeriesEvent(testUser.getUserId(), "Forward orphaned occurrence", 10, defaultFolderId);
        eventData.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser3));
        EventData createdEvent = eventManager.createEvent(eventData);
        /*
         * Create a single change occurrence as organizer by inviting user 4
         */
        List<EventData> allEvents = getAllEventsOfEvent(eventManager, createdEvent);
        String recurrenceId = allEvents.get(2).getRecurrenceId();
        TestUser testUser4 = testContext.acquireUser();
        EventData changeException = eventManager.createChangeException(createdEvent, null, recurrenceId, delta -> delta.addAttendeesItem(createIndividual(testUser4)));
        /*
         * Receive notification as user 4 and check that it is only one (orphaned) event
         */
        receiveNotification(testUser4.getApiClient(), testUser.getLogin(), changeException.getSummary());
        String folderId4 = getDefaultFolder(testUser4.getApiClient());
        UserApi userApi4 = new UserApi(testUser4.getApiClient(), testUser4);
        EventManager eventManager4 = new EventManager(userApi4, folderId4);

        List<EventData> eventsOfUser4 = getAllEventsOfEvent(eventManager4, createdEvent);
        assertNotNull(eventsOfUser4);
        assertEquals(1, eventsOfUser4.size());
        EventData forwardEvent = eventsOfUser4.get(0);
        assertEquals(createdEvent.getId(), forwardEvent.getSeriesId());
        assertEquals(changeException.getRecurrenceId(), forwardEvent.getRecurrenceId());
        /*
         * Forward orphaned event to recipient
         */
        EventForwardedResponse response = forwardToUser(userApi4.getChronosApi(), forwardEvent, folderId4, forwardTarget);
        checkResponse(response);
        /*
         * Get mail and analyze static iCAL
         * Note that a normal introduction should be used, instead for an occurrence
         */
        IMipReceiver iMipReceiver = new IMipReceiver(forwardTarget.getApiClient());
        MailData iMIP = iMipReceiver.event(forwardEvent).from(testUser4.getLogin()).method(SchedulingMethod.REQUEST).receive();
        checkMailSubject(iMIP);
        ImportedCalendar calendar = ITipUtil.parseICalAttachment(forwardTarget.getApiClient(), iMIP, SchedulingMethod.REQUEST);
        Event event = assertCalendar(calendar);
        assertThat(event.getRecurrenceRule(), is(nullValue()));
        assertContent(iMIP, FORWARDED_INTRO, COMMENT);
    }

    @ParameterizedTest
    @ValueSource(booleans =
    { true, false })
    public void testSeriesExceptionsForward(boolean crossContext) throws Exception {
        prepare(crossContext);
        EventData eventData = EventFactory.createSeriesEvent(testUser.getUserId(), "Forward change exception", 10, defaultFolderId);
        eventData.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser3));
        EventData createdEvent = eventManager.createEvent(eventData);
        /*
         * Create a single change occurrence as organizer
         */
        List<EventData> allEvents = getAllEventsOfEvent(eventManager, createdEvent);
        String recurrenceId = allEvents.get(2).getRecurrenceId();
        EventData changeException = eventManager.createChangeException(createdEvent, null, recurrenceId, delta -> delta.setLocation("Olpe"));
        /*
         * Forward series with exception to recipient
         */
        EventForwardedResponse response = forwardToTargetUser(changeException);
        checkResponse(response);
        /*
         * Get mail and analyze static iCAL
         */
        IMipReceiver iMipReceiver = new IMipReceiver(forwardTarget.getApiClient());
        MailData iMIP = iMipReceiver.event(changeException).from(testUser.getLogin()).method(SchedulingMethod.REQUEST).receive();
        checkMailSubject(iMIP);
        ImportedCalendar calendar = ITipUtil.parseICalAttachment(forwardTarget.getApiClient(), iMIP, SchedulingMethod.REQUEST);
        Event event = assertCalendar(calendar);
        assertThat(event.getRecurrenceId(), is(notNullValue()));
        assertThat(event.getRecurrenceId().toString(), is(changeException.getRecurrenceId()));
        assertContent(iMIP, FORWARDED_INSTANCE_INTRO, COMMENT);
    }

    @ParameterizedTest
    @ValueSource(booleans =
    { true, false })
    public void testSeriesEventWithDeleteExceptionsForward(boolean crossContext) throws Exception {
        prepare(crossContext);
        EventData eventData = EventFactory.createSeriesEvent(testUser.getUserId(), "Forward series event with delete excpetion from attendee view", 10, defaultFolderId);
        eventData.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser3, testContext.acquireUser())); // Series with user4
        EventData createdEvent = eventManager.createEvent(eventData);
        /*
         * Create a single change occurrence as organizer, remove user 3 from the occurrence
         */
        List<EventData> allEvents = getAllEventsOfEvent(eventManager, createdEvent);
        String recurrenceId = allEvents.get(2).getRecurrenceId();
        eventManager.createChangeException(createdEvent, null, recurrenceId,
                                           delta -> delta.attendees(removeIfPresent(createdEvent.getAttendees(), testUser3)));
        /*
         * Forward series with the "delete" exception to the target
         */
        ChronosApi api = new ChronosApi(testUser3.getApiClient());
        EventForwardedResponse response = forwardToUser(api, createdEvent.getId(), null, getDefaultFolder(testUser3.getApiClient()), forwardTarget);
        checkResponse(response);
        /*
         * Get mail and analyze static iCAL
         */
        IMipReceiver iMipReceiver = new IMipReceiver(forwardTarget.getApiClient());
        MailData iMIP = iMipReceiver.uid(createdEvent.getUid()).subject(createdEvent.getSummary()).from(testUser3.getLogin()).method(SchedulingMethod.REQUEST).receive();
        checkMailSubject(iMIP);
        ImportedCalendar calendar = ITipUtil.parseICalAttachment(forwardTarget.getApiClient(), iMIP, SchedulingMethod.REQUEST);
        Event event = assertCalendar(calendar);
        assertThat(event.getDeleteExceptionDates(), is(notNullValue()));
        assertThat(event.getDeleteExceptionDates().first().getValue(), is(DateTimeUtil.fromRecurrenceId(recurrenceId)));
        assertContent(iMIP, FORWARDED_INTRO, COMMENT);
    }

}
