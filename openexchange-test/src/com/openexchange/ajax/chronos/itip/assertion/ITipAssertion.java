/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.chronos.itip.assertion;

import static com.openexchange.chronos.common.CalendarUtils.getURI;
import static com.openexchange.java.Autoboxing.I;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import org.junit.jupiter.api.Assertions;
import com.openexchange.ajax.chronos.factory.PartStat;
import com.openexchange.ajax.chronos.util.ChronosUtils;
import com.openexchange.exception.Category;
import com.openexchange.testing.httpclient.models.ActionResponse;
import com.openexchange.testing.httpclient.models.Analysis;
import com.openexchange.testing.httpclient.models.AnalysisChange;
import com.openexchange.testing.httpclient.models.AnalyzeResponse;
import com.openexchange.testing.httpclient.models.Annotations;
import com.openexchange.testing.httpclient.models.Attendee;
import com.openexchange.testing.httpclient.models.CalendarResult;
import com.openexchange.testing.httpclient.models.CalendarUser;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.MailAttachment;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.models.UserData;

/**
 * {@link ITipAssertion}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @since v7.10.3
 */
public class ITipAssertion {

    /**
     * identifier of the internal calendar access provider
     */
    public static final String INTERNAL_CALENDAR = "Internal Calendar";

    private ITipAssertion() {}

    /**
     * Asserts that only one analyze with exact one change was provided in the response
     *
     * @param analyzeResponse The response to check
     * @return The {@link AnalysisChange} provided by the server
     */
    public static AnalysisChange assertSingleChange(AnalyzeResponse analyzeResponse) {
        assertTrue(null == analyzeResponse.getError() || Category.EnumType.WARNING.name().equals(analyzeResponse.getCategories()), "error during analysis: " + analyzeResponse.getError());
        assertEquals(1, analyzeResponse.getData().size(), "unexpected analysis number in response");
        Analysis analysis = analyzeResponse.getData().get(0);
        assertEquals(1, analysis.getChanges().size(), "unexpected number of changes in analysis. Changes: " + analysis.getChanges().toString());
        return analysis.getChanges().get(0);
    }

    /**
     * Asserts that only one analyze with exact one change was provided in the response
     *
     * @param analyzeResponse The response to check
     * @param size The expected size of events
     * @param index The index of events to return
     * @return The {@link AnalysisChange} provided by the server
     */
    public static AnalysisChange assertChanges(AnalyzeResponse analyzeResponse, int size, int index) {
        assertTrue(null == analyzeResponse.getError() || Category.EnumType.WARNING.name().equals(analyzeResponse.getCategories()), "error during analysis: " + analyzeResponse.getError());
        assertEquals(1, analyzeResponse.getData().size(), "unexpected analysis number in response");
        Analysis analysis = analyzeResponse.getData().get(0);
        assertEquals(size, analysis.getChanges().size(), "unexpected number of changes in analysis");
        return analysis.getChanges().get(index);
    }

    /**
     * Asserts that only one analyze with exact one change was provided in the response
     *
     * @param analyzeResponse The response to check
     * @return The {@link AnalysisChange} provided by the server
     */
    public static Annotations assertSingleAnnotations(AnalyzeResponse analyzeResponse) {
        assertTrue(null == analyzeResponse.getError() || Category.EnumType.WARNING.name().equals(analyzeResponse.getCategories()), "error during analysis: " + analyzeResponse.getError());
        assertEquals(1, analyzeResponse.getData().size(), "unexpected analysis number in response");
        Analysis analysis = analyzeResponse.getData().get(0);
        assertEquals(1, analysis.getAnnotations().size(), "unexpected number of annotations in analysis");
        return analysis.getAnnotations().get(0);
    }
    
    /**
     * Asserts that only one analyze with exact one change was provided in the response
     *
     * @param analyzeResponse The response to check
     * @param amount The expected amount of annotations, or <code>-1</code> if not of interest
     * @param content The content to match in the annotations
     */
    public static void assertAnnotations(AnalyzeResponse analyzeResponse, String... content) {
        assertAnnotations(analyzeResponse, -1, content);
    }

    /**
     * Asserts that only one analyze with exact one change was provided in the response
     *
     * @param analyzeResponse The response to check
     * @param amount The expected amount of annotations, or <code>-1</code> if not of interest
     * @param content The content to match in the annotations
     */
    public static void assertAnnotations(AnalyzeResponse analyzeResponse, int amount, String... content) {
        assertNotNull(content);
        assertTrue(null == analyzeResponse.getError() || Category.EnumType.WARNING.name().equals(analyzeResponse.getCategories()), "error during analysis: " + analyzeResponse.getError());
        assertNotNull(analyzeResponse.getData(), "unexpected analysis number in response");
        assertEquals(1, analyzeResponse.getData().size(), "unexpected analysis number in response");
        if (amount > -1) {
            assertEquals(amount, analyzeResponse.getData().get(0).getAnnotations().size(), "unexpected number of annotations in analysis");
        }
        Analysis analysis = analyzeResponse.getData().get(0);
        for (String match : content) {
            assertTrue(analysis.getAnnotations().stream().filter(a -> a.getMessage().contains(match)).findAny().isPresent(), "Unable to find content " + match);
        }
    }

    /**
     * Asserts that the given attendee represented by its mail has the desired participant status
     *
     * @param attendees The attendees of the event
     * @param email The attendee to check represented by its mail
     * @param partStat The participant status of the attendee
     * @return The attendee to check as {@link Attendee} object
     */
    public static Attendee assertAttendeePartStat(List<Attendee> attendees, String email, PartStat partStat) {
        Attendee attendee = ChronosUtils.find(attendees, email);
        partStat.assertStatus(attendee);
        return attendee;
    }

    /**
     * Asserts that the given attendee represented by its mail has the desired participant status
     *
     * @param attendees The attendees of the event
     * @param email The attendee to check represented by its mail
     * @param expectedPartStat The participant status of the attendee
     * @return The attendee to check as {@link Attendee} object
     */
    public static Attendee assertAttendeePartStat(List<Attendee> attendees, String email, String expectedPartStat) {
        Attendee attendee = ChronosUtils.find(attendees, email);
        assertNotNull(attendee);
        assertEquals(expectedPartStat, attendee.getPartStat());
        return attendee;
    }

    /**
     * Asserts that the given attendee represented by its entity ID has the desired participant status
     *
     * @param attendees The attendees of the event
     * @param entity The attendee to check represented by its entity ID
     * @param expectedPartStat The participant status of the attendee
     * @return The attendee to check as {@link Attendee} object
     */
    public static Attendee assertPartStat(List<Attendee> attendees, Integer entity, PartStat expectedPartStat) {
        Attendee matchingAttendee = ChronosUtils.find(attendees, entity);
        expectedPartStat.assertStatus(matchingAttendee);
        return matchingAttendee;
    }

    /**
     * Asserts that the given attendee represented by its mail has the desired participant status
     *
     * @param attendees The attendees of the event
     * @param email The attendee to check represented by its mail
     * @param expectedPartStat The participant status of the attendee as {@link com.openexchange.chronos.ParticipationStatus}
     * @return The attendee to check as {@link com.openexchange.chronos.Attendee} object
     */
    public static com.openexchange.chronos.Attendee assertAttendeePartStat(List<com.openexchange.chronos.Attendee> attendees, String email, com.openexchange.chronos.ParticipationStatus expectedPartStat) {
        com.openexchange.chronos.Attendee matchingAttendee = null;
        if (null != attendees) {
            for (com.openexchange.chronos.Attendee attendee : attendees) {
                String uri = attendee.getUri();
                if (null != uri && uri.toLowerCase().contains(email.toLowerCase())) {
                    matchingAttendee = attendee;
                    break;
                }
            }
        }
        assertNotNull(matchingAttendee);
        assertEquals(expectedPartStat, matchingAttendee.getPartStat());
        return matchingAttendee;
    }

    /**
     * Asserts that exactly one event was handled by the server
     *
     * @param actionResponse The {@link ActionResponse} from the server
     * @return The {@link EventData} of the handled event
     */
    public static EventData assertSingleEvent(ActionResponse actionResponse) {
        return assertSingleEvent(actionResponse, null);
    }

    /**
     * Asserts that exactly one event was handled by the server
     *
     * @param actionResponse The {@link ActionResponse} from the server
     * @param uid The uid the event should have or <code>null</code>
     * @return The {@link EventData} of the handled event
     */
    public static EventData assertSingleEvent(ActionResponse actionResponse, String uid) {
        return assertEvents(actionResponse, uid, 1).get(0);
    }

    /**
     * Asserts that the given count on events was handled by the server
     *
     * @param actionResponse The {@link ActionResponse} from the server
     * @param uid The uid the event should have or <code>null</code>
     * @param size The expected size of the returned events
     * @return The {@link EventData} of the handled event
     */
    public static List<EventData> assertEvents(ActionResponse actionResponse, String uid, int size) {
        assertNotNull(actionResponse.getData());
        assertThat("Only one object should have been handled", Integer.valueOf(actionResponse.getData().size()), is(I(size)));
        List<EventData> events = new LinkedList<>();
        for (EventData eventData : actionResponse.getData()) {
            if (null != uid) {
                assertEquals(uid, eventData.getUid());
            }
            events.add(eventData);
        }
        return events;
    }

    /**
     * Asserts that exactly one event was handled by the server
     *
     * @param actionResponse The {@link ActionResponse} from the server
     * @return The {@link EventData} of the handled event
     */
    public static EventData assertSingleEvent(CalendarResult actionResponse) {
        return assertSingleEvent(actionResponse, null);
    }

    /**
     * Asserts that exactly one event was handled by the server
     *
     * @param actionResponse The {@link ActionResponse} from the server
     * @param uid The uid the event should have or <code>null</code>
     * @return The {@link EventData} of the handled event
     */
    public static EventData assertSingleEvent(CalendarResult actionResponse, String uid) {
        return assertEvents(actionResponse, uid, 1).get(0);
    }

    /**
     * Asserts that the given count on events was handled by the server
     *
     * @param actionResponse The {@link ActionResponse} from the server
     * @param uid The uid the event should have or <code>null</code>
     * @param size The expected size of the returned events
     * @return The {@link EventData} of the handled event
     */
    public static List<EventData> assertEvents(CalendarResult actionResponse, String uid, int size) {
        assertNotNull(actionResponse);
        assertThat(I(actionResponse.getCreated().size() + actionResponse.getUpdated().size()), is(I(size)));
        List<EventData> events = new LinkedList<>();
        gatherEventGroup(actionResponse.getCreated(), uid, events);
        gatherEventGroup(actionResponse.getUpdated(), uid, events);
        Assertions.assertTrue(events.size() == size);
        return events;
    }

    /**
     * Asserts that one series instance event, along with the series master event was handled by the server
     *
     * @param actionResponse The {@link ActionResponse} from the server
     * @param uid The uid the event should have or <code>null</code>
     * @return The {@link EventData} of the handled event occurrence (not the series master)
     */
    public static EventData assertSingleEventAndMaster(CalendarResult actionResponse, String uid) {
        List<EventData> events = assertEvents(actionResponse, uid, 2);
        EventData seriesMaster = null;
        EventData eventOccurrence = null;
        for (EventData eventData : events) {
            if (Objects.equals(eventData.getSeriesId(), eventData.getId())) {
                seriesMaster = eventData;
            } else {
                eventOccurrence = eventData;
            }
        }
        assertNotNull(seriesMaster);
        assertNotNull(eventOccurrence);
        assertEquals(seriesMaster.getSeriesId(), eventOccurrence.getSeriesId());
        return eventOccurrence;
    }

    private static void gatherEventGroup(List<EventData> response, String uid, List<EventData> events) {
        for (EventData eventData : response) {
            if (null != uid) {
                assertEquals(uid, eventData.getUid());
            }
            events.add(eventData);
        }
    }

    /**
     * Assert that the action failed due missing calendar access
     *
     * @param response The response to check
     */
    public static void assertNoCalendarAccess(AnalyzeResponse response) {
        assertNoCalendarAccess(response.getError(), response.getErrorDesc());
    }

    /**
     * Assert that the action failed due missing calendar access
     *
     * @param error The error to check
     * @param errorDesc The error description to log
     */
    public static void assertNoCalendarAccess(String error, String errorDesc) {
        assertNoCalendarAccess(error, errorDesc, INTERNAL_CALENDAR);
    }

    /**
     * Assert that the action failed due missing calendar access
     *
     * @param error The error to check
     * @param errorDesc The error description to log
     * @param providerId The provider ID that shall be checked
     */
    public static void assertNoCalendarAccess(String error, String errorDesc, String providerId) {
        // com.openexchange.chronos.exception.CalendarExceptionMessages.UNSUPPORTED_OPERATION_FOR_PROVIDER_MSG
        assertThat("User has no access, so no operation should have been executed. " + errorDesc, error, is("The requested operation is not supported for calendar provider \"" + providerId + "\"."));
    }

    /**
     * Searches for the HTML part of the mail, containing the detailed changes
     *
     * @param content The expected content of the mail
     * @param mail The mail with the actual content to search in
     * @param errorMsg The error message to use
     */
    public static void assertContent(String content, MailData mail, String errorMsg) {
        assertNotNull(content);
        assertContent(ct -> assertTrue(ct.contains(content), errorMsg), mail);
    }

    /**
     * Searches for the HTML part of the mail, containing the detailed changes
     *
     * @param content The expected content of the mail
     * @param mail The mail with the actual content to search in
     */
    public static void assertContent(String content, MailData mail) {
        assertContent(mail, new String[] { content });
    }

    /**
     * Searches for the HTML part of the mail, containing the detailed changes
     *
     * @param mail The mail with the actual content to search in
     * @param content The expected content of the mail
     */
    public static void assertContent(MailData mail, String... content) {
        assertNotNull(content);
        assertContent(ct -> {
            Arrays.asList(content).forEach(c -> assertTrue(ct.contains(c), "Didn't find content: " + c));
        }, mail);
    }

    /**
     * Searches for the HTML part of the mail, containing the detailed changes
     *
     * @param assertion The assertion containing the expected data to find in the content
     * @param mail The mail with the actual content to search in
     */
    public static void assertContent(Consumer<String> assertion, MailData mail) {
        assertNotNull(mail);
        for (MailAttachment attachment : mail.getAttachments()) {
            if ("text/html".equals(attachment.getContentType())) {
                assertion.accept(attachment.getContent());
                return;
            }
        }
        Assertions.fail("Unable to find HTML part of the mail");
    }

    /**
     * Asserts that the organizer and attendees in an event are matching certain expectations towards being <i>internal</i> or <i>external</i>.
     * 
     * @param eventData The event data to check the participants in
     * @param expectedOrganizer The expected internal organizer
     * @param expectedInternalAttendees The expected internal attendees
     * @param expectedExternalAttendees The expected external attendees
     */
    public static void assertInternalOrganizerAndAttendees(EventData eventData, UserData expectedOrganizer, UserData[] expectedInternalAttendees, UserData[] expectedExternalAttendees) {
        assertOrganizer(eventData, expectedOrganizer, true);
        assertAttendees(eventData, expectedInternalAttendees, expectedExternalAttendees);
    }

    /**
     * Asserts that the organizer and attendees in an event are matching certain expectations towards being <i>internal</i> or <i>external</i>.
     * 
     * @param eventData The event data to check the participants in
     * @param expectedOrganizer The expected external organizer
     * @param expectedInternalAttendees The expected internal attendees
     * @param expectedExternalAttendees The expected external attendees
     */
    public static void assertExternalOrganizerAndAttendees(EventData eventData, UserData expectedOrganizer, UserData[] expectedInternalAttendees, UserData[] expectedExternalAttendees) {
        assertOrganizer(eventData, expectedOrganizer, false);
        assertAttendees(eventData, expectedInternalAttendees, expectedExternalAttendees);
    }

    private static CalendarUser assertOrganizer(EventData eventData, UserData expectedOrganizer, boolean internal) {
        CalendarUser organizer = eventData.getOrganizer();
        assertNotNull(organizer);
        if (internal) {
            assertEquals(expectedOrganizer.getUserId(), organizer.getEntity());
        } else {
            assertEquals(getURI(expectedOrganizer.getEmail1()), organizer.getUri());
        }
        return organizer;
    }

    private static List<Attendee> assertAttendees(EventData eventData, UserData[] expectedInternals, UserData[] expectedExternals) {
        return assertAttendees(eventData.getAttendees(), expectedInternals, expectedExternals);
    }

    private static List<Attendee> assertAttendees(List<Attendee> attendees, UserData[] expectedInternals, UserData[] expectedExternals) {
        int expectedCount = 0;
        if (null != expectedExternals && 0 < expectedExternals.length) {
            expectedCount += expectedExternals.length;
            for (UserData expectedExternal : expectedExternals) {
                assertNotNull(ChronosUtils.lookupAttendee(attendees, expectedExternal.getEmail1()));
            }
        }
        if (null != expectedInternals && 0 < expectedInternals.length) {
            expectedCount += expectedInternals.length;
            for (UserData expectedInternal : expectedInternals) {
                assertNotNull(ChronosUtils.lookupAttendee(attendees, expectedInternal.getUserId()));
            }
        }
        assertEquals(expectedCount, null == attendees ? 0 : attendees.size());
        return attendees;
    }

}
