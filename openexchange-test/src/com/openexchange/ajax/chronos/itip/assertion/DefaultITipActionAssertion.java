/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.chronos.itip.assertion;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.List;
import org.jcodec.codecs.mjpeg.tools.AssertionException;
import com.openexchange.testing.httpclient.models.Analysis;
import com.openexchange.testing.httpclient.models.Analysis.ActionsEnum;

/**
 * {@link DefaultITipActionAssertion}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public abstract class DefaultITipActionAssertion implements ITipActionAssertion {

    @Override
    public void validate(List<Analysis> analysis) throws AssertionException {
        assertFalse(null == analysis, "No analysis was given");
        assertTrue(analysis.size() == 1, "Only one event should have been analyzed");
        Analysis a = analysis.get(0);
        assertFalse(a.getActions() == null || a.getActions().isEmpty(), "There a no suggested actions");
        validateActions(a.getActions());
    }

    /**
     * Validates the actions
     * 
     * @param actions The actions to validate
     */
    protected abstract void validateActions(List<ActionsEnum> actions);

    /**
     * Checks the size of the action list
     *
     * @param actions The actions
     * @param size The size
     */
    protected void checkSize(List<ActionsEnum> actions, int size) {
        assertTrue(actions.size() == size, "There should be " + size + " action(s), but was " + actions.size());
    }

    /**
     * Checks that all given wanted actions are within the actions list
     *
     * @param actions The action list
     * @param wanted The action that must be in the list
     */
    protected void checkWantedActions(List<ActionsEnum> actions, ActionsEnum... wanted) {
        for (ActionsEnum action : wanted) {
            assertTrue(actions.contains(action), "Missing action " + action);
        }
    }

    /**
     * Check that all given unwanted actions are NOT within the actions list
     *
     * @param actions The action list
     * @param unwanted The action NOT to be in the list
     */
    protected void checkUnwantedActions(List<ActionsEnum> actions, ActionsEnum... unwanted) {
        for (ActionsEnum action : unwanted) {
            assertFalse(actions.contains(action), "Unwanted action!" + action);
        }
    }

    /**
     * Checks that one of the given actions is contained in the action list
     *
     * @param actions The action list
     * @param wanted The actions from which at least one shall be within the list
     */
    protected void containsOneOf(List<ActionsEnum> actions, ActionsEnum... wanted) {
        for (ActionsEnum action : wanted) {
            if (actions.contains(action)) {
                return;
            }
        }
        fail("None of the actions were found " + wanted);
    }

}
