/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.chronos.itip.assertion;

import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.ACCEPT;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.ACCEPT_AND_IGNORE_CONFLICTS;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.ACCEPT_PARTY_CRASHER;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.APPLY_CHANGE;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.APPLY_CREATE;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.APPLY_PROPOSAL;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.APPLY_REMOVE;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.APPLY_RESPONSE;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.DECLINE;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.DECLINE_PARTY_CRASHER;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.TENTATIVE;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import com.openexchange.testing.httpclient.models.Analysis;
import com.openexchange.testing.httpclient.models.Analysis.ActionsEnum;

/**
 * {@link ITipActionAssertions}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public final class ITipActionAssertions {

    /**
     * Initializes a new {@link ITipActionAssertions}.
     *
     */
    private ITipActionAssertions() {
        super();
    }

    /** Validates that the response does contain all common actions */
    public static final ITipActionAssertion ALL = new DefaultITipActionAssertion() {

        @Override
        protected void validateActions(List<ActionsEnum> actions) {
            containsOneOf(actions, ACCEPT, ACCEPT_AND_IGNORE_CONFLICTS);
            checkWantedActions(actions, TENTATIVE, DECLINE);
            containsOneOf(actions, APPLY_PROPOSAL, APPLY_REMOVE, APPLY_RESPONSE, APPLY_CREATE, APPLY_CHANGE);
        }
    };

    /** Validates that the response does only contain actions for the participant status of the user */
    public static final ITipActionAssertion ACTIONS = new DefaultITipActionAssertion() {

        @Override
        protected void validateActions(List<ActionsEnum> actions) {
            containsOneOf(actions, ACCEPT, ACCEPT_AND_IGNORE_CONFLICTS);
            checkWantedActions(actions, TENTATIVE, DECLINE);
            checkUnwantedActions(actions, APPLY_PROPOSAL, APPLY_REMOVE, APPLY_RESPONSE, APPLY_CREATE, APPLY_CHANGE);
        }
    };

    /** Validates that the response does only contain the APPLY action */
    public static final ITipActionAssertion APPLY = new DefaultITipActionAssertion() {

        @Override
        protected void validateActions(List<ActionsEnum> actions) {
            checkUnwantedActions(actions, ACCEPT, ACCEPT_AND_IGNORE_CONFLICTS, TENTATIVE, DECLINE);
            containsOneOf(actions, APPLY_PROPOSAL, APPLY_REMOVE, APPLY_RESPONSE, APPLY_CREATE, APPLY_CHANGE);
        }
    };

    /** Validates that the response doesn't contain any action */
    public static final ITipActionAssertion EMPTY = new ITipActionAssertion() {

        @Override
        public void validate(List<Analysis> analysis) throws AssertionError {
            assertTrue(null != analysis && analysis.size() == 1, "Only one event should have been analyzed");
            assertTrue(analysis.get(0).getActions() == null || analysis.get(0).getActions().isEmpty(), "There should be no action, but was " + analysis.get(0).getActions());
        }

    };

    /** Validates that the response does contain the party crasher action */
    public static final ITipActionAssertion PARTY_CRASHER = new DefaultITipActionAssertion() {

        @Override
        protected void validateActions(List<ActionsEnum> actions) {
            checkSize(actions, 2);
            checkWantedActions(actions, ACCEPT_PARTY_CRASHER, DECLINE_PARTY_CRASHER);
        }
    };

    /** Validates that the response does contain the cancel action */
    public static final ITipActionAssertion CANCEL = new DefaultITipActionAssertion() {

        @Override
        protected void validateActions(List<ActionsEnum> actions) {
            checkSize(actions, 1);
            checkWantedActions(actions, APPLY_REMOVE);
        }
    };

    /** Validates that the response does contain the ignore action */
    public static final ITipActionAssertion IGNORE = new DefaultITipActionAssertion() {

        @Override
        protected void validateActions(List<ActionsEnum> actions) {
            checkSize(actions, 1);
            checkWantedActions(actions, ActionsEnum.IGNORE);
        }
    };
}
