/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.chronos.itip;

import static com.openexchange.ajax.chronos.itip.ITipUtil.constructBody;
import static com.openexchange.ajax.chronos.itip.ITipUtil.forwardMail;
import static com.openexchange.ajax.chronos.itip.ITipUtil.receiveIMip;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertAnnotations;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertAttendeePartStat;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertContent;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleChange;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleEvent;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import com.openexchange.ajax.chronos.factory.EventFactory;
import com.openexchange.ajax.chronos.factory.PartStat;
import com.openexchange.ajax.chronos.itip.assertion.ITipActionAssertions;
import com.openexchange.ajax.chronos.util.DateTimeUtil;
import com.openexchange.ajax.framework.SessionAwareClient;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.test.common.test.TestClassConfig;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.AnalysisChangeDeletedEvent;
import com.openexchange.testing.httpclient.models.AnalysisChangeNewEvent;
import com.openexchange.testing.httpclient.models.AnalyzeResponse;
import com.openexchange.testing.httpclient.models.Attendee;
import com.openexchange.testing.httpclient.models.DeclinePartyCrasherResponse;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.models.SchedulingMailResponseData;
import com.openexchange.testing.httpclient.models.SchedulingMailResponseData.StatusEnum;

/**
 * {@link ITipDeclinePartyCrasherTest}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class ITipDeclinePartyCrasherTest extends AbstractITipAnalyzeTest {

    private static final String SINGLE = "single";
    private static final String SERIES = "series";
    private static final String EXCEPTION = "exception";

    @Override
    public TestClassConfig getTestConfig() {
        return TestClassConfig.builder().withContexts(2).build();
    }

    @ParameterizedTest
    @MethodSource(
    { "data" })
    public void testDecline(String type) throws Exception {
        /*
         * As user A, create event
         */
        Attendee replyingAttendee = createEvent(type);
        String summary = EXCEPTION.equals(type) ? EXCEPTION : createdEvent.getSummary();
        int sequenceToMatch = EXCEPTION.equals(type) ? 1 : 0;
        /*
         * As user B, forward to user C of same context
         */
        TestUser partyCrasher = context2.acquireUser();
        MailData invitation = receiveIMip(apiClientC2, userResponseC1.getData().getEmail1(), summary, sequenceToMatch, SchedulingMethod.REQUEST);
        forwardMail(testUserC2, partyCrasher, invitation.getId());
        /*
         * As user C alias the party crasher, receive forwarded mail
         */
        SessionAwareClient apiClient3 = partyCrasher.getApiClient();
        MailData iMip = receiveIMip(apiClient3, userResponseC2.getData().getEmail1(), summary, sequenceToMatch, SchedulingMethod.REQUEST);
        AnalysisChangeNewEvent newEvent = assertSingleChange(analyze(apiClient3, iMip)).getNewEvent();
        assertNotNull(newEvent);
        assertEquals(createdEvent.getUid(), newEvent.getUid());
        assertAttendeePartStat(newEvent.getAttendees(), replyingAttendee.getEmail(), PartStat.NEEDS_ACTION.getStatus());
        /*
         * As user C, accept
         */
        EventData eventData = assertSingleEvent(accept(apiClient3, constructBody(iMip), null), createdEvent.getUid());
        assertAttendeePartStat(eventData.getAttendees(), partyCrasher.getLogin(), PartStat.ACCEPTED.getStatus());
        /*
         * As user A, receive mail as organizer and check actions for party crasher
         */
        MailData reply = receiveIMip(apiClient, partyCrasher.getLogin(), summary, sequenceToMatch, SchedulingMethod.REPLY);
        analyze(reply.getId(), ITipActionAssertions.PARTY_CRASHER);
        /*
         * As user A, decline participation of party crasher
         */
        String comment = "I don't like you!";
        DeclinePartyCrasherResponse crasherResponse = chronosApi.declinePartyCrasherBuilder()
                                                                .withMessage(comment)
                                                                .withConversionDataSource(constructBody(reply))
                                                                .execute();
        SchedulingMailResponseData sentData = checkResponse(crasherResponse.getError(), crasherResponse.getErrorDesc(), crasherResponse.getData());
        assertEquals(StatusEnum.SENT, sentData.getStatus());
        assertEquals(partyCrasher.getLogin(), sentData.getRecipient().getEmail());
        /*
         * As user A, update the event, so that data in the mail and on the server differ
         */
        createdEvent = eventManager.getEvent(defaultFolderId, createdEvent.getId());
        createdEvent = eventManager.updateEvent(EventFactory.deltaOf(createdEvent).location("Olpe"));
        /*
         * As user C, receive CANCEL mail as party crasher from the organizer
         */
        MailData cancel = receiveIMip(apiClient3, userResponseC1.getData().getEmail1(), summary, sequenceToMatch + 1, SchedulingMethod.CANCEL);
        AnalyzeResponse analyze = analyze(apiClient3, cancel);
        AnalysisChangeDeletedEvent canceledEvent = assertSingleChange(analyze).getDeletedEvent();
        assertNotNull(canceledEvent);
        assertEquals(createdEvent.getUid(), canceledEvent.getUid());
        assertAnalyzeActions(analyze, ITipActionAssertions.CANCEL);
        /*
         * As user C, check that comment of organizer is set
         */
        assertContent(comment, cancel);
        assertAnnotations(analyze, comment);
        /*
         * As user C, check that the canceled event from the mail doesn't contain the latest changes from the event stored on the server
         */
        assertNull(canceledEvent.getLocation());
    }

    /**
     * Type of the event to create. Creating events is depending on created API clients or rather test users.
     * Therefore, events must be created dynamically
     *
     * @return The type of event to create
     */
    public static Stream<String> data() {
        return Stream.of(SINGLE, SERIES, EXCEPTION);
    }

    private Attendee createEvent(String type) throws ApiException, Exception {
        return switch (type) {
            case SINGLE -> createSingleEvent();
            case SERIES -> createSeries();
            case EXCEPTION -> createSeriesExceptionEvent();
            default -> throw new IllegalArgumentException("Unexpected value: " + type);
        };

    }

    private Attendee createSingleEvent() throws ApiException, Exception {
        EventData eventToCreate = EventFactory.createSingleTwoHourEvent(0, this.getClass().getSimpleName() + UUID.randomUUID().toString());
        Attendee replyingAttendee = prepareCommonAttendees(eventToCreate);
        createdEvent = eventManager.createEvent(eventToCreate, true);
        createAndApplyAccept(replyingAttendee, null);
        return replyingAttendee;
    }

    private Attendee createSeries() throws ApiException, Exception {
        EventData seriesToCreate = EventFactory.createSeriesEvent(testUser.getUserId(), this.getClass().getSimpleName() + UUID.randomUUID().toString(), 10, defaultFolderId);
        Attendee replyingAttendee = prepareCommonAttendees(seriesToCreate);
        createdEvent = eventManager.createEvent(seriesToCreate, true);
        createAndApplyAccept(replyingAttendee, null);
        return replyingAttendee;
    }

    private Attendee createSeriesExceptionEvent() throws ApiException, Exception {
        /*
         * Create series
         */
        EventData seriesToCreate = EventFactory.createSeriesEvent(testUser.getUserId(), this.getClass().getSimpleName() + UUID.randomUUID().toString(), 10, defaultFolderId);
        Attendee replyingAttendee = prepareCommonAttendees(seriesToCreate);
        createdEvent = eventManager.createEvent(seriesToCreate, true);
        createAndApplyAccept(replyingAttendee, null);
        /*
         * Create a single change occurrence as organizer
         */
        List<EventData> allEvents = getAllEventsOfEvent(eventManager, createdEvent);
        EventData calculatedEvent = allEvents.get(2);
        String recurrenceId = calculatedEvent.getRecurrenceId();
        createdEvent = eventManager.createChangeException(createdEvent, null, recurrenceId,
                                                          delta -> delta.summary(EXCEPTION)
                                                                        .endDate(DateTimeUtil.incrementDateTimeData(calculatedEvent.getEndDate(), TimeUnit.HOURS.toMillis(1L))));
        return replyingAttendee;
    }

}
