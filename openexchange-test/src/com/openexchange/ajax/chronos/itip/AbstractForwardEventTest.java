/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.chronos.itip;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.chronos.factory.AttendeeFactory;
import com.openexchange.chronos.Event;
import com.openexchange.chronos.ical.ImportedCalendar;
import com.openexchange.test.common.test.TestClassConfig;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.CalendarUser;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.EventForwardMailBody;
import com.openexchange.testing.httpclient.models.EventForwardedResponse;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.models.SchedulingMailResponseData;
import com.openexchange.testing.httpclient.models.SchedulingMailResponseData.StatusEnum;
import com.openexchange.testing.httpclient.modules.ChronosApi;

/**
 * {@link AbstractForwardEventTest} - Tooling for the forward HTTP action within the chronos module
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class AbstractForwardEventTest extends AbstractITipAnalyzeTest {

    protected static final String FORWARDED_INTRO = "has forwarded an invitation to the appointment";
    protected static final String FORWARDED_INSTANCE_INTRO = "has forwarded an invitation to an appointment of the series";
    protected static final String COMMENT = "Can you take over for me?";

    protected TestUser forwardTarget;
    protected TestUser testUser3;

    @BeforeEach
    @Override
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        testUser3 = testContext.acquireUser();
    }

    @Override
    public TestClassConfig getTestConfig() {
        return TestClassConfig.builder().withContexts(2).withUserPerContext(4).build();
    }

    /**
     * Prepares the test
     *
     * @param crossContext <code>true</code> if the test prepare for a cross-context setup
     */
    protected void prepare(boolean crossContext) {
        if (crossContext) {
            forwardTarget = testUserC2;
        } else {
            forwardTarget = testUser2;
        }
    }

    /*
     * ============================== HELPERS ==============================
     */

    /**
     * Forwards the event to the second user {@link #forwardTarget}
     *
     * @param event The event to send
     * @return The response
     * @throws ApiException In case of error
     */
    protected EventForwardedResponse forwardToTargetUser(EventData event) throws ApiException {
        return forwardToUser(chronosApi, event, defaultFolderId, forwardTarget);
    }

    /**
     * Forwards the event to the given user
     *
     * @param api The API to use
     * @param event The event to forward
     * @param folderId The folder identifier of the event
     * @param user The user to forward to
     * @return The response
     * @throws ApiException In case of error
     */
    public static EventForwardedResponse forwardToUser(ChronosApi api, EventData event, String folderId, TestUser user) throws ApiException {
        return forwardToUser(api, event.getId(), event.getRecurrenceId(), folderId, user);
    }

    /**
     * Forwards the event to the given user
     *
     * @param api The API to use
     * @param eventId The event ID
     * @param reccurrenceId The recurrence ID
     * @param folderId The folder identifier of the event
     * @param user The user to forward to
     * @return The response
     * @throws ApiException In case of error
     */
    public static EventForwardedResponse forwardToUser(ChronosApi api, String eventId, String reccurrenceId, String folderId, TestUser user) throws ApiException {
        return api.forwardEventBuilder().withId(eventId).withFolder(folderId).withRecurrenceId(reccurrenceId).withEventForwardMailBody(createBody(user)).execute();
    }

    /**
     * Creates an {@link EventForwardMailBody} based on the given event
     *
     * @param users The users to forward to
     * @return The body
     */
    public static EventForwardMailBody createBody(TestUser... users) {
        EventForwardMailBody body = new EventForwardMailBody();
        body.setComment(COMMENT);
        List<CalendarUser> recipients = new ArrayList<>(users.length);
        for (TestUser u : users) {
            CalendarUser calendarUser = AttendeeFactory.createCalendarUser(u);
            calendarUser.setEntity(null);
            recipients.add(calendarUser);
        }
        body.setRecipients(recipients);
        return body;
    }

    /**
     * Assert the given calendar is correctly generated and contains one event
     *
     * @param calendar The calendar
     * @return The event
     */
    public static Event assertCalendar(ImportedCalendar calendar) {
        return assertCalendar(calendar, 1).get(0);
    }

    /**
     * Assert the given calendar is correctly generated
     *
     * @param calendar The calendar
     * @param eventSize The size of events it should contain
     * @return The event in the calendar
     */
    public static List<Event> assertCalendar(ImportedCalendar calendar, int eventSize) {
        assertTrue(null != calendar && null != calendar.getEvents() && eventSize == calendar.getEvents().size());
        assertThat(calendar.getExtendedProperties().get("X-OX-ITIP"), is(notNullValue()));
        return calendar.getEvents();
    }

    /**
     * Check that the response contains a {@link StatusEnum#SENT}
     *
     * @param response The response to check
     * @return The status
     */
    public static StatusEnum checkResponse(EventForwardedResponse response) {
        return checkResponse(response, StatusEnum.SENT).get(0);
    }

    /**
     * Check that the response contains a {@link StatusEnum#SENT}
     *
     * @param response The response to check
     * @param expectedValues The expected values
     * @return The status
     */
    public static List<StatusEnum> checkResponse(EventForwardedResponse response, StatusEnum... expectedValues) {
        List<SchedulingMailResponseData> data = checkResponse(response.getError(), response.getErrorDesc(), response.getData());
        assertThat(data, is(notNullValue()));
        assertEquals(data.size(), expectedValues.length);
        for (int i = 0; i < expectedValues.length; i++) {
            StatusEnum status = expectedValues[i];
            assertThat(response.getData().get(i).getStatus(), is(status));
        }
        return data.stream().map(r -> r.getStatus()).toList();
    }

    /**
     * Check that the mail subject begins with <code>Fwd:</code>
     *
     * @param mail The mail to check
     * @return The mail
     */
    public static MailData checkMailSubject(MailData mail) {
        assertTrue(mail.getSubject().startsWith("Fwd: "));
        return mail;
    }

}
