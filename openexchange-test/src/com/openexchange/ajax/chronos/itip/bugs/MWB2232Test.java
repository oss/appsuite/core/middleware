/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
* 
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.chronos.itip.bugs;

import static com.openexchange.ajax.chronos.factory.EventFactory.prepareDeltaEvent;
import static com.openexchange.ajax.chronos.itip.ITipUtil.constructBody;
import static com.openexchange.ajax.chronos.itip.ITipUtil.prepareForAttendeeUpdate;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertAttendeePartStat;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleEvent;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleEventAndMaster;
import static com.openexchange.ajax.chronos.util.ChronosUtils.getAllEventsOfEvent;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.chronos.AbstractExtendedChronosTest;
import com.openexchange.ajax.chronos.factory.EventFactory;
import com.openexchange.ajax.chronos.factory.PartStat;
import com.openexchange.ajax.chronos.itip.IMipReceiver;
import com.openexchange.ajax.chronos.manager.EventManager;
import com.openexchange.ajax.framework.SessionAwareClient;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.test.common.test.TestClassConfig;
import com.openexchange.test.common.test.pool.TestContext;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.models.ActionResponse;
import com.openexchange.testing.httpclient.models.AttendeeAndAlarm;
import com.openexchange.testing.httpclient.models.CalendarResult;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.models.UserResponse;
import com.openexchange.testing.httpclient.modules.ChronosApi;

/**
 * 
 * {@link MWB2232Test}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class MWB2232Test extends AbstractExtendedChronosTest {

    protected UserResponse userResponseC2;

    protected SessionAwareClient apiClientC2;

    protected TestUser testUserC2;

    protected TestContext context2;

    protected String folderIdC2;

    protected EventManager eventManagerC2;

    @Override
    public TestClassConfig getTestConfig() {
        return TestClassConfig.builder().withContexts(2).build();
    }

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        context2 = testContextList.get(1);
        testUserC2 = context2.acquireUser();
        apiClientC2 = testUserC2.getApiClient();
        folderIdC2 = getDefaultFolder(apiClientC2);
        eventManagerC2 = new EventManager(new com.openexchange.ajax.chronos.UserApi(apiClientC2, testUserC2), folderIdC2);
        eventManagerC2.setIgnoreConflicts(true);
    }

    //@Test
    public void testAttendeeCopyUpdate() throws Exception {
        /*
         * As organizer, create event with external users
         */
        EventData eventData = EventFactory.createSeriesEvent(testUserC2.getUserId(), this.getClass().getSimpleName() + "#testMailSentAfterComment", 10, folderIdC2);
        eventData.attendees(List.of(getExternalAttendee(testUser), getExternalAttendee(testUser2)));
        EventData createdEvent = eventManagerC2.createEvent(eventData);
        assertTrue(createdEvent.getAttendees().size() == 3);
        assertEquals(createdEvent.getId(), createdEvent.getSeriesId());
        String uid = createdEvent.getUid();
        /*
         * As attendees, receive invitation and accept
         */
        for (TestUser t : List.of(testUser, testUser2)) {
            MailData iMIP = new IMipReceiver(t.getApiClient()).event(createdEvent).from(testUserC2.getLogin()).method(SchedulingMethod.REQUEST).receive();
            ActionResponse response = new ChronosApi(t.getApiClient()).accept(constructBody(iMIP), null, null);
            EventData event = assertSingleEvent(response);
            assertAttendeePartStat(event.getAttendees(), t.getLogin(), PartStat.ACCEPTED);
            assertEquals(event.getId(), event.getSeriesId());
        }
        /*
         * As attendee A, decline second occurrence
         */
        List<EventData> events = getAllEventsOfEvent(eventManager, uid);
        EventData secondOccurrence = events.get(2);
        EventData deltaEvent = prepareDeltaEvent(secondOccurrence);
        String comment = "No time";
        AttendeeAndAlarm attendeeAndAlarm = prepareForAttendeeUpdate(deltaEvent, testUser.getLogin(), PartStat.DECLINED.getStatus(), comment);
        CalendarResult calendarResult = eventManager.updateAttendee(secondOccurrence.getId(), secondOccurrence.getRecurrenceId(), null, attendeeAndAlarm, false);
        EventData eventException = assertSingleEventAndMaster(calendarResult, uid);
        assertAttendeePartStat(eventException.getAttendees(), testUser.getLogin(), PartStat.DECLINED);
        assertFalse(eventException.getSeriesId().equals(eventException.getId()), "No exception was created");
        /*
         * As organizer, receive reply and update series and check updates
         */
        MailData reply = new IMipReceiver(testUserC2.getApiClient()).event(eventException).from(testUser.getLogin()).method(SchedulingMethod.REPLY).receive();
        new ChronosApi(testUserC2.getApiClient()).applyChange(constructBody(reply), null);
        for (EventData data : getAllEventsOfEvent(eventManagerC2, uid)) {
            if (data.getId().equals(data.getSeriesId())) {
                assertAttendeePartStat(data.getAttendees(), testUser.getLogin(), PartStat.ACCEPTED);
            } else {
                // Exception was created
                assertAttendeePartStat(data.getAttendees(), testUser.getLogin(), PartStat.DECLINED);
            }
        }
        /*
         * As attendee B, get all events and check data
         */
        for (EventData data : getAllEventsOfEvent(eventManager2, uid)) {
            if (null != data.getRecurrenceId() && eventException.getRecurrenceId().equals(data.getRecurrenceId())) {
                assertEquals(data.getSeriesId(), data.getId(), "There should not be an event exception for this attendee!");
                assertAttendeePartStat(data.getAttendees(), testUser.getLogin(), PartStat.DECLINED);
            } else {
                assertAttendeePartStat(data.getAttendees(), testUser.getLogin(), PartStat.ACCEPTED);
            }
        }

    }

}
