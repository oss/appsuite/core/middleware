/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.chronos.itip.bugs;

import static com.openexchange.ajax.chronos.LinkedAttachmentsTest.assertAttachments;
import static com.openexchange.ajax.chronos.LinkedAttachmentsTest.prepareInlineAttachment;
import static com.openexchange.ajax.chronos.itip.ITipUtil.constructBody;
import static com.openexchange.ajax.chronos.itip.ITipUtil.receiveIMip;
import static com.openexchange.java.Autoboxing.L;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.chronos.itip.AbstractITipAnalyzeTest;
import com.openexchange.ajax.chronos.manager.ICalImportExportManager;
import com.openexchange.ajax.chronos.util.DateTimeUtil;
import com.openexchange.chronos.common.CalendarUtils;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.exception.Category;
import com.openexchange.java.util.UUIDs;
import com.openexchange.test.common.asset.Asset;
import com.openexchange.test.common.asset.AssetType;
import com.openexchange.test.common.groupware.calendar.TimeTools;
import com.openexchange.test.common.test.TestClassConfig;
import com.openexchange.test.common.test.TestContextConfig;
import com.openexchange.test.common.test.pool.ConfigAwareProvisioningService;
import com.openexchange.testing.httpclient.models.Attendee;
import com.openexchange.testing.httpclient.models.CalendarResult;
import com.openexchange.testing.httpclient.models.CalendarUser;
import com.openexchange.testing.httpclient.models.ChronosAttachment;
import com.openexchange.testing.httpclient.models.ChronosCalendarResultResponse;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.EventId;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.modules.ChronosApi;

/**
 * {@link Core113Test} - Handle iMIP events with attachments when filestore is disabled
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class Core113Test extends AbstractITipAnalyzeTest {

    @Override
    public TestClassConfig getTestConfig() {
        return TestClassConfig.builder().withContexts(2).build();
    }
    
    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        TestContextConfig config = TestContextConfig.builder().withMaxQuota(L(1L)).withConfig(Map.of("com.openexchange.capability.filestore", "false")).build();
        ConfigAwareProvisioningService.getService().changeContext(testContext.getId(), config, testContext.getUsedBy());
    }

    @Test
    public void testInviteWithAttachments() throws Exception {
        /*
         * prepare event with attachments, organized from user in context 2
         */
        List<Asset> assets = Arrays.asList(assetManager.getAsset(AssetType.pdf, "26926_27394.pdf"), assetManager.getAsset(AssetType.jpg, "27872_cmyk.jpg"));
        EventData eventData = prepareEventWithAttachments(assets, getCalendarUser(testUserC2), getExternalAttendee(testUser), getUserAttendee(testUserC2));
        /*
         * create event as user in context 2 & check stored attachment metadata
         */
        JSONObject response = eventManagerC2.createEventWithAttachments(eventData, assets);
        EventData createdEvent = eventManagerC2.getEvent(response.getString("folder"), response.getString("id"));
        assertAttachments(eventData.getAttachments(), createdEvent);
        /*
         * receive iMIP as user in context 1 & apply the changes into the user's calendar
         */
        MailData iMipMail = receiveIMip(apiClient, userResponseC2.getData().getEmail1(), createdEvent.getSummary(), createdEvent.getSequence().intValue(), SchedulingMethod.REQUEST);
        ChronosCalendarResultResponse applyResponse = new ChronosApi(apiClient).applyCreate(constructBody(iMipMail), null);
        CalendarResult createResult = applyResponse.getData();
        assertTrue(null != createResult.getCreated() && 1 == createResult.getCreated().size());
        assertTrue(null != applyResponse.getError() && Category.EnumType.WARNING.name().equals(applyResponse.getCategories()) && "CAL-1990".equals(applyResponse.getCode()));
        /*
         * lookup event in calendar & check that it was imported w/o attachments
         */
        createdEvent = eventManager.getEvent(createdEvent.getFolder(), createdEvent.getId());
        assertNotNull(createdEvent);
        List<ChronosAttachment> attachments = createdEvent.getAttachments();
        assertTrue(null == attachments || attachments.isEmpty());
    }

    @Test
    public void testCreateWithAttachments() throws Exception {
        /*
         * prepare own event with attachments
         */
        List<Asset> assets = Arrays.asList(assetManager.getAsset(AssetType.pdf, "26926_27394.pdf"), assetManager.getAsset(AssetType.jpg, "27872_cmyk.jpg"));
        EventData eventData = prepareEventWithAttachments(assets, getCalendarUser(testUser), getUserAttendee(testUser));
        /*
         * try to create event
         */
        JSONObject response = eventManager.createEventWithAttachments(eventData, assets, true);
        assertEquals("CAL-1991", response.getString("code"));
    }

    @Test
    public void testImportWithAttachments() throws Exception {
        /*
         * prepare iCal file & import it
         */
        String iCal = """
            BEGIN:VCALENDAR
            VERSION:2.0
            PRODID:-//Open-Xchange//8.19.0//EN
            BEGIN:VEVENT
            DTSTAMP:20241002T134253Z
            ATTACH;FMTTYPE=text/plain;ENCODING=BASE64;VALUE=BINARY:VGhlIH
             F1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4
            DTEND:20241005T203000Z
            DTSTART:20241005T190000Z
            SUMMARY:Core113Test
            END:VEVENT
            END:VCALENDAR
            """;
        String importResponse = new ICalImportExportManager(apiClient).importICalFile(defaultFolderId, iCal.getBytes(StandardCharsets.UTF_8), Boolean.TRUE, Boolean.FALSE);
        List<EventId> eventIds = ICalImportExportManager.parseImportJSONResponseToEventIds(importResponse);
        assertTrue(null != eventIds && 1 == eventIds.size());
        assertEquals(defaultFolderId, eventIds.get(0).getFolder());
        /*
         * get imported event & check that it was imported w/o attachments
         */
        String id = eventIds.get(0).getId();
        EventData eventData = chronosApi.getEventBuilder().withFolder(defaultFolderId).withId(id).execute().getData();
        assertNotNull(eventData);
        List<ChronosAttachment> attachments = eventData.getAttachments();
        assertTrue(null == attachments || attachments.isEmpty());
    }

    private EventData prepareEventWithAttachments(List<Asset> assets, CalendarUser organizer, Attendee... attendees) {
        List<ChronosAttachment> chronosAttachments = new ArrayList<ChronosAttachment>();
        for (int i = 0; i < assets.size(); i++) {
            chronosAttachments.add(prepareInlineAttachment(assets.get(i), i));
        }
        TimeZone timeZone = TimeZone.getTimeZone("Australia/Melbourne");
        Date start = TimeTools.D("Next week in the morning", timeZone);
        Date end = CalendarUtils.add(start, Calendar.HOUR, 2, timeZone);
        EventData eventData = new EventData();
        eventData.setSummary(UUIDs.getUnformattedStringFromRandom());
        eventData.setStartDate(DateTimeUtil.getDateTime(timeZone.getID(), start.getTime()));
        eventData.setEndDate(DateTimeUtil.getDateTime(timeZone.getID(), end.getTime()));
        eventData.setAttachments(chronosAttachments);
        eventData.setOrganizer(organizer);
        if (null != attendees) {
            eventData.setAttendees(Arrays.asList(attendees));
        }
        return eventData;
    }
}
