/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.chronos.itip.bugs;

import static com.openexchange.ajax.chronos.itip.ITipUtil.constructBody;
import static com.openexchange.ajax.chronos.itip.ITipUtil.receiveIMip;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertAttendeePartStat;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleChange;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleEvent;
import static com.openexchange.java.Autoboxing.i;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.chronos.factory.PartStat;
import com.openexchange.ajax.chronos.itip.AbstractITipAnalyzeTest;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.models.UserData;
import com.openexchange.testing.httpclient.modules.UserApi;

/**
 * {@link MWB1730Test}
 * 
 * com.openexchange.ajax.chronos.scheduling.WithoutCalendarAccessTest is flaky
 * 
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class MWB1730Test extends AbstractITipAnalyzeTest {

    private UserData user1;
    private UserData user2;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        setUpConfiguration();
        user1 = userResponseC1.getData();
        user2 = new UserApi(testUser2.getApiClient()).getUser(String.valueOf(testUser2.getUserId())).getData();
    }

    @Override
    protected Map<String, String> getNeededConfigurations() {
        return Collections.singletonMap("com.openexchange.calendar.useIMipForInternalUsers", Boolean.TRUE.toString());
    }

    @Override
    protected String getScope() {
        return "context";
    }

    @Test
    public void testAcceptInvitationToOccurrence() throws Exception {
        /*
         * as user 1, create event series, then add user 2 to third occurrence
         */
        EventData eventData = prepareSeriesEventData("FREQ=DAILY;COUNT=7", defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        EventData eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent1.getSeriesId(), 3);
        /*
         * as user 1, add user 2 to the occurrence
         */
        EventData occurrenceUpdate = new EventData()
            .folder(eventOccurrence1.getFolder()).id(eventOccurrence1.getId()).recurrenceId(eventOccurrence1.getRecurrenceId())
            .attendees(Arrays.asList(getUserAttendee(testUser), getUserAttendee(testUser2)));
        EventData updatedOccurrence1 = eventManager.updateOccurenceEvent(occurrenceUpdate, occurrenceUpdate.getRecurrenceId(), true);
        /*
         * as user 2, receive & analyze the invitation, the accept it
         */
        MailData iMip2 = receiveIMip(testUser2.getApiClient(), user1.getEmail1(), updatedOccurrence1.getSummary(), i(updatedOccurrence1.getSequence()), SchedulingMethod.REQUEST);
        assertSingleChange(analyze(testUser2.getApiClient(), iMip2));
        EventData acceptedEvent2 = assertSingleEvent(accept(testUser2.getApiClient(), constructBody(iMip2), null), updatedOccurrence1.getUid());
        assertAttendeePartStat(acceptedEvent2.getAttendees(), user2.getEmail1(), PartStat.ACCEPTED);
    }

}
