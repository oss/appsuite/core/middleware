/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.chronos.itip.bugs;

import static com.openexchange.ajax.chronos.itip.ITipUtil.constructBody;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertAttendeePartStat;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertEvents;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertExternalOrganizerAndAttendees;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertInternalOrganizerAndAttendees;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleChange;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleEvent;
import static com.openexchange.ajax.chronos.util.ChronosUtils.forwardEvent;
import static com.openexchange.ajax.chronos.util.ChronosUtils.loadEventData;
import static com.openexchange.ajax.chronos.util.ChronosUtils.lookupIMip;
import static com.openexchange.ajax.chronos.util.ChronosUtils.reloadEventData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.chronos.factory.PartStat;
import com.openexchange.ajax.chronos.itip.AbstractITipAnalyzeTest;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.test.common.test.TestClassConfig;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.models.UserData;
import com.openexchange.testing.httpclient.modules.UserApi;

/**
 * {@link MWB2380Test}
 * 
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class MWB2380Test extends AbstractITipAnalyzeTest {

    private UserData user1;
    private UserData user2;
    private UserData user3;
    private TestUser testUser3;
    private String defaultFolderId2;
    private String defaultFolderId3;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        setUpConfiguration();
        user1 = userResponseC1.getData();
        user2 = new UserApi(testUser2.getApiClient()).getUser(String.valueOf(testUser2.getUserId())).getData();
        defaultFolderId2 = getDefaultFolder(testUser2.getApiClient());
        testUser3 = testUserC2;
        defaultFolderId3 = folderIdC2;
        user3 = new UserApi(testUser3.getApiClient()).getUser(String.valueOf(testUser3.getUserId())).getData();
    }

    @Override
    public TestClassConfig getTestConfig() {
        return TestClassConfig.builder().withContexts(2).withUserPerContext(2).build();
    }

    @Test
    public void testForwardOccurrence() throws Exception {
        /*
         * as user 1, create event series and add user 2
         */
        EventData eventData = prepareSeriesEventData("FREQ=DAILY;COUNT=7", defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * as user 1, get one occurrence of the event series
         */
        EventData eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent1.getSeriesId(), 4);
        eventOccurrence1 = reloadEventData(testUser, eventOccurrence1, false);
        /*
         * as user 2, lookup appointment in the calendar and forward it using the API
         */
        EventData reloadedEventOccurrence2 = loadEventData(testUser2, defaultFolderId2, createdEvent1.getId(), eventOccurrence1.getRecurrenceId(), false);
        forwardEvent(testUser2.getApiClient(), reloadedEventOccurrence2, user3.getEmail1());
        /*
         * as user 3, receive & check forwarded mail, then 'accept' the invitation
         */
        MailData iMip3 = lookupIMip(testUser3, user2.getEmail1(), SchedulingMethod.REQUEST, eventOccurrence1);
        assertSingleChange(analyze(testUser3.getApiClient(), iMip3));
        EventData importedEventOccurrence3 = assertSingleEvent(accept(testUser3.getApiClient(), constructBody(iMip3), null), eventOccurrence1.getUid());
        assertAttendeePartStat(importedEventOccurrence3.getAttendees(), user3.getEmail1(), PartStat.ACCEPTED);
        /*
         * lookup event in user 3's calendar & check stored data
         */
        EventData reloadedEventOccurrence3 = reloadEventData(testUser3, importedEventOccurrence3, false);
        assertExternalOrganizerAndAttendees(reloadedEventOccurrence3, user1, new UserData[] { user3 }, new UserData[] { user1, user2 });
        /*
         * as user 1, receive unsolicited reply from user 3 & 'accept' the party crasher, then check resulting events
         */
        MailData iMip1 = lookupIMip(testUser, user3.getEmail1(), SchedulingMethod.REPLY, eventOccurrence1);
        assertSingleChange(analyze(testUser.getApiClient(), iMip1));
        assertEvents(acceptPartyCrasher(apiClient, constructBody(iMip1)), eventOccurrence1.getUid(), 2);
        /*
         * reload & verify occurrence in calendars of all users
         */
        EventData reloadedEventOccurrence1 = loadEventData(testUser, defaultFolderId, createdEvent1.getId(), eventOccurrence1.getRecurrenceId(), false);
        assertInternalOrganizerAndAttendees(reloadedEventOccurrence1, user1, new UserData[] { user1, user2, user3 }, null);
        reloadedEventOccurrence2 = reloadEventData(testUser2, reloadedEventOccurrence2, false);
        assertInternalOrganizerAndAttendees(reloadedEventOccurrence2, user1, new UserData[] { user1, user2, user3 }, null);
        reloadedEventOccurrence3 = reloadEventData(testUser3, reloadedEventOccurrence3, false);
        assertExternalOrganizerAndAttendees(reloadedEventOccurrence3, user1, new UserData[] { user3 }, new UserData[] { user1, user2 });
        /*
         * reload & verify the event series as well
         */
        EventData reloadedEventSeries1 = reloadEventData(testUser, createdEvent1, false);
        assertInternalOrganizerAndAttendees(reloadedEventSeries1, user1, new UserData[] { user1, user2 }, null);
        EventData reloadedEventSeries2 = loadEventData(testUser2, defaultFolderId2, createdEvent1.getId(), null, false);
        assertInternalOrganizerAndAttendees(reloadedEventSeries2, user1, new UserData[] { user1, user2 }, null);
        reloadedEventSeries2 = loadEventData(testUser3, defaultFolderId3, reloadedEventOccurrence3.getSeriesId(), null, true);
    }

}
