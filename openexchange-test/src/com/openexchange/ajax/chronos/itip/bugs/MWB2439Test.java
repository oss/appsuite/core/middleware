/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.chronos.itip.bugs;

import static com.openexchange.ajax.chronos.itip.ITipUtil.constructBody;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleChange;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleEvent;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import com.openexchange.ajax.chronos.factory.AttendeeFactory;
import com.openexchange.ajax.chronos.factory.EventFactory;
import com.openexchange.ajax.chronos.itip.AbstractForwardEventTest;
import com.openexchange.ajax.chronos.itip.IMipReceiver;
import com.openexchange.ajax.chronos.itip.ITipUtil;
import com.openexchange.ajax.chronos.itip.assertion.ITipActionAssertions;
import com.openexchange.ajax.chronos.util.ChronosUtils;
import com.openexchange.ajax.framework.SessionAwareClient;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.testing.httpclient.models.AnalysisChangeNewEvent;
import com.openexchange.testing.httpclient.models.AnalyzeResponse;
import com.openexchange.testing.httpclient.models.Attendee;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.EventForwardedResponse;
import com.openexchange.testing.httpclient.models.EventResponse;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.modules.ChronosApi;

/**
 * {@link MWB2439Test} - Tests that comments are properly handled when forwarding events
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class MWB2439Test extends AbstractForwardEventTest {

    @BeforeEach
    @Override
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
    }

    @ParameterizedTest
    @CsvSource(
    { "true, true", "true, false", "false, true", "false, false" })
    public void testEventForwardWithComment(boolean crossContext, boolean includeComment) throws Exception {
        /*
         * As user A, create event with user B
         */
        prepare(crossContext);
        EventData eventData = EventFactory.createSingleTwoHourEvent(testUser.getUserId(), "Forward single event");
        eventData.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser3));
        EventData createdEvent = eventManager.createEvent(eventData);
        /*
         * As user B, forward to user C
         */
        EventForwardedResponse response = forwardToTargetUser(createdEvent);
        checkResponse(response);
        /*
         * 
         * As user C, get mail and analyze
         */
        SessionAwareClient crasherClient = forwardTarget.getApiClient();
        IMipReceiver iMipReceiver = new IMipReceiver(crasherClient);
        MailData iMIP = iMipReceiver.event(createdEvent).from(testUser.getLogin()).method(SchedulingMethod.REQUEST).receive();
        checkMailSubject(iMIP);
        AnalyzeResponse analyze = analyze(crasherClient, iMIP);
        AnalysisChangeNewEvent newEvent = assertSingleChange(analyze).getNewEvent();
        assertNotNull(newEvent);
        assertEquals(createdEvent.getUid(), newEvent.getUid());
        assertAnalyzeActions(analyze, ITipActionAssertions.ALL);
        /*
         * As user C, accept and check event in calendar, comment especially
         */
        String attendeeComment = includeComment ? "Another comment" : null;
        EventData acceptedEvent = assertSingleEvent(accept(crasherClient, ITipUtil.constructBody(iMIP), attendeeComment));
        EventResponse eventResponse = new ChronosApi(crasherClient).getEventBuilder().withId(acceptedEvent.getId()).withFolder(getDefaultFolder(crasherClient)).execute();
        EventData crasherEvent = checkResponse(eventResponse.getError(), eventResponse.getErrorDesc(), eventResponse.getData());
        checkComment(crasherEvent, attendeeComment);
        /*
         * As user A, receive unsolicited reply from user C, comment especially
         */
        MailData reply = ITipUtil.receiveIMip(apiClient, forwardTarget.getLogin(), createdEvent.getSummary(), 0, SchedulingMethod.REPLY);
        acceptPartyCrasher(apiClient, constructBody(reply));
        EventData updatedEvent = eventManager.getEvent(defaultFolderId, createdEvent.getId());
        checkComment(updatedEvent, attendeeComment);
    }

    private void checkComment(EventData event, String expectedComment) {
        Attendee partyCrasher = ChronosUtils.find(event.getAttendees(), forwardTarget.getLogin());
        assertNotNull(partyCrasher);
        assertEquals(expectedComment, partyCrasher.getComment());
    }

}
