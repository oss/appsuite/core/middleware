/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
* 
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.chronos.itip.bugs;

import static com.openexchange.ajax.chronos.factory.AttendeeFactory.from;
import static com.openexchange.ajax.chronos.itip.ITipUtil.constructBody;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleChange;
import static com.openexchange.ajax.chronos.util.ChronosUtils.find;
import static com.openexchange.java.Autoboxing.L;
import static java.lang.Boolean.TRUE;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.chronos.AbstractExtendedChronosTest;
import com.openexchange.ajax.chronos.factory.EventFactory;
import com.openexchange.ajax.chronos.factory.PartStat;
import com.openexchange.ajax.chronos.itip.IMipReceiver;
import com.openexchange.ajax.framework.SerializationAware;
import com.openexchange.ajax.framework.SessionAwareClient;
import com.openexchange.ajax.framework.config.util.TestConfigurationChangeUtil;
import com.openexchange.ajax.framework.config.util.TestConfigurationChangeUtil.Scopes;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.AnalysisChange;
import com.openexchange.testing.httpclient.models.Attendee;
import com.openexchange.testing.httpclient.models.AttendeeAndAlarm;
import com.openexchange.testing.httpclient.models.ChronosCalendarResultResponse;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.modules.ChronosApi;

/**
 * 
 * {@link MWB2401Test}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class MWB2401Test extends AbstractExtendedChronosTest {

    private static final String COMMENT = "okay";

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        TestConfigurationChangeUtil.changeConfigWithOwnClient(testUser,
                                                              Map.of("com.openexchange.calendar.useIMipForInternalUsers", Boolean.TRUE.toString()),
                                                              Scopes.CONTEXT);
    }

    @Test
    public void testMailSentAfterComment() throws Exception {
        /*
         * As organizer, create event with internal user
         */
        EventData eventData = EventFactory.createSingleTwoHourEvent(testUser.getUserId(), this.getClass().getSimpleName() + "#testMailSentAfterComment");
        eventData.attendees(List.of(getUserAttendee(testUser2)));
        EventData createdEvent = eventManager.createEvent(eventData);
        assertTrue(createdEvent.getAttendees().size() == 2);
        /*
         * As attendee, receive notification, get event per API and accept with comment
         */
        new IMipReceiver(testUser2.getApiClient()).event(createdEvent).from(testUser.getLogin()).method(SchedulingMethod.REQUEST).receive();
        EventData attendeeEvent = eventManager2.getEvent(defaultFolderId2, createdEvent.getId());
        eventManager2.updateAttendee(attendeeEvent.getId(),
                                     new AttendeeAndAlarm().attendee(getUserAttendee(testUser2).partStat(PartStat.ACCEPTED.getStatus()).comment(COMMENT)), false);
        attendeeEvent = eventManager2.getEvent(defaultFolderId2, createdEvent.getId());
        checkComment(attendeeEvent.getAttendees(), COMMENT);
        /*
         * As organizer, receive reply with comment
         */
        createdEvent = eventManager.getEvent(defaultFolderId, createdEvent.getId());
        checkComment(createdEvent.getAttendees(), COMMENT);
        MailData reply = new IMipReceiver(testUser.getApiClient()).event(createdEvent).from(testUser2.getLogin()).method(SchedulingMethod.REPLY).receive();
        AnalysisChange change = assertSingleChange(chronosApi.analyze(constructBody(reply), null));
        checkComment(change.getNewEvent().getAttendees(), COMMENT);
        /*
         * As attendee, remove comment
         */
        attendeeEvent = eventManager2.getEvent(defaultFolderId2, createdEvent.getId());
        SessionAwareClient client = testUser2.getApiClient();
        client.registerSerialiser(new NullCommentSerializer());
        ChronosCalendarResultResponse updateAttendee = new ChronosApi(client).updateAttendeeBuilder()
                                                                             .withId(attendeeEvent.getId())
                                                                             .withFolder(defaultFolderId2)
                                                                             .withAttendeeAndAlarm(new AttendeeAndAlarm().attendee(from(find(attendeeEvent.getAttendees(), testUser2.getUserId()))
                                                                                                                                                                                                  .partStat(PartStat.ACCEPTED.getStatus())
                                                                                                                                                                                                  .comment(null)))
                                                                             .withExtendedEntities(TRUE)
                                                                             .withTimestamp(L(eventManager2.getLastTimeStamp()))
                                                                             .execute();
        checkResponse(updateAttendee.getError(), updateAttendee.getErrorDesc(), updateAttendee.getCategories(), updateAttendee.getData());
        eventManager2.setLastTimeStamp(updateAttendee.getTimestamp());
        attendeeEvent = eventManager2.getEvent(defaultFolderId2, createdEvent.getId());
        checkComment(attendeeEvent.getAttendees(), null);
        /*
         * As organizer, receive reply without comment
         */
        createdEvent = eventManager.getEvent(defaultFolderId, createdEvent.getId());
        checkComment(createdEvent.getAttendees(), null);
        reply = new IMipReceiver(testUser.getApiClient()).event(createdEvent).from(testUser2.getLogin()).dtstamp(attendeeEvent.getLastModified()).method(SchedulingMethod.REPLY).receive();
        change = assertSingleChange(chronosApi.analyze(constructBody(reply), null));
        checkComment(change.getNewEvent().getAttendees(), null);
    }

    /*
     * ============================== HELPERS ==============================
     */

    private void checkComment(List<Attendee> attendees, String comment) {
        Attendee attendee = find(attendees, testUser2.getUserId());
        assertNotNull(attendee);
        Assertions.assertEquals(comment, attendee.getComment());
    }

    /**
     * Creates anSerializer that will set the comment of an attendee to <code>null</code>
     * in the {@link ChronosApi#updateAttendee(String, String, Long, AttendeeAndAlarm, String, String, Boolean, String, String, Boolean, String, String, Boolean)}
     * update call.
     */
    private class NullCommentSerializer implements SerializationAware.Serialiser {

        @Override
        public Optional<String> serialize(Object obj, String contentType) throws ApiException {
            if (obj instanceof AttendeeAndAlarm aa) {
                return Optional.of(String.format("""
                    {
                    "attendee":
                    {
                        "cuType": "INDIVIDUAL",
                        "folder": "%s",
                        "partStat": "ACCEPTED",
                        "comment": null,
                        "entity": %d
                    }
                    }
                    """, aa.getAttendee().getFolder(), aa.getAttendee().getEntity()));
            }
            return Optional.empty();
        }

    }
}
