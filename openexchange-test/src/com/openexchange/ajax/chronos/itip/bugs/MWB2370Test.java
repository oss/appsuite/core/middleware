/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.chronos.itip.bugs;

import static com.openexchange.ajax.chronos.itip.ITipUtil.constructBody;
import static com.openexchange.ajax.chronos.itip.ITipUtil.receiveIMip;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleChange;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import com.openexchange.ajax.chronos.UserApi;
import com.openexchange.ajax.chronos.itip.AbstractITipAnalyzeTest;
import com.openexchange.ajax.chronos.manager.ChronosApiException;
import com.openexchange.ajax.chronos.util.DateTimeUtil;
import com.openexchange.ajax.framework.SessionAwareClient;
import com.openexchange.chronos.AttendeePrivileges;
import com.openexchange.chronos.common.CalendarUtils;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.CalendarResult;
import com.openexchange.testing.httpclient.models.ChronosCalendarResultResponse;
import com.openexchange.testing.httpclient.models.ChronosMultipleCalendarResultResponse;
import com.openexchange.testing.httpclient.models.DeleteEventBody;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.EventId;
import com.openexchange.testing.httpclient.models.EventsResponse;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.models.UpdateEventBody;
import com.openexchange.testing.httpclient.modules.ChronosApi;
import com.openexchange.testing.httpclient.modules.MailApi;

/**
 * {@link MWB2370Test}
 * 
 * "Appointment not found" error when changing appointment series as "privileged" attendee with deleted occurrence
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class MWB2370Test extends AbstractITipAnalyzeTest {

    private ChronosApi chronosApi2;
    private String defaultFolderId2;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        SessionAwareClient apiClient2 = testUser2.getApiClient();
        UserApi userApi2 = new UserApi(apiClient2, testUser2);
        chronosApi2 = userApi2.getChronosApi();
        defaultFolderId2 = getDefaultFolder(apiClient2);
    }

    @ParameterizedTest
    @ValueSource(booleans = { true, false })
    public void testRequestWithOtherExceptionView(boolean hidden) throws Exception {
        /*
         * prepare series event where user b is hidden/removed from third occurrence
         */
        EventData eventSeries2 = prepareSeriesWithOtherExceptionView(hidden);
        String recurrenceId = eventSeries2.getDeleteExceptionDates().get(0);
        /*
         * as user b, update event series and invite user c from context 2 
         */
        EventData seriesUpdate2 = new EventData().folder(eventSeries2.getFolder()).id(eventSeries2.getId())
            .attendees(com.openexchange.java.Lists.combine(eventSeries2.getAttendees(), getExternalAttendee(userResponseC2.getData())));
        ChronosCalendarResultResponse updateResponse = chronosApi2.updateEventBuilder() // @formatter:off
            .withFolder(eventSeries2.getFolder()).withId(eventSeries2.getId()).withTimestamp(eventSeries2.getTimestamp())
            .withUpdateEventBody(new UpdateEventBody().event(seriesUpdate2)).withCheckConflicts(Boolean.FALSE)
        .execute(); // @formatter:on
        checkResponse(updateResponse.getError(), updateResponse.getErrorDesc());
        /*
         * as user c in context 2, receive the invitation & apply it to the calendar
         */
        MailData iMip = receiveIMip(apiClientC2, userResponseC1.getData().getEmail1(), eventSeries2.getSummary(), -1, eventSeries2.getUid(), null, SchedulingMethod.REQUEST);
        assertSingleChange(analyze(testUserC2.getApiClient(), iMip));
        CalendarResult applyResult = applyCreate(testUserC2.getApiClient(), constructBody(iMip));
        assertTrue(null != applyResult.getCreated() && 1 == applyResult.getCreated().size());
        deleteMail(new MailApi(testUserC2.getApiClient()), iMip);
        String seriesIdC2 = applyResult.getCreated().get(0).getSeriesId();
        /*
         * as user c, check the view on the event series
         */
        ChronosApi chronosApiC2 = new ChronosApi(apiClientC2);
        getExpandedEvents(chronosApiC2, folderIdC2).forEach(e -> assertFalse(Objects.equals(seriesIdC2, e.getSeriesId()) && Objects.equals(recurrenceId, e.getRecurrenceId())));
        EventData eventSeriesC2 = chronosApiC2.getEvent(seriesIdC2, folderIdC2, null, null, null).getData();
        assertTrue(null != eventSeriesC2.getDeleteExceptionDates() && 1 == eventSeriesC2.getDeleteExceptionDates().size());
        assertTrue(null == eventSeriesC2.getChangeExceptionDates() || eventSeriesC2.getChangeExceptionDates().isEmpty());
    }
    
    @ParameterizedTest
    @ValueSource(booleans = { true, false })
    public void testRescheduleSeriesWithOtherExceptionView(boolean hidden) throws Exception {
        /*
         * prepare series event where user b is hidden/removed from third occurrence
         */
        EventData eventSeries2 = prepareSeriesWithOtherExceptionView(hidden);
        /*
         * as user b, update event series and change start/end 
         */
        EventData seriesUpdate2 = new EventData().folder(eventSeries2.getFolder()).id(eventSeries2.getId())
            .startDate(DateTimeUtil.add(DateTimeUtil.add(eventSeries2.getStartDate(), Calendar.MINUTE, 17), Calendar.SECOND, 56))
            .endDate(DateTimeUtil.add(DateTimeUtil.add(eventSeries2.getEndDate(), Calendar.MINUTE, 17), Calendar.SECOND, 56));
        ChronosCalendarResultResponse updateResponse = chronosApi2.updateEventBuilder() // @formatter:off
            .withFolder(eventSeries2.getFolder()).withId(eventSeries2.getId()).withTimestamp(eventSeries2.getTimestamp())
            .withUpdateEventBody(new UpdateEventBody().event(seriesUpdate2)).withCheckConflicts(Boolean.FALSE)
        .execute(); // @formatter:on
        checkResponse(updateResponse.getError(), updateResponse.getErrorDesc());
        EventData updatedSeries2 = chronosApi2.getEvent(eventSeries2.getId(), defaultFolderId2, null, null, null).getData();
        /*
         * as user b, check the view on the event series
         */
        List<EventData> expandedEvents2 = getExpandedEvents(chronosApi2, defaultFolderId2).stream().filter(e -> Objects.equals(updatedSeries2.getSeriesId(), e.getSeriesId())).toList();
        assertEquals(4, expandedEvents2.size());
        expandedEvents2.stream().forEach(e -> assertTrue(e.getRecurrenceId().contains("1756") && e.getStartDate().getValue().contains("1756")));
        /*
         * as user a, check the view on the event series
         */
        List<EventData> expandedEvents1 = getExpandedEvents(chronosApi, defaultFolderId).stream().filter(e -> Objects.equals(updatedSeries2.getSeriesId(), e.getSeriesId())).toList();
        assertEquals(5, expandedEvents1.size());
        expandedEvents2.stream().forEach(e -> assertTrue(e.getRecurrenceId().contains("1756")));
        assertEquals(4, expandedEvents1.stream().filter(e -> e.getStartDate().getValue().contains("1756")).toList().size());
    }

    @ParameterizedTest
    @ValueSource(booleans = { true, false })
    public void testUpdateSeriesWithOtherExceptionView(boolean hidden) throws Exception {
        /*
         * prepare series event where user b is hidden/removed from third occurrence
         */
        EventData eventSeries2 = prepareSeriesWithOtherExceptionView(hidden);
        String recurrenceId = eventSeries2.getDeleteExceptionDates().get(0);
        /*
         * as user b, update event series and change start/end 
         */
        EventData seriesUpdate2 = new EventData().folder(eventSeries2.getFolder()).id(eventSeries2.getId())
            .summary(eventSeries2.getSummary() + "_edit");
        ChronosCalendarResultResponse updateResponse = chronosApi2.updateEventBuilder() // @formatter:off
            .withFolder(eventSeries2.getFolder()).withId(eventSeries2.getId()).withTimestamp(eventSeries2.getTimestamp())
            .withUpdateEventBody(new UpdateEventBody().event(seriesUpdate2)).withCheckConflicts(Boolean.FALSE)
        .execute(); // @formatter:on
        checkResponse(updateResponse.getError(), updateResponse.getErrorDesc());
        EventData updatedSeries2 = chronosApi2.getEvent(eventSeries2.getId(), defaultFolderId2, null, null, null).getData();
        /*
         * as user b, check the view on the event series
         */
        getExpandedEvents(chronosApi2, defaultFolderId2).stream().filter(e -> Objects.equals(updatedSeries2.getSeriesId(), e.getSeriesId())).forEach(e -> {
            assertNotEquals(recurrenceId, e.getRecurrenceId());
            assertEquals(seriesUpdate2.getSummary(), e.getSummary());
        });
        /*
         * as user a, check the view on the event series
         */
        getExpandedEvents(chronosApi, defaultFolderId).stream().filter(e -> Objects.equals(updatedSeries2.getSeriesId(), e.getSeriesId())).forEach(e -> {
            if (Objects.equals(recurrenceId, e.getRecurrenceId())) {
                assertEquals(eventSeries2.getSummary(), e.getSummary());
            } else {
                assertEquals(seriesUpdate2.getSummary(), e.getSummary());
            }
        });
    }

    private EventData prepareSeriesWithOtherExceptionView(boolean hidden) throws ApiException, ChronosApiException {
        /*
         * as user a, generate event series with user b and attendee privileges MODIFY context 1
         */
        EventData eventData = prepareSeriesEventData("FREQ=DAILY;COUNT=5", defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        eventData.setAttendeePrivileges(AttendeePrivileges.MODIFY);
        EventData createdEvent = eventManager.createEvent(eventData, true);
        String recurrenceId;
        if (hidden) {
            /*
             * as user b, lookup the third occurrence of the generated event series
             */
            EventData eventOccurrence2 = null;
            int i = 0;
            for (EventData loadedEventData : getExpandedEvents(chronosApi2, defaultFolderId2)) {
                if (Objects.equals(createdEvent.getSeriesId(), loadedEventData.getSeriesId()) && null != loadedEventData.getRecurrenceId() && ++i > 2) {
                    eventOccurrence2 = loadedEventData;
                    break;
                }
            }
            assertNotNull(eventOccurrence2);
            recurrenceId = eventOccurrence2.getRecurrenceId();
            /*
             * as user b, delete the occurrence (attendee gets 'hidden' flag internally)
             */
            ChronosMultipleCalendarResultResponse deleteResponse2 = chronosApi2.deleteEventBuilder()
                .withDeleteEventBody(new DeleteEventBody().addEventsItem(new EventId().folder(eventOccurrence2.getFolder()).id(eventOccurrence2.getId()).recurrenceId(recurrenceId)))
                .withTimestamp(eventOccurrence2.getTimestamp())
            .execute();
            assertNull(deleteResponse2.getError(), deleteResponse2.getErrorDesc());
        } else {
            /*
             * as user a, lookup the third occurrence of the generated event series
             */
            EventData eventOccurrence = null;
            int i = 0;
            for (EventData loadedEventData : getExpandedEvents(chronosApi, defaultFolderId)) {
                if (Objects.equals(createdEvent.getSeriesId(), loadedEventData.getSeriesId()) && null != loadedEventData.getRecurrenceId() && ++i > 2) {
                    eventOccurrence = loadedEventData;
                    break;
                }
            }
            assertNotNull(eventOccurrence);
            recurrenceId = eventOccurrence.getRecurrenceId();
            /*
             * as user a, delete user b from the occurrence
             */
            EventData occurrenceUpdate = new EventData()
                .folder(eventOccurrence.getFolder()).id(eventOccurrence.getId()).recurrenceId(recurrenceId)
                .attendees(Collections.singletonList(getUserAttendee(testUser)))
            ; 
            eventManager.updateOccurenceEvent(occurrenceUpdate, recurrenceId, true);
        }
        /*
         * as user b, check the view on the event series
         */
        getExpandedEvents(chronosApi2, defaultFolderId2).forEach(e -> assertFalse(Objects.equals(createdEvent.getSeriesId(), e.getSeriesId()) && Objects.equals(recurrenceId, e.getRecurrenceId())));
        EventData eventSeries2 = chronosApi2.getEvent(createdEvent.getSeriesId(), defaultFolderId2, null, null, null).getData();
        assertTrue(null != eventSeries2.getDeleteExceptionDates() && 1 == eventSeries2.getDeleteExceptionDates().size());
        assertTrue(null == eventSeries2.getChangeExceptionDates() || eventSeries2.getChangeExceptionDates().isEmpty());
        return eventSeries2;
    }
    
    /**
     * Gets all events under the perspective of a certain calendar folder, expanded, in a range of +/- 1 month around <i>now</i>.
     * 
     * @param chronosApi The chronos API to use
     * @param folderId The folder identifier to lookup the events in
     * @return The events
     */
    protected static List<EventData> getExpandedEvents(ChronosApi chronosApi, String folderId) throws ApiException {
        EventsResponse eventsResponse = chronosApi.getAllEventsBuilder() // @formatter:off
            .withFolder(folderId)
            .withRangeStart(DateTimeUtil.getZuluDateTime(CalendarUtils.add(new Date(), Calendar.MONTH, -1).getTime()).getValue())
            .withRangeEnd(DateTimeUtil.getZuluDateTime(CalendarUtils.add(new Date(), Calendar.MONTH, 1).getTime()).getValue())
            .withFields("lastModified,color,createdBy,endDate,flags,folder,id,location,recurrenceId,rrule,seriesId,startDate,summary,timestamp,transp,attendeePrivileges")
            .withExpand(Boolean.TRUE)
        .execute(); // @formatter:on
        return checkResponse(eventsResponse.getError(), eventsResponse.getErrorDesc(), eventsResponse.getCategories(), eventsResponse.getData());
    }

}
