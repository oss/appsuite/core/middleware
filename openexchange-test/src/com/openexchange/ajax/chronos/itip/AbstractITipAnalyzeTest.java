/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.chronos.itip;

import static com.openexchange.ajax.chronos.itip.ITipUtil.constructBody;
import static com.openexchange.ajax.chronos.itip.ITipUtil.convertToAttendee;
import static com.openexchange.ajax.chronos.itip.ITipUtil.receiveIMip;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertAttendeePartStat;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleChange;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleEvent;
import static com.openexchange.java.Autoboxing.I;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import java.util.ArrayList;
import java.util.List;
import com.openexchange.ajax.chronos.factory.ConferenceBuilder;
import com.openexchange.ajax.chronos.factory.PartStat;
import com.openexchange.ajax.chronos.itip.assertion.ITipActionAssertion;
import com.openexchange.ajax.chronos.itip.assertion.ITipActionAssertions;
import com.openexchange.ajax.chronos.manager.EventManager;
import com.openexchange.ajax.chronos.util.ChronosUtils;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.Analysis;
import com.openexchange.testing.httpclient.models.AnalysisChangeNewEvent;
import com.openexchange.testing.httpclient.models.AnalyzeResponse;
import com.openexchange.testing.httpclient.models.Attendee;
import com.openexchange.testing.httpclient.models.CalendarUser;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.EventResponse;
import com.openexchange.testing.httpclient.models.MailData;

/**
 * {@link AbstractITipAnalyzeTest}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @since v7.10.0
 */
public abstract class AbstractITipAnalyzeTest extends AbstractITipTest {

    protected EventData createdEvent;

    protected Attendee prepareCommonAttendees(EventData event) {
        Attendee replyingAttendee = convertToAttendee(testUserC2, Integer.valueOf(0));
        replyingAttendee.setPartStat(PartStat.NEEDS_ACTION.getStatus());

        Attendee organizer = convertToAttendee(testUser, I(testUser.getUserId()));
        organizer.setPartStat(PartStat.ACCEPTED.getStatus());

        ArrayList<Attendee> attendees = new ArrayList<>(5);
        attendees.add(organizer);
        attendees.add(replyingAttendee);

        event.setAttendees(attendees);
        CalendarUser c = new CalendarUser();
        c.cn(userResponseC1.getData().getDisplayName());
        c.email(userResponseC1.getData().getEmail1());
        c.entity(Integer.valueOf(userResponseC1.getData().getId()));
        event.setOrganizer(c);
        event.setCalendarUser(c);

        return replyingAttendee;
    }

    protected EventData prepareAttendeeConference(EventData eventData) {
        ConferenceBuilder builder = ConferenceBuilder.newBuilder() //@formatter:off
            .setDefaultFeatures()
            .setAttendeeLable()
            .setVideoChatUri()
            .setGroupId();
        eventData.addConferencesItem(builder.build()); //@formatter:on
        return eventData;
    }

    protected EventData prepareModeratorConference(EventData eventData) {
        ConferenceBuilder builder = ConferenceBuilder.newBuilder() //@formatter:off
            .setDefaultFeatures()
            .setModeratorLable()
            .setVideoChatUri();
        eventData.addConferencesItem(builder.build()); //@formatter:on
        return eventData;
    }

    /**
     * Get all events of of a series
     * <p>
     * Note: The {@link #createdEvent} must be a series
     *
     * @return All events of the series
     * @throws ApiException
     */
    protected List<EventData> getAllEventsOfCreatedEvent() throws ApiException {
        return getAllEventsOfEvent(eventManager, createdEvent);
    }

    /**
     * Get all events of of a series
     * <p>
     * Note: The {@link #createdEvent} must be a series
     * 
     * @param eventManager The manager to user
     * @param event The event to get the UID from
     * @return All events of the series
     * @throws ApiException
     */
    protected List<EventData> getAllEventsOfEvent(EventManager eventManager, EventData event) throws ApiException {
        assertNotNull(event);
        return ChronosUtils.getAllEventsOfEvent(eventManager, event.getUid());
    }

    protected void analyze(String mailId) throws Exception {
        analyze(mailId, ITipActionAssertions.APPLY);
    }

    protected void analyze(String mailId, ITipActionAssertion actionAssertion) throws Exception {
        AnalyzeResponse response = analyze(ITipUtil.constructBody(mailId));
        assertThat("Found error: " + response.getErrorDesc(), response.getError(), nullValue());
        assertThat("Should have no error", response.getErrorId(), nullValue());

        assertAnalyzeActions(response, actionAssertion);
    }

    protected void assertAnalyzeActions(AnalyzeResponse response, ITipActionAssertion actionAssertion) {
        List<Analysis> data = checkResponse(response.getError(), response.getErrorDesc(), response.getCategories(), response.getData());
        if (null == actionAssertion) {
            ITipActionAssertions.ACTIONS.validate(data);
        } else {
            actionAssertion.validate(data);
        }
    }

    /**
     * Creates an event and afterwards applies a REPLY of an attendee.
     * <p>
     * The created event is save into the member <code>createdEvent</code>.
     * 
     * @param replyingAttendee The attendee that replies by accepting the event
     * @param comment The comment the attendee sets
     * @return The created event as seen by the attendee.
     * @throws ApiException In case of error
     * @throws Exception In case of error
     */
    protected EventData createAndApplyAccept(Attendee replyingAttendee, String comment) throws ApiException, Exception {
        /*
         * Receive mail as attendee
         */
        MailData iMip = receiveIMip(apiClientC2, userResponseC1.getData().getEmail1(), createdEvent.getSummary(), 0, SchedulingMethod.REQUEST);
        AnalysisChangeNewEvent newEvent = assertSingleChange(analyze(apiClientC2, iMip)).getNewEvent();
        assertNotNull(newEvent);
        assertEquals(createdEvent.getUid(), newEvent.getUid());
        assertAttendeePartStat(newEvent.getAttendees(), replyingAttendee.getEmail(), PartStat.NEEDS_ACTION);
        /*
         * Reply with "accepted"
         */
        EventData attendeeEvent = assertSingleEvent(accept(apiClientC2, constructBody(iMip), comment), createdEvent.getUid());
        assertAttendeePartStat(attendeeEvent.getAttendees(), replyingAttendee.getEmail(), PartStat.ACCEPTED);
        /*
         * Receive mail as organizer and check actions
         */
        MailData reply = receiveIMip(apiClient, replyingAttendee.getEmail(), createdEvent.getSummary(), 0, SchedulingMethod.REPLY);
        analyze(reply.getId());
        /*
         * Apply change as organizer via iTIP API
         */
        assertSingleEvent(applyResponse(apiClient, constructBody(reply)), createdEvent.getUid());
        EventResponse eventResponse = chronosApi.getEvent(createdEvent.getId(), createdEvent.getFolder(), createdEvent.getRecurrenceId(), null, null);
        assertNull(eventResponse.getError(), eventResponse.getError());
        createdEvent = eventResponse.getData();
        for (Attendee attendee : createdEvent.getAttendees()) {
            PartStat.ACCEPTED.assertStatus(attendee);
        }
        return attendeeEvent;
    }
}
