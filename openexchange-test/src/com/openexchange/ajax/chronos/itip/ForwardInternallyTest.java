/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.chronos.itip;

import static com.openexchange.ajax.chronos.itip.ITipUtil.constructBody;
import static com.openexchange.ajax.chronos.itip.ITipUtil.receiveIMip;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertAttendeePartStat;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertChanges;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertEvents;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertExternalOrganizerAndAttendees;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertInternalOrganizerAndAttendees;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleChange;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleEvent;
import static com.openexchange.ajax.chronos.itip.assertion.ITipAssertion.assertSingleEventAndMaster;
import static com.openexchange.ajax.chronos.util.ChronosUtils.forwardEvent;
import static com.openexchange.ajax.chronos.util.ChronosUtils.loadEventData;
import static com.openexchange.ajax.chronos.util.ChronosUtils.reloadEventData;
import static com.openexchange.chronos.common.CalendarUtils.decode;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import com.google.gson.Gson;
import com.openexchange.ajax.chronos.factory.AttendeeFactory;
import com.openexchange.ajax.chronos.factory.PartStat;
import com.openexchange.ajax.chronos.util.DateTimeUtil;
import com.openexchange.chronos.ParticipationStatus;
import com.openexchange.chronos.common.DefaultRecurrenceId;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.invoker.ApiClient;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.Analysis;
import com.openexchange.testing.httpclient.models.Analysis.ActionsEnum;
import com.openexchange.testing.httpclient.models.AnalysisChange;
import com.openexchange.testing.httpclient.models.AnalyzeResponse;
import com.openexchange.testing.httpclient.models.Attendee;
import com.openexchange.testing.httpclient.models.Attendee.CuTypeEnum;
import com.openexchange.testing.httpclient.models.AttendeeAndAlarm;
import com.openexchange.testing.httpclient.models.CalendarResult;
import com.openexchange.testing.httpclient.models.ChronosCalendarResultResponse;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.models.MailReplyData;
import com.openexchange.testing.httpclient.models.MailReplyResponse;
import com.openexchange.testing.httpclient.models.UpdateEventBody;
import com.openexchange.testing.httpclient.models.UserData;
import com.openexchange.testing.httpclient.modules.ChronosApi;
import com.openexchange.testing.httpclient.modules.MailApi;
import com.openexchange.testing.httpclient.modules.UserApi;

/**
 * {@link ForwardInternallyTest}
 * 
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
@SuppressWarnings("unused")
public class ForwardInternallyTest extends AbstractITipAnalyzeTest {

    private UserData user1;
    private UserData user2;
    private UserData user3;
    private TestUser testUser3;
    private String defaultFolderId2;
    private String defaultFolderId3;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        setUpConfiguration();
        user1 = userResponseC1.getData();
        user2 = new UserApi(testUser2.getApiClient()).getUser(String.valueOf(testUser2.getUserId())).getData();
        testUser3 = testContext.acquireUser();
        defaultFolderId2 = getDefaultFolder(testUser2.getApiClient());
        defaultFolderId3 = getDefaultFolder(testUser3.getApiClient());
        user3 = new UserApi(testUser3.getApiClient()).getUser(String.valueOf(testUser3.getUserId())).getData();
    }

    @Override
    protected Map<String, String> getNeededConfigurations() {
        return Collections.singletonMap("com.openexchange.calendar.useIMipForInternalUsers", Boolean.TRUE.toString());
    }

    @Override
    protected String getScope() {
        return "context";
    }

    @ParameterizedTest
    @CsvSource({ "true, true", "true, false", "false, true", "false, false" })
    public void testForwardedEvent(boolean forwardMail, boolean applyOnly) throws Exception {
        /*
         * as user 1, create event and add user 2
         */
        EventData eventData = prepareEventData(defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * test forwarding from user 2 to user 3
         */
        testForwardedInvitation(createdEvent1, forwardMail, applyOnly, true);
    }

    @ParameterizedTest
    @CsvSource({ "true, true", "true, false", "false, true", "false, false" })
    public void testForwardedSeries(boolean forwardMail, boolean applyOnly) throws Exception {
        /*
         * as user 1, create event series and add user 2
         */
        EventData eventData = prepareSeriesEventData("FREQ=DAILY;COUNT=7", defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * test forwarding from user 2 to user 3
         */
        testForwardedInvitation(createdEvent1, forwardMail, applyOnly, true);
    }

    @ParameterizedTest
    @CsvSource({ "true, true", "true, false", "false, true", "false, false" })
    public void testForwardedOrphanedInstance(boolean forwardMail, boolean applyOnly) throws Exception {
        /*
         * as user 1, create event series w/o other attendees
         */
        EventData eventData = prepareSeriesEventData("FREQ=DAILY;COUNT=7", defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * as user 1, add user 2 to a single occurrence
         */
        EventData eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent1.getSeriesId(), 3);
        EventData occurrenceUpdate1 = new EventData()
            .folder(eventOccurrence1.getFolder()).id(eventOccurrence1.getId()).recurrenceId(eventOccurrence1.getRecurrenceId())
            .attendees(Arrays.asList(getUserAttendee(testUser), getUserAttendee(testUser2)));
        EventData updatedOccurrence1 = eventManager.updateOccurenceEvent(occurrenceUpdate1, occurrenceUpdate1.getRecurrenceId(), true);
        /*
         * test forwarding from user 2 to user 3
         */
        testForwardedInvitation(updatedOccurrence1, forwardMail, applyOnly, false);
    }

    @ParameterizedTest
    @ValueSource(booleans = { true, false })
    public void testForwardedOccurrence(boolean applyOnly) throws Exception {
        /*
         * as user 1, create event series and add user 2
         */
        EventData eventData = prepareSeriesEventData("FREQ=DAILY;COUNT=7", defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * as user 1, get one occurrence of the event series
         */
        EventData eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent1.getSeriesId(), 4);
        eventOccurrence1 = reloadEventData(testUser, eventOccurrence1, false);
        /*
         * test forwarding from user 2 to user 3
         */
        testForwardedInvitation(eventOccurrence1, false, applyOnly, true);
    }

    @ParameterizedTest
    @CsvSource({ "true, true", "true, false", "false, true", "false, false" })
    public void testForwardedException(boolean forwardMail, boolean applyOnly) throws Exception {
        /*
         * as user 1, create event series and add user 2
         */
        EventData eventData = prepareSeriesEventData("FREQ=DAILY;COUNT=7", defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * as user 1, re-schedule an occurrence
         */
        EventData eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent1.getSeriesId(), 3);
        EventData occurrenceUpdate1 = new EventData()
            .folder(eventOccurrence1.getFolder()).id(eventOccurrence1.getId()).recurrenceId(eventOccurrence1.getRecurrenceId())
            .startDate(DateTimeUtil.add(eventOccurrence1.getStartDate(), Calendar.HOUR, 1))
            .endDate(DateTimeUtil.add(eventOccurrence1.getEndDate(), Calendar.HOUR, 1));
        EventData updatedOccurrence1 = eventManager.updateOccurenceEvent(occurrenceUpdate1, occurrenceUpdate1.getRecurrenceId(), true);
        /*
         * test forwarding from user 2 to user 3
         */
        testForwardedInvitation(updatedOccurrence1, forwardMail, applyOnly, true);
    }

    @Test
    public void testForwardedSeriesWithPropagatedException() throws Exception {
        /*
         * as user 1, create event series and add user 2
         */
        EventData eventData = prepareSeriesEventData("FREQ=DAILY;COUNT=7", defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * as user 1, re-schedule an occurrence (attendee line-up remains constant, so propagation of master changes is possible)
         */
        EventData eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent1.getSeriesId(), 3);
        EventData occurrenceUpdate1 = new EventData()
            .folder(eventOccurrence1.getFolder()).id(eventOccurrence1.getId()).recurrenceId(eventOccurrence1.getRecurrenceId())
            .startDate(DateTimeUtil.add(eventOccurrence1.getStartDate(), Calendar.HOUR, 1))
            .endDate(DateTimeUtil.add(eventOccurrence1.getEndDate(), Calendar.HOUR, 1));
        EventData updatedOccurrence1 = eventManager.updateOccurenceEvent(occurrenceUpdate1, occurrenceUpdate1.getRecurrenceId(), true);
        /*
         * as user 2, lookup appointment series in the calendar and forward it using the API
         */
        EventData reloadedEvent2 = loadEventData(testUser2, defaultFolderId2, createdEvent1.getId(), createdEvent1.getRecurrenceId(), false);
        forwardEvent(testUser2.getApiClient(), reloadedEvent2, user3.getEmail1());
        /*
         * as user 3, receive & check forwarded mail, then 'accept' the invitation & check imported events
         */
        MailData iMip3 = lookupIMip(testUser3, user2.getEmail1(), SchedulingMethod.REQUEST, createdEvent1);
        AnalysisChange seriesChange3 = assertChanges(analyze(testUser3.getApiClient(), iMip3), 2, 0);
        for (EventData importedEvent3 : assertEvents(accept(testUser3.getApiClient(), constructBody(iMip3), null), createdEvent1.getUid(), 2)) {
            assertAttendeePartStat(importedEvent3.getAttendees(), user3.getEmail1(), PartStat.ACCEPTED);
            assertExternalOrganizerAndAttendees(importedEvent3, user1, new UserData[] { user3 }, new UserData[] { user1, user2 });
            EventData reloadedEvent3 = reloadEventData(testUser3, importedEvent3, false);
            assertExternalOrganizerAndAttendees(reloadedEvent3, user1, new UserData[] { user3 }, new UserData[] { user1, user2 });
        }
        /*
         * as user 1, receive unsolicited reply from user 3 & 'accept' the party crasher, then check resulting events
         */
        MailData iMip1 = lookupIMip(testUser, user3.getEmail1(), SchedulingMethod.REPLY, createdEvent1);
        AnalysisChange seriesChange1 = assertChanges(analyze(testUser.getApiClient(), iMip1), 2, 0);
        for (EventData updatedEvent1 : assertEvents(acceptPartyCrasher(testUser.getApiClient(), constructBody(iMip1)), createdEvent1.getUid(), 2)) {
            assertInternalOrganizerAndAttendees(updatedEvent1, user1, new UserData[] { user1, user2, user3 }, null);
            assertAttendeePartStat(updatedEvent1.getAttendees(), user3.getEmail1(), PartStat.ACCEPTED);
            EventData reloadedEvent1 = loadEventData(testUser, defaultFolderId, updatedEvent1.getId(), updatedEvent1.getRecurrenceId(), false);
            assertInternalOrganizerAndAttendees(reloadedEvent1, user1, new UserData[] { user1, user2, user3 }, null);
            reloadedEvent2 = loadEventData(testUser2, defaultFolderId2, updatedEvent1.getId(), updatedEvent1.getRecurrenceId(), false);
            assertInternalOrganizerAndAttendees(reloadedEvent2, user1, new UserData[] { user1, user2, user3 }, null);
            EventData reloadedEvent3 = loadEventData(testUser3, defaultFolderId3, updatedEvent1.getId(), updatedEvent1.getRecurrenceId(), false);
            assertInternalOrganizerAndAttendees(reloadedEvent3, user1, new UserData[] { user1, user2, user3 }, null);
        }
    }

    @Test
    public void testForwardedSeriesWithException() throws Exception {
        /*
         * as user 1, create event series and add user 2
         */
        EventData eventData = prepareSeriesEventData("FREQ=DAILY;COUNT=7", defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * as user 1, re-schedule an occurrence & change attendee line-up (so that propagation of master changes won't happen)
         */
        UserData externalUserData = new UserData().email1("test@example.org");
        EventData eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent1.getSeriesId(), 3);
        EventData occurrenceUpdate1 = new EventData()
            .folder(eventOccurrence1.getFolder()).id(eventOccurrence1.getId()).recurrenceId(eventOccurrence1.getRecurrenceId())
            .attendees(Arrays.asList(getUserAttendee(testUser), getUserAttendee(testUser2), AttendeeFactory.createIndividual(externalUserData.getEmail1())))
            .startDate(DateTimeUtil.add(eventOccurrence1.getStartDate(), Calendar.HOUR, 1))
            .endDate(DateTimeUtil.add(eventOccurrence1.getEndDate(), Calendar.HOUR, 1));
        EventData updatedOccurrence1 = eventManager.updateOccurenceEvent(occurrenceUpdate1, occurrenceUpdate1.getRecurrenceId(), true);
        /*
         * as user 2, lookup appointment series in the calendar and forward it using the API
         */
        EventData reloadedEvent2 = loadEventData(testUser2, defaultFolderId2, createdEvent1.getId(), createdEvent1.getRecurrenceId(), false);
        forwardEvent(testUser2.getApiClient(), reloadedEvent2, user3.getEmail1());
        /*
         * as user 3, receive & check forwarded mail, then 'accept' the invitation & check imported events
         */
        MailData iMip3 = lookupIMip(testUser3, user2.getEmail1(), SchedulingMethod.REQUEST, createdEvent1);
        AnalysisChange seriesChange3 = assertChanges(analyze(testUser3.getApiClient(), iMip3), 2, 0);
        for (EventData importedEvent3 : assertEvents(accept(testUser3.getApiClient(), constructBody(iMip3), null), createdEvent1.getUid(), 2)) {
            assertAttendeePartStat(importedEvent3.getAttendees(), user3.getEmail1(), PartStat.ACCEPTED);
            UserData[] expectedExternalAttendees = Objects.equals(updatedOccurrence1.getRecurrenceId(), importedEvent3.getRecurrenceId()) ? 
                new UserData[] { user1, user2, externalUserData } : new UserData[] { user1, user2 };
            assertExternalOrganizerAndAttendees(importedEvent3, user1, new UserData[] { user3 }, expectedExternalAttendees);
            EventData reloadedEvent3 = reloadEventData(testUser3, importedEvent3, false);
            assertExternalOrganizerAndAttendees(reloadedEvent3, user1, new UserData[] { user3 }, expectedExternalAttendees);
        }
        /*
         * as user 1, receive unsolicited reply from user 3 & 'accept' the party crasher, then check resulting events
         */
        MailData iMip1 = lookupIMip(testUser, user3.getEmail1(), SchedulingMethod.REPLY, createdEvent1);
        AnalysisChange seriesChange1 = assertChanges(analyze(testUser.getApiClient(), iMip1), 2, 0);
        for (EventData updatedEvent1 : assertEvents(acceptPartyCrasher(testUser.getApiClient(), constructBody(iMip1)), createdEvent1.getUid(), 2)) {
            assertAttendeePartStat(updatedEvent1.getAttendees(), user3.getEmail1(), PartStat.ACCEPTED);
            UserData[] expectedExternalAttendees = Objects.equals(updatedOccurrence1.getRecurrenceId(), updatedEvent1.getRecurrenceId()) ? 
                new UserData[] { externalUserData } : null;            
            assertInternalOrganizerAndAttendees(updatedEvent1, user1, new UserData[] { user1, user2, user3 }, expectedExternalAttendees);
            EventData reloadedEvent1 = loadEventData(testUser, defaultFolderId, updatedEvent1.getId(), updatedEvent1.getRecurrenceId(), false);
            assertInternalOrganizerAndAttendees(reloadedEvent1, user1, new UserData[] { user1, user2, user3 }, expectedExternalAttendees);
            reloadedEvent2 = loadEventData(testUser2, defaultFolderId2, updatedEvent1.getId(), updatedEvent1.getRecurrenceId(), false);
            assertInternalOrganizerAndAttendees(reloadedEvent2, user1, new UserData[] { user1, user2, user3 }, expectedExternalAttendees);
            EventData reloadedEvent3 = loadEventData(testUser3, defaultFolderId3, updatedEvent1.getId(), updatedEvent1.getRecurrenceId(), false);
            assertInternalOrganizerAndAttendees(reloadedEvent3, user1, new UserData[] { user1, user2, user3 }, expectedExternalAttendees);
        }
    }

    @Test
    public void testForwardedSeriesWithoutException() throws Exception {
        /*
         * as user 1, create event series and add user 2, lookup received mail
         */
        EventData eventData = prepareSeriesEventData("FREQ=DAILY;COUNT=7", defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        MailData iMip2 = lookupIMip(testUser2, user1.getEmail1(), SchedulingMethod.REQUEST, createdEvent1);
        assertSingleChange(analyze(testUser2.getApiClient(), iMip2));
        /*
         * as user 1, re-schedule an occurrence
         */
        EventData eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent1.getSeriesId(), 3);
        EventData occurrenceUpdate1 = new EventData()
            .folder(eventOccurrence1.getFolder()).id(eventOccurrence1.getId()).recurrenceId(eventOccurrence1.getRecurrenceId())
            .startDate(DateTimeUtil.add(eventOccurrence1.getStartDate(), Calendar.HOUR, 1))
            .endDate(DateTimeUtil.add(eventOccurrence1.getEndDate(), Calendar.HOUR, 1));
        EventData updatedOccurrence1 = eventManager.updateOccurenceEvent(occurrenceUpdate1, occurrenceUpdate1.getRecurrenceId(), true);
        /*
         * as user 2, forward received mail for event series to user 3
         */
        forwardMail(testUser2.getApiClient(), iMip2, user3.getEmail1());
        /*
         * as user 3, receive & check forwarded mail, then 'accept' the invitation
         */
        MailData iMip3 = lookupIMip(testUser3, user2.getEmail1(), SchedulingMethod.REQUEST, createdEvent1);
        assertSingleChange(analyze(testUser3.getApiClient(), iMip3));
        EventData importedEvent3 = assertSingleEvent(accept(testUser3.getApiClient(), constructBody(iMip3), null), createdEvent1.getUid());
        assertAttendeePartStat(importedEvent3.getAttendees(), user3.getEmail1(), PartStat.ACCEPTED);
        assertExternalOrganizerAndAttendees(importedEvent3, user1, new UserData[] { user3 }, new UserData[] { user1, user2 });
        /*
         * lookup event in user 3's calendar & check stored data
         */
        EventData reloadedEvent3 = reloadEventData(testUser3, importedEvent3, false);
        assertExternalOrganizerAndAttendees(reloadedEvent3, user1, new UserData[] { user3 }, new UserData[] { user1, user2 });
        /*
         * as user 1, receive unsolicited reply from user 3 & 'accept' the party crasher, then check resulting events
         */
        MailData iMip1 = lookupIMip(testUser, user3.getEmail1(), SchedulingMethod.REPLY, createdEvent1);
        assertSingleChange(analyze(testUser.getApiClient(), iMip1));
        for (EventData updatedEvent1 : assertEvents(acceptPartyCrasher(testUser.getApiClient(), constructBody(iMip1)), createdEvent1.getUid(), 2)) {
            assertInternalOrganizerAndAttendees(updatedEvent1, user1, new UserData[] { user1, user2, user3 }, null);
            assertAttendeePartStat(updatedEvent1.getAttendees(), user3.getEmail1(), PartStat.ACCEPTED);
            EventData reloadedEvent1 = loadEventData(testUser, defaultFolderId, updatedEvent1.getId(), updatedEvent1.getRecurrenceId(), false);
            assertInternalOrganizerAndAttendees(reloadedEvent1, user1, new UserData[] { user1, user2, user3 }, null);
            EventData reloadedEvent2 = loadEventData(testUser2, defaultFolderId2, updatedEvent1.getId(), updatedEvent1.getRecurrenceId(), false);
            assertInternalOrganizerAndAttendees(reloadedEvent2, user1, new UserData[] { user1, user2, user3 }, null);
            reloadedEvent3 = loadEventData(testUser3, defaultFolderId3, updatedEvent1.getId(), updatedEvent1.getRecurrenceId(), false);
            assertInternalOrganizerAndAttendees(reloadedEvent3, user1, new UserData[] { user1, user2, user3 }, null);
        }
    }

    @ParameterizedTest
    @ValueSource(booleans = { true, false })
    public void testMultipleOccurrences(boolean applyOnly) throws Exception {
        /*
         * as user 1, create event series and add user 2
         */
        EventData eventData = prepareSeriesEventData("FREQ=DAILY;COUNT=7", defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * as user 1, get one occurrence of the event series
         */
        EventData eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent1.getSeriesId(), 4);
        eventOccurrence1 = reloadEventData(testUser, eventOccurrence1, false);
        /*
         * test forwarding from user 2 to user 3
         */
        testForwardedInvitation(eventOccurrence1, false, applyOnly, false);
        /*
         * as user 1, get another occurrence of the event series
         */
        eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent1.getSeriesId(), 6);
        eventOccurrence1 = reloadEventData(testUser, eventOccurrence1, false);
        /*
         * test forwarding from user 2 to user 3
         */
        testForwardedInvitation(eventOccurrence1, false, applyOnly, true);
    }

    @Test
    public void testApplyMultipleOccurrences() throws Exception {
        /*
         * as user 1, create event series and add user 2
         */
        EventData eventData = prepareSeriesEventData("FREQ=DAILY;COUNT=7", defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * as user 1, get one occurrence of the event series
         */
        EventData eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent1.getSeriesId(), 4);
        eventOccurrence1 = reloadEventData(testUser, eventOccurrence1, false);
        /*
         * test forwarding from user 2 to user 3
         */
        EventData eventOccurrence3 = testForwardedInvitation(eventOccurrence1, false, true, false);
        /*
         * as user 1, get another occurrence of the event series
         */
        eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent1.getSeriesId(), 6);
        eventOccurrence1 = reloadEventData(testUser, eventOccurrence1, false);
        /*
         * test forwarding from user 2 to user 3
         */
        testForwardedInvitation(eventOccurrence1, false, true, true);
        /*
         * verify that there are two occurrences visible from the same series for user 3
         */
        assertEquals(2, getExpandedEvents(testUser3, defaultFolderId3).stream().filter(e -> Objects.equals(eventOccurrence3.getSeriesId(), e.getSeriesId())).count());
    }

    @Test
    public void testOutdatedReply() throws Exception {
        /*
         * as user 1, create event and add user 2
         */
        EventData eventData = prepareEventData(defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * forward from user 2 to user 3 & take over the data into user 3's calendar
         */
        EventData appliedEvent3 = testForwardedInvitation(createdEvent1, true, true, false);
        /*
         * as user 1, re-schedule the event 
         */
        EventData eventUpdate1 = new EventData()
            .folder(createdEvent1.getFolder()).id(createdEvent1.getId()).recurrenceId(createdEvent1.getRecurrenceId())
            .startDate(DateTimeUtil.add(createdEvent1.getStartDate(), Calendar.DATE, 1))
            .endDate(DateTimeUtil.add(createdEvent1.getEndDate(), Calendar.DATE, 1));
        EventData updatedEvent1 = eventManager.updateEvent(eventUpdate1);
        /*
         * as user 3, 'accept' the forwarded mail
         */
        ChronosCalendarResultResponse updateAttendeeResponse3 = new ChronosApi(testUser3.getApiClient()).updateAttendeeBuilder()
            .withFolder(appliedEvent3.getFolder()).withId(appliedEvent3.getId()).withRecurrenceId(appliedEvent3.getRecurrenceId()).withTimestamp(appliedEvent3.getTimestamp())
            .withAttendeeAndAlarm(new AttendeeAndAlarm().attendee(new Attendee().entity(I(testUser3.getUserId())).cuType(CuTypeEnum.INDIVIDUAL).partStat("ACCEPTED")))
        .execute();
        EventData acceptedEvent3 = assertSingleEvent(updateAttendeeResponse3.getData());
        /*
         * as user 1, receive unsolicited reply from user 3 & verify that the reply is outdated
         */
        MailData iMip1 = receiveIMip(testUser.getApiClient(), user3.getEmail1(), createdEvent1.getSummary(), i(createdEvent1.getSequence()), SchedulingMethod.REPLY);
        AnalyzeResponse analyzeResponse1 = analyze(testUser.getApiClient(), iMip1);
        assertTrue(null != analyzeResponse1.getData() && 1 == analyzeResponse1.getData().size() && null != analyzeResponse1.getData().get(0));
        Analysis analysis1 = analyzeResponse1.getData().get(0);
        assertTrue(null != analysis1.getActions() && 1 == analysis1.getActions().size() && ActionsEnum.IGNORE.equals(analysis1.getActions().get(0)));
    }

    @Test
    public void testApplyOutdatedReply() throws Exception {
        /*
         * as user 1, create event and add user 2
         */
        EventData eventData = prepareEventData(defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * forward from user 2 to user 3 & take over the data into user 3's calendar
         */
        EventData appliedEvent3 = testForwardedInvitation(createdEvent1, true, true, false);
        /*
         * as user 3, 'accept' the initial forwarded mail
         */
        ChronosCalendarResultResponse updateAttendeeResponse3 = new ChronosApi(testUser3.getApiClient()).updateAttendeeBuilder()
            .withFolder(appliedEvent3.getFolder()).withId(appliedEvent3.getId()).withRecurrenceId(appliedEvent3.getRecurrenceId()).withTimestamp(appliedEvent3.getTimestamp())
            .withAttendeeAndAlarm(new AttendeeAndAlarm().attendee(new Attendee().entity(I(testUser3.getUserId())).cuType(CuTypeEnum.INDIVIDUAL).partStat("ACCEPTED")))
        .execute();
        EventData acceptedEvent3 = assertSingleEvent(updateAttendeeResponse3.getData());
        EventData reloadedEvent3 = reloadEventData(testUser3, acceptedEvent3, false);
        /*
         * as user 1, receive & verify unsolicited reply from user 3
         */
        MailData iMip1 = lookupIMip(testUser, user3.getEmail1(), SchedulingMethod.REPLY, createdEvent1);
        assertSingleChange(analyze(testUser.getApiClient(), iMip1));
        /*
         * as user 3, send another 'tentative' reply
         */
        updateAttendeeResponse3 = new ChronosApi(testUser3.getApiClient()).updateAttendeeBuilder()
            .withFolder(reloadedEvent3.getFolder()).withId(reloadedEvent3.getId()).withRecurrenceId(reloadedEvent3.getRecurrenceId()).withTimestamp(reloadedEvent3.getTimestamp())
            .withAttendeeAndAlarm(new AttendeeAndAlarm().attendee(new Attendee().entity(I(testUser3.getUserId())).cuType(CuTypeEnum.INDIVIDUAL).partStat("TENTATIVE")))
        .execute();
        EventData tentativeEvent3 = assertSingleEvent(updateAttendeeResponse3.getData());
        /*
         * as user 1, try and 'accept' the initial (now outdated) reply from the party crasher, expecting an error response
         */
        ChronosCalendarResultResponse acceptResponse1 = chronosApi.acceptPartyCrasher(constructBody(iMip1), null);
        assertEquals("CAL-4121", acceptResponse1.getCode());
        /*
         * as user 1, delete outdated reply mail, then receive the updated reply from user 3
         */
        deleteMail(new MailApi(testUser.getApiClient()), iMip1);
        iMip1 = lookupIMip(testUser, user3.getEmail1(), SchedulingMethod.REPLY, createdEvent1);
        assertSingleChange(analyze(testUser.getApiClient(), iMip1));
        /*
         * as user 1, 'accept' the party crasher, then check resulting events
         */
        CalendarResult acceptResult1 = acceptPartyCrasher(testUser.getApiClient(), constructBody(iMip1));
        EventData updatedEvent1 = null == createdEvent1.getSeriesId() || createdEvent1.getSeriesId().equals(createdEvent1.getId()) && null == createdEvent1.getRecurrenceId() ? 
            assertSingleEvent(acceptResult1, createdEvent1.getUid()) : assertSingleEventAndMaster(acceptResult1, createdEvent1.getUid()); 
        assertInternalOrganizerAndAttendees(updatedEvent1, user1, new UserData[] { user1, user2, user3 }, null);
        assertAttendeePartStat(updatedEvent1.getAttendees(), user3.getEmail1(), PartStat.TENTATIVE);
        /*
         * verify the previous attendee copy was removed from user 3's calendar
         */
        reloadEventData(testUser3.getApiClient(), reloadedEvent3, true);
        /*
         * reload & verify single event copy in calendars of all users
         */
        EventData reloadedEvent1 = loadEventData(testUser, defaultFolderId, createdEvent1.getId(), createdEvent1.getRecurrenceId(), false);
        assertInternalOrganizerAndAttendees(reloadedEvent1, user1, new UserData[] { user1, user2, user3 }, null);
        EventData reloadedEvent2 = loadEventData(testUser2, defaultFolderId2, createdEvent1.getId(), createdEvent1.getRecurrenceId(), false);
        assertInternalOrganizerAndAttendees(reloadedEvent2, user1, new UserData[] { user1, user2, user3 }, null);
        reloadedEvent3 = loadEventData(testUser3, defaultFolderId3, createdEvent1.getId(), createdEvent1.getRecurrenceId(), false);
        assertInternalOrganizerAndAttendees(reloadedEvent3, user1, new UserData[] { user1, user2, user3 }, null);
    }

    @Test
    public void testAddAttendeeAfterForward() throws Exception {
        /*
         * as user 1, create event and add user 2
         */
        EventData eventData = prepareEventData(defaultFolderId, getCalendarUser(testUser), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent1 = eventManager.createEvent(eventData, true);
        /*
         * forward from user 2 to user 3 & take over the data into user 3's calendar
         */
        EventData appliedEvent3 = testForwardedInvitation(createdEvent1, true, true, true);
        /*
         * as user 1, update event and try to add user 3 as attendee, expecting a conflict error
         */
        createdEvent1 = eventManager.getEvent(defaultFolderId, createdEvent1.getId());
        ChronosCalendarResultResponse updateResponse1 = chronosApi.updateEventBuilder()
            .withFolder(createdEvent1.getFolder()).withId(createdEvent1.getId()).withRecurrenceId(createdEvent1.getRecurrenceId())
            .withTimestamp(createdEvent1.getTimestamp()).withUpdateEventBody(new UpdateEventBody().event(
                new EventData().attendees(Arrays.asList(getUserAttendee(testUser), getUserAttendee(testUser2), getUserAttendee(testUser3)))))
        .execute();
        assertEquals("CAL-4093", updateResponse1.getCode());
    }
    
    /**
     * Tests the forward request / accept / add to calendar / add party crasher workflow, assuming that
     * <ul>
     * <li>{@link #testUser}</li> is the event's organizer
     * <li>{@link #testUser2}</li> is attendee in the event
     * <li>{@link #testUser3}</li> will be the recipient of the forwarded event
     * </ul>
     * 
     * @param createdEvent1 The event to forward, as seen by the organizer ({@link #testUser})
     * @param applyOnly <code>true</code> if the forwarded request should only be added to the recipient's calendar, <code>false</code> to do a reply to the organizer
     * @param forwardMail <code>true</code> to forward the email directly, <code>false</code> to use the chronos API to forward the event
     * @return The event data as seen by {@link testUser3}
     */
    private EventData testForwardedInvitation(EventData createdEvent1, boolean forwardMail, boolean applyOnly, boolean accept) throws Exception {
        if (forwardMail) {
            /*
             * as user 2, receive imip & forward received mail to user 3
             */
            MailData iMip2 = lookupIMip(testUser2, user1.getEmail1(), SchedulingMethod.REQUEST, createdEvent1);
            assertSingleChange(analyze(testUser2.getApiClient(), iMip2));
            forwardMail(testUser2.getApiClient(), iMip2, user3.getEmail1());
        } else {
            /*
             * as user 2, lookup appointment in the calendar and forward it using the API
             */
            EventData reloadedEvent2 = loadEventData(testUser2, defaultFolderId2, createdEvent1.getId(), createdEvent1.getRecurrenceId(), false);
            forwardEvent(testUser2.getApiClient(), reloadedEvent2, user3.getEmail1());
        }
        /*
         * as user 2, update the participation status, so that the master copy differs
         */
        if (accept) {
            acceptAs(testUser2, createdEvent1, defaultFolderId2);
        }
        /*
         * as user 3, receive & check forwarded mail, then either 'apply' or 'accept' the invitation
         */
        MailData iMip3 = lookupIMip(testUser3, user2.getEmail1(), SchedulingMethod.REQUEST, createdEvent1);
        assertSingleChange(analyze(testUser3.getApiClient(), iMip3));
        EventData importedEvent3;
        if (applyOnly) {
            importedEvent3 = assertSingleEvent(applyCreate(testUser3.getApiClient(), constructBody(iMip3)));
        } else {
            importedEvent3 = assertSingleEvent(accept(testUser3.getApiClient(), constructBody(iMip3), null), createdEvent1.getUid());
            assertAttendeePartStat(importedEvent3.getAttendees(), user3.getEmail1(), PartStat.ACCEPTED);
        }
        assertExternalOrganizerAndAttendees(importedEvent3, user1, new UserData[] { user3 }, new UserData[] { user1, user2 });
        /*
         * lookup event in user 3's calendar & check stored data
         */
        EventData reloadedEvent3 = reloadEventData(testUser3, importedEvent3, false);
        assertExternalOrganizerAndAttendees(reloadedEvent3, user1, new UserData[] { user3 }, new UserData[] { user1, user2 });
        assertAttendeePartStat(reloadedEvent3.getAttendees(), testUser2.getLogin(), PartStat.NEEDS_ACTION);
        if (applyOnly) {
            /*
             * lookup event in user 1 and 2's calendars & check stored data not affected
             */
            EventData reloadedEvent1 = reloadEventData(testUser, createdEvent1, false);
            assertInternalOrganizerAndAttendees(reloadedEvent1, user1, new UserData[] { user1, user2 }, null);
            EventData reloadedEvent2 = loadEventData(testUser2, defaultFolderId2, createdEvent1.getId(), createdEvent1.getRecurrenceId(), false);
            assertInternalOrganizerAndAttendees(reloadedEvent2, user1, new UserData[] { user1, user2 }, null);
            return reloadedEvent3;
        }
        /*
         * as user 1, receive unsolicited reply from user 3 & 'accept' the party crasher, then check resulting events
         */
        MailData iMip1 = lookupIMip(testUser, user3.getEmail1(), SchedulingMethod.REPLY, createdEvent1);
        assertSingleChange(analyze(testUser.getApiClient(), iMip1));
        CalendarResult acceptResult1 = acceptPartyCrasher(testUser.getApiClient(), constructBody(iMip1));
        EventData updatedEvent1 = null == createdEvent1.getSeriesId() || createdEvent1.getSeriesId().equals(createdEvent1.getId()) && null == createdEvent1.getRecurrenceId() ? 
            assertSingleEvent(acceptResult1, createdEvent1.getUid()) : assertSingleEventAndMaster(acceptResult1, createdEvent1.getUid()); 
        assertInternalOrganizerAndAttendees(updatedEvent1, user1, new UserData[] { user1, user2, user3 }, null);
        assertAttendeePartStat(updatedEvent1.getAttendees(), user3.getEmail1(), PartStat.ACCEPTED);
        /*
         * verify the previous attendee copy was removed from user 3's calendar
         */
        reloadEventData(testUser3.getApiClient(), reloadedEvent3, true);
        /*
         * reload & verify single event copy in calendars of all users
         */
        EventData reloadedEvent1 = loadEventData(testUser, defaultFolderId, createdEvent1.getId(), createdEvent1.getRecurrenceId(), false);
        assertInternalOrganizerAndAttendees(reloadedEvent1, user1, new UserData[] { user1, user2, user3 }, null);
        EventData reloadedEvent2 = loadEventData(testUser2, defaultFolderId2, createdEvent1.getId(), createdEvent1.getRecurrenceId(), false);
        assertInternalOrganizerAndAttendees(reloadedEvent2, user1, new UserData[] { user1, user2, user3 }, null);
        reloadedEvent3 = loadEventData(testUser3, defaultFolderId3, createdEvent1.getId(), createdEvent1.getRecurrenceId(), false);
        assertInternalOrganizerAndAttendees(reloadedEvent3, user1, new UserData[] { user1, user2, user3 }, null);
        return reloadedEvent3;
    }

    private static String forwardMail(ApiClient apiClient, MailData mail, String recipient) throws ApiException {
        MailApi mailApi = new MailApi(apiClient);
        MailReplyResponse mailReplyResponse = mailApi.forwardMailBuilder().withFolder(mail.getFolderId()).withId(mail.getId()).execute();
        MailReplyData replyData = mailReplyResponse.getData();
        replyData.setTo(Collections.singletonList(java.util.Arrays.asList((String) null, recipient)));
        String result = mailApi.sendMailBuilder().withJson0(new Gson().toJson(replyData)).execute();
        assertNotNull(result);
        return result;
    }

    private static MailData lookupIMip(TestUser testUser, String fromToMatch, SchedulingMethod methodToMatch, EventData eventToMatch) throws Exception {
        return lookupIMip(testUser, fromToMatch, methodToMatch, eventToMatch, null);
    }

    private static MailData lookupIMip(TestUser testUser, String fromToMatch, SchedulingMethod methodToMatch, EventData eventToMatch, ParticipationStatus partStatToMatch) throws Exception {
        return new IMipReceiver(testUser.getApiClient())
            .from(fromToMatch).method(methodToMatch).subject(eventToMatch.getSummary())
            .sequence(eventToMatch.getSequence()).uid(eventToMatch.getUid())
            .recurrenceId(null == eventToMatch.getRecurrenceId() ? null : new DefaultRecurrenceId(decode(eventToMatch.getRecurrenceId())))
            .partStat(partStatToMatch)
        .receive();
    }

    private static void acceptAs(TestUser user, EventData event, String folderId) throws ApiException {
        ChronosCalendarResultResponse attendeeUpdateResult = new ChronosApi(user.getApiClient()).updateAttendeeBuilder()
                                                                                                .withId(null != event.getSeriesId() ? event.getSeriesId() : event.getId())
                                                                                                .withRecurrenceId(null != event.getSeriesId() ? null : event.getRecurrenceId())
                                                                                                .withFolder(folderId)
                                                                                                .withAttendeeAndAlarm(new AttendeeAndAlarm().attendee(getUserAttendee(user).partStat(PartStat.ACCEPTED.getStatus())))
                                                                                                .withTimestamp(event.getLastModified())
                                                                                                .withExtendedEntities(Boolean.TRUE)
                                                                                                .execute();
        EventData attendeeUpdateEvent = assertSingleEvent(attendeeUpdateResult.getData());
        assertAttendeePartStat(attendeeUpdateEvent.getAttendees(), user.getLogin(), PartStat.ACCEPTED);
    }
}
