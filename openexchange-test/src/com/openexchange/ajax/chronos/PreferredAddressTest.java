/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.chronos;

import static com.openexchange.ajax.chronos.factory.EventFactory.createSingleTwoHourEvent;
import static com.openexchange.ajax.chronos.itip.ITipUtil.receiveIMip;
import static com.openexchange.ajax.chronos.itip.assertion.ITipActionAssertions.ACTIONS;
import static com.openexchange.chronos.scheduling.SchedulingMethod.REQUEST;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.ACCEPT;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.ACCEPT_AND_IGNORE_CONFLICTS;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.APPLY_CREATE;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.APPLY_PROPOSAL;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.APPLY_REMOVE;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.APPLY_RESPONSE;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.DECLINE;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.IGNORE;
import static com.openexchange.testing.httpclient.models.Analysis.ActionsEnum.TENTATIVE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.openexchange.admin.soap.user.dataobjects.Context;
import com.openexchange.admin.soap.user.dataobjects.User;
import com.openexchange.ajax.chronos.factory.AttendeeFactory;
import com.openexchange.ajax.chronos.factory.EventFactory;
import com.openexchange.ajax.chronos.itip.assertion.DefaultITipActionAssertion;
import com.openexchange.ajax.chronos.scheduling.AbstractSchedulingTest;
import com.openexchange.java.Strings;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.test.common.test.pool.soap.SoapUserService;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.Analysis.ActionsEnum;
import com.openexchange.testing.httpclient.models.Attendee;
import com.openexchange.testing.httpclient.models.CommonResponse;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.JSlobData;
import com.openexchange.testing.httpclient.models.JSlobsResponse;
import com.openexchange.testing.httpclient.models.UserData;
import com.openexchange.testing.httpclient.modules.JSlobApi;
import com.openexchange.testing.httpclient.modules.UserApi;

/**
 * {@link PreferredAddressTest}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class PreferredAddressTest extends AbstractSchedulingTest {

    private static final String PREFERRED_CALENDAR_USER_ADDRESS = "preferredAddress";

    @Test
    public void testRandmoMail_fails() throws Exception {
        String alias = "foo@example.org";
        setPreferredAddress(testUser, alias, true);
    }

    @Test
    public void testInjectionOfDisplayName() throws Exception {
        /*
         * Create event with independent common name
         */
        String cn = "Horsti";
        EventData eventToCreate = createSingleTwoHourEvent(getUserId(), "testDefaultInjection");
        eventToCreate.getAttendees().getFirst().setCn(cn);
        EventData event = eventManager.createEvent(eventToCreate);
        assertMailAddress(event, testUser.getLogin(), cn);
    }

    @Test
    public void testDefaultInjection() throws Exception {
        /*
         * Create event with default settings
         */
        EventData event = eventManager.createEvent(createSingleTwoHourEvent(getUserId(), "testDefaultInjection"));
        assertMailAddress(event, testUser.getLogin());
        /*
         * Set new preferred address
         */
        String alias = "foo@example.org";
        setAlias(testUser, alias);
        setPreferredAddress(testUser, alias);
        /*
         * Update event and check that mail address was *NOT* updated
         */
        EventData delta = EventFactory.deltaOf(event);
        delta.setDescription("New desc");
        EventData updateEvent = eventManager.updateEvent(delta);
        assertMailAddress(updateEvent, testUser.getLogin());
        /*
         * Create new event and check that alias is used
         */
        assertMailAddress(eventManager.createEvent(createSingleTwoHourEvent(getUserId(), "testAlias"), true), alias);
    }

    @Test
    public void testAliasInjection() throws Exception {
        /*
         * Set alias and create event
         */
        String alias = "foo@example.org";
        setAlias(testUser, alias);
        assertMailAddress(eventManager.createEvent(createSingleTwoHourEvent(getUserId(), "w/o Alias")), testUser.getLogin());
        /*
         * Set new preferred address
         */
        setPreferredAddress(testUser, alias);
        /*
         * Check that preferred address is chosen on newly created events
         */
        String subject = "testAliasInjection";
        EventData preparedEvent = createSingleTwoHourEvent(getUserId(), subject);
        preparedEvent.setAttendees(AttendeeFactory.createAsExternals(testUserC2));
        EventData event = eventManager.createEvent(preparedEvent, true);
        assertMailAddress(event, alias);
        assertAnalyzeActions(analyze(testUserC2.getApiClient(), receiveIMip(testUserC2.getApiClient(), alias, subject, 0, REQUEST)), ACTIONS);
        /*
         * Delete alias
         */
        setAlias(testUser, null);
        /*
         * Check that alias in existing event has changed and outgoing
         * scheduling mails are sent with the primary mail address
         * See also com.openexchange.chronos.storage.rdb.groupware.CalendarStorageInterceptor
         */
        EventData existingEvent = eventManager.getEvent(null, event.getId());
        assertMailAddress(existingEvent, testUser.getLogin());
        /*
         * Check that updates on the event do use the primary mail address
         */
        EventData delta = EventFactory.deltaOf(existingEvent);
        delta.setLocation("Olpe");
        EventData updateEvent = eventManager.updateEvent(delta);
        assertMailAddress(updateEvent, testUser.getLogin());
        assertAnalyzeActions(analyze(testUserC2.getApiClient(), receiveIMip(testUserC2.getApiClient(), testUser.getLogin(), subject, 1, REQUEST)), new DefaultITipActionAssertion() {

            @Override
            protected void validateActions(List<ActionsEnum> actions) {
                /*
                 * event hasn't been touched by user, so more actions than usual are sent back
                 * AND organizer has changed, so require IGNORE action
                 */
                containsOneOf(actions, ACCEPT, ACCEPT_AND_IGNORE_CONFLICTS);
                checkWantedActions(actions, TENTATIVE, DECLINE, IGNORE);
                checkUnwantedActions(actions, APPLY_PROPOSAL, APPLY_REMOVE, APPLY_RESPONSE, APPLY_CREATE);
            }
        });
        /*
         * Create new event and check that default value is used again,
         * even though preferred calendar user address is still the deleted alias
         */
        assertMailAddress(eventManager.createEvent(createSingleTwoHourEvent(getUserId(), "defaultValue"), true), testUser.getLogin());
    }

    /*
     * ============================== HELPERS ==============================
     */

    private void assertMailAddress(EventData event, String expectedMailAddress) {
        assertMailAddress(event, expectedMailAddress, null);
    }

    private void assertMailAddress(EventData event, String expectedMailAddress, String expectedCN) {
        assertNotNull(event.getAttendees());
        assertFalse(event.getAttendees().isEmpty());
        Optional<Attendee> attendee = event.getAttendees().stream().filter(a -> expectedMailAddress.equalsIgnoreCase(a.getEmail())).findAny();
        assertTrue(attendee.isPresent());
        if (Strings.isNotEmpty(expectedCN)) {
            assertEquals(expectedCN, attendee.get().getCn());
        }
    }

    private static void setPreferredAddress(TestUser testUser, String alias) throws ApiException {
        setPreferredAddress(testUser, alias, false);
    }

    private static void setPreferredAddress(TestUser testUser, String alias, boolean expectError) throws ApiException {
        JSlobApi jslobApi = new JSlobApi(testUser.getApiClient());
        CommonResponse response = jslobApi.setJSlob(Collections.singletonMap("chronos", Collections.singletonMap(PREFERRED_CALENDAR_USER_ADDRESS, alias)), "io.ox/calendar", null);
        assertNotNull(response, "Response missing!");
        if (expectError) {
            assertNotNull(response.getError());
            return;
        }
        assertNull(response.getError());
        JSlobsResponse jSlobsResponse = jslobApi.getJSlobList(Collections.singletonList("io.ox/calendar"), null);
        assertNotNull(jSlobsResponse, "Response missing!");
        assertNull(jSlobsResponse.getError());
        for (JSlobData jSlobData : jSlobsResponse.getData()) {
            Assertions.assertTrue(jSlobData.getTree().toString().toLowerCase().contains((PREFERRED_CALENDAR_USER_ADDRESS + "=" + alias).toLowerCase()));
        }
    }

    private static void setAlias(TestUser testUser, String alias) throws Exception {
        UserApi userApi = new com.openexchange.testing.httpclient.modules.UserApi(testUser.getApiClient());
        UserData userData = userApi.getUser(Integer.toString(testUser.getUserId())).getData();
        List<String> newAliases = new ArrayList<String>(userData.getAliases());
        if (null == alias) {
            newAliases.clear();
            newAliases.add(testUser.getLogin());
        } else {
            newAliases.add(alias);
        }
        User user = soapUserFor(i(userData.getUserId()));
        user.setAliases(newAliases);
        SoapUserService.getInstance().change(soapContextFor(testUser.getContextId()), user);
        assertThat(userApi.getUser(Integer.toString(testUser.getUserId())).getData().getAliases()).containsExactlyInAnyOrderElementsOf(newAliases);

    }

    private static Context soapContextFor(int contextId) {
        Context context = new Context();
        context.setId(I(contextId));
        return context;
    }

    private static User soapUserFor(int userId) {
        User user = new User();
        user.setId(I(userId));
        return user;
    }

}
