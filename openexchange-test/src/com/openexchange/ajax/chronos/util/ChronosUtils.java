/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.chronos.util;

import static com.openexchange.ajax.framework.ClientCommons.checkResponse;
import static com.openexchange.chronos.common.CalendarUtils.decode;
import static com.openexchange.chronos.common.CalendarUtils.getURI;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.function.Function;
import java.util.stream.Collectors;
import com.openexchange.ajax.chronos.itip.IMipReceiver;
import com.openexchange.ajax.chronos.manager.EventManager;
import com.openexchange.chronos.EventField;
import com.openexchange.chronos.common.CalendarUtils;
import com.openexchange.chronos.common.DefaultRecurrenceId;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.invoker.ApiClient;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.Attendee;
import com.openexchange.testing.httpclient.models.CalendarUser;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.EventForwardMailBody;
import com.openexchange.testing.httpclient.models.EventForwardedResponse;
import com.openexchange.testing.httpclient.models.EventResponse;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.models.SchedulingMailResponseData;
import com.openexchange.testing.httpclient.models.SchedulingMailResponseData.StatusEnum;
import com.openexchange.testing.httpclient.modules.ChronosApi;

/**
 * {@link ChronosUtils}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class ChronosUtils {

    /**
     * Try to find an attendee with the given ID
     *
     * @param attendees The list of attendees to search in
     * @param entity The entity ID of the attendee to find
     * @return The {@link Attendee} or <code>null</code>
     */
    public static Attendee find(List<Attendee> attendees, int entity) {
        return find(attendees, Integer.valueOf(entity));
    }

    /**
     * Try to find an attendee with the given ID
     *
     * @param attendees The list of attendees to search in
     * @param entity The entity ID of the attendee to find
     * @return The {@link Attendee} or <code>null</code>
     */
    public static Attendee find(List<Attendee> attendees, Integer entity) {
        if (null != attendees) {
            for (Attendee a : attendees) {
                if (null != a.getEntity() && a.getEntity().equals(entity)) {
                    return a;
                }
            }
        }
        return null;
    }

    /**
     * Try to find an attendee with the given email
     *
     * @param attendees The list of attendees to search in
     * @param email The email of the attendee to find
     * @return The {@link Attendee} or <code>null</code>
     */
    public static Attendee find(List<Attendee> attendees, String email) {
        if (null != attendees) {
            for (Attendee attendee : attendees) {
                if (isUriMatching(attendee, email)) {
                    return attendee;
                }
            }
        }
        return null;
    }

    /**
     * Remove the given user if found in the attendee list
     *
     * @param attendees The attendees
     * @param user The user
     * @return A list of attendees
     */
    public static List<Attendee> removeIfPresent(List<Attendee> attendees, TestUser user) {
        attendees.removeIf(a -> isUriMatching(a, user.getLogin()));
        return attendees;
    }

    private static boolean isUriMatching(Attendee attendee, String email) {
        return null != attendee.getUri() && attendee.getUri().toLowerCase().contains(email.toLowerCase());
    }

    /**
     * Reloads the data of a certain event.
     * 
     * @param testUser The test user to use
     * @param eventData The event to reload
     * @param expectNotFound <code>true</code> if a <i>not found</i> error is expected, <code>false</code>, otherwise
     * @return The reloaded event, or <code>null</code> if none was found and this was expected
     */
    public static EventData reloadEventData(TestUser testUser, EventData eventData, boolean expectNotFound) throws ApiException {
        return reloadEventData(testUser.getApiClient(), eventData, expectNotFound);
    }

    /**
     * Reloads the data of a certain event.
     * 
     * @param apiClient The API client to use
     * @param eventData The event to reload
     * @param expectNotFound <code>true</code> if a <i>not found</i> error is expected, <code>false</code>, otherwise
     * @return The reloaded event, or <code>null</code> if none was found and this was expected
     */
    public static EventData reloadEventData(ApiClient apiClient, EventData eventData, boolean expectNotFound) throws ApiException {
        return loadEventData(apiClient, eventData.getFolder(), eventData.getId(), eventData.getRecurrenceId(), expectNotFound);
    }

    /**
     * Loads the data of a certain event.
     * 
     * @param testUser The test user to use
     * @param folderId The folder identifier of the event to load
     * @param id The identifier of the event to load
     * @param recurrenceId The recurrence identifier of the event to load, or <code>null</code> if not applicable
     * @param expectNotFound <code>true</code> if a <i>not found</i> error is expected, <code>false</code>, otherwise
     * @return The reloaded event, or <code>null</code> if none was found and this was expected
     */
    public static EventData loadEventData(TestUser testUser, String folderId, String id, String recurrenceId, boolean expectNotFound) throws ApiException {
        return loadEventData(testUser.getApiClient(), folderId, id, recurrenceId, expectNotFound);
    }

    /**
     * Loads the data of a certain event.
     * 
     * @param apiClient The API client to use
     * @param folderId The folder identifier of the event to load
     * @param id The identifier of the event to load
     * @param recurrenceId The recurrence identifier of the event to load, or <code>null</code> if not applicable
     * @param expectNotFound <code>true</code> if a <i>not found</i> error is expected, <code>false</code>, otherwise
     * @return The reloaded event, or <code>null</code> if none was found and this was expected
     */
    protected static EventData loadEventData(ApiClient apiClient, String folderId, String id, String recurrenceId, boolean expectNotFound) throws ApiException {
        EventResponse response = new ChronosApi(apiClient).getEvent(id, folderId, recurrenceId, null, null);
        if (expectNotFound) {
            assertNotNull(response.getError());
            assertNotNull(response.getCode());
            assertTrue(response.getCode().contains("404"));
            return null;
        }
        return checkResponse(response.getError(), response.getError(), response.getCategories(), response.getData());
    }

    /**
     * Forwards an event to a specific recipient.
     * 
     * @param apiClient The API client to use
     * @param eventData The event to forward
     * @param recipient The recipient
     * @return The resulting forward status
     */
    public static StatusEnum forwardEvent(ApiClient apiClient, EventData eventData, String recipient) throws ApiException {
        ChronosApi chronosApi = new ChronosApi(apiClient);
        EventForwardedResponse forwardedResponse = chronosApi.forwardEventBuilder()
                                                             .withFolder(eventData.getFolder())
                                                             .withId(eventData.getId())
                                                             .withRecurrenceId(eventData.getRecurrenceId())
                                                             .withEventForwardMailBody(new EventForwardMailBody().addRecipientsItem(new CalendarUser().uri(getURI(recipient))))
                                                             .execute();
        List<SchedulingMailResponseData> forwardedResponseData = checkResponse(forwardedResponse.getError(), forwardedResponse.getErrorDesc(), forwardedResponse.getCategories(), forwardedResponse.getData());
        assertTrue(null != forwardedResponseData && 1 == forwardedResponseData.size());
        StatusEnum status = forwardedResponseData.get(0).getStatus();
        assertTrue(StatusEnum.SENT.equals(status) || StatusEnum.DELIVERED.equals(status));
        return status;
    }

    /**
     * Looks up a certain attendee by its URI based on the supplied email address.
     * 
     * @param attendees The attendees to search in
     * @param email The email to match (to be compared as <code>mailto:</code> URI)
     * @return The matching attendee, or <code>null</code> if not found
     */
    public static Attendee lookupAttendee(List<Attendee> attendees, String email) {
        if (null != attendees) {
            String uri = getURI(email);
            for (Attendee attendee : attendees) {
                if (uri.equals(attendee.getUri())) {
                    return attendee;
                }
            }
        }
        return null;
    }

    /**
     * Looks up a certain attendee by its internal entity identifier.
     * 
     * @param attendees The attendees to search in
     * @param entity The entity identifier to match
     * @return The matching attendee, or <code>null</code> if not found
     */
    public static Attendee lookupAttendee(List<Attendee> attendees, Integer entity) {
        if (null != attendees) {
            for (Attendee attendee : attendees) {
                if (entity.equals(attendee.getEntity())) {
                    return attendee;
                }
            }
        }
        return null;
    }

    /**
     * Looks up a certain iMIP mail, using the criteria of the supplied event.
     * 
     * @param testUser The test user to use
     * @param fromToMatch The <i>From</i> address to match in the mail
     * @param methodToMatch The scheduling method to match
     * @param eventToMatch The event to match
     * @return The matching iMIP mail
     */
    public static MailData lookupIMip(TestUser testUser, String fromToMatch, SchedulingMethod methodToMatch, EventData eventToMatch) throws Exception {
        return new IMipReceiver(testUser.getApiClient())
                                                        .from(fromToMatch)
                                                        .method(methodToMatch)
                                                        .subject(eventToMatch.getSummary())
                                                        .sequence(eventToMatch.getSequence())
                                                        .uid(eventToMatch.getUid())
                                                        .recurrenceId(null == eventToMatch.getRecurrenceId() ? null : new DefaultRecurrenceId(decode(eventToMatch.getRecurrenceId())))
                                                        .receive();
    }

    /**
     * Get all events of of a series
     * 
     * @param eventManager The manager to user
     * @param event The event to get the UID from
     * @return All events of the series
     * @throws ApiException In case of error
     */
    public static List<EventData> getAllEventsOfEvent(EventManager eventManager, EventData event) throws ApiException {
        Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        instance.setTimeInMillis(System.currentTimeMillis());
        instance.add(Calendar.DAY_OF_MONTH, -1);
        Date from = instance.getTime();
        instance.add(Calendar.DAY_OF_MONTH, 7);
        Date until = instance.getTime();
        List<EventData> allEvents = eventManager.getAllEvents(null, from, until, true);
        return getEventsByUid(allEvents, event.getUid()); // Filter by series uid
    }

    /**
     * Gets a value indicating whether the supplied event is considered as the <i>master</i> event of a recurring series or not, based
     * on checking the properties {@link EventField#ID} and {@link EventField#SERIES_ID} for equality and the absence of an assigned
     * {@link EventField#RECURRENCE_ID}.
     *
     * @param event The event to check
     * @return <code>true</code> if the event is the series master, <code>false</code>, otherwise
     */
    public static boolean isSeriesMaster(EventData event) {
        return null != event && null != event.getId() && event.getId().equals(event.getSeriesId()) && null == event.getRecurrenceId();
    }

    /**
     * Filters the given list of event
     *
     * @param events The event to filter by summary
     * @param summary The summary each event must begin with
     * @return A list of events with the same summary
     */
    public static List<EventData> filterEventBySummary(List<EventData> events, String summary) {
        return filterEventBy(events, summary, EventData::getSummary);
    }

    /**
     * Filters the given list of event
     *
     * @param events The event to filter by summary
     * @param comparee The string each result of the given function must begin with
     * @param f The function to get a string from the event data to compare to the comparee
     * @return A list of events with the same summary
     */
    public static List<EventData> filterEventBy(List<EventData> events, String comparee, Function<EventData, String> f) {
        return events.stream().filter(e -> f.apply(e).startsWith(comparee)).collect(Collectors.toList()); //NOSONARLINT - explicitly want a modifiable list
    }

    /**
     * Filters the given event by the given event UID
     *
     * @param events The events to filter
     * @param uid The UID to filter for
     * @return A modifiable list containing only events with the given UID
     */
    public static List<EventData> getEventsByUid(List<EventData> events, String uid) {
        if (null == events || events.isEmpty()) {
            return List.of();
        }
        return new ArrayList<>(events.stream().filter(e -> uid.equals(e.getUid())).sorted(new Comparator<EventData>() {

            @Override
            public int compare(EventData event1, EventData event2) {
                String recurrenceId1 = event1.getRecurrenceId();
                String recurrenceId2 = event2.getRecurrenceId();
                if (null == recurrenceId1) {
                    return null == recurrenceId2 ? 0 : -1;
                }
                if (null == recurrenceId2) {
                    return 1;
                }
                long dateTime1 = CalendarUtils.decode(recurrenceId1).getTimestamp();
                long dateTime2 = CalendarUtils.decode(recurrenceId2).getTimestamp();
                if (dateTime1 == dateTime2) {
                    return 0;
                }
                return dateTime1 < dateTime2 ? -1 : 1;
            }
        }).collect(Collectors.toList())); //NOSONARLINT - explicitly want a modifiable list
    }

    /**
     * Get all events of fitting to a UID
     * 
     * @param eventManager The manager to user
     * @param uid The event UID to get
     * @return All events of the series
     * @throws ApiException
     */
    public static List<EventData> getAllEventsOfEvent(EventManager eventManager, String uid) throws ApiException {
        Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        instance.setTimeInMillis(System.currentTimeMillis());
        instance.add(Calendar.DAY_OF_MONTH, -1);
        Date from = instance.getTime();
        instance.add(Calendar.DAY_OF_MONTH, 7);
        Date until = instance.getTime();
        List<EventData> allEvents = eventManager.getAllEvents(null, from, until, true);
        allEvents = getEventsByUid(allEvents, uid); // Filter by series uid
        return allEvents;
    }

}
