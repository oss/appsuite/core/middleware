/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.chronos;

import static com.openexchange.chronos.exception.CalendarExceptionMessages.MOVE_OCCURRENCE_NOT_SUPPORTED_MSG;
import static com.openexchange.java.Autoboxing.l;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import com.openexchange.ajax.chronos.factory.AttendeeFactory;
import com.openexchange.ajax.chronos.factory.EventFactory;
import com.openexchange.ajax.chronos.manager.CalendarFolderManager;
import com.openexchange.ajax.chronos.manager.ChronosApiException;
import com.openexchange.ajax.chronos.manager.EventManager;
import com.openexchange.ajax.chronos.util.ChronosUtils;
import com.openexchange.java.Consumers.OXConsumer;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.FolderData;
import com.openexchange.testing.httpclient.modules.FoldersApi;

/**
 * {@link MoveBetweenFolderTest}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class MoveBetweenFolderTest extends AbstractSecondUserChronosTest {

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
    }

    @ParameterizedTest
    @ValueSource(booleans =
    { true, false })
    public void testMoveBetweenPrivateFolders(boolean series) throws Exception {
        /*
         * Create event in standard folder
         */
        EventData eventInStandardPrivateFolder = eventManager.createEvent(prepareEvent(series));
        /*
         * Create new private calendar folder and move event into it
         */
        String privateCalendarFolder = folderManager.createCalendarFolder("Learning", getUserId());
        eventManager.move(privateCalendarFolder, eventInStandardPrivateFolder.getId());
        /*
         * Get event from ("new") folder and check
         */
        EventManager privateFolderEventManager = new EventManager(defaultUserApi, privateCalendarFolder);
        EventData eventInCreatedPrivateFolder = privateFolderEventManager.getEvent(null, eventInStandardPrivateFolder.getId());
        assertEquals(eventInCreatedPrivateFolder.getSummary(), eventInStandardPrivateFolder.getSummary());
    }

    @Test
    public void testMoveSeriesException() throws Exception {
        /*
         * Create event series with exception in standard folder
         */
        EventData eventInStandardPrivateFolder = eventManager.createEvent(prepareEvent(true));
        List<EventData> events = ChronosUtils.getAllEventsOfEvent(eventManager, eventInStandardPrivateFolder);
        EventData changeException = eventManager.createChangeException(eventInStandardPrivateFolder, null, events.get(2).getRecurrenceId(), delta -> delta.setSummary("Move it!"));
        assertNotEquals(eventInStandardPrivateFolder.getId(), changeException.getId());
        /*
         * Create new private calendar folder and move exception into it, expect to fail
         */
        String privateCalendarFolder = folderManager.createCalendarFolder("CalendarFail", getUserId());
        changeException = eventManager.getEvent(null, changeException.getId());
        eventManager.tryMove(privateCalendarFolder, changeException.getId(), com.openexchange.chronos.exception.CalendarExceptionMessages.MOVE_OCCURRENCE_NOT_SUPPORTED_MSG);
    }

    @Test
    public void testMoveOrphanedSeriesException() throws Exception {
        /*
         * As user B, create event series with exception in standard folder
         */
        EventData eventInStandardPrivateFolder = eventManager2.createEvent(prepareEvent(true, testUser2.getUserId()));
        List<EventData> events = ChronosUtils.getAllEventsOfEvent(eventManager2, eventInStandardPrivateFolder);
        EventData changeException = createChangeException(eventInStandardPrivateFolder, events, 2);
        /*
         * As user B, create an additional change exception, user A has no access for
         */
        EventData anotherChangeException = createChangeException(eventInStandardPrivateFolder, events, 4, delta -> delta.setLocation("Somewhere else"));
        /*
         * As user A, create new private calendar folder and move the change exception into the new folder
         */
        String privateCalendarFolder = folderManager.createCalendarFolder("OrphanedChangeExceptions", getUserId());
        EventData orphanedException = eventManager.getEvent(null, changeException.getId());
        eventManager.setLastTimeStamp(System.currentTimeMillis());
        eventManager.move(privateCalendarFolder, orphanedException.getSeriesId());
        /*
         * As user A, get event from ("new") folder and check
         */
        checkOrphanedException(new EventManager(defaultUserApi, privateCalendarFolder), orphanedException.getId());
        /*
         * As user B, check events
         */
        equalsEvent(eventInStandardPrivateFolder, eventManager2.getEvent(null, eventInStandardPrivateFolder.getSeriesId()));
        equalsEvent(changeException, eventManager2.getEvent(null, changeException.getId()));
        equalsEvent(anotherChangeException, eventManager2.getEvent(null, anotherChangeException.getId()));
        /*
         * As user B, move complete series
         */
        CalendarFolderManager folderManager2 = new CalendarFolderManager(userApi2, new FoldersApi(testUser2.getApiClient()));
        String calendarFolder = folderManager2.createCalendarFolder("SeriesMove", testUser2.getUserId());
        eventInStandardPrivateFolder = eventManager2.getEvent(null, eventInStandardPrivateFolder.getSeriesId());
        eventManager2.setLastTimeStamp(System.currentTimeMillis());
        eventManager2.move(calendarFolder, eventInStandardPrivateFolder.getSeriesId());
        eventInStandardPrivateFolder = new EventManager(userApi2, calendarFolder).getEvent(null, eventInStandardPrivateFolder.getSeriesId());
        assertTrue(eventInStandardPrivateFolder.getChangeExceptionDates().size() == 2);
    }

    @Test
    public void testMoveMultipleOrphanedSeriesException() throws Exception {
        /*
         * Create event series with exception in standard folder, as user B
         */
        EventData eventInStandardPrivateFolder = eventManager2.createEvent(prepareEvent(true, testUser2.getUserId()));
        List<EventData> events = ChronosUtils.getAllEventsOfEvent(eventManager2, eventInStandardPrivateFolder);
        EventData changeException = createChangeException(eventInStandardPrivateFolder, events, 2);
        EventData changeException2 = createChangeException(eventInStandardPrivateFolder, events, 4);
        /*
         * Create new private calendar folder and move the single change exception for user A into the new folder
         */
        String privateCalendarFolder = folderManager.createCalendarFolder("OrphanedChangeExceptions", getUserId());
        EventData orphanedException = eventManager.getEvent(null, changeException.getId());
        EventData orphanedException2 = eventManager.getEvent(null, changeException2.getId());
        /*
         * Try move only one orphaned exception
         */
        eventManager.tryMove(privateCalendarFolder, orphanedException.getId(), MOVE_OCCURRENCE_NOT_SUPPORTED_MSG);
        /*
         * Move all orphaned exceptions belonging to a specific series
         */
        eventManager.move(privateCalendarFolder, orphanedException.getSeriesId());
        /*
         * Get event from ("new") folder and check
         */
        EventManager privateFolderEventManager = new EventManager(defaultUserApi, privateCalendarFolder);
        checkOrphanedException(privateFolderEventManager, orphanedException.getId());
        checkOrphanedException(privateFolderEventManager, orphanedException2.getId());
    }

    private void checkOrphanedException(EventManager manager, String eventId) throws Exception {
        EventData event = manager.getEvent(null, eventId);
        assertNotNull(event.getAttendees());
        assertEquals(event.getAttendees().size(), Integer.valueOf(2));
    }

    @Test
    public void testMovePseudoScheduledSeriesWithOneGroupScheduledExceptionForSameUser() throws Exception {
        /*
         * Create event series with exception in standard folder
         */
        EventData eventInStandardPrivateFolder = eventManager.createEvent(prepareEvent(true));
        List<EventData> events = ChronosUtils.getAllEventsOfEvent(eventManager, eventInStandardPrivateFolder);
        EventData changeException = eventManager.createChangeException(eventInStandardPrivateFolder, null, events.get(2).getRecurrenceId(), delta -> delta.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser2)));
        assertNotEquals(eventInStandardPrivateFolder.getId(), changeException.getId());
        /*
         * Create new private calendar folder and move series containing the exception into it
         */
        String privateCalendarFolder = folderManager.createCalendarFolder("ExceptionTest", getUserId());
        eventInStandardPrivateFolder = eventManager.getEvent(null, eventInStandardPrivateFolder.getId());
        eventManager.move(privateCalendarFolder, eventInStandardPrivateFolder.getId());
        /*
         * Get event from ("new") folder and check
         */
        EventManager privateFolderEventManager = new EventManager(defaultUserApi, privateCalendarFolder);
        EventData eventInCreatedPrivateFolder = privateFolderEventManager.getEvent(null, eventInStandardPrivateFolder.getId());
        assertEquals(eventInCreatedPrivateFolder.getSummary(), eventInStandardPrivateFolder.getSummary());
        EventData exceptionInCreatedPrivateFolder = privateFolderEventManager.getEvent(null, changeException.getId());
        assertNotNull(ChronosUtils.find(exceptionInCreatedPrivateFolder.getAttendees(), testUser2.getUserId()));
        assertEquals(exceptionInCreatedPrivateFolder.getCalendarUser().getEntity(), testUser.getUserId());
    }

    @Test
    public void testMovePseudoScheduledSeriesWithOneGroupScheduledExceptionIntoOtherUsersFolder() throws Exception {
        /*
         * Create event series with exception in standard folder
         */
        EventData eventInStandardPrivateFolder = eventManager.createEvent(prepareEvent(true));
        List<EventData> events = ChronosUtils.getAllEventsOfEvent(eventManager, eventInStandardPrivateFolder);
        EventData changeException = eventManager.createChangeException(eventInStandardPrivateFolder, null, events.get(2).getRecurrenceId(), delta -> delta.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser2)));
        assertNotEquals(eventInStandardPrivateFolder.getId(), changeException.getId());
        /*
         * Create new private calendar folder and move series containing the exception into it
         */
        CalendarFolderManager folderManager2 = new CalendarFolderManager(userApi2, userApi2.getFoldersApi());
        String sharedPrivateFolderId = folderManager2.createCalendarFolder("ExceptionTest", testUser2.getUserId());
        FolderData folder = folderManager2.getFolder(sharedPrivateFolderId);
        folderManager2.shareFolder(folder, testUser);
        eventInStandardPrivateFolder = eventManager.getEvent(null, eventInStandardPrivateFolder.getId());
        /*
         * User B shared the calendar, User A tries to move the event into one of his folders, fails due to group scheduled change exception
         */
        eventManager.tryMove(sharedPrivateFolderId, eventInStandardPrivateFolder.getId(), com.openexchange.chronos.exception.CalendarExceptionMessages.UNSUPPORTED_FOLDER_MSG);
    }

    @DisplayName("Move event from private to public folder:")
    @ParameterizedTest(name = "Used series event: {0}, Get as owner: {1}")
    @CsvSource(delimiter = '|', textBlock = """
        true  | true
        false | true
        true  | false
        false | false
        """)
    public void testMoveBetweenPrivateAndPublicFolder(boolean series, boolean owner) throws Exception {
        /*
         * Create event in standard folder
         */
        EventData eventInStandardPrivateFolder = eventManager.createEvent(prepareEvent(series));
        /*
         * Create new public calendar folder and move event into it
         */
        String publicFolderId = createAndSharePublicFolder("publicFolder");
        eventManager.move(publicFolderId, eventInStandardPrivateFolder.getId());
        /*
         * Get event from ("new") folder and check
         */
        EventManager publicFolderEventManager = new EventManager(owner ? defaultUserApi : userApi2, publicFolderId);
        EventData eventInCreatedPrivateFolder = publicFolderEventManager.getEvent(null, eventInStandardPrivateFolder.getId());
        assertEquals(eventInCreatedPrivateFolder.getSummary(), eventInStandardPrivateFolder.getSummary());
    }

    @DisplayName("Move event between public folders:")
    @ParameterizedTest(name = "Used series event: {0}, Get as owner: {1}")
    @CsvSource(delimiter = '|', textBlock = """
        true  | true
        false | true
        true  | false
        false | false
        """)
    public void testMoveBetweenPublicFolders(boolean series, boolean owner) throws Exception {
        /*
         * Create first public folder
         */
        String firstPublicFolderId = createAndSharePublicFolder("firstPublic");
        EventManager pfem1 = new EventManager(defaultUserApi, firstPublicFolderId);
        /*
         * Create event in public folder
         */
        EventData eventInPublicFolder = pfem1.createEvent(prepareEvent(series));
        /*
         * Create second public folder and move event into it
         */
        String secondPublicFolderId = createAndSharePublicFolder("secondPublic");
        pfem1.move(secondPublicFolderId, eventInPublicFolder.getId());
        /*
         * Get event from ("new") folder and check
         */
        EventManager pfem2 = new EventManager(owner ? defaultUserApi : userApi2, secondPublicFolderId);
        EventData eventInSecondPublicFolder = pfem2.getEvent(null, eventInPublicFolder.getId());
        assertEquals(eventInSecondPublicFolder.getSummary(), eventInPublicFolder.getSummary());
    }

    @Test
    public void testMoveSeriesWithHiddenExceptions() throws Exception {
        /*
         * as user B, create event series and invite user A
         */
        EventData seriesEventData = prepareSeriesEventData("FREQ=DAILY;COUNT=10", getDefaultFolder(testUser2.getApiClient()), getCalendarUser(testUser2), getUserAttendee(testUser), getUserAttendee(testUser2));
        EventData createdEvent = eventManager2.createEvent(seriesEventData, true);
        /*
         * as user A, delete certain occurrences
         */
        EventData eventOccurrence1 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent.getSeriesId(), 4);
        eventManager.deleteEvent(getEventId(eventOccurrence1), l(eventOccurrence1.getTimestamp()), false);
        EventData eventOccurrence2 = getNthOccurrence(getExpandedEvents(testUser, defaultFolderId), createdEvent.getSeriesId(), 7);
        eventManager.deleteEvent(getEventId(eventOccurrence2), l(eventOccurrence2.getTimestamp()), false);
        /*
         * as user A, move event series to subfolder
         */
        String subfolderId = folderManager.createCalendarFolder("subfolder", testUser.getUserId());
        eventManager.move(subfolderId, createdEvent.getSeriesId());
        /*
         * verify move was successful
         */
        assertEquals(0, getExpandedEvents(testUser, defaultFolderId).stream().filter(e -> Objects.equals(e.getUid(), createdEvent.getUid())).count());
        assertEquals(8, getExpandedEvents(testUser, subfolderId).stream().filter(e -> Objects.equals(e.getUid(), createdEvent.getUid())).count());
    }

    /*
     * ============================== HELPERS ==============================
     */

    private EventData prepareEvent(boolean series) {
        return prepareEvent(series, getUserId());
    }

    private static EventData prepareEvent(boolean series, int userId) {
        if (series) {
            return EventFactory.createSeriesEvent(userId, "I like to move it!", 10, null);
        }
        return EventFactory.createSingleTwoHourEvent(userId, "Move it!");
    }

    private String createAndSharePublicFolder(String folderName) throws ApiException, ChronosApiException {
        return createAndSharePublicFolder(folderManager, folderName, testUser2);
    }

    private static String createAndSharePublicFolder(CalendarFolderManager folderManager, String folderName, TestUser testUser) throws ApiException, ChronosApiException {
        String folderId = folderManager.createPublicCalendarFolder(folderName);
        FolderData folder = folderManager.getFolder(folderId);
        folderManager.shareFolder(folder, testUser);
        return folderId;
    }

    private EventData createChangeException(EventData eventInStandardPrivateFolder, List<EventData> events, int occurrence) throws Exception {
        EventData changeException = createChangeException(eventInStandardPrivateFolder, events, occurrence, delta -> delta.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser2)));
        assertNotNull(ChronosUtils.find(changeException.getAttendees(), testUser.getUserId()));
        return changeException;
    }

    private EventData createChangeException(EventData eventInStandardPrivateFolder, List<EventData> events, int occurrence, OXConsumer<EventData, Exception> change) throws Exception {
        EventData changeException = eventManager2.createChangeException(eventInStandardPrivateFolder, null, events.get(occurrence).getRecurrenceId(), change);
        assertNotEquals(eventInStandardPrivateFolder.getId(), changeException.getId());
        EventData loadedEvent = eventManager2.getEvent(null, eventInStandardPrivateFolder.getId());
        assertNull(ChronosUtils.find(loadedEvent.getAttendees(), testUser.getUserId()));
        return changeException;
    }

    private void equalsEvent(EventData expected, EventData actual) {
        assertEquals(expected.getSeriesId(), actual.getSeriesId());
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getFolder(), actual.getFolder());
        assertEquals(expected.getAttendees().size(), actual.getAttendees().size());
    }

}
