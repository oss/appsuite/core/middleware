/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.chronos;

import static com.openexchange.java.Autoboxing.I;
import static java.lang.Boolean.TRUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.chronos.manager.CalendarFolderManager;
import com.openexchange.ajax.folder.manager.FolderPermissionsBits;
import com.openexchange.ajax.infostore.thirdparty.federatedSharing.FederatedSharingUtil;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.java.util.UUIDs;
import com.openexchange.testing.httpclient.models.FolderBody;
import com.openexchange.testing.httpclient.models.FolderBodyNotification;
import com.openexchange.testing.httpclient.models.FolderData;
import com.openexchange.testing.httpclient.models.FolderPermission;
import com.openexchange.testing.httpclient.models.FolderUpdateResponse;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.models.ShareLinkAnalyzeResponse;
import com.openexchange.testing.httpclient.models.ShareLinkAnalyzeResponseData;
import com.openexchange.testing.httpclient.models.ShareLinkAnalyzeResponseData.StateEnum;
import com.openexchange.testing.httpclient.models.SubscribeShareBody;
import com.openexchange.testing.httpclient.modules.FederatedSharingApi;

/**
 * {@link ShareToGroupTest}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class ShareToGroupTest extends AbstractSecondUserChronosTest {
    
    private Integer groupId;
    private CalendarFolderManager folderManager2;
    
    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        groupId = testContext.acquireGroup(Optional.of(List.of(I(testUser.getUserId()), I(testUser2.getUserId()))));
        folderManager2 = new CalendarFolderManager(userApi2, userApi2.getFoldersApi());
    }

    @Test
    public void testShareDefaultCalendarToGroup() throws Exception {
        testShareToGroup(defaultFolderId);
    }

    @Test
    public void testShareCalendarSubfolderToGroup() throws Exception {
        String folderId = folderManager.createCalendarFolder(UUIDs.getUnformattedStringFromRandom(), testUser.getUserId());
        testShareToGroup(folderId);
    }

    @Test
    public void testSharePublicCalendarToGroup() throws Exception {
        String folderId = folderManager.createPublicCalendarFolder(UUIDs.getUnformattedStringFromRandom());
        testShareToGroup(folderId);
    }

    @Test
    public void testShareDefaultAddressbookToGroup() throws Exception {
        testShareToGroup(getDefaultAddressBookFolder());
    }

    private void testShareToGroup(String folderId) throws Exception {
        /*
         * share user A's default calendar folder to group (containing user 2)
         */
        FolderData folder = folderManager.getFolder(folderId);
        List<FolderPermission> updatedPermissions = new ArrayList<FolderPermission>(folder.getPermissions());
        FolderPermission folderPermission = FolderPermissionsBits.AUTHOR.getPermission(groupId);
        folderPermission.setGroup(TRUE);
        updatedPermissions.add(folderPermission);
        // @formatter:off
        FolderUpdateResponse folderUpdateResponse = foldersApi.updateFolderBuilder()
            .withId(folder.getId())
            .withTimestamp(folder.getLastModifiedUtc())
            .withFolderBody(new FolderBody()
                .folder(new FolderData().permissions(updatedPermissions).id(folder.getId()))
                .notification(new FolderBodyNotification().transport("mail").message("msg")))
            .execute();
        // @formatter:on
        checkResponse(folderUpdateResponse.getError(), folderUpdateResponse.getErrorDesc(), folderUpdateResponse);
        /*
         * as user 2 (member of group), check for a notification mail
         */
        String expectedFrom = new com.openexchange.testing.httpclient.modules.UserApi(testUser.getApiClient()).getUser(String.valueOf(testUser.getUserId())).getData().getEmail1();
        MailData mailData = FederatedSharingUtil.receiveShareMail(testUser2.getApiClient(), expectedFrom, folder.getTitle());
        assertNotNull(mailData);
        Map<?, ?> mailHeaders = (Map<?, ?>) mailData.getHeaders();
        assertEquals("share-created", mailHeaders.get("X-Open-Xchange-Share-Type"));
        String shareUrl = (String) mailHeaders.get("X-Open-Xchange-Share-URL");
        assertNotNull(shareUrl);
        List<NameValuePair> urlParameters = URLEncodedUtils.parse(shareUrl, StandardCharsets.UTF_8);
        String folderParameter = urlParameters.stream().filter(nv -> "folder".equals(nv.getName())).findFirst().orElseThrow().getValue();
        assertEquals(folder.getId(), folderParameter);
        /*
         * analyze share link
         */
        FederatedSharingApi federatedSharingApi = new FederatedSharingApi(testUser2.getApiClient());
        ShareLinkAnalyzeResponse analyzeResponse = federatedSharingApi.analyzeShareLink(new SubscribeShareBody().link(shareUrl));
        ShareLinkAnalyzeResponseData analyzeResponseData = checkResponse(analyzeResponse.getError(), analyzeResponse.getErrorDesc(), analyzeResponse).getData();
        assertEquals(StateEnum.SUBSCRIBED, analyzeResponseData.getState());
        assertEquals(folder.getId(), analyzeResponseData.getFolder());
        assertEquals(folder.getModule(), analyzeResponseData.getModule());
        /*
         * check that the folder can be accessed and is subscribed for user 2
         */
        FolderData sharedFolder = folderManager2.getFolder(folder.getId());
        assertNotNull(sharedFolder);
        if (I(FolderObject.PUBLIC).equals(folder.getType())) {
            assertEquals(I(FolderObject.PUBLIC), sharedFolder.getType());
        } else {
            assertEquals(I(FolderObject.SHARED), sharedFolder.getType());
        }
        assertEquals(TRUE, sharedFolder.getSubscribed());
    }

}
