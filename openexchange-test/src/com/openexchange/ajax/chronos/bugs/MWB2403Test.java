/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.chronos.bugs;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import com.openexchange.ajax.chronos.AbstractChronosTest;
import com.openexchange.ajax.folder.manager.FolderPermissionsBits;
import com.openexchange.folderstorage.type.PrivateType;
import com.openexchange.test.common.test.pool.ConfigAwareProvisioningService;
import com.openexchange.testing.httpclient.models.FolderBody;
import com.openexchange.testing.httpclient.models.FolderData;
import com.openexchange.testing.httpclient.models.FolderUpdateResponse;
import com.openexchange.testing.httpclient.modules.FoldersApi;

/**
 * {@link MWB2403Test}
 * 
 * Delete of user removes calendars of other users
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class MWB2403Test extends AbstractChronosTest {

    @Test
    public void testDeleteUserWithSharedFolder() throws Exception {
        /*
         * as user a, share your calendar folder to user b 
         */
        String folderId = getDefaultFolder();
        FolderData folder = folderManager.getFolder(folderId);
        folder = folderManager.shareFolder(folder, testUser2, FolderPermissionsBits.VIEWER);
        /*
         * as user b, update the folder and assign a personal color to it
         */
        FolderUpdateResponse folderUpdateResponse = new FoldersApi(testUser2.getApiClient()).updateFolderBuilder()
            .withTree("1")
            .withId(folderId)
            .withFolderBody(FolderBody.fromJson("{\"folder\":{\"com.openexchange.calendar.extendedProperties\":{\"color\":{\"value\":\"#ff9500\"}}}}"))
        .execute();
        checkResponse(folderUpdateResponse.getError(), folderUpdateResponse.getErrorDesc(), folderUpdateResponse.getData());
        /*
         * as user a, check that the folder indicates that user b did the last modification
         */
        assertEquals(I(testUser2.getUserId()), folderManager.getFolder(folderId).getModifiedBy());
        /*
         * delete user b
         */
        ConfigAwareProvisioningService.getService().deleteUser(testUser2.getContextId(), testUser2.getUserId(), testContext.getUsedBy());
        /*
         * as user a, check the folder is still the default private calendar folder
         */
        folder = folderManager.getFolder(folderId);
        assertEquals(I(PrivateType.getInstance().getType()), folder.getType());
        assertEquals(I(testUser.getUserId()), folder.getCreatedBy());
        assertEquals(I(testContext.getAdmin().getUserId()), folder.getModifiedBy());
        ArrayList<ArrayList<?>> privateFolderList = getPrivateFolderList(foldersApi, "event", "1,308", "1");
        assertTrue(null != privateFolderList && 0 < privateFolderList.size());
    }

}
