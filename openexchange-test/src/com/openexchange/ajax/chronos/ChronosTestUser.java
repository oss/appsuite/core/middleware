/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.chronos;

import com.openexchange.ajax.chronos.manager.CalendarFolderManager;
import com.openexchange.ajax.chronos.manager.EventManager;
import com.openexchange.ajax.framework.SessionAwareClient;
import com.openexchange.ajax.infostore.manager.InfostoreManager;
import com.openexchange.test.common.asset.AssetManager;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.modules.ChronosApi;
import com.openexchange.testing.httpclient.modules.FoldersApi;
import com.openexchange.testing.httpclient.modules.InfostoreApi;

/**
 * {@link ChronosTestUser}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class ChronosTestUser {

    protected final TestUser testUser;

    protected final UserApi defaultUserApi;
    protected final ChronosApi chronosApi;
    protected final FoldersApi foldersApi;
    protected final String defaultFolderId;

    protected final EventManager eventManager;
    protected final AssetManager assetManager;
    protected final CalendarFolderManager folderManager;
    protected final InfostoreManager infostoreManager;

    /**
     * Initializes a new {@link ChronosTestUser}.
     * 
     * @param testUser The test user to build upon
     * @throws Exception In case client can't be
     */
    public ChronosTestUser(TestUser testUser) throws Exception {
        this.testUser = testUser;
        SessionAwareClient client = testUser.getApiClient();
        defaultUserApi = new UserApi(client, testUser);
        chronosApi = defaultUserApi.getChronosApi();
        foldersApi = defaultUserApi.getFoldersApi();
        defaultFolderId = CalendarFolderManager.getDefaultFolder(foldersApi);
        assetManager = new AssetManager();
        eventManager = new EventManager(defaultUserApi, defaultFolderId);
        folderManager = new CalendarFolderManager(defaultUserApi, foldersApi);
        infostoreManager = new InfostoreManager(new InfostoreApi(client));
    }

    /**
     * Get the underlying {@link TestUser}
     *
     * @return The user
     */
    public TestUser testUser() {
        return testUser;
    }

    /**
     * Get the {@link UserApi} based on the {@link TestUser#getApiClient()}
     *
     * @return The API
     */
    public UserApi defaultUserApi() {
        return defaultUserApi;
    }

    /**
     * Get the {@link ChronosApi} based on the {@link TestUser#getApiClient()}
     *
     * @return The API
     */
    public ChronosApi chronosApi() {
        return chronosApi;
    }

    /**
     * Get the {@link FoldersApi} based on the {@link TestUser#getApiClient()}
     *
     * @return The API
     */
    public FoldersApi foldersApi() {
        return foldersApi;
    }

    /**
     * Get the default Calendar folder identifier
     *
     * @return The default calendar
     */
    public String defaultFolderId() {
        return defaultFolderId;
    }

    /**
     * Get an {@link EventManager} based on the {@link #defaultFolderId}
     *
     * @return The manager
     */
    public EventManager eventManager() {
        return eventManager;
    }

    /**
     * Get the {@link AssetManager}
     *
     * @return The manager
     */
    public AssetManager assetManager() {
        return assetManager;
    }

    /**
     * Get the {@link CalendarFolderManager}
     *
     * @return The manager
     */
    public CalendarFolderManager folderManager() {
        return folderManager;
    }

    /**
     * Get the {@link InfostoreManager}
     *
     * @return The manager
     */
    public InfostoreManager infostoreManager() {
        return infostoreManager;
    }

    @Override
    public String toString() {
        return "ChronosTestUser [testUser=" + testUser + ", defaultFolderId=" + defaultFolderId + "]";
    }

}
