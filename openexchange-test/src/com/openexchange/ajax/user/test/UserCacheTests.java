/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.user.test;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.net.MalformedURLException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Predicate;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.admin.soap.group.dataobjects.Group;
import com.openexchange.admin.soap.user.dataobjects.Context;
import com.openexchange.admin.soap.user.dataobjects.SOAPMapEntry;
import com.openexchange.admin.soap.user.dataobjects.SOAPStringMap;
import com.openexchange.admin.soap.user.dataobjects.SOAPStringMapMap;
import com.openexchange.admin.soap.user.dataobjects.User;
import com.openexchange.ajax.framework.AbstractConfigAwareAPIClientSession;
import com.openexchange.exception.OXException;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.test.common.test.pool.soap.SoapGroupService;
import com.openexchange.test.common.test.pool.soap.SoapUserService;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.CapabilityData;
import com.openexchange.testing.httpclient.models.CommonResponse;
import com.openexchange.testing.httpclient.models.ContactData;
import com.openexchange.testing.httpclient.models.ContactUpdateResponse;
import com.openexchange.testing.httpclient.models.UserData;
import com.openexchange.testing.httpclient.modules.AddressbooksApi;
import com.openexchange.testing.httpclient.modules.CapabilitiesApi;
import com.openexchange.testing.httpclient.modules.UserApi;

/**
 * {@link UserCacheTests}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class UserCacheTests extends AbstractConfigAwareAPIClientSession {

    /** The maximum duration to wait until changes become visible (due to delayed invalidations) */
    private static final Duration MAX_AWAIT_TIME = Duration.ofSeconds(20L);

    private UserApi userApi;
    private CapabilitiesApi capabilitiesApi;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        userApi = new UserApi(getApiClient());
        capabilitiesApi = new CapabilitiesApi(getApiClient());
    }

    @Test
    public void testChangeTimezoneViaHttpAPI() throws ApiException {
        Long timestamp = userApi.getUser(Integer.toString(testUser.getUserId())).getTimestamp();
        updateUser(testUser, new UserData().timezone("Europe/Madrid"), timestamp);
        assertEquals("Europe/Madrid", getUser(testUser).getTimezone());
    }

    @Test
    public void testChangeTimezoneViaProvisioning() throws Exception {
        UserData userData = userApi.getUser(Integer.toString(testUser.getUserId())).getData();
        User user = soapUserFor(i(userData.getUserId()));
        user.setTimezone("Europe/Madrid");
        SoapUserService.getInstance().change(soapContextFor(testUser.getContextId()), user);
        assertEquals("Europe/Madrid", getUser(testUser).getTimezone());
    }

    @Test
    public void testChangeModulePermissionViaProvisioning() throws Exception {
        userApi.getUser(Integer.toString(testUser.getUserId())).getData();
        assertNotNull(findValue(capabilitiesApi.getAllCapabilities().getData(), c -> "tasks".equals(c.getId())));
        com.openexchange.test.common.test.pool.UserModuleAccess moduleAccess = new com.openexchange.test.common.test.pool.UserModuleAccess();
        moduleAccess.setTasks(Boolean.FALSE);
        SoapUserService.getInstance().changeModuleAccess(testUser.getContextId(), testUser.getUserId(), moduleAccess);
        Awaitility.await().atMost(MAX_AWAIT_TIME).untilAsserted(() -> assertNull(lookupCapability(capabilitiesApi, "tasks")));
    }

    @Test
    public void testChangeLastnameViaUserHttpAPI() throws ApiException {
        Long timestamp = userApi.getUser(Integer.toString(testUser.getUserId())).getTimestamp();
        updateUser(testUser, new UserData().lastName("Changed"), timestamp);
        assertEquals("Changed", getUser(testUser).getLastName());
    }

    @Test
    public void testChangeLastnameViaAddressbooksHttpAPI() throws ApiException {
        AddressbooksApi addressbooksApi = new AddressbooksApi(getApiClient());
        UserData userData = userApi.getUser(Integer.toString(testUser.getUserId())).getData();
        ContactUpdateResponse response = addressbooksApi.updateContactInAddressbookBuilder()
            .withTimestamp(userData.getLastModified())
            .withFolder("con://0/" + userData.getFolderId())
            .withId(userData.getContactId())
            .withContactData(new ContactData().lastName("Changed")).execute();
        checkResponse(response.getError(), response.getErrorDesc());
        assertEquals("Changed", getUser(testUser).getLastName());
    }

    @Test
    public void testChangeLastnameViaProvisioning() throws Exception {
        UserData userData = userApi.getUser(Integer.toString(testUser.getUserId())).getData();
        User user = soapUserFor(i(userData.getUserId()));
        user.setSurName("Changed");
        SoapUserService.getInstance().change(soapContextFor(testUser.getContextId()), user);
        assertEquals("Changed", getUser(testUser).getLastName());
    }

    @Test
    public void testChangeAliasesViaProvisioning() throws Exception {
        UserData userData = userApi.getUser(Integer.toString(testUser.getUserId())).getData();
        List<String> newAliases = new ArrayList<String>(userData.getAliases());
        newAliases.add("test@example.com");
        User user = soapUserFor(i(userData.getUserId()));
        user.setAliases(newAliases);
        SoapUserService.getInstance().change(soapContextFor(testUser.getContextId()), user);
        assertThat(getUser(testUser).getAliases()).containsExactlyInAnyOrderElementsOf(newAliases);
        assertThat(getUserViaSoap(testUser).getAliases()).containsExactlyInAnyOrderElementsOf(newAliases);
    }

    @Test
    public void testChangeImapServerViaProvisioning() throws Exception {
        UserData userData = userApi.getUser(Integer.toString(testUser.getUserId())).getData();
        User user = soapUserFor(i(userData.getUserId()));
        user.setImapServer("imap.example.org");
        SoapUserService.getInstance().change(soapContextFor(testUser.getContextId()), user);
        assertEquals("imap.example.org", getUserViaSoap(testUser).getImapServer());
    }

    @Test
    public void testChangeCalDAVEnabledViaProvisioning() throws Exception {
        testChangeConfigDependingCapabilityViaProvisioning("caldav", "com.openexchange.caldav.enabled");
    }

    @Test
    public void testChangeMailCategoriesEnabledViaProvisioning() throws Exception {
        testChangeConfigDependingCapabilityViaProvisioning("mail_categories", "com.openexchange.mail.categories");
    }

    @Test
    public void testChangeBirthdaysCalendarEnabledViaProvisioning() throws Exception {
        testChangeConfigDependingCapabilityViaProvisioning("calendar_birthdays", "com.openexchange.calendar.birthdays.enabled");
    }

    @Test
    public void testChangeAloneCapabilityViaProvisioning() throws Exception {
        userApi.getUser(Integer.toString(testUser.getUserId())).getData();
        assertNull(findValue(capabilitiesApi.getAllCapabilities().getData(), c -> "alone".equals(c.getId())));
        SoapUserService.getInstance().changeCapabilities(testUser.getContextId(), testUser.getUserId(), Collections.singleton("alone"), Collections.emptySet(), Collections.emptySet());
        Awaitility.await().atMost(MAX_AWAIT_TIME).untilAsserted(() -> assertNotNull(lookupCapability(capabilitiesApi, "alone")));
    }

    @Test
    public void testChangeGroupsViaProvisioning() throws Exception {
        userApi.getUser(Integer.toString(testUser.getUserId())).getData();
        Group group = SoapGroupService.getInstance().create(testUser.getContextId(), Optional.of(Arrays.asList(I(testUser.getUserId()), I(testUser2.getUserId()))));
        assertNotNull(findValue(getUser(testUser).getGroups(), v -> group.getId().equals(v)));
    }

    private void testChangeConfigDependingCapabilityViaProvisioning(String capabilityName, String propertyName) throws Exception {
        UserData userData = userApi.getUser(Integer.toString(testUser.getUserId())).getData();
        CapabilityData originalCapability = findValue(capabilitiesApi.getAllCapabilities().getData(), c -> capabilityName.equals(c.getId()));
        User user = soapUserFor(i(userData.getUserId()));
        Map<String, Map<String, String>> nestedMap = Collections.singletonMap("config", Collections.singletonMap(propertyName, null == originalCapability ? "true" : "false"));
        user.setUserAttributes(soapStringMapMapFor(nestedMap));
        SoapUserService.getInstance().change(soapContextFor(testUser.getContextId()), user);
        Awaitility.await().atMost(MAX_AWAIT_TIME).untilAsserted(() -> assertTrue((null == originalCapability) != (null == lookupCapability(capabilitiesApi, capabilityName))));
    }

    private UserData getUser(TestUser testUser) throws ApiException {
        return userApi.getUser(Integer.toString(testUser.getUserId())).getData();
    }

    private User getUserViaSoap(TestUser testUser) throws MalformedURLException, OXException {
        return SoapUserService.getInstance().get(soapContextFor(testUser.getContextId()), soapUserFor(testUser.getUserId()));
    }

    private void updateUser(TestUser testUser, UserData updatedData, Long timestamp) throws ApiException {
        CommonResponse response = userApi.updateUserBuilder().withId(Integer.toString(testUser.getUserId())).withUserData(updatedData).withTimestamp(timestamp).execute();
        checkResponse(response);
    }

    private static CapabilityData lookupCapability(CapabilitiesApi capabilitiesApi, String capabilityName) throws ApiException {
        return findValue(capabilitiesApi.getAllCapabilities().getData(), c -> capabilityName.equals(c.getId()));
    }

    private static <V> V findValue(List<V> values, Predicate<V> predicate) {
        return null == values ? null : values.stream().filter(predicate).findAny().orElse(null);
    }

    private static SOAPStringMap soapStringMapFor(Map<String, String> map) {
        List<com.openexchange.admin.soap.user.dataobjects.Entry> soapEntries = new ArrayList<>(map.size());
        for (Entry<String, String> entry : map.entrySet()) {
            com.openexchange.admin.soap.user.dataobjects.Entry soapEntry = new com.openexchange.admin.soap.user.dataobjects.Entry();
            soapEntry.setKey(entry.getKey());
            soapEntry.setValue(entry.getValue());
            soapEntries.add(soapEntry);
        }
        SOAPStringMap soapMap = new SOAPStringMap();
        soapMap.setEntries(soapEntries);
        return soapMap;
    }

    private static SOAPStringMapMap soapStringMapMapFor(Map<String, Map<String, String>> nestedMap) {
        List<SOAPMapEntry> soapMapEntries = new ArrayList<SOAPMapEntry>(nestedMap.size());
        for (Entry<String, Map<String, String>> entry : nestedMap.entrySet()) {
            SOAPMapEntry soapMapEntry = new SOAPMapEntry();
            soapMapEntry.setKey(entry.getKey());
            soapMapEntry.setValue(soapStringMapFor(entry.getValue()));
            soapMapEntries.add(soapMapEntry);
        }
        SOAPStringMapMap soapMapMap = new SOAPStringMapMap();
        soapMapMap.setEntries(soapMapEntries);
        return soapMapMap;
    }

    private static Context soapContextFor(int contextId) {
        Context context = new Context();
        context.setId(I(contextId));
        return context;
    }

    private static User soapUserFor(int userId) {
        User user = new User();
        user.setId(I(userId));
        return user;
    }

}
