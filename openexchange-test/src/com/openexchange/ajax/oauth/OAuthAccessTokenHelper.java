/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.oauth;

import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openexchange.ajax.apiclient.oauth.AccessTokenResponse;
import com.openexchange.java.Strings;
import com.openexchange.test.common.configuration.AJAXConfig;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.invoker.ApiException;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * {@link OAuthAccessTokenHelper}
 *
 * Helper class that requests OAuth grant for a test user
 * using a pre-defined client of the external authorization server.
 *
 * Requires the "Resource Owner Password Credentials Grant" oauth flow to be enabled at the authorization server.
 *
 * @author <a href="mailto:anna.ottersbach@open-xchange.com">Anna Schuerholz</a>
 */
public class OAuthAccessTokenHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(OAuthAccessTokenHelper.class);

    /**
     * Private constructor to avoid Java from adding an implicit public default constructor.
     */
    private OAuthAccessTokenHelper() {}

    /**
     * Gets an access token from the configured token endpoint.
     * Requires the "Resource Owner Password Credentials Grant" oauth flow to be enabled at the authorization server.
     *
     * @param testUser The test user.
     * @param scopes The OAuth scopes.
     * @return The access token.
     * @throws ApiException if reading configuration fails or unable to get access token.
     */
    public static String getAccessToken(TestUser testUser, List<String> scopes) throws ApiException {
        return getAccessTokenResponse(testUser, scopes).getAccessToken();
    }

    /**
     * Gets an access token response from the configured token endpoint.
     * Requires the "Resource Owner Password Credentials Grant" oauth flow to be enabled at the authorization server.
     *
     * @param testUser The test user.
     * @param scopes The OAuth scopes.
     * @return The AccessTokenResponse.
     * @throws ApiException if reading configuration fails or unable to get access token.
     */
    private static synchronized AccessTokenResponse getAccessTokenResponse(TestUser testUser, List<String> scopes) throws ApiException {
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder().add("username", testUser.getUser() + "@" + testUser.getContext())
                                                     .add("password", testUser.getPassword())
                                                     .add("grant_type", "password")
                                                     .add("scope", Strings.join(scopes, ""))
                                                     .build();

        String tokenEndpoint = AJAXConfig.getProperty(AJAXConfig.Property.OAUTH_TOKEN_ENDPOINT);
        String clientId = AJAXConfig.getProperty(AJAXConfig.Property.OAUTH_CLIENT_ID);
        String clientPW = AJAXConfig.getProperty(AJAXConfig.Property.OAUTH_CLIENT_PASSWORD);
        if (Strings.isEmpty(tokenEndpoint) || Strings.isEmpty(clientId) || Strings.isEmpty(clientPW)) {
            throw new ApiException("Invalid OAuth configuration. Missing one of the following properties: " + AJAXConfig.Property.OAUTH_TOKEN_ENDPOINT + "," + AJAXConfig.Property.OAUTH_CLIENT_ID + "," + AJAXConfig.Property.OAUTH_CLIENT_PASSWORD);
        }

        Request request = new Request.Builder().url(tokenEndpoint)
                                               .addHeader("Authorization", Credentials.basic(clientId, clientPW))
                                               .post(formBody)
                                               .build();

        AccessTokenResponse resp = null;
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                String body = response.body().string();
                response.body().close();
                throw new ApiException("Unable to get access token: " + body);
            }
            resp = new ObjectMapper().readValue(response.body().string(), AccessTokenResponse.class);
            LOGGER.trace("Received token: {}", resp.getAccessToken());
        } catch (IOException e) {
            throw new ApiException("Unable to get access token: " + e.getMessage());
        }
        return resp;
    }
}
