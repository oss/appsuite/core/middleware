/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.ajax.oauth;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.apiclient.oauth.AbstractOAuthAPIClient;
import com.openexchange.testing.httpclient.invoker.ApiClient;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.ContactSearchBody;
import com.openexchange.testing.httpclient.models.ContactsResponse;
import com.openexchange.testing.httpclient.modules.ContactsApi;

/**
 * Tests if the connection to the external authorization server
 * (configured via ajax properties) is working properly,
 * allowing access via HTTP API with the OAuth access token.
 *
 * @author <a href="mailto:anna.ottersbach@open-xchange.com">Anna Schuerholz</a>
 */
public class OAuthFlowTests extends AbstractOAuthAPIClient {

    private String testScope;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
    }

    /**
     * Tests if a test user has access to the HTTP API with an OAuth access token.
     */
    @Test
    public void testApiAccess() {
        testScope = "read_contacts";
        try {
            testApiAccess(getOAuthBasedClient());
        } catch (ApiException e) {
            fail(e.getResponseBody());
        }
    }

    /**
     * Tests if a test user has no access to the HTTP API if the OAuth access token is invalid.
     */
    @Test
    public void testFailingApiAccessWithInvalidAccessToken() {
        testScope = "read_contacts";
        ApiClient oAuthBasedClient = getOAuthBasedClient();
        oAuthBasedClient.setAccessToken("abcd1234");
        try {
            testApiAccess(oAuthBasedClient);
            fail("Expected exception");
        } catch (ApiException e) {
            String responseBody = e.getResponseBody();
            assertTrue(responseBody.contains("invalid_token"), "Expected \"invalid_token\" but was:" + responseBody);
        }
    }

    /**
     * Tests if a test user has no access to the HTTP API if access token to a non-existing OAuth scope is requested.
     */
    @Test
    public void testFailingApiAccessWithNotExistingScope() {
        testScope = "read_addresses";
        try {
            testApiAccess(getOAuthBasedClient());
            fail("Expected exception");
        } catch (ApiException e) {
            String message = e.getMessage();
            assertTrue(message.contains("invalid_scope"), "Expected \"invalid_scope\" but was:" + message);
        }
    }

    /**
     * Tests if a test user has no access to the HTTP API if the requested access token has a wrong OAuth scope.
     */
    @Test
    public void testFailingApiAccessWithWrongScope() {
        testScope = "read_files";
        try {
            testApiAccess(getOAuthBasedClient());
            fail("Expected exception");
        } catch (ApiException e) {
            String responseBody = e.getResponseBody();
            assertTrue(responseBody.contains("insufficient_scope"), "Expected \"insufficient_scope\" but was:" + responseBody);
        }
    }

    /**
     * Tests if the api client has access to the HTTP API by searching for contacts and validating if contact Berta is found.
     *
     * @param apiClient The api client.
     * @throws ApiException If fail to call the API.
     */
    private void testApiAccess(ApiClient apiClient) throws ApiException {
        ContactsApi contactsApi = new ContactsApi(apiClient);

        ContactSearchBody contactSearchBody = new ContactSearchBody();
        ContactsResponse searchContactsRespose = contactsApi.searchContacts("500", contactSearchBody, null, null, null);
        ArrayList<ArrayList<String>> searchResponse = null;
        try {
            searchResponse = (ArrayList<ArrayList<String>>) checkResponse(searchContactsRespose.getError(), searchContactsRespose.getErrorDesc(), searchContactsRespose.getData());
        } catch (ClassCastException e) {
            fail("Unexpected response: " + searchContactsRespose.getData());
        }

        assertNotNull(searchResponse, "Response is null");
        assertFalse(searchResponse.isEmpty(), "Unexpected empty response");
        boolean foundBerta = false;
        String berta = "Berta";
        for (ArrayList<String> element : searchResponse) {
            if (StringUtils.containsIgnoreCase(element.get(0), berta)) {
                foundBerta = true;
                break;
            }
        }
        assertTrue(foundBerta, "The searched contacts do not contain berta: " + searchResponse.toString());
    }

    @Override
    public List<String> getScopes() {
        return List.of(testScope);
    }

}
