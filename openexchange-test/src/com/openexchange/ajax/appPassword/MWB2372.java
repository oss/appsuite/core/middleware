/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.ajax.appPassword;

import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import com.openexchange.java.Strings;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.invoker.ApiClient;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.AppPasswordApplication;
import com.openexchange.testing.httpclient.models.AppPasswordRegistrationResponseData;
import com.openexchange.testing.httpclient.models.FolderResponse;
import com.openexchange.testing.httpclient.models.FolderUpdatesResponse;
import com.openexchange.testing.httpclient.models.FoldersResponse;
import com.openexchange.testing.httpclient.modules.FoldersApi;

/**
 * {@link MWB2372} - Folder API requests not working with "Application Specific Passwords"
 *
 * A client logged in  with an drive related "application password" is not able to perform the
 * updates request even though it is allowed to do so by configuration.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public class MWB2372 extends AbstractAppPasswordTest {

    private AppPasswordRegistrationResponseData registerPassword(String appType) throws ApiException {
        List<AppPasswordApplication> apps = getApps();
        Optional<AppPasswordApplication> driveApp = apps.stream().filter(a -> a.getName().startsWith(appType)).findFirst();
        assertTrue(driveApp.isPresent(), "drive app profile should be present.");
        AppPasswordRegistrationResponseData loginData = addPassword(driveApp.get().getName());
        return loginData;
    }

    private ApiClient login(AppPasswordRegistrationResponseData loginData) throws ApiException {
        String login = loginData.getLogin();
        String username = login.contains("@") ? login.substring(0, login.indexOf("@")) : login;
        String domain = login.contains("@") ? login.substring(login.indexOf("@") + 1) : testUser.getContext();
        TestUser test = new TestUser(username, domain, loginData.getPassword(), testUser.getCreatedBy());
        return test.generateApiClient("testclient");
    }

    private void validate(String error, String description, boolean expectSuccess) {
        if(expectSuccess) {
            checkResponse(error, description);
        }
        else {
            assertTrue(Strings.isNotEmpty(error), "Expected an error message in the reponse but got none.");
            assertTrue(Strings.isNotEmpty(description), "Expected an error description in the reponse but got none.");
        }
    }

    private void testGetUpdates(ApiClient client, String tree, String allowedModules, boolean expectSuccess) throws Exception {
        FoldersApi foldersApi = new FoldersApi(client);
        FolderUpdatesResponse response = foldersApi.getFolderUpdatesBuilder()
            .withColumns("1")
            .withAllowedModules(allowedModules)
            .withTree(tree)
            .withTimestamp(Long.valueOf(Long.MAX_VALUE))
            .execute();
        validate(response.getError(), response.getErrorDesc(), expectSuccess);
    }

    private void testGetFolder(ApiClient client, String tree, String allowedModules, boolean expectSuccess) throws Exception {
        FoldersApi foldersApi = new FoldersApi(client);
        FolderResponse response = foldersApi.getFolderBuilder()
            .withId("9")
            .withTree(tree)
            .withAllowedModules(allowedModules)
            .execute();
        validate(response.getError(), response.getErrorDesc(), expectSuccess);
    }

    private void testListFolder(ApiClient client, String tree, String allowedModules, boolean expectSuccess) throws Exception {
        FoldersApi foldersApi = new FoldersApi(client);
        FoldersResponse response = foldersApi.getSubFoldersBuilder()
            .withParent("9")
            .withColumns("1,300")
            .withTree(tree)
            .withAllowedModules(allowedModules)
            .execute();
        validate(response.getError(), response.getErrorDesc(), expectSuccess);
    }

    /*
     * Tests if sessions, which are restricted due application specific scopes, are allowed to access
     * infostore based folders
     */
    @ParameterizedTest
    @CsvSource(
    { "drive, 0, '', true", /* drive clients should be able to access infostore specific folders with tree 0 */
        "drive, 0, 'infostore', true", /* drive clients should be able to access infostore specific folders with tree 0 */
        "drive, 1, '', true", /* drive clients should be able to access infostore specific folders with tree 1 */
        "drive, 1, 'infostore', true", /* drive clients should be able to access infostore specific folders with tree 1 */
        "mail, 0, '', false", /* mail clients should not */
        "mail, 0, 'infostore', false" }) /* mail clients should not */
    public void testAccessInfostoreFolders(String appType, String tree, String allowedModules, boolean expectSuccess) throws Exception {
        AppPasswordRegistrationResponseData registrationData = registerPassword(appType);
        ApiClient apiClient = login(registrationData);
        testListFolder(apiClient, tree, Strings.isEmpty(allowedModules) ? null : allowedModules, expectSuccess);
        testGetFolder(apiClient, tree, Strings.isEmpty(allowedModules) ? null : allowedModules, expectSuccess);
        testGetUpdates(apiClient, tree, Strings.isEmpty(allowedModules) ? null : allowedModules, Strings.isEmpty(allowedModules) ? true : expectSuccess);
    }
}
