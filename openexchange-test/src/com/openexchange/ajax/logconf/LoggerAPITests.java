/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.logconf;

import org.junit.jupiter.api.Test;
import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import com.openexchange.testing.restclient.invoker.ApiException;
import com.openexchange.testing.restclient.models.ContextLoggers;
import com.openexchange.testing.restclient.models.LogLevel;
import com.openexchange.testing.restclient.models.Logger;
import com.openexchange.testing.restclient.models.SessionLoggers;
import com.openexchange.testing.restclient.models.UsersLoggers;

/**
 * {@link LoggerAPITests} - Tests for the logconf admin REST API
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public class LoggerAPITests extends AbstractLogconfTest {

    private static final String INVALID_LOG_LEVEL = "sidgCdW";
    private static final String UNKNOWN_LOGGER = "com.openexchange.sidgcdw";

    @Test
    public void testGetSystemLoggers() throws Exception {
        List<Logger> systemLoggers = getAdminAPI().listSystemLoggers();
        assertFalse(systemLoggers.isEmpty(),  "At least one system logger should have been returned.");
    }

    @Test
    public void testSetAndGetSystemLoggerLogLevel() throws Exception  {
        LogLevel setSystemLogLevel = getAdminAPI().setSystemLogLevel("com.openexchange.drive", JSONObject.quote("DEBUG"));
        assertEquals(LogLevel.DEBUG, setSystemLogLevel, "The new log level should have been returned.");
        LogLevel changedLevel = getAdminAPI().getSystemLogger("com.openexchange.drive");
        assertEquals(LogLevel.DEBUG, changedLevel, "The loglevel should have been changed.");
    }

    @Test
    public void testGetUnknownSystemLoggerShouldReturn404() {
        ApiException error = Assertions.assertThrows(ApiException.class, () -> {
            getAdminAPI().getSystemLogger(UNKNOWN_LOGGER);
        });
        assertEquals(404, error.getCode(), "The response code should have been 404");
    }

    @Test
    public void testSetLevelForUnknownSystemLoggerShouldReturn404() {
        ApiException error = Assertions.assertThrows(ApiException.class, () -> {
            getAdminAPI().setSystemLogLevel(UNKNOWN_LOGGER, JSONObject.quote("DEBUG"));
        });
        assertEquals(404, error.getCode(), "The response code should have been 404");
    }

    @Test
    public void testSetInvalidLogLevelForSystemLoggerShouldReturn400() {
        ApiException error = Assertions.assertThrows(ApiException.class, () -> {
            getAdminAPI().setSystemLogLevel("com.openexchange.drive", JSONObject.quote(INVALID_LOG_LEVEL));
        });
        assertEquals(400, error.getCode(), "The response code should have been 400");
    }

    @Test
    public void testSetAndGetContextLoggers() throws Exception {
        final Integer cid = I(getApiClient().getUser().getContextId());

        //Set two loggers for the given context
        var loggerOne = new Logger();
        loggerOne.setName("com.openexchange.drive");
        loggerOne.setLevel(LogLevel.DEBUG);
        var loggerTwo = new Logger();
        loggerTwo.setName("com.openexchange.multifactor");
        loggerTwo.setLevel(LogLevel.TRACE);
        List<Logger> contextLoggers = getAdminAPI().setContextLoggers(cid, Arrays.asList(loggerOne, loggerTwo));
        assertTrue(contextLoggers.contains(loggerOne), "The new logger should have been returned");
        assertTrue(contextLoggers.contains(loggerTwo), "The new logger should have been returned");

        //List the two loggers
        contextLoggers  = getAdminAPI().listContextLoggers(cid);
        assertTrue(contextLoggers.contains(loggerOne), "The new logger should have been returned");
        assertTrue(contextLoggers.contains(loggerTwo), "The new logger should have been returned");

        //Get the log levels
        LogLevel contextLogger = getAdminAPI().getContextLogger(cid, "com.openexchange.drive");
        assertEquals(LogLevel.DEBUG, contextLogger, "The loglevel should be correct");
        contextLogger = getAdminAPI().getContextLogger(cid, "com.openexchange.multifactor");
        assertEquals(LogLevel.TRACE, contextLogger, "The loglevel should be correct");
    }

    @Test
    public void testSetAndDeleteContextLoggers() throws Exception {
        final Integer cid = I(getApiClient().getUser().getContextId());

        //Setup one logger for the context
        var loggerOne = new Logger();
        loggerOne.setName("com.openexchange.drive");
        loggerOne.setLevel(LogLevel.DEBUG);
        List<Logger> contextLoggers = getAdminAPI().setContextLoggers(cid, Collections.singletonList(loggerOne));
        assertTrue(contextLoggers.contains(loggerOne), "The new logger should have been returned");

        //List the new logger
        contextLoggers  = getAdminAPI().listContextLoggers(cid);
        assertTrue(contextLoggers.contains(loggerOne), "The new logger should have been returned");

        //Delete the new loggers
        getAdminAPI().deleteContextLogger(cid, "com.openexchange.drive");

        //And check again if it was removed
        contextLoggers  = getAdminAPI().listContextLoggers(cid);
        assertFalse(contextLoggers.contains(loggerOne), "The new logger should not be present anymore");
    }

    @Test
    public void testGetUnknownContextLoggerShouldReturn404() throws Exception {
        final Integer cid = I(getApiClient().getUser().getContextId());
        ApiException error = Assertions.assertThrows(ApiException.class, () -> {
            getAdminAPI().getContextLogger(cid, UNKNOWN_LOGGER);
        });
        assertEquals(404, error.getCode(), "The response code should have been 404");
    }

    @Test
    public void testSetLoggerForUnknownContextShouldReturn404() {
        ApiException error = Assertions.assertThrows(ApiException.class, () -> {
            getAdminAPI().setContextLogLevel(I(-1), "com.openexchange.drive", JSONObject.quote("DEBUG"));
        });
        assertEquals(404, error.getCode(), "The response code should have been 404");
    }

    @Test
    public void testSetInvalidLogLevelForContextLoggerShouldReturn400() throws Exception {
        final Integer cid = I(getApiClient().getUser().getContextId());
        ApiException error = Assertions.assertThrows(ApiException.class, () -> {
            getAdminAPI().setContextLogLevel(cid, "com.openexchange.drive", JSONObject.quote(INVALID_LOG_LEVEL));
        });
        assertEquals(400, error.getCode(), "The response code should have been 400");
    }

    @Test
    public void testListAllContextLoggers() throws Exception {
        final Integer cid = I(getApiClient().getUser().getContextId());
        getAdminAPI().setContextLogLevel(cid, "com.openexchange.drive", JSONObject.quote("DEBUG"));
        getAdminAPI().setContextLogLevel(cid, "com.openexchange.mail", JSONObject.quote("DEBUG"));
        List<ContextLoggers> all = getAdminAPI().listAllContextsLoggers();
        Optional<ContextLoggers> contextLoggerCollection = all.stream().filter(l -> l.getContextId().equals(cid.toString())).findFirst();
        assertTrue(contextLoggerCollection.isPresent(), "The logger collection must be present");
        assertEquals(2, contextLoggerCollection.get().getLoggers().size(), "There should be two logger set");
        getAdminAPI().deleteContextLoggers(cid);
    }

    @Test
    public void testListAllContextLoggerIsEmptyIfNoLoggerIsSet() throws Exception {
        final Integer cid = I(getApiClient().getUser().getContextId());
        getAdminAPI().deleteContextLoggers(cid);
        List<ContextLoggers> all = getAdminAPI().listAllContextsLoggers();
        List<ContextLoggers> contextLoggers = all.stream().filter(l -> l.getContextId().equals(cid.toString())).toList();
        assertTrue(contextLoggers.isEmpty(), "There should be no context logger set");
    }

    @Test
    public void testSetAndGetUserLoggers() throws Exception {
        final Integer userId = I(getApiClient().getUser().getUserId());
        final Integer cid = I(getApiClient().getUser().getContextId());

        //Set two loggers for the given user
        var loggerOne = new Logger();
        loggerOne.setName("com.openexchange.drive");
        loggerOne.setLevel(LogLevel.DEBUG);
        var loggerTwo = new Logger();
        loggerTwo.setName("com.openexchange.multifactor");
        loggerTwo.setLevel(LogLevel.TRACE);
        List<Logger> userLoggers = getAdminAPI().setUserLoggers(cid, userId, Arrays.asList(loggerOne, loggerTwo));
        assertTrue(userLoggers.contains(loggerOne), "The new logger should have been returned");
        assertTrue(userLoggers.contains(loggerTwo), "The new logger should have been returned");

        //List the two loggers
        userLoggers  = getAdminAPI().listUserLoggers(cid, userId);
        assertTrue(userLoggers.contains(loggerOne), "The new logger should have been returned");
        assertTrue(userLoggers.contains(loggerTwo), "The new logger should have been returned");

        //Get the log levels
        LogLevel contextLogger = getAdminAPI().getUserLogger(cid, userId, "com.openexchange.drive");
        assertEquals(LogLevel.DEBUG, contextLogger, "The loglevel should be correct");
        contextLogger = getAdminAPI().getUserLogger(cid, userId, "com.openexchange.multifactor");
        assertEquals(LogLevel.TRACE, contextLogger, "The loglevel should be correct");
    }

    @Test
    public void testListAllUsersLogger() throws Exception {
        final Integer cid = I(getApiClient().getUser().getContextId());
        final Integer userId = I(getApiClient().getUser().getUserId());
        getAdminAPI().setUserLogLevel(cid, userId, "com.openexchange.drive", JSONObject.quote("DEBUG"));
        getAdminAPI().setUserLogLevel(cid, userId, "com.openexchange.mail", JSONObject.quote("DEBUG"));
        List<UsersLoggers> all = getAdminAPI().listAllUserLoggers(cid);
        Optional<UsersLoggers> userLoggerCollection = all.stream().filter(l -> l.getUserId().equals(userId.toString())).findFirst();
        assertTrue(userLoggerCollection.isPresent(), "The logger collection must be present");
        assertEquals(2, userLoggerCollection.get().getLoggers().size(), "There should be two logger set");
        getAdminAPI().deleteUserLoggers(cid, userId);
    }

    @Test
    public void testListAllUsersLoggerIsEmptyIfNoLoggerIsSet() throws Exception {
        final Integer cid = I(getApiClient().getUser().getContextId());
        final Integer userId = I(getApiClient().getUser().getUserId());
        getAdminAPI().deleteUserLoggers(cid, userId);
        List<UsersLoggers> all = getAdminAPI().listAllUserLoggers(cid);
        List<UsersLoggers> usersLoggers = all.stream().filter(l -> l.getUserId().equals(cid.toString())).toList();
        assertTrue(usersLoggers.isEmpty(), "There should be no context logger set");
    }

    @Test
    public void testSetAndDeleteUserLoggers() throws Exception {
        final Integer userId = I(getApiClient().getUser().getUserId());
        final Integer cid = I(getApiClient().getUser().getContextId());

        //Setup one logger for the user
        var loggerOne = new Logger();
        loggerOne.setName("com.openexchange.drive");
        loggerOne.setLevel(LogLevel.DEBUG);
        List<Logger> userLoggers = getAdminAPI().setUserLoggers(cid, userId, Collections.singletonList(loggerOne));
        assertTrue(userLoggers.contains(loggerOne), "The new logger should have been returned");

        //List the new logger
        userLoggers  = getAdminAPI().listUserLoggers(cid, userId);
        assertTrue(userLoggers.contains(loggerOne), "The new logger should have been returned");

        //Delete the new loggers
        getAdminAPI().deleteUserLogger(cid, userId, "com.openexchange.drive");

        //And check again if it was removed
        userLoggers  = getAdminAPI().listUserLoggers(cid, userId);
        assertFalse(userLoggers.contains(loggerOne), "The new logger should not be present anymore");
    }

    @Test
    public void testGetUnknownUserLoggerShouldReturn404() throws Exception {
        final Integer userId = I(getApiClient().getUser().getUserId());
        final Integer cid = I(getApiClient().getUser().getContextId());
        ApiException error = Assertions.assertThrows(ApiException.class, () -> {
            getAdminAPI().getUserLogger(cid, userId, UNKNOWN_LOGGER);
        });
        assertEquals(404, error.getCode(), "The response code should have been 404");
    }

    @Test
    public void testSetLoggerForUnknownUserShouldReturn404() throws Exception {
        final Integer cid = I(getApiClient().getUser().getContextId());

        ApiException error = Assertions.assertThrows(ApiException.class, () -> {
            getAdminAPI().setUserLogLevel(cid, I(-1), "com.openexchange.drive", JSONObject.quote("DEBUG"));
        });
        assertEquals(404, error.getCode(), "The response code should have been 404");
    }

    @Test
    public void testSetInvalidLogLevelForUserLoggerShouldReturn400() throws Exception {
        final Integer userId = I(getApiClient().getUser().getUserId());
        final Integer cid = I(getApiClient().getUser().getContextId());
        ApiException error = Assertions.assertThrows(ApiException.class, () -> {
            getAdminAPI().setUserLogLevel(cid, userId, "com.openexchange.drive", JSONObject.quote(INVALID_LOG_LEVEL));
        });
        assertEquals(400, error.getCode(), "The response code should have been 400");
    }

    @Test
    public void testSetAndGetSessionLoggers() throws Exception {
        String sessionId = getApiClient().getSession();

        //Set two loggers for the given session
        var loggerOne = new Logger();
        loggerOne.setName("com.openexchange.drive");
        loggerOne.setLevel(LogLevel.DEBUG);
        var loggerTwo = new Logger();
        loggerTwo.setName("com.openexchange.multifactor");
        loggerTwo.setLevel(LogLevel.TRACE);
        List<Logger> sessionLoggers = getAdminAPI().setSessionLoggers(sessionId, Arrays.asList(loggerOne, loggerTwo));
        assertTrue(sessionLoggers.contains(loggerOne), "The new logger should have been returned");
        assertTrue(sessionLoggers.contains(loggerTwo), "The new logger should have been returned");

        //List the two loggers
        sessionLoggers  = getAdminAPI().listSessionLoggers(sessionId);
        assertTrue(sessionLoggers.contains(loggerOne), "The new logger should have been returned");
        assertTrue(sessionLoggers.contains(loggerTwo), "The new logger should have been returned");

        //Get the log levels
        LogLevel contextLogger = getAdminAPI().getSessionLogger(sessionId, "com.openexchange.drive");
        assertEquals(LogLevel.DEBUG, contextLogger, "The loglevel should be correct");
        contextLogger = getAdminAPI().getSessionLogger(sessionId, "com.openexchange.multifactor");
        assertEquals(LogLevel.TRACE, contextLogger, "The loglevel should be correct");
    }

    @Test
    public void testListAllSessionLoggers() throws Exception {
        String sessionId = getApiClient().getSession();
        getAdminAPI().setSessionLogLevel(sessionId, "com.openexchange.drive", JSONObject.quote("DEBUG"));
        getAdminAPI().setSessionLogLevel(sessionId, "com.openexchange.mail", JSONObject.quote("DEBUG"));
        List<SessionLoggers> all = getAdminAPI().listAllSessiosLoggers();
        Optional<SessionLoggers> sessionLoggerCollection = all.stream().filter(l -> l.getSessionId().equals(sessionId)).findFirst();
        assertTrue(sessionLoggerCollection.isPresent(), "The logger collection must be present");
        assertEquals(2, sessionLoggerCollection.get().getLoggers().size(), "There should be two logger set");
        getAdminAPI().deleteSessionLoggers(sessionId);
    }

    @Test
    public void testListAllSessionsLoggerIsEmptyIfNoLoggerIsSet() throws Exception {
        String sessionId = getApiClient().getSession();
        getAdminAPI().deleteSessionLoggers(sessionId);
        List<SessionLoggers> all = getAdminAPI().listAllSessiosLoggers();
        List<SessionLoggers> sessionLoggers = all.stream().filter(l -> l.getSessionId().equals(sessionId.toString())).toList();
        assertTrue(sessionLoggers.isEmpty(), "There should be no context logger set");
    }

    @Test
    public void testSetAndDeleteSessionLoggers() throws Exception {
        final String sessionId = getApiClient().getSession();

        //Setup one logger for the session
        var loggerOne = new Logger();
        loggerOne.setName("com.openexchange.drive");
        loggerOne.setLevel(LogLevel.DEBUG);
        List<Logger> sessonLoggers = getAdminAPI().setSessionLoggers(sessionId, Collections.singletonList(loggerOne));
        assertTrue(sessonLoggers.contains(loggerOne), "The new logger should have been returned");

        //List the new logger
        sessonLoggers  = getAdminAPI().listSessionLoggers(sessionId);
        assertTrue(sessonLoggers.contains(loggerOne), "The new logger should have been returned");

        //Delete the new loggers
        getAdminAPI().deleteSessionLogger(sessionId, "com.openexchange.drive");

        //And check again if it was removed
        sessonLoggers  = getAdminAPI().listSessionLoggers(sessionId);
        assertFalse(sessonLoggers.contains(loggerOne), "The new logger should not be present anymore");
    }

    @Test
    public void testGetUnknownSessionLoggerShouldReturn404() throws Exception {
        final String sessionId = getApiClient().getSession();
        ApiException error = Assertions.assertThrows(ApiException.class, () -> {
            getAdminAPI().getSessionLogger(sessionId, UNKNOWN_LOGGER);
        });
        assertEquals(404, error.getCode(), "The response code should have been 404");
    }

    @Test
    public void testSetLoggerForUnknownSessionShouldReturn404() {
        ApiException error = Assertions.assertThrows(ApiException.class, () -> {
            getAdminAPI().setSessionLogLevel("invalidSessionID", "com.openexchange.drive", JSONObject.quote("DEBUG"));
        });
        assertEquals(404, error.getCode(), "The response code should have been 404");
    }

    @Test
    public void testSetInvalidLogLevelForSessionLoggerShouldReturn400() throws Exception {
        final String sessionId = getApiClient().getSession();
        ApiException error = Assertions.assertThrows(ApiException.class, () -> {
            getAdminAPI().setSessionLogLevel(sessionId, "com.openexchange.drive", JSONObject.quote(INVALID_LOG_LEVEL));
        });
        assertEquals(400, error.getCode(), "The response code should have been 400");
    }
}
