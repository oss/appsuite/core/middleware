/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax.logconf;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.openexchange.testing.restclient.models.ExceptionCategory;
import com.openexchange.tools.arrays.Arrays;

/**
 * {@link SuppressedExceptionCategoriesTests} - Tests management of suppressed exception categories via the logconf admin REST API
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public class SuppressedExceptionCategoriesTests extends AbstractLogconfTest {

    @Test
    public void testSuppressedExceptionCategories() throws Exception {

        //Test initial Get
        List<ExceptionCategory> originalCategories = getAdminAPI().listSuppressedExceptionCategories();

        try {
            //Ensure at least one category is set
            List<ExceptionCategory> setExceptionCategories = getAdminAPI().setSuppressedExceptionCategories(Collections.singletonList(ExceptionCategory.USER_INPUT));
            assertTrue(setExceptionCategories.contains(ExceptionCategory.USER_INPUT), "The exception categories should have been returned");

            //Test Get
            List<ExceptionCategory> exceptionCategories = getAdminAPI().listSuppressedExceptionCategories();
            assertFalse(exceptionCategories.isEmpty(), "At least one suppressed exception categorie should have been returned.");

            //Test delete
            getAdminAPI().deleteSuppressedExceptionCategories();
            exceptionCategories = getAdminAPI().listSuppressedExceptionCategories();
            assertTrue(exceptionCategories.isEmpty(), "All suppressed exception categorie should have been deleted.");

            //Test to add everything
            setExceptionCategories = getAdminAPI().setSuppressedExceptionCategories(Arrays.toList(ExceptionCategory.values()));
            assertEquals(setExceptionCategories.size(), ExceptionCategory.values().length, "The amount of categories should match.");
            for (var c : ExceptionCategory.values()) {
                assertTrue(setExceptionCategories.contains(c), "The exception categories should have been returned.");
            }
        } finally {
            //We try to reset the initial state, if possible
            getAdminAPI().setSuppressedExceptionCategories(originalCategories);
        }
    }
}
