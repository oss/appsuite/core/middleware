/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.ajax.logconf;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.b;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.InfoItemUpdatesResponse;
import com.openexchange.testing.httpclient.modules.InfostoreApi;

/**
 * {@link IncludeStacktraceOnErrorTests}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public class IncludeStacktraceOnErrorTests extends AbstractLogconfTest {

    private InfostoreApi infostoreApi;
    private static final String INVALID_FOLDER_ID = "sidgCdW";

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        infostoreApi = new InfostoreApi(getApiClient());
    }

    /**
     * Internal method to provoke an error and return the stack trace, if included in the error response
     *
     * @return A stack trace as list, or <code>null</code> if no stack trace was returned
     * @throws ApiException
     */
    private List<String> provokeAnError() throws ApiException {
        InfoItemUpdatesResponse response = infostoreApi.getInfoItemUpdatesBuilder()
                                                       .withFolder(INVALID_FOLDER_ID)
                                                       .withColumns("1")
                                                       .execute();
        return response.getErrorStack();
    }

    @Test
    public void testGetSetAndTestIncludeStackTraceOnError() throws Exception {
        final Integer userId = I(getApiClient().getUser().getUserId());
        final Integer cid = I(getApiClient().getUser().getContextId());

        //Enable stack trace on error
        getAdminAPI().setIncludeOnError(cid, userId, Boolean.TRUE);

        //Check if set
        boolean strackTraceEnabled = b(getAdminAPI().getIncludeOnError(cid, userId));
        assertTrue(strackTraceEnabled, "The includeStackTraceOnError option should have been enabled for the user");

        //Provoke another error and check that the stack trace is present
        var stackTrace = provokeAnError();
        assertNotNull(stackTrace, "The stack trace should be present in the error response");

        //Disable stack trace on error
        getAdminAPI().setIncludeOnError(cid, userId, Boolean.FALSE);

        //Disabling this option for a single user does not necessarily mean that no stack trace gets returned:
        //It could be enabled globally via server configuration.
        //However, the setting should have been changed for the user:
        strackTraceEnabled = b(getAdminAPI().getIncludeOnError(cid, userId));
        assertFalse(strackTraceEnabled, "The includeStackTraceOnError option should have been disabled for the user");
    }

    @Test
    public void testGetIncludeStackTraceOnErrorForUnknownContextShouldReturn404() throws ApiException {
        final Integer userId = I(getApiClient().getUser().getUserId());
        var error = Assertions.assertThrows(com.openexchange.testing.restclient.invoker.ApiException.class, () -> {
            getAdminAPI().getIncludeOnError(I(-1), userId);
        });
        assertEquals(404, error.getCode(), "The response code should have been 404");
    }

    @Test
    public void testGetIncludeStackTraceOnErrorForUnknownUserShouldReturn404() throws ApiException {
        final Integer cid = I(getApiClient().getUser().getContextId());
        var error = Assertions.assertThrows(com.openexchange.testing.restclient.invoker.ApiException.class, () -> {
            getAdminAPI().getIncludeOnError(cid, I(-1));
        });
        assertEquals(404, error.getCode(), "The response code should have been 404");
    }

    @Test
    public void testSetIncludeStackTraceOnErrorForUnknownContextShouldReturn404() throws ApiException {
        final Integer userId = I(getApiClient().getUser().getUserId());
        var error = Assertions.assertThrows(com.openexchange.testing.restclient.invoker.ApiException.class, () -> {
            getAdminAPI().setIncludeOnError(I(-1), userId, Boolean.TRUE);
        });
        assertEquals(404, error.getCode(), "The response code should have been 404");
    }

    @Test
    public void testSetIncludeStackTraceOnErrorForUnknownUserShouldReturn404() throws ApiException {
        final Integer cid = I(getApiClient().getUser().getContextId());
        var error = Assertions.assertThrows(com.openexchange.testing.restclient.invoker.ApiException.class, () -> {
            getAdminAPI().setIncludeOnError(cid, I(-1), Boolean.TRUE);
        });
        assertEquals(404, error.getCode(), "The response code should have been 404");
    }
}
