/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.contact;

import static com.openexchange.ajax.contact.AbstractApiClientContactTest.ContactColoumns.DISPLAY_NAME;
import static com.openexchange.ajax.contact.AbstractApiClientContactTest.ContactColoumns.FIST_NAME;
import static com.openexchange.ajax.contact.AbstractApiClientContactTest.ContactColoumns.ID;
import static com.openexchange.ajax.contact.AbstractApiClientContactTest.ContactColoumns.LAST_NAME;
import static com.openexchange.ajax.contact.AbstractApiClientContactTest.ContactColoumns.MARK_AS_DISTRIBUTIONLIST;
import static com.openexchange.ajax.contact.AbstractApiClientContactTest.ContactColoumns.SORT_NAME;
import static com.openexchange.ajax.contact.AbstractApiClientContactTest.ContactColoumns.SPECIAL_SORTING_FIRST_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import com.openexchange.testing.httpclient.models.ContactData;
import com.openexchange.testing.httpclient.models.ContactsResponse;
import com.openexchange.testing.httpclient.models.DistributionListMember;
import com.openexchange.testing.httpclient.models.DistributionListMember.MailFieldEnum;

/**
 * 
 * {@link DistListSortingTest}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class DistListSortingTest extends AbstractApiClientContactTest {

    /**
     * The DistributionListSortingTest.java.
     */
    private static final int SIZE = 10;
    private LinkedList<ContactData> distLists = new LinkedList<>();

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);

        for (int i = 0; i < SIZE; i++) {
            // Create contact 1
            ContactData distListMember1 = createContactObject(getPrefix(i) + "_First Contact");
            distListMember1.setFirstName(getPrefix(i) + "First");
            distListMember1.setLastName(getPrefix(i) + "Last");
            String distListMemberId1 = createContact(distListMember1);
            distListMember1.setId(distListMemberId1);
            // Create contact 1
            ContactData distListMember2 = createContactObject(getPrefix(i) + "_Second Contact");
            distListMember2.setFirstName(getPrefix(i) + "Second");
            distListMember2.setLastName(getPrefix(i) + "Lastname");
            String distListMemberId2 = createContact(distListMember2);
            distListMember2.setId(distListMemberId2);
            // Create distribution list with both new contacts
            ContactData distList = new ContactData();
            distList.setDisplayName(getPrefix(i) + "DistributionList");
            distList.setMarkAsDistributionlist(Boolean.TRUE);
            List<DistributionListMember> members = new ArrayList<>();
            members.add(getMemberFromContact(distListMember2));
            members.add(getMemberFromContact(distListMember1));
            distList.setDistributionList(members);
            distList.setFolderId(contactFolderId);
            distList.setId(createContact(distList));
            distLists.add(distList);
        }
    }

    @ParameterizedTest
    @ValueSource(strings =
    { "asc", "desc" })
    public void testSortingBySortName(String sort) throws Exception {
        ContactsResponse allContacts = contactsApi.getAllContactsBuilder()
                                                  .withFolder(contactFolderId)
                                                  .withColumns(ContactColoumns.getColoumns(ID, DISPLAY_NAME, MARK_AS_DISTRIBUTIONLIST))
                                                  .withSort(SORT_NAME.getCol())
                                                  .withOrder(sort)
                                                  .execute();
        ArrayList<?> contacts = extractContacts(allContacts);
        assertEquals(SIZE * 3, contacts.size());
        int j = 0;
        for (int i = 0; i < contacts.size(); i++) {
            ArrayList<?> contactData = getContactData(contacts.get(i));
            assertEquals(3, contactData.size());
            if (Boolean.FALSE.equals(contactData.get(2))) {
                continue; // Ignore "normal" contacts
            }
            /*
             * Expect the list to be sorted alphabetically by display name of the distribution list
             */
            ContactData expectedDistList = distLists.get("asc".equals(sort) ? SIZE - ++j : j++);
            assertEquals(expectedDistList.getId(), contactData.get(0), "Wrong sort order at element " + i);
            assertEquals(expectedDistList.getDisplayName(), contactData.get(1));
        }
    }

    @ParameterizedTest
    @ValueSource(strings =
    { "asc", "desc" })
    public void testSortingBySpecialSortingFirstNname(String sort) throws Exception {
        ContactsResponse allContacts = contactsApi.getAllContactsBuilder()
                                                  .withFolder(contactFolderId)
                                                  .withColumns(ContactColoumns.getColoumns(ID, DISPLAY_NAME, MARK_AS_DISTRIBUTIONLIST, FIST_NAME, LAST_NAME))
                                                  .withSort(SPECIAL_SORTING_FIRST_NAME.getCol())
                                                  .withOrder(sort)
                                                  .execute();
        ArrayList<?> contacts = extractContacts(allContacts);
        assertEquals(SIZE * 3, contacts.size());
        int j = 0;
        for (int i = 0; i < contacts.size(); i++) {
            ArrayList<?> contactData = getContactData(contacts.get(i));
            assertEquals(5, contactData.size());
            //@formatter:off
            /*
             * Expect to be sorted by displanme
             * |    asc    |    desc   |
             * |-----------|-----------|
             * | aDistList |    ...    |
             * |   aFirst  |  aSecond  |
             * |  aSecond  |  aFirst   |
             * |    ...    | aDistList |
             */
            //@formatter:on
            if (("asc".equals(sort) && (0 == i % 3)) || ("desc".equals(sort) && (2 == i % 3))) {
                assertTrue(Boolean.TRUE.equals(contactData.get(2)));
                ContactData expectedDistList = distLists.get("asc".equals(sort) ? SIZE - ++j : j++);
                assertEquals(expectedDistList.getId(), contactData.get(0), "Wrong sort order at element " + i);
                assertEquals(expectedDistList.getDisplayName(), contactData.get(1));
            } else {
                assertTrue(Boolean.FALSE.equals(contactData.get(2)));
            }

        }
    }

    @ParameterizedTest
    @ValueSource(strings =
    { "asc", "desc" })
    public void testSortingByFirstName(String sort) throws Exception {
        ContactsResponse allContacts = contactsApi.getAllContactsBuilder()
                                                  .withFolder(contactFolderId)
                                                  .withColumns(ContactColoumns.getColoumns(ID, DISPLAY_NAME, MARK_AS_DISTRIBUTIONLIST))
                                                  .withSort(FIST_NAME.getCol())
                                                  .withOrder(sort)
                                                  .execute();
        ArrayList<?> contacts = extractContacts(allContacts);
        assertEquals(SIZE * 3, contacts.size());
        for (int i = 0; i < contacts.size(); i++) {
            ArrayList<?> contactData = getContactData(contacts.get(i));
            assertEquals(3, contactData.size());
            if (Boolean.FALSE.equals(contactData.get(2))) {
                /*
                 * Check that contacts with first name will be returned before contacts without
                 */
                assertTrue(i < SIZE * 2);
                continue; // Ignore "normal" contacts
            }
            /*
             * Check that distribution lists without first name are sorted last
             */
            assertTrue(i >= SIZE * 2);
            /*
             * Expect the entries to be sorted by IDs of the distribution list
             */
            ContactData expectedDistList = distLists.get(i - (SIZE * 2));
            assertEquals(expectedDistList.getId(), contactData.get(0), "Wrong sort order at element " + i);
            assertEquals(expectedDistList.getDisplayName(), contactData.get(1));
        }
    }

    /*
     * ============================== HELPERS ==============================
     */

    private String getPrefix(int i) {
        return String.valueOf((char) (106 - i));
    }

    private DistributionListMember getMemberFromContact(ContactData con) {
        DistributionListMember result = new DistributionListMember();
        result.setDisplayName(con.getDisplayName());
        result.setFolderId(con.getFolderId());
        result.setId(con.getId());
        result.setMail(con.getEmail1());
        result.setMailField(MailFieldEnum.NUMBER_1);
        return result;
    }

    private ArrayList<?> extractContacts(ContactsResponse allContacts) {
        Object data = checkResponse(allContacts.getError(), allContacts.getErrorDesc(), allContacts.getData());
        assertTrue(data instanceof ArrayList<?>, "Wrong response type. Expected ArrayList but received: " + data.getClass().getSimpleName());
        ArrayList<?> contacts = (ArrayList<?>) data;
        assertFalse(contacts.isEmpty(), "No contacts found.");
        return contacts;
    }

    private ArrayList<?> getContactData(Object contact) {
        assertTrue(contact instanceof ArrayList<?>, "Wrong contact data type. Expected ArrayList but was: " + contact.getClass().getSimpleName());
        ArrayList<?> contactData = (ArrayList<?>) contact;
        assertFalse(contactData.isEmpty(), "No contact data found.");
        return contactData;
    }

}
