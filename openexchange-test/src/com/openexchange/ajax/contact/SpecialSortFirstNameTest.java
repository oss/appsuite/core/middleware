/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.contact;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.framework.AbstractAPIClientSession;
import com.openexchange.groupware.container.Contact;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.CommonResponse;
import com.openexchange.testing.httpclient.models.ContactData;
import com.openexchange.testing.httpclient.models.ContactUpdateResponse;
import com.openexchange.testing.httpclient.models.ContactsResponse;
import com.openexchange.testing.httpclient.models.UserData;
import com.openexchange.testing.httpclient.models.UserResponse;
import com.openexchange.testing.httpclient.modules.AddressbooksApi;
import com.openexchange.testing.httpclient.modules.UserApi;

/**
 * {@link SpecialSortFirstNameTest}
 *
 * @author <a href="mailto:anna.ottersbach@open-xchange.com">Anna Schuerholz</a>
 * @since v8
 */
public class SpecialSortFirstNameTest extends AbstractAPIClientSession {

    private static final String ID_GIVEN_NAME = "501";
    private static final String ID_SPECIAL_SORTING_FIRST_NAME = "623";
    private static final String ID_SPECIAL_SORTING_LAST_NAME = "607";
    private static final String ID_SUR_NAME = "502";
    private static final String ID_YOMI_FIRST_NAME = "616";
    private static final String ID_YOMI_LAST_NAME = "617";
    private static final String[] orderedYOMIFirstNames = new String[] { "\u306f", "\u3070", "\u3072", "\u3073" };
    private static final String[] orderedYOMILastNames = new String[] { "\u307e", "\u307f", "\u3080", "\u3081" };
    private AddressbooksApi addressbooksApi;
    private Contact[] contactsSortedFirstNames;
    private Contact[] contactsSortedLastNames;
    private Contact[] contactsSortedYOMIFirstNames;
    private Contact[] contactsSortedYOMILastNames;
    private String privateContactFolderId;
    private UserApi userApi;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        addressbooksApi = new AddressbooksApi(getApiClient());
        privateContactFolderId = I(getClient().getValues().getPrivateContactFolder()).toString();
    }

    @Test
    public void testSpecialSortingFirstName() throws Exception {
        generateAndCreateContacts();
        String columns = ID_GIVEN_NAME + "," + ID_SPECIAL_SORTING_FIRST_NAME;
        String sort = ID_SPECIAL_SORTING_FIRST_NAME;
        ArrayList<ArrayList<Object>> contacts = requestContacts(columns, sort, "ASC");
        assertEquals(contacts.size(), contactsSortedFirstNames.length, "Wrong number of contacts received");
        for (int i = 0; i < contactsSortedFirstNames.length; i++) {
            ArrayList<Object> contact = contacts.get(i);
            assertEquals(contact.get(0).toString(), contactsSortedFirstNames[i].getGivenName(), "Unexpected sorting.");
        }
    }

    @Test
    public void testSpecialSortingFirstNameDescending() throws Exception {
        generateAndCreateContacts();

        String columns = ID_GIVEN_NAME + "," + ID_SPECIAL_SORTING_FIRST_NAME;
        String sort = ID_SPECIAL_SORTING_FIRST_NAME;
        ArrayList<ArrayList<Object>> contacts = requestContacts(columns, sort, "DESC");
        assertEquals(contacts.size(), contactsSortedFirstNames.length, "Wrong number of contacts received");
        for (int i = 0; i < contactsSortedFirstNames.length; i++) {
            ArrayList<Object> contact = contacts.get(i);
            assertEquals(contact.get(0).toString(), contactsSortedFirstNames[contactsSortedFirstNames.length - 1 - i].getGivenName(), "Unexpected sorting.");
        }
    }

    @Test
    public void testSpecialSortingFirstNameLocaleJapan() throws Exception {
        generateAndCreateContacts();
        setJapanLocale();

        String columns = ID_YOMI_FIRST_NAME + "," + ID_SPECIAL_SORTING_FIRST_NAME;
        String sort = ID_SPECIAL_SORTING_FIRST_NAME;
        ArrayList<ArrayList<Object>> contacts = requestContacts(columns, sort, "ASC");
        assertEquals(contacts.size(), contactsSortedYOMIFirstNames.length, "Wrong number of contacts received");
        for (int i = 0; i < contactsSortedYOMIFirstNames.length; i++) {
            ArrayList<Object> contact = contacts.get(i);
            assertEquals(contact.get(0).toString(), contactsSortedYOMIFirstNames[i].getYomiFirstName(), "Unexpected sorting.");
        }
    }

    @Test
    public void testSpecialSortingFirstNameLocaleJapanDescending() throws Exception {
        generateAndCreateContacts();
        setJapanLocale();

        String columns = ID_YOMI_FIRST_NAME + "," + ID_SPECIAL_SORTING_FIRST_NAME;
        String sort = ID_SPECIAL_SORTING_FIRST_NAME;
        ArrayList<ArrayList<Object>> contacts = requestContacts(columns, sort, "DESC");
        assertEquals(contacts.size(), contactsSortedYOMIFirstNames.length, "Wrong number of contacts received");
        for (int i = 0; i < contactsSortedYOMIFirstNames.length; i++) {
            ArrayList<Object> contact = contacts.get(i);
            assertEquals(contact.get(0).toString(), contactsSortedYOMIFirstNames[contactsSortedYOMIFirstNames.length - 1 - i].getYomiFirstName(), "Unexpected sorting.");
        }
    }

    @Test
    public void testSpecialSortingLastName() throws Exception {
        generateAndCreateContacts();

        String columns = ID_SUR_NAME + "," + ID_SPECIAL_SORTING_LAST_NAME;
        String sort = ID_SPECIAL_SORTING_LAST_NAME;
        ArrayList<ArrayList<Object>> contacts = requestContacts(columns, sort, "ASC");
        assertEquals(contacts.size(), contactsSortedLastNames.length, "Wrong number of contacts received");
        for (int i = 0; i < contactsSortedLastNames.length; i++) {
            ArrayList<Object> contact = contacts.get(i);
            assertEquals(contact.get(0).toString(), contactsSortedLastNames[i].getSurName(), "Unexpected sorting.");
        }
    }

    @Test
    public void testSpecialSortingLastNameLocaleJapan() throws Exception {
        generateAndCreateContacts();
        setJapanLocale();

        String columns = ID_YOMI_LAST_NAME + "," + ID_SPECIAL_SORTING_LAST_NAME;
        String sort = ID_SPECIAL_SORTING_LAST_NAME;
        ArrayList<ArrayList<Object>> contacts = requestContacts(columns, sort, "ASC");
        assertEquals(contacts.size(), contactsSortedYOMILastNames.length, "Wrong number of contacts received");
        for (int i = 0; i < contactsSortedYOMILastNames.length; i++) {
            ArrayList<Object> contact = contacts.get(i);
            assertEquals(contact.get(0).toString(), contactsSortedYOMILastNames[i].getYomiLastName(), "Unexpected sorting.");
        }
    }

    @Test
    public void testSpecialSortingOnlyYOMIFirstName() throws Exception {
        generateAndCreateContactsOnlyYomiNames();

        String columns = ID_YOMI_FIRST_NAME + "," + Contact.SPECIAL_SORTING_FIRST_NAME;
        String sort = ID_SPECIAL_SORTING_FIRST_NAME;
        ArrayList<ArrayList<Object>> contacts = requestContacts(columns, sort, "ASC");
        assertEquals(contacts.size(), orderedYOMIFirstNames.length, "Wrong number of contacts received");
        for (int i = 0; i < orderedYOMIFirstNames.length; i++) {
            ArrayList<Object> contact = contacts.get(i);
            assertEquals(contact.get(0).toString(), orderedYOMIFirstNames[i], "Unexpected sorting.");
        }
        System.out.println(contacts);
    }

    @Test
    public void testSpecialSortingOnlyYOMILastName() throws Exception {
        generateAndCreateContactsOnlyYomiNames();

        String columns = ID_YOMI_LAST_NAME + "," + ID_SPECIAL_SORTING_LAST_NAME;
        String sort = ID_SPECIAL_SORTING_LAST_NAME;
        ArrayList<ArrayList<Object>> contacts = requestContacts(columns, sort, "ASC");
        assertEquals(contacts.size(), orderedYOMILastNames.length, "Wrong number of contacts received");
        for (int i = 0; i < orderedYOMILastNames.length; i++) {
            ArrayList<Object> contact = contacts.get(i);
            assertEquals(contact.get(0).toString(), orderedYOMILastNames[i], "Unexpected sorting.");
        }

    }

    /**
     * Creates a given contact with Yomi information in the address book.
     *
     * @param contact The contact
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    private void createYOMIContact(Contact contact) throws ApiException {
        createYOMIContact(contact.getGivenName(), contact.getSurName(), contact.getYomiFirstName(), contact.getYomiLastName(), privateContactFolderId);
    }

    /**
     * Creates a contact with yomi names in the address book.
     *
     * @param firstName The first name.
     * @param lastName The last name.
     * @param yomiFirstName The yomi first name.
     * @param yomiLastName The yomi last name.
     * @param folderId The folder id.
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    private void createYOMIContact(String firstName, String lastName, String yomiFirstName, String yomiLastName, String folderId) throws ApiException {
        ContactData contact = new ContactData();
        contact.setFirstName(firstName);
        contact.setLastName(lastName);
        contact.setYomiLastName(yomiLastName);
        contact.setYomiFirstName(yomiFirstName);
        contact.setDisplayName(firstName + " " + lastName);
        contact.setFolderId(folderId);
        ContactUpdateResponse createContactInAddressbook = addressbooksApi.createContactInAddressbook(contact);
        checkResponse(createContactInAddressbook.getError(), createContactInAddressbook.getErrorDesc());
    }

    /**
     * Generates contacts with yomi names and creates them in the address book.
     *
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    private void generateAndCreateContacts() throws ApiException {
        Contact contact1 = generateContact("Berta", "Arod", orderedYOMIFirstNames[0], orderedYOMILastNames[0]);
        Contact contact2 = generateContact("Anton", "Notan", orderedYOMIFirstNames[1], orderedYOMILastNames[2]);
        Contact contact3 = generateContact("Caesar", "Atreb", orderedYOMIFirstNames[2], orderedYOMILastNames[1]);
        Contact contact4 = generateContact("Dora", "Rasear", orderedYOMIFirstNames[3], orderedYOMILastNames[3]);

        contactsSortedYOMIFirstNames = new Contact[] { contact1, contact2, contact3, contact4 };
        contactsSortedYOMILastNames = new Contact[] { contact1, contact3, contact2, contact4 };
        contactsSortedFirstNames = new Contact[] { contact2, contact1, contact3, contact4 };
        contactsSortedLastNames = new Contact[] { contact1, contact3, contact2, contact4 };

        ArrayList<Contact> unorderedContacts = new ArrayList<>(Arrays.asList(contactsSortedYOMIFirstNames));
        Collections.shuffle(unorderedContacts);
        for (Contact c : unorderedContacts) {
            createYOMIContact(c);
        }
    }

    /**
     * Generate and create contacts which only contain yomi names in the address book.
     *
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body.
     */
    private void generateAndCreateContactsOnlyYomiNames() throws ApiException {
        List<String> unorderedYomiFirstNames = new ArrayList<>(Arrays.asList(orderedYOMIFirstNames));
        Collections.shuffle(unorderedYomiFirstNames);
        List<String> unorderedYomiLastNames = new ArrayList<>(Arrays.asList(orderedYOMILastNames));
        Collections.shuffle(unorderedYomiLastNames);

        for (int i = 0; i < unorderedYomiFirstNames.size(); i++) {
            createYOMIContact(null, null, unorderedYomiFirstNames.get(i), unorderedYomiLastNames.get(i), privateContactFolderId);
        }
    }

    /**
     * Generates a contact with the given names.
     *
     * @param firstName The first name.
     * @param lastName The last name.
     * @param yomiFirstName The yomi first name.
     * @param yomiLastName The yomi last name.
     * @return The contact.
     */
    private Contact generateContact(String firstName, String lastName, String yomiFirstName, String yomiLastName) {
        Contact contact = new Contact();
        contact.setSurName(lastName);
        contact.setGivenName(firstName);
        contact.setDisplayName(contact.getSurName() + ", " + contact.getGivenName());
        contact.setYomiFirstName(yomiFirstName);
        contact.setYomiLastName(yomiLastName);
        return contact;
    }

    /**
     * Requests the contacts from the address book.
     *
     * @param columns The column identifiers.
     * @param sort The sort identifier.
     * @return An array list with the contact information.
     * @throws ApiException
     */
    @SuppressWarnings("unchecked")
    private ArrayList<ArrayList<Object>> requestContacts(String columns, String sort, String order) throws ApiException {
        ContactsResponse contactResponse = addressbooksApi.getAllContactsFromAddressbook(privateContactFolderId, columns, sort, order, null);
        ArrayList<ArrayList<Object>> contacts = (ArrayList<ArrayList<Object>>) checkResponse(contactResponse.getError(), contactResponse.getErrorDesc(), contactResponse.getData());
        return contacts;
    }

    /**
     * Sets the locale of the user to Japanese ("ja-JP").
     *
     * @throws ApiException If fail to call the API
     */
    private void setJapanLocale() throws ApiException {
        userApi = new UserApi(getApiClient());
        String userId = I(testUser.getUserId()).toString();
        UserResponse userResponse = userApi.getUser(userId);
        UserData userData = new UserData();
        userData.setLocale("ja-JP");

        CommonResponse response = userApi.updateUser(userId, userResponse.getTimestamp(), userData);
        checkResponse(response.getError(), response.getErrorDesc());
    }

}
