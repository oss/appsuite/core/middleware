/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.ajax.oidc;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import org.junit.jupiter.api.Test;
import com.openexchange.ajax.chronos.scheduling.RESTUtilities;
import com.openexchange.ajax.framework.SessionAwareClient;
import com.openexchange.ajax.framework.config.util.TestConfigurationChangeUtil;
import com.openexchange.ajax.framework.config.util.TestConfigurationChangeUtil.Scopes;
import com.openexchange.test.Host;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.test.common.test.pool.soap.SoapProvisioningService;
import com.openexchange.test.common.tools.LoginTools;
import com.openexchange.testing.httpclient.invoker.ApiClient;
import com.openexchange.testing.httpclient.models.SessionManagementData;
import com.openexchange.testing.httpclient.models.UserResponse;
import com.openexchange.testing.httpclient.modules.SessionmanagementApi;
import com.openexchange.testing.httpclient.modules.UserApi;
import com.openexchange.testing.restclient.models.CloseSessionByUserBody;
import com.openexchange.testing.restclient.models.ContextUserTuple;
import com.openexchange.testing.restclient.modules.AdminApi;
import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * {@link CloseOIDCSessionsTest} - Tests for closing (other) sessions on keycloak
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class CloseOIDCSessionsTest extends AbstractOIDCTest {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CloseOIDCSessionsTest.class);

    @Test
    public void testCloseViaSessionMgmtWithOpLogout() throws Exception {
        configureOpLogoutOnSessionRemoval(testUser, "user_closed");
        testCloseViaSessionMgmt(true);
    }

    @Test
    public void testCloseViaSessionMgmtWithoutOpLogout() throws Exception {
        configureOpLogoutOnSessionRemoval(testUser, "");
        testCloseViaSessionMgmt(false);
    }

    @Test
    public void testCloseViaAdminSOAPWithOpLogout() throws Exception {
        configureOpLogoutOnSessionRemoval(testUser, "admin_closed");
        testCloseViaAdminSOAP(true);
    }

    @Test
    public void testCloseViaAdminSoapWithoutOpLogout() throws Exception {
        configureOpLogoutOnSessionRemoval(testUser, "");
        testCloseViaAdminSOAP(false);
    }

    @Test
    public void testCloseViaAdminRESTWithOpLogout() throws Exception {
        configureOpLogoutOnSessionRemoval(testUser, "admin_closed");
        testCloseViaAdminREST(true);
    }

    @Test
    public void testCloseViaAdminRESTWithoutOpLogout() throws Exception {
        configureOpLogoutOnSessionRemoval(testUser, "");
        testCloseViaAdminREST(false);
    }

    private void testCloseViaSessionMgmt(boolean expectSSOSesssionsClosed) throws Exception {
        /*
         * login 3 times
         */
        List<SessionAwareClient> apiClients = loginViaOIDC(testUser, 3);
        assertSSOSessionCount(testUser, 3);
        /*
         * use first session to close others
         */
        closeOtherSessions(apiClients.get(0));
        /*
         * verify first session still there, and other's have been terminated
         */
        assertSessionDataFound(apiClients.get(0), apiClients.get(0).getSession());
        assertLoggedIn(apiClients.get(0));
        for (int i = 1; i < apiClients.size(); i++) {
            assertSessionDataNotFound(apiClients.get(0), apiClients.get(i).getSession());
            assertLoggedOut(apiClients.get(i));
        }
        /*
         * verify SSO sessions
         */
        assertSSOSessionCount(testUser, expectSSOSesssionsClosed ? 1 : 3);
    }

    private void testCloseViaAdminSOAP(boolean expectSSOSesssionsClosed) throws Exception {
        /*
         * login 3 times
         */
        List<SessionAwareClient> apiClients = loginViaOIDC(testUser, 3);
        assertSSOSessionCount(testUser, 3);
        /*
         * close all sessions via SOAP
         */
        SoapProvisioningService.getInstance().clearUserSessions(testUser.getContextId(), testUser.getUserId(), testUser.getCreatedBy());
        assertSSOSessionCount(testUser, expectSSOSesssionsClosed ? 0 : 3);
        /*
         * login another time
         */
        SessionAwareClient newApiClient = loginViaOIDC(testUser);
        /*
         * verify previous sessions have all been terminated
         */
        for (SessionAwareClient apiClient : apiClients) {
            assertSessionDataNotFound(newApiClient, apiClient.getSession());
            assertLoggedOut(apiClient);
        }
        /*
         * verify SSO sessions
         */
        assertSSOSessionCount(testUser, expectSSOSesssionsClosed ? 1 : 4);
    }

    private void testCloseViaAdminREST(boolean expectSSOSesssionsClosed) throws Exception {
        /*
         * login 3 times
         */
        List<SessionAwareClient> apiClients = loginViaOIDC(testUser, 3);
        assertSSOSessionCount(testUser, 3);
        /*
         * close all sessions via SOAP
         */
        closeSessionsByUser(testUser);
        assertSSOSessionCount(testUser, expectSSOSesssionsClosed ? 0 : 3);
        /*
         * login another time
         */
        SessionAwareClient newApiClient = loginViaOIDC(testUser);
        /*
         * verify previous sessions have all been terminated
         */
        for (SessionAwareClient apiClient : apiClients) {
            assertSessionDataNotFound(newApiClient, apiClient.getSession());
            assertLoggedOut(apiClient);
        }
        /*
         * verify SSO sessions
         */
        assertSSOSessionCount(testUser, expectSSOSesssionsClosed ? 1 : 4);
    }

    protected List<KeycloakSessionData> assertSSOSessionCount(TestUser testUser, int ecpectedSessionCount) throws Exception {
        List<KeycloakSessionData> sessions = listSSOSessions(testUser);
        if (0 > ecpectedSessionCount || ecpectedSessionCount == sessions.size()) {
            return sessions;
        }
        for (int i = 0; i < 10 && sessions.size() != ecpectedSessionCount; i++) {
            LOG.debug("Actual SSO session count {} not yet matching expectation of {}, trying again...", I(sessions.size()), I(ecpectedSessionCount));
            LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(400));
            sessions = listSSOSessions(testUser);
        }
        assertEquals(ecpectedSessionCount, sessions.size());
        return sessions;
    }

    private static void assertLoggedOut(ApiClient apiClient) throws Exception {
        UserResponse response = new UserApi(apiClient).getUserBuilder().execute();
        assertNotNull(response.getError());
        assertEquals("SES-0203", response.getCode());
    }

    private void assertLoggedIn(ApiClient apiClient) throws Exception {
        assertNull(new UserApi(apiClient).getUserBuilder().execute().getError());
    }

    private static void closeOtherSessions(ApiClient apiClient) throws Exception {
        new SessionmanagementApi(apiClient).clear();
    }

    private static void assertSessionDataFound(ApiClient apiClient, String sessionId) throws Exception {
        assertNotNull(lookupSessionData(new SessionmanagementApi(apiClient).all().getData(), sessionId));
    }

    private static void assertSessionDataNotFound(ApiClient apiClient, String sessionId) throws Exception {
        assertNull(lookupSessionData(new SessionmanagementApi(apiClient).all().getData(), sessionId));
    }

    private List<SessionAwareClient> loginViaOIDC(TestUser testUser, int count) throws Exception {
        List<SessionAwareClient> apiClients = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            apiClients.add(loginViaOIDC(testUser));
        }
        for (SessionAwareClient apiClient : apiClients) {
            assertLoggedIn(apiClient);
            assertNotNull(lookupSessionData(new SessionmanagementApi(apiClient).all().getData(), apiClient.getSession()));
        }
        return apiClients;
    }

    private static void closeSessionsByUser(TestUser testUser) throws Exception {
        com.openexchange.testing.restclient.invoker.ApiClient adminApiClient = RESTUtilities.createRESTMasterClient(testUser.getCreatedBy(), Host.OIDC);
        AdminApi adminApi = new AdminApi(adminApiClient);
        adminApi.closeSessionsByUser(new CloseSessionByUserBody().addUsersItem(
            new ContextUserTuple().contextId(I(testUser.getContextId())).userId(I(testUser.getUserId()))), Boolean.TRUE);
    }

    private SessionAwareClient loginViaOIDC(TestUser testUser) throws Exception {
        SessionAwareClient apiClient = new SessionAwareClient(testUser, Optional.empty(), Optional.of(LoginTools.HTTP_API_TESTING_AGENT), Optional.of(
            builder -> builder.addInterceptor(c -> c.proceed(c.request().newBuilder().addHeader("User-Agent", LoginTools.HTTP_API_TESTING_AGENT).build())).followRedirects(false)));
        apiClient.setBasePath(getOidcBasePath());
        apiClient.setUserAgent(LoginTools.HTTP_API_TESTING_AGENT);
        OkHttpClient httpClient = apiClient.getHttpClient();
        Response loginPage = initLoginFlow(httpClient);
        String rpRedirectURIAuth = keycloakLogin(httpClient, testUser.getLogin(), testUser.getPassword(), loginPage);
        loginPage.close();
        OIDCSession session = middlewareLogin(httpClient, rpRedirectURIAuth);
        apiClient.setSession(session.session());
        return apiClient;
    }

    private static SessionManagementData lookupSessionData(List<SessionManagementData> dataList, String sessionId) {
        LOG.debug("Looking for session {} in these sessions from session management: {}", sessionId, dataList);
        if (null != dataList) {
            for (SessionManagementData data : dataList) {
                if (sessionId.equals(data.getSessionId())) {
                    return data;
                }
            }
        }
        return null;
    }

    private static void configureOpLogoutOnSessionRemoval(TestUser testUser, String value) throws Exception {
        TestConfigurationChangeUtil.changeConfigWithOwnClient(testUser, Map.of("com.openexchange.oidc.opLogoutOnSessionRemoval", value), Scopes.SERVER);
    }

}
