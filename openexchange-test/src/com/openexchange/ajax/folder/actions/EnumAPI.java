/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.folder.actions;

import com.openexchange.test.common.configuration.AJAXConfig;

/**
 * Enumeration of possible folder APIs.
 *
 * @author <a href="mailto:marcus.klein@open-xchange.com">Marcus Klein</a>
 */
public enum EnumAPI implements API {

    OX_OLD(AJAXConfig.getProperty(AJAXConfig.Property.BASE_PATH) + "folders", -1),
    OX_NEW(AJAXConfig.getProperty(AJAXConfig.Property.BASE_PATH) + "folders", 0),
    OUTLOOK(AJAXConfig.getProperty(AJAXConfig.Property.BASE_PATH) + "folders", 1),
    EAS_FOLDERS(AJAXConfig.getProperty(AJAXConfig.Property.BASE_PATH) + "folders", 20);

    private final String url;
    private final int treeId;

    private EnumAPI(final String url, final int treeId) {
        this.url = url;
        this.treeId = treeId;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public int getTreeId() {
        return treeId;
    }
}
