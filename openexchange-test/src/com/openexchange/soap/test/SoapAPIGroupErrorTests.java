
package com.openexchange.soap.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.net.MalformedURLException;
import java.util.List;
import javax.xml.ws.BindingProvider;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import com.openexchange.admin.soap.context.dataobjects.SchemaSelectStrategy;
import com.openexchange.admin.soap.context.dataobjects.User;
import com.openexchange.admin.soap.context.soap.ContextExistsException_Exception;
import com.openexchange.admin.soap.context.soap.OXContextServicePortType;
import com.openexchange.admin.soap.group.dataobjects.Context;
import com.openexchange.admin.soap.group.dataobjects.Credentials;
import com.openexchange.admin.soap.group.dataobjects.Group;
import com.openexchange.admin.soap.group.soap.DatabaseUpdateException_Exception;
import com.openexchange.admin.soap.group.soap.InvalidCredentialsException_Exception;
import com.openexchange.admin.soap.group.soap.InvalidDataException_Exception;
import com.openexchange.admin.soap.group.soap.NoSuchContextException_Exception;
import com.openexchange.admin.soap.group.soap.NoSuchUserException_Exception;
import com.openexchange.admin.soap.group.soap.OXGroupServicePortType;
import com.openexchange.admin.soap.group.soap.RemoteException_Exception;
import com.openexchange.admin.soap.group.soap.StorageException_Exception;
import com.openexchange.admin.soap.util.soap.OXUtilServicePortType;
import com.openexchange.ajax.framework.AbstractTestEnvironment;
import com.openexchange.test.common.test.pool.soap.SoapContextService;
import com.openexchange.test.common.test.pool.soap.SoapGroupService;
import com.openexchange.test.common.test.pool.soap.SoapUtilService;

class SoapAPIGroupErrorTests extends AbstractTestEnvironment {

    private Credentials groupContextCreds;

    private OXGroupServicePortType groupPort;

    private OXContextServicePortType contextPort;

    private com.openexchange.admin.soap.context.dataobjects.Credentials contextMasterCreds;

    private OXUtilServicePortType utilPort;

    private com.openexchange.admin.soap.util.dataobjects.Credentials utilMasterCreds;

    private Credentials wrongGroupServiceCreds;

    @BeforeEach
    public void init() throws MalformedURLException {
        SoapGroupService soapGroupService = SoapGroupService.getInstance();
        groupPort = (OXGroupServicePortType) soapGroupService.getPortType();
        ((BindingProvider) groupPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, soapGroupService.getWsdlLocation().toString());
        groupContextCreds = soapGroupService.getSoapContextCreds();
        wrongGroupServiceCreds = soapGroupService.getWrongCreds();

        SoapContextService soapContextService = SoapContextService.getInstance();
        contextPort = (OXContextServicePortType) soapContextService.getPortType();
        ((BindingProvider) contextPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, soapContextService.getWsdlLocation().toString());
        contextMasterCreds = soapContextService.getSoapMasterCreds();

        SoapUtilService soapUtilService = SoapUtilService.getInstance();
        utilPort = (OXUtilServicePortType) soapUtilService.getPortType();
        ((BindingProvider) utilPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, soapUtilService.getWsdlLocation().toString());
        utilMasterCreds = soapUtilService.getSoapMasterCreds();
    }

    @Test
    void create_paramsNull() {
        try {
            groupPort.create(null, null, null);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | DatabaseUpdateException_Exception | NoSuchUserException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_emptyParams() {
        Context context = new Context();
        Group group = new Group();
        try {
            groupPort.create(context, group, wrongGroupServiceCreds);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | DatabaseUpdateException_Exception | NoSuchUserException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    // Tests weird API behavior: Creds correct, but context not = InvalidCredentialsException_Exception
    @Test
    void create_contextWithoutId_throwsInvalidCredentialsException() {
        Context context = new Context();
        Group group = new Group();
        try {
            groupPort.create(context, group, groupContextCreds);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | DatabaseUpdateException_Exception | NoSuchUserException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void create_groupNotCorrectDefined() {
        Context context = new Context();
        context.setId(Integer.valueOf(5));
        Group group = new Group();
        try {
            groupPort.create(context, group, groupContextCreds);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | DatabaseUpdateException_Exception | NoSuchUserException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Disabled("Struggeling with correct auth")
    @Test
    void create_allGood() throws com.openexchange.admin.soap.context.soap.StorageException_Exception, com.openexchange.admin.soap.context.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.context.soap.InvalidDataException_Exception, com.openexchange.admin.soap.context.soap.RemoteException_Exception, com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, ContextExistsException_Exception {
        ContextWithAdminUser contextWithAdminUser = createContext();
        Group newGroup = new Group();
        String randomAlphabetic = RandomStringUtils.randomAlphabetic(5);
        newGroup.setDisplayname(randomAlphabetic);
        newGroup.setName(randomAlphabetic);
        try {
            Group createdGroup = groupPort.create(contextWithAdminUser.context(), newGroup, toCreds(contextWithAdminUser));
            Assertions.assertEquals(randomAlphabetic, createdGroup.getDisplayname());
            Assertions.assertEquals(randomAlphabetic, createdGroup.getName());
            Assertions.assertTrue(createdGroup.getId().intValue() > 0);
            Assertions.assertEquals(0, createdGroup.getMembers().size());
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | DatabaseUpdateException_Exception | NoSuchUserException_Exception e) {
            fail("Unexpected exception.", e);
        }
    }

    private com.openexchange.admin.soap.group.dataobjects.Credentials toCreds(ContextWithAdminUser contextWithAdminUser) {
        com.openexchange.admin.soap.group.dataobjects.Credentials creds = new com.openexchange.admin.soap.group.dataobjects.Credentials();
        creds.setLogin(contextWithAdminUser.admin().getPrimaryEmail());
        creds.setPassword(contextWithAdminUser.admin().getPassword());
        return creds;
    }

    private ContextWithAdminUser createContext() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, ContextExistsException_Exception, com.openexchange.admin.soap.context.soap.StorageException_Exception, com.openexchange.admin.soap.context.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.context.soap.InvalidDataException_Exception, com.openexchange.admin.soap.context.soap.RemoteException_Exception {
        com.openexchange.admin.soap.context.dataobjects.Context newContext = getDefinedContext();
        User admin = getDefinedUser();
        com.openexchange.admin.soap.context.dataobjects.Context context = contextPort.create(newContext, admin, contextMasterCreds, new SchemaSelectStrategy());
        return new ContextWithAdminUser(toGroupServiceObject(context), toGroupServiceObject(admin));
    }

    private com.openexchange.admin.soap.context.dataobjects.Context getDefinedContext() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilMasterCreds).get(0);
        Integer filestoreId = filestore.getId();
        com.openexchange.admin.soap.context.dataobjects.Context newContext = new com.openexchange.admin.soap.context.dataobjects.Context();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestoreId);
        newContext.setName(RandomStringUtils.randomAlphabetic(10));
        return newContext;
    }

    private com.openexchange.admin.soap.group.dataobjects.Context toGroupServiceObject(com.openexchange.admin.soap.context.dataobjects.Context createdContext) {
        com.openexchange.admin.soap.group.dataobjects.Context userContext = new com.openexchange.admin.soap.group.dataobjects.Context();
        userContext.setName(createdContext.getName());
        userContext.setFilestoreId(createdContext.getFilestoreId());
        userContext.setId(createdContext.getId());
        userContext.setMaxQuota(createdContext.getMaxQuota());
        return userContext;
    }

    private com.openexchange.admin.soap.group.dataobjects.User toGroupServiceObject(User createdUser) {
        com.openexchange.admin.soap.group.dataobjects.User user = new com.openexchange.admin.soap.group.dataobjects.User();
        user.setName(createdUser.getName());
        user.setDisplayName(createdUser.getDisplayName());
        user.setPassword(createdUser.getPassword());
        user.setGivenName(createdUser.getGivenName());
        user.setSurName(createdUser.getSurName());
        user.setPrimaryEmail(createdUser.getPrimaryEmail());
        user.setEmail1(createdUser.getEmail1());
        user.setId(createdUser.getId());
        return user;
    }

    private User getDefinedUser() {
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);
        return admin;
    }

    @Test
    void listAll_paramNull() {
        try {
            groupPort.listAll(null, groupContextCreds);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | DatabaseUpdateException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void listAll_wrongCreds() {
        try {
            groupPort.listAll(null, wrongGroupServiceCreds);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | DatabaseUpdateException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    // Tests weird API behavior: Creds correct, but context not = InvalidCredentialsException_Exception
    @Test
    void listAll_contextEmpty() {
        Context context = new Context();

        try {
            groupPort.listAll(context, groupContextCreds);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | DatabaseUpdateException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Disabled("Struggeling with correct auth")
    @Test
    void listAll_allGood() throws com.openexchange.admin.soap.context.soap.StorageException_Exception, com.openexchange.admin.soap.context.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.context.soap.InvalidDataException_Exception, com.openexchange.admin.soap.context.soap.RemoteException_Exception, com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, ContextExistsException_Exception, StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, NoSuchContextException_Exception, RemoteException_Exception, NoSuchUserException_Exception, DatabaseUpdateException_Exception {
        ContextWithAdminUser contextWithAdminUser = createContext();
        Group newGroup = new Group();
        String randomAlphabetic = RandomStringUtils.randomAlphabetic(5);
        newGroup.setDisplayname(randomAlphabetic);
        newGroup.setName(randomAlphabetic);
        groupPort.create(contextWithAdminUser.context(), newGroup, toCreds(contextWithAdminUser));

        boolean groupFound = false;
        try {
            List<Group> listAll = groupPort.listAll(contextWithAdminUser.context(), toCreds(contextWithAdminUser));
            for (Group group : listAll) {
                if (group.getName().equalsIgnoreCase(randomAlphabetic)) {
                    assertEquals(newGroup.getDisplayname(), randomAlphabetic);
                    Assertions.assertTrue(group.getId().intValue() > 0);
                    Assertions.assertEquals(0, group.getMembers().size());
                    groupFound = true;
                }
            }
            assertTrue(groupFound, "Group not found!");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | DatabaseUpdateException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    private record ContextWithAdminUser(com.openexchange.admin.soap.group.dataobjects.Context context, com.openexchange.admin.soap.group.dataobjects.User admin) {
    }
}
