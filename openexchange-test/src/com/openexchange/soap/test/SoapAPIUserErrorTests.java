
package com.openexchange.soap.test;

import static org.junit.jupiter.api.Assertions.fail;
import java.net.MalformedURLException;
import javax.xml.ws.BindingProvider;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import com.openexchange.admin.soap.context.dataobjects.Context;
import com.openexchange.admin.soap.context.dataobjects.Credentials;
import com.openexchange.admin.soap.context.dataobjects.SchemaSelectStrategy;
import com.openexchange.admin.soap.context.soap.ContextExistsException_Exception;
import com.openexchange.admin.soap.context.soap.OXContextServicePortType;
import com.openexchange.admin.soap.user.dataobjects.User;
import com.openexchange.admin.soap.user.soap.Change;
import com.openexchange.admin.soap.user.soap.DatabaseUpdateException_Exception;
import com.openexchange.admin.soap.user.soap.InvalidCredentialsException_Exception;
import com.openexchange.admin.soap.user.soap.InvalidDataException_Exception;
import com.openexchange.admin.soap.user.soap.NoSuchContextException_Exception;
import com.openexchange.admin.soap.user.soap.NoSuchUserException_Exception;
import com.openexchange.admin.soap.user.soap.OXUserServicePortType;
import com.openexchange.admin.soap.user.soap.RemoteException_Exception;
import com.openexchange.admin.soap.user.soap.StorageException_Exception;
import com.openexchange.admin.soap.util.soap.OXUtilServicePortType;
import com.openexchange.ajax.framework.AbstractTestEnvironment;
import com.openexchange.test.common.test.pool.soap.SoapContextService;
import com.openexchange.test.common.test.pool.soap.SoapUserService;
import com.openexchange.test.common.test.pool.soap.SoapUtilService;

class SoapAPIUserErrorTests extends AbstractTestEnvironment {

    private OXUserServicePortType userPort;

    private com.openexchange.admin.soap.user.dataobjects.Credentials userContextCreds;

    private OXUtilServicePortType utilPort;
    private OXContextServicePortType contextPort;


    private Credentials contextMasterCreds;

    private com.openexchange.admin.soap.user.dataobjects.Credentials wrongUserServiceCreds;

    @BeforeEach
    public void init() throws MalformedURLException {
        SoapUserService soapUserService = SoapUserService.getInstance();
        userPort = (OXUserServicePortType) soapUserService.getPortType();
        ((BindingProvider) userPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, soapUserService.getWsdlLocation().toString());
        userContextCreds = soapUserService.getSoapContextCreds();
        wrongUserServiceCreds = soapUserService.getWrongCreds();

        SoapUtilService soapUtilService = SoapUtilService.getInstance();
        utilPort = (OXUtilServicePortType) soapUtilService.getPortType();
        ((BindingProvider) utilPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, soapUtilService.getWsdlLocation().toString());

        SoapContextService soapContextService = SoapContextService.getInstance();
        contextPort = (OXContextServicePortType) soapContextService.getPortType();
        ((BindingProvider) contextPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, soapContextService.getWsdlLocation().toString());
        contextMasterCreds = soapContextService.getSoapMasterCreds();
    }

    private ContextWithAdminUser createContext() throws com.openexchange.admin.soap.context.soap.StorageException_Exception, com.openexchange.admin.soap.context.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.context.soap.InvalidDataException_Exception, ContextExistsException_Exception, com.openexchange.admin.soap.context.soap.RemoteException_Exception {
        com.openexchange.admin.soap.context.dataobjects.Context newContext = getDefinedContext();
        com.openexchange.admin.soap.context.dataobjects.User admin = getDefinedUser();
        Context createdContext = contextPort.create(newContext, admin, contextMasterCreds, new SchemaSelectStrategy());
        com.openexchange.admin.soap.user.dataobjects.User userAdmin = toUserServiceObject(admin);

        //        toCreds(userContextCreds);
        //        com.openexchange.admin.soap.context.dataobjects.User newUser = getDefinedUser();
        //        User user = userPort.create(toUserServiceObject(createdContext), toUserServiceObject(newUser), userContextCreds);

        return new ContextWithAdminUser(toUserServiceObject(createdContext), userAdmin);
    }

    private com.openexchange.admin.soap.context.dataobjects.User getDefinedUser() {
        com.openexchange.admin.soap.context.dataobjects.User admin = new com.openexchange.admin.soap.context.dataobjects.User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);
        return admin;
    }

    private Context getDefinedContext() {
        Context newContext = new Context();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setName(RandomStringUtils.randomAlphabetic(10));
        return newContext;
    }

    private com.openexchange.admin.soap.user.dataobjects.User toUserServiceObject(com.openexchange.admin.soap.context.dataobjects.User filledUser) {
        com.openexchange.admin.soap.user.dataobjects.User user = new com.openexchange.admin.soap.user.dataobjects.User();
        user.setName(filledUser.getName());
        user.setDisplayName(filledUser.getDisplayName());
        user.setPassword(filledUser.getPassword());
        user.setGivenName(filledUser.getGivenName());
        user.setSurName(filledUser.getSurName());
        user.setPrimaryEmail(filledUser.getPrimaryEmail());
        user.setEmail1(filledUser.getEmail1());
        user.setId(filledUser.getId());
        return user;
    }

    private com.openexchange.admin.soap.user.dataobjects.Context toUserServiceObject(com.openexchange.admin.soap.context.dataobjects.Context createdContext) {
        com.openexchange.admin.soap.user.dataobjects.Context userContext = new com.openexchange.admin.soap.user.dataobjects.Context();
        userContext.setName(createdContext.getName());
        userContext.setFilestoreId(createdContext.getFilestoreId());
        userContext.setId(createdContext.getId());
        userContext.setMaxQuota(createdContext.getMaxQuota());
        return userContext;
    }

    @Test
    void change_emptyParam() {
        Change change = new Change();
        try {
            userPort.change(change);
            fail("Exception not thrown");

        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | NoSuchUserException_Exception | DatabaseUpdateException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void change_contextDoesNotExist() {
        Change change = new Change();
        change.setAuth(userContextCreds);
        change.setCtx(toUserServiceObject(getDefinedContext()));
        try {
            userPort.change(change);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | NoSuchUserException_Exception | DatabaseUpdateException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void change_noUser() throws com.openexchange.admin.soap.context.soap.StorageException_Exception, com.openexchange.admin.soap.context.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.context.soap.InvalidDataException_Exception, ContextExistsException_Exception, com.openexchange.admin.soap.context.soap.RemoteException_Exception {
        ContextWithAdminUser contextWithAdminUser = createContext();
        Change change = new Change();
        change.setAuth(toCreds(contextWithAdminUser));
        change.setCtx(contextWithAdminUser.context());
        try {
            userPort.change(change);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | NoSuchUserException_Exception | DatabaseUpdateException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    private com.openexchange.admin.soap.user.dataobjects.Credentials toCreds(ContextWithAdminUser contextWithAdminUser) {
        com.openexchange.admin.soap.user.dataobjects.Credentials creds = new com.openexchange.admin.soap.user.dataobjects.Credentials();
        creds.setLogin(contextWithAdminUser.admin().getPrimaryEmail());
        creds.setPassword(contextWithAdminUser.admin().getPassword());
        return creds;
    }

    @Test
    void change_emptyUser() throws com.openexchange.admin.soap.context.soap.StorageException_Exception, com.openexchange.admin.soap.context.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.context.soap.InvalidDataException_Exception, ContextExistsException_Exception, com.openexchange.admin.soap.context.soap.RemoteException_Exception {
        ContextWithAdminUser contextWithAdminUser = createContext();
        Change change = new Change();
        change.setAuth(toCreds(contextWithAdminUser));
        change.setCtx(contextWithAdminUser.context());
        User user = new User();
        change.setUsrdata(user);
        try {
            userPort.change(change);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | NoSuchUserException_Exception | DatabaseUpdateException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void change_unknownUser() throws com.openexchange.admin.soap.context.soap.StorageException_Exception, com.openexchange.admin.soap.context.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.context.soap.InvalidDataException_Exception, ContextExistsException_Exception, com.openexchange.admin.soap.context.soap.RemoteException_Exception {
        ContextWithAdminUser contextWithAdminUser = createContext();
        Change change = new Change();
        change.setAuth(toCreds(contextWithAdminUser));
        change.setCtx(contextWithAdminUser.context());
        User user = new User();
        user.setId(Integer.valueOf(11));
        change.setUsrdata(user);
        try {
            userPort.change(change);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | NoSuchUserException_Exception | DatabaseUpdateException_Exception e) {
            Assertions.assertInstanceOf(NoSuchUserException_Exception.class, e);
        }
    }

    @Disabled("Struggeling with correct auth")
    @Test
    void change_allGood() throws com.openexchange.admin.soap.context.soap.StorageException_Exception, com.openexchange.admin.soap.context.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.context.soap.InvalidDataException_Exception, ContextExistsException_Exception, com.openexchange.admin.soap.context.soap.RemoteException_Exception {
        ContextWithAdminUser contextWithAdminUser = createContext();
        Change change = new Change();
        change.setAuth(toCreds(contextWithAdminUser));
        change.setCtx(contextWithAdminUser.context());
        change.setUsrdata(contextWithAdminUser.admin());
        try {
            userPort.change(change);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | NoSuchUserException_Exception | DatabaseUpdateException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    // Tests weird API behavior: admin creds wrong, user cannot be found, but instead of InvalidCredentialsException_Exception a NoSuchUserException_Exception is thrown
    @Test
    void change_wrongCreds() throws com.openexchange.admin.soap.context.soap.StorageException_Exception, com.openexchange.admin.soap.context.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.context.soap.InvalidDataException_Exception, ContextExistsException_Exception, com.openexchange.admin.soap.context.soap.RemoteException_Exception {
        ContextWithAdminUser contextWithAdminUser = createContext();
        Change change = new Change();
        change.setAuth(wrongUserServiceCreds);
        change.setCtx(contextWithAdminUser.context());
        change.setUsrdata(contextWithAdminUser.admin());
        try {
            userPort.change(change);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | NoSuchContextException_Exception | RemoteException_Exception | NoSuchUserException_Exception | DatabaseUpdateException_Exception e) {
            Assertions.assertInstanceOf(NoSuchUserException_Exception.class, e);
        }
    }

    private record ContextWithAdminUser(com.openexchange.admin.soap.user.dataobjects.Context context, com.openexchange.admin.soap.user.dataobjects.User admin) {
    }
}
