
package com.openexchange.soap.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.net.MalformedURLException;
import java.util.List;
import javax.xml.ws.BindingProvider;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.openexchange.admin.soap.context.dataobjects.Context;
import com.openexchange.admin.soap.context.dataobjects.Credentials;
import com.openexchange.admin.soap.context.dataobjects.Database;
import com.openexchange.admin.soap.context.dataobjects.Filestore;
import com.openexchange.admin.soap.context.dataobjects.Quota;
import com.openexchange.admin.soap.context.dataobjects.SchemaSelectStrategy;
import com.openexchange.admin.soap.context.dataobjects.User;
import com.openexchange.admin.soap.context.dataobjects.UserModuleAccess;
import com.openexchange.admin.soap.context.soap.Change;
import com.openexchange.admin.soap.context.soap.ChangeCapabilities;
import com.openexchange.admin.soap.context.soap.ChangeModuleAccess;
import com.openexchange.admin.soap.context.soap.ChangeModuleAccessByName;
import com.openexchange.admin.soap.context.soap.ContextExistsException_Exception;
import com.openexchange.admin.soap.context.soap.Disable;
import com.openexchange.admin.soap.context.soap.Enable;
import com.openexchange.admin.soap.context.soap.InvalidCredentialsException_Exception;
import com.openexchange.admin.soap.context.soap.InvalidDataException_Exception;
import com.openexchange.admin.soap.context.soap.NoSuchContextException_Exception;
import com.openexchange.admin.soap.context.soap.NoSuchDatabaseException_Exception;
import com.openexchange.admin.soap.context.soap.NoSuchFilestoreException_Exception;
import com.openexchange.admin.soap.context.soap.NoSuchReasonException_Exception;
import com.openexchange.admin.soap.context.soap.OXContextException_Exception;
import com.openexchange.admin.soap.context.soap.OXContextServicePortType;
import com.openexchange.admin.soap.context.soap.RemoteException_Exception;
import com.openexchange.admin.soap.context.soap.StorageException_Exception;
import com.openexchange.admin.soap.util.soap.OXUtilServicePortType;
import com.openexchange.ajax.framework.AbstractTestEnvironment;
import com.openexchange.test.common.test.pool.soap.SoapContextService;
import com.openexchange.test.common.test.pool.soap.SoapUtilService;

class SoapAPIContextErrorTests extends AbstractTestEnvironment {

    private OXContextServicePortType contextPort;

    private Credentials contextMasterCreds;

    private SoapContextService soapContextService;

    private OXUtilServicePortType utilPort;

    private com.openexchange.admin.soap.util.dataobjects.Credentials utilMasterCreds;

    @BeforeEach
    public void init() throws MalformedURLException {
        soapContextService = SoapContextService.getInstance();
        contextPort = (OXContextServicePortType) soapContextService.getPortType();
        ((BindingProvider) contextPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, soapContextService.getWsdlLocation().toString());
        contextMasterCreds = soapContextService.getSoapMasterCreds();

        SoapUtilService soapUtilService = SoapUtilService.getInstance();
        utilPort = (OXUtilServicePortType) soapUtilService.getPortType();
        ((BindingProvider) utilPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, soapUtilService.getWsdlLocation().toString());
        utilMasterCreds = soapUtilService.getSoapMasterCreds();
    }

    private Context createContext() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception {
        Context newContext = getDefinedContext();
        User admin = getDefinedUser();
        Context context = contextPort.create(newContext, admin, contextMasterCreds, new SchemaSelectStrategy());
        return context;
    }

    private Context getDefinedContext() {
        Context newContext = new Context();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setName(RandomStringUtils.randomAlphabetic(10));
        return newContext;
    }

    private User getDefinedUser() {
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);
        return admin;
    }

    //====== listAll ======
    @Test
    void testListAll_credParamNull() {
        try {
            contextPort.listAll(null);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void testListAll_invalidCredsParam() {
        try {
            contextPort.listAll(soapContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    //====== list ======
    @Test
    void testList_emptyStringAuthParamNull() {
        try {
            contextPort.list("", Boolean.FALSE, null);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void testList_emptyStringWrongCreds() {
        try {
            contextPort.list("", Boolean.FALSE, soapContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void testList_nullParamWrongCreds() {
        try {
            contextPort.list(null, Boolean.FALSE, soapContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void testList_searchParamNull_returnAllContexts() {
        try {
            List<Context> list = contextPort.list(null, Boolean.FALSE, contextMasterCreds);
            assertTrue(list.size() > 0);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    @Test
    void testList_emptyString_returnAllContexts() {
        try {
            List<Context> list = contextPort.list("", Boolean.FALSE, contextMasterCreds);
            assertTrue(list.size() > 0);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    @Test
    void testList_notExistingContext() {
        try {
            List<Context> list = contextPort.list(RandomStringUtils.randomAlphanumeric(10), Boolean.FALSE, contextMasterCreds);
            assertEquals(0, list.size());
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    //====== listByDatabase ======
    @Test
    void testListByDatabase_dbNameNullAndWrongCreds() {
        try {
            contextPort.listByDatabase(null, Boolean.FALSE, soapContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (NoSuchDatabaseException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void testListByDatabase_dbWithNoDataAndWrongCreds() {
        Database db = new Database();
        try {
            contextPort.listByDatabase(db, Boolean.FALSE, soapContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (NoSuchDatabaseException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void testListByDatabase_dbCorrectDataButWrongCreds() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        Database db = new Database();
        List<com.openexchange.admin.soap.util.dataobjects.Database> listDatabase = utilPort.listDatabase("*", utilMasterCreds);
        com.openexchange.admin.soap.util.dataobjects.Database database = listDatabase.get(0);
        db.setId(database.getId());
        try {
            contextPort.listByDatabase(db, Boolean.FALSE, soapContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (NoSuchDatabaseException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void testListByDatabase_dbNameNull() {
        try {
            contextPort.listByDatabase(null, Boolean.FALSE, contextMasterCreds);
            fail("Exception not thrown");
        } catch (NoSuchDatabaseException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void testListByDatabase_dbWithNoData() {
        Database db = new Database();
        try {
            contextPort.listByDatabase(db, Boolean.FALSE, contextMasterCreds);
            fail("Exception not thrown");
        } catch (NoSuchDatabaseException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    //====== listByFilestore ======
    @Test
    void listByFilestore_fsCorrectData() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        Filestore fs = new Filestore();
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilMasterCreds).get(0);
        fs.setId(filestore.getId());
        try {
            List<Context> listByFilestore = contextPort.listByFilestore(fs, Boolean.FALSE, contextMasterCreds);
            assertTrue(listByFilestore.size() > 0);
        } catch (NoSuchFilestoreException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    @Test
    void listByFilestore_fsCorrectDataWrongCreds() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        Filestore fs = new Filestore();
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilMasterCreds).get(0);
        fs.setId(filestore.getId());
        try {
            contextPort.listByFilestore(fs, Boolean.FALSE, soapContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (NoSuchFilestoreException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void listByFilestore_fsNullWrongCreds() {
        try {
            contextPort.listByFilestore(null, Boolean.FALSE, soapContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (NoSuchFilestoreException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void listByFilestore_fsNull() {
        try {
            contextPort.listByFilestore(null, Boolean.FALSE, contextMasterCreds);
            fail("Exception not thrown");
        } catch (NoSuchFilestoreException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    //====== listQuota ======
    @Test
    void listQuota_doNotThrowException() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, RemoteException_Exception {
        List<Context> context = contextPort.list(null, Boolean.FALSE, contextMasterCreds);
        try {
            List<Quota> listQuota = contextPort.listQuota(context.get(0), contextMasterCreds);
            assertNotNull(listQuota);
        } catch (NoSuchContextException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    @Test
    void listQuota_wrongCreds() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, RemoteException_Exception {
        List<Context> context = contextPort.list(null, Boolean.FALSE, contextMasterCreds);
        try {
            contextPort.listQuota(context.get(0), soapContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (NoSuchContextException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void listQuota_badParam() {
        try {
            contextPort.listQuota(null, contextMasterCreds);
            fail("Exception not thrown");
        } catch (NoSuchContextException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void listQuota_credsNull() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, RemoteException_Exception {
        List<Context> context = contextPort.list(null, Boolean.FALSE, contextMasterCreds);
        try {
            contextPort.listQuota(context.get(0), null);
            fail("Exception not thrown");
        } catch (NoSuchContextException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    //====== create ======
    @Test
    void create_noParam() {
        try {
            contextPort.create(null, null, null, null);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_credsOnly() {
        try {
            contextPort.create(null, null, contextMasterCreds, null);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_credsAndSchemaSelectStrategy() {
        try {
            contextPort.create(null, null, contextMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_contextCredsAndSchemaSelectStrategy() {
        try {
            Context newContext = new Context();
            contextPort.create(newContext, null, contextMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_allParamsButSomethingMissing() {
        try {
            Context newContext = new Context();
            User admin = new User();

            contextPort.create(newContext, admin, contextMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_allParamsForContextButAdminNotCorrect() {
        try {
            Context newContext = new Context();
            newContext.setMaxQuota(Long.valueOf(1000));
            User admin = new User();

            contextPort.create(newContext, admin, contextMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_allButFileStoreIdCorrectlySet() {
        try {
            Context newContext = new Context();
            newContext.setMaxQuota(Long.valueOf(1000));
            User admin = new User();
            admin.setName(RandomStringUtils.randomAlphabetic(5));
            admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
            admin.setPassword(RandomStringUtils.randomAlphabetic(5));
            admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
            admin.setSurName(RandomStringUtils.randomAlphabetic(5));
            admin.setPrimaryEmail(RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org");
            admin.setEmail1(RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org");

            contextPort.create(newContext, admin, contextMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            Assertions.assertInstanceOf(StorageException_Exception.class, e);
        }
    }

    @Test
    void create_priamryMailAndEmailDifferent() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilMasterCreds).get(0);

        Context newContext = new Context();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestore.getId());
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        admin.setPrimaryEmail(RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org");
        admin.setEmail1(RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org");

        try {
            contextPort.create(newContext, admin, contextMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            Assertions.assertInstanceOf(StorageException_Exception.class, e);
        }
    }

    @Test
    void create_allCorrectlySet() {

        Context newContext = new Context();
        newContext.setMaxQuota(Long.valueOf(1000));
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);

        try {
            Context context = contextPort.create(newContext, admin, contextMasterCreds, new SchemaSelectStrategy());
            assertTrue(context.isEnabled().booleanValue());
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    @Test
    void create_credsNull() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilMasterCreds).get(0);
        Integer filestoreId = filestore.getId();

        Context newContext = new Context();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestoreId);
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);

        try {
            contextPort.create(newContext, admin, null, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void create_credsWrong() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilMasterCreds).get(0);
        Integer filestoreId = filestore.getId();

        Context newContext = new Context();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestoreId);
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);

        try {
            contextPort.create(newContext, admin, soapContextService.getWrongCreds(), new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void create_schemaSelectNull() {
        Context newContext = new Context();
        newContext.setMaxQuota(Long.valueOf(1000));
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);

        try {
            Context context = contextPort.create(newContext, admin, contextMasterCreds, null);
            assertTrue(context.isEnabled().booleanValue());
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    @Test
    void create_adminNull() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilMasterCreds).get(0);
        Integer filestoreId = filestore.getId();

        Context newContext = new Context();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestoreId);
        try {
            contextPort.create(newContext, null, contextMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_contextNull() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilMasterCreds).get(0);
        Integer filestoreId = filestore.getId();

        Context newContext = new Context();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestoreId);
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);

        try {
            contextPort.create(null, admin, contextMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    //====== change ======
    @Test
    void change_noCredsInChange() {
        try {
            Change params = new Change();

            contextPort.change(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void change_wrongCredsInChange() {
        try {
            Change params = new Change();
            params.setAuth(soapContextService.getWrongCreds());

            contextPort.change(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void change_noContextInChange() {
        try {
            Change params = new Change();
            params.setAuth(contextMasterCreds);

            contextPort.change(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void change_allSet() {
        try {
            Change params = new Change();
            params.setAuth(contextMasterCreds);
            params.setCtx(new Context());
            contextPort.change(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    //====== changeCapabilities ======
    @Test
    void changeCapabilities_noCredsInChange() {
        ChangeCapabilities params = new ChangeCapabilities();
        try {
            contextPort.changeCapabilities(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeCapabilities_wrongCredsInChange() {
        ChangeCapabilities params = new ChangeCapabilities();
        params.setAuth(soapContextService.getWrongCreds());
        try {
            contextPort.changeCapabilities(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeCapabilities_noContextInChange() {
        ChangeCapabilities params = new ChangeCapabilities();
        params.setAuth(contextMasterCreds);
        try {
            contextPort.changeCapabilities(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeCapabilities_noContextInChange2() {
        ChangeCapabilities params = new ChangeCapabilities();
        params.setAuth(contextMasterCreds);
        params.setCapsToAdd("guard");
        try {
            contextPort.changeCapabilities(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeCapabilities_contextButNothingToChange() {
        ChangeCapabilities params = new ChangeCapabilities();
        params.setAuth(contextMasterCreds);
        params.setCtx(new Context());
        try {
            contextPort.changeCapabilities(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeCapabilities_contextNotCorrectDefined() {
        ChangeCapabilities params = new ChangeCapabilities();
        params.setAuth(contextMasterCreds);
        params.setCtx(new Context());
        params.setCapsToAdd("guard");
        try {
            contextPort.changeCapabilities(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeCapabilities_allGood() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception {
        Context context = createContext();

        ChangeCapabilities params = new ChangeCapabilities();
        params.setAuth(contextMasterCreds);
        params.setCtx(context);
        params.setCapsToAdd("guard");
        try {
            contextPort.changeCapabilities(params);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    //====== changeModuleAccess ======
    @Test
    void changeModuleAccess_emptyParam() {
        ChangeModuleAccess cma = new ChangeModuleAccess();
        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_paramWithCreds() {
        ChangeModuleAccess cma = new ChangeModuleAccess();
        cma.setAuth(contextMasterCreds);
        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_paramWithAuthAndEmptyUserModuleAccess() {
        ChangeModuleAccess cma = new ChangeModuleAccess();
        cma.setAuth(contextMasterCreds);
        UserModuleAccess uma = new UserModuleAccess();
        cma.setAccess(uma);
        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_userModuleAccessButNoCreds() {
        ChangeModuleAccess cma = new ChangeModuleAccess();
        UserModuleAccess uma = new UserModuleAccess();
        cma.setAccess(uma);
        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_paramWithAuthAndUserModuleAccessAndEmptyContext() {
        ChangeModuleAccess cma = new ChangeModuleAccess();
        cma.setAuth(contextMasterCreds);
        UserModuleAccess uma = new UserModuleAccess();
        cma.setAccess(uma);
        cma.setCtx(new Context());
        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_contextDoesNotExist(){
        ChangeModuleAccess cma = new ChangeModuleAccess();
        cma.setAuth(contextMasterCreds);
        UserModuleAccess uma = new UserModuleAccess();
        cma.setAccess(uma);
        Context newContext = getDefinedContext();
        cma.setCtx(newContext);

        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(NoSuchContextException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_paramsGoodButWrongCreds() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception {
        Context context = createContext();

        ChangeModuleAccess cma = new ChangeModuleAccess();
        cma.setAuth(soapContextService.getWrongCreds());
        UserModuleAccess uma = new UserModuleAccess();
        cma.setAccess(uma);
        cma.setCtx(context);
        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_allGood() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception {
        Context context = createContext();

        ChangeModuleAccess cma = new ChangeModuleAccess();
        cma.setAuth(contextMasterCreds);
        UserModuleAccess uma = new UserModuleAccess();
        cma.setAccess(uma);
        cma.setCtx(context);
        try {
            contextPort.changeModuleAccess(cma);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    //====== changeModuleAccessByName ======
    @Test
    void changeModuleAccessByName_emptyParam() {
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_paramWithCreds() {
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(contextMasterCreds);
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_paramWithAuthAndUnknownAccessCombinationName() {
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(contextMasterCreds);
        cma.setAccessCombinationName(RandomStringUtils.randomAlphabetic(5));
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_unknownContext() {
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(contextMasterCreds);
        cma.setCtx(getDefinedContext());
        cma.setAccessCombinationName("calendar=false");
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(NoSuchContextException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_paramsGoodButUnknownAccessConbination() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception {
        Context context = createContext();
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(contextMasterCreds);
        cma.setCtx(context);
        cma.setAccessCombinationName(RandomStringUtils.randomAlphabetic(6).concat("=".concat(RandomStringUtils.randomAlphabetic(6))));
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_paramsGoodButBrokenAccessConbination() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception {
        Context context = createContext();
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(contextMasterCreds);
        cma.setCtx(context);
        cma.setAccessCombinationName(RandomStringUtils.randomAlphabetic(6));
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_paramsGoodButWrongCreds() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception {
        Context context = createContext();
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(soapContextService.getWrongCreds());
        cma.setCtx(context);
        cma.setAccessCombinationName("calendar=false");
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_allGood() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception {
        Context context = createContext();
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(contextMasterCreds);
        cma.setCtx(context);
        cma.setAccessCombinationName("all");
        try {
            contextPort.changeModuleAccessByName(cma);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    //====== enable ======
    @Test
    void enable_emptyParam() {
        Enable enable = new Enable();
        try {
            contextPort.enable(enable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void enable_credsButNoContext() {
        Enable enable = new Enable();
        enable.setAuth(contextMasterCreds);
        try {
            contextPort.enable(enable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void enable_contextButNoCreds() {
        Enable enable = new Enable();
        enable.setCtx(getDefinedContext());
        try {
            contextPort.enable(enable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(NoSuchContextException_Exception.class, e);
        }
    }

    @Test
    void enable_credsButNotExistingContext() {
        Enable enable = new Enable();
        enable.setCtx(getDefinedContext());
        enable.setAuth(contextMasterCreds);
        try {
            contextPort.enable(enable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(NoSuchContextException_Exception.class, e);
        }
    }

    @Test
    void enable_WrongCreds() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception {
        Context context = createContext();

        Enable enable = new Enable();
        enable.setCtx(context);
        enable.setAuth(soapContextService.getWrongCreds());
        try {
            contextPort.enable(enable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void enable_allGood() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception {
        Context context = createContext();
        Enable enable = new Enable();
        enable.setCtx(context);
        enable.setAuth(contextMasterCreds);
        try {
            contextPort.enable(enable);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            fail("Unexpected exception.", e);
        }
    }

    //====== disable ======
    @Test
    void disable_emptyParam() {
        Disable disable = new Disable();
        try {
            contextPort.disable(disable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | NoSuchReasonException_Exception | OXContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void disable_credsButNoContext() {
        Disable disable = new Disable();
        disable.setAuth(contextMasterCreds);
        try {
            contextPort.disable(disable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | NoSuchReasonException_Exception | OXContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void disable_contextButNoCreds() {
        Disable disable = new Disable();
        disable.setCtx(getDefinedContext());
        try {
            contextPort.disable(disable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | NoSuchReasonException_Exception | OXContextException_Exception e) {
            Assertions.assertInstanceOf(NoSuchContextException_Exception.class, e);
        }
    }

    @Test
    void disable_credsButNotExistingContext() {
        Disable disable = new Disable();
        disable.setCtx(getDefinedContext());
        disable.setAuth(contextMasterCreds);
        try {
            contextPort.disable(disable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | NoSuchReasonException_Exception | OXContextException_Exception e) {
            Assertions.assertInstanceOf(NoSuchContextException_Exception.class, e);
        }
    }

    @Test
    void disable_WrongCreds() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception {
        Context context = createContext();

        Disable disable = new Disable();
        disable.setCtx(context);
        disable.setAuth(soapContextService.getWrongCreds());
        try {
            contextPort.disable(disable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | NoSuchReasonException_Exception | OXContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void disable_allGood() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception {
        Context context = createContext();
        Disable disable = new Disable();
        disable.setCtx(context);
        disable.setAuth(contextMasterCreds);
        try {
            contextPort.disable(disable);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | NoSuchReasonException_Exception | OXContextException_Exception e) {
            fail("Unexpected exception.", e);
        }
    }
}
