
package com.openexchange.soap.test;

import static org.junit.jupiter.api.Assertions.fail;
import java.net.MalformedURLException;
import java.util.List;
import javax.xml.ws.BindingProvider;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.openexchange.admin.soap.util.dataobjects.Database;
import com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception;
import com.openexchange.admin.soap.util.soap.InvalidDataException_Exception;
import com.openexchange.admin.soap.util.soap.OXUtilServicePortType;
import com.openexchange.admin.soap.util.soap.RemoteException_Exception;
import com.openexchange.admin.soap.util.soap.StorageException_Exception;
import com.openexchange.ajax.framework.AbstractTestEnvironment;
import com.openexchange.test.common.test.pool.soap.SoapUtilService;

class SoapAPIUtilErrorTests extends AbstractTestEnvironment {

    private com.openexchange.admin.soap.util.dataobjects.Credentials utilMasterCreds;

    private OXUtilServicePortType utilPort;

    private SoapUtilService soapUtilService;

    @BeforeEach
    public void init() throws MalformedURLException {
        soapUtilService = SoapUtilService.getInstance();
        utilPort = (OXUtilServicePortType) soapUtilService.getPortType();
        ((BindingProvider) utilPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, soapUtilService.getWsdlLocation().toString());
        utilMasterCreds = soapUtilService.getSoapMasterCreds();
    }

    @Test
    void checkDatabase_paramNull() {
        try {
            utilPort.checkDatabase(null);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void checkDatabase_wrongCreds() {
        try {
            utilPort.checkDatabase(soapUtilService.getWrongCreds());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void checkDatabase_allGood() {
        try {
            utilPort.checkDatabase(utilMasterCreds);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    @Test
    void listDatabase_parametersNull() {
        try {
            utilPort.listDatabase(null, null);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void listDatabase_searchPatternGivenNoCreds() {
        try {
            utilPort.listDatabase("*", null);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void listDatabase_credsGivenNoSearchParam() {
        try {
            List<Database> listDatabase = utilPort.listDatabase(null, utilMasterCreds);
            Assertions.assertTrue(listDatabase.size() > 0);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexception exception", e);
        }
    }

    @Test
    void listDatabase_wrongCreds() {
        try {
            utilPort.listDatabase(null, soapUtilService.getWrongCreds());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void listDatabase_searchWithoutResult() {
        try {
            List<Database> listDatabase = utilPort.listDatabase(RandomStringUtils.randomAlphabetic(6), utilMasterCreds);
            Assertions.assertEquals(0, listDatabase.size());
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexception exception", e);
        }
    }

    @Test
    void listDatabase_allGood() {
        try {
            List<Database> listDatabase = utilPort.listDatabase("*", utilMasterCreds);
            Assertions.assertTrue(listDatabase.size() > 0);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexception exception", e);
        }
    }
}
