
package com.openexchange.soap.test.reseller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.net.MalformedURLException;
import java.util.List;
import javax.xml.ws.BindingProvider;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import com.openexchange.admin.soap.reseller.context.reseller.soap.Change;
import com.openexchange.admin.soap.reseller.context.reseller.soap.ChangeModuleAccess;
import com.openexchange.admin.soap.reseller.context.reseller.soap.ChangeModuleAccessByName;
import com.openexchange.admin.soap.reseller.context.reseller.soap.ContextExistsException_Exception;
import com.openexchange.admin.soap.reseller.context.reseller.soap.Disable;
import com.openexchange.admin.soap.reseller.context.reseller.soap.DuplicateExtensionException_Exception;
import com.openexchange.admin.soap.reseller.context.reseller.soap.Enable;
import com.openexchange.admin.soap.reseller.context.reseller.soap.InvalidCredentialsException_Exception;
import com.openexchange.admin.soap.reseller.context.reseller.soap.InvalidDataException_Exception;
import com.openexchange.admin.soap.reseller.context.reseller.soap.NoSuchContextException_Exception;
import com.openexchange.admin.soap.reseller.context.reseller.soap.NoSuchDatabaseException_Exception;
import com.openexchange.admin.soap.reseller.context.reseller.soap.NoSuchFilestoreException_Exception;
import com.openexchange.admin.soap.reseller.context.reseller.soap.NoSuchReasonException_Exception;
import com.openexchange.admin.soap.reseller.context.reseller.soap.OXContextException_Exception;
import com.openexchange.admin.soap.reseller.context.reseller.soap.OXResellerContextServicePortType;
import com.openexchange.admin.soap.reseller.context.reseller.soap.RemoteException_Exception;
import com.openexchange.admin.soap.reseller.context.reseller.soap.StorageException_Exception;
import com.openexchange.admin.soap.reseller.context.reseller.soap.dataobjects.ResellerContext;
import com.openexchange.admin.soap.reseller.context.rmi.dataobjects.Database;
import com.openexchange.admin.soap.reseller.context.rmi.dataobjects.Filestore;
import com.openexchange.admin.soap.reseller.context.soap.dataobjects.Context;
import com.openexchange.admin.soap.reseller.context.soap.dataobjects.SchemaSelectStrategy;
import com.openexchange.admin.soap.reseller.context.soap.dataobjects.User;
import com.openexchange.admin.soap.reseller.context.soap.dataobjects.UserModuleAccess;
import com.openexchange.admin.soap.util.soap.OXUtilServicePortType;
import com.openexchange.ajax.framework.AbstractTestEnvironment;
import com.openexchange.test.common.test.pool.soap.SoapUtilService;
import com.openexchange.test.common.test.pool.soap.reseller.SoapResellerContextService;

@Disabled("Reseller Setup not tested in pipeline automation.")
class SoapAPIResellerContextErrorTests extends AbstractTestEnvironment {

    private OXResellerContextServicePortType contextPort;

    private com.openexchange.admin.soap.reseller.context.rmi.dataobjects.Credentials soapMasterCreds;

    private SoapResellerContextService soapResellerContextService;

    private OXUtilServicePortType utilPort;

    private com.openexchange.admin.soap.util.dataobjects.Credentials utilCreds;

    @BeforeEach
    public void init() throws MalformedURLException {
        soapResellerContextService = SoapResellerContextService.getInstance();
        contextPort = (OXResellerContextServicePortType) soapResellerContextService.getPortType();
        ((BindingProvider) contextPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, soapResellerContextService.getWsdlLocation().toString());
        soapMasterCreds = soapResellerContextService.getSoapMasterCreds();

        SoapUtilService soapUtilService = SoapUtilService.getInstance();
        utilPort = (OXUtilServicePortType) soapUtilService.getPortType();
        utilCreds = soapUtilService.getSoapMasterCreds();
    }

    private ResellerContext createContext() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception, DuplicateExtensionException_Exception {
        ResellerContext newContext = getFilledContext();
        User admin = getFilledUser();
        ResellerContext context = contextPort.create(newContext, admin, soapMasterCreds, new SchemaSelectStrategy());
        return context;
    }

    private User getFilledUser() {
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);
        return admin;
    }

    private ResellerContext getFilledContext() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilCreds).get(0);
        Integer filestoreId = filestore.getId();
        ResellerContext newContext = new ResellerContext();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestoreId);
        newContext.setName(RandomStringUtils.randomAlphabetic(10));
        return newContext;
    }

    //====== listAll ======
    @Test
    void testListAll_credParamNull() {
        try {
            contextPort.listAll(null);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void testListAll_invalidCredsParam() {
        try {
            contextPort.listAll(soapResellerContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    //====== list ======
    @Test
    void testList_emptyStringAuthParamNull() {
        try {
            contextPort.list("", Boolean.FALSE, null);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void testList_emptyStringWrongCreds() {
        try {
            contextPort.list("", Boolean.FALSE, soapResellerContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void testList_nullParamWrongCreds() {
        try {
            contextPort.list(null, Boolean.FALSE, soapResellerContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void testList_searchParamNull_returnAllContexts() {
        try {
            List<ResellerContext> list = contextPort.list(null, Boolean.FALSE, soapMasterCreds);
            assertTrue(list.size() > 0);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    @Test
    void testList_emptyString_returnAllContexts() {
        try {
            List<ResellerContext> list = contextPort.list("", Boolean.FALSE, soapMasterCreds);
            assertTrue(list.size() > 0);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    @Test
    void testList_notExistingContext() {
        try {
            List<ResellerContext> list = contextPort.list(RandomStringUtils.randomAlphanumeric(10), Boolean.FALSE, soapMasterCreds);
            assertEquals(0, list.size());
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    //====== listByDatabase ======
    @Test
    void testListByDatabase_dbNameNullAndWrongCreds() {
        try {
            contextPort.listByDatabase(null, Boolean.FALSE, soapResellerContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (NoSuchDatabaseException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void testListByDatabase_dbWithNoDataAndWrongCreds() {
        Database db = new Database();
        try {
            contextPort.listByDatabase(db, Boolean.FALSE, soapResellerContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (NoSuchDatabaseException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void testListByDatabase_dbCorrectDataButWrongCreds() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        Database db = new Database();
        com.openexchange.admin.soap.util.dataobjects.Database database = utilPort.listDatabase("oxdatabase", utilCreds).get(0);
        db.setId(database.getId());
        try {
            contextPort.listByDatabase(db, Boolean.FALSE, soapResellerContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (NoSuchDatabaseException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void testListByDatabase_dbNameNull() {
        try {
            contextPort.listByDatabase(null, Boolean.FALSE, soapMasterCreds);
            fail("Exception not thrown");
        } catch (NoSuchDatabaseException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void testListByDatabase_dbWithNoData() {
        Database db = new Database();
        try {
            contextPort.listByDatabase(db, Boolean.FALSE, soapMasterCreds);
            fail("Exception not thrown");
        } catch (NoSuchDatabaseException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void testListByDatabase_dbCorrectData() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        Database db = new Database();
        com.openexchange.admin.soap.util.dataobjects.Database database = utilPort.listDatabase("oxdatabase", utilCreds).get(0);
        db.setId(database.getId());
        try {
            List<ResellerContext> listByDatabase = contextPort.listByDatabase(db, Boolean.FALSE, soapMasterCreds);
            assertTrue(listByDatabase.size() > 0);
        } catch (NoSuchDatabaseException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    //====== listByFilestore ======
    @Test
    void listByFilestore_fsCorrectData() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        Filestore fs = new Filestore();
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilCreds).get(0);
        fs.setId(filestore.getId());
        try {
            List<ResellerContext> listByFilestore = contextPort.listByFilestore(fs, Boolean.FALSE, soapMasterCreds);
            assertTrue(listByFilestore.size() > 0);
        } catch (NoSuchFilestoreException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    @Test
    void listByFilestore_fsCorrectDataWrongCreds() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        Filestore fs = new Filestore();
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilCreds).get(0);
        fs.setId(filestore.getId());
        try {
            contextPort.listByFilestore(fs, Boolean.FALSE, soapResellerContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (NoSuchFilestoreException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void listByFilestore_fsNullWrongCreds() {
        try {
            contextPort.listByFilestore(null, Boolean.FALSE, soapResellerContextService.getWrongCreds());
            fail("Exception not thrown");
        } catch (NoSuchFilestoreException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void listByFilestore_fsNull() {
        try {
            contextPort.listByFilestore(null, Boolean.FALSE, soapMasterCreds);
            fail("Exception not thrown");
        } catch (NoSuchFilestoreException_Exception | StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    //====== create ======
    @Test
    void create_noParam() {
        try {
            contextPort.create(null, null, null, null);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_credsOnly() {
        try {
            contextPort.create(null, null, soapMasterCreds, null);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_credsAndSchemaSelectStrategy() {
        try {
            contextPort.create(null, null, soapMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_contextCredsAndSchemaSelectStrategy() {
        try {
            ResellerContext newContext = new ResellerContext();
            contextPort.create(newContext, null, soapMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_allParamsButSomethingMissing() {
        try {
            ResellerContext newContext = new ResellerContext();
            User admin = new User();

            contextPort.create(newContext, admin, soapMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_allParamsForContextButAdminNotCorrect() {
        try {
            ResellerContext newContext = new ResellerContext();
            newContext.setMaxQuota(Long.valueOf(1000));
            User admin = new User();

            contextPort.create(newContext, admin, soapMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_allButFileStoreIdCorrectlySet() {
        ResellerContext newContext = new ResellerContext();
        newContext.setMaxQuota(Long.valueOf(1000));
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        admin.setPrimaryEmail(RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org");
        admin.setEmail1(RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org");

        try {
            contextPort.create(newContext, admin, soapMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(StorageException_Exception.class, e);
        }
    }

    @Test
    void create_priamryMailAndEmailDifferent() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilCreds).get(0);

        ResellerContext newContext = new ResellerContext();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestore.getId());
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        admin.setPrimaryEmail(RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org");
        admin.setEmail1(RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org");

        try {
            contextPort.create(newContext, admin, soapMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(StorageException_Exception.class, e);
        }
    }

    @Test
    void create_allCorrectlySet() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilCreds).get(0);
        Integer filestoreId = filestore.getId();

        ResellerContext newContext = new ResellerContext();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestoreId);
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);

        try {
            Context context = contextPort.create(newContext, admin, soapMasterCreds, new SchemaSelectStrategy());
            assertEquals(filestoreId, context.getFilestoreId());
            assertTrue(context.isEnabled().booleanValue());
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    @Test
    void create_credsNull() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilCreds).get(0);
        Integer filestoreId = filestore.getId();

        ResellerContext newContext = new ResellerContext();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestoreId);
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);

        try {
            contextPort.create(newContext, admin, null, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void create_credsWrong() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilCreds).get(0);
        Integer filestoreId = filestore.getId();

        ResellerContext newContext = new ResellerContext();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestoreId);
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);

        try {
            contextPort.create(newContext, admin, soapResellerContextService.getWrongCreds(), new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void create_schemaSelectNull() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilCreds).get(0);
        Integer filestoreId = filestore.getId();

        ResellerContext newContext = new ResellerContext();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestoreId);
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);

        try {
            Context context = contextPort.create(newContext, admin, soapMasterCreds, null);
            assertEquals(filestoreId, context.getFilestoreId());
            assertTrue(context.isEnabled().booleanValue());
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    @Test
    void create_adminNull() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilCreds).get(0);
        Integer filestoreId = filestore.getId();

        ResellerContext newContext = new ResellerContext();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestoreId);
        try {
            contextPort.create(newContext, null, soapMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void create_contextNull() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        com.openexchange.admin.soap.util.dataobjects.Filestore filestore = utilPort.listAllFilestore(utilCreds).get(0);
        Integer filestoreId = filestore.getId();

        ResellerContext newContext = new ResellerContext();
        newContext.setMaxQuota(Long.valueOf(1000));
        newContext.setFilestoreId(filestoreId);
        User admin = new User();
        admin.setName(RandomStringUtils.randomAlphabetic(5));
        admin.setDisplayName(RandomStringUtils.randomAlphabetic(5));
        admin.setPassword(RandomStringUtils.randomAlphabetic(5));
        admin.setGivenName(RandomStringUtils.randomAlphabetic(5));
        admin.setSurName(RandomStringUtils.randomAlphabetic(5));
        String email = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".org";
        admin.setPrimaryEmail(email);
        admin.setEmail1(email);

        try {
            contextPort.create(null, admin, soapMasterCreds, new SchemaSelectStrategy());
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | ContextExistsException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    //====== change ======
    @Test
    void change_noCredsInChange() {
        try {
            Change params = new Change();

            contextPort.change(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void change_wrongCredsInChange() {
        try {
            Change params = new Change();
            params.setAuth(soapResellerContextService.getWrongCreds());

            contextPort.change(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void change_noContextInChange() {
        try {
            Change params = new Change();
            params.setAuth(soapMasterCreds);

            contextPort.change(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void change_allSet() {
        try {
            Change params = new Change();
            params.setAuth(soapMasterCreds);
            params.setCtx(new ResellerContext());
            contextPort.change(params);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    //====== changeModuleAccess ======
    @Test
    void changeModuleAccess_emptyParam() {
        ChangeModuleAccess cma = new ChangeModuleAccess();
        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_paramWithCreds() {
        ChangeModuleAccess cma = new ChangeModuleAccess();
        cma.setAuth(soapMasterCreds);
        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_paramWithAuthAndEmptyUserModuleAccess() {
        ChangeModuleAccess cma = new ChangeModuleAccess();
        cma.setAuth(soapMasterCreds);
        UserModuleAccess uma = new UserModuleAccess();
        cma.setAccess(uma);
        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_userModuleAccessButNoCreds() {
        ChangeModuleAccess cma = new ChangeModuleAccess();
        UserModuleAccess uma = new UserModuleAccess();
        cma.setAccess(uma);
        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_paramWithAuthAndUserModuleAccessAndEmptyContext() {
        ChangeModuleAccess cma = new ChangeModuleAccess();
        cma.setAuth(soapMasterCreds);
        UserModuleAccess uma = new UserModuleAccess();
        cma.setAccess(uma);
        cma.setCtx(new ResellerContext());
        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_contextDoesNotExist() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        ChangeModuleAccess cma = new ChangeModuleAccess();
        cma.setAuth(soapMasterCreds);
        UserModuleAccess uma = new UserModuleAccess();
        cma.setAccess(uma);
        ResellerContext newContext = getFilledContext();
        cma.setCtx(newContext);

        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(NoSuchContextException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_paramsGoodButWrongCreds() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception, DuplicateExtensionException_Exception {
        ResellerContext context = createContext();

        ChangeModuleAccess cma = new ChangeModuleAccess();
        cma.setAuth(soapResellerContextService.getWrongCreds());
        UserModuleAccess uma = new UserModuleAccess();
        cma.setAccess(uma);
        cma.setCtx(context);
        try {
            contextPort.changeModuleAccess(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccess_allGood() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception, DuplicateExtensionException_Exception {
        ResellerContext context = createContext();

        ChangeModuleAccess cma = new ChangeModuleAccess();
        cma.setAuth(soapMasterCreds);
        UserModuleAccess uma = new UserModuleAccess();
        cma.setAccess(uma);
        cma.setCtx(context);
        try {
            contextPort.changeModuleAccess(cma);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    //====== changeModuleAccessByName ======
    @Test
    void changeModuleAccessByName_emptyParam() {
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_paramWithCreds() {
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(soapMasterCreds);
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_paramWithAuthAndUnknownAccessCombinationName() {
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(soapMasterCreds);
        cma.setAccessCombinationName(RandomStringUtils.randomAlphabetic(5));
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_unknownContext() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(soapMasterCreds);
        cma.setCtx(getFilledContext());
        cma.setAccessCombinationName("calendar=false");
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(NoSuchContextException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_paramsGoodButUnknownAccessConbination() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception, DuplicateExtensionException_Exception {
        ResellerContext context = createContext();
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(soapMasterCreds);
        cma.setCtx(context);
        cma.setAccessCombinationName(RandomStringUtils.randomAlphabetic(6).concat("=".concat(RandomStringUtils.randomAlphabetic(6))));
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_paramsGoodButBrokenAccessConbination() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception, DuplicateExtensionException_Exception {
        ResellerContext context = createContext();
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(soapMasterCreds);
        cma.setCtx(context);
        cma.setAccessCombinationName(RandomStringUtils.randomAlphabetic(6));
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_paramsGoodButWrongCreds() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception, DuplicateExtensionException_Exception {
        ResellerContext context = createContext();
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(soapResellerContextService.getWrongCreds());
        cma.setCtx(context);
        cma.setAccessCombinationName("calendar=false");
        try {
            contextPort.changeModuleAccessByName(cma);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void changeModuleAccessByName_allGood() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception, DuplicateExtensionException_Exception {
        ResellerContext context = createContext();
        ChangeModuleAccessByName cma = new ChangeModuleAccessByName();
        cma.setAuth(soapMasterCreds);
        cma.setCtx(context);
        cma.setAccessCombinationName("all");
        try {
            contextPort.changeModuleAccessByName(cma);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            fail("Unexpected exception", e);
        }
    }

    //====== enable ======
    @Test
    void enable_emptyParam() {
        Enable enable = new Enable();
        try {
            contextPort.enable(enable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void enable_credsButNoContext() {
        Enable enable = new Enable();
        enable.setAuth(soapMasterCreds);
        try {
            contextPort.enable(enable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void enable_contextButNoCreds() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        Enable enable = new Enable();
        enable.setCtx(getFilledContext());
        try {
            contextPort.enable(enable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(NoSuchContextException_Exception.class, e);
        }
    }

    @Test
    void enable_credsButNotExistingContext() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        Enable enable = new Enable();
        enable.setCtx(getFilledContext());
        enable.setAuth(soapMasterCreds);
        try {
            contextPort.enable(enable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(NoSuchContextException_Exception.class, e);
        }
    }

    @Test
    void enable_WrongCreds() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception, DuplicateExtensionException_Exception {
        ResellerContext context = createContext();

        Enable enable = new Enable();
        enable.setCtx(context);
        enable.setAuth(soapResellerContextService.getWrongCreds());
        try {
            contextPort.enable(enable);
            fail("Exception not thrown");
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void enable_allGood() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception, DuplicateExtensionException_Exception {
        ResellerContext context = createContext();
        Enable enable = new Enable();
        enable.setCtx(context);
        enable.setAuth(soapMasterCreds);
        try {
            contextPort.enable(enable);
        } catch (StorageException_Exception | InvalidCredentialsException_Exception | InvalidDataException_Exception | RemoteException_Exception | NoSuchContextException_Exception | DuplicateExtensionException_Exception e) {
            fail("Unexpected exception.", e);
        }
    }

    //====== disable ======
    @Test
    void disable_emptyParam() {
        Disable disable = new Disable();
        try {
            contextPort.disable(disable);
            fail("Exception not thrown");
        } catch (InvalidCredentialsException_Exception | DuplicateExtensionException_Exception | NoSuchContextException_Exception | NoSuchReasonException_Exception | StorageException_Exception | RemoteException_Exception | OXContextException_Exception | InvalidDataException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void disable_credsButNoContext() {
        Disable disable = new Disable();
        disable.setAuth(soapMasterCreds);
        try {
            contextPort.disable(disable);
            fail("Exception not thrown");
        } catch (InvalidCredentialsException_Exception | DuplicateExtensionException_Exception | NoSuchContextException_Exception | NoSuchReasonException_Exception | StorageException_Exception | RemoteException_Exception | OXContextException_Exception | InvalidDataException_Exception e) {
            Assertions.assertInstanceOf(InvalidDataException_Exception.class, e);
        }
    }

    @Test
    void disable_contextButNoCreds() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        Disable disable = new Disable();
        disable.setCtx(getFilledContext());
        try {
            contextPort.disable(disable);
            fail("Exception not thrown");
        } catch (InvalidCredentialsException_Exception | DuplicateExtensionException_Exception | NoSuchContextException_Exception | NoSuchReasonException_Exception | StorageException_Exception | RemoteException_Exception | OXContextException_Exception | InvalidDataException_Exception e) {
            Assertions.assertInstanceOf(NoSuchContextException_Exception.class, e);
        }
    }

    @Test
    void disable_credsButNotExistingContext() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception {
        Disable disable = new Disable();
        disable.setCtx(getFilledContext());
        disable.setAuth(soapMasterCreds);
        try {
            contextPort.disable(disable);
            fail("Exception not thrown");
        } catch (InvalidCredentialsException_Exception | DuplicateExtensionException_Exception | NoSuchContextException_Exception | NoSuchReasonException_Exception | StorageException_Exception | RemoteException_Exception | OXContextException_Exception | InvalidDataException_Exception e) {
            Assertions.assertInstanceOf(NoSuchContextException_Exception.class, e);
        }
    }

    @Test
    void disable_WrongCreds() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception, DuplicateExtensionException_Exception {
        ResellerContext context = createContext();

        Disable disable = new Disable();
        disable.setCtx(context);
        disable.setAuth(soapResellerContextService.getWrongCreds());
        try {
            contextPort.disable(disable);
            fail("Exception not thrown");
        } catch (InvalidCredentialsException_Exception | DuplicateExtensionException_Exception | NoSuchContextException_Exception | NoSuchReasonException_Exception | StorageException_Exception | RemoteException_Exception | OXContextException_Exception | InvalidDataException_Exception e) {
            Assertions.assertInstanceOf(InvalidCredentialsException_Exception.class, e);
        }
    }

    @Test
    void disable_allGood() throws com.openexchange.admin.soap.util.soap.StorageException_Exception, com.openexchange.admin.soap.util.soap.InvalidCredentialsException_Exception, com.openexchange.admin.soap.util.soap.InvalidDataException_Exception, com.openexchange.admin.soap.util.soap.RemoteException_Exception, StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, ContextExistsException_Exception, RemoteException_Exception, DuplicateExtensionException_Exception {
        ResellerContext context = createContext();
        Disable disable = new Disable();
        disable.setCtx(context);
        disable.setAuth(soapMasterCreds);
        try {
            contextPort.disable(disable);
        } catch (InvalidCredentialsException_Exception | DuplicateExtensionException_Exception | NoSuchContextException_Exception | NoSuchReasonException_Exception | StorageException_Exception | RemoteException_Exception | OXContextException_Exception | InvalidDataException_Exception e) {
            fail("Unexpected exception.", e);
        }
    }
}
