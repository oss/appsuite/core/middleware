/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.soap.test;

import static com.openexchange.java.Autoboxing.I;
import java.net.MalformedURLException;
import java.util.Optional;
import javax.xml.ws.BindingProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.openexchange.admin.soap.context.dataobjects.Context;
import com.openexchange.admin.soap.context.dataobjects.Credentials;
import com.openexchange.admin.soap.context.soap.Disable;
import com.openexchange.admin.soap.context.soap.InvalidCredentialsException_Exception;
import com.openexchange.admin.soap.context.soap.InvalidDataException_Exception;
import com.openexchange.admin.soap.context.soap.NoSuchContextException_Exception;
import com.openexchange.admin.soap.context.soap.NoSuchReasonException_Exception;
import com.openexchange.admin.soap.context.soap.OXContextException_Exception;
import com.openexchange.admin.soap.context.soap.OXContextServicePortType;
import com.openexchange.admin.soap.context.soap.RemoteException_Exception;
import com.openexchange.admin.soap.context.soap.StorageException_Exception;
import com.openexchange.ajax.framework.AbstractAPIClientSession;
import com.openexchange.test.common.test.pool.soap.SoapContextService;
import com.openexchange.test.common.tools.LoginTools;
import com.openexchange.testing.httpclient.invoker.ApiException;

/**
 * {@link ContextCacheTest}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class ContextCacheTest extends AbstractAPIClientSession {

    private OXContextServicePortType contextPort;
    private Credentials contextMasterCreds;

    @BeforeEach
    public void init() throws MalformedURLException {
        SoapContextService soapContextService = SoapContextService.getInstance();
        contextPort = (OXContextServicePortType) soapContextService.getPortType();
        ((BindingProvider) contextPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, soapContextService.getWsdlLocation().toString());
        contextMasterCreds = soapContextService.getSoapMasterCreds();
    }

    @Test
    public void testContextChange() throws StorageException_Exception, InvalidCredentialsException_Exception, InvalidDataException_Exception, NoSuchContextException_Exception, RemoteException_Exception, ApiException, NoSuchReasonException_Exception, OXContextException_Exception {
        // Call getApiClient to init the client
        this.testUser.getApiClient();

        // 1. Check context is enabled
        this.testUser.performLogout();
        this.testUser.performLogin();

        // Logout again before change
        this.testUser.performLogout();

        // 2. Change the context name
        Context ctx = new Context();
        ctx.setId(I(this.testContext.getId()));
        ctx.setEnabled(Boolean.FALSE);
        Disable disable = new Disable();
        disable.setCtx(ctx);
        disable.setAuth(contextMasterCreds);
        contextPort.disable(disable);

        // 3. Check context is disabled
        LoginTools.performLogin(getApiClient(), Optional.empty(), null, "AUTHORIZATION-0001");
    }

}
