/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.dav.caldav.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import java.util.Map;
import org.apache.jackrabbit.webdav.client.methods.MoveMethod;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import com.openexchange.ajax.chronos.factory.AttendeeFactory;
import com.openexchange.ajax.chronos.factory.EventFactory;
import com.openexchange.ajax.chronos.manager.EventManager;
import com.openexchange.ajax.chronos.util.ChronosUtils;
import com.openexchange.dav.StatusCodes;
import com.openexchange.dav.SyncToken;
import com.openexchange.dav.caldav.CalDAVChronosBridge;
import com.openexchange.dav.caldav.ICalResource;
import com.openexchange.dav.caldav.ical.SimpleICal.Property;
import com.openexchange.java.Consumers.OXConsumer;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.webdav.protocol.WebdavPath;

/**
 * {@link MoveSeriesTest}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class MoveSeriesTest extends CalDAVChronosBridge {

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
    }

    @ParameterizedTest
    @MethodSource("availableAuthMethods")
    public void testMoveSeriesOnClient(@SuppressWarnings("unused") String authMethod) throws Exception {
        /*
         * create subfolder & fetch sync token for later synchronization
         */
        String privateCalendarFolder = userA.folderManager().createCalendarFolder("CalDAVTest", getUserId());
        EventManager privateFolderEventManager = new EventManager(userA.defaultUserApi(), privateCalendarFolder);
        SyncToken syncToken = new SyncToken(fetchSyncToken());
        /*
         * Create event in standard folder
         */
        EventData eventInStandardPrivateFolder = userA.eventManager()
                                                      .createEvent(EventFactory.createSeriesEvent(userA.testUser().getUserId(), "I like to move it!", 10, null)
                                                                               .attendees(List.of(AttendeeFactory.createIndividual(testUser2))));
        String uid = eventInStandardPrivateFolder.getUid();
        /*
         * verify appointment on client
         */
        Map<String, String> eTags = syncCollection(syncToken).getETagsStatusOK();
        assertTrue(0 < eTags.size(), "no resource changes reported on sync collection");
        List<ICalResource> calendarData = calendarMultiget(eTags.keySet());
        ICalResource iCalResource = assertContains(uid, calendarData);
        assertNotNull(iCalResource.getVEvent(), "No VEVENT in iCal found");
        Property attendee = iCalResource.getVEvent().getAttendee(getClient().getValues().getDefaultAddress());
        assertNotNull(attendee, "Attendee not found in iCal");
        /*
         * move appointment to subfolder on client
         */
        MoveMethod move = null;
        String targetHref = "/caldav/" + encodeFolderID(privateCalendarFolder) + '/' + new WebdavPath(iCalResource.getHref()).name();
        try {
            move = new MoveMethod(getBaseUri() + iCalResource.getHref(), getBaseUri() + targetHref, false);
            Assertions.assertEquals(StatusCodes.SC_CREATED, webDAVClient.executeMethod(move), "response code wrong");
        } finally {
            release(move);
        }
        /*
         * Get event from ("new") folder and check
         */
        EventData eventInCreatedPrivateFolder = privateFolderEventManager.getEvent(null, eventInStandardPrivateFolder.getId());
        assertEquals(eventInCreatedPrivateFolder.getSummary(), eventInStandardPrivateFolder.getSummary());
    }

    @ParameterizedTest
    @MethodSource("availableAuthMethods")
    public void testMoveOrphanedExceptionsOnClient(@SuppressWarnings("unused") String authMethod) throws Exception {
        SyncToken syncToken = new SyncToken(fetchSyncToken());
        /*
         * As user B, create event series with exception in standard folder
         */
        EventData eventInStandardPrivateFolder = userB.eventManager().createEvent(EventFactory.createSeriesEvent(userB.testUser().getUserId(), "I like to move it!", 10, null));
        List<EventData> events = ChronosUtils.getAllEventsOfEvent(userB.eventManager(), eventInStandardPrivateFolder);
        EventData changeException = createChangeException(eventInStandardPrivateFolder, events, 2);
        EventData changeException2 = createChangeException(eventInStandardPrivateFolder, events, 4);
        String uid = eventInStandardPrivateFolder.getUid();
        /*
         * As user A, create new private calendar folder and verify appointment on client
         */
        String privateCalendarFolder = userA.folderManager().createCalendarFolder("OrphanedChangeExceptions", getUserId());
        Map<String, String> eTags = syncCollection(syncToken).getETagsStatusOK();
        assertTrue(0 < eTags.size(), "no resource changes reported on sync collection");
        List<ICalResource> calendarData = calendarMultiget(eTags.keySet());
        ICalResource iCalResource = assertContains(uid, calendarData);
        assertNotNull(iCalResource.getVEvent(), "No VEVENT in iCal found");
        assertEquals(2, iCalResource.getVEvents().size());
        Property attendee = iCalResource.getVEvent().getAttendee(getClient().getValues().getDefaultAddress());
        assertNotNull(attendee, "Attendee not found in iCal");
        /*
         * As user A on client, move appointment to subfolder (implicitly using the series ID like real world clients)
         */
        MoveMethod move = null;
        String targetHref = "/caldav/" + encodeFolderID(privateCalendarFolder) + '/' + new WebdavPath(iCalResource.getHref()).name();
        try {
            move = new MoveMethod(getBaseUri() + iCalResource.getHref(), getBaseUri() + targetHref, false);
            Assertions.assertEquals(StatusCodes.SC_CREATED, webDAVClient.executeMethod(move), "response code wrong");
        } finally {
            release(move);
        }
        /*
         * As user A, get event from ("new") folder and check
         */
        EventManager eventManager = new EventManager(userA.defaultUserApi(), privateCalendarFolder);
        EventData orphanedException = eventManager.getEvent(null, changeException.getId());
        assertEquals(2, orphanedException.getAttendees().size());
        EventData orphanedException2 = eventManager.getEvent(null, changeException2.getId());
        assertEquals(2, orphanedException2.getAttendees().size());
        /*
         * As user B, get event and check that folder was untouched
         */
        List<EventData> untouchedEvents = ChronosUtils.getAllEventsOfEvent(userB.eventManager(), eventInStandardPrivateFolder);
        untouchedEvents.add(userB.eventManager().getEvent(null, changeException.getId()));//Explicit, even tough events should already be in response
        untouchedEvents.add(userB.eventManager().getEvent(null, changeException2.getId()));
        for (EventData eventData : untouchedEvents) {
            assertEquals(userB.defaultFolderId(), eventData.getFolder());
        }
    }

    private EventData createChangeException(EventData eventInStandardPrivateFolder, List<EventData> events, int occurrence) throws Exception {
        EventData changeException = createChangeException(eventInStandardPrivateFolder, events, occurrence, delta -> delta.setAttendees(AttendeeFactory.createIndividuals(testUser, testUser2)));
        assertNotNull(ChronosUtils.find(changeException.getAttendees(), testUser.getUserId()));
        return changeException;
    }

    private EventData createChangeException(EventData eventInStandardPrivateFolder, List<EventData> events, int occurrence, OXConsumer<EventData, Exception> change) throws Exception {
        EventData changeException = userB.eventManager().createChangeException(eventInStandardPrivateFolder, null, events.get(occurrence).getRecurrenceId(), change);
        assertNotEquals(eventInStandardPrivateFolder.getId(), changeException.getId());
        EventData loadedEvent = userB.eventManager().getEvent(null, eventInStandardPrivateFolder.getId());
        assertNull(ChronosUtils.find(loadedEvent.getAttendees(), testUser.getUserId()));
        return changeException;
    }

}
