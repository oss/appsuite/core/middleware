/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.dav.caldav.tests.chronos;

import static com.openexchange.dav.DAVTest.usesOAuth;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import com.openexchange.ajax.chronos.UserApi;
import com.openexchange.ajax.chronos.itip.IMipReceiver;
import com.openexchange.ajax.chronos.manager.EventManager;
import com.openexchange.ajax.framework.SessionAwareClient;
import com.openexchange.ajax.oauth.OAuthAccessTokenHelper;
import com.openexchange.chronos.common.CalendarUtils;
import com.openexchange.dav.StatusCodes;
import com.openexchange.dav.WebDAVClient;
import com.openexchange.dav.caldav.CalDAVTest;
import com.openexchange.dav.caldav.ICalResource;
import com.openexchange.dav.caldav.ical.SimpleICal.Property;
import com.openexchange.test.common.groupware.calendar.TimeTools;
import com.openexchange.testing.httpclient.invoker.ApiClient;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.Attendee;
import com.openexchange.testing.httpclient.models.EventData;
import com.openexchange.testing.httpclient.models.MailData;
import com.openexchange.testing.httpclient.models.MultipleEventData;
import com.openexchange.testing.httpclient.modules.ChronosApi;

/**
 * {@link ScheduleAgentTest}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class ScheduleAgentTest extends ChronosCaldavTest {

    protected UserApi userApi2;
    protected EventManager eventManager2;
    protected String defaultFolderId2;
    protected WebDAVClient webDAVClient2;

    @Override
    @BeforeEach
    public void setUp(TestInfo testInfo) throws Exception {
        super.setUp(testInfo);
        SessionAwareClient apiClient2 = testUser2.getApiClient();
        userApi2 = new UserApi(apiClient2, testUser2);
        defaultFolderId2 = getDefaultFolder(userApi2.getFoldersApi());
        eventManager2 = new EventManager(userApi2, defaultFolderId2);
        webDAVClient2 = new WebDAVClient(testUser2, getDefaultUserAgent(), usesOAuth(testInfo) ? OAuthAccessTokenHelper.getAccessToken(testUser2, List.of(SCOPE)) : null);
    }

    @ParameterizedTest
    @MethodSource("availableAuthMethods")
    public void testRequestScheduleAgentServer(@SuppressWarnings("unused") String authMethod) throws Exception {
        testRequest("SERVER", true);
    }

    @ParameterizedTest
    @MethodSource("availableAuthMethods")
    public void testRequestScheduleAgentNone(@SuppressWarnings("unused") String authMethod) throws Exception {
        testRequest("NONE", false);
    }

    @ParameterizedTest
    @MethodSource("availableAuthMethods")
    public void testRequestScheduleAgentClient(@SuppressWarnings("unused") String authMethod) throws Exception {
        testRequest("CLIENT", false);
    }

    private void testRequest(String scheduleAgent, boolean expectMail) throws Exception {
        /*
         * create appointment on client
         */
        String organizerMail = getClient().getValues().getDefaultAddress();
        String organizerUri = CalendarUtils.getURI(organizerMail);
        String attendeeUri = CalendarUtils.getURI(new com.openexchange.testing.httpclient.modules.UserApi(testUser2.getApiClient()).getUser(Integer.toString(testUser2.getUserId())).getData().getEmail1());
        String uid = randomUID();
        String summary = randomUID();
        Date start = TimeTools.D("next monday at 12:00");
        Date end = TimeTools.D("next monday at 13:00");
        String iCal = // @formatter:off
            "BEGIN:VCALENDAR" + "\r\n" +
            "VERSION:2.0" + "\r\n" +
            "BEGIN:VEVENT" + "\r\n" +
            "UID:" + uid + "\r\n" +
            "DTSTAMP:" + formatAsUTC(new Date()) + "\r\n" +
            "DTSTART:" + formatAsUTC(start) + "\r\n" +
            "DTEND:" + formatAsUTC(end) + "\r\n" +
            "SUMMARY:" + summary + "\r\n" +
            "ORGANIZER:" + organizerUri + "\r\n" +
            "ATTENDEE;PARTSTAT=ACCEPTED:" + organizerUri + "\r\n" +
            "ATTENDEE;PARTSTAT=NEEDS-ACTION;SCHEDULE-AGENT=" + scheduleAgent + ":" + attendeeUri + "\r\n" +
            "END:VEVENT" + "\r\n" +
            "END:VCALENDAR" + "\r\n";
        // @formatter:on
        assertEquals(StatusCodes.SC_CREATED, putICal(encodeFolderID(defaultFolderId), uid, iCal), "response code wrong");
        /*
         * verify appointment on server as organizer
         */
        List<EventData> eventDatas = lookupEvents(getApiClient(), defaultFolderId, uid);
        assertTrue(null != eventDatas && 1 == eventDatas.size());
        Attendee attendee = extractAttendee(eventDatas.get(0), attendeeUri);
        assertTrue(null != attendee && "NEEDS-ACTION".equals(attendee.getPartStat()));
        assertNull(attendee.getAdditionalProperty("SCHEDULE-AGENT"));
        /*
         * verify appointment on client as organizer
         */
        ICalResource iCalResource = CalDAVTest.get(webDAVClient, encodeFolderID(defaultFolderId), uid, null, null);
        assertTrue(null != iCalResource && null != iCalResource.getVEvent());
        Property attendeeProperty = iCalResource.getVEvent().getAttendee(attendeeUri);
        assertNotNull(attendeeProperty);
        assertTrue(null != attendeeProperty && "NEEDS-ACTION".equals(attendeeProperty.getAttribute("PARTSTAT")));
        assertNull(attendeeProperty.getAttribute("SCHEDULE-AGENT"));
        /*
         * verify appointment on server as attendee
         */
        eventDatas = lookupEvents(testUser2.getApiClient(), defaultFolderId2, uid);
        assertTrue(null != eventDatas && 1 == eventDatas.size());
        attendee = extractAttendee(eventDatas.get(0), attendeeUri);
        assertTrue(null != attendee && "NEEDS-ACTION".equals(attendee.getPartStat()));
        assertNull(attendee.getAdditionalProperty("SCHEDULE-AGENT"));
        /*
         * verify appointment on client as attendee
         */
        iCalResource = CalDAVTest.get(webDAVClient2, encodeFolderID(defaultFolderId2), uid, null, null);
        assertTrue(null != iCalResource && null != iCalResource.getVEvent());
        attendeeProperty = iCalResource.getVEvent().getAttendee(attendeeUri);
        assertNotNull(attendeeProperty);
        assertTrue(null != attendeeProperty && "NEEDS-ACTION".equals(attendeeProperty.getAttribute("PARTSTAT")));
        assertNull(attendeeProperty.getAttribute("SCHEDULE-AGENT"));
        /*
         * verify that no email was received
         */
        MailData mailData = new IMipReceiver(testUser2.getApiClient()).subject(summary).from(organizerMail).receive(expectMail);
        if (expectMail) {
            assertNotNull(mailData);
        } else {
            assertNull(mailData);
        }
    }

    protected static List<EventData> lookupEvents(ApiClient apiClient, String folderId, String uid) throws ApiException {
        String body = """
            {  
                "folders" : [ "%s" ], 
                "filter" : [ "=" , { "field" : "uid" }, "%s"]
            }
            """.formatted(folderId, uid);
        List<MultipleEventData> results = new ChronosApi(apiClient).searchChronosAdvancedBuilder()
            .withRangeStart(formatAsUTC(CalendarUtils.add(new Date(), Calendar.YEAR, -1)))
            .withRangeEnd(formatAsUTC(CalendarUtils.add(new Date(), Calendar.YEAR, 1)))
            .withBody(body).execute().getData();
        List<EventData> foundEvents = new ArrayList<EventData>();
        for (MultipleEventData result : results) {
            foundEvents.addAll(result.getEvents());
        }
        return foundEvents;
    }

    protected static Attendee extractAttendee(EventData eventData, String uri) {
        if (null != eventData) {
            List<Attendee> attendees = eventData.getAttendees();
            if (null != attendees) {
                for (Attendee attendee : attendees) {
                    if (uri.equals(attendee.getUri())) {
                        return attendee;
                    }
                }
            }
        }
        return null;
    }

}
