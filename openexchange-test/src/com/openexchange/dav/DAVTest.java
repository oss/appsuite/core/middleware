/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.dav;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.client.methods.PropFindMethod;
import org.apache.jackrabbit.webdav.property.DavPropertyNameSet;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.ajax.framework.ClientCommons;
import com.openexchange.dav.caldav.UserAgents;
import com.openexchange.exception.OXException;
import com.openexchange.test.common.configuration.AJAXConfig;
import okhttp3.ConnectionPool;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * {@link DAVTest} an interface that defines auth methods for DAV tests
 *
 * @author <a href="mailto:philipp.schumacher@open-xchange.com">Philipp Schumacher</a>
 */
public interface DAVTest {

    /**
     * The basic auth method identifier
     */
    static final String AUTH_METHOD_BASIC = "Basic Auth";

    /**
     * The oauth method identifier
     */
    static final String AUTH_METHOD_OAUTH = "OAuth";

    /**
     * If auto discovery should be used to determine the auth methods
     */
    static final boolean AUTODISCOVER_AUTH = true;

    /**
     * Static method that determines different auth methods for a DAV test.
     *
     * Example:
     * <pre>
     *
     * public class ExampleTest implements DAVTest {
     *
     *     &commat;ParameterizedTest
     *     &commat;MethodSource("availableAuthMethods")
     *     public void exampleTest(@SuppressWarnings("unused") String authMethod) throws Exception {
     *       // test code
     *     }
     *
     * }
     * </pre>
     */
    static Iterable<Object[]> availableAuthMethods() {
        if (!AUTODISCOVER_AUTH) {
            List<Object[]> authMethods = new ArrayList<>(2);
            authMethods.add(new Object[] { AUTH_METHOD_OAUTH });
            authMethods.add(new Object[] { AUTH_METHOD_BASIC });

            return authMethods;
        }
        List<Object[]> authMethods = new ArrayList<Object[]>(2);
        try {
            AJAXConfig.init();
            DavPropertyNameSet props = new DavPropertyNameSet();
            props.add(PropertyNames.CURRENT_USER_PRINCIPAL);

            // @formatter:off
            OkHttpClient client = new OkHttpClient.Builder()
                .hostnameVerifier((hostname, session) -> true)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws IOException {
                        Request request = chain.request();
                        Request newRequest = request.newBuilder()
                                .addHeader(ClientCommons.X_OX_HTTP_TEST_HEADER_NAME, DAVTest.class.getName() + ".availableAuthMethods")
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .connectionPool(new ConnectionPool(10,5, TimeUnit.SECONDS))
                .build();
            // @formatter:on
            PropFindMethod req = new PropFindMethod(Config.getBaseUri() + Config.getPathPrefix() + "/caldav/", DavConstants.PROPFIND_BY_PROPERTY, props, DavConstants.DEPTH_0);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            req.getRequestEntity().writeRequest(bos);
            // @formatter:off
            @SuppressWarnings("deprecation")
            Request propFind = new Request.Builder()
                                          .url(req.getURI().toString())
                                          .method("PROPFIND", RequestBody.create(MediaType.parse("text/xml; charset=UTF-8"), bos.toByteArray()))
                                          .header("User-Agent", UserAgents.IOS_12_0)
                                          .build();
            // @formatter:on
            Response resp = client.newCall(propFind).execute();
            assertEquals(HttpServletResponse.SC_UNAUTHORIZED, resp.code(), "Unexpected response status");
            for (String header : resp.headers("WWW-Authenticate")) {
                if (header.startsWith("Bearer")) {
                    authMethods.add(new Object[] { AUTH_METHOD_OAUTH });
                } else if (header.startsWith("Basic")) {
                    authMethods.add(new Object[] { AUTH_METHOD_BASIC });
                }
            }
            if(!resp.isSuccessful()) {
                resp.body().close();
            }
            assertFalse(authMethods.isEmpty(), "No available authentication mode detected");
        } catch (OXException | IOException e) {
            fail(e.getMessage());
        }
        return authMethods;
    }

    /**
     * Derives whether a test execution is using {@link #AUTH_METHOD_OAUTH}, based on the display name found in the supplied
     * {@link TestInfo} reference, assuming that the default of
     * {@link org.junit.jupiter.params.ParameterizedTest.DEFAULT_DISPLAY_NAME} applies.
     *
     * @param testInfo The test info
     * @return <code>true</code> if OAuth is used, <code>false</code>, otherwise
     */
    public static boolean usesOAuth(TestInfo testInfo) {
        String displayName = testInfo.getDisplayName();
        return (null != displayName && displayName.contains(AUTH_METHOD_OAUTH));
    }

}
