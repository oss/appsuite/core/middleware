/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.dav.carddav.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import com.openexchange.ajax.framework.AJAXClient;
import com.openexchange.dav.DAVTest;
import com.openexchange.dav.StatusCodes;
import com.openexchange.dav.SyncToken;
import com.openexchange.dav.carddav.CardDAVTest;
import com.openexchange.dav.carddav.UserAgents;
import com.openexchange.groupware.container.Contact;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.server.impl.OCLPermission;
import com.openexchange.test.FolderTestManager;
import com.openexchange.test.common.test.pool.TestUser;

/**
 * {@link FallbackToDefaultFolderTest}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class FallbackToDefaultFolderTest extends CardDAVTest {

    /**
     * Initializes a new {@link FallbackToDefaultFolderTest}.
     */
    public FallbackToDefaultFolderTest() {
        super();
    }

    @Override
    protected String getDefaultUserAgent() {
        return UserAgents.IOS_18_1;
    }

    static Iterable<Object[]> testParameters() {
        List<Object[]> testParameters = new ArrayList<Object[]>();
        Iterable<Object[]> availableAuthMethods = DAVTest.availableAuthMethods();
        for (Object[] authMethods : availableAuthMethods) {
            for (String fallbackSetting : new String[] { "always", "disabled", "insufficient_permissions" }) {
                List<Object> params = new ArrayList<Object>();
                params.addAll(Arrays.asList(authMethods));
                params.add(fallbackSetting);
                testParameters.add(params.toArray(new Object[params.size()]));
            }
        }
        return testParameters;
    }

    @ParameterizedTest
    @MethodSource("testParameters")
    public void testInPersonalFolder(@SuppressWarnings("unused") String authMethod, String fallbackSetting) throws Exception {
        setIosFallbackToDefaultCollectionForNewResources(fallbackSetting);
        int targetFolderId = getDefaultFolderID();
        int expectedFolderId = targetFolderId;
        boolean expectSuccess = true;
        testCreateContact(targetFolderId, expectedFolderId, expectSuccess);
    }

    @ParameterizedTest
    @MethodSource("testParameters")
    public void testInOtherPersonalFolder(@SuppressWarnings("unused") String authMethod, String fallbackSetting) throws Exception {
        setIosFallbackToDefaultCollectionForNewResources(fallbackSetting);
        int targetFolderId = createFolder(randomUID()).getObjectID();
        int expectedFolderId = "always".equalsIgnoreCase(fallbackSetting) ? getDefaultFolderID() : targetFolderId;
        boolean expectSuccess = true;
        testCreateContact(targetFolderId, expectedFolderId, expectSuccess);
    }

    @ParameterizedTest
    @MethodSource("testParameters")
    public void testInGAB(@SuppressWarnings("unused") String authMethod, String fallbackSetting) throws Exception {
        setIosFallbackToDefaultCollectionForNewResources(fallbackSetting);
        int targetFolderId = FolderObject.SYSTEM_LDAP_FOLDER_ID;
        int expectedFolderId = getDefaultFolderID();
        boolean expectSuccess = false == "disabled".equals(fallbackSetting);
        testCreateContact(targetFolderId, expectedFolderId, expectSuccess);
    }

    @ParameterizedTest
    @MethodSource("testParameters")
    public void testInSharedReadOnly(@SuppressWarnings("unused") String authMethod, String fallbackSetting) throws Exception {
        setIosFallbackToDefaultCollectionForNewResources(fallbackSetting);
        FolderObject sharedFolder = createSharedFolder(testContext.acquireUser(), FolderTestManager.createPermission(testUser.getUserId(), false, OCLPermission.READ_FOLDER, OCLPermission.READ_ALL_OBJECTS, OCLPermission.NO_PERMISSIONS, OCLPermission.NO_PERMISSIONS, false));
        int targetFolderId = sharedFolder.getObjectID();
        int expectedFolderId = getDefaultFolderID();
        boolean expectSuccess = false == "disabled".equals(fallbackSetting);
        testCreateContact(targetFolderId, expectedFolderId, expectSuccess);
    }

    @ParameterizedTest
    @MethodSource("testParameters")
    public void testInSharedReadWrite(@SuppressWarnings("unused") String authMethod, String fallbackSetting) throws Exception {
        setIosFallbackToDefaultCollectionForNewResources(fallbackSetting);
        FolderObject sharedFolder = createSharedFolder(testContext.acquireUser(), FolderTestManager.createPermission(testUser.getUserId(), false, OCLPermission.CREATE_OBJECTS_IN_FOLDER, OCLPermission.READ_ALL_OBJECTS, OCLPermission.WRITE_ALL_OBJECTS, OCLPermission.DELETE_ALL_OBJECTS, false));
        int targetFolderId = sharedFolder.getObjectID();
        int expectedFolderId = "always".equalsIgnoreCase(fallbackSetting) ? getDefaultFolderID() : targetFolderId;
        boolean expectSuccess = true;
        testCreateContact(targetFolderId, expectedFolderId, expectSuccess);
    }

    private FolderObject createSharedFolder(TestUser sharer, OCLPermission shareePermission) throws Exception {
        AJAXClient ajaxClient2 = sharer.getAjaxClient();
        FolderTestManager ftm2 = new FolderTestManager(ajaxClient2);
        FolderObject defaultFolder2 = ftm2.getFolderFromServer(ajaxClient2.getValues().getPrivateContactFolder());
        FolderObject folder = new FolderObject();
        folder.setFolderName(randomUID());
        folder.setParentFolderID(defaultFolder2.getObjectID());
        folder.setModule(defaultFolder2.getModule());
        folder.setType(defaultFolder2.getType());
        List<OCLPermission> permissions = new ArrayList<OCLPermission>(defaultFolder2.getPermissions());
        permissions.add(shareePermission);
        folder.setPermissions(permissions);
        return ftm2.insertFolderOnServer(folder);
    }

    private void setIosFallbackToDefaultCollectionForNewResources(String value) throws Exception {
        Map<String, String> configuration = new HashMap<String, String>();
        configuration.put("com.openexchange.carddav.iosFallbackToDefaultCollectionForNewResources", value);
        changeConfigWithOwnClient(testUser, configuration);
    }

    private void testCreateContact(int targetFolderId, int expectedFolderId, boolean expectSuccess) throws Exception {
        testCreateContact(Integer.toString(targetFolderId), Integer.toString(expectedFolderId), expectSuccess);
    }

    private void testCreateContact(String targetFolderId, String expectedFolderId, boolean expectSuccess) throws Exception {
        SyncToken syncTokenExpectedFolder = new SyncToken(fetchSyncToken(expectedFolderId));
        SyncToken syncTokenTargetFolder = targetFolderId.equals(expectedFolderId) ? syncTokenExpectedFolder : new SyncToken(fetchSyncToken(targetFolderId));
        /*
         * prepare vCard
         */
        String uid = randomUID();
        String vCard = // @formatter:off
            "BEGIN:VCARD" + "\r\n" +
            "VERSION:3.0" + "\r\n" +
            "PRODID:-//Apple Inc.//AddressBook 6.1//EN" + "\r\n" +
            "REV:" + formatAsUTC(new Date()) + "\r\n" +
            "UID:" + uid + "\r\n" +
            "FN:Test GAB" + "\r\n" +
            "N:;Test GAB;;;" + "\r\n" +
            "END:VCARD" + "\r\n";
        // @formatter:on
        /*
         * create vCard resource in targeted folder on server
         */
        assertEquals(StatusCodes.SC_CREATED, putVCard(uid, vCard, targetFolderId), "response code wrong");
        /*
         * get & verify created contact on server
         */
        Contact contact = getContact(uid);
        if (false == expectSuccess) {
            assertNull(contact);
            return;
        }
        assertNotNull(contact);
        rememberForCleanUp(contact);
        assertEquals(uid, contact.getUid(), "uid wrong");
        /*
         * sync expected folder on client, expecting the contact is present there
         */
        Map<String, String> eTags = syncCollection(expectedFolderId, syncTokenExpectedFolder).getETagsStatusOK();
        assertTrue(0 < eTags.size(), "no resource changes reported on sync collection");
        assertContains(uid, addressbookMultiget(expectedFolderId, eTags.keySet()));
        /*
         * if different, sync targeted folder on client, expecting the contact to be removed there
         */
        if (false == targetFolderId.equals(expectedFolderId)) {
            List<String> hrefs = syncCollection(targetFolderId, syncTokenTargetFolder).getHrefsStatusNotFound();
            assertTrue(0 < hrefs.size(), "no resource changes reported on sync collection");
            boolean found = false;
            for (String href : hrefs) {
                if (null != href && href.contains(uid)) {
                    found = true;
                    break;
                }
            }
            assertTrue(found, "contact not reported as deleted in targeted folder");
        }
    }

}
