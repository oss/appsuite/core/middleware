/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.test;

import org.slf4j.Logger;
import com.openexchange.java.Strings;
import com.openexchange.test.common.configuration.AJAXConfig;

/**
 *
 * {@link Host} Enumeration for different host types
 *
 * @author <a href="mailto:philipp.schumacher@open-xchange.com">Philipp Schumacher</a>
 */
public enum Host {

    /**
     * Default host
     */
    SERVER,
    /**
     * Host that ensures the traffic always routed to the same pod
     */
    SINGLENODE,
    /**
     * Host that is configured for authentication via OpenID Connect
     */
    OIDC,
    ;

    /*
     * ============================== Tooling ==============================
     */

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(Host.class);
    }

    /**
     * Get the host name fitting to the given host
     * <p>
     * If no host name is set per configuration, <code>localhost</code> is used
     *
     * @param host The host
     * @return The host name
     */
    public String getHostname() {
        String name;
        switch (this) {
            case OIDC:
                name = AJAXConfig.getProperty(AJAXConfig.Property.OIDC_HOSTNAME);
                if (Strings.isNotEmpty(name)) {
                    break;
                }
                LoggerHolder.LOG.warn("No hostname for OIDC_HOSTNAME was configured! Using SERVER_HOSTNAME as fallback");
                name = AJAXConfig.getProperty(AJAXConfig.Property.SERVER_HOSTNAME);
                break;
            case SINGLENODE:
                name = AJAXConfig.getProperty(AJAXConfig.Property.SINGLENODE_HOSTNAME);
                if (Strings.isNotEmpty(name)) {
                    break;
                }
                LoggerHolder.LOG.warn("No hostname for SINGLENODE_HOSTNAME was configured! Using SERVER_HOSTNAME as fallback");
                // $FALL-THROUGH$ and use common host name
            case SERVER:
            default:
                name = AJAXConfig.getProperty(AJAXConfig.Property.SERVER_HOSTNAME);
        }
        if (Strings.isEmpty(name)) {
            LoggerHolder.LOG.warn("No hostname was configured! Using localhost as fallback");
            return "localhost";
        }
        return name;
    }

    /**
     * Get the host including the path
     *
     * @return The base path, e.g. <code>https://localhost/api</code>
     */
    public String getPath() {
        String protocol = AJAXConfig.getProperty(AJAXConfig.Property.PROTOCOL);
        if (protocol == null) {
            protocol = "http";
        }
        return protocol + "://" + getHostname() + getBasePath();
    }

    /**
     * Get the base path.
     *
     * @return The base path, e.g. <code>/api</code>
     */
    public static String getBasePath() {
        String basePath = AJAXConfig.getProperty(AJAXConfig.Property.BASE_PATH);
        if (basePath.endsWith("/")) {
            basePath = Strings.trimEnd(basePath, '/');
        }
        return basePath;
    }

    /**
     * Gets a valued indicating whether the given path contains the
     * {@link #getHostname()} of this host
     *
     * @param path The path to check
     * @return <code>true</code> if the path contains this host, <code>false</code> otherwise
     */
    public boolean isSameHost(String path) {
        if (Strings.isEmpty(path)) {
            return false;
        }
        return path.contains(getHostname());
    }
}
