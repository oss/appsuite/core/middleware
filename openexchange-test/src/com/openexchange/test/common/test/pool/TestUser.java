/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.test.common.test.pool;

import static com.openexchange.java.Autoboxing.I;
import static org.assertj.core.api.Assertions.fail;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.ajax.framework.AJAXClient;
import com.openexchange.ajax.framework.SessionAwareClient;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.test.Host;
import com.openexchange.test.common.test.TestUserConfig;
import com.openexchange.test.common.tools.LoginTools;
import com.openexchange.testing.httpclient.invoker.ApiClient;
import com.openexchange.testing.httpclient.invoker.ApiException;

/**
 * {@link TestUser}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v7.8.3
 */
public class TestUser implements Serializable {

    /** Wrapper object that delays initialization of logger class until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(TestUser.LoggerHolder.class);
    }

    private static final long serialVersionUID = 7616495104780779831L;

    private final String login;

    private final String password;

    private final String user;

    private final String context;

    private final String createdBy;

    private final int userId;

    private final int ctxId;

    private final transient List<AJAXClient> ajaxClients = Collections.synchronizedList(new LinkedList<>());

    private final transient List<SessionAwareClient> apiClients = Collections.synchronizedList(new LinkedList<>());

    private final transient Optional<TestUserConfig> config;

    /**
     * Initializes a new {@link TestUser} using <code>-1</code> for context
     * and user identifier.
     *
     * @param user The user name
     * @param context The context of the user
     * @param password The password of the user
     * @param createdBy The creator of the user
     */
    public TestUser(String user, String context, String password, String createdBy) {
        this(user, context, password, I(-1), I(-1), createdBy);
    }

    /**
     * Initializes a new {@link TestUser}.
     *
     * @param user The user name
     * @param context The context of the user
     * @param password The password of the user
     * @param userId The user identifier
     * @param ctxId The context identifier
     * @param createdBy The creator of the user
     */
    public TestUser(String user, String context, String password, Integer userId, Integer ctxId, String createdBy) {
        this(user, context, password, userId, ctxId, Optional.empty(), createdBy);
    }

    /**
     * Initializes a new {@link TestUser}.
     *
     * @param user The user name
     * @param context The context of the user
     * @param password The password of the user
     * @param userId The user identifier
     * @param ctxId The context identifier
     * @param config The configuration for the user
     * @param createdBy The creator of the user
     */
    public TestUser(String user, String context, String password, Integer userId, Integer ctxId, Optional<TestUserConfig> config, String createdBy) {
        this.user = user;
        this.context = context;
        this.login = Strings.isNotEmpty(context) ? user + "@" + context : user;
        this.password = password;
        this.userId = null == userId ? -1 : userId.intValue();
        this.ctxId = null == ctxId ? -1 : ctxId.intValue();
        this.config = config;
        this.createdBy = null == createdBy ? "" : createdBy;
    }

    /*
     * ============================== static data ==============================
     */

    /**
     * Get the login name
     * E.g. <code>anton@context1.ox.test</code>
     *
     * @return The login name
     */
    public String getLogin() {
        return login;
    }

    /**
     * gets the password for the user
     *
     * @return The user password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Get the user name
     * E.g. <code>anton</code>
     *
     * @return The user name
     */
    public String getUser() {
        return user;
    }

    /**
     * Get the context identifier the user belongs to
     *
     * @return The users context
     */
    public String getContext() {
        return context;
    }

    /**
     * Gets the userId
     *
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Gets the context id
     *
     * @return The context id
     */
    public int getContextId() {
        return ctxId;
    }

    /**
     * Gets the creator
     *
     * @return The creator
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /*
     * ============================== AJAX client ==============================
     */

    /**
     * Gets or creates the standard {@link AJAXClient} for this user
     *
     * @return The {@link AJAXClient}
     * @throws OXException If creation fails
     * @throws IOException If creation fails
     * @throws JSONException If creation fails
     */
    public AJAXClient getAjaxClient() throws OXException, IOException, JSONException {
        return ajaxClients.stream().findFirst().orElse(generateAjaxClient());
    }

    /**
     * Generates a new {@link AJAXClient} in addition to the standard
     * {@link AJAXClient}. If you want to get "just a client" use
     * {@link #getAjaxClient()}
     *
     * @return A new created {@link AJAXClient}
     * @throws OXException If creation fails
     * @throws IOException If creation fails
     * @throws JSONException If creation fails
     */
    public AJAXClient generateAjaxClient() throws OXException, IOException, JSONException {
        AJAXClient ajaxClient = new AJAXClient(this);
        ajaxClients.add(ajaxClient);
        return ajaxClient;
    }

    /**
     * Closes all AJAX clients
     */
    public void closeAjaxClients() {
        for (AJAXClient c : ajaxClients) {
            try {
                c.getSession().getHttpClient().close();
            } catch (IOException e) {
                LoggerHolder.LOGGER.info("Unable to close resource", e);
            }
        }
    }

    /*
     * ============================== API client ==============================
     */

    /**
     * Performs the login for all API clients
     * <p>
     * Note: AJAX clients are not logged in by this method
     *
     * @throws ApiException In case of error
     */
    public void performLogin() throws ApiException {
        if (apiClients.isEmpty()) {
            fail("No client to login");
            return;
        }
        for (SessionAwareClient client : apiClients) {
            LoginTools.performLogin(client, Optional.empty(), null);
        }
    }

    /**
     * Performs the login for all API clients
     * <p>
     * Note: AJAX clients are not logged in by this method
     * 
     * @param clientId The client identifier to use for requests
     * @throws ApiException In case of error
     */
    public void performLogin(String clientId) throws ApiException {
        if (apiClients.isEmpty()) {
            fail("No client to login");
            return;
        }
        for (SessionAwareClient client : apiClients) {
            LoginTools.performLogin(client, Optional.empty(), clientId);
        }
    }

    /**
     * Performs the logout for all API clients
     * <p>
     * Note: AJAX clients are not logged out by this method
     *
     * @throws ApiException In case of error
     */
    public void performLogout() throws ApiException {
        if (apiClients.isEmpty()) {
            return;
        }
        ApiException e = null;
        for (SessionAwareClient client : apiClients) {
            try {
                LoginTools.performLogout(client);
            } catch (ApiException ex) {
                e = ex;
            }
        }
        if (null != e) {
            throw e;
        }
    }

    /**
     * Gets or creates the standard {@link ApiClient} for this user
     *
     * @return The {@link SessionAwareClient}
     * @throws ApiException If creating fails
     */
    public SessionAwareClient getApiClient() throws ApiException {
        return getApiClient(Host.SERVER);
    }

    /**
     * Gets or creates the standard {@link ApiClient} for this user
     *
     * @param host The {@link Host} the test shall send requests to
     * @return The {@link SessionAwareClient}
     * @throws ApiException If creating fails
     */
    public SessionAwareClient getApiClient(Host host) throws ApiException {
        for (SessionAwareClient client : apiClients) {
            if (host.isSameHost(client.getBasePath())) {
                return client;
            }
        }
        return configureApiClient(config, host, null);
    }

    /**
     * Generates a new {@link ApiClient} in addition to the standard
     * {@link ApiClient}. If you want to get "just a client" use
     * {@link #getApiClient()}
     *
     * @return A new created {@link ApiClient}
     * @throws ApiException If generation fails
     */
    public SessionAwareClient generateApiClient() throws ApiException {
        return configureApiClient(config, Host.SERVER, null);
    }

    /**
     * Generates a new {@link ApiClient} in addition to the standard
     * {@link ApiClient}. If you want to get "just a client" use
     * {@link #getApiClient()}
     *
     * @param clientId The client identifier
     * @return A new created {@link ApiClient}
     * @throws ApiException If generation fails
     */
    public SessionAwareClient generateApiClient(String clientId) throws ApiException {
        return configureApiClient(config, Host.SERVER, clientId);
    }

    private SessionAwareClient configureApiClient(Optional<TestUserConfig> config, Host server, String clientId) throws ApiException {
        SessionAwareClient apiClient = LoginTools.createConnectedApiClient(this, config, Optional.empty(), server, clientId);
        apiClients.add(apiClient);
        return apiClient;
    }

    /*
     * ============================== Class methods ==============================
     */

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(64);
        builder.append("TestUser [");
        if (Strings.isNotEmpty(login)) {
            builder.append("login=").append(login);
        }
        builder.append(']');
        return builder.toString();
    }

}
