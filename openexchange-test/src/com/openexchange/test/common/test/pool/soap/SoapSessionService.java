/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.test.common.test.pool.soap;

import static com.openexchange.java.Autoboxing.I;
import java.net.MalformedURLException;
import java.net.URL;
import com.openexchange.ajax.framework.ProvisioningSetup;
import com.openexchange.exception.OXException;
import com.openexchange.sessiond.soap.dataobjects.Credentials;
import com.openexchange.sessiond.soap.soap.ClearUserSession;
import com.openexchange.sessiond.soap.soap.ClearUserSessions;
import com.openexchange.sessiond.soap.soap.InvalidCredentialsException_Exception;
import com.openexchange.sessiond.soap.soap.NoSuchUserException_Exception;
import com.openexchange.sessiond.soap.soap.OXSessionService;
import com.openexchange.sessiond.soap.soap.OXSessionServicePortType;
import com.openexchange.sessiond.soap.soap.RemoteException_Exception;
import com.openexchange.test.common.test.pool.TestUser;


/**
 * {@link SoapSessionService}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class SoapSessionService implements SoapService {

    private static SoapSessionService INSTANCE;

    private final OXSessionService oxSessionService;
    private final OXSessionServicePortType oxSessionServicePortType;
    private final Credentials soapMasterCreds;
    private final URL wsdlLocation;

    /**
     * Gets the {@link SoapSessionService}
     *
     * @return The {@link SoapSessionService}
     * @throws MalformedURLException In case service can't be initialized
     */
    public static SoapSessionService getInstance() throws MalformedURLException {
        if (INSTANCE == null) {
            synchronized (SoapSessionService.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SoapSessionService();
                }
            }
        }
        return INSTANCE;
    }

    private SoapSessionService() throws MalformedURLException {
        this.wsdlLocation = new URL(SoapProvisioningService.getSOAPHostUrl(), "/webservices/OXSessionService?wsdl");
        this.oxSessionService = new OXSessionService(getWsdlLocation());
        this.oxSessionServicePortType = oxSessionService.getOXSessionServiceHttpsEndpoint();
        TestUser oxAdminMaster = ProvisioningSetup.getOxAdminMaster();
        Credentials credentials = new Credentials();
        credentials.setLogin(oxAdminMaster.getUser());
        credentials.setPassword(oxAdminMaster.getPassword());
        this.soapMasterCreds = credentials;
    }

    @Override
    public Object getPortType() {
        return oxSessionServicePortType;
    }

    @Override
    public URL getWsdlLocation() {
        return wsdlLocation;
    }

    public void clearUserSessions(int contextId, int userId) throws OXException {
        ClearUserSessions parameters = new ClearUserSessions();
        parameters.setAuth(soapMasterCreds);
        parameters.setContextId(I(contextId));
        parameters.setUserId(I(userId));
        try {
            oxSessionServicePortType.clearUserSessions(parameters);
        } catch (InvalidCredentialsException_Exception | RemoteException_Exception | NoSuchUserException_Exception e) {
            throw OXException.general("Error clearing user sessions", e);
        }
    }

    public void clearUserSession(String sessionId) throws OXException {
        ClearUserSession parameters = new ClearUserSession();
        parameters.setAuth(soapMasterCreds);
        parameters.setSessionId(sessionId);
        try {
            oxSessionServicePortType.clearUserSession(parameters);
        } catch (InvalidCredentialsException_Exception | RemoteException_Exception | NoSuchUserException_Exception e) {
            throw OXException.general("Error clearing user session", e);
        }
    }

}
