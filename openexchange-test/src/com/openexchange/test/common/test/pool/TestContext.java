/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.test.common.test.pool;

import static com.openexchange.java.Autoboxing.I;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import org.junit.jupiter.api.Assertions;
import com.openexchange.ajax.framework.ConfigurableResource;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.test.common.test.TestClassConfig;
import com.openexchange.test.common.test.TestUserConfig;

/**
 * {@link TestContext}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @author <a href="mailto:sebastian.lutz@open-xchange.com">Sebastian Lutz</a>
 * @since v7.8.3
 */
public class TestContext implements Serializable, ConfigurableResource {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8836508664321761890L;

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(TestContext.class);

    private final int contextId;
    private final String name;
    private final String usedByTest;

    private final TestUser contextAdmin;

    /** Overall test users by this context */
    private final LinkedList<TestUser> users = new LinkedList<>();

    /** Pre-provisioned test users */
    private final LinkedList<TestUser> userPool = new LinkedList<>();

    /**
     * Initializes a new {@link TestContext}.
     *
     * @param contextId The context identifier
     * @param name The name
     * @param admin The admin of the context
     * @param usedBy The test class using this the context
     * @param users already existing users for this context
     * @param httpClient The HTTP client for the context
     */
    public TestContext(int contextId, String name, TestUser admin, String usedByTest, List<TestUser> users) {
        this.name = name;
        this.contextId = contextId;
        this.contextAdmin = (admin);
        this.usedByTest = usedByTest;
        addUsers(users);
    }

    public TestContext(int contextId, String name, TestUser admin, String usedByTest) {
        this(contextId, name, admin, usedByTest, null);
    }

    /*
     * ============================== static data ==============================
     */

    /**
     * Gets the context id.
     *
     * @return The context id.
     */
    public int getId() {
        return contextId;
    }

    /**
     * Get the context admin
     *
     * @return The admin as {@link TestUser}
     */
    public TestUser getAdmin() {
        return contextAdmin;
    }

    /**
     * Get the context name
     *
     * @return The context name
     */
    public String getName() {
        return name;
    }

    /**
     * Get the test that uses this {@link TestContext}.
     *
     * @return The user of this test context
     */
    public String getUsedBy() {
        return usedByTest;
    }

    /*
     * ============================== inherited methods ==============================
     */

    @Override
    public void configure(TestClassConfig testConfig) throws Exception {
        if (testConfig.optUserConfig().isPresent() || testConfig.getNumberOfUsersPerContext() > ProvisioningUtils.USER_NAMES_POOL.length) {
            for (int i = 0; i < testConfig.getNumberOfUsersPerContext(); i++) {
                TestUser createUser = createUser(testConfig.optUserConfig(), usedByTest);
                if (null != createUser) {
                    LOG.info("Created user {} in context {} for {}", createUser, I(contextId), usedByTest);
                    userPool.addFirst(createUser);
                    users.addFirst(createUser);
                }
            }
        }
    }

    /*
     * ============================== Test user ==============================
     */

    /**
     * Get all created users in this context
     *
     * @return All users
     */
    public List<TestUser> getUsers() {
        return users;
    }

    public void addUsers(List<TestUser> users) {
        if (users != null && !users.isEmpty()) {
            this.users.addAll(users);
            this.userPool.addAll(users);
        }
    }

    /**
     * Acquire a unique user from this context
     *
     * @return A unique user
     */
    public TestUser acquireUser() {
        // Get pre-provisioned user
        TestUser user = userPool.poll();
        if (null != user) {
            return user;
        }
        // Create new user
        TestUser createUser = createUser(Optional.empty(), usedByTest);
        users.add(createUser);
        return createUser;
    }

    /**
     * Creates an additional user
     *
     * @param username The user name
     * @param userConfig The user configuration of the user
     * @return The created user
     */
    public TestUser createUser(String username, Optional<TestUserConfig> userConfig) {
        // Create new user
        TestUser createUser = createUser(username, userConfig, usedByTest);
        users.add(createUser);
        return createUser;
    }

    private TestUser createUser(Optional<TestUserConfig> userConfig, String acquiredBy) {
        try {
            return ConfigAwareProvisioningService.getService().createUser(contextId, getUserNameFromPool(), userConfig, acquiredBy);
        } catch (OXException e) {
            LOG.error("Unable to pre provision test user", e);
        }
        return null;
    }

    private TestUser createUser(String username, Optional<TestUserConfig> userConfig, String acquiredBy) {
        try {
            return ConfigAwareProvisioningService.getService().createUser(contextId, username, userConfig, acquiredBy);
        } catch (OXException e) {
            LOG.error("Unable to pre provision test user", e);
        }
        return null;
    }

    /**
     *
     * Gets the next unused user name from the user name pool.
     *
     * @return The user name. Returns null, if the user name pool can not be read of if there are more users than names in the pool.
     */
    private String getUserNameFromPool() {
        int usedUserSize = users.size();
        String[] pool = ProvisioningUtils.USER_NAMES_POOL;
        if (pool == null) {
            return null;
        }
        int poolSize = pool.length;
        if (usedUserSize < poolSize) {
            try {
                return pool[users.size()];
            } catch (@SuppressWarnings("unused") Exception e) {
                return null;
            }
        }
        return null;
    }

    private final Random rand = new Random(System.currentTimeMillis());

    /**
     * Gets a user from this context. Can be a user that has already
     * been acquired by {@link #acquireUser()}
     *
     * @return A user
     */
    public TestUser getRandomUser() {
        if (userPool.isEmpty()) {
            // Create a new one
            return acquireUser();
        }
        if (1 == userPool.size()) {
            TestUser result = userPool.poll();
            if (result != null) {
                return result;
            }
        }

        int next = rand.nextInt(userPool.size());
        return userPool.remove(next);
    }

    /**
     * Acquire a resource from this context
     *
     * @return The resource identifier
     */
    public Integer acquireResource() {
        try {
            return ConfigAwareProvisioningService.getService().createResource(contextId, usedByTest);
        } catch (OXException e) {
            LOG.error("Unable to acquire resource", e);
            Assertions.fail();
        }
        return null;
    }

    /**
     * Acquire a group from this context
     *
     * @param optUsers The optional list of group members
     * @return The group identifier
     */
    public Integer acquireGroup(Optional<List<Integer>> optUsers) {
        try {
            return ConfigAwareProvisioningService.getService().createGroup(contextId, optUsers, usedByTest);
        } catch (OXException e) {
            LOG.error("Unable to acquire group", e);
            Assertions.fail();
        }
        return null;
    }

    /*
     * ============================== Class methods ==============================
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TestContext other = (TestContext) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equalsIgnoreCase(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(64);
        builder.append("TestContext [");
        if (Strings.isNotEmpty(name)) {
            builder.append("name=").append(name).append(", ");
        }
        builder.append("contextId=").append(contextId);
        builder.append(']');
        return builder.toString();
    }

}
