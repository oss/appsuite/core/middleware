/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.test.common.test.pool.soap.reseller;

import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.lang3.RandomStringUtils;
import com.openexchange.admin.soap.reseller.context.reseller.soap.OXResellerContextService;
import com.openexchange.admin.soap.reseller.context.reseller.soap.OXResellerContextServicePortType;
import com.openexchange.admin.soap.reseller.context.rmi.dataobjects.Credentials;
import com.openexchange.ajax.framework.ProvisioningSetup;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.test.common.test.pool.soap.SoapProvisioningService;
import com.openexchange.test.common.test.pool.soap.SoapService;

/**
 * {@link SoapResellerContextService}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class SoapResellerContextService implements SoapService {

    private static SoapResellerContextService INSTANCE;

    private final OXResellerContextService oxResellerContextService;
    private final OXResellerContextServicePortType oxResellerContextServicePortType;

    private final Credentials soapMasterCreds;

    private final URL wsdlLocation;

    /**
     * Gets the {@link SoapResellerContextService}. Use this method for the happy flow.
     *
     * @return The {@link SoapResellerContextService}
     * @throws MalformedURLException In case service can't be initialized
     */
    public static SoapResellerContextService getInstance() throws MalformedURLException {
        if (INSTANCE == null) {
            synchronized (SoapResellerContextService.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SoapResellerContextService();
                }
            }
        }
        return INSTANCE;
    }

    private SoapResellerContextService() throws MalformedURLException {
        this.wsdlLocation = new URL(SoapProvisioningService.getSOAPHostUrl(), "/webservices/OXResellerContextService?wsdl");
        this.oxResellerContextService = new OXResellerContextService(wsdlLocation);
        this.oxResellerContextServicePortType = oxResellerContextService.getOXResellerContextServiceHttpSoap11Endpoint();
        TestUser oxAdminMaster = ProvisioningSetup.getOxAdminMaster();
        this.soapMasterCreds = createCreds(oxAdminMaster.getLogin(), oxAdminMaster.getPassword());
    }

    @Override
    public Object getPortType() {
        return oxResellerContextServicePortType;
    }

    @Override
    public URL getWsdlLocation() {
        return wsdlLocation;
    }

    /**
     * Creates new credentials based on the given paramters
     *
     * @param login The login name to be used
     * @param password The password to be used
     * @return The credential obj
     */
    private Credentials createCreds(String login, String password) {
        Credentials creds = new Credentials();
        creds.setLogin(login);
        creds.setPassword(password);
        return creds;
    }

    public Credentials getSoapMasterCreds() {
        return soapMasterCreds;
    }
    
    public Credentials getWrongCreds() {
        Credentials credentials = new Credentials();
        credentials.setLogin(RandomStringUtils.randomAlphabetic(6));
        credentials.setPassword(RandomStringUtils.randomAlphabetic(6));
        return credentials;
    }
}
