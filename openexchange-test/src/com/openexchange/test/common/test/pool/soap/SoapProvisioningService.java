/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.test.common.test.pool.soap;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import static java.util.Optional.empty;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.admin.soap.context.dataobjects.Context;
import com.openexchange.admin.soap.context.dataobjects.Entry;
import com.openexchange.admin.soap.context.dataobjects.SOAPMapEntry;
import com.openexchange.admin.soap.context.dataobjects.SOAPStringMap;
import com.openexchange.admin.soap.context.dataobjects.SOAPStringMapMap;
import com.openexchange.ajax.framework.ClientCommons;
import com.openexchange.exception.OXException;
import com.openexchange.java.Consumers.OXConsumer;
import com.openexchange.java.Functions.OXFunction;
import com.openexchange.java.Strings;
import com.openexchange.test.Host;
import com.openexchange.test.common.configuration.AJAXConfig;
import com.openexchange.test.common.test.TestContextConfig;
import com.openexchange.test.common.test.TestUserConfig;
import com.openexchange.test.common.test.pool.ProvisioningService;
import com.openexchange.test.common.test.pool.ProvisioningUtils;
import com.openexchange.test.common.test.pool.TestContext;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.test.common.test.pool.UserModuleAccess;

/**
 * {@link SoapProvisioningService}
 *
 * @author <a href="mailto:sebastian.lutz@open-xchange.com">Sebastian Lutz</a>
 * @since v8.0.0
 */
public class SoapProvisioningService implements ProvisioningService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SoapProvisioningService.class);

    private static final SoapProvisioningService INSTANCE = new SoapProvisioningService();

    /**
     * Gets the {@link SoapProvisioningService}
     *
     * @return The {@link SoapProvisioningService}
     */
    public static SoapProvisioningService getInstance() {
        return INSTANCE;
    }

    /**
     * Initializes a new {@link SoapProvisioningService}.
     */
    private SoapProvisioningService() {
        super();
    }

    private SoapContextService getContextService() throws OXException {
        try {
            return SoapContextService.getInstance();
        } catch (MalformedURLException e) {
            throw new OXException(e);
        }
    }

    private SoapUserService getUserService() throws OXException {
        try {
            return SoapUserService.getInstance();
        } catch (MalformedURLException e) {
            throw new OXException(e);
        }
    }

    private SoapGroupService getGroupService() throws OXException {
        try {
            return SoapGroupService.getInstance();
        } catch (MalformedURLException e) {
            throw new OXException(e);
        }
    }

    private SoapResourceService getResourceService() throws OXException {
        try {
            return SoapResourceService.getInstance();
        } catch (MalformedURLException e) {
            throw new OXException(e);
        }
    }

    private SoapSessionService getSessionService() throws OXException {
        try {
            return SoapSessionService.getInstance();
        } catch (MalformedURLException e) {
            throw new OXException(e);
        }
    }

    @Override
    public TestContext createContext(String createdBy) throws OXException {
        return createContext(TestContextConfig.EMPTY_CONFIG, createdBy);
    }

    @Override
    public TestContext createContext(TestContextConfig config, String createdBy) throws OXException {
        return execute(getContextService(), s -> s.createTestContext(config, createdBy), createdBy);
    }

    @Override
    public void changeContexConfig(int cid, Map<String, String> configs, String changedBy) throws OXException {
        Context ctx = new Context();
        ctx.setId(I(cid));

        SOAPStringMapMap soapStringMapMap = new SOAPStringMapMap();
        SOAPMapEntry soapMapEntry = new SOAPMapEntry();
        SOAPStringMap soapStringMap = new SOAPStringMap();
        ArrayList<Entry> entries = new ArrayList<>();

        configs.forEach((key, value) -> {
            Entry entry = new Entry();
            entry.setKey(key);
            entry.setValue(value);
            entries.add(entry);
        });

        soapStringMap.setEntries(entries);
        soapMapEntry.setValue(soapStringMap);
        soapMapEntry.setKey("config");

        soapStringMapMap.setEntries(Collections.singletonList(soapMapEntry));

        ctx.setUserAttributes(soapStringMapMap);

        perform(getContextService(), s -> s.change(ctx), changedBy);
    }

    @Override
    public void deleteContext(int cid, String deletedBy) throws OXException {
        perform(getContextService(), s -> s.delete(cid), deletedBy);
    }

    @Override
    public TestUser createUser(int cid, String createdBy) throws OXException {
        return createUser(cid, null, empty(), createdBy);
    }

    @Override
    public TestUser createUser(int cid, String userLogin, Optional<TestUserConfig> userConfig, String createdBy) throws OXException {
        return execute(getUserService(), s -> s.createTestUser(cid, userConfig, userLogin, createdBy), createdBy);
    }

    @Override
    public void changeUser(int cid, int userID, Optional<Map<String, String>> config, String changedBy) throws OXException {
        perform(getUserService(), s -> s.changeUser(cid, userID, config, changedBy), changedBy);
    }

    @Override
    public void deleteUser(int cid, int userID, String changedBy) throws OXException {
        perform(getUserService(), s -> s.delete(cid, userID), changedBy);
    }

    @Override
    public UserModuleAccess getModuleAccess(int contextId, int userId, String queriedBy) throws OXException {
        return query(getUserService(), s -> s.getModuleAccess(contextId, userId), queriedBy);
    }

    @Override
    public void changeCapability(int cid, int userID, Set<String> capsToAdd, Set<String> capsToRemove, Set<String> capsToDrop, String changedBy) throws OXException {
        perform(getUserService(), s -> s.changeCapabilities(cid, userID, capsToAdd, capsToRemove, capsToDrop), changedBy);
    }

    @Override
    public void changeModuleAccess(int cid, int userID, UserModuleAccess userModuleAccess, String changedBy) throws OXException {
        perform(getUserService(), s -> s.changeModuleAccess(cid, userID, userModuleAccess), changedBy);
    }

    @Override
    public Integer createGroup(int cid, Optional<List<Integer>> optUserIds, String createdBy) throws OXException {
        return execute(getGroupService(), s -> s.create(cid, optUserIds).getId(), createdBy);
    }

    @Override
    public Integer createResource(int cid, String createdBy) throws OXException {
        return execute(getResourceService(), s -> s.create(cid).getId(), createdBy);
    }

    @Override
    public TestUser getResource(int cid, int resrourceId, String queriedBy) throws OXException {
        return query(getResourceService(), s -> s.get(cid, resrourceId), queriedBy);
    }
    
    @Override
    public void clearUserSessions(int contextId, int userId, String xOxTestHeader) throws OXException {
        perform(getSessionService(), s -> s.clearUserSessions(contextId, userId), xOxTestHeader);
    }

    @Override
    public void clearUserSession(String sessionId, String xOxTestHeader) throws OXException {
        perform(getSessionService(), s -> s.clearUserSession(sessionId), xOxTestHeader);
    }

    /**
     * Gets the soap url
     *
     * @return The soap url
     * @throws MalformedURLException
     */
    public static URL getSOAPHostUrl() throws MalformedURLException {
        String soapProtocol = AJAXConfig.getProperty(AJAXConfig.Property.PROTOCOL);
        String soapHost = Host.SERVER.getHostname();

        String scheme = AJAXConfig.getProperty(AJAXConfig.Property.PROTOCOL);
        int soapPort = scheme.equals("https") ? 443 : Integer.parseInt(AJAXConfig.getProperty(AJAXConfig.Property.SERVER_PORT));
        return new URL(soapProtocol, soapHost, soapPort, "/");
    }

    @Override
    public void changeContext(int cid, TestContextConfig config, String changedBy) throws OXException {
        Optional<Map<String, String>> optConfig = config.optConfig();
        Optional<String> optTaxonomyType = config.optTaxonomyType();
        Long maxQuota = config.optMaxQuota().orElse(ProvisioningUtils.DEFAULT_MAX_QUOTA);
        if (optConfig.isPresent() || optTaxonomyType.isPresent()) {
            perform(getContextService(), s -> {
                Context ctx = s.createContext(maxQuota, optConfig, optTaxonomyType);
                ctx.setId(I(cid));
                s.change(ctx);
            }, changedBy);
        }
        Optional<UserModuleAccess> optAccess = config.optAccess();
        if (optAccess.isPresent()) {
            perform(getContextService(), s -> s.changeModuleAccess(cid, optAccess.get()), changedBy);
        }
    }

    /**
     * Query data from the given service
     * <p>
     * Call is executed <b>thread local</b> if needed, to exclusively add the calledBy parameters as header to the call
     * 
     * @param <T> The return type
     * @param <S> The service type
     * @param service The actual service
     * @param fun The query to call on the service
     * @param calledBy A string defining which method called the service, used for routing
     * @return The value of the function
     * @throws OXException In case of error
     */
    private <T, S extends SoapService> T query(S service, OXFunction<S, T, OXException> fun, String calledBy) throws OXException {
        Object oldValue = prepareCall(service, calledBy);
        long current = System.nanoTime();
        try {
            return fun.apply(service);
        } finally {
            LOGGER.debug("Needed {} nanoseconds to finish the query", L(System.nanoTime() - current));
            cleanUp(service, oldValue);
        }
    }

    /**
     * Performs a method on the given service
     * <p>
     * Call is executed <b>thread local</b> if needed, to exclusively add the calledBy parameters as header to the call
     *
     * @param <S> The type of the service
     * @param service The actual service
     * @param fun The method to call on the service
     * @param calledBy A string defining which method called the service, used for routing
     * @throws OXException In case of error
     */
    private <S extends SoapService> void perform(S service, OXConsumer<S, OXException> fun, String calledBy) throws OXException {
        Object oldValue = prepareCall(service, calledBy);
        long current = System.nanoTime();
        try {
            fun.accept(service);
        } finally {
            LOGGER.debug("Needed {} nanoseconds to execute the request", L(System.nanoTime() - current));
            cleanUp(service, oldValue);
        }
    }

    /**
     * Executes a method on the given service and returns the functions result
     * <p>
     * Call is executed <b>thread local</b> if needed, to exclusively add the calledBy parameters as header to the call
     *
     * @param <T> The return type
     * @param <S> The type of the service
     * @param service The actual service
     * @param fun The method to call on the service
     * @param calledBy A string defining which method called the service, used for routing
     * @return the result of the function
     * @throws OXException In case of error
     */
    private <T, S extends SoapService> T execute(S service, OXFunction<S, T, OXException> fun, String calledBy) throws OXException {
        Object oldValue = prepareCall(service, calledBy);
        long current = System.nanoTime();
        try {
            return fun.apply(service);
        } finally {
            LOGGER.debug("Needed {} nanoseconds to execute the request and get the response", L(System.nanoTime() - current));
            cleanUp(service, oldValue);
        }
    }

    private <S extends SoapService> Object prepareCall(S service, String calledBy) {
        if (Strings.isEmpty(calledBy)) {
            return null;
        }
        org.apache.cxf.endpoint.Client client = ClientProxy.getClient(service.getPortType());
        client.setThreadLocalRequestContext(true);
        Object oldValue = client.getRequestContext().get(Message.PROTOCOL_HEADERS);
        client.getRequestContext().put(Message.PROTOCOL_HEADERS, Map.of(ClientCommons.X_OX_HTTP_TEST_HEADER_NAME, List.of(calledBy)));
        return oldValue;
    }

    private <S extends SoapService> void cleanUp(S service, Object oldValue) {
        org.apache.cxf.endpoint.Client client = ClientProxy.getClient(service.getPortType());
        if (null != oldValue) {
            client.getRequestContext().put(Message.PROTOCOL_HEADERS, oldValue);
        } else {
            // Remove test reference
            client.getRequestContext().put(Message.PROTOCOL_HEADERS, Map.of());
        }
    }

}
