/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.test.common.test.pool.soap;

import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.lang3.RandomStringUtils;
import com.openexchange.admin.soap.util.dataobjects.Credentials;
import com.openexchange.admin.soap.util.soap.OXUtilService;
import com.openexchange.admin.soap.util.soap.OXUtilServicePortType;
import com.openexchange.ajax.framework.ProvisioningSetup;
import com.openexchange.test.common.test.pool.TestUser;

/**
 * {@link SoapUtilService}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class SoapUtilService implements SoapService {

    private static SoapUtilService INSTANCE;

    private final OXUtilService oxUtilService;
    private final OXUtilServicePortType oxUtilServicePortType;
    private final Credentials soapMasterCreds;
    private final URL wsdlLocation;

    /**
     * Gets the {@link SoapUtilService}
     *
     * @return The {@link SoapUtilService}
     * @throws MalformedURLException In case service can't be initialized
     */
    public static SoapUtilService getInstance() throws MalformedURLException {
        if (INSTANCE == null) {
            synchronized (SoapUtilService.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SoapUtilService();
                }
            }
        }
        return INSTANCE;
    }

    private SoapUtilService() throws MalformedURLException {
        this.wsdlLocation = new URL(SoapProvisioningService.getSOAPHostUrl(), "/webservices/OXUtilService?wsdl");
        this.oxUtilService = new OXUtilService(wsdlLocation);
        this.oxUtilServicePortType = oxUtilService.getOXUtilServiceHttpSoap12Endpoint();
        TestUser oxAdminMaster = ProvisioningSetup.getOxAdminMaster();
        this.soapMasterCreds = createCreds(oxAdminMaster.getUser(), oxAdminMaster.getPassword());
    }

    @Override
    public Object getPortType() {
        return oxUtilServicePortType;
    }

    @Override
    public URL getWsdlLocation() {
        return wsdlLocation;
    }

    /**
     * Creates new credentials based on the given paramters
     *
     * @param login The login name to be used
     * @param password The password to be used
     * @return The credential obj
     */
    private Credentials createCreds(String login, String password) {
        Credentials creds = new Credentials();
        creds.setLogin(login);
        creds.setPassword(password);
        return creds;
    }

    public Credentials getSoapMasterCreds() {
        return soapMasterCreds;
    }

    public Credentials getWrongCreds() {
        Credentials credentials = new Credentials();
        credentials.setLogin(RandomStringUtils.randomAlphabetic(6));
        credentials.setPassword(RandomStringUtils.randomAlphabetic(6));
        return credentials;
    }
}
