/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.test.common.test.pool.soap.reseller;

import java.net.MalformedURLException;
import java.net.URL;
import com.openexchange.admin.soap.reseller.resource.reseller.soap.OXResellerResourceService;
import com.openexchange.admin.soap.reseller.resource.reseller.soap.OXResellerResourceServicePortType;
import com.openexchange.admin.soap.reseller.resource.rmi.dataobjects.Credentials;
import com.openexchange.ajax.framework.ProvisioningSetup;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.test.common.test.pool.soap.SoapProvisioningService;
import com.openexchange.test.common.test.pool.soap.SoapService;

/**
 * {@link SoapResellerResourceService}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class SoapResellerResourceService implements SoapService {

    private static SoapResellerResourceService INSTANCE;

    private final OXResellerResourceService oxResellerResourceService;
    private final OXResellerResourceServicePortType oxResellerResourceServicePortType;
    private final Credentials soapMasterCreds;
    private final URL wsdlLocation;

    /**
     * Gets the {@link SoapResellerResourceService}
     *
     * @return The {@link SoapResellerResourceService}
     * @throws MalformedURLException In case service can't be initialized
     */
    public static SoapResellerResourceService getInstance() throws MalformedURLException {
        if (INSTANCE == null) {
            synchronized (SoapResellerResourceService.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SoapResellerResourceService();
                }
            }
        }
        return INSTANCE;
    }

    private SoapResellerResourceService() throws MalformedURLException {
        this.wsdlLocation = new URL(SoapProvisioningService.getSOAPHostUrl(), "/webservices/OXResellerResourceService?wsdl");
        this.oxResellerResourceService = new OXResellerResourceService(wsdlLocation);
        this.oxResellerResourceServicePortType = oxResellerResourceService.getOXResellerResourceServiceHttpSoap12Endpoint();
        TestUser oxAdminMaster = ProvisioningSetup.getOxAdminMaster();
        this.soapMasterCreds = createCreds(oxAdminMaster.getLogin(), oxAdminMaster.getPassword());
    }

    @Override
    public Object getPortType() {
        return oxResellerResourceServicePortType;
    }

    @Override
    public URL getWsdlLocation() {
        return wsdlLocation;
    }

    /**
     * Creates new credentials based on the given paramters
     *
     * @param login The login name to be used
     * @param password The password to be used
     * @return The credential obj
     */
    private Credentials createCreds(String login, String password) {
        Credentials creds = new Credentials();
        creds.setLogin(login);
        creds.setPassword(password);
        return creds;
    }

    public Credentials getSoapMasterCreds() {
        return soapMasterCreds;
    }

}
