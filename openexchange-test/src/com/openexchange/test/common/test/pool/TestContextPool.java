/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.test.common.test.pool;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.JSONServices;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestInfo;
import com.openexchange.admin.rmi.dataobjects.User;
import com.openexchange.ajax.framework.ProvisioningSetup;
import com.openexchange.exception.OXException;
import com.openexchange.test.Host;
import com.openexchange.test.common.configuration.AJAXConfig;
import com.openexchange.test.common.test.TestClassConfig;
import com.openexchange.test.common.test.TestContextConfig;
import com.openexchange.testing.httpclient.invoker.ApiException;

/**
 * {@link TestContextPool} - This class will manage the context handling, esp. providing unused contexts and queue related requests
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v7.8.3
 */
public class TestContextPool {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(TestContextPool.class);

    private TestContextPool() {
        super();
    }

    /**
     * Returns an exclusive {@link TestContext} which means this context is currently not used by any other test.<br>
     * <br>
     * <b>Caution: After using the {@link TestContext} make sure it will be returned to pool by using {@link #deleteContext(List)}!</b>
     *
     * @param acquiredByTest The name of the class that acquires the context
     * @return {@link TestContext} to be used for tests.
     * @throws OXException In case context can't be acquired
     */
    public static TestContext acquireContext(String acquiredByTest) throws OXException {
        TestClassConfig config = TestClassConfig.builder().build();
        return acquireContext(acquiredByTest, Optional.of(config));
    }

    /**
     * Returns an exclusive {@link TestContext} which means this context is currently not used by any other test.<br>
     * <br>
     * <b>Caution: After using the {@link TestContext} make sure it will be returned to pool by using {@link #deleteContext(List)}!</b>
     *
     * @param testInfo Information about the test requesting contexts
     * @param config The optional context configuration
     * @return {@link TestContext} to be used for tests.
     * @throws OXException In case context can't be acquired
     */
    public static TestContext acquireContext(TestInfo testInfo, Optional<TestClassConfig> config) throws OXException {
        if (testInfo.getTestClass().isPresent()) {
            if (testInfo.getTestMethod().isPresent()) {
                return acquireContext(testInfo.getTestClass().get().getCanonicalName() + "." + testInfo.getTestMethod().get().getName(), config);
            }
            LOG.warn("Using fallback for test method name. TestInfo: {}", testInfo);
            return acquireContext(testInfo.getTestClass().get().getCanonicalName() + "." + UUID.randomUUID().toString(), config);
        }
        if (testInfo.getTestMethod().isPresent()) {
            LOG.warn("Using fallback for test class name. TestInfo: {}", testInfo);
            return acquireContext(TestContextPool.class.getCanonicalName() + "." + testInfo.getTestMethod().get().getName(), config);
        }
        LOG.warn("Using fallback for test class and test method name. TestInfo: {}", testInfo);
        return acquireContext(TestContextPool.class.getCanonicalName() + "." + UUID.randomUUID().toString(), config);
    }

    /**
     * Returns an exclusive {@link TestContext} which means this context is currently not used by any other test.<br>
     * <br>
     * <b>Caution: After using the {@link TestContext} make sure it will be returned to pool by using {@link #deleteContext(List)}!</b>
     *
     * @param acquiredByTest The name of the class that acquires the context
     * @param config The optional context configuration
     * @return {@link TestContext} to be used for tests.
     * @throws OXException In case context can't be acquired
     */
    public static TestContext acquireContext(String acquiredByTest, Optional<TestClassConfig> config) throws OXException {
        List<TestContext> contextList = acquireOrCreateContexts(acquiredByTest, config, 1);
        assertFalse(contextList.isEmpty(), "Unable to acquire test context due to an empty pool.");
        TestContext context = contextList.get(0);
        LOG.debug("Context '{}' has been acquired by {}.", context.getName(), acquiredByTest);
        return context;
    }

    /**
     * <br>
     * Similar to {@link #acquireContext(String)} but returns any number of contexts instead of only one <br>
     * <br>
     * In addition the contexts will be created if config is provided or has changed
     *
     * @param acquiredByTest The name of the class that acquires the context
     * @param testClassConfig The optional context config
     * @param amount The amount of contexts to acquire
     * @return a list of {@link TestContext} to be used for tests.
     * @throws OXException In case context can't be acquired
     */
    public static List<TestContext> acquireOrCreateContexts(String acquiredByTest, Optional<TestClassConfig> testClassConfig, int amount) throws OXException {
        if (testClassConfig.isPresent() && testClassConfig.get().isEmptyContext()) {
            return createEmptyContexts(acquiredByTest, amount);
        }
        List<TestContext> result;
        if (testClassConfig.isPresent() && testClassConfig.get().optContextConfig().isPresent() && testClassConfig.get().optContextConfig().get().hasChanges()) {
            result = createCustomContexts(testClassConfig.get(), acquiredByTest, amount);
            LOG.debug("{} - Created custom contexts: {}", acquiredByTest, result);
        } else if (!createContextAndUser() && (testClassConfig.isPresent() && testClassConfig.get().optUserConfig().isPresent()) || testClassConfig.get().getNumberOfUsersPerContext() > 5) {
            result = createUsersForPreProvisionedContexts(testClassConfig, acquiredByTest, amount);
            LOG.debug("{} - Created custom users for pre-provisioned contexts: {}", acquiredByTest, result);
        } else if (testClassConfig.isPresent() && testClassConfig.get().optUserConfig().isPresent()) {
            result = createContexts(acquiredByTest, testClassConfig, amount);
            LOG.debug("{} - Created contexts with custom user config: {}", acquiredByTest, result);
        } else if (createContextAndUser()) {
            result = createContexts(acquiredByTest, testClassConfig, amount);
            LOG.debug("{} - Created contexts: {}", acquiredByTest, result);
        } else {
            result = usePreProvisionedContexts(acquiredByTest, amount);
            LOG.debug("{} - Used pre-provisioned contexts: {}", acquiredByTest, result);
        }

        checkContexts(acquiredByTest, result, amount);
        return result;
    }

    private static boolean createContextAndUser() {
        return Boolean.parseBoolean(AJAXConfig.getProperty(AJAXConfig.Property.CREATE_CONTEXT_AND_USER));
    }

    private static List<TestContext> createContexts(String acquiredByTest, Optional<TestClassConfig> testClassConfig, int amount) throws OXException {
        List<TestContext> result = new ArrayList<>(amount);
        for (int i = amount; i > 0; i--) {
            TestContext context = ConfigAwareProvisioningService.getService().createContext(acquiredByTest);

            List<TestUser> users = new ArrayList<>();
            for (int j = 0; j < ProvisioningUtils.USER_NAMES_POOL.length; j++) {
                users.add(ConfigAwareProvisioningService.getService()
                                                        .createUser(context.getId(),
                                                                    ProvisioningUtils.USER_NAMES_POOL[j],
                                                                    testClassConfig.isEmpty() ? Optional.empty() : testClassConfig.get().optUserConfig(),
                                                                    acquiredByTest));
            }
            context = new TestContext(context.getId(), acquiredByTest, context.getAdmin(), acquiredByTest, users);

            result.add(context);
        }
        return result;
    }

    private static List<TestContext> createUsersForPreProvisionedContexts(Optional<TestClassConfig> testClassConfig, String acquiredByTest, int amount) {
        List<TestContext> result = new ArrayList<>();
        for (int i = amount; i > 0; i--) {
            try {
                TestContext context = getPreProvisionedContext(acquiredByTest);
                if (testClassConfig.isPresent()) {
                    context.configure(testClassConfig.get());
                }
                result.add(context);
            } catch (Exception e) {
                LOG.error("Unable to get pre-provisioned context.", e);
            }
        }
        return result;
    }

    private static List<TestContext> usePreProvisionedContexts(String acquiredByTest, int amount) {
        List<TestContext> result = new ArrayList<>(amount);

        for (int i = amount; i > 0; i--) {
            try {
                result.add(getPreProvisionedContext(acquiredByTest));
            } catch (Exception e) {
                LOG.error("Unable to get pre-provisioned context.", e);
            }
        }
        return result;
    }

    private static List<TestContext> createEmptyContexts(String acquiredByTest, int amount) {
        List<TestContext> result = new ArrayList<>(amount);
        for (int i = amount; i > 0; i--) {
            try {
                result.add(ConfigAwareProvisioningService.getService().createContext(TestContextConfig.builder().build(), acquiredByTest));
            } catch (Exception e) {
                LOG.error("Unable to create context.", e);
            }
        }
        return result;
    }

    private static List<TestContext> createCustomContexts(TestClassConfig testClassConfig, String acquiredByTest, int amount) throws OXException {
        List<TestContext> result = new ArrayList<>(amount);
        for (int i = amount; i > 0; i--) {
            TestContext context = ConfigAwareProvisioningService.getService().createContext(testClassConfig.optContextConfig().get(), acquiredByTest);

            if (testClassConfig.optUserConfig().isPresent()) {
                try {
                    context.configure(testClassConfig);
                } catch (Exception e) {
                    LOG.error("Unable to configure custom context.", e);
                }
            } else {
                List<TestUser> users = new ArrayList<>();
                for (int j = 0; j < ProvisioningUtils.USER_NAMES_POOL.length; j++) {
                    users.add(ConfigAwareProvisioningService.getService().createUser(context.getId(), ProvisioningUtils.USER_NAMES_POOL[j], testClassConfig.optUserConfig(), acquiredByTest));
                }
                context = new TestContext(context.getId(), acquiredByTest, ProvisioningSetup.getOxAdminMaster(), acquiredByTest, users);
            }

            result.add(context);
        }
        return result;
    }

    private static TestContext getPreProvisionedContext(String acquiredByTest) throws Exception {
        int cid = getNextContextId(acquiredByTest);
        if (cid == -1) {
            throw ProvisioningExceptionCode.UNABLE_TO_CREATE_CONTEXT.create("Cannot retrieve next context id from global database");
        }
        User adminUser = ProvisioningUtils.createAdminUser(cid);
        List<TestUser> users = new ArrayList<>();
        for (int i = 0; i < ProvisioningUtils.USER_NAMES_POOL.length; i++) {
            TestUser userToTestUser = ProvisioningUtils.userToTestUser(ProvisioningUtils.getContextName(cid), ProvisioningUtils.USER_NAMES_POOL[i], "secret", I(i + 3), I(cid), acquiredByTest);
            users.add(userToTestUser);
        }

        return ProvisioningUtils.toTestContext(cid, ProvisioningUtils.userToTestUser(ProvisioningUtils.getContextName(cid), adminUser.getName(), adminUser.getPassword(), I(2), I(cid), acquiredByTest), acquiredByTest, users);
    }

    private static final String SCHEME = AJAXConfig.getProperty(AJAXConfig.Property.PROTOCOL);

    private static int getNextContextId(String acquiredByTest) throws Exception {
        URI uri = new URIBuilder().setScheme(SCHEME)
                                  .setHost(Host.SERVER.getHostname())
                                  .setPath(AJAXConfig.getProperty(AJAXConfig.Property.BASE_PATH) + "test")
                                  .setPort(SCHEME.equals("https") ? 443 : Integer.parseInt(AJAXConfig.getProperty(AJAXConfig.Property.SERVER_PORT)))
                                  .addParameter("acquiredBy", acquiredByTest)
                                  .build();

        HttpGet httpGet = new HttpGet(uri);
        try (CloseableHttpResponse response = ProvisioningSetup.getGlobalClient().execute(httpGet, new BasicHttpContext())) {
            assertEquals(200, response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
            String respString = EntityUtils.toString(response.getEntity());
            JSONObject contextObject = JSONServices.parseObject(respString);
            return contextObject.getInt("contextId");
        }
    }

    private static void checkContexts(String acquiredByTest, List<TestContext> result, int amount) {
        assertFalse(result.isEmpty(), "Unable to acquire test context due to an empty pool.");
        result.forEach(context -> assertFalse(context.getUsers().isEmpty(), "User for new context missing."));
        result.forEach(c -> LOG.info("Context '{}' has been acquired by {}.", Integer.valueOf(c.getId()), acquiredByTest));
        Assertions.assertEquals(result.size(), amount, "Was unable to acquire contexts for requested amount");
    }

    /**
     * Deletes given context
     *
     * @param contexts The context to delete
     * @throws OXException In case context can't be deleted
     */
    public static void deleteContext(List<TestContext> contexts) throws OXException {
        if (null == contexts || contexts.isEmpty()) {
            LOG.debug("No context to delete");
            return;
        }
        for (TestContext context : contexts) {
            deleteContext(context);
        }
    }

    /**
     * Delete given context
     *
     * @param context The context to delete
     * @throws OXException In case context can't be deleted
     */
    public static void deleteContext(TestContext context) throws OXException {
        if (context == null) {
            return;
        }
        for (TestUser user : context.getUsers()) {
            user.closeAjaxClients();
            try {
                user.performLogout();
            } catch (ApiException e) {
                LOG.error(e.getMessage(), e);
            }
        }
        if (createContextAndUser()) {
            ConfigAwareProvisioningService.getService().deleteContext(context.getId(), context.getUsedBy());
        }
    }

}
