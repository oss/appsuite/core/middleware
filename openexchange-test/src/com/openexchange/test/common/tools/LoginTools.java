/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.test.common.tools;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.ajax.framework.SessionAwareClient;
import com.openexchange.test.Host;
import com.openexchange.test.common.test.TestUserConfig;
import com.openexchange.test.common.test.pool.TestUser;
import com.openexchange.testing.httpclient.invoker.ApiClient;
import com.openexchange.testing.httpclient.invoker.ApiException;
import com.openexchange.testing.httpclient.models.LoginResponse;
import com.openexchange.testing.httpclient.modules.LoginApi;

/**
 *
 * {@link LoginTools}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class LoginTools {

    /** Wrapper object that delays initialization of logger class until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(LoginTools.class);
    }

    /** The user agent sent for all requests and the client identifier sent during the login request if not set otherwise **/
    public static final String HTTP_API_TESTING_AGENT = "HTTP API Testing Agent";

    /**
     * Creates an logged in {@link SessionAwareClient}
     *
     * @param user The user to create the client for
     * @param clientId The client identifier to user
     * @return The configured client
     * @throws ApiException in case of error
     */
    public static SessionAwareClient createConnectedApiClient(TestUser user, String clientId) throws ApiException {
        return createConnectedApiClient(user, Optional.empty(), Host.SERVER, clientId);
    }

    /**
     * Creates an logged in {@link SessionAwareClient}
     *
     * @param user The user to create the client for
     * @param config The optional configuration
     * @param host The host to configure the client against
     * @return The configured client
     * @throws ApiException in case of error
     */
    public static SessionAwareClient createConnectedApiClient(TestUser user, Optional<TestUserConfig> config, Host host) throws ApiException {
        return createConnectedApiClient(user, config, Optional.empty(), host);
    }

    /**
     * Creates an logged in {@link SessionAwareClient}
     *
     * @param user The user to create the client for
     * @param config The optional configuration
     * @param host The host to configure the client against
     * @param clientId The client identifier to user
     * @return The configured client
     * @throws ApiException in case of error
     */
    public static SessionAwareClient createConnectedApiClient(TestUser user, Optional<TestUserConfig> config, Host host, String clientId) throws ApiException {
        return createConnectedApiClient(user, config, Optional.empty(), host, clientId);
    }

    /**
     * Creates an logged in {@link SessionAwareClient}
     *
     * @param user The user to create the client for
     * @param config The optional configuration
     * @param userAgent The optional user agent
     * @param host The host to configure the client against
     * @return The configured client
     * @throws ApiException in case of error
     */
    public static SessionAwareClient createConnectedApiClient(TestUser user, Optional<TestUserConfig> config, Optional<String> userAgent, Host host) throws ApiException {
        return createConnectedApiClient(user, config, userAgent, host, null);
    }

    /**
     * Creates an logged in {@link SessionAwareClient}
     *
     * @param user The user to create the client for
     * @param config The optional configuration
     * @param userAgent The optional user agent
     * @param host The host to configure the client against
     * @param clientId The client identifier to user
     * @return The configured client
     * @throws ApiException in case of error
     */
    public static SessionAwareClient createConnectedApiClient(TestUser user, Optional<TestUserConfig> config, Optional<String> userAgent, Host host, String clientId) throws ApiException {
        SessionAwareClient apiClient = createApiClient(user, config, userAgent, host);
        performLogin(apiClient, userAgent, clientId);
        return apiClient;
    }

    /**
     * Creates an {@link SessionAwareClient} with set configuration
     *
     * @param user The user to create the client for
     * @param config The optional configuration
     * @param userAgent The optional user agent
     * @param host The host to configure the client against
     * @return The configured client
     */
    public static SessionAwareClient createApiClient(TestUser user, Optional<TestUserConfig> config, Optional<String> userAgent, Host host) {
        SessionAwareClient apiClient = new SessionAwareClient(user, config, userAgent);
        configureApiClient(apiClient, host, userAgent);
        return apiClient;
    }

    /**
     * Configures the API client
     *
     * @param apiClient The client to configure
     * @param host The host to set
     * @param userAgent The user agent to set
     */
    public static void configureApiClient(SessionAwareClient apiClient, Host host, Optional<String> userAgent) {
        apiClient.setBasePath(host.getPath());
        apiClient.setUserAgent(userAgent.orElse(HTTP_API_TESTING_AGENT));
    }

    /**
     * Perform a login
     *
     * @param client The {@link SessionAwareClient} to use
     * @param userAgent The user agent to use during login. Note: Must be the same as set in {@link ApiClient#setUserAgent(String)} for later cookie has calculation
     * @return The given client
     * @throws ApiException In case of error during login
     */
    public static SessionAwareClient performLogin(SessionAwareClient client, Optional<String> userAgent) throws ApiException {
        return performLogin(client, userAgent, null);
    }

    /**
     * Perform a login
     *
     * @param client The {@link SessionAwareClient} to use
     * @param userAgent The user agent to use during login. Note: Must be the same as set in {@link ApiClient#setUserAgent(String)} for later cookie has calculation
     * @param clientId The client identifier
     * @return The given client
     * @throws ApiException In case of error during login
     */
    public static SessionAwareClient performLogin(SessionAwareClient client, Optional<String> userAgent, String clientId) throws ApiException {
        String ua = userAgent.orElse(HTTP_API_TESTING_AGENT);
        try {
            LoginResponse resp = performLoginInternal(client, clientId, ua);
            assertNull(resp.getError(), resp.getErrorDesc() + client.getUser().getLogin() + "@" + client.getUser().getPassword());
            client.setSession(resp.getSession());
            client.setUserId(resp.getUserId());
            client.setSessionUserAgent(ua);
            return client;
        } catch (ApiException e) {
            LoggerHolder.LOGGER.error("Unable to peform login. Received status '{}' and message '{}'.", I(e.getCode()), e.getMessage(), e);
            throw e;
        }
    }

    /**
     * Performs a logout operation
     *
     * @param client The {@link SessionAwareClient} to logout
     * @throws ApiException In case of error during logout
     */
    public static void performLogout(SessionAwareClient client) throws ApiException {
        performLogout(client, client.getSession());
    }

    /**
     * Performs a logout operation
     *
     * @param client The {@link SessionAwareClient} to logout
     * @param sessionId The session identifier
     * @throws ApiException In case of error during logout
     */
    public static void performLogout(ApiClient client, String sessionId) throws ApiException {
        new LoginApi(client).doLogout(sessionId);
    }

    /**
     * Performs a login with an expected error
     *
     * @param client The {@link SessionAwareClient} to use
     * @param userAgent The user agent to use during login. Note: Must be the same as set in {@link ApiClient#setUserAgent(String)} for later cookie has calculation
     * @param clientId The client identifier
     * @param expectedErrorCode The expected error code
     */
    public static void performLogin(SessionAwareClient client, Optional<String> userAgent, String clientId, String expectedErrorCode) {
        String ua = userAgent.orElse(HTTP_API_TESTING_AGENT);
        try {
            LoginResponse resp = performLoginInternal(client, clientId, ua);
            assertNotNull(resp.getError());
            assertEquals(expectedErrorCode, resp.getCode());
        } catch (ApiException e) {
            // In case of other errors
            LoggerHolder.LOGGER.error("Unable to peform login. Received status '{}' and message '{}'.", I(e.getCode()), e.getMessage(), e);
            fail("Wrong error received: %s".formatted(e.getMessage()));
        }

    }

    // ----------------------------- private methods --------------------

    /**
     * Performs a login without error handling
     *
     * @param client The client to use
     * @param clientId The client id
     * @param userAgent The user agent
     * @return
     * @throws ApiException
     */
    private static LoginResponse performLoginInternal(SessionAwareClient client, String clientId, String userAgent) throws ApiException {
        LoginApi api = new LoginApi(client);
        LoginResponse resp = api.doLogin(client.getUser().getLogin(), client.getUser().getPassword(), null, null, clientId, null, null, userAgent, Boolean.TRUE);
        return resp;
    }

}
