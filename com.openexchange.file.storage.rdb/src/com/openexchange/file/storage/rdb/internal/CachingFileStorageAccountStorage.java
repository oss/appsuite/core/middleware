/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.file.storage.rdb.internal;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.database.DatabaseConnectionListeners.addAfterCommitCallbackElseExecute;
import static com.openexchange.file.storage.rdb.internal.RdbFileStorageAccountStorage.PARAM_CONNECTION;
import static com.openexchange.java.Autoboxing.I;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import org.slf4j.LoggerFactory;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.FileStorageAccount;
import com.openexchange.file.storage.FileStorageExceptionCodes;
import com.openexchange.file.storage.FileStorageService;
import com.openexchange.file.storage.generic.DefaultFileStorageAccount;
import com.openexchange.file.storage.rdb.Services;
import com.openexchange.file.storage.rdb.internal.caching.FileStorageAccountData;
import com.openexchange.file.storage.rdb.internal.caching.FileStorageAccountsCodec;
import com.openexchange.file.storage.registry.FileStorageServiceRegistry;
import com.openexchange.folderstorage.cache.service.FolderCacheInvalidationService;
import com.openexchange.java.Strings;
import com.openexchange.session.Session;

/**
 * {@link CachingFileStorageAccountStorage} - The messaging account manager backed by {@link CacheService}.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since Open-Xchange v6.18.2
 */
public final class CachingFileStorageAccountStorage implements FileStorageAccountStorage {

    private static final CachingFileStorageAccountStorage INSTANCE = new CachingFileStorageAccountStorage();

    /** Options for the cache holding file storage accounts per user */
    private static final CacheOptions<Map<String, List<FileStorageAccountData>>> ACCOUNTS_CACHE_OPTIONS = CacheOptions.<Map<String, List<FileStorageAccountData>>> builder()
                                                                                 .withCoreModuleName(CoreModuleName.FILE_STORAGE_ACCOUNTS)
                                                                                 .withCodecAndVersion(new FileStorageAccountsCodec())
                                                                                 .build();

    /**
     * Gets the cache-backed instance.
     *
     * @return The cache-backed instance
     */
    public static CachingFileStorageAccountStorage getInstance() {
        return INSTANCE;
    }

    /*-
     * ------------------------------ Member section ------------------------------
     */

    /**
     * The database-backed delegatee.
     */
    private final RdbFileStorageAccountStorage delegatee;

    /**
     * Initializes a new {@link CachingFileStorageAccountStorage}.
     */
    private CachingFileStorageAccountStorage() {
        super();
        delegatee = RdbFileStorageAccountStorage.getInstance();
    }

    /**
     * Invalidates specified account.
     *
     * @param serviceId The service identifier
     * @param id The account identifier
     * @param user The user identifier
     * @param contextId The context identifier
     * @throws OXException If invalidation fails
     */
    public void invalidate(String serviceId, int id, int user, int contextId) throws OXException {
        invalidateAccounts(contextId, user);
    }

    /**
     * Gets the first account matching specified account identifier.
     *
     * @param accountId The account identifier
     * @param session The session
     * @return The matching account or <code>null</code>
     * @throws OXException If look-up fails
     */
    public FileStorageAccount getAccount(int accountId, Session session) throws OXException {
        Optional<Cache<Map<String, List<FileStorageAccountData>>>> optCache = optCache();
        if (optCache.isEmpty()) {
            return delegatee.getAccount(accountId, session);
        }
        Cache<Map<String, List<FileStorageAccountData>>> cache = optCache.get();
        Map<String, List<FileStorageAccountData>> accountsByServiceId = cache.get(newCacheKey(cache, session), k -> delegatee.loadAccountsByServiceId(session));
        for (Entry<String, List<FileStorageAccountData>> entry : accountsByServiceId.entrySet()) {
            for (FileStorageAccountData accountData : entry.getValue()) {
                if (accountId == accountData.id()) {
                    return convertAccount(entry.getKey(), accountData);
                }
            }
        }
        return null;
    }

    @Override
    public int addAccount(final String serviceId, final FileStorageAccount account, final Session session) throws OXException {
        int accountId = delegatee.addAccount(serviceId, account, session);
        invalidateAccounts(session);
        return accountId;
    }

    @Override
    public void deleteAccount(final String serviceId, final FileStorageAccount account, final Session session) throws OXException {
        delegatee.deleteAccount(serviceId, account, session);
        addAfterCommitCallbackElseExecute((Connection) session.getParameter(PARAM_CONNECTION), c -> invalidateAccounts(session));
    }

    @Override
    public FileStorageAccount getAccount(String serviceId, int id, Session session) throws OXException {
        if (Strings.isEmpty(serviceId) || id < 0 || session == null) {
            throw FileStorageExceptionCodes.ACCOUNT_NOT_FOUND.create(I(id), serviceId, I(session == null ? -1 : session.getUserId()), I(session == null ? -1 : session.getContextId()));
        }
        Optional<Cache<Map<String, List<FileStorageAccountData>>>> optCache = optCache();
        if (optCache.isEmpty()) {
            return delegatee.getAccount(serviceId, id, session);
        }
        Cache<Map<String, List<FileStorageAccountData>>> cache = optCache.get();
        List<FileStorageAccountData> accountsData = cache.get(newCacheKey(cache, session), k -> delegatee.loadAccountsByServiceId(session)).get(serviceId);
        if (null != accountsData) {
            for (FileStorageAccountData accountData : accountsData) {
                if (id == accountData.id()) {
                    return convertAccount(serviceId, accountData);
                }
            }
        }
        throw FileStorageExceptionCodes.ACCOUNT_NOT_FOUND.create(I(id), serviceId, I(session.getUserId()), I(session.getContextId()));
    }

    @Override
    public List<FileStorageAccount> getAccounts(String serviceId, Session session) throws OXException {
        if (Strings.isEmpty(serviceId) || session == null) {
            return Collections.emptyList();
        }
        Optional<Cache<Map<String, List<FileStorageAccountData>>>> optCache = optCache();
        if (optCache.isEmpty()) {
            return delegatee.getAccounts(serviceId, session);
        }
        Cache<Map<String, List<FileStorageAccountData>>> cache = optCache.get();
        List<FileStorageAccountData> accountsData = cache.get(newCacheKey(cache, session), k -> delegatee.loadAccountsByServiceId(session)).get(serviceId);
        return null == accountsData ? Collections.emptyList() : convertAccounts(serviceId, accountsData);
    }

    @Override
    public void updateAccount(final String serviceId, final FileStorageAccount account, final Session session) throws OXException {
        delegatee.updateAccount(serviceId, account, session);
        invalidateAccounts(session);
        invalidateFolderCache(session, serviceId, account.getId());
    }

    public boolean hasEncryptedItems(final FileStorageService service, final Session session) throws OXException {
        return delegatee.hasEncryptedItems(service, session);
    }

    public void migrateToNewSecret(final FileStorageService parentService, final String oldSecret, final String newSecret, final Session session) throws OXException {
        delegatee.migrateToNewSecret(parentService, oldSecret, newSecret, session);
    }

    public void cleanUp(final FileStorageService parentService, final String secret, final Session session) throws OXException {
        delegatee.cleanUp(parentService, secret, session);
    }

    public void removeUnrecoverableItems(final FileStorageService parentService, final String secret, final Session session) throws OXException {
        delegatee.removeUnrecoverableItems(parentService, secret, session);
    }

    /**
     * Invalidates any cached file storage accounts associated with the session's user.
     *
     * @param session The session to invalidate the cache for
     * @throws OXException If invalidation fails
     */
    private void invalidateAccounts(Session session) throws OXException {
        invalidateAccounts(session.getContextId(), session.getUserId());
    }

    /**
     * Invalidates any cached file storage accounts associated with the session's user.
     *
     * @param contextId The context id
     * @param userId The identifier of the user to invalidate the cache for
     * @throws OXException If invalidation fails
     */
    private void invalidateAccounts(int contextId, int userId) throws OXException {
        Optional<Cache<Map<String, List<FileStorageAccountData>>>> optAccountCache = optCache();
        if (optAccountCache.isPresent()) {
            Cache<Map<String, List<FileStorageAccountData>>> cache = optAccountCache.get();
            cache.invalidate(newCacheKey(cache, contextId, userId));
        }
    }

    /**
     * Converts data of multiple file storage accounts to {@link FileStorageAccount} objects.
     *
     * @param serviceId The identifier of the associated file storage service
     * @param accountsData The accounts data to convert
     * @return The converted file storage accounts
     * @throws OXException If retrieving the file storage service fails
     */
    private List<FileStorageAccount> convertAccounts(String serviceId, List<FileStorageAccountData> accountsData) throws OXException {
        if (null == accountsData) {
            return null;
        }
        FileStorageService fileStorageService = Services.getServices().getServiceSafe(FileStorageServiceRegistry.class).getFileStorageService(serviceId);
        return accountsData.stream().map(a -> convertAccount(fileStorageService, a)).toList();
    }

    /**
     * Converts data of a specific file storage account to a {@link FileStorageAccount} object.
     *
     * @param serviceId The identifier of the associated file storage service
     * @param accountData The account data to convert
     * @return The converted file storage account
     * @throws OXException If retrieving the file storage service fails
     */
    private FileStorageAccount convertAccount(String serviceId, FileStorageAccountData accountData) throws OXException {
        FileStorageService fileStorageService = Services.getServices().getServiceSafe(FileStorageServiceRegistry.class).getFileStorageService(serviceId);
        return convertAccount(fileStorageService, accountData);
    }

    /**
     * Converts data of a specific file storage account to a {@link FileStorageAccount} object.
     *
     * @param fileStorageService The associated file storage service
     * @param accountData The account data to convert
     * @return The converted file storage account
     */
    private static FileStorageAccount convertAccount(FileStorageService fileStorageService, FileStorageAccountData accountData) {
        DefaultFileStorageAccount account = new DefaultFileStorageAccount();
        account.setId(Integer.toString(accountData.id()));
        account.setDisplayName(accountData.displayname());
        if (null != accountData.configuration()) {
            account.setConfiguration(accountData.configuration());
        }
        if (null != accountData.metadata()) {
            account.setMetaData(accountData.metadata());
        }
        account.setFileStorageService(fileStorageService);
        return account;
    }

    /**
     * Get the accounts cache if available.
     *
     * @return The optional cache
     */
    private Optional<Cache<Map<String, List<FileStorageAccountData>>>> optCache() {
        CacheService cacheService = Services.getServices().getOptionalService(CacheService.class);
        if (cacheService == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(cacheService.getCache(ACCOUNTS_CACHE_OPTIONS));
    }

    /**
     * Generates cache key for a specific user's file storage accounts.
     *
     * @param cache The file storage accounts cache
     * @param Session session The session
     * @return The new cache key
     */
    private static CacheKey newCacheKey(Cache<?> cache, Session session) {
        return newCacheKey(cache, session.getContextId(), session.getUserId());
    }

    /**
     * Generates cache key for a specific user's file storage accounts.
     *
     * @param cache The file storage accounts cache
     * @param contextId The context identifier
     * @param userId The user identifier
     * @return The new cache key
     */
    private static CacheKey newCacheKey(Cache<?> cache, int contextId, int userId) {
        return cache.newKey(asHashPart(contextId), Integer.toString(userId));
    }

    /**
     * Invalidates the folder cache for a specific account's root folder identifier.
     *
     * @param session The session
     * @param serviceId The identifier of the file storage service
     * @param accountId The identifier of the file storage account to invalidate
     */
    private static void invalidateFolderCache(Session session, String serviceId, String accountId) {
        try {
            FolderCacheInvalidationService invalidationService = Services.getServices().getServiceSafe(FolderCacheInvalidationService.class);
            invalidationService.invalidateSingle(serviceId + "://" + accountId + "/", "1", session);
        } catch (OXException e) {
            LoggerFactory.getLogger(CachingFileStorageAccountStorage.class).error("Unable to invalidate folder cache.", e);
        }
    }

}
