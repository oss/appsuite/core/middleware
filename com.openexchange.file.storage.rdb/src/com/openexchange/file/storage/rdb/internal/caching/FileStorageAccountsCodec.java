/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.file.storage.rdb.internal.caching;

import java.util.Map;
import java.util.UUID;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.AbstractMultiMapCodec;
import com.openexchange.file.storage.FileStorageAccount;

/**
 * {@link FileStorageAccountsCodec} is a codec for multiple {@link FileStorageAccount}s
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class FileStorageAccountsCodec extends AbstractMultiMapCodec<FileStorageAccountData> {

    private static final String CONFIGURATION_FIELD = "c";
    private static final String META_DATA_FIELD = "m";
    private static final String DISPLAY_NAME_FIELD = "d";
    private static final String ID_FIELD = "i";

    private static final UUID CODEC_ID = UUID.fromString("9c51c12b-b82f-4f2d-00fc-c0e11e000001");

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONObject serializeValue(FileStorageAccountData value) throws Exception {
        if (null == value) {
            return null;
        }
        JSONObject jsonObject = new JSONObject(4);
        jsonObject.put(ID_FIELD, value.id());
        jsonObject.putOpt(DISPLAY_NAME_FIELD, value.displayname());
        jsonObject.putOpt(META_DATA_FIELD, value.metadata());
        Map<String, Object> accountConfiguration = value.configuration();
        if (null != accountConfiguration && 0 < accountConfiguration.size()) {
            jsonObject.put(CONFIGURATION_FIELD, new JSONObject(accountConfiguration));
        }
        return jsonObject;
    }

    @Override
    protected FileStorageAccountData deserializeValue(JSONObject jsonObject) throws Exception {
        if (null == jsonObject) {
            return null;
        }
        int id = jsonObject.getInt(ID_FIELD);
        String displayName = jsonObject.optString(DISPLAY_NAME_FIELD, null);
        JSONObject configurationObject = jsonObject.optJSONObject(CONFIGURATION_FIELD);
        Map<String, Object> configuration = null != configurationObject ? configurationObject.asMap() : null;
        JSONObject metaData = jsonObject.optJSONObject(META_DATA_FIELD);
        return new FileStorageAccountData(id, displayName, metaData, configuration);
    }

}
