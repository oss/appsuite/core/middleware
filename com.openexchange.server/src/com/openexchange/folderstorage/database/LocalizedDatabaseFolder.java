/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.folderstorage.database;

import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.ConcurrentMap;
import org.jctools.maps.NonBlockingHashMap;
import com.openexchange.folderstorage.LocalizableNameFolder;
import com.openexchange.folderstorage.NameTranslator;
import com.openexchange.folderstorage.NameTranslatorFactory;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.i18n.LocaleTools;
import com.openexchange.i18n.tools.StringHelper;
import com.openexchange.java.Strings;

/**
 * {@link LocalizedDatabaseFolder} - A locale-sensitive database folder.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class LocalizedDatabaseFolder extends DatabaseFolder implements LocalizableNameFolder {

    private static final long serialVersionUID = 3830248343115931304L;

    /**
     * Initializes a new cacheable {@link LocalizedDatabaseFolder} from given database folder.
     * <p>
     * Subfolder identifiers and tree identifier are not set within this constructor. Moreover passed database folder is considered to be
     * subscribed.
     *
     * @param folderObject The underlying database folder
     */
    public LocalizedDatabaseFolder(final FolderObject folderObject) {
        this(folderObject, true);
    }

    /**
     * Initializes a new {@link LocalizedDatabaseFolder} from given database folder.
     * <p>
     * Subfolder identifiers and tree identifier are not set within this constructor. Moreover passed database folder is considered to be
     * subscribed.
     *
     * @param folderObject The underlying database folder
     * @param cacheable <code>true</code> if this database folder is cacheable; otherwise <code>false</code>
     */
    public LocalizedDatabaseFolder(final FolderObject folderObject, final boolean cacheable) {
        super(folderObject, cacheable);
    }

    @Override
    public Object clone() {
        return super.clone();
    }

    @Override
    public String getLocalizedName(final Locale locale) {
        return TRANSLATOR.translate(getName(), locale);
    }

    @Override
    public Optional<NameTranslatorFactory> getNameTranslatorFactory() {
        return Optional.of(TRANSLATOR_FACTORY);
    }

    /** The factory for the default name translator */
    public static final NameTranslatorFactory TRANSLATOR_FACTORY = new NameTranslatorFactory() {

        @Override
        public int getRegistrationNumber() {
            return 1;
        }

        @Override
        public NameTranslator getNameTranslator(int contextId) {
            return TRANSLATOR;
        }
    };

    /** The (caching) translator for names of database folders */
    protected static final NameTranslator TRANSLATOR = new NameTranslator() {

        /** Cache for already translated names */
        private final ConcurrentMap<NameAndLocale, String> localizedNames = new NonBlockingHashMap<NameAndLocale, String>();

        @Override
        public String translate(String toTranslate, Locale locale) {
            if (null == toTranslate) {
                return null;
            }

            Locale loc = null == locale ? LocaleTools.DEFAULT_LOCALE : locale;
            NameAndLocale key = new NameAndLocale(toTranslate, loc);
            String translation = localizedNames.get(key);
            if (null == translation) {
                final String ntranslation = StringHelper.valueOf(loc).getString(toTranslate);
                translation = localizedNames.putIfAbsent(key, ntranslation);
                if (null == translation) {
                    translation = ntranslation;
                }
            }
            return translation;
        }
    };

    /** Helper class for a tuple of (untranslated) name and locale */
    private static final class NameAndLocale implements Comparable<NameAndLocale> {

        private final String name;
        private final Locale locale;

        /**
         * Initializes a new instance of {@link NameAndLocale}.
         *
         * @param name The untranslated name
         * @param locale The locale to use for translation
         */
        private NameAndLocale(String name, Locale locale) {
            super();
            this.name = name;
            this.locale = locale;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            result = prime * result + ((locale == null) ? 0 : locale.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            NameAndLocale other = (NameAndLocale) obj;
            if (name == null) {
                if (other.name != null) {
                    return false;
                }
            } else if (!name.equals(other.name)) {
                return false;
            }
            if (locale == null) {
                if (other.locale != null) {
                    return false;
                }
            } else if (!locale.equals(other.locale)) {
                return false;
            }
            return true;
        }

        @Override
        public int compareTo(NameAndLocale o) {
            int c = Strings.compare(name, o.name);
            return c == 0 ? Strings.compare(locale.toString(), o.locale.toString()) : c;
        }
    } // End of class NameAndLocale

}
