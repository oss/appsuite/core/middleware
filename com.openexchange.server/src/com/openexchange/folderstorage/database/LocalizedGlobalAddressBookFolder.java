/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.folderstorage.database;

import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.ConcurrentMap;
import org.jctools.maps.NonBlockingHashMap;
import com.openexchange.folderstorage.GlobalAddressBookUtils;
import com.openexchange.folderstorage.NameTranslator;
import com.openexchange.folderstorage.NameTranslatorFactory;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.groupware.i18n.FolderStrings;
import com.openexchange.i18n.LocaleTools;
import com.openexchange.java.Strings;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link LocalizedGlobalAddressBookFolder}
 *
 * A locale-sensitive global address book database folder
 *
 * @author <a href="mailto:philipp.schumacher@open-xchange.com">Philipp Schumacher</a>
 * @since v7.10.6
 */
public class LocalizedGlobalAddressBookFolder extends LocalizedDatabaseFolder {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(LocalizedGlobalAddressBookFolder.class);

    private static final long serialVersionUID = -1837825836010345977L;

    /** Cache for already translated names of THIS instance */
    private final ConcurrentMap<Locale, String> localizedNames = new NonBlockingHashMap<>();

    /**
     * Initializes a new instance of {@link LocalizedGlobalAddressBookFolder}.
     *
     * @param folderObject The database folder
     */
    public LocalizedGlobalAddressBookFolder(FolderObject folderObject) {
        super(folderObject);
    }

    @Override
    public String getLocalizedName(Locale locale) {
        final Locale loc = null == locale ? LocaleTools.DEFAULT_LOCALE : locale;
        String translation = localizedNames.get(loc);
        if (null == translation) {
            try {
                ServerSession session = getSession();
                if (session != null) {
                    translation = translateGlobalAddressBookNameFor(session.getContextId(), loc);
                    localizedNames.put(loc, translation);
                    return translation;
                }
            } catch (Exception e) {
                LOG.debug("Unable to localize global address book folder name, assuming '{}'.", FolderStrings.SYSTEM_LDAP_FOLDER_NAME, e);
            }
            return FolderStrings.SYSTEM_LDAP_FOLDER_NAME;
        }
        return translation;
    }

    @Override
    public Optional<NameTranslatorFactory> getNameTranslatorFactory() {
        return Optional.of(GAB_TRANSLATOR_FACTORY);
    }

    // ----------------------------------------------- Name Translator stuff ---------------------------------------------------------------

    /** The factory for the special Global Address Book folder */
    public static final NameTranslatorFactory GAB_TRANSLATOR_FACTORY = new NameTranslatorFactory() {

        @Override
        public int getRegistrationNumber() {
            return 2;
        }

        @Override
        public NameTranslator getNameTranslator(int contextId) {
            return new GlobalAddressBookNameTranslator(contextId);
        }
    };

    /** Looks-up the appropriate translated name for the special Global Address Book folder */
    private static final class GlobalAddressBookNameTranslator implements NameTranslator {

        private final int contextId;

        /**
         * Initializes a new instance.
         *
         * @param contextId The context identifier for this translator
         */
        private GlobalAddressBookNameTranslator(int contextId) {
            super();
            this.contextId = contextId;
        }

        @Override
        public String translate(String name, Locale locale) {
            return translateGlobalAddressBookNameFor(contextId, null == locale ? LocaleTools.DEFAULT_LOCALE : locale);
        }
    }

    // ----------------------------------------------- Translation stuff -------------------------------------------------------------------

    /** The cache for already translated names for the Global Address Book folder */
    private static final ConcurrentMap<ContextIdAndLocale, String> TRANSLATIONS = new NonBlockingHashMap<>();

    /**
     * Gets the translated name of the special Global Address Book folder for given context using specified locale.
     *
     * @param contextId The identifier of the context to translate for
     * @param locale The locale
     * @return The translated name of the special Global Address Book folder
     */
    private static String translateGlobalAddressBookNameFor(int contextId, Locale locale) {
        ContextIdAndLocale key = new ContextIdAndLocale(contextId, locale);
        String translation = TRANSLATIONS.get(key);
        if (translation == null) {
            String ntranslation = GlobalAddressBookUtils.getFolderName(locale, contextId);
            translation = TRANSLATIONS.putIfAbsent(key, ntranslation);
            if (translation == null) {
                translation = ntranslation;
            }
        }
        return translation;
    }

    /** Helper class for a tuple of context identifier and locale */
    private static final class ContextIdAndLocale implements Comparable<ContextIdAndLocale> {

        private final int contextId;
        private final Locale locale;

        /**
         * Initializes a new instance of {@link ContextIdAndLocale}.
         *
         * @param contextId The context identifier
         * @param locale The locale
         */
        private ContextIdAndLocale(int contextId, Locale locale) {
            super();
            this.contextId = contextId;
            this.locale = locale;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + contextId;
            result = prime * result + ((locale == null) ? 0 : locale.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            ContextIdAndLocale other = (ContextIdAndLocale) obj;
            if (contextId != other.contextId) {
                return false;
            }
            if (locale == null) {
                if (other.locale != null) {
                    return false;
                }
            } else if (!locale.equals(other.locale)) {
                return false;
            }
            return true;
        }

        @Override
        public int compareTo(ContextIdAndLocale o) {
            int c = Integer.compare(contextId, o.contextId);
            return c == 0 ? Strings.compare(locale.toString(), o.locale.toString()) : c;
        }
    }

}
