/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.folderstorage.internal.performers;

import static com.openexchange.server.services.ServerServiceRegistry.getInstance;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import com.openexchange.concurrent.CallerRunsCompletionService;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.CalculatePermission;
import com.openexchange.folderstorage.Folder;
import com.openexchange.folderstorage.FolderServiceDecorator;
import com.openexchange.folderstorage.FolderStorage;
import com.openexchange.folderstorage.Permission;
import com.openexchange.folderstorage.SearchableFileFolderNameFolderStorage;
import com.openexchange.folderstorage.StorageParameters;
import com.openexchange.folderstorage.UserizedFolder;
import com.openexchange.java.Collators;
import com.openexchange.threadpool.ThreadPoolCompletionService;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.threadpool.ThreadPools;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link FileStorageSearchPerformer}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 * @since v7.10.5
 */
public class FileStorageSearchPerformer extends AbstractUserizedFolderPerformer {

    public FileStorageSearchPerformer(ServerSession session, FolderServiceDecorator decorator) throws OXException {
        super(session, decorator);
    }

    /**
     * Searches a folder below given folder identifier by folder name
     *
     * @param treeId The tree identifier
     * @param folderId The 'root' folder for search operation
     * @param query The query to search
     * @param date Time stamp to limit search result to folders that are newer
     * @param includeSubfolders Include all sub-folders below given folder identifier
     * @param all <code>true</code> to deliver all sub-folders regardless of their subscribed status; <code>false</code> to deliver
     *            subscribed folders only
     * @param start A start index (inclusive) for the search results. Useful for paging.
     * @param end An end index (exclusive) for the search results. Useful for paging.
     * @return {@link List} of {@link UserizedFolder} sorted by name
     * @throws OXException If search fails
     */
    public List<UserizedFolder> doSearch(String treeId, String folderId, String query, long date, boolean includeSubfolders, boolean all, int start, int end) throws OXException {
        FolderStorage[] neededStorages = folderStorageDiscoverer.getFolderStoragesForParent(treeId, folderId);
        if (null == neededStorages || 0 == neededStorages.length) {
            return Collections.emptyList();
        }

        List<SearchableFileFolderNameFolderStorage> searchableStorages = Arrays.stream(neededStorages).filter(SearchableFileFolderNameFolderStorage.class::isInstance).map(s -> (SearchableFileFolderNameFolderStorage) s).toList();
        if (null == searchableStorages || searchableStorages.isEmpty()) {
            return Collections.emptyList();
        }

        // Process by folder storage
        CompletionService<List<UserizedFolder>> completionService;
        StorageParametersProvider paramsProvider;
        boolean single = 1 == searchableStorages.size();
        if (single) {
            completionService = new CallerRunsCompletionService<>();
            paramsProvider = new InstanceStorageParametersProvider(storageParameters);
        } else {
            completionService = new ThreadPoolCompletionService<>(getInstance().getService(ThreadPoolService.class, true));
            paramsProvider = null == session ? new SessionStorageParametersProvider(user, context) : new SessionStorageParametersProvider(session);
        }

        int taskCount = 0;
        for (SearchableFileFolderNameFolderStorage searchableStorage : searchableStorages) {
            completionService.submit(new Callable<List<UserizedFolder>>() {

                @Override
                public List<UserizedFolder> call() throws Exception {
                    StorageParameters newParameters = paramsProvider.getStorageParameters();
                    List<FolderStorage> openedStorages = new ArrayList<FolderStorage>(2);
                    try {
                        if (searchableStorage.startTransaction(newParameters, false)) {
                            openedStorages.add(searchableStorage);
                        }

                        ServerSession serverSession = getSession();

                        int st = single ? start : 0;
                        List<Folder> folders = searchableStorage.searchFileStorageFolders(treeId, folderId, query, date, includeSubfolders, st, end, newParameters);
                        List<UserizedFolder> result = null;
                        for (Folder folder : folders) {
                            Permission ownPermission;
                            if (null == serverSession) {
                                ownPermission = CalculatePermission.calculate(folder, getUser(), getContext(), getAllowedContentTypes());
                            } else {
                                ownPermission = CalculatePermission.calculate(folder, serverSession, getAllowedContentTypes());
                            }
                            if (ownPermission.isVisible() && (all || folder.isSubscribed())) {
                                if (result == null) {
                                    result = new ArrayList<UserizedFolder>(folders.size());
                                }
                                result.add(getUserizedFolder(folder, ownPermission, treeId, true, true, newParameters, openedStorages));
                            }
                        }

                        for (Iterator<FolderStorage> it = openedStorages.iterator(); it.hasNext();) {
                            it.next().commitTransaction(newParameters);
                            it.remove();
                        }

                        return result == null ? Collections.emptyList() : result;
                    } finally {
                        for (FolderStorage fs : openedStorages) {
                            fs.rollback(newParameters);
                        }
                    }
                }
            });
            taskCount++;
        }

        // Wait for completion
        List<List<UserizedFolder>> results = ThreadPools.takeCompletionService(completionService, taskCount, FACTORY);
        List<UserizedFolder> overallResult;
        if (single) {
            overallResult = results.get(0);
        } else {
            overallResult = new ArrayList<>();
            for (List<UserizedFolder> folders : results) {
                overallResult.addAll(folders);
            }
            // Sort by name
            Collections.sort(overallResult, new FolderComparator(getLocale()));

            // Slice
            overallResult = overallResult.subList(start, end);
        }
        return overallResult;
    }

    private static final class FolderComparator implements Comparator<Folder> {

        private final Locale locale;
        private final Collator collator;

        /**
         * Initializes a new {@link FolderComparator}.
         *
         * @param locale The locale to use, or <code>null</code> to fall back to the default locale
         */
        public FolderComparator(Locale locale) {
            super();
            this.locale = locale;
            collator = Collators.getSecondaryInstance(null == locale ? Locale.US : locale);
        }

        @Override
        public int compare(Folder folder1, Folder folder2) {
            return collator.compare(folder1.getName(locale), folder2.getName(locale));
        }

    }

}
