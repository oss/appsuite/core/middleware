/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.folderstorage;


/**
 * {@link UserSensitiveAttributesProvider} - Provides the user-sensitive attributes for a folder.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public interface UserSensitiveAttributesProvider {

    /**
     * Gets the registration number.
     *
     * @return The registration number
     */
    int getRegistrationNumber();

    /**
     * Indicates if this folder is subscribed.
     *
     * @param folder The folder to check for
     * @return <code>true</code> if this folder is subscribed; otherwise <code>false</code>
     */
    boolean isSubscribed(Folder folder);

    /**
     * Whether the folder is used for sync or not. Defaults to {@link UsedForSync#DEFAULT}
     *
     * @param folder The folder to check for
     * @return <code>true</code> if the folder is used for sync, <code>false</code> otherwise
     */
    default UsedForSync getUsedForSync(Folder folder) {
        return UsedForSync.DEFAULT;
    }

}
