/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.folderstorage.cache;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.mail.utils.MailFolderUtility.prepareFullname;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import com.google.common.collect.ImmutableSet;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.concurrent.CallerRunsCompletionService;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.AfterReadAwareFolderStorage;
import com.openexchange.folderstorage.AfterReadAwareFolderStorage.Mode;
import com.openexchange.folderstorage.ContentType;
import com.openexchange.folderstorage.Folder;
import com.openexchange.folderstorage.FolderExceptionErrorMessage;
import com.openexchange.folderstorage.FolderServiceDecorator;
import com.openexchange.folderstorage.FolderStorage;
import com.openexchange.folderstorage.FolderType;
import com.openexchange.folderstorage.ReinitializableFolderStorage;
import com.openexchange.folderstorage.RemoveAfterAccessFolder;
import com.openexchange.folderstorage.RestoringFolderStorage;
import com.openexchange.folderstorage.SearchableFileFolderNameFolderStorage;
import com.openexchange.folderstorage.SortableId;
import com.openexchange.folderstorage.StorageParameters;
import com.openexchange.folderstorage.StoragePriority;
import com.openexchange.folderstorage.StorageType;
import com.openexchange.folderstorage.SubfolderListingFolderStorage;
import com.openexchange.folderstorage.TrashAwareFolderStorage;
import com.openexchange.folderstorage.TrashResult;
import com.openexchange.folderstorage.Type;
import com.openexchange.folderstorage.UserizedFolder;
import com.openexchange.folderstorage.cache.codec.FolderAndContextId;
import com.openexchange.folderstorage.cache.codec.FolderCacheValueCodec;
import com.openexchange.folderstorage.cache.memory.FolderMap;
import com.openexchange.folderstorage.cache.memory.FolderMapManagement;
import com.openexchange.folderstorage.cache.service.FolderCacheInvalidationService;
import com.openexchange.folderstorage.database.DatabaseFolderType;
import com.openexchange.folderstorage.internal.StorageParametersImpl;
import com.openexchange.folderstorage.internal.Tools;
import com.openexchange.folderstorage.internal.performers.ClearPerformer;
import com.openexchange.folderstorage.internal.performers.CreatePerformer;
import com.openexchange.folderstorage.internal.performers.DeletePerformer;
import com.openexchange.folderstorage.internal.performers.InstanceStorageParametersProvider;
import com.openexchange.folderstorage.internal.performers.PathPerformer;
import com.openexchange.folderstorage.internal.performers.SessionStorageParametersProvider;
import com.openexchange.folderstorage.internal.performers.StorageParametersProvider;
import com.openexchange.folderstorage.internal.performers.UpdatePerformer;
import com.openexchange.folderstorage.internal.performers.UpdatesPerformer;
import com.openexchange.folderstorage.mail.MailFolderType;
import com.openexchange.folderstorage.osgi.FolderStorageServices;
import com.openexchange.folderstorage.outlook.OutlookFolderStorage;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.userconfiguration.UserPermissionBits;
import com.openexchange.java.Collators;
import com.openexchange.java.Strings;
import com.openexchange.mail.dataobjects.MailFolder;
import com.openexchange.mailaccount.Account;
import com.openexchange.mailaccount.MailAccount;
import com.openexchange.mailaccount.MailAccountStorageService;
import com.openexchange.osgi.ServiceRegistry;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.session.Session;
import com.openexchange.threadpool.RefusedExecutionBehavior;
import com.openexchange.threadpool.ThreadPoolCompletionService;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.threadpool.ThreadPools;
import com.openexchange.threadpool.ThreadPools.TrackableCallable;
import com.openexchange.threadpool.behavior.AbortBehavior;
import com.openexchange.tools.oxfolder.OXFolderExceptionCode;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tools.session.ServerSessionAdapter;
import com.openexchange.user.User;
import com.openexchange.userconf.UserPermissionService;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;

/**
 * {@link CacheFolderStorage} - The cache folder storage.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class CacheFolderStorage implements ReinitializableFolderStorage, FolderCacheInvalidationService, TrashAwareFolderStorage, SubfolderListingFolderStorage, RestoringFolderStorage, SearchableFileFolderNameFolderStorage {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CacheFolderStorage.class);

    private static final ThreadPools.ExpectedExceptionFactory<OXException> FACTORY = new ThreadPools.ExpectedExceptionFactory<OXException>() {

        @Override
        public Class<OXException> getType() {
            return OXException.class;
        }

        @Override
        public OXException newUnexpectedError(Throwable t) {
            return FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(t, t.getMessage());
        }
    };

    private static final CacheFolderStorage INSTANCE = new CacheFolderStorage();

    /**
     * Gets the instance
     *
     * @return The instance
     */
    public static CacheFolderStorage getInstance() {
        return INSTANCE;
    }

    private static final String ROOT_ID = FolderStorage.ROOT_ID;

    private static final Set<String> NON_CACHEABLE_TREES = Set.of(OutlookFolderStorage.OUTLOOK_TREE_ID);

    /**
     * Checks if given tree identifier holds cacheable folders.
     *
     * @param treeId The tree identifier
     * @return <code>true</code> if folders from given tree are cacheable; otherwise <code>false</code> if known to be non-cacheable
     */
    private static boolean isCacheableTree(String treeId) {
        return treeId != null && !NON_CACHEABLE_TREES.contains(treeId);
    }

    // ------------------------------------------------------------------------ //

    private final String realTreeId;
    private final CacheFolderStorageRegistry registry;
    private final AtomicReference<Cache<FolderAndContextId>> globalCacheRef;

    /**
     * Initializes a new {@link CacheFolderStorage}.
     */
    private CacheFolderStorage() {
        super();
        realTreeId = REAL_TREE_ID;
        registry = CacheFolderStorageRegistry.getInstance();
        globalCacheRef = new AtomicReference<>();
    }

    /**
     * Removes denoted folder from global cache.
     *
     * @param folderId The folder identifier
     * @param treeId The tree identifier
     * @param contextId The context identifier
     */
    public void removeFromGlobalCache(String folderId, String treeId, int contextId) {
        Cache<FolderAndContextId> cache = globalCacheRef.get();
        if (null != cache && Tools.isGlobalId(folderId) && isCacheableTree(treeId)) {
            try {
                cache.invalidate(newCacheKey(folderId, treeId, contextId, cache));
            } catch (Exception e) {
                LOG.error("Failed to invalidate folder {} in tree {} of context {}", folderId, treeId, I(contextId), e);
            }
        }
    }

    @Override
    public void clearCache(int userId, int contextId) {
        if (contextId <= 0) {
            return;
        }
        Cache<FolderAndContextId> cache = globalCacheRef.get();
        if (null != cache) {
            try {
                cache.invalidateGroup(cache.newGroupKey(asHashPart(contextId)));
            } catch (Exception e) {
                LOG.error("Failed to invalidate group {}", I(contextId), e);
            }
        }
        if (userId > 0) {
            FolderMapManagement.getInstance().dropFor(userId, contextId);
        } else {
            FolderMapManagement.getInstance().dropFor(contextId);
        }
    }

    /** The options for folder cache */
    private static final CacheOptions<FolderAndContextId> OPTIONS = CacheOptions.<FolderAndContextId> builder()
        .withCoreModuleName(CoreModuleName.GLOBAL_FOLDER)
        .withCodecAndVersion(new FolderCacheValueCodec())
        .build();

    /**
     * Initializes this folder cache on available cache service.
     *
     * @param cacheService A reference to the cache service
     */
    public void onCacheAvailable(CacheService cacheService) {
        ConfigurationService configService = CacheServiceRegistry.getServiceRegistry().getOptionalService(ConfigurationService.class);
        if (null != configService && configService.getBoolProperty("com.openexchange.folderstorage.cache.enableGlobalFolderCache", false)) {
            globalCacheRef.set(cacheService.getCache(OPTIONS));
        }
    }

    /**
     * Disposes this folder cache on absent cache service.
     */
    public void onCacheAbsent() {
        globalCacheRef.set(null);
    }

    @Override
    public boolean reinitialize(String treeId, StorageParameters storageParameters) throws OXException {
        boolean reinitialized = false;
        for (FolderStorage folderStorage : registry.getFolderStoragesForTreeID(treeId)) {
            if (folderStorage instanceof ReinitializableFolderStorage) {
                boolean started = folderStorage.startTransaction(storageParameters, false);
                try {
                    reinitialized |= ((ReinitializableFolderStorage) folderStorage).reinitialize(treeId, storageParameters);
                    if (started) {
                        folderStorage.commitTransaction(storageParameters);
                        started = false;
                    }
                } catch (RuntimeException e) {
                    throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
                } finally {
                    if (started) {
                        folderStorage.rollback(storageParameters);
                    }
                }
            }
        }
        return reinitialized;
    }

    private static final Set<String> IGNORABLES = RemoveAfterAccessFolder.IGNORABLES;

    @Override
    public void checkConsistency(String treeId, final StorageParameters storageParameters) throws OXException {
        for (FolderStorage folderStorage : registry.getFolderStoragesForTreeID(treeId)) {
            boolean started = folderStorage.startTransaction(storageParameters, false);
            try {
                folderStorage.checkConsistency(treeId, storageParameters);
                if (started) {
                    folderStorage.commitTransaction(storageParameters);
                    started = false;
                }
            } catch (RuntimeException e) {
                throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
            } finally {
                if (started) {
                    folderStorage.rollback(storageParameters);
                }
            }
        }
        final String realTreeId = this.realTreeId;
        if (realTreeId.equals(treeId)) {
            try {
                final int contextId = storageParameters.getContextId();
                final int userId = storageParameters.getUserId();
                final UserPermissionBits userPermissionBits = FolderStorageServices.requireService(UserPermissionService.class).getUserPermissionBits(userId, contextId);
                final ServiceRegistry serviceRegistry = CacheServiceRegistry.getServiceRegistry();
                final ThreadPoolService threadPool = ThreadPools.getThreadPool();
                final RefusedExecutionBehavior<Object> behavior = AbortBehavior.getInstance();
                /*
                 * Traverse mail accounts in separate task
                 */
                Runnable task = new Runnable() {

                    @Override
                    public void run() {
                        try {
                            final StorageParameters params = newStorageParameters(storageParameters);
                            params.putParameter(MailFolderType.getInstance(), StorageParameters.PARAM_ACCESS_FAST, Boolean.FALSE);
                            MailAccountStorageService storageService = serviceRegistry.getService(MailAccountStorageService.class, true);
                            MailAccount[] accounts;
                            if (userPermissionBits.isMultipleMailAccounts()) {
                                accounts = storageService.getUserMailAccounts(userId, contextId);
                            } else {
                                accounts = storageService.getUserDefaultAndSecondaryMailAccounts(userId, contextId);
                            }
                            /*
                             * Gather tasks...
                             */
                            List<Runnable> tasks = new ArrayList<Runnable>(accounts.length);
                            for (MailAccount mailAccount : accounts) {
                                int accountId = mailAccount.getId();
                                if (accountId != Account.DEFAULT_ID && !IGNORABLES.contains(mailAccount.getMailProtocol()) && !mailAccount.isDeactivated()) {
                                    final String folderId = prepareFullname(accountId, MailFolder.ROOT_FOLDER_ID);
                                    /*
                                     * Check if already present
                                     */
                                    Folder rootFolder = getRefFromCache(realTreeId, folderId, params, false);
                                    if (null == rootFolder) {
                                        Runnable mailAccountTask = new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    /*
                                                     * Load it
                                                     */
                                                    Folder rootFolder = loadFolder(realTreeId, folderId, StorageType.WORKING, params);
                                                    putFolder(rootFolder, realTreeId, params, false);
                                                    String[] subfolderIDs = rootFolder.getSubfolderIDs();
                                                    if (null != subfolderIDs) {
                                                        for (String subfolderId : subfolderIDs) {
                                                            Folder folder = loadFolder(realTreeId, subfolderId, StorageType.WORKING, params);
                                                            putFolder(folder, realTreeId, params, false);
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    // Pre-Accessing external account folder failed.
                                                    LOG.debug("", e);
                                                }
                                            }
                                        };
                                        tasks.add(mailAccountTask);
                                    }
                                }
                            }
                            if (!tasks.isEmpty()) {
                                for (Runnable task : tasks) {
                                    threadPool.submit(ThreadPools.trackableTask(task), behavior);
                                }
                            }
                        } catch (Exception e) {
                            LOG.debug("", e);
                        }
                    }
                };
                threadPool.submit(ThreadPools.trackableTask(task), behavior);
            } catch (Exception e) {
                LOG.debug("", e);
            }
        }
    }

    /**
     * Checks if given folder storage is already contained in collection of opened storages. If yes, this method terminates immediately.
     * Otherwise the folder storage is opened according to specified modify flag and is added to specified collection of opened storages.
     */
    protected static void checkOpenedStorage(FolderStorage checkMe, StorageParameters params, boolean modify, java.util.Collection<FolderStorage> openedStorages) throws OXException {
        if (openedStorages.contains(checkMe)) {
            // Passed storage is already opened
            return;
        }
        // Passed storage has not been opened before. Open now and add to collection
        if (checkMe.startTransaction(params, modify)) {
            openedStorages.add(checkMe);
        }
    }

    @Override
    public void restore(String treeId, String folderId, StorageParameters storageParameters) throws OXException {
        FolderStorage storage = registry.getFolderStorage(treeId, folderId);
        if (null == storage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, folderId);
        }
        boolean started = storage.startTransaction(storageParameters, false);
        try {
            storage.restore(treeId, folderId, storageParameters);
            if (started) {
                storage.commitTransaction(storageParameters);
                started = false;
            }
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (started) {
                storage.rollback(storageParameters);
            }
            clearCache(storageParameters.getUserId(), storageParameters.getContextId());
        }
    }

    @Override
    public Folder prepareFolder(String treeId, Folder folder, StorageParameters storageParameters) throws OXException {
        String folderId = folder.getID();
        FolderStorage storage = registry.getFolderStorage(treeId, folderId);
        if (null == storage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, folderId);
        }
        boolean started = storage.startTransaction(storageParameters, false);
        try {
            Folder preparedFolder = storage.prepareFolder(treeId, folder, storageParameters);
            if (started) {
                storage.commitTransaction(storageParameters);
                started = false;
            }
            if (preparedFolder.isCacheable() && preparedFolder.isGlobalID() != folder.isGlobalID()) {
                putFolder(preparedFolder, treeId, storageParameters, false);
            }
            return preparedFolder;
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (started) {
                storage.rollback(storageParameters);
            }
        }
    }

    private PathPerformer newPathPerformer(StorageParameters storageParameters) throws OXException {
        Session session = storageParameters.getSession();
        FolderServiceDecorator decorator = storageParameters.getDecorator();
        PathPerformer performer;
        if (null == session) {
            performer = new PathPerformer(storageParameters.getUser(), storageParameters.getContext(), decorator, registry);
        } else {
            performer = new PathPerformer(ServerSessionAdapter.valueOf(session), decorator, registry);
        }
        performer.setStorageParameters(storageParameters);
        return performer;
    }

    private CreatePerformer newCreatePerformer(StorageParameters storageParameters) throws OXException {
        Session session = storageParameters.getSession();
        FolderServiceDecorator decorator = storageParameters.getDecorator();
        CreatePerformer performer;
        if (null == session) {
            performer = new CreatePerformer(storageParameters.getUser(), storageParameters.getContext(), decorator, registry);
        } else {
            performer = new CreatePerformer(ServerSessionAdapter.valueOf(session), decorator, registry);
        }
        performer.setStorageParameters(storageParameters);
        return performer;
    }

    private DeletePerformer newDeletePerformer(StorageParameters storageParameters) throws OXException {
        Session session = storageParameters.getSession();
        FolderServiceDecorator decorator = storageParameters.getDecorator();
        DeletePerformer performer;
        if (null == session) {
            performer = new DeletePerformer(storageParameters.getUser(), storageParameters.getContext(), decorator, registry);
        } else {
            performer = new DeletePerformer(ServerSessionAdapter.valueOf(session), decorator, registry);
        }
        performer.setStorageParameters(storageParameters);
        return performer;
    }

    private UpdatePerformer newUpdatePerformer(StorageParameters storageParameters) throws OXException {
        Session session = storageParameters.getSession();
        FolderServiceDecorator decorator = storageParameters.getDecorator();
        UpdatePerformer performer;
        if (null == session) {
            performer = new UpdatePerformer(storageParameters.getUser(), storageParameters.getContext(), decorator, registry);
        } else {
            performer = new UpdatePerformer(ServerSessionAdapter.valueOf(session), decorator, registry);
        }
        performer.setStorageParameters(storageParameters);
        return performer;
    }

    @Override
    public ContentType getDefaultContentType() {
        return null;
    }

    @Override
    public void commitTransaction(StorageParameters params) throws OXException {
        // Nothing to do
    }

    @Override
    public void createFolder(Folder folder, StorageParameters storageParameters) throws OXException {
        String treeId = folder.getTreeID();
        boolean created = false;
        Session session = storageParameters.getSession();
        int userId = storageParameters.getUserId();
        try {
            /*
             * Perform create operation via non-cache storage
             */
            CreatePerformer createPerformer = newCreatePerformer(storageParameters);
            createPerformer.setCheck4Duplicates(false);
            String folderId = createPerformer.doCreate(folder);
            created = true;
            /*
             * Get folder from appropriate storage
             */
            FolderStorage storage = registry.getFolderStorage(treeId, folderId);
            if (null == storage) {
                throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, folderId);
            }

            // Load created folder from real tree
            int contextId = storageParameters.getContextId();
            Folder createdFolder = null;
            try {
                createdFolder = loadFolder(realTreeId, folderId, StorageType.WORKING, true, storageParameters);
                if (createdFolder.isCacheable()) {
                    putFolder(createdFolder, realTreeId, storageParameters, true);
                }
            } catch (OXException e) {
                LOG.warn("Newly created folder could not be loaded from appropriate storage.", e);
            }

            // Remove parent from cache(s)
            FolderMapManagement folderMapManagement = FolderMapManagement.getInstance();
            Cache<FolderAndContextId> cache = globalCacheRef.get();
            String sContextId = Integer.toString(contextId);
            List<CacheKey> keys = new ArrayList<>(4);
            for (String tid : new String[] { treeId, realTreeId }) {
                // Cleanse parent from caches, too
                if (cache != null && Tools.isGlobalId(folder.getParentID()) && isCacheableTree(tid)) {
                    keys.add(newCacheKey(folder.getParentID(), tid, sContextId, cache));
                }
                folderMapManagement.dropFor(folder.getParentID(), tid, userId, contextId, session);
            }
            if (cache != null && !keys.isEmpty()) {
                cache.invalidate(keys);
            }

            if (null != createdFolder && !createdFolder.getParentID().equals(folder.getParentID())) {
                keys.clear();
                for (String tid : new String[] { treeId, realTreeId }) {
                    // Cleanse parent from caches, too
                    if (cache != null && Tools.isGlobalId(createdFolder.getParentID()) && isCacheableTree(tid)) {
                        keys.add(newCacheKey(createdFolder.getParentID(), tid, sContextId, cache));
                    }
                    folderMapManagement.dropFor(createdFolder.getParentID(), tid, userId, contextId, session);
                }
                if (cache != null && !keys.isEmpty()) {
                    cache.invalidate(keys);
                }
            }
            /*
             * Load parent from real tree
             */
            Folder parentFolder;
            Folder parentFromCache = getRefFromCache(treeId, folder.getParentID(), storageParameters, false);
            if (null != parentFromCache && null != parentFromCache.getSubfolderIDs()) {
                // Only update cache reference if sub-folders are contained
                parentFolder = loadFolder(realTreeId, folder.getParentID(), StorageType.WORKING, true, storageParameters);
                if (parentFolder.isCacheable()) {
                    putFolder(parentFolder, realTreeId, storageParameters, false);
                } else {
                    removeSingleFromCache(Collections.singletonList(folder.getParentID()), treeId, userId, session, false);
                }
            }
            if (null != createdFolder && !createdFolder.getParentID().equals(folder.getParentID())) {
                parentFromCache = getRefFromCache(treeId, createdFolder.getParentID(), storageParameters, false);
                if (null != parentFromCache && null != parentFromCache.getSubfolderIDs()) {
                    parentFolder = loadFolder(realTreeId, createdFolder.getParentID(), StorageType.WORKING, true, storageParameters);
                    if (parentFolder.isCacheable()) {
                        putFolder(parentFolder, realTreeId, storageParameters, false);
                    }
                } else {
                    removeSingleFromCache(Collections.singletonList(createdFolder.getParentID()), treeId, userId, session, false);
                }
            }
        } finally {
            if (!created) {
                removeSingleFromCache(Collections.singletonList(folder.getParentID()), treeId, userId, session, false);
            }
        }
    }

    /**
     * Puts specified folder into appropriate cache.
     *
     * @param folder The folder
     * @param treeId The tree identifier
     * @param storageParameters The storage parameters
     * @param putIfAbsent <code>true</code> to only put folder into (global) cache if absent; otherwise <code>false</code> for forced put possibly replacing an existent value
     * @throws OXException If put into cache fails
     */
    public void putFolder(Folder folder, String treeId, StorageParameters storageParameters, boolean putIfAbsent) throws OXException {
        if (!folder.isCacheable()) {
            return;
        }

        if (!folder.isGlobalID()) {
            // Put into non-global cache
            getFolderMapFor(storageParameters).put(treeId, folder, storageParameters.getSession());
            return;
        }

        if (!isCacheableTree(treeId)) {
            return;
        }

        Cache<FolderAndContextId> cache = globalCacheRef.get();
        if (cache != null) {
            CacheKey cacheKey = newCacheKey(folder.getID(), treeId, storageParameters.getContextId(), cache);
            if (putIfAbsent) {
                cache.putIfAbsent(cacheKey, new FolderAndContextId(folder, storageParameters.getContextId()));
            } else {
                cache.put(cacheKey, new FolderAndContextId(folder, storageParameters.getContextId()));
            }
        }
    }

    @Override
    public void invalidateSingle(String folderId, String treeId, Session session) throws OXException {
        removeFromCache(folderId, treeId, true, null, null, session);
    }

    @Override
    public void invalidate(String folderId, String treeId, boolean includeParents, Session session) throws OXException {
        removeFromCache(folderId, treeId, !includeParents, null, null, session);
    }

    /**
     * Removes specified folder and all of its predecessor folders from cache.
     *
     * @param id The folder identifier
     * @param treeId The tree identifier
     * @param singleOnly <code>true</code> if only specified folder should be removed; otherwise <code>false</code> for complete folder's
     *            path to root folder
     * @param session The session; never <code>null</code>
     * @throws OXException If removal fails
     */
    public void removeFromCache(String id, String treeId, boolean singleOnly, Session session) throws OXException {
        if (null != session) {
            removeFromCache(id, treeId, singleOnly, null, null, session, null);
        }
    }

    /**
     * Removes specified folder and all of its predecessor folders from cache.
     *
     * @param id The folder identifier
     * @param treeId The tree identifier
     * @param singleOnly <code>true</code> if only specified folder should be removed; otherwise <code>false</code> for complete folder's
     *            path to root folder
     * @param user The user or <code>null</code>, if a session is given
     * @param context The context or <code>null</code>, if a session is given
     * @param optSession The session or <code>null</code>, then user and context must be given
     * @throws OXException If removal fails
     */
    public void removeFromCache(String id, String treeId, boolean singleOnly, User user, Context context, Session optSession) throws OXException {
        removeFromCache(id, treeId, singleOnly, user, context, optSession, null);
    }

    /**
     * Removes specified folder and all of its predecessor folders from cache.
     *
     * @param id The folder identifier
     * @param treeId The tree identifier
     * @param singleOnly <code>true</code> if only specified folder should be removed; otherwise <code>false</code> for complete folder's
     *            path to root folder
     * @param user The user or <code>null</code>, if a session is given
     * @param context The context or <code>null</code>, if a session is given
     * @param optSession The session or <code>null</code>, then user and context must be given
     * @param folderPath The folderPath to <code>rootFolder</code>, if known
     * @throws OXException If removal fails
     */
    public void removeFromCache(String id, String treeId, boolean singleOnly, User user, Context context, Session optSession, List<String> folderPath) throws OXException {
        int userId;
        int contextId;
        if (optSession == null) {
            userId = user.getId();
            contextId = context.getContextId();
        } else {
            userId = optSession.getUserId();
            contextId = optSession.getContextId();
        }
        if (singleOnly) {
            removeSingleFromCache(Collections.singletonList(id), treeId, userId, contextId, true, null);
        } else {
            if (null != folderPath) {
                removeFromCache(id, treeId, userId, contextId, null, folderPath);
            } else {
                PathPerformer pathPerformer;
                if (optSession != null) {
                    pathPerformer = new PathPerformer(ServerSessionAdapter.valueOf(optSession), null, registry);
                } else {
                    pathPerformer = new PathPerformer(user, context, null, registry);
                }
                removeFromCache(id, treeId, userId, contextId, pathPerformer);
            }

        }
    }

    private void removeFromCache(String id, String treeId, int userId, int contextId, PathPerformer pathPerformer) throws OXException {
        removeFromCache(id, treeId, userId, contextId, pathPerformer, null);
    }

    private void removeFromCache(String id, String treeId, int userId, int contextId, PathPerformer pathPerformer, List<String> folderPath) throws OXException { // NOSONARLINT
        if (null == id) {
            return;
        }
        // at least one way to get paths should be provided
        if (null == pathPerformer && null == folderPath) {
            return;
        }
        // but not both
        if (null != pathPerformer && null != folderPath) {
            return;
        }
        List<String> ids;
        if (null != pathPerformer) {
            try {
                pathPerformer.getStorageParameters().setIgnoreCache(Boolean.TRUE);
                pathPerformer.getStorageParameters().putParameter(FolderType.GLOBAL, "DO_NOT_CACHE", Boolean.TRUE);
                if (existsFolder(treeId, id, StorageType.WORKING, pathPerformer.getStorageParameters())) {
                    UserizedFolder[] path = pathPerformer.doPath(treeId, id, true);
                    ids = new ArrayList<>(path.length);
                    for (UserizedFolder userizedFolder : path) {
                        ids.add(userizedFolder.getID());
                    }
                } else {
                    ids = Collections.singletonList(id);
                }
            } catch (Exception e) {
                org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(CacheFolderStorage.class);
                log.debug("", e);
                try {
                    ids = new ArrayList<>(Arrays.asList(pathPerformer.doForcePath(treeId, id)));
                } catch (Exception e1) {
                    log.debug("", e1);
                    ids = Collections.singletonList(id);
                }
            } finally {
                pathPerformer.getStorageParameters().setIgnoreCache(null);
            }
        } else {
            ids = folderPath;
        }
        Cache<FolderAndContextId> cache = globalCacheRef.get();
        FolderMapManagement folderMapManagement = FolderMapManagement.getInstance();
        if (realTreeId.equals(treeId)) {
            List<CacheKey> keys = new ArrayList<>(4);
            for (String folderId : ids) {
                if (cache != null && Tools.isGlobalId(folderId)) {
                    keys.add(newCacheKey(folderId, treeId, contextId, cache));
                }
                folderMapManagement.dropFor(folderId, treeId, userId, contextId);
            }
            if (cache != null && !keys.isEmpty()) {
                cache.invalidate(keys);
            }
        } else {
            List<CacheKey> keys = new LinkedList<>();
            for (String folderId : ids) {
                if (cache != null && Tools.isGlobalId(folderId)) {
                    if (isCacheableTree(treeId)) {
                        keys.add(newCacheKey(folderId, treeId, contextId, cache));
                    }
                    keys.add(newCacheKey(folderId, realTreeId, contextId, cache));
                }
                folderMapManagement.dropFor(folderId, treeId, userId, contextId);
                folderMapManagement.dropFor(folderId, realTreeId, userId, contextId);
            }
            if (cache != null && !keys.isEmpty()) {
                cache.invalidate(keys);
            }
        }
    }

    /**
     * Removes a single folder from cache.
     *
     * @param id The folder identifier
     * @param treeId The tree identifier
     * @param session The session
     */
    public void removeSingleFromCache(Collection<String> ids, String treeId, int userId, Session session, boolean deleted) {
        removeSingleFromCache(ids, treeId, userId, session.getContextId(), deleted, session);
    }

    /**
     * Removes a single folder from cache.
     *
     * @param ids The folder identifiers
     * @param treeId The tree identifier
     * @param contextId The context identifier
     */
    public void removeSingleFromCache(Collection<String> ids, String treeId, int optUserId, int contextId, boolean deleted, Session optSession) {
        removeSingleFromCache(ids, treeId, optUserId, contextId, deleted, false, optSession);
    }

    /**
     * Removes a single folder from cache.
     *
     * @param ids The folder identifiers
     * @param treeId The tree identifier
     * @param contextId The context identifier
     */
    public void removeSingleFromCache(Collection<String> ids, String treeId, int optUserId, int contextId, boolean deleted, boolean userCacheOnly, Session optSession) {
        try {
            // Perform for given folder tree and real tree
            Cache<FolderAndContextId> cache = userCacheOnly ? null : globalCacheRef.get();
            if (null == cache) {
                for (String tid : new HashSet<String>(Arrays.asList(treeId, realTreeId))) {
                    for (String id : ids) {
                        cleanseFromFolderManagement(optUserId, contextId, deleted, optSession, tid, id);
                    }
                }
                return;
            }

            // Cache present...
            String sContextId = Integer.toString(contextId);
            List<CacheKey> keys = new ArrayList<>(ids.size() << 1);
            for (String tid : new HashSet<String>(Arrays.asList(treeId, realTreeId))) {
                for (String id : ids) {
                    // Add affected cache keys
                    if (deleted) {
                        CacheKey cacheKey = Tools.isGlobalId(id) && isCacheableTree(tid) ? newCacheKey(id, tid, sContextId, cache) : null;
                        if (cacheKey != null) {
                            FolderAndContextId cachedFolder = cache.get(cacheKey);
                            if (null != cachedFolder) {
                                /*
                                 * Drop parent, too
                                 */
                                String parentID = cachedFolder.getFolder().getParentID();
                                if (Tools.isGlobalId(parentID)) {
                                    keys.add(newCacheKey(parentID, tid, sContextId, cache));
                                }
                            }
                            keys.add(cacheKey);
                        }
                    } else {
                        if (Tools.isGlobalId(id) && isCacheableTree(tid)) {
                            keys.add(newCacheKey(id, tid, sContextId, cache));
                        }
                    }
                    // Cleanse from folder management
                    cleanseFromFolderManagement(optUserId, contextId, deleted, optSession, tid, id);
                }
            }
            if (!keys.isEmpty()) {
                cache.invalidate(keys);
            }
        } catch (Exception x) {
            LOG.debug("", x);
        }
    }

    private static void cleanseFromFolderManagement(int optUserId, int contextId, boolean deleted, Session optSession, String tid, String id) {
        FolderMapManagement folderMapManagement = FolderMapManagement.getInstance();
        if (optUserId > 0) {
            FolderMap folderMap = folderMapManagement.optFor(optUserId, contextId);
            if (null != folderMap && deleted) {
                Folder cachedFolder = folderMap.get(id, tid, optSession);
                if (null != cachedFolder) {
                    /*
                     * Drop parent, too
                     */
                    String parentID = cachedFolder.getParentID();
                    if (null != parentID) {
                        folderMapManagement.dropFor(parentID, tid, optUserId, contextId, optSession);
                    }
                }
            }
        }
        folderMapManagement.dropFor(id, tid, optUserId, contextId, optSession);
    }

    @Override
    public void clearFolder(String treeId, String folderId, StorageParameters storageParameters) throws OXException {
        boolean cacheable;
        int contextId = storageParameters.getContextId();
        int userId = storageParameters.getUserId();
        Session session = storageParameters.getSession();
        String sContextId = Integer.toString(contextId);
        String[] subfolderIDs;
        { // NOSONARLINT
            Folder clearMe = getFolder(treeId, folderId, storageParameters);
            /*
             * Load all subfolders
             */
            subfolderIDs = loadAllSubfolders(treeId, clearMe, false, storageParameters);
            {
                FolderMapManagement folderMapManagement = FolderMapManagement.getInstance();
                folderMapManagement.dropFor(folderId, treeId, userId, contextId, session);
                folderMapManagement.dropFor(folderId, realTreeId, userId, contextId, session);
                folderMapManagement.dropFor(clearMe.getParentID(), treeId, userId, contextId, session);
                folderMapManagement.dropFor(clearMe.getParentID(), realTreeId, userId, contextId, session);
            }
            cacheable = clearMe.isCacheable();
        }
        if (cacheable) {
            /*
             * Delete from cache
             */
            if (Tools.isGlobalId(folderId)) {
                Cache<FolderAndContextId> cache = isCacheableTree(treeId) ? globalCacheRef.get() : null;
                if (cache != null) {
                    cache.invalidate(newCacheKey(folderId, treeId, sContextId, cache));
                }
            } else {
                FolderMapManagement.getInstance().dropFor(folderId, treeId, userId, contextId, session);
            }
        }
        /*
         * Drop subfolders from cache
         */
        removeSingleFromCache(Arrays.asList(subfolderIDs), treeId, userId, contextId, true, session);
        /*
         * Perform clear
         */
        if (null == session) {
            new ClearPerformer(storageParameters.getUser(), storageParameters.getContext(), registry).doClear(treeId, folderId);
        } else {
            new ClearPerformer(ServerSessionAdapter.valueOf(session), registry).doClear(treeId, folderId);
        }
        /*
         * Refresh
         */
        try {
            Folder clearedFolder = loadFolder(realTreeId, folderId, StorageType.WORKING, true, storageParameters);
            if (clearedFolder.isCacheable()) {
                putFolder(clearedFolder, realTreeId, storageParameters, true);
            }
        } catch (Exception e) {
            // Ignore
        }
    }

    @Override
    public void deleteFolder(String treeId, String folderId, StorageParameters storageParameters) throws OXException {
        String parentId;
        String realParentId;
        int contextId = storageParameters.getContextId();
        int userId = storageParameters.getUserId();
        Session session = storageParameters.getSession();
        String sContextId = Integer.toString(contextId);
        String[] subfolderIDs;
        Cache<FolderAndContextId> cache = globalCacheRef.get();
        {
            Folder deleteMe;
            try {
                deleteMe = getFolder(treeId, folderId, storageParameters);
            } catch (OXException e) {
                /*
                 * Obviously folder does not exist
                 */
                if (Tools.isGlobalId(folderId) && cache != null) {
                    cache.invalidate(newCacheKey(folderId, treeId, sContextId, cache));
                }
                FolderMapManagement.getInstance().dropFor(folderId, treeId, userId, contextId, session);
                return;
            }
            /*
             * Load all subfolders
             */
            try {
                subfolderIDs = loadAllSubfolders(treeId, deleteMe, false, storageParameters);
            } catch (Exception e) {
                LOG.warn("Failed to load subfolders of {} for user {} in context {}", folderId, I(userId), I(contextId), e);
                subfolderIDs = Strings.getEmptyStrings();
            }
            /*
             * Determine parent
             */
            parentId = deleteMe.getParentID();
            if (!realTreeId.equals(treeId)) {
                StorageParameters parameters = newStorageParameters(storageParameters); // NOSONARLINT
                FolderStorage folderStorage = registry.getFolderStorage(realTreeId, folderId);
                if (folderStorage == null) {
                    throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(realTreeId, folderId);
                }
                boolean started = folderStorage.startTransaction(parameters, false);
                try {
                    realParentId = folderStorage.getFolder(realTreeId, folderId, parameters).getParentID();
                    if (started) {
                        folderStorage.commitTransaction(parameters);
                        started = false;
                    }
                } catch (RuntimeException e) {
                    throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e);
                } finally {
                    if (started) {
                        folderStorage.rollback(parameters);
                    }
                }
            } else {
                realParentId = null;
            }
        }
        /*
         * Delete from cache
         */
        {
            FolderMapManagement folderMapManagement = FolderMapManagement.getInstance();
            folderMapManagement.dropFor(Arrays.asList(folderId, parentId), treeId, userId, contextId, session);
            if (!treeId.equals(realTreeId)) {
                List<String> fids = new ArrayList<>(Arrays.asList(folderId, parentId));
                if (null != realParentId) {
                    fids.add(realParentId);
                }
                folderMapManagement.dropFor(fids, realTreeId, userId, contextId, session);
            }
        }
        if (cache != null) {
            List<CacheKey> keys = new ArrayList<>(4);
            if (Tools.isGlobalId(folderId)) {
                keys.add(newCacheKey(folderId, treeId, sContextId, cache));
            }
            if (Tools.isGlobalId(parentId)) {
                keys.add(newCacheKey(parentId, treeId, sContextId, cache));
            }
            if (null != realParentId && !realParentId.equals(parentId)) {
                if (Tools.isGlobalId(realParentId)) { // NOSONARLINT
                    keys.add(newCacheKey(realParentId, realTreeId, sContextId, cache));
                }
            }
            if (!keys.isEmpty()) {
                cache.invalidate(keys);
            }
        }
        registry.clearCaches(storageParameters.getUserId(), storageParameters.getContextId());
        /*
         * Drop subfolders from cache
         */
        removeSingleFromCache(Arrays.asList(subfolderIDs), treeId, userId, contextId, true, session);
        /*
         * Perform delete
         */
        newDeletePerformer(storageParameters).doDelete(
            treeId,
            folderId,
            storageParameters.getTimeStamp());
        /*
         * Refresh
         */
        if (null != realParentId && !ROOT_ID.equals(realParentId)) {
            if (session == null) {
                removeFromCache(realParentId, treeId, storageParameters.getUserId(), storageParameters.getContextId(), newPathPerformer(storageParameters));
            } else {
                removeFromCache(realParentId, treeId, session.getUserId(), session.getContextId(), newPathPerformer(storageParameters));
            }
        }
        if (!ROOT_ID.equals(parentId)) {
            if (session == null) {
                removeFromCache(parentId, treeId, storageParameters.getUserId(), storageParameters.getContextId(), newPathPerformer(storageParameters));
            } else {
                removeFromCache(parentId, treeId, session.getUserId(), session.getContextId(), newPathPerformer(storageParameters));
            }
        }
    }

    @Override
    public TrashResult trashFolder(String treeId, String folderId, StorageParameters storageParameters) throws OXException {
        String parentId;
        String realParentId;
        int contextId = storageParameters.getContextId();
        int userId = storageParameters.getUserId();
        Session session = storageParameters.getSession();
        String sContextId = Integer.toString(contextId);
        String[] subfolderIDs;
        Cache<FolderAndContextId> cache = globalCacheRef.get();
        {
            Folder deleteMe;
            try {
                deleteMe = getFolder(treeId, folderId, storageParameters);
                /*
                 * Load all subfolders
                 */
                subfolderIDs = loadAllSubfolders(treeId, deleteMe, false, storageParameters);
            } catch (OXException e) {
                /*
                 * Obviously folder does not exist
                 */
                if ((cache != null) && Tools.isGlobalId(folderId) && isCacheableTree(treeId)) {
                    cache.invalidate(newCacheKey(folderId, treeId, sContextId, cache));
                }
                FolderMapManagement.getInstance().dropFor(folderId, treeId, userId, contextId, session);
                return new TrashResult(null, folderId);
            }
            parentId = deleteMe.getParentID();
            if (!realTreeId.equals(treeId)) {
                StorageParameters parameters = newStorageParameters(storageParameters);
                FolderStorage folderStorage = registry.getFolderStorage(realTreeId, folderId);
                if (null == folderStorage) {
                    throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(realTreeId, folderId);
                }
                boolean started = folderStorage.startTransaction(parameters, false);
                try {
                    realParentId = folderStorage.getFolder(realTreeId, folderId, parameters).getParentID();
                    if (started) {
                        folderStorage.commitTransaction(parameters);
                        started = false;
                    }
                } catch (RuntimeException e) {
                    throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e);
                } finally {
                    if (started) {
                        folderStorage.rollback(parameters);
                    }
                }
            } else {
                realParentId = null;
            }
        }
        /*
         * Delete from cache
         */
        {
            FolderMapManagement folderMapManagement = FolderMapManagement.getInstance();
            folderMapManagement.dropFor(Arrays.asList(folderId, parentId), treeId, userId, contextId, session);
            if (!treeId.equals(realTreeId)) {
                List<String> fids = new ArrayList<>(Arrays.asList(folderId, parentId));
                if (null != realParentId) {
                    fids.add(realParentId);
                }
                folderMapManagement.dropFor(fids, realTreeId, userId, contextId, session);
            }
        }
        if (cache != null) {
            List<CacheKey> keys = new ArrayList<>(4);
            if (Tools.isGlobalId(folderId) && isCacheableTree(treeId)) {
                keys.add(newCacheKey(folderId, treeId, sContextId, cache));
            }
            if (Tools.isGlobalId(parentId) && isCacheableTree(treeId)) {
                keys.add(newCacheKey(parentId, treeId, sContextId, cache));
            }
            if (null != realParentId && !realParentId.equals(parentId)) {
                if (Tools.isGlobalId(realParentId)) { // NOSONARLINT
                    keys.add(newCacheKey(realParentId, realTreeId, sContextId, cache));
                }
            }
            if (!keys.isEmpty()) {
                cache.invalidate(keys);
            }
        }
        registry.clearCaches(storageParameters.getUserId(), storageParameters.getContextId());
        /*
         * Drop subfolders from cache
         */
        removeSingleFromCache(Arrays.asList(subfolderIDs), treeId, userId, contextId, true, session);
        /*
         * Perform delete
         */
        TrashResult trashResult = newDeletePerformer(storageParameters).doTrash(treeId, folderId, storageParameters.getTimeStamp());
        /*
         * Refresh
         */
        if (null != realParentId && !ROOT_ID.equals(realParentId)) {
            if (session == null) {
                removeFromCache(realParentId, treeId, storageParameters.getUserId(), storageParameters.getContextId(), newPathPerformer(storageParameters));
            } else {
                removeFromCache(realParentId, treeId, session.getUserId(), session.getContextId(), newPathPerformer(storageParameters));
            }
        }
        if (!ROOT_ID.equals(parentId)) {
            if (session == null) {
                removeFromCache(parentId, treeId, storageParameters.getUserId(), storageParameters.getContextId(), newPathPerformer(storageParameters));
            } else {
                removeFromCache(parentId, treeId, session.getUserId(), session.getContextId(), newPathPerformer(storageParameters));
            }
        }
        return trashResult;
    }

    @Override
    public String getDefaultFolderID(User user, String treeId, ContentType contentType, Type type, StorageParameters storageParameters) throws OXException {
        FolderStorage storage = registry.getFolderStorageByContentType(treeId, contentType);
        if (null == storage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_CT.create(treeId, contentType);
        }
        String folderId;
        boolean started = storage.startTransaction(storageParameters, false);
        try {
            folderId = storage.getDefaultFolderID(user, treeId, contentType, type, storageParameters);
            if (started) {
                storage.commitTransaction(storageParameters);
                started = false;
            }
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (started) {
                storage.rollback(storageParameters);
            }
        }
        return folderId;
    }

    @Override
    public Type getTypeByParent(User user, String treeId, String parentId, StorageParameters storageParameters) throws OXException {
        FolderStorage storage = registry.getFolderStorage(treeId, parentId);
        if (null == storage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, parentId);
        }
        Type type;
        boolean started = storage.startTransaction(storageParameters, false);
        try {
            type = storage.getTypeByParent(user, treeId, parentId, storageParameters);
            if (started) {
                storage.commitTransaction(storageParameters);
                started = false;
            }
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (started) {
                storage.rollback(storageParameters);
            }
        }
        return type;
    }

    @Override
    public boolean containsForeignObjects(User user, String treeId, String folderId, StorageParameters storageParameters) throws OXException {
        /*
         * Get folder storage
         */
        FolderStorage storage = registry.getFolderStorage(treeId, folderId);
        if (null == storage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, folderId);
        }
        boolean started = storage.startTransaction(storageParameters, false);
        try {
            boolean containsForeignObjects = storage.containsForeignObjects(user, treeId, folderId, storageParameters);
            if (started) {
                storage.commitTransaction(storageParameters);
                started = false;
            }
            return containsForeignObjects;
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (started) {
                storage.rollback(storageParameters);
            }
        }
    }

    @Override
    public boolean isEmpty(String treeId, String folderId, StorageParameters storageParameters) throws OXException {
        /*
         * Get folder storage
         */
        FolderStorage storage = registry.getFolderStorage(treeId, folderId);
        if (null == storage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, folderId);
        }
        boolean started = storage.startTransaction(storageParameters, false);
        try {
            boolean isEmpty = storage.isEmpty(treeId, folderId, storageParameters);
            if (started) {
                storage.commitTransaction(storageParameters);
                started = false;
            }
            return isEmpty;
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (started) {
                storage.rollback(storageParameters);
            }
        }
    }

    @Override
    public void updateLastModified(long lastModified, String treeId, String folderId, StorageParameters storageParameters) throws OXException {
        FolderStorage storage = registry.getFolderStorage(treeId, folderId);
        if (null == storage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, folderId);
        }
        boolean started = storage.startTransaction(storageParameters, false);
        try {
            storage.updateLastModified(lastModified, treeId, folderId, storageParameters);
            if (started) {
                storage.commitTransaction(storageParameters);
                started = false;
            }
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (started) {
                storage.rollback(storageParameters);
            }
        }
        /*
         * Invalidate cache entry
         */
        removeFromCache(folderId, treeId, storageParameters.getUserId(), storageParameters.getContextId(), newPathPerformer(storageParameters));
    }

    @Override
    public Folder getFolder(String treeId, String folderId, StorageParameters storageParameters) throws OXException {
        return getFolder(treeId, folderId, StorageType.WORKING, storageParameters);
    }

    @Override
    public List<Folder> getFolders(String treeId, List<String> folderIds, StorageParameters storageParameters) throws OXException {
        return getFolders(treeId, folderIds, StorageType.WORKING, storageParameters);
    }

    @Override
    public Folder getFolder(String treeId, String folderId, StorageType storageType, StorageParameters storageParameters) throws OXException {
        /*
         * Try from cache
         */
        Folder folder = getCloneFromCache(treeId, folderId, storageParameters);
        if (null != folder) {
            return folder;
        }
        /*
         * Load folder from appropriate storage
         */
        folder = loadFolder(treeId, folderId, storageType, storageParameters);
        /*
         * Check if folder is cacheable
         */
        if (folder.isCacheable()) {
            /*
             * Put to cache and return a cloned version
             */
            putFolder(folder, treeId, storageParameters, true);
            return (Folder) folder.clone();
        }
        /*
         * Return as-is since not cached
         */
        return folder;
    }

    private Folder getCloneFromCache(String treeId, String folderId, StorageParameters storageParameters) throws OXException {
        return getRefFromCache(treeId, folderId, storageParameters, true);
    }

    /**
     * Gets the folder reference from cache.
     *
     * @param treeId The tree identifier
     * @param folderId The folder identifier
     * @param params The storage parameters
     * @param cloneIfRequired <code>true</code> to clone folder instance (if required); otherwise <code>false</code>
     * @return The folder or <code>null</code> on cache miss
     * @throws OXException
     */
    private Folder getRefFromCache(String treeId, String folderId, StorageParameters params, boolean cloneIfRequired) throws OXException {
        if (folderId == null) {
            LOG.info("Unable to retrieve folder reference from cache as the folder identifier is 'null'.");
            return null;
        }
        int contextId = params.getContextId();
        if (null != params.getUser() && params.getUser().isAnonymousGuest()) {
            LOG.debug("Bypassing cache for folder {} from context {} for user {}", folderId, I(contextId), I(params.getUserId()));
            return null; // bypass cache for anonymous users
        }
        /*
         * Try global cache key
         */
        if (folderId.length() <= 0 || Strings.isDigit(folderId.charAt(0))) {
            Cache<FolderAndContextId> cache = globalCacheRef.get();
            if (cache != null && isCacheableTree(treeId)) {
                FolderAndContextId fromCache = cache.get(newCacheKey(folderId, treeId, contextId, cache));
                if (null != fromCache) {
                    /*
                     * Return version from global cache; no need to clone
                     */
                    return fromCache.getFolder();
                }
            }
        }
        /*
         * Try user cache key
         */
        FolderMap folderMap = optFolderMapFor(params);
        if (null != folderMap) {
            Folder folder = folderMap.get(folderId, treeId, params.getSession());
            if (null != folder) {
                /*
                 * Return version from user-bound cache
                 */
                if (folder instanceof RemoveAfterAccessFolder) {
                    RemoveAfterAccessFolder raaf = (RemoveAfterAccessFolder) folder;
                    int fUserId = raaf.getUserId();
                    int fContextId = raaf.getContextId();
                    if ((fUserId >= 0 && params.getUserId() != fUserId) || (fContextId >= 0 && params.getContextId() != fContextId)) {
                        return null;
                    }
                }
                LOG.debug("Locally loaded folder {} from context {} for user {}", folderId, I(contextId), I(params.getUserId()));
                if (cloneIfRequired) {
                    folder = (Folder) folder.clone();
                }
                return folder;
            }
        }
        /*
         * Cache miss
         */
        return null;
    }

    @Override
    public List<Folder> getFolders(String treeId, List<String> folderIds, StorageType storageType, StorageParameters storageParameters) throws OXException {
        int size = folderIds.size();
        Folder[] ret = new Folder[size];
        TObjectIntMap<String> toLoad = new TObjectIntHashMap<String>(size);
        /*
         * Get the ones from cache
         */
        for (int i = 0; i < size; i++) {
            /*
             * Try from cache
             */
            String folderId = folderIds.get(i);
            Folder folder = getCloneFromCache(treeId, folderId, storageParameters);
            if (null == folder) {
                /*
                 * Cache miss; Load from storage
                 */
                toLoad.put(folderId, i);
            } else {
                /*
                 * Cache hit
                 */
                ret[i] = folder;
            }
        }
        /*
         * Load the ones from storage
         */
        if (!toLoad.isEmpty()) {
            Map<String, Folder> fromStorage = loadFolders(
                treeId,
                Arrays.asList(toLoad.keys(new String[toLoad.size()])),
                storageType,
                storageParameters);
            /*
             * Fill return value
             */
            for (Map.Entry<String, Folder> entry : fromStorage.entrySet()) {
                Folder folder = entry.getValue();
                int index = toLoad.get(entry.getKey());
                /*
                 * Put into cache
                 */
                if (folder.isCacheable()) {
                    /*
                     * Put to cache and create a cloned version
                     */
                    putFolder(folder, treeId, storageParameters, true);
                    folder = (Folder) folder.clone();
                }
                ret[index] = folder;
            }
        }
        /*
         * Return
         */
        List<Folder> l = new ArrayList<Folder>(ret.length);
        for (Folder folder : ret) {
            if (null != folder) {
                l.add(folder);
            }
        }
        return l;
    }

    @Override
    public FolderType getFolderType() {
        return CacheFolderType.getInstance();
    }

    @Override
    public StoragePriority getStoragePriority() {
        return StoragePriority.HIGHEST;
    }

    @Override
    public SortableId[] getVisibleFolders(String treeId, ContentType contentType, Type type, StorageParameters storageParameters) throws OXException {
        FolderStorage folderStorage = registry.getFolderStorageByContentType(treeId, contentType);
        if (null == folderStorage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_CT.create(treeId, contentType);
        }
        boolean started = startTransaction(Mode.WRITE_AFTER_READ, storageParameters, folderStorage);
        try {
            SortableId[] ret = folderStorage.getVisibleFolders(treeId, contentType, type, storageParameters);
            if (started) {
                folderStorage.commitTransaction(storageParameters);
                started = false;
            }
            return ret;
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (started) {
                folderStorage.rollback(storageParameters);
            }
        }
    }

    @Override
    public SortableId[] getVisibleFolders(String rootFolderId, String treeId, ContentType contentType, Type type, StorageParameters storageParameters) throws OXException {
        FolderStorage folderStorage = registry.getFolderStorageByContentType(treeId, contentType);
        if (null == folderStorage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_CT.create(treeId, contentType);
        }
        boolean started = startTransaction(Mode.WRITE_AFTER_READ, storageParameters, folderStorage);
        try {
            SortableId[] ret = folderStorage.getVisibleFolders(rootFolderId, treeId, contentType, type, storageParameters);
            if (started) {
                folderStorage.commitTransaction(storageParameters);
                started = false;
            }
            return ret;
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (started) {
                folderStorage.rollback(storageParameters);
            }
        }
    }

    @Override
    public SortableId[] getUserSharedFolders(String treeId, ContentType contentType, StorageParameters storageParameters) throws OXException {
        FolderStorage folderStorage = registry.getFolderStorageByContentType(treeId, contentType);
        if (null == folderStorage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_CT.create(treeId, contentType);
        }
        boolean started = startTransaction(Mode.WRITE_AFTER_READ, storageParameters, folderStorage);
        try {
            SortableId[] ret = folderStorage.getUserSharedFolders(treeId, contentType, storageParameters);
            if (started) {
                folderStorage.commitTransaction(storageParameters);
                started = false;
            }
            return ret;
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (started) {
                folderStorage.rollback(storageParameters);
            }
        }
    }

    @Override
    public Folder[] getSubfolderObjects(String treeId, String parentId, StorageParameters storageParameters) throws OXException {
        if (ROOT_ID.equals(parentId)) {
            // Cannot be served
            return null; // NOSONARLINT
        }

        FolderStorage[] neededStorages = getFolderStoragesForParent(treeId, parentId, storageParameters);
        if (0 == neededStorages.length) {
            return new Folder[0];
        }

        if (neededStorages.length > 1) {
            // Cannot be served from one storage
            return null; // NOSONARLINT
        }

        FolderStorage neededStorage = neededStorages[0];
        if (!(neededStorage instanceof SubfolderListingFolderStorage)) {
            // Cannot be delegated
            return null; // NOSONARLINT
        }

        SubfolderListingFolderStorage listingStorage = (SubfolderListingFolderStorage) neededStorage;
        boolean started = neededStorage.startTransaction(storageParameters, false);
        try {
            Folder[] folders = listingStorage.getSubfolderObjects(treeId, parentId, storageParameters);
            if (started) {
                neededStorage.commitTransaction(storageParameters);
                started = false;
            }
            return folders;
        } finally {
            if (started) {
                neededStorage.rollback(storageParameters);
            }
        }
    }


    @Override
    public SortableId[] getSubfolders(final String treeId, final String parentId, final StorageParameters storageParameters) throws OXException { // NOSONARLINT
        Folder parent = getFolder(treeId, parentId, storageParameters);
        String[] subfolders = ROOT_ID.equals(parentId) ? null : parent.getSubfolderIDs();
        if (null != subfolders) {
            SortableId[] ret = new SortableId[subfolders.length];
            for (int i = 0; i < ret.length; i++) {
                ret[i] = new CacheSortableId(subfolders[i], i, null);
            }
            return ret;
        }

        // Get needed storages
        FolderStorage[] neededStorages = getFolderStoragesForParent(treeId, parentId, storageParameters);
        if (0 == neededStorages.length) {
            return new SortableId[0];
        }

        try {
            List<SortableId> allSubfolderIds;
            if (1 == neededStorages.length) {
                FolderStorage neededStorage = neededStorages[0];
                boolean started = neededStorage.startTransaction(storageParameters, false);
                try {
                    allSubfolderIds = Arrays.asList(neededStorage.getSubfolders(treeId, parentId, storageParameters));
                    if (started) {
                        neededStorage.commitTransaction(storageParameters);
                        started = false;
                    }
                } finally {
                    if (started) {
                        neededStorage.rollback(storageParameters);
                    }
                }
            } else {
                allSubfolderIds = new LinkedList<SortableId>();

                // Query storages (except first one) using dedicated threads
                ThreadPoolService tps = CacheServiceRegistry.getServiceRegistry().getService(ThreadPoolService.class);
                CompletionService<List<SortableId>> completionService = null == tps ? new CallerRunsCompletionService<>() : new ThreadPoolCompletionService<java.util.List<SortableId>>(tps).setTrackable(true);
                int submittedTasks = 0;
                for (int i = 1; i < neededStorages.length; i++) {
                    final FolderStorage neededStorage = neededStorages[i];
                    completionService.submit(new TrackableCallable<List<SortableId>>() {

                        @Override
                        public List<SortableId> call() throws Exception {
                            StorageParameters newParameters = newStorageParameters(storageParameters);
                            newParameters.setDecorator(storageParameters.getDecorator());
                            boolean started = neededStorage.startTransaction(newParameters, false);
                            try {
                                List<SortableId> l = Arrays.asList(neededStorage.getSubfolders(treeId, parentId, newParameters));
                                if (started) {
                                    neededStorage.commitTransaction(newParameters);
                                    started = false;
                                }
                                return l;
                            } finally {
                                if (started) {
                                    neededStorage.rollback(newParameters);
                                }
                            }
                        }
                    });
                    submittedTasks++;
                }

                // Query the first one with this thread
                {
                    FolderStorage neededStorage = neededStorages[0];
                    boolean started = neededStorage.startTransaction(storageParameters, false);
                    try {
                        List<SortableId> l = Arrays.asList(neededStorage.getSubfolders(treeId, parentId, storageParameters));
                        if (started) {
                            neededStorage.commitTransaction(storageParameters);
                            started = false;
                        }
                        allSubfolderIds.addAll(l);
                    } finally {
                        if (started) {
                            neededStorage.rollback(storageParameters);
                        }
                    }
                }

                // Wait for completion
                List<List<SortableId>> results = ThreadPools.takeCompletionService(completionService, submittedTasks, FACTORY);
                for (List<SortableId> result : results) {
                    allSubfolderIds.addAll(result);
                }
            }

            // Sort them
            Collections.sort(allSubfolderIds);
            return allSubfolderIds.toArray(new SortableId[allSubfolderIds.size()]);
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public ContentType[] getSupportedContentTypes() {
        return new ContentType[0];
    }

    @Override
    public void rollback(StorageParameters params) {
        // Nothing to do
    }

    @Override
    public boolean startTransaction(StorageParameters parameters, boolean modify) throws OXException {
        return false;
    }

    @Override
    public void updateFolder(Folder folder, StorageParameters storageParameters) throws OXException { // NOSONARLINT
        String treeId = folder.getTreeID();
        /*
         * Perform update operation via non-cache storage
         */
        String oldFolderId = folder.getID();
        Folder storageVersion = getCloneFromCache(treeId, oldFolderId, storageParameters);
        if (null == storageVersion) {
            storageVersion = getFolder(treeId, oldFolderId, storageParameters);
        }
        String oldParentId = storageVersion.getParentID();
        if (oldParentId == null){
            throw OXFolderExceptionCode.UNKNOWN_EXCEPTION.create("Missing old parent id for folder " + oldFolderId + " in tree " + treeId);
        }
        boolean isMove = null != folder.getParentID();
        { // NOSONARLINT
            UpdatePerformer updatePerformer = newUpdatePerformer(storageParameters);
            updatePerformer.setCheck4Duplicates(false);
            updatePerformer.setIncreaseObjectUseCount(false);
            updatePerformer.setCollectFolderMoveWarnings(false);
            updatePerformer.doUpdate(folder, storageParameters.getTimeStamp());

            Set<OXException> warnings = updatePerformer.getWarnings();
            if (null != warnings) {
                for (OXException warning : warnings) {
                    storageParameters.addWarning(warning);
                }
            }
        }
        /*
         * Get folder from appropriate storage
         */
        String newFolderId = folder.getID();
        if (null == newFolderId) {
            // cancel cache invalidations if no folder identifier set
            return;
        }
        /*
         * Refresh/Invalidate folder
         */
        Folder updatedFolder = null;
        if (storageVersion.isCacheable()) {
            updatedFolder = loadFolder(treeId, newFolderId, StorageType.WORKING, true, storageParameters);
            int userId = storageParameters.getUserId();
            int contextId = storageParameters.getContextId();
            {
                Collection<String> ids = isMove ? ImmutableSet.of(oldFolderId, oldParentId, updatedFolder.getParentID()) : ImmutableSet.of(oldFolderId, oldParentId); // NOSONARLINT
                FolderMapManagement folderMapManagement = FolderMapManagement.getInstance();
                folderMapManagement.dropHierarchyFor(ids, treeId, userId, contextId);
                if (!treeId.equals(realTreeId)) {
                    folderMapManagement.dropHierarchyFor(ids, realTreeId, userId, contextId);
                }
            }
        }
        registry.clearCaches(storageParameters.getUserId(), storageParameters.getContextId());
        /*
         * Put updated folder
         */
        if (isMove) {
            /*-
             * Do not reload folders.
             *
             * In case of a cross file storage move (e.g. Dropbox to InfoStore), the previously opened in-transaction connection will not
             * see the newly created folder (as not yet committed) or read a stale state.
             */
        } else {
            Folder f;
            if (null != updatedFolder && treeId.equals(realTreeId)) {
                f = updatedFolder;
            } else {
                f = loadFolder(realTreeId, newFolderId, StorageType.WORKING, true, storageParameters);
                int userId = storageParameters.getUserId();
                int contextId = storageParameters.getContextId();
                Collection<String> ids = Set.of(oldFolderId, oldParentId);
                FolderMapManagement.getInstance().dropHierarchyFor(ids, realTreeId, userId, contextId);
            }
            if (f.isCacheable()) {
                putFolder(f, realTreeId, storageParameters, false);
            }
        }
        if (null != updatedFolder && updatedFolder.isCacheable()) {
            putFolder(updatedFolder, treeId, storageParameters, false);
        }
    }

    @Override
    public boolean containsFolder(String treeId, String folderId, StorageParameters storageParameters) throws OXException {
        return containsFolder(treeId, folderId, StorageType.WORKING, storageParameters);
    }

    @Override
    public String[] getModifiedFolderIDs(String treeId, Date timeStamp, ContentType[] includeContentTypes, StorageParameters storageParameters) throws OXException {
        return getChangedFolderIDs(0, treeId, timeStamp, includeContentTypes, storageParameters);
    }

    @Override
    public String[] getDeletedFolderIDs(String treeId, Date timeStamp, StorageParameters storageParameters) throws OXException {
        return getChangedFolderIDs(1, treeId, timeStamp, null, storageParameters);
    }

    private String[] getChangedFolderIDs(int index, String treeId, Date timeStamp, ContentType[] includeContentTypes, StorageParameters storageParameters) throws OXException {
        Session session = storageParameters.getSession();
        /*
         * Perform update operation via non-cache storage
         */
        UserizedFolder[] folders;
        boolean ignoreDelete = index == 0;
        if (null == session) {
            folders = new UpdatesPerformer(
                storageParameters.getUser(),
                storageParameters.getContext(),
                storageParameters.getDecorator(),
                registry).doUpdates(treeId, timeStamp, ignoreDelete, includeContentTypes)[index];
        } else {
            folders = new UpdatesPerformer(ServerSessionAdapter.valueOf(session), storageParameters.getDecorator(), registry).doUpdates(
                treeId,
                timeStamp,
                ignoreDelete,
                includeContentTypes)[index];
        }
        if (null == folders || folders.length == 0) {
            return Strings.getEmptyStrings();
        }
        String[] ids = new String[folders.length];
        for (int i = 0; i < ids.length; i++) {
            ids[i] = folders[i].getID();
        }
        return ids;
    }

    @Override
    public boolean containsFolder(String treeId, String folderId, StorageType storageType, StorageParameters storageParameters) throws OXException {
        FolderStorage storage = registry.getFolderStorage(treeId, folderId);
        if (null == storage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, folderId);
        }
        boolean started = startTransaction(Mode.WRITE_AFTER_READ, storageParameters, storage);
        try {
            boolean contains = storage.containsFolder(treeId, folderId, storageType, storageParameters);
            if (started) {
                storage.commitTransaction(storageParameters);
                started = false;
            }
            return contains;
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (started) {
                storage.rollback(storageParameters);
            }
        }
    }

    @Override
    public Map<String, String> restoreFromTrash(String treeId, List<String> folderIds, String defaultDestFolderId, StorageParameters storageParameters) throws OXException {
        FolderStorage folderStorage = registry.getFolderStorage(treeId, defaultDestFolderId);
        if (null == folderStorage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, defaultDestFolderId);
        }

        if (!(folderStorage instanceof RestoringFolderStorage)) {
            throw FolderExceptionErrorMessage.NO_RESTORE_SUPPORT.create();
        }

        for (String folderId : folderIds) {
            FolderStorage storage = registry.getFolderStorage(treeId, folderId);
            if (null == storage) {
                throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, defaultDestFolderId);
            }

            if (!folderStorage.equals(storage)) {
                throw FolderExceptionErrorMessage.INVALID_FOLDER_ID.create(folderId);
            }
        }

        RestoringFolderStorage restoringFolderStorage = (RestoringFolderStorage) folderStorage;
        return restoringFolderStorage.restoreFromTrash(treeId, folderIds, defaultDestFolderId, storageParameters);
    }

    @Override
    public List<Folder> searchFileStorageFolders(String treeId, String rootFolderId, String query, long date, boolean includeSubfolders, int start, int end, StorageParameters storageParameters) throws OXException { // NOSONARLINT
        FolderStorage[] folderStorages = getFolderStoragesForParent(treeId, rootFolderId, storageParameters);
        if (0 == folderStorages.length) {
            return Collections.emptyList();
        }

        List<SearchableFileFolderNameFolderStorage> neededStorages = Arrays.stream(folderStorages).filter(SearchableFileFolderNameFolderStorage.class::isInstance).map(s -> (SearchableFileFolderNameFolderStorage) s).toList();
        if (null == neededStorages || neededStorages.isEmpty()) {
            return Collections.emptyList();
        }

        if (1 == neededStorages.size()) {
            // Search in single storage
            SearchableFileFolderNameFolderStorage neededStorage = neededStorages.get(0);
            boolean started = neededStorage.startTransaction(storageParameters, false);
            try {
                List<Folder> folders = neededStorage.searchFileStorageFolders(treeId, rootFolderId, query, date, includeSubfolders, start, end, storageParameters);
                if (started) {
                    neededStorage.commitTransaction(storageParameters);
                    started = false;
                }
                return folders;
            } finally {
                if (started) {
                    neededStorage.rollback(storageParameters);
                }
            }
        }

        // Search in multiple storages
        // Query storages (except first one) using dedicated threads
        ThreadPoolService tps = CacheServiceRegistry.getServiceRegistry().getService(ThreadPoolService.class);
        CompletionService<List<Folder>> completionService = null == tps ? new CallerRunsCompletionService<>() : new ThreadPoolCompletionService<List<Folder>>(tps).setTrackable(true);
        int submittedTasks = 0;
        for (int i = 1; i < neededStorages.size(); i++) {
            final SearchableFileFolderNameFolderStorage neededStorage = neededStorages.get(i);
            completionService.submit(new TrackableCallable<List<Folder>>() {

                @Override
                public List<Folder> call() throws Exception {
                    StorageParameters newParameters = newStorageParameters(storageParameters);
                    newParameters.setDecorator(storageParameters.getDecorator());
                    boolean started = neededStorage.startTransaction(newParameters, false);
                    try {
                        // Search for subfolders. "start" explicitly set to 0 (zero) since sort & slice happens when all folder storages are considered
                        List<Folder> l = neededStorage.searchFileStorageFolders(treeId, rootFolderId, query, date, includeSubfolders, 0, end, newParameters);
                        if (started) {
                            neededStorage.commitTransaction(newParameters);
                            started = false;
                        }
                        return l;
                    } finally {
                        if (started) {
                            neededStorage.rollback(newParameters);
                        }
                    }
                }
            });
            submittedTasks++;
        }

        // Query the first one with this thread
        List<Folder> allFolders = new LinkedList<>();
        { // NOSONARLINT
            SearchableFileFolderNameFolderStorage neededStorage = neededStorages.get(0);
            boolean started = neededStorage.startTransaction(storageParameters, false);
            try {
                // Search for subfolders. "start" explicitly set to 0 (zero) since sort & slice happens when all folder storages are considered
                List<Folder> l = neededStorage.searchFileStorageFolders(treeId, rootFolderId, query, date, includeSubfolders, 0, end, storageParameters);
                if (started) {
                    neededStorage.commitTransaction(storageParameters);
                    started = false;
                }
                allFolders.addAll(l);
            } finally {
                if (started) {
                    neededStorage.rollback(storageParameters);
                }
            }
        }

        // Wait for completion
        List<List<Folder>> results = ThreadPools.takeCompletionService(completionService, submittedTasks, FACTORY);
        for (List<Folder> result : results) {
            allFolders.addAll(result);
        }

        // Sort
        Locale locale = storageParameters.getDecorator().getLocale();
        Collections.sort(allFolders, new FolderComparator(locale == null ? storageParameters.getUser().getLocale() : locale));

        // Slice
        if (start >= allFolders.size()) {
            return Collections.emptyList();
        }
        int toIndex = end;
        if (toIndex >= allFolders.size()) {
            if (start == 0) {
                return allFolders;
            }
            toIndex = allFolders.size();
        }
        return allFolders.subList(start, toIndex);
    }

    /*-
     * ++++++++++++++++++++++++++++++++++++ ++ + HELPERS + ++ ++++++++++++++++++++++++++++++++++++ // NOSONARLINT
     */

    private static boolean startTransaction(Mode mode, StorageParameters storageParameters, FolderStorage storage) throws OXException {
        if (storage instanceof AfterReadAwareFolderStorage) {
            return ((AfterReadAwareFolderStorage) storage).startTransaction(storageParameters, mode);
        }
        return storage.startTransaction(storageParameters, Mode.READ != mode);
    }

    /**
     * Creates the cache key for specified arguments.
     *
     * @param folderId The folder identifier
     * @param treeId The tree identifier
     * @param contextId The context identifier used as group
     * @param cache The cache to use for key creation
     * @return The cache key
     * @throws OXException If cache service is absent
     */
    private static CacheKey newCacheKey(String folderId, String treeId, int contextId, Cache<FolderAndContextId> cache) throws OXException {
        return newCacheKey(folderId, treeId, Integer.toString(contextId), cache);
    }

    /**
     * Creates the cache key for specified arguments.
     *
     * @param folderId The folder identifier
     * @param treeId The tree identifier
     * @param sContextId The context identifier used as group
     * @param cache The cache to use for key creation
     * @return The cache key
     * @throws OXException If cache service is absent
     */
    private static CacheKey newCacheKey(String folderId, String treeId, String sContextId, Cache<FolderAndContextId> cache) throws OXException {
        if (null == cache) {
            throw ServiceExceptionCode.absentService(CacheService.class);
        }
        return cache.newGroupMemberKey(asHashPart(sContextId), treeId, folderId);
    }

    private boolean existsFolder(String treeId, String folderId, StorageType storageType, StorageParameters storageParameters) throws OXException {
        FolderStorage storage = registry.getFolderStorage(treeId, folderId);
        if (null == storage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, folderId);
        }
        boolean started = storage.startTransaction(storageParameters, false);
        try {
            boolean exists = storage.containsFolder(treeId, folderId, storageType, storageParameters);
            if (started) {
                storage.commitTransaction(storageParameters);
                started = false;
            }
            return exists;
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (started) {
                storage.rollback(storageParameters);
            }
        }
    }

    /**
     * Loads denoted folder from un-cached storage.
     *
     * @param treeId The tree identifier
     * @param folderId The folder identifier
     * @param storageType The storage type
     * @param storageParameters The storage parameters
     * @return The loaded folder
     * @throws OXException If loading folder fails
     */
    public Folder loadFolder(String treeId, String folderId, StorageType storageType, StorageParameters storageParameters) throws OXException {
        return loadFolder(treeId, folderId, storageType, false, storageParameters);
    }

    private Folder loadFolder(String treeId, String folderId, StorageType storageType, boolean readWrite, StorageParameters storageParameters) throws OXException {
        return loadFolder0(treeId, folderId, storageType, readWrite, storageParameters);
    }

    private Folder loadFolder0(String treeId, String folderId, StorageType storageType, boolean readWrite, StorageParameters storageParameters) throws OXException {
        FolderStorage storage = registry.getFolderStorage(treeId, folderId);
        if (null == storage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, folderId);
        }
        boolean started = startTransaction(readWrite ? Mode.WRITE_AFTER_READ : Mode.READ, storageParameters, storage);
        boolean rollback = true;
        try {
            storageParameters.setIgnoreCache(Boolean.valueOf(readWrite));
            Folder folder = storage.getFolder(treeId, folderId, storageType, storageParameters);
            if (started) {
                storage.commitTransaction(storageParameters);
            }
            rollback = false;
            return folder;
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            storageParameters.setIgnoreCache(null);
            if (started && rollback) {
                storage.rollback(storageParameters);
            }
        }
    }

    private String[] loadAllSubfolders(String treeId, Folder folder, boolean readWrite, StorageParameters storageParameters) throws OXException {
        Set<FolderStorage> openedStorages = HashSet.newHashSet(2);
        Set<String> ids = HashSet.newHashSet(16);
        boolean rollback = true;
        try {
            String[] subfolderIds = folder.getSubfolderIDs();
            if (null == subfolderIds) {
                loadAllSubfolders(treeId, folder.getID(), readWrite, storageParameters, ids, openedStorages);
            } else {
                ids.addAll(Arrays.asList(subfolderIds));
                for (String subfolderId : subfolderIds) {
                    loadAllSubfolders(treeId, subfolderId, readWrite, storageParameters, ids, openedStorages);
                }
            }
            for (FolderStorage fs : openedStorages) {
                fs.commitTransaction(storageParameters);
            }
            rollback = false;
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (rollback) {
                for (FolderStorage fs : openedStorages) {
                    fs.rollback(storageParameters);
                }
            }
        }
        return ids.toArray(new String[ids.size()]);
    }

    private void loadAllSubfolders(String treeId, String folderId, boolean readWrite, StorageParameters storageParameters, Set<String> ids, Set<FolderStorage> openedStorages) throws OXException {
        FolderStorage storage = registry.getFolderStorage(treeId, folderId);
        if (null == storage) {
            throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, folderId);
        }
        checkOpenedStorage(storage, readWrite, openedStorages, storageParameters);
        try {
            SortableId[] subfolders = storage.getSubfolders(treeId, folderId, storageParameters);
            for (SortableId sortableId : subfolders) {
                String id = sortableId.getId();
                loadAllSubfolders(treeId, id, readWrite, storageParameters, ids, openedStorages);
                ids.add(id);
            }
        } catch (RuntimeException e) {
            throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    protected static void checkOpenedStorage(FolderStorage checkMe, boolean modify, Collection<FolderStorage> openedStorages, StorageParameters storageParameters) throws OXException {
        if (openedStorages.contains(checkMe)) {
            // Passed storage is already opened
            return;
        }
        // Passed storage has not been opened before. Open now and add to collection
        if (checkMe.startTransaction(storageParameters, modify)) {
            openedStorages.add(checkMe);
        }
    }

    private static final Function<? super FolderStorage, ? extends TIntList> FUNCTION_NEW_TINTLIST = f -> new TIntArrayList();

    private Map<String, Folder> loadFolders(final String treeId, List<String> folderIds, final StorageType storageType, StorageParameters storageParameters) throws OXException {
        /*
         * Collect by folder storage
         */
        int size = folderIds.size();
        Map<FolderStorage, TIntList> map = HashMap.newHashMap(4);
        for (int i = 0; i < size; i++) {
            String id = folderIds.get(i);
            FolderStorage tmp = registry.getFolderStorage(treeId, id);
            if (null == tmp) {
                throw FolderExceptionErrorMessage.NO_STORAGE_FOR_ID.create(treeId, id);
            }
            map.computeIfAbsent(tmp, FUNCTION_NEW_TINTLIST).add(i);
        }
        /*
         * Process by folder storage
         */
        CompletionService<Object> completionService;
        final StorageParametersProvider paramsProvider;
        if (1 == map.size()) {
            completionService = new CallerRunsCompletionService<Object>();
            paramsProvider = new InstanceStorageParametersProvider(storageParameters);
        } else {
            ThreadPoolService tps = CacheServiceRegistry.getServiceRegistry().getService(ThreadPoolService.class, true);
            completionService = null == tps ? new CallerRunsCompletionService<>() : new ThreadPoolCompletionService<Object>(tps).setTrackable(true);

            Session session = storageParameters.getSession();
            paramsProvider = null == session ? new SessionStorageParametersProvider(
                storageParameters.getUser(),
                storageParameters.getContext()) : new SessionStorageParametersProvider((ServerSession) session);
        }
        /*
         * Create destination map
         */
        final Map<String, Folder> ret = new ConcurrentHashMap<String, Folder>(size, 0.9F, 1);
        int taskCount = 0;
        for (Map.Entry<FolderStorage, TIntList> entry : map.entrySet()) {
            final FolderStorage fs = entry.getKey();
            /*
             * Create the list of IDs to load with current storage
             */
            final List<String> ids;
            {
                int[] indexes = entry.getValue().toArray();
                ids = new ArrayList<String>(indexes.length);
                for (int index : indexes) {
                    ids.add(folderIds.get(index));
                }
            }
            /*
             * Submit task
             */
            completionService.submit(new TrackableCallable<Object>() {

                @Override
                public Object call() throws Exception {
                    StorageParameters newParameters = paramsProvider.getStorageParameters();
                    boolean started = !DatabaseFolderType.getInstance().equals(fs.getFolderType()) && fs.startTransaction(
                        newParameters,
                        false);
                    try {
                        /*
                         * Load them & commit
                         */
                        List<Folder> folders = fs.getFolders(treeId, ids, storageType, newParameters);
                        if (started) {
                            fs.commitTransaction(newParameters);
                            started = false;
                        }
                        /*
                         * Fill into map
                         */
                        for (Folder folder : folders) {
                            ret.put(folder.getID(), folder);
                        }
                        /*
                         * Return
                         */
                        return null;
                    } catch (RuntimeException e) {
                        throw FolderExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
                    } finally {
                        if (started) {
                            fs.rollback(newParameters);
                        }
                    }
                }
            });
            taskCount++;
        }
        /*
         * Wait for completion
         */
        ThreadPools.takeCompletionService(completionService, taskCount, FACTORY);
        return ret;
    }

    /**
     * Creates a new storage parameter instance.
     *
     * @return A new storage parameter instance.
     */
    static StorageParameters newStorageParameters(StorageParameters source) {
        Session session = source.getSession();
        if (null == session) {
            return new StorageParametersImpl(source.getUser(), source.getContext());
        }
        return new StorageParametersImpl((ServerSession) session, source.getUser(), source.getContext());
    }

    private static FolderMap getFolderMapFor(StorageParameters parameters) {
        return FolderMapManagement.getInstance().getFor(parameters.getContextId(), parameters.getUserId());
    }

    private static FolderMap optFolderMapFor(StorageParameters parameters) {
        return FolderMapManagement.getInstance().optFor(parameters.getUserId(), parameters.getContextId());
    }

    /**
     * Drops entries associated with specified user in given context.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param notify Whether to post notification or not
     */
    public static void dropUserEntries(int userId, int contextId, boolean notify) {
        FolderMapManagement.getInstance().dropFor(userId, contextId, notify);
    }

    private FolderStorage[] getFolderStoragesForParent(String treeId, String parentId, StorageParameters storageParameters) {
        FolderStorage[] folderStorages = registry.getFolderStoragesForParent(treeId, parentId);
        if (null == folderStorages) {
            return new FolderStorage[0];
        }
        if (0 == folderStorages.length || null == storageParameters.getDecorator()) {
            return folderStorages;
        }
        List<ContentType> allowedContentTypes = storageParameters.getDecorator().getAllowedContentTypes();
        if (null == allowedContentTypes || allowedContentTypes.isEmpty()) {
            return folderStorages;
        }
        List<FolderStorage> possibleStorages = new ArrayList<FolderStorage>(folderStorages.length);
        for (FolderStorage folderStorage : folderStorages) {
            /*
             * only include if at least one allowed content type is supported by storage
             */
            if (supportsAnyContentType(folderStorage, allowedContentTypes)) {
                possibleStorages.add(folderStorage);
            }
        }
        return possibleStorages.toArray(new FolderStorage[possibleStorages.size()]);
    }

    private static boolean supportsAnyContentType(FolderStorage folderStorage, List<ContentType> allowedContentTypes) {
        if (null == allowedContentTypes || allowedContentTypes.isEmpty()) {
            return true;
        }
        for (ContentType contentType : allowedContentTypes) {
            if (Tools.supportsContentType(contentType, folderStorage)) {
                return true;
            }
        }
        return false;
    }

    private static final class FolderComparator implements Comparator<Folder> {

        private final Locale locale;
        private final Collator collator;

        /**
         * Initializes a new {@link FolderComparator}.
         *
         * @param locale The locale to use, or <code>null</code> to fall back to the default locale
         */
        public FolderComparator(Locale locale) {
            super();
            this.locale = locale;
            collator = Collators.getSecondaryInstance(null == locale ? Locale.US : locale);
        }

        @Override
        public int compare(Folder folder1, Folder folder2) {
            return collator.compare(folder1.getName(locale), folder2.getName(locale));
        }

    }

}
