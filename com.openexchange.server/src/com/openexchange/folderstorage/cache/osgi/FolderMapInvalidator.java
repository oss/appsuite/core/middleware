/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.folderstorage.cache.osgi;

import org.osgi.framework.BundleContext;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.cache.memory.FolderMapManagement;
import com.openexchange.pubsub.AbstractTrackingChannelListener;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.Message;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.segmenter.sitechange.SiteChangedListener;
import com.openexchange.session.UserAndContext;

/**
 * {@link FolderMapInvalidator} - Invalidates folder map through pub/sub service.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class FolderMapInvalidator extends AbstractTrackingChannelListener<UserAndContext> implements SiteChangedListener {

    /**
     * Initializes a new {@link FolderMapInvalidator}.
     *
     * @param context The bundle context
     */
    public FolderMapInvalidator(BundleContext context) {
        super(context);
    }

    @Override
    public void onMessage(Message<UserAndContext> message) {
        if (message.isRemote()) {
            // Remotely received
            UserAndContext userAndContext = message.getData();
            int contextId = userAndContext.getContextId();
            if (contextId <= 0) {
                FolderMapManagement.getInstance().clear(false);
            }

            int userId = userAndContext.getUserId();
            if (userId <= 0) {
                FolderMapManagement.getInstance().dropFor(contextId, false);
            } else {
                FolderMapManagement.getInstance().dropFor(userId, contextId, false);
            }
        }
    }

    @Override
    protected Channel<UserAndContext> getChannel(PubSubService service) {
        return FolderMapManagement.getChannel(service);
    }

    @Override
    public void onSiteChange() throws OXException {
        FolderMapManagement.getInstance().clear(false);
    }

}
