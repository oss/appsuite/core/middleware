/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.folderstorage.cache.osgi;

import java.sql.Connection;
import java.util.Map;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.cache.memory.FolderMapManagement;
import com.openexchange.mailaccount.MailAccountListener;


/**
 * {@link CacheFolderStorageInvalidator} - Invalidates folder map.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class CacheFolderStorageInvalidator implements MailAccountListener {

    /**
     * Initializes a new {@link CacheFolderStorageInvalidator}.
     */
    public CacheFolderStorageInvalidator() {
        super();
    }

    @Override
    public void onBeforeMailAccountDeletion(int id, Map<String, Object> eventProps, int userId, int contextId, Connection con) throws OXException {
        // Don't care
    }

    @Override
    public void onAfterMailAccountDeletion(int id, Map<String, Object> eventProps, int userId, int contextId, Connection con) throws OXException {
        invalidateFor(userId, contextId);
    }

    @Override
    public void onMailAccountCreated(int id, Map<String, Object> eventProps, int userId, int contextId, Connection con) {
        invalidateFor(userId, contextId);
    }

    @Override
    public void onMailAccountModified(int id, Map<String, Object> eventProps, int userId, int contextId, Connection con) {
        invalidateFor(userId, contextId);
    }

    private static void invalidateFor(int userId, int contextId) {
        FolderMapManagement.getInstance().dropFor(userId, contextId, true);
    }

}
