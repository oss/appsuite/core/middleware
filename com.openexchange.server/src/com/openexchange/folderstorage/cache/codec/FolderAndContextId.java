/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.folderstorage.cache.codec;

import com.openexchange.folderstorage.Folder;

/**
 * {@link FolderAndContextId} -  A tuple for an instance of {@link Folder} and the identifier of the context, in which the folder resides.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class FolderAndContextId {

    private final Folder folder;
    private final int contextId;

    /**
     * Initializes a new instance of {@link FolderAndContextId}.
     *
     * @param folder The folder instance
     * @param contextId The context identifier
     */
    public FolderAndContextId(Folder folder, int contextId) {
        super();
        this.folder = folder;
        this.contextId = contextId;
    }

    /**
     * Gets the folder
     *
     * @return The folder
     */
    public Folder getFolder() {
        return folder;
    }

    /**
     * Gets the context identifier
     *
     * @return The context identifier
     */
    public int getContextId() {
        return contextId;
    }

}
