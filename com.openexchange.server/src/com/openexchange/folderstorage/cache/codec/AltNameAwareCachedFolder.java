/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.folderstorage.cache.codec;

import java.util.Locale;
import com.openexchange.folderstorage.AltNameAwareFolder;
import com.openexchange.folderstorage.NameTranslatorFactory;
import com.openexchange.folderstorage.UserSensitiveAttributesProvider;


/**
 * {@link AltNameAwareCachedFolder}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
class AltNameAwareCachedFolder extends LocalizedCacheFolder implements AltNameAwareFolder {

    /** The serial version UID */
    private static final long serialVersionUID = -4105725581915764351L;

    private final String altName;

    /**
     * Initializes a new instance of {@link AltNameAwareCachedFolder}.
     *
     * @param altName The alternative name
     * @param contextId The context identifier
     * @param nameTranslator The name translator
     * @param attributesProvider The attributes provider to use or <code>null</code>
     */
    AltNameAwareCachedFolder(String altName, int contextId, NameTranslatorFactory nameTranslatorFactory, UserSensitiveAttributesProvider attributesProvider) {
        super(contextId, nameTranslatorFactory, attributesProvider);
        this.altName = altName;
    }

    @Override
    public boolean supportsAltName() {
        return true;
    }

    @Override
    public String getAltName() {
        return altName;
    }

    @Override
    public String getLocalizedName(Locale locale, boolean altName) {
        if (!altName) {
            return getLocalizedName(locale);
        }
        final String toTranslate = this.altName;
        if (null == toTranslate) {
            return getLocalizedName(locale);
        }
        return nameTranslatorFactory.getNameTranslator(contextId).translate(toTranslate, locale);
    }

}
