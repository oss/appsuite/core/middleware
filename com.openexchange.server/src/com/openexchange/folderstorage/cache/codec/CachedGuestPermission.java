/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.folderstorage.cache.codec;

import com.openexchange.folderstorage.FolderPermissionType;
import com.openexchange.folderstorage.GuestPermission;
import com.openexchange.groupware.EntityInfo;
import com.openexchange.share.recipient.ShareRecipient;

/**
 * {@link CachedGuestPermission} - A guest permission implementation used by deserialization in cache codec.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
class CachedGuestPermission extends CachedPermission implements GuestPermission {

    private static final long serialVersionUID = 1829193652742803741L;

    private final ShareRecipient recipient;

    /**
     * Initializes a new instance of {@link CachedGuestPermission}.
     *
     * @param entity The entity identifier
     * @param group The group flag
     * @param identifier The qualified identifier of the entity
     * @param legator The legator identifier (identifier of the sharing folder)
     * @param folderPermissionType The permission type
     * @param admin The amdinistrator flag
     * @param fp The folder permission
     * @param rp The read permission
     * @param wp The write permission
     * @param dp The delete permission
     * @param system The system bitmask
     * @param entityInfo The entity information
     * @param recipient The share recipient
     */
    CachedGuestPermission(int entity, boolean group, String identifier, String legator, FolderPermissionType folderPermissionType, boolean admin, int fp, int rp, int wp, int dp, int system, EntityInfo entityInfo, ShareRecipient recipient) { // NOSONARLINT
        super(entity, group, identifier, legator, folderPermissionType, admin, fp, rp, wp, dp, system, entityInfo);
        this.recipient = recipient;
    }

    @Override
    public ShareRecipient getRecipient() {
        return recipient;
    }
}
