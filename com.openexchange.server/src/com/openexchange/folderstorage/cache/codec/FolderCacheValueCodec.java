/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.folderstorage.cache.codec;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONCoercion;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.Codecs;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;
import com.openexchange.folderstorage.AltNameAwareFolder;
import com.openexchange.folderstorage.ContentTypeByClassName;
import com.openexchange.folderstorage.Folder;
import com.openexchange.folderstorage.FolderPath;
import com.openexchange.folderstorage.FolderPermissionType;
import com.openexchange.folderstorage.GuestPermission;
import com.openexchange.folderstorage.LocalizableNameFolder;
import com.openexchange.folderstorage.NameTranslatorFactory;
import com.openexchange.folderstorage.Permission;
import com.openexchange.folderstorage.Permissions;
import com.openexchange.folderstorage.TypeByClassName;
import com.openexchange.folderstorage.UsedForSync;
import com.openexchange.folderstorage.UserSensitiveAttributesFolder;
import com.openexchange.folderstorage.UserSensitiveAttributesProvider;
import com.openexchange.groupware.EntityInfo;
import com.openexchange.share.recipient.AnonymousRecipient;
import com.openexchange.share.recipient.GuestRecipient;
import com.openexchange.share.recipient.InternalRecipient;
import com.openexchange.share.recipient.RecipientType;
import com.openexchange.share.recipient.ShareRecipient;

/**
 * {@link FolderCacheValueCodec} - The cache value codec for folder.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class FolderCacheValueCodec extends AbstractJSONObjectCacheValueCodec<FolderAndContextId> {

    private static final String FIELD_CONTEXT_ID = "x";
    private static final String FIELD_ID = "i";
    private static final String FIELD_PARENT_ID = "p";
    private static final String FIELD_TREE_ID = "ti";
    private static final String FIELD_ACCOUNT_ID = "ai";
    private static final String FIELD_NAME = "n";
    private static final String FIELD_DEFAULT_TYPE = "dt";
    private static final String FIELD_DEFAULT_FLAG = "d";
    private static final String FIELD_SUBSCRIBED_FLAG = "sf";
    private static final String FIELD_HAS_SUBSCRIBED_SUBFOLDERS_FLAG = "ssf";
    private static final String FIELD_VIRTUAL_FLAG = "v";
    private static final String FIELD_BITS = "b";
    private static final String FIELD_CAPABILITIES = "c";
    private static final String FIELD_TOTAL = "tl";
    private static final String FIELD_NEW = "nw";
    private static final String FIELD_UNREAD = "ur";
    private static final String FIELD_DELETED = "dd";
    private static final String FIELD_CONTENT_TYPE = "ct";
    private static final String FIELD_CREATED_BY = "cb";
    private static final String FIELD_MODIFIED_BY = "mb";
    private static final String FIELD_CREATED_FROM = "cf";
    private static final String FIELD_MODIFIED_FROM = "mf";
    private static final String FIELD_CREATION_DATE = "cd";
    private static final String FIELD_LAST_MODIFIED = "lm";
    private static final String FIELD_META = "m";
    private static final String FIELD_NEW_ID = "ni";
    private static final String FIELD_ORIGIN_PATH = "op";
    private static final String FIELD_PERMISSIONS = "ps";
    private static final String FIELD_SUBFOLDER_IDENTIFIERS = "si";
    private static final String FIELD_SUMMARY = "su";
    private static final String FIELD_SUPPORTED_CAPABILITIES = "sc";
    private static final String FIELD_TYPE = "t";
    private static final String FIELD_USED_FOR_SYNC = "ufs";
    private static final String FIELD_PROTECTED = "pd";
    private static final String FIELD_ENTITY = "e";
    private static final String FIELD_GROUP = "g";
    private static final String FIELD_IDENTIFIER = "id";
    private static final String FIELD_LEGATOR = "l";
    private static final String FIELD_PERMISSION_BITS = "pb";
    private static final String FIELD_SYSTEM_FLAG = "s";
    private static final String GUEST_FLAG = "gt";
    private static final String FIELD_PASSWORD = "pw";
    private static final String FIELD_EXPIRY_DATE = "ed";
    private static final String FIELD_INCLUDE_SUBFOLDERS = "is";
    private static final String FIELD_EMAIL_ADDRESS = "ea";
    private static final String FIELD_DISPLAY_NAME = "dn";
    private static final String FIELD_CONTACT_ID = "ci";
    private static final String FIELD_CONTACT_FOLDER = "cfd";
    private static final String FIELD_PREFERRED_LANGUAGE = "pl";
    private static final String FIELD_RECIPIENT = "r";
    private static final String FIELD_TRANSLATOR_FACTORY = "tf";
    private static final String FIELD_ALTERNATIVE_NAME = "a";
    private static final String FIELD_ATTRS_PROVIDER = "ap";

    private static final UUID CODEC_ID = UUID.fromString("964bdd8f-3df2-41d4-b276-92e5d0424be5");

    /**
     * Initializes a new {@link FolderCacheValueCodec}.
     */
    public FolderCacheValueCodec() {
        super();
    }

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONObject writeJson(FolderAndContextId folderAndContextId) throws Exception { // NOSONARLINT
        Folder folder = folderAndContextId.getFolder();
        if (!folder.isCacheable()) {
            throw new IllegalArgumentException("Specified folder is not cacheable");
        }
        if (!folder.isGlobalID()) {
            throw new IllegalArgumentException("Specified folder is NOT globally valid, but only per user");
        }

        JSONObject j = new JSONObject(32);
        j.put(FIELD_CONTEXT_ID, folderAndContextId.getContextId());
        j.putOpt(FIELD_ID, folder.getID());
        j.putOpt(FIELD_PARENT_ID, folder.getParentID());
        j.putOpt(FIELD_TREE_ID, folder.getTreeID());
        j.putOpt(FIELD_ACCOUNT_ID, compressAccountId(folder.getAccountID()));
        j.putOpt(FIELD_NAME, compressName(folder.getName()));
        j.put(FIELD_DEFAULT_TYPE, folder.getDefaultType());
        if (folder.isDefault()) {
            j.put(FIELD_DEFAULT_FLAG, 1);
        }
        if (folder.isSubscribed()) {
            j.put(FIELD_SUBSCRIBED_FLAG, 1);
        }
        if (folder.hasSubscribedSubfolders()) {
            j.put(FIELD_HAS_SUBSCRIBED_SUBFOLDERS_FLAG, 1);
        }
        if (folder.isVirtual()) {
            j.put(FIELD_VIRTUAL_FLAG, 1);
        }
        if (folder.getBits() >= 0) {
            j.put(FIELD_BITS, folder.getBits());
        }
        if (folder.getCapabilities() >= 0) {
            j.put(FIELD_CAPABILITIES, folder.getCapabilities());
        }
        if (folder.getTotal() >= 0) {
            j.put(FIELD_TOTAL, folder.getTotal());
        }
        if (folder.getNew() >= 0) {
            j.put(FIELD_NEW, folder.getNew());
        }
        if (folder.getUnread() >= 0) {
            j.put(FIELD_UNREAD, folder.getUnread());
        }
        if (folder.getDeleted() >= 0) {
            j.put(FIELD_DELETED, folder.getDeleted());
        }
        int contentTypeRegNumber = folder.getContentType() == null ? 0 : ContentTypeByClassName.getRegistrationNumberByContentType(folder.getContentType());
        if (contentTypeRegNumber > 0) {
            j.put(FIELD_CONTENT_TYPE, contentTypeRegNumber);
        }
        if (folder.getCreatedBy() >= 0) {
            j.put(FIELD_CREATED_BY, folder.getCreatedBy());
        }
        if (folder.getModifiedBy() >= 0) {
            j.put(FIELD_MODIFIED_BY, folder.getModifiedBy());
        }
        j.putOpt(FIELD_CREATED_FROM, folder.getCreatedFrom() == null ? null : folder.getCreatedFrom().toJSON());
        j.putOpt(FIELD_MODIFIED_FROM, folder.getModifiedFrom() == null ? null : folder.getModifiedFrom().toJSON());
        if (folder.getCreationDate() != null) {
            j.put(FIELD_CREATION_DATE, folder.getCreationDate().getTime());
        }
        if (folder.getLastModified() != null) {
            j.put(FIELD_LAST_MODIFIED, folder.getLastModified().getTime());
        }
        j.putOpt(FIELD_META, folder.getMeta() == null ? null : JSONCoercion.coerceToJSON(folder.getMeta()));
        j.putOpt(FIELD_NEW_ID, folder.getNewID());
        j.putOpt(FIELD_ORIGIN_PATH, folder.getOriginPath() == null ? null : folder.getOriginPath().toString());
        j.putOpt(FIELD_PERMISSIONS, writePermissions(folder.getPermissions()));
        j.putOpt(FIELD_SUBFOLDER_IDENTIFIERS, folder.getSubfolderIDs() == null ? null : JSONCoercion.coerceToJSON(folder.getSubfolderIDs()));
        j.putOpt(FIELD_SUMMARY, folder.getSummary());
        j.putOpt(FIELD_SUPPORTED_CAPABILITIES, folder.getSupportedCapabilities() == null ? null : JSONCoercion.coerceToJSON(compressCapabilities(folder.getSupportedCapabilities())));
        j.put(FIELD_TYPE, folder.getType() == null ? 0 : TypeByClassName.getRegistrationNumberByType(folder.getType()));
        j.putOpt(FIELD_USED_FOR_SYNC, writeUsedForSync(folder.getUsedForSync()));

        if (folder instanceof LocalizableNameFolder l) {
            // Localization is supported
            j.putOpt(FIELD_TRANSLATOR_FACTORY, l.getNameTranslatorFactory().map(f -> Integer.valueOf(f.getRegistrationNumber())).orElse(null));
            if (folder instanceof AltNameAwareFolder a) {
                j.putOpt(FIELD_ALTERNATIVE_NAME, a.getAltName());
            }
        }

        if (folder instanceof UserSensitiveAttributesFolder a) {
            // User-senstive attributes
            j.put(FIELD_ATTRS_PROVIDER, a.getUserSensitiveAttributesProvider().getRegistrationNumber());
        }

        return j;
    }

    private static JSONObject writeUsedForSync(UsedForSync usedForSync) throws JSONException {
        if (usedForSync == null) {
            return null;
        }
        JSONObject j = new JSONObject(2);
        if (usedForSync.isProtected()) {
            j.put(FIELD_PROTECTED, 1);
        }
        if (usedForSync.isUsedForSync()) {
            j.put(FIELD_USED_FOR_SYNC, 1);
        }
        return j;
    }

    private static JSONArray writePermissions(Permission[] permissions) throws JSONException { // NOSONARLINT
        if (permissions == null) {
            return null;
        }
        JSONArray j = new JSONArray(permissions.length);
        for (Permission permission : permissions) {
            JSONObject jp = new JSONObject(16);
            jp.put(FIELD_ENTITY, permission.getEntity());
            if (permission.isGroup()) {
                jp.put(FIELD_GROUP, 1);
            }
            jp.putOpt(FIELD_IDENTIFIER, permission.getIdentifier());
            jp.putOpt(FIELD_LEGATOR, permission.getPermissionLegator());
            jp.putOpt(FIELD_TYPE, permission.getType() == null ? null : I(permission.getType().getTypeNumber()));
            jp.put(FIELD_PERMISSION_BITS, Permissions.createPermissionBits(permission));
            jp.put(FIELD_SYSTEM_FLAG, permission.getSystem());
            jp.putOpt(FIELD_ENTITY, permission.getEntityInfo() == null ? null : permission.getEntityInfo().toJSON());
            if (permission instanceof GuestPermission) {
                writeGuestPermissionInfo(permission, jp);
            }
            j.put(jp);
        }
        return j;
    }

    private static void writeGuestPermissionInfo(Permission permission, JSONObject jp) throws JSONException {
        jp.put(GUEST_FLAG, 1);
        ShareRecipient recipient = ((GuestPermission) permission).getRecipient();
        if (recipient != null) {
            RecipientType recipientType = recipient.getType();
            JSONObject jRecipient = new JSONObject(8);
            jRecipient.put(FIELD_BITS, recipient.getBits());
            jRecipient.put(FIELD_TYPE, recipientType.getIdentifier());
            switch (recipientType) {
                case ANONYMOUS:
                    AnonymousRecipient anonymousRecipient = (AnonymousRecipient) recipient;
                    jRecipient.putOpt(FIELD_PASSWORD, anonymousRecipient.getPassword());
                    jRecipient.putOpt(FIELD_EXPIRY_DATE, anonymousRecipient.getExpiryDate() == null ? null : L(anonymousRecipient.getExpiryDate().getTime()));
                    if (anonymousRecipient.getIncludeSubfolders()) {
                        jRecipient.put(FIELD_INCLUDE_SUBFOLDERS, 1);
                    }
                    break;
                case GROUP: //$FALL-THROUGH$
                case USER: // NOSONARLINT
                    InternalRecipient internalRecipient = (InternalRecipient) recipient;
                    jRecipient.put(FIELD_ENTITY, internalRecipient.getEntity());
                    if (internalRecipient.isGroup()) {
                        jRecipient.put(FIELD_GROUP, 1);
                    }
                    break;
                case GUEST:
                    GuestRecipient guestRecipient = (GuestRecipient) recipient;
                    jRecipient.putOpt(FIELD_EMAIL_ADDRESS, guestRecipient.getEmailAddress());
                    jRecipient.putOpt(FIELD_DISPLAY_NAME, guestRecipient.getDisplayName());
                    jRecipient.putOpt(FIELD_CONTACT_ID, guestRecipient.getContactID());
                    jRecipient.putOpt(FIELD_CONTACT_FOLDER, guestRecipient.getContactFolder());
                    jRecipient.putOpt(FIELD_PASSWORD, guestRecipient.getPassword());
                    jRecipient.putOpt(FIELD_PREFERRED_LANGUAGE, guestRecipient.getPreferredLanguage());
                    break;
                default:
                    break;
            }
            jp.put(FIELD_RECIPIENT, jRecipient);
        }
    }

    @Override
    protected FolderAndContextId parseJson(JSONObject j) throws Exception { // NOSONARLINT
        int contextId = j.getInt(FIELD_CONTEXT_ID);

        CachedFolder folder = initFolder(j, contextId);

        { // NOSONARLINT
            String id = j.optString(FIELD_ID, null);
            if (id != null) {
                folder.setID(id);
            }
        }
        { // NOSONARLINT
            String parentId = j.optString(FIELD_PARENT_ID, null);
            if (parentId != null) {
                folder.setParentID(parentId);
            }
        }
        { // NOSONARLINT
            String treeId = j.optString(FIELD_TREE_ID, null);
            if (treeId != null) {
                folder.setTreeID(treeId);
            }
        }
        { // NOSONARLINT
            String accountId = decompressAccountId(j.optString(FIELD_ACCOUNT_ID, null));
            if (accountId != null) {
                folder.setAccountID(accountId);
            }
        }
        { // NOSONARLINT
            String name = decompressName(j.optString(FIELD_NAME, null));
            if (name != null) {
                folder.setName(name);
            }
        }

        folder.setDefaultType(j.optInt(FIELD_DEFAULT_TYPE, 0));
        folder.setDefault(j.optInt(FIELD_DEFAULT_FLAG, 0) > 0);
        folder.setSubscribed(j.optInt(FIELD_SUBSCRIBED_FLAG, 0) > 0);
        folder.setSubscribedSubfolders(j.optInt(FIELD_HAS_SUBSCRIBED_SUBFOLDERS_FLAG, 0) > 0);
        folder.setVirtual(j.optInt(FIELD_VIRTUAL_FLAG, 0) > 0);

        folder.setBits(j.optInt(FIELD_BITS, -1));
        folder.setCapabilities(j.optInt(FIELD_CAPABILITIES, -1));
        folder.setTotal(j.optInt(FIELD_TOTAL, -1));
        folder.setNew(j.optInt(FIELD_NEW, -1));
        folder.setUnread(j.optInt(FIELD_UNREAD, -1));
        folder.setDeleted(j.optInt(FIELD_DELETED, -1));

        { // NOSONARLINT
            int registrationNumber = j.optInt(FIELD_CONTENT_TYPE, 0);
            if (registrationNumber != 0) {
                folder.setContentType(ContentTypeByClassName.getContentTypeByNumber(registrationNumber));
            }
        }

        folder.setCreatedBy(j.optInt(FIELD_CREATED_BY, -1));
        folder.setModifiedBy(j.optInt(FIELD_MODIFIED_BY, -1));

        { // NOSONARLINT
            JSONObject jCreatedFrom = j.optJSONObject(FIELD_CREATED_FROM);
            if (jCreatedFrom != null) {
                folder.setCreatedFrom(EntityInfo.parseJSON(jCreatedFrom));
            }
        }
        { // NOSONARLINT
            JSONObject jModifiedFrom = j.optJSONObject(FIELD_MODIFIED_FROM);
            if (jModifiedFrom != null) {
                folder.setModifiedFrom(EntityInfo.parseJSON(jModifiedFrom));
            }
        }

        { // NOSONARLINT
            long creationDate = j.optLong(FIELD_CREATION_DATE, 0);
            if (creationDate > 0) {
                folder.setCreationDate(new Date(creationDate)); // NOSONARLINT
            }
        }
        { // NOSONARLINT
            long lastModified = j.optLong(FIELD_LAST_MODIFIED, 0);
            if (lastModified > 0) {
                folder.setLastModified(new Date(lastModified)); // NOSONARLINT
            }
        }

        { // NOSONARLINT
            JSONObject jMeta = j.optJSONObject(FIELD_META);
            if (jMeta != null) {
                folder.setMeta((Map<String, Object>) JSONCoercion.coerceToNative(jMeta));
            }
        }

        { // NOSONARLINT
            String newId = j.optString(FIELD_NEW_ID, null);
            if (newId != null) {
                folder.setNewID(newId);
            }
        }

        { // NOSONARLINT
            String originPath = j.optString(FIELD_ORIGIN_PATH, null);
            if (originPath != null) {
                folder.setOriginPath(FolderPath.parseFrom(originPath));
            }
        }

        { // NOSONARLINT
            JSONArray jPermissions = j.optJSONArray(FIELD_PERMISSIONS);
            if (jPermissions != null) {
                folder.setPermissions(readPermissions(jPermissions));
            }
        }

        { // NOSONARLINT
            JSONArray jSubfolderIds = j.optJSONArray(FIELD_SUBFOLDER_IDENTIFIERS);
            if (jSubfolderIds != null) {
                String[] subfolderIds = new String[jSubfolderIds.length()];
                int i = 0;
                for (Object object : jSubfolderIds) {
                    subfolderIds[i++] = object.toString();
                }
                folder.setSubfolderIDs(subfolderIds);
            }
        }

        { // NOSONARLINT
            String summary = j.optString(FIELD_SUMMARY, null);
            if (summary != null) {
                folder.setSummary(summary);
            }
        }

        { // NOSONARLINT
            JSONArray jSupportedCapabilities = j.optJSONArray(FIELD_SUPPORTED_CAPABILITIES);
            if (jSupportedCapabilities != null) {
                Set<String> supportedCapabilities = HashSet.newHashSet(jSupportedCapabilities.length());
                for (Object cap : jSupportedCapabilities) {
                    supportedCapabilities.add(cap.toString());
                }
                folder.setSupportedCapabilities(decompressCapabilities(supportedCapabilities));
            }
        }

        { // NOSONARLINT
            int registrationNumber = j.optInt(FIELD_TYPE, 0);
            if (registrationNumber != 0) {
                folder.setType(TypeByClassName.getTypeByNumber(registrationNumber));
            }
        }

        { // NOSONARLINT
            UsedForSync usedForSync = readUsedForSync(j.optJSONObject(FIELD_USED_FOR_SYNC));
            if (usedForSync != null) {
                folder.setUsedForSync(usedForSync);
            }
        }

        return new FolderAndContextId(folder, contextId);
    }

    /**
     * Initializes the appropriate instance of <tt>CachedFolder</tt> for given JSON representation of a folder that
     * <ul>
     * <li>might be aware of user-sensitive attributes for a folder's subscription status or used-for-sync information
     * <li>might be aware of translatable name
     * <li>might be aware of an alternative (translatable) name
     *</ul>
     *
     * @param jFolder The JSON representation of a folder
     * @param contextId The context identifier
     * @return The appropriate instance of <tt>CachedFolder</tt>
     */
    private static CachedFolder initFolder(JSONObject jFolder, int contextId) {
        NameTranslatorFactory translatorFactory = NameTranslatorFactoryRegistry.getInstance().getFactory(jFolder.optInt(FIELD_TRANSLATOR_FACTORY, 0)).orElse(null);
        if (translatorFactory == null) {
            UserSensitiveAttributesProvider attributesProvider = UserSensitiveAttributesRegistry.getInstance().getProvider(jFolder.optInt(FIELD_ATTRS_PROVIDER, 0)).orElse(null);
            return new CachedFolder(attributesProvider);
        }

        UserSensitiveAttributesProvider attributesProvider = UserSensitiveAttributesRegistry.getInstance().getProvider(jFolder.optInt(FIELD_ATTRS_PROVIDER, 0)).orElse(null);
        String altName = jFolder.optString(FIELD_ALTERNATIVE_NAME, null);
        if (altName == null) {
            return new LocalizedCacheFolder(contextId, translatorFactory, attributesProvider);
        }
        return new AltNameAwareCachedFolder(altName, contextId, translatorFactory, attributesProvider);
    }

    private static UsedForSync readUsedForSync(JSONObject jUsedForSync) {
        return jUsedForSync == null ? null : new UsedForSync(jUsedForSync.optInt(FIELD_USED_FOR_SYNC, 0) > 0, jUsedForSync.optInt(FIELD_PROTECTED, 0) > 0);
    }

    private static Permission[] readPermissions(JSONArray jPermissions) throws JSONException { // NOSONARLINT
        if (jPermissions == null) {
            return null; // NOSONARLINT
        }

        Permission[] permissions = new Permission[jPermissions.length()];
        int i = 0;
        for (Object o : jPermissions) {
            JSONObject jp = (JSONObject) o;
            int entity = jp.getInt(FIELD_ENTITY);
            boolean group = jp.optInt(FIELD_GROUP, 0) > 0;
            String identifier = jp.optString(FIELD_IDENTIFIER, null);
            String legator = jp.optString(FIELD_LEGATOR, null);
            int optType = jp.optInt(FIELD_TYPE, -1);
            // Obtain & parse permission bits
            int[] permissionBits = Permissions.parsePermissionBits(jp.optInt(FIELD_PERMISSION_BITS, 0));
            int fp = permissionBits[0];
            int rp = permissionBits[1];
            int wp = permissionBits[2];
            int dp = permissionBits[3];
            boolean admin = permissionBits[4] > 0;
            int system = jp.getInt(FIELD_SYSTEM_FLAG);
            JSONObject optJEntityInfo = jp.optJSONObject(FIELD_ENTITY);
            if (jp.optInt(GUEST_FLAG, 0) > 0) {
                ShareRecipient recipient = readGuestPermissionInfo(jp);
                permissions[i++] = new CachedGuestPermission(entity, group, identifier, legator, optType < 0 ? null : FolderPermissionType.getType(optType), admin, fp, rp, wp, dp, system, optJEntityInfo == null ? null : EntityInfo.parseJSON(optJEntityInfo), recipient);
            } else {
                permissions[i++] = new CachedPermission(entity, group, identifier, legator, optType < 0 ? null : FolderPermissionType.getType(optType), admin, fp, rp, wp, dp, system, optJEntityInfo == null ? null : EntityInfo.parseJSON(optJEntityInfo));
            }
        }
        return permissions;
    }

    private static ShareRecipient readGuestPermissionInfo(JSONObject jp) throws JSONException {
        ShareRecipient recipient = null;
        JSONObject jRecipient = jp.optJSONObject(FIELD_RECIPIENT);
        if (jRecipient != null) {
            int bits = jRecipient.getInt(FIELD_BITS);
            RecipientType recipientType = RecipientType.recipientTypeFor(jRecipient.getString(FIELD_TYPE));
            switch (recipientType) {
                case ANONYMOUS:
                    Long expiryDate = (Long) jRecipient.opt(FIELD_EXPIRY_DATE);
                    recipient = new AnonymousRecipient(bits, jRecipient.optString(FIELD_PASSWORD, null), expiryDate == null ? null : new Date(expiryDate.longValue()), jRecipient.optInt(FIELD_INCLUDE_SUBFOLDERS, 0) > 0); // NOSONARLINT
                    break;
                case USER: //$FALL-THROUGH$
                case GROUP: // NOSONARLINT
                    InternalRecipient iRecipient = new InternalRecipient();
                    iRecipient.setEntity(jRecipient.getInt(FIELD_ENTITY));
                    iRecipient.setGroup(jRecipient.optInt(FIELD_GROUP, 0) > 0);
                    iRecipient.setBits(bits);
                    recipient = iRecipient;
                    break;
                case GUEST:
                    GuestRecipient gRecipient = new GuestRecipient();
                    gRecipient.setEmailAddress(jRecipient.optString(FIELD_EMAIL_ADDRESS, null));
                    gRecipient.setDisplayName(jRecipient.optString(FIELD_DISPLAY_NAME, null));
                    gRecipient.setContactID(jRecipient.optString(FIELD_CONTACT_ID, null));
                    gRecipient.setContactFolder(jRecipient.optString(FIELD_CONTACT_FOLDER, null));
                    gRecipient.setPassword(jRecipient.optString(FIELD_PASSWORD, null));
                    gRecipient.setPreferredLanguage(jRecipient.optString(FIELD_PREFERRED_LANGUAGE, null));
                    gRecipient.setBits(bits);
                    recipient = gRecipient;
                    break;
                default:
                    break;
            }
        }
        return recipient;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * A list of replacements for folder names.
     */
    private static final List<String[]> NAME_REPLACEMENTS = List.of(
        new String[] {"Global address book", "^G"},
        new String[] {"Private folders", "^P"},
        new String[] {"Public folders", "^U"},
        new String[] {"Calendar", "^C"},
        new String[] {"Tasks", "^T"},
        new String[] {"Contacts", "^O"},
        new String[] {"Documents", "^D"},
        new String[] {"Trash", "^H"},
        new String[] {"Music", "^Z"},
        new String[] {"Templates", "^L"},
        new String[] {"Pictures", "^X"},
        new String[] {"Videos", "^Y"},
        new String[] {"private", "^p"},
        new String[] {"public", "^u"},
        new String[] {"shared", "^s"},
        new String[] {"infostore", "^i"},
        new String[] {"system_ldap", "^l"},
        new String[] {"userstore", "^t"}
    );

    private static String compressName(String name) {
        return compress(name, NAME_REPLACEMENTS, false);
    }

    private static String decompressName(String compressedName) {
        return decompress(compressedName, NAME_REPLACEMENTS, false);
    }

    /**
     * A list of replacements for account identifiers.
     */
    private static final List<String[]> ACCOUNT_ID_REPLACEMENTS = List.of(
        new String[] {"com.openexchange.infostore://infostore", "^i"},
        new String[] {"cal://", "^c"},
        new String[] {"con://", "^o"}
    );

    private static String compressAccountId(String accountId) {
        return compress(accountId, ACCOUNT_ID_REPLACEMENTS, false);
    }

    private static String decompressAccountId(String compressedAccountId) {
        return decompress(compressedAccountId, ACCOUNT_ID_REPLACEMENTS, false);
    }

    /**
     * A list of replacements for supported capabilities.
     */
    private static final List<String[]> CAPABILITIES_REPLACEMENTS = List.of(
        new String[] {"search_in_folder_name", "^s"},
        new String[] {"search_by_term", "^t"},
        new String[] {"extended_metadata", "^m"},
        new String[] {"file_versions", "^v"},
        new String[] {"case_insensitive", "^c"},
        new String[] {"zippable_folder", "^z"},
        new String[] {"restore", "^r"},
        new String[] {"locks", "^l"},
        new String[] {"permissions", "^cp"},
        new String[] {"quota", "^q"},
        new String[] {"sort", "^o"},
        new String[] {"subscription", "^b"}
    );

    private static Set<String> compressCapabilities(Set<String> capabilities) {
        Set<String> retval = new LinkedHashSet<>(capabilities.size());
        for (String capability : capabilities) {
            retval.add(compress(capability, CAPABILITIES_REPLACEMENTS, true));
        }
        return retval;
    }

    private static Set<String> decompressCapabilities(Set<String> compressedCapabilities) {
        Set<String> retval = new LinkedHashSet<>(compressedCapabilities.size());
        for (String compressedCapability : compressedCapabilities) {
            retval.add(decompress(compressedCapability, CAPABILITIES_REPLACEMENTS, true));
        }
        return retval;
    }

    // ---------------------------------------------- Generic compression ---------------------------------------------------------

    private static String compress(String str, Iterable<String[]> replacements, boolean matchCompletely) {
        if (str == null) {
            return null;
        }

        if (matchCompletely) {
            // Replace complete input string if equal to replacement
            for (String[] replacement : replacements) {
                if (str.equals(replacement[0])) {
                    return replacement[1];
                }
            }
            return str;
        }

        // Replace portions of input string
        return Codecs.replace(str, replacements);
    }

    private static String decompress(String str, Iterable<String[]> replacements, boolean matchCompletely) {
        if (str == null) {
            return null;
        }

        if (matchCompletely) {
            for (String[] replacement : replacements) {
                if (str.equals(replacement[1])) {
                    return replacement[0];
                }
            }
            return str;
        }

        return Codecs.unreplace(str, replacements);
    }

}
