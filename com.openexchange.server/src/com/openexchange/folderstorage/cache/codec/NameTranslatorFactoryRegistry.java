/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.folderstorage.cache.codec;

import java.util.Optional;
import java.util.concurrent.ConcurrentMap;
import org.jctools.maps.NonBlockingHashMap;
import com.openexchange.folderstorage.NameTranslatorFactory;

/**
 * {@link NameTranslatorFactoryRegistry} - A registry for known name translator factories.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class NameTranslatorFactoryRegistry {

    private static final NameTranslatorFactoryRegistry INSTANCE = new NameTranslatorFactoryRegistry();

    /**
     * Gets the instance
     *
     * @return The instance
     */
    public static NameTranslatorFactoryRegistry getInstance() {
        return INSTANCE;
    }

    // -----------------------------------------------------------------------------------------------------------------

    private final ConcurrentMap<Integer, NameTranslatorFactory> factories;

    /**
     * Initializes a new instance of {@link NameTranslatorFactoryRegistry}.
     */
    private NameTranslatorFactoryRegistry() {
        super();
        factories = new NonBlockingHashMap<>();
    }

    /**
     * Registers given name translator factory.
     *
     * @param factory The name translator factory to register
     * @return <code>true</code> if successfully registered; otherwise <code>false</code>
     */
    public boolean registerFactory(NameTranslatorFactory factory) {
        return factories.putIfAbsent(Integer.valueOf(factory.getRegistrationNumber()), factory) == null;
    }

    /**
     * Gets the factory associated with given registration number.
     *
     * @param registrationNumber The registration number to look-up
     * @return The factor or empty
     */
    public Optional<NameTranslatorFactory> getFactory(int registrationNumber) {
        return Optional.ofNullable(factories.get(Integer.valueOf(registrationNumber)));
    }

}
