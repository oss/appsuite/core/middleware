/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.folderstorage.cache.codec;

import static com.openexchange.java.Autoboxing.B;
import com.openexchange.folderstorage.AbstractFolder;
import com.openexchange.folderstorage.UsedForSync;
import com.openexchange.folderstorage.UserSensitiveAttributesProvider;

/**
 * {@link CachedFolder} - A folder implementation used by deserialization in cache codec.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
class CachedFolder extends AbstractFolder {

    private static final long serialVersionUID = -4925098032630445773L;

    private Boolean virtual;
    private final UserSensitiveAttributesProvider attributesProvider;

    /**
     * Initializes a new instance.
     *
     * @param attributesProvider The attributes provider to use or <code>null</code>
     */
    CachedFolder(UserSensitiveAttributesProvider attributesProvider) {
        super();
        this.attributesProvider = attributesProvider;
    }

    public void setVirtual(boolean virtual) {
        this.virtual = B(virtual);
    }

    @Override
    public boolean isVirtual() {
        return virtual != null ? virtual.booleanValue() : super.isVirtual();
    }

    @Override
    public boolean isCacheable() {
        return true;
    }

    @Override
    public boolean isGlobalID() {
        return true;
    }

    @Override
    public boolean isSubscribed() {
        return attributesProvider != null ? attributesProvider.isSubscribed(this) : super.isSubscribed();
    }

    @Override
    public UsedForSync getUsedForSync() {
        return attributesProvider != null ? attributesProvider.getUsedForSync(this) :  super.getUsedForSync();
    }
}
