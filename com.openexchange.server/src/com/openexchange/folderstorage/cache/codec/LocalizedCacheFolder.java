/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.folderstorage.cache.codec;

import java.util.Locale;
import java.util.Optional;
import com.openexchange.folderstorage.LocalizableNameFolder;
import com.openexchange.folderstorage.NameTranslatorFactory;
import com.openexchange.folderstorage.UserSensitiveAttributesProvider;


/**
 * {@link LocalizedCacheFolder} - The cache folder that is localize-aware.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
class LocalizedCacheFolder extends CachedFolder implements LocalizableNameFolder {

    /** The serial version UID */
    private static final long serialVersionUID = 4718560039334224814L;

    /** The context identifier */
    protected final int contextId;

    /** The name translator factory */
    protected final NameTranslatorFactory nameTranslatorFactory;

    /**
     * Initializes a new instance of {@link LocalizedCacheFolder}.
     *
     * @param contextId The context identifier
     * @param nameTranslatorFactory The name translator factory
     * @param attributesProvider The attributes provider to use or <code>null</code>
     */
    LocalizedCacheFolder(int contextId, NameTranslatorFactory nameTranslatorFactory, UserSensitiveAttributesProvider attributesProvider) {
        super(attributesProvider);
        this.contextId = contextId;
        this.nameTranslatorFactory = nameTranslatorFactory;
    }

    @Override
    public Optional<NameTranslatorFactory> getNameTranslatorFactory() {
        return Optional.of(nameTranslatorFactory);
    }

    @Override
    public String getLocalizedName(final Locale locale) {
        return nameTranslatorFactory.getNameTranslator(contextId).translate(getName(), locale);
    }

}
