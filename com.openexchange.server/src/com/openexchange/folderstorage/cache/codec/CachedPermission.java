/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.folderstorage.cache.codec;

import com.openexchange.folderstorage.FolderPermissionType;
import com.openexchange.folderstorage.Permission;
import com.openexchange.groupware.EntityInfo;

/**
 * {@link CachedPermission} - A permission implementation used by deserialization in cache codec.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
class CachedPermission implements Permission {

    private static final long serialVersionUID = 5750588294780470850L;

    private int entity;
    private boolean group;
    private String identifier;
    private String legator;
    private FolderPermissionType folderPermissionType;
    private boolean admin;
    private int fp;
    private int rp;
    private int wp;
    private int dp;
    private int system;
    private EntityInfo entityInfo;

    /**
     * Initializes a new instance of {@link CachedPermission}.
     *
     * @param entity The entity identifier
     * @param group The group flag
     * @param identifier The qualified identifier of the entity
     * @param legator The legator identifier (identifier of the sharing folder)
     * @param folderPermissionType The permission type
     * @param admin The amdinistrator flag
     * @param fp The folder permission
     * @param rp The read permission
     * @param wp The write permission
     * @param dp The delete permission
     * @param system The system bitmask
     * @param entityInfo The entity information
     */
    CachedPermission(int entity, boolean group, String identifier, String legator, FolderPermissionType folderPermissionType, boolean admin, int fp, int rp, int wp, int dp, int system, EntityInfo entityInfo) { // NOSONARLINT
        super();
        this.entity = entity;
        this.group = group;
        this.identifier = identifier;
        this.legator = legator;
        this.folderPermissionType = folderPermissionType;
        this.admin = admin;
        this.fp = fp;
        this.rp = rp;
        this.wp = wp;
        this.dp = dp;
        this.system = system;
        this.entityInfo = entityInfo;
    }

    @Override
    public Object clone() { // NOSONARLINT
        try {
            CachedPermission clone = (CachedPermission) super.clone();
            clone.identifier = getIdentifier();
            clone.entity = getEntity();
            clone.entityInfo = getEntityInfo();
            clone.group = isGroup();
            clone.system = getSystem();
            clone.folderPermissionType = getType();
            clone.legator = getPermissionLegator();
            clone.admin = isAdmin();
            clone.fp = getFolderPermission();
            clone.rp = getReadPermission();
            clone.wp = getWritePermission();
            clone.dp = getDeletePermission();
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new InternalError("Error although Cloneable is implemented", e);
        }
    }

    @Override
    public int getEntity() {
        return entity;
    }

    @Override
    public boolean isGroup() {
        return group;
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public String getPermissionLegator() {
        return legator;
    }

    @Override
    public FolderPermissionType getType() {
        return folderPermissionType;
    }

    @Override
    public boolean isAdmin() {
        return admin;
    }

    @Override
    public int getFolderPermission() {
        return fp;
    }

    @Override
    public int getReadPermission() {
        return rp;
    }

    @Override
    public int getWritePermission() {
        return wp;
    }

    @Override
    public int getDeletePermission() {
        return dp;
    }

    @Override
    public int getSystem() {
        return system;
    }

    @Override
    public EntityInfo getEntityInfo() {
        return entityInfo;
    }

    @Override
    public void setSystem(int system) {
        this.system = system;
    }

    @Override
    public void setType(FolderPermissionType type) {
        this.folderPermissionType = type;
    }

    @Override
    public void setPermissionLegator(String legator) {
        this.legator = legator;
    }

    @Override
    public void setGroup(boolean group) {
        this.group = group;
    }

    @Override
    public boolean isVisible() {
        return fp >= Permission.READ_FOLDER;
    }

    @Override
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public void setEntity(int entity) {
        this.entity = entity;
    }

    @Override
    public void setEntityInfo(EntityInfo entityInfo) {
        this.entityInfo = entityInfo;
    }

    @Override
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    @Override
    public void setFolderPermission(int permission) {
        this.fp = permission;
    }

    @Override
    public void setReadPermission(int permission) {
        this.rp = permission;
    }

    @Override
    public void setWritePermission(int permission) {
        this.wp = permission;
    }

    @Override
    public void setDeletePermission(int permission) {
        this.dp = permission;
    }

    @Override
    public void setAllPermissions(int folderPermission, int readPermission, int writePermission, int deletePermission) {
        this.fp = folderPermission;
        this.rp = readPermission;
        this.wp = writePermission;
        this.dp = deletePermission;
    }

    @Override
    public void setMaxPermissions() {
        this.fp = Permission.MAX_PERMISSION;
        this.rp = Permission.MAX_PERMISSION;
        this.wp = Permission.MAX_PERMISSION;
        this.dp = Permission.MAX_PERMISSION;
        admin = true;
    }

    @Override
    public void setNoPermissions() {
        this.fp = Permission.NO_PERMISSIONS;
        this.rp = Permission.NO_PERMISSIONS;
        this.wp = Permission.NO_PERMISSIONS;
        this.dp = Permission.NO_PERMISSIONS;
        admin = false;
    }
}
