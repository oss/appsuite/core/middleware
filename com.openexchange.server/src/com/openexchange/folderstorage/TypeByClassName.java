/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.folderstorage;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.slf4j.Logger;
import com.openexchange.java.Strings;

/**
 * {@link TypeByClassName} - Static registry that maps instances of {@link Type} to class name of implementing instance.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.0.0
 */
public final class TypeByClassName {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(TypeByClassName.class);

    /**
     * Initializes a new {@link TypeByClassName}.
     */
    private TypeByClassName() {
        super();
    }

    private static final ConcurrentMap<String, Entry> TYPE_BY_CLASS_NAME = new ConcurrentHashMap<>(32);
    private static final ConcurrentMap<Integer, Type> TYPE_BY_NUMBER = new ConcurrentHashMap<>(32);

    static {
        registerType(com.openexchange.folderstorage.addressbook.AddressbookType.getInstance());
        registerType(com.openexchange.folderstorage.type.CalendarType.getInstance());
        registerType(com.openexchange.folderstorage.type.DocumentsType.getInstance());
        registerType(com.openexchange.folderstorage.type.FileStorageType.getInstance());
        registerType(com.openexchange.folderstorage.type.MailType.getInstance());
        registerType(com.openexchange.folderstorage.type.MessagingType.getInstance());
        registerType(com.openexchange.folderstorage.type.MusicType.getInstance());
        registerType(com.openexchange.folderstorage.type.PicturesType.getInstance());
        registerType(com.openexchange.folderstorage.type.PrivateType.getInstance());
        registerType(com.openexchange.folderstorage.type.PublicType.getInstance());
        registerType(com.openexchange.folderstorage.type.SharedType.getInstance());
        registerType(com.openexchange.folderstorage.type.SystemType.getInstance());
        registerType(com.openexchange.folderstorage.type.TemplatesType.getInstance());
        registerType(com.openexchange.folderstorage.type.TrashType.getInstance());
        registerType(com.openexchange.folderstorage.type.VideosType.getInstance());
    }

    /**
     * Adds given type to this registry.
     *
     * @param type The type to add
     * @return <code>true</code> if successfully added; otherwise <code>false</code>
     */
    public static boolean registerType(Type type) {
        if (type == null) {
            return false;
        }

        boolean registered = TYPE_BY_CLASS_NAME.putIfAbsent(type.getClass().getName(), new Entry(type, type.getRegistrationNumber())) == null;
        if (registered) {
            registered = TYPE_BY_NUMBER.putIfAbsent(Integer.valueOf(type.getRegistrationNumber()), type) == null;
            if (!registered) {
                LOG.warn("Detected duplicate folder type registration number for class {}: {}", type.getClass().getName(), Integer.valueOf(type.getRegistrationNumber()));
                TYPE_BY_CLASS_NAME.remove(type.getClass().getName());
            }
        } else {
            LOG.warn("Detected duplicate folder type for: {}", type.getClass().getName());
        }
        return registered;
    }

    /**
     * Gets the instance of <code>Type</code> for specified class name.
     *
     * @param className The class name
     * @return The instance of <code>Type</code> or <code>null</code>
     */
    public static Type getTypeByClassName(String className) {
        if (Strings.isEmpty(className)) {
            return null;
        }

        Entry entry = TYPE_BY_CLASS_NAME.get(className);
        return entry == null ? null : entry.type;
    }

    /**
     * Gets the instance of <code>Type</code> for specified registration number.
     *
     * @param number The registration number
     * @return The instance of <code>Type</code> or <code>null</code>
     */
    public static Type getTypeByNumber(int number) {
        return number <= 0 ? null : TYPE_BY_NUMBER.get(Integer.valueOf(number));
    }

    /**
     * Gets the (positive) registration number for specified type.
     *
     * @param type The type
     * @return The registration number or <code>0</code>
     */
    public static int getRegistrationNumberByType(Type type) {
        return type == null ? 0 : getRegistrationNumberByClassName(type.getClass().getName());
    }

    /**
     * Gets the registration number for specified class name.
     *
     * @param className The class name
     * @return The registration number or <code>0</code>
     */
    public static int getRegistrationNumberByClassName(String className) {
        if (Strings.isEmpty(className)) {
            return 0;
        }

        Entry entry = TYPE_BY_CLASS_NAME.get(className);
        return entry == null ? 0 : entry.number;
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    private static record Entry(Type type, int number) {
        // Nothing
    }

}
