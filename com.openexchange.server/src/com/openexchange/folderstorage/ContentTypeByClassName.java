/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.folderstorage;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.slf4j.Logger;
import com.openexchange.folderstorage.database.contentType.UnboundContentType;
import com.openexchange.java.Strings;

/**
 * {@link ContentTypeByClassName} - Static registry that maps instances of {@link ContentType} to class name of implementing instance.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class ContentTypeByClassName {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ContentTypeByClassName.class);

    /**
     * Initializes a new {@link ContentTypeByClassName}.
     */
    private ContentTypeByClassName() {
        super();
    }

    private static final ConcurrentMap<String, Entry> CONTENT_TYPE_BY_CLASS_NAME = new ConcurrentHashMap<>(32);
    private static final ConcurrentMap<Integer, ContentType> CONTENT_TYPE_BY_NUMBER = new ConcurrentHashMap<>(32);

    static {
        registerContentType(SystemContentType.getInstance());
        registerContentType(UnboundContentType.getInstance());
    }

    /**
     * Adds given content type to this registry.
     *
     * @param contentType The content type to add
     * @return <code>true</code> if successfully added; otherwise <code>false</code>
     */
    public static boolean registerContentType(ContentType contentType) {
        if (contentType == null) {
            return false;
        }

        int number = contentType.getRegistrationNumber();
        boolean registered = CONTENT_TYPE_BY_CLASS_NAME.putIfAbsent(contentType.getClass().getName(), new Entry(contentType, number)) == null;
        if (registered) {
            registered = CONTENT_TYPE_BY_NUMBER.putIfAbsent(Integer.valueOf(number), contentType) == null;
            if (!registered) {
                LOG.warn("Detected duplicate folder content type registration number for class {}: {}", contentType.getClass().getName(), Integer.valueOf(number));
                CONTENT_TYPE_BY_CLASS_NAME.remove(contentType.getClass().getName());
            }
        } else {
            LOG.warn("Detected duplicate folder content type for: {}", contentType.getClass().getName());
        }
        return registered;
    }

    /**
     * Gets the instance of <code>ContentType</code> for specified class name.
     *
     * @param className The class name
     * @return The instance of <code>ContentType</code> or <code>null</code>
     */
    public static ContentType getContentTypeByClassName(String className) {
        if (Strings.isEmpty(className)) {
            return null;
        }

        Entry entry = CONTENT_TYPE_BY_CLASS_NAME.get(className);
        return entry == null ? null : entry.contentType;
    }

    /**
     * Gets the instance of <code>ContentType</code> for specified registration number.
     *
     * @param number The registration number
     * @return The instance of <code>ContentType</code> or <code>null</code>
     */
    public static ContentType getContentTypeByNumber(int number) {
        return number <= 0 ? null : CONTENT_TYPE_BY_NUMBER.get(Integer.valueOf(number));
    }

    /**
     * Gets the (positive) registration number for specified content type.
     *
     * @param contentType The content type
     * @return The registration number or <code>0</code>
     */
    public static int getRegistrationNumberByContentType(ContentType contentType) {
        return contentType == null ? 0 : getRegistrationNumberByClassName(contentType.getClass().getName());
    }

    /**
     * Gets the registration number for specified class name.
     *
     * @param className The class name
     * @return The registration number or <code>0</code>
     */
    public static int getRegistrationNumberByClassName(String className) {
        if (Strings.isEmpty(className)) {
            return 0;
        }

        Entry entry = CONTENT_TYPE_BY_CLASS_NAME.get(className);
        return entry == null ? 0 : entry.number;
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    private static record Entry(ContentType contentType, int number) {
        // Nothing
    }

}
