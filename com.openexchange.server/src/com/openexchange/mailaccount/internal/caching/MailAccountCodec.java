/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.mailaccount.internal.caching;

import java.util.Map;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;
import com.openexchange.mailaccount.Account;
import com.openexchange.mailaccount.MailAccount;
import com.openexchange.mailaccount.TransportAuth;
import com.openexchange.mailaccount.internal.AbstractMailAccount;
import com.openexchange.mailaccount.internal.CustomMailAccount;
import com.openexchange.mailaccount.internal.DefaultMailAccount;

/**
 * {@link MailAccountCodec} is a codec for {@link MailAccount}s
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class MailAccountCodec extends AbstractJSONObjectCacheValueCodec<MailAccount> {

    private static final UUID UID = UUID.fromString("340e5b36-a4d5-4ca8-a78e-8f04a2363540");

    private static final String ID = "i";
    private static final String NAME = "n";
    private static final String USER_ID = "u";

    // Mail Account fields
    private static final String LOGIN = "l";
    private static final String PASSWORD = "p";
    private static final String MAIL_SERVER = "mSrvr";
    private static final String MAIL_PORT = "mPort";
    private static final String MAIL_PROTOCOL = "mPrtcl";
    private static final String MAIL_SECURE = "mSec";
    private static final String MAIL_START_TLS = "mSTls";
    private static final String MAIL_OAUTH_ID = "mOAuthId";
    private static final String MAIL_DISABLED = "mDsbld";
    private static final String SPAM_HANDLER = "spmH";
    private static final String DRAFTS = "drfts";
    private static final String SENT = "snt";
    private static final String SPAM = "spm";
    private static final String TRASH = "trsh";
    private static final String ARCHIVE = "rchiv";
    private static final String SCHEDULED = "sched";
    private static final String CONFIRMED_HAM = "cHam";
    private static final String CONFIRMED_SPAM = "cSpm";
    private static final String UNIFIED_INBOX_ENABLED = "unif";
    private static final String TRASH_FULL_NAME = "trshF";
    private static final String ARCHIVE_FULL_NAME = "rchivF";
    private static final String SCHEDULED_FULL_NAME = "schedF";
    private static final String SENT_FULL_NAME = "sntF";
    private static final String DRAFTS_FULL_NAME = "drftsF";
    private static final String SPAM_FULL_NAME = "spmF";
    private static final String CONFIRMED_SPAM_FULL_NAME = "cSpmF";
    private static final String CONFIRMED_HAM_FULL_NAME = "cHamF";
    private static final String ROOT_FOLDER = "root";
    private static final String PROPERTIES = "ps";

    // Account fields
    private static final String PRIMARY_ADDRESS = "prim";
    private static final String PERSONAL = "prsnl";
    private static final String REPLY_TO = "rplyT";
    private static final String TRANSPORT_AUTH = "tAuth";
    private static final String TRANSPORT_LOGIN = "tLogin";
    private static final String TRANSPORT_PASSWORD = "tPass";
    private static final String TRANSPORT_PORT = "tPort";
    private static final String TRANSPORT_PROTOCOL = "tPrtcl";
    private static final String TRANSPORT_SERVER = "tSrvr";
    private static final String TRANSPORT_SECURE = "tSec";
    private static final String TRANSPORT_START_TLS = "tSTls";
    private static final String TRANSPORT_OAUTH_ID = "tOAuthId";
    private static final String TRANSPORT_DISABLED = "tDsbld";
    private static final String SECONDARY_ACCOUNT = "sAcc";
    private static final String DEACTIVATED = "d";
    private static final String TRANSPORT_PROPERTIES = "tPs";

    /**
     * Initializes a new {@link MailAccountCodec}.
     */
    public MailAccountCodec() {
        super();
    }

    @Override
    public UUID getCodecId() {
        return UID;
    }

    @Override
    protected JSONObject writeJson(MailAccount mailAccount) throws Exception {
        JSONObject result = new JSONObject();
        result.put(ID, mailAccount.getId());
        result.putOpt(NAME, mailAccount.getName());
        result.put(USER_ID, mailAccount.getUserId());

        result.putOpt(LOGIN, mailAccount.getLogin());
        result.putOpt(PASSWORD, mailAccount.getPassword());
        result.putOpt(MAIL_SERVER, mailAccount.getMailServer());
        result.put(MAIL_PORT, mailAccount.getMailPort());
        result.putOpt(MAIL_PROTOCOL, mailAccount.getMailProtocol());
        putOpt(result, MAIL_SECURE, mailAccount.isMailSecure());
        putOpt(result, MAIL_START_TLS, mailAccount.isMailStartTls());
        result.put(MAIL_OAUTH_ID, mailAccount.getMailOAuthId());
        putOpt(result, MAIL_DISABLED, mailAccount.isMailDisabled());
        result.putOpt(SPAM_HANDLER, mailAccount.getSpamHandler());
        result.putOpt(DRAFTS, mailAccount.getDrafts());
        result.putOpt(SENT, mailAccount.getSent());
        result.putOpt(SPAM, mailAccount.getSpam());
        result.putOpt(TRASH, mailAccount.getTrash());
        result.putOpt(ARCHIVE, mailAccount.getArchive());
        result.putOpt(SCHEDULED, mailAccount.getScheduled());
        result.putOpt(CONFIRMED_HAM, mailAccount.getConfirmedHam());
        result.putOpt(CONFIRMED_SPAM, mailAccount.getConfirmedSpam());
        result.put(UNIFIED_INBOX_ENABLED, mailAccount.isUnifiedINBOXEnabled());
        result.putOpt(TRASH_FULL_NAME, mailAccount.getTrashFullname());
        result.putOpt(ARCHIVE_FULL_NAME, mailAccount.getArchiveFullname());
        result.putOpt(SCHEDULED_FULL_NAME, mailAccount.getScheduledFullname());
        result.putOpt(SENT_FULL_NAME, mailAccount.getSentFullname());
        result.putOpt(DRAFTS_FULL_NAME, mailAccount.getDraftsFullname());
        result.putOpt(SPAM_FULL_NAME, mailAccount.getSpamFullname());
        result.putOpt(CONFIRMED_SPAM_FULL_NAME, mailAccount.getConfirmedSpamFullname());
        result.putOpt(CONFIRMED_HAM_FULL_NAME, mailAccount.getConfirmedHamFullname());
        result.put(ROOT_FOLDER, mailAccount.getRootFolder());
        putOpt(result, PROPERTIES, mailAccount.getProperties());

        result.putOpt(PRIMARY_ADDRESS, mailAccount.getPrimaryAddress());
        result.putOpt(PERSONAL, mailAccount.getPersonal());
        result.putOpt(REPLY_TO, mailAccount.getReplyTo());
        TransportAuth transportAuth = mailAccount.getTransportAuth();
        if (null != transportAuth) {
            result.put(TRANSPORT_AUTH, transportAuth.getId());
        }
        result.putOpt(TRANSPORT_LOGIN, mailAccount.getTransportLogin());
        result.putOpt(TRANSPORT_PASSWORD, mailAccount.getTransportPassword());
        result.put(TRANSPORT_PORT, mailAccount.getTransportPort());
        result.putOpt(TRANSPORT_PROTOCOL, mailAccount.getTransportProtocol());
        result.putOpt(TRANSPORT_SERVER, mailAccount.getTransportServer());
        putOpt(result, TRANSPORT_SECURE, mailAccount.isTransportSecure());
        putOpt(result, TRANSPORT_START_TLS, mailAccount.isTransportStartTls());
        result.put(TRANSPORT_OAUTH_ID, mailAccount.getTransportOAuthId());
        putOpt(result, TRANSPORT_DISABLED, mailAccount.isTransportDisabled());
        putOpt(result, SECONDARY_ACCOUNT, mailAccount.isSecondaryAccount());
        putOpt(result, DEACTIVATED, mailAccount.isDeactivated());
        putOpt(result, TRANSPORT_PROPERTIES, mailAccount.getTransportProperties());
        return result;
    }

    private static void putOpt(JSONObject jsonObject, String key, boolean value) throws JSONException {
        if (value) {
            jsonObject.put(key, value);
        }
    }

    private static void putOpt(JSONObject jsonObject, String key, Map<String, String> value) throws JSONException {
        if (null != value && false == value.isEmpty()) {
            jsonObject.put(key, value);
        }
    }

    @Override
    protected MailAccount parseJson(JSONObject jsonObject) throws Exception {
        int id = jsonObject.getInt(ID);
        AbstractMailAccount result = Account.DEFAULT_ID == id ? new DefaultMailAccount() : new CustomMailAccount(id);
        result.setName(jsonObject.optString(NAME, null));
        result.setUserId(jsonObject.optInt(USER_ID));

        result.setLogin(jsonObject.optString(LOGIN, null));
        result.setPassword(jsonObject.optString(PASSWORD, null));
        result.setMailServer(jsonObject.optString(MAIL_SERVER, null));
        result.setMailPort(jsonObject.optInt(MAIL_PORT));
        result.setMailProtocol(jsonObject.optString(MAIL_PROTOCOL, null));
        result.setMailSecure(jsonObject.optBoolean(MAIL_SECURE));
        result.setMailStartTls(jsonObject.optBoolean(MAIL_START_TLS));
        result.setMailOAuthId(jsonObject.optInt(MAIL_OAUTH_ID));
        result.setMailDisabled(jsonObject.optBoolean(MAIL_DISABLED));
        result.setSpamHandler(jsonObject.optString(SPAM_HANDLER, null));
        result.setDrafts(jsonObject.optString(DRAFTS, null));
        result.setSent(jsonObject.optString(SENT, null));
        result.setSpam(jsonObject.optString(SPAM, null));
        result.setTrash(jsonObject.optString(TRASH, null));
        result.setArchive(jsonObject.optString(ARCHIVE, null));
        result.setScheduled(jsonObject.optString(SCHEDULED, null));
        result.setConfirmedHam(jsonObject.optString(CONFIRMED_HAM, null));
        result.setConfirmedSpam(jsonObject.optString(CONFIRMED_SPAM, null));
        result.setUnifiedINBOXEnabled(jsonObject.optBoolean(UNIFIED_INBOX_ENABLED));
        result.setTrashFullname(jsonObject.optString(TRASH_FULL_NAME, null));
        result.setArchiveFullname(jsonObject.optString(ARCHIVE_FULL_NAME, null));
        result.setScheduledFullname(jsonObject.optString(SCHEDULED_FULL_NAME, null));
        result.setSentFullname(jsonObject.optString(SENT_FULL_NAME, null));
        result.setDraftsFullname(jsonObject.optString(DRAFTS_FULL_NAME, null));
        result.setSpamFullname(jsonObject.optString(SPAM_FULL_NAME, null));
        result.setConfirmedSpamFullname(jsonObject.optString(CONFIRMED_SPAM_FULL_NAME, null));
        result.setConfirmedHamFullname(jsonObject.optString(CONFIRMED_HAM_FULL_NAME, null));
        result.setRootFolder(jsonObject.optString(ROOT_FOLDER, null));
        JSONObject optPropertiesMap = jsonObject.optJSONObject(PROPERTIES);
        if (null != optPropertiesMap) {
            optPropertiesMap.asMap().forEach((k, v) -> result.addProperty(k, String.valueOf(v)));
        }

        result.setPrimaryAddress(jsonObject.optString(PRIMARY_ADDRESS, null));
        result.setPersonal(jsonObject.optString(PERSONAL, null));
        result.setReplyTo(jsonObject.optString(REPLY_TO, null));
        result.setTransportAuth(TransportAuth.transportAuthFor(jsonObject.optString(TRANSPORT_AUTH, null)));
        result.setTransportLogin(jsonObject.optString(TRANSPORT_LOGIN, null));
        result.setTransportPassword(jsonObject.optString(TRANSPORT_PASSWORD, null));
        result.setTransportPort(jsonObject.optInt(TRANSPORT_PORT));
        result.setTransportProtocol(jsonObject.optString(TRANSPORT_PROTOCOL, null));
        result.setTransportServer(jsonObject.optString(TRANSPORT_SERVER, null));
        result.setTransportSecure(jsonObject.optBoolean(TRANSPORT_SECURE));
        result.setTransportStartTls(jsonObject.optBoolean(TRANSPORT_START_TLS));
        result.setTransportOAuthId(jsonObject.optInt(TRANSPORT_OAUTH_ID, -1));
        result.setTransportDisabled(jsonObject.optBoolean(TRANSPORT_DISABLED));
        result.setSecondaryAccount(jsonObject.optBoolean(SECONDARY_ACCOUNT));
        result.setDeactivated(jsonObject.optBoolean(DEACTIVATED));
        JSONObject optTransportPropertiesMap = jsonObject.optJSONObject(TRANSPORT_PROPERTIES);
        if (null != optTransportPropertiesMap) {
            optTransportPropertiesMap.asMap().forEach((k, v) -> result.addTransportProperty(k, String.valueOf(v)));
        }
        return result;
    }

}
