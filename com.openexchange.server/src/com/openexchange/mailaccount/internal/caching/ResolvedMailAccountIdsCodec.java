/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.mailaccount.internal.caching;

import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONServices;
import com.openexchange.cache.v2.codec.json.AbstractSimpleJSONValueCacheValueCodec;
import com.openexchange.mailaccount.internal.IdAndUser;

/**
 * {@link ResolvedMailAccountIdsCodec} - The codec for resolved mail accounts from either login or primary address string.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class ResolvedMailAccountIdsCodec extends AbstractSimpleJSONValueCacheValueCodec<List<IdAndUser>, JSONArray> {

    private static final UUID CODEC_ID = UUID.fromString("5ababac4-3a7d-42eb-870d-be6de53c0d73");

    /**
     * Initializes a new {@link ResolvedMailAccountIdsCodec}.
     */
    public ResolvedMailAccountIdsCodec() {
        super();
    }

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected boolean isAscii() {
        return true;
    }

    @Override
    protected JSONArray newJsonValueFrom(InputStream data) throws JSONException {
        return JSONServices.parseArray(data);
    }

    @Override
    protected void writeJson(List<IdAndUser> value, Writer writer) throws Exception {
        writer.append('[');

        boolean first = true;
        for (IdAndUser iau : value) {
            if (first) {
                first = false;
            } else {
                writer.append(',');
            }

            writer.append('[');

            writer.append(Integer.toString(iau.getId()));
            writer.append(',');
            writer.append(Integer.toString(iau.getUserId()));

            writer.append(']');
        }

        writer.append(']');
    }

    @Override
    protected List<IdAndUser> parseJson(JSONArray jArray) throws Exception {
        int length = jArray.length();
        if (length <= 0) {
            return Collections.emptyList();
        }

        List<IdAndUser> retval = new ArrayList<>(length);
        for (Object object : jArray) {
            JSONArray nested = (JSONArray) object;
            retval.add(new IdAndUser(nested.getInt(0), nested.getInt(1)));
        }
        return retval;
    }

}
