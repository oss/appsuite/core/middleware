/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mailaccount.internal;

import org.osgi.framework.BundleContext;
import com.openexchange.pubsub.AbstractTrackingChannelListener;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.Message;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.session.UserAndContext;

/**
 * {@link Pop3SessionCacheInvalidator}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.2
 */
public class Pop3SessionCacheInvalidator extends AbstractTrackingChannelListener<UserAndContext> {

    /**
     * Initializes a new {@link Pop3SessionCacheInvalidator}.
     */
    public Pop3SessionCacheInvalidator(BundleContext context) {
        super(context);
    }

    @Override
    public void onMessage(Message<UserAndContext> message) {
        if (message.isRemote()) {
            // Remotely received
            UserAndContext userAndContext = message.getData();
            RdbMailAccountStorage.dropPOP3StorageFolders(userAndContext.getUserId(), userAndContext.getContextId(), false);
        }
    }

    @Override
    protected Channel<UserAndContext> getChannel(PubSubService service) {
        return RdbMailAccountStorage.getChannel(service);
    }

}
