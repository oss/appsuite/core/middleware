/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mailaccount.internal;

import static com.openexchange.java.Autoboxing.I;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import com.openexchange.exception.OXException;
import com.openexchange.mailaccount.MailAccount;
import com.openexchange.mailaccount.MailAccountDeleteListener;
import com.openexchange.mailaccount.MailAccountExceptionCodes;
import com.openexchange.mailaccount.MailAccountStorageService;
import com.openexchange.mailaccount.MailAccounts;
import com.openexchange.mailaccount.TransportAccount;
import com.openexchange.mailaccount.utils.MailAccountUtils;
import com.openexchange.oauth.OAuthAccount;
import com.openexchange.oauth.OAuthConstants;
import com.openexchange.oauth.OAuthService;
import com.openexchange.oauth.scope.OAuthScope;
import com.openexchange.oauth.scope.OXScope;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.session.Session;

/**
 * {@link OAuthMailAccountDeleteListener}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @since v7.10.3
 */
public class OAuthMailAccountDeleteListener implements MailAccountDeleteListener {

    /** The name for the property holding the OAuth account identifier */
    private static final String PROP_OAUTH_ACCOUNT_ID = "com.openexchange.mailaccount.oauthAccountId";

    /**
     * Initializes a new {@link OAuthMailAccountDeleteListener}.
     */
    public OAuthMailAccountDeleteListener() {
        super();
    }

    @Override
    public void onBeforeMailAccountDeletion(int id, Map<String, Object> eventProps, int userId, int contextId, Connection con) throws OXException {
        if ("oauth".equals(eventProps.get("com.openexchange.mailaccount.deleteOrigin"))) {
            return;
        }

        MailAccountStorageService mass = ServerServiceRegistry.getInstance().getService(MailAccountStorageService.class);
        if (null == mass) {
            return;
        }
        Optional<MailAccount> optMailAccount = loadMailAccount(id, userId, contextId, con, mass);
        if (optMailAccount.isPresent()) {
            MailAccount mailAccount = optMailAccount.get();
            if (false == mailAccount.isDefaultAccount() && false == MailAccountUtils.isUnifiedINBOXAccount(mailAccount) && mailAccount.isMailOAuthAble()) {
                int oauthAccountId = mailAccount.getMailOAuthId();
                if (oauthAccountId > 0) {
                    eventProps.put(PROP_OAUTH_ACCOUNT_ID, I(oauthAccountId));
                }
            }
        } else {
            // Check for transport-only account
            Optional<TransportAccount> optTransportAccount = loadTransportAccount(id, userId, contextId, con, mass);
            if (optTransportAccount.isPresent()) {
                TransportAccount transportAccount = optTransportAccount.get();
                if (false == transportAccount.isDefaultAccount() && transportAccount.isTransportOAuthAble()) {
                    int oauthAccountId = transportAccount.getTransportOAuthId();
                    if (oauthAccountId > 0) {
                        eventProps.put(PROP_OAUTH_ACCOUNT_ID, I(oauthAccountId));
                    }
                }
            }
        }
    }

    private static Optional<MailAccount> loadMailAccount(int accountId, int userId, int contextId, Connection con, MailAccountStorageService mass) throws OXException {
        try {
            return Optional.of(mass.getMailAccount(accountId, userId, contextId, con));
        } catch (OXException e) {
            if (MailAccountExceptionCodes.NOT_FOUND.equals(e)) {
                return Optional.empty();
            }
            throw e;
        }
    }

    private static Optional<TransportAccount> loadTransportAccount(int accountId, int userId, int contextId, Connection con, MailAccountStorageService mass) throws OXException {
        try {
            return Optional.of(mass.getTransportAccount(accountId, userId, contextId, con));
        } catch (OXException e) {
            if (MailAccountExceptionCodes.NOT_FOUND.equals(e)) {
                return Optional.empty();
            }
            throw e;
        }
    }

    @Override
    public void onAfterMailAccountDeletion(int id, Map<String, Object> eventProps, int userId, int contextId, Connection con) throws OXException {
        Integer iOAuthAccountId = (Integer) eventProps.get(PROP_OAUTH_ACCOUNT_ID);
        if (iOAuthAccountId == null) {
            // No OAuth account identifier
            return;
        }

        OAuthService oauthService = ServerServiceRegistry.getInstance().getService(OAuthService.class);
        if (null == oauthService) {
            // Missing OAuth service
            return;
        }

        Optional<Session> optionalSession = MailAccounts.tryGetSession(eventProps, userId, contextId);
        if (!optionalSession.isPresent()) {
            // No session available
            return;
        }

        Session session = optionalSession.get();
        session.setParameter("__connection", con);
        try {
            int oauthAccountId = iOAuthAccountId.intValue();
            OAuthAccount oauthAccount = oauthService.getAccount(session, oauthAccountId);

            // Get the enabled scopes...
            Set<OAuthScope> scopes = new HashSet<>();
            for (OAuthScope scope : oauthAccount.getEnabledScopes()) {
                scopes.add(scope);
            }
            // ...and remove the 'mail' scope.
            boolean scopesChanged = false;
            for (Iterator<OAuthScope> it = scopes.iterator(); it.hasNext();) {
                if (OXScope.mail == it.next().getOXScope()) {
                    it.remove();
                    scopesChanged = true;
                }
            }
            if (scopesChanged) {
                // Check if any scope is left
                if (scopes.isEmpty()) {
                    // No scopes anymore. Delete the OAuth account.
                    oauthService.deleteAccount(session, oauthAccountId);
                } else {
                    // Update OAuth the account to drop scope
                    eventProps.put(OAuthConstants.ARGUMENT_SCOPES, scopes);
                    oauthService.updateAccount(session, oauthAccountId, eventProps);
                }
            }
        } finally {
            session.setParameter("__connection", null);
        }
    }
}
