/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mailaccount.internal;

/**
 * {@link IdAndUser} - Simple helper class for an account identifier and user identifier tuple.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class IdAndUser {

    private final int id;
    private final int userId;
    private int hash = 0;

    /**
     * Initializes a new {@link IdAndUser}.
     *
     * @param id The account identifier
     * @param userId The user identifier
     */
    public IdAndUser(int id, int userId) {
        super();
        this.id = id;
        this.userId = userId;
    }

    /**
     * Gets the account identifier.
     *
     * @return The account identifier
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the user identifier
     *
     * @return The user identifier
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Converts this instance to an <code>int</code> array of length 2:
     * <pre>
     *  new int[] { id, userId };
     * </pre>
     *
     * @return The <code>int</code> array
     */
    public int[] toArray() {
        return new int[] { id, userId };
    }

    @Override
    public int hashCode() {
        int result = hash;
        if (result == 0) {
            int prime = 31;
            result = 1;
            result = prime * result + id;
            result = prime * result + userId;
            hash = result;
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        IdAndUser other = (IdAndUser) obj;
        if (id != other.id) {
            return false;
        }
        if (userId != other.userId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("[id=").append(id).append(", userId=").append(userId).append(']').toString();
    }

}
