/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mailaccount.internal;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.database.DatabaseConnectionListeners.addAfterCommitCallbackElseExecute;
import static com.openexchange.database.Databases.autocommit;
import static com.openexchange.database.Databases.rollback;
import static com.openexchange.mail.utils.ProviderUtility.toSocketAddrString;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.codec.IntArrayCacheValueCodec;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.database.DatabaseConnectionListeners;
import com.openexchange.database.DatabaseService;
import com.openexchange.databaseold.Database;
import com.openexchange.event.CommonEvent;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.FolderStorage;
import com.openexchange.folderstorage.cache.memory.FolderMap;
import com.openexchange.folderstorage.cache.memory.FolderMapManagement;
import com.openexchange.folderstorage.outlook.OutlookFolderStorage;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.mail.dataobjects.MailFolder;
import com.openexchange.mail.utils.MailFolderUtility;
import com.openexchange.mail.utils.StorageUtility;
import com.openexchange.mailaccount.Account;
import com.openexchange.mailaccount.Attribute;
import com.openexchange.mailaccount.MailAccount;
import com.openexchange.mailaccount.MailAccountDescription;
import com.openexchange.mailaccount.MailAccountExceptionCodes;
import com.openexchange.mailaccount.MailAccountStorageService;
import com.openexchange.mailaccount.TransportAccount;
import com.openexchange.mailaccount.TransportAccountDescription;
import com.openexchange.mailaccount.UpdateProperties;
import com.openexchange.mailaccount.internal.caching.MailAccountCodec;
import com.openexchange.mailaccount.internal.caching.ResolvedMailAccountIdsCodec;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.threadpool.AbstractTask;
import com.openexchange.threadpool.Task;
import com.openexchange.threadpool.ThreadPools;

/**
 * {@link CachingMailAccountStorage} - The caching implementation of mail account storage.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
final class CachingMailAccountStorage implements MailAccountStorageService {

    // --------------------------------------- Constants for event --------------------------------------------------------------

    private static final String PROP_DRAFTS_FULL_NAME = "com.openexchange.mailaccount.draftsFullName";
    private static final String PROP_SENT_FULL_NAME = "com.openexchange.mailaccount.sentFullName";
    private static final String PROP_SPAM_FULL_NAME = "com.openexchange.mailaccount.spamFullName";
    private static final String PROP_TRASH_FULL_NAME = "com.openexchange.mailaccount.trashFullName";
    private static final String PROP_CONFIRMED_SPAM_FULL_NAME = "com.openexchange.mailaccount.confirmedSpamFullName";
    private static final String PROP_CONFIRMED_HAM_FULL_NAME = "com.openexchange.mailaccount.confirmedHamFullName";

    private static final String PROP_DRAFTS_NAME = "com.openexchange.mailaccount.draftsName";
    private static final String PROP_SENT_NAME = "com.openexchange.mailaccount.sentName";
    private static final String PROP_SPAM_NAME = "com.openexchange.mailaccount.spamName";
    private static final String PROP_TRASH_NAME = "com.openexchange.mailaccount.trashName";
    private static final String PROP_CONFIRMED_SPAM_NAME = "com.openexchange.mailaccount.confirmedSpamName";
    private static final String PROP_CONFIRMED_HAM_NAME = "com.openexchange.mailaccount.confirmedHamName";

    private static final String PROP_ACCOUNT_ID = "com.openexchange.mailaccount.accountId";
    private static final String PROP_USER_ID = "com.openexchange.mailaccount.userId";
    private static final String PROP_CONTEXT_ID = "com.openexchange.mailaccount.contextId";

    private static final String TOPIC_CHANGED_DEFAULT_FOLDERS = "com/openexchange/mailaccount/changeddefaultfolders";

    // ---------------------------------------------------------------------------------------------------------------------------

    /** Options for the cache holding mail account data */
    private static final CacheOptions<MailAccount> MAIL_ACCOUNT_CACHE_OPTIONS = CacheOptions.<MailAccount> builder()
                                                                                            .withCoreModuleName(CoreModuleName.MAIL_ACCOUNT)
                                                                                            .withCodecAndVersion(new MailAccountCodec())
                                                                                            .build();

    /** Options for the cache holding all known account identifiers per user */
    private static final CacheOptions<int[]> USER_ACCOUNTS_CACHE_OPTIONS = CacheOptions.<int[]> builder()
                                                                                       .withCoreModuleName(CoreModuleName.USER_MAIL_ACCOUNTS)
                                                                                       .withCodecAndVersion(IntArrayCacheValueCodec.getInstance())
                                                                                       .build();

    /** Options for the cache holding user-/account-id pairs by login name or mail address, grouped by their context */
    private static final CacheOptions<List<IdAndUser>> RESOLVED_LOGINS_CACHE_OPTIONS = CacheOptions.<List<IdAndUser>> builder()
                                                                                           .withCoreModuleName(CoreModuleName.RESOLVED_MAIL_ACCOUNT_IDS)
                                                                                           .withCodecAndVersion(new ResolvedMailAccountIdsCodec())
                                                                                           .build();

    /**
     * Proxy attribute for the object implementing the persistent methods.
     */
    private final RdbMailAccountStorage delegate;

    /**
     * Initializes a new {@link CachingMailAccountStorage}.
     *
     * @param delegate The database-backed delegate storage
     */
    CachingMailAccountStorage(RdbMailAccountStorage delegate) {
        super();
        this.delegate = delegate;
    }

    /**
     * Optionally gets the cache holding mail account data.
     *
     * @return The cache, or <code>null</code> if not available
     */
    private static Cache<MailAccount> optMailAccountCache() {
        CacheService cacheService = ServerServiceRegistry.getServize(CacheService.class);
        return null == cacheService ? null : cacheService.getCache(MAIL_ACCOUNT_CACHE_OPTIONS);
    }

    private static CacheKey newCacheKey(Cache<MailAccount> mailAccountCache, int contextId, int userId, int id) {
        return mailAccountCache.newKey(mailAccountCache.suffixBuilder().appendHashPart(contextId).append(id).append(userId));
    }

    /**
     * Optionally gets the cache holding all known account identifiers per user.
     *
     * @return The cache, or <code>null</code> if not available
     */
    private static Cache<int[]> optUserAccountsCache() {
        CacheService cacheService = ServerServiceRegistry.getServize(CacheService.class);
        return null == cacheService ? null : cacheService.getCache(USER_ACCOUNTS_CACHE_OPTIONS);
    }

    private static CacheKey accountsCacheKey(Cache<int[]> userAccountsCache, int contextId, int userId) {
        return userAccountsCache.newKey(userAccountsCache.suffixBuilder().appendHashPart(contextId).append(userId));
    }

    /**
     * Optionally gets the cache holding user-/account-id pairs by login name or mail address, grouped by their context
     *
     * @return The cache, or <code>null</code> if not available
     */
    private static Cache<List<IdAndUser>> optResolvedLoginsCache() {
        CacheService cacheService = ServerServiceRegistry.getServize(CacheService.class);
        return null == cacheService ? null : cacheService.getCache(RESOLVED_LOGINS_CACHE_OPTIONS);
    }

    /**
     * Generates a new group cache key for the resolved accounts cache holding user-/account-id pairs by login name or mail address,
     * grouped by their context.
     *
     * @param resolvedLoginsCache The cache
     * @param contextId The context identifier to use as group key
     * @param type The resolve type to distinguish between logins and mail addresses
     * @param pattern The login or mail address string
     * @return The group cache key
     */
    private static CacheKey resolvedLoginsCacheKey(Cache<List<IdAndUser>> resolvedLoginsCache, int contextId, CachedResolveType type, String pattern) {
        return resolvedLoginsCache.newGroupMemberKey(asHashPart(contextId), Integer.toString(type.ordinal()), pattern);
    }

    /**
     * Generates a new group cache key targeting all data grouped by the given context.
     *
     * @param resolvedLoginsCache The cache
     * @param contextId The context identifier to use as group key
     * @return The group cache key
     */
    private static CacheKey resolvedLoginsCacheKey(Cache<List<IdAndUser>> resolvedLoginsCache, int contextId) {
        return resolvedLoginsCache.newGroupKey(asHashPart(contextId));
    }

    private static void postChangedDefaultFolders(int id, int[] indexes, String[] values, boolean isFullName, boolean distributeRemotely, int userId, int contextId) {
        EventAdmin eventAdmin = ServerServiceRegistry.getInstance().getService(EventAdmin.class);
        if (null != eventAdmin) {
            Map<String, Object> properties = HashMap.newHashMap(9);
            properties.put(PROP_CONTEXT_ID, Integer.valueOf(contextId));
            properties.put(PROP_USER_ID, Integer.valueOf(userId));
            properties.put(PROP_ACCOUNT_ID, Integer.valueOf(id));

            if (distributeRemotely) {
                properties.put(CommonEvent.PUBLISH_MARKER, Boolean.TRUE);
            }

            if (null != indexes && null != values) {
                for (int i = 0; i < indexes.length; i++) {
                    switch (indexes[i]) {
                        case StorageUtility.INDEX_DRAFTS:
                            properties.put(isFullName ? PROP_DRAFTS_FULL_NAME : PROP_DRAFTS_NAME, values[i]);
                            break;
                        case StorageUtility.INDEX_SENT:
                            properties.put(isFullName ? PROP_SENT_FULL_NAME : PROP_SENT_NAME, values[i]);
                            break;
                        case StorageUtility.INDEX_SPAM:
                            properties.put(isFullName ? PROP_SPAM_FULL_NAME : PROP_SPAM_NAME, values[i]);
                            break;
                        case StorageUtility.INDEX_TRASH:
                            properties.put(isFullName ? PROP_TRASH_FULL_NAME : PROP_TRASH_NAME, values[i]);
                            break;
                        case StorageUtility.INDEX_CONFIRMED_SPAM:
                            properties.put(isFullName ? PROP_CONFIRMED_SPAM_FULL_NAME : PROP_CONFIRMED_SPAM_NAME, values[i]);
                            break;
                        case StorageUtility.INDEX_CONFIRMED_HAM:
                            properties.put(isFullName ? PROP_CONFIRMED_HAM_FULL_NAME : PROP_CONFIRMED_HAM_NAME, values[i]);
                            break;
                        default:
                            break;
                    }
                }
            }

            eventAdmin.postEvent(new Event(TOPIC_CHANGED_DEFAULT_FOLDERS, properties));
        }
    }

    @Override
    public void invalidateMailAccount(int id, int userId, int contextId) throws OXException {
        Cache<MailAccount> mailAccountCache = optMailAccountCache();
        if (null != mailAccountCache) {
            mailAccountCache.invalidate(newCacheKey(mailAccountCache, contextId, userId, id));
        }
        Cache<int[]> userAccountsCache = optUserAccountsCache();
        if (null != userAccountsCache) {
            userAccountsCache.invalidate(accountsCacheKey(userAccountsCache, contextId, userId));
        }
        Cache<List<IdAndUser>> resolvedLoginsCache = optResolvedLoginsCache();
        if (null != resolvedLoginsCache) {
            resolvedLoginsCache.invalidateGroup(resolvedLoginsCacheKey(resolvedLoginsCache, contextId));
        }

        FolderMap folderMap = FolderMapManagement.getInstance().optFor(userId, contextId);
        if (null != folderMap) {
            String rootId = MailFolderUtility.prepareFullname(id, MailFolder.ROOT_FOLDER_ID);
            folderMap.remove(rootId, FolderStorage.REAL_TREE_ID);
            folderMap.remove(rootId, OutlookFolderStorage.OUTLOOK_TREE_ID);
        }
    }

    @Override
    public void invalidateMailAccounts(int userId, int contextId) throws OXException {
        DatabaseService db = ServerServiceRegistry.getInstance().getService(DatabaseService.class);
        Connection con = db.getWritable(contextId);
        int[] ids;
        try {
            ids = delegate.getUserMailAccountIDs(userId, contextId, con);
        } finally {
            db.backWritableAfterReading(contextId, con);
        }

        Cache<int[]> userAccountsCache = optUserAccountsCache();
        if (null != userAccountsCache) {
            userAccountsCache.invalidate(accountsCacheKey(userAccountsCache, contextId, userId));
        }
        Cache<MailAccount> mailAccountCache = optMailAccountCache();
        if (null != mailAccountCache) {
            mailAccountCache.invalidate(IntStream.of(ids).mapToObj(i -> newCacheKey(mailAccountCache, contextId, userId, i)).toList());
        }
        Cache<List<IdAndUser>> resolvedLoginsCache = optResolvedLoginsCache();
        if (null != resolvedLoginsCache) {
            resolvedLoginsCache.invalidateGroup(resolvedLoginsCacheKey(resolvedLoginsCache, contextId));
        }

        FolderMapManagement.getInstance().dropFor(userId, contextId);
    }

    @Override
    public void clearFullNamesForMailAccount(int id, int userId, int contextId) throws OXException {
        delegate.clearFullNamesForMailAccount(id, userId, contextId);
        invalidateMailAccount(id, userId, contextId);

        postChangedDefaultFolders(id, null, null, false, true, userId, contextId);
    }

    @Override
    public void clearFullNamesForMailAccount(int id, int[] indexes, int userId, int contextId) throws OXException {
        delegate.clearFullNamesForMailAccount(id, indexes, userId, contextId);
        invalidateMailAccount(id, userId, contextId);

        postChangedDefaultFolders(id, null, null, false, true, userId, contextId);
    }

    @Override
    public boolean setFullNamesForMailAccount(int id, int[] indexes, String[] fullNames, int userId, int contextId) throws OXException {
        boolean modified = delegate.setFullNamesForMailAccount(id, indexes, fullNames, userId, contextId);
        if (modified) {
            invalidateMailAccount(id, userId, contextId);
            postChangedDefaultFolders(id, indexes, fullNames, true, true, userId, contextId);
        }

        return modified;
    }

    @Override
    public boolean setNamesForMailAccount(int id, int[] indexes, String[] names, int userId, int contextId) throws OXException {
        boolean modified = delegate.setNamesForMailAccount(id, indexes, names, userId, contextId);
        if (modified) {
            invalidateMailAccount(id, userId, contextId);
            postChangedDefaultFolders(id, indexes, names, false, true, userId, contextId);
        }

        return modified;
    }

    @Override
    public void propagateEvent(com.openexchange.mailaccount.Event event, int id, Map<String, Object> eventProps, int userId, int contextId) throws OXException {
        delegate.propagateEvent(event, id, eventProps, userId, contextId);
    }

    @Override
    public boolean deleteMailAccount(int id, Map<String, Object> properties, int userId, int contextId, boolean deletePrimaryOrSecondary, Connection con) throws OXException {
        dropSessionParameter(userId, contextId);

        boolean deleted = delegate.deleteMailAccount(id, properties, userId, contextId, deletePrimaryOrSecondary, con);
        DatabaseConnectionListeners.addAfterCommitCallbackElseExecute(con, c -> invalidateMailAccount(id, userId, contextId));
        return deleted;
    }

    @Override
    public void deleteAllMailAccounts(int userId, int contextId, Connection con) throws OXException {
        dropSessionParameter(userId, contextId);

        delegate.deleteAllMailAccounts(userId, contextId, con);
        DatabaseConnectionListeners.addAfterCommitCallbackElseExecute(con, c -> invalidateMailAccounts(userId, contextId));
    }

    @Override
    public boolean deleteMailAccount(int id, Map<String, Object> properties, int userId, int contextId, boolean deletePrimaryOrSecondary) throws OXException {
        dropSessionParameter(userId, contextId);

        boolean deleted = delegate.deleteMailAccount(id, properties, userId, contextId, deletePrimaryOrSecondary);
        invalidateMailAccount(id, userId, contextId);
        return deleted;
    }

    @Override
    public boolean deleteMailAccount(int id, Map<String, Object> properties, int userId, int contextId) throws OXException {
        dropSessionParameter(userId, contextId);

        boolean deleted = delegate.deleteMailAccount(id, properties, userId, contextId);
        invalidateMailAccount(id, userId, contextId);
        return deleted;
    }

    private void dropSessionParameter(final int userId, final int contextId) {
        Task<Void> task = new AbstractTask<>() {

            @Override
            public Void call() {
                SessiondService service = ServerServiceRegistry.getInstance().getService(SessiondService.class);
                if (null != service) {
                    for (Session session : service.getLocalSessions(userId, contextId)) {
                        session.setParameter("com.openexchange.mailaccount.unifiedMailEnabled", null);
                    }
                }
                return null;
            }
        };
        ThreadPools.getThreadPool().submit(task);
    }

    @Override
    public int acquireId(int userId, Context ctx) throws OXException {
        return delegate.acquireId(userId, ctx);
    }

    @Override
    public String getDefaultFolderPrefix(Session session) throws OXException {
        return delegate.getDefaultFolderPrefix(session);
    }

    @Override
    public char getDefaultSeparator(Session session) throws OXException {
        return delegate.getDefaultSeparator(session);
    }

    @Override
    public boolean incrementFailedMailAuthCount(int accountId, int userId, int contextId, Exception optReason) throws OXException {
        boolean disabled = delegate.incrementFailedMailAuthCount(accountId, userId, contextId, optReason);
        if (disabled) {
            invalidateMailAccount(accountId, userId, contextId);
        }
        return disabled;
    }

    @Override
    public boolean incrementFailedTransportAuthCount(int accountId, int userId, int contextId, Exception optReason) throws OXException {
        boolean disabled = delegate.incrementFailedTransportAuthCount(accountId, userId, contextId, optReason);
        if (disabled) {
            invalidateMailAccount(accountId, userId, contextId);
        }
        return disabled;
    }

    @Override
    public MailAccount getDefaultMailAccount(int userId, int contextId) throws OXException {
        return getMailAccount(Account.DEFAULT_ID, userId, contextId);
    }

    @Override
    public boolean existsMailAccount(int id, int userId, int contextId) throws OXException {
        Cache<MailAccount> mailAccountCache = optMailAccountCache();
        if (null != mailAccountCache && mailAccountCache.exists(newCacheKey(mailAccountCache, contextId, userId, id))) {
            return true;
        }

        // Check in listing of account IDs
        Cache<int[]> userAccountsCache = optUserAccountsCache();
        if (null != userAccountsCache) {
            int[] accounts = userAccountsCache.get(accountsCacheKey(userAccountsCache, contextId, userId));
            if (null != accounts) {
                return Arrays.stream(accounts).anyMatch(accId -> accId == id);
            }
        }

        return delegate.existsMailAccount(id, userId, contextId);
    }

    @Override
    public boolean isSecondaryMailAccount(int id, int userId, int contextId) throws OXException {
        if (id < 0 || id == Account.DEFAULT_ID) {
            return false;
        }
        return getMailAccount(id, userId, contextId).isSecondaryAccount();
    }

    @Override
    public boolean isDeactivatedMailAccount(int id, int userId, int contextId) throws OXException {
        return getMailAccount(id, userId, contextId).isDeactivated();
    }

    @Override
    public MailAccount getMailAccount(int id, int userId, int contextId) throws OXException {
        Cache<MailAccount> mailAccountCache = optMailAccountCache();
        if (null == mailAccountCache) {
            return delegate.getMailAccount(id, userId, contextId);
        }
        CacheKey key = newCacheKey(mailAccountCache, contextId, userId, id);
        return mailAccountCache.get(key, k -> delegate.getMailAccount(id, userId, contextId));
    }

    @Override
    public int getByPrimaryAddress(String primaryAddress, int userId, int contextId) throws OXException {
        return delegate.getByPrimaryAddress(primaryAddress, userId, contextId);
    }

    @Override
    public int getTransportByPrimaryAddress(String primaryAddress, int userId, int contextId) throws OXException {
        return delegate.getTransportByPrimaryAddress(primaryAddress, userId, contextId);
    }

    @Override
    public TransportAccount getTransportByReference(String reference, int userId, int contextId) throws OXException {
        return delegate.getTransportByReference(reference, userId, contextId);
    }

    @Override
    public int[] getByHostNames(Collection<String> hostNames, int userId, int contextId) throws OXException {
        return delegate.getByHostNames(hostNames, userId, contextId);
    }

    @Override
    public MailAccount[] getUserMailAccounts(int userId, int contextId, Connection con) throws OXException {
        int[] ids = delegate.getUserMailAccountIDs(userId, contextId, con);
        MailAccount[] accounts = new MailAccount[ids.length];
        for (int i = 0; i < accounts.length; i++) {
            accounts[i] = getUnfilteredMailAccount(ids[i], userId, contextId, con);
        }
        return accounts;
    }

    @Override
    public MailAccount[] getUserDefaultAndSecondaryMailAccounts(int userId, int contextId, Connection con) throws OXException {
        int[] ids = delegate.getUserDefaultAndSecondaryMailAccountIDs(userId, contextId, con);
        MailAccount[] accounts = new MailAccount[ids.length];
        for (int i = 0; i < accounts.length; i++) {
            MailAccount tmp = getUnfilteredMailAccount(ids[i], userId, contextId, con);
            accounts[i] = tmp;
        }
        return accounts;
    }

    @Override
    public TransportAccount[] getUserTransportAccounts(int userId, int contextId, Connection con) throws OXException {
        int[] ids = delegate.getUserTransportAccountIDs(userId, contextId, con);
        TransportAccount[] accounts = new TransportAccount[ids.length];
        for (int i = 0; i < accounts.length; i++) {
            accounts[i] = getTransportAccount(ids[i], userId, contextId, con);
        }
        return accounts;
    }

    @Override
    public List<MailAccount> getUserMailAccounts(int contextId) throws OXException {
        return delegate.getUserMailAccounts(contextId);
    }

    @Override
    public MailAccount getRawMailAccount(int id, int userId, int contextId) throws OXException {
        return delegate.getRawMailAccount(id, userId, contextId);
    }

    @Override
    public MailAccount getMailAccount(int id, int userId, int contextId, Connection con) throws OXException {
        Cache<MailAccount> mailAccountCache = optMailAccountCache();
        if (null == mailAccountCache) {
            return delegate.getMailAccount(id, userId, contextId, con);
        }

        // Use cache...
        CacheKey key = newCacheKey(mailAccountCache, contextId, userId, contextId);
        return mailAccountCache.get(key, k -> delegate.getMailAccount(id, userId, contextId, con));
    }

    /**
     * Gets the mail account with the given identifier.
     *
     * @param id The account identifier
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param con The database connection to use
     * @return The mail account
     * @throws OXException If mail account cannot be returned; e.g. does not exist
     */
    private MailAccount getUnfilteredMailAccount(int id, int userId, int contextId, Connection con) throws OXException {
        Cache<MailAccount> mailAccountCache = optMailAccountCache();
        if (null == mailAccountCache) {
            return delegate.getMailAccount(id, userId, contextId, con);
        }

        // Use cache...
        CacheKey key = newCacheKey(mailAccountCache, contextId, userId, id);
        return mailAccountCache.get(key, k -> delegate.getMailAccount(id, userId, contextId, con));
    }

    @Override
    public MailAccount[] getUserMailAccounts(int userId, int contextId) throws OXException {
        List<MailAccount> accounts = new LinkedList<>();
        for (int id : getUserMailAccountIDs(userId, contextId)) {
            try {
                accounts.add(getMailAccount(id, userId, contextId));
            } catch (OXException e) {
                if (MailAccountExceptionCodes.EXTERNAL_ACCOUNTS_DISABLED.equals(e)) {
                    continue;
                }
                throw e;
            }
        }
        return accounts.toArray(new MailAccount[accounts.size()]);
    }

    private int[] getUserMailAccountIDs(int userId, int contextId) throws OXException {
        Cache<int[]> userAccountsCache = optUserAccountsCache();
        if (null == userAccountsCache) {
            return delegate.getUserMailAccountIDs(userId, contextId);
        }
        CacheKey key = accountsCacheKey(userAccountsCache, contextId, userId);
        return userAccountsCache.get(key, k -> delegate.getUserMailAccountIDs(userId, contextId));
    }

    @Override
    public MailAccount[] getUserDefaultAndSecondaryMailAccounts(int userId, int contextId) throws OXException {
        int[] ids = getUserMailAccountIDs(userId, contextId);
        List<MailAccount> accounts = new ArrayList<>(ids.length);
        for (int i = 0; i < ids.length; i++) {
            MailAccount account = getMailAccount(ids[i], userId, contextId);
            if (account.isDefaultAccount() || account.isSecondaryAccount()) {
                accounts.add(account);
            }
        }
        return accounts.toArray(new MailAccount[accounts.size()]);
    }

    @Override
    public TransportAccount[] getUserTransportAccounts(int userId, int contextId) throws OXException {
        int[] ids = delegate.getUserTransportAccountIDs(userId, contextId);

        TransportAccount[] accounts = new TransportAccount[ids.length];
        for (int i = 0; i < accounts.length; i++) {
            try {
                accounts[i] = getTransportAccount(ids[i], userId, contextId);
            } catch (OXException e) {
                if (MailAccountExceptionCodes.EXTERNAL_ACCOUNTS_DISABLED.equals(e)) {
                    continue;
                }
                throw e;
            }
        }
        return accounts;
    }

    @Override
    public MailAccount[] resolveLogin(String login, int contextId) throws OXException {
        List<IdAndUser> idsAndUsers;
        Cache<List<IdAndUser>> resolvedLoginsCache = optResolvedLoginsCache();
        if (null != resolvedLoginsCache) {
            CacheKey key = resolvedLoginsCacheKey(resolvedLoginsCache, contextId, CachedResolveType.LOGIN, login);
            idsAndUsers = resolvedLoginsCache.get(key);
            if (idsAndUsers == null) {
                idsAndUsers = resolvedLoginsCache.get(key, k -> RdbMailAccountStorage.resolveLogin2IDs(login, contextId));
            }
        } else {
            idsAndUsers = RdbMailAccountStorage.resolveLogin2IDs(login, contextId);
        }
        MailAccount[] accounts = new MailAccount[idsAndUsers.size()];
        int i = 0;
        for (IdAndUser idAndUser : idsAndUsers) {
            accounts[i++] = getMailAccount(idAndUser.getId(), idAndUser.getUserId(), contextId);
        }
        return accounts;
    }

    @Override
    public MailAccount[] resolveLogin(String login, String serverUrl, int contextId) throws OXException {
        MailAccount[] candidates = resolveLogin(login, contextId);
        if (null == candidates || 0 == candidates.length) {
            return candidates;
        }
        List<MailAccount> matchingAccounts = new ArrayList<MailAccount>(candidates.length);
        for (MailAccount candidate : candidates) {
            if (serverUrl.equals(toSocketAddrString(candidate.generateMailServerURL(), 143))) {
                matchingAccounts.add(candidate);
            }
        }
        return matchingAccounts.toArray(new MailAccount[matchingAccounts.size()]);
    }

    @Override
    public void updateMailAccount(MailAccountDescription mailAccount, Set<Attribute> attributes, int userId, int contextId, Session session) throws OXException {
        if (null != session) {
            session.setParameter("com.openexchange.mailaccount.unifiedMailEnabled", null);
        }
        dropSessionParameter(userId, contextId);

        Connection con = Database.get(contextId, true);
        int rollback = 0;
        try {
            con.setAutoCommit(false);
            rollback = 1;
            updateMailAccount(mailAccount, attributes, userId, contextId, session, con, false);
            con.commit();
            rollback = 2;
        } catch (SQLException e) {
            throw MailAccountExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw MailAccountExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (rollback > 0) {
                if (rollback == 1) {
                    rollback(con);
                }
                autocommit(con);
            }
            Database.back(contextId, true, con);
        }
    }

    @Override
    public void updateMailAccount(MailAccountDescription mailAccount, Set<Attribute> attributes, int userId, int contextId, Session session, Connection con, boolean changePrimary) throws OXException {
        UpdateProperties updateProperties = new UpdateProperties.Builder().setChangePrimary(changePrimary).setChangeProtocol(false).setCon(con).setSession(session).build();
        updateMailAccount(mailAccount, attributes, userId, contextId, updateProperties);
    }

    @Override
    public void updateMailAccount(MailAccountDescription mailAccount, Set<Attribute> attributes, int userId, int contextId, UpdateProperties updateProperties) throws OXException {
        Connection con = updateProperties == null ? null : updateProperties.getCon();
        Session session = updateProperties == null ? null : updateProperties.getSession();

        if (null != session) {
            session.setParameter("com.openexchange.mailaccount.unifiedMailEnabled", null);
        }
        dropSessionParameter(userId, contextId);

        delegate.updateMailAccount(mailAccount, attributes, userId, contextId, updateProperties);
        addAfterCommitCallbackElseExecute(con, c -> invalidateMailAccount(mailAccount.getId(), userId, contextId));
    }

    @Override
    public void updateMailAccount(MailAccountDescription mailAccount, int userId, int contextId, Session session) throws OXException {
        if (null != session) {
            session.setParameter("com.openexchange.mailaccount.unifiedMailEnabled", null);
        }
        dropSessionParameter(userId, contextId);

        MailAccount changedAccount = delegate.updateAndReturnMailAccount(mailAccount, userId, contextId, session);
        invalidateMailAccount(mailAccount.getId(), userId, contextId);

        Cache<MailAccount> mailAccountCache = optMailAccountCache();
        if (null != mailAccountCache) {
            CacheKey key = newCacheKey(mailAccountCache, contextId, userId, mailAccount.getId());
            mailAccountCache.put(key, changedAccount);
        }
    }

    @Override
    public void enableMailAccount(int accountId, int userId, int contextId) throws OXException {
        enableMailAccount(accountId, userId, contextId, null);
    }

    @Override
    public void enableMailAccount(int accountId, int userId, int contextId, Connection con) throws OXException {
        if(con == null) {
            delegate.enableMailAccount(accountId, userId, contextId);
            invalidateMailAccount(accountId, userId, contextId);
            return;
        }
        delegate.enableMailAccount(accountId, userId, contextId, con);
        addAfterCommitCallbackElseExecute(con, c -> invalidateMailAccount(accountId, userId, contextId));
    }

    @Override
    public int insertMailAccount(MailAccountDescription mailAccount, int userId, int contextId, Session session) throws OXException {
        return insertMailAccount(mailAccount, userId, contextId, session, null);
    }

    @Override
    public int insertMailAccount(MailAccountDescription mailAccount, int userId, int contextId, Session session, Connection con) throws OXException {
        int id = con == null ? delegate.insertMailAccount(mailAccount, userId, contextId, session) : delegate.insertMailAccount(mailAccount, userId, contextId, session, con);
        addAfterCommitCallbackElseExecute(con, c -> invalidateMailAccount(id, userId, contextId));
        return id;
    }

    @Override
    public MailAccount[] resolvePrimaryAddr(String primaryAddress, int contextId) throws OXException {
        List<IdAndUser> idsAndUsers;
        Cache<List<IdAndUser>> resolvedLoginsCache = optResolvedLoginsCache();
        if (null != resolvedLoginsCache) {
            CacheKey key = resolvedLoginsCacheKey(resolvedLoginsCache, contextId, CachedResolveType.PRIMARY_ADDRESS, primaryAddress);
            idsAndUsers = resolvedLoginsCache.get(key);
            if (idsAndUsers == null) {
                idsAndUsers = resolvedLoginsCache.get(key, k -> RdbMailAccountStorage.resolvePrimaryAddr2IDs(primaryAddress, contextId));
            }
        } else {
            idsAndUsers = RdbMailAccountStorage.resolvePrimaryAddr2IDs(primaryAddress, contextId);
        }
        MailAccount[] l = new MailAccount[idsAndUsers.size()];
        int i = 0;
        for (IdAndUser idAndUser : idsAndUsers) {
            l[i++] = getMailAccount(idAndUser.getId(), idAndUser.getUserId(), contextId);
        }
        return l;
    }

    @Override
    public void migratePasswords(String oldSecret, String newSecret, Session session) throws OXException {
        delegate.migratePasswords(oldSecret, newSecret, session);
        invalidateMailAccounts(session.getUserId(), session.getContextId());
    }

    @Override
    public boolean hasAccounts(Session session) throws OXException {
        return delegate.hasAccounts(session);
    }

    @Override
    public void cleanUp(String secret, Session session) throws OXException {
        delegate.cleanUp(secret, session);
    }

    @Override
    public void removeUnrecoverableItems(String secret, Session session) throws OXException {
        delegate.removeUnrecoverableItems(secret, session);
    }

    @Override
    public int insertTransportAccount(TransportAccountDescription transportAccount, int userId, int contextId, Session session) throws OXException {
        int id = delegate.insertTransportAccount(transportAccount, userId, contextId, session);
        invalidateMailAccount(id, userId, contextId);
        return id;
    }

    @Override
    public void deleteTransportAccount(int id, int userId, int contextId) throws OXException {
        dropSessionParameter(userId, contextId);
        delegate.deleteTransportAccount(id, userId, contextId);
        invalidateMailAccount(id, userId, contextId);
    }

    @Override
    public void deleteTransportAccount(int id, int userId, int contextId, Connection con) throws OXException {
        dropSessionParameter(userId, contextId);
        delegate.deleteTransportAccount(id, userId, contextId, con);
        addAfterCommitCallbackElseExecute(con, c -> invalidateMailAccount(id, userId, contextId));
    }

    @Override
    public TransportAccount getTransportAccount(int accountId, int userId, int contextId, Connection con) throws OXException {
        return getMailAccount(accountId, userId, contextId, con); // load as full mail account for consistent caching
    }

    @Override
    public TransportAccount getTransportAccount(int accountId, int userId, int contextId) throws OXException {
        return getMailAccount(accountId, userId, contextId); // load as full mail account for consistent caching
    }

    @Override
    public void updateTransportAccount(TransportAccountDescription transportAccount, int userId, int contextId, Session session) throws OXException {
        delegate.updateTransportAccount(transportAccount, userId, contextId, session);
        invalidateMailAccount(transportAccount.getId(), userId, contextId);
    }

    @Override
    public void updateTransportAccount(TransportAccountDescription transportAccount, Set<Attribute> attributes, int userId, int contextId, Session session) throws OXException {
        delegate.updateTransportAccount(transportAccount, attributes, userId, contextId, session);
        invalidateMailAccount(transportAccount.getId(), userId, contextId);
    }

    @Override
    public void updateTransportAccount(TransportAccountDescription transportAccount, Set<Attribute> attributes, int userId, int contextId, UpdateProperties updateProperties) throws OXException {
        Connection optConnection = null != updateProperties ? updateProperties.getCon() : null;
        delegate.updateTransportAccount(transportAccount, attributes, userId, contextId, updateProperties);
        addAfterCommitCallbackElseExecute(optConnection, c -> invalidateMailAccount(transportAccount.getId(), userId, contextId));
    }

}
