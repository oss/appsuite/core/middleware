/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.ajax;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import com.openexchange.groupware.settings.Setting;

/**
 * {@link SettingsConverter}
 *
 * @author <a href="mailto:marcus@open-xchange.org">Marcus Klein</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v8.0.0
 */
public class SettingsConverter {

    private SettingsConverter() {}

    /**
     * Converts a tree of settings into the according java script objects.
     *
     * @param setting Tree of settings.
     * @return java script object representing the setting tree.
     * @throws JSONException if the conversion to java script objects fails.
     */
    public static Object convert2JS(final Setting setting) throws JSONException {
        // Handle tree
        if (!setting.isLeaf()) {
            final Setting[] elements = setting.getElements();
            final JSONObject json = new JSONObject(elements.length);
            for (final Setting subSetting : elements) {
                json.put(subSetting.getName(), convert2JS(subSetting));
            }
            return json;
        }

        // Handle multivalue
        final Object[] multiValue = setting.getMultiValue();
        if (null != multiValue) {
            final JSONArray array = new JSONArray();
            for (final Object value : multiValue) {
                array.put(value);
            }
            return array;
        }

        // Handle single value
        final Object singleValue = setting.getSingleValue();
        if (null == singleValue) {
            return JSONObject.NULL;
        }
        if (singleValue instanceof JSONObject) {
            return singleValue;
        }
        try {
            return JSONServices.parseObject(singleValue.toString());
        } catch (@SuppressWarnings("unused") JSONException e) {
            return singleValue;
        }
    }
}
