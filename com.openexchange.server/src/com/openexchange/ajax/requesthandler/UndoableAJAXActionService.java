/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.requesthandler;

import static com.openexchange.ajax.AJAXServlet.PARAMETER_UNDO;
import static com.openexchange.java.Autoboxing.L;
import java.util.Map;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.exception.OXException;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link UndoableAJAXActionService} - Marks an {@link AJAXActionService} as undo-able and sets {@link AJAXRequestResult#setUndoTokenIfNotEmpty(String)}
 * if parameter {@link AJAXServlet#PARAMETER_UNDO} is set for request.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public interface UndoableAJAXActionService extends AJAXActionService {

    /**
     * Checks if this action is undo-able with regard to specified request data and session.
     *
     * @param request The request data
     * @param session The session
     * @return The undo-able result
     * @throws OXException If check fails
     */
    Result isUndoable(AJAXRequestData request, ServerSession session) throws OXException;

    /**
     * Allows to perform certain actions as preparation prior to compiling undo state.
     * <p>
     * E.g. if the action expects binary uploads (files) of possibly big size, which might not be consumed before.
     *
     * @param request The request data
     * @param session The session
     * @throws OXException If preparations fails
     */
    default void prepareForUndo(AJAXRequestData request, ServerSession session) throws OXException {
        // Nothing
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Gets the <code>"undo"</code> parameter from specified request data.
     *
     * @param requestData The request data to obtain from
     * @return The undo time-to-live or <code>-1</code>
     */
    public static long getUndoTimeToLiveFrom(AJAXRequestData requestData) {
        Long undoTimeToLive = requestData.getProperty("undoToken");
        if (undoTimeToLive == null) {
            undoTimeToLive = L(AJAXRequestDataTools.parseUnsignedLongParameter(PARAMETER_UNDO, requestData));
            requestData.setProperty("undoToken", undoTimeToLive);
        }
        return undoTimeToLive.longValue();
    }

    /**
     * Gets the result for specified undo-able flag
     *
     * @param undoable The undo-able flag
     * @return The result
     */
    public static Result resultFor(boolean undoable) {
        return undoable ? Result.TRUE : Result.FALSE;
    }

    /**
     * Gets the result for specified undo-able flag
     *
     * @param undoable The undo-able flag
     * @param undoableAction The undo-able action; or <code>null</code>
     * @return The result
     */
    public static Result resultFor(boolean undoable, UndoableAJAXActionService undoableAction) {
        return undoableAction == null ? resultFor(undoable) : new Result(undoable, undoableAction);
    }

    /**
     * The result for checking if an action is undo-able.
     */
    public static final class Result {

        private static final Result TRUE = new Result(true);

        private static final Result FALSE = new Result(false);

        private final boolean undoable;
        private final UndoableAJAXActionService undoableAction;
        private final Map<String, Object> state;

        /**
         * Initializes a new {@link Result}.
         *
         * @param undoable The undo-able flag
         */
        private Result(boolean undoable) {
            this(undoable, null);
        }

        /**
         * Initializes a new {@link Result}.
         *
         * @param undoable The undo-able flag
         * @param undoableAction The undo-able action; or <code>null</code>
         */
        private Result(boolean undoable, UndoableAJAXActionService undoableAction) {
            this(undoable, undoableAction, null);
        }

        /**
         * Initializes a new {@link Result}.
         *
         * @param undoable The undo-able flag
         * @param undoableAction The undo-able action; or <code>null</code>
         * @param state The state or <code>null</code>
         */
        private Result(boolean undoable, UndoableAJAXActionService undoableAction, Map<String, Object> state) {
            super();
            this.undoable = undoable;
            this.undoableAction = undoableAction;
            this.state = state;
        }

        /**
         * Gets the reverse undo-able flag
         *
         * @return The reverse undo-able flag
         */
        public boolean isNotUndoable() {
            return !undoable;
        }

        /**
         * Gets the undo-able flag
         *
         * @return The undo-able flag
         */
        public boolean isUndoable() {
            return undoable;
        }

        /**
         * Gets the undo-able action.
         *
         * @return The undo-able action
         */
        public UndoableAJAXActionService getUndoableAction() {
            return undoableAction;
        }

        /**
         * Gets the state
         *
         * @return The state
         */
        public Map<String, Object> getState() {
            return state;
        }
    }

}
