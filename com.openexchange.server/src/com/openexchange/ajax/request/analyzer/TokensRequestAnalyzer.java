/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.ajax.request.analyzer;

import static com.openexchange.ajax.AJAXServlet.PARAMETER_CONTEXT_ID;
import static com.openexchange.ajax.LoginServlet.ACTION_TOKENS;
import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.optParameterValue;
import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.parseBodyParameters;
import java.util.Optional;
import com.openexchange.database.ConfigDatabaseService;
import com.openexchange.database.DatabaseService;
import com.openexchange.dispatcher.DispatcherPrefixService;
import com.openexchange.exception.OXException;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.BodyData;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.request.analyzer.utils.RequestAnalyzerUtils;
import com.openexchange.server.ServiceLookup;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * {@link TokensRequestAnalyzer} - {@link RequestAnalyzer} for the action=tokens request
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public class TokensRequestAnalyzer extends AbstractLoginRequestAnalyzer {

    private final ErrorAwareSupplier<? extends ConfigDatabaseService> dbServiceSupplier;

    /**
     * Initializes a new {@link TokensRequestAnalyzer}.
     *
     * @param services The service lookup to use, which should yield the {@link SessiondService}, {@link DispatcherPrefixService} and the {@link DatabaseService}.
     * @throws OXException If the {@link DispatcherPrefixService} is missing
     */
    public TokensRequestAnalyzer(ServiceLookup services) throws OXException {
        super(services, ACTION_TOKENS);
        dbServiceSupplier = () -> services.getServiceSafe(DatabaseService.class);
    }

    @Override
    public Optional<AnalyzeResult> analyze(RequestData data) throws OXException {
        /*
         * check request method, module and action from URL
         */
        if (false == matchesMethodModuleAndAction(data)) {
            return Optional.empty();
        }
        /*
         * check & parse request login body
         */
        Optional<BodyData> requestBody = data.optBody();
        if (requestBody.isEmpty()) {
            return Optional.of(AnalyzeResult.MISSING_BODY); // body required for further processing
        }
        Optional<String> optContextId = optParameterValue(parseBodyParameters(requestBody.get()), PARAMETER_CONTEXT_ID);
        /*
         * yield analysis result from context id parameter
         */
        return Optional.of(optContextId.isEmpty() ? AnalyzeResult.UNKNOWN : RequestAnalyzerUtils.createAnalyzeResultFromCid(optContextId.get(), dbServiceSupplier));
    }

}
