/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.ajax.request.analyzer;

import static com.openexchange.ajax.fields.LoginFields.AUTHID_PARAM;
import static com.openexchange.ajax.fields.LoginFields.CLIENT_IP_PARAM;
import static com.openexchange.ajax.fields.LoginFields.CLIENT_PARAM;
import static com.openexchange.ajax.fields.LoginFields.CLIENT_TOKEN;
import static com.openexchange.ajax.fields.LoginFields.LOGIN_PARAM;
import static com.openexchange.ajax.fields.LoginFields.PASSWORD_PARAM;
import static com.openexchange.ajax.fields.LoginFields.SHARE_TOKEN;
import static com.openexchange.ajax.fields.LoginFields.STAY_SIGNED_IN;
import static com.openexchange.ajax.fields.LoginFields.TRANSIENT;
import static com.openexchange.ajax.fields.LoginFields.USER_AGENT;
import static com.openexchange.ajax.fields.LoginFields.VERSION_PARAM;
import static com.openexchange.login.Interface.HTTP_JSON;
import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.addToMap;
import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.analyzeViaLogin;
import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.getFirstParameterValue;
import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.parseBodyParameters;
import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.parseRequestParameters;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.UUID;
import com.openexchange.ajax.LoginServlet;
import com.openexchange.ajax.fields.LoginFields;
import com.openexchange.ajax.login.HashCalculator;
import com.openexchange.ajax.login.LoginConfiguration;
import com.openexchange.ajax.login.LoginRequestImpl;
import com.openexchange.ajax.login.LoginTools;
import com.openexchange.ajax.requesthandler.AJAXRequestDataTools;
import com.openexchange.authentication.Cookie;
import com.openexchange.dispatcher.DispatcherPrefixService;
import com.openexchange.exception.OXException;
import com.openexchange.java.util.UUIDs;
import com.openexchange.login.LoginRequest;
import com.openexchange.login.listener.internal.LoginListenerRegistry;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.BodyData;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.request.analyzer.RequestURL;
import com.openexchange.server.ServiceLookup;
import com.openexchange.sessiond.SessiondService;

/**
 * {@link GenericLoginRequestAnalyzer}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class GenericLoginRequestAnalyzer extends AbstractLoginRequestAnalyzer {

    private final LoginListenerRegistry loginListenerRegistry;
    private final String loginNameParameter;

    /**
     * Initializes a new {@link GenericLoginRequestAnalyzer}, using the prefix path to match from the {@link DispatcherPrefixService}, and
     * the common {@value LoginFields#LOGIN_PARAM} as login parameter name.
     *
     * @param services The service lookup to use, which should yield the {@link SessiondService} and the {@link DispatcherPrefixService}
     * @param loginListenerRegistry The {@link LoginListenerRegistry} to use
     * @param actionName The name of the login action
     * @throws OXException If the {@link DispatcherPrefixService} is missing
     */
    public GenericLoginRequestAnalyzer(ServiceLookup services, LoginListenerRegistry loginListenerRegistry, String actionName) throws OXException {
        this(services, loginListenerRegistry, actionName, LOGIN_PARAM);
    }

    /**
     * Initializes a new {@link GenericLoginRequestAnalyzer}, using the prefix path to match from the {@link DispatcherPrefixService}.
     *
     * @param services The service lookup to use, which should yield the {@link SessiondService} and the {@link DispatcherPrefixService}
     * @param loginListenerRegistry The {@link LoginListenerRegistry} to use
     * @param actionName The name of the login action
     * @param loginNameParameter The expected name of the login parameter when building the login request.
     * @throws OXException If the {@link DispatcherPrefixService} is missing
     */
    public GenericLoginRequestAnalyzer(ServiceLookup services, LoginListenerRegistry loginListenerRegistry, String actionName, String loginNameParameter) throws OXException {
        super(services, actionName);
        this.loginListenerRegistry = loginListenerRegistry;
        this.loginNameParameter = loginNameParameter;
    }

    @Override
    public Optional<AnalyzeResult> analyze(RequestData data) throws OXException {
        /*
         * check request method, module and action from URL
         */
        if (false == matchesMethodModuleAndAction(data)) {
            return Optional.empty();
        }
        /*
         * check & parse request login body
         */
        Optional<BodyData> body = data.optBody();
        if (body.isEmpty()) {
            return Optional.of(AnalyzeResult.MISSING_BODY); // body required for further processing
        }
        Optional<LoginRequest> loginRequest = buildLoginRequest(data, loginNameParameter);
        /*
         * create & return analysis result from login request by probing authentication
         */
        return Optional.of(loginRequest.isEmpty() ? AnalyzeResult.UNKNOWN : analyzeViaLogin(loginRequest.get(), loginListenerRegistry));
    }

    /**
     * Method to build a LoginRequest
     *
     * @param data The request data
     * @param loginParameterName The expected name of the login parameter
     * @return The LoginRequest, or empty if no value login request could be parsed
     * @throws OXException If parsing share information fails
     */
    protected Optional<LoginRequest> buildLoginRequest(RequestData data, String loginParameterName) throws OXException {
        /*
         * extract body parameters & parse login name
         */
        Optional<BodyData> requestBody = data.optBody();
        if (requestBody.isEmpty()) {
            return Optional.empty();
        }
        Map<String, String[]> parametersMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        addToMap(parseBodyParameters(requestBody.get()), parametersMap);
        String login = getFirstParameterValue(parametersMap, loginParameterName);
        if (null == login) {
            return Optional.empty(); // no login name in body, no useful login request
        }
        /*
         * parse further login request parameters from request URI/headers/body
         */
        RequestURL url = data.getParsedURL();
        addToMap(parseRequestParameters(url), parametersMap);
        LoginConfiguration loginConf = LoginServlet.getLoginConfiguration();
        String password = getFirstParameterValue(parametersMap, PASSWORD_PARAM);
        String authId = getFirstParameterValue(parametersMap, AUTHID_PARAM, UUIDs.getUnformattedString(UUID.randomUUID()));
        String client = getFirstParameterValue(parametersMap, CLIENT_PARAM, loginConf.getDefaultClient());
        String version = getFirstParameterValue(parametersMap, VERSION_PARAM);
        String userAgent = getFirstParameterValue(parametersMap, USER_AGENT, data.getHeaders().getFirstHeaderValue("User-Agent"));
        String clientIp = getFirstParameterValue(parametersMap, CLIENT_IP_PARAM, data.getClientIp());
        boolean staySignedIn = AJAXRequestDataTools.parseBoolParameter(getFirstParameterValue(parametersMap, STAY_SIGNED_IN));
        String tranzient = getFirstParameterValue(parametersMap, TRANSIENT, "false");
        String[] additionals = LoginTools.parseShareInformation(url.optParameter(SHARE_TOKEN).orElse(null));
        String clientToken = getFirstParameterValue(parametersMap, CLIENT_TOKEN);
        /*
         * extract and convert cookies
         */
        Map<String, Cookie> cookieMap = AbstractCookieBasedRequestAnalyzer.getCookies(data);
        Cookie[] cookies = cookieMap.values().toArray(new Cookie[cookieMap.size()]);
        /*
         * build & return login request
         */
        return Optional.of(new LoginRequestImpl.Builder()
                .login(login)
                .password(password)
                .clientIP(clientIp)
                .userAgent(userAgent)
                .authId(authId)
                .client(client)
                .version(version)
                .hash(HashCalculator.getInstance().getHash(data.getHeaders(), userAgent, clientIp, additionals))
                .headers(data.getHeaders().getMapping())
                .requestParameter(parametersMap)
                .serverName(url.getURL().getHost())
                .serverPort(url.getURL().getPort())
                .staySignedIn(staySignedIn)
                .cookies(cookies)
                .iface(HTTP_JSON)
                .tranzient(Boolean.parseBoolean(tranzient))
                .clientToken(clientToken)
                .build());
    }

}
