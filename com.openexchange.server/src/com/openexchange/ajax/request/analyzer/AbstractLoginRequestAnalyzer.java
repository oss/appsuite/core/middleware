/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.ajax.request.analyzer;

import static com.openexchange.ajax.LoginServlet.SERVLET_PATH_APPENDIX;
import com.openexchange.dispatcher.DispatcherPrefixService;
import com.openexchange.exception.OXException;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.request.analyzer.utils.RequestAnalyzerUtils;
import com.openexchange.server.ServiceLookup;
import com.openexchange.sessiond.SessiondService;

/**
 * {@link AbstractLoginRequestAnalyzer}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public abstract class AbstractLoginRequestAnalyzer extends AbstractPrefixAwareRequestAnalyzer {

    private final String actionName;

    /**
     * Initializes a new {@link AbstractLoginRequestAnalyzer}, using the prefix path to match from the {@link DispatcherPrefixService}.
     *
     * @param services The service lookup to use, which should yield the {@link SessiondService} and the {@link DispatcherPrefixService}
     * @param actionName The name of the login action
     * @throws OXException If the {@link DispatcherPrefixService} is missing
     */
    protected AbstractLoginRequestAnalyzer(ServiceLookup services, String actionName) throws OXException {
        super(services);
        this.actionName = actionName;
    }
    
    /**
     * Gets a value indicating whether the supplied request data indicates a suitable HTTP method targeting the <code>login</code>
     * module, with the appropriate <code>action</code> parameter set.
     * 
     * @param data The request data to check
     * @return <code>true</code> if method, module and action matches, <code>false</code>, otherwise
     * @throws OXException In case of an invalid or malformed URL
     */
    protected boolean matchesMethodModuleAndAction(RequestData data) throws OXException {
        return RequestAnalyzerUtils.matchesMethodModuleAndAction(data, getMethod(), prefix, SERVLET_PATH_APPENDIX, actionName);
    }
    
    /**
     * Gets the expected HTTP method of the analyzed login request.
     * <p/>
     * Defaults to <code>POST</code>, override if applicable
     * 
     * @return The method
     */
    protected String getMethod() {
        return "POST";
    }

    @Override
    public String toString() {
        return "AbstractLoginRequestAnalyzer [actionName=" + actionName + "]";
    }
}
