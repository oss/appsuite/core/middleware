/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.login;

import static com.openexchange.ajax.SettingsConverter.convert2JS;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.LoginServlet;
import com.openexchange.ajax.Multiple;
import com.openexchange.ajax.SessionUtility;
import com.openexchange.ajax.container.Response;
import com.openexchange.ajax.fields.Header;
import com.openexchange.ajax.fields.LoginFields;
import com.openexchange.ajax.requesthandler.responseRenderers.APIResponseRenderer;
import com.openexchange.ajax.writer.LoginWriter;
import com.openexchange.ajax.writer.ResponseWriter;
import com.openexchange.authentication.LoginExceptionCodes;
import com.openexchange.authentication.ResultCode;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.settings.Setting;
import com.openexchange.groupware.settings.impl.ConfigTree;
import com.openexchange.groupware.settings.impl.SettingStorage;
import com.openexchange.i18n.LocaleTools;
import com.openexchange.java.Strings;
import com.openexchange.log.LogProperties;
import com.openexchange.login.LoginJsonEnhancer;
import com.openexchange.login.LoginResult;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.session.Session;
import com.openexchange.session.ThreadLocalSessionHolder;
import com.openexchange.threadpool.AbstractTask;
import com.openexchange.threadpool.ThreadPools;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.servlet.OXJSONExceptionCodes;
import com.openexchange.tools.servlet.http.Tools;
import com.openexchange.tools.servlet.ratelimit.Key;
import com.openexchange.tools.servlet.ratelimit.RateLimitedException;
import com.openexchange.tools.servlet.ratelimit.RateLimiter;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tools.session.ServerSessionAdapter;
import com.openexchange.user.User;

/**
 * {@link AbstractLoginRequestHandler}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 */
public abstract class AbstractLoginRequestHandler implements LoginRequestHandler {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(AbstractLoginRequestHandler.class);

    private static final String HTTP_HEADER_USER_AGENT = Header.USER_AGENT;

    private static volatile Integer maxLoginRate;
    private static int maxLoginRate() {
        Integer tmp = maxLoginRate;
        if (null == tmp) {
            synchronized (RateLimiter.class) {
                tmp = maxLoginRate;
                if (null == tmp) {
                    Integer defaultValue = Integer.valueOf(50);

                    ConfigurationService service = ServerServiceRegistry.getInstance().getService(ConfigurationService.class);
                    if (null == service) {
                        // Service not yet available
                        return defaultValue.intValue();
                    }

                    String propertyName = "com.openexchange.ajax.login.maxRate";
                    String propertyValue = service.getProperty(propertyName);
                    if (Strings.isEmpty(propertyValue)) {
                        tmp = defaultValue;
                    } else {
                        try {
                            tmp = Integer.valueOf(propertyValue.trim());
                        } catch (NumberFormatException e) {
                            LOG.warn("Invalid value for property '{}'. Going to use default value {} instead.", propertyName, defaultValue);
                            tmp = defaultValue;
                        }
                    }

                    maxLoginRate = tmp;
                }
            }
        }
        return tmp.intValue();
    }

    private static volatile Integer maxLoginRateTimeWindow;
    private static int maxLoginRateTimeWindow() {
        Integer tmp = maxLoginRateTimeWindow;
        if (null == tmp) {
            synchronized (RateLimiter.class) {
                tmp = maxLoginRateTimeWindow;
                if (null == tmp) {
                    Integer defaultValue = Integer.valueOf(300000);

                    ConfigurationService service = ServerServiceRegistry.getInstance().getService(ConfigurationService.class);
                    if (null == service) {
                        // Service not yet available
                        return defaultValue.intValue();
                    }

                    String propertyName = "com.openexchange.ajax.login.maxRateTimeWindow";
                    String propertyValue = service.getProperty(propertyName);
                    if (Strings.isEmpty(propertyValue)) {
                        tmp = defaultValue;
                    } else {
                        try {
                            tmp = Integer.valueOf(propertyValue.trim());
                        } catch (NumberFormatException e) {
                            LOG.warn("Invalid value for property '{}'. Going to use default value {} instead.", propertyName, defaultValue);
                            tmp = defaultValue;
                        }
                    }

                    maxLoginRateTimeWindow = tmp;
                }
            }
        }
        return tmp.intValue();
    }

    // ------------------------------------------------------------------------------------------------------------------------------ //

    /**
     * Initializes a new {@link AbstractLoginRequestHandler}.
     */
    protected AbstractLoginRequestHandler() {
        super();
    }

    /**
     * Invokes given {@link LoginClosure}'s {@link LoginClosure#doLogin(HttpServletRequest) doLogin()} method to obtain a session.
     * <p>
     * Having the session further ramp-up operations are performed and finally an appropriate JSON result object is composed that gets written to passed HTTP response.
     *
     * @param req The associated HTTP request
     * @param resp The associated HTTP response
     * @param login The login closure to invoke
     * @param conf The login configuration
     * @param requestContext The request's context
     * @return <code>true</code> if an auto-login should proceed afterwards; otherwise <code>false</code>
     * @throws IOException If an I/O error occurs
     * @throws OXException If an Open-Xchange error occurs
     */
    protected boolean loginOperation(HttpServletRequest req, HttpServletResponse resp, LoginClosure login, LoginConfiguration conf, LoginRequestContext requestContext) throws IOException, OXException {
        return loginOperation(req, resp, login, null, conf, requestContext);
    }

    /**
     * Invokes given {@link LoginClosure}'s {@link LoginClosure#doLogin(HttpServletRequest) doLogin()} method to obtain a session.
     * <p>
     * Having the session further ramp-up operations are performed and finally an appropriate JSON result object is composed that gets written to passed HTTP response.
     *
     * @param req The associated HTTP request
     * @param resp The associated HTTP response
     * @param login The login closure to invoke
     * @param conf The login configuration
     * @param requestContext The request's context
     * @return <code>true</code> if an auto-login should proceed afterwards; otherwise <code>false</code>
     * @throws IOException If an I/O error occurs
     * @throws OXException If an Open-Xchange error occurs
     */
    protected boolean loginOperation(HttpServletRequest req, HttpServletResponse resp, LoginClosure login, LoginCookiesSetter cookiesSetter, LoginConfiguration conf, LoginRequestContext requestContext) throws IOException, OXException {
        Tools.disableCaching(resp);
        AJAXServlet.setDefaultContentType(resp);

        // Perform the login
        final Response response = new Response();
        LoginResult result = null;
        ServerSession serverSession = null;
        try {
            // Do the login...
            {
                int rate = maxLoginRate();
                int timeWindow = maxLoginRateTimeWindow();
                if (rate <= 0 || timeWindow <= 0) {
                    // No rate limit enabled
                    result = login.doLogin(req);
                    if (null == result) {
                        return true;
                    }
                } else {
                    Key rateLimitKey = new Key(req, req.getHeader(HTTP_HEADER_USER_AGENT), "__login.failed");
                    try {
                        // Optionally consume one permit
                        boolean consumed = RateLimiter.optRateLimitFor(rateLimitKey, rate, timeWindow, req);
                        try {
                            result = login.doLogin(req);
                            if (null == result) {
                                return true;
                            }
                            // Successful login (so far) -- clean rate limit trace
                            RateLimiter.removeRateLimit(rateLimitKey);
                        } catch (OXException e) {
                            if (!consumed && LoginExceptionCodes.INVALID_CREDENTIALS.equals(e)) {
                                // Consume one permit
                                RateLimiter.checkRateLimitFor(rateLimitKey, rate, timeWindow, req);
                            }
                            throw e;
                        }
                    } catch (RateLimitedException rateLimitExceeded) {
                        throw rateLimitExceeded;
                    }
                }
            }

            // The associated session
            final Session session = result.getSession();

            // Add session log properties
            LogProperties.putSessionProperties(session);

            // Add headers and cookies from login result
            LoginServlet.addHeadersAndCookies(result, resp);

            // Check result code
            {
                final ResultCode code = result.getCode();
                if (null != code) {
                    switch (code) {
                        case FAILED:
                            return true;
                        case REDIRECT:
                            throw LoginExceptionCodes.REDIRECT.create(result.getRedirect());
                        default:
                            break;
                    }
                }
            }

            // Request modules
            Future<Object> optModules = getModulesAsync(session, req);

            // Remember User-Agent
            session.setParameter(Session.PARAM_USER_AGENT, req.getHeader(HTTP_HEADER_USER_AGENT));
            serverSession = ServerSessionAdapter.valueOf(session);

            // Write response
            JSONObject json = new JSONObject(13);
            LoginWriter.write(result, json);
            if (result instanceof LoginJsonEnhancer) {
                ((LoginJsonEnhancer) result).enhanceJson(json);
            }

            // Handle initial multiple
            {
                String multipleRequest = req.getParameter("multiple");
                if (multipleRequest != null) {
                    final JSONArray dataArray = JSONServices.parseArray(multipleRequest);
                    if (dataArray.length() > 0) {
                        JSONArray responses = Multiple.perform(dataArray, req, serverSession);
                        json.put("multiple", responses);
                    } else {
                        json.put("multiple", JSONArray.EMPTY_ARRAY);
                    }
                }
            }

            // Add modules information to JSON object
            if (null != optModules) {
                // Append "config/modules"
                try {
                    final Object oModules = optModules.get();
                    if (null != oModules) {
                        json.put("modules", oModules);
                    }
                } catch (InterruptedException e) {
                    // Keep interrupted state
                    Thread.currentThread().interrupt();
                    throw LoginExceptionCodes.UNKNOWN.create(e, "Thread interrupted.");
                } catch (ExecutionException e) {
                    // Cannot occur
                    final Throwable cause = e.getCause();
                    LOG.warn("Modules could not be added to login JSON response", cause);
                }
            }

            // Set response
            response.setData(json);
        } catch (OXException e) {
            if (AjaxExceptionCodes.PREFIX.equals(e.getPrefix())) {
                throw e;
            }
            if (LoginExceptionCodes.NOT_SUPPORTED.equals(e)) {
                // Rethrow according to previous behavior
                LOG.debug("", e);
                throw AjaxExceptionCodes.DISABLED_ACTION.create("autologin");
            }
            if (LoginExceptionCodes.REDIRECT.equals(e) || LoginExceptionCodes.AUTHENTICATION_DISABLED.equals(e) || LoginExceptionCodes.INVALID_CREDENTIALS.equals(e)) {
                LOG.debug("", e);
            } else {
                LOG.error("", e);
            }
            response.setException(e);
            requestContext.getMetricProvider().recordException(e);
        } catch (JSONException e) {
            final OXException oje = OXJSONExceptionCodes.JSON_WRITE_ERROR.create(e);
            LOG.error("", oje);
            response.setException(oje);
        }

        try {
            if (response.hasError() || null == result) {
                ResponseWriter.write(response, resp.getWriter(), extractLocale(req, result));
                requestContext.getMetricProvider().recordException(response.getException());
                return false;
            }

            Session session = result.getSession();
            // Store associated session
            if (null == serverSession) {
                serverSession = new ServerSessionAdapter(session);
            }
            SessionUtility.rememberSession(req, serverSession);
            ThreadLocalSessionHolder.getInstance().setSession(serverSession);

            // Set cookies
            if (null == cookiesSetter) {
                // Create/re-write secret cookie
                LoginServlet.writeSecretCookie(req, resp, session, session.getHash(), req.isSecure(), req.getServerName(), conf);

                // Create/re-write session cookie
                LoginServlet.writeSessionCookie(resp, session, session.getHash(), req.isSecure(), req.getServerName());
            } else {
                cookiesSetter.setLoginCookies(session, req, resp, conf);
            }

            // Login response is unfortunately not conform to default responses.
            if (req.getParameter("callback") != null && LoginServlet.ACTION_LOGIN.equals(req.getParameter("action"))) {
                APIResponseRenderer.writeResponse(response, LoginServlet.ACTION_LOGIN, req, resp);
            } else {
                ((JSONObject) response.getData()).write(resp.getWriter());
            }
        } catch (JSONException e) {
            if (e.getCause() instanceof IOException) {
                // Throw proper I/O error since a serious socket error could been occurred which prevents further communication. Just
                // throwing a JSON error possibly hides this fact by trying to write to/read from a broken socket connection.
                throw (IOException) e.getCause();
            }
            LOG.error(LoginServlet.RESPONSE_ERROR, e);
            LoginServlet.sendError(resp);
            requestContext.getMetricProvider().recordHTTPStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            return false;
        } finally {
            ThreadLocalSessionHolder.getInstance().clear();
        }

        requestContext.getMetricProvider().recordSuccess();
        return false;
    }

    /**
     * Extracts the locale from the specified request. First tries the {@link LoginFields#LANGUAGE_PARAM},
     * then the {@link LoginFields#LOCALE_PARAM} and finally a best guess according to the <code>Accept-Language</code>
     * header if set.
     *
     * @param req The request
     * @param result The result
     * @return The {@link Locale}
     */
    private Locale extractLocale(HttpServletRequest req, LoginResult result) {
        String sLanguage = req.getParameter(LoginFields.LANGUAGE_PARAM);
        if (null == sLanguage) {
            sLanguage = req.getParameter(LoginFields.LOCALE_PARAM);
            if (null == sLanguage) {
                return bestGuessLocale(result, req);
            }
            return LocaleTools.getLocale(sLanguage);
        }
        Locale loc = LocaleTools.getLocale(sLanguage);
        return null == loc ? bestGuessLocale(result, req) : loc;
    }

    /**
     * Tries to determine the {@link Locale} based on the <code>Accept-Language</code> header
     *
     * @param result the result
     * @param req The request
     * @return The {@link Locale}
     */
    private Locale bestGuessLocale(LoginResult result, final HttpServletRequest req) {
        if (null == result) {
            return Tools.getLocaleByAcceptLanguage(req, null);
        }
        User user = result.getUser();
        return null == user ? Tools.getLocaleByAcceptLanguage(req, null) : user.getLocale();
    }

    /**
     * Asynchronously retrieves modules.
     *
     * @param session The associated session
     * @param req The request
     * @return The resulting object or <code>null</code>
     */
    public Future<Object> getModulesAsync(final Session session, final HttpServletRequest req) {
        final String modules = "modules";
        if (!LoginServlet.parseBoolean(req.getParameter(modules))) {
            return null;
        }
        // Submit task
        final org.slf4j.Logger logger = LOG;
        return ThreadPools.getThreadPool().submit(new AbstractTask<Object>() {

            @Override
            public Object call() throws Exception {
                try {
                    final Setting setting = ConfigTree.getInstance().getSettingByPath(modules);
                    SettingStorage.getInstance(session).readValues(setting);
                    return convert2JS(setting);
                } catch (OXException e) {
                    logger.warn("Modules could not be added to login JSON response", e);
                } catch (JSONException e) {
                    logger.warn("Modules could not be added to login JSON response", e);
                } catch (Exception e) {
                    logger.warn("Modules could not be added to login JSON response", e);
                }
                return null;
            }
        });
    }
}
