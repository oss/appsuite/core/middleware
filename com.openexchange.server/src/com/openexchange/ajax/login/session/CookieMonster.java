/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.login.session;

import static com.openexchange.java.Autoboxing.b;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.ajax.LoginServlet;
import com.openexchange.ajax.SessionServletInterceptor;
import com.openexchange.ajax.SessionUtility;
import com.openexchange.cluster.map.ClusterMap;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.exception.OXException;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessiondService;


/**
 * {@link CookieMonster} - Purges orphaned session cookies.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class CookieMonster implements SessionServletInterceptor {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CookieMonster.class);

    private final Cache<String, Boolean> checksBySessionId;
    private final boolean checkClusterWide;
    private final long checkIntervalMillis;

    /**
     * Initializes a new {@link CookieMonster} checking a session's cookies node-local every 4 hours.
     */
    public CookieMonster() {
        this(false, TimeUnit.HOURS.toMillis(4L));
    }

    /**
     * Initializes a new {@link CookieMonster}.
     *
     * @param checkClusterWide <code>true</code> to synchronize the checks cluster-wide, <code>false</code> for node-local checks
     * @param checkIntervalMillis The interval between checks for the same session in milliseconds
     */
    public CookieMonster(boolean checkClusterWide, long checkIntervalMillis) {
        super();
        this.checkClusterWide = checkClusterWide;
        this.checkIntervalMillis = checkIntervalMillis;
        this.checksBySessionId = CacheBuilder.newBuilder().expireAfterWrite(checkIntervalMillis, TimeUnit.MILLISECONDS).build();
    }

    @Override
    public void intercept(Session session, HttpServletRequest req, HttpServletResponse resp) throws OXException {
        if (needsCheck(session)) {
            LOG.debug("Checking for orphaned session cookies in incoming request associated with session {}.", session.getSessionID());
            try {
                removeOrphanedSessionCookies(session, req, resp);
                removeOrphanedPublicSessionCookies(session, req, resp);
            } catch (Exception e) {
                LOG.warn("Unexpected error checking for orphaned cookies", e);
            }
        }
    }

    /**
     * Gets a value whether a check for the cookies sent with the underlying request is due or not, based on the supplied session
     * associated with the request.
     *
     * @param session The session associated with the request to check
     * @return <code>true</code> if cookies should be checked, <code>false</code>, otherwise
     */
    private boolean needsCheck(Session session) {
        if (null == session || null == session.getSessionID()) {
            return false;
        }
        /*
         * probe cluster-wide marker if enabled
         */
        if (checkClusterWide) {
            ClusterMapService service = ServerServiceRegistry.getInstance().getService(ClusterMapService.class);
            if (service != null) {
                try {
                    return needsCheck(session, service);
                } catch (Exception e) {
                    LOG.warn("Failed to cluster-wide check if client-passed session cookies need to be revisited. Continuing with node-local check...", e);
                }
            }
        }
        /*
         * probe local cache marker, otherwise
         */
        if (checksBySessionId.getIfPresent(session.getSessionID()) != null) {
            return false;
        }
        AtomicReference<Boolean> loaded = new AtomicReference<Boolean>(Boolean.FALSE);
        try {
            checksBySessionId.get(session.getSessionID(), () -> {
                loaded.set(Boolean.TRUE);
                return Boolean.TRUE;
            });
        } catch (ExecutionException e) {
            LOG.warn("Unexpected error evaluating if cookie check is due", e);
            return false;
        }
        return b(loaded.get());
    }

    /**
     * Gets a value whether a check for the cookies sent with the underlying request is due or not, based on the supplied session
     * associated with the request.
     *
     * @param session The session associated with the request to check
     * @param service The cluster map service to use
     * @return <code>true</code> if cookies should be checked; otherwise <code>false</code>
     * @throws OXException If cluster-wide check fails
     */
    private boolean needsCheck(Session session, ClusterMapService service) throws OXException {
        ClusterMap<String> map = service.getGeneralMap();
        String key = "cookie-monster-" + session.getSessionID();

        String marker = map.get(key);
        if (marker != null) {
            // Not yet expired
            return false;
        }
        return map.putIfAbsent(key, Boolean.TRUE.toString(), checkIntervalMillis) == null;
    }

    /**
     * Removes all session/secret/share cookies referencing no longer existing session data by setting the cookie expiration times in
     * the headers of the HTTP response.
     * <p/>
     * Detection is done based on the presence of the session identifier in the value of cookies with the
     * {@link LoginServlet#SESSION_PREFIX}, removal is then done based on the corresponding session hash.
     *
     * @param session The session associated with the request
     * @param request The incoming HTTP request
     * @param response The corresponding HTTP response
     */
    private static void removeOrphanedSessionCookies(Session session, HttpServletRequest request, HttpServletResponse response) {
        /*
         * extract all cookies with matching prefix from request
         */
        List<Cookie> sessionCookies = getCookiesWithPrefix(request.getCookies(), LoginServlet.SESSION_PREFIX);
        if (null == sessionCookies || 2 > sessionCookies.size()) {
            return;
        }
        /*
         * check if associated sessions still exist, remove cookies by session hash if not
         */
        SessiondService sessiondService = ServerServiceRegistry.getInstance().getService(SessiondService.class);
        for (Cookie sessionCookie : sessionCookies) {
            String sessionId = sessionCookie.getValue();
            if (null == sessionId || sessionId.equals(session.getSessionID())) {
                continue;
            }
            Session peekedSession = sessiondService.peekSession(sessionId);
            if (null == peekedSession) {
                String hash = sessionCookie.getName().substring(LoginServlet.SESSION_PREFIX.length());
                LOG.info("Removing orphaned cookies for session {} with hash {}.", sessionCookie.getValue(), hash);
                SessionUtility.removeOXCookies(hash, request, response);
            }
        }
    }

    /**
     * Removes all public session cookies referencing no longer existing session data by setting the cookie expiration times in the
     * headers of the HTTP response.
     * <p/>
     * Detection is done based on the presence of the alternative session identifier in the value of cookies with the
     * {@link LoginServlet#PUBLIC_SESSION_PREFIX}.
     *
     * @param session The session associated with the request
     * @param request The incoming HTTP request
     * @param response The corresponding HTTP response
     */
    private static void removeOrphanedPublicSessionCookies(Session session, HttpServletRequest request, HttpServletResponse response) {
        /*
         * extract all cookies with matching prefix from request
         */
        List<Cookie> publicSessionCookies = getCookiesWithPrefix(request.getCookies(), LoginServlet.PUBLIC_SESSION_PREFIX);
        if (null == publicSessionCookies || 2 > publicSessionCookies.size()) {
            return;
        }
        /*
         * check if associated sessions still exist, remove cookie by name if not
         */
        SessiondService sessiondService = ServerServiceRegistry.getInstance().getService(SessiondService.class);
        for (Cookie publicSessionCookie : publicSessionCookies) {
            String altId = publicSessionCookie.getValue();
            if (null == altId || altId.equals(session.getParameter(Session.PARAM_ALTERNATIVE_ID))) {
                continue;
            }
            Session peekedSession = sessiondService.peekSessionByAlternativeId(altId);
            if (null == peekedSession) {
                LOG.info("Removing orphaned public session cookie {}.", publicSessionCookie.getName());
                SessionUtility.removeOXCookies(request, response, Collections.singletonList(publicSessionCookie.getName()));
            }
        }
    }

    private static List<Cookie> getCookiesWithPrefix(Cookie[] cookies, String prefix) {
        if (null == cookies) {
            return null;
        }
        List<Cookie> cookiesWithPrefix = new LinkedList<Cookie>();
        for (Cookie cookie : cookies) {
            String name = cookie.getName();
            if (null != name && name.startsWith(prefix)) {
                cookiesWithPrefix.add(cookie);
            }
        }
        return cookiesWithPrefix;
    }

}
