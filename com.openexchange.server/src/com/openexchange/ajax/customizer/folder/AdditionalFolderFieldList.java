/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.customizer.folder;

import static com.openexchange.java.Autoboxing.I;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.ajax.customizer.AdditionalFieldsUtils;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.folderstorage.Folder;
import com.openexchange.tools.session.ServerSession;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * {@link AdditionalFolderFieldList}
 *
 * @author <a href="mailto:francisco.laguna@open-xchange.com">Francisco Laguna</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class AdditionalFolderFieldList {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(AdditionalFolderFieldList.class);

    private static final AdditionalFolderFieldList INSTANCE = new AdditionalFolderFieldList();

    /**
     * Gets the instance
     *
     * @return The instance
     */
    public static AdditionalFolderFieldList getInstance() {
        return INSTANCE;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

	 // TODO: Track service ranking and allow fields to overwrite other fields.

    private final AtomicReference<StringAndIntMap> mapReference;

    /**
     * Initializes a new instance of {@link AdditionalFolderFieldList}.
     */
    protected AdditionalFolderFieldList() {
        super();
        mapReference = new AtomicReference<>(new StringAndIntMap());
    }

    /**
     * Adds an additional folder field to this list.
     *
     * @param field The additional folder field
     */
    public void addField(final AdditionalFolderField field) {
        int key = field.getColumnID();

        StringAndIntMap map;
        StringAndIntMap newMap;
        do {
            map = mapReference.get();
            if (map.byColId.containsKey(key) || map.byName.containsKey(field.getColumnName())) {
                warnAboutCollision(field);
                return;
            }
            newMap = new StringAndIntMap(map, field, true);
        } while (!mapReference.compareAndSet(map, newMap));
    }

    private static void warnAboutCollision(final AdditionalFolderField field) {
        LOG.warn("Collision in folder fields. Field '{}' : {} has already been taken. Ignoring second service.", field.getColumnName(), I(field.getColumnID()));
    }

    /**
     * Removes the additional folder field associated with specified column number.
     *
     * @param colId The column number
     */
    public void remove(final int colId) {
        StringAndIntMap map;
        StringAndIntMap newMap;
        do {
            map = mapReference.get();
            AdditionalFolderField toRemove = map.byColId.get(colId);
            if (toRemove == null) {
                // Unknown...
                return;
            }
            newMap = new StringAndIntMap(map, toRemove, false);
        } while (!mapReference.compareAndSet(map, newMap));
    }

    /**
     * Gets the additional folder field associated with specified column number.
     *
     * @param col The column number
     * @return The additional folder field associated with specified column number or a neutral <code>null</code> field
     */
    public AdditionalFolderField get(final int col) {
        StringAndIntMap map = mapReference.get();
        final AdditionalFolderField additionalFolderField = map.byColId.get(col);
        return null == additionalFolderField ? new NullField(col) : additionalFolderField;
    }

    /**
     * Optionally gets the additional folder field associated with specified column number.
     *
     * @param col The column number
     * @return The additional folder field associated with specified column number or <code>null</code>
     */
    public AdditionalFolderField opt(final int col) {
        StringAndIntMap map = mapReference.get();
        return map.byColId.get(col);
    }

    /**
     * Gets known fields.
     *
     * @return The known fields
     */
    public int[] getKnownFields() {
        StringAndIntMap map = mapReference.get();
        return map.byColId.keys();
    }

    /**
     * Gets the additional folder field associated with specified column name.
     *
     * @param col The column name
     * @return The additional folder field associated with specified column name or a <code>null</code>
     */
    public AdditionalFolderField get(final String col) {
        StringAndIntMap map = mapReference.get();
        return map.byName.get(col);
    }

    /**
     * Checks if an additional folder field is associated with specified column number.
     *
     * @param col The column number
     * @return <code>true</code> if an additional folder field is associated with specified column number; otherwise <code>false</code>
     */
    public boolean knows(int col) {
        StringAndIntMap map = mapReference.get();
        return map.byColId.containsKey(col);
    }

    /**
     * Checks if an additional folder field is associated with specified column name.
     *
     * @param col The column name
     * @return <code>true</code> if an additional folder field is associated with specified column name; otherwise <code>false</code>
     */
    public boolean knows(String col) {
        StringAndIntMap map = mapReference.get();
        return map.byName.containsKey(col);
    }

    /**
     * A neutral <code>null</code> field implementation.
     */
    private static final class NullField implements AdditionalFolderField {

        private final int columnId;

        NullField(final int columnId) {
            super();
            this.columnId = columnId;
        }

        @Override
        public int getColumnID() {
            return columnId;
        }

        @Override
        public String getColumnName() {
            return null;
        }

        @Override
        public Object getValue(final Folder folder, final ServerSession session) {
            return null;
        }

        @Override
        public Object renderJSON(AJAXRequestData requestData, final Object value) {
            return null;
        }

        @Override
        public List<Object> getValues(List<Folder> folder, ServerSession session) {
            return AdditionalFieldsUtils.bulk(this, folder, session);
        }
    }

    private static final class StringAndIntMap {

        final TIntObjectMap<AdditionalFolderField> byColId;
        final Map<String, AdditionalFolderField> byName;

        /**
         * Initializes a new instance of {@link StringAndIntMap}.
         */
        private StringAndIntMap() {
            super();
            byColId = new TIntObjectHashMap<AdditionalFolderField>();
            byName = Collections.emptyMap();
        }

        /**
         * Initializes a new instance of {@link StringAndIntMap}.
         *
         * @param prev The previous mapping
         * @param field The field to add or remove
         * @param add <code>true</code> to add the field; otherwise <code>false</code> to remove it
         */
        private StringAndIntMap(StringAndIntMap prev, AdditionalFolderField field, boolean add) {
            super();
            TIntObjectMap<AdditionalFolderField> newColMap = prev.byColId == null ? new TIntObjectHashMap<AdditionalFolderField>() : new TIntObjectHashMap<AdditionalFolderField>(prev.byColId);
            Map<String, AdditionalFolderField> newNameMap = prev.byName == null ? new HashMap<String, AdditionalFolderField>() : new HashMap<String, AdditionalFolderField>(prev.byName);
            if (add) {
                newColMap.put(field.getColumnID(), field);
                newNameMap.put(field.getColumnName(), field);
            } else {
                newColMap.remove(field.getColumnID());
                newNameMap.remove(field.getColumnName());
            }
            this.byColId = newColMap;
            this.byName = Map.copyOf(newNameMap);
        }
    }

}
