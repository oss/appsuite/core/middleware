/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.resource.internal;

import static com.openexchange.resource.ResourcePermissionUtility.DEFAULT_PERMISSIONS;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;
import com.openexchange.java.Enums;
import com.openexchange.resource.Resource;
import com.openexchange.resource.ResourcePermission;
import com.openexchange.resource.SchedulingPrivilege;

/**
 * {@link ResourceCodec} - Cache codec for {@link Resource}s.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class ResourceCodec extends AbstractJSONObjectCacheValueCodec<Resource> {

    private static final UUID CODEC_ID = UUID.fromString("4371c12b-b82f-4f2d-00fc-c0e11e000001");

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONObject writeJson(Resource value) throws Exception {
        JSONObject jsonObject = new JSONObject(8);
        jsonObject.putOpt("c", value.getDescription());
        jsonObject.putOpt("d", value.getDisplayName());
        jsonObject.putOpt("m", value.getMail());
        jsonObject.putOpt("s", value.getSimpleName());
        jsonObject.put("i", value.getIdentifier());
        if (false == value.isAvailable()) {
            jsonObject.put("na", true);
        }
        if (null != value.getLastModified()) {
            jsonObject.put("l", value.getLastModified().getTime());
        }
        ResourcePermission[] permissions = value.getPermissions();
        if (null != permissions && 0 < permissions.length && false == Arrays.equals(permissions, DEFAULT_PERMISSIONS)) {
            JSONArray jsonArray = new JSONArray(permissions.length);
            for (int i = 0; i < permissions.length; i++) {
                jsonArray.put(i, writePermission(permissions[i]));
            }
            jsonObject.put("p", jsonArray);
        }
        return jsonObject;
    }

    @Override
    protected Resource parseJson(JSONObject jObject) throws Exception {
        Resource resource = new Resource();
        resource.setDescription(jObject.optString("c"));
        resource.setDisplayName(jObject.optString("d"));
        resource.setMail(jObject.optString("m"));
        resource.setSimpleName(jObject.optString("s"));
        resource.setIdentifier(jObject.getInt("i"));
        long lastModifiedMillis = jObject.optLong("l", -1L);
        resource.setLastModified(-1 == lastModifiedMillis ? null : new Date(lastModifiedMillis));
        resource.setAvailable(false == jObject.optBoolean("na", false));
        JSONArray jsonArray = jObject.optJSONArray("p");
        if (null == jsonArray || jsonArray.isEmpty()) {
            resource.setPermissions(DEFAULT_PERMISSIONS);
        } else {
            ResourcePermission[] permissions = new ResourcePermission[jsonArray.length()];
            for (int i = 0; i < permissions.length; i++) {
                permissions[i] = parsePermission(jsonArray.optJSONObject(i));
            }
            resource.setPermissions(permissions);
        }
        return resource;
    }

    private static ResourcePermission parsePermission(JSONObject jObject) throws JSONException {
        if (null == jObject) {
            return null;
        }
        int entity = jObject.getInt("e");
        boolean group = jObject.optBoolean("g");
        SchedulingPrivilege schedulingPrivilege = Enums.parse(SchedulingPrivilege.class, jObject.optString("p", null), null);
        return new ResourcePermission(entity, group, schedulingPrivilege);
    }

    private static JSONObject writePermission(ResourcePermission value) throws JSONException {
        if (null == value) {
            return null;
        }
        JSONObject jsonObject = new JSONObject(3);
        jsonObject.put("e", value.getEntity());
        if (value.isGroup()) {
            jsonObject.put("g", true);
        }
        if (null != value.getSchedulingPrivilege()) {
            jsonObject.put("p", value.getSchedulingPrivilege().name());
        }
        return jsonObject;
    }

}
