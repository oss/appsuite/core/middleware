/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.resource.internal;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.database.DatabaseConnectionListeners.addAfterCommitCallbackElseExecute;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.codec.IntArrayCacheValueCodec;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.resource.Resource;
import com.openexchange.resource.ResourceGroup;
import com.openexchange.resource.ResourcePermission;
import com.openexchange.resource.SchedulingPrivilege;

/**
 * {@link CachingResourceStorage} is a {@link ExtendedResourceStorage} which caches the resources
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v8
 */
public class CachingResourceStorage implements ExtendedResourceStorage, CachingAwareResourceStorage {

    /** Cache options using {@link ResourceCodec} for cached resources */
    private static final CacheOptions<Resource> OPTIONS_RESOURCES = CacheOptions.<Resource> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.RESOURCE)
        .withCodecAndVersion(new ResourceCodec())
    .build(); // formatter:on

    /** Cache options using the default <code>int[]</code> codec for cached resource ids */
    private static final CacheOptions<int[]> OPTIONS_RESOURCE_IDS = CacheOptions.<int[]> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.RESOURCE)
        .withCodecAndVersion(IntArrayCacheValueCodec.getInstance())
    .build(); // formatter:on

    private final RdbResourceStorage delegate;
    private final CacheService cacheService;

    /**
     * Initializes a new {@link CachingResourceStorage}.
     *
     * @param delegate The delegating resource storage
     * @param cacheService The cache service
     */
    public CachingResourceStorage(RdbResourceStorage delegate, CacheService cacheService) {
        super();
        this.delegate = delegate;
        this.cacheService = cacheService;
    }

    @Override
    public ResourceGroup getGroup(int groupId, Context context) throws OXException {
        return delegate.getGroup(groupId, context);
    }

    @Override
    public ResourceGroup[] getGroups(Context context) throws OXException {
        return delegate.getGroups(context);
    }

    @Override
    public Resource getResource(int resourceId, Context context) throws OXException {
        return getResource(resourceId, context, null);
    }

    @Override
    public Resource getResource(int resourceId, Context context, Connection connection) throws OXException {
        Cache<Resource> cache = getCache();
        CacheKey key = toKey(cache, context, resourceId);
        return cache.get(key, k -> delegate.getResource(resourceId, context, connection));
    }

    @Override
    public ResourceGroup[] searchGroups(String pattern, Context context) throws OXException {
        return delegate.searchGroups(pattern, context);
    }

    @Override
    public Resource[] searchResources(String pattern, Context context) throws OXException {
        if (SEARCH_PATTERN_ALL.equals(pattern)) {
            // Use cache in case of all pattern
            Cache<int[]> resourceIdsCache = getIdsCache();
            CacheKey key = toKey(resourceIdsCache, context, SPECIAL_FOR_ALL_RESOURCE_IDS);
            return loadResources(context, resourceIdsCache.get(key, k -> delegate.loadAllResourceIds(context)));
        }
        return delegate.searchResources(pattern, context);
    }

    @Override
    public Resource getByIdentifier(String identifier, Context context) throws OXException {
        return delegate.getByIdentifier(identifier, context);
    }

    @Override
    public Resource getByMail(String mail, Context context) throws OXException {
        return delegate.getByMail(mail, context);
    }

    @Override
    public Resource[] searchResourcesByMail(String pattern, Context context) throws OXException {
        return delegate.searchResourcesByMail(pattern, context);
    }

    @Override
    public Resource[] searchResourcesByPrivilege(int[] entities, SchedulingPrivilege privilege, Context context) throws OXException {
        return delegate.searchResourcesByPrivilege(entities, privilege, context);
    }

    @Override
    public Resource[] listModified(Date modifiedSince, Context context) throws OXException {
        return delegate.listModified(modifiedSince, context);
    }

    @Override
    public Resource[] listDeleted(Date modifiedSince, Context context) throws OXException {
        return delegate.listDeleted(modifiedSince, context);
    }

    @Override
    public void insertResource(Context ctx, Connection con, Resource resource, StorageType type) throws OXException {
        delegate.insertResource(ctx, con, resource, type);
        if (StorageType.ACTIVE.equals(type)) {
            addAfterCommitCallbackElseExecute(con, c -> invalidateAllResourceIds(ctx));
        }
    }

    @Override
    public void updateResource(Context ctx, Connection con, Resource resource) throws OXException {
        delegate.updateResource(ctx, con, resource);
        addAfterCommitCallbackElseExecute(con, c -> invalidateResource(ctx, resource.getIdentifier()));
    }

    @Override
    public void deleteResourceById(Context ctx, Connection con, int resourceId) throws OXException {
        delegate.deleteResourceById(ctx, con, resourceId);
        addAfterCommitCallbackElseExecute(con, c -> invalidate(ctx, resourceId));
    }

    // ------------------------------ User based methods ------------------------

    @Override
    public Resource[] searchResources(String pattern, Context context, int userId) throws OXException {
        return loadResources(context, delegate.searchResourceIds(context, userId, pattern));
    }

    @Override
    public Resource[] searchResourcesByMail(String pattern, Context context, int userId) throws OXException {
        return delegate.searchResourcesByMail(pattern, context, userId);
    }

    // ----------------------------- Caching methods ---------------------------

    @Override
    public void invalidate(Context context, int resourceId) throws OXException {
        invalidateResource(context, resourceId);
        invalidateAllResourceIds(context);
    }

    private Cache<Resource> getCache() {
        return cacheService.getCache(OPTIONS_RESOURCES);
    }

    private Cache<int[]> getIdsCache() {
        return cacheService.getCache(OPTIONS_RESOURCE_IDS);
    }

    private void invalidateResource(Context context, int resourceId) throws OXException {
        Cache<Resource> cache = getCache();
        cache.invalidate(toKey(cache, context, resourceId));
    }

    private void invalidateAllResourceIds(Context context) throws OXException {
        Cache<int[]> cache = getIdsCache();
        cache.invalidate(toKey(cache, context, SPECIAL_FOR_ALL_RESOURCE_IDS));
    }

    /**
     * Creates a new cache key
     *
     * @param cache The cache view
     * @param ctx The context
     * @param resourceId The resource id
     * @return The cache key
     */
    private CacheKey toKey(Cache<?> cache, Context ctx, int resourceId) {
        return cache.newKey(asHashPart(ctx.getContextId()), Integer.toString(resourceId));
    }

    // ------------------ extended methods --------------------

    @Override
    public List<Resource> getResourceIdsWithPermissionsForEntity(Context ctx, Connection con, int entity, boolean group) throws OXException {
        return delegate.getResourceIdsWithPermissionsForEntity(ctx, con, entity, group);
    }

    @Override
    public int deletePermissions(Context ctx, Connection connection, int resourceId) throws OXException {
        int result = delegate.deletePermissions(ctx, connection, resourceId);
        invalidateResource(ctx, resourceId);
        return result;
    }

    @Override
    public int[] insertPermissions(Context ctx, Connection connection, int resourceId, ResourcePermission[] permissions) throws OXException {
        int[] result = delegate.insertPermissions(ctx, connection, resourceId, permissions);
        invalidateResource(ctx, resourceId);
        return result;
    }

    // ---------------------------- helper methods -----------------

    /**
     * Loads the given resources
     *
     * @param context The context to load the resources from
     * @param ids The resource ids
     * @return The resources
     * @throws OXException
     */
    private Resource[] loadResources(Context context, int[] ids) throws OXException {
        if (null == ids || 0 == ids.length) {
            return new Resource[0];
        }
        Cache<Resource> cache = getCache();
        Resource[] result = new Resource[ids.length];
        for (int x = 0; x < result.length; x++) {
            int id = ids[x];
            result[x] = cache.get(toKey(cache, context, id), k -> delegate.getResource(id, context));
        }
        return result;
    }

}
