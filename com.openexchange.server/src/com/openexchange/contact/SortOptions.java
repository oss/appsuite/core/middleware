/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.contact;

import com.openexchange.groupware.contact.helpers.ContactField;
import com.openexchange.groupware.search.Order;

/**
 * {@link SortOptions}
 *
 * Defines sort options for the results of storage operations. This includes
 * the specification ranged results, a collation and multiple sort orders.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class SortOptions {

    /**
     * Empty sort options
     */
	public static final SortOptions EMPTY = new EmptySortOptions();

	/**
     * Creates a new {@link SortOrder} instance for given arguments.
     *
     * @param by The contact field for ordering
     * @param order The order
     * @return The sort order or <code>null</code> (in case the contact field for ordering is <code>null</code>)
     */
    public static final SortOrder sortOrderFor(ContactField by, Order order) {
        return by == null ? null : new SortOrder(by, order);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

	private SortOrder[] sortOrders;
	private String collation;
	private int rangeStart;
	private int limit;

	/**
	 * Initializes a new {@link SortOptions}.
	 *
	 * @param collation the collation
	 * @param sortOrders the sort order definitions
	 */
	public SortOptions(String collation, SortOrder... sortOrders) {
		super();
		this.collation = collation;
		this.sortOrders = sortOrders;
	}

	/**
	 * Initializes a new {@link SortOptions}.
	 *
	 * @param rangeStart the start index for the results
	 * @param limit the maximum number of results to return
	 */
	public SortOptions(int rangeStart, int limit) {
		this();
		this.limit = limit;
		this.rangeStart = rangeStart;
	}

	/**
	 * Initializes a new {@link SortOptions}.
	 *
	 * @param order the sort order definitions
	 */
	public SortOptions(SortOrder... order) {
		this(null, order);
	}

	/**
	 * Initializes a new {@link SortOptions}.
	 *
     * @param collation the collation
	 */
	public SortOptions(String collation) {
		this(collation, (SortOrder[])null);
	}

	/**
	 * Initializes a new {@link SortOptions}.
	 */
	public SortOptions() {
		this((SortOrder[])null);
	}

	/**
	 * Initializes a new {@link SortOptions}.
	 *
     * @param collation the collation
	 * @param orderBy the field to order by
	 * @param order the order
     * @param rangeStart the start index for the results
     * @param limit the maximum number of results to return
	 */
	public SortOptions(String collation, ContactField orderBy, Order order, int rangeStart, int limit) {
		this(collation, orderBy, order);
		this.limit = limit;
		this.rangeStart = rangeStart;
	}

	/**
	 * Initializes a new {@link SortOptions}.
	 *
     * @param collation the collation
     * @param orderBy the field to order by
     * @param order the order
	 */
	public SortOptions(String collation, ContactField orderBy, Order order) {
		this(collation, sortOrderFor(orderBy, order));
	}

	/**
	 * Initializes a new {@link SortOptions}.
	 *
     * @param orderBy the field to order by
     * @param order the order
	 */
	public SortOptions(ContactField orderBy, Order order) {
		this(sortOrderFor(orderBy, order));
	}

	/**
	 * Initializes a new {@link SortOptions}.
	 *
     * @param collation the collation
     * @param orderBy1 the 1st field to order by
     * @param order1 the 1st order
     * @param orderBy2 the 2nd field to order by
     * @param order2 the 2nd order
	 */
	public SortOptions(String collation, ContactField orderBy1, Order order1, ContactField orderBy2, Order order2) {
		this(collation, sortOrderFor(orderBy1, order1), sortOrderFor(orderBy2, order2));
	}

	/**
	 * Initializes a new {@link SortOptions}.
	 *
     * @param orderBy1 the 1st field to order by
     * @param order1 the 1st order
     * @param orderBy2 the 2nd field to order by
     * @param order2 the 2nd order
	 */
	public SortOptions(ContactField orderBy1, Order order1, ContactField orderBy2, Order order2) {
		this((String)null, sortOrderFor(orderBy1, order1), sortOrderFor(orderBy2, order2));
	}

	/**
	 * @return the collation
	 */
	public String getCollation() {
		return collation;
	}

	/**
	 * @param collation the collation to set
	 */
	public void setCollation(String collation) {
		this.collation = collation;
	}

	/**
	 * @return the order
	 */
	public SortOrder[] getOrder() {
		return sortOrders;
	}

	/**
	 * @param order the orderBy to set
	 */
	public void setOrderBy(SortOrder[] order) {
		this.sortOrders = order;
	}

	/**
	 * @return the rangeStart
	 */
	public int getRangeStart() {
		return rangeStart;
	}

	/**
	 * @param rangeStart the rangeStart to set
	 */
	public void setRangeStart(int rangeStart) {
		this.rangeStart = rangeStart;
	}

	/**
	 * @return the limit
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * @param limit the limit to set
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

    @Override
    public String toString() {
        if (SortOptions.EMPTY.equals(this)) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        if (sortOrders != null && sortOrders.length > 0) {
            sb.append("ORDER BY ").append(sortOrders[0].getBy()).append(' ').append(Order.orderDirectionFor(sortOrders[0].getOrder()));
            for (int i = 1; i < sortOrders.length; i++) {
                sb.append(", ").append(sortOrders[i].getBy()).append(' ').append((Order.orderDirectionFor(sortOrders[i].getOrder())));
            }
        }
        if (limit > 0) {
            sb.append(" LIMIT ");
            if (rangeStart > 0) {
                sb.append(rangeStart).append(", ");
            }
            sb.append(limit);
        }
        return sb.toString();
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    /** The immutable empty sort options */
    private static final class EmptySortOptions extends SortOptions {

        /**
         * Initializes a new instance of {@link SortOptions.EmptySortOptions}.
         */
        private EmptySortOptions() {
            super();
        }

        @Override
        public void setCollation(String collation) {
            throw new UnsupportedOperationException("setCollation() is not allowed for empty sort options");
        }

        @Override
        public void setLimit(int limit) {
            throw new UnsupportedOperationException("setLimit() is not allowed for empty sort options");
        }

        @Override
        public void setOrderBy(SortOrder[] order) {
            throw new UnsupportedOperationException("setOrderBy() is not allowed for empty sort options");
        }

        @Override
        public void setRangeStart(int rangeStart) {
            throw new UnsupportedOperationException("setRangeStart() is not allowed for empty sort options");
        }
    }

}
