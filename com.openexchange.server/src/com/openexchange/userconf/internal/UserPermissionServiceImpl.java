/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.userconf.internal;

import static com.openexchange.tools.oxfolder.OXFolderAdminHelper.CHANGEABLE_PUBLIC_FOLDERS;
import java.sql.Connection;
import java.sql.SQLException;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.userconfiguration.RdbUserPermissionBitsStorage;
import com.openexchange.groupware.userconfiguration.UserConfiguration;
import com.openexchange.groupware.userconfiguration.UserConfigurationCodes;
import com.openexchange.groupware.userconfiguration.UserPermissionBits;
import com.openexchange.groupware.userconfiguration.UserPermissionBitsStorage;
import com.openexchange.tools.oxfolder.OXFolderAccess;
import com.openexchange.user.User;
import com.openexchange.userconf.UserPermissionService;


/**
 * {@link UserPermissionServiceImpl}
 *
 * @author <a href="mailto:francisco.laguna@open-xchange.com">Francisco Laguna</a>
 */
public class UserPermissionServiceImpl implements UserPermissionService {

    private final AccessCombinationNameCache cache;

    /**
     * Initializes a new {@link UserPermissionServiceImpl}.
     *
     * @throws OXException
     * @throws ClassNotFoundException
     */
    public UserPermissionServiceImpl() throws OXException {
        super();
        cache = new AccessCombinationNameCache();
        cache.initAccessCombinations();
    }

    @Override
    public UserPermissionBits getUserPermissionBits(int userId, int contextId) throws OXException {
        return UserPermissionBitsStorage.getInstance().getUserPermissionBits(userId, contextId);
    }

    @Override
    public UserPermissionBits getUserPermissionBits(int userId, Context ctx) throws OXException {
        return UserPermissionBitsStorage.getInstance().getUserPermissionBits(userId, ctx);
    }

    @Override
    public UserPermissionBits[] getUserPermissionBits(Context ctx, User[] users) throws OXException {
        return UserPermissionBitsStorage.getInstance().getUserPermissionBits(ctx, users);
    }

    @Override
    public UserPermissionBits getUserPermissionBits(Connection connection, int userId, Context ctx) throws OXException {
        return UserPermissionBitsStorage.getInstance().getUserPermissionBits(connection, userId, ctx);
    }

    @Override
    public void saveUserPermissionBits(UserPermissionBits permissionBits) throws OXException {
        UserPermissionBitsStorage.getInstance().saveUserPermissionBits(permissionBits.getPermissionBits(), permissionBits.getUserId(), permissionBits.getContext());
    }

    @Override
    public void saveUserPermissionBits(Connection connection, UserPermissionBits permissionBits) throws OXException {
        UserPermissionBitsStorage.getInstance().saveUserPermissionBits(connection, permissionBits.getPermissionBits(), permissionBits.getUserId(), permissionBits.getContext());
    }

    @Override
    public void deleteUserPermissionBits(Context context, int userId) throws OXException {
        try {
            RdbUserPermissionBitsStorage.deleteUserPermissionBits(userId, context);
        } catch (SQLException e) {
            throw UserConfigurationCodes.SQL_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public void deleteUserPermissionBits(Connection connection, Context context, int userId) throws OXException {
        try {
            RdbUserPermissionBitsStorage.deleteUserPermissionBits(userId, connection, context);
        } catch (SQLException e) {
            throw UserConfigurationCodes.SQL_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public UserPermissionBits[] getUserPermissionBits(Context ctx, int[] userIds) throws OXException {
        return UserPermissionBitsStorage.getInstance().getUserPermissionBits(ctx, userIds);
    }

    @Override
    public void removeUserPermissionBits(int userId, Context ctx) throws OXException {
        UserPermissionBitsStorage.getInstance().removeUserPermissionBits(userId, ctx);
    }

    @Override
    public String getAccessCombinationName(Context context, int userId) throws OXException {
        UserPermissionBits permissionBits = getUserPermissionBits(userId, context);
        return getAccessCombinationName(permissionBits, isPublicFolderEditable(context, userId, permissionBits));
    }

    private String getAccessCombinationName(UserPermissionBits permissionBits, boolean publicFolderEditable) {
        UserModuleAccess moduleAccess = getModuleAccess(permissionBits, publicFolderEditable);
        return cache.getNameForAccessCombination(moduleAccess);
    }

    /**
     * Gets a value indicating whether the <i>publicfoldereditable</i> right can be assumed for the given user, which is the case if he
     * is the context administrator, and has administrative access to certain public root folders.
     *
     * @param context The context
     * @param userId The identifier of the user to check
     * @param permissionBits The user's permission bits
     * @return <code>true</code> if the right <i>publicfoldereditable</i> can be assumed, <code>false</code>, otherwise
     * @throws OXException If looking up the information fails
     */
    private static boolean isPublicFolderEditable(Context context, int userId, UserPermissionBits permissionBits) throws OXException {
        if (userId != context.getMailadmin()) {
            return false;
        }
        OXFolderAccess folderAccess = new OXFolderAccess(context);
        for (int folderId : CHANGEABLE_PUBLIC_FOLDERS) {
            if (false == folderAccess.getFolderPermission(folderId, userId, permissionBits).isFolderAdmin()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Initializes a {@link UserModuleAccess} instance representing a user's module access combination based on the supplied user
     * permission bits.
     *
     * @param permissionBits The permission bits to use for initialization
     * @param publicFolderEditable <code>true</code> to add the right <i>publicfoldereditable</i>, which allows the admin to change the
     *            access rights to public folders for groups, <code>false</code>, otherwise
     * @return The user module access
     */
    @SuppressWarnings("deprecation")
    private static UserModuleAccess getModuleAccess(UserPermissionBits permissionBits, boolean publicFolderEditable) {
        UserModuleAccess moduleAccess = new UserModuleAccess();
        moduleAccess.setCalendar(permissionBits.hasPermission(UserConfiguration.CALENDAR));
        moduleAccess.setContacts(permissionBits.hasPermission(UserConfiguration.CONTACTS));
        moduleAccess.setEditPublicFolders(permissionBits.hasPermission(UserConfiguration.EDIT_PUBLIC_FOLDERS));
        moduleAccess.setReadCreateSharedFolders(permissionBits.hasPermission(UserConfiguration.READ_CREATE_SHARED_FOLDERS));
        moduleAccess.setIcal(permissionBits.hasPermission(UserConfiguration.ICAL));
        moduleAccess.setInfostore(permissionBits.hasPermission(UserConfiguration.INFOSTORE));
        moduleAccess.setSyncml(permissionBits.hasPermission(UserConfiguration.MOBILITY));
        moduleAccess.setTasks(permissionBits.hasPermission(UserConfiguration.TASKS));
        moduleAccess.setVcard(permissionBits.hasPermission(UserConfiguration.VCARD));
        moduleAccess.setWebdav(permissionBits.hasPermission(UserConfiguration.WEBDAV));
        moduleAccess.setWebdavXml(permissionBits.hasPermission(UserConfiguration.WEBDAV_XML));
        moduleAccess.setWebmail(permissionBits.hasPermission(UserConfiguration.WEBMAIL));
        moduleAccess.setDelegateTask(permissionBits.hasPermission(UserConfiguration.DELEGATE_TASKS));
        moduleAccess.setEditGroup(permissionBits.hasPermission(UserConfiguration.EDIT_GROUP));
        moduleAccess.setEditResource(permissionBits.hasPermission(UserConfiguration.EDIT_RESOURCE));
        moduleAccess.setEditPassword(permissionBits.hasPermission(UserConfiguration.EDIT_PASSWORD));
        moduleAccess.setCollectEmailAddresses(permissionBits.hasPermission(UserConfiguration.COLLECT_EMAIL_ADDRESSES));
        moduleAccess.setMultipleMailAccounts(permissionBits.hasPermission(UserConfiguration.MULTIPLE_MAIL_ACCOUNTS));
        moduleAccess.setSubscription(permissionBits.hasPermission(UserConfiguration.SUBSCRIPTION));
        moduleAccess.setActiveSync(permissionBits.hasPermission(UserConfiguration.ACTIVE_SYNC));
        moduleAccess.setUSM(permissionBits.hasPermission(UserConfiguration.USM));
        moduleAccess.setOLOX20(permissionBits.hasPermission(UserConfiguration.OLOX20));
        moduleAccess.setPublication(permissionBits.hasPermission(UserConfiguration.PUBLICATION));
        moduleAccess.setDeniedPortal(permissionBits.hasPermission(UserConfiguration.DENIED_PORTAL));
        moduleAccess.setGlobalAddressBookDisabled(false == permissionBits.isGlobalAddressBookEnabled());
        moduleAccess.setPublicFolderEditable(publicFolderEditable);
        return moduleAccess;
    }

}
