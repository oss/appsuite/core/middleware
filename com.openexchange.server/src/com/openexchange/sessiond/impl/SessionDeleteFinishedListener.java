/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.sessiond.impl;

import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.delete.DeleteEvent;
import com.openexchange.groupware.delete.DeleteFinishedListener;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.session.RemovalReason;
import com.openexchange.sessiond.SessiondService;

/**
 * {@link SessionDeleteFinishedListener} - Cleanses sessions for deleted users/contexts.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class SessionDeleteFinishedListener implements DeleteFinishedListener {

    /**
     * Initializes a new {@link SessionDeleteFinishedListener}.
     */
    public SessionDeleteFinishedListener() {
        super();
    }

    @Override
    public void deleteFinished(DeleteEvent event) throws OXException {
        SessiondService sessiondService = ServerServiceRegistry.getInstance().getService(SessiondService.class);
        if (null == sessiondService) {
            return;
        }

        Context context = event.getContext();
        int type = event.getType();
        if (type == DeleteEvent.TYPE_USER) {
            int userId = event.getId();
            sessiondService.removeUserSessions(userId, context.getContextId(), RemovalReason.INVALIDATED);
        } else if (type == DeleteEvent.TYPE_CONTEXT) {
            if (sessiondService.isCentral()) {
                // Prefer cleansing sessions one-by-one for each affected user
                List<Integer> userIds = event.getUserIds();
                if (userIds == null) {
                    // No users specified
                    sessiondService.removeContextSessions(context.getContextId(), RemovalReason.INVALIDATED);
                } else {
                    // Remove for each affected user
                    for (Integer userId : userIds) {
                        sessiondService.removeUserSessions(userId.intValue(), context.getContextId(), RemovalReason.INVALIDATED);
                    }
                }
            } else {
                sessiondService.removeContextSessions(context.getContextId(), RemovalReason.INVALIDATED);
            }
        }
    }

}
