/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.group.internal;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.database.DatabaseConnectionListeners.addAfterCommitCallbackElseExecute;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.codec.IntArrayCacheValueCodec;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.exception.OXException;
import com.openexchange.group.Group;
import com.openexchange.group.GroupStorage;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.ldap.LdapExceptionCode;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Strings;
import com.openexchange.server.services.ServerServiceRegistry;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;

/**
 * Implementation of the RDB group storage with caching capability.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.4
 */
public final class CachingGroupStorage implements GroupStorage {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(CachingGroupStorage.class);
    }

    /** Cache options using {@link GroupCodec} for cached groups */
    private static final CacheOptions<Group> OPTIONS_GROUP = CacheOptions.<Group> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.GROUP)
        .withCodecAndVersion(new GroupCodec())
    .build(); // formatter:on

    /** Cache options using {@link GroupIdsCodec} for cached group ids */
    private static final CacheOptions<int[]> OPTIONS_GROUP_IDS = CacheOptions.<int[]> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.GROUP)
        .withCodecAndVersion(IntArrayCacheValueCodec.getInstance())
    .build(); // formatter:on

    /**
     * Underlying group storage.
     */
    private final RdbGroupStorage delegate;

    /**
     * Default constructor.
     *
     * @param delegate The underlying group storage.
     */
    public CachingGroupStorage(final RdbGroupStorage delegate) {
        super();
        this.delegate = delegate;
    }

    @Override
    public boolean exists(int gid, Context context) throws OXException {
        Optional<Cache<Group>> optionalCache = optCache();
        if (optionalCache.isEmpty()) {
            // No cache available. Therefore simply delegate the call.
            return delegate.exists(gid, context);
        }

        // Cache is available
        Cache<Group> cache = optionalCache.get();
        if (cache.exists(cacheKey(cache, context, gid))) {
            return true;
        }

        return delegate.exists(gid, context);
    }

    @Override
    public Group getGroup(final int groupId, final Context ctx) throws OXException {
        return getGroup(groupId, true, ctx);
    }

    @Override
    public Group getGroup(final int groupId, boolean loadMembers, final Context ctx) throws OXException {
        return getGroups(new int[] { groupId }, loadMembers, ctx)[0];
    }

    @Override
    public Group[] getGroup(int[] groupIds, Context context) throws OXException {
        if (groupIds == null || groupIds.length <= 0) {
            return new Group[0];
        }
        return getGroups(groupIds, true, context);
    }

    private Group[] getGroups(int[] groupIds, boolean loadMembers, Context context) throws OXException {
        return getGroups(groupIds, loadMembers, context, optCache().orElse(null));
    }

    private Group[] getGroups(int[] groupIds, boolean loadMembers, Context context, Cache<Group> optionalCache) throws OXException {
        if (optionalCache == null) {
            // No cache available. Therefore simply delegate the call.
            return delegate.getGroup(groupIds, loadMembers, context);
        }

        // Cache is available
        Cache<Group> cache = optionalCache;
        int length = groupIds.length;
        TIntObjectMap<Group> groups = new TIntObjectHashMap<>(length);
        TIntList toLoad = null;

        TObjectIntMap<CacheKey> key2groupId = new TObjectIntHashMap<>(length);
        List<CacheKey> keys = Arrays.stream(groupIds).mapToObj(groupId -> createAndPutCacheKey(groupId, context, key2groupId, cache)).collect(CollectorUtils.toList(length));
        for (CacheKeyValue<Group> cacheKeyValue : cache.mget(keys)) {
            Group group = cacheKeyValue.getValueOrElse(null);
            int groupId = key2groupId.get(cacheKeyValue.getKey());
            if (group != null) {
                if (loadMembers && !group.isMemberSet()) {
                    if (toLoad == null) {
                        toLoad = new TIntArrayList(length);
                    }
                    toLoad.add(groupId);
                } else {
                    groups.put(groupId, group);
                }
            } else {
                if (toLoad == null) {
                    toLoad = new TIntArrayList(length);
                }
                toLoad.add(groupId);
            }
        }
        keys = null;

        if (toLoad != null) {
            Map<CacheKey, Group> values = HashMap.newHashMap(toLoad.size());
            for (Group loadedGroup : delegate.getGroup(toLoad.toArray(), context)) {
                int groupId = loadedGroup.getIdentifier();
                groups.put(groupId, loadedGroup);
                values.put(cacheKey(cache, context, groupId), loadedGroup);
            }
            cache.mput(values);
            toLoad = null;
        }

        Group[] retval = new Group[length];
        for (int i = length; i-- > 0;) {
            Group group = groups.get(groupIds[i]);
            if (group == null) {
                throw LdapExceptionCode.GROUP_NOT_FOUND.create(Integer.valueOf(groupIds[i]), Integer.valueOf(context.getContextId())).setPrefix("GRP");
            }
            retval[i] = group;
        }
        return retval;
    }

    private static CacheKey createAndPutCacheKey(int groupId, Context context, TObjectIntMap<CacheKey> key2groupId, Cache<Group> cache) {
        CacheKey cacheKey = cacheKey(cache, context, groupId);
        key2groupId.put(cacheKey, groupId);
        return cacheKey;
    }

    @Override
    public Group[] getGroups(final boolean loadMembers, final Context ctx) throws OXException {
        int[] groupIds;

        Optional<Cache<int[]>> optionalCache = optCache(OPTIONS_GROUP_IDS);
        if (optionalCache.isPresent()) {
            Cache<int[]> cache = optionalCache.get();
            CacheKey cacheKey = cacheKey(cache, ctx, SPECIAL_FOR_ALL_GROUP_IDS);
            groupIds = cache.get(cacheKey, k -> delegate.getGroupIds(ctx));
        } else {
            groupIds = delegate.getGroupIds(ctx);
        }

        if (groupIds.length <= 0) {
            return new Group[0];
        }
        return getGroups(groupIds, loadMembers, ctx);
    }

    @Override
    public Group[] listModifiedGroups(final Date modifiedSince, final Context ctx) throws OXException {
        int[] groupIds = delegate.listModifiedGroupIds(modifiedSince, ctx);
        if (groupIds.length <= 0) {
            return new Group[0];
        }
        return getGroups(groupIds, true, ctx);
    }

    @Override
    public Group[] listDeletedGroups(final Date modifiedSince, final Context ctx) throws OXException {
        return delegate.listDeletedGroups(modifiedSince, ctx);
    }

    @Override
    public Group[] searchGroups(final String pattern, final boolean loadMembers, final Context ctx) throws OXException {
        if (referencesAllGroups(pattern)) {
            return getGroups(loadMembers, ctx);
        }

        int[] groupIds = delegate.searchGroupIds(pattern, ctx);
        if (groupIds.length <= 0) {
            return new Group[0];
        }
        return getGroups(groupIds, loadMembers, ctx);
    }

    /**
     * Tests whether the given pattern references all groups or not.
     * <p>
     * E.g. <code>"*"</code>
     *
     * @param pattern The pattern to check
     * @return <code>true</code> if the pattern references all groups, <code>false</code> otherwise
     */
    private static boolean referencesAllGroups(String pattern) {
        if (Strings.isEmpty(pattern)) {
            // Empty pattern is interpreted as "all"
            return true;
        }

        int length = pattern.length();
        for (int i = length; i-- > 0;) {
            char ch = pattern.charAt(i);
            if ('*' != ch && !Strings.isWhitespace(ch)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void insertGroup(final Context ctx, final Connection con, final Group group, final StorageType type) throws OXException {
        delegate.insertGroup(ctx, con, group, type);
        tryInvalidateFromCache(ctx, con, SPECIAL_FOR_ALL_GROUP_IDS);
    }

    @Override
    public void deleteMember(final Context ctx, final Connection con, final Group group, final int[] members) throws OXException {
        delegate.deleteMember(ctx, con, group, members);
        tryInvalidateFromCache(ctx, con, group.getIdentifier());
    }

    @Override
    public void insertMember(final Context ctx, final Connection con, final Group group, final int[] members) throws OXException {
        delegate.insertMember(ctx, con, group, members);
        tryInvalidateFromCache(ctx, con, group.getIdentifier());
    }

    @Override
    public void updateGroup(final Context ctx, final Connection con, final Group group, final Date lastRead) throws OXException {
        delegate.updateGroup(ctx, con, group, lastRead);
        tryInvalidateFromCache(ctx, con, group.getIdentifier());
    }

    @Override
    public void deleteGroup(final Context ctx, final Connection con, final int groupId, final Date lastRead) throws OXException {
        delegate.deleteGroup(ctx, con, groupId, lastRead);
        tryInvalidateFromCache(ctx, con, groupId, SPECIAL_FOR_ALL_GROUP_IDS);
    }

    /**
     * Tries to invalidate the groups from the cache if the cache exists.
     *
     * @param ctx The context
     * @param connection The database connection used for the storage operation, or <code>null</code> if not available
     * @param groupIds The identifiers of the groups to invalidate
     * @return <code>true</code> if the groups were successfully invalidated, <code>false</code> otherwise
     */
    private static boolean tryInvalidateFromCache(Context ctx, Connection connection, int... groupIds) {
        Optional<Cache<Group>> optionalCache = optCache();
        if (optionalCache.isEmpty()) {
            return false;
        }
        return invalidateFromCache(ctx, optionalCache.get(), connection, groupIds);
    }

    /**
     * Invalidates the group identifiers from the given cache.
     * <p/>
     * If a suitable database connection is passed, invalidation will be performed after the transaction is committed, otherwise directly.
     *
     * @param ctx The context
     * @param cache The cache
     * @param connection The database connection used for the storage operation, or <code>null</code> if not available
     * @param groupIds The group identifiers to invalidate
     * @return <code>true</code> if the groups were successfully invalidated, <code>false</code> otherwise
     */
    private static boolean invalidateFromCache(Context ctx, Cache<Group> cache, Connection connection, int... groupIds) {
        try {
            addAfterCommitCallbackElseExecute(connection, c -> invalidateFromCache(cache, ctx, groupIds));
            return true ;
        } catch (OXException e) {
            LoggerHolder.LOG.error("Failed to invalidate from cache", e);
            return false;
        }
    }

    private static Optional<Cache<Group>> optCache() {
        return optCache(OPTIONS_GROUP);
    }

    private static <V> Optional<Cache<V>> optCache(CacheOptions<V> options) {
        CacheService service = ServerServiceRegistry.getInstance().getService(CacheService.class);
        if (null == service) {
            return Optional.empty();
        }
        return Optional.of(service.getCache(options));
    }

    /**
     * Creates a new cache key.
     *
     * @param cache The cache
     * @param ctx The context
     * @param groupId The group identifier
     * @return The newly created cache key
     */
    private static <V> CacheKey cacheKey(Cache<V> cache, Context ctx, int groupId) {
        return cache.newKey(asHashPart(ctx.getContextId()), Integer.toString(groupId));
    }

    /**
     * Invalidates the group identifiers from the given cache.
     * @param cache The cache
     * @param ctx The context
     * @param groupIds The group identifiers to invalidate
     *
     * @throws OXException If invalidation fails
     */
    private static void invalidateFromCache(Cache<Group> cache, Context ctx, int... groupIds) throws OXException {
        if (null == groupIds || 0 == groupIds.length) {
            return;
        }
        if (1 == groupIds.length) {
            cache.invalidate(cacheKey(cache, ctx, groupIds[0]));
        } else {
            cache.invalidate(IntStream.of(groupIds).mapToObj(i -> cacheKey(cache, ctx, i)).toList());
        }
    }

}
