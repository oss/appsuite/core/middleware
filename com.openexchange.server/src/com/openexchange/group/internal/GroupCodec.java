/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.group.internal;

import java.io.Writer;
import java.util.Date;
import java.util.UUID;
import org.json.JSONObject;
import org.json.SimpleJSONSerializer;
import com.openexchange.cache.v2.codec.Codecs;
import com.openexchange.cache.v2.codec.json.AbstractSimpleJSONObjectCacheValueCodec;
import com.openexchange.group.Group;

/**
 * {@link GroupCodec} - Cache codec for {@link Group}s.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class GroupCodec extends AbstractSimpleJSONObjectCacheValueCodec<Group> {

    private static final UUID CODEC_ID = UUID.fromString("8871c12b-b82f-4f2d-00fc-c0e11e000001");

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected void writeJson(Group value, Writer writer) throws Exception {
        writer.write('{');

        SimpleJSONSerializer.writeJsonValue("d", writer);
        writer.write(':');
        SimpleJSONSerializer.writeJsonValue(value.getDisplayName(), writer);

        writer.write(',');
        SimpleJSONSerializer.writeJsonValue("s", writer);
        writer.write(':');
        SimpleJSONSerializer.writeJsonValue(value.getSimpleName(), writer);

        writer.write(',');
        SimpleJSONSerializer.writeJsonValue("i", writer);
        writer.write(':');
        SimpleJSONSerializer.writeJsonValue(value.getIdentifier(), writer);

        Date lastModified = value.getLastModified();
        if (null != lastModified) {
            writer.write(',');
            SimpleJSONSerializer.writeJsonValue("l", writer);
            writer.write(':');
            SimpleJSONSerializer.writeJsonValue(lastModified.getTime(), writer);
        }

        int[] members = value.getMember();
        if (null != members) {
            writer.write(',');
            SimpleJSONSerializer.writeJsonValue("m", writer);
            writer.write(':');

            writer.write('[');

            boolean first = true;
            for (int member : members) {
                if (first) {
                    first = false;
                } else {
                    writer.write(',');
                }
                SimpleJSONSerializer.writeJsonValue(member, writer);
            }

            writer.write(']');
        }

        writer.write('}');
    }

    @Override
    protected Group parseJson(JSONObject jObject) throws Exception {
        Group group = new Group();
        group.setDisplayName(jObject.optString("d"));
        group.setSimpleName(jObject.optString("s"));
        group.setIdentifier(jObject.getInt("i"));
        long lastModifiedMillis = jObject.optLong("l", -1L);
        group.setLastModified(-1 == lastModifiedMillis ? null : new Date(lastModifiedMillis));
        group.setMember(Codecs.jsonArrayToIntArray(jObject.optJSONArray("m")));
        return group;
    }

}
