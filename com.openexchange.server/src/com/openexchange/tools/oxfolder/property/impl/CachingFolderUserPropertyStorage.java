/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tools.oxfolder.property.impl;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.database.DatabaseConnectionListeners.addAfterCommitCallbackElseExecute;
import static com.openexchange.java.Autoboxing.I;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheLoader;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.exception.OXException;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.tools.oxfolder.property.FolderUserPropertyStorage;

/**
 * {@link CachingFolderUserPropertyStorage}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.0
 */
public class CachingFolderUserPropertyStorage implements FolderUserPropertyStorage {

    /** Cache options for folder user properties */
    private static final CacheOptions<Map<Integer, Map<String, String>>> OPTIONS_PROPERTIES = CacheOptions.<Map<Integer, Map<String, String>>> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.FOLDER_USER_PROPERTY)
        .withCodecAndVersion(new FolderPropertiesCodec())
    .build(); // formatter:on

    /**
     * Proxy attribute for the object implementing the persistent methods.
     */
    private final RdbFolderUserPropertyStorage delegate;

    /**
     * Initializes a new {@link CachingFolderUserPropertyStorage}.
     *
     * @param delegate The database-backed delegate storage
     */
    public CachingFolderUserPropertyStorage(RdbFolderUserPropertyStorage delegate) {
        super();
        this.delegate = delegate;
    }

    @Override
    public void deleteFolderProperties(int contextId, int folderId, int[] userIds, Set<String> propertyKeys) throws OXException {
        delegate.deleteFolderProperties(contextId, folderId, userIds, propertyKeys);
        invalidateCache(contextId, userIds);
    }

    @Override
    public void deleteFolderProperties(int contextId, int folderId, int[] userIds, Set<String> propertyKeys, Connection connection) throws OXException {
        delegate.deleteFolderProperties(contextId, folderId, userIds, propertyKeys, connection);
        addAfterCommitCallbackElseExecute(connection, c -> invalidateCache(contextId, userIds));
    }

    @Override
    public boolean exists(int contextId, int folderId, int userId) throws OXException {
        return exists(contextId, folderId, userId, null);
    }

    @Override
    public boolean exists(int contextId, int folderId, int userId, Connection connection) throws OXException {
        Optional<Cache<Map<Integer, Map<String, String>>>> optCache = optCache();
        if (optCache.isEmpty()) {
            return delegate.exists(contextId, folderId, userId, connection);
        }
        return false == getFolderProperties(contextId, folderId, userId, connection).isEmpty();
    }

    @Override
    public Map<String, String> getFolderProperties(int contextId, int folderId, int userId) throws OXException {
        return getFolderProperties(contextId, folderId, userId, null);
    }

    @Override
    public Map<String, String> getFolderProperties(int contextId, int folderId, int userId, Connection connection) throws OXException {
        Optional<Cache<Map<Integer, Map<String, String>>>> optCache = optCache();
        if (optCache.isEmpty()) {
            return delegate.getFolderProperties(contextId, folderId, userId, connection);
        }
        Cache<Map<Integer, Map<String, String>>> cache = optCache.get();
        return cache.get(newCacheKey(cache, userId, contextId), new LoadPropertiesCallable(delegate, contextId, userId, connection)).getOrDefault(I(folderId), Collections.emptyMap());
    }

    @Override
    public Map<Integer, Map<String, String>> getFolderProperties(int contextId, int[] folderIds, int userId) throws OXException {
        return getFolderProperties(contextId, folderIds, userId, null);
    }

    @Override
    public Map<Integer, Map<String, String>> getFolderProperties(int contextId, int[] folderIds, int userId, Connection connection) throws OXException {
        if (null == folderIds) {
            return Collections.emptyMap();
        }

        int length = folderIds.length;
        if (length == 0) {
            return Collections.emptyMap();
        }

        if (1 == length) {
            int folderId = folderIds[0];
            return Collections.singletonMap(Integer.valueOf(folderId), getFolderProperties(contextId, folderId, userId, connection));
        }

        Optional<Cache<Map<Integer, Map<String, String>>>> optCache = optCache();
        if (optCache.isEmpty()) {
            return delegate.getFolderProperties(contextId, folderIds, userId, connection);
        }

        Cache<Map<Integer, Map<String, String>>> cache = optCache.get();
        Map<Integer, Map<String, String>> propertiesByFolderId = cache.get(newCacheKey(cache, userId, contextId), new LoadPropertiesCallable(delegate, contextId, userId, connection));
        Map<Integer, Map<String, String>> retval = LinkedHashMap.newLinkedHashMap(length);
        for (int folderId : folderIds) {
            retval.put(I(folderId), propertiesByFolderId.getOrDefault(I(folderId), Collections.emptyMap()));
        }
        return retval;
    }

    @Override
    public String getFolderProperty(int contextId, int folderId, int userId, String key) throws OXException {
        return getFolderProperty(contextId, folderId, userId, key, null);
    }

    @Override
    public String getFolderProperty(int contextId, int folderId, int userId, String propertyKey, Connection connection) throws OXException {
        if (null == propertyKey) {
            return null;
        }
        Map<String, String> properties = getFolderProperties(contextId, folderId, userId, connection);
        return null != properties ? properties.get(propertyKey) : null;
    }

    @Override
    public void insertFolderProperties(int contextId, int folderId, int userId, Map<String, String> properties) throws OXException {
        insertFolderProperties(contextId, folderId, userId, properties, null);
    }

    @Override
    public void insertFolderProperties(int contextId, int folderId, int userId, Map<String, String> properties, Connection connection) throws OXException {
        if (null == properties || properties.isEmpty()) {
            // Nothing to insert
            return;
        }

        delegate.insertFolderProperties(contextId, folderId, userId, properties, connection);
        addAfterCommitCallbackElseExecute(connection, c -> invalidateCache(contextId, userId));
    }

    @Override
    public void insertFolderProperty(int contextId, int folderId, int userId, String key, String value) throws OXException {
        insertFolderProperty(contextId, folderId, userId, key, value, null);
    }

    @Override
    public void insertFolderProperty(int contextId, int folderId, int userId, String propertyKey, String newValue, Connection connection) throws OXException {
        if (null == propertyKey || null == newValue) {
            return;
        }

        delegate.insertFolderProperty(contextId, folderId, userId, propertyKey, newValue, connection);
        addAfterCommitCallbackElseExecute(connection, c -> invalidateCache(contextId, userId));
    }

    @Override
    public void setFolderProperties(int contextId, int folderId, int userId, Map<String, String> properties) throws OXException {
        setFolderProperties(contextId, folderId, userId, properties, null);
    }

    @Override
    public void setFolderProperties(int contextId, int folderId, int userId, Map<String, String> properties, Connection connection) throws OXException {
        if (null == properties || properties.isEmpty()) {
            // Nothing to set
            return;
        }

        delegate.setFolderProperties(contextId, folderId, userId, properties, connection);
        addAfterCommitCallbackElseExecute(connection, c -> invalidateCache(contextId, userId));
    }

    @Override
    public void updateFolderProperties(int contextId, int folderId, int userId, Map<String, String> properties) throws OXException {
        updateFolderProperties(contextId, folderId, userId, properties, null);
    }

    @Override
    public void updateFolderProperties(int contextId, int folderId, int userId, Map<String, String> properties, Connection connection) throws OXException {
        if (null == properties || properties.isEmpty()) {
            // Nothing to update
            return;
        }

        delegate.updateFolderProperties(contextId, folderId, userId, properties, connection);
        addAfterCommitCallbackElseExecute(connection, c -> invalidateCache(contextId, userId));
    }

    @Override
    public void updateFolderProperty(int contextId, int folderId, int userId, String key, String value) throws OXException {
        updateFolderProperty(contextId, folderId, userId, key, value, null);
    }

    @Override
    public void updateFolderProperty(int contextId, int folderId, int userId, String propertyKey, String newValue, Connection connection) throws OXException {
        if (null == propertyKey) {
            return;
        }

        if (null == newValue) {
            deleteFolderProperty(contextId, folderId, userId, propertyKey, connection);
            return;
        }

        delegate.updateFolderProperty(contextId, folderId, userId, propertyKey, newValue, connection);
        addAfterCommitCallbackElseExecute(connection, c -> invalidateCache(contextId, userId));
    }

    /**
     * Generates a key for the cache holding user property data per folder, pointing to a certain user's folder properties.
     *
     * @param cache The cache
     * @param userId The identifier of the user to get the key for
     * @param contextId The context identifier
     * @return The cache key
     */
    private static CacheKey newCacheKey(Cache<Map<Integer, Map<String, String>>> cache, int userId, int contextId) {
        return cache.newKey(asHashPart(contextId), Integer.toString(userId));
    }

    /**
     * Optionally gets the cache holding user property data per folder.
     *
     * @return The cache, or <code>null</code> if the cache service is absent
     */
    private static Optional<Cache<Map<Integer, Map<String, String>>>> optCache() {
        CacheService cacheService = ServerServiceRegistry.getInstance().getService(CacheService.class);
        return null != cacheService ? Optional.of(cacheService.getCache(OPTIONS_PROPERTIES)) : Optional.empty();
    }

    /**
     * Invalidates the cached user properties for one or more folders.
     *
     * @param contextId The context identifier
     * @param userIds The identifiers of the users to invalidate the folder properties for
     */
    private static void invalidateCache(int contextId, int...userIds) {
        if (null == userIds || 0 == userIds.length) {
            return;
        }
        Optional<Cache<Map<Integer, Map<String, String>>>> optCache = optCache();
        if (optCache.isPresent()) {
            Cache<Map<Integer, Map<String, String>>> cache = optCache.get();
            List<CacheKey> keys = Arrays.stream(userIds).mapToObj(userId -> newCacheKey(cache, userId, contextId)).toList();
            try {
                cache.invalidate(keys);
            } catch (OXException e) {
                org.slf4j.LoggerFactory.getLogger(CachingFolderUserPropertyStorage.class).warn("Unexpected error invalidating cache", e);
            }
        }
    }

    /**
     * {@link LoadPropertiesCallable} - {@link Callable} to load a user's folder properties from the storage.
     *
     * @param storage The storage to load from
     * @param contextId The context identifier
     * @param userId The user identifier
     * @param readCon The read-only connection to use (<b>optional</b>, pass <code>null</code> to fetch a new one from connection pool)
     */
    private static final record LoadPropertiesCallable(RdbFolderUserPropertyStorage storage, int contextId, int userId, Connection optConnection) implements CacheLoader<Map<Integer, Map<String, String>>> {

        @Override
        public Map<Integer, Map<String, String>> load(CacheKey key) throws Exception {
            return null == optConnection ? storage.loadFolderProperties(contextId, userId) : storage.loadFolderProperties(contextId, userId, optConnection);
        }
    }

}
