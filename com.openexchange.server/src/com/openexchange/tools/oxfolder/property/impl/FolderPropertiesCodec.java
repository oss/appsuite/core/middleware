/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tools.oxfolder.property.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.common.collect.ImmutableBiMap;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;

/**
 * {@link FolderPropertiesCodec} - Cache codec for folder properties.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class FolderPropertiesCodec extends AbstractJSONObjectCacheValueCodec<Map<Integer, Map<String, String>>> {

    private static final UUID CODEC_ID = UUID.fromString("4f0bf1a3-3b51-4226-00fc-c0e11e000001");

    /** Mapping of well-known property names to compact replacements */
    private static final ImmutableBiMap<String, String> PROPERTY_REPLACEMENTS = ImmutableBiMap.of( // @formatter:off
        "con/subscribed", "^1", "con/usedForSync", "^2",
        "cal/subscribed", "^3", "cal/usedForSync", "^4", "cal/color", "^5",
        "tsk/subscribed", "^6", "tsk/usedForSync", "^7",
        "inf/subscribed", "^8"
    );  // @formatter:on

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONObject writeJson(Map<Integer, Map<String, String>> value) throws Exception {
        if (null == value || value.isEmpty()) {
            return JSONObject.EMPTY_OBJECT;
        }
        JSONObject jsonObject = new JSONObject(value.size());
        for (Entry<Integer, Map<String, String>> entry : value.entrySet()) {
            jsonObject.put(entry.getKey().toString(), encodeProperties(entry.getValue()));
        }
        return jsonObject;
    }

    @Override
    protected Map<Integer, Map<String, String>> parseJson(JSONObject jsonObject) throws Exception {
        if (null == jsonObject || jsonObject.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<Integer, Map<String, String>> multiMap = HashMap.newHashMap(jsonObject.length());
        for (Entry<String, Object> entry : jsonObject.entrySet()) {
            multiMap.put(Integer.valueOf(entry.getKey()), decodeProperties((JSONObject) entry.getValue()));
        }
        return multiMap;
    }

    private static JSONObject encodeProperties(Map<String, String> properties) throws JSONException {
        if (null == properties || properties.isEmpty()) {
            return JSONObject.EMPTY_OBJECT;
        }
        JSONObject jsonObject = new JSONObject(properties.size());
        for (Entry<String, String> entry : properties.entrySet()) {
            String key = PROPERTY_REPLACEMENTS.getOrDefault(entry.getKey(), entry.getKey());
            String value = entry.getValue();
            if ("true".equals(value)) {
                jsonObject.put(key, 1);
            } else if ("false".equals(value)) {
                jsonObject.put(key, 0);
            } else {
                jsonObject.put(key, value);
            }
        }
        return jsonObject;
    }

    private static Map<String, String> decodeProperties(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject || jsonObject.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<String, String> properties = HashMap.newHashMap(jsonObject.length());
        for (Entry<String, Object> entry : jsonObject.entrySet()) {
            String key = PROPERTY_REPLACEMENTS.inverse().getOrDefault(entry.getKey(), entry.getKey());
            Object value = entry.getValue();
            if (value instanceof Number number) {
                if (1 == number.intValue()) {
                    properties.put(key, "true");
                } else if (0 == number.intValue()) {
                    properties.put(key, "false");
                } else {
                    throw new JSONException("Unexpected numeric value " + number);
                }
            } else {
                properties.put(key, (String) value);
            }
        }
        return properties;
    }

}
