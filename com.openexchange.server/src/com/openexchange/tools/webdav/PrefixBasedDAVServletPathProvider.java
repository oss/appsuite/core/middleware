/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.tools.webdav;

/**
 * {@link PrefixBasedDAVServletPathProvider} implements {@link DAVServletPathProvider} using a static prefix.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class PrefixBasedDAVServletPathProvider implements DAVServletPathProvider {

    private final String internalPathPrefix;
    private final String externalPathPrefix;

    /**
     * Initializes a new {@link PrefixBasedDAVServletPathProvider}.
     * 
     * @param internalPathPrefix The static <i>internal</i> path prefix to match, e.g. <code>/servlet/dav/caldav</code>
     * @param externalPathPrefix The static <i>external</i> path prefix to match, e.g. <code>/caldav</code>, or <code>null</code> to
     *            only match against the internal path prefix
     */
    public PrefixBasedDAVServletPathProvider(String internalPathPrefix, String externalPathPrefix) {
        super();
        this.internalPathPrefix = internalPathPrefix;
        this.externalPathPrefix = externalPathPrefix;
    }

    @Override
    public boolean servesPath(String path) {
        return null != path && (path.startsWith(internalPathPrefix) || null != externalPathPrefix && path.startsWith(externalPathPrefix));
    }

    @Override
    public String toString() {
        return "PrefixBasedDAVServletPathProvider [internalPathPrefix=" + internalPathPrefix + ", externalPathPrefix=" + externalPathPrefix + "]";
    }

}
