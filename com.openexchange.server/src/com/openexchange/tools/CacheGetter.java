/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tools;

import java.util.Map;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.codec.CacheValueCodec;
import com.openexchange.exception.OXException;

/**
 * {@link CacheGetter} - Interface used for chaining multiple cache get operations for {@link CacheService#mget(java.util.Map)}.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public interface CacheGetter<V> {

    /**
     * Gets the cache value codec to (de-) serialize cached data.
     *
     * @return The cache codec
     */
    CacheValueCodec<?> getCodec();

    /**
     * Gets the cache key of the item being retrieved from cache.
     *
     * @return The cache key
     */
    CacheKey getKey();

    /**
     * Optionally retrieves and checks the cached value after it has been acquired from cache.
     *
     * @param cachedValues The values retrieved from cache
     * @return The value, or <code>null</code> if not set or applicable
     * @throws OXException If getting the cached value fails, or if the value cannot be used
     */
    default V optValue(Map<CacheKey, CacheKeyValue<Object>> cachedValues) throws OXException {
        CacheKeyValue<Object> cacheValue = cachedValues.get(getKey());
        return cacheValue == null ? null : optValue(cacheValue);
    }

    /**
     * Optionally retrieves and checks the cached value after it has been acquired from cache.
     *
     * @param cacheValue The cache key value
     * @return The value, or <code>null</code> if not set or applicable
     * @throws OXException If getting the cached value fails, or if the value cannot be used
     */
    V optValue(CacheKeyValue<?> cacheValue) throws OXException;

}
