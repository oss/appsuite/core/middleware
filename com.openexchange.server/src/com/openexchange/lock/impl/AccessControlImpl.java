/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.lock.impl;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.slf4j.Logger;
import com.openexchange.exception.OXException;
import com.openexchange.lock.AccessControl;

/**
 * {@link AccessControlImpl}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.3
 */
public final class AccessControlImpl implements AccessControl {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(AccessControlImpl.class);
    }

    private static final ConcurrentMap<Key, AccessControlImpl> CONTROLS = new ConcurrentHashMap<>(512);

    /**
     * Gets the associated access control for given session
     *
     * @param id The identifier
     * @param max The max. grants
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The access control
     * @throws OXException If access control cannot be returned
     */
    public static AccessControlImpl getAccessControl(String id, int max, int userId, int contextId) throws OXException {
        Key key = Key.newKey(id, userId, contextId);

        AccessControlImpl accessControl = null;
        while (null == accessControl) {
            accessControl = CONTROLS.get(key);
            if (null == accessControl) {
                AccessControlImpl newAccessControl = new AccessControlImpl(max, key);
                accessControl = CONTROLS.putIfAbsent(key, newAccessControl);
                if (null == accessControl) {
                    // Current thread grabbed the slot
                    accessControl = newAccessControl;
                } else if (accessControl.isNotAlive()) {
                    // No more alive... Retry
                    accessControl = null;
                } else if (accessControl.maxAccess != max) {
                    // Decrement the previously incremented "in-use" counter from call to AccessControlImpl.isActive()
                    accessControl.decrementInUse();
                    throw OXException.general("Access control requested with different max. number of grants");
                }
            } else if (accessControl.isNotAlive()) {
                // No more alive... Retry
                accessControl = null;
            } else if (accessControl.maxAccess != max) {
                // Decrement the previously incremented "in-use" counter from call to AccessControlImpl.isActive()
                accessControl.decrementInUse();
                throw OXException.general("Access control requested with different max. number of grants");
            }
        }
        // Leave...
        return accessControl;
    }

    // -------------------------------------------------------------------------------------------------------------

    private final AtomicReference<Object> valueReference;
    private final Condition accessible;
    private final Key key;
    private final Lock lock;
    private final int maxAccess;
    private int inUse;
    private int grants;

    /**
     * Initializes a new {@link AccessControl}.
     */
    private AccessControlImpl(int maxAccess, Key key) {
        super();
        this.maxAccess = maxAccess;
        this.key = key;
        lock = new ReentrantLock();
        accessible = lock.newCondition();
        inUse = 1; // Apparently... the creating thread
        grants = 0;
        valueReference = new AtomicReference<>(null);
    }

    /**
     * Acquires a grant from this access control; waiting for an available grant if needed.
     *
     * @throws InterruptedException If interrupted while waiting for a grant
     */
    @Override
    public void acquireGrant() throws InterruptedException {
        lock.lockInterruptibly();
        try {
            while (grants >= maxAccess) {
                accessible.await();
            }
            grants++;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean tryAcquireGrant() {
        lock.lock();
        try {
            if (grants >= maxAccess) {
                return false;
            }
            grants++;
            return true;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean tryAcquireGrant(long timeout, TimeUnit unit) throws InterruptedException {
        long nanos = unit.toNanos(timeout);
        lock.lockInterruptibly();
        try {
            while (grants >= maxAccess) {
                if (nanos <= 0L) {
                    return false;
                }
                nanos = accessible.awaitNanos(nanos);
            }
            grants++;
            return true;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Checks if this access control is not alive
     *
     * @return <code>true</code> if not alive; otherwise <code>false</code> (if still alive)
     */
    private boolean isNotAlive() {
        return !isAlive();
    }

    /**
     * Checks if this access control is still alive
     *
     * @return <code>true</code> if alive; otherwise <code>false</code>
     */
    private boolean isAlive() {
        lock.lock();
        try {
            if (inUse <= 0) {
                return false;
            }

            inUse++;
            return true;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Decrements "in-use" counter.
     */
    private void decrementInUse() {
        lock.lock();
        try {
            inUse--;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean release() {
        return release(true);
    }

    @Override
    public boolean release(boolean acquired) {
        lock.lock();
        try {
            inUse--;
            if (acquired) {
                grants--;
            }

            if (inUse == 0) {
                // The last one to release
                if (grants > 0) {
                    LoggerHolder.LOG.error("Invalid state: Access control instance no more in use, but still grants open for {}", key);
                }
                CONTROLS.remove(key);
                return true;
            }

            accessible.signal();
            return false;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void close() {
        release();
    }

    @Override
    public boolean valueSupported() {
        return true;
    }

    @Override
    public Object getValue() {
        return valueReference.get();
    }

    @Override
    public void setValue(Object value) {
        valueReference.set(value);
    }

    // -------------------------------------------------------------------------------------------------------------

    private static final class Key implements Comparable<Key> {

        static Key newKey(String id, int userId, int contextId) {
            return new Key(id, userId, contextId);
        }

        private final int contextId;
        private final int userId;
        private final int hash;
        private final String id;

        /**
         * Initializes a new {@link Key}.
         *
         * @param userId The user identifier
         * @param contextId The context identifier
         */
        Key(String id, int userId, int contextId) {
            super();
            this.id = id;
            this.userId = userId;
            this.contextId = contextId;

            int prime = 31;
            int result = prime * 1 + contextId;
            result = prime * result + userId;
            result = prime * result + (null == id ? 0 : id.hashCode());
            hash = result;
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Key)) {
                return false;
            }
            Key other = (Key) obj;
            if (contextId != other.contextId) {
                return false;
            }
            if (userId != other.userId) {
                return false;
            }
            if (id == null) {
                if (other.id != null) {
                    return false;
                }
            } else if (!id.equals(other.id)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("[contextId=").append(contextId).append(", userId=").append(userId).append(", ");
            if (id != null) {
                builder.append("id=").append(id);
            }
            builder.append(']');
            return builder.toString();
        }

        @Override
        public int compareTo(Key o) {
            int c = Integer.compare(contextId, o.contextId);
            if (c != 0) {
                return c;
            }
            c = Integer.compare(userId, o.userId);
            if (c != 0) {
                return c;
            }
            return id == null ? (o.id == null ? 0 : -1) : (o.id == null ? 1 : id.compareTo(o.id));
        }
    }

}
