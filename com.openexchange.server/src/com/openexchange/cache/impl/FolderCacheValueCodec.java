/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.impl;

import static com.openexchange.java.Autoboxing.I;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONCoercion;
import org.json.JSONException;
import com.openexchange.cache.v2.codec.Codecs;
import com.openexchange.cache.v2.codec.json.AbstractJSONArrayCacheValueCodec;
import com.openexchange.folderstorage.FolderPermissionType;
import com.openexchange.folderstorage.Permissions;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.groupware.container.FolderPathObject;
import com.openexchange.java.Strings;
import com.openexchange.server.impl.OCLPermission;

/**
 * {@link FolderCacheValueCodec} - The codec for database folder objects.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class FolderCacheValueCodec extends AbstractJSONArrayCacheValueCodec<FolderObject> {

    private static final UUID CODEC_ID = UUID.fromString("b9214830-a0dc-426a-89db-537af5a08ff7");

    /**
     * Initializes a new {@link FolderCacheValueCodec}.
     */
    public FolderCacheValueCodec() {
        super();
    }

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONArray writeJson(FolderObject value) throws Exception {
        JSONArray jFolder = new JSONArray(16);

        jFolder.put(value.getObjectID());
        jFolder.put(value.getParentFolderID());

        jFolder.put(value.getCreatedBy());
        jFolder.put(value.getModifiedBy());
        jFolder.put(value.getCreationDate().getTime());
        jFolder.put(value.getLastModified().getTime());

        jFolder.put(compressName(value.getFolderName(), ""));

        jFolder.put(value.getModule());
        jFolder.put(value.getType());
        jFolder.put(value.getPermissionFlag());
        jFolder.put(value.hasSubfolders() ? 1 : 0);
        jFolder.put(value.isDefaultFolder() ? 1 : 0);
        jFolder.put(value.getOriginPath() == null ? "" : value.getOriginPath().toString());

        jFolder.put(JSONCoercion.coerceToJSON(value.getMeta()));
        jFolder.put(JSONCoercion.coerceToJSON(value.getSubfolderIds()));
        jFolder.put(writePermissions(value.getPermissions()));

        return jFolder;
    }

    private static JSONArray writePermissions(List<OCLPermission> permissions) {
        if (permissions == null) {
            return null;
        }
        JSONArray jPermissions = new JSONArray(permissions.size());
        for (OCLPermission permission : permissions) {
            JSONArray jPermission = new JSONArray(6);
            jPermission.put(permission.getEntity());
            jPermission.put(permission.isGroupPermission() ? 1 : 0);
            jPermission.put(Permissions.createPermissionBits(permission.getFolderPermission(), permission.getReadPermission(), permission.getWritePermission(), permission.getDeletePermission(), permission.isFolderAdmin()));
            jPermission.put(permission.getSystem());
            jPermission.put(I(permission.getType() == null ? FolderPermissionType.NORMAL.getTypeNumber() : permission.getType().getTypeNumber()));
            jPermission.put(permission.getPermissionLegator() == null ? "" : permission.getPermissionLegator());
            jPermissions.put(jPermission);
        }
        return jPermissions;
    }

    @Override
    protected FolderObject parseJson(JSONArray jFolder) throws Exception {
        FolderObject fo = new FolderObject();

        fo.setObjectID(jFolder.getInt(0));
        fo.setParentFolderID(jFolder.getInt(1));

        fo.setCreatedBy(jFolder.getInt(2));
        fo.setModifiedBy(jFolder.getInt(3));
        fo.setCreationDate(new Date(jFolder.getLong(4))); // NOSONARLINT
        fo.setLastModified(new Date(jFolder.getLong(5))); // NOSONARLINT

        String name = decompressName(jFolder.optString(6, ""));
        if (Strings.isNotEmpty(name)) {
            fo.setFolderName(name);
        }

        fo.setModule(jFolder.getInt(7));
        fo.setType(jFolder.getInt(8));
        fo.setPermissionFlag(jFolder.getInt(9));
        fo.setSubfolderFlag(jFolder.optInt(10, 0) > 0);
        fo.setDefaultFolder(jFolder.optInt(11, 0) > 0);
        String optOriginPath = jFolder.optString(12, "");
        if (Strings.isNotEmpty(optOriginPath)) {
            fo.setOriginPath(FolderPathObject.parseFrom(optOriginPath));
        }

        Map<String, Object> meta = (Map<String, Object>) JSONCoercion.coerceToNative(jFolder.optJSONObject(13));
        if (meta != null) {
            fo.setMeta(meta);
        }
        List<Integer> subfolderIds = (List<Integer>) JSONCoercion.coerceToNative(jFolder.optJSONArray(14));
        if (subfolderIds != null) {
            fo.setSubfolderIds(subfolderIds);
        }
        List<OCLPermission> permissions = parsePermissions(jFolder.optJSONArray(15));
        if (permissions != null) {
            fo.setPermissions(permissions);
        }
        return fo;
    }

    private static List<OCLPermission> parsePermissions(JSONArray jPermissions) throws JSONException {
        if (jPermissions == null) {
            return null; // NOSONARLINT
        }

        List<OCLPermission> permissions = new ArrayList<>(jPermissions.length());
        for (Object o : jPermissions) {
            JSONArray jPermission = (JSONArray) o;
            OCLPermission permission = new OCLPermission();
            permission.setEntity(jPermission.getInt(0));
            permission.setGroupPermission(jPermission.optInt(1, 0) > 0);
            // Obtain & parse permission bits
            int[] permissionBits = Permissions.parsePermissionBits(jPermission.optInt(2, 0));
            permission.setFolderPermission(permissionBits[0]);
            permission.setReadObjectPermission(permissionBits[1]);
            permission.setWriteObjectPermission(permissionBits[2]);
            permission.setDeleteObjectPermission(permissionBits[3]);
            permission.setFolderAdmin(permissionBits[4] > 0);
            permission.setSystem(jPermission.getInt(3));
            permission.setType(FolderPermissionType.getType(jPermission.optInt(4, FolderPermissionType.NORMAL.getTypeNumber())));
            String legator = jPermission.optString(5, "");
            if (Strings.isNotEmpty(legator)) {
                permission.setPermissionLegator(legator);
            }
            permissions.add(permission);
        }
        return permissions;
    }

    /**
     * A list of replacements
     */
    private static final List<String[]> NAME_REPLACEMENTS = List.of(
        new String[] {"Global address book", "^G"},
        new String[] {"Private folders", "^P"},
        new String[] {"Public folders", "^U"},
        new String[] {"Collected addresses", "^A"},
        new String[] {"Calendar", "^C"},
        new String[] {"Tasks", "^T"},
        new String[] {"Contacts", "^O"},
        new String[] {"private", "^p"},
        new String[] {"public", "^u"},
        new String[] {"shared", "^s"},
        new String[] {"infostore", "^i"},
        new String[] {"system_ldap", "^l"},
        new String[] {"userstore", "^t"}
    );

    private static String compressName(String name, String def) {
        return name == null ? def : Codecs.replace(name, NAME_REPLACEMENTS);
    }

    private static String decompressName(String compressedName) {
        return Codecs.unreplace(compressedName, NAME_REPLACEMENTS);
    }

}
