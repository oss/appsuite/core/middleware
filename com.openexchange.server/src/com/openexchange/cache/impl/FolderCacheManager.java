/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.impl;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.database.DatabaseConnectionListeners.addAfterCommitCallback;
import static com.openexchange.java.Autoboxing.I;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.ajax.fields.DataFields;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.CacheLoader;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;
import com.openexchange.database.DatabaseConnectionListener;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.FolderStorage;
import com.openexchange.folderstorage.cache.memory.FolderMapManagement;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.java.util.Tools;
import com.openexchange.log.LogProperties;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.tools.oxfolder.OXFolderExceptionCode;
import com.openexchange.tools.oxfolder.OXFolderProperties;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;

/**
 * {@link FolderCacheManager} - Holds a cache for instances of {@link FolderObject}
 * <p>
 * <b>NOTE:</b> Only cloned versions of {@link FolderObject} instances are put into or received from cache. That prevents the danger of
 * further working on and therefore changing cached instances.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class FolderCacheManager {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(FolderCacheManager.class);

    /** Cache options for database folders */
    private static final CacheOptions<FolderObject> OPTIONS = CacheOptions.<FolderObject> builder()
        .withCoreModuleName(CoreModuleName.DATABASE_FOLDER)
        .withCodecAndVersion(new FolderCacheValueCodec())
        .withFireCacheEvents(true)
        .build();

    /**
     * Checks if folder cache is enabled (through configuration).
     *
     * @return <code>true</code> if folder cache is enabled; otherwise <code>false</code>
     * @deprecated Prefer to use {@link #optInstance()}
     */
    @Deprecated
    public static boolean isEnabled() {
        return OXFolderProperties.isEnableFolderCache();
    }

    private static final FolderCacheManager INSTANCE = new FolderCacheManager();

    /**
     * Gets the singleton instance of folder cache {@link FolderCacheManager manager}.
     *
     * @return The singleton instance
     * @throws OXException If not enabled via <code>"ENABLE_FOLDER_CACHE"</code> property
     * @deprecated Prefer to use {@link #optInstance()}
     */
    @Deprecated
    public static FolderCacheManager getInstance() throws OXException {
        FolderCacheManager instance = optInstance();
        if (instance == null) {
            throw OXFolderExceptionCode.CACHE_NOT_ENABLED.create();
        }
        return instance;
    }

    /**
     * Gets the singleton instance of folder cache {@link FolderCacheManager manager} or <code>null</code> if disabled.
     *
     * @return The singleton instance or <code>null</code> if disabled
     */
    public static FolderCacheManager optInstance() {
        return OXFolderProperties.isEnableFolderCache() ? INSTANCE : null;
    }

    // -------------------------------------------------------------------------------------------------------------------------------

    private final AtomicReference<Cache<FolderObject>> folderCacheRef;

    /**
     * Initializes a new {@link FolderCacheManager}.
     */
    private FolderCacheManager() {
        super();
        folderCacheRef = new AtomicReference<>();
    }

    /**
     * Initializes cache reference.
     *
     * @param cacheService The cache service that appeared
     */
    public void initCache(CacheService cacheService) {
        folderCacheRef.compareAndSet(null, cacheService.getCache(OPTIONS));
    }

    /**
     * Releases cache reference.
     */
    public void releaseCache() {
        folderCacheRef.set(null);
    }

    /**
     * Creates a new cache key for given arguments.
     *
     * @param context The context providing the identifier used as group
     * @param folderId The folder/object identifier
     * @param folderCache The cache to use
     * @return The newly created cache key
     */
    private static CacheKey getCacheKeyUsing(Context context, int folderId, Cache<FolderObject> folderCache) {
        return getCacheKeyUsing(context.getContextId(), folderId, folderCache);
    }

    /**
     * Creates a new cache key for given arguments.
     *
     * @param contextId The context identifier used as group
     * @param folderId The folder/object identifier
     * @param folderCache The cache to use
     * @return The newly created cache key
     */
    private static CacheKey getCacheKeyUsing(int contextId, int folderId, Cache<FolderObject> folderCache) {
        return folderCache.newGroupMemberKey(asHashPart(contextId), Integer.valueOf(folderId));
    }

    /**
     * Clears all cache entries for specified context.
     *
     * @param context The context to drop for
     */
    public void clearFor(Context context) {
        Cache<FolderObject> folderCache = folderCacheRef.get();
        if (folderCache != null) {
            try {
                folderCache.invalidateGroup(folderCache.newGroupKey(asHashPart(context.getContextId())));
            } catch (Exception x) {
                // Ignore
                LOG.trace("", x);
            }
        }
    }

    /**
     * Fetches <code>FolderObject</code> that matches given folder identifier.
     * <p>
     * If none found or <code>fromCache</code> is not set, the folder is loaded from database and automatically put into cache.
     *
     * @param folderId The identifier of the folder
     * @param fromCache <code>true</code> to attempt fetching appropriate folder from cache; otherwise <code>false</code> to omit cache look-up and load folder directly from database (and then put freshly loaded folder into cache probably replacing an outdated version)
     * @param context The context in which the folder resides
     * @param readCon A read-only connection (<b>optional</b>, pass <code>null</code> to fetch a new one from connection pool)
     * @throws OXException If a caching error occurs
     */
    public FolderObject getFolderObject(int folderId, boolean fromCache, Context context, Connection readCon) throws OXException {
        if (!fromCache) {
            // Load from database w/o checking cache
            FolderObject loaded = loadFolderObject(folderId, context, readCon);
            Cache<FolderObject> folderCache = folderCacheRef.get();
            if (folderCache != null) {
                folderCache.put(getCacheKeyUsing(context, folderId, folderCache), loaded);
            }
            return loaded;
        }

        Cache<FolderObject> folderCache = folderCacheRef.get();
        if (null == folderCache) {
            throw OXFolderExceptionCode.CACHE_NOT_ENABLED.create();
        }

        // Look-up cache w/ load-through
        return folderCache.get(getCacheKeyUsing(context, folderId, folderCache), new LoadFolderObjectCallable(folderId, context, readCon));
    }

    /**
     * Fetches <code>FolderObject</code> which matches given object identifier.
     *
     * @param folderId The identifier of the folder
     * @param context The context in which the folder resides
     * @return The matching <code>FolderObject</code> instance; else <code>null</code>
     * @throws OXException If fetch from cache fails
     */
    public FolderObject getFolderObject(int folderId, Context context) throws OXException {
        Cache<FolderObject> folderCache = folderCacheRef.get();
        if (null == folderCache) {
            return null;
        }

        return folderCache.get(getCacheKeyUsing(context, folderId, folderCache));
    }

    /**
     * Fetches those <code>FolderObject</code>s that match given folder identifiers.
     *
     * @param folderIds The folder identifiers
     * @param loadIfAbsent <code>true</code> to load folders if absent; otherwise <code>false</code>
     * @param context The context in which the folders reside
     * @return The matching <code>FolderObject</code> instances
     * @throws OXException If fetch from cache fails
     */
    public List<FolderObject> getTrimedFolderObjects(int[] folderIds, boolean loadIfAbsent, Context context) throws OXException {
        Cache<FolderObject> folderCache = folderCacheRef.get();
        if (null == folderCache) {
            return Collections.emptyList();
        }

        int contextId = context.getContextId();
        List<CacheKey> cacheKeys = new ArrayList<>(folderIds.length);
        TObjectIntMap<CacheKey> key2folderId = loadIfAbsent ? new TObjectIntHashMap<>(folderIds.length) : null;
        for (int j = 0; j < folderIds.length; j++) {
            CacheKey cacheKey = getCacheKeyUsing(contextId, folderIds[j], folderCache);
            if (key2folderId != null) {
                key2folderId.put(cacheKey, j);
            }
            cacheKeys.add(cacheKey);
        }

        List<FolderObject> ret = new ArrayList<FolderObject>(folderIds.length);
        Map<CacheKey, FolderObject> values = loadIfAbsent ? HashMap.newHashMap(folderIds.length) : null;
        for (CacheKeyValue<FolderObject> keyValue : folderCache.mget(cacheKeys)) {
            FolderObject folderObject = keyValue.getValueOrElse(null);
            if (folderObject != null) {
                ret.add(folderObject);
            } else if (loadIfAbsent) {
                if (key2folderId == null) { // NOSONARLINT
                    // Cannot happen. Just to avoid IDE rendering a "Potential null pointer access" warning
                    throw new IllegalStateException("The \"key2folderId\" TObjectIntMap reference is null, but shouldn't");
                }
                if (values == null) { // NOSONARLINT
                    // Cannot happen. Just to avoid IDE rendering a "Potential null pointer access" warning
                    throw new IllegalStateException("The \"values\" HashMap reference is null, but shouldn't");
                }
                int folderId = key2folderId.get(keyValue.getKey());
                FolderObject loaded = loadFolderObjectInternal(folderId, context, null);
                values.put(keyValue.getKey(), loaded);
                ret.add(loaded);
            }
        }
        if (values != null) {
            folderCache.mput(values);
        }
        return ret;
    }

    /**
     * Loads the folder from database which matches given object id and puts it into cache (possibly replacing an existent value).
     * <p>
     * <b>NOTE:</b> This method returns a clone of cached <code>FolderObject</code> instance. Thus any modifications made to the referenced
     * object will not affect cached version
     *
     * @param folderId The identifier of the folder
     * @param context The context in which the folder resides
     * @param readCon A read-only connection (<b>optional</b>, pass <code>null</code> to fetch a new one from connection pool)
     * @return The matching <code>FolderObject</code> instance fetched from storage
     * @throws OXException If a caching error occurs
     */
    public FolderObject loadFolderObject(int folderId, Context context, Connection readCon) throws OXException {
        Cache<FolderObject> folderCache = folderCacheRef.get();
        if (null == folderCache) {
            throw OXFolderExceptionCode.CACHE_NOT_ENABLED.create();
        }

        FolderObject loaded = loadFolderObjectInternal(folderId, context, readCon);
        folderCache.put(getCacheKeyUsing(context, folderId, folderCache), loaded);
        return loaded;
    }

    /**
     * If the specified folder object is not already in cache, it is put into cache.
     * <p>
     * <b>NOTE:</b> This method puts a clone of given <code>FolderObject</code> instance into cache. Thus any modifications made to the
     * referenced object will not affect cached version
     *
     * @param folderObj The folder object
     * @param context The context
     * @return The existent folder object available in cache, or <tt>null</tt> if there was none (indicating that given folder has been put into cache)
     * @throws OXException If put-if-absent operation fails
     */
    public void putIfAbsent(FolderObject folderObj, Context context) throws OXException {
        if (folderObj == null) {
            throw new IllegalArgumentException("Folder must not be null");
        }
        if (!folderObj.containsObjectID()) {
            throw OXFolderExceptionCode.MISSING_FOLDER_ATTRIBUTE.create(DataFields.ID, I(-1), I(context.getContextId()));
        }

        Cache<FolderObject> folderCache = folderCacheRef.get();
        if (null == folderCache) {
            throw OXFolderExceptionCode.CACHE_NOT_ENABLED.create();
        }

        // Do "put if absent"
        folderCache.putIfAbsent(getCacheKeyUsing(context, folderObj.getObjectID(), folderCache), folderObj);
    }

    /**
     * Simply puts given <code>FolderObject</code> into cache if object's identifier is different from <code>0</code> (zero).
     *
     * @param folderObj The folder object
     * @param context The context
     * @throws OXException If a caching error occurs
     */
    public void putFolderObject(FolderObject folderObj, Context context) throws OXException {
        putFolderObject(folderObj, context, true);
    }

    /**
     * Simply puts given <code>FolderObject</code> into cache if object's id is different to zero.
     * <p>
     * If flag <code>overwrite</code> is set to <code>false</code> then this method returns immediately if cache already holds an entry.
     *
     * @param folderObj The folder object to put into cache
     * @param context The context
     * @param overwrite <code>true</code> to overwrite; otherwise <code>false</code>
     * @throws OXException If a caching error occurs
     */
    public void putFolderObject(FolderObject folderObj, Context context, boolean overwrite) throws OXException {
        if (!overwrite) {
            putIfAbsent(folderObj, context);
            return;
        }

        // Put into cache
        if (folderObj == null) {
            throw new IllegalArgumentException("Folder must not be null");
        }
        if (!folderObj.containsObjectID()) {
            throw OXFolderExceptionCode.MISSING_FOLDER_ATTRIBUTE.create(DataFields.ID, I(-1), I(context.getContextId()));
        }

        Cache<FolderObject> folderCache = folderCacheRef.get();
        if (null == folderCache) {
            return;
        }

        // Put object into cache.
        folderCache.put(getCacheKeyUsing(context, folderObj.getObjectID(), folderCache), folderObj);
    }

    /**
     * Removes matching folder object from cache
     *
     * @param folderId The key
     * @param context The context
     * @throws OXException If a caching error occurs
     */
    public void removeFolderObject(int folderId, Context context) throws OXException {
        removeFolderObject(context.getContextId(), folderId, null);
    }

    /**
     * Removes a matching folder object from cache.
     * <p/>
     * If a database connection is supplied and is currently in a transaction, the cache removal will implicitly be repeated after the
     * connection is committed by registering an appropriate {@link DatabaseConnectionListener}.
     *
     * @param contextId The context identifier the folder is located in
     * @param folderId The identifier of the folder to remove from the cache
     * @param optConnection A database connection to register an additional invalidation callback for, or <code>null</code> if not applicable
     */
    public void removeFolderObject(int contextId, int folderId, Connection optConnection) throws OXException {
        Cache<FolderObject> folderCache = folderCacheRef.get();
        if (null == folderCache) {
            return;
        }

        // Remove object from cache if exist
        if (folderId > 0) {
            folderCache.invalidate(getCacheKeyUsing(contextId, folderId, folderCache));

            // Dirty hack
            InvalidationCacheService cacheService = ServerServiceRegistry.getInstance().getService(InvalidationCacheService.class);
            if (null != cacheService) {
                try {
                    cacheService.invalidateGroupMember(CoreModuleName.GLOBAL_FOLDER, asHashPart(contextId), false, FolderStorage.REAL_TREE_ID, Integer.toString(folderId));
                } catch (OXException e) {
                    LOG.warn("", e);
                }
            }
            cleanseFromLocalCache(folderId, contextId);
        }

        // Invalidate again after transaction is committed
        if (null != optConnection) {
            addAfterCommitCallback(optConnection, c -> {
                try {
                    removeFolderObject(contextId, folderId, null);
                } catch (Exception e) {
                    LOG.warn("", e);
                }
            });
        }
    }

    /**
     * Removes matching folder objects from cache
     *
     * @param folderIds The folder identifiers
     * @param context The context
     * @throws OXException If a caching error occurs
     */
    public void removeFolderObjects(int[] folderIds, Context context) throws OXException {
        removeFolderObjects(context.getContextId(), folderIds, null);
    }

    /**
     * Removes matching folder objects from cache.
     * <p/>
     * If a database connection is supplied and is currently in a transaction, the cache removal will implicitly be repeated after the
     * connection is committed by registering an appropriate {@link DatabaseConnectionListener}.
     *
     * @param contextId The context identifier the folders are located in
     * @param folderIds The identifiers of the folders to remove from the cache
     * @param optConnection A database connection to register an additional invalidation callback for, or <code>null</code> if not applicable
     */
    public void removeFolderObjects(int contextId, int[] folderIds, Connection optConnection) throws OXException {
        if (folderIds == null || folderIds.length == 0) {
            return;
        }

        Cache<FolderObject> folderCache = folderCacheRef.get();
        if (null == folderCache) {
            return;
        }

        List<CacheKey> cacheKeys = new ArrayList<>(folderIds.length);
        for (int folderId : folderIds) {
            if (folderId > 0) {
                cacheKeys.add(getCacheKeyUsing(contextId, folderId, folderCache));
            }
        }

        // Remove objects from cache
        folderCache.invalidate(cacheKeys);

        // Dirty hack
        InvalidationCacheService cacheService = ServerServiceRegistry.getInstance().getService(InvalidationCacheService.class);
        if (null != cacheService) {
            try {
                for (int key : folderIds) {
                    cacheService.invalidateGroupMember(CoreModuleName.GLOBAL_FOLDER, asHashPart(contextId), false, FolderStorage.REAL_TREE_ID, Integer.toString(key));
                }
            } catch (OXException e) {
                LOG.warn("", e);
            }
        }
        cleanseFromLocalCache(folderIds, contextId);

        // Invalidate again after transaction is committed
        if (null != optConnection) {
            addAfterCommitCallback(optConnection, (c) -> {
                try {
                    removeFolderObjects(contextId, folderIds, null);
                } catch (OXException e) {
                    LOG.warn("", e);
                }
            });
        }
    }

    private static void cleanseFromLocalCache(int folderId, int contextId) {
        int userId = Tools.getUnsignedInteger(LogProperties.get(LogProperties.Name.SESSION_USER_ID));
        if (userId > 0) {
            FolderMapManagement.getInstance().dropFor(Integer.toString(folderId), FolderStorage.REAL_TREE_ID, userId, contextId);
        }
    }

    private static void cleanseFromLocalCache(int[] folderIds, int contextId) {
        int userId = Tools.getUnsignedInteger(LogProperties.get(LogProperties.Name.SESSION_USER_ID));
        if (userId > 0) {
            List<String> fids = new ArrayList<>(folderIds.length);
            for (int folderId : folderIds) {
                fids.add(Integer.toString(folderId));
            }
            FolderMapManagement.getInstance().dropFor(fids, FolderStorage.REAL_TREE_ID, userId, contextId, null);
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Loads the folder object from underlying database storage whose id matches given parameter <code>folderId</code>.
     *
     * @param folderId The folder identifier
     * @param context The context
     * @param readCon A read-only connection (<b>optional</b>, pass <code>null</code> to fetch a new one from connection pool)
     * @return The object loaded from DB.
     * @throws OXException If folder object could not be loaded or a caching error occurs
     */
    static FolderObject loadFolderObjectInternal(int folderId, Context context, Connection readCon) throws OXException {
        if (folderId <= 0) {
            throw OXFolderExceptionCode.NOT_EXISTS.create(Integer.valueOf(folderId), Integer.valueOf(context.getContextId()));
        }
        return FolderObject.loadFolderObjectFromDB(folderId, context, readCon);
    }

    private static final class LoadFolderObjectCallable implements CacheLoader<FolderObject> {

        private final Context context;
        private final Connection readCon;
        private final int folderId;

        /**
         * Initializes a new instance of {@link LoadFolderObjectCallable}.
         *
         * @param folderId The folder identifier
         * @param context The context
         * @param readCon The read-only connection to use (<b>optional</b>, pass <code>null</code> to fetch a new one from connection pool)
         */
        private LoadFolderObjectCallable(int folderId, Context context, Connection readCon) {
            super();
            this.context = context;
            this.readCon = readCon;
            this.folderId = folderId;
        }

        @Override
        public FolderObject load(CacheKey key) throws Exception {
            return loadFolderObjectInternal(folderId, context, readCon);
        }
    }

}
