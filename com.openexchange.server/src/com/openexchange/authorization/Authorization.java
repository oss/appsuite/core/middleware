/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.authorization;

import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.osgi.RankingAwareServiceListing;

/**
 * A utility class that provides {@link #getService() static access} to the tracked instance of {@link AuthorizationService}.
 */
public final class Authorization {

    private static final AtomicReference<RankingAwareServiceListing<AuthorizationService>> SERVICE_LISTING_REF = new AtomicReference<RankingAwareServiceListing<AuthorizationService>>();

    /**
     * Default constructor.
     */
    private Authorization() {
        super();
    }

    /**
     * Gets the currently highest-ranked tracked instance of {@code AuthorizationService}.
     *
     * @return The service or <code>null</code> if no instance of <code>AuthorizationService</code> appeared yet
     */
    public static AuthorizationService getService() {
        RankingAwareServiceListing<AuthorizationService> serviceListing = SERVICE_LISTING_REF.get();
        return serviceListing == null ? null : serviceListing.getHighestRanked();
    }

    /**
     * Sets the given service listing instance.
     *
     * @param serviceListing The service listing to set
     * @return <code>true</code> if given service instance could be successfully set; otherwise <code>false</code> if a concurrent modification occurred
     */
    public static boolean setServiceListing(final RankingAwareServiceListing<AuthorizationService> serviceListing) {
        return SERVICE_LISTING_REF.compareAndSet(null, serviceListing);
    }

    /**
     * Unsets the given service listing in case that service is currently active.
     *
     * @param serviceListing The service listing to unset
     * @return <code>true</code> if service could be successfully unset; otherwise <code>false</code>
     */
    public static boolean dropServiceListing(final RankingAwareServiceListing<AuthorizationService> serviceListing) {
        return SERVICE_LISTING_REF.compareAndSet(serviceListing, null);
    }

}
