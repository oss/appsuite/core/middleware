/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.groupware.contexts.impl.caching;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.contexts.FileStorageInfo;
import com.openexchange.groupware.contexts.impl.ContextExtended;
import com.openexchange.groupware.contexts.impl.ContextImpl;
import com.openexchange.pubsub.Codecs;

/**
 * {@link ContextCodec} - The codec for {@link ContextExtended} type.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class ContextCodec extends AbstractJSONObjectCacheValueCodec<ContextExtended> {

    /*
     * JSON field names
     */
    private static final String FIELD_ATTRIBUTES = "a";
    private static final String FIELD_ENABLED = "e";
    private static final String FIELD_MAIL_ADMIN = "ma";
    private static final String FIELD_LOGIN_INFOS = "li";
    private static final String FIELD_NAME = "n";
    private static final String FIELD_QUOTA = "q";
    private static final String FIELD_AUTH_PW = "apw";
    private static final String FIELD_AUTH_USER = "au";
    private static final String FIELD_CONTEXT = "c";
    private static final String FIELD_FILESTORE_INFO = "fs";
    private static final String FIELD_ID = "id";
    private static final String FIELD_UPDATING = "up";
    private static final String FIELD_READ_ONLY = "ro";
    private static final String FIELD_UPDATE_NEEDED = "un";

    private static final UUID ID = UUID.fromString("3d139077-40e6-4f22-9377-be8077496d8f");

    @Override
    public UUID getCodecId() {
        return ID;
    }

    @Override
    protected JSONObject writeJson(ContextExtended value) throws Exception {
        JSONObject result = new JSONObject();
        // FileStorageInfo interface fields
        result.put(FIELD_FILESTORE_INFO, convertFileStorageInfo(value));
        // Context interface fields
        result.put(FIELD_CONTEXT, convertContext(value));
        return result;
    }

    @Override
    protected ContextExtended parseJson(JSONObject jObject) throws Exception {
        JSONObject core = jObject.getJSONObject(FIELD_CONTEXT);
        JSONObject fs = jObject.getJSONObject(FIELD_FILESTORE_INFO);
        ContextImpl result = new ContextImpl(core.getInt(FIELD_ID));
        // Apply FileStorageInfo fields
        applyFileStorageInfo(fs, result);
        // Apply Context fields
        applyContext(core, result);

        return result;
    }

    // ------------------------------ context 2 JSON converters  -----------------------

    /**
     * Converts the {@link FileStorageInfo} to a {@link JSONObject}
     *
     * @param info The info to convert
     * @return The converted info
     * @throws JSONException
     */
    private JSONObject convertFileStorageInfo(FileStorageInfo info) throws JSONException {
        JSONObject result = new JSONObject(6);

        String[] auth = info.getFileStorageAuth();
        if (auth != null && auth.length == 2) {
            result.putOpt(FIELD_AUTH_USER, auth[0]);
            result.putOpt(FIELD_AUTH_PW, auth[1]);
        }

        result.put(FIELD_QUOTA, info.getFileStorageQuota());
        result.put(FIELD_ID, info.getFilestoreId());
        result.put(FIELD_NAME, info.getFilestoreName());
        return result;
    }

    /**
     * Converts the given {@link Context} to a {@link JSONObject}
     *
     * @param context The context to convert
     * @return The converted context
     * @throws JSONException
     */
    private JSONObject convertContext(ContextExtended context) throws JSONException {
        JSONObject result = new JSONObject(10);
        result.put(FIELD_ID, context.getContextId());
        result.put(FIELD_NAME, context.getName());
        result.putOpt(FIELD_LOGIN_INFOS, Codecs.stringArrayToJsonArray(context.getLoginInfo()));
        result.put(FIELD_MAIL_ADMIN, context.getMailadmin());
        if (!context.isEnabled()) {
            // Only set if NOT enabled; otherwise assume as enabled on deserialization
            result.put(FIELD_ENABLED, Boolean.FALSE);
        }
        Map<String, List<String>> attributes = context.getAttributes();
        if (attributes != null) {
            JSONObject map = new JSONObject();
            for (Map.Entry<String, List<String>> att : attributes.entrySet()) {
                map.put(att.getKey(), Codecs.stringCollectionToJsonArray(att.getValue()));
            }
            result.put(FIELD_ATTRIBUTES, map);
        }
        if (context.isUpdating()) {
            result.put(FIELD_UPDATING, true);
        }
        if (context.isReadOnly()) {
            result.put(FIELD_READ_ONLY, true);
        }
        if (context.isUpdateNeeded()) {
            result.put(FIELD_UPDATE_NEEDED, true);
        }
        return result;
    }

    // ----------------------------- convert JSON 2 context --------------------------

    /**
     * Applies the {@link FileStorageInfo} part to the given {@link ContextImpl}
     *
     * @param fs The JSON containing the {@link FileStorageInfo} data
     * @param target The {@link ContextImpl} to apply it to
     * @throws JSONException
     */
    private void applyFileStorageInfo(JSONObject fs, ContextImpl target) throws JSONException {
        String[] fsAuth = new String[] { fs.optString(FIELD_AUTH_USER, null), fs.optString(FIELD_AUTH_PW, null) };
        target.setFilestoreAuth(fsAuth);
        target.setFilestoreId(fs.getInt(FIELD_ID));
        target.setFilestoreName(fs.getString(FIELD_NAME));
        target.setFileStorageQuota(fs.getLong(FIELD_QUOTA));
    }

    /**
     * Applies the {@link FileStorageInfo} part to the given {@link ContextImpl}
     *
     * @param core The JSON containing the {@link Context} data
     * @param target The context to apply it to
     * @throws JSONException
     */
    private void applyContext(JSONObject core, ContextImpl target) throws JSONException {
        target.setName(core.getString(FIELD_NAME));
        target.setLoginInfo(Codecs.jsonArrayToStringArray(core.optJSONArray(FIELD_LOGIN_INFOS)));
        target.setMailadmin(core.getInt(FIELD_MAIL_ADMIN));
        target.setEnabled(core.optBoolean(FIELD_ENABLED, true));
        applyAttributes(core.optJSONObject(FIELD_ATTRIBUTES), target);
        target.setUpdating(core.optBoolean(FIELD_UPDATING, false));
        target.setReadOnly(core.optBoolean(FIELD_READ_ONLY, false));
        target.setUpdateNeeded(core.optBoolean(FIELD_UPDATE_NEEDED, false));
    }

    /**
     * Applies the attributes to the given {@link ContextImpl}
     *
     * @param attributes The JSON containing the attributes
     * @param target The context} to apply it to
     * @throws JSONException
     */
    private void applyAttributes(JSONObject attributes, ContextImpl target) throws JSONException {
        if (attributes != null) {
            for (Map.Entry<String, Object> e : attributes.entrySet()) {
                for (Object o : (JSONArray) e.getValue()) {
                    target.addAttribute(e.getKey(), o.toString());
                }
            }
        }
    }

}
