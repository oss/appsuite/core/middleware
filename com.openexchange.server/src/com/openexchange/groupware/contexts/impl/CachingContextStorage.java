/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.contexts.impl;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.java.Autoboxing.I;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import com.google.common.cache.CacheBuilder;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.SignalingCacheLoader;
import com.openexchange.cache.v2.codec.CacheValueCodec;
import com.openexchange.cache.v2.codec.IntegerCodec;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;
import com.openexchange.config.ConfigurationService;
import com.openexchange.context.PoolAndSchema;
import com.openexchange.databaseold.Database;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.contexts.UpdateBehavior;
import com.openexchange.groupware.contexts.impl.caching.ContextCodec;
import com.openexchange.groupware.update.UpdateProperty;
import com.openexchange.groupware.update.UpdateStatus;
import com.openexchange.groupware.update.Updater;
import com.openexchange.groupware.update.internal.SchemaExceptionCodes;
import com.openexchange.java.BoolReference;
import com.openexchange.java.CollectorUtils;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.tools.CacheGetter;

/**
 * This class implements a caching for the context storage. It provides a proxy implementation for the Context interface to the outside
 * world to be able to keep the referenced context data up-to-date.
 *
 * @author <a href="mailto:marcus@open-xchange.org">Marcus Klein</a>
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class CachingContextStorage extends ContextStorage {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CachingContextStorage.class);

    /**
     * Initializes a new {@link CacheGetter} to retrieve a specific context from cache.
     *
     * @param cacheService A reference to the cache service
     * @param contextId The identifier of the context to get the getter for
     * @return The cache getter
     */
    public static CacheGetter<Context> cacheGetterFor(CacheService cacheService, int contextId) {
        Cache<ContextExtended> cache = cacheService.getCache(CONTEXT_CACHE_OPTIONS);
        return new CacheGetter<Context>() {

            @Override
            public CacheKey getKey() {
                return createKey(cache, contextId);
            }

            @Override
            public CacheValueCodec<?> getCodec() {
                return CONTEXT_CACHE_OPTIONS.getCodec();
            }

            @Override
            public Context optValue(CacheKeyValue<?> cacheValue) throws OXException {
                if (cacheValue != null && cacheValue.getValueOrElse(null) instanceof ContextExtended contextExtended &&
                    false == CachingContextStorage.checkPendingUpdateTasks(contextExtended, cacheValue.getKey(), cache)) {
                    return contextExtended;
                }
                return null;
            }
        };
    }

    private static final CacheOptions<ContextExtended> CONTEXT_CACHE_OPTIONS = CacheOptions.<ContextExtended> builder()
                                                                                           .withCoreModuleName(CoreModuleName.CONTEXT)
                                                                                           .withCodecAndVersion(new ContextCodec())
                                                                                           .withFireCacheEvents(true) // Fire events for ContextCacheListener
                                                                                           .build();

    private static final CacheOptions<Integer> CONTEXT_ID_CACHE_OPTIONS = CacheOptions.<Integer> builder()
                                                                                      .withCoreModuleName(CoreModuleName.CONTEXT_ID)
                                                                                      .withCodecAndVersion(IntegerCodec.getInstance())
                                                                                      .build();

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final RdbContextStorage persistentImpl;
    private boolean started;

    /**
     * Initializes a new instance of {@link CachingContextStorage}.
     *
     * @param persistentImpl The implementation for persistent storage
     */
    public CachingContextStorage(final RdbContextStorage persistentImpl) {
        super();
        this.persistentImpl = persistentImpl;
    }

    @Override
    public int getContextId(final String loginInfo) throws OXException {
        Cache<Integer> cache = getContextIdCache();
        CacheKey key = createKey(cache, loginInfo);
        Integer contextId = cache.get(key);
        if (null != contextId) {
            LOG.trace("Cache HIT. Login info: {}", loginInfo);
            return contextId.intValue();
        }
        LOG.trace("Cache MISS. Login info: {}", loginInfo);
        ContextExtended context = persistentImpl.getContext(loginInfo);
        if (null == context) {
            contextId = I(NOT_FOUND);
            return contextId.intValue();
        }
        triggerUpdate(context, UpdateBehavior.NONE);
        contextId = I(context.getContextId());
        try {
            cache.put(key, contextId);
            Cache<ContextExtended> ctxCache = getContextCache();
            CacheKey ctxKey = createKey(ctxCache, contextId.intValue());
            ctxCache.put(ctxKey, context);
        } catch (OXException e) {
            LOG.error("", e);
        }
        return contextId.intValue();
    }

    @Override
    public ContextExtended loadContext(int contextId, UpdateBehavior updateBehavior) throws OXException {
        ContextExtended context;
        do {
            context = loadElseNullOnReload(contextId, updateBehavior);
        } while (context == null);
        return context;
    }

    /**
     * Loads specified context and returns <code>null</code> if the context should be reloaded.
     *
     * @param contextId The context identifier
     * @param updateBehavior The update behavior
     * @return The context or <code>null</code>
     * @throws OXException If loading context fails
     */
    private ContextExtended loadElseNullOnReload(int contextId, UpdateBehavior updateBehavior) throws OXException {
        Cache<ContextExtended> cache = getContextCache();
        CacheKey key = createKey(cache, contextId);
        ContextExtended context = cache.get(key);
        if (context != null) {
            return checkPendingUpdateTasks(context, key, cache) ? null : context;
        }

        ContextLoader loader = new ContextLoader(contextId, persistentImpl);
        context = cache.get(key, loader);
        if (loader.isValuePutToCache()) {
            // Successfully populated context cache from database with current thread
            triggerUpdate(context, updateBehavior);
            return context;
        }

        return checkPendingUpdateTasks(context, key, cache) ? null : context;
    }

    @Override
    public boolean exists(int contextId) throws OXException {
        Cache<ContextExtended> cache = getContextCache();
        CacheKey key = createKey(cache, contextId);
        ContextExtended ctx = cache.get(key);
        if (ctx != null) {
            return true;
        }
        return persistentImpl.exists(contextId);
    }

    @Override
    public List<Integer> getAllContextIds() throws OXException {
        return persistentImpl.getAllContextIds();
    }

    @Override
    public List<Integer> getDistinctContextsPerSchema() throws OXException {
        return persistentImpl.getDistinctContextsPerSchema();
    }

    @Override
    public Map<PoolAndSchema, List<Integer>> getSchemaAssociations() throws OXException {
        return persistentImpl.getSchemaAssociations();
    }

    @Override
    public Map<PoolAndSchema, List<Integer>> getSchemaAssociationsFor(List<Integer> contextIds) throws OXException {
        return persistentImpl.getSchemaAssociationsFor(contextIds);
    }

    @Override
    public List<Integer> getAllContextIdsForFilestore(int filestoreId) throws OXException {
        return persistentImpl.getAllContextIdsForFilestore(filestoreId);
    }

    @Override
    protected void startUp() throws OXException {
        if (started) {
            LOG.error("Duplicate initialization of CachingContextStorage.");
            return;
        }
        persistentImpl.startUp();
        started = true;
    }

    @Override
    public void setAttribute(String name, String value, int contextId) throws OXException {
        persistentImpl.setAttribute(name, value, contextId);
        invalidateContexts(new int[] { contextId });
    }

    @Override
    protected void shutDown() throws OXException {
        if (!started) {
            LOG.error("Duplicate shutdown of CachingContextStorage.");
            return;
        }
        persistentImpl.shutDown();
        started = false;
    }

    @Override
    public void invalidateContext(final int contextId) throws OXException {
        invalidateContexts(new int[] { contextId });
    }

    @Override
    public void invalidateAll() throws OXException {
        List<CacheFilter> filters = List.of(
            CacheFilter.builder().withCoreModuleName(CoreModuleName.CONTEXT).addSuffixAsWildcard("").build(),
            CacheFilter.builder().withCoreModuleName(CoreModuleName.CONTEXT_ID).addSuffixAsWildcard("").build()
        );
        ServerServiceRegistry.getServize(InvalidationCacheService.class, true).invalidate(filters, true);
    }

    @Override
    public void invalidateContexts(int[] contextIds) throws OXException {
        if (contextIds == null) {
            return;
        }

        int length = contextIds.length;
        if (length == 0) {
            return;
        }

        Cache<ContextExtended> cache = getContextCache();
        Cache<Integer> idCache = getContextIdCache();

        // Gather cache keys to invalidate
        List<CacheKey> keysToRetrieve = Arrays.stream(contextIds).mapToObj(contextId -> createKey(cache, contextId)).collect(CollectorUtils.toList(length));
        List<CacheKey> keysToInvalidate = new ArrayList<CacheKey>(length);
        List<CacheKey> loginInfoKeysToInvalidate = null;
        for (CacheKeyValue<ContextExtended> cacheKeyValue : cache.mget(keysToRetrieve)) {
            keysToInvalidate.add(cacheKeyValue.getKey());
            Context context = cacheKeyValue.getValueOrElse(null);
            if (context != null) {
                // Such a context exists in cache, invalidate cached login info as well
                loginInfoKeysToInvalidate = addLoginInfoKeys(context, loginInfoKeysToInvalidate, idCache, length);
            }
        }

        // Invalidate caches
        cache.invalidate(keysToInvalidate);
        if (loginInfoKeysToInvalidate != null) {
            idCache.invalidate(loginInfoKeysToInvalidate);
        }
    }

    /**
     * Adds cache keys for given context's login information to list of cache keys to invalidate.
     *
     * @param context The context providing login information
     * @param loginInfoKeysToInvalidate The list of cache keys to invalidate
     * @param idCache The cache for login information
     * @param numContexts The number of contexts to invalidate
     * @return The list of cache keys to invalidate
     */
    private static List<CacheKey> addLoginInfoKeys(Context context, List<CacheKey> loginInfoKeysToInvalidate, Cache<Integer> idCache, int numContexts) {
        String[] loginInfos = context.getLoginInfo();
        if (null == loginInfos || loginInfos.length <= 0) {
            // No login information associated with given context
            return loginInfoKeysToInvalidate;
        }

        List<CacheKey> loginInfoKeys = loginInfoKeysToInvalidate;
        for (String loginInfo : loginInfos) {
            if (loginInfo != null) {
                loginInfoKeys = addElseCreate(createKey(idCache, loginInfo), loginInfoKeys, numContexts);
            }
        }
        return loginInfoKeys;
    }

    /**
     * Adds given element to either specified list or to a newly created list.
     *
     * @param <E> The type of the element
     * @param element The element to add
     * @param list The existent list or <code>null</code>
     * @param initialCapacity The initial capacity for the list in case it is newly created
     * @return Either specified list or to a newly created list
     */
    private static <E> List<E> addElseCreate(E element, List<E> list, int initialCapacity) {
        List<E> l = list;
        if (l == null) {
            l = new ArrayList<>(initialCapacity);
        }
        l.add(element);
        return l;
    }

    @Override
    public void invalidateLoginInfo(final String loginContextInfo) throws OXException {
        Cache<Integer> cache = getContextIdCache();
        CacheKey key = createKey(cache, loginContextInfo);
        cache.invalidate(key);
    }

    /**
     * Gets the underlying implementation for the persistent storage.
     *
     * @return The implementation for the persistent storage
     */
    ContextStorage getPersistentImpl() {
        return persistentImpl;
    }

    /** Volatile cache to remember context to database schema mappings */
    private static final com.google.common.cache.Cache<Integer, String> CONTEXT_TO_SCHMEMA = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.MINUTES).build();

    /**
     * Gets the name of the database schema to which given context has been assigned.
     *
     * @param contextId The context identifier
     * @return The name of the database schema associated with the context
     * @throws ExecutionException If database schema cannot be determined
     */
    private static String getSchemaNameFor(int contextId) throws ExecutionException {
        String schemaName = CONTEXT_TO_SCHMEMA.getIfPresent(I(contextId));
        return schemaName == null ? CONTEXT_TO_SCHMEMA.get(I(contextId), () -> Database.getSchema(contextId)) : schemaName;
    }

    /** Long-living cache for schema update information */
    private static final com.google.common.cache.Cache<String, UpdateInfoSupplier> SCHEMA_UPDATE_INFO = CacheBuilder.newBuilder().maximumSize(1 << 20).expireAfterWrite(1, TimeUnit.DAYS).build();

    /**
     * Checks for possible pending update tasks just once for context-associated database schema. Signals to reload the context if there are
     * pending update tasks; otherwise the loaded context may be used as-is.
     *
     * @param context The context fetched from cache
     * @param updateBehavior The demanded update behavior
     * @param key The cache key for the context
     * @param cache The context cache in which to possibly invalidate the context entry
     * @return <code>true</code> if context needs to be re-loaded; otherwise <code>false</code> if it can be used as-is
     * @throws OXException If check for pending update tasks fails
     */
    private static boolean checkPendingUpdateTasks(ContextExtended context, CacheKey key, Cache<ContextExtended> cache) throws OXException {
        try {
            // Get name of the database schema associated with given context
            int contextId = context.getContextId();
            String schemaName = getSchemaNameFor(contextId);

            // Test if update status needs to checked for context-associated database schema
            UpdateInfoSupplier supplier = SCHEMA_UPDATE_INFO.getIfPresent(schemaName);
            if (supplier == null) {
                // Not yet tested. Create UpdateInfoSupplier if absent.
                BoolReference loaded = new BoolReference(false);
                supplier = SCHEMA_UPDATE_INFO.get(schemaName, () -> createFutureUpdateInfoSupplier(loaded));

                // Test if current thread created the UpdateInfoSupplier instance
                if (loaded.getValue()) {
                    // Current thread is the loader and thus is used to check update status just once for context-associated schema
                    UpdateStatus status = Updater.getInstance().getStatus(contextId);
                    UpdateInfo info = new UpdateInfo(status.blockingUpdatesRunning(), status.updateNeeded());
                    boolean updatingOrNeedsUpdate = info.isUpdatingOrNeedsUpdate();
                    if (updatingOrNeedsUpdate) {
                        // Invalidate context-associated cache entry to have it being updated with new "updating" and "needs update" flags
                        cache.invalidate(key);

                        // Drop UpdateInfoSupplier instance to ensure newest status is re-read
                        SCHEMA_UPDATE_INFO.invalidate(schemaName);
                    } else {
                        // Replace with InstanceUpdateInfoSupplier to avoid steadily reading from Future w/ concurrency overhead
                        SCHEMA_UPDATE_INFO.put(schemaName, new InstanceUpdateInfoSupplier(info));
                    }

                    // Set future's value
                    ((FutureUpdateInfoSupplier) supplier).getFuture().set(info);
                    applyUpdateInfo(info, context);
                    return updatingOrNeedsUpdate;
                }
            }

            // Current thread awaits update status check
            applyUpdateInfo(supplier.get(), context);
            return false;
        } catch (OXException e) {
            throw e;
        } catch (InterruptedException e) {
            // Keep interrupted flag
            Thread.currentThread().interrupt();
            throw OXException.general("Interrupted while awaiting update status", e);
        } catch (ExecutionException | UncheckedExecutionException e) {
            Throwable cause = e.getCause();
            throw OXException.general("Error while awaiting update status", cause == null ? e : cause);
        } catch (Exception e) {
            throw OXException.general("Error while awaiting update status", e);
        }
    }

    /**
     * Applies update information to given context instance.
     *
     * @param updateInfo The update information
     * @param context The context to apply to
     */
    private static void applyUpdateInfo(UpdateInfo updateInfo, ContextExtended context) {
        if (updateInfo.isUpdating()) {
            context.setUpdating(true);
        }
        if (updateInfo.isUpdateNeeded()) {
            context.setUpdateNeeded(true);
        }
    }

    /**
     * Create an instance of <code>FutureUpdateInfoSupplier</code>.
     *
     * @param loaded The flag to set
     * @return The created instance of <code>FutureUpdateInfoSupplier</code>
     */
    private static FutureUpdateInfoSupplier createFutureUpdateInfoSupplier(BoolReference loaded) {
        loaded.setValue(true);
        return new FutureUpdateInfoSupplier();
    }

    /**
     * Checks update status and possibly triggers context-associated database schema update.
     *
     * @param context The context to be decorated with update information
     * @param updateBehavior The demanded update behavior
     * @return The passed context
     */
    private static ContextExtended triggerUpdate(ContextExtended context, UpdateBehavior updateBehavior) {
        // We should introduce a logic layer above this context storage
        // layer. That layer should then trigger the update tasks.
        // Nearly all accesses to the ContextStorage need then to be replaced
        // with an access to the ContextService.
        try {
            Updater updater = Updater.getInstance();
            UpdateStatus status = updater.getStatus(context.getContextId());
            context.setUpdating(status.blockingUpdatesRunning());
            if (status.updateNeeded()) {
                if (denyImplicitUpdateOnContextLoad(updateBehavior)) {
                    context.setUpdateNeeded(true);
                } else {
                    if (status.needsBlockingUpdates()) {
                        context.setUpdating(true);
                    }
                    updater.startUpdate(context);
                }
            }
        } catch (OXException e) {
            if (SchemaExceptionCodes.DATABASE_DOWN.equals(e)) {
                LOG.warn("Switching to read only mode for context {} because master database is down.", I(context.getContextId()), e);
                context.setReadOnly(true);
            }
        }
        return context;
    }

    private static boolean denyImplicitUpdateOnContextLoad(UpdateBehavior updateBehavior) {
        UpdateBehavior behavior = updateBehavior;
        if (behavior == null) {
            behavior = UpdateBehavior.NONE;
        }

        return switch (behavior) {
            case DENY_UPDATE -> true;
            case TRIGGER_UPDATE -> false;
            case NONE -> isDenyImplicitUpdateOnContextLoadProperty();
            default -> isDenyImplicitUpdateOnContextLoadProperty();
        };
    }

    private static boolean isDenyImplicitUpdateOnContextLoadProperty() {
        boolean defaultValue = UpdateProperty.DENY_IMPLICIT_UPDATE_ON_CONTEXT_LOAD.getDefaultValue(Boolean.class).booleanValue();

        ConfigurationService configService = ServerServiceRegistry.getInstance().getService(ConfigurationService.class);
        if (null == configService) {
            return defaultValue;
        }

        return configService.getBoolProperty(UpdateProperty.DENY_IMPLICIT_UPDATE_ON_CONTEXT_LOAD.getFQPropertyName(), defaultValue);
    }

    /**
     * Gets the context cache.
     *
     * @return The {@link Cache}
     * @throws OXException If the {@link CacheService} is missing
     */
    private static Cache<ContextExtended> getContextCache() throws OXException {
        return ServerServiceRegistry.getInstance().getService(CacheService.class, true).getCache(CONTEXT_CACHE_OPTIONS);
    }

    /**
     * Gets the context identifier cache.
     *
     * @return The {@link Cache}
     * @throws OXException If the {@link CacheService} is missing
     */
    private static Cache<Integer> getContextIdCache() throws OXException {
        return ServerServiceRegistry.getInstance().getService(CacheService.class, true).getCache(CONTEXT_ID_CACHE_OPTIONS);
    }

    /**
     * Creates a new cache key for given login information.
     *
     * @param cache The cache to create the key for
     * @param loginInfo The login information
     * @return The key
     */
    private static CacheKey createKey(Cache<Integer> cache, String loginInfo) {
        return cache.newKey(loginInfo);
    }

    /**
     * Creates a new cache key for given context identifier.
     *
     * @param cache The cache to create the key for
     * @param contextId The context identifier
     * @return The key
     */
    private static CacheKey createKey(Cache<ContextExtended> cache, int contextId) {
        return cache.newKey(asHashPart(contextId));
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** Responsible for loading a context with respect to possibly triggering pending update tasks */
    private static final class ContextLoader extends SignalingCacheLoader<ContextExtended> {

        private final int contextId;
        private final RdbContextStorage persistentImpl;

        /**
         * Initializes a new instance of {@link ContextLoader}.
         *
         * @param contextId unique identifier of the context to load.
         * @param persistentImpl The persistent storage implementation
         */
        ContextLoader(int contextId, RdbContextStorage persistentImpl) {
            super();
            this.contextId = contextId;
            this.persistentImpl = persistentImpl;
        }

        @Override
        public ContextExtended load(CacheKey key) throws Exception {
            return persistentImpl.loadContext(contextId);
        }
    }

    private static interface UpdateInfoSupplier {

        /**
         * Gets the update information.
         *
         * @return The update information
         * @throws ExecutionException If the computation threw an exception
         * @throws InterruptedException If the current thread was interrupted while waiting
         */
        UpdateInfo get() throws InterruptedException, ExecutionException;
    }

    private static class InstanceUpdateInfoSupplier implements UpdateInfoSupplier {

        private final UpdateInfo updateInfo;

        /**
         * Initializes a new instance of {@link InstanceUpdateInfoSupplier}.
         *
         * @param updateInfo The update information
         */
        InstanceUpdateInfoSupplier(UpdateInfo updateInfo) {
            super();
            this.updateInfo = updateInfo;
        }

        @Override
        public UpdateInfo get() throws InterruptedException, ExecutionException {
            return updateInfo;
        }
    }

    private static class FutureUpdateInfoSupplier implements UpdateInfoSupplier {

        private final SettableFutureTask<UpdateInfo> future;

        /**
         * Initializes a new instance of {@link InstanceUpdateInfoSupplier}.
         */
        FutureUpdateInfoSupplier() {
            super();
            this.future = new SettableFutureTask<>();
        }

        /**
         * Gets the future whose result can be set.
         *
         * @return The future
         */
        SettableFutureTask<UpdateInfo> getFuture() {
            return future;
        }

        @Override
        public UpdateInfo get() throws InterruptedException, ExecutionException {
            return future.get();
        }
    }

    /** Helper class to remember if either an update process is currently running or if an update is needed */
    private static class UpdateInfo {

        private final boolean updating;
        private final boolean updateNeeded;

        /**
         * Initializes a new instance of {@link UpdateInfo}.
         *
         * @param updating <code>true</code> if an update process is currently running; otherwise <code>false</code>
         * @param updateNeeded <code>true</code> if an update is needed; otherwise <code>false</code>
         */
        UpdateInfo(boolean updating, boolean updateNeeded) {
            super();
            this.updating = updating;
            this.updateNeeded = updateNeeded;
        }

        /**
         * Checks if an update is needed.
         *
         * @return <code>true</code> if an update is needed; otherwise <code>false</code>
         */
        boolean isUpdateNeeded() {
            return updateNeeded;
        }

        /**
         * Checks if an update process is currently running.
         *
         * @return <code>true</code> if an update process is currently running; otherwise <code>false</code>
         */
        boolean isUpdating() {
            return updating;
        }

        /**
         * Checks for updating or update needed status.
         *
         * @return <code>true</code> for updating or update needed status; otherwise <code>false</code> if neither nor
         */
        boolean isUpdatingOrNeedsUpdate() {
            return isUpdating() || isUpdateNeeded();
        }
    }

    /** A simple extension of FutureTask allowing to set the result */
    private static final class SettableFutureTask<V> extends FutureTask<V> {

        /**
         * Initializes a new instance of {@link SettableFutureTask}.
         */
        @SuppressWarnings("unchecked")
        SettableFutureTask() {
            super(CALLABLE_YIELD_NULL);
        }

        @Override
        public void set(V v) {
            super.set(v);
        }

    }

    @SuppressWarnings("rawtypes")
    private static final Callable CALLABLE_YIELD_NULL = new NullYieldingCallable<>();

    private static final class NullYieldingCallable<V> implements Callable<V> {

        @Override
        public V call() throws Exception {
            return null;
        }
    }

}
