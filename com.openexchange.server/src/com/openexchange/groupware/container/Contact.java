/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.container;

import static com.openexchange.java.Autoboxing.B;
import static com.openexchange.java.Autoboxing.I;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contact.helpers.ContactField;
import com.openexchange.java.Strings;

/**
 * {@link Contact} - Represents a contact.
 *
 * @author <a href="mailto:sebastian.kauss@open-xchange.com">Sebastian Kauss</a>
 * @author <a href="mailto:ben.pahne@open-xchange.com">Benjamin Frederic Pahne</a>
 * @author <a href="mailto:tobias.prinz@open-xchange.com">Tobias Prinz</a> - clone, hashCode, equals, toString, display name checking
 */
public class Contact extends CommonObject {

    private static final long serialVersionUID = -1556083622176209459L;

    public static final int DISPLAY_NAME = 500;

    public static final int GIVEN_NAME = 501;

    public static final int SUR_NAME = 502;

    public static final int MIDDLE_NAME = 503;

    public static final int SUFFIX = 504;

    public static final int TITLE = 505;

    public static final int STREET_HOME = 506;

    public static final int POSTAL_CODE_HOME = 507;

    public static final int CITY_HOME = 508;

    public static final int STATE_HOME = 509;

    public static final int COUNTRY_HOME = 510;

    public static final int BIRTHDAY = 511;

    public static final int MARITAL_STATUS = 512;

    public static final int NUMBER_OF_CHILDREN = 513;

    public static final int PROFESSION = 514;

    public static final int NICKNAME = 515;

    public static final int SPOUSE_NAME = 516;

    public static final int ANNIVERSARY = 517;

    public static final int NOTE = 518;

    public static final int DEPARTMENT = 519;

    public static final int POSITION = 520;

    public static final int EMPLOYEE_TYPE = 521;

    public static final int ROOM_NUMBER = 522;

    public static final int STREET_BUSINESS = 523;

    public static final int INTERNAL_USERID = 524;

    public static final int POSTAL_CODE_BUSINESS = 525;

    public static final int CITY_BUSINESS = 526;

    public static final int STATE_BUSINESS = 527;

    public static final int COUNTRY_BUSINESS = 528;

    public static final int NUMBER_OF_EMPLOYEE = 529;

    public static final int SALES_VOLUME = 530;

    public static final int TAX_ID = 531;

    public static final int COMMERCIAL_REGISTER = 532;

    public static final int BRANCHES = 533;

    public static final int BUSINESS_CATEGORY = 534;

    public static final int INFO = 535;

    public static final int MANAGER_NAME = 536;

    public static final int ASSISTANT_NAME = 537;

    public static final int STREET_OTHER = 538;

    public static final int CITY_OTHER = 539;

    public static final int POSTAL_CODE_OTHER = 540;

    public static final int COUNTRY_OTHER = 541;

    public static final int TELEPHONE_BUSINESS1 = 542;

    public static final int TELEPHONE_BUSINESS2 = 543;

    public static final int FAX_BUSINESS = 544;

    public static final int TELEPHONE_CALLBACK = 545;

    public static final int TELEPHONE_CAR = 546;

    public static final int TELEPHONE_COMPANY = 547;

    public static final int TELEPHONE_HOME1 = 548;

    public static final int TELEPHONE_HOME2 = 549;

    public static final int FAX_HOME = 550;

    public static final int CELLULAR_TELEPHONE1 = 551;

    public static final int CELLULAR_TELEPHONE2 = 552;

    public static final int TELEPHONE_OTHER = 553;

    public static final int FAX_OTHER = 554;

    /**
     * Business email address.
     */
    public static final int EMAIL1 = 555;

    /**
     * Private email address.
     */
    public static final int EMAIL2 = 556;

    public static final int EMAIL3 = 557;

    public static final int URL = 558;

    public static final int TELEPHONE_ISDN = 559;

    public static final int TELEPHONE_PAGER = 560;

    public static final int TELEPHONE_PRIMARY = 561;

    public static final int TELEPHONE_RADIO = 562;

    public static final int TELEPHONE_TELEX = 563;

    public static final int TELEPHONE_TTYTDD = 564;

    public static final int INSTANT_MESSENGER1 = 565;

    public static final int INSTANT_MESSENGER2 = 566;

    public static final int TELEPHONE_IP = 567;

    public static final int TELEPHONE_ASSISTANT = 568;

    public static final int COMPANY = 569;

    public static final int IMAGE1 = 570;

    public static final int USERFIELD01 = 571;

    public static final int USERFIELD02 = 572;

    public static final int USERFIELD03 = 573;

    public static final int USERFIELD04 = 574;

    public static final int USERFIELD05 = 575;

    public static final int USERFIELD06 = 576;

    public static final int USERFIELD07 = 577;

    public static final int USERFIELD08 = 578;

    public static final int USERFIELD09 = 579;

    public static final int USERFIELD10 = 580;

    public static final int USERFIELD11 = 581;

    public static final int USERFIELD12 = 582;

    public static final int USERFIELD13 = 583;

    public static final int USERFIELD14 = 584;

    public static final int USERFIELD15 = 585;

    public static final int USERFIELD16 = 586;

    public static final int USERFIELD17 = 587;

    public static final int USERFIELD18 = 588;

    public static final int USERFIELD19 = 589;

    public static final int USERFIELD20 = 590;

    public static final int DISTRIBUTIONLIST = 592;

    public static final int CONTEXTID = 593;

    public static final int NUMBER_OF_DISTRIBUTIONLIST = 594;

    public static final int NUMBER_OF_IMAGES = 596;

    public static final int IMAGE_LAST_MODIFIED = 597;

    public static final int STATE_OTHER = 598;

    public static final int FILE_AS = 599;

    public static final int IMAGE1_CONTENT_TYPE = 601;

    public static final int MARK_AS_DISTRIBUTIONLIST = 602;

    public static final int DEFAULT_ADDRESS = 605;

    public static final int IMAGE1_URL = 606;

    /**
     * This attribute identifier has only a sorting purpose. This does not represent a contact attribute. This identifier can be specified
     * only for the sorting column. The sorting is then done the following way: Use one of {@link #SUR_NAME}, {@link #DISPLAY_NAME},
     * {@link #COMPANY}, {@link #EMAIL1} or {@link #EMAIL2} in this order whichever is first not null and not empty. Use the selected
     * value for sorting with the AlphanumComparator.
     */
    public static final int SPECIAL_SORTING = 607;

    public static final int USE_COUNT = 608;

    /**
     * Additional fields for kana based search in japanese environments.
     */
    public static final int YOMI_FIRST_NAME = 616;
    public static final int YOMI_LAST_NAME = 617;
    public static final int YOMI_COMPANY = 618;

    /**
     * Additional fields for Outlook address fields.
     */
    public static final int ADDRESS_HOME = 619;
    public static final int ADDRESS_BUSINESS = 620;
    public static final int ADDRESS_OTHER = 621;

    public static final int VCARD_ID = 622;

    /**
     * This attribute identifier has only a sorting purpose. This does not represent a contact attribute. This identifier can be specified
     * only for the sorting column. The sorting is then done the following way: Use one of {@link #GIVEN_NAME}, {@link #SUR_NAME},
     * {@link #COMPANY}, {@link #EMAIL1} or {@link #EMAIL2} in this order whichever is first not null and not empty. Use the selected
     * value for sorting with the AlphanumComparator.
     */
    public static final int SPECIAL_SORTING_FIRST_NAME = 623;

    /**
     * Sorted array of fields belonging to business address.
     */
    public static final int[] ADDRESS_FIELDS_BUSINESS;
    /**
     * Sorted array of fields belonging to home address.
     */
    public static final int[] ADDRESS_FIELDS_HOME;
    /**
     * Sorted array of fields belonging to other address.
     */
    public static final int[] ADDRESS_FIELDS_OTHER;

    static {
        int[] ia = new int[] { Contact.CITY_BUSINESS, Contact.COUNTRY_BUSINESS, Contact.POSTAL_CODE_BUSINESS, Contact.STATE_BUSINESS, Contact.STREET_BUSINESS };
        Arrays.sort(ia);
        ADDRESS_FIELDS_BUSINESS = ia;

        ia = new int[] { Contact.CITY_HOME, Contact.COUNTRY_HOME, Contact.POSTAL_CODE_HOME, Contact.STATE_HOME, Contact.STREET_HOME };
        Arrays.sort(ia);
        ADDRESS_FIELDS_HOME = ia;

        ia = new int[] { Contact.CITY_OTHER, Contact.COUNTRY_OTHER, Contact.POSTAL_CODE_OTHER, Contact.STATE_OTHER, Contact.STREET_OTHER };
        Arrays.sort(ia);
        ADDRESS_FIELDS_OTHER = ia;
    }

    // @formatter:off
    public static final int[] CONTENT_COLUMNS = {   DISPLAY_NAME, GIVEN_NAME, SUR_NAME, MIDDLE_NAME, SUFFIX, TITLE, STREET_HOME, POSTAL_CODE_HOME, CITY_HOME, STATE_HOME, COUNTRY_HOME,
                                                    BIRTHDAY, MARITAL_STATUS, NUMBER_OF_CHILDREN, PROFESSION, NICKNAME, SPOUSE_NAME, ANNIVERSARY, NOTE, DEPARTMENT, POSITION, EMPLOYEE_TYPE,
                                                    ROOM_NUMBER, STREET_BUSINESS, POSTAL_CODE_BUSINESS, CITY_BUSINESS, STATE_BUSINESS, COUNTRY_BUSINESS, NUMBER_OF_EMPLOYEE, SALES_VOLUME,
                                                    TAX_ID, COMMERCIAL_REGISTER, BRANCHES, BUSINESS_CATEGORY, INFO, MANAGER_NAME, ASSISTANT_NAME, STREET_OTHER, POSTAL_CODE_OTHER, CITY_OTHER,
                                                    STATE_OTHER, COUNTRY_OTHER, TELEPHONE_BUSINESS1, TELEPHONE_BUSINESS2, FAX_BUSINESS, TELEPHONE_CALLBACK, TELEPHONE_CAR, TELEPHONE_COMPANY,
                                                    TELEPHONE_HOME1, TELEPHONE_HOME2, FAX_HOME, CELLULAR_TELEPHONE1, CELLULAR_TELEPHONE2, TELEPHONE_OTHER, FAX_OTHER, EMAIL1, EMAIL2, EMAIL3,
                                                    URL, TELEPHONE_ISDN, TELEPHONE_PAGER, TELEPHONE_PRIMARY, TELEPHONE_RADIO, TELEPHONE_TELEX, TELEPHONE_TTYTDD, INSTANT_MESSENGER1,
                                                    INSTANT_MESSENGER2, TELEPHONE_IP, TELEPHONE_ASSISTANT, COMPANY, IMAGE1, IMAGE1_CONTENT_TYPE, USERFIELD01, USERFIELD02, USERFIELD03,
                                                    USERFIELD04, USERFIELD05, USERFIELD06, USERFIELD07, USERFIELD08, USERFIELD09, USERFIELD10, USERFIELD11, USERFIELD12, USERFIELD13,
                                                    USERFIELD14, USERFIELD15, USERFIELD16, USERFIELD17, USERFIELD18, USERFIELD19, USERFIELD20, DISTRIBUTIONLIST, YOMI_FIRST_NAME, YOMI_LAST_NAME,
                                                    YOMI_COMPANY, ADDRESS_BUSINESS, ADDRESS_HOME, ADDRESS_OTHER, UID
    };
    // @formatter:on

    public static final int[] ALL_COLUMNS = com.openexchange.tools.arrays.Arrays.addUniquely(CONTENT_COLUMNS, new int[] {
        // From ContactObject itself
        INTERNAL_USERID,
        // Produces error: missing field in mapping: 593 (ContactWriter.java:603)// CONTEXTID,
        NUMBER_OF_DISTRIBUTIONLIST,
        // NUMBER_OF_IMAGES,
        // IMAGE_LAST_MODIFIED, FILE_AS,
        // Produces a MySQLDataException// ATTACHMENT,
        // IMAGE1_CONTENT_TYPE, MARK_AS_DISTRIBUTIONLIST,
        DEFAULT_ADDRESS,
        // IMAGE1_URL,
        USE_COUNT,
        // From CommonObject
        // Left out as it is unclear what these are for and they produce an error//LABEL_NONE, LABEL_1, LABEL_2, LABEL_3, LABEL_4,
        // LABEL_5, LABEL_6, LABEL_7, LABEL_8, LABEL_9, LABEL_10,
        CATEGORIES, PRIVATE_FLAG, COLOR_LABEL, NUMBER_OF_ATTACHMENTS,
        // From FolderChildObject
        FOLDER_ID,
        // From DataObject
        OBJECT_ID, CREATED_BY, MODIFIED_BY, CREATION_DATE, LAST_MODIFIED, LAST_MODIFIED_UTC });

    public static final int[] JSON_COLUMNS = com.openexchange.tools.arrays.Arrays.addUniquely(CONTENT_COLUMNS, new int[] { OBJECT_ID, CREATED_BY, MODIFIED_BY, CREATION_DATE, LAST_MODIFIED, LAST_MODIFIED_UTC, USE_COUNT, FILE_AS, NUMBER_OF_IMAGES, INTERNAL_USERID, CATEGORIES, FOLDER_ID, IMAGE1_URL, LAST_MODIFIED_OF_NEWEST_ATTACHMENT, NUMBER_OF_ATTACHMENTS, PRIVATE_FLAG, USE_COUNT, DEFAULT_ADDRESS, MARK_AS_DISTRIBUTIONLIST
    });

    protected String display_name;

    protected String given_name;

    protected String sur_name;

    protected String middle_name;

    protected String suffix;

    protected String title;

    protected String street;

    protected String postal_code;

    protected String city;

    protected String state;

    protected String country;

    protected Date birthday;

    protected String marital_status;

    protected String number_of_children;

    protected String profession;

    protected String nickname;

    protected String spouse_name;

    protected Date anniversary;

    protected String note;

    protected String department;

    protected String position;

    protected String employee_type;

    protected String room_number;

    protected String street_business;

    protected String postal_code_business;

    protected String city_business;

    protected String state_business;

    protected String country_business;

    protected String number_of_employee;

    protected String sales_volume;

    protected String tax_id;

    protected String commercial_register;

    protected String branches;

    protected String business_category;

    protected String info;

    protected String manager_name;

    protected String assistant_name;

    protected String street_other;

    protected String postal_code_other;

    protected String city_other;

    protected String state_other;

    protected String country_other;

    protected String telephone_business1;

    protected String telephone_business2;

    protected String fax_business;

    protected String telephone_callback;

    protected String telephone_car;

    protected String telephone_company;

    protected String telephone_home1;

    protected String telephone_home2;

    protected String fax_home;

    protected String cellular_telephone1;

    protected String cellular_telephone2;

    protected String telephone_other;

    protected String fax_other;

    protected String email1;

    protected String email2;

    protected String email3;

    protected String url;

    protected String telephone_isdn;

    protected String telephone_pager;

    protected String telephone_primary;

    protected String telephone_radio;

    protected String telephone_telex;

    protected String telephone_ttytdd;

    protected String instant_messenger1;

    protected String instant_messenger2;

    protected String telephone_ip;

    protected String telephone_assistant;

    protected String company;

    protected String userfield01;

    protected String userfield02;

    protected String userfield03;

    protected String userfield04;

    protected String userfield05;

    protected String userfield06;

    protected String userfield07;

    protected String userfield08;

    protected String userfield09;

    protected String userfield10;

    protected String userfield11;

    protected String userfield12;

    protected String userfield13;

    protected String userfield14;

    protected String userfield15;

    protected String userfield16;

    protected String userfield17;

    protected String userfield18;

    protected String userfield19;

    protected String userfield20;

    protected int cid;

    protected int internal_userId;

    protected int defaultaddress;

    protected volatile byte[] image1;

    protected Date image_last_modified;

    protected int number_of_images;

    protected String file_as;

    protected volatile String imageContentType;

    protected boolean mark_as_distributionlist;

    protected String yomiFirstName;

    protected String yomiLastName;

    protected String yomiCompany;

    protected String addressHome;

    protected String addressBusiness;

    protected String addressOther;

    protected String vCardId;

    protected boolean display_nameIsSet;

    protected boolean given_nameIsSet;

    protected boolean sur_nameIsSet;

    protected boolean middle_nameIsSet;

    protected boolean suffixIsSet;

    protected boolean titleIsSet;

    protected boolean streetIsSet;

    protected boolean postal_codeIsSet;

    protected boolean cityIsSet;

    protected boolean stateIsSet;

    protected boolean countryIsSet;

    protected boolean birthdayIsSet;

    protected boolean marital_statusIsSet;

    protected boolean number_of_childrenIsSet;

    protected boolean professionIsSet;

    protected boolean nicknameIsSet;

    protected boolean spouse_nameIsSet;

    protected boolean anniversaryIsSet;

    protected boolean noteIsSet;

    protected boolean departmentIsSet;

    protected boolean positionIsSet;

    protected boolean employee_typeIsSet;

    protected boolean room_numberIsSet;

    protected boolean street_businessIsSet;

    protected boolean postal_code_businessIsSet;

    protected boolean city_businessIsSet;

    protected boolean state_businessIsSet;

    protected boolean country_businessIsSet;

    protected boolean number_of_employeeIsSet;

    protected boolean sales_volumeIsSet;

    protected boolean tax_idIsSet;

    protected boolean commercial_registerIsSet;

    protected boolean branchesIsSet;

    protected boolean business_categoryIsSet;

    protected boolean infoIsSet;

    protected boolean manager_nameIsSet;

    protected boolean assistant_nameIsSet;

    protected boolean street_otherIsSet;

    protected boolean postal_code_otherIsSet;

    protected boolean city_otherIsSet;

    protected boolean state_otherIsSet;

    protected boolean country_otherIsSet;

    protected boolean telephone_business1IsSet;

    protected boolean telephone_business2IsSet;

    protected boolean fax_businessIsSet;

    protected boolean telephone_callbackIsSet;

    protected boolean telephone_carIsSet;

    protected boolean telephone_companyIsSet;

    protected boolean telephone_home1IsSet;

    protected boolean telephone_home2IsSet;

    protected boolean fax_homeIsSet;

    protected boolean cellular_telephone1IsSet;

    protected boolean cellular_telephone2IsSet;

    protected boolean telephone_otherIsSet;

    protected boolean fax_otherIsSet;

    protected boolean email1IsSet;

    protected boolean email2IsSet;

    protected boolean email3IsSet;

    protected boolean urlIsSet;

    protected boolean telephone_isdnIsSet;

    protected boolean telephone_pagerIsSet;

    protected boolean telephone_primaryIsSet;

    protected boolean telephone_radioIsSet;

    protected boolean telephone_telexIsSet;

    protected boolean telephone_ttytddIsSet;

    protected boolean instant_messenger1IsSet;

    protected boolean instant_messenger2IsSet;

    protected boolean telephone_ipIsSet;

    protected boolean telephone_assistantIsSet;

    protected boolean defaultaddressIsSet;

    protected boolean companyIsSet;

    protected volatile boolean image1IsSet;

    protected volatile boolean containsImageIsSet;

    protected boolean userfield01IsSet;

    protected boolean userfield02IsSet;

    protected boolean userfield03IsSet;

    protected boolean userfield04IsSet;

    protected boolean userfield05IsSet;

    protected boolean userfield06IsSet;

    protected boolean userfield07IsSet;

    protected boolean userfield08IsSet;

    protected boolean userfield09IsSet;

    protected boolean userfield10IsSet;

    protected boolean userfield11IsSet;

    protected boolean userfield12IsSet;

    protected boolean userfield13IsSet;

    protected boolean userfield14IsSet;

    protected boolean userfield15IsSet;

    protected boolean userfield16IsSet;

    protected boolean userfield17IsSet;

    protected boolean userfield18IsSet;

    protected boolean userfield19IsSet;

    protected boolean userfield20IsSet;

    protected boolean linksIsSet;

    protected boolean created_fromIsSet;

    protected boolean changed_fromIsSet;

    protected boolean creating_dateIsSet;

    protected boolean changing_dateIsSet;

    protected boolean cidIsSet;

    protected boolean internal_userIdIsSet;

    protected boolean image_last_modifiedIsSet;

    protected boolean file_asIsSet;

    protected volatile boolean bImageContentType;

    protected boolean mark_as_distributionlistIsSet;

    protected boolean number_of_dlistsIsSet;

    protected int number_of_dlists;

    protected int useCount;

    protected boolean useCountIsSet;

    protected boolean yomiFirstNameIsSet;

    protected boolean yomiLastNameIsSet;

    protected boolean yomiCompanyIsSet;

    protected boolean addressHomeIsSet;

    protected boolean addressBusinessIsSet;

    protected boolean addressOtherIsSet;

    protected boolean vCardIdIsSet;

    protected DistributionListEntryObject[] dlists;

    private final Collection<OXException> warnings;

    /** The new account-aware parentFolderID */
    private String folderId;
    /** The new contactId */
    private String id;

    private boolean folder_idIsSet;
    private boolean idIsSet;

    /**
     * Initializes a new {@link Contact}.
     */
    public Contact() {
        this(true);
    }

    /**
     * Initializes a new {@link Contact}.
     *
     * @param reset optionally performs a reset
     */
    public Contact(boolean reset) {
        if (reset) {
            reset();
        }
        topic = "ox/common/contact";
        warnings = new LinkedList<>();
    }

    /**
     * Adds specified warning to this contact.
     *
     * @param warning The warning
     */
    public void addWarning(final OXException warning) {
        if (null == warning) {
            return;
        }
        warnings.add(warning);
    }

    /**
     * Gets the warnings.
     *
     * @return The warnings
     */
    public Collection<OXException> getWarnings() {
        return Collections.unmodifiableCollection(warnings);
    }

    // GET METHODS

    /**
     * Gets the contact's object identifier.
     *
     * @return The object identifier, or <code>null</code> if not set
     */
    public String getId() {
        return id;
    }

    /**
     * Gets the contact's object identifier.
     *
     * @param fallbackToObjectId <code>true</code> to fall back to the numerical object identifier if the id field is not set, <code>false</code>, otherwise
     * @return The object identifier, or <code>null</code> if not set
     */
    public String getId(boolean fallbackToObjectId) {
        String id = getId();
        if (false == fallbackToObjectId || null != id) {
            return id;
        }
        int objectId = getObjectID();
        return 0 < objectId ? String.valueOf(objectId) : null;
    }

    /**
     * Gets the identifier of the folder the contact is located in.
     *
     * @return The folder identifier, or <code>null</code> if not set
     */
    public String getFolderId() {
        return folderId;
    }

    /**
     * Gets the identifier of the folder the contact is located in.
     *
     * @param fallbackToParentFolderId <code>true</code> to fall back to the numerical parent folder identifier if the id field is not set, <code>false</code>, otherwise
     * @return The folder identifier, or <code>null</code> if not set
     */
    public String getFolderId(boolean fallbackToParentFolderId) {
        String folderId = getFolderId();
        if (false == fallbackToParentFolderId || null != folderId) {
            return folderId;
        }
        int parentFolderId = getParentFolderID();
        return 0 < parentFolderId ? String.valueOf(parentFolderId) : null;
    }

    public String getDisplayName() {
        return display_name;
    }

    public String getGivenName() {
        return given_name;
    }

    public String getSurName() {
        return sur_name;
    }

    public String getMiddleName() {
        return middle_name;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getTitle() {
        return title;
    }

    public String getStreetHome() {
        return street;
    }

    public String getPostalCodeHome() {
        return postal_code;
    }

    public String getCityHome() {
        return city;
    }

    public String getStateHome() {
        return state;
    }

    public String getCountryHome() {
        return country;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getMaritalStatus() {
        return marital_status;
    }

    public String getNumberOfChildren() {
        return number_of_children;
    }

    public String getProfession() {
        return profession;
    }

    public String getNickname() {
        return nickname;
    }

    public String getSpouseName() {
        return spouse_name;
    }

    public Date getAnniversary() {
        return anniversary;
    }

    public String getNote() {
        return note;
    }

    public String getDepartment() {
        return department;
    }

    public String getPosition() {
        return position;
    }

    public String getEmployeeType() {
        return employee_type;
    }

    public String getRoomNumber() {
        return room_number;
    }

    public String getStreetBusiness() {
        return street_business;
    }

    public String getPostalCodeBusiness() {
        return postal_code_business;
    }

    public String getCityBusiness() {
        return city_business;
    }

    public String getStateBusiness() {
        return state_business;
    }

    public String getCountryBusiness() {
        return country_business;
    }

    public String getNumberOfEmployee() {
        return number_of_employee;
    }

    public String getSalesVolume() {
        return sales_volume;
    }

    public String getTaxID() {
        return tax_id;
    }

    public String getCommercialRegister() {
        return commercial_register;
    }

    public String getBranches() {
        return branches;
    }

    public String getBusinessCategory() {
        return business_category;
    }

    public String getInfo() {
        return info;
    }

    public String getManagerName() {
        return manager_name;
    }

    public String getAssistantName() {
        return assistant_name;
    }

    public String getStreetOther() {
        return street_other;
    }

    public String getPostalCodeOther() {
        return postal_code_other;
    }

    public String getCityOther() {
        return city_other;
    }

    public String getStateOther() {
        return state_other;
    }

    public String getCountryOther() {
        return country_other;
    }

    public String getTelephoneBusiness1() {
        return telephone_business1;
    }

    public String getTelephoneBusiness2() {
        return telephone_business2;
    }

    public String getFaxBusiness() {
        return fax_business;
    }

    public String getTelephoneCallback() {
        return telephone_callback;
    }

    public String getTelephoneCar() {
        return telephone_car;
    }

    public String getTelephoneCompany() {
        return telephone_company;
    }

    public String getTelephoneHome1() {
        return telephone_home1;
    }

    public String getTelephoneHome2() {
        return telephone_home2;
    }

    public String getFaxHome() {
        return fax_home;
    }

    public String getCellularTelephone1() {
        return cellular_telephone1;
    }

    public String getCellularTelephone2() {
        return cellular_telephone2;
    }

    public String getTelephoneOther() {
        return telephone_other;
    }

    public String getFaxOther() {
        return fax_other;
    }

    public String getEmail1() {
        return email1;
    }

    public String getEmail2() {
        return email2;
    }

    public String getEmail3() {
        return email3;
    }

    public String getURL() {
        return url;
    }

    public String getTelephoneISDN() {
        return telephone_isdn;
    }

    public String getTelephonePager() {
        return telephone_pager;
    }

    public String getTelephonePrimary() {
        return telephone_primary;
    }

    public String getTelephoneRadio() {
        return telephone_radio;
    }

    public String getTelephoneTelex() {
        return telephone_telex;
    }

    public String getTelephoneTTYTTD() {
        return telephone_ttytdd;
    }

    public String getInstantMessenger1() {
        return instant_messenger1;
    }

    public String getInstantMessenger2() {
        return instant_messenger2;
    }

    public String getTelephoneIP() {
        return telephone_ip;
    }

    public String getTelephoneAssistant() {
        return telephone_assistant;
    }

    public int getDefaultAddress() {
        return defaultaddress;
    }

    public String getCompany() {
        return company;
    }

    public byte[] getImage1() {
        return image1;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public int getNumberOfImages() {
        return number_of_images;
    }

    public String getUserField01() {
        return userfield01;
    }

    public String getUserField02() {
        return userfield02;
    }

    public String getUserField03() {
        return userfield03;
    }

    public String getUserField04() {
        return userfield04;
    }

    public String getUserField05() {
        return userfield05;
    }

    public String getUserField06() {
        return userfield06;
    }

    public String getUserField07() {
        return userfield07;
    }

    public String getUserField08() {
        return userfield08;
    }

    public String getUserField09() {
        return userfield09;
    }

    public String getUserField10() {
        return userfield10;
    }

    public String getUserField11() {
        return userfield11;
    }

    public String getUserField12() {
        return userfield12;
    }

    public String getUserField13() {
        return userfield13;
    }

    public String getUserField14() {
        return userfield14;
    }

    public String getUserField15() {
        return userfield15;
    }

    public String getUserField16() {
        return userfield16;
    }

    public String getUserField17() {
        return userfield17;
    }

    public String getUserField18() {
        return userfield18;
    }

    public String getUserField19() {
        return userfield19;
    }

    public String getUserField20() {
        return userfield20;
    }

    public int getNumberOfDistributionLists() {
        return number_of_dlists;
    }

    public DistributionListEntryObject[] getDistributionList() {
        return dlists;
    }

    public int getContextId() {
        return cid;
    }

    public int getInternalUserId() {
        return internal_userId;
    }

    public Date getImageLastModified() {
        return image_last_modified;
    }

    public String getFileAs() {
        return file_as;
    }

    public boolean getMarkAsDistribtuionlist() {
        return mark_as_distributionlist;
    }

    public int getUseCount() {
        return useCount;
    }

    public String getYomiFirstName() {
        return yomiFirstName;
    }

    public String getYomiLastName() {
        return yomiLastName;
    }

    public String getYomiCompany() {
        return yomiCompany;
    }

    public String getAddressBusiness() {
        return addressBusiness;
    }

    public String getAddressHome() {
        return addressHome;
    }

    public String getAddressOther() {
        return addressOther;
    }

    public String getVCardId() {
        return vCardId;
    }

    // SET METHODS

    public void setId(String id) {
        this.id = id;
        idIsSet = true;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
        folder_idIsSet = true;
    }

    public void setDisplayName(final String display_name) {
        this.display_name = display_name;
        display_nameIsSet = true;
    }

    public void setGivenName(final String given_name) {
        this.given_name = given_name;
        given_nameIsSet = true;
    }

    public void setSurName(final String sur_name) {
        this.sur_name = sur_name;
        sur_nameIsSet = true;
    }

    public void setMiddleName(final String middle_name) {
        this.middle_name = middle_name;
        middle_nameIsSet = true;
    }

    public void setSuffix(final String suffix) {
        this.suffix = suffix;
        suffixIsSet = true;
    }

    public void setTitle(final String title) {
        this.title = title;
        titleIsSet = true;
    }

    public void setStreetHome(final String street) {
        this.street = street;
        streetIsSet = true;
    }

    public void setPostalCodeHome(final String postal_code) {
        this.postal_code = postal_code;
        postal_codeIsSet = true;
    }

    public void setCityHome(final String city) {
        this.city = city;
        cityIsSet = true;
    }

    public void setStateHome(final String state) {
        this.state = state;
        stateIsSet = true;
    }

    public void setCountryHome(final String country) {
        this.country = country;
        countryIsSet = true;
    }

    public void setBirthday(final Date birthday) {
        this.birthday = birthday;
        birthdayIsSet = true;
    }

    public void setMaritalStatus(final String marital_status) {
        this.marital_status = marital_status;
        marital_statusIsSet = true;
    }

    public void setNumberOfChildren(final String number_of_children) {
        this.number_of_children = number_of_children;
        number_of_childrenIsSet = true;
    }

    public void setProfession(final String profession) {
        this.profession = profession;
        professionIsSet = true;
    }

    public void setNickname(final String nickname) {
        this.nickname = nickname;
        nicknameIsSet = true;
    }

    public void setSpouseName(final String spouse_name) {
        this.spouse_name = spouse_name;
        spouse_nameIsSet = true;
    }

    public void setAnniversary(final Date anniversary) {
        this.anniversary = anniversary;
        anniversaryIsSet = true;
    }

    public void setNote(final String note) {
        this.note = note;
        noteIsSet = true;
    }

    public void setDepartment(final String department) {
        this.department = department;
        departmentIsSet = true;
    }

    public void setPosition(final String position) {
        this.position = position;
        positionIsSet = true;
    }

    public void setEmployeeType(final String employee_type) {
        this.employee_type = employee_type;
        employee_typeIsSet = true;
    }

    public void setRoomNumber(final String room_number) {
        this.room_number = room_number;
        room_numberIsSet = true;
    }

    public void setStreetBusiness(final String street_business) {
        this.street_business = street_business;
        street_businessIsSet = true;
    }

    public void setPostalCodeBusiness(final String postal_code_business) {
        this.postal_code_business = postal_code_business;
        postal_code_businessIsSet = true;
    }

    public void setCityBusiness(final String city_business) {
        this.city_business = city_business;
        city_businessIsSet = true;
    }

    public void setStateBusiness(final String state_business) {
        this.state_business = state_business;
        state_businessIsSet = true;
    }

    public void setCountryBusiness(final String country_business) {
        this.country_business = country_business;
        country_businessIsSet = true;
    }

    public void setNumberOfEmployee(final String number_of_employee) {
        this.number_of_employee = number_of_employee;
        number_of_employeeIsSet = true;
    }

    public void setSalesVolume(final String sales_volume) {
        this.sales_volume = sales_volume;
        sales_volumeIsSet = true;
    }

    public void setTaxID(final String tax_id) {
        this.tax_id = tax_id;
        tax_idIsSet = true;
    }

    public void setCommercialRegister(final String commercial_register) {
        this.commercial_register = commercial_register;
        commercial_registerIsSet = true;
    }

    public void setBranches(final String branches) {
        this.branches = branches;
        branchesIsSet = true;
    }

    public void setBusinessCategory(final String business_category) {
        this.business_category = business_category;
        business_categoryIsSet = true;
    }

    public void setInfo(final String info) {
        this.info = info;
        infoIsSet = true;
    }

    public void setManagerName(final String manager_name) {
        this.manager_name = manager_name;
        manager_nameIsSet = true;
    }

    public void setAssistantName(final String assistant_name) {
        this.assistant_name = assistant_name;
        assistant_nameIsSet = true;
    }

    public void setStreetOther(final String street_other) {
        this.street_other = street_other;
        street_otherIsSet = true;
    }

    public void setPostalCodeOther(final String postal_code_other) {
        this.postal_code_other = postal_code_other;
        postal_code_otherIsSet = true;
    }

    public void setCityOther(final String city_other) {
        this.city_other = city_other;
        city_otherIsSet = true;
    }

    public void setStateOther(final String state_other) {
        this.state_other = state_other;
        state_otherIsSet = true;
    }

    public void setCountryOther(final String country_other) {
        this.country_other = country_other;
        country_otherIsSet = true;
    }

    public void setTelephoneBusiness1(final String telephone_business1) {
        this.telephone_business1 = telephone_business1;
        telephone_business1IsSet = true;
    }

    public void setTelephoneBusiness2(final String telephone_business2) {
        this.telephone_business2 = telephone_business2;
        telephone_business2IsSet = true;
    }

    public void setFaxBusiness(final String fax_business) {
        this.fax_business = fax_business;
        fax_businessIsSet = true;
    }

    public void setTelephoneCallback(final String telephone_callback) {
        this.telephone_callback = telephone_callback;
        telephone_callbackIsSet = true;
    }

    public void setTelephoneCar(final String telephone_car) {
        this.telephone_car = telephone_car;
        telephone_carIsSet = true;
    }

    public void setTelephoneCompany(final String telephone_company) {
        this.telephone_company = telephone_company;
        telephone_companyIsSet = true;
    }

    public void setTelephoneHome1(final String telephone_home1) {
        this.telephone_home1 = telephone_home1;
        telephone_home1IsSet = true;
    }

    public void setTelephoneHome2(final String telephone_home2) {
        this.telephone_home2 = telephone_home2;
        telephone_home2IsSet = true;
    }

    public void setFaxHome(final String fax_home) {
        this.fax_home = fax_home;
        fax_homeIsSet = true;
    }

    public void setCellularTelephone1(final String cellular_telephone1) {
        this.cellular_telephone1 = cellular_telephone1;
        cellular_telephone1IsSet = true;
    }

    public void setCellularTelephone2(final String cellular_telephone2) {
        this.cellular_telephone2 = cellular_telephone2;
        cellular_telephone2IsSet = true;
    }

    public void setTelephoneOther(final String telephone_other) {
        this.telephone_other = telephone_other;
        telephone_otherIsSet = true;
    }

    public void setFaxOther(final String fax_other) {
        this.fax_other = fax_other;
        fax_otherIsSet = true;
    }

    public void setEmail1(final String email1) {
        this.email1 = email1;
        email1IsSet = true;
    }

    public void setEmail2(final String email2) {
        this.email2 = email2;
        email2IsSet = true;
    }

    public void setEmail3(final String email3) {
        this.email3 = email3;
        email3IsSet = true;
    }

    public void setURL(final String url) {
        this.url = url;
        urlIsSet = true;
    }

    public void setTelephoneISDN(final String telephone_isdn) {
        this.telephone_isdn = telephone_isdn;
        telephone_isdnIsSet = true;
    }

    public void setTelephonePager(final String telephone_pager) {
        this.telephone_pager = telephone_pager;
        telephone_pagerIsSet = true;
    }

    public void setTelephonePrimary(final String telephone_primary) {
        this.telephone_primary = telephone_primary;
        telephone_primaryIsSet = true;
    }

    public void setTelephoneRadio(final String telephone_radio) {
        this.telephone_radio = telephone_radio;
        telephone_radioIsSet = true;
    }

    public void setTelephoneTelex(final String telephone_telex) {
        this.telephone_telex = telephone_telex;
        telephone_telexIsSet = true;
    }

    public void setTelephoneTTYTTD(final String telephone_ttyttd) {
        telephone_ttytdd = telephone_ttyttd;
        telephone_ttytddIsSet = true;
    }

    public void setInstantMessenger1(final String instant_messenger1) {
        this.instant_messenger1 = instant_messenger1;
        instant_messenger1IsSet = true;
    }

    public void setInstantMessenger2(final String instant_messenger2) {
        this.instant_messenger2 = instant_messenger2;
        instant_messenger2IsSet = true;
    }

    public void setTelephoneIP(final String phone_ip) {
        telephone_ip = phone_ip;
        telephone_ipIsSet = true;
    }

    public void setTelephoneAssistant(final String telephone_assistant) {
        this.telephone_assistant = telephone_assistant;
        telephone_assistantIsSet = true;
    }

    public void setDefaultAddress(final int defaultaddress) {
        this.defaultaddress = defaultaddress;
        defaultaddressIsSet = true;
    }

    public void setCompany(final String company) {
        this.company = company;
        companyIsSet = true;
    }

    public void setUserField01(final String userfield01) {
        this.userfield01 = userfield01;
        userfield01IsSet = true;
    }

    public void setUserField02(final String userfield02) {
        this.userfield02 = userfield02;
        userfield02IsSet = true;
    }

    public void setUserField03(final String userfield03) {
        this.userfield03 = userfield03;
        userfield03IsSet = true;
    }

    public void setUserField04(final String userfield04) {
        this.userfield04 = userfield04;
        userfield04IsSet = true;
    }

    public void setUserField05(final String userfield05) {
        this.userfield05 = userfield05;
        userfield05IsSet = true;
    }

    public void setUserField06(final String userfield06) {
        this.userfield06 = userfield06;
        userfield06IsSet = true;
    }

    public void setUserField07(final String userfield07) {
        this.userfield07 = userfield07;
        userfield07IsSet = true;
    }

    public void setUserField08(final String userfield08) {
        this.userfield08 = userfield08;
        userfield08IsSet = true;
    }

    public void setUserField09(final String userfield09) {
        this.userfield09 = userfield09;
        userfield09IsSet = true;
    }

    public void setUserField10(final String userfield10) {
        this.userfield10 = userfield10;
        userfield10IsSet = true;
    }

    public void setUserField11(final String userfield11) {
        this.userfield11 = userfield11;
        userfield11IsSet = true;
    }

    public void setUserField12(final String userfield12) {
        this.userfield12 = userfield12;
        userfield12IsSet = true;
    }

    public void setUserField13(final String userfield13) {
        this.userfield13 = userfield13;
        userfield13IsSet = true;
    }

    public void setUserField14(final String userfield14) {
        this.userfield14 = userfield14;
        userfield14IsSet = true;
    }

    public void setUserField15(final String userfield15) {
        this.userfield15 = userfield15;
        userfield15IsSet = true;
    }

    public void setUserField16(final String userfield16) {
        this.userfield16 = userfield16;
        userfield16IsSet = true;
    }

    public void setUserField17(final String userfield17) {
        this.userfield17 = userfield17;
        userfield17IsSet = true;
    }

    public void setUserField18(final String userfield18) {
        this.userfield18 = userfield18;
        userfield18IsSet = true;
    }

    public void setUserField19(final String userfield19) {
        this.userfield19 = userfield19;
        userfield19IsSet = true;
    }

    public void setUserField20(final String userfield20) {
        this.userfield20 = userfield20;
        userfield20IsSet = true;
    }

    public void setImage1(final byte[] image1) {
        this.image1 = image1;
        containsImageIsSet = true;
        image1IsSet = true;
        number_of_images = 1;
    }

    public void setImageContentType(final String imageContentType) {
        this.imageContentType = imageContentType;
        bImageContentType = true;
    }

    public void setNumberOfImages(final int number_of_images) {
        this.number_of_images = number_of_images;
    }

    public void setNumberOfDistributionLists(final int listsize) {
        number_of_dlists = listsize;
        number_of_dlistsIsSet = true;
    }

    public void setDistributionList(final DistributionListEntryObject[] dleo) {
        dlists = dleo;
        setNumberOfDistributionLists(null == dleo ? 0 : dleo.length);
        setMarkAsDistributionlist(null == dleo ? false : true);
    }

    public void setContextId(final int cid) {
        this.cid = cid;
        cidIsSet = true;
    }

    public void setInternalUserId(final int internal_userId) {
        this.internal_userId = internal_userId;
        internal_userIdIsSet = true;
    }

    public void setImageLastModified(final Date image_last_modified) {
        this.image_last_modified = image_last_modified;
        image_last_modifiedIsSet = true;
    }

    public void setFileAs(final String file_as) {
        this.file_as = file_as;
        file_asIsSet = true;
    }

    public void setMarkAsDistributionlist(final boolean mark_as_disitributionlist) {
        mark_as_distributionlist = mark_as_disitributionlist;
        mark_as_distributionlistIsSet = true;
    }

    public void setUseCount(final int useCount) {
        this.useCount = useCount;
        useCountIsSet = true;
    }

    public void setYomiFirstName(final String yomiFirstName) {
        this.yomiFirstName = yomiFirstName;
        yomiFirstNameIsSet = true;
    }

    public void setYomiLastName(final String yomiLastName) {
        this.yomiLastName = yomiLastName;
        yomiLastNameIsSet = true;
    }

    public void setYomiCompany(final String yomiCompany) {
        this.yomiCompany = yomiCompany;
        yomiCompanyIsSet = true;
    }

    public void setAddressBusiness(final String addressBusiness) {
        this.addressBusiness = addressBusiness;
        addressBusinessIsSet = true;
    }

    public void setAddressHome(final String addressHome) {
        this.addressHome = addressHome;
        addressHomeIsSet = true;
    }

    public void setAddressOther(final String addressOther) {
        this.addressOther = addressOther;
        addressOtherIsSet = true;
    }

    public void setVCardId(final String vCardId) {
        this.vCardId = vCardId;
        vCardIdIsSet = true;
    }

    // REMOVE METHODS

    public void removeId() {
        id = null;
        idIsSet = false;
    }

    /**
     * Removes a previously set object identifier.
     *
     * @param removeObjectId <code>true</code> to also remove the numerical object identifier implicitly, <code>false</code>, otherwise
     */
    public void removeId(boolean removeObjectId) {
        removeId();
        if (removeObjectId) {
            removeObjectID();
        }
    }

    public void removeFolderId() {
        folderId = null;
        folder_idIsSet = false;
    }

    /**
     * Removes a previously set identifier of the folder the contact is located in.
     *
     * @param removeParentFolderId <code>true</code> to also remove the numerical parent folder identifier implicitly, <code>false</code>, otherwise
     */
    public void removeFolderId(boolean removeParentFolderId) {
        removeFolderId();
        if (removeParentFolderId) {
            removeParentFolderID();
        }
    }

    public void removeDisplayName() {
        display_name = null;
        display_nameIsSet = false;
    }

    public void removeGivenName() {
        given_name = null;
        given_nameIsSet = false;
    }

    public void removeSurName() {
        sur_name = null;
        sur_nameIsSet = false;
    }

    public void removeMiddleName() {
        middle_name = null;
        middle_nameIsSet = false;
    }

    public void removeSuffix() {
        suffix = null;
        suffixIsSet = false;
    }

    public void removeTitle() {
        title = null;
        titleIsSet = false;
    }

    public void removeStreetHome() {
        street = null;
        streetIsSet = false;
    }

    public void removePostalCodeHome() {
        postal_code = null;
        postal_codeIsSet = false;
    }

    public void removeCityHome() {
        city = null;
        cityIsSet = false;
    }

    public void removeStateHome() {
        state = null;
        stateIsSet = false;
    }

    public void removeCountryHome() {
        country = null;
        countryIsSet = false;
    }

    public void removeBirthday() {
        birthday = null;
        birthdayIsSet = false;
    }

    public void removeMaritalStatus() {
        marital_status = null;
        marital_statusIsSet = false;
    }

    public void removeNumberOfChildren() {
        number_of_children = null;
        number_of_childrenIsSet = false;
    }

    public void removeProfession() {
        profession = null;
        professionIsSet = false;
    }

    public void removeNickname() {
        nickname = null;
        nicknameIsSet = false;
    }

    public void removeSpouseName() {
        spouse_name = null;
        spouse_nameIsSet = false;
    }

    public void removeAnniversary() {
        anniversary = null;
        anniversaryIsSet = false;
    }

    public void removeNote() {
        note = null;
        noteIsSet = false;
    }

    public void removeDepartment() {
        department = null;
        departmentIsSet = false;
    }

    public void removePosition() {
        position = null;
        positionIsSet = false;
    }

    public void removeEmployeeType() {
        employee_type = null;
        employee_typeIsSet = false;
    }

    public void removeRoomNumber() {
        room_number = null;
        room_numberIsSet = false;
    }

    public void removeStreetBusiness() {
        street_business = null;
        street_businessIsSet = false;
    }

    public void removePostalCodeBusiness() {
        postal_code_business = null;
        postal_code_businessIsSet = false;
    }

    public void removeCityBusiness() {
        city_business = null;
        city_businessIsSet = false;
    }

    public void removeStateBusiness() {
        state_business = null;
        state_businessIsSet = false;
    }

    public void removeCountryBusiness() {
        country_business = null;
        country_businessIsSet = false;
    }

    public void removeNumberOfEmployee() {
        number_of_employee = null;
        number_of_employeeIsSet = false;
    }

    public void removeSalesVolume() {
        sales_volume = null;
        sales_volumeIsSet = false;
    }

    public void removeTaxID() {
        tax_id = null;
        tax_idIsSet = false;
    }

    public void removeCommercialRegister() {
        commercial_register = null;
        commercial_registerIsSet = false;
    }

    public void removeBranches() {
        branches = null;
        branchesIsSet = false;
    }

    public void removeBusinessCategory() {
        business_category = null;
        business_categoryIsSet = false;
    }

    public void removeInfo() {
        info = null;
        infoIsSet = false;
    }

    public void removeManagerName() {
        manager_name = null;
        manager_nameIsSet = false;
    }

    public void removeAssistantName() {
        assistant_name = null;
        assistant_nameIsSet = false;
    }

    public void removeStreetOther() {
        street_other = null;
        street_otherIsSet = false;
    }

    public void removePostalCodeOther() {
        postal_code_other = null;
        postal_code_otherIsSet = false;
    }

    public void removeCityOther() {
        city_other = null;
        city_otherIsSet = false;
    }

    public void removeStateOther() {
        state_other = null;
        state_otherIsSet = false;
    }

    public void removeCountryOther() {
        country_other = null;
        country_otherIsSet = false;
    }

    public void removeTelephoneBusiness1() {
        telephone_business1 = null;
        telephone_business1IsSet = false;
    }

    public void removeTelephoneBusiness2() {
        telephone_business2 = null;
        telephone_business2IsSet = false;
    }

    public void removeFaxBusiness() {
        fax_business = null;
        fax_businessIsSet = false;
    }

    public void removeFileAs() {
        file_as = null;
        file_asIsSet = false;
    }

    public void removeTelephoneCallback() {
        telephone_callback = null;
        telephone_callbackIsSet = false;
    }

    public void removeTelephoneCar() {
        telephone_car = null;
        telephone_carIsSet = false;
    }

    public void removeTelephoneCompany() {
        telephone_company = null;
        telephone_companyIsSet = false;
    }

    public void removeTelephoneHome1() {
        telephone_home1 = null;
        telephone_home1IsSet = false;
    }

    public void removeTelephoneHome2() {
        telephone_home2 = null;
        telephone_home2IsSet = false;
    }

    public void removeFaxHome() {
        fax_home = null;
        fax_homeIsSet = false;
    }

    public void removeCellularTelephone1() {
        cellular_telephone1 = null;
        cellular_telephone1IsSet = false;
    }

    public void removeCellularTelephone2() {
        cellular_telephone2 = null;
        cellular_telephone2IsSet = false;
    }

    public void removeTelephoneOther() {
        telephone_other = null;
        telephone_otherIsSet = false;
    }

    public void removeFaxOther() {
        fax_other = null;
        fax_otherIsSet = false;
    }

    public void removeEmail1() {
        email1 = null;
        email1IsSet = false;
    }

    public void removeEmail2() {
        email2 = null;
        email2IsSet = false;
    }

    public void removeEmail3() {
        email3 = null;
        email3IsSet = false;
    }

    public void removeURL() {
        url = null;
        urlIsSet = false;
    }

    public void removeTelephoneISDN() {
        telephone_isdn = null;
        telephone_isdnIsSet = false;
    }

    public void removeTelephonePager() {
        telephone_pager = null;
        telephone_pagerIsSet = false;
    }

    public void removeTelephonePrimary() {
        telephone_primary = null;
        telephone_primaryIsSet = false;
    }

    public void removeTelephoneRadio() {
        telephone_radio = null;
        telephone_radioIsSet = false;
    }

    public void removeTelephoneTelex() {
        telephone_telex = null;
        telephone_telexIsSet = false;
    }

    public void removeTelephoneTTYTTD() {
        telephone_ttytdd = null;
        telephone_ttytddIsSet = false;
    }

    public void removeInstantMessenger1() {
        instant_messenger1 = null;
        instant_messenger1IsSet = false;
    }

    public void removeInstantMessenger2() {
        instant_messenger2 = null;
        instant_messenger2IsSet = false;
    }

    public void removeImageLastModified() {
        image_last_modified = null;
        image_last_modifiedIsSet = false;
    }

    public void removeTelephoneIP() {
        telephone_ip = null;
        telephone_ipIsSet = false;
    }

    public void removeTelephoneAssistant() {
        telephone_assistant = null;
        telephone_assistantIsSet = false;
    }

    public void removeDefaultAddress() {
        defaultaddress = 0;
        defaultaddressIsSet = false;
    }

    public void removeCompany() {
        company = null;
        companyIsSet = false;
    }

    public void removeImage1() {
        image1 = null;
        containsImageIsSet = false;
        image1IsSet = false;
        number_of_images = 0;
    }

    public void removeImageContentType() {
        imageContentType = null;
        bImageContentType = false;
    }

    public void removeUserField01() {
        userfield01 = null;
        userfield01IsSet = false;
    }

    public void removeUserField02() {
        userfield02 = null;
        userfield02IsSet = false;
    }

    public void removeUserField03() {
        userfield03 = null;
        userfield03IsSet = false;
    }

    public void removeUserField04() {
        userfield04 = null;
        userfield04IsSet = false;
    }

    public void removeUserField05() {
        userfield05 = null;
        userfield05IsSet = false;
    }

    public void removeUserField06() {
        userfield06 = null;
        userfield06IsSet = false;
    }

    public void removeUserField07() {
        userfield07 = null;
        userfield07IsSet = false;
    }

    public void removeUserField08() {
        userfield08 = null;
        userfield08IsSet = false;
    }

    public void removeUserField09() {
        userfield09 = null;
        userfield09IsSet = false;
    }

    public void removeUserField10() {
        userfield10 = null;
        userfield10IsSet = false;
    }

    public void removeUserField11() {
        userfield11 = null;
        userfield11IsSet = false;
    }

    public void removeUserField12() {
        userfield12 = null;
        userfield12IsSet = false;
    }

    public void removeUserField13() {
        userfield13 = null;
        userfield13IsSet = false;
    }

    public void removeUserField14() {
        userfield14 = null;
        userfield14IsSet = false;
    }

    public void removeUserField15() {
        userfield15 = null;
        userfield15IsSet = false;
    }

    public void removeUserField16() {
        userfield16 = null;
        userfield16IsSet = false;
    }

    public void removeUserField17() {
        userfield17 = null;
        userfield17IsSet = false;
    }

    public void removeUserField18() {
        userfield18 = null;
        userfield18IsSet = false;
    }

    public void removeUserField19() {
        userfield19 = null;
        userfield19IsSet = false;
    }

    public void removeUserField20() {
        userfield20 = null;
        userfield20IsSet = false;
    }

    public void removeNumberOfDistributionLists() {
        dlists = null;
        number_of_dlists = 0;
        number_of_dlistsIsSet = false;
    }

    public void removeDistributionLists() {
        dlists = null;
        number_of_dlists = 0;
        number_of_dlistsIsSet = false;
    }

    public void removeMarkAsDistributionlist() {
        mark_as_distributionlist = false;
        mark_as_distributionlistIsSet = false;
    }

    public void removeContextID() {
        cid = 0;
        cidIsSet = false;
    }

    public void removeInternalUserId() {
        internal_userId = 0;
        internal_userIdIsSet = false;
    }

    public void removeUseCount() {
        useCount = 0;
        useCountIsSet = false;
    }

    public void removeYomiFirstName() {
        yomiFirstName = null;
        yomiFirstNameIsSet = false;
    }

    public void removeYomiLastName() {
        yomiLastName = null;
        yomiLastNameIsSet = false;
    }

    public void removeYomiCompany() {
        yomiCompany = null;
        yomiCompanyIsSet = false;
    }

    public void removeAddressHome() {
        addressHome = null;
        addressHomeIsSet = false;
    }

    public void removeAddressBusiness() {
        addressBusiness = null;
        addressBusinessIsSet = false;
    }

    public void removeAddressOther() {
        addressOther = null;
        addressOtherIsSet = false;
    }

    public void removeVCardId() {
        vCardId = null;
        vCardIdIsSet = false;
    }

    // CONTAINS METHODS

    public boolean containsId() {
        return idIsSet;
    }

    /**
     * Gets a value indicating whether the contact's object identifier has previously been <i>set</i> or not.
     *
     * @param fallbackToObjectId <code>true</code> to also consider whether the numerical object identifier is set, <code>false</code>, otherwise
     * @return <code>true</code> if the object identifier is set, <code>false</code>, otherwise
     */
    public boolean containsId(boolean fallbackToObjectId) {
        boolean idIsSet = containsId();
        return idIsSet || fallbackToObjectId && containsObjectID();
    }

    public boolean containsFolderId() {
        return folder_idIsSet;
    }

    /**
     * Gets a value indicating whether the identifier of the folder the contact is located in has previously been <i>set</i> or not.
     *
     * @param fallbackToParentFolderId <code>true</code> to also consider whether the numerical parent folder identifier is set, <code>false</code>, otherwise
     * @return <code>true</code> if the folder identifier is set, <code>false</code>, otherwise
     */
    public boolean containsFolderId(boolean fallbackToObjectId) {
        boolean folder_idIsSet = containsFolderId();
        return folder_idIsSet || fallbackToObjectId && containsParentFolderID();
    }

    public boolean containsDisplayName() {
        return display_nameIsSet;
    }

    public boolean containsGivenName() {
        return given_nameIsSet;
    }

    public boolean containsSurName() {
        return sur_nameIsSet;
    }

    public boolean containsMiddleName() {
        return middle_nameIsSet;
    }

    public boolean containsSuffix() {
        return suffixIsSet;
    }

    public boolean containsTitle() {
        return titleIsSet;
    }

    public boolean containsStreetHome() {
        return streetIsSet;
    }

    public boolean containsPostalCodeHome() {
        return postal_codeIsSet;
    }

    public boolean containsCityHome() {
        return cityIsSet;
    }

    public boolean containsStateHome() {
        return stateIsSet;
    }

    public boolean containsCountryHome() {
        return countryIsSet;
    }

    public boolean containsBirthday() {
        return birthdayIsSet;
    }

    public boolean containsMaritalStatus() {
        return marital_statusIsSet;
    }

    public boolean containsNumberOfChildren() {
        return number_of_childrenIsSet;
    }

    public boolean containsProfession() {
        return professionIsSet;
    }

    public boolean containsNickname() {
        return nicknameIsSet;
    }

    public boolean containsSpouseName() {
        return spouse_nameIsSet;
    }

    public boolean containsAnniversary() {
        return anniversaryIsSet;
    }

    public boolean containsNote() {
        return noteIsSet;
    }

    public boolean containsDepartment() {
        return departmentIsSet;
    }

    public boolean containsPosition() {
        return positionIsSet;
    }

    public boolean containsEmployeeType() {
        return employee_typeIsSet;
    }

    public boolean containsRoomNumber() {
        return room_numberIsSet;
    }

    public boolean containsStreetBusiness() {
        return street_businessIsSet;
    }

    public boolean containsPostalCodeBusiness() {
        return postal_code_businessIsSet;
    }

    public boolean containsCityBusiness() {
        return city_businessIsSet;
    }

    public boolean containsStateBusiness() {
        return state_businessIsSet;
    }

    public boolean containsCountryBusiness() {
        return country_businessIsSet;
    }

    public boolean containsNumberOfEmployee() {
        return number_of_employeeIsSet;
    }

    public boolean containsSalesVolume() {
        return sales_volumeIsSet;
    }

    public boolean containsTaxID() {
        return tax_idIsSet;
    }

    public boolean containsCommercialRegister() {
        return commercial_registerIsSet;
    }

    public boolean containsBranches() {
        return branchesIsSet;
    }

    public boolean containsBusinessCategory() {
        return business_categoryIsSet;
    }

    public boolean containsInfo() {
        return infoIsSet;
    }

    public boolean containsManagerName() {
        return manager_nameIsSet;
    }

    public boolean containsAssistantName() {
        return assistant_nameIsSet;
    }

    public boolean containsStreetOther() {
        return street_otherIsSet;
    }

    public boolean containsPostalCodeOther() {
        return postal_code_otherIsSet;
    }

    public boolean containsCityOther() {
        return city_otherIsSet;
    }

    public boolean containsStateOther() {
        return state_otherIsSet;
    }

    public boolean containsCountryOther() {
        return country_otherIsSet;
    }

    public boolean containsTelephoneBusiness1() {
        return telephone_business1IsSet;
    }

    public boolean containsTelephoneBusiness2() {
        return telephone_business2IsSet;
    }

    public boolean containsFaxBusiness() {
        return fax_businessIsSet;
    }

    public boolean containsTelephoneCallback() {
        return telephone_callbackIsSet;
    }

    public boolean containsTelephoneCar() {
        return telephone_carIsSet;
    }

    public boolean containsTelephoneCompany() {
        return telephone_companyIsSet;
    }

    public boolean containsTelephoneHome1() {
        return telephone_home1IsSet;
    }

    public boolean containsTelephoneHome2() {
        return telephone_home2IsSet;
    }

    public boolean containsFaxHome() {
        return fax_homeIsSet;
    }

    public boolean containsCellularTelephone1() {
        return cellular_telephone1IsSet;
    }

    public boolean containsCellularTelephone2() {
        return cellular_telephone2IsSet;
    }

    public boolean containsTelephoneOther() {
        return telephone_otherIsSet;
    }

    public boolean containsFaxOther() {
        return fax_otherIsSet;
    }

    public boolean containsEmail1() {
        return email1IsSet;
    }

    public boolean containsEmail2() {
        return email2IsSet;
    }

    public boolean containsEmail3() {
        return email3IsSet;
    }

    public boolean containsURL() {
        return urlIsSet;
    }

    public boolean containsTelephoneISDN() {
        return telephone_isdnIsSet;
    }

    public boolean containsTelephonePager() {
        return telephone_pagerIsSet;
    }

    public boolean containsTelephonePrimary() {
        return telephone_primaryIsSet;
    }

    public boolean containsTelephoneRadio() {
        return telephone_radioIsSet;
    }

    public boolean containsTelephoneTelex() {
        return telephone_telexIsSet;
    }

    public boolean containsTelephoneTTYTTD() {
        return telephone_ttytddIsSet;
    }

    public boolean containsInstantMessenger1() {
        return instant_messenger1IsSet;
    }

    public boolean containsInstantMessenger2() {
        return instant_messenger2IsSet;
    }

    public boolean containsTelephoneIP() {
        return telephone_ipIsSet;
    }

    public boolean containsTelephoneAssistant() {
        return telephone_assistantIsSet;
    }

    public boolean containsDefaultAddress() {
        return defaultaddressIsSet;
    }

    public boolean containsCompany() {
        return companyIsSet;
    }

    public boolean containsUserField01() {
        return userfield01IsSet;
    }

    public boolean containsUserField02() {
        return userfield02IsSet;
    }

    public boolean containsUserField03() {
        return userfield03IsSet;
    }

    public boolean containsUserField04() {
        return userfield04IsSet;
    }

    public boolean containsUserField05() {
        return userfield05IsSet;
    }

    public boolean containsUserField06() {
        return userfield06IsSet;
    }

    public boolean containsUserField07() {
        return userfield07IsSet;
    }

    public boolean containsUserField08() {
        return userfield08IsSet;
    }

    public boolean containsUserField09() {
        return userfield09IsSet;
    }

    public boolean containsUserField10() {
        return userfield10IsSet;
    }

    public boolean containsUserField11() {
        return userfield11IsSet;
    }

    public boolean containsUserField12() {
        return userfield12IsSet;
    }

    public boolean containsUserField13() {
        return userfield13IsSet;
    }

    public boolean containsUserField14() {
        return userfield14IsSet;
    }

    public boolean containsUserField15() {
        return userfield15IsSet;
    }

    public boolean containsUserField16() {
        return userfield16IsSet;
    }

    public boolean containsUserField17() {
        return userfield17IsSet;
    }

    public boolean containsUserField18() {
        return userfield18IsSet;
    }

    public boolean containsUserField19() {
        return userfield19IsSet;
    }

    public boolean containsUserField20() {
        return userfield20IsSet;
    }

    public boolean containsImage1() {
        return containsImageIsSet;
    }

    public boolean containsImageContentType() {
        return bImageContentType;
    }

    public boolean containsNumberOfDistributionLists() {
        return number_of_dlistsIsSet;
    }

    public boolean containsDistributionLists() {
        return (dlists != null);
    }

    public int getSizeOfDistributionListArray() {
        return number_of_dlists;
    }

    public boolean containsInternalUserId() {
        return internal_userIdIsSet;
    }

    public boolean containsContextId() {
        return cidIsSet;
    }

    public boolean containsImageLastModified() {
        return image_last_modifiedIsSet;
    }

    public boolean containsFileAs() {
        return file_asIsSet;
    }

    public boolean containsMarkAsDistributionlist() {
        return mark_as_distributionlistIsSet;
    }

    public boolean containsUseCount() {
        return useCountIsSet;
    }

    public boolean containsYomiFirstName() {
        return yomiFirstNameIsSet;
    }

    public boolean containsYomiLastName() {
        return yomiLastNameIsSet;
    }

    public boolean containsYomiCompany() {
        return yomiCompanyIsSet;
    }

    public boolean containsAddressHome() {
        return addressHomeIsSet;
    }

    public boolean containsAddressBusiness() {
        return addressBusinessIsSet;
    }

    public boolean containsAddressOther() {
        return addressOtherIsSet;
    }

    public boolean containsVCardId() {
        return vCardIdIsSet;
    }

    @Override
    public void set(final int field, final Object value) {
        switch (field) {
            case POSTAL_CODE_HOME:
                setPostalCodeHome((String) value);
                break;
            case USERFIELD08:
                setUserField08((String) value);
                break;
            case CITY_OTHER:
                setCityOther((String) value);
                break;
            case USERFIELD09:
                setUserField09((String) value);
                break;
            case USERFIELD06:
                setUserField06((String) value);
                break;
            case STATE_BUSINESS:
                setStateBusiness((String) value);
                break;
            case NUMBER_OF_IMAGES:
                setNumberOfImages(null == value ? 0 : ((Integer) value).intValue());
                break;
            case IMAGE1_CONTENT_TYPE:
                setImageContentType((String) value);
                break;
            case GIVEN_NAME:
                setGivenName((String) value);
                break;
            case ANNIVERSARY:
                setAnniversary((Date) value);
                break;
            case USERFIELD18:
                setUserField18((String) value);
                break;
            case SALES_VOLUME:
                setSalesVolume((String) value);
                break;
            case STREET_OTHER:
                setStreetOther((String) value);
                break;
            case USERFIELD04:
                setUserField04((String) value);
                break;
            case POSTAL_CODE_BUSINESS:
                setPostalCodeBusiness((String) value);
                break;
            case TELEPHONE_HOME1:
                setTelephoneHome1((String) value);
                break;
            case USERFIELD19:
                setUserField19((String) value);
                break;
            case FAX_OTHER:
                setFaxOther((String) value);
                break;
            case USERFIELD14:
                setUserField14((String) value);
                break;
            case CITY_HOME:
                setCityHome((String) value);
                break;
            case USERFIELD07:
                setUserField07((String) value);
                break;
            case TITLE:
                setTitle((String) value);
                break;
            case TELEPHONE_ASSISTANT:
                setTelephoneAssistant((String) value);
                break;
            case FAX_BUSINESS:
                setFaxBusiness((String) value);
                break;
            case PROFESSION:
                setProfession((String) value);
                break;
            case DEPARTMENT:
                setDepartment((String) value);
                break;
            case USERFIELD01:
                setUserField01((String) value);
                break;
            case USERFIELD12:
                setUserField12((String) value);
                break;
            case TELEPHONE_IP:
                setTelephoneIP((String) value);
                break;
            case URL:
                setURL((String) value);
                break;
            case NUMBER_OF_EMPLOYEE:
                setNumberOfEmployee((String) value);
                break;
            case POSTAL_CODE_OTHER:
                setPostalCodeOther((String) value);
                break;
            case USERFIELD10:
                setUserField10((String) value);
                break;
            case BIRTHDAY:
                setBirthday((Date) value);
                break;
            case EMAIL1:
                setEmail1((String) value);
                break;
            case STATE_HOME:
                setStateHome((String) value);
                break;
            case TELEPHONE_HOME2:
                setTelephoneHome2((String) value);
                break;
            case TELEPHONE_TTYTDD:
                setTelephoneTTYTTD((String) value);
                break;
            case TELEPHONE_OTHER:
                setTelephoneOther((String) value);
                break;
            case COMMERCIAL_REGISTER:
                setCommercialRegister((String) value);
                break;
            case COUNTRY_BUSINESS:
                setCountryBusiness((String) value);
                break;
            case USERFIELD11:
                setUserField11((String) value);
                break;
            case BUSINESS_CATEGORY:
                setBusinessCategory((String) value);
                break;
            case CONTEXTID:
                setContextId(null == value ? 0 : ((Integer) value).intValue());
                break;
            case STATE_OTHER:
                setStateOther((String) value);
                break;
            case INTERNAL_USERID:
                setInternalUserId(null == value ? 0 : ((Integer) value).intValue());
                break;
            case CELLULAR_TELEPHONE1:
                setCellularTelephone1((String) value);
                break;
            case BRANCHES:
                setBranches((String) value);
                break;
            case NOTE:
                setNote((String) value);
                break;
            case EMAIL3:
                setEmail3((String) value);
                break;
            case CELLULAR_TELEPHONE2:
                setCellularTelephone2((String) value);
                break;
            case INSTANT_MESSENGER1:
                setInstantMessenger1((String) value);
                break;
            case MANAGER_NAME:
                setManagerName((String) value);
                break;
            case TELEPHONE_TELEX:
                setTelephoneTelex((String) value);
                break;
            case EMAIL2:
                setEmail2((String) value);
                break;
            case EMPLOYEE_TYPE:
                setEmployeeType((String) value);
                break;
            case TELEPHONE_RADIO:
                setTelephoneRadio((String) value);
                break;
            case NUMBER_OF_CHILDREN:
                setNumberOfChildren((String) value);
                break;
            case STREET_BUSINESS:
                setStreetBusiness((String) value);
                break;
            case DEFAULT_ADDRESS:
                setDefaultAddress(null == value ? 0 : ((Integer) value).intValue());
                break;
            case MARK_AS_DISTRIBUTIONLIST:
                setMarkAsDistributionlist(null == value ? false : ((Boolean) value).booleanValue());
                break;
            case TELEPHONE_ISDN:
                setTelephoneISDN((String) value);
                break;
            case FAX_HOME:
                setFaxHome((String) value);
                break;
            case MIDDLE_NAME:
                setMiddleName((String) value);
                break;
            case USERFIELD13:
                setUserField13((String) value);
                break;
            case ROOM_NUMBER:
                setRoomNumber((String) value);
                break;
            case MARITAL_STATUS:
                setMaritalStatus((String) value);
                break;
            case USERFIELD15:
                setUserField15((String) value);
                break;
            case COUNTRY_HOME:
                setCountryHome((String) value);
                break;
            case NICKNAME:
                setNickname((String) value);
                break;
            case SUR_NAME:
                setSurName((String) value);
                break;
            case CITY_BUSINESS:
                setCityBusiness((String) value);
                break;
            case USERFIELD20:
                setUserField20((String) value);
                break;
            case TELEPHONE_CALLBACK:
                setTelephoneCallback((String) value);
                break;
            case USERFIELD17:
                setUserField17((String) value);
                break;
            case TELEPHONE_PAGER:
                setTelephonePager((String) value);
                break;
            case COUNTRY_OTHER:
                setCountryOther((String) value);
                break;
            case TAX_ID:
                setTaxID((String) value);
                break;
            case USERFIELD03:
                setUserField03((String) value);
                break;
            case TELEPHONE_COMPANY:
                setTelephoneCompany((String) value);
                break;
            case SUFFIX:
                setSuffix((String) value);
                break;
            case FILE_AS:
                setFileAs((String) value);
                break;
            case USERFIELD02:
                setUserField02((String) value);
                break;
            case TELEPHONE_BUSINESS2:
                setTelephoneBusiness2((String) value);
                break;
            case USERFIELD05:
                setUserField05((String) value);
                break;
            case USERFIELD16:
                setUserField16((String) value);
                break;
            case INFO:
                setInfo((String) value);
                break;
            case COMPANY:
                setCompany((String) value);
                break;
            case DISPLAY_NAME:
                setDisplayName((String) value);
                break;
            case STREET_HOME:
                setStreetHome((String) value);
                break;
            case ASSISTANT_NAME:
                setAssistantName((String) value);
                break;
            case TELEPHONE_CAR:
                setTelephoneCar((String) value);
                break;
            case POSITION:
                setPosition((String) value);
                break;
            case TELEPHONE_PRIMARY:
                setTelephonePrimary((String) value);
                break;
            case SPOUSE_NAME:
                setSpouseName((String) value);
                break;
            case IMAGE_LAST_MODIFIED:
                setImageLastModified((Date) value);
                break;
            case INSTANT_MESSENGER2:
                setInstantMessenger2((String) value);
                break;
            case IMAGE1:
                setImage1((byte[]) value);
                break;
            case TELEPHONE_BUSINESS1:
                setTelephoneBusiness1((String) value);
                break;
            case DISTRIBUTIONLIST:
                setDistributionList((DistributionListEntryObject[]) value);
                break;
            case NUMBER_OF_DISTRIBUTIONLIST:
                setNumberOfDistributionLists(null == value ? 0 : ((Integer) value).intValue());
                break;
            case USE_COUNT:
                setUseCount(null == value ? 0 : ((Integer) value).intValue());
                break;
            case YOMI_FIRST_NAME:
                setYomiFirstName((String) value);
                break;
            case YOMI_LAST_NAME:
                setYomiLastName((String) value);
                break;
            case YOMI_COMPANY:
                setYomiCompany((String) value);
                break;
            case ADDRESS_BUSINESS:
                setAddressBusiness((String) value);
                break;
            case ADDRESS_HOME:
                setAddressHome((String) value);
                break;
            case ADDRESS_OTHER:
                setAddressOther((String) value);
                break;
            case VCARD_ID:
                setVCardId((String) value);
                break;
            default:
                super.set(field, value);

        }
    }

    @Override
    public Object get(final int field) {
        switch (field) {
            case POSTAL_CODE_HOME:
                return getPostalCodeHome();
            case USERFIELD08:
                return getUserField08();
            case CITY_OTHER:
                return getCityOther();
            case USERFIELD09:
                return getUserField09();
            case USERFIELD06:
                return getUserField06();
            case STATE_BUSINESS:
                return getStateBusiness();
            case NUMBER_OF_IMAGES:
                return I(getNumberOfImages());
            case IMAGE1_CONTENT_TYPE:
                return getImageContentType();
            case GIVEN_NAME:
                return getGivenName();
            case ANNIVERSARY:
                return getAnniversary();
            case USERFIELD18:
                return getUserField18();
            case SALES_VOLUME:
                return getSalesVolume();
            case STREET_OTHER:
                return getStreetOther();
            case USERFIELD04:
                return getUserField04();
            case POSTAL_CODE_BUSINESS:
                return getPostalCodeBusiness();
            case TELEPHONE_HOME1:
                return getTelephoneHome1();
            case USERFIELD19:
                return getUserField19();
            case FAX_OTHER:
                return getFaxOther();
            case USERFIELD14:
                return getUserField14();
            case CITY_HOME:
                return getCityHome();
            case USERFIELD07:
                return getUserField07();
            case TITLE:
                return getTitle();
            case TELEPHONE_ASSISTANT:
                return getTelephoneAssistant();
            case FAX_BUSINESS:
                return getFaxBusiness();
            case PROFESSION:
                return getProfession();
            case DEPARTMENT:
                return getDepartment();
            case USERFIELD01:
                return getUserField01();
            case USERFIELD12:
                return getUserField12();
            case TELEPHONE_IP:
                return getTelephoneIP();
            case URL:
                return getURL();
            case NUMBER_OF_EMPLOYEE:
                return getNumberOfEmployee();
            case POSTAL_CODE_OTHER:
                return getPostalCodeOther();
            case USERFIELD10:
                return getUserField10();
            case BIRTHDAY:
                return getBirthday();
            case EMAIL1:
                return getEmail1();
            case STATE_HOME:
                return getStateHome();
            case TELEPHONE_HOME2:
                return getTelephoneHome2();
            case TELEPHONE_TTYTDD:
                return getTelephoneTTYTTD();
            case TELEPHONE_OTHER:
                return getTelephoneOther();
            case COMMERCIAL_REGISTER:
                return getCommercialRegister();
            case COUNTRY_BUSINESS:
                return getCountryBusiness();
            case USERFIELD11:
                return getUserField11();
            case BUSINESS_CATEGORY:
                return getBusinessCategory();
            case CONTEXTID:
                return I(getContextId());
            case STATE_OTHER:
                return getStateOther();
            case INTERNAL_USERID:
                return I(getInternalUserId());
            case CELLULAR_TELEPHONE1:
                return getCellularTelephone1();
            case BRANCHES:
                return getBranches();
            case NOTE:
                return getNote();
            case EMAIL3:
                return getEmail3();
            case CELLULAR_TELEPHONE2:
                return getCellularTelephone2();
            case INSTANT_MESSENGER1:
                return getInstantMessenger1();
            case MANAGER_NAME:
                return getManagerName();
            case TELEPHONE_TELEX:
                return getTelephoneTelex();
            case EMAIL2:
                return getEmail2();
            case EMPLOYEE_TYPE:
                return getEmployeeType();
            case TELEPHONE_RADIO:
                return getTelephoneRadio();
            case NUMBER_OF_CHILDREN:
                return getNumberOfChildren();
            case STREET_BUSINESS:
                return getStreetBusiness();
            case DEFAULT_ADDRESS:
                return I(getDefaultAddress());
            case MARK_AS_DISTRIBUTIONLIST:
                return B(getMarkAsDistribtuionlist());
            case TELEPHONE_ISDN:
                return getTelephoneISDN();
            case FAX_HOME:
                return getFaxHome();
            case MIDDLE_NAME:
                return getMiddleName();
            case USERFIELD13:
                return getUserField13();
            case ROOM_NUMBER:
                return getRoomNumber();
            case MARITAL_STATUS:
                return getMaritalStatus();
            case USERFIELD15:
                return getUserField15();
            case COUNTRY_HOME:
                return getCountryHome();
            case NICKNAME:
                return getNickname();
            case SUR_NAME:
                return getSurName();
            case CITY_BUSINESS:
                return getCityBusiness();
            case USERFIELD20:
                return getUserField20();
            case TELEPHONE_CALLBACK:
                return getTelephoneCallback();
            case USERFIELD17:
                return getUserField17();
            case TELEPHONE_PAGER:
                return getTelephonePager();
            case COUNTRY_OTHER:
                return getCountryOther();
            case TAX_ID:
                return getTaxID();
            case USERFIELD03:
                return getUserField03();
            case TELEPHONE_COMPANY:
                return getTelephoneCompany();
            case SUFFIX:
                return getSuffix();
            case FILE_AS:
                return getFileAs();
            case USERFIELD02:
                return getUserField02();
            case TELEPHONE_BUSINESS2:
                return getTelephoneBusiness2();
            case USERFIELD05:
                return getUserField05();
            case USERFIELD16:
                return getUserField16();
            case INFO:
                return getInfo();
            case COMPANY:
                return getCompany();
            case DISPLAY_NAME:
                return getDisplayName();
            case STREET_HOME:
                return getStreetHome();
            case ASSISTANT_NAME:
                return getAssistantName();
            case TELEPHONE_CAR:
                return getTelephoneCar();
            case POSITION:
                return getPosition();
            case TELEPHONE_PRIMARY:
                return getTelephonePrimary();
            case SPOUSE_NAME:
                return getSpouseName();
            case IMAGE_LAST_MODIFIED:
                return getImageLastModified();
            case INSTANT_MESSENGER2:
                return getInstantMessenger2();
            case IMAGE1:
                return getImage1();
            case TELEPHONE_BUSINESS1:
                return getTelephoneBusiness1();
            case DISTRIBUTIONLIST:
                return getDistributionList();
            case NUMBER_OF_DISTRIBUTIONLIST:
                return Integer.valueOf(getNumberOfDistributionLists());
            case USE_COUNT:
                return Integer.valueOf(getUseCount());
            case YOMI_FIRST_NAME:
                return getYomiFirstName();
            case YOMI_LAST_NAME:
                return getYomiLastName();
            case YOMI_COMPANY:
                return getYomiCompany();
            case ADDRESS_BUSINESS:
                return getAddressBusiness();
            case ADDRESS_HOME:
                return getAddressHome();
            case ADDRESS_OTHER:
                return getAddressOther();
            case SPECIAL_SORTING:
                return getSortName();
            case SPECIAL_SORTING_FIRST_NAME:
                return getSortFirstName();
            case VCARD_ID:
                return getVCardId();
            default:
                return super.get(field);

        }
    }

    @Override
    public boolean contains(final int field) {
        switch (field) {
            case POSTAL_CODE_HOME:
                return containsPostalCodeHome();
            case USERFIELD08:
                return containsUserField08();
            case CITY_OTHER:
                return containsCityOther();
            case USERFIELD09:
                return containsUserField09();
            case USERFIELD06:
                return containsUserField06();
            case STATE_BUSINESS:
                return containsStateBusiness();
            case IMAGE1_CONTENT_TYPE:
                return containsImageContentType();
            case GIVEN_NAME:
                return containsGivenName();
            case ANNIVERSARY:
                return containsAnniversary();
            case USERFIELD18:
                return containsUserField18();
            case SALES_VOLUME:
                return containsSalesVolume();
            case STREET_OTHER:
                return containsStreetOther();
            case USERFIELD04:
                return containsUserField04();
            case POSTAL_CODE_BUSINESS:
                return containsPostalCodeBusiness();
            case TELEPHONE_HOME1:
                return containsTelephoneHome1();
            case USERFIELD19:
                return containsUserField19();
            case FAX_OTHER:
                return containsFaxOther();
            case USERFIELD14:
                return containsUserField14();
            case CITY_HOME:
                return containsCityHome();
            case USERFIELD07:
                return containsUserField07();
            case TITLE:
                return containsTitle();
            case TELEPHONE_ASSISTANT:
                return containsTelephoneAssistant();
            case FAX_BUSINESS:
                return containsFaxBusiness();
            case PROFESSION:
                return containsProfession();
            case DEPARTMENT:
                return containsDepartment();
            case USERFIELD01:
                return containsUserField01();
            case USERFIELD12:
                return containsUserField12();
            case TELEPHONE_IP:
                return containsTelephoneIP();
            case URL:
                return containsURL();
            case NUMBER_OF_EMPLOYEE:
                return containsNumberOfEmployee();
            case POSTAL_CODE_OTHER:
                return containsPostalCodeOther();
            case USERFIELD10:
                return containsUserField10();
            case BIRTHDAY:
                return containsBirthday();
            case EMAIL1:
                return containsEmail1();
            case STATE_HOME:
                return containsStateHome();
            case TELEPHONE_HOME2:
                return containsTelephoneHome2();
            case TELEPHONE_TTYTDD:
                return containsTelephoneTTYTTD();
            case TELEPHONE_OTHER:
                return containsTelephoneOther();
            case COMMERCIAL_REGISTER:
                return containsCommercialRegister();
            case COUNTRY_BUSINESS:
                return containsCountryBusiness();
            case USERFIELD11:
                return containsUserField11();
            case BUSINESS_CATEGORY:
                return containsBusinessCategory();
            case CONTEXTID:
                return containsContextId();
            case STATE_OTHER:
                return containsStateOther();
            case INTERNAL_USERID:
                return containsInternalUserId();
            case CELLULAR_TELEPHONE1:
                return containsCellularTelephone1();
            case BRANCHES:
                return containsBranches();
            case NOTE:
                return containsNote();
            case EMAIL3:
                return containsEmail3();
            case CELLULAR_TELEPHONE2:
                return containsCellularTelephone2();
            case INSTANT_MESSENGER1:
                return containsInstantMessenger1();
            case MANAGER_NAME:
                return containsManagerName();
            case TELEPHONE_TELEX:
                return containsTelephoneTelex();
            case EMAIL2:
                return containsEmail2();
            case EMPLOYEE_TYPE:
                return containsEmployeeType();
            case TELEPHONE_RADIO:
                return containsTelephoneRadio();
            case NUMBER_OF_CHILDREN:
                return containsNumberOfChildren();
            case STREET_BUSINESS:
                return containsStreetBusiness();
            case DEFAULT_ADDRESS:
                return containsDefaultAddress();
            case MARK_AS_DISTRIBUTIONLIST:
                return containsMarkAsDistributionlist();
            case TELEPHONE_ISDN:
                return containsTelephoneISDN();
            case FAX_HOME:
                return containsFaxHome();
            case MIDDLE_NAME:
                return containsMiddleName();
            case USERFIELD13:
                return containsUserField13();
            case ROOM_NUMBER:
                return containsRoomNumber();
            case MARITAL_STATUS:
                return containsMaritalStatus();
            case USERFIELD15:
                return containsUserField15();
            case COUNTRY_HOME:
                return containsCountryHome();
            case NICKNAME:
                return containsNickname();
            case SUR_NAME:
                return containsSurName();
            case CITY_BUSINESS:
                return containsCityBusiness();
            case USERFIELD20:
                return containsUserField20();
            case TELEPHONE_CALLBACK:
                return containsTelephoneCallback();
            case USERFIELD17:
                return containsUserField17();
            case TELEPHONE_PAGER:
                return containsTelephonePager();
            case COUNTRY_OTHER:
                return containsCountryOther();
            case TAX_ID:
                return containsTaxID();
            case USERFIELD03:
                return containsUserField03();
            case TELEPHONE_COMPANY:
                return containsTelephoneCompany();
            case SUFFIX:
                return containsSuffix();
            case FILE_AS:
                return containsFileAs();
            case USERFIELD02:
                return containsUserField02();
            case TELEPHONE_BUSINESS2:
                return containsTelephoneBusiness2();
            case USERFIELD05:
                return containsUserField05();
            case USERFIELD16:
                return containsUserField16();
            case INFO:
                return containsInfo();
            case COMPANY:
                return containsCompany();
            case DISPLAY_NAME:
                return containsDisplayName();
            case STREET_HOME:
                return containsStreetHome();
            case ASSISTANT_NAME:
                return containsAssistantName();
            case TELEPHONE_CAR:
                return containsTelephoneCar();
            case POSITION:
                return containsPosition();
            case TELEPHONE_PRIMARY:
                return containsTelephonePrimary();
            case SPOUSE_NAME:
                return containsSpouseName();
            case IMAGE_LAST_MODIFIED:
                return containsImageLastModified();
            case INSTANT_MESSENGER2:
                return containsInstantMessenger2();
            case IMAGE1:
                return containsImage1();
            case TELEPHONE_BUSINESS1:
                return containsTelephoneBusiness1();
            case DISTRIBUTIONLIST:
                return containsDistributionLists();
            case NUMBER_OF_DISTRIBUTIONLIST:
                return containsNumberOfDistributionLists();
            case NUMBER_OF_IMAGES:
                return containsImage1();
            case USE_COUNT:
                return containsUseCount();
            case YOMI_FIRST_NAME:
                return containsYomiFirstName();
            case YOMI_LAST_NAME:
                return containsYomiLastName();
            case YOMI_COMPANY:
                return containsYomiCompany();
            case ADDRESS_BUSINESS:
                return containsAddressBusiness();
            case ADDRESS_HOME:
                return containsAddressHome();
            case ADDRESS_OTHER:
                return containsAddressOther();
            case VCARD_ID:
                return containsVCardId();
            default:
                return super.contains(field);

        }
    }

    @Override
    public void remove(final int field) {
        switch (field) {
            case POSTAL_CODE_HOME:
                removePostalCodeHome();
                break;
            case USERFIELD08:
                removeUserField08();
                break;
            case CITY_OTHER:
                removeCityOther();
                break;
            case USERFIELD09:
                removeUserField09();
                break;
            case USERFIELD06:
                removeUserField06();
                break;
            case STATE_BUSINESS:
                removeStateBusiness();
                break;
            case IMAGE1_CONTENT_TYPE:
                removeImageContentType();
                break;
            case GIVEN_NAME:
                removeGivenName();
                break;
            case ANNIVERSARY:
                removeAnniversary();
                break;
            case USERFIELD18:
                removeUserField18();
                break;
            case SALES_VOLUME:
                removeSalesVolume();
                break;
            case STREET_OTHER:
                removeStreetOther();
                break;
            case USERFIELD04:
                removeUserField04();
                break;
            case POSTAL_CODE_BUSINESS:
                removePostalCodeBusiness();
                break;
            case TELEPHONE_HOME1:
                removeTelephoneHome1();
                break;
            case USERFIELD19:
                removeUserField19();
                break;
            case FAX_OTHER:
                removeFaxOther();
                break;
            case USERFIELD14:
                removeUserField14();
                break;
            case CITY_HOME:
                removeCityHome();
                break;
            case USERFIELD07:
                removeUserField07();
                break;
            case TITLE:
                removeTitle();
                break;
            case TELEPHONE_ASSISTANT:
                removeTelephoneAssistant();
                break;
            case FAX_BUSINESS:
                removeFaxBusiness();
                break;
            case PROFESSION:
                removeProfession();
                break;
            case DEPARTMENT:
                removeDepartment();
                break;
            case USERFIELD01:
                removeUserField01();
                break;
            case USERFIELD12:
                removeUserField12();
                break;
            case TELEPHONE_IP:
                removeTelephoneIP();
                break;
            case URL:
                removeURL();
                break;
            case NUMBER_OF_EMPLOYEE:
                removeNumberOfEmployee();
                break;
            case POSTAL_CODE_OTHER:
                removePostalCodeOther();
                break;
            case USERFIELD10:
                removeUserField10();
                break;
            case BIRTHDAY:
                removeBirthday();
                break;
            case EMAIL1:
                removeEmail1();
                break;
            case STATE_HOME:
                removeStateHome();
                break;
            case TELEPHONE_HOME2:
                removeTelephoneHome2();
                break;
            case TELEPHONE_TTYTDD:
                removeTelephoneTTYTTD();
                break;
            case TELEPHONE_OTHER:
                removeTelephoneOther();
                break;
            case COMMERCIAL_REGISTER:
                removeCommercialRegister();
                break;
            case COUNTRY_BUSINESS:
                removeCountryBusiness();
                break;
            case USERFIELD11:
                removeUserField11();
                break;
            case BUSINESS_CATEGORY:
                removeBusinessCategory();
                break;
            case CONTEXTID:
                removeContextID();
                break;
            case STATE_OTHER:
                removeStateOther();
                break;
            case INTERNAL_USERID:
                removeInternalUserId();
                break;
            case CELLULAR_TELEPHONE1:
                removeCellularTelephone1();
                break;
            case BRANCHES:
                removeBranches();
                break;
            case NOTE:
                removeNote();
                break;
            case EMAIL3:
                removeEmail3();
                break;
            case CELLULAR_TELEPHONE2:
                removeCellularTelephone2();
                break;
            case INSTANT_MESSENGER1:
                removeInstantMessenger1();
                break;
            case MANAGER_NAME:
                removeManagerName();
                break;
            case TELEPHONE_TELEX:
                removeTelephoneTelex();
                break;
            case EMAIL2:
                removeEmail2();
                break;
            case EMPLOYEE_TYPE:
                removeEmployeeType();
                break;
            case TELEPHONE_RADIO:
                removeTelephoneRadio();
                break;
            case NUMBER_OF_CHILDREN:
                removeNumberOfChildren();
                break;
            case STREET_BUSINESS:
                removeStreetBusiness();
                break;
            case DEFAULT_ADDRESS:
                removeDefaultAddress();
                break;
            case MARK_AS_DISTRIBUTIONLIST:
                removeMarkAsDistributionlist();
                break;
            case TELEPHONE_ISDN:
                removeTelephoneISDN();
                break;
            case FAX_HOME:
                removeFaxHome();
                break;
            case MIDDLE_NAME:
                removeMiddleName();
                break;
            case USERFIELD13:
                removeUserField13();
                break;
            case ROOM_NUMBER:
                removeRoomNumber();
                break;
            case MARITAL_STATUS:
                removeMaritalStatus();
                break;
            case USERFIELD15:
                removeUserField15();
                break;
            case COUNTRY_HOME:
                removeCountryHome();
                break;
            case NICKNAME:
                removeNickname();
                break;
            case SUR_NAME:
                removeSurName();
                break;
            case CITY_BUSINESS:
                removeCityBusiness();
                break;
            case USERFIELD20:
                removeUserField20();
                break;
            case TELEPHONE_CALLBACK:
                removeTelephoneCallback();
                break;
            case USERFIELD17:
                removeUserField17();
                break;
            case TELEPHONE_PAGER:
                removeTelephonePager();
                break;
            case COUNTRY_OTHER:
                removeCountryOther();
                break;
            case TAX_ID:
                removeTaxID();
                break;
            case USERFIELD03:
                removeUserField03();
                break;
            case TELEPHONE_COMPANY:
                removeTelephoneCompany();
                break;
            case SUFFIX:
                removeSuffix();
                break;
            case USERFIELD02:
                removeUserField02();
                break;
            case TELEPHONE_BUSINESS2:
                removeTelephoneBusiness2();
                break;
            case USERFIELD05:
                removeUserField05();
                break;
            case USERFIELD16:
                removeUserField16();
                break;
            case INFO:
                removeInfo();
                break;
            case COMPANY:
                removeCompany();
                break;
            case DISPLAY_NAME:
                removeDisplayName();
                break;
            case STREET_HOME:
                removeStreetHome();
                break;
            case ASSISTANT_NAME:
                removeAssistantName();
                break;
            case TELEPHONE_CAR:
                removeTelephoneCar();
                break;
            case POSITION:
                removePosition();
                break;
            case TELEPHONE_PRIMARY:
                removeTelephonePrimary();
                break;
            case SPOUSE_NAME:
                removeSpouseName();
                break;
            case INSTANT_MESSENGER2:
                removeInstantMessenger2();
                break;
            case IMAGE1:
                removeImage1();
                break;
            case TELEPHONE_BUSINESS1:
                removeTelephoneBusiness1();
                break;
            case FILE_AS:
                removeFileAs();
                break;
            case IMAGE_LAST_MODIFIED:
                removeImageLastModified();
                break;
            case DISTRIBUTIONLIST:
                removeDistributionLists();
                break;
            case NUMBER_OF_DISTRIBUTIONLIST:
                removeNumberOfDistributionLists();
                break;
            case USE_COUNT:
                removeUseCount();
                break;
            case YOMI_FIRST_NAME:
                removeYomiFirstName();
                break;
            case YOMI_LAST_NAME:
                removeYomiLastName();
                break;
            case YOMI_COMPANY:
                removeYomiCompany();
                break;
            case ADDRESS_BUSINESS:
                removeAddressBusiness();
                break;
            case ADDRESS_HOME:
                removeAddressHome();
                break;
            case ADDRESS_OTHER:
                removeAddressOther();
                break;
            case VCARD_ID:
                removeVCardId();
                break;
            default:
                super.remove(field);

        }
    }

    public boolean canFormDisplayName() {
        return contains(DISPLAY_NAME) || contains(SUR_NAME) || contains(GIVEN_NAME) || contains(EMAIL1) || contains(EMAIL2) || contains(EMAIL3) || contains(COMPANY) || contains(NICKNAME) || contains(MIDDLE_NAME);
    }

    @Override
    public String toString() {
        final StringBuilder name = new StringBuilder(32);
        if (containsTitle()) {
            name.append(getTitle());
            name.append(' ');
        }
        if (containsYomiFirstName()) {
            name.append(getYomiFirstName());
            name.append(' ');
        } else if (containsGivenName()) {
            name.append(getGivenName());
            name.append(' ');
        }
        if (containsMiddleName()) {
            name.append(getMiddleName());
            name.append(' ');
        }
        if (containsYomiLastName()) {
            name.append(getYomiLastName());
            name.append(' ');
        } else if (containsSurName()) {
            name.append(getSurName());
            name.append(' ');
        }
        if (containsSuffix()) {
            name.append(getSuffix());
            name.append(' ');
        }
        if (containsEmail1()) {
            if (name.length() > 0) {
                name.append('(');
                name.append(getEmail1());
                name.append(") ");
            } else {
                name.append(getEmail1());
            }
        }
        // final preparations
        if (name.length() == 0 && containsDisplayName()) {
            name.append(getDisplayName());
        }
        name.insert(0, "] ");
        if (containsId()) {
            name.insert(0, getId());
        } else if (containsObjectID()) {
            name.insert(0, getObjectID());
        } else {
            name.insert(0, "new");
        }
        name.insert(0, "[");
        return name.toString();
    }

    @Override
    public Contact clone() {
        final Contact clone = new Contact();
        for (final ContactField field : ContactField.values()) {
            final int fieldNum = field.getNumber();
            if (!field.isVirtual() && contains(fieldNum)) {
                clone.set(fieldNum, get(fieldNum));
            }
        }
        if (containsId()) {
            clone.setId(this.id);
        }
        if (containsFolderId()) {
            clone.setFolderId(this.folderId);
        }
        return clone;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        for (int col : Contact.ALL_COLUMNS) {
            if (contains(col)) {
                Object object = get(col);
                if (null != object) {
                    result = prime * result + object.hashCode();
                } else {
                    result = prime * result;
                }
            }
        }
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contact other = (Contact) obj;
        return matches(other, ALL_COLUMNS);
    }

    public boolean equalsContentwise(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contact other = (Contact) obj;
        return matches(other, CONTENT_COLUMNS);
    }

    public boolean matches(final Contact other, final int[] fields) {
        for (final int col : fields) {
            if (!contains(col) && other.contains(col)) {
                return false;
            }
            if (contains(col) && !other.contains(col)) {
                return false;
            }
            if (contains(col) && other.contains(col)) {
                final Object thisValue = get(col);
                final Object otherValue = other.get(col);
                if (thisValue == null) {
                    if (otherValue != null) {
                        return false;
                    }
                    continue;
                }
                if (!thisValue.equals(otherValue)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Gets a string suitable to sort contacts (as used when ordering contact results via {@link Contact#SPECIAL_SORTING}), using no
     * specific locale.
     *
     * @return The sort name
     */
    public String getSortName() {
        return getSortName(null);
    }

    /**
     * Gets a string suitable to sort contacts (as used when ordering contact results via {@link Contact#SPECIAL_SORTING}).
     *
     * @param locale The locale to consider, or <code>null</code> to fallback to the default sort name
     * @return The sort name
     */
    public String getSortName(Locale locale) {
        return getSortName(locale, true);
    }

    /**
     * Gets a string suitable to sort contacts preferring the first name (as used when ordering contact results via {@link Contact#SPECIAL_SORTING_FIRST_NAME}), using no
     * specific locale.
     *
     * @return The sort first name
     */
    public String getSortFirstName() {
        return getSortFirstName(null);
    }

    /**
     * Gets a string suitable to sort contacts preferring the first name (as used when ordering contact results via {@link Contact#SPECIAL_SORTING_FIRST_NAME}).
     *
     * @param locale The locale to consider, or <code>null</code> to fallback to the default sort name
     * @return The sort first name
     */
    public String getSortFirstName(Locale locale) {
        return getSortName(locale, false);

    }

    /**
     * Gets the sort name depending on the locale and the sort by first name or last name.
     *
     * @param locale The locale to consider, or <code>null</code> to fallback to the default sort name
     * @param sortByLastName <code>true</code> if sorting should be by last name, <code>false</code> if it should be by first name
     * @return The sort name.
     */
    private String getSortName(Locale locale, boolean sortByLastName) {
        /*
         * prefer display name for distribution lists
         */
        if (getMarkAsDistribtuionlist()) {
            String sortName = getDisplayName();
            if (Strings.isEmpty(sortName)) {
                sortName = sortByLastName ? getSurName() : getGivenName();
                if (Strings.isEmpty(sortName)) {
                    sortName = ""; // empty
                }
            }
            return sortName;
        }

        /*
         * for contacts, use last- and firstname, preferring the yomi names if suitable
         */
        boolean preferYomiFields = null != locale && Locale.JAPANESE.getLanguage().equals(locale.getLanguage());
        {
            String[] names;
            if (sortByLastName) {
                if (preferYomiFields) {
                    names = new String[] { getYomiLastName(), getYomiFirstName(), getSurName(), getGivenName() };
                } else {
                    names = new String[] { getSurName(), getGivenName(), getYomiLastName(), getYomiFirstName() };
                }
            } else {
                if (preferYomiFields) {
                    names = new String[] { getYomiFirstName(), getYomiLastName(), getGivenName(), getSurName(), };
                } else {
                    names = new String[] { getGivenName(), getSurName(), getYomiFirstName(), getYomiLastName() };
                }
            }
            String sortName = concatenateNonEmptyValues(names);
            if (!sortName.isEmpty()) {
                return sortName;
            }
        }
        /*
         * otherwise, use first non-empty value in displayname, (yomi)company, email1, email2
         */
        return getSortNameFallback(preferYomiFields);
    }

    /**
     * Concatenates non empty values.
     *
     * @param names The names to concatenate.
     * @return The concatenated names or an empty string if all names are empty.
     */
    private String concatenateNonEmptyValues(String[] names) {
        StringBuilder stringBuilder = new StringBuilder(64);
        for (String value : names) {
            if (Strings.isNotEmpty(value)) {
                if (0 < stringBuilder.length()) {
                    stringBuilder.append('_');
                }
                stringBuilder.append(value);
            }
        }
        if (0 < Strings.trim(stringBuilder).length()) {
            return stringBuilder.toString();
        }
        return "";
    }

    /**
     * Gets a fallback sort name, using the first non-empty value in displayname, (yomi)company, email1, email2.
     *
     * @param preferYomiFields <code>true</code> if YOMI fields should be preferred, <code>false</code> otherwise.
     * @return The fallback sort name.
     */
    private String getSortNameFallback(boolean preferYomiFields) {
        /*
         * use first non-empty value in displayname, (yomi)company, email1, email2
         */
        int[] columns;
        if (preferYomiFields) {
            columns = new int[] { DISPLAY_NAME, YOMI_COMPANY, COMPANY, EMAIL1, EMAIL2 };
        } else {
            columns = new int[] { DISPLAY_NAME, COMPANY, YOMI_COMPANY, EMAIL1, EMAIL2 };
        }
        for (int column : columns) {
            String value = (String) get(column);
            if (Strings.isNotEmpty(value)) {
                return value;
            }
        }
        /*
         * fallback to empty sortname
         */
        return "";
    }
}
