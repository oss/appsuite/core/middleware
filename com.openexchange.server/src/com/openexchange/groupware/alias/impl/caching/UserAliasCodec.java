/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.groupware.alias.impl.caching;

import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import com.google.common.collect.ImmutableSet;
import com.openexchange.cache.v2.codec.json.AbstractJSONArrayCacheValueCodec;

/**
 * {@link UserAliasCodec} is a codec for user alias sets
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class UserAliasCodec extends AbstractJSONArrayCacheValueCodec<Set<String>> {

    private static final UUID ID = UUID.fromString("110c62c0-338a-47bf-902f-b8f7754a992b");

    @Override
    public UUID getCodecId() {
        return ID;
    }

    @Override
    protected JSONArray writeJson(Set<String> value) throws Exception {
        return JSONArray.jsonArrayFor(value);
    }

    @Override
    protected Set<String> parseJson(JSONArray jObject) throws Exception {
        ImmutableSet.Builder<String> set = ImmutableSet.builderWithExpectedSize(jObject.length());
        for (Object object : jObject) {
            set.add(object.toString());
        }
        return set.build();
    }

}
