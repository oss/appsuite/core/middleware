/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.alias.impl;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.google.common.collect.ImmutableSet;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.database.DatabaseConnectionListeners;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.alias.UserAliasStorage;
import com.openexchange.groupware.alias.impl.caching.UserAliasCodec;
import com.openexchange.java.CollectorUtils;
import com.openexchange.server.services.ServerServiceRegistry;
import gnu.trove.iterator.TIntObjectIterator;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;

/**
 * {@link CachingAliasStorage}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.0
 */
public class CachingAliasStorage implements UserAliasStorage {

    private static final CacheOptions<Set<String>> USER_ALIAS_CACHE_OPTIONS = CacheOptions.<Set<String>> builder()
                                                                                          .withCoreModuleName(CoreModuleName.ALIAS)
                                                                                          .withCodecAndVersion(new UserAliasCodec())
                                                                                          .build();

    static CacheKey newCacheKey(Cache<Set<String>> cache, int contextId, int userId) {
        return cache.newKey(asHashPart(contextId), Integer.toString(userId));
    }

    // -----------------------------------------------------------------------------------------------------------------------------

    /** Proxy attribute for the object implementing the persistent methods. */
    private final RdbAliasStorage delegate;

    /**
     * Initializes a new {@link CachingAliasStorage}.
     */
    public CachingAliasStorage(RdbAliasStorage delegate) {
        super();
        this.delegate = delegate;
    }

    @Override
    public void invalidateAliases(int contextId, int userId) throws OXException {
        CacheService cacheService = ServerServiceRegistry.getInstance().getService(CacheService.class);
        if (null == cacheService) {
            return;
        }
        Cache<Set<String>> cache = cacheService.getCache(USER_ALIAS_CACHE_OPTIONS);
        cache.invalidate(newCacheKey(cache, contextId, userId));
    }

    @Override
    public Set<String> getAliases(int contextId) throws OXException {
        return delegate.getAliases(contextId);
    }

    @Override
    public Set<String> getAliases(int contextId, int userId) throws OXException {
        CacheService cacheService = ServerServiceRegistry.getInstance().getService(CacheService.class);
        if (cacheService == null) {
            return delegate.getAliases(contextId, userId);
        }

        Cache<Set<String>> cache = cacheService.getCache(USER_ALIAS_CACHE_OPTIONS);
        CacheKey key = newCacheKey(cache, contextId, userId);
        return cache.get(key, k -> delegate.getAliases(contextId, userId));
    }

    @Override
    public List<Set<String>> getAliases(int contextId, int... userIds) throws OXException {
        if (null == userIds) {
            return Collections.emptyList();
        }

        int length = userIds.length;
        if (length == 0) {
            return Collections.emptyList();
        }

        if (1 == length) {
            return Collections.<Set<String>> singletonList(getAliases(contextId, userIds[0]));
        }

        CacheService cacheService = ServerServiceRegistry.getInstance().getService(CacheService.class);
        if (cacheService == null) {
            return delegate.getAliases(contextId, userIds);
        }

        Cache<Set<String>> cache = cacheService.getCache(USER_ALIAS_CACHE_OPTIONS);
        TIntObjectMap<Set<String>> user2Aliases = new TIntObjectHashMap<>(length);
        TIntList toLoad = new TIntArrayList(length);

        TObjectIntMap<CacheKey> key2userId = new TObjectIntHashMap<>(length);
        List<CacheKey> keys = Arrays.stream(userIds).mapToObj(userId -> createAndPutCacheKey(userId, contextId, key2userId, cache)).collect(CollectorUtils.toList(length));
        for (CacheKeyValue<Set<String>> cacheKeyValue : cache.mget(keys)) {
            Set<String> cachedAliases = cacheKeyValue.getValueOrElse(null);
            int userId = key2userId.get(cacheKeyValue.getKey());
            if (cachedAliases != null) {
                user2Aliases.put(userId, cachedAliases);
            } else {
                toLoad.add(userId);
            }
        }
        keys = null;

        if (!toLoad.isEmpty()) {
            TIntObjectMap<ImmutableSet<String>> loaded = delegate.getAliasesMapping(contextId, toLoad.toArray());
            user2Aliases.putAll(loaded);

            Map<CacheKey, Set<String>> values = HashMap.newHashMap(loaded.size());
            TIntObjectIterator<ImmutableSet<String>> iterator = loaded.iterator();
            for (int i = loaded.size(); i-- > 0;) {
                iterator.advance();
                values.put(newCacheKey(cache, contextId, iterator.key()), iterator.value());
            }
            cache.mput(values);
            toLoad = null;
        }

        List<Set<String>> list = new ArrayList<>(length);
        for (int userId : userIds) {
            list.add(user2Aliases.get(userId));
        }
        return list;
    }

    private static CacheKey createAndPutCacheKey(int userId, int contextId, TObjectIntMap<CacheKey> key2userId, Cache<Set<String>> cache) {
        CacheKey key = newCacheKey(cache, contextId, userId);
        key2userId.put(key, userId); // Remember user identifier association
        return key;
    }

    @Override
    public int getUserId(int contextId, String alias) throws OXException {
        return delegate.getUserId(contextId, alias);
    }

    @Override
    public void setAliases(Connection con, int contextId, int userId, Set<String> aliases) throws OXException {
        delegate.setAndGetAliases(con, contextId, userId, aliases);
        DatabaseConnectionListeners.addAfterCommitCallbackElseExecute(con, c -> invalidateAliases(contextId, userId));
    }

    @Override
    public boolean createAlias(Connection con, int contextId, int userId, String alias) throws OXException {
        boolean success = delegate.createAlias(con, contextId, userId, alias);
        if (false == success) {
            return false;
        }
        DatabaseConnectionListeners.addAfterCommitCallbackElseExecute(con, c -> invalidateAliases(contextId, userId));
        return true;
    }

    @Override
    public boolean updateAlias(Connection con, int contextId, int userId, String oldAlias, String newAlias) throws OXException {
        boolean success = delegate.updateAlias(con, contextId, userId, oldAlias, newAlias);
        if (false == success) {
            return false;
        }
        DatabaseConnectionListeners.addAfterCommitCallbackElseExecute(con, c -> invalidateAliases(contextId, userId));
        return true;
    }

    @Override
    public boolean deleteAlias(Connection con, int contextId, int userId, String alias) throws OXException {
        boolean success = delegate.deleteAlias(con, contextId, userId, alias);
        if (false == success) {
            return false;
        }
        DatabaseConnectionListeners.addAfterCommitCallbackElseExecute(con, c -> invalidateAliases(contextId, userId));
        return success;
    }

    @Override
    public boolean deleteAliases(Connection con, int contextId, int userId) throws OXException {
        boolean success = delegate.deleteAliases(con, contextId, userId);
        if (false == success) {
            return false;
        }
        DatabaseConnectionListeners.addAfterCommitCallbackElseExecute(con, c -> invalidateAliases(contextId, userId));
        return true;
    }

    @Override
    public List<Integer> getUserIdsByAliasDomain(int contextId, String domain) throws OXException {
        return delegate.getUserIdsByAliasDomain(contextId, domain);
    }

}
