/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.notify.hostname.internal;

import com.openexchange.groupware.notify.hostname.HostData;

/**
 * {@link HostDataImpl} - The {@link HostData} implementation.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class HostDataImpl implements HostData {

    private String host;
    private int port;
    private boolean secure;
    private final String dispatcherPrefix;

    /**
     * Initializes a new {@link HostDataImpl}.
     *
     * @param secure Whether a secure connection was established
     * @param host The name of the host the client connected to
     * @param port The number of the port the client connected to
     * @param dispatcherPrefix The dispatcher prefix
     */
    public HostDataImpl(boolean secure, String host, int port, String dispatcherPrefix) {
        super();
        this.secure = secure;
        this.host = host;
        this.port = port;
        this.dispatcherPrefix = dispatcherPrefix;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public int getPort() {
        return port < 0 ? (secure ? 443 : 80) : port;
    }

    @Override
    public boolean isSecure() {
        return secure;
    }

    @Override
    public String getDispatcherPrefix() {
        return dispatcherPrefix;
    }

    /**
     * Sets the host
     *
     * @param host The host to set
     */
    public void setHost(final String host) {
        this.host = host;
    }

    /**
     * Sets the port
     *
     * @param port The port to set
     */
    public void setPort(final int port) {
        this.port = port;
    }

    /**
     * Sets the secure
     *
     * @param secure The secure to set
     */
    public void setSecure(final boolean secure) {
        this.secure = secure;
    }

}
