/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.notify;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.config.ConfigurationService;
import com.openexchange.configuration.ConfigurationExceptionCodes;
import com.openexchange.exception.OXException;
import com.openexchange.server.Initialization;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.tools.conf.AbstractConfig;

/**
 * DEPENDS ON: SystemConfig
 */
public class NotificationConfig extends AbstractConfig implements Initialization {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(NotificationConfig.class);

    public enum NotificationProperty{

        OBJECT_LINK("object_link"),
        FROM_SOURCE("com.openexchange.notification.fromSource");


        private final String name;

        private NotificationProperty(final String name){
            this.name = name;
        }

        public String getName(){
            return name;
        }

    }

    private static final AtomicReference<ConcurrentMap<String, String>> OVERRIDDEN_PROPERTIES = new AtomicReference<>();

    private static final AtomicReference<NotificationConfig> INSTANCE = new AtomicReference<>(new NotificationConfig());

    /**
     * Gets the currently active instance.
     *
     * @return The instance
     */
    public static NotificationConfig getInstance() {
        return INSTANCE.get();
    }

    // --------------------------------------------------------------------------------------------------------------------------------

    @Override
    protected String getPropertyFileName() throws OXException {
        ConfigurationService configService = ServerServiceRegistry.getInstance().getService(ConfigurationService.class);
		final File file = configService.getFileByName("notification.properties");
        final String filename = null == file ? null : file.getPath();
        if (null == filename) {
            throw ConfigurationExceptionCodes.PROPERTY_MISSING.create("notification.properties");
        }
        return filename;
    }

    public static String getProperty(final NotificationProperty prop, final String def) {
        ConcurrentMap<String, String> overriddenProperties = OVERRIDDEN_PROPERTIES.get();
        String overridden = overriddenProperties == null ? null : overriddenProperties.get(prop.name);
        if (overridden != null) {
            return overridden;
        }
    	NotificationConfig instance = INSTANCE.get();
        if (!instance.isPropertiesLoadInternal()) {
            try {
                instance.loadPropertiesInternal();
            } catch (OXException e) {
                LOG.error("", e);
                return def;
            }
        }
        if (!instance.isPropertiesLoadInternal()) {
            return def;
        }
        return instance.getPropertyInternal(prop.getName(), def);
    }

    public static boolean getPropertyAsBoolean(final NotificationProperty prop, final boolean def) { // NOSONARLINT
        String boolVal = getProperty(prop,null);
        return boolVal == null ? def : Boolean.parseBoolean(boolVal);
    }

    public static void override(NotificationProperty prop, String value) {
        ConcurrentMap<String, String> overriddenProperties = OVERRIDDEN_PROPERTIES.get();
    	if (overriddenProperties == null) {
    	    ConcurrentMap<String, String> newOverriddenProperties = new ConcurrentHashMap<String, String>();
    	    overriddenProperties = OVERRIDDEN_PROPERTIES.compareAndExchange(null, newOverriddenProperties);
    	    if (overriddenProperties == null) {
                overriddenProperties = newOverriddenProperties;
            }
    	}
    	overriddenProperties.put(prop.name, value);
    }

    public static void forgetOverrides() {
        OVERRIDDEN_PROPERTIES.set(null);
    }

    @Override
    public void start() throws OXException {
        NotificationConfig instance = INSTANCE.get();
        if (!instance.isPropertiesLoadInternal()) {
            instance.loadPropertiesInternal();
        }
        NotificationPool.getInstance().startup();
    }

    @Override
    public void stop() {
         NotificationPool.getInstance().shutdown();
        INSTANCE.set(new NotificationConfig());
    }

}
