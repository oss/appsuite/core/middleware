/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.contact;

import static com.openexchange.java.Autoboxing.L;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.openexchange.contact.ContactFieldOperand;
import com.openexchange.contact.common.ContactsAccount;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contact.helpers.ContactField;
import com.openexchange.groupware.container.Contact;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.java.util.MsisdnCheck;
import com.openexchange.search.CompositeSearchTerm;
import com.openexchange.search.CompositeSearchTerm.CompositeOperation;
import com.openexchange.search.SearchTerm;
import com.openexchange.search.SingleSearchTerm;
import com.openexchange.search.SingleSearchTerm.SingleOperation;
import com.openexchange.search.internal.operands.ConstantOperand;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.session.Session;
import com.openexchange.tools.id.IDMangler;

/**
 * First start of a utility class for contacts. This class should contain methods that are useful for the complete backend and not only the
 * contacts component.
 *
 * @author <a href="mailto:marcus.klein@open-xchange.com">Marcus Klein</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ContactUtil {

    /**
     * The default contacts account prefix.
     */
    public static final String DEFAULT_ACCOUNT_PREFIX = IDMangler.mangle(ContactsAccount.ID_PREFIX, Integer.toString(ContactsAccount.DEFAULT_ACCOUNT.getAccountId()));

    /** A timestamp in the distant future as substitute for the client timestamp when circumventing concurrent modification checks */
    public static final long DISTANT_FUTURE = Long.MAX_VALUE;

    /**
     * Initializes a new {@link ContactUtil}.
     */
    private ContactUtil() {
        super();
    }

    /**
     * Generates the display name for given contact
     *
     * @param contact The contact
     */
    public static void generateDisplayName(final Contact contact) {
        if (contact.containsDisplayName()) {
            return;
        }
        final boolean hasUsefulGivenName = contact.containsGivenName() && contact.getGivenName() != null && contact.getGivenName().length() > 0;
        final boolean hasUsefulSureName = contact.containsSurName() && contact.getSurName() != null && contact.getSurName().length() > 0;
        if (hasUsefulGivenName || hasUsefulSureName) {
            final StringBuilder sb = new StringBuilder();
            if (hasUsefulSureName) {
                sb.append(contact.getSurName());
            }
            if (hasUsefulGivenName && hasUsefulSureName) {
                sb.append(", ");
            }
            if (hasUsefulGivenName) {
                sb.append(contact.getGivenName());
            }
            contact.setDisplayName(sb.toString());
            return;
        }
        if (contact.containsCompany() && contact.getCompany() != null && contact.getCompany().length() > 0) {
            contact.setDisplayName(contact.getCompany());
            return;
        }
    }

    /**
     * Gathers valid MSISDN telephone numbers for given contact
     *
     * @param contact The contact
     * @return The set providing valid MSISDN number for given contact
     */
    public static Set<String> gatherTelephoneNumbers(final Contact contact) {
        if (null == contact) {
            return Collections.emptySet();
        }
        final Set<String> set = HashSet.newHashSet(20);
        String tmp = contact.getCellularTelephone1();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getCellularTelephone2();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneAssistant();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneBusiness1();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneBusiness2();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneCallback();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneCar();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneCompany();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneHome1();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneHome2();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneIP();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneISDN();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneOther();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephonePager();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephonePrimary();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneRadio();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneTelex();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        tmp = contact.getTelephoneTTYTTD();
        if (MsisdnCheck.checkMsisdn(tmp)) {
            set.add(tmp);
        }
        return set;
    }

    /**
     * Creates an URL to the contact image of the given contact.
     *
     * @param session The user session
     * @param con The contact
     * @return The URL or <code>null</code>
     * @throws OXException If services or parameter are missing
     */
    public static String generateImageUrl(Session session, Contact con) throws OXException {
        if (0 < con.getNumberOfImages() || con.containsImage1() && null != con.getImage1()) {
            Date lastModified = con.getImageLastModified();
            ContactPictureURLService service = ServerServiceRegistry.getInstance().getService(ContactPictureURLService.class, true);
            if (con.containsInternalUserId() && (FolderObject.SYSTEM_LDAP_FOLDER_ID == con.getParentFolderID() || 
                FolderObject.VIRTUAL_GUEST_CONTACT_FOLDER_ID == con.getParentFolderID())) { 
                return service.getUserPictureUrl(con.getInternalUserId(), session, lastModified == null ? null : L(lastModified.getTime()), true);
            }
            return service.getContactPictureUrl(con.getId(true), con.getFolderId(true), session, lastModified == null ? null : L(lastModified.getTime()), true);
        }
        return null;
    }

    /**
     * Initializes a composite search term to match the given email address in one of a contact's email fields.
     * 
     * @param email The email to match
     * @return The search term
     */
    public static CompositeSearchTerm getSearchByEmailTerm(String email) {
        return new CompositeSearchTerm(CompositeOperation.OR)
            .addSearchTerm(getEqualsTerm(ContactField.EMAIL1, email))
            .addSearchTerm(getEqualsTerm(ContactField.EMAIL2, email))
            .addSearchTerm(getEqualsTerm(ContactField.EMAIL3, email))
        ;
    }
    
    /**
     * Constructs a search term to match a certain contact field against the given value.
     *
     * @param field The contact field for the 'column' operator
     * @param value The value for the 'constant' operator
     * @return The search term
     */
    public static <V> SingleSearchTerm getEqualsTerm(ContactField field, V value) {
        return createContactFieldTerm(field, SingleOperation.EQUALS, value);
    }

    /**
     * Constructs a search term to check a certain contact field is <code>null</code>.
     *
     * @param field The contact field for the 'column' operator
     * @return The search term
     */
    public static SingleSearchTerm getIsNullTerm(ContactField field) {
        return new SingleSearchTerm(SingleOperation.ISNULL).addOperand(new ContactFieldOperand(field));
    }

    /**
     * Constructs a search term to match a certain contact field against one of the given values.
     *
     * @param field The contact field for the 'column' operator
     * @param values The possible values for the 'constant' operator
     * @return The search term
     */
    public static <T> SearchTerm<?> getEqualsAnyValueTerm(ContactField field, List<T> values) {
        if (null == values || 0 == values.size()) {
            return null;
        }
        if (1 == values.size()) {
            return createContactFieldTerm(field, SingleOperation.EQUALS, values.get(0));
        }
        CompositeSearchTerm orTerm = new CompositeSearchTerm(CompositeOperation.OR);
        for (T value : values) {
            orTerm.addSearchTerm(createContactFieldTerm(field, SingleOperation.EQUALS, value));
        }
        return orTerm;
    }

    /**
     * Creates a new {@link SingleSearchTerm} using the supplied operation, the contact field as 'column'-, and the constant as
     * 'constant'-type operators.
     *
     * @param field The contact field for the 'column' operator
     * @param operation The operation
     * @param constant The 'constant' operator
     * @return The search term
     */
    public static <T> SingleSearchTerm createContactFieldTerm(ContactField field, SingleOperation operation, T constant) {
        return new SingleSearchTerm(operation).addOperand(new ContactFieldOperand(field)).addOperand(new ConstantOperand<T>(constant));
    }

}
