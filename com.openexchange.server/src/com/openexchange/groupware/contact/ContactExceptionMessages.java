/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.contact;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link ContactExceptionMessages}
 *
 * @author <a href="mailto:marcus.klein@open-xchange.com">Marcus Klein</a>
 */
public class ContactExceptionMessages implements LocalizableStrings {

    public static final String INVALID_EMAIL_DISPLAY = "Invalid E-Mail address: '%s'. Please correct the E-Mail address.";

    public static final String IMAGE_SCALE_PROBLEM_DISPLAY = "Unable to import this contact picture.";

    public static final String NON_CONTACT_FOLDER_DISPLAY = "Folder is not of type Contact.";

    public static final String NO_ACCESS_DISPLAY = "You do not have the appropriate permission to access objects in the folder \"%1$s\".";

    public static final String IMAGE_DOWNSCALE_FAILED_DISPLAY = "Unable to scale image down.";

    public static final String AGGREGATING_CONTACTS_DISABLED_DISPLAY = "This feature has not been enabled";

    public static final String NO_CREATE_DISPLAY = "You do not have the appropriate permission to create objects in the folder \"%1$s\".";

    public static final String LOAD_OLD_CONTACT_FAILED_DISPLAY = "Unable to synchronize the old contact with the new changes.";

    public static final String MARK_PRIVATE_NOT_ALLOWED_DISPLAY = "You are not allowed to mark this contact as private contact.";

    // Somebody else modified the same object just before the actual change should be saved. Actual change is denied and user should refresh
    // his object.
    public static final String OBJECT_HAS_CHANGED_DISPLAY = "An edit conflict occurred. To edit the contact please reload it.";

    public static final String NO_CHANGES_DISPLAY = "No changes found. No update required.";

    public static final String CONTACT_NOT_FOUND_DISPLAY = "Contact \"%1$s\" not found.";

    public static final String IMAGE_BROKEN_DISPLAY = "The image appears to be broken.";

    public static final String DATA_TRUNCATION_DISPLAY = "Some data entered exceeded the field limit. Please shorten the value for \"%1$s\" (limit: %2$s, current: %3$s) and try again.";

    public static final String NOT_VALID_IMAGE_DISPLAY = "The image you tried to attach is not a valid picture. It may be broken or is not a valid file.";

    public static final String FIRST_NAME_MANDATORY_DISPLAY = "Required  value \"first name\" was not supplied.";

    public static final String DISPLAY_NAME_MANDATORY_DISPLAY = "Required  value \"display name\" was not supplied.";

    public static final String NO_PRIVATE_MOVE = "Unable to move this contact because it is marked as private.";

    public static final String DISPLAY_NAME_IN_USE_DISPLAY = "The name you entered is already assigned to another user. Please choose another display name.";

    public static final String BAD_CHARACTER_DISPLAY = "Bad character in field \"%2$s\".";

    public static final String NO_DELETE_PERMISSION_DISPLAY = "You do not have the appropriate permission to delete objects from the folder \"%1$s\".";

    public static final String MIME_TYPE_NOT_DEFINED_DISPLAY = "Mime type is not defined.";

    public static final String PFLAG_IN_PUBLIC_FOLDER_DISPLAY = "Storing a contact with private flag in a shared folder is not allowed.";

    public static final String IMAGE_TOO_LARGE_DISPLAY = "Image size too large. Image size: %1$s. Max. size: %2$s.";

    public static final String NO_PRIMARY_EMAIL_EDIT_DISPLAY = "Primary E-Mail address in system contact must not be edited.";

    public static final String NOT_IN_FOLDER_DISPLAY = "The contact %1$d is not located in folder %2$s (%3$d).";

    public static final String LAST_NAME_MANDATORY_DISPLAY = "Required  value \"last name\" was not supplied.";

    public static final String NO_CHANGE_PERMISSION_DISPLAY = "You are not allowed to modify that contact";

    public static final String EMAIL_MANDATORY_FOR_EXTERNAL_MEMBERS_DISPLAY = "An E-Mail address is mandatory for external distribution list members. Please add a valid E-Mail address to the contact \"%1$s\".";

    public static final String OBJECT_ID_MANDATORY_FOR_REFERENCED_MEMBERS_DISPLAY = "The object identifier is mandatory for distribution list members referencing existing contacts.";

    public static final String NO_USER_CONTACT_DELETE_DISPLAY = "User contacts can not be deleted.";

    public static final String TOO_FEW_ATTACHMENTS_DISPLAY = "Number of documents attached to this contact is below zero. You can not remove any more attachments.";

    public static final String TOO_FEW_ATTRIBUTES_DISPLAY = "Setting %s requires at least a contact and a value.";

    public static final String DATE_CONVERSION_FAILED_DISPLAY = "Given string %1$s could not be converted to a date.";

    public static final String CONV_OBJ_2_DATE_FAILED_DISPLAY = "Could not convert given object %s to a date when setting %s.";

    public static final String CONTACT_OBJECT_MISSING_DISPLAY = "Getting the value of %s requires at least a ContactObject";

    public static final String TOO_FEW_SEARCH_CHARS_DISPLAY = "In order to accomplish the search, %1$d or more characters are required.";

    public static final String INCORRECT_STRING_DISPLAY = "The character \"%1$s\" in field \"%2$s\" can't be saved. Please remove the problematic character and try again.";

    public static final String TOO_MANY_PATTERNS_DISPLAY = "The query \"%1$s\" contains too many patterns. Please shorten the query and try again.";

    public static final String IGNORED_PATTERN_DISPLAY = "The pattern \"%1$s\" has been ignored during search.";

    public static final String SET_DISTRIBUTION_LIST_ERROR_DISPLAY = "Invalid number of properties for distribution list.";

    public static final String SUBSCRIPTION_NOT_ALLOWED = "This operation is not allowed in this folder because it has a subscription.";

    public static final String MISSING_CAPABILITY_MSG = "The operation could not be completed due to missing capabilities.";

    private ContactExceptionMessages() {
        super();
    }
}
