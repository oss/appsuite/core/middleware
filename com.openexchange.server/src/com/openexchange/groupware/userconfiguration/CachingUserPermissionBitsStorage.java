/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.userconfiguration;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.database.DatabaseConnectionListeners.addAfterCommitCallbackElseExecute;
import static com.openexchange.database.Databases.isInTransactionSafe;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import java.sql.Connection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.codec.CacheValueCodec;
import com.openexchange.cache.v2.codec.IntegerCacheValueCodec;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.java.CollectorUtils;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.tools.CacheGetter;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;


/**
 * {@inheritDoc}
 *
 * @author <a href="mailto:francisco.laguna@open-xchange.com">Francisco Laguna</a>
 */
public class CachingUserPermissionBitsStorage extends UserPermissionBitsStorage {

    /**
     * Initializes a new {@link CacheGetter} to retrieve specific user permission bits from cache.
     *
     * @param cacheService A reference to the cache service
     * @param contextId The identifier of the context to get the getter for
     * @param userId The identifier of the user to get the getter for
     * @return The cache getter
     */
    public static CacheGetter<Integer> cacheGetterFor(CacheService cacheService, int contextId, int userId) {
        Cache<Integer> cache = cacheService.getCache(CACHE_USER_PERMISSION_BITS);
        return new CacheGetter<Integer>() {

            @Override
            public CacheKey getKey() {
                return CachingUserPermissionBitsStorage.getKey(cache, contextId, userId);
            }

            @Override
            public CacheValueCodec<?> getCodec() {
                return CACHE_USER_PERMISSION_BITS.getCodec();
            }

            @Override
            public Integer optValue(CacheKeyValue<?> cacheValue) throws OXException {
                return cacheValue != null && cacheValue.getValueOrElse(null) instanceof Integer bits ? bits : null;
            }
        };
    }

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CachingUserPermissionBitsStorage.class);

    /** Options for the user permission bits cache */
    private static final CacheOptions<Integer> CACHE_USER_PERMISSION_BITS = CacheOptions.<Integer> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.USER_PERMISSION_BITS)
        .withCodecAndVersion(IntegerCacheValueCodec.getInstance())
    .build(); // formatter:on

    private final UserPermissionBitsStorage delegateStorage;

    /**
     * Initializes a new {@link CachingUserPermissionBitsStorage}.
     */
    public CachingUserPermissionBitsStorage() {
        super();
        this.delegateStorage = new RdbUserPermissionBitsStorage();
    }

    @Override
    public UserPermissionBits getUserPermissionBits(int userId, Context ctx) throws OXException {
        return getUserPermissionBits(null, userId, ctx);
    }

    @Override
    public UserPermissionBits getUserPermissionBits(Connection con, int userId, Context ctx) throws OXException {
        if (isInTransactionSafe(con)) {
            return delegateStorage.getUserPermissionBits(con, userId, ctx); // bypass cache within active database transaction
        }
        Optional<Cache<Integer>> optCache = optCache();
        if (optCache.isEmpty()) {
            return delegateStorage.getUserPermissionBits(con, userId, ctx);
        }
        Cache<Integer> cache = optCache.get();
        Integer permissionBits = cache.get(getKey(cache, ctx, userId), k -> loadPermissionBits(ctx, userId, con));
        return new UserPermissionBits(i(permissionBits), userId, ctx);
    }

    @Override
    public UserPermissionBits[] getUserPermissionBits(Context ctx, int[] userIds) throws OXException {
        if (null == userIds || 0 == userIds.length) {
            return new UserPermissionBits[0];
        }
        Optional<Cache<Integer>> optCache = optCache();
        if (optCache.isEmpty()) {
            return delegateStorage.getUserPermissionBits(ctx, userIds);
        }
        Cache<Integer> cache = optCache.get();

        int length = userIds.length;
        TIntObjectMap<UserPermissionBits> permissionsBitsByUserId = new TIntObjectHashMap<>(length);
        TIntList toLoad = new TIntArrayList();

        TObjectIntMap<CacheKey> key2userId = new TObjectIntHashMap<>(length);
        List<CacheKey> keys = Arrays.stream(userIds).mapToObj(userId -> createAndPutCacheKey(userId, ctx, key2userId, cache)).collect(CollectorUtils.toList(length));
        for (CacheKeyValue<Integer> cacheKeyValue : cache.mget(keys)) {
            Integer value = cacheKeyValue.getValueOrElse(null);
            int userId = key2userId.get(cacheKeyValue.getKey());
            if (null != value) {
                permissionsBitsByUserId.put(userId, new UserPermissionBits(i(value), userId, ctx));
            } else {
                toLoad.add(userId);
            }
        }
        keys = null;

        if (!toLoad.isEmpty()) {
            int[] userIdsToLoad = toLoad.toArray();
            toLoad = null;
            UserPermissionBits[] loadedPermissionBits = delegateStorage.getUserPermissionBits(ctx, userIdsToLoad);
            Map<CacheKey, Integer> values = HashMap.newHashMap(userIdsToLoad.length);
            for (int i = 0; i < userIdsToLoad.length; i++) {
                UserPermissionBits userPermissionBits = loadedPermissionBits[i];
                if (null != userPermissionBits) {
                    int userId = userIdsToLoad[i];
                    permissionsBitsByUserId.put(userId, userPermissionBits);
                    values.put(getKey(cache, ctx, userId), I(userPermissionBits.getPermissionBits()));
                }
            }
            cache.mput(values);
        }

        UserPermissionBits[] retval = new UserPermissionBits[length];
        for (int i = 0; i < length; i++) {
            retval[i] = permissionsBitsByUserId.get(userIds[i]);
        }
        return retval;
    }

    private static CacheKey createAndPutCacheKey(int userId, Context ctx, TObjectIntMap<CacheKey> key2userId, Cache<Integer> cache) {
        CacheKey cacheKey = getKey(cache, ctx, userId);
        key2userId.put(cacheKey, userId);
        return cacheKey;
    }

    @Override
    public void removeUserPermissionBits(int userId, Context ctx) throws OXException {
        invalidateCache(ctx, userId);
    }

    @Override
    public void saveUserPermissionBits(int permissionBits, int userId, Context ctx) throws OXException {
        delegateStorage.saveUserPermissionBits(permissionBits, userId, ctx);
        removeUserPermissionBits(userId, ctx);
    }

    @Override
    public void saveUserPermissionBits(Connection con, int permissionBits, int userId, Context ctx) throws OXException {
        delegateStorage.saveUserPermissionBits(con, permissionBits, userId, ctx);
        addAfterCommitCallbackElseExecute(con, c -> removeUserPermissionBits(userId, ctx));
    }

    /**
     * Loads the permission bits of a certain user from the underlying storage.
     *
     * @param context The context
     * @param userId The identifier of the user
     * @param optConnection The optional database connection to use, or <code>null</code> if not set
     * @return The permission bits
     * @throws OXException If loading permission bits fails
     */
    private Integer loadPermissionBits(Context context, int userId, Connection optConnection) throws OXException {
        if (null == optConnection) {
            return I(delegateStorage.getUserPermissionBits(userId, context).getPermissionBits());
        }
        return I(delegateStorage.getUserPermissionBits(optConnection, userId, context).getPermissionBits());
    }

    /**
     * Initializes a new cache key for the permission bits of a certain user.
     *
     * @param cache The underlying cache
     * @param contextId The identifier of the context to generate the cache key for
     * @param userId The identifier of the user to generate the cache key for
     * @return The cache key
     */
    private static final CacheKey getKey(Cache<Integer> cache, int contextId, int userId) {
        return cache.newKey(asHashPart(contextId), Integer.toString(userId));
    }

    /**
     * Initializes a new cache key for the permission bits of a certain user.
     *
     * @param cache The underlying cache
     * @param context The context to generate the cache key for
     * @param userId The identifier of the user to generate the cache key for
     * @return The cache key
     */
    private static final CacheKey getKey(Cache<Integer> cache, Context context, int userId) {
        return getKey(cache, context.getContextId(), userId);
    }

    /**
     * Optionally gets the user permission bits cache.
     *
     * @return The cache, or {@link Optional#empty()} if not yet available
     */
    private static Optional<Cache<Integer>> optCache() {
        CacheService cacheService = ServerServiceRegistry.getServize(CacheService.class);
        if (null == cacheService) {
            return Optional.empty();
        }
        return Optional.of(cacheService.getCache(CACHE_USER_PERMISSION_BITS));
    }

    /**
     * Invalidates the cached data of a certain user.
     *
     * @param ctx The context
     * @param userId The identifier of the user to invalidate the cache for
     */
    private static void invalidateCache(Context ctx, int userId) {
        Optional<Cache<Integer>> optCache = optCache();
        if (optCache.isPresent()) {
            Cache<Integer> cache = optCache.get();
            try {
                cache.invalidate(getKey(cache, ctx, userId));
            } catch (OXException e) {
                LOG.warn("Error invalidating cache for user {} in context {}: {}", I(userId), I(ctx.getContextId()), e.getMessage(), e);
            }
        }
    }

}
