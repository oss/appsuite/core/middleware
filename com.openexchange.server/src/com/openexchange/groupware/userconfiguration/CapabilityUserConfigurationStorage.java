/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.userconfiguration;

import java.util.ArrayList;
import java.util.List;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.capabilities.CapabilitySet;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.contexts.impl.ContextStorage;
import com.openexchange.groupware.ldap.UserStorage;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.session.Session;
import com.openexchange.user.User;

/**
 * {@link CapabilityUserConfigurationStorage} - The implementation of a user configuration storage.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class CapabilityUserConfigurationStorage extends UserConfigurationStorage {

    /**
     * Initializes a new {@link CapabilityUserConfigurationStorage}
     */
    public CapabilityUserConfigurationStorage() {
        super();
    }
    @Override
    public UserConfiguration getUserConfiguration(Session session, int[] groups) throws OXException {
        return loadUserConfiguration(session, groups);
    }

    @Override
    public UserConfiguration getUserConfiguration(int userId, int[] groups, Context ctx) throws OXException {
        return loadUserConfiguration(userId, groups, ctx);
    }

    @Override
    public UserConfiguration[] getUserConfiguration(Context ctx, User[] users) throws OXException {
        return loadUserConfiguration(ctx, users);
    }

    /*-
     * ------------- Methods for loading -------------
     */

    /**
     * Loads the user configuration from database specified through user identifier and context
     *
     * @param userId The user identifier
     * @param ctx The context
     * @return the instance of <code>{@link UserConfiguration}</code>
     * @throws OXException If user's groups are <code>null</code> and could not be determined by <code>{@link UserStorage}</code>
     *             implementation
     */
    public static UserConfiguration loadUserConfiguration(int userId, Context ctx) throws OXException {
        return loadUserConfiguration(userId, null, ctx);
    }

    private static CapabilitySet getCapabilities(int userId, int cid) throws OXException {
        CapabilityService capabilityService = ServerServiceRegistry.getInstance().getService(CapabilityService.class, true);
        return capabilityService.getCapabilities(userId, cid);
    }

    private static CapabilitySet getCapabilities(Session session) throws OXException {
        CapabilityService capabilityService = ServerServiceRegistry.getInstance().getService(CapabilityService.class, true);
        return capabilityService.getCapabilities(session);
    }

    /**
     * Loads the user configuration from database specified through user identifier and context
     *
     * @param userId The user identifier
     * @param groupsArg The group identifiers the user belongs to; may be <code>null</code>
     * @param ctx The context
     * @return the instance of <code>{@link UserConfiguration}</code>
     * @throws OXException If user's groups are <code>null</code> and could not be determined by <code>{@link UserStorage}</code>
     *             implementation
     * @throws OXException If a readable connection could not be obtained from connection pool
     * @throws OXException If no matching user configuration is kept in database
     */
    public static UserConfiguration loadUserConfiguration(int userId, int[] groupsArg, Context ctx) throws OXException {
        // Check existence of the user
        User user = UserStorage.getInstance().getUser(userId, ctx);

        int[] groups = groupsArg == null ? user.getGroups() : groupsArg;
        return new UserConfiguration(getCapabilities(userId, ctx.getContextId()), userId, groups, ctx);
    }

    /**
     * Loads the user configuration from database specified through session
     *
     * @param session The session providing use/context information
     * @param groupsArg The group identifiers the user belongs to; may be <code>null</code>
     * @return the instance of <code>{@link UserConfiguration}</code>
     * @throws OXException If user's groups are <code>null</code> and could not be determined by <code>{@link UserStorage}</code> implementation
     * @throws OXException If a readable connection could not be obtained from connection pool
     * @throws OXException If no matching user configuration is kept in database
     */
    public static UserConfiguration loadUserConfiguration(Session session, int[] groupsArg) throws OXException {
        Context ctx = ContextStorage.getStorageContext(session);
        int[] groups = groupsArg == null ? UserStorage.getInstance().getUser(session.getUserId(), ctx).getGroups() : groupsArg;
        return new UserConfiguration(getCapabilities(session), session.getUserId(), groups, ctx);
    }

    public static UserConfiguration[] loadUserConfiguration(Context ctx, User[] users) throws OXException {

        UserConfiguration[] retval = new UserConfiguration[users.length];
        // Here we just assume the users exist
        for (int i = 0; i < users.length; i++) {
            User user = users[i];
            retval[i] = new UserConfiguration(getCapabilities(user.getId(), ctx.getContextId()), user.getId(), user.getGroups(), ctx);
        }

        return retval;
    }

    @Override
    public UserConfiguration[] getUserConfigurations(Context ctx, int[] userIds, int[][] groups) throws OXException {
        if (0 == userIds.length) {
            return new UserConfiguration[0];
        }
        return loadUserConfigurations(ctx, userIds, groups);
    }

    private static UserConfiguration[] loadUserConfigurations(Context ctx, int[] userIds, int[][] groupsArg) throws OXException {
        List<UserConfiguration> list = new ArrayList<UserConfiguration>(userIds.length);
        for (int i = 0; i < userIds.length; i++) {
            int userId = userIds[i];
            int[] groups = groupsArg[i] == null ? UserStorage.getInstance().getUser(userId, ctx).getGroups() : groupsArg[i];
            list.add(new UserConfiguration(getCapabilities(userId, ctx.getContextId()), userId, groups, ctx));
        }
        return list.toArray(new UserConfiguration[userIds.length]);
    }
}
