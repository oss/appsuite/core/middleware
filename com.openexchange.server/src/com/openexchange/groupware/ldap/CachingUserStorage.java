/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.ldap;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.database.DatabaseConnectionListeners.addAfterCommitCallbackElseExecute;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.codec.CacheValueCodec;
import com.openexchange.cache.v2.codec.IntArrayCacheValueCodec;
import com.openexchange.cache.v2.codec.IntegerCacheValueCodec;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.ldap.RdbUserStorage.ValuePair;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Strings;
import com.openexchange.log.LogProperties;
import com.openexchange.password.mechanism.PasswordMech;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.tools.CacheGetter;
import com.openexchange.tools.StringCollection;
import com.openexchange.user.User;
import com.openexchange.user.UserExceptionCode;
import com.openexchange.user.internal.mapping.UserMapper;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;

/**
 * This class implements the user storage using a cache to store once read objects.
 */
public class CachingUserStorage extends UserStorage {

    /**
     * Initializes a new {@link CacheGetter} to retrieve a specific user from cache.
     *
     * @param cacheService A reference to the cache service
     * @param contextId The identifier of the context to get the getter for
     * @param userId The identifier of the user to get the getter for
     * @return The cache getter
     */
    public static CacheGetter<User> cacheGetterFor(CacheService cacheService, int contextId, int userId) {
        Cache<User> cache = cacheService.getCache(CACHE_OPTIONS_USER);
        return new CacheGetter<User>() {

            @Override
            public CacheKey getKey() {
                return CachingUserStorage.getKey(cache, contextId, userId);
            }

            @Override
            public CacheValueCodec<?> getCodec() {
                return CACHE_OPTIONS_USER.getCodec();
            }

            @Override
            public User optValue(CacheKeyValue<?> cacheValue) throws OXException {
                return cacheValue != null && cacheValue.getValueOrElse(null) instanceof User user ? user : null;
            }
        };
    }

    private static final UserMapper MAPPER = new UserMapper();

    /** Options for the cache holding user data */
    private static final CacheOptions<User> CACHE_OPTIONS_USER = CacheOptions.<User> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.USER)
        .withCodecAndVersion(new UserCodec())
    .build(); // formatter:on

    /** Options for the cache mapping login names to user identifiers */
    private static final CacheOptions<Integer> CACHE_OPTIONS_LOGINS = CacheOptions.<Integer> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.USER_LOGINS)
        .withCodecAndVersion(IntegerCacheValueCodec.getInstance())
    .build(); // formatter:on

    /** Options for the cache mapping IMAP login names to arrays of user identifiers */
    private static final CacheOptions<int[]> CACHE_OPTIONS_IMAP_LOGINS = CacheOptions.<int[]> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.USER_IMAP_LOGINS)
        .withCodecAndVersion(IntArrayCacheValueCodec.getInstance())
    .build(); // formatter:on

    /**
     * Proxy attribute for the object implementing the persistent methods.
     */
    private final RdbUserStorage delegate;

    /**
     * Default constructor.
     */
    public CachingUserStorage(RdbUserStorage delegate) {
        super();
        this.delegate = delegate;
    }

    @Override
    public boolean isGuest(int userId, int contextId) throws OXException {
        return getUser(userId, contextId).isGuest();
    }

    @Override
    public boolean isGuest(int userId, Context context) throws OXException {
        return getUser(userId, context).isGuest();
    }

    @Override
    public boolean exists(int userId, int contextId) throws OXException {
        Optional<Cache<User>> optCache = optUserCache();
        if (optCache.isPresent()) {
            Cache<User> cache = optCache.get();
            if (null != cache.get(getKey(cache, contextId, userId))) {
                return true;
            }
        }
        return delegate.exists(userId, contextId);
    }

    @Override
    public User getUser(int userId, Context context) throws OXException {
        Optional<Cache<User>> optCache = optUserCache();
        if (optCache.isEmpty()) {
            return delegate.getUser(userId, context);
        }
        Cache<User> cache = optCache.get();
        return cache.get(getKey(cache, context, userId), k -> delegate.getUser(userId, context));
    }

    @Override
    public User loadIfAbsent(int userId, Context context, Connection con) throws OXException {
        Optional<Cache<User>> optCache = optUserCache();
        if (optCache.isEmpty()) {
            return delegate.loadIfAbsent(userId, context, con);
        }
        Cache<User> cache = optCache.get();
        return cache.get(getKey(cache, context, userId), k -> delegate.loadIfAbsent(userId, context, con));
    }

    @Override
    public int createUser(Connection con, Context context, User user) throws OXException {
        return delegate.createUser(con, context, user);
    }

    @Override
    public void deleteUser(Context context, int userId) throws OXException {
        delegate.deleteUser(context, userId);
        invalidateUser(context, userId);
    }

    @Override
    public void deleteUser( Connection con, Context context, int userId) throws OXException {
        delegate.deleteUser(con, context, userId);
        addAfterCommitCallbackElseExecute(con, c -> invalidateUser(context, userId));
    }

    @Override
    public User getUser(int userId, int contextId) throws OXException {
        Optional<Cache<User>> optCache = optUserCache();
        if (optCache.isEmpty()) {
            return delegate.getUser(userId, contextId);
        }
        Cache<User> cache = optCache.get();
        return cache.get(getKey(cache, contextId, userId), k -> delegate.getUser(userId, contextId));
    }

    @Override
    public User getUser(Context ctx, int userId, Connection con) throws OXException {
        Optional<Cache<User>> optCache = optUserCache();
        if (optCache.isEmpty()) {
            return delegate.getUser(ctx, userId, con);
        }
        Cache<User> cache = optCache.get();
        return cache.get(getKey(cache, ctx, userId), k -> delegate.getUser(ctx, userId, con));
    }

    @Override
    public int[] getUserIds(int contextId) throws OXException {
        return delegate.getUserIds(contextId);
    }

    @Override
    public User[] getUser(Context ctx, boolean includeGuests, boolean excludeUsers) throws OXException {
        return getUser(ctx, listAllUser(null, ctx, includeGuests, excludeUsers));
    }

    @Override
    public User[] getUser(Connection con, Context ctx, boolean includeGuests, boolean excludeUsers) throws OXException {
        return getUser(ctx, listAllUser(con, ctx, includeGuests, excludeUsers), con);
    }

    @Override
    public User[] getUser(Context ctx, int[] userIds, Connection con) throws OXException {
        if (userIds == null) {
            throw new IllegalArgumentException("User identifiers must not be null");
        }

        int length = userIds.length;
        if (length <= 0) {
            return new User[0];
        }

        if (length == 1) {
            return new User[] { null != con ? getUser(ctx, userIds[0], con) : getUser(userIds[0], ctx.getContextId()) };
        }

        Optional<Cache<User>> optCache = optUserCache();
        if (optCache.isEmpty()) {
            return null != con ? delegate.getUser(ctx, userIds, con) : delegate.getUser(ctx, userIds);
        }

        Cache<User> cache = optCache.get();
        TIntObjectMap<User> usersById = new TIntObjectHashMap<>(length);
        TIntList usersToLoad = new TIntArrayList(length);

        TObjectIntMap<CacheKey> key2userId = new TObjectIntHashMap<>(length);
        List<CacheKey> keys = Arrays.stream(userIds).mapToObj(userId -> createAndPutCacheKey(userId, ctx, key2userId, cache)).collect(CollectorUtils.toList(length));
        for (CacheKeyValue<User> cacheKeyValue : cache.mget(keys)) {
            User user = cacheKeyValue.getValueOrElse(null);
            int userId = key2userId.get(cacheKeyValue.getKey());
            if (user != null) {
                usersById.put(userId, user);
            } else {
                usersToLoad.add(userId);
            }
        }
        keys = null;

        if (!usersToLoad.isEmpty()) {
            Map<CacheKey, User> map = HashMap.newHashMap(usersToLoad.size());
            for (User loadedUser : null != con ? delegate.getUser(ctx, usersToLoad.toArray(), con) : delegate.getUser(ctx, usersToLoad.toArray())) {
                usersById.put(loadedUser.getId(), loadedUser);
                map.put(getKey(cache, ctx, loadedUser.getId()), loadedUser);
            }
            cache.mput(map);
            usersToLoad = null;
        }

        User[] users = new User[length];
        for (int i = 0; i < length; i++) {
            users[i] = usersById.get(userIds[i]);
        }
        return users;
    }

    private static CacheKey createAndPutCacheKey(int userId, Context ctx, TObjectIntMap<CacheKey> key2userId, Cache<User> cache) {
        CacheKey key = getKey(cache, ctx, userId);
        key2userId.put(key, userId); // Remember user identifier association
        return key;
    }

    @Override
    public User[] getUser(Context ctx, int[] userIds) throws OXException {
        return getUser(ctx, userIds, null);
    }

    @Override
    public User[] getGuestsCreatedBy(Connection connection, Context context, int userId) throws OXException {
        return getUser(context, RdbUserStorage.getGuestsCreatedById(userId, context, connection), connection);
    }

    @Override
    protected void updateUserInternal(Connection con, User user, Context context) throws OXException {
        // First try to detect some lousy client writing the same values all the time.
        boolean doUpdate = false;
        Optional<Cache<User>> optCache = optUserCache();
        if (optCache.isPresent()) {
            Cache<User> cache = optCache.get();
            User oldUser = cache.get(getKey(cache, context, user.getId()));
            if (null != oldUser) {
                // Okay, we still have that user in the cache. Now compare attributes.
                User differences = MAPPER.getDifferences(oldUser, user);
                doUpdate = MAPPER.getAssignedFields(differences).length != 0;
                // All attributes the same? Then check for changed user attributes.
                if (!doUpdate && null != user.getAttributes()) {
                    final Map<String, String> oldAttributes = oldUser.getAttributes();
                    final Map<String, String> attributes = user.getAttributes();
                    final Map<String, String> added = new HashMap<>();
                    final Map<String, String> removed = new HashMap<>();
                    final Map<String, ValuePair> changed = new HashMap<>();
                    RdbUserStorage.calculateDifferences(oldAttributes, attributes, added, removed, changed);
                    doUpdate = !added.isEmpty() || !removed.isEmpty() || !changed.isEmpty();
                }
                if (!doUpdate && oldUser.isGuest()) {
                    doUpdate |= null != user.getPasswordMech() && !user.getPasswordMech().equals(oldUser.getPasswordMech());
                    doUpdate |= null != user.getUserPassword() && !user.getUserPassword().equals(oldUser.getUserPassword());
                }
            } else {
                doUpdate = true;
            }
        } else {
            doUpdate = true;
        }
        if (doUpdate) {
            // Only update the user in the database if it differs from the one in the cache, the user is not cached or the service is not available.
            if (con == null) {
                delegate.updateUser(user, context);
                invalidateUser(context, user.getId());
            } else {
                delegate.updateUser(con, user, context);
                addAfterCommitCallbackElseExecute(con, c -> invalidateUser(context, user.getId()));
            }
        }
    }

    @Override
    public String getUserAttribute(String name, int userId, Context context) throws OXException {
        Optional<Cache<User>> optCache = optUserCache();
        if (optCache.isEmpty()) {
            return delegate.getUserAttribute(name, userId, context);
        }
        Map<String, String> attributes = getUser(userId, context).getAttributes();
        if (null == attributes || attributes.isEmpty()) {
            return null;
        }
        return attributes.get(new StringBuilder("attr_").append(name).toString());
    }

    @Override
    public void setUserAttribute(String name, String value, int userId, Context context) throws OXException {
        delegate.setUserAttribute(name, value, userId, context);
        invalidateUser(context, userId);
    }

    @Override
    public void setAttribute(String name, String value, int userId, Context context) throws OXException {
        setAttribute(null, name, value, userId, context);
    }

    @Override
    public void setAttribute(Connection con, String name, String value, int userId, Context context) throws OXException {
        setAttribute(con, name, value, userId, context, true);
    }

    @Override
    public void setAttribute(Connection con, String name, String value, int userId, Context context, boolean invalidate) throws OXException {
        if (null == name) {
            throw LdapExceptionCode.UNEXPECTED_ERROR.create("Attribute name is null.").setPrefix("USR");
        }
        if (null == con) {
            delegate.setAttribute(name, value, userId, context);
            invalidateUser(context, userId);
        } else {
            delegate.setAttribute(con, name, value, userId, context);
            addAfterCommitCallbackElseExecute(con, c -> invalidateUser(context, userId));
        }
    }

    @Override
    public int getUserId(String loginInfo, Context context) throws OXException {
        Optional<Cache<Integer>> optCache = optCache(CACHE_OPTIONS_LOGINS);
        if (optCache.isEmpty()) {
            return delegate.getUserId(loginInfo, context);
        }
        Cache<Integer> cache = optCache.get();
        CacheKey key = cache.newKey(asHashPart(context.getContextId()), loginInfo);
        Integer resolvedUserId = cache.get(key, k -> I(delegate.getUserId(loginInfo, context)));
        LogProperties.put(LogProperties.Name.LOGIN_RESOLVED_LOGIN, loginInfo);
        return i(resolvedUserId);
    }

    @Override
    public int[] listModifiedUser(final Date modifiedSince, final Context context) throws OXException {
        // Caching doesn't make any sense here.
        return delegate.listModifiedUser(modifiedSince, context);
    }

    @Override
    public User searchUser(String email, Context context, boolean considerAliases, boolean includeGuests, boolean excludeUsers) throws OXException {
        return delegate.searchUser(email, context, considerAliases, includeGuests, excludeUsers);
    }

    @Override
    public User[] searchUserByMailLogin(String login, Context context) throws OXException {
        if (Strings.isNotEmpty(login) && false == StringCollection.containsWildcards(login)) {
            int[] userIds = resolveIMAPLogin(login, context);
            return getUser(context, userIds);
        }
        // Caching doesn't make any sense here.
        return delegate.searchUserByMailLogin(login, context);
    }

    @Override
    public User[] searchUserByName(final String name, final Context context, final int searchType) throws OXException {
        // Caching doesn't make any sense here.
        return delegate.searchUserByName(name, context, searchType);
    }

    @Override
    public int[] listAllUser(Connection con, Context context, boolean includeGuests, boolean excludeUsers) throws OXException {
        return delegate.listAllUser(con, context, includeGuests, excludeUsers);
    }

    @Override
    public int[] listAllUser(Connection con, int contextID, boolean includeGuests, boolean excludeUsers) throws OXException {
        return delegate.listAllUser(con, contextID, includeGuests, excludeUsers);
    }

    @Override
    public int[] resolveIMAPLogin(String imapLogin, Context context) throws OXException {
        Optional<Cache<int[]>> optCache = optCache(CACHE_OPTIONS_IMAP_LOGINS);
        if (optCache.isEmpty()) {
            return delegate.resolveIMAPLogin(imapLogin, context);
        }
        Cache<int[]> cache = optCache.get();
        CacheKey key = cache.newKey(asHashPart(context.getContextId()), imapLogin);
        return cache.get(key, k -> delegate.resolveIMAPLogin(imapLogin, context));
    }

    @Override
    public void invalidateUser(Context ctx, int userId) throws OXException {
        InvalidationCacheService invalidationService = ServerServiceRegistry.getInstance().getService(InvalidationCacheService.class);
        if (null == invalidationService) {
            return;
        }
        CacheFilter cacheFilter = CacheFilter.builder().withCoreModuleName(CoreModuleName.USER).addSuffix(asHashPart(ctx.getContextId())).addSuffix(userId).build();
        try {
            invalidationService.invalidate(cacheFilter, true); // fire events for com.openexchange.config.cascade.impl.UserCacheListener
        } catch (OXException e) {
            throw UserExceptionCode.CACHE_PROBLEM.create(e);
        }
    }

    @Override
    protected void startInternal() {
        // Nothing to start
    }

    @Override
    protected void stopInternal() throws OXException {
        // Nothing to stop
    }

    @Override
    protected void updatePasswordInternal(Connection connection, Context context, int userId, PasswordMech mech, String password, byte[] salt) throws OXException {
        delegate.updatePasswordInternal(connection, context, userId, mech, password, salt);
        addAfterCommitCallbackElseExecute(connection, (c) -> invalidateUser(context, userId));
    }

    private static CacheKey getKey(Cache<?> cache, Context context, int userId) {
        return getKey(cache, context.getContextId(), userId);
    }

    private static CacheKey getKey(Cache<?> cache, int contextId, int userId) {
        return cache.newKey(asHashPart(contextId), Integer.toString(userId));
    }

    private Optional<Cache<User>> optUserCache() {
        return optCache(CACHE_OPTIONS_USER);
    }

    private <V> Optional<Cache<V>> optCache(CacheOptions<V> options) {
        CacheService cacheService = ServerServiceRegistry.getInstance().getService(CacheService.class);
        if (null == cacheService) {
            return Optional.empty();
        }
        return Optional.of(cacheService.getCache(options));
    }

}
