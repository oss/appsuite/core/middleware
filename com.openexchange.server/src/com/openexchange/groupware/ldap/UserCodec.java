/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.ldap;

import java.util.HexFormat;
import java.util.UUID;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.Codecs;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;
import com.openexchange.user.User;

/**
 * {@link UserCodec} - Cache codec for user objects.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class UserCodec extends AbstractJSONObjectCacheValueCodec<User> {

    private static final UUID CODEC_ID = UUID.fromString("be1aa1ad-5b09-4a95-00fc-c0e11e000001");

    private static final String FIELD_ID = "i";
    private static final String FIELD_USER_PASSWORD = "p";
    private static final String FIELD_MAIL_ENABLED = "e";
    private static final String FIELD_IMAP_SERVER = "v";
    private static final String FIELD_IMAP_LOGIN = "k";
    private static final String FIELD_SMTP_SERVER = "w";
    private static final String FIELD_MAIL_DOMAIN = "d";
    private static final String FIELD_SHADOW_LAST_CHANGE = "s";
    private static final String FIELD_MAIL = "m";
    private static final String FIELD_TIMEZONE = "t";
    private static final String FIELD_PREFERRED_LANGUAGE = "l";
    private static final String FIELD_PASSWORD_MECH = "q";
    private static final String FIELD_CONTACT_ID = "c";
    private static final String FIELD_CREATED_BY = "y";
    private static final String FIELD_FILESTORE_ID = "fI";
    private static final String FIELD_FILE_STORAGE_OWNER = "fO";
    private static final String FIELD_FILESTORE_NAME = "fN";
    private static final String FIELD_FILE_STORAGE_LOGIN = "fL";
    private static final String FIELD_FILE_STORAGE_PASSWORD = "fP";
    private static final String FIELD_FILE_STORAGE_QUOTA = "fQ";
    private static final String FIELD_SALT = "x";
    private static final String FIELD_LOGIN_INFO = "o";
    private static final String FIELD_GIVEN_NAME = "g";
    private static final String FIELD_SURNAME = "u";
    private static final String FIELD_DISPLAY_NAME = "n";
    private static final String FIELD_ATTRIBUTES = "b";
    private static final String FIELD_ALIASES = "a";
    private static final String FIELD_GROUPS = "r";

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONObject writeJson(User value) throws Exception {
        JSONObject jsonObject = new JSONObject(32);
        jsonObject.put(FIELD_ID, value.getId());
        jsonObject.putOpt(FIELD_USER_PASSWORD, value.getUserPassword());
        if (false == value.isMailEnabled()) {
            jsonObject.put(FIELD_MAIL_ENABLED, false);
        }
        jsonObject.putOpt(FIELD_IMAP_LOGIN, value.getImapServer());
        jsonObject.putOpt(FIELD_IMAP_SERVER, value.getImapServer());
        jsonObject.putOpt(FIELD_SMTP_SERVER, value.getSmtpServer());
        jsonObject.putOpt(FIELD_MAIL_DOMAIN, value.getMailDomain());
        if (-1 != value.getShadowLastChange()) {
            jsonObject.put(FIELD_SHADOW_LAST_CHANGE, value.getShadowLastChange());
        }
        jsonObject.putOpt(FIELD_MAIL, value.getMail());
        jsonObject.putOpt(FIELD_TIMEZONE, value.getTimeZone());
        jsonObject.putOpt(FIELD_PREFERRED_LANGUAGE, value.getPreferredLanguage());
        jsonObject.putOpt(FIELD_PASSWORD_MECH, value.getPasswordMech());
        jsonObject.put(FIELD_CONTACT_ID, value.getContactId());
        if (0 < value.getCreatedBy()) {
            jsonObject.put(FIELD_CREATED_BY, value.getCreatedBy());
        }
        if (0 < value.getFilestoreId()) {
            jsonObject.put(FIELD_FILESTORE_ID, value.getFilestoreId());
        }
        if (0 < value.getFileStorageOwner()) {
            jsonObject.put(FIELD_FILE_STORAGE_OWNER, value.getFileStorageOwner());
        }
        jsonObject.putOpt(FIELD_FILESTORE_NAME, value.getFilestoreName());
        String[] fileStorageAuth = value.getFileStorageAuth();
        if (null != fileStorageAuth && 2 == fileStorageAuth.length) {
            jsonObject.putOpt(FIELD_FILE_STORAGE_LOGIN, fileStorageAuth[0]);
            jsonObject.putOpt(FIELD_FILE_STORAGE_PASSWORD, fileStorageAuth[1]);
        }
        if (-1L != value.getFileStorageQuota()) {
            jsonObject.put(FIELD_FILE_STORAGE_QUOTA, value.getFileStorageQuota());
        }
        if (null != value.getSalt()) {
            jsonObject.put(FIELD_SALT, HexFormat.of().formatHex(value.getSalt()));
        }
        jsonObject.putOpt(FIELD_LOGIN_INFO, value.getLoginInfo());
        jsonObject.putOpt(FIELD_GIVEN_NAME, value.getGivenName());
        jsonObject.putOpt(FIELD_SURNAME, value.getSurname());
        jsonObject.putOpt(FIELD_DISPLAY_NAME, value.getDisplayName());
        jsonObject.putOpt(FIELD_ATTRIBUTES, Codecs.stringMapToJsonObject(value.getAttributes()));
        jsonObject.putOpt(FIELD_ALIASES, Codecs.stringArrayToJsonArray(value.getAliases()));
        jsonObject.putOpt(FIELD_GROUPS, Codecs.intArrayToJsonArray(value.getGroups()));

        return jsonObject;
    }

    @Override
    protected User parseJson(JSONObject jObject) throws Exception {

        UserImpl user = new UserImpl();

        user.setId(jObject.getInt(FIELD_ID));
        user.setUserPassword(jObject.optString(FIELD_USER_PASSWORD, null));
        user.setMailEnabled(jObject.optBoolean(FIELD_MAIL_ENABLED, true));
        user.setImapServer(jObject.optString(FIELD_IMAP_SERVER, null));
        user.setImapLogin(jObject.optString(FIELD_IMAP_LOGIN, null));
        user.setSmtpServer(jObject.optString(FIELD_SMTP_SERVER, null));
        user.setMailDomain(jObject.optString(FIELD_MAIL_DOMAIN, null));
        user.setShadowLastChange(jObject.optInt(FIELD_SHADOW_LAST_CHANGE, -1));
        user.setMail(jObject.optString(FIELD_MAIL, ""));
        user.setTimeZone(jObject.optString(FIELD_TIMEZONE, null));
        user.setPreferredLanguage(jObject.optString(FIELD_PREFERRED_LANGUAGE, null));
        user.setPasswordMech(jObject.optString(FIELD_PASSWORD_MECH, null));
        user.setContactId(jObject.getInt(FIELD_CONTACT_ID));
        user.setCreatedBy(jObject.optInt(FIELD_CREATED_BY, 0));
        user.setFilestoreId(jObject.optInt(FIELD_FILESTORE_ID, 0));
        user.setFileStorageOwner(jObject.optInt(FIELD_FILE_STORAGE_OWNER, 0));
        user.setFilestoreName(jObject.optString(FIELD_FILESTORE_NAME, null));
        user.setFilestoreAuth(new String[] { jObject.optString(FIELD_FILE_STORAGE_LOGIN, null), jObject.optString(FIELD_FILE_STORAGE_PASSWORD, null) });
        user.setFileStorageQuota(jObject.optLong(FIELD_FILE_STORAGE_QUOTA, -1L));
        user.setSalt(jObject.hasAndNotNull(FIELD_SALT) ? HexFormat.of().parseHex(jObject.getString(FIELD_SALT)) : null);
        user.setLoginInfo(jObject.optString(FIELD_LOGIN_INFO, null));
        user.setGivenName(jObject.optString(FIELD_GIVEN_NAME, null));
        user.setSurname(jObject.optString(FIELD_SURNAME, null));
        user.setDisplayName(jObject.optString(FIELD_DISPLAY_NAME, null));
        user.setAttributes(Codecs.jsonObjectToStringMap(jObject.optJSONObject(FIELD_ATTRIBUTES)));
        user.setAliases(Codecs.jsonArrayToStringArray(jObject.optJSONArray(FIELD_ALIASES)));
        user.setGroups(Codecs.jsonArrayToIntArray(jObject.optJSONArray(FIELD_GROUPS)));
        return user;
    }

}
