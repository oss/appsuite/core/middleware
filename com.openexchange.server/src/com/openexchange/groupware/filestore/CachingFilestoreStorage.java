/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.filestore;

import static com.openexchange.java.Autoboxing.I;
import java.net.URI;
import java.sql.Connection;
import java.util.Optional;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.exception.OXException;
import com.openexchange.server.services.ServerServiceRegistry;

public class CachingFilestoreStorage extends FilestoreStorage {

    private static final CacheOptions<Filestore> CACHE_OPTIONS = CacheOptions.<Filestore> builder()
                                                                             .withCoreModuleName(CoreModuleName.FILESTORE)
                                                                             .withCodecAndVersion(new FilestoreCodec())
                                                                             .build();

    private final FilestoreStorage delegate;

    public CachingFilestoreStorage(final FilestoreStorage fs) {
        this.delegate = fs;
    }

    @Override
    public Filestore getFilestore(int id) throws OXException {
        Optional<Cache<Filestore>> optCache = optCache();
        if (false == optCache.isPresent()) {
            return delegate.getFilestore(id);
        }
        Cache<Filestore> cache = optCache.get();
        CacheKey cacheKey = createCacheKey(cache, id);
        return cache.get(cacheKey, k -> delegate.getFilestore(id));
    }

    @Override
    public Filestore getFilestore(URI uri) throws OXException {
        return delegate.getFilestore(uri);
    }

    @Override
    public Filestore getFilestore(Connection con, int id) throws OXException {
        Optional<Cache<Filestore>> optCache = optCache();
        if (false == optCache.isPresent()) {
            return delegate.getFilestore(con, id);
        }
        Cache<Filestore> cache = optCache.get();
        CacheKey cacheKey = createCacheKey(cache, id);
        return cache.get(cacheKey, k -> delegate.getFilestore(con, id));
    }

    private static Optional<Cache<Filestore>> optCache() {
        CacheService cacheService = ServerServiceRegistry.getInstance().getService(CacheService.class);
        if (null == cacheService) {
            return Optional.empty();
        }
        return Optional.ofNullable(cacheService.getCache(CACHE_OPTIONS));
    }

    /**
     * Creates a cache key for a specific filestore.
     *
     * @param cache The cache reference
     * @param id The identifier of the filestore to create the cache key for
     * @return The cache key
     */
    private static CacheKey createCacheKey(Cache<?> cache, int id) {
        return cache.newKey(I(id));
    }

}
