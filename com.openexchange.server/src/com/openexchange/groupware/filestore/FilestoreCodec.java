/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.groupware.filestore;

import java.net.URI;
import java.util.UUID;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;


/**
 * {@link FilestoreCodec}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public class FilestoreCodec extends AbstractJSONObjectCacheValueCodec<Filestore> {

    private static final UUID CODEC_ID = UUID.fromString("3525698c-ef97-4b93-a553-08c2c3bb0b67");
    private static final String ID = "i";
    private static final String URI = "u";
    private static final String SIZE = "s";
    private static final String MAX_CONTEXT = "m";

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONObject writeJson(Filestore value) throws Exception {
        JSONObject jsonObject = new JSONObject(4);
        jsonObject.put(ID, value.getId());
        jsonObject.put(URI, value.getUri().toASCIIString());
        jsonObject.put(SIZE, value.getSize());
        jsonObject.put(MAX_CONTEXT, value.getMaxContext());
        return jsonObject;
    }

    @Override
    protected Filestore parseJson(JSONObject jObject) throws Exception {
        FilestoreImpl filestore = new FilestoreImpl();
        filestore.setId(jObject.getInt(ID));
        filestore.setUri(new URI(jObject.getString(URI)));
        filestore.setSize(jObject.getLong(SIZE));
        filestore.setMaxContext(jObject.getLong(MAX_CONTEXT));
        return filestore;
    }

}
