/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.filestore;

import static com.openexchange.java.Autoboxing.I;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import com.openexchange.database.Databases;

/**
 * {@link AbstractFileLocationHandler} - The abstract file location handler.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.1
 */
public abstract class AbstractFileLocationHandler implements FileLocationHandler {

    /** The limitation for SQL IN values. */
    protected static final int IN_LIMIT = Databases.IN_LIMIT;

    /** Named logger instance */
    protected static final Logger LOG = org.slf4j.LoggerFactory.getLogger(AbstractFileLocationHandler.class);

    /** The maximum number of UPDATE statements per batch */
    protected static final int BATCH_LIMIT = 10000;

    /**
     * Initializes a new {@link AbstractFileLocationHandler}.
     */
    protected AbstractFileLocationHandler() {
        super();
    }

    /**
     * Default implementation delegating to {@link #updateFileLocations(Map, int, Connection)}, override if applicable.
     */
    @Override
    public Set<String> updateAllFileLocations(Map<String, String> prevFileName2newFileName, int contextId, Connection con) throws SQLException {
        return updateFileLocations(prevFileName2newFileName, contextId, con);
    }

    /**
     * Generates an SQL IN string with specified number of placeholders.
     *
     * @param size The number of placeholders
     * @return The SQL IN string
     */
    protected static String getSqlInStringFor(int size) {
        StringBuilder sb = new StringBuilder(size << 1);
        sb.append("(?");

        for (int i = size-1; i-- > 0;) {
            sb.append(", ?");
        }
        sb.append(')');

        return sb.toString();
    }

    /**
     * Updates certain file locations in the context using given statements, by first looking up the matching filestore references in the
     * table, then exchanging the ones where a mapping to a new location is defined.
     *
     * @param prevFileName2newFileName The file name mappings
     * @param contextId The context identifier
     * @param selectStmt The SELECT statement; e.g. <code>"SELECT file_id FROM prg_attachment WHERE cid=? AND file_id IN "</code>
     * @param updateStmt The UPDATE statement; e.g. <code>"UPDATE prg_attachment SET file_id = ? WHERE cid = ? AND file_id = ?"</code>
     * @param con The connection to use
     * @return The (previous) file names that have been updated
     * @throws SQLException If an SQL error occurs
     */
    protected static Set<String> updateFileLocationsUsing(Map<String, String> prevFileName2newFileName, int contextId, String selectStmt, String updateStmt, Connection con) throws SQLException {
        int size = prevFileName2newFileName.size();
        if (size <= 0) {
            return Collections.emptySet();
        }

        Set<String> updatedFileNames = HashSet.newHashSet(size);
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            Iterator<String> allPrevFileNames = prevFileName2newFileName.keySet().iterator();
            for (int i = 0; i < size; i += IN_LIMIT) {
                int chunkSizeSize = i + IN_LIMIT > size ? size - i : IN_LIMIT;

                List<String> chunkPrevFileNames = new ArrayList<String>(chunkSizeSize);
                for (int j = chunkSizeSize; j-- > 0;) {
                    chunkPrevFileNames.add(allPrevFileNames.next());
                }

                stmt = con.prepareStatement(selectStmt + getSqlInStringFor(chunkSizeSize));
                // Fill values
                int pos = 0;
                stmt.setInt(++pos, contextId);
                {
                    Iterator<String> chunkIter = chunkPrevFileNames.iterator();
                    for (int j = chunkSizeSize; j-- > 0;) {
                        stmt.setString(++pos, chunkIter.next());
                    }
                }

                // Query for existent file names (if any)
                rs = stmt.executeQuery();
                if (rs.next()) {
                    // Collect existent file names
                    List<String> existent = chunkPrevFileNames;
                    existent.clear();
                    chunkPrevFileNames = null;
                    do {
                        existent.add(rs.getString(1));
                    } while (rs.next());
                    Databases.closeSQLStuff(rs, stmt);
                    rs = null;

                    stmt = con.prepareStatement(updateStmt);
                    for (String prevFileName : existent) {
                        String newFileName = prevFileName2newFileName.get(prevFileName);
                        stmt.setString(1, newFileName);
                        stmt.setInt(2, contextId);
                        stmt.setString(3, prevFileName);
                        stmt.addBatch();
                        updatedFileNames.add(prevFileName);
                    }
                    stmt.executeBatch();
                }
                Databases.closeSQLStuff(rs, stmt);
            }
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
        return updatedFileNames;
    }

    /**
     * Updates the file locations in the context using given statements, by first querying the list of all current filestore identifiers
     * in the table, then exchanging the ones where a mapping to a new location is defined.
     *
     * @param prevFileName2newFileName The file name mappings
     * @param contextId The context identifier (to insert as parameter into the queries)
     * @param selectStmt The <code>SELECT</code> query to obtain all filestore locations (context identifier is inserted as first parameter
     *            into the query; first column of the result set is expected to be the filename); e.g.
     *            <code>"SELECT file_id FROM prg_attachment WHERE cid=? AND file_id IS NOT NULL AND file_id <> ''"</code>
     * @param updateStmt The <code>UPDATE</code> query to obtain the filestore locations (new filestore location is inserted as first
     *            parameter into the statement; context identifier as second parameter, new filestore location as third parameter); e.g.
     *            <code>"UPDATE prg_attachment SET file_id = ? WHERE cid = ? AND file_id = ?"</code>
     * @param con The connection to use
     * @return The (previous) file names that have been updated
     * @throws SQLException If an SQL error occurs
     */
    protected static Set<String> updateAllFileLocationsUsing(Map<String, String> prevFileName2newFileName, int contextId, String selectStmt, String updateStmt, Connection con) throws SQLException {
        int size = prevFileName2newFileName.size();
        if (size <= 0) {
            return Collections.emptySet();
        }
        /*
         * get currently stored filenames
         */
        Set<String> prevFileNames = getCurrentFileLocations(con, contextId, selectStmt);
        if (null == prevFileNames || prevFileNames.isEmpty()) {
            LOG.info("No updateable filestore locations found in table.");
            return Collections.emptySet();
        }
        LOG.info("Processing {} possible filestore locations.", I(prevFileNames.size()));
        /*
         * update filenames based on mapping in batches
         */
        return updateFileLocations(con, contextId, updateStmt, prevFileNames, prevFileName2newFileName);
    }

    /**
     * Updates the file storage locations in the database using the supplied <code>UPDATE</code> statement.
     * 
     * @param connection The database connection to use
     * @param contextId The context identifier (to insert as first parameter into the query)
     * @param updateStmt The <code>UPDATE</code> query to obtain the filestore locations (new filestore location is inserted as first
     *            parameter into the statement; context identifier as second parameter, new filestore location as third parameter); e.g.
     *            <code>"UPDATE prg_attachment SET file_id = ? WHERE cid = ? AND file_id = ?"</code>
     * @param prevFileNames The currently stored filestore locations to update
     * @param prevFileName2newFileName The filestore location mapping to use
     * @return The (previous) file names that have been updated
     */
    private static Set<String> updateFileLocations(Connection connection, int contextId, String updateStmt, Set<String> prevFileNames, Map<String, String> prevFileName2newFileName) throws SQLException {
        Set<String> updatedFileNames = HashSet.newHashSet(prevFileNames.size());
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(updateStmt);
            stmt.setInt(2, contextId);
            int batchSize = 0;
            for (String prevFileName : prevFileNames) {
                String newFileName = prevFileName2newFileName.get(prevFileName);
                if (null == newFileName) {
                    LOG.trace("No updated location for \"{}\" given, skipping update.", prevFileName);
                    continue;
                }
                stmt.setString(1, newFileName);
                stmt.setString(3, prevFileName);
                stmt.addBatch();
                updatedFileNames.add(prevFileName);
                batchSize++;
                if (BATCH_LIMIT <= batchSize) {
                    stmt.executeBatch();
                    LOG.info("Successfully updated {} filestore locations.", I(updatedFileNames.size()));
                    batchSize = 0;
                }
            }
            if (0 < batchSize) {
                stmt.executeBatch();
                LOG.info("Successfully updated {} filestore locations.", I(updatedFileNames.size()));
            }
        } finally {
            Databases.closeSQLStuff(stmt);
        }
        return updatedFileNames;
    }

    /**
     * Reads out the currently stored filestore locations from the database using the supplied <code>SELECT</code> statement.
     * 
     * @param connection The database connection to use
     * @param contextId The context identifier (to insert as first parameter into the query)
     * @param selectStmt The <code>SELECT</code> query to obtain all filestore locations (context identifier is inserted as first parameter
     *            into the query; first column of the result set is expected to be the filename); e.g.
     *            <code>"SELECT file_id FROM prg_attachment WHERE cid=? AND file_id IS NOT NULL AND file_id <> ''"</code>
     * @return The currently stored filestore locations, or an empty set if there are none
     */
    private static Set<String> getCurrentFileLocations(Connection connection, int contextId, String selectStmt) throws SQLException {
        Set<String> prevFileNames = new LinkedHashSet<String>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement(selectStmt);
            stmt.setInt(1, contextId);
            rs = stmt.executeQuery();
            while (rs.next()) {
                prevFileNames.add(rs.getString(1));
            }
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
        return prevFileNames;
    }

}
