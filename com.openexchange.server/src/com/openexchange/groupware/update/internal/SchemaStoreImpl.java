/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.update.internal;

import static com.openexchange.database.Databases.closeSQLStuff;
import static com.openexchange.database.Databases.createTable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.OptionalLong;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.LockSupport;
import java.util.function.BiConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableMap;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadable;
import com.openexchange.config.Reloadables;
import com.openexchange.config.lean.Property;
import com.openexchange.context.PoolAndSchema;
import com.openexchange.database.Databases;
import com.openexchange.databaseold.Database;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.update.ExecutedTask;
import com.openexchange.groupware.update.Schema;
import com.openexchange.groupware.update.SchemaStore;
import com.openexchange.groupware.update.SchemaUpdateState;
import com.openexchange.groupware.update.UpdateProperty;
import com.openexchange.java.util.UUIDs;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.ScheduledTimerTasks;
import com.openexchange.timer.TimerService;
import com.openexchange.tools.update.Tools;

/**
 * Implements loading and storing the schema version information.
 *
 * @author <a href="mailto:marcus.klein@open-xchange.com">Marcus Klein</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class SchemaStoreImpl extends SchemaStore implements Reloadable {

    private static final Logger LOG = LoggerFactory.getLogger(SchemaStoreImpl.class);
    private static final String TABLE_NAME = "updateTask";

    /** Options for the schema update state cache */
    // @formatter:off
    private static final CacheOptions<SchemaUpdateState> OPTIONS_SCHEMA_UPDATE_STATE = CacheOptions.<SchemaUpdateState> builder()
        .withCoreModuleName(CoreModuleName.SCHEMA_UPDATE_STATE)
        .withCodecAndVersion(new SchemaUpdateStateCodec())
        .build();
    // formatter:on

    /** The special LOCK entry */
    private static final UpdateTaskInfo LOCK = new UpdateTaskInfo() {

        private final byte[] uuid = UUIDs.toByteArray(UUID.fromString("26f3894a-c8cc-4889-bf58-aeaa9427d622"));

        @Override
        public byte[] getUuid() {
            return uuid;
        }

        @Override
        public String getName() {
            return "LOCK";
        }
    };

    /** The idioms for either blocking or background updates */
    private static enum Idiom implements UpdateTaskInfo {

        /**
         * The idiom for a blocking update
         */
        LOCKED("LOCKED", UUIDs.toByteArray(UUIDs.fromUnformattedString("8d8b93e559794baca39daef8f87087a1"))),
        /**
         * The idiom for a background update
         */
        BACKGROUND("BACKGROUND", UUIDs.toByteArray(UUIDs.fromUnformattedString("5b7a2847985a49aa874c17df79af48b3")));

        private final String name;
        private final byte[] uuid;
        private final AtomicReference<Long> idleTimeMillis;

        /**
         * Initializes a new instance of {@link Idiom}.
         *
         * @param name The idiom's name
         * @param uuid The idiom's UUID
         */
        private Idiom(String name, byte[] uuid) {
            this.name = name;
            this.uuid = uuid;
            this.idleTimeMillis = new AtomicReference<>(null);
        }

        @Override
        public byte[] getUuid() {
            return uuid;
        }

        @Override
        public String getName() {
            return name;
        }

        /**
         * Gets the number of milliseconds of idleness after which an update process is considered as stalled.
         *
         * @return The idle duration in milliseconds
         */
        long getIdleMillis() {
            Long idleMillis = idleTimeMillis.get();
            if (idleMillis == null) {
                synchronized (this) {
                    idleMillis = idleTimeMillis.get();
                    if (idleMillis == null) {
                        Property property = Idiom.LOCKED == this ? UpdateProperty.BLOCKED_IDLE_MILLIS : UpdateProperty.BACKGROUND_IDLE_MILLIS;
                        long defaultMillis = property.getDefaultValue(Long.class).longValue();

                        ConfigurationService configService = ServerServiceRegistry.getInstance().getService(ConfigurationService.class);
                        if (configService == null) {
                            return defaultMillis;
                        }

                        idleMillis = Long.valueOf(configService.getIntProperty(property.getFQPropertyName(), (int) defaultMillis));
                        idleTimeMillis.set(idleMillis);
                    }
                }
            }
            return idleMillis.longValue();
        }

        /**
         * Clears possibly cached idle time milliseconds.
         */
        void clearIdleMillis() {
            idleTimeMillis.set(null);
        }
    }

    private static final int MYSQL_DEADLOCK = 1213;
    private static final int MYSQL_DUPLICATE = 1062;

    private static final SchemaStoreImpl INSTANCE = new SchemaStoreImpl();

    /**
     * Gets the instance.
     *
     * @return The instance
     */
    public static SchemaStoreImpl getInstance() {
        return INSTANCE;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static CacheKey newCacheKey(Cache<SchemaUpdateState> cache, int poolId, String schema) {
        return cache.newGroupMemberKey("sus", String.valueOf(poolId), schema);
    }

    private final AtomicReference<Cache<SchemaUpdateState>> cacheRef;

    /**
     * Initializes a new {@link SchemaStoreImpl}.
     */
    private SchemaStoreImpl() {
        super();
        cacheRef = new AtomicReference<>(null);
    }

    @Override
    public Interests getInterests() {
        return Reloadables.interestsForProperties(
            UpdateProperty.BLOCKED_IDLE_MILLIS.getFQPropertyName(),
            UpdateProperty.BACKGROUND_IDLE_MILLIS.getFQPropertyName());
    }

    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        for (Idiom idiom : Idiom.values()) {
            idiom.clearIdleMillis();
        }
    }

    @Override
    public SchemaUpdateState getSchema(int contextId) throws OXException {
        int poolId = Database.resolvePool(contextId, true);
        String schemaName = Database.getSchema(contextId);
        Cache<SchemaUpdateState> cache = cacheRef.get();
        if (null == cache) {
            return loadSchemaByContextId(contextId, poolId, schemaName);
        }
        return cache.get(newCacheKey(cache, poolId, schemaName), k -> loadSchemaByContextId(contextId, poolId, schemaName));
    }

    @Override
    public SchemaUpdateState getSchema(int poolId, String schemaName) throws OXException {
        Cache<SchemaUpdateState> cache = cacheRef.get();
        if (null == cache) {
            return loadSchemaByPoolId(poolId, schemaName);
        }
        return cache.get(newCacheKey(cache, poolId, schemaName), k -> loadSchemaByPoolId(poolId, schemaName));
    }

    @Override
    protected SchemaUpdateState getSchema(final int poolId, final String schemaName, final Connection con) throws OXException {
        Cache<SchemaUpdateState> cache = cacheRef.get();
        if (null == cache) {
            return loadSchema(poolId, schemaName, con);
        }
        return cache.get(newCacheKey(cache, poolId, schemaName), k -> loadSchema(poolId, schemaName, con));
    }

    /**
     * Loads the schema by using a new writable connection for the given context id (see {@link Database#get(int, boolean)})
     *
     * @param contextId The context id to get the connection for
     * @param poolId The pool id
     * @param schemaName The schema name
     * @return The {@link SchemaUpdateState}
     * @throws OXException
     */
    private static SchemaUpdateState loadSchemaByContextId(int contextId, int poolId, String schemaName) throws OXException {
        // This method is used when doing a normal login. In this case fetching the Connection runs through replication monitor and
        // initializes the transaction counter from the master. This allows redirecting subsequent reads after normal login to the master
        // if the slave is not actual. See bugs 19817 and 27460.
        Connection con = Database.get(contextId, true);
        try {
            return loadSchema(poolId, schemaName, con);
        } finally {
            // In fact the transaction counter is initialized when returning the connection to the pool ;-)
            Database.backAfterReading(contextId, con);
        }
    }

    /**
     * Loads the schema by using a new writable connection for the given pool id and schema name (see {@link Database#get(int, String)})
     *
     * @param poolId The pool id
     * @param schemaName The schema name
     * @return The {@link SchemaUpdateState}
     * @throws OXException
     */
    private static SchemaUpdateState loadSchemaByPoolId(int poolId, String schemaName) throws OXException {
        // This method is used when creating a context through the administration daemon. In this case we did not write yet the information
        // into the ConfigDB in which database and schema the context is located. Therefore we are not able to initialize the transaction
        // counter for the replication monitor.
        Connection con = Database.get(poolId, schemaName);
        try {
            return loadSchema(poolId, schemaName, con);
        } finally {
            Database.back(poolId, con);
        }
    }

    static SchemaUpdateState loadSchema(int poolId, String schemaName, Connection con) throws OXException {
        if (con == null) {
            return loadSchemaByPoolId(poolId, schemaName);
        }

        try {
            checkForTable(poolId, schemaName, con);
            return loadSchemaStatus(poolId, schemaName, con);
        } catch (SQLException e) {
            throw SchemaExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        }
    }

    private static final com.google.common.cache.Cache<PoolAndSchema, Boolean> TABLE_EXISTS_CACHE = CacheBuilder.newBuilder().build();

    /**
     * @param con connection to master in transaction mode.
     */
    private static void checkForTable(int poolId, String schemaName, final Connection con) throws SQLException {
        PoolAndSchema key = new PoolAndSchema(poolId, schemaName);

        Boolean exists = TABLE_EXISTS_CACHE.getIfPresent(key);
        if (exists == null) {
            try {
                TABLE_EXISTS_CACHE.get(key, () -> checkAndCreateIfNecessary(con));
                return; // Implicitly created through loader
            } catch (ExecutionException e) {
                SQLException sqle = ExceptionUtils.extractFrom(e, SQLException.class);
                throw sqle == null ? new SQLException(e.getCause() == null ? e : e.getCause()) : sqle;
            }
        }

        if (!exists.booleanValue()) {
            // Should never be reached...
            createTable(CreateUpdateTaskTable.CREATES_PRIMARY_KEY[0], true, con);
            TABLE_EXISTS_CACHE.put(key, Boolean.TRUE);
        }
    }

    private static Boolean checkAndCreateIfNecessary(final Connection con) throws SQLException {
        if (Tools.tableExists(con, TABLE_NAME)) {
            return Boolean.TRUE;
        }

        createTable(CreateUpdateTaskTable.CREATES_PRIMARY_KEY[0], true, con);
        return Boolean.TRUE;
    }

    @Override
    public void lockSchema(Schema schema, boolean background) throws OXException {
        DatabaseLock databaseLock = lock(schema.getPoolId(), schema.getSchema());
        try {
            Connection con = Database.get(schema.getPoolId(), schema.getSchema());
            try {
                // Insert lock
                insertLock(con, schema, background ? Idiom.BACKGROUND : Idiom.LOCKED);
                // Everything went fine. Schema is marked as locked
                // Invalidate cache
                invalidateCache(schema);
            } finally {
                Database.back(schema.getPoolId(), con);
            }
        } finally {
            databaseLock.unlock();
        }
    }

    private static void insertLock(Connection con, Schema schema, Idiom idiom) throws OXException {
        // Check for existing lock exclusively
        Date update = null;
        {
            ExecutedTask[] executedTasks = readUpdateTasks(idiom.getName(), con);
            for (int i = 0; update == null && i < executedTasks.length; i++) {
                ExecutedTask task = executedTasks[i];
                if (idiom.getName().equals(task.getTaskName())) {
                    // Check if threshold is not exceeded
                    if (isNotTimedOut(task, idiom)) {
                        throw SchemaExceptionCodes.ALREADY_LOCKED.create(schema.getSchema());
                    }
                    update = task.getLastModified();
                }
            }
        }

        // Insert or update lock
        PreparedStatement stmt = null;
        try {
            if (update == null) {
                stmt = con.prepareStatement("INSERT INTO updateTask (cid, taskName, successful, lastModified, uuid) VALUES (0,?,true,?,?)");
                stmt.setString(1, idiom.getName());
                stmt.setLong(2, System.currentTimeMillis());
                stmt.setBytes(3, idiom.getUuid());
                try {
                    if (stmt.executeUpdate() <= 0) {
                        throw SchemaExceptionCodes.LOCK_FAILED.create(schema.getSchema());
                    }
                } catch (SQLException e) {
                    if (MYSQL_DEADLOCK == e.getErrorCode() || MYSQL_DUPLICATE == e.getErrorCode() || Databases.isPrimaryKeyConflictInMySQL(e)) {
                        throw SchemaExceptionCodes.ALREADY_LOCKED.create(e, schema.getSchema());
                    }
                    throw e;
                }
            } else {
                stmt = con.prepareStatement("UPDATE updateTask SET lastModified=? WHERE cid=0 AND uuid=? AND taskName=? AND lastModified=?");
                stmt.setLong(1, System.currentTimeMillis());
                stmt.setBytes(2, idiom.getUuid());
                stmt.setString(3, idiom.getName());
                stmt.setLong(4, update.getTime());
                if (stmt.executeUpdate() <= 0) {
                    throw SchemaExceptionCodes.ALREADY_LOCKED.create(schema.getSchema());
                }
            }
            // If thread comes that far, lock has been acquired
        } catch (SQLException e) {
            throw SchemaExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        } finally {
            closeSQLStuff(stmt);
        }
    }

    private static boolean isNotTimedOut(ExecutedTask task, Idiom idiom) {
        return isTimedOut(task, idiom) == false;
    }

    private static boolean isTimedOut(ExecutedTask task, Idiom idiom) {
        // Check if threshold is not exceeded
        long idleMillis = idiom.getIdleMillis();
        return idleMillis > 0 && (System.currentTimeMillis() - task.getLastModified().getTime() > idleMillis);
    }

    @Override
    public long getIdleMillis(boolean background) {
        return (background ? Idiom.BACKGROUND : Idiom.LOCKED).getIdleMillis();
    }

    @Override
    public boolean tryRefreshSchemaLock(Schema schema, boolean background) throws OXException {
        int poolId = schema.getPoolId();
        Connection con = Database.get(poolId, schema.getSchema());
        try {
            // Refresh lock
            boolean refreshed = tryRefreshLock(con, background ? Idiom.BACKGROUND : Idiom.LOCKED);
            if (refreshed) {
                invalidateCache(schema);
            }
            return refreshed;
        } finally {
            Database.back(poolId, con);
        }
    }

    private static boolean tryRefreshLock(Connection con, Idiom idiom) throws OXException {
        // Refresh lock
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("UPDATE updateTask SET lastModified = ? WHERE cid=0 AND uuid=?");
            stmt.setLong(1, System.currentTimeMillis());
            stmt.setBytes(2, idiom.getUuid());
            return stmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw SchemaExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        } finally {
            closeSQLStuff(stmt);
        }
    }

    @Override
    public void unlockSchema(final Schema schema, final boolean background) throws OXException {
        DatabaseLock databaseLock = lock(schema.getPoolId(), schema.getSchema());
        try {
            Connection con = Database.get(schema.getPoolId(), schema.getSchema());
            try {
                // Delete lock
                deleteLock(con, schema, background ? Idiom.BACKGROUND : Idiom.LOCKED);
                // Everything went fine. Schema is marked as unlocked
                // Invalidate
                invalidateCache(schema);
            } finally {
                Database.back(schema.getPoolId(), con);
            }
        } finally {
            databaseLock.unlock();
        }
    }

    private static void deleteLock(final Connection con, final Schema schema, final Idiom idiom) throws OXException {
        // Check for existing lock exclusively
        final ExecutedTask[] tasks = readUpdateTasks(idiom.getName(), con);
        boolean found = false;
        for (final ExecutedTask task : tasks) {
            if (idiom.getName().equals(task.getTaskName())) {
                found = true;
                break;
            }
        }
        if (!found) {
            throw SchemaExceptionCodes.UPDATE_CONFLICT.create(schema.getSchema());
        }
        // Delete lock
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("DELETE FROM updateTask WHERE cid=0 AND uuid=?");
            stmt.setBytes(1, idiom.getUuid());
            if (stmt.executeUpdate() == 0) {
                throw SchemaExceptionCodes.UNLOCK_FAILED.create(schema.getSchema());
            }
        } catch (SQLException e) {
            if (MYSQL_DEADLOCK == e.getErrorCode() || MYSQL_DUPLICATE == e.getErrorCode()) {
                throw SchemaExceptionCodes.UNLOCK_FAILED.create(e, schema.getSchema());
            }
            throw SchemaExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        } finally {
            closeSQLStuff(stmt);
        }
    }

    /**
     * @param con connection to the master in transaction mode.
     */
    private static SchemaUpdateState loadSchemaStatus(int poolId, String schemaName, Connection con) throws OXException {
        final SchemaUpdateStateImpl retval = new SchemaUpdateStateImpl();
        retval.setBlockingUpdatesRunning(false);
        retval.setBackgroundUpdatesRunning(false);
        loadUpdateTasks(con, retval);
        retval.setServer(Database.getServerName());
        retval.setSchema(schemaName);
        retval.setPoolId(poolId);
        return retval;
    }

    private static final Map<String, BiConsumer<ExecutedTask, SchemaUpdateStateImpl>> STATE_UPDATERS = ImmutableMap.<String, BiConsumer<ExecutedTask, SchemaUpdateStateImpl>> builder()
        .put(Idiom.LOCKED.getName(), (task, state) -> {
            if (isNotTimedOut(task, Idiom.LOCKED)) {
                state.setBlockingUpdatesRunning(true);
                state.setBlockingUpdatesRunningSince(task.getLastModified());
            }
        })
        .put(Idiom.BACKGROUND.getName(), (task, state) -> {
            if (isNotTimedOut(task, Idiom.BACKGROUND)) {
                state.setBackgroundUpdatesRunning(true);
                state.setBackgroundUpdatesRunningSince(task.getLastModified());
            }
        })
        .build();

    private static void loadUpdateTasks(final Connection con, final SchemaUpdateStateImpl state) throws OXException {
        for (ExecutedTask task : readUpdateTasks(null, con)) {
            BiConsumer<ExecutedTask, SchemaUpdateStateImpl> c = STATE_UPDATERS.get(task.getTaskName());
            if (c == null) {
                state.addExecutedTask(task.getTaskName(), task.isSuccessful());
            } else {
                c.accept(task, state);
            }
        }
    }

    private static ExecutedTask[] readUpdateTasks(String optionalTaskName, Connection con) throws OXException {
        if (optionalTaskName != null) {
            PreparedStatement stmt = null;
            ResultSet result = null;
            try {
                stmt = con.prepareStatement("SELECT taskName,successful,lastModified,uuid FROM updateTask WHERE cid=0 AND taskName=?");
                stmt.setString(1, optionalTaskName);
                result = stmt.executeQuery();
                if (!result.next()) {
                    return new ExecutedTask[0];
                }
                ExecutedTask executedTask = createExecutedTaskFrom(result);
                return executedTask == null ? new ExecutedTask[0] : new ExecutedTask[] { executedTask };
            } catch (SQLException e) {
                throw SchemaExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
            } finally {
                closeSQLStuff(result, stmt);
            }
        }

        // No task name filter. Read them all.
        List<ExecutedTask> retval;
        {
            Statement stmt = null;
            ResultSet result = null;
            try {
                stmt = con.createStatement();
                result = stmt.executeQuery("SELECT taskName,successful,lastModified,uuid FROM updateTask WHERE cid=0");
                if (result.next() == false) {
                    return new ExecutedTask[0];
                }

                retval = new ArrayList<ExecutedTask>(512);
                do {
                    ExecutedTask executedTask = createExecutedTaskFrom(result);
                    if (executedTask != null) {
                        retval.add(executedTask);
                    }
                } while (result.next());
            } catch (SQLException e) {
                throw SchemaExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
            } finally {
                closeSQLStuff(result, stmt);
            }
        }

        Collections.sort(retval, new Comparator<ExecutedTask>() {

            @Override
            public int compare(ExecutedTask o1, ExecutedTask o2) {
                Date lastModified1 = o1.getLastModified();
                Date lastModified2 = o2.getLastModified();
                if (null == lastModified1) {
                    return null == lastModified2 ? 0 : -1;
                } else if (null == lastModified2) {
                    return 1;
                }
                return lastModified1.compareTo(lastModified2);
            }
        });
        return retval.toArray(new ExecutedTask[retval.size()]);
    }

    private static ExecutedTask createExecutedTaskFrom(ResultSet result) throws SQLException {
        String taskName = result.getString(1);
        return LOCK.getName().equals(taskName) ? null : new ExecutedTaskImpl(taskName, result.getBoolean(2), new Date(result.getLong(3)), UUIDs.toUUID(result.getBytes(4)));
    }

    @Deprecated
    @Override
    public void addExecutedTask(String taskName, boolean success, int poolId, String schema) throws OXException {
        addExecutedTask(null, taskName, success, poolId, schema);
    }

    @Override
    public void addExecutedTask(Connection con, String taskName, boolean success, int poolId, String schema) throws OXException {
        doAddExecutedTasks(con, Collections.singletonList(taskName), success, poolId, schema);
    }

    @Deprecated
    @Override
    public void addExecutedTasks(int contextId, Collection<String> taskNames, boolean success, int poolId, String schema) throws OXException {
        addExecutedTasks(null, taskNames, success, poolId, schema);
    }

    @Override
    public void addExecutedTasks(Connection con, Collection<String> taskNames, boolean success, int poolId, String schema) throws OXException {
        if (null == taskNames || taskNames.isEmpty()) {
            return;
        }

        doAddExecutedTasks(con, taskNames, success, poolId, schema);
    }

    private void doAddExecutedTasks(Connection con, Collection<String> taskNames, boolean success, int poolId, String schema) throws OXException {
        insertExecutedTasks(con, taskNames, success, poolId, schema);
        invalidateCache(poolId, schema);
    }

    private static void insertExecutedTasks(Collection<String> taskNames, boolean success, int poolId, String schema) throws OXException {
        Connection con = Database.get(poolId, schema);
        try {
            insertExecutedTasks(con, taskNames, success, poolId, schema);
        } finally {
            Database.back(poolId, con);
        }
    }

    private static final Set<String> RESERVED = Set.of(Idiom.LOCKED.getName(), Idiom.BACKGROUND.getName(), LOCK.getName());

    private static void insertExecutedTasks(Connection con, Collection<String> taskNames, boolean success, int poolId, String schema) throws OXException {
        if (con == null) {
            insertExecutedTasks(taskNames, success, poolId, schema);
            return;
        }

        PreparedStatement insertStatement = null;
        try {
            long now = System.currentTimeMillis();
            insertStatement = con.prepareStatement("INSERT INTO updateTask (cid,successful,lastModified,taskName,uuid) VALUES (0,?,?,?,?) ON DUPLICATE KEY UPDATE successful=?, lastModified=?, taskName=?, uuid=?");
            for (String taskName : taskNames) {
                if (RESERVED.contains(taskName)) {
                    throw OXException.general("Illegal update task name: " + taskName);
                }
                insertTask(insertStatement, taskName, success, now);
            }
            insertStatement.executeBatch();
        } catch (SQLException e) {
            throw SchemaExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        } finally {
            closeSQLStuff(insertStatement);
        }
    }

    /**
     * Inserts an update task
     *
     * @param insertStatement The {@link PreparedStatement} for the insert
     * @param taskName the task's name
     * @param success whether the task was successful
     * @param now The current time stamp
     * @throws SQLException if an SQL error is occurred
     */
    private static void insertTask(PreparedStatement insertStatement, String taskName, boolean success, long now) throws SQLException {
        int parameterIndex = 1;
        // VALUES
        insertStatement.setBoolean(parameterIndex++, success);
        insertStatement.setLong(parameterIndex++, now);
        insertStatement.setString(parameterIndex++, taskName);
        insertStatement.setBytes(parameterIndex++, UUIDs.toByteArray(UUID.randomUUID()));
        // ON DUPLICATE KEY UPDATE
        insertStatement.setBoolean(parameterIndex++, success);
        insertStatement.setLong(parameterIndex++, now);
        insertStatement.setString(parameterIndex++, taskName);
        insertStatement.setBytes(parameterIndex, UUIDs.toByteArray(UUID.randomUUID()));
        insertStatement.addBatch();
    }

    @Override
    public ExecutedTask[] getExecutedTasks(final int poolId, final String schemaName) throws OXException {
        Connection con = Database.get(poolId, schemaName);
        try {
            return readUpdateTasks(null, con);
        } finally {
            Database.back(poolId, con);
        }
    }

    @Override
    public void setCacheService(final CacheService cacheService) {
        cacheRef.set(cacheService.getCache(OPTIONS_SCHEMA_UPDATE_STATE));
    }

    @Override
    public void removeCacheService() {
        Cache<SchemaUpdateState> cache = cacheRef.getAndSet(null);
        if (null != cache) {
            try {
                cache.invalidateGroup(cache.newGroupKey("sus"));
            } catch (Exception e) {
                LOG.warn("Unexpected error while invalidating schema update states.", e);
            }
        }
    }

    @Override
    public void invalidateCache(Schema schema) {
        invalidateCache(schema.getPoolId(), schema.getSchema());
    }

    private void invalidateCache(int poolId, String schema) {
        Cache<SchemaUpdateState> cache = cacheRef.get();
        if (null != cache) {
            CacheKey newCacheKey = newCacheKey(cache, poolId, schema);
            try {
                cache.invalidate(newCacheKey);
            } catch (OXException e) {
                LOG.error("", e);
            }
        }
    }

    private static DatabaseLock lock(int poolId, String schemaName) throws OXException {
        int maxRetries = 5;
        int loopCount = 0;
        int rows;
        do {
            PreparedStatement stmt = null;
            ResultSet rs = null;
            Connection con = Database.get(poolId, schemaName);
            try {
                stmt = con.prepareStatement("SELECT lastModified FROM `updateTask` WHERE `cid`=0 AND `uuid`=?");
                stmt.setBytes(1, LOCK.getUuid());
                rs = stmt.executeQuery();
                OptionalLong stamp = rs.next() ? OptionalLong.of(rs.getLong(1)) : OptionalLong.empty();
                closeSQLStuff(rs, stmt);
                rs = null;
                stmt = null;

                long now = System.currentTimeMillis();
                if (stamp.isEmpty()) {
                    // No lock entry
                    stmt = con.prepareStatement("INSERT INTO `updateTask` (`cid`, `uuid`, `taskName`, `successful`, `lastModified`) VALUES (0, ?, ?, 1, ?) ON DUPLICATE KEY UPDATE cid=cid");
                    stmt.setBytes(1, LOCK.getUuid());
                    stmt.setString(2, LOCK.getName());
                    stmt.setLong(3, now);
                    rows = stmt.executeUpdate();
                    closeSQLStuff(stmt);
                    stmt = null;
                } else if (now - stamp.getAsLong() > 12000L) {
                    // Lock entry expired
                    stmt = con.prepareStatement("UPDATE `updateTask` SET `lastModified`=? WHERE `cid`=0 AND `uuid`=? AND `lastModified`=?");
                    stmt.setLong(1, now);
                    stmt.setBytes(2, LOCK.getUuid());
                    stmt.setLong(3, stamp.getAsLong());
                    rows = stmt.executeUpdate();
                    closeSQLStuff(stmt);
                    stmt = null;
                } else {
                    // Still locked...
                    rows = 0;
                    if (++loopCount > maxRetries) {
                        throw new SQLException("Schema lock conflict: Another process currently holds schema lock. Please try again later.");
                    }
                    Database.back(poolId, con);
                    con = null;
                    exponentialBackoffWait(loopCount, 1000L);
                }
            } catch (SQLException e) {
                throw SchemaExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
            } finally {
                closeSQLStuff(rs, stmt);
                if (con != null) {
                    Database.back(poolId, con);
                }
            }
        } while (rows <= 0);

        ScheduledTimerTask timerTask = null;
        boolean error = true;
        try {
            TimerService timerService = ServerServiceRegistry.getInstance().getService(TimerService.class);
            if (timerService != null) {
                Runnable task = () -> updateLock(poolId, schemaName);
                long refreshIntervalMillis = 3000L;
                timerTask = timerService.scheduleWithFixedDelay(task, refreshIntervalMillis, refreshIntervalMillis, TimeUnit.MILLISECONDS);
            }

            DatabaseLock databaseLock = new DatabaseLock(poolId, schemaName, timerTask);
            error = false;
            return databaseLock;
        } finally {
            if (error) {
                deleteLock(poolId, schemaName);
                if (timerTask != null) { // NOSONARLINT
                    timerTask.cancel();
                }
            }
        }
    }

    private static void exponentialBackoffWait(int retryCount, long baseMillis) {
        long nanosToWait = TimeUnit.NANOSECONDS.convert((retryCount * baseMillis) + ((long) (Math.random() * baseMillis)), TimeUnit.MILLISECONDS);
        LockSupport.parkNanos(nanosToWait);
    }

    private static void updateLock(int poolId, String schemaName) {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = Database.get(poolId, schemaName);
            stmt = con.prepareStatement("UPDATE `updateTask` SET `lastModified`=? WHERE cid=0 AND uuid=?");
            stmt.setLong(1, System.currentTimeMillis());
            stmt.setBytes(2, LOCK.getUuid());
            stmt.executeUpdate();
            LOG.debug("Updated schema lock");
        } catch (Exception e) {
            LOG.warn("Failed to refresh schema lock", e);
        } finally {
            closeSQLStuff(stmt);
            Database.back(poolId, con);
        }
    }

    private static void deleteLock(int poolId, String schemaName) {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = Database.get(poolId, schemaName);
            stmt = con.prepareStatement("DELETE FROM `updateTask` WHERE cid=0 AND uuid=?");
            stmt.setBytes(1, LOCK.getUuid());
            stmt.executeUpdate();
        } catch (Exception e) {
            LOG.warn("Failed to delete schema lock", e);
        } finally {
            closeSQLStuff(stmt);
            Database.back(poolId, con);
        }
    }

    private static class DatabaseLock {

        private final ScheduledTimerTask timerTask;
        private final int poolId;
        private final String schemaName;

        DatabaseLock(int poolId, String schemaName, ScheduledTimerTask timerTask) {
            super();
            this.poolId = poolId;
            this.schemaName = schemaName;
            this.timerTask = timerTask;
        }

        /**
         * Unlocks this lock.
         */
        public void unlock() {
            ScheduledTimerTasks.cancelSafely(timerTask);
            ScheduledTimerTasks.purgeSafely(ServerServiceRegistry.getInstance().getService(TimerService.class));
            deleteLock(poolId, schemaName);
        }
    }

}
