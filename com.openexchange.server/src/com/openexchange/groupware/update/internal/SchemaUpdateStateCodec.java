/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.update.internal;

import static com.openexchange.java.Autoboxing.C;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;
import com.openexchange.groupware.update.NamesOfExecutedTasks;
import com.openexchange.groupware.update.SchemaUpdateState;

/**
 * {@link SchemaUpdateStateCodec} - Cache codec for the {@link SchemaUpdateState}.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class SchemaUpdateStateCodec extends AbstractJSONObjectCacheValueCodec<SchemaUpdateState> {

    private static final UUID CODEC_ID = UUID.fromString("45c8f7b0-198a-4e31-b0a9-b4a29d9dc3a2");

    private static final String KEY_LOCKED = "l";
    private static final String KEY_SERVER = "s";
    private static final String KEY_SCHEMA = "n";
    private static final String KEY_POOL_ID = "p";
    private static final String KEY_SUCCESSFULLY_EXECUTED_TASKS = "c";
    private static final String KEY_FAILED_EXECUTED_TASKS = "f";
    private static final String KEY_BACKGROUND_UPDATES_RUNNING = "b";
    private static final String KEY_BACKGROUND_UPDATES_RUNNING_SINCE = "k";
    private static final String KEY_LOCKED_SINCE = "d";

    /** Basic implementation of {@link SchemaUpdateStateImpl} used for cached instances */
    private static class SchemaUpdateStateData extends SchemaUpdateStateImpl {

        private static final long serialVersionUID = 3971408129291036334L;

        /**
         * Initializes a new {@link SchemaData}.
         */
        SchemaUpdateStateData() {
            super();
        }
    }

    /** Replacements for common substrings in encoded update task names */
    private static final LinkedHashMap<Character, String> TASKNAME_REPLACEMENTS;
    static {
        String[] replacedStrings = { // @formatter:off
            "com.openexchange.groupware.update.tasks.",
            "com.openexchange.",
            "CreateTableTask",
            "UpdateTask",
            "groupware.",
            "storage.",
            "Task",
            "Add",
            "Calendar",
            "Contact",
            "Drop",
            "Folder",
            "Infostore",
            "Table",
            "Change",
        }; // @formatter:on
        String replacements = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        TASKNAME_REPLACEMENTS = LinkedHashMap.newLinkedHashMap(replacedStrings.length);
        for (int i = 0; i < replacedStrings.length && i < replacements.length(); i++) {
            TASKNAME_REPLACEMENTS.put(C(replacements.charAt(i)), replacedStrings[i]);
        }
    }

    /**
     * Initializes a new {@link SchemaUpdateStateCodec}.
     */
    public SchemaUpdateStateCodec() {
        super();
    }

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected boolean isAscii() {
        return true;
    }

    @Override
    protected JSONObject writeJson(SchemaUpdateState schemaUpdateState) throws Exception {
        JSONObject jsonObject = new JSONObject(10);
        if (schemaUpdateState.isLocked()) {
            jsonObject.put(KEY_LOCKED, true);
        }
        jsonObject.putOpt(KEY_SERVER, schemaUpdateState.getServer());
        jsonObject.putOpt(KEY_SCHEMA, schemaUpdateState.getSchema());
        if (0 != schemaUpdateState.getPoolId()) {
            jsonObject.put(KEY_POOL_ID, schemaUpdateState.getPoolId());
        }
        NamesOfExecutedTasks namesOfExecutedTasks = schemaUpdateState.getExecuted();
        if (null != namesOfExecutedTasks) {
            jsonObject.putOpt(KEY_SUCCESSFULLY_EXECUTED_TASKS, encodeExecutedTasks(namesOfExecutedTasks.getSuccessfullyExecutedTasks()));
            jsonObject.putOpt(KEY_FAILED_EXECUTED_TASKS, encodeExecutedTasks(namesOfExecutedTasks.getFailedExecutedTasks()));
        }
        if (schemaUpdateState.backgroundUpdatesRunning()) {
            jsonObject.put(KEY_BACKGROUND_UPDATES_RUNNING, true);
        }
        if (null != schemaUpdateState.backgroundUpdatesRunningSince()) {
            jsonObject.put(KEY_BACKGROUND_UPDATES_RUNNING_SINCE, schemaUpdateState.backgroundUpdatesRunningSince().getTime());
        }
        if (null != schemaUpdateState.blockingUpdatesRunningSince()) {
            jsonObject.put(KEY_LOCKED_SINCE, schemaUpdateState.blockingUpdatesRunningSince().getTime());
        }
        return jsonObject;
    }

    @Override
    protected SchemaUpdateState parseJson(JSONObject jsonObject) throws Exception {
        SchemaUpdateStateData schemaUpdateState = new SchemaUpdateStateData();
        schemaUpdateState.setBlockingUpdatesRunning(jsonObject.optBoolean(KEY_LOCKED));
        schemaUpdateState.setServer(jsonObject.optString(KEY_SERVER, null));
        schemaUpdateState.setSchema(jsonObject.optString(KEY_SCHEMA, null));
        schemaUpdateState.setPoolId(jsonObject.optInt(KEY_POOL_ID, 0));
        decodeExecutedTasks(schemaUpdateState, jsonObject.optJSONArray(KEY_SUCCESSFULLY_EXECUTED_TASKS), true);
        decodeExecutedTasks(schemaUpdateState, jsonObject.optJSONArray(KEY_FAILED_EXECUTED_TASKS), false);
        schemaUpdateState.setBackgroundUpdatesRunning(jsonObject.optBoolean(KEY_BACKGROUND_UPDATES_RUNNING));
        long l = jsonObject.optLong(KEY_BACKGROUND_UPDATES_RUNNING_SINCE, 0);
        if (l != 0) {
            schemaUpdateState.setBackgroundUpdatesRunningSince(new Date(l));
        }
        l = jsonObject.optLong(KEY_LOCKED_SINCE, 0);
        if (l != 0) {
            schemaUpdateState.setBackgroundUpdatesRunningSince(new Date(l));
        }
        return schemaUpdateState;
    }

    /**
     * De-serializes the names of executed update tasks from a JSON array by substituting any inserted replacements back with the original
     * substrings, taking over the task names in the supplied schema update state.
     *
     * @param schemaUpdateState The schema update state to add the task names to
     * @param executedTasksArray The JSON array of encoded update task names to take over
     * @param success <code>true</code> to take the tasks over as successfully executed, <code>false</code>, otherwise
     * @throws JSONException If de-serialization fails
     */
    private static void decodeExecutedTasks(SchemaUpdateState schemaUpdateState, JSONArray executedTasksArray, boolean success) throws JSONException {
        if (null != executedTasksArray) {
            for (int i = 0; i < executedTasksArray.length(); i++) {
                schemaUpdateState.addExecutedTask(decodeTaskname(executedTasksArray.getString(i)), success);
            }
        }
    }

    /**
     * Encodes the names of executed update tasks and serializes them to a JSON array, by replacing common substrings with a compact
     * substitution.
     *
     * @param executedTasks The task names to encode
     * @return The encoded task names as JSON array
     */
    private static JSONArray encodeExecutedTasks(Set<String> executedTasks) {
        if (null == executedTasks) {
            return null;
        }
        JSONArray jsonArray = new JSONArray(executedTasks.size());
        for (String taskName : executedTasks) {
            jsonArray.put(encodeTaskname(taskName));
        }
        return jsonArray;
    }

    /**
     * Encodes the name of an executed update task prior serializing to JSON by replacing common substrings with a compact substitution.
     *
     * @param taskName The task name to encode
     * @return The encoded task name
     */
    private static String encodeTaskname(String taskName) {
        String adjustedName = taskName.replace("^", "\\^");
        for (Map.Entry<Character, String> entry : TASKNAME_REPLACEMENTS.entrySet()) {
            adjustedName = adjustedName.replace(entry.getValue(), "^" + entry.getKey());
        }
        return adjustedName;
    }

    /**
     * Decodes the name of an executed update task after de-serializing from JSON by substituting any inserted replacements back with the
     * original substrings.
     *
     * @param taskName The task name to decode
     * @return The decoded task name
     */
    private static String decodeTaskname(String taskName) {
        int index = taskName.indexOf('^');
        int mlen = taskName.length() - 1;
        if (index < 0 || index >= mlen) {
            return taskName;
        }

        StringBuilder sb = new StringBuilder();
        int lastIndex = 0;
        do {
            if (index > lastIndex) {
                sb.append(taskName.substring(lastIndex, index));
            }
            if (index > 0 && taskName.charAt(index - 1) == '\\') {
                // Escaped...
                sb.setLength(sb.length() - 1);
                sb.append('^');
                lastIndex = index + 1;
            } else {
                sb.append(TASKNAME_REPLACEMENTS.get(C(taskName.charAt(index + 1))));
                lastIndex = index + 2;
            }
            index = taskName.indexOf('^', lastIndex);
        } while (index >= 0 && index < mlen);
        if (lastIndex < taskName.length()) {
            sb.append(taskName.substring(lastIndex));
        }
        return sb.toString();
    }

}
