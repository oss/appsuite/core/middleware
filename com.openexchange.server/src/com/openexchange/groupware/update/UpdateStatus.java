/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.update;

import java.util.Date;

/**
 * {@link UpdateStatus} - Represents the update status.
 *
 * @author <a href="mailto:marcus.klein@open-xchange.com">Marcus Klein</a>
 */
public interface UpdateStatus {

    /**
     * Checks if blocking updates are currently running.
     *
     * @return <code>true</code> if blocking updates are currently running; otherwise <code>false</code>
     */
    boolean blockingUpdatesRunning();

    /**
     * Checks if background updates are currently running.
     *
     * @return <code>true</code> if background updates are currently running; otherwise <code>false</code>
     */
    boolean backgroundUpdatesRunning();

    /**
     * Checks if blocking updates are pending.
     *
     * @return <code>true</code> if blocking updates are pending; otherwise <code>false</code>
     */
    boolean needsBlockingUpdates();

    /**
     * Checks if background updates are pending.
     *
     * @return <code>true</code> if background updates are pending; otherwise <code>false</code>
     */
    boolean needsBackgroundUpdates();

    /**
     * Determines the date since when blocking updates are running.
     *
     * @return The date or <code>null</code> if no blocking updates are running
     */
    Date blockingUpdatesRunningSince();

    /**
     * Determines the date since when background updates are running.
     *
     * @return The date or <code>null</code> if no background updates are running
     */
    Date backgroundUpdatesRunningSince();

    /**
     * Checks if an update is needed. That is the case if:
     * <ul>
     * <li>Either blocking updates or background updates are pending
     * <li>Neither blocking nor background updates are currently running
     * </ul>
     *
     * @return <code>true</code> if an update is needed; otherwise <code>false</code>
     */
    default boolean updateNeeded() {
        return (needsBlockingUpdates() || needsBackgroundUpdates()) && !blockingUpdatesRunning() && !backgroundUpdatesRunning();
    }

    /**
     * Gets a value indicating whether a specific update task has been executed successfully on the associated database schema or not.
     *
     * @param taskName The name of the update task to check
     * @return <code>true</code> if the update task was executed successfully, otherwise <code>false</code>
     */
    boolean isExecutedSuccessfully(String taskName);

    /**
     * Checks if blocking updates are considered as timed-out.
     *
     * @return <code>true</code> if timed-out; otherwise <code>false</code>
     */
    boolean blockingUpdatesTimedOut();

    /**
     * Gets the identifier of the database pool associated with thus status' schema.
     *
     * @return The database pool identifier
     */
    int getPoolId();

    /**
     * Gets the name of the schema.
     *
     * @return The schema name
     */
    String getSchemaName();
}
