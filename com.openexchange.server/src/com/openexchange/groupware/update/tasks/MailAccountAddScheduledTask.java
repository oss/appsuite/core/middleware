package com.openexchange.groupware.update.tasks;

import static com.openexchange.database.Databases.closeSQLStuff;
import static com.openexchange.tools.update.Tools.columnExists;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.update.Attributes;
import com.openexchange.groupware.update.PerformParameters;
import com.openexchange.groupware.update.TaskAttributes;
import com.openexchange.groupware.update.UpdateConcurrency;
import com.openexchange.groupware.update.UpdateExceptionCodes;
import com.openexchange.groupware.update.UpdateTaskAdapter;

/**
 * {@link MailAccountAddScheduledTask} - Adds "scheduled" and "scheduled_fullname" columns to mail account table.
 *
 * @author <a href="mailto:marcel.broecher@open-xchange.com">Marcel Broecher</a>
 */
public final class MailAccountAddScheduledTask extends UpdateTaskAdapter {

    public MailAccountAddScheduledTask() {
        super();
    }

    @Override
    public TaskAttributes getAttributes() {
        return new Attributes(UpdateConcurrency.BLOCKING);
    }

    @Override
    public String[] getDependencies() {
        return new String[] { AllowNullValuesForStandardFolderNamesUpdateTask.class.getName()};
    }

    @Override
    public void perform(final PerformParameters params) throws OXException {
        Connection con = params.getConnection();
        PreparedStatement stmt = null;
        int doRollback = 0;
        try {
            Databases.startTransaction(con);
            doRollback = 1;

            if (!columnExists(con, "user_mail_account", "scheduled")) {
                stmt = con.prepareStatement("ALTER TABLE `user_mail_account` ADD COLUMN `scheduled` VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT ''");
                stmt.executeUpdate();
                stmt.close();
                stmt = null;
            }
            if (!columnExists(con, "user_mail_account", "scheduled_fullname")) {
                stmt = con.prepareStatement("ALTER TABLE `user_mail_account` ADD COLUMN `scheduled_fullname` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT ''");
                stmt.executeUpdate();
                stmt.close();
                stmt = null;
            }

            con.commit();
            doRollback = 2;
        } catch (SQLException e) {
            throw UpdateExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        } finally {
            closeSQLStuff(stmt);
            if (doRollback > 0) {
                if (doRollback == 1) {
                    Databases.rollback(con);
                }
                Databases.autocommit(con);
            }
        }
    }

}
