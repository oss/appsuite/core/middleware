/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.update.tasks;

import java.sql.Connection;
import java.sql.SQLException;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.update.PerformParameters;
import com.openexchange.groupware.update.UpdateExceptionCodes;
import com.openexchange.groupware.update.UpdateTaskAdapter;
import com.openexchange.java.Strings;
import com.openexchange.tools.update.Tools;

/**
 * {@link InfostoreDocumentDropForeignKey} - Drops foreign key from "infostore_document" table due to missing unique key in the referenced
 * table "infostore".
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class InfostoreDocumentDropForeignKey extends UpdateTaskAdapter {

    /**
     * Initializes a new {@link InfostoreDocumentDropForeignKey}.
     */
    public InfostoreDocumentDropForeignKey() {
        super();
    }

    @Override
    public String[] getDependencies() {
        return new String[] { InfostoreDocumentDropForeignKeyUpdateTask.class.getName() };
    }

    @Override
    public void perform(final PerformParameters params) throws OXException {
        Connection con = params.getConnection();
        try {
            dropForeignKeyFromInfostoreDocument(con);
            addIndexToInfostoreDocument(con);
        } catch (SQLException e) {
            throw UpdateExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw UpdateExceptionCodes.OTHER_PROBLEM.create(e, e.getMessage());
        }
    }

    private static void dropForeignKeyFromInfostoreDocument(Connection con) throws SQLException {
        int rollback = 0;
        try {
            String infostoreDocumentTable = "infostore_document";
            String foreignKey = Tools.existsForeignKey(con, "infostore", new String[] {"cid", "id"}, infostoreDocumentTable, new String[] {"cid", "infostore_id"});
            if (Strings.isEmpty(foreignKey)) {
                // No such foreign key. Leave...
                return;
            }

            // Drop the foreign key from "infostore_document" table
            Databases.startTransaction(con);
            rollback = 1;

            Tools.dropForeignKey(con, infostoreDocumentTable, foreignKey);

            con.commit();
            rollback = 2;
        } finally {
            if (rollback > 0) {
                if (rollback == 1) {
                    Databases.rollback(con);
                }
                Databases.autocommit(con);
            }
        }
    }

    private static void addIndexToInfostoreDocument(Connection con) throws SQLException {
        int rollback = 0;
        try {
            String infostoreDocumentTable = "infostore_document";
            String[] columns = {"cid", "infostore_id"};

            if (Tools.existsIndex(con, infostoreDocumentTable, columns) != null) {
                // Such an index already exists
                return;
            }

            // Add index to "infostore_document" table
            Databases.startTransaction(con);
            rollback = 1;

            Tools.createIndex(con, infostoreDocumentTable, "infostoreIdIndex", columns, false);

            con.commit();
            rollback = 2;
        } finally {
            if (rollback > 0) {
                if (rollback == 1) {
                    Databases.rollback(con);
                }
                Databases.autocommit(con);
            }
        }
    }

}
