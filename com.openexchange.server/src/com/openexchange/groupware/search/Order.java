/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.groupware.search;

import java.util.Optional;

/**
 * Enumeration for the order direction.
 *
 * @author <a href="mailto:marcus@open-xchange.org">Marcus Klein</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public enum Order {

    /**
     * The unspecified order direction.
     */
    NO_ORDER(""),
    /**
     * The ascending order direction.
     */
    ASCENDING("ASC"),
    /**
     * The descending order direction.
     */
    DESCENDING("DESC");

    private final String identifier;

    private Order(String identifier) {
        this.identifier = identifier;
    }

    /**
     * Gets the identifier.
     *
     * @return The identifier; e.g. <code>"ASC"</code>
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Gets the string representation for given order direction.
     * <p>
     * Returns the empty string in case given order direction is <code>null</code> or equal to {@link #NO_ORDER}.
     *
     * @param order The order direction
     * @return The string representation; e.g. <code>"ASC"</code>
     */
    public static String orderDirectionFor(Order order) {
        return order == null ? "" : order.getIdentifier();
    }

    /**
     * Returns the given order if it appears to be valid (not <code>null</code> and not unspecified).
     *
     * @param order The order to check
     * @return The validated order or empty
     */
    public static Optional<Order> isValidOrderDirection(Order order) {
        return order == null || order == Order.NO_ORDER ? Optional.empty() : Optional.of(order);
    }

}
