/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.request.analyzer.utils;

import static com.openexchange.ajax.AJAXServlet.PARAMETER_ACTION;
import static com.openexchange.authentication.AuthenticationResult.Status.SUCCESS;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.login.LoginRequestImpl;
import com.openexchange.ajax.requesthandler.AJAXRequestDataTools;
import com.openexchange.authentication.Authenticated;
import com.openexchange.authentication.AuthenticationRequest;
import com.openexchange.authentication.AuthenticationResult;
import com.openexchange.authentication.AuthenticationServiceRegistry;
import com.openexchange.authentication.ResolvedAuthenticated;
import com.openexchange.context.ContextService;
import com.openexchange.database.ConfigDatabaseService;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.java.Strings;
import com.openexchange.log.DurationOutputter;
import com.openexchange.login.LoginRequest;
import com.openexchange.login.listener.LoginListener;
import com.openexchange.login.listener.internal.LoginListenerRegistry;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.BodyData;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.request.analyzer.RequestURL;
import com.openexchange.request.analyzer.Type;
import com.openexchange.request.analyzer.UserInfo;
import com.openexchange.segment.SegmentMarker;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.servlet.Headers;
import com.openexchange.session.Session;
import com.openexchange.tools.functions.ErrorAwareSupplier;
import com.openexchange.tools.servlet.http.Authorization;
import com.openexchange.tools.servlet.http.Authorization.Credentials;
import com.openexchange.user.UserService;

/**
 * {@link RequestAnalyzerUtils} - A collection of utility methods for the request analyzer implementations.
 *
 * @author <a href="mailto:marcel.broecher@open-xchange.com">Marcel Broecher</a>
 */
public class RequestAnalyzerUtils {

    private static final Logger LOG = LoggerFactory.getLogger(RequestAnalyzerUtils.class);

    /**
     * Prevent instantiation.
     */
    private RequestAnalyzerUtils() {
        super();
    }

    // -------------------- public methods -----------------

    /**
     * Tries to perform a login to get the analyze result. Necessary service references are directly obtained from the
     * {@link ServerServiceRegistry} instance.
     * <p/>
     * The result is either {@link Type#UNKNOWN} if the authentication failed or {@link Type#SUCCESS}
     *
     * @param loginRequest The login request to analyze
     * @param loginListenerRegistry The login listener registry to use
     * @return The {@link AnalyzeResult}
     */
    public static AnalyzeResult analyzeViaLogin(LoginRequest loginRequest, LoginListenerRegistry loginListenerRegistry) {
        try {
            AuthenticationServiceRegistry authenticationServiceRegistry = ServerServiceRegistry.getServize(AuthenticationServiceRegistry.class, true);
            Optional<Authenticated> optAuth = login(authenticationServiceRegistry, loginRequest, loginListenerRegistry.getLoginListeners());
            if (optAuth.isEmpty()) {
                return AnalyzeResult.UNKNOWN;
            }
            Authenticated auth = optAuth.get();
            ContextService contextService = ServerServiceRegistry.getServize(ContextService.class, true);
            UserService userService = ServerServiceRegistry.getServize(UserService.class, true);
            DatabaseService databaseService = ServerServiceRegistry.getServize(DatabaseService.class, true);
            return convert2AnalyzeResult(contextService, userService, databaseService, auth, loginRequest.getLogin());
        } catch (OXException e) {
            LOG.debug("Generating an AnalyzeResult by performing a login failed. Returning UNKNOWN result.", e);
            return AnalyzeResult.UNKNOWN;
        }
    }

    /**
     * Tries to perform a login to get the analyze result.
     * <p>
     * The result is either {@link Type#UNKNOWN} if the authentication failed or {@link Type#SUCCESS}
     *
     * @param contextService The context service
     * @param userService The user service
     * @param databaseService A reference to the database service to derive the schema name associated with the context
     * @param authenticationServiceRegistry The authentication service registry to use
     * @param loginRequest The login request
     * @param loginListener The login listener
     * @return The {@link AnalyzeResult}
     * @deprecated Seems to be unused, we should check if this is needed after finalizing request analyzers. feel free to remove annotation if this method is needed somewhere
     */
    @Deprecated
    public static AnalyzeResult analyzeViaLogin(ContextService contextService, // @formatter:off
                                                UserService userService,
                                                ConfigDatabaseService databaseService,
                                                AuthenticationServiceRegistry authenticationServiceRegistry,
                                                LoginRequest loginRequest,
                                                List<LoginListener> loginListener) { // @formatter:on
        try {
            Optional<Authenticated> optAuth = login(authenticationServiceRegistry, loginRequest, loginListener);
            if (optAuth.isEmpty()) {
                return AnalyzeResult.UNKNOWN;
            }
            Authenticated auth = optAuth.get();
            return convert2AnalyzeResult(contextService, userService, databaseService, auth, loginRequest.getLogin());
        } catch (OXException e) {
            LOG.debug("Generating an AnalyzeResult by performing a login failed. Returning UNKNOWN result.", e);
            return AnalyzeResult.UNKNOWN;
        }
    }

    /**
     * Creates an {@link AnalyzeResult} from a given session
     *
     * @param session The session, or <code>null</code> to produce {@link AnalyzeResult#UNKNOWN}
     * @return The {@link AnalyzeResult}
     */
    public static AnalyzeResult createAnalyzeResultFromSession(Session session) {
        if (session == null) {
            return AnalyzeResult.UNKNOWN;
        }
        Object schema = session.getParameter(Session.PARAM_USER_SCHEMA);
        if (schema == null) {
            return AnalyzeResult.UNKNOWN;
        }

        // Create result
        return new AnalyzeResult(SegmentMarker.of(schema.toString()), UserInfo.userInfoFor(session));
    }

    /**
     * Creates an {@link AnalyzeResult} from a given context identifier.
     *
     * @param cid The context identifier
     * @param dbServiceSupplier The config database service supplier
     * @return The {@link AnalyzeResult}
     */
    public static AnalyzeResult createAnalyzeResultFromCid(String cid, ErrorAwareSupplier<? extends ConfigDatabaseService> dbServiceSupplier) {
        if (cid == null || dbServiceSupplier == null) {
            return AnalyzeResult.UNKNOWN;
        }
        try {
            int contextId = Integer.parseInt(cid);
            String schemaName = dbServiceSupplier.get().getSchemaName(contextId);
            return new AnalyzeResult(SegmentMarker.of(schemaName), null);
        } catch (NumberFormatException e) {
            LOG.debug("The value '{}' set as context identifier could not be parsed to an integer.", cid, e);
            return AnalyzeResult.UNKNOWN;
        } catch (OXException e) {
            LOG.debug("No schema for context '{}' could be found.", cid, e);
            return AnalyzeResult.UNKNOWN;
        }
    }

    /**
     * Converts the given headers to a map
     *
     * @param headers The headers to convert
     * @return The map
     */
    public static Map<String, List<String>> convert2headerMap(Headers headers) {
        return headers.getMapping();
    }

    /**
     * Creates a login request which can be used for {@link #analyzeViaLogin(ContextService, UserService, AuthenticationServiceRegistry, LoginRequest, List)}
     *
     * @param login The login
     * @param password The password
     * @param client The client identifier
     * @param clientIp The client IP address
     * @param userAgent The user agent
     * @param headers The request headers
     * @return The login request
     */
    public static LoginRequest createLoginRequest(String login, // @formatter:off
                                            String password,
                                            String client,
                                            String clientIp,
                                            String userAgent,
                                            Headers headers) { // @formatter:on
        return new LoginRequestImpl.Builder().login(login)
                                             .password(password)
                                             .client(client)
                                             .clientIP(clientIp)
                                             .userAgent(userAgent)
                                             .headers(headers.getMapping())
                                             .build();
    }

    /**
     * Checks if the password contains illegal values or the login is empty
     *
     * @param creds The credentials to validate
     * @return <code>true</code> if the credentials are valid, <code>false</code> otherwise
     */
    public static boolean checkCredentials(Credentials creds) {
        return Authorization.checkLogin(creds.getPassword()) && Strings.isNotEmpty(creds.getLogin());
    }

    /**
     * Optionally gets the first parameter with the given name from the list of name-value pairs.
     *
     * @param parameters A list of name/value pairs
     * @param name The name of the parameter to match
     * @return The optional parameter value
     */
    public static Optional<String> optParameterValue(List<NameValuePair> parameters, String name) {
        return parameters.stream()
                          .filter(q -> name.equals(q.getName()))
                          .findFirst()
                          .map(NameValuePair::getValue);
    }

    /**
     * Gets a value indicating whether a specific path starts with one of the supplied path prefixes.
     *
     * @param requestPath The path of the request
     * @param qualifiedPaths The prefixes of the paths to match the request path against
     * @return <code>true</code> if the request is qualified, <code>false</code> otherwise
     */
    public static boolean isQualifiedPath(String requestPath, Set<String> qualifiedPaths) {
        if (Strings.isEmpty(requestPath)) {
            return false;
        }
        for (String path : qualifiedPaths) {
            if (requestPath.startsWith(path)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Parse application/x-www-form-urlencoded body into a map
     *
     * @param body The body data
     * @return The body parsed as map
     * @throws IOException On error
     */
    public static Map<String, String> parseUrlencodedBody(Optional<BodyData> body) throws IOException {
        if (body.isEmpty()) {
            return Collections.emptyMap();
        }
        List<NameValuePair> bodyParams = URLEncodedUtils.parse(body.get().getDataAsString(), StandardCharsets.UTF_8);
        Map<String, String> result = HashMap.newHashMap(bodyParams.size());
        bodyParams.stream().forEach(t -> result.put(t.getName(), t.getValue()));
        return result;
    }

    /**
     * Parses the parameters from the given <code>application/x-www-form-urlencoded</code> request body data as name-value-pairs.
     *
     * @param bodyData The body data to parse
     * @return The parsed form data parameters
     */
    public static List<NameValuePair> parseBodyParameters(BodyData bodyData) {
        if (null != bodyData) {
            try {
                return URLEncodedUtils.parse(bodyData.getDataAsString(), StandardCharsets.UTF_8);
            } catch (IOException e) {
                LOG.debug("Error parsing form parameters from body: {}", e.getMessage(), e);
            }
        }
        return Collections.emptyList();
    }

    /**
     * Parses the parameters from the given request URI as name-value-pairs.
     *
     * @param requestURL The request URL to parse
     * @return The parsed parameters
     */
    public static List<NameValuePair> parseRequestParameters(RequestURL requestURL) {
        if (null != requestURL && null != requestURL.getURL()) {
            String query = requestURL.getURL().getQuery();
            if (null != query) {
                return URLEncodedUtils.parse(query, StandardCharsets.UTF_8);
            }
        }
        return Collections.emptyList();
    }

    /**
     * Gets a value indicating whether the supplied request data matches certain criteria:
     * <ul>
     * <li>The HTTP method</li>
     * <li>The dispatcher prefix as first part of the request URI's path</li>
     * <li>The Ajax module (as supplied after the dispatcher prefix in the request URI)</li>
     * <li>The Ajax action (as supplied via parameter {@value AJAXServlet#PARAMETER_ACTION} in the request URI)</li>
     * </ul>
     *
     * @param data The reuqest data to check
     * @param httpMethod The HTTP method to match, or <code>null</code> to skip the check
     * @param dispatcherPrefix The dispatcher prefix to match
     * @param module The module string to match, or <code>null</code> to skip the check
     * @param action The action string to match, or <code>null</code> to skip the check
     *
     * @return <code>true</code> if method, module and action matches, <code>false</code>, otherwise
     * @throws OXException In case of an invalid or malformed URL
     */
    public static boolean matchesMethodModuleAndAction(RequestData data, String httpMethod, String dispatcherPrefix, String module, String action) throws OXException {
        if (null != httpMethod && false == httpMethod.equals(data.getMethod())) {
            return false; // HTTP method does not match
        }
        Optional<String> optPath = data.getParsedURL().getPath();
        if (optPath.isEmpty()) {
            return false; // empty path
        }
        String path = optPath.get();
        if (false == path.startsWith(dispatcherPrefix)) {
            return false; // prefix does not match
        }
        if (null != action && false == action.equals(data.getParsedURL().optParameter(PARAMETER_ACTION).orElse(null))) {
            return false; // action not set or different
        }
        if (null != module && false == module.equals(AJAXRequestDataTools.getModuleFromPath(dispatcherPrefix, path))) {
            return false; // module not set or different
        }
        return true; // everything matched
    }

    /**
     * Gets the first parameter value matching the given name from the supplied parameter map, falling back to the supplied alternative.
     *
     * @param parametersMap The parameter map to get the first matching value from
     * @param parameterName The name of the parameter to match
     * @return The first matching value, or the given fallback if not found
     */
    public static String getFirstParameterValue(Map<String, String[]> parametersMap, String parameterName) {
        if (null == parametersMap || parametersMap.isEmpty()) {
            return null;
        }
        String[] values = parametersMap.get(parameterName);
        if (null == values || 0 == values.length) {
            return null;
        }
        return values[0];
    }

    /**
     * Adds a collection of name-value-pairs into the supplied map holding all parameter values by their name.
     *
     * @param nameValuePairs The name-value-pairs to add
     * @param parametersMap The map to put the name-value-pairs into
     */
    public static void addToMap(List<NameValuePair> nameValuePairs, Map<String, String[]> parametersMap) {
        if (null == nameValuePairs || nameValuePairs.isEmpty()) {
            return;
        }
        for (NameValuePair nameValuePair : nameValuePairs) {
            String[] values = parametersMap.get(nameValuePair.getName());
            if (null == values) {
                parametersMap.put(nameValuePair.getName(), new String[] { nameValuePair.getValue() });
            } else {
                parametersMap.put(nameValuePair.getName(), com.openexchange.tools.arrays.Arrays.add(values, nameValuePair.getValue()));
            }
        }
    }

    /**
     * Gets the first parameter value matching the given name from the supplied parameter map, falling back to the supplied alternative.
     *
     * @param parametersMap The parameter map to get the first matching value from
     * @param parameterName The name of the parameter to match
     * @param fallback The value to return as fallback
     * @return The first matching value, or the given fallback if not found
     */
    public static String getFirstParameterValue(Map<String, String[]> parametersMap, String parameterName, String fallback) {
        if (null == parametersMap || parametersMap.isEmpty()) {
            return fallback;
        }
        String[] values = parametersMap.get(parameterName);
        if (null == values || 0 == values.length) {
            return fallback;
        }
        return values[0];
    }

    // -------------------- private methods -----------------

    /**
     * Converts the authenticated result into an {@link AnalyzeResult}.
     *
     * @param contextService The context service
     * @param userService The user service
     * @param databaseService A reference to the database service to derive the schema name associated with the context
     * @param auth The authenticated result
     * @param login The users login
     * @return The {@link AnalyzeResult}
     * @throws OXException in case an error occurred while determine the marker
     */
    private static AnalyzeResult convert2AnalyzeResult(ContextService contextService, UserService userService, ConfigDatabaseService databaseService, Authenticated auth, String login) throws OXException {
        int contextId;
        int userId;
        if (auth instanceof ResolvedAuthenticated resolvedAuth) {
            contextId = resolvedAuth.getContextID();
            userId = resolvedAuth.getUserID();
        } else {
            contextId = contextService.getContextId(auth.getContextInfo());
            Context context = contextService.getContext(contextId);
            userId = userService.getUserId(auth.getUserInfo(), context);
        }
        String schemaName = databaseService.getSchemaName(contextId);
        UserInfo userInfo = UserInfo.builder(contextId)
                                    .withUserId(userId)
                                    .withLogin(login)
                                    .build();
        return new AnalyzeResult(SegmentMarker.of(schemaName), userInfo);
    }

    /**
     * Performs a login and returns the {@link Authenticated} if the authentication was successful. Otherwise an empty {@link Optional} is returned.
     *
     * @param authenticationServiceRegistry The authentication service registry
     * @param loginReq The login request
     * @param loginListeners The login listeners
     * @return The optional authenticated
     * @throws OXException if the login attempt is rendered as invalid or something goes wrong while authenticating
     */
    private static Optional<Authenticated> login(AuthenticationServiceRegistry authenticationServiceRegistry, LoginRequest loginReq, List<LoginListener> loginListeners) throws OXException {
        long start = System.nanoTime();
        Map<String, Object> props = triggerLoginListeners(loginReq, loginListeners);
        AuthenticationRequest authRequest = AuthenticationRequest.builder()
                                                                 .withLogin(loginReq.getLogin())
                                                                 .withPassword(loginReq.getPassword())
                                                                 .withClient(loginReq.getClient())
                                                                 .withClientIP(loginReq.getClientIP())
                                                                 .withUserAgent(loginReq.getUserAgent())
                                                                 .withProperties(props)
                                                                 .build();

        AuthenticationResult result = authenticationServiceRegistry.doLogin(authRequest, false);
        LOG.trace("Authentication probe for login \"{}\" finished with result {} [{} elapsed]", loginReq.getLogin(), result.getStatus(), DurationOutputter.startNanos(start));
        return SUCCESS.equals(result.getStatus()) ? result.optAuthenticated() : Optional.empty();
    }

    /**
     * Triggers the {@link LoginListener}s and returns a properties map of properties set by these listeners
     *
     * @param loginRequest The login request
     * @param loginListener The login listener
     * @return The properties map
     * @throws OXException if the login attempt is rendered as invalid
     */
    private static Map<String, Object> triggerLoginListeners(LoginRequest loginRequest, List<LoginListener> loginListener) throws OXException {
        HashMap<String, Object> result = HashMap.newHashMap(1);
        for (LoginListener listener : loginListener) {
            listener.onBeforeAuthentication(loginRequest, result);
        }
        return result;
    }
}
