/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.transport.config;

import org.slf4j.Logger;
import com.openexchange.config.AbstractCompositeReloadable;
import com.openexchange.config.ConfigurationService;

/**
 * {@link TransportReloadable} - Collects reloadables for transport module.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since 7.6.0
 */
public final class TransportReloadable extends AbstractCompositeReloadable {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(TransportReloadable.class);

    private static final TransportReloadable INSTANCE = new TransportReloadable();

    /**
     * Gets the instance.
     *
     * @return The instance
     */
    public static TransportReloadable getInstance() {
        return INSTANCE;
    }

    // --------------------------------------------------------------------------------------------------- //

    /**
     * Initializes a new {@link TransportReloadable}.
     */
    private TransportReloadable() {
        super();
    }

    @Override
    protected void preReloadConfiguration(ConfigurationService configService) {
        try {
            final TransportProperties transportProperties = TransportProperties.getInstance();
            if (null != transportProperties) {
                transportProperties.resetProperties();
                transportProperties.loadProperties();
            }
        } catch (Exception e) {
            LOGGER.warn("Failed to reload transport properties", e);
        }
    }

    @Override
    protected void postReloadConfiguration(ConfigurationService configService) {
        // Nothing
    }

    @Override
    protected String[] getFileNamesOfInterest() {
        return new String[] { "transport.properties" };
    }

    @Override
    protected String[] getPropertiesOfInterest() {
        return new String[] { "com.openexchange.mail.transport.*" };
    }

}
