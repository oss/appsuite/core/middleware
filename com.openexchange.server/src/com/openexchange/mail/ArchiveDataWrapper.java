/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail;

import java.util.Map;

/**
 * A simple wrapper class for archive actions.
 *
 * @since v7.10.4
 */
public class ArchiveDataWrapper {

    private final String folderId;
    private final boolean created;
    private final Map<FolderAndId, String> mailLocations;

    /**
     * Initializes a new {@link ArchiveDataWrapper}.
     *
     * @param folderId The identifier of the (sub-)archive folder; e.g. <code>"default0/Archive/2018"</code>
     * @param created <code>true</code> if folder has been created; otherwise <code>false</code>
     * @param mailLocations The mail location mappings; e.g. <code>"default0/INBOX/122 -&gt; 1"</code>
     */
    public ArchiveDataWrapper(String folderId, boolean created, Map<FolderAndId, String> mailLocations) {
        super();
        this.folderId = folderId;
        this.created = created;
        this.mailLocations = mailLocations == null ? null : Map.copyOf(mailLocations);
    }

    /**
     * Gets the identifier of the (sub-)archive folder.
     *
     * @return The identifier
     */
    public String getFolderId() {
        return folderId;
    }

    /**
     * Signals whether that folder has been created or not.
     *
     * @return <code>true</code> if created; otherwise <code>false</code>
     */
    public boolean isCreated() {
        return created;
    }

    /**
     * Gets the mail location mappings; e.g. <code>"default0/INBOX/122 -&gt; 1"</code>.
     *
     * @return The mail location mappings
     */
    public Map<FolderAndId, String> getMailLocations() {
        return mailLocations; // NOSONARLINT
    }

}