/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.mail.api;

import com.openexchange.exception.OXException;
import com.openexchange.mail.IndexRange;
import com.openexchange.mail.dataobjects.SyncToken;
import com.openexchange.mail.dataobjects.SynchronizationResult;

/**
 * {@link IMailMessageStorageSyncSupport} - Extends basic message storage by retrieving the synchronization result for a specified mail folder.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public interface IMailMessageStorageSyncSupport {

    /**
     * Indicates if mailbox synchronization is supported.
     *
     * @return <code>true</code> if supported; otherwise <code>false</code>
     * @throws OXException If check fails
     */
    boolean isSyncSupported() throws OXException;

    /**
     * Gets the synchronization result for specified mail folder's full name and synchronization token.
     *
     * @param fullName The folder full name
     * @param optSyncToken The synchronization token provided by client or <code>null</code>
     * @param indexRange The index range or {@link IndexRange#NULL}
     * @param optKnownMailIds The client's known mail identifiers or <code>null</code>
     * @return The synchronization result
     * @throws OXException If an error occurs
     */
    SynchronizationResult getSynchronizationResult(String fullName, SyncToken optSyncToken, String[] optKnownMailIds) throws OXException;

}
