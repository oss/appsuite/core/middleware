/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.api;

import com.openexchange.exception.OXException;

/**
 * {@link IMailFolderStorageSubscription} - Allows easy access for subscribing/un-subscribing mail folders.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public interface IMailFolderStorageSubscription {

    /**
     * Checks if this instance supports {@link #subscribe(String, boolean, boolean)} method.
     *
     * @return <code>true</code> if supported; otherwise <code>false</code>
     * @throws OXException If check fails
     */
    boolean isSubscribeSupported() throws OXException;

    /**
     * Deletes an existing mail folder identified through given full name.
     * This method tries to move the folder to trash first, before deleting it permanently.
     *
     * @param fullName The full name of the mail folder to subscribe/un-subscribe
     * @param subscribe <code>true</code> to subscribe or <code>false</code> to un-subscribe
     * @param errorOnFailure <code>true</code> to throw an appropriate exception; otherwise <code>false</code> to ignore a possible failure attempting to subscribe/un-subscribe the mail folder
     * @return <code>true</code> if (un-)subscription was successful; otherwise <code>false</code>
     * @throws OXException If subscribing/un-subscribing mail folder fails
     */
    boolean subscribe(String fullName, boolean subscribe, boolean errorOnFailure) throws OXException;

}
