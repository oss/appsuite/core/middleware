/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.api;

import com.openexchange.exception.OXException;

/**
 * {@link IMailFolderStorageTrashAware} - Moves existent folders to special trash folder.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v7.8.4
 */
public interface IMailFolderStorageTrashAware {

    /**
     * Deletes an existing mail folder identified through given full name.
     * <p>
     * This method tries to move the folder to trash first, before deleting it permanently.
     *
     * @param fullName The full name of the mail folder to delete
     * @return The full name of the new folder or <code>null</code> if hard-deleted
     * @throws OXException If either folder does not exist or cannot be deleted
     */
    String trashFolder(String fullName) throws OXException;

}
