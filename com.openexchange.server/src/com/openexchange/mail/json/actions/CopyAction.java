/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.json.actions;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.fields.DataFields;
import com.openexchange.ajax.fields.FolderChildFields;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.UndoableAJAXActionService;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.json.MailRequest;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.DefaultUndoOperation;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.UndoToken;

/**
 * {@link CopyAction}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@RestrictedAction(module = AbstractMailAction.MODULE, type = RestrictedAction.Type.WRITE)
public final class CopyAction extends AbstractMailAction implements UndoableAJAXActionService {

    /**
     * Initializes a new {@link CopyAction}.
     *
     * @param services
     */
    public CopyAction(ServiceLookup services) {
        super(services);
    }

    @Override
    protected AJAXRequestResult perform(MailRequest req) throws OXException {
        try {
            //Read in parameters
            String uid = req.checkParameter(AJAXServlet.PARAMETER_ID);
            String sourceFolder = req.checkParameter(AJAXServlet.PARAMETER_FOLDERID);
            String destFolder = ((JSONObject) req.getRequest().requireData()).getString(FolderChildFields.FOLDER_ID);
            long undoTimeToLive = UndoableAJAXActionService.getUndoTimeToLiveFrom(req.getRequest());

            // Get mail interface
            MailServletInterface mailInterface = getMailInterface(req);
            String msgUID = mailInterface.copyMessages(sourceFolder, destFolder, new String[] { uid }, false)[0];

            // Prepare undo
            DefaultUndoOperation.Builder undoOperation = null;
            if (undoTimeToLive > 0) {
                undoOperation = DefaultUndoOperation.builder();
                undoOperation.withModule("mail");
                undoOperation.withAction("delete");
                Map<String, String> parameters = HashMap.newHashMap(3);
                parameters.put(AJAXServlet.PARAMETER_HARDDELETE, "true");
                parameters.put("returnAffectedFolders", "false");
                JSONArray jArray = Arrays.stream(new String[] { msgUID }).map(id -> toJson(id, destFolder)).collect(CollectorUtils.toJsonArray());
                undoOperation.withData(jArray);
            }

            JSONObject data = new JSONObject(2);
            data.put(FolderChildFields.FOLDER_ID, destFolder);
            data.put(DataFields.ID, msgUID);

            AJAXRequestResult requestResult = new AJAXRequestResult(data, "json");

            if (undoOperation != null) {
                UndoService undoService = getService(UndoService.class);
                if (undoService != null) {
                    UndoToken undoToken = undoService.registerUndoOperation(undoOperation.build(), undoTimeToLive, req.getSession());
                    requestResult.setUndoTokenIfNotEmpty(undoToken);
                }
            }

            return requestResult;
        } catch (JSONException e) {
            throw MailExceptionCode.JSON_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    private static Object toJson(String id, String destFolder) {
        if (id != null) {
            return new JSONObject(2).putSafe(AJAXServlet.PARAMETER_FOLDERID, destFolder).putSafe(AJAXServlet.PARAMETER_ID, id);
        }
        return JSONObject.NULL;
    }

    @Override
    public UndoableAJAXActionService.Result isUndoable(AJAXRequestData request, ServerSession session) throws OXException {
        UndoService undoService = getService(UndoService.class);
        if (undoService == null) {
            return UndoableAJAXActionService.resultFor(false, this);
        }
        return UndoableAJAXActionService.resultFor(true, this);
    }

}
