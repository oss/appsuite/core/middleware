/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.json.actions;

import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.fields.DataFields;
import com.openexchange.ajax.fields.FolderChildFields;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.UndoableAJAXActionService;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailListField;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.json.MailRequest;
import com.openexchange.preferences.ServerUserSetting;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.DefaultUndoOperation;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.UndoToken;

/**
 * {@link MoveAction}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@RestrictedAction(module = AbstractMailAction.MODULE, type = RestrictedAction.Type.WRITE)
public class MoveAction extends AbstractModifyingMailAction implements UndoableAJAXActionService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(MoveAction.class);

    /**
     * Initializes a new {@link MoveAction}.
     *
     * @param services
     */
    public MoveAction(ServiceLookup services) {
        super(services);
    }

    @Override
    protected AJAXRequestResult perform(MailRequest req) throws OXException {
        try {
            // Read in parameters
            JSONObject jBody = (JSONObject) req.getRequest().requireData();
            String[] flds = getSourceAndDestinationFolderFrom(jBody);
            String sourceFolder = flds[0];
            String destFolder = flds[1];

            return performMove(sourceFolder, destFolder, jBody, true, req);
        } catch (JSONException e) {
            throw MailExceptionCode.JSON_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Determines the source and destination folder from given JSON request body.
     *
     * @param jBody The JSON request body
     * @return The source and destination folder
     * @throws JSONException If a JSON error occurs
     */
    protected static String[] getSourceAndDestinationFolderFrom(JSONObject jBody) throws JSONException {
        String sourceFolder = jBody.optString("source", null);
        if (null == sourceFolder) {
            sourceFolder = jBody.getString("from");
        }
        String destFolder = jBody.optString("target", null);
        if (null == destFolder) {
            destFolder = jBody.getString("to");
        }
        return new String[] { sourceFolder, destFolder };
    }

    /**
     * Performs the move operation.
     *
     * @param sourceFolder The source folder from which to move mails
     * @param destFolder The destination folder as target for moved mails
     * @param jBody The JSON request body
     * @param expectIds <code>true</code> to expect mail identifiers in JSON request body; otherwise <code>false</code>
     * @param req The mail request
     * @return The result
     * @throws OXException If move operation fails
     */
    protected AJAXRequestResult performMove(String sourceFolder, String destFolder, JSONObject jBody, boolean expectIds, MailRequest req) throws OXException {
        try {
            // Get mail interface
            MailServletInterface mailInterface = getMailInterface(req);
            String[] ids = getMailIdsOrAll(expectIds ? jBody : null, sourceFolder, mailInterface);
            boolean collectAddresses = jBody.optBoolean("collect_addresses", false);
            long undoTimeToLive = UndoableAJAXActionService.getUndoTimeToLiveFrom(req.getRequest());

            // Move messages
            String[] destIds;
            DefaultUndoOperation.Builder undoOperation = null;
            if (sourceFolder.equals(destFolder)) {
                // Nothing to move if source and destination are equal
                destIds = ids;
            } else {
                destIds = mailInterface.copyMessages(sourceFolder, destFolder, ids, true);

                // Prepare undo
                if (undoTimeToLive > 0) {
                    undoOperation = DefaultUndoOperation.builder();
                    undoOperation.withModule("mail");
                    undoOperation.withAction("move");
                    JSONObject undoData = new JSONObject(3);
                    undoData.put("source", destFolder);
                    undoData.put("target", sourceFolder);
                    undoData.put("ids", Arrays.stream(destIds).collect(CollectorUtils.toJsonArray(destIds.length)));
                    undoOperation.withData(undoData);
                }
            }

            if (collectAddresses) {
                // Trigger contact collector
                try {
                    ServerSession session = req.getSession();
                    boolean memorizeAddresses = ServerUserSetting.getInstance().isContactCollectOnMailAccess(session.getContextId(), session.getUserId()).booleanValue();
                    if (memorizeAddresses) {
                        MailMessage[] addresses = mailInterface.getMessageList(destFolder, destIds, new int[] { MailListField.FROM.getField(), MailListField.TO.getField(), MailListField.CC.getField(), MailListField.BCC.getField() }, null);
                        triggerContactCollector(session, Arrays.asList(addresses), true, false);
                    }
                } catch (Exception e) {
                    LOG.warn("Contact collector could not be triggered.", e);
                }
            }

            String dstFld = destFolder;
            JSONArray jData = Arrays.stream(destIds).map(mailId -> new JSONObject(2).putSafe(FolderChildFields.FOLDER_ID, dstFld).putSafe(DataFields.ID, mailId)).collect(CollectorUtils.toJsonArray(destIds.length));
            AJAXRequestResult requestResult = new AJAXRequestResult(jData, "json");

            if (undoOperation != null) {
                UndoService undoService = getService(UndoService.class);
                if (undoService != null) {
                    UndoToken undoToken = undoService.registerUndoOperation(undoOperation.build(), undoTimeToLive, req.getSession());
                    requestResult.setUndoTokenIfNotEmpty(undoToken);
                }
            }

            return requestResult;
        } catch (JSONException e) {
            throw MailExceptionCode.JSON_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public UndoableAJAXActionService.Result isUndoable(AJAXRequestData request, ServerSession session) throws OXException {
        UndoService undoService = getService(UndoService.class);
        return UndoableAJAXActionService.resultFor(undoService != null, this);
    }

}
