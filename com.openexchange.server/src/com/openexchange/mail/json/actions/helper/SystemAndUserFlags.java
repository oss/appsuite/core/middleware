/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.json.actions.helper;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * {@link SystemAndUserFlags} - A tuple for system and user flags of a mail that can be used as key for a map.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class SystemAndUserFlags implements Comparable<SystemAndUserFlags> {

    private final int flags;
    private final TreeSet<String> userFlags;
    private final int hash;

    /**
     * Initializes a new instance of {@link CombinedFlags}.
     *
     * @param flags The system flags
     * @param userFlags The user flags
     */
    public SystemAndUserFlags(int flags, Set<String> userFlags) {
        super();
        this.flags = flags;
        this.userFlags = new TreeSet<>(userFlags);
        int prime = 31;
        int result = 1;
        result = prime * result + flags;
        result = prime * result + ((userFlags == null) ? 0 : userFlags.hashCode());
        this.hash = result;
    }

    /**
     * Gets the (system) flags.
     *
     * @return The flags
     */
    public int getFlags() {
        return flags;
    }

    /**
     * Gets the user flags.
     *
     * @return The user flags
     */
    public Set<String> getUserFlags() {
        return userFlags;
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SystemAndUserFlags other = (SystemAndUserFlags) obj;
        if (flags != other.flags) {
            return false;
        }
        if (userFlags == null) {
            if (other.userFlags != null) {
                return false;
            }
        } else if (!userFlags.equals(other.userFlags)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(SystemAndUserFlags o) {
        int c = Integer.compare(flags, o.flags);
        return c != 0 ? c : compare(userFlags, o.userFlags);
    }

    /**
     * Compares the specified sets.
     *
     * @param userFlags The first set of user flags
     * @param otherUserFlags The other set of user flags
     * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object
     */
    private static int compare(TreeSet<String> userFlags, TreeSet<String> otherUserFlags) {
        int c = Integer.compare(userFlags.size(), otherUserFlags.size());
        if (c != 0) {
            return c;
        }

        Iterator<String> otherIter = otherUserFlags.iterator();
        for (String userFlag : userFlags) {
            String otherUserFlag = otherIter.next();
            c = userFlag.compareTo(otherUserFlag);
            if (c != 0) {
                return c;
            }
        }

        return 0;
    }

}
