/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.mail.json.actions;

import java.util.Arrays;
import org.json.ImmutableJSONObject;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.java.Functions;
import com.openexchange.mail.MailField;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.MailSortField;
import com.openexchange.mail.OrderDirection;
import com.openexchange.server.ServiceLookup;


/**
 * {@link AbstractModifyingMailAction} - Abstract class for modifying mail actions.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public abstract class AbstractModifyingMailAction extends AbstractMailAction {

    /** The standard JSON response if everything went well: <code>{"success": true}</code> */
    protected static final JSONObject JSON_RESPONSE_SUCCESS = ImmutableJSONObject.immutableFor(new JSONObject(1).putSafe("success", Boolean.TRUE));

    /**
     * Initializes a new instance of {@link AbstractModifyingMailAction}.
     *
     * @param services The service look-up
     */
    protected AbstractModifyingMailAction(ServiceLookup services) {
        super(services);
    }

    /**
     * Gets the provided mail identifiers from given JSON body or queries all available mail identifiers from folder.
     *
     * @param optJBody The JSON body or <code>null</code> to enforce querying all mail identifiers
     * @param folder The folder
     * @param mailInterface The mail interface to use
     * @return The mail identifiers
     * @throws OXException If mail identifiers cannot be queried
     */
    protected static String[] getMailIdsOrAll(JSONObject optJBody, String folder, MailServletInterface mailInterface) throws OXException {
        JSONArray jIds = optJBody == null ? null : optJBody.optJSONArray("ids");
        if (jIds != null) {
            return jIds.stream().map(Object::toString).toArray(Functions.getNewStringArrayIntFunction());
        }

        return Arrays.stream(mailInterface.searchMails(folder, null, MailSortField.RECEIVED_DATE, OrderDirection.DESC, null, new MailField[] { MailField.ID }, null))
                     .map(m -> m.getMailId())
                     .toArray(Functions.getNewStringArrayIntFunction());
    }

}
