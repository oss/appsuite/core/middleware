/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.json.actions;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestDataTools;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.UndoableAJAXActionService;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXRuntimeException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Functions;
import com.openexchange.java.Strings;
import com.openexchange.mail.ArchiveDataWrapper;
import com.openexchange.mail.FolderAndId;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.config.MailProperties;
import com.openexchange.mail.json.MailRequest;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.DefaultUndoOperation;
import com.openexchange.undo.UndoOperation;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.UndoToken;

/**
 * {@link ArchiveFolderAction}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
@RestrictedAction(module = AbstractMailAction.MODULE, type = RestrictedAction.Type.WRITE)
public final class ArchiveFolderAction extends AbstractArchiveMailAction implements UndoableAJAXActionService {

    /**
     * Initializes a new {@link ArchiveFolderAction}.
     *
     * @param services
     */
    public ArchiveFolderAction(ServiceLookup services) {
        super(services);
    }

    @SuppressWarnings("null")
    @Override
    protected AJAXRequestResult performArchive(MailRequest req) throws OXException {
        ServerSession session = req.getSession();
        int days;
        {
            String sDays = req.getRequest().getParameter("days");
            days = Strings.isEmpty(sDays) ? MailProperties.getInstance().getDefaultArchiveDays(session.getUserId(), session.getContextId()) : Strings.parsePositiveInt(sDays.trim());
        }
        try {
            /*
             * Read in parameters
             */
            final String folderId = req.checkParameter(AJAXServlet.PARAMETER_FOLDERID);
            /*
             * Get mail interface
             */
            final MailServletInterface mailInterface = getMailInterface(req);
            boolean useDefaultName = AJAXRequestDataTools.parseBoolParameter("useDefaultName", req.getRequest(), true);
            boolean createIfAbsent = AJAXRequestDataTools.parseBoolParameter("createIfAbsent", req.getRequest(), true);
            long undoTimeToLive = UndoableAJAXActionService.getUndoTimeToLiveFrom(req.getRequest());

            // Archive mails
            List<ArchiveDataWrapper> retval = mailInterface.archiveMailFolder(days, folderId, session, useDefaultName, createIfAbsent);

            // Prepare undo
            List<UndoOperation> undoOperations = null;
            if (undoTimeToLive > 0 && !retval.isEmpty()) {
                undoOperations = new ArrayList<>(retval.size());
                for (ArchiveDataWrapper adw : retval) {
                    String archiveFolder = adw.getFolderId();
                    Map<String, List<String>> rollbacks = new LinkedHashMap<>();
                    for (Map.Entry<FolderAndId, String> e : adw.getMailLocations().entrySet()) {
                        FolderAndId source = e.getKey();
                        String archiveMailId = e.getValue();

                        rollbacks.computeIfAbsent(source.getFolderId(), Functions.getNewArrayListFuntion()).add(archiveMailId);
                    }

                    for (Map.Entry<String, List<String>> e : rollbacks.entrySet()) {
                        String destFolder = e.getKey();
                        List<String> ids = e.getValue();

                        DefaultUndoOperation.Builder undoOperation = DefaultUndoOperation.builder();
                        undoOperation.withModule("mail");
                        undoOperation.withAction("move");
                        JSONObject undoData = new JSONObject(3);
                        undoData.put("source", archiveFolder);
                        undoData.put("target", destFolder);
                        undoData.put("ids", ids.stream().collect(CollectorUtils.toJsonArray(ids.size())));
                        undoOperation.withData(undoData);
                        undoOperations.add(undoOperation.build());
                    }
                    rollbacks = null; // Might help GC NOSONARLINT
                }
            }

            AJAXRequestResult requestResult = new AJAXRequestResult(Boolean.TRUE, "native");

            if (undoOperations != null) {
                UndoService undoService = getService(UndoService.class);
                if (undoService != null) {
                    UndoToken undoToken = undoService.registerUndoOperations(undoOperations, undoTimeToLive, req.getSession());
                    requestResult.setUndoTokenIfNotEmpty(undoToken);
                }
            }

            return requestResult;
        } catch (JSONException e) {
            throw MailExceptionCode.JSON_ERROR.create(e, e.getMessage());
        } catch (OXRuntimeException e) { // NOSONARLINT
            throw e.getException();
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public Result isUndoable(AJAXRequestData request, ServerSession session) throws OXException {
        UndoService undoService = getService(UndoService.class);
        if (undoService == null) {
            return UndoableAJAXActionService.resultFor(false, this);
        }
        return UndoableAJAXActionService.resultFor(true, this);
    }

}
