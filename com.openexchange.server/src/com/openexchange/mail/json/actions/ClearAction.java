/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.json.actions;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestDataTools;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.EnqueuableAJAXActionService;
import com.openexchange.ajax.requesthandler.UndoableAJAXActionService;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.ajax.requesthandler.jobqueue.JobKey;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Functions;
import com.openexchange.java.Predicates;
import com.openexchange.java.Strings;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailField;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.MailSortField;
import com.openexchange.mail.OrderDirection;
import com.openexchange.mail.json.MailRequest;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.DefaultUndoOperation;
import com.openexchange.undo.UndoOperation;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.UndoToken;

/**
 * {@link ClearAction}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@RestrictedAction(module = AbstractMailAction.MODULE, type = RestrictedAction.Type.WRITE)
public final class ClearAction extends AbstractMailAction implements EnqueuableAJAXActionService, UndoableAJAXActionService {

    /**
     * Initializes a new {@link ClearAction}.
     *
     * @param services The service look-up
     */
    public ClearAction(ServiceLookup services) {
        super(services);
    }

    @Override
    protected AJAXRequestResult perform(MailRequest req) throws OXException {
        try {
            // Read in parameters
            JSONArray ja = (JSONArray) req.getRequest().requireData();
            boolean hardDelete = req.optBool(AJAXServlet.PARAMETER_HARDDELETE, false);
            long undoTimeToLive = UndoableAJAXActionService.getUndoTimeToLiveFrom(req.getRequest());

            // Preparations
            MailServletInterface mailInterface = getMailInterface(req);

            List<UndoOperation> undoOperations = null;
            if (!hardDelete && undoTimeToLive > 0) {
                undoOperations = new LinkedList<>();
            }

            // Clear folders sequentially
            int length = ja.length();
            JSONArray ret = null;
            for (int i = 0; i < length; i++) {
                String folderId = ja.getString(i);
                if (undoOperations != null) {
                    // Query all mail identifiers from affected folder
                    String[] mailIds = Arrays.stream(mailInterface.searchMails(folderId, null, MailSortField.RECEIVED_DATE, OrderDirection.DESC, null, new MailField[] { MailField.ID }, null))
                        .filter(Predicates.isNotNullPredicate())
                        .map(m -> m.getMailId())
                        .toArray(Functions.getNewStringArrayIntFunction());

                    // Move mails to trash folder
                    String trashFolder = mailInterface.getTrashFolder(mailInterface.getAccountID());
                    String[] destIds = mailInterface.copyMessages(folderId, trashFolder, mailIds, true);

                    // Compile undo operation
                    DefaultUndoOperation.Builder undoOperation = DefaultUndoOperation.builder();
                    undoOperation.withModule("mail");
                    undoOperation.withAction("move");
                    JSONObject undoData = new JSONObject(3);
                    undoData.put("source", trashFolder);
                    undoData.put("target", folderId);
                    undoData.put("ids", Arrays.stream(destIds).collect(CollectorUtils.toJsonArray(true)));
                    undoOperation.withData(undoData);
                    undoOperations.add(undoOperation.build());
                } else {
                    if (!mailInterface.clearFolder(folderId, hardDelete)) {
                        // Something went wrong
                        if (ret == null) {
                            ret = new JSONArray();
                        }
                        ret.put(folderId);
                    }
                }
            }

            AJAXRequestResult requestResult = new AJAXRequestResult(ret == null ? JSONArray.EMPTY_ARRAY : ret, "json");

            if (undoOperations != null) {
                UndoService undoService = getService(UndoService.class);
                if (undoService != null) {
                    UndoToken undoToken = undoService.registerUndoOperations(undoOperations, undoTimeToLive, req.getSession());
                    requestResult.setUndoTokenIfNotEmpty(undoToken);
                }
            }

            return requestResult;
        } catch (JSONException e) {
            throw MailExceptionCode.JSON_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public EnqueuableAJAXActionService.Result isEnqueueable(AJAXRequestData request, ServerSession session) throws OXException {
        try {
            JSONObject jKeyDesc = new JSONObject(6);
            jKeyDesc.put("module", "mail");
            jKeyDesc.put("action", "clear");

            boolean hardDelete = AJAXRequestDataTools.parseBoolParameter(request.getParameter(AJAXServlet.PARAMETER_HARDDELETE));
            jKeyDesc.put(AJAXServlet.PARAMETER_HARDDELETE, hardDelete);

            JSONArray ja = (JSONArray) request.requireData();
            int length = ja.length();
            List<String> l = ja.stream().map(o -> o.toString()).collect(CollectorUtils.toList(length));
            Collections.sort(l);
            jKeyDesc.put("ids", getKeyObjectFor(l));

            return EnqueuableAJAXActionService.resultFor(true, new JobKey(session.getUserId(), session.getContextId(), jKeyDesc.toString()), this);
        } catch (JSONException e) {
            throw AjaxExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        }
    }

    private static Object getKeyObjectFor(List<String> l) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("md5");
            for (String folderId : l) {
                md5.update(folderId.getBytes(StandardCharsets.UTF_8));
            }
            return Strings.asHex(md5.digest());
        } catch (Exception e) {
            JSONArray jsonIds = new JSONArray(l.size());
            for (String folderId : l) {
                jsonIds.put(folderId);
            }
            return jsonIds;
        }
    }

    @Override
    public UndoableAJAXActionService.Result isUndoable(AJAXRequestData request, ServerSession session) throws OXException {
        UndoService undoService = getService(UndoService.class);
        if (undoService == null) {
            return UndoableAJAXActionService.resultFor(false, this);
        }
        boolean hardDelete = AJAXRequestDataTools.parseBoolParameter(request.getParameter(AJAXServlet.PARAMETER_HARDDELETE));
        return UndoableAJAXActionService.resultFor(!hardDelete, this);
    }

}
