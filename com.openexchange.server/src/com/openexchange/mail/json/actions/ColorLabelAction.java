/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.json.actions;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.fields.CommonFields;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.UndoableAJAXActionService;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXRuntimeException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Functions;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailListField;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.json.MailRequest;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.DefaultUndoOperation;
import com.openexchange.undo.UndoOperation;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.UndoToken;

/**
 * {@link ColorLabelAction}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@RestrictedAction(module = AbstractMailAction.MODULE, type = RestrictedAction.Type.WRITE)
public final class ColorLabelAction extends AbstractModifyingMailAction implements UndoableAJAXActionService {

    /**
     * Initializes a new {@link ColorLabelAction}.
     *
     * @param services
     */
    public ColorLabelAction(ServiceLookup services) {
        super(services);
    }

    @Override
    protected AJAXRequestResult perform(MailRequest req) throws OXException {
        try {
            // Read in parameters
            JSONObject jBody = (JSONObject) req.getRequest().requireData();
            String folder = jBody.getString(AJAXServlet.PARAMETER_FOLDERID);

            // Get mail interface
            MailServletInterface mailInterface = getMailInterface(req);
            String[] ids = getMailIdsOrAll(jBody, folder, mailInterface);
            int colorLabelToSet = jBody.getInt(CommonFields.COLORLABEL);

            long undoTimeToLive = UndoableAJAXActionService.getUndoTimeToLiveFrom(req.getRequest());

            // Prepare undo
            List<UndoOperation> undoOperations = null;
            if (undoTimeToLive > 0) {
                Map<Integer, List<String>> label22ids = new LinkedHashMap<>();
                MailMessage[] colorLabels = mailInterface.getMessageList(folder, ids, new int[] { MailListField.ID.getField(), MailListField.COLOR_LABEL.getField() }, null);
                for (MailMessage mail : colorLabels) {
                    int colorLabel = mail.getColorLabel();
                    if (colorLabel != colorLabelToSet) {
                        label22ids.computeIfAbsent(Integer.valueOf(colorLabel), Functions.getNewArrayListFuntion()).add(mail.getMailId());
                    }
                }

                undoOperations = new ArrayList<>(label22ids.size());
                for (Map.Entry<Integer, List<String>> e : label22ids.entrySet()) {
                    DefaultUndoOperation.Builder undoOperation = initUndoOperation();
                    JSONObject undoData = initUndoData(undoOperation);
                    undoData.put(AJAXServlet.PARAMETER_FOLDERID, folder);
                    undoData.put("ids", e.getValue().stream().collect(CollectorUtils.toJsonArray()));
                    undoData.put(CommonFields.COLORLABEL, e.getKey());
                    undoOperations.add(undoOperation.build());
                }
            }

            // Update color label
            mailInterface.updateMessageColorLabel(folder, ids, colorLabelToSet);

            AJAXRequestResult requestResult = new AJAXRequestResult(JSON_RESPONSE_SUCCESS, "json");

            if (undoOperations != null) {
                UndoService undoService = getService(UndoService.class);
                if (undoService != null) {
                    UndoToken undoToken = undoService.registerUndoOperations(undoOperations, undoTimeToLive, req.getSession());
                    requestResult.setUndoTokenIfNotEmpty(undoToken);
                }
            }

            return requestResult;
        } catch (JSONException e) {
            throw MailExceptionCode.JSON_ERROR.create(e, e.getMessage());
        } catch (OXRuntimeException e) { // NOSONARLINT
            throw e.getException();
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Initializes the undo operation for <code>/mail?action=flags</code> request.
     *
     * @param undoOperation The possibly pre-initialzed undo operation builder
     * @return The initialized undo operation
     */
    private static DefaultUndoOperation.Builder initUndoOperation() {
        DefaultUndoOperation.Builder newUndoOperation = DefaultUndoOperation.builder();
        newUndoOperation.withModule("mail");
        newUndoOperation.withAction("color_label");
        return newUndoOperation;
    }

    /**
     * Initializes the undo data for <code>/mail?action=flags</code> request.
     *
     * @param undoData The possibly pre-initialized undo data
     * @param undoOperation The initialized undo operation builder
     * @return The initialized undo data
     */
    private static JSONObject initUndoData(DefaultUndoOperation.Builder undoOperation) {
        JSONObject newUndoData = new JSONObject();
        undoOperation.withData(newUndoData);
        return newUndoData;
    }

    @Override
    public UndoableAJAXActionService.Result isUndoable(AJAXRequestData request, ServerSession session) throws OXException {
        UndoService undoService = getService(UndoService.class);
        if (undoService == null) {
            return UndoableAJAXActionService.resultFor(false, this);
        }
        return UndoableAJAXActionService.resultFor(true, this);
    }

}
