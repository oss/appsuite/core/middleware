/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.json.actions;

import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.Mail;
import com.openexchange.ajax.fields.DataFields;
import com.openexchange.ajax.fields.FolderChildFields;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestDataTools;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.UndoableAJAXActionService;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Functions;
import com.openexchange.mail.FullnameArgument;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailPath;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.IMailMessageStorageEnhancedDeletion;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.mail.json.MailRequest;
import com.openexchange.mail.utils.MailFolderUtility;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.DefaultUndoOperation;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.UndoToken;

/**
 * {@link TrashAction}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@RestrictedAction(module = AbstractMailAction.MODULE, type = RestrictedAction.Type.WRITE)
public final class TrashAction extends AbstractMailAction implements UndoableAJAXActionService {

    private final JSONArray emptyJsonArray;

    /**
     * Initializes a new {@link TrashAction}.
     *
     * @param services
     */
    public TrashAction(ServiceLookup services) {
        super(services);
        emptyJsonArray = JSONArray.EMPTY_ARRAY;
    }

    @Override
    protected AJAXRequestResult perform(MailRequest req) throws OXException {
        try {
            // Read in parameters
            String folderId = req.checkParameter(Mail.PARAMETER_MAILFOLDER);

            MailServletInterface mailInterface = getMailInterface(req);
            mailInterface.openFor(folderId);
            MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess = mailInterface.getMailAccess();

            IMailMessageStorage messageStorage = mailAccess.getMessageStorage();

            IMailMessageStorageEnhancedDeletion enhancedDeletionMessageStorage = messageStorage.supports(IMailMessageStorageEnhancedDeletion.class);
            if (null == enhancedDeletionMessageStorage) {
                throw MailExceptionCode.UNSUPPORTED_OPERATION.create();
            }

            if (!enhancedDeletionMessageStorage.isEnhancedDeletionSupported()) {
                throw MailExceptionCode.UNSUPPORTED_OPERATION.create();
            }

            boolean hardDelete = AJAXRequestDataTools.parseBoolParameter(req.getParameter(AJAXServlet.PARAMETER_HARDDELETE));
            JSONArray jsonIds = (JSONArray) req.getRequest().requireData();
            long undoTimeToLive = UndoableAJAXActionService.getUndoTimeToLiveFrom(req.getRequest());
            FullnameArgument fa = MailFolderUtility.prepareMailFolderParam(folderId);

            String[] mailIds = jsonIds.stream().map(o -> JSONObject.NULL.equals(o) ? null : o.toString()).toArray(Functions.getNewStringArrayIntFunction());
            MailPath[] mailPaths = enhancedDeletionMessageStorage.deleteMessagesEnhanced(fa.getFullName(), mailIds, hardDelete);
            if (null == mailPaths || mailPaths.length == 0) {
                return new AJAXRequestResult(emptyJsonArray, "json");
            }

            DefaultUndoOperation.Builder undoOperation = null;
            if (!hardDelete && undoTimeToLive > 0) {
                undoOperation = DefaultUndoOperation.builder();
                undoOperation.withModule("mail");
                undoOperation.withAction("move");
                JSONObject undoData = new JSONObject(3);
                undoData.put("source", MailFolderUtility.prepareFullname(fa.getAccountId(), mailAccess.getFolderStorage().getTrashFolder()));
                undoData.put("target", folderId);
                undoData.put("ids", Arrays.stream(mailPaths).map(MailPath::getMailID).collect(CollectorUtils.toJsonArray(true)));
                undoOperation.withData(undoData);
            }

            JSONArray jPaths = Arrays.stream(mailPaths).map(TrashAction::toJsonObject).collect(CollectorUtils.toJsonArray());
            AJAXRequestResult requestResult = new AJAXRequestResult(jPaths, "json");

            if (undoOperation != null) {
                UndoService undoService = getService(UndoService.class);
                if (undoService != null) {
                    UndoToken undoToken = undoService.registerUndoOperation(undoOperation.build(), undoTimeToLive, req.getSession());
                    requestResult.setUndoTokenIfNotEmpty(undoToken);
                }
            }

            return requestResult;
        } catch (JSONException e) {
            throw MailExceptionCode.JSON_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    private static JSONObject toJsonObject(MailPath mailPath) {
        if (mailPath == null) {
            return null;
        }
        return new JSONObject(2).putSafe(FolderChildFields.FOLDER_ID, mailPath.getFolderArgument()).putSafe(DataFields.ID, mailPath.getMailID());
    }

    @Override
    public UndoableAJAXActionService.Result isUndoable(AJAXRequestData request, ServerSession session) throws OXException {
        UndoService undoService = getService(UndoService.class);
        if (undoService == null) {
            return UndoableAJAXActionService.resultFor(false, this);
        }
        boolean hardDelete = AJAXRequestDataTools.parseBoolParameter(request.getParameter(AJAXServlet.PARAMETER_HARDDELETE));
        return UndoableAJAXActionService.resultFor(!hardDelete, this);
    }

}
