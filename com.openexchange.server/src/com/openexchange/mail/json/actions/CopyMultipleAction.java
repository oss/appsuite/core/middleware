/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.json.actions;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.fields.DataFields;
import com.openexchange.ajax.fields.FolderChildFields;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.UndoableAJAXActionService;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.json.MailRequest;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.DefaultUndoOperation;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.UndoToken;

/**
 * {@link CopyMultipleAction}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@RestrictedAction(module = AbstractMailAction.MODULE, type = RestrictedAction.Type.WRITE)
public class CopyMultipleAction extends AbstractModifyingMailAction implements UndoableAJAXActionService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CopyMultipleAction.class);

    /**
     * Initializes a new {@link CopyMultipleAction}.
     *
     * @param services
     */
    public CopyMultipleAction(ServiceLookup services) {
        super(services);
    }

    @Override
    protected AJAXRequestResult perform(MailRequest req) throws OXException {
        try {
            // Read in parameters
            JSONObject jBody = (JSONObject) req.getRequest().requireData();
            String[] flds = getSourceAndDestinationFolderFrom(jBody);
            String sourceFolder = flds[0];
            String destFolder = flds[1];

            return performCopy(sourceFolder, destFolder, jBody, req);
        } catch (JSONException e) {
            throw MailExceptionCode.JSON_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Determines the source and destination folder from given JSON request body.
     *
     * @param jBody The JSON request body
     * @return The source and destination folder
     * @throws JSONException If a JSON error occurs
     */
    protected static String[] getSourceAndDestinationFolderFrom(JSONObject jBody) throws JSONException {
        String sourceFolder = jBody.optString("source", null);
        if (null == sourceFolder) {
            sourceFolder = jBody.getString("from");
        }
        String destFolder = jBody.optString("target", null);
        if (null == destFolder) {
            destFolder = jBody.getString("to");
        }
        return new String[] { sourceFolder, destFolder };
    }

    /**
     * Performs the copy operation.
     *
     * @param sourceFolder The source folder from which to move mails
     * @param destFolder The destination folder as target for moved mails
     * @param jBody The JSON request body
     * @param req The mail request
     * @return The result
     * @throws OXException If move operation fails
     */
    protected AJAXRequestResult performCopy(String sourceFolder, String destFolder, JSONObject jBody, MailRequest req) throws OXException {
        try {
            // Get mail interface
            MailServletInterface mailInterface = getMailInterface(req);
            String[] ids = getMailIdsOrAll(jBody, sourceFolder, mailInterface);
            long undoTimeToLive = UndoableAJAXActionService.getUndoTimeToLiveFrom(req.getRequest());

            // Move messages
            String[] destIds = mailInterface.copyMessages(sourceFolder, destFolder, ids, false);

            // Prepare undo
            DefaultUndoOperation.Builder undoOperation = null;
            if (undoTimeToLive > 0) {
                undoOperation = DefaultUndoOperation.builder();
                undoOperation.withModule("mail");
                undoOperation.withAction("delete");
                Map<String, String> parameters = HashMap.newHashMap(3);
                parameters.put(AJAXServlet.PARAMETER_HARDDELETE, "true");
                parameters.put("returnAffectedFolders", "false");
                JSONArray jArray = Arrays.stream(destIds).map(id -> toJson(id, destFolder)).collect(CollectorUtils.toJsonArray(destIds.length));
                undoOperation.withData(jArray);
            }

            JSONArray jData = Arrays.stream(destIds).map(mailId -> toJson2(mailId, destFolder)).collect(CollectorUtils.toJsonArray(destIds.length));
            AJAXRequestResult requestResult = new AJAXRequestResult(jData, "json");

            if (undoOperation != null) {
                UndoService undoService = getService(UndoService.class);
                if (undoService != null) {
                    UndoToken undoToken = undoService.registerUndoOperation(undoOperation.build(), undoTimeToLive, req.getSession());
                    requestResult.setUndoTokenIfNotEmpty(undoToken);
                }
            }

            return requestResult;
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    private static Object toJson2(String mailId, String dstFld) {
        if (mailId != null) {
            return new JSONObject(2).putSafe(FolderChildFields.FOLDER_ID, dstFld).putSafe(DataFields.ID, mailId);
        }
        return JSONObject.NULL;
    }

    private static Object toJson(String id, String destFolder) {
        if (id != null) {
            return new JSONObject(2).putSafe(AJAXServlet.PARAMETER_FOLDERID, destFolder).putSafe(AJAXServlet.PARAMETER_ID, id);
        }
        return JSONObject.NULL;
    }

    @Override
    public UndoableAJAXActionService.Result isUndoable(AJAXRequestData request, ServerSession session) throws OXException {
        UndoService undoService = getService(UndoService.class);
        return UndoableAJAXActionService.resultFor(undoService != null, this);
    }

}
