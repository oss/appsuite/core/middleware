/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.json.actions;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestDataTools;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.UndoableAJAXActionService;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXRuntimeException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Functions;
import com.openexchange.mail.ArchiveDataWrapper;
import com.openexchange.mail.FolderAndId;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.json.MailRequest;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.DefaultUndoOperation;
import com.openexchange.undo.UndoOperation;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.UndoToken;

/**
 * {@link ArchiveAction}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
@RestrictedAction(module = AbstractMailAction.MODULE, type = RestrictedAction.Type.WRITE)
public final class ArchiveAction extends AbstractArchiveMailAction implements UndoableAJAXActionService {

    /**
     * Initializes a new {@link ArchiveAction}.
     *
     * @param services
     */
    public ArchiveAction(ServiceLookup services) {
        super(services);
    }

    @Override
    protected AJAXRequestResult performArchive(MailRequest req) throws OXException {
        try {
            // Read in parameters
            JSONArray jArray = ((JSONArray) req.getRequest().getData());
            if (null == jArray) {
                throw AjaxExceptionCodes.MISSING_REQUEST_BODY.create();
            }
            String optFolderId = req.getParameter(AJAXServlet.PARAMETER_FOLDERID);
            boolean useDefaultName = AJAXRequestDataTools.parseBoolParameter("useDefaultName", req.getRequest(), true);
            boolean createIfAbsent = AJAXRequestDataTools.parseBoolParameter("createIfAbsent", req.getRequest(), true);
            long undoTimeToLive = UndoableAJAXActionService.getUndoTimeToLiveFrom(req.getRequest());

            // Archive mails
            List<ArchiveDataWrapper> retval = archive(jArray, optFolderId, useDefaultName, createIfAbsent, req);

            // Prepare undo
            List<UndoOperation> undoOperations = null;
            if (undoTimeToLive > 0 && !retval.isEmpty()) {
                undoOperations = new ArrayList<>(retval.size());
                for (ArchiveDataWrapper adw : retval) {
                    String archiveFolder = adw.getFolderId();
                    Map<String, List<String>> rollbacks = new LinkedHashMap<>();
                    for (Map.Entry<FolderAndId, String> e : adw.getMailLocations().entrySet()) {
                        FolderAndId source = e.getKey();
                        String archiveMailId = e.getValue();

                        rollbacks.computeIfAbsent(source.getFolderId(), Functions.getNewArrayListFuntion()).add(archiveMailId);
                    }

                    for (Map.Entry<String, List<String>> e : rollbacks.entrySet()) {
                        String destFolder = e.getKey();
                        List<String> ids = e.getValue();

                        DefaultUndoOperation.Builder undoOperation = DefaultUndoOperation.builder();
                        undoOperation.withModule("mail");
                        undoOperation.withAction("move");
                        JSONObject undoData = new JSONObject(3);
                        undoData.put("source", archiveFolder);
                        undoData.put("target", destFolder);
                        undoData.put("ids", ids.stream().collect(CollectorUtils.toJsonArray(ids.size())));
                        undoOperation.withData(undoData);
                        undoOperations.add(undoOperation.build());
                    }
                    rollbacks = null; // Might help GC NOSONARLINT
                }
            }

            JSONArray jResponse = retval.stream().map(ArchiveAction::toJsonObject).collect(CollectorUtils.toJsonArray(retval.size()));
            AJAXRequestResult requestResult = new AJAXRequestResult(jResponse, "json");

            if (undoOperations != null) {
                UndoService undoService = getService(UndoService.class);
                if (undoService != null) {
                    UndoToken undoToken = undoService.registerUndoOperations(undoOperations, undoTimeToLive, req.getSession());
                    requestResult.setUndoTokenIfNotEmpty(undoToken);
                }
            }

            return requestResult;
        } catch (JSONException e) {
            throw MailExceptionCode.JSON_ERROR.create(e, e.getMessage());
        } catch (OXRuntimeException e) { // NOSONARLINT
            throw e.getException();
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    private static JSONObject toJsonObject(ArchiveDataWrapper adw) {
        return new JSONObject(2).putSafe("id", adw.getFolderId()).putSafe("created", Boolean.valueOf(adw.isCreated()));
    }

    private List<ArchiveDataWrapper> archive(JSONArray jArray, String optFolderId, boolean useDefaultName, boolean createIfAbsent, MailRequest req) throws OXException {
        ServerSession session = req.getSession();
        int length = jArray.length();
        if (optFolderId == null) {
            // Archive mails
            List<FolderAndId> paraList = jArray.stream().map(o -> toFolderAndId((JSONObject) o)).collect(CollectorUtils.toList(length));
            MailServletInterface mailInterface = getMailInterface(req);
            return mailInterface.archiveMultipleMail(paraList, session, useDefaultName, createIfAbsent);
        }

        // Archive mails
        List<String> ids = jArray.stream().map(Object::toString).collect(CollectorUtils.toList(length));
        MailServletInterface mailInterface = getMailInterface(req);
        return mailInterface.archiveMail(optFolderId, ids, session, useDefaultName, createIfAbsent);
    }

    private static FolderAndId toFolderAndId(JSONObject jObject) {
        try {
            String folder = jObject.getString(AJAXServlet.PARAMETER_FOLDERID);
            String id = jObject.getString(AJAXServlet.PARAMETER_ID);
            return new FolderAndId(folder, id);
        } catch (JSONException e) {
            throw new OXRuntimeException(MailExceptionCode.JSON_ERROR.create(e, e.getMessage()));
        }
    }

    @Override
    public Result isUndoable(AJAXRequestData request, ServerSession session) throws OXException {
        UndoService undoService = getService(UndoService.class);
        if (undoService == null) {
            return UndoableAJAXActionService.resultFor(false, this);
        }
        return UndoableAJAXActionService.resultFor(true, this);
    }

}
