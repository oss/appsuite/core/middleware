/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.json.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.UndoableAJAXActionService;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXRuntimeException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Functions;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailListField;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.json.MailRequest;
import com.openexchange.mail.json.actions.helper.SystemAndUserFlags;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.DefaultUndoOperation;
import com.openexchange.undo.UndoOperation;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.UndoToken;

/**
 * {@link FlagsAction}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@RestrictedAction(module = AbstractMailAction.MODULE, type = RestrictedAction.Type.WRITE)
public final class FlagsAction extends AbstractModifyingMailAction implements UndoableAJAXActionService {

    private static final String FIELD_SET_FLAGS = "set_flags";
    private static final String FIELD_CLEAR_FLAGS = "clear_flags";
    private static final String FIELD_SET_USER_FLAGS = "set_user_flags";
    private static final String FIELD_CLEAR_USER_FLAGS = "clear_user_flags";

    /**
     * Initializes a new {@link FlagsAction}.
     *
     * @param services
     */
    public FlagsAction(ServiceLookup services) {
        super(services);
    }

    private static final int[] SYSTEM_FLAGS = MailMessage.getAllKnownFlags();

    @Override
    protected AJAXRequestResult perform(MailRequest req) throws OXException {
        try {
            // Read in parameters
            JSONObject jBody = (JSONObject) req.getRequest().requireData();
            String folder = jBody.getString(AJAXServlet.PARAMETER_FOLDERID);

            // Get mail interface
            MailServletInterface mailInterface = getMailInterface(req);
            String[] ids = getMailIdsOrAll(jBody, folder, mailInterface);
            int setFlags = jBody.optInt(FIELD_SET_FLAGS, -1);
            int clearFlags = jBody.optInt(FIELD_CLEAR_FLAGS, -1);
            String[] setUserFlags = getStringArrayFor(FIELD_SET_USER_FLAGS, jBody);
            String[] clearUserFlags = getStringArrayFor(FIELD_CLEAR_USER_FLAGS, jBody);
            long undoTimeToLive = UndoableAJAXActionService.getUndoTimeToLiveFrom(req.getRequest());

            // Set messages' flags
            List<UndoOperation> undoOperations = null;
            if (setUserFlags != null) {
                if (setFlags < 0) {
                    // No system flags to add
                    setFlags = 0;
                }

                // Prepare undo
                if (undoTimeToLive > 0) {
                    Map<SystemAndUserFlags, List<String>> flags2ids = new LinkedHashMap<>(); // NOSONARLINT
                    MailMessage[] flags = mailInterface.getMessageList(folder, ids, new int[] { MailListField.ID.getField(), MailListField.FLAGS.getField(), MailListField.USER_FLAGS.getField() }, null);
                    for (MailMessage mail : flags) {
                        int systemFlags = mail.getFlags();
                        Set<String> userFlags = mail.getUserFlagsAsSet();

                        int clear = 0;
                        for (int systemFlag : SYSTEM_FLAGS) {
                            if ((setFlags & systemFlag) > 0 && (systemFlags & systemFlag) <= 0) {
                                // Answered flag shall be cleared
                                clear |= systemFlag;
                            }
                        }

                        Set<String> userClear = null;
                        for (String userFlag : setUserFlags) {
                            if (!userFlags.contains(userFlag)) {
                                if (userClear == null) {
                                    userClear = HashSet.newHashSet(setUserFlags.length);
                                }
                                userClear.add(userFlag);
                            }
                        }

                        flags2ids.computeIfAbsent(new SystemAndUserFlags(clear, userClear), Functions.getNewArrayListFuntion()).add(mail.getMailId());
                    }

                    undoOperations = new ArrayList<>(flags2ids.size());
                    for (Map.Entry<SystemAndUserFlags, List<String>> e : flags2ids.entrySet()) {
                        DefaultUndoOperation.Builder undoOperation = initUndoOperation();
                        JSONObject undoData = initUndoData(undoOperation);
                        undoData.put(AJAXServlet.PARAMETER_FOLDERID, folder);
                        undoData.put("ids", e.getValue().stream().collect(CollectorUtils.toJsonArray()));
                        undoData.put(FIELD_CLEAR_USER_FLAGS, e.getKey().getUserFlags().stream().collect(CollectorUtils.toJsonArray()));
                        undoData.put(FIELD_CLEAR_FLAGS, e.getKey().getFlags());
                        undoOperations.add(undoOperation.build());
                    }
                }

                // Set system and user flags
                mailInterface.updateMessageFlags(folder, ids, setFlags, setUserFlags, true);
            } else if (setFlags >= 0) {
                // Prepare undo
                if (undoTimeToLive > 0) {
                    Map<SystemAndUserFlags, List<String>> flags2ids = new LinkedHashMap<>(); // NOSONARLINT
                    MailMessage[] flags = mailInterface.getMessageList(folder, ids, new int[] { MailListField.ID.getField(), MailListField.FLAGS.getField() }, null);
                    for (MailMessage mail : flags) {
                        int systemFlags = mail.getFlags();

                        int clear = 0;
                        for (int systemFlag : SYSTEM_FLAGS) {
                            if ((setFlags & systemFlag) > 0 && (systemFlags & systemFlag) <= 0) {
                                // Answered flag shall be cleared
                                clear |= systemFlag;
                            }
                        }

                        flags2ids.computeIfAbsent(new SystemAndUserFlags(clear, Collections.emptySet()), Functions.getNewArrayListFuntion()).add(mail.getMailId());
                    }

                    undoOperations = new ArrayList<>(flags2ids.size());
                    for (Map.Entry<SystemAndUserFlags, List<String>> e : flags2ids.entrySet()) {
                        DefaultUndoOperation.Builder undoOperation = initUndoOperation();
                        JSONObject undoData = initUndoData(undoOperation);
                        undoData.put(AJAXServlet.PARAMETER_FOLDERID, folder);
                        undoData.put("ids", e.getValue().stream().collect(CollectorUtils.toJsonArray()));
                        undoData.put(FIELD_CLEAR_FLAGS, e.getKey().getFlags());
                        undoOperations.add(undoOperation.build());
                    }
                }

                // Set system flags
                mailInterface.updateMessageFlags(folder, ids, setFlags, true);
            }

            // Clear messages' flags
            if (clearUserFlags != null) {
                if (clearFlags < 0) {
                    // No system flags to remove
                    clearFlags = 0;
                }

                // Prepare undo
                if (undoTimeToLive > 0) {
                    Map<SystemAndUserFlags, List<String>> flags2ids = new LinkedHashMap<>(); // NOSONARLINT
                    MailMessage[] flags = mailInterface.getMessageList(folder, ids, new int[] { MailListField.ID.getField(), MailListField.FLAGS.getField(), MailListField.USER_FLAGS.getField() }, null);
                    for (MailMessage mail : flags) {
                        int systemFlags = mail.getFlags();
                        Set<String> userFlags = mail.getUserFlagsAsSet();

                        int restore = 0;
                        for (int systemFlag : SYSTEM_FLAGS) {
                            if ((clearFlags & systemFlag) > 0 && (systemFlags & systemFlag) > 0) {
                                // Answered flag shall be cleared
                                restore |= systemFlag;
                            }
                        }

                        Set<String> userRestore = null;
                        for (String userFlag : clearUserFlags) {
                            if (userFlags.contains(userFlag)) {
                                if (userRestore == null) {
                                    userRestore = HashSet.newHashSet(clearUserFlags.length);
                                }
                                userRestore.add(userFlag);
                            }
                        }

                        flags2ids.computeIfAbsent(new SystemAndUserFlags(restore, userRestore), Functions.getNewArrayListFuntion()).add(mail.getMailId());
                    }

                    if (undoOperations == null) {
                        undoOperations = new ArrayList<>(flags2ids.size());
                    }
                    for (Map.Entry<SystemAndUserFlags, List<String>> e : flags2ids.entrySet()) {
                        DefaultUndoOperation.Builder undoOperation = initUndoOperation();
                        JSONObject undoData = initUndoData(undoOperation);
                        undoData.put(AJAXServlet.PARAMETER_FOLDERID, folder);
                        undoData.put("ids", e.getValue().stream().collect(CollectorUtils.toJsonArray()));
                        undoData.put(FIELD_SET_USER_FLAGS, e.getKey().getUserFlags().stream().collect(CollectorUtils.toJsonArray()));
                        undoData.put(FIELD_SET_FLAGS, e.getKey().getFlags());
                        undoOperations.add(undoOperation.build());
                    }
                }

                // Remove system and user flags
                mailInterface.updateMessageFlags(folder, ids, clearFlags, clearUserFlags, false);
            } else if (clearFlags >= 0) {
                // Prepare undo
                if (undoTimeToLive > 0) {
                    Map<SystemAndUserFlags, List<String>> flags2ids = new LinkedHashMap<>(); // NOSONARLINT
                    MailMessage[] flags = mailInterface.getMessageList(folder, ids, new int[] { MailListField.ID.getField(), MailListField.FLAGS.getField() }, null);
                    for (MailMessage mail : flags) {
                        int systemFlags = mail.getFlags();

                        int restore = 0;
                        for (int systemFlag : SYSTEM_FLAGS) {
                            if ((clearFlags & systemFlag) > 0 && (systemFlags & systemFlag) > 0) {
                                // Answered flag shall be cleared
                                restore |= systemFlag;
                            }
                        }

                        flags2ids.computeIfAbsent(new SystemAndUserFlags(restore, Collections.emptySet()), Functions.getNewArrayListFuntion()).add(mail.getMailId());
                    }

                    if (undoOperations == null) {
                        undoOperations = new ArrayList<>(flags2ids.size());
                    }
                    for (Map.Entry<SystemAndUserFlags, List<String>> e : flags2ids.entrySet()) {
                        DefaultUndoOperation.Builder undoOperation = initUndoOperation();
                        JSONObject undoData = initUndoData(undoOperation);
                        undoData.put(AJAXServlet.PARAMETER_FOLDERID, folder);
                        undoData.put("ids", e.getValue().stream().collect(CollectorUtils.toJsonArray()));
                        undoData.put(FIELD_SET_FLAGS, e.getKey().getFlags());
                        undoOperations.add(undoOperation.build());
                    }
                }

                // Remove system flags
                mailInterface.updateMessageFlags(folder, ids, clearFlags, false);
            }

            AJAXRequestResult requestResult = new AJAXRequestResult(JSON_RESPONSE_SUCCESS, "json");

            if (undoOperations != null) {
                UndoService undoService = getService(UndoService.class);
                if (undoService != null) {
                    UndoToken undoToken = undoService.registerUndoOperations(undoOperations, undoTimeToLive, req.getSession());
                    requestResult.setUndoTokenIfNotEmpty(undoToken);
                }
            }

            return requestResult;
        } catch (JSONException e) {
            throw MailExceptionCode.JSON_ERROR.create(e, e.getMessage());
        } catch (OXRuntimeException e) { // NOSONARLINT
            throw e.getException();
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Initializes the undo operation for <code>/mail?action=flags</code> request.
     *
     * @param undoOperation The possibly pre-initialzed undo operation builder
     * @return The initialized undo operation
     */
    private static DefaultUndoOperation.Builder initUndoOperation() {
        DefaultUndoOperation.Builder newUndoOperation = DefaultUndoOperation.builder();
        newUndoOperation.withModule("mail");
        newUndoOperation.withAction("flags");
        return newUndoOperation;
    }

    /**
     * Initializes the undo data for <code>/mail?action=flags</code> request.
     *
     * @param undoData The possibly pre-initialized undo data
     * @param undoOperation The initialized undo operation builder
     * @return The initialized undo data
     */
    private static JSONObject initUndoData(DefaultUndoOperation.Builder undoOperation) {
        JSONObject newUndoData = new JSONObject();
        undoOperation.withData(newUndoData);
        return newUndoData;
    }

    @Override
    public UndoableAJAXActionService.Result isUndoable(AJAXRequestData request, ServerSession session) throws OXException {
        UndoService undoService = getService(UndoService.class);
        if (undoService == null) {
            return UndoableAJAXActionService.resultFor(false, this);
        }
        return UndoableAJAXActionService.resultFor(true, this);
    }

    private static String[] getStringArrayFor(String field, JSONObject jBody) {
        JSONArray jClearUserFlags = jBody.optJSONArray(field);
        if (jClearUserFlags == null || jClearUserFlags.length() <= 0) {
            return null; // NOSONARLINT
        }
        return jClearUserFlags.stream().map(Object::toString).filter(uf -> checkUserFlag(uf)).toArray(Functions.getNewStringArrayIntFunction());
    }

    private static final String SYSTEM_PREFIX = "\\";

    private static boolean checkUserFlag(String userFlag) {
        if (userFlag.startsWith(SYSTEM_PREFIX)) {
            throw new OXRuntimeException(MailExceptionCode.INVALID_FLAG_WITH_LEADING_BACKSLASH.create(userFlag));
        }
        return true;
    }

}
