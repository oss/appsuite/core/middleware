/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.json.actions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.mail.FullnameArgument;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.IMailMessageStorageSyncSupport;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.mail.api.MailCapabilities;
import com.openexchange.mail.api.MailConfig;
import com.openexchange.mail.dataobjects.SyncToken;
import com.openexchange.mail.dataobjects.SynchronizationResult;
import com.openexchange.mail.json.MailRequest;
import com.openexchange.mail.utils.MailFolderUtility;
import com.openexchange.server.ServiceLookup;

/**
 * {@link SyncAction} - Returns synchronization information for a certain mail folder.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@RestrictedAction(module = AbstractMailAction.MODULE, type = RestrictedAction.Type.READ)
public class SyncAction extends AbstractMailAction {

    /**
     * Initializes a new {@link SyncAction}.
     *
     * @param services The service look-up
     */
    public SyncAction(ServiceLookup services) {
        super(services);
    }

    @Override
    protected AJAXRequestResult perform(MailRequest req) throws OXException, JSONException {
        String optToken = req.getParameter("token");
        String folder = req.checkParameter("folder");

        MailServletInterface mailInterface = getMailInterface(req);
        mailInterface.openFor(folder);
        MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess = mailInterface.getMailAccess();
        MailConfig mailConfig = mailAccess.getMailConfig();
        MailCapabilities capabilities = mailConfig.getCapabilities();

        if (!capabilities.hasSynchronization()) {
            throw MailExceptionCode.UNSUPPORTED_OPERATION.create();
        }

        IMailMessageStorage messageStorage = mailAccess.getMessageStorage();

        IMailMessageStorageSyncSupport syncMessageStorage = messageStorage.supports(IMailMessageStorageSyncSupport.class);
        if (null == syncMessageStorage) {
            throw MailExceptionCode.UNSUPPORTED_OPERATION.create();
        }

        if (!syncMessageStorage.isSyncSupported()) {
            throw MailExceptionCode.UNSUPPORTED_OPERATION.create();
        }

        FullnameArgument fullnameArgument = MailFolderUtility.prepareMailFolderParam(folder);

        JSONArray jKnownMailIds = (JSONArray) req.getRequest().getData();
        String[] optKnownMailIds = jKnownMailIds == null ? null : jKnownMailIds.stream().map(Object::toString).collect(CollectorUtils.toList(jKnownMailIds.length())).toArray(new String[jKnownMailIds.length()]);
        SyncToken syncToken = optToken == null ? null : SyncToken.parseToken(optToken);
        SynchronizationResult syncResult = syncMessageStorage.getSynchronizationResult(fullnameArgument.getFullName(), syncToken, optKnownMailIds);

        JSONObject jResult = new JSONObject(6);
        jResult.put("deleted", syncResult.getDeletedMailIds().length <= 0 ? JSONArray.EMPTY_ARRAY : new JSONArray(syncResult.getDeletedMailIds()));
        jResult.put("modified", syncResult.getModifiedMailIds().length <= 0 ? JSONArray.EMPTY_ARRAY : new JSONArray(syncResult.getModifiedMailIds()));
        jResult.put("new", syncResult.getNewMailIds().length <= 0 ? JSONArray.EMPTY_ARRAY : new JSONArray(syncResult.getNewMailIds()));
        jResult.put("token", syncResult.getNewSyncToken().getToken());

        return new AJAXRequestResult(jResult, "json");
    }

}
