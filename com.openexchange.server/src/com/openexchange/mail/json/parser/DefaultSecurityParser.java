/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.mail.json.parser;

import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.dataobjects.SecuritySettings;
import com.openexchange.mail.service.SecurityParsingService;

/**
 * Implements a Default Parser for Security JSON Object
 * Confirms there are no JSON elements that are set otherwise throws error
 * 
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since 8.21
 * 
 */
public class DefaultSecurityParser implements SecurityParsingService {

    @Override
    public SecuritySettings parseJson(JSONObject json) throws OXException {
        if (json == null) {
            return null;
        }
        Iterator<String> keys = json.keys();
        // Iterate JSON.  Only acceptable values are false boolean.  Anything else
        // is unsupported and will throw error
        while (keys.hasNext()) {
            String key = keys.next();
            try {
                Object obj = json.get(key);
                if (obj instanceof Boolean b && false == b.booleanValue()) {
                    continue;
                }
            } catch (JSONException e) {
                throw MailExceptionCode.INVALID_PARAMETER.create(e, key);
            }
            throw MailExceptionCode.INVALID_PARAMETER.create("security json: " + key);
        }
        return null;
    }

}
