/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.mail.cache;


/**
 * {@link AccountIdAndFolder} - The key for a mail folder of a certain mail account.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
class AccountIdAndFolder {

    private final int accountId;
    private final String fullName;
    private final int hash;

    /**
     * Initializes a new instance of {@link AccountIdAndFolder}.
     *
     * @param accountId The identifier of the mail account
     * @param fullName The full name of the mail folder
     */
    AccountIdAndFolder(int accountId, String fullName) {
        super();
        this.accountId = accountId;
        this.fullName = fullName;
        int prime = 31;
        int result = 1;
        result = prime * result + accountId;
        result = prime * result + ((fullName == null) ? 0 : fullName.hashCode());
        this.hash = result;
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AccountIdAndFolder other = (AccountIdAndFolder) obj;
        if (accountId != other.accountId) {
            return false;
        }
        if (fullName == null) {
            if (other.fullName != null) {
                return false;
            }
        } else if (!fullName.equals(other.fullName)) {
            return false;
        }
        return true;
    }

}
