/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.cache;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.concurrent.TimeoutConcurrentMap;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadable;
import com.openexchange.config.Reloadables;
import com.openexchange.exception.OXException;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.mail.config.MailProperties;
import com.openexchange.mail.config.MailReloadable;
import com.openexchange.mailaccount.MailAccount;
import com.openexchange.mailaccount.MailAccountStorageService;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.session.Session;

/**
 * {@link SingletonMailAccessCache} - A very volatile cache for already connected instances of {@link MailAccess}.
 * <p>
 * Only one mail access can be cached per user and is dedicated to fasten sequential mail requests<br>
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class SingletonMailAccessCache implements IMailAccessCache {

    private static final AtomicReference<SingletonMailAccessCache> INSTANCE_REF = new AtomicReference<>();

    /**
     * Gets the singleton instance.
     *
     * @return The singleton instance
     * @throws OXException If initialization fails
     */
    public static SingletonMailAccessCache getInstance() throws OXException {
        SingletonMailAccessCache instance = INSTANCE_REF.get();
        if (instance == null) {
            synchronized (SingletonMailAccessCache.class) {
                instance = INSTANCE_REF.get();
                if (instance == null) {
                    instance = new SingletonMailAccessCache();
                    INSTANCE_REF.set(instance);
                }
            }
        }
        return instance;
    }

    /**
     * Gets the singleton instance (if available).
     *
     * @return The singleton instance or <code>null</code>
     */
    public static SingletonMailAccessCache optInstance() {
        return INSTANCE_REF.get();
    }

    /**
     * Releases the instance.
     */
    public static void releaseInstance() {
        SingletonMailAccessCache instance = INSTANCE_REF.getAndSet(null);
        if (instance != null) {
            instance.close();
        }
    }

    static {
        MailReloadable.getInstance().addReloadable(new Reloadable() {

            @Override
            public void reloadConfiguration(ConfigurationService configService) {
                SingletonMailAccessCache tmp = INSTANCE_REF.get();
                if (tmp != null) {
                    try {
                        int shrinkerSeconds = configService.getIntProperty("com.openexchange.mail.mailAccessCacheShrinkerSeconds", 3);
                        int idleSeconds = configService.getIntProperty("com.openexchange.mail.mailAccessCacheIdleSeconds", 4);

                        tmp.defaultIdleSeconds.set(idleSeconds);
                        tmp.timeoutMap.setShrinkerIntervalSeconds(shrinkerSeconds);
                    } catch (OXException e) {
                        org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SingletonMailAccessCache.class);
                        logger.error("Failed to re-initialize singleton mail-access cache", e);
                    }
                }
            }

            @Override
            public Interests getInterests() {
                return Reloadables.interestsForProperties("com.openexchange.mail.mailAccessCacheShrinkerSeconds", "com.openexchange.mail.mailAccessCacheIdleSeconds");
            }
        });
    }

    // --------------------------------------------------------------------------------------------------------------------------------

    private final TimeoutConcurrentMap<Key, MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage>> timeoutMap;
    private final AtomicInteger defaultIdleSeconds;

    /**
     * Prevent instantiation.
     *
     * @throws OXException If initialization fails
     */
    private SingletonMailAccessCache() throws OXException {
        super();
        timeoutMap = new TimeoutConcurrentMap<Key, MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage>>(MailProperties.getInstance().getMailAccessCacheShrinkerSeconds());
        timeoutMap.setDefaultTimeoutListener(new MailAccessTimeoutListener());
        defaultIdleSeconds = new AtomicInteger(MailProperties.getInstance().getMailAccessCacheIdleSeconds());
    }

    @Override
    public void close() {
        releaseCache();
    }

    /**
     * Releases cache reference.
     */
    public void releaseCache() {
        timeoutMap.timeoutAll();
        timeoutMap.dispose();
        defaultIdleSeconds.set(0);
    }

    @Override
    public int numberOfMailAccesses(Session session, int accountId) throws OXException {
        return null == timeoutMap.get(getUserKey(accountId, session.getUserId(), session.getContextId())) ? 0 : 1;
    }

    /**
     * Removes and returns a mail access from cache.
     *
     * @param session The session
     * @param accountId The account ID
     * @return An active instance of {@link MailAccess} or <code>null</code>
     */
    @Override
    public MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> removeMailAccess(Session session, int accountId) {
        final MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess = timeoutMap.remove(getUserKey(accountId, session.getUserId(), session.getContextId()));
        if (null == mailAccess) {
            return null;
        }
        mailAccess.setCached(false);
        return mailAccess;
    }

    /**
     * Puts given mail access into cache if none user-bound connection is already contained in cache.
     *
     * @param session The session
     * @param accountId The account ID
     * @param mailAccess The mail access to put into cache
     * @return <code>true</code> if mail access could be successfully cached; otherwise <code>false</code>
     */
    @Override
    public boolean putMailAccess(Session session, int accountId, MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess) {
        int idleTime = mailAccess.getCacheIdleSeconds();
        if (idleTime <= 0) {
            idleTime = defaultIdleSeconds.get();
        }
        if (null == timeoutMap.putIfAbsent(getUserKey(accountId, session.getUserId(), session.getContextId()), mailAccess, idleTime)) {
            mailAccess.setCached(true);
            return true;
        }
        return false;
    }

    /**
     * Checks if cache already holds a user-bound mail access for specified account.
     *
     * @param session The session
     * @param accountId The account ID
     * @return <code>true</code> if a user-bound mail access is already present in cache; otherwise <code>false</code>
     */
    @Override
    public boolean containsMailAccess(Session session, int accountId) {
        return (timeoutMap.get(getUserKey(accountId, session.getUserId(), session.getContextId())) != null);
    }

    @Override
    public void clearUserEntries(int userId, int contextId) throws OXException {
        MailAccountStorageService storageService = ServerServiceRegistry.getInstance().getService(MailAccountStorageService.class, true);
        final MailAccount[] accounts = storageService.getUserMailAccounts(userId, contextId);
        for (MailAccount mailAccount : accounts) {
            timeoutMap.timeout(getUserKey(mailAccount.getId(), userId, contextId));
        }
    }

    /**
     * Creates a new key for specified arguments.
     *
     * @param accountId The account identifier
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The newly created key
     */
    private static Key getUserKey(int accountId, int userId, int contextId) {
        return new Key(accountId, userId, contextId);
    }

    /** Key for in-memory cache */
    private static final class Key {

        private final int userId;
        private final int contextId;
        private final int accountId;
        private final int hash;

        /**
         * Initializes a new {@link Key}.
         *
         * @param accountId The account identifier
         * @param userId The user identifier
         * @param contextId The context identifier
         */
        Key(int accountId, int userId, int contextId) {
            super();
            this.userId = userId;
            this.contextId = contextId;
            this.accountId = accountId;

            int prime = 31;
            int result = 1;
            result = prime * result + contextId;
            result = prime * result + userId;
            result = prime * result + accountId;
            hash = result;
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Key other = (Key) obj;
            if (contextId != other.contextId) {
                return false;
            }
            if (userId != other.userId) {
                return false;
            }
            if (accountId != other.accountId) {
                return false;
            }
            return true;
        }
    }

}
