/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.cache;

import java.util.Objects;

/**
 * {@link SessionMailCacheKey} - The key for a session mail cache entry.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class SessionMailCacheKey {

    private final int code;
    private final String keyPart;
    private final int hash;

    /**
     * Initializes a new {@link SessionMailCacheKey}.
     *
     * @param code The code value
     * @param keyPart The key part
     */
    public SessionMailCacheKey(int code, String keyPart) {
        super();
        this.code = code;
        this.keyPart = keyPart;
        this.hash = Objects.hash(Integer.valueOf(code), keyPart);
    }

    /**
     * Gets the code
     *
     * @return The code
     */
    public int getCode() {
        return code;
    }

    /**
     * Gets the key part
     *
     * @return The key part
     */
    public String getKeyPart() {
        return keyPart;
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SessionMailCacheKey other = (SessionMailCacheKey) obj;
        return code == other.code && Objects.equals(keyPart, other.keyPart);
    }

}
