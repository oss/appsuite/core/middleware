/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.IntFunction;
import java.util.function.Predicate;

/**
 * {@link DoubleKeyMap} - A double-key map.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
final class DoubleKeyMap<K1, K2, V> {

    private final ConcurrentMap<K1, ConcurrentMap<K2, V>> map;
    private final IntFunction<V[]> arrayGenerator;
    private final K2 poisonKey;
    private final V poisonValue;
    private final Predicate<V> noPoisonValue;

    /**
     * Initializes a new {@link DoubleKeyMap}.
     *
     * @param arrayGenerator A a function which produces a new array of the desired type and the provided length
     * @param poisonKey The poison key used to mark a collection as invalid
     * @param poisonValue The poison value to pass for specified poison key
     */
    public DoubleKeyMap(IntFunction<V[]> arrayGenerator, K2 poisonKey, V poisonValue) {
        super();
        this.map = new ConcurrentHashMap<>();
        this.arrayGenerator = arrayGenerator;
        this.poisonKey = poisonKey;
        this.poisonValue = poisonValue;
        this.noPoisonValue = v -> poisonValue != v; // Yes, we're using "!=" operator
    }

    /**
     * Detects if first key is contained in this map.
     *
     * @param k1 The first key
     * @return <code>true</code> if first key is contained in this map; otherwise <code>false</code>
     */
    public boolean containsKey(K1 k1) {
        return map.containsKey(k1);
    }

    /**
     * Detects if key pair is contained in this map.
     *
     * @param k1 The first key
     * @param k2 The second key
     * @return <code>true</code> if key pair is contained in this map; otherwise <code>false</code>
     */
    public boolean containsKeyPair(K1 k1, K2 k2) {
        ConcurrentMap<K2, V> innerMap = map.get(k1);
        return null != innerMap && innerMap.containsKey(k2);
    }

    /**
     * Gets all values associated with given first key.
     *
     * @param k1 The first key
     * @return All values associated with given first key or <code>null</code> if none found
     */
    public V[] getValues(K1 k1) {
        ConcurrentMap<K2, V> innerMap = map.get(k1);
        if (innerMap == null) {
            return null; // NOSONARLINT
        }
        // Drop possibly contained poison value
        return innerMap.values().stream().filter(noPoisonValue).toArray(arrayGenerator);
    }

    /**
     * Gets the values associated with given first key and given second keys.
     *
     * @param k1 The first key
     * @param keys The second keys
     * @return The values associated with given first key and given second keys or <code>null</code>
     */
    public V[] getValues(K1 k1, K2[] keys) {
        ConcurrentMap<K2, V> innerMap = map.get(k1);
        if (innerMap == null) {
            return null; // NOSONARLINT
        }
        V[] retval = arrayGenerator.apply(keys.length);
        for (int i = 0; i < keys.length; i++) {
            retval[i] = innerMap.get(keys[i]);
        }
        return retval;
    }

    /**
     * Gets the single value associated with given key pair.
     *
     * @param k1 The first key
     * @param k2 The second key
     * @return The single value associated with given key pair or <code>null</code> if not present
     */
    public V getValue(K1 k1, K2 k2) {
        ConcurrentMap<K2, V> innerMap = map.get(k1);
        return null == innerMap ? null : innerMap.get(k2);
    }

    /**
     * Puts given values into map.
     *
     * @param k1 The first key
     * @param keys The second keys
     * @param values The values to insert
     */
    public void putValues(K1 k1, K2[] keys, V[] values) {
        if ((k1 == null) || (keys == null) || (values == null)) {
            throw new IllegalArgumentException("Argument must not be null");
        }
        ConcurrentMap<K2, V> innerMap = map.get(k1);
        if (innerMap == null) {
            ConcurrentMap<K2, V> newInnerMap = new ConcurrentHashMap<K2, V>(values.length, 0.9F, 1);
            innerMap = map.putIfAbsent(k1, newInnerMap);
            if (innerMap == null) {
                innerMap = newInnerMap;
            }
        }
        synchronized (innerMap) {
            if (innerMap.containsKey(poisonKey)) {
                // Inner map has been dropped in the meantime. Retry...
                putValues(k1, keys, values);
                return;
            }

            K2 key;
            V value;
            for (int i = 0; i < values.length; i++) {
                key = keys[i];
                if (key != null) {
                    value = values[i];
                    if (value != null) {
                        innerMap.put(key, value);
                    }
                }
            }
        }
    }

    /**
     * Puts a single value into map.
     *
     * @param k1 The first key
     * @param k2 The second key
     * @param value The value to insert
     * @return The value formerly bound to given key pair or <code>null</code> if none was bound before
     */
    public V putValue(K1 k1, K2 k2, V value) {
        if (k1 == null) {
            throw new IllegalArgumentException("First key must not be null");
        }
        if (k2 == null) {
            throw new IllegalArgumentException("Second key must not be null");
        }
        if (value == null) {
            throw new IllegalArgumentException("Value must not be null");
        }
        ConcurrentMap<K2, V> innerMap = map.get(k1);
        if (innerMap == null) {
            ConcurrentMap<K2, V> newInnerMap = new ConcurrentHashMap<K2, V>(16, 0.9F, 1);
            innerMap = map.putIfAbsent(k1, newInnerMap);
            if (innerMap == null) {
                innerMap = newInnerMap;
            }
        }
        synchronized (innerMap) {
            if (innerMap.containsKey(poisonKey)) {
                // Inner map has been dropped in the meantime. Retry...
                return putValue(k1, k2, value);
            }
            return innerMap.put(k2, value);
        }
    }

    /**
     * Removes all values associated with given first key.
     *
     * @param k1 The first key
     */
    public void removeValues(K1 k1) {
        map.remove(k1);
    }

    /**
     * Removes the values associated with given first key and is in list of second keys.
     *
     * @param k1 The first key
     * @param keys The second keys
     */
    public void removeValues(K1 k1, K2[] keys) {
        Map<K2, V> innerMap = map.get(k1);
        if (null == innerMap) {
            return;
        }
        boolean somethingRemoved = false;
        for (K2 k2 : keys) {
            somethingRemoved = innerMap.remove(k2) != null;
        }
        if (somethingRemoved) {
            synchronized (innerMap) {
                if (innerMap.isEmpty()) {
                    // Mark as invalid
                    innerMap.put(poisonKey, poisonValue);

                    // Remove empty inner map
                    map.remove(k1);
                }
            }
        }
    }

    /**
     * Removes the single value associated with given key pair.
     *
     * @param k1 The first key
     * @param k2 The second key
     * @return The removed value or <code>null</code> if not present
     */
    public V removeValue(K1 k1, K2 k2) {
        Map<K2, V> innerMap = map.get(k1);
        if (null == innerMap) {
            return null;
        }
        V retval = innerMap.remove(k2);
        if ((retval != null)) {
            synchronized (innerMap) {
                if (innerMap.isEmpty()) {
                    // Mark as invalid
                    innerMap.put(poisonKey, poisonValue);

                    // Remove empty inner map
                    map.remove(k1);
                }
            }
        }
        return retval;
    }

    /**
     * Checks if no values are bound to given first key.
     *
     * @param k1 The first key
     * @return <code>true</code> if no values are bound to given first key; otherwise <code>false</code>
     */
    public boolean isEmpty(K1 k1) {
        Map<K2, V> innerMap = map.get(k1);
        if (null == innerMap) {
            return true;
        }
        synchronized (innerMap) {
            if (innerMap.isEmpty()) {
                // Mark as invalid
                innerMap.put(poisonKey, poisonValue);

                // Remove empty inner map
                map.remove(k1);
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if whole map is empty.
     *
     * @return <code>true</code> if whole map is empty; otherwise <code>false</code>
     */
    public boolean isEmpty() {
        return map.isEmpty();
    }

    /**
     * Clears whole map.
     */
    public void clear() {
        map.clear();
    }

}
