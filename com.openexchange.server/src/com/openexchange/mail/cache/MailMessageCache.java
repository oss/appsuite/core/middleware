/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.IntFunction;
import org.json.JSONArray;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.java.Strings;
import com.openexchange.mail.MailListField;
import com.openexchange.mail.dataobjects.IDMailMessage;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.session.UserAndContext;

/**
 * {@link MailMessageCache} - Caches instances of {@link MailMessage} which are prepared for caching by invoking
 * {@link MailMessage#prepareForCaching()}; meaning to release all kept content references. Thus only message's header data is going to be
 * cached.
 * <p>
 * This cache is highly volatile. With every new list request all caches entries belonging to requesting user are removed. See this cache
 * region's configuration settings in file "@conf-path@/mailcache.ccf" for further information.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class MailMessageCache {

    private static interface MailFieldUpdater {

        public void updateField(MailMessage mail, Object newValue);
    }

    private static final MailFieldUpdater UPDATER_FLAGS = (mail, newValue) -> {
        int newFlags = mail.getFlags();
        int flags = ((Integer) newValue).intValue();
        boolean set = flags > 0;
        flags = set ? flags : flags * -1;
        if (((flags & MailMessage.FLAG_ANSWERED) > 0)) {
            newFlags = set ? (newFlags | MailMessage.FLAG_ANSWERED) : (newFlags & ~MailMessage.FLAG_ANSWERED);
        }
        if (((flags & MailMessage.FLAG_DELETED) > 0)) {
            newFlags = set ? (newFlags | MailMessage.FLAG_DELETED) : (newFlags & ~MailMessage.FLAG_DELETED);
        }
        if (((flags & MailMessage.FLAG_DRAFT) > 0)) {
            newFlags = set ? (newFlags | MailMessage.FLAG_DRAFT) : (newFlags & ~MailMessage.FLAG_DRAFT);
        }
        if (((flags & MailMessage.FLAG_FLAGGED) > 0)) {
            newFlags = set ? (newFlags | MailMessage.FLAG_FLAGGED) : (newFlags & ~MailMessage.FLAG_FLAGGED);
        }
        if (((flags & MailMessage.FLAG_SEEN) > 0)) {
            newFlags = set ? (newFlags | MailMessage.FLAG_SEEN) : (newFlags & ~MailMessage.FLAG_SEEN);
        }
        if (((flags & MailMessage.FLAG_USER) > 0)) {
            newFlags = set ? (newFlags | MailMessage.FLAG_USER) : (newFlags & ~MailMessage.FLAG_USER);
        }
        if (((flags & MailMessage.FLAG_SPAM) > 0)) {
            newFlags = set ? (newFlags | MailMessage.FLAG_SPAM) : (newFlags & ~MailMessage.FLAG_SPAM);
        }
        if (((flags & MailMessage.FLAG_FORWARDED) > 0)) {
            newFlags = set ? (newFlags | MailMessage.FLAG_FORWARDED) : (newFlags & ~MailMessage.FLAG_FORWARDED);
        }
        if (((flags & MailMessage.FLAG_READ_ACK) > 0)) {
            newFlags = set ? (newFlags | MailMessage.FLAG_READ_ACK) : (newFlags & ~MailMessage.FLAG_READ_ACK);
        }
        if (((flags & MailMessage.FLAG_CUSTOM) > 0)) {
            newFlags = set ? (newFlags | MailMessage.FLAG_CUSTOM) : (newFlags & ~MailMessage.FLAG_CUSTOM);
        }
        mail.setFlags(newFlags);
    };

    private static final MailFieldUpdater UPDATER_COLOR_FLAG = (mail, newValue) -> mail.setColorLabel(((Integer) newValue).intValue());

    private static final MailFieldUpdater UPDATER_USER_FLAGS = (mail, newValue) -> {
        if (newValue instanceof JSONArray) {
            Collection<Object> objs = ((JSONArray) newValue).asList();
            List<String> uf = new ArrayList<>(objs.size());
            for (Object object : objs) {
                uf.add(object.toString());
            }
            mail.addUserFlags(uf);
            return;
        }
        if (newValue instanceof Collection) {
            mail.addUserFlags((Collection<String>) newValue);
            return;
        }
        mail.addUserFlags(Strings.splitByComma(newValue.toString()));
    };

    private static MailFieldUpdater[] createMailFieldUpdater(MailListField[] changedFields) {
        MailFieldUpdater[] updaters = new MailFieldUpdater[changedFields.length];
        for (int i = 0; i < changedFields.length; i++) {
            switch (changedFields[i]) {
            case FLAGS:
                updaters[i] = UPDATER_FLAGS;
                break;
            case COLOR_LABEL:
                updaters[i] = UPDATER_COLOR_FLAG;
                break;
            case USER_FLAGS:
                updaters[i] = UPDATER_USER_FLAGS;
                break;
            default:
                throw new IllegalStateException("No Updater for MailListField." + changedFields[i].toString());
            }
        }
        return updaters;
    }

    private static final MailMessageCache INSTANCE = new MailMessageCache();

    /**
     * Gets the singleton instance.
     *
     * @return The singleton instance
     */
    public static MailMessageCache getInstance() {
        return INSTANCE;
    }

    /*-
     * ############################################## Field members ##############################################
     */

    private final Cache<UserAndContext, DoubleKeyMap<AccountIdAndFolder, String, MailMessage>> cache;

    /**
     * Singleton instantiation.
     */
    private MailMessageCache() {
        super();
        cache = CacheBuilder.newBuilder().maximumSize(10000000).expireAfterAccess(60, TimeUnit.SECONDS).build();
    }

    /**
     * Invalidates all cached messages
     */
    public void invalidateAllCachedMessages() {
        cache.invalidateAll();
    }

    /**
     * Updates cached message.
     *
     * @param mailIds The mail identifiers; pass <code>null</code> to update all cached message of given folder
     * @param accountId The account identifier
     * @param fullName The full name
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param changedFields The changed fields
     * @param newValues The new values
     */
    public void updateCachedMessages(String[] mailIds, int accountId, String fullName, int userId, int contextId, MailListField[] changedFields, Object[] newValues) {
        if (null == mailIds) {
            updateCachedMessages(accountId, fullName, userId, contextId, changedFields, newValues);
            return;
        }
        DoubleKeyMap<AccountIdAndFolder, String, MailMessage> map = cache.getIfPresent(getMapKey(userId, contextId));
        if (map == null) {
            return;
        }
        updateMails(map.getValues(getEntryKey(accountId, fullName), mailIds), changedFields, newValues);
    }

    /**
     * Updates cached message
     *
     * @param accountId The account identifier
     * @param fullName The full name
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param changedFields The changed fields
     * @param newValues The new values
     */
    public void updateCachedMessages(int accountId, String fullName, int userId, int contextId, MailListField[] changedFields, Object[] newValues) {
        DoubleKeyMap<AccountIdAndFolder, String, MailMessage> map = cache.getIfPresent(getMapKey(userId, contextId));
        if (map == null) {
            return;
        }
        updateMails(map.getValues(getEntryKey(accountId, fullName)), changedFields, newValues);
    }

    /**
     * Detects if cache holds messages belonging to given user.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> if messages are present; otherwise <code>false</code>
     */
    public boolean containsUserMessages(int userId, int contextId) {
        return cache.getIfPresent(getMapKey(userId, contextId)) != null;
    }

    /**
     * Detects if cache holds messages belonging to a certain folder.
     *
     * @param accountId The account identifier
     * @param fullName The folder full name
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> if cache holds messages belonging to a certain folder; otherwise <code>false</code>
     */
    public boolean containsFolderMessages(int accountId, String fullName, int userId, int contextId) {
        DoubleKeyMap<AccountIdAndFolder, String, MailMessage> map = cache.getIfPresent(getMapKey(userId, contextId));
        return map != null && map.containsKey(getEntryKey(accountId, fullName));
    }

    /**
     * Removes the messages cached for a user.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     */
    public void removeUserMessages(int userId, int contextId) {
        cache.invalidate(getMapKey(userId, contextId));
    }

    /**
     * Removes cached messages belonging to a certain folder.
     *
     * @param accountId The account identifier
     * @param fullName The folder full name
     * @param userId The user identifier
     * @param contextId The context identifier
     */
    public void removeFolderMessages(int accountId, String fullName, int userId, int contextId) {
        DoubleKeyMap<AccountIdAndFolder, String, MailMessage> map = cache.getIfPresent(getMapKey(userId, contextId));
        if (map == null) {
            return;
        }
        map.removeValues(getEntryKey(accountId, fullName));
    }

    /**
     * Removes the messages appearing in given UIDs belonging to a certain folder.
     *
     * @param mailIds The mail identifiers; pass <code>null</code> to remove all associated with folder
     * @param accountId The account identifier
     * @param fullName The folder full name
     * @param userId The user identifier
     * @param contextId The context identifier
     */
    public void removeMessages(String[] mailIds, int accountId, String fullName, int userId, int contextId) {
        UserAndContext mapKey = getMapKey(userId, contextId);
        DoubleKeyMap<AccountIdAndFolder, String, MailMessage> map = cache.getIfPresent(mapKey);
        if (map == null) {
            return;
        }
        if (null == mailIds) {
            map.removeValues(getEntryKey(accountId, fullName));
        } else {
            map.removeValues(getEntryKey(accountId, fullName), mailIds);
        }
    }

    /**
     * Gets the corresponding messages from cache. If a cache entry could not be found <code>null</code> is returned to force a reload from
     * mail server.
     *
     * @param mailIds The mail identifiers
     * @param accountId The account identifier
     * @param fullName The folder full name
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return An array of {@link MailMessage} containing the fetched messages or <code>null</code>
     */
    public MailMessage[] getMessages(String[] mailIds, int accountId, String fullName, int userId, int contextId) {
        DoubleKeyMap<AccountIdAndFolder, String, MailMessage> map = cache.getIfPresent(getMapKey(userId, contextId));
        if (null == map) {
            return null; // NOSONARLINT
        }
        MailMessage[] retval = map.getValues(getEntryKey(accountId, fullName), mailIds);
        if (null == retval) {
            return null; // not cached
        }
        for (MailMessage fromCache : retval) {
            if (null == fromCache) {
                // Not all desired messages can be served from cache
                return null;
            }
        }
        return retval;
    }

    /**
     * Puts given messages into cache.
     *
     * @param accountId The account identifier
     * @param mails The messages to cache
     * @param userId The user identifier
     * @param contextId The context identifier
     */
    public void putMessages(int accountId, MailMessage[] mails, int userId, int contextId) {
        if (mails == null || mails.length == 0) {
            return;
        }
        UserAndContext mapKey = getMapKey(userId, contextId);
        DoubleKeyMap<AccountIdAndFolder, String, MailMessage> map = cache.getIfPresent(mapKey);
        if (null == map) {
            DoubleKeyMap<AccountIdAndFolder, String, MailMessage> newMap = createDoubleKeyMap();
            map = cache.asMap().putIfAbsent(mapKey, newMap);
            if (map == null) {
                map = newMap;
            }
        }
        for (MailMessage mail : mails) {
            if (mail == null) {
                continue;
            }
            mail.prepareForCaching();
            map.putValue(getEntryKey(accountId, mail.getFolder()), mail.getMailId(), mail);
        }
    }

    /** The array generator for <code>MailMessage</code> type */
    private static final IntFunction<MailMessage[]> ARRAY_GENERATOR = length -> new MailMessage[length];

    /** The poison key */
    private static final String POISON_KEY = "3a6a178e82cd45e8a5693bf43db168fd";

    /** The poison value */
    private static final MailMessage POISON_VALUE = new IDMailMessage();

    /**
     * Creates a new instance of <code>DoubleKeyMap</code>.
     *
     * @return The newly created instance of <code>DoubleKeyMap</code>
     */
    private static DoubleKeyMap<AccountIdAndFolder, String, MailMessage> createDoubleKeyMap() {
        return new DoubleKeyMap<>(ARRAY_GENERATOR, POISON_KEY, POISON_VALUE);
    }

    private static UserAndContext getMapKey(int userId, int contextId) {
        return UserAndContext.newInstance(userId, contextId);
    }

    private static AccountIdAndFolder getEntryKey(int accountId, String fullName) {
        return new AccountIdAndFolder(accountId, fullName);
    }

    /**
     * Updates the given mails
     *
     * @param mails The mails to update
     * @param changedFields The changed fields
     * @param newValues The new values
     */
    private static void updateMails(MailMessage[] mails, MailListField[] changedFields, Object[] newValues) {
        if (mails == null || mails.length <= 0) {
            // Nothing to update
            return;
        }

        MailFieldUpdater[] updaters = createMailFieldUpdater(changedFields);
        for (MailMessage mail : mails) {
            if (mail != null) {
                for (int i = updaters.length; i-- > 0;) {
                    updaters[i].updateField(mail, newValues[i]);
                }
            }
        }
    }

}
