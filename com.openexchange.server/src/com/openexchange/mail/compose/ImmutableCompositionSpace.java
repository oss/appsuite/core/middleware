/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.compose;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import com.openexchange.mail.MailPath;

/**
 * {@link ImmutableCompositionSpace}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 * @since v7.10.2
 */
public class ImmutableCompositionSpace implements CompositionSpace {

    private final CompositionSpaceId id;
    private final MailPath mailPath;
    private final Message message;
    private final long lastModified;
    private final ClientToken clientToken;
    private final List<String> capabilities;

    /**
     * Initializes a new {@link ImmutableCompositionSpace}.
     *
     * @param id The composition space identifier
     * @param mailPath The optional mail path associated with composition space or <code>null</code>
     * @param message The message
     * @param lastModified The last-modified time stamp; the number of milliseconds since January 1, 1970, 00:00:00 GMT
     * @param clientToken The client token
     * @param capabilities The capabilities of this composition space
     */
    public ImmutableCompositionSpace(CompositionSpaceId id, MailPath mailPath, Message message, long lastModified, ClientToken clientToken, String... capabilities) {
        this(id, mailPath, message, lastModified, clientToken, capabilities == null || capabilities.length <= 0 ? null : Arrays.stream(capabilities));
    }

    /**
     * Initializes a new {@link ImmutableCompositionSpace}.
     *
     * @param id The composition space identifier
     * @param mailPath The optional mail path associated with composition space or <code>null</code>
     * @param message The message
     * @param lastModified The last-modified time stamp; the number of milliseconds since January 1, 1970, 00:00:00 GMT
     * @param clientToken The client token
     * @param capabilities The capabilities of this composition space
     */
    public ImmutableCompositionSpace(CompositionSpaceId id, MailPath mailPath, Message message, long lastModified, ClientToken clientToken, Collection<String> capabilities) {
        this(id, mailPath, message, lastModified, clientToken, capabilities == null || capabilities.isEmpty() ? null : capabilities.stream());
    }

    /**
     * Initializes a new {@link ImmutableCompositionSpace}.
     *
     * @param id The composition space identifier
     * @param mailPath The optional mail path associated with composition space or <code>null</code>
     * @param message The message
     * @param lastModified The last-modified time stamp; the number of milliseconds since January 1, 1970, 00:00:00 GMT
     * @param clientToken The client token
     * @param capabilities The capabilities of this composition space
     */
    public ImmutableCompositionSpace(CompositionSpaceId id, MailPath mailPath, Message message, long lastModified, ClientToken clientToken, Stream<String> capabilities) {
        super();
        this.id = id;
        this.mailPath = mailPath;
        this.message = message;
        this.lastModified = lastModified;
        this.clientToken = clientToken;
        List<String> nonNullCaps = capabilities == null ? null : capabilities.filter(Objects::nonNull).toList();
        this.capabilities = nonNullCaps == null ? List.of() : List.copyOf(Set.copyOf(nonNullCaps));
    }

    @Override
    public CompositionSpaceId getId() {
        return id;
    }

    @Override
    public Optional<MailPath> getMailPath() {
        return Optional.ofNullable(mailPath);
    }

    @Override
    public Message getMessage() {
        return message;
    }

    @Override
    public long getLastModified() {
        return lastModified;
    }

    @Override
    public ClientToken getClientToken() {
        return clientToken;
    }

    @Override
    public List<String> getCapabilities() {
        return capabilities;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append('[');
        if (id != null) {
            builder.append("id=").append(id).append(", ");
        }
        if (mailPath != null) {
            builder.append("mailPath=").append(mailPath).append(", ");
        }
        if (message != null) {
            builder.append("message=").append(message).append(", ");
        }
        builder.append("lastModified=").append(new Date(lastModified)).append(", ");
        builder.append("clientToken=").append(clientToken.toString()).append(']');
        return builder.toString();
    }

}
