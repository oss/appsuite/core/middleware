/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.mail.compose;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import com.openexchange.ajax.container.ThresholdFileHolder;
import com.openexchange.config.lean.Property;

/**
 * {@link CompositionSpaceProperties} - Properties for composition spaces.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public enum CompositionSpaceProperties implements Property {

    /**
     * The max. time for an opened composition space laying idle before being evicted.
     */
    MAX_IDLE_TIME("maxIdleTimeMillis", "1W"),
    /**
     * The max. number of opened composition spaces for an individual user.
     */
    MAX_SPACES_PER_USER("maxSpacesPerUser", I(20)),

    // --------------------------------- Properties for database-backed composition spaces -------------------------------------------------

    /**
     * The identifier of file storage used to manage attachments.
     */
    FILE_STORAGE_ID("fileStorageId", I(0)),
    /**
     * Whether to use an intermediate in-memory storage to manage opened composition spaces.
     * <p>
     * Changes are periodically written to persistent storage.
     */
    USE_IN_MEMORY_STORAGE("useInMemoryStorage", Boolean.FALSE),
    /**
     * Whether to use an intermediate in-memory storage to manage opened composition spaces.
     * <p>
     * Changes are periodically written to persistent storage.
     */
    IN_MEMORY_STORAGE_DELAY_MILLIS("delayDuration", L(60000)),
    /**
     * Whether to use an intermediate in-memory storage to manage opened composition spaces.
     * <p>
     * Changes are periodically written to persistent storage.
     */
    IN_MEMORY_STORAGE_MAX_DELAY_MILLIS("maxDelayDuration", L(300000)),

    /**
     * Specifies the directory for the file cache used by database-backed storage for contents of composition spaces.
     */
    RDB_STORAGE_FILE_CACHE_DIRECTORY("rdbstorage.content.fileCacheDir", ""),
    /**
     * Specifies the maximum size for the content of a composition space.
     */
    RDB_STORAGE_MAX_SIZE("rdbstorage.content.maxSize", L(-1)),

    /**
     * Whether encryption of composition spaces is enabled.
     * <p>
     * <b>Note</b>: Requires the <code>"guard"</code> capability being available in order for being effective.
     */
    SECURITY_ENCRYPTION_ENABLED("security.encryptionEnabled", Boolean.TRUE),
    /**
     * Whether to automatically delete an encrypted composition space if its encryption key is missing.
     */
    SECURITY_AUTO_DELETE_IF_KEY_MISSING("security.autoDeleteIfKeyIsMissing", Boolean.FALSE),

    // --------------------------------- Properties for mail-storage-backed composition spaces ------------------------------------------------

    /**
     * Whether the mail-storage-backed composition spaces are enabled.
     */
    MAILSTORAGE_ENABLED("mailstorage.enabled", Boolean.TRUE),
    /**
     * Specifies the in-memory threshold in bytes for composition space data. Messages exceeding that threshold are spooled to disk.
     */
    MAILSTORAGE_IN_MEMORY_THRESHOLD("mailstorage.inMemoryThreshold", L(ThresholdFileHolder.DEFAULT_IN_MEMORY_THRESHOLD)),
    /**
     * Specifies the max. time a MIME message file might stay in local cache without being read.
     */
    MAILSTORAGE_IN_MEMORY_CACHE_MAX_IDLE_SECONDS("mailstorage.inMemoryCacheMaxIdleSeconds", I(3600)),
    /**
     * Specifies the dedicated spool directory used for temporary composition space data that is too big to be held in memory.
     */
    MAILSTORAGE_SPOOL_DIR("mailstorage.spoolDir", ""),
    /**
     * Whether the local file cache is enabled.
     */
    MAILSTORAGE_FILE_CACHE_ENABLED("mailstorage.fileCacheEnabled", Boolean.FALSE),
    /**
     * Specifies the directory for the file cache.
     */
    MAILSTORAGE_FILE_CACHE_DIR("mailstorage.fileCacheDir", ""),
    /**
     * Specifies the max. time that metadata records about open composition spaces might stay in an in-memory cache without being accessed.
     */
    MAILSTORAGE_FILE_CACHE_MAX_IDLE_SECONDS("mailstorage.fileCacheMaxIdleSeconds", I(300)),
    /**
     * Specifies whether eager upload checks for max. mail size and quota are enabled.
     */
    MAILSTORAGE_EAGER_UPLOAD_CHECKS_ENABLED("mailstorage.eagerUploadChecksEnabled", Boolean.FALSE),
    /**
     * Specifies the default timeout for reading responses from mail server.
     */
    MAILSTORAGE_DEFAULT_READ_RESPONSE_TIMEOUT("mailstorage.readResponseTimeout", L(60_000L)),
    /**
     * Specifies the long timeout for reading responses from mail server.
     */
    MAILSTORAGE_LONG_READ_RESPONSE_TIMEOUT("mailstorage.longReadResponseTimeout", L(300_000L)),
    /**
     * Specifies the operation wait time when waiting for concurrent operations to complete.
     */
    MAILSTORAGE_OPERATION_WAIT_TIME("mailstorage.operationWaitTime", L(10_000L)),
    /**
     * Specifies whether to iterate messages in draft folder if a search-based look-up does not find the associated draft mail for a
     * composition space.
     */
    MAILSTORAGE_ITERATE_IF_NOT_FOUND("mailstorage.iterateDraftMessagesIfNotFound", Boolean.FALSE),

    ;

    private final Object defaultValue;
    private final String fqn;

    /**
     * Initializes a new {@link RedisProperty}.
     *
     * @param appendix The appendix for full-qualified name
     * @param defaultValue The default value
     */
    private CompositionSpaceProperties(String appendix, Object defaultValue) {
        this.fqn = "com.openexchange.mail.compose." + appendix;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
