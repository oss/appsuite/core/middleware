/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.compose;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import com.openexchange.java.Strings;
import com.openexchange.mail.compose.Message.ContentType;
import com.openexchange.mail.compose.Message.Priority;
import com.openexchange.mail.dataobjects.SecuritySettings;

/**
 * {@link MessageDescription} - A data holder for creating/updating messages.
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.2
 */
public class MessageDescription {

    private Address from;
    private Address sender;
    private List<Address> to;
    private List<Address> cc;
    private List<Address> bcc;
    private List<Address> replyTo;
    private String subject;
    private String content;
    private ContentType contentType;
    private boolean requestReadReceipt;
    private SharedAttachmentsInfo sharedAttachmentsInfo;
    private List<Attachment> attachments;
    private Meta meta;
    private SecuritySettings security;
    private Priority priority;
    private ClientToken clientToken;
    private boolean contentEncrypted;
    private Map<String, String> customHeaders;
    private Date dateToSend;
    private UUID scheduledMailId;

    private boolean containsFrom;
    private boolean containsSender;
    private boolean containsTo;
    private boolean containsCc;
    private boolean containsBcc;
    private boolean containsReplyTo;
    private boolean containsSubject;
    private boolean containsContent;
    private boolean containsContentType;
    private boolean containsRequestReadReceipt;
    private boolean containsSharedAttachmentsInfo;
    private boolean containsAttachments;
    private boolean containsMeta;
    private boolean containsSecurity;
    private boolean containsPriority;
    private boolean containsClientToken;
    private boolean containsContentEncrypted;
    private boolean containsCustomHeaders;
    private boolean containsDateToSend;
    private boolean containsScheduledMailId;

    /**
     * Initializes a new {@link MessageDescription}.
     */
    public MessageDescription() {
        super();
        priority = Priority.NORMAL;
        sharedAttachmentsInfo = SharedAttachmentsInfo.DISABLED;
        security = null;
        meta = Meta.META_NEW;
    }

    /**
     * Checks if this message description seems to be equal to specified other message description.
     *
     * @param other The other message description to check against
     * @return <code>true</code> if seems to be equal; otherwise <code>false</code>
     */
    public boolean seemsEqual(MessageDescription other) {
        // Check flags
        if (other.containsContentEncrypted && contentEncrypted != other.contentEncrypted) {
            return false;
        }
        if (other.containsContentType && contentType != other.contentType) {
            return false;
        }
        if (other.containsPriority && priority != other.priority) {
            return false;
        }
        if (other.containsRequestReadReceipt && requestReadReceipt != other.requestReadReceipt) {
            return false;
        }

        // Check addresses
        if (other.containsFrom) {
            if (from == null) {
                if (other.from != null) {
                    return false;
                }
            } else if (!from.equals(other.from)) {
                return false;
            }
        }
        if (other.containsTo) {
            if (to == null) {
                if (other.to != null) {
                    return false;
                }
            } else if (!to.equals(other.to)) {
                return false;
            }
        }
        if (other.containsCc) {
            if (cc == null) {
                if (other.cc != null) {
                    return false;
                }
            } else if (!cc.equals(other.cc)) {
                return false;
            }
        }
        if (other.containsBcc) {
            if (bcc == null) {
                if (other.bcc != null) {
                    return false;
                }
            } else if (!bcc.equals(other.bcc)) {
                return false;
            }
        }
        if (other.containsReplyTo) {
            if (replyTo == null) {
                if (other.replyTo != null) {
                    return false;
                }
            } else if (!replyTo.equals(other.replyTo)) {
                return false;
            }
        }
        if (other.containsSender) {
            if (sender == null) {
                if (other.sender != null) {
                    return false;
                }
            } else if (!sender.equals(other.sender)) {
                return false;
            }
        }

        // Check subject
        if (other.containsSubject) {
            if (subject == null) {
                if (other.subject != null) {
                    return false;
                }
            } else if (!subject.equals(other.subject)) {
                return false;
            }
        }

        // Check content
        if (other.containsContent) {
            if (content == null) {
                if (other.content != null) {
                    return false;
                }
            } else if (!content.equals(other.content)) {
                return false;
            }
        }

        // Check rest
        if (other.containsCustomHeaders) {
            if (customHeaders == null) {
                if (other.customHeaders != null) {
                    return false;
                }
            } else if (!customHeaders.equals(other.customHeaders)) {
                return false;
            }
        }
        if (other.containsMeta) {
            if (meta == null) {
                if (other.meta != null) {
                    return false;
                }
            } else if (!meta.equals(other.meta)) {
                return false;
            }
        }
        if (other.containsSecurity) {
            if (security == null) {
                if (other.security != null) {
                    return false;
                }
            } else if (!security.equals(other.security)) {
                return false;
            }
        }
        if (other.containsSharedAttachmentsInfo) {
            if (sharedAttachmentsInfo == null) {
                if (other.sharedAttachmentsInfo != null) {
                    return false;
                }
            } else if (!sharedAttachmentsInfo.equals(other.sharedAttachmentsInfo)) {
                return false;
            }
        }
        if (other.containsDateToSend) {
            if (dateToSend == null) {
                if (other.dateToSend != null) {
                    return false;
                }
            } else if (!dateToSend.equals(other.dateToSend)) {
                return false;
            }
        }
        if (other.containsScheduledMailId) {
            if (scheduledMailId == null) {
                if (other.scheduledMailId != null) {
                    return false;
                }
            } else if (!scheduledMailId.equals(other.scheduledMailId)) {
                return false;
            }
        }
        if (other.containsValidClientToken()) {
            if (clientToken == null) {
                if (other.clientToken != null) {
                    return false;
                }
            } else if (!clientToken.equals(other.clientToken)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Gets the "From" address.
     *
     * @return The "From" address
     */
    public Address getFrom() {
        return from;
    }

    /**
     * Sets the "From" address.
     *
     * @param from The "From" address
     * @return This instance
     */
    public MessageDescription setFrom(Address from) {
        this.from = from;
        containsFrom = true;
        return this;
    }

    /**
     * Checks if "From" address has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsFrom() {
        return containsFrom;
    }

    /**
     * Removes (aka unsets) the "From" address.
     */
    public void removeFrom() {
        from = null;
        containsFrom = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the "Sender" address.
     *
     * @return The "Sender" address
     */
    public Address getSender() {
        return sender;
    }

    /**
     * Sets the "Sender" address.
     *
     * @param sender The "Sender" address
     * @return This instance
     */
    public MessageDescription setSender(Address sender) {
        this.sender = sender;
        containsSender = true;
        return this;
    }

    /**
     * Checks if "Sender" address has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsSender() {
        return containsSender;
    }

    /**
     * Removes (aka unsets) the "Sender" address.
     */
    public void removeSender() {
        sender = null;
        containsSender = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the "To" addresses.
     *
     * @return The "To" addresses
     */
    public List<Address> getTo() {
        return to;
    }

    /**
     * Sets the "To" addresses.
     *
     * @param to The "To" addresses
     * @return This instance
     */
    public MessageDescription setTo(List<Address> to) {
        this.to = to;
        containsTo = true;
        return this;
    }

    /**
     * Add given "To" addresses.
     *
     * @param to The "To" addresses
     * @return This instance
     */
    public MessageDescription addTo(List<Address> to) {
        if (this.to == null) {
            return setTo(to);
        }

        if (to != null) {
            this.to.addAll(to);
        }
        return this;
    }

    /**
     * Checks if "To" addresses has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsTo() {
        return containsTo;
    }

    /**
     * Removes (aka unsets) the "To" addresses.
     */
    public void removeTo() {
        to = null;
        containsTo = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the "Cc" addresses.
     *
     * @return The "Cc" addresses
     */
    public List<Address> getCc() {
        return cc;
    }

    /**
     * Sets the "Cc" addresses.
     *
     * @param cc The "Cc" addresses
     * @return This instance
     */
    public MessageDescription setCc(List<Address> cc) {
        this.cc = null == cc ? cc : new LinkedList<>(cc);
        containsCc = true;
        return this;
    }

    /**
     * Add given "Cc" addresses.
     *
     * @param cc The "Cc" addresses
     * @return This instance
     */
    public MessageDescription addCc(List<Address> cc) {
        if (this.cc == null) {
            return setCc(cc);
        }

        if (cc != null) {
            this.cc.addAll(cc);
        }
        return this;
    }

    /**
     * Checks if "Cc" addresses has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsCc() {
        return containsCc;
    }

    /**
     * Removes (aka unsets) the "Cc" addresses.
     */
    public void removeCc() {
        cc = null;
        containsCc = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the "Bcc" addresses.
     *
     * @return The "Bcc" addresses
     */
    public List<Address> getBcc() {
        return bcc;
    }

    /**
     * Sets the "Bcc" addresses.
     *
     * @param bcc The "Bcc" addresses
     * @return This instance
     */
    public MessageDescription setBcc(List<Address> bcc) {
        this.bcc = bcc;
        containsBcc = true;
        return this;
    }

    /**
     * Adds given "Bcc" addresses.
     *
     * @param bcc The "Bcc" addresses
     * @return This instance
     */
    public MessageDescription addBcc(List<Address> bcc) {
        if (this.bcc == null) {
            return setBcc(bcc);
        }

        if (bcc != null) {
            this.bcc.addAll(bcc);
        }
        return this;
    }

    /**
     * Checks if "Bcc" addresses has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsBcc() {
        return containsBcc;
    }

    /**
     * Removes (aka unsets) the "Bcc" addresses.
     */
    public void removeBcc() {
        bcc = null;
        containsBcc = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the "Reply-To" addresses.
     *
     * @return The "Reply-To" addresses
     */
    public List<Address> getReplyTo() {
        return replyTo;
    }

    /**
     * Sets the "Reply-To" addresses.
     *
     * @param replyTo The "Reply-To" addresses
     * @return This instance
     */
    public MessageDescription setReplyTo(List<Address> replyTo) {
        this.replyTo = replyTo;
        containsReplyTo = true;
        return this;
    }

    /**
     * Adds given "Reply-To" addresses.
     *
     * @param replyTo The "Reply-To" addresses
     * @return This instance
     */
    public MessageDescription addReplyTo(List<Address> replyTo) {
        if (this.replyTo == null) {
            return setReplyTo(replyTo);
        }

        if (replyTo != null) {
            this.replyTo.addAll(replyTo);
        }
        return this;
    }

    /**
     * Checks if "Reply-To" addresses has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsReplyTo() {
        return containsReplyTo;
    }

    /**
     * Removes (aka unsets) the "Reply-To" addresses.
     */
    public void removeReplyTo() {
        replyTo = null;
        containsReplyTo = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the subject.
     *
     * @return The subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Sets the subject.
     *
     * @param subject The subject to set
     * @return This instance
     */
    public MessageDescription setSubject(String subject) {
        this.subject = subject;
        containsSubject = true;
        return this;
    }

    /**
     * Checks if subject has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsSubject() {
        return containsSubject;
    }

    /**
     * Removes (aka unsets) the subject.
     */
    public void removeSubject() {
        subject = null;
        containsSubject = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the content.
     *
     * @return The content
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets the content.
     *
     * @param content The content
     * @return This instance
     */
    public MessageDescription setContent(String content) {
        this.content = content;
        containsContent = true;
        return this;
    }

    /**
     * Checks if content has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsContent() {
        return containsContent;
    }

    /**
     * Removes (aka unsets) the content.
     */
    public void removeContent() {
        content = null;
        containsContent = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the Content-Type.
     *
     * @return The Content-Type
     */
    public ContentType getContentType() {
        return contentType;
    }

    /**
     * Sets the Content-Type.
     *
     * @param contentType The Content-Type
     * @return This instance
     */
    public MessageDescription setContentType(ContentType contentType) {
        this.contentType = contentType;
        containsContentType = true;
        return this;
    }

    /**
     * Checks if Content-Type has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsContentType() {
        return containsContentType;
    }

    /**
     * Removes (aka unsets) the Content-Type.
     */
    public void removeContentType() {
        contentType = null;
        containsContentType = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Checks for content-encrypted flag.
     *
     * @return <code>true</code> if content-encrypted flag is set; otherwise <code>false</code>
     */
    public boolean isContentEncrypted() {
        return contentEncrypted;
    }

    /**
     * Sets the content-encrypted flag.
     *
     * @param contentEncrypted The content-encrypted flag
     * @return This instance
     */
    public MessageDescription setContentEncrypted(boolean contentEncrypted) {
        this.contentEncrypted = contentEncrypted;
        containsContentEncrypted = true;
        return this;
    }

    /**
     * Checks if content-encrypted flag has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsContentEncrypted() {
        return containsContentEncrypted;
    }

    /**
     * Removes (aka unsets) the content-encrypted flag.
     */
    public void removeContentEncrypted() {
        contentEncrypted = false;
        containsContentEncrypted = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the custom headers.
     *
     * @return The custom headers
     */
    public Map<String, String> getCustomHeaders() {
        return customHeaders;
    }

    /**
     * Sets the custom headers.
     *
     * @param customHeaders The custom headers
     */
    public void setCustomHeaders(Map<String, String> customHeaders) {
        this.customHeaders = customHeaders;
        containsCustomHeaders = true;
    }

    /**
     * Adds specified custom header.
     *
     * @param name The header name
     * @param value The header value
     */
    public void addCustomHeader(String name, String value) {
        if (Strings.isNotEmpty(name) && Strings.isNotEmpty(value)) {
            Map<String, String> customHeaders = this.customHeaders;
            if (customHeaders == null) {
                customHeaders = new LinkedHashMap<>();
                this.customHeaders = customHeaders;
            }
            customHeaders.put(name, value);
            containsCustomHeaders = true;
        }
    }

    /**
     * Checks if custom headers have been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsCustomHeaders() {
        return containsCustomHeaders;
    }

    /**
     * Removes (aka unsets) the custom headers.
     */
    public void removeCustomHeaders() {
        customHeaders = null;
        containsCustomHeaders = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Checks for request-read-receipt flag.
     *
     * @return <code>true</code> if request-read-receipt flag is set; otherwise <code>false</code>
     */
    public boolean isRequestReadReceipt() {
        return requestReadReceipt;
    }

    /**
     * Sets the request-read-receipt flag.
     *
     * @param requestReadReceipt The request-read-receipt flag
     * @return This instance
     */
    public MessageDescription setRequestReadReceipt(boolean requestReadReceipt) {
        this.requestReadReceipt = requestReadReceipt;
        containsRequestReadReceipt = true;
        return this;
    }

    /**
     * Checks if request-read-receipt flag has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsRequestReadReceipt() {
        return containsRequestReadReceipt;
    }

    /**
     * Removes (aka unsets) the request-read-receipt flag.
     */
    public void removeRequestReadReceipt() {
        requestReadReceipt = false;
        containsRequestReadReceipt = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the shared attachments info.
     *
     * @return The shared attachments info
     */
    public SharedAttachmentsInfo getSharedAttachmentsInfo() {
        return sharedAttachmentsInfo;
    }

    /**
     * Sets the shared attachments info.
     *
     * @param sharedAttachmentsInfo The shared attachments info
     * @return This instance
     */
    public MessageDescription setSharedAttachmentsInfo(SharedAttachmentsInfo sharedAttachmentsInfo) {
        this.sharedAttachmentsInfo = sharedAttachmentsInfo;
        containsSharedAttachmentsInfo = true;
        return this;
    }

    /**
     * Checks if shared attachments info has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsSharedAttachmentsInfo() {
        return containsSharedAttachmentsInfo;
    }

    /**
     * Removes (aka unsets) the shared attachments info.
     */
    public void removeSharedAttachmentsInfo() {
        sharedAttachmentsInfo = SharedAttachmentsInfo.DISABLED;
        containsSharedAttachmentsInfo = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the attachments.
     *
     * @return The attachments
     */
    public List<Attachment> getAttachments() {
        return attachments;
    }

    /**
     * Sets the attachments.
     *
     * @param attachments The attachments to set
     * @return This instance
     */
    public MessageDescription setAttachments(List<? extends Attachment> attachments) {
        if (attachments == null) {
            this.attachments = null;
        } else {
            this.attachments = new ArrayList<>(attachments.size());
            for (Attachment attachment : attachments) {
                this.attachments.add(attachment);
            }
        }
        containsAttachments = true;
        return this;
    }

    /**
     * Checks if attachments have been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsAttachments() {
        return containsAttachments;
    }

    /**
     * Removes (aka unsets) the attachments.
     */
    public void removeAttachments() {
        attachments = null;
        containsAttachments = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets meta data.
     *
     * @return The meta data
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * Sets the meta data.
     *
     * @param meta The meta data
     * @return This instance
     */
    public MessageDescription setMeta(Meta meta) {
        this.meta = meta;
        containsMeta = true;
        return this;
    }

    /**
     * Checks if meta data has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsMeta() {
        return containsMeta;
    }

    /**
     * Removes (aka unsets) the meta data.
     */
    public void removeMeta() {
        meta = null;
        containsMeta = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the securitySettings object.
     *
     * @return The securitySettings object
     */
    public SecuritySettings getSecurity() {
        return security;
    }

    /**
     * Sets the securitySettings object.
     *
     * @param security The securitySettings object
     * @return This instance
     */
    public MessageDescription setSecurity(SecuritySettings security) {
        this.security = security;
        containsSecurity = true;
        return this;
    }

    /**
     * Checks if security object has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsSecurity() {
        return containsSecurity;
    }

    /**
     * Checks if security object has been set before and its value is not <code>null</code>.
     *
     * @return <code>true</code> if previously set and not <code>null</code>; otherwise <code>false</code>
     */
    public boolean containsNotNullSecurity() {
        return containsSecurity && security != null;
    }

    /**
     * Removes (aka unsets) the security object.
     */
    public void removeSecurity() {
        security = null;
        containsSecurity = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the priority.
     *
     * @return The priority
     */
    public Priority getPriority() {
        return priority;
    }

    /**
     * Sets the priority.
     *
     * @param priority The priority to set
     * @return This instance
     */
    public MessageDescription setPriority(Priority priority) {
        this.priority = priority;
        containsPriority = true;
        return this;
    }

    /**
     * Checks if priority has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsPriority() {
        return containsPriority;
    }

    /**
     * Removes (aka unsets) the priority.
     */
    public void removePriority() {
        priority = null;
        containsPriority = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the client token
     *
     * @return The client token
     */
    public ClientToken getClientToken() {
        return clientToken;
    }

    /**
     * Sets the client token.
     *
     * @param clientToken The client token to set
     * @return This instance
     */
    public MessageDescription setClientToken(ClientToken clientToken) {
        this.clientToken = clientToken;
        containsClientToken = true;
        return this;
    }

    /**
     * Checks if client token has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsClientToken() {
        return containsClientToken;
    }

    /**
     * Checks if this instance contains a valid client token.
     *
     * @return <code>true</code> if a valid client token is contained; otherwise <code>false</code>
     */
    public boolean containsValidClientToken() {
        return clientToken != null && clientToken.isPresent();
    }

    /**
     * Removes (aka unsets) the client token.
     */
    public void removeClientToken() {
        clientToken = null;
        containsClientToken = false;
    }

    // --------------------------------------------------------------------------------

    public Date getDateToSend() {
        return dateToSend;
    }

    /**
     * Sets the date-to-send.
     *
     * @param dateToSend The date-to-send to set
     * @return This instance
     */
    public MessageDescription setDateToSend(Date dateToSend) {
        this.dateToSend = dateToSend;
        containsDateToSend = true;
        return this;
    }

    /**
     * Checks if date-to-send has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsDateToSend() {
        return containsDateToSend;
    }

    /**
     * Removes (aka unsets) the date-to-send.
     */
    public void removeDateToSend() {
        dateToSend = null;
        containsDateToSend = false;
    }

    // --------------------------------------------------------------------------------

    /**
     * Gets the scheduled mail identifier.
     *
     * @return The scheduled mail identifier
     */
    public UUID getScheduledMailId() {
        return scheduledMailId;
    }

    /**
     * Sets the scheduled mail identifier.
     *
     * @param scheduledMailId The scheduled mail identifier
     * @return This instance
     */
    public MessageDescription setScheduledMailId(UUID scheduledMailId) {
        this.scheduledMailId = scheduledMailId;
        containsScheduledMailId = true;
        return this;
    }

    /**
     * Checks if scheduled mail identifier has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsScheduledMailId() {
        return containsScheduledMailId;
    }

    /**
     * Removes (aka unsets) the scheduled mail identifier.
     */
    public void removeScheduledMailId() {
        scheduledMailId = null;
        containsScheduledMailId = false;
    }

    // --------------------------------------------------------------------------------

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append('[');
        if (from != null) {
            builder.append("from=").append(from).append(", ");
        }
        if (sender != null) {
            builder.append("sender=").append(sender).append(", ");
        }
        if (to != null) {
            builder.append("to=").append(to).append(", ");
        }
        if (cc != null) {
            builder.append("cc=").append(cc).append(", ");
        }
        if (bcc != null) {
            builder.append("bcc=").append(bcc).append(", ");
        }
        if (subject != null) {
            builder.append("subject=").append(subject).append(", ");
        }
        if (content != null) {
            builder.append("content=").append(content).append(", ");
        }
        if (contentType != null) {
            builder.append("contentType=").append(contentType).append(", ");
        }
        builder.append("requestReadReceipt=").append(requestReadReceipt).append(", ");
        if (sharedAttachmentsInfo != null) {
            builder.append("sharedAttachmentsInfo=").append(sharedAttachmentsInfo).append(", ");
        }
        if (attachments != null) {
            builder.append("attachments=").append(attachments).append(", ");
        }
        if (meta != null) {
            builder.append("meta=").append(meta).append(", ");
        }
        if (security != null) {
            builder.append("security=").append(security).append(", ");
        }
        if (priority != null) {
            builder.append("priority=").append(priority).append(", ");
        }
        if (clientToken != null) {
            builder.append("clientToken=").append(clientToken);
        }
        builder.append(']');
        return builder.toString();
    }

}
