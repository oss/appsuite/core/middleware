/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.compose;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import javax.mail.internet.MimeUtility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import org.slf4j.Logger;
import com.openexchange.exception.OXException;
import com.openexchange.i18n.LocaleTools;
import com.openexchange.java.Strings;
import com.openexchange.java.util.UUIDs;
import com.openexchange.mail.MailPath;
import com.openexchange.mail.compose.Meta.MetaType;
import com.openexchange.mail.dataobjects.SecuritySettings;
import com.openexchange.mail.mime.MessageHeaders;
import com.openexchange.mail.mime.utils.MimeMessageUtility;
import com.openexchange.mail.service.SecurityParsingService;
import com.openexchange.mail.utils.MailPasswordUtil;
import com.openexchange.server.services.ServerServiceRegistry;

/**
 * {@link HeaderUtility} - Utility class to set/read headers used for composing a mail.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.2
 */
public class HeaderUtility {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(HeaderUtility.class);
    }

    /** "X-OX-Shared-Attachments" */
    public static final String HEADER_X_OX_SHARED_ATTACHMENTS = MessageHeaders.HDR_X_OX_SHARED_ATTACHMENTS;

    /** "X-OX-Shared-Attachment-Reference" */
    public static final String HEADER_X_OX_SHARED_ATTACHMENT_REFERENCE = MessageHeaders.HDR_X_OX_SHARED_ATTACHMENT_REFERENCE;

    /** "X-OX-Shared-Folder-Reference" */
    public static final String HEADER_X_OX_SHARED_FOLDER_REFERENCE = MessageHeaders.HDR_X_OX_SHARED_FOLDER_REFERENCE;

    /** "X-OX-Security" */
    public static final String HEADER_X_OX_SECURITY = MessageHeaders.HDR_X_OX_SECURITY;

    /** "X-OX-Meta" */
    public static final String HEADER_X_OX_META = MessageHeaders.HDR_X_OX_META;

    /** "X-OX-Read-Receipt" */
    public static final String HEADER_X_OX_READ_RECEIPT = MessageHeaders.HDR_X_OX_READ_RECEIPT;

    /** "X-OX-Custom-Headers" */
    public static final String HEADER_X_OX_CUSTOM_HEADERS = MessageHeaders.HDR_X_OX_CUSTOM_HEADERS;

    /** "X-OX-Content-Type" */
    public static final String HEADER_X_OX_CONTENT_TYPE = MessageHeaders.HDR_X_OX_CONTENT_TYPE;

    /** "X-OX-Composition-Space-Id" */
    public static final String HEADER_X_OX_COMPOSITION_SPACE_ID = MessageHeaders.HDR_X_OX_COMPOSITION_SPACE_ID;

    /** {@value MessageHeaders#HDR_X_OX_CLIENT_TOKEN} */
    public static final String HEADER_X_OX_CLIENT_TOKEN = MessageHeaders.HDR_X_OX_CLIENT_TOKEN;

    /** "X-OX-Reply-To" */
    public static final String HEADER_X_OX_REPLY_TO = MessageHeaders.HDR_X_OX_REPLY_TO;

    /** "X-OX-Composition-Space-Look-Up" */
    public static final String HEADER_X_OX_COMPOSITION_SPACE_LOOK_UP = MessageHeaders.HDR_X_OX_COMPOSITION_SPACE_LOOK_UP;

    /** "X-OX-Date-To-Send" */
    public static final String HEADER_X_OX_DATE_TO_SEND = MessageHeaders.HDR_X_OX_DATE_TO_SEND;

    /** "X-OX-Scheduled-Mail-Id" */
    public static final String HEADER_X_OX_SCHEDULED_MAIL_ID = MessageHeaders.HDR_X_OX_SCHEDULED_MAIL_ID;

    /**
     * Initializes a new {@link HeaderUtility}.
     */
    private HeaderUtility() {
        super();
    }

    private static final java.security.Key HEADER_PW = MailPasswordUtil.generateSecretKey("open-xchange");

    /**
     * Encodes given header value
     *
     * @param used The number of already consumed characters in header line
     * @param raw The raw header value
     * @return The encoded header value
     */
    public static String encodeHeaderValue(int used, String raw) {
        if (null == raw) {
            return null;
        }

        try {
            return MimeMessageUtility.forceFold(used, MailPasswordUtil.encrypt(raw, HEADER_PW));
        } catch (GeneralSecurityException x) {
            LoggerHolder.LOG.debug("Failed to encode header value", x);
            return MimeMessageUtility.forceFold(used, raw);
        }
    }

    /**
     * Decodes given header value
     *
     * @param encoded The encoded header value
     * @return The decoded header value
     */
    public static String decodeHeaderValue(String encoded) {
        if (null == encoded) {
            return null;
        }

        try {
            return MailPasswordUtil.decrypt(MimeUtility.unfold(encoded), HEADER_PW);
        } catch (GeneralSecurityException x) {
            LoggerHolder.LOG.debug("Failed to decode header value", x);
            return MimeUtility.unfold(encoded);
        }
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Generates the header value for given shared folder reference.
     *
     * @param sharedFolderReference The shared folder reference
     * @return The resulting header value
     */
    public static String sharedFolderReference2HeaderValue(SharedFolderReference sharedFolderReference) {
        return new JSONObject(8).putSafe("folderId", sharedFolderReference.getFolderId()).toString();
    }

    /**
     * Parses given header value to appropriate shared folder reference.
     *
     * @param headerValue The header value to parse
     * @return The resulting shared folder reference
     */
    public static SharedFolderReference headerValue2SharedFolderReference(String headerValue) {
        if (Strings.isEmpty(headerValue)) {
            return null;
        }

        try {
            JSONObject jSharedAttachment = JSONServices.parseObject(headerValue);
            String folderId = jSharedAttachment.optString("folderId", null);
            return SharedFolderReference.valueOf(folderId);
        } catch (JSONException e) {
            LoggerHolder.LOG.warn("Header value cannot be parsed to shared folder reference: {}", headerValue, e);
            return null;
        }
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Generates the header value for given shared attachment reference.
     *
     * @param sharedAttachmentReference The shared attachment reference
     * @return The resulting header value
     */
    public static String sharedAttachmentReference2HeaderValue(SharedAttachmentReference sharedAttachmentReference) {
        return new JSONObject(8).putSafe("id", sharedAttachmentReference.getAttachmentId()).putSafe("folderId", sharedAttachmentReference.getFolderId()).toString();
    }

    /**
     * Parses given header value to appropriate shared attachment reference.
     *
     * @param headerValue The header value to parse
     * @return The resulting shared attachment reference
     */
    public static SharedAttachmentReference headerValue2SharedAttachmentReference(String headerValue) {
        if (Strings.isEmpty(headerValue)) {
            return null;
        }

        try {
            JSONObject jSharedAttachment = JSONServices.parseObject(headerValue);
            String id = jSharedAttachment.optString("id", null);
            String folderId = jSharedAttachment.optString("folderId", null);
            return new SharedAttachmentReference(id, folderId);
        } catch (JSONException e) {
            LoggerHolder.LOG.warn("Header value cannot be parsed to shared attachment reference: {}", headerValue, e);
            return null;
        }
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Generates the header value for given meta instance.
     *
     * @param meta The meta instance
     * @return The resulting header value
     */
    public static String meta2HeaderValue(Meta meta) {
        if (null == meta) {
            return new JSONObject(2).putSafe("type", Type.NEW.getId()).toString();
        }

        JSONObject jMeta = new JSONObject(8).putSafe("type", meta.getType().getId());
        {
            Date date = meta.getDate();
            if (null != date) {
                jMeta.putSafe("date", Long.valueOf(date.getTime()));
            }
        }
        {
            String timeZoneId = meta.getTimeZoneId();
            if (null != timeZoneId) {
                jMeta.putSafe("timeZoneId", timeZoneId);
            }
        }
        {
            MailPath replyFor = meta.getReplyFor();
            if (null != replyFor) {
                jMeta.putSafe("replyFor", replyFor.toString());
            }
        }
        {
            MailPath editFor = meta.getEditFor();
            if (null != editFor) {
                jMeta.putSafe("editFor", editFor.toString());
            }
        }
        {
            List<MailPath> forwardsFor = meta.getForwardsFor();
            if (null != forwardsFor) {
                JSONArray jForwardsFor = new JSONArray(forwardsFor.size());
                for (MailPath forwardFor : forwardsFor) {
                    jForwardsFor.put(forwardFor.toString());
                }
                jMeta.putSafe("forwardsFor", jForwardsFor);
            }
        }

        return jMeta.toString();
    }

    /**
     * Parses given header value to appropriate meta instance.
     *
     * @param headerValue The header value to parse
     * @return The resulting meta instance
     */
    public static Meta headerValue2Meta(String headerValue) {
        if (Strings.isEmpty(headerValue)) {
            return Meta.META_NEW;
        }

        try {
            JSONObject jMeta = JSONServices.parseObject(headerValue);

            Meta.Builder meta = Meta.builder();
            meta.withType(MetaType.typeFor(jMeta.optString("type", Type.NEW.getId())));
            {
                long lDate = jMeta.optLong("date", -1L);
                meta.withDate(lDate < 0 ? null : new Date(lDate));
            }
            {
                meta.withTimeZoneId(jMeta.optString("timeZoneId", null));
            }
            {
                String path = jMeta.optString("replyFor", null);
                meta.withReplyFor(Strings.isEmpty(path) ? null : new MailPath(path));
            }
            {
                String path = jMeta.optString("editFor", null);
                meta.withEditFor(Strings.isEmpty(path) ? null : new MailPath(path));
            }
            {
                JSONArray jPaths = jMeta.optJSONArray("forwardsFor");
                if (null != jPaths) {
                    List<MailPath> paths = new ArrayList<MailPath>(jPaths.length());
                    for (Object jPath : jPaths) {
                        paths.add(new MailPath(jPath.toString()));
                    }
                    meta.withForwardsFor(paths);
                }
            }
            return meta.build();
        } catch (Exception e) {
            LoggerHolder.LOG.warn("Header value cannot be parsed to meta information: {}", headerValue, e);
            return Meta.META_NEW;
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Generates the header value for given date to send.
     *
     * @param dateToSend The date to send
     * @return The resulting header value
     */
    public static String dateToSend2HeaderValue(Date dateToSend) {
        if (null == dateToSend) {
            return null;
        }

        return Long.toString(dateToSend.getTime());
    }

    /**
     * Parses given header value to appropriate date to send.
     *
     * @param headerValue The header value
     * @return The resulting date to send
     */
    public static Date headerValue2DateToSend(String headerValue) {
        if (Strings.isEmpty(headerValue)) {
            return null;
        }

        try {
            return new Date(Long.parseLong(headerValue.trim()));
        } catch (NumberFormatException e) {
            LoggerHolder.LOG.warn("Header value cannot be parsed to date: {}", headerValue, e);
            return null;
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Generates the header value for given scheduled mail identifier.
     *
     * @param scheduledMailId The scheduled mail identifier
     * @return The resulting header value
     */
    public static String scheduledMailId2HeaderValue(UUID scheduledMailId) {
        if (null == scheduledMailId) {
            return null;
        }

        return UUIDs.getUnformattedString(scheduledMailId);
    }

    /**
     * Parses given header value to appropriate scheduled mail identifier.
     *
     * @param headerValue The header value
     * @return The resulting scheduled mail identifier
     */
    public static UUID headerValue2ScheduledMailId(String headerValue) {
        if (Strings.isEmpty(headerValue)) {
            return null;
        }

        try {
            return UUIDs.fromUnformattedString(headerValue.trim());
        } catch (Exception e) {
            LoggerHolder.LOG.warn("Header value cannot be parsed to scheduled mail identifier: {}", headerValue, e);
            return null;
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Generates the header value for given custom headers.
     *
     * @param customHeaders The custom headers
     * @return The resulting header value
     */
    public static String customHeaders2HeaderValue(Map<String, String> customHeaders) {
        if (null == customHeaders) {
            return null;
        }

        JSONObject jCustomHeaders = new JSONObject(customHeaders.size());
        for (Map.Entry<String, String> customHeader : customHeaders.entrySet()) {
            jCustomHeaders.putSafe(customHeader.getKey(), customHeader.getValue());
        }
        return jCustomHeaders.toString();
    }

    /**
     * Parses given header value to appropriate custom headers.
     *
     * @param headerValue The header value
     * @return The resulting custom headers
     */
    public static Map<String, String> headerValue2CustomHeaders(String headerValue) {
        if (Strings.isEmpty(headerValue)) {
            return null;
        }

        try {
            JSONObject jCustomHeaders = JSONServices.parseObject(headerValue);
            Map<String, String> customHeaders = LinkedHashMap.newLinkedHashMap(jCustomHeaders.length());
            for (Map.Entry<String, Object> jCustomHeader : jCustomHeaders.entrySet()) {
                customHeaders.put(jCustomHeader.getKey(), jCustomHeader.getValue().toString());
            }
            return customHeaders;
        } catch (JSONException e) {
            LoggerHolder.LOG.warn("Header value cannot be parsed to custom headers: {}", headerValue, e);
            return null;
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static final String JSON_SHARED_ATTACHMENTS_DISABLED = new JSONObject(4).putSafe("enabled", Boolean.FALSE).toString();

    /**
     * Generates the header value for given shared-attachment info instance.
     *
     * @param sharedAttachmentsInfo The shared-attachment info instance
     * @return The resulting header value
     */
    public static String sharedAttachments2HeaderValue(SharedAttachmentsInfo sharedAttachmentsInfo) {
        if (null == sharedAttachmentsInfo || sharedAttachmentsInfo.isDisabled()) {
            return JSON_SHARED_ATTACHMENTS_DISABLED;
        }

        return new JSONObject(8).putSafe("enabled", Boolean.valueOf(sharedAttachmentsInfo.isEnabled()))
            .putSafe("language", sharedAttachmentsInfo.getLanguage() == null ? JSONObject.NULL : sharedAttachmentsInfo.getLanguage().toString())
            .putSafe("autoDelete", Boolean.valueOf(sharedAttachmentsInfo.isAutoDelete()))
            .putSafe("expiryDate", sharedAttachmentsInfo.getExpiryDate() == null ? JSONObject.NULL : Long.valueOf(sharedAttachmentsInfo.getExpiryDate().getTime()))
            .putSafe("password", sharedAttachmentsInfo.getPassword() == null ? JSONObject.NULL : sharedAttachmentsInfo.getPassword())
            .toString();
    }

    /**
     * Parses given header value to appropriate shared-attachment info instance.
     *
     * @param headerValue The header value to parse
     * @return The resulting shared-attachment info instance
     */
    public static SharedAttachmentsInfo headerValue2SharedAttachments(String headerValue) {
        if (Strings.isEmpty(headerValue)) {
            return SharedAttachmentsInfo.DISABLED;
        }

        try {
            JSONObject jSharedAttachments = JSONServices.parseObject(headerValue);

            SharedAttachmentsInfo.Builder sharedAttachments = SharedAttachmentsInfo.builder();
            sharedAttachments.withEnabled(jSharedAttachments.optBoolean("enabled", false));
            {
                String language = jSharedAttachments.optString("language", null);
                sharedAttachments.withLanguage(Strings.isEmpty(language) ? null : LocaleTools.getLocale(language));
            }
            sharedAttachments.withAutoDelete(jSharedAttachments.optBoolean("autoDelete", false));
            {
                long lDate = jSharedAttachments.optLong("expiryDate", -1L);
                sharedAttachments.withExpiryDate(lDate < 0 ? null : new Date(lDate));
            }
            sharedAttachments.withPassword(jSharedAttachments.optString("password", null));
            return sharedAttachments.build();
        } catch (JSONException e) {
            LoggerHolder.LOG.warn("Header value cannot be parsed to shared-attachments settings: {}", headerValue, e);
            return SharedAttachmentsInfo.DISABLED;
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static final String JSON_SECURITY_DISABLED = new JSONObject(4).putSafe("encrypt", Boolean.FALSE).putSafe("pgpInline", Boolean.FALSE).putSafe("sign", Boolean.FALSE).toString();

    /**
     * Generates the header value for given security instance.
     *
     * @param security The security instance
     * @return The resulting header value
     */
    public static String security2HeaderValue(SecuritySettings security) {
        if (null == security || !security.anythingSet()) {
            return JSON_SECURITY_DISABLED;
        }
        try {
            return security.toJSON().toString();
        } catch (JSONException e) {
            LoggerHolder.LOG.error("Error converting Security to JSON", e);
        }
        return "";
    }

    /**
     * Parses given header value to appropriate security instance.
     *
     * @param headerValue The header value to parse
     * @return The resulting security instance
     * @throws OXException
     */
    public static Optional<SecuritySettings> headerValue2Security(String headerValue) throws OXException {
        if (Strings.isEmpty(headerValue)) {
            return Optional.empty();
        }
        try {
            JSONObject json = JSONServices.parseObject(headerValue);
            if (json != null) {
                // If header is present, and we can't parse it, then throw error
                SecurityParsingService parser = ServerServiceRegistry.getInstance().getService(SecurityParsingService.class, true);
                return Optional.ofNullable(parser.parseJson(json));
            }

        } catch (JSONException e) {
            LoggerHolder.LOG.error("Problem parsing security object", e);
        }
        return Optional.empty();
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Generates the header value for given addresses.
     *
     * @param addresses The addresses
     * @return The resulting header value
     */
    public static String addresses2HeaderValue(List<Address> addresses) {
        if (addresses == null) {
            return null;
        }

        JSONArray jAddresses = new JSONArray(addresses.size());
        for (Address address : addresses) {
            JSONObject jAddress = new JSONObject(2)
                .putSafe("personal", address.getPersonal())
                .putSafe("address", address.getAddress());
            jAddresses.put(jAddress);
        }
        return jAddresses.toString();
    }

    /**
     * Parses given header value to appropriate security instance.
     *
     * @param headerValue The header value to parse
     * @return The resulting security instance
     */
    public static List<Address> headerValue2Addresses(String headerValue) {
        if (headerValue == null) {
            return null;
        }

        try {
            JSONArray jAddresses = JSONServices.parseArray(headerValue);
            List<Address> addresses = new ArrayList<Address>(jAddresses.length());
            for (Object oAddress : jAddresses) {
                JSONObject jAddress = (JSONObject) oAddress;
                addresses.add(new Address(jAddress.optString("personal", null), jAddress.optString("address", null)));
            }
            return addresses;
        } catch (JSONException e) {
            LoggerHolder.LOG.warn("Header value cannot be parsed to addresses: {}", headerValue, e);
            return Collections.emptyList();
        }
    }

}
