/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail;

import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;
import com.openexchange.exception.OXException;
import com.openexchange.mail.cache.EnqueueingMailAccessCache;
import com.openexchange.mail.cache.MailMessageCache;
import com.openexchange.mail.cache.SingletonMailAccessCache;
import com.openexchange.mail.config.MailPropertiesInit;
import com.openexchange.mail.event.EventPool;
import com.openexchange.mail.mime.MimeType2ExtMap;
import com.openexchange.mail.usersetting.UserSettingMailStorage;
import com.openexchange.server.Initialization;

/**
 * {@link MailInitialization} - Initializes whole mail implementation and therefore provides a central point for starting/stopping mail
 * implementation.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class MailInitialization implements Initialization {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(MailInitialization.class);

    private static final MailInitialization INSTANCE = new MailInitialization();

    private final AtomicBoolean started;

    /**
     * No instantiation
     */
    private MailInitialization() {
        super();
        started = new AtomicBoolean();
    }

    /**
     * @return The singleton instance of {@link MailInitialization}
     */
    public static MailInitialization getInstance() {
        return INSTANCE;
    }

    @Override
    public void start() throws OXException {
        if (!started.compareAndSet(false, true)) {
            return;
        }
        final Stack<Initialization> startedStack = new Stack<Initialization>();
        try {
            /*
             * Start global mail system
             */
            startUp(MailPropertiesInit.getInstance(), startedStack);
            startUp(new Initialization() {

                @Override
                public void start() throws OXException {
                    MailAccessWatcher.init();
                }

                @Override
                public void stop() {
                    MailAccessWatcher.stop();
                }
            }, startedStack);
//            startUp(new Initialization() {
//
//                public void start() throws AbstractOXException {
//                    JSONMessageCache.initInstance();
//                }
//
//                public void stop() {
//                    JSONMessageCache.releaseInstance();
//                }
//            }, startedStack);
            startUp(new Initialization() {

                @Override
                public void start() throws OXException {
                    EventPool.initInstance();
                }

                @Override
                public void stop() {
                    EventPool.releaseInstance();
                }
            }, startedStack);
            startUp(new Initialization() {

                @Override
                public void start() {
                    MailcapInitialization.getInstance().init();
                }

                @Override
                public void stop() {
                    // Nope
                }
            }, startedStack);
            // Ensure storage instance is initialized during start-up
            UserSettingMailStorage.getInstance();
        } catch (OXException e) {
            started.set(false);
            // Revoke
            for (Initialization startedInit : startedStack) {
                try {
                    startedInit.stop();
                } catch (Exception e1) {
                    LOG.error("Initialization could not be revoked", e1);
                }
            }
            throw e;
        }
    }

    private static void startUp(Initialization initialization, Stack<Initialization> startedStack) throws OXException {
        initialization.start();
        startedStack.push(initialization);
    }

    @Override
    public void stop() {
        if (!started.compareAndSet(true, false)) {
            LOG.warn("Duplicate shut-down of mail module aborted.");
            return;
        }
        /*
         * TODO: Remove Simulate bundle disappearance
         */
        // MailProvider.resetMailProvider();
        /*
         * Stop global mail system
         */
        EventPool.releaseInstance();
        MimeType2ExtMap.reset();
        EnqueueingMailAccessCache.releaseInstance();
        SingletonMailAccessCache.releaseInstance();
        MailMessageCache.getInstance().invalidateAllCachedMessages();
        UserSettingMailStorage.releaseInstance();
        MailAccessWatcher.stop();
        MailPropertiesInit.getInstance().stop();
    }

    public boolean isInitialized() {
        return started.get();
    }
}
