/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.mail.dataobjects;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Interface containing security information for a mail object.
 *
 */
public interface SecuritySettings {

    /**
     * Checks if any considerable settings have been applied.
     *
     * @return <code>true</code> if any considerable settings have been applied; otherwise <code>false</code>
     */
    boolean anythingSet();

    /**
     * Returns <code>true</code> if set for encryption
     *
     * @return <code>true</code> if set for encryption; otherwise <code>false</code>
     */
    boolean isEncrypt();
    
    /**
     * Returns <code>true</code> if requires authentication
     *
     * @return <code>true</code> if requires authentication; otherwise <code>false</code>
     */
    boolean isAuthRequired();

    /**
     * Returns <code>true</code> if <b>NOT</b> set for encryption
     *
     * @return <code>true</code> if <b>NOT</b> set for encryption; otherwise <code>false</code>
     */
    default boolean isNoEncrypt() {
        return !isEncrypt();
    }

    /**
     * Returns the authentication/token
     *
     * @return Authentication/token
     */
    String getAuthentication();

    /**
     * Update an authentication
     * updateAuthentication
     *
     * @param authToken
     */
    void updateAuthentication(String authToken);

    /**
     * Returns the type of crypto
     *
     * @return String representation of the crypto type
     */
    String getType();

    /**
     * toJSON
     *
     * @return JSON Representation of the object
     * @throws JSONException
     */
    JSONObject toJSON() throws JSONException;

    /**
     * Gets a duplicate SecurityObject
     * duplicate
     *
     * @param toCopy
     * @return A copy of the SecurityObject
     */
    SecuritySettings duplicate();

}
