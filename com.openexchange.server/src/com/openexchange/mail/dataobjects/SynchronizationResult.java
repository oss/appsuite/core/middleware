/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.dataobjects;

import com.openexchange.java.Strings;

/**
 * {@link SynchronizationResult} - Represents the synchronization result for a certain mail folder and a given synchronization token.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class SynchronizationResult {

    private final String[] deletedMailIds;
    private final String[] modifiedMailIds;
    private final String[] newMailIds;
    private final SyncToken newSyncToken;

    /**
     * Initializes a new (either final or continuation signaling) instance of {@link SynchronizationResult}.
     *
     * @param deletedMailIds The identifiers of the mails that were deleted
     * @param modifiedMailIds The identifiers of the mails that have changed
     * @param newMailIds The identifiers of new mails
     * @param newSyncToken The new sync token that shall be replayed to client
     */
    public SynchronizationResult(String[] deletedMailIds, String[] modifiedMailIds, String[] newMailIds, SyncToken newSyncToken) {
        super();
        this.deletedMailIds = deletedMailIds == null ? Strings.getEmptyStrings() : deletedMailIds;
        this.modifiedMailIds = modifiedMailIds == null ? Strings.getEmptyStrings() : modifiedMailIds;
        this.newMailIds = newMailIds == null ? Strings.getEmptyStrings() : newMailIds;
        this.newSyncToken = newSyncToken;
    }

    /**
     * Gets the identifiers of the mails that were deleted.
     *
     * @return The identifiers of the mails that were deleted (never <code>null</code>)
     */
    public String[] getDeletedMailIds() {
        return deletedMailIds;
    }

    /**
     * Gets the identifiers of the mails that have changed.
     *
     * @return The identifiers of the mails that have changed (never <code>null</code>)
     */
    public String[] getModifiedMailIds() {
        return modifiedMailIds;
    }

    /**
     * Gets the identifiers of new mails.
     *
     * @return The identifiers of new mails (never <code>null</code>)
     */
    public String[] getNewMailIds() {
        return newMailIds;
    }

    /**
     * Gets the new sync token that shall be replayed to client.
     *
     * @return The new sync token
     */
    public SyncToken getNewSyncToken() {
        return newSyncToken;
    }

}
