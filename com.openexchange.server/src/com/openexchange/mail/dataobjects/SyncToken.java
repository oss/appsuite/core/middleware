/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.mail.dataobjects;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;
import org.json.JSONObject;
import org.json.JSONServices;
import com.openexchange.java.Strings;
import com.openexchange.mail.api.MailCapabilities;

/**
 * {@link SyncToken} - Represents a synchronization token for a certain mail folder.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class SyncToken {

    /**
     * Parses specified token to an appropriate instance of <code>SyncToken</code>.
     *
     * @param token The token to parse
     * @return The resulting instance of <code>SyncToken</code>
     * @throws IllegalArgumentException If given token is invalid
     */
    public static SyncToken parseToken(String token) {
        if (Strings.isEmpty(token)) {
            throw new IllegalArgumentException("Token must not be null or empty");
        }

        try {
            JSONObject jSyncToken = JSONServices.parseObject(Base64.getDecoder().decode(token.getBytes(StandardCharsets.US_ASCII)));
            String modseq = jSyncToken.optString("modseq");
            String nextId = jSyncToken.optString("nextId");
            String validity = jSyncToken.optString("validity");
            return new SyncToken(validity, modseq, nextId, token);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid synchronization token: " + token, e);
        }
    }

    /**
     * Creates the appropriate instance of <code>SyncToken</code> for specified arguments - if possible.
     *
     * @param validity The validity string
     * @param modseq The modification sequence
     * @param nextId The expected next identifier
     * @return The resulting instance of <code>SyncToken</code> or empty
     */
    public static Optional<SyncToken> syncTokenFor(String validity, String modseq, String nextId) {
        try {
            return Optional.of(new SyncToken(validity, modseq, nextId));
        } catch (IllegalArgumentException e) { // NOSONARLINT
            return Optional.empty();
        }
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    private final String validity;
    private final String modseq;
    private final String nextId;
    private final String token;

    /**
     * Initializes a new instance of {@link SyncToken}.
     *
     * @param validity The validity string
     * @param modseq The modification sequence
     * @param nextId The expected next identifier
     * @throws IllegalArgumentException If initialization fails
     */
    public SyncToken(String validity, String modseq, String nextId) {
        this(validity, modseq, nextId, null);
    }

    /**
     * Initializes a new instance of {@link SyncToken}.
     *
     * @param validity The validity string
     * @param modseq The modification sequence
     * @param nextId The expected next identifier
     * @param token The token or <code>null</code>
     * @throws IllegalArgumentException If initialization fails
     */
    private SyncToken(String validity, String modseq, String nextId, String token) {
        super();
        if (Strings.isEmpty(validity)) {
            throw new IllegalArgumentException("Validity must not be null or empty");
        }
        if (Strings.isEmpty(modseq)) {
            throw new IllegalArgumentException("Modification sequence must not be null or empty");
        }
        if (Strings.isEmpty(nextId)) {
            throw new IllegalArgumentException("The expected next identifier must not be null or empty");
        }
        this.validity = validity;
        this.modseq = modseq;
        this.nextId = nextId;
        if (token != null) {
            this.token = token;
        } else {
            try {
                JSONObject jSyncToken = new JSONObject(4);
                jSyncToken.putSafe("modseq", modseq);
                jSyncToken.putSafe("nextId", nextId);
                jSyncToken.putSafe("validity", validity);
                this.token = new String(Base64.getEncoder().encode(jSyncToken.toByteArray()), StandardCharsets.US_ASCII);
            } catch (Exception e) {
                throw new IllegalArgumentException("Failed to encode synchronization token", e);
            }
        }
    }

    /**
     * Gets the expected next identifier.
     *
     * @return The next identifier.
     */
    public String getNextId() {
        return nextId;
    }

    /**
     * Gets the validity string
     *
     * @return The validity string
     */
    public String getValidity() {
        return validity;
    }

    /**
     * Gets the modification sequence.
     * <p>
     * Only available if {@link MailCapabilities#hasSynchronization()} signals <code>true</code>; otherwise <code>null</code> is returned.
     *
     * @return The modification sequence or <code>null</code>
     */
    public String getModseq() {
        return modseq;
    }

    /**
     * Gets the token representing this <code>SyncToken</code>'s state.
     *
     * @return The token
     */
    public String getToken() {
        return token;
    }

}
