/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.usersetting;

import java.util.Arrays;
import java.util.Map.Entry;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;
import com.openexchange.mail.usersetting.UserSettingMail.Signature;

/**
 * {@link UserSettingMailCodec} - Cache codec for user mail settings.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class UserSettingMailCodec extends AbstractJSONObjectCacheValueCodec<UserSettingMail> {

    private static final UUID CODEC_ID = UUID.fromString("be1411ad-2b99-4a95-00fc-c0e11e000001");

    private static final String CONTEXT_ID_FIELD = "c";
    private static final String USER_ID_FIELD = "u";
    private static final String BITS_FIELD = "b";
    private static final String SEND_ADDR_FIELD = "s";
    private static final String REPLY_TO_ADDR_FIELD = "r";
    private static final String MSG_FORMAT_FIELD = "f";
    private static final String DISPLAY_MSG_HEADERS_FIELD = "d";
    private static final String AUTO_LINEBREAK_FIELD = "a";
    private static final String STD_TRASH_FIELD = "st";
    private static final String STD_SENT_FIELD = "ss";
    private static final String STD_DRAFTS_FIELD = "sd";
    private static final String STD_SPAM_FIELD = "sp";
    private static final String CONFIRMED_SPAM_FIELD = "cs";
    private static final String CONFIRMED_HAM_FIELD = "ch";
    private static final String UPLOAD_QUOTA_FIELD = "q";
    private static final String QUOTA_PER_FILE_FIELD = "qf";
    private static final String SIGNATURES_FIELD = "i";

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONObject writeJson(UserSettingMail value) throws Exception {
        JSONObject jsonObject = new JSONObject(20);
        jsonObject.put(CONTEXT_ID_FIELD, value.getCid());
        jsonObject.put(USER_ID_FIELD, value.getUserId());
        jsonObject.put(BITS_FIELD, value.getBitsValue());
        jsonObject.putOpt(SEND_ADDR_FIELD, value.getSendAddr());
        jsonObject.putOpt(REPLY_TO_ADDR_FIELD, value.getReplyToAddr());
        jsonObject.put(MSG_FORMAT_FIELD, value.getMsgFormat());
        String[] displayMsgHeaders = value.getDisplayMsgHeaders();
        if (null != displayMsgHeaders && 0 < displayMsgHeaders.length) {
            jsonObject.put(DISPLAY_MSG_HEADERS_FIELD, new JSONArray(Arrays.asList(displayMsgHeaders)));
        }
        jsonObject.put(AUTO_LINEBREAK_FIELD, value.getAutoLinebreak());
        jsonObject.putOpt(STD_TRASH_FIELD, value.getStdTrashName());
        jsonObject.putOpt(STD_SENT_FIELD, value.getStdSentName());
        jsonObject.putOpt(STD_DRAFTS_FIELD, value.getStdDraftsName());
        jsonObject.putOpt(STD_SPAM_FIELD, value.getStdSpamName());
        jsonObject.putOpt(CONFIRMED_SPAM_FIELD, value.getConfirmedSpam());
        jsonObject.putOpt(CONFIRMED_HAM_FIELD, value.getConfirmedHam());
        jsonObject.put(UPLOAD_QUOTA_FIELD, value.getUploadQuota());
        jsonObject.put(QUOTA_PER_FILE_FIELD, value.getUploadQuotaPerFile());
        jsonObject.put(CONFIRMED_SPAM_FIELD, value.getConfirmedSpam());
        Signature[] signatures = value.getSignatures();
        if (null != signatures && 0 < signatures.length) {
            JSONObject jsonSignatures = new JSONObject(signatures.length);
            for (Signature signature : signatures) {
                jsonObject.put(signature.getId(), signature.getSignature());
            }
            jsonObject.put(SIGNATURES_FIELD, jsonSignatures);
        }
        return jsonObject;
    }

    @Override
    protected UserSettingMail parseJson(JSONObject jObject) throws Exception {
        UserSettingMail userSettingMail = new UserSettingMail(jObject.getInt(USER_ID_FIELD), jObject.getInt(CONTEXT_ID_FIELD));
        userSettingMail.parseBits(jObject.getInt(BITS_FIELD));
        userSettingMail.setSendAddr(jObject.optString(SEND_ADDR_FIELD, null));
        userSettingMail.setReplyToAddr(jObject.optString(REPLY_TO_ADDR_FIELD, null));
        userSettingMail.setMsgFormat(jObject.getInt(MSG_FORMAT_FIELD));
        JSONArray jsonDisplayMsgHeaders = jObject.optJSONArray(DISPLAY_MSG_HEADERS_FIELD);
        if (null != jsonDisplayMsgHeaders) {
            String[] displayMsgHeaders = new String[jsonDisplayMsgHeaders.length()];
            for (int i = 0; i < jsonDisplayMsgHeaders.length(); i++) {
                displayMsgHeaders[i] = jsonDisplayMsgHeaders.getString(i);
            }
            userSettingMail.setDisplayMsgHeaders(displayMsgHeaders);
        }
        userSettingMail.setAutoLinebreak(jObject.getInt(AUTO_LINEBREAK_FIELD));
        userSettingMail.setStdTrashName(jObject.optString(STD_TRASH_FIELD, null));
        userSettingMail.setStdSentName(jObject.optString(STD_SENT_FIELD, null));
        userSettingMail.setStdDraftsName(jObject.optString(STD_DRAFTS_FIELD, null));
        userSettingMail.setStdSpamName(jObject.optString(STD_SPAM_FIELD, null));
        userSettingMail.setConfirmedSpam(jObject.optString(CONFIRMED_SPAM_FIELD, null));
        userSettingMail.setConfirmedHam(jObject.optString(CONFIRMED_HAM_FIELD, null));
        JSONObject jsonSignatures = jObject.optJSONObject(SIGNATURES_FIELD);
        if (null != jsonSignatures) {
            Signature[] signatures = new Signature[jsonSignatures.length()];
            int i = 0;
            for (Entry<String, Object> entry : jsonSignatures.entrySet()) {
                signatures[i++] = new Signature(entry.getKey(), (String) entry.getValue());
            }
            userSettingMail.setSignatures(signatures);
        }
        return userSettingMail;
    }

}
