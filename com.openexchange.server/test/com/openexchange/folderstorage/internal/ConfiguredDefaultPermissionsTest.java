package com.openexchange.folderstorage.internal;

import static com.openexchange.java.Autoboxing.B;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.config.cascade.ComposedConfigProperty;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.ImmutablePermission;
import com.openexchange.folderstorage.Permission;
import com.openexchange.server.services.ServerServiceRegistry;

public class ConfiguredDefaultPermissionsTest {

    private static final String PARENTFOLDER_PERMISSIONS = "parent=group_2@2.4.0.0,admin_user_5@8.4.4.4";
    private static final String CUSTOM_FOLDER_PERMISSIONS = "15=admin_group_2@author";
    private static final String FAIL_PERMISSION_EXPRESSION_PREFIX = "folder=failPerm@author";
    private static final String FAIL_PERMISSION_EXPRESSION_NO_EQUAL = "folderfailPerm@author";
    private static final String FAIL_PERMISSION_EXPRESSION_NO_AT = "folder=admin_user_5author";
    private static final String FAIL_PERMISSION_EXPRESSION_NO_PERMISSIONS = "folder=admin_user_5@";
    private static final String FAIL_PERMISSION_EXPRESSION_ONLY_FOLDER = "folder=admin_user_5@1";
    private static final String FAIL_PERMISSION_EXPRESSION_ONLY_FOLDER_READ = "folder=admin_user_5@1.2";
    private static final String FAIL_PERMISSION_EXPRESSION_ONLY_FOLDER_READ_WRITE = "folder=admin_user_5@1.2.3";

    // ATTENTION, must be equal to tested class: ConfiguredDefaultPermissions member: 'PROP_DEFAULT_PERMISSIONS'
    private static final String PROP_DEFAULT_PERMISSIONS = "com.openexchange.folderstorage.defaultPermissions";

    private ConfiguredDefaultPermissions testedClass;

    @Mock
    private ConfigViewFactory configViewFactory;
    @Mock
    private ServerServiceRegistry serverServiceRegistry;
    @Mock
    private ConfigView configView;
    @Mock
    private ComposedConfigProperty<String> composedConfigProperty;
    private MockedStatic<ServerServiceRegistry> staticServerServiceRegistryMock;


    //ServerServiceRegistry.getInstance().getService

    @BeforeEach
    public void setUp() throws OXException {
        MockitoAnnotations.openMocks(this);
        testedClass = ConfiguredDefaultPermissions.getInstance();
        staticServerServiceRegistryMock = Mockito.mockStatic(ServerServiceRegistry.class);
        Mockito.when(ServerServiceRegistry.getInstance()).thenReturn(serverServiceRegistry);
        Mockito.when(serverServiceRegistry.getService(ConfigViewFactory.class)).thenReturn(configViewFactory);
        Mockito.when(configViewFactory.getView(1,1)).thenReturn(configView);
        Mockito.when(configView.property(PROP_DEFAULT_PERMISSIONS, String.class)).thenReturn(composedConfigProperty);
        Mockito.when(B(composedConfigProperty.isDefined())).thenReturn(B(true));
    }

    @AfterEach
    public void tearDown() {
        staticServerServiceRegistryMock.close();
    }

    @Test
    public void testGetConfiguredDefaultPermissionsFor_successMultiFolder() {
        try {
            Mockito.when(composedConfigProperty.get()).thenReturn(PARENTFOLDER_PERMISSIONS + "|" + CUSTOM_FOLDER_PERMISSIONS);
            Permission[] result = testedClass.getConfiguredDefaultPermissionsFor("15", 1, 1);
            assertTrue(result.length == 1);
            assertTrue(result[0].equals(getCustomPermissions()));
            result = testedClass.getConfiguredDefaultPermissionsFor("parent", 1, 1);
            assertTrue(result.length == 2);
            assertTrue(result[0].equals(getAdminPermissions()));
            assertTrue(result[1].equals(getGroupPermissions()));
        } catch (OXException e) {
            fail("Exception thrown");
        }
    }

    @Test
    public void testGetConfiguredDefaultPermissionsFor_failNoEqual() {
        testedClass.invalidateCache();
        try {
            Mockito.when(composedConfigProperty.get()).thenReturn(FAIL_PERMISSION_EXPRESSION_NO_EQUAL);
            testedClass.getConfiguredDefaultPermissionsFor("folder", 1, 1);
        } catch (OXException e) {
            assertTrue(e.getMessage().contains("Missing '=' character in expression:"));
            return;
        }
        fail("No Exception thrown");
    }

    @Test
    public void testGetConfiguredDefaultPermissionsFor_failNoPrefix() {
        testedClass.invalidateCache();
        try {
            Mockito.when(composedConfigProperty.get()).thenReturn(FAIL_PERMISSION_EXPRESSION_PREFIX);
            testedClass.getConfiguredDefaultPermissionsFor("folder", 1, 1);
        } catch (OXException e) {
            assertTrue(e.getMessage().contains("Permission expression is required to start with either \"admin_user_\", \"user_\", \"admin_group_\" or \"group_\" prefix:"));
            return;
        }
        fail("No Exception thrown");
    }

    @Test
    public void testGetConfiguredDefaultPermissionsFor_failNoAT() {
        testedClass.invalidateCache();
        try {
            Mockito.when(composedConfigProperty.get()).thenReturn(FAIL_PERMISSION_EXPRESSION_NO_AT);
            testedClass.getConfiguredDefaultPermissionsFor("folder", 1, 1);
        } catch (OXException e) {
            assertTrue(e.getMessage().contains("Missing '@' character in expression:"));
            return;
        }
        fail("No Exception thrown");
    }

    @Test
    public void testGetConfiguredDefaultPermissionsFor_failNoPermissions() {
        testedClass.invalidateCache();
        try {
            Mockito.when(composedConfigProperty.get()).thenReturn(FAIL_PERMISSION_EXPRESSION_NO_PERMISSIONS);
            testedClass.getConfiguredDefaultPermissionsFor("folder", 1, 1);
        } catch (OXException e) {
            assertTrue(e.getMessage().contains("Expected a '.' delimiter in rights expression:"));
            return;
        }
        fail("No Exception thrown");
    }

    @Test
    public void testGetConfiguredDefaultPermissionsFor_failOnlyFolder() {
        testedClass.invalidateCache();
        try {
            Mockito.when(composedConfigProperty.get()).thenReturn(FAIL_PERMISSION_EXPRESSION_ONLY_FOLDER);
            testedClass.getConfiguredDefaultPermissionsFor("folder", 1, 1);
        } catch (OXException e) {
            assertTrue(e.getMessage().contains("Expected a '.' delimiter in rights expression:"));
            return;
        }
        fail("No Exception thrown");
    }

    @Test
    public void testGetConfiguredDefaultPermissionsFor_failOnlyFolderRead() {
        testedClass.invalidateCache();
        try {
            Mockito.when(composedConfigProperty.get()).thenReturn(FAIL_PERMISSION_EXPRESSION_ONLY_FOLDER_READ);
            testedClass.getConfiguredDefaultPermissionsFor("folder", 1, 1);
        } catch (OXException e) {
            assertTrue(e.getMessage().contains("Expected a '.' delimiter in rights expression:"));
            return;
        }
        fail("No Exception thrown");
    }

    @Test
    public void testGetConfiguredDefaultPermissionsFor_failOnlyFolderReadWrite() {
        testedClass.invalidateCache();
        try {
            Mockito.when(composedConfigProperty.get()).thenReturn(FAIL_PERMISSION_EXPRESSION_ONLY_FOLDER_READ_WRITE);
            testedClass.getConfiguredDefaultPermissionsFor("folder", 1, 1);
        } catch (OXException e) {
            assertTrue(e.getMessage().contains("Expected a '.' delimiter in rights expression:"));
            return;
        }
        fail("No Exception thrown");
    }


    private Object getAdminPermissions() {
        ImmutablePermission.Builder permissionBuilder = ImmutablePermission.builder();
        permissionBuilder.setAdmin(false).setGroup(true).setDeletePermission(0).setEntity(2).setFolderPermission(2).setReadPermission(4).setSystem(0).setWritePermission(0);
        return permissionBuilder.build();
    }

    private Object getGroupPermissions() {
        ImmutablePermission.Builder permissionBuilder = ImmutablePermission.builder();
        permissionBuilder.setAdmin(true).setGroup(false).setDeletePermission(4).setEntity(5).setFolderPermission(8).setReadPermission(4).setSystem(0).setWritePermission(4);
        return permissionBuilder.build();
    }

    private Object getCustomPermissions() {
        ImmutablePermission.Builder permissionBuilder = ImmutablePermission.builder();
        permissionBuilder.setAdmin(true).setGroup(true).setDeletePermission(4).setEntity(2).setFolderPermission(8).setReadPermission(4).setSystem(0).setWritePermission(4);
        return permissionBuilder.build();
    }
}
