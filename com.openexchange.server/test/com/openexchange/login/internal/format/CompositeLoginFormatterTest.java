/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.login.internal.format;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import org.junit.jupiter.api.Test;


/**
 * {@link CompositeLoginFormatterTest}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class CompositeLoginFormatterTest {

    /**
     * Initializes a new {@link CompositeLoginFormatterTest}.
     */
    public CompositeLoginFormatterTest() {
        super();
    }

    @Test
    public void testLoginFormat() {
        final CompositeLoginFormatter cp = new CompositeLoginFormatter("$u - $c - $s - $agent $client end", null);
        List<LoginFormatter> loginFormatters = cp.getLoginFormatters();

        assertEquals(10, loginFormatters.size(), "Unexpected size");

        assertTrue(TokenFormatter.USER.equals(loginFormatters.get(0)), "Unexpected formatter");
        assertTrue(TokenFormatter.CONTEXT.equals(loginFormatters.get(2)), "Unexpected formatter");
        assertTrue(TokenFormatter.SESSION.equals(loginFormatters.get(4)), "Unexpected formatter");
        assertTrue(TokenFormatter.AGENT.equals(loginFormatters.get(6)), "Unexpected formatter");
        assertTrue(TokenFormatter.CLIENT.equals(loginFormatters.get(8)), "Unexpected formatter");

        assertEquals(" - ", loginFormatters.get(1).toString(), "Unexpected formatter");
        assertEquals(" - ", loginFormatters.get(3).toString(), "Unexpected formatter");
        assertEquals(" - ", loginFormatters.get(5).toString(), "Unexpected formatter");
        assertEquals(" ", loginFormatters.get(7).toString(), "Unexpected formatter");
        assertEquals(" end", loginFormatters.get(9).toString(), "Unexpected formatter");
    }

}
