
package com.openexchange.server.services;

import static com.openexchange.java.Autoboxing.B;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import java.net.URI;
import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.configuration.ServerConfig;
import com.openexchange.configuration.ServerConfig.Property;
import com.openexchange.exception.OXException;
import com.openexchange.filestore.FileStorages;
import com.openexchange.filestore.Info;
import com.openexchange.filestore.QuotaFileStorageService;
import com.openexchange.groupware.attach.AttachmentConfig;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.contexts.impl.ContextImpl;
import com.openexchange.groupware.filestore.FilestoreStorage;
import com.openexchange.groupware.infostore.InfostoreConfig;
import com.openexchange.groupware.userconfiguration.UserPermissionBits;
import com.openexchange.jslob.JSlob;
import com.openexchange.mail.usersetting.UserSettingMail;
import com.openexchange.mail.usersetting.UserSettingMailStorage;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tools.session.ServerSessionAdapter;

/**
 * Unit tests for {@link SharedInfostoreJSlobTest}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 7.4.2
 */
public class SharedInfostoreJSlobTest {

    @InjectMocks
    private SharedInfostoreJSlob sharedInfostoreJSlob;

    @Mock
    private ServerSession session;

    //    @Mock
    //    private QuotaFileStorage quotaFileStorage;
    @Mock
    private com.openexchange.filestore.QuotaFileStorage quotaFileStorage;

    @Mock
    private UserPermissionBits permissionBits;

    private final Context context = new ContextImpl(999999);

    private final int maxBodySize = 11111;

    private final Long infostoreMaxUploadSize = L(22222L);

    private final Long attachmentMaxUploadSize = L(33333L);

    private final Long quotaUsage = L(44444L);

    private final Long maxQuota = L(55555L);

    private MockedStatic<ServerSessionAdapter> serverSessionAdapterMock;
    private MockedStatic<ServerConfig> serverConfigMock;
    private MockedStatic<InfostoreConfig> infostoreConfigMock;
    private MockedStatic<AttachmentConfig> attachmentConfigMock;
    private MockedStatic<FilestoreStorage> filestoreStorageMock;
    private MockedStatic<FileStorages> fileStoragesMock;
    private MockedStatic<UserSettingMailStorage> userSettingMailStorageMock;


    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);

        // Static mocks initialization
        serverSessionAdapterMock = Mockito.mockStatic(ServerSessionAdapter.class);
        serverConfigMock = Mockito.mockStatic(ServerConfig.class);
        infostoreConfigMock = Mockito.mockStatic(InfostoreConfig.class);
        attachmentConfigMock = Mockito.mockStatic(AttachmentConfig.class);
        filestoreStorageMock = Mockito.mockStatic(FilestoreStorage.class);
        fileStoragesMock = Mockito.mockStatic(FileStorages.class);
        userSettingMailStorageMock = Mockito.mockStatic(UserSettingMailStorage.class);

        // Mocking behavior
        Mockito.when(ServerSessionAdapter.valueOf((com.openexchange.session.Session) ArgumentMatchers.any())).thenReturn(session);

        Mockito.when(session.getContext()).thenReturn(context);
        Mockito.when(session.getUserPermissionBits()).thenReturn(permissionBits);

        Mockito.when(B(permissionBits.hasInfostore())).thenReturn(B(true));
        Mockito.when(B(permissionBits.hasWebMail())).thenReturn(B(true));

        Mockito.when(I(ServerConfig.getInt((Property) ArgumentMatchers.any()))).thenReturn(I(this.maxBodySize));

        Mockito.when(L(InfostoreConfig.getMaxUploadSize())).thenReturn(infostoreMaxUploadSize);

        Mockito.when(L(AttachmentConfig.getMaxUploadSize())).thenReturn(attachmentMaxUploadSize);

        Mockito.when(FilestoreStorage.createURI(ArgumentMatchers.eq(context))).thenReturn(new URI(""));

        QuotaFileStorageService qfsService = Mockito.mock(QuotaFileStorageService.class);
        Mockito.when(qfsService.getQuotaFileStorage(ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.any(Info.class))).thenReturn(quotaFileStorage);

        Mockito.when(FileStorages.getQuotaFileStorageService()).thenReturn(qfsService);

        Mockito.when(L(quotaFileStorage.getQuota())).thenReturn(maxQuota);
        Mockito.when(L(quotaFileStorage.getUsage())).thenReturn(quotaUsage);

        UserSettingMailStorage userSettingMailStorage = Mockito.mock(UserSettingMailStorage.class);
        Mockito.when(UserSettingMailStorage.getInstance()).thenReturn(userSettingMailStorage);

        UserSettingMail userSettingMail = Mockito.mock(UserSettingMail.class);
        Mockito.when(userSettingMailStorage.getUserSettingMail(ArgumentMatchers.anyInt(), ArgumentMatchers.eq(context)))
               .thenReturn(userSettingMail);
    }

    @AfterEach
    public void tearDown() {
        // Closing static mocks
        serverSessionAdapterMock.close();
        serverConfigMock.close();
        infostoreConfigMock.close();
        attachmentConfigMock.close();
        filestoreStorageMock.close();
        fileStoragesMock.close();
        userSettingMailStorageMock.close();
    }

    @Test
    public void testGetJSlob_fine_maxBodySizeSet() throws OXException, JSONException {
        JSlob jSlob = sharedInfostoreJSlob.getJSlob(session);

        Assertions.assertEquals(I(this.maxBodySize), jSlob.getJsonObject().get("maxBodySize"));
    }

    @Test
    public void testGetJSlob_fine_infostoreMaxUploadSizeSet() throws OXException, JSONException {
        JSlob jSlob = sharedInfostoreJSlob.getJSlob(session);

        Assertions.assertEquals(this.infostoreMaxUploadSize, jSlob.getJsonObject().get("infostoreMaxUploadSize"));
    }

    @Test
    public void testGetJSlob_fine_attachmentMaxUploadSizeSet() throws OXException, JSONException {
        JSlob jSlob = sharedInfostoreJSlob.getJSlob(session);

        Assertions.assertEquals(this.attachmentMaxUploadSize, jSlob.getJsonObject().get("attachmentMaxUploadSize"));
    }

    @Test
    public void testGetJSlob_fine_infostoreQuotaSet() throws OXException, JSONException {
        JSlob jSlob = sharedInfostoreJSlob.getJSlob(session);

        Assertions.assertEquals(this.maxQuota, jSlob.getJsonObject().get("infostoreQuota"));
    }

    @Test
    public void testGetJSlob_fine_infostoreUsageSet() throws OXException, JSONException {
        JSlob jSlob = sharedInfostoreJSlob.getJSlob(session);

        Assertions.assertEquals(this.quotaUsage, jSlob.getJsonObject().get("infostoreUsage"));
    }

    @Test
    public void testGetJSlob_fine_attachmentQuotaSet() throws OXException, JSONException {
        JSlob jSlob = sharedInfostoreJSlob.getJSlob(session);

        Assertions.assertEquals(L(-1L), jSlob.getJsonObject().get("attachmentQuota"));
    }

    @Test
    public void testGetJSlob_fine_attachmentQuotaPerFileSet() throws OXException, JSONException {
        JSlob jSlob = sharedInfostoreJSlob.getJSlob(session);

        Assertions.assertEquals(L(-1L), jSlob.getJsonObject().get("attachmentQuotaPerFile"));
    }
}
