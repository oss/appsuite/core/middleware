/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.writer;

import static com.openexchange.java.Autoboxing.I;
import java.io.StringWriter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.openexchange.ajax.fields.TaskFields;
import com.openexchange.groupware.tasks.Task;
import com.openexchange.java.util.TimeZones;

/**
 * {@link TaskWriterTest}
 *
 * @author <a href="mailto:marcus.klein@open-xchange.com">Marcus Klein</a>
 * @since 7.8.0
 */
@SuppressWarnings("static-method")
public class TaskWriterTest {

    public TaskWriterTest() {
        super();
    }

    /**
     * Tests if a priority value is written correctly.
     * @throws JSONException
     */
     @Test
     public void testWriteTaskPriority() throws JSONException {
        Task task = new Task();
        task.setPriority(I(0));
        JSONObject json = new JSONObject();
        new TaskWriter(TimeZones.UTC).writeTask(task, json);
        Assertions.assertTrue(json.has(TaskFields.PRIORITY), "Task priority was not written.");
        StringWriter sw = new StringWriter();
        json.write(sw);
        Assertions.assertTrue(sw.toString().contains('\"' + TaskFields.PRIORITY + "\":0"), "Task priority was not written as expected.");
    }

    /**
     * Tests if a null priority is not written.
     * @throws JSONException
     */
     @Test
     public void testWriteTaskPriorityNull() throws JSONException {
        Task task = new Task();
        task.setPriority(null);
        JSONObject json = new JSONObject();
        new TaskWriter(TimeZones.UTC).writeTask(task, json);
        StringWriter sw = new StringWriter();
        json.write(sw);
        Assertions.assertTrue(!sw.toString().contains(TaskFields.PRIORITY), "Task priority should not be written.");
        Assertions.assertFalse(json.has(TaskFields.PRIORITY), "Task priority should not be written.");
    }

     @Test
     public void testWriteTaskArrayPriority() throws JSONException {
        Task task = new Task();
        task.setPriority(I(0));
        JSONArray tmp = new JSONArray();
        new TaskWriter(TimeZones.UTC).writeArray(task, new int[] { Task.PRIORITY }, tmp);
        JSONArray json = tmp.getJSONArray(0);
        Assertions.assertEquals(1, json.length(), "Written array should contain exactly and only written priority.");
        StringWriter sw = new StringWriter();
        json.write(sw);
        Assertions.assertEquals("[0]", sw.toString(), "Written json array does not look like expected.");
    }

     @Test
     public void testWriteTaskArrayPriorityNull() throws JSONException {
        Task task = new Task();
        task.setPriority(null);
        JSONArray tmp = new JSONArray();
        new TaskWriter(TimeZones.UTC).writeArray(task, new int[] { Task.PRIORITY }, tmp);
        JSONArray json = tmp.getJSONArray(0);
        Assertions.assertEquals(1, json.length(), "Written array should contain exactly and only written priority.");
        StringWriter sw = new StringWriter();
        json.write(sw);
        Assertions.assertEquals("[null]", sw.toString(), "Written json array does not look like expected.");
    }
}
