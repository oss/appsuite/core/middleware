/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.ajax.requesthandler.converters.preview.cache;

import static com.openexchange.java.Autoboxing.I;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.startsWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DataTruncation;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import com.openexchange.ajax.requesthandler.cache.CachedResource;
import com.openexchange.config.ConfigurationService;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.server.ServiceLookup;

/**
 * {@link FileStoreResourceCacheImplTest}
 *
 * @author <a href="mailto:steffen.templin@open-xchange.com">Steffen Templin</a>
 */
public class FileStoreResourceCacheImplTest {

    @Test
    public void testFileIsDeletedOnDataTruncation() throws Exception {
        testFileIsDeletedOnRollback(DataTruncation.class);
    }

    @Test
    public void testFileIsDeletedOnSqlException() throws Exception {
        testFileIsDeletedOnRollback(SQLException.class);
    }

    /**
     * Verify that a created file is deleted if the subsequent db transaction throws a given exception
     */
    private void testFileIsDeletedOnRollback(Class<? extends Exception> exceptionClass) throws Exception {
        PreparedStatement statement = mock(PreparedStatement.class);
        when(I(statement.executeUpdate())).thenThrow(exceptionClass);
        when(statement.executeQuery()).thenReturn(mock(ResultSet.class));
        Connection connection = mock(Connection.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(connection.createStatement()).thenReturn(mock(Statement.class));
        DatabaseService databaseService = mock(DatabaseService.class);
        when(databaseService.getWritable(anyInt())).thenReturn(connection);
        ConfigurationService configMock = mock(ConfigurationService.class);
        when(configMock.getProperty(anyString(), anyString())).thenReturn("-1");
        ServiceLookup serviceLookupMock = mock(ServiceLookup.class);
        when(serviceLookupMock.getService(DatabaseService.class)).thenReturn(databaseService);
        when(serviceLookupMock.getService(ConfigurationService.class)).thenReturn(configMock);
        FileStoreResourceCacheImpl cache = spy(new FileStoreResourceCacheImpl(serviceLookupMock));
        try (MockedStatic<FileStoreResourceCacheImpl> mockedStatic = Mockito.mockStatic(FileStoreResourceCacheImpl.class)) {
            // Define the behavior of the static method

            com.openexchange.filestore.FileStorage fileStorage = mock(com.openexchange.filestore.FileStorage.class);
            String fileId = "12345";
            when(fileStorage.saveNewFile(any(InputStream.class))).thenReturn(fileId);
            mockedStatic.when(() -> FileStoreResourceCacheImpl.getFileStorage(anyInt(), anyBoolean())).thenReturn(fileStorage);
            boolean exceptionThrown = false;
            try {
                byte[] bytes = new byte[100];
                CachedResource resource = new CachedResource(bytes, "some_image.jpg", "image/jpeg", 100);
                cache.save("resource-id", resource, 1, 1);
            } catch (OXException e) {
                exceptionThrown = true;
                Assertions.assertTrue(exceptionClass.isInstance(e.getCause()));
            }
            Assertions.assertTrue(exceptionThrown);
            Mockito.verify(databaseService, Mockito.times(1)).getWritable(anyInt());
            InOrder autoCommitOrder = Mockito.inOrder(connection);
            autoCommitOrder.verify(connection).setAutoCommit(false);
            autoCommitOrder.verify(connection).setAutoCommit(true);
            Mockito.verify(connection).prepareStatement(startsWith("INSERT INTO"));
            Mockito.verify(statement).executeUpdate();
            Mockito.verify(connection).rollback();
            Mockito.verify(fileStorage).deleteFile(fileId);
        }
    }
}
