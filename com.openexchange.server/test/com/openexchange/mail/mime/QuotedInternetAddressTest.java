/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.mime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.junit.jupiter.api.Test;
import com.openexchange.mail.mime.utils.MimeMessageUtility;

/**
 * {@link QuotedInternetAddressTest}
 *
 * @author <a href="mailto:lars.hoogestraat@open-xchange.com">Lars Hoogestraat</a>
 * @since v7.6.1
 */
public class QuotedInternetAddressTest {

    /**
     * Initializes a new {@link QuotedInternetAddressTest}.
     */
    public QuotedInternetAddressTest() {
        super();
    }

    @Test
    public void testBug56693_Keep_Fullwidth_Characters_In_Personal() throws Exception {
        InternetAddress addr = new QuotedInternetAddress("=?ISO-2022-JP?B?GyRCIzAjOSM2IzI7dkwzNkkbKEI=?= <info@example.co.jp>");
        assertEquals("\uff10\uff19\uff16\uff12\u4e8b\u52d9\u5c40", addr.getPersonal(), "Unexpected personal");
        assertEquals("info@example.co.jp", addr.getAddress(), "Unexpected address");

        addr = new QuotedInternetAddress("=?ISO-2022-JP?B?GyRCIXkheiVTJUMlMCVtITwlViF6IXk7dkwzNkkbKEI=?= <info@example.co.jp>");
        assertEquals("\u2606\u2605\u30d3\u30c3\u30b0\u30ed\u30fc\u30d6\u2605\u2606\u4e8b\u52d9\u5c40", addr.getPersonal(), "Unexpected personal");
        assertEquals("info@example.co.jp", addr.getAddress(), "Unexpected address");
    }

    @Test
    public void testBug56322() throws Exception {
        InternetAddress addr = new QuotedInternetAddress("=?utf-8?B?KExlcyBHZW50bGVtZW4gZHUgRMOpbcOpbmFnZW1lbnQp?= <enquete@les-gd.com>");
        assertEquals("(Les Gentlemen du D\u00e9m\u00e9nagement)", addr.getPersonal(), "Unexpected personal");
        assertEquals("enquete@les-gd.com", addr.getAddress(), "Unexpected address");

        addr = new QuotedInternetAddress("\"=?utf-8?B?KExlcyBHZW50bGVtZW4gZHUgRMOpbcOpbmFnZW1lbnQp?=\" <enquete@les-gd.com>");
        assertEquals("(Les Gentlemen du D\u00e9m\u00e9nagement)", addr.getPersonal(), "Unexpected personal");
        assertEquals("enquete@les-gd.com", addr.getAddress(), "Unexpected address");

        InternetAddress[] addresses = QuotedInternetAddress.parse("\"=?utf-8?B?KExlcyBHZW50bGVtZW4gZHUgRMOpbcOpbmFnZW1lbnQp?=\" <enquete@les-gd.com>", false);
        assertNotNull(addresses);
        assertTrue(addresses.length == 1, "Unexpected number of addresses");
        addr = addresses[0];
        assertEquals("(Les Gentlemen du D\u00e9m\u00e9nagement)", addr.getPersonal(), "Unexpected personal");
        assertEquals("enquete@les-gd.com", addr.getAddress(), "Unexpected address");
    }

    @Test
    public void testBug56407() {
        ensureNoWhitespaceOrControl("=?utf-8?b?dGVzdCIgPHBvdHVzQHdoaXRlaG91c2UuZ292Pg==?==?utf-8?Q?=00=0A?=\" <demo@mailsploit.com>");
        ensureNoWhitespaceOrControl("\"=?utf-8?b?cG90dXNAd2hpdGVob3VzZS5nb3Y=?=\" <demo@mailsploit.com>");
        ensureNoWhitespaceOrControl("\"=?utf-8?b?cG90dXNAd2hpdGVob3VzZS5nb3YiIDxwb3R1c0B3aGl0ZWhvdXNlLmdvdj4=?==?utf-8?Q?=00=0A?=\" <demo@mailsploit.com>");
        ensureNoWhitespaceOrControl("\"=?utf-8?b?cG90dXNAd2hpdGVob3VzZS5nb3YiIDx0ZXN0Pg==?==?utf-8?Q?=00=0A?=\" <demo@mailsploit.com>");
        ensureNoWhitespaceOrControl("\"=?utf-8?b?InRlc3QiIDxwb3R1c0B3aGl0ZWhvdXNlLmdvdj4=?==?utf-8?Q?=0A=00=00=00?=\" <demo@mailsploit.com>");
        ensureNoWhitespaceOrControl("\"=?utf-8?b?InBvdHVzQHdoaXRlaG91c2UuZ292IiA8dGVzdD4=?==?utf-8?Q?=0A=00=00=00?=\" <demo@mailsploit.com>");
    }

    private void ensureNoWhitespaceOrControl(String sAddress) {
        try {
            QuotedInternetAddress addr = new QuotedInternetAddress(sAddress);

            ensureNoWhitespaceOrControlInString(addr.toString());
            ensureNoWhitespaceOrControlInString(addr.toUnicodeString());
            ensureNoWhitespaceOrControlInString(addr.getAddress());
            ensureNoWhitespaceOrControlInString(addr.getPersonal());
        } catch (AddressException e) {
            fail(e.getMessage());
        }
    }

    private void ensureNoWhitespaceOrControlInString(String toTest) {
        assertTrue(toTest.indexOf('\0') < 0, "Contains null byte, but shouldn't: " + toTest);
        assertTrue(toTest.indexOf('\r') < 0, "Contains CR, but shouldn't: " + toTest);
        assertTrue(toTest.indexOf('\n') < 0, "Contains LF, but shouldn't: " + toTest);
    }

    @Test
    public void testBug52107() throws Exception {
        QuotedInternetAddress addr = new QuotedInternetAddress("(just a comment) \"Doe, Jane (JD)\" <doe.jane@domain.de>", true);
        assertEquals("Doe, Jane (JD)", addr.getPersonal(), "Unexpected personal");
        assertEquals("doe.jane@domain.de", addr.getAddress(), "Unexpected address");

        addr = new QuotedInternetAddress("(just a comment(with more) inside) \"Doe, Jane (JD)\" <doe.jane@domain.de>", true);
        assertEquals("Doe, Jane (JD)", addr.getPersonal(), "Unexpected personal");
        assertEquals("doe.jane@domain.de", addr.getAddress(), "Unexpected address");

        addr = new QuotedInternetAddress("(just a comment(with more) inside) \"Doe, Jane (JD)\" (in the mid) <doe.jane@domain.de> (last comment)", true);
        assertEquals("Doe, Jane (JD)", addr.getPersonal(), "Unexpected personal");
        assertEquals("doe.jane@domain.de", addr.getAddress(), "Unexpected address");
    }

    @Test
    public void testBug55360_2() {
        try {
            QuotedInternetAddress addr = new QuotedInternetAddress("=?utf-8?b?c2VydmljZUBwYXlwYWwuY29tKFBheVBhbClgYGA=?==?utf-8?Q?=0A=00?=@pwnsdx.pw", false);
            String personal = addr.getPersonal();
            assertNotNull(personal);
            assertTrue(personal.indexOf('\0') < 0, "Contains null byte, but shouldn't");
            assertTrue(personal.indexOf('\r') < 0, "Contains CR, but shouldn't");
            assertTrue(personal.indexOf('\n') < 0, "Contains LF, but shouldn't");
        } catch (AddressException e) {
            // All fine
            String reference = e.getRef();
            assertTrue(reference.indexOf('\0') < 0, "Contains null byte, but shouldn't");
            assertTrue(reference.indexOf('\r') < 0, "Contains CR, but shouldn't");
            assertTrue(reference.indexOf('\n') < 0, "Contains LF, but shouldn't");
        }
    }

    @Test
    public void testBug61107() throws Exception {
        QuotedInternetAddress addr = new QuotedInternetAddress("\"oxwebgppri Jane Doe\\\"\" <oxweb@domain.tld>");
        String personal = addr.getPersonal();
        assertNotNull(personal);
        assertEquals("oxwebgppri Jane Doe\"", personal);

        addr = new QuotedInternetAddress("oxweb@domain.tld", "\"oxwebgppri Jane Doe\"\"", "UTF-8");
        personal = addr.getPersonal();
        assertNotNull(personal);
        assertEquals("\"oxwebgppri Jane Doe\"\"", personal);
    }

    @SuppressWarnings("unused")
    @Test
    public void testBug55360() throws Exception {
        try {
            new QuotedInternetAddress("=?utf-8?Q?=42=45=47=49=4E=20=2F=20=20=2F=20=00=20=50=41=53=53=45=44=20=4E=55=4C=4C=20=42=59=54=45=20=2F=20=0D=0A=20=50=41=53=53=45=44=20=43=52=4C=46=20=2F=20=45=4E=44?=@companyemail.com", false);
            fail("Address should not be parseable");
        } catch (AddressException e) {
            // All fine
            String reference = e.getRef();
            assertTrue(reference.indexOf('\0') < 0, "Contains null byte, but shouldn't");
            assertTrue(reference.indexOf('\r') < 0, "Contains CR, but shouldn't");
            assertTrue(reference.indexOf('\n') < 0, "Contains LF, but shouldn't");
        }

        QuotedInternetAddress addr = new QuotedInternetAddress("=?utf-8?Q?=42=45=47=49=4E=20=2F=20=20=2F=20=00=20=50=41=53=53=45=44=20=4E=55=4C=4C=20=42=59=54=45=20=2F=20=0D=0A=20=50=41=53=53=45=44=20=43=52=4C=46=20=2F=20=45=4E=44?=<your@companyemail.com>", false);
        String personal = addr.getPersonal();
        assertNotNull(personal);
        assertTrue(personal.indexOf('\0') < 0, "Contains null byte, but shouldn't");
        assertTrue(personal.indexOf('\r') < 0, "Contains CR, but shouldn't");
        assertTrue(personal.indexOf('\n') < 0, "Contains LF, but shouldn't");
    }

    @Test
    public void testBug54879() throws Exception {
        QuotedInternetAddress addr = new QuotedInternetAddress("\"atest\"@example.com");
        assertEquals("\"atest\"@example.com", addr.toString(), "Address does not equals \"\"atest\"@example.com\"");

        addr = new QuotedInternetAddress("\"atest\"@example.com", false);
        assertEquals("\"atest\"@example.com", addr.toString(), "Address does not equals \"\"atest\"@example.com\"");
    }

    @Test
    public void testBug33552() throws Exception {
        String s = "Foo \u00e0 Bar <foo@bar.info>, =?UTF-8?Q?Foo_=C3=A0_Bar_=3Cfoo=40bar=2Einfo=3E?=, \"Foo, Bar\" <foo@bar.info>";
        InternetAddress[] parsed = QuotedInternetAddress.parse(s);

        assertEquals("Foo \u00e0 Bar", parsed[0].getPersonal(), "Display name does not equals \"Foo \u00e0 Bar\"");
        assertEquals("foo@bar.info", parsed[0].getAddress(), "Address does not equals \"foo@bar.info\"");

        assertEquals("Foo \u00e0 Bar", parsed[1].getPersonal(), "Display name does not equals \"Foo \u00e0 Bar\"");
        assertEquals("foo@bar.info", parsed[1].getAddress(), "Address does not equals \"foo@bar.info\"");

        assertEquals("Foo, Bar", parsed[2].getPersonal(), "Display name does not equals \"Foo, Bar\"");
        assertEquals("foo@bar.info", parsed[2].getAddress(), "Address does not equals \"foo@bar.info\"");
    }

    @Test
    public void testBug33305() throws Exception {
        QuotedInternetAddress a = new QuotedInternetAddress("\u00d6tt\u00f6 <stark@wie-die-wutz.de>");
        assertEquals("\u00d6tt\u00f6", a.getPersonal(), "Unexpected personal");
        assertEquals("=?UTF-8?B?w5Z0dMO2?= <stark@wie-die-wutz.de>", a.toString(), "Unexpected mail-safe form");
        assertEquals("\u00d6tt\u00f6 <stark@wie-die-wutz.de>", a.toUnicodeString(), "Unexpected unicode form");

        InternetAddress[] parsed = QuotedInternetAddress.parse("\u00d6tt\u00f6 <stark@wie-die-wutz.de>, Foo \u00e0 Bar <foo@bar.info>");
        assertEquals("\u00d6tt\u00f6", parsed[0].getPersonal(), "Unexpected personal");
        assertEquals("=?UTF-8?B?w5Z0dMO2?= <stark@wie-die-wutz.de>", parsed[0].toString(), "Unexpected mail-safe form");
        assertEquals("\u00d6tt\u00f6 <stark@wie-die-wutz.de>", parsed[0].toUnicodeString(), "Unexpected unicode form");

        assertEquals("Foo \u00e0 Bar", parsed[1].getPersonal(), "Unexpected personal");
        assertEquals("=?UTF-8?Q?Foo_=C3=A0_Bar?= <foo@bar.info>", parsed[1].toString(), "Unexpected mail-safe form");
        assertEquals("Foo \u00e0 Bar <foo@bar.info>", parsed[1].toUnicodeString(), "Unexpected unicode form");
    }

    @Test
    public void testBug34070() throws Exception {
        String s = "=?windows-1252?Q?Betz=2C_C=E4cilia?= <caecilia.betz@invalid.org>";
        QuotedInternetAddress addr = new QuotedInternetAddress(s);

        assertEquals("Betz, C\u00e4cilia", addr.getPersonal(), "Display name does not match \"Betz, C\u00e4cilia\"");
        assertEquals("caecilia.betz@invalid.org", addr.getAddress(), "Address does not match \"caecilia.betz@open-xchange.com\"");
    }

    @Test
    public void testBug34755() throws Exception {
        String s = "=?windows-1252?Q?Kr=F6ning=2C_User?= <user4@ox.microdoc.de>";
        QuotedInternetAddress addr = new QuotedInternetAddress(s);

        assertEquals("Kr\u00f6ning, User", addr.getPersonal(), "Display name does not match \"Kr\u00f6ning, User\"");
        assertEquals("user4@ox.microdoc.de", addr.getAddress(), "Address does not match \"user4@ox.microdoc.de\"");
    }

    @Test
    public void testBug36095() {
        String s = "=?UTF-8?Q?F=C3=B6oooo=2C_Bar?= <s.foeoooobar@foobar.org>";
        InternetAddress[] parsed = MimeMessageUtility.getAddressHeader(s);
        assertEquals(1, parsed.length, "Unexpected amount of addresses");

        assertEquals("F\u00f6oooo, Bar", parsed[0].getPersonal(), "Display name does not equals \"F\u00f6oooo, Bar\"");
        assertEquals("s.foeoooobar@foobar.org", parsed[0].getAddress(), "Address does not equals \"s.foeoooobar@foobar.org\"");
    }

    @Test
    public void testBug36866() throws Exception {
        String s = "=?iso-8859-1?Q?Mustermann=2C_J=F6rg?= <Joerg.Mustermann@musterfirma.org>";
        InternetAddress[] parsed = QuotedInternetAddress.parseHeader(s, true);
        assertEquals(1, parsed.length, "Unexpected amount of addresses");

        assertEquals("Mustermann, J\u00f6rg", parsed[0].getPersonal(), "Display name does not equals \"Mustermann, J\u00f6rg\"");
        assertEquals("Joerg.Mustermann@musterfirma.org", parsed[0].getAddress(), "Address does not equals \"Joerg.Mustermann@musterfirma.org\"");
    }

    @Test
    public void testBug38365() throws Exception {
        QuotedInternetAddress addr = new QuotedInternetAddress("\"Peter \\\" Lustig\" <bar@foo.org>");
        assertEquals("Peter \" Lustig", addr.getPersonal(), "Display name does not equals \"Peter \" Lustig\"");

        addr = new QuotedInternetAddress("bar@foo.org", "Peter Lustig \\");
        assertEquals("Peter Lustig \\", addr.getPersonal(), "Display name does not equals \"Peter Lustig \\\"");
    }

    @Test
    public void testBug43709() throws Exception {
        String addresses = "\"pere6@20101027.de\" <pere6@20101027.de>, =?iso-8859-1?Q?'Jochum=2C_Christel;_Sch=F6ndorf=2C_Werner'?= <boeser.recipient@example.com>, \"pere20@20101027.de\" <pere20@20101027.de>";
        InternetAddress[] addrs = QuotedInternetAddress.parseHeader(addresses, true);

        assertNotNull(addrs, "Unexpected parse result");
        assertEquals(3, addrs.length, "Unexpected number of addresses");

        // Check first address
        assertEquals("pere6@20101027.de", addrs[0].getPersonal(), "Unexpected personal");
        assertEquals("pere6@20101027.de", addrs[0].getAddress(), "Unexpected address");

        // Check second address
        assertEquals("Jochum, Christel; Sch\u00f6ndorf, Werner", addrs[1].getPersonal(), "Unexpected personal");
        assertEquals("boeser.recipient@example.com", addrs[1].getAddress(), "Unexpected address");
    }

    @Test
    public void testProperToString() {
        try {
            QuotedInternetAddress adr = new QuotedInternetAddress("bar@foo.org", "Doe, Jane", "UTF-8");

            assertEquals("\"Doe, Jane\" <bar@foo.org>", adr.toString(), "Unexpected toString() representation");
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testBug68346() {
        try {
            QuotedInternetAddress adr = new QuotedInternetAddress("mail@\u0938\u0902\u0935\u093e\u0926.net", false);

            assertEquals("mail@\u0938\u0902\u0935\u093e\u0926.net", adr.toUnicodeString(), "Unexpected toUnicodeString() representation");
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testMWB2420() {
        try {
            QuotedInternetAddress adr = new QuotedInternetAddress("fru <<2fra@alice.it>>", false);

            String personal = adr.getPersonal();
            assertEquals("fru", personal, "Unexpected personal: " + personal);

            String address = adr.getAddress();
            assertEquals("2fra@alice.it", address, "Unexpected address: " + address);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testMWB2504() {
        try {
            {
                InternetAddress[] addrs = QuotedInternetAddress.parse("Fux, Mara, 34 anni <info@invalid.it>", true);

                assertEquals(1, addrs.length, "Unexpected number of addresses: " + addrs.length);

                InternetAddress addr = addrs[0];
                String personal = addr.getPersonal();
                assertEquals("Fux, Mara, 34 anni", personal, "Unexpected personal: " + personal);

                String address = addr.getAddress();
                assertEquals("info@invalid.it", address, "Unexpected address: " + address);
            }
            {
                InternetAddress[] addrs = QuotedInternetAddress.parse("Fux, Mara, 34 anni <info@invalid.it>, foobar@example.com", true);

                assertEquals(2, addrs.length, "Unexpected number of addresses: " + addrs.length);

                InternetAddress addr = addrs[0];
                String personal = addr.getPersonal();
                assertEquals("Fux, Mara, 34 anni", personal, "Unexpected personal: " + personal);

                String address = addr.getAddress();
                assertEquals("info@invalid.it", address, "Unexpected address: " + address);

                addr = addrs[1];
                personal = addr.getPersonal();
                assertEquals(null, personal, "Unexpected personal: " + personal);

                address = addr.getAddress();
                assertEquals("foobar@example.com", address, "Unexpected address: " + address);
            }
            {
                InternetAddress[] addrs = QuotedInternetAddress.parse("Fux, Mara, 34 anni <info@invalid.it>, Bar, Foo <foobar@example.com>", true);

                assertEquals(2, addrs.length, "Unexpected number of addresses: " + addrs.length);

                InternetAddress addr = addrs[0];
                String personal = addr.getPersonal();
                assertEquals("Fux, Mara, 34 anni", personal, "Unexpected personal: " + personal);

                String address = addr.getAddress();
                assertEquals("info@invalid.it", address, "Unexpected address: " + address);

                addr = addrs[1];
                personal = addr.getPersonal();
                assertEquals("Bar, Foo", personal, "Unexpected personal: " + personal);

                address = addr.getAddress();
                assertEquals("foobar@example.com", address, "Unexpected address: " + address);
            }
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testCore122() {
        try {
            // Keep subsequent space characters through quoting
            QuotedInternetAddress adr = new QuotedInternetAddress();
            adr.setPersonal("Christoph Test  Test  Test  Test  Test   Kopp");
            adr.setAddress("ckopp@box.ox.io");

            assertEquals("\"Christoph Test  Test  Test  Test  Test   Kopp\" <ckopp@box.ox.io>", adr.toString(), "Unexpected toString() representation");
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

}
