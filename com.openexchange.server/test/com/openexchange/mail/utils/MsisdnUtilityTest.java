
package com.openexchange.mail.utils;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import javax.mail.internet.InternetAddress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.contact.ContactService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contact.ContactUtil;
import com.openexchange.groupware.container.Contact;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.groupware.ldap.UserStorage;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.session.Session;
import com.openexchange.user.User;

/**
 * Tests for class {@link MsisdnUtility}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 7.2.2
 */
public class MsisdnUtilityTest {

    @Mock
    private Session mockedSession;

    @Mock
    private User mockedUser;

    @Mock
    private ServerServiceRegistry serverServiceRegistry;

    @Mock
    private ContactService contactService;

    Set<String> numbers = new TreeSet<String>();

    Contact contact = new Contact();

    private MockedStatic<UserStorage> userStorageMock;
    private MockedStatic<ServerServiceRegistry> serverServiceRegistryMock;
    private MockedStatic<ContactUtil> contactUtilMock;

    @SuppressWarnings("deprecation")
    @BeforeEach
    public void setUp() throws OXException {
        MockitoAnnotations.openMocks(this);

        // Static mocks initialization
        userStorageMock = Mockito.mockStatic(UserStorage.class);
        serverServiceRegistryMock = Mockito.mockStatic(ServerServiceRegistry.class);
        contactUtilMock = Mockito.mockStatic(ContactUtil.class);

        // Mocking behavior
        Mockito.when(I(mockedSession.getUserId())).thenReturn(I(1));
        Mockito.when(I(mockedSession.getContextId())).thenReturn(I(1));
        Mockito.when(I(mockedUser.getContactId())).thenReturn(I(1));
        Mockito.when(serverServiceRegistry.getService(ContactService.class)).thenReturn(contactService);
        Mockito.when(contactService.getContact(mockedSession, Integer.toString(FolderObject.SYSTEM_LDAP_FOLDER_ID), Integer.toString(1)))
               .thenReturn(contact);
        Mockito.when(contactService.getUser(mockedSession, 1)).thenReturn(contact);
        Mockito.when(UserStorage.getStorageUser(1, 1)).thenReturn(mockedUser);
        Mockito.when(ServerServiceRegistry.getInstance()).thenReturn(serverServiceRegistry);
        Mockito.when(ContactUtil.gatherTelephoneNumbers(contact)).thenReturn(numbers);
    }

    @AfterEach
    public void tearDown() {
        userStorageMock.close();
        serverServiceRegistryMock.close();
        contactUtilMock.close();
    }

    @Test
    public final void testAddMsisdnAddress_noNumberFound_returnWithoutAddedNumber() {
        final Set<InternetAddress> validAddrs = new HashSet<InternetAddress>();

        MsisdnUtility.addMsisdnAddress(validAddrs, mockedSession);

        assertEquals(numbers.size(), validAddrs.size());
    }

    @Test
    public final void testAddMsisdnAddress_oneNumberFound_returnWithAddedNumber() {
        numbers.add("myNumber");

        final Set<InternetAddress> validAddrs = new HashSet<InternetAddress>();

        MsisdnUtility.addMsisdnAddress(validAddrs, mockedSession);

        assertEquals(numbers.size(), validAddrs.size());
    }

    @Test
    public final void testAddMsisdnAddress_fiveNumberFound_returnWithAddedNumber() {
        numbers.add("myNumber0");
        numbers.add("myNumber1");
        numbers.add("myNumber2");
        numbers.add("myNumber3");
        numbers.add("myNumber4");

        final Set<InternetAddress> validAddrs = new HashSet<InternetAddress>();

        MsisdnUtility.addMsisdnAddress(validAddrs, mockedSession);

        assertEquals(numbers.size(), validAddrs.size());
    }

    @Test
    public final void testAddMsisdnAddress_noContactFound_return() {
        Mockito.when(I(mockedUser.getContactId())).thenReturn(I(0));

        final Set<InternetAddress> validAddrs = new HashSet<InternetAddress>();

        MsisdnUtility.addMsisdnAddress(validAddrs, mockedSession);

        assertEquals(0, validAddrs.size());
    }

    @Test
    public final void testAddMsisdnAddress_contactServiceNull_return() {
        Mockito.when(serverServiceRegistry.getService(ContactService.class)).thenReturn(null);

        final Set<InternetAddress> validAddrs = new HashSet<InternetAddress>();

        MsisdnUtility.addMsisdnAddress(validAddrs, mockedSession);

        assertEquals(0, validAddrs.size());
    }

    @Test
    public final void testAddMsisdnAddress_contactNull_return() throws OXException {
        Mockito.when(
            contactService.getContact(mockedSession, Integer.toString(FolderObject.SYSTEM_LDAP_FOLDER_ID), Integer.toString(1))).thenReturn(
                null);

        final Set<InternetAddress> validAddrs = new HashSet<InternetAddress>();

        MsisdnUtility.addMsisdnAddress(validAddrs, mockedSession);

        assertEquals(0, validAddrs.size());
    }
}
