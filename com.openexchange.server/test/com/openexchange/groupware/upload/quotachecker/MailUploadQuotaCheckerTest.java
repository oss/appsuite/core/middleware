package com.openexchange.groupware.upload.quotachecker;

import static com.openexchange.java.Autoboxing.L;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.configuration.ServerConfig;
import com.openexchange.configuration.ServerConfig.Property;
import com.openexchange.exception.OXException;
import com.openexchange.mail.usersetting.UserSettingMail;


/**
 * Unit tests for {@link MailUploadQuotaCheckerTest}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 7.4.2
 */
public class MailUploadQuotaCheckerTest {

    /**
     * Class under test
     */
    private MailUploadQuotaChecker mailUploadQuotaChecker;

    @Mock
    private UserSettingMail userSettingMail;

    private final long quota = 10000000L;

    private final long quotaFromFile = 9999L;

    private final long unlimitedQuota = 0L;

    private final long unlimitedQuota2 = -1L;

    private MockedStatic<ServerConfig> staticServerConfigMock;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);

        staticServerConfigMock = Mockito.mockStatic(ServerConfig.class);
        Mockito.when(ServerConfig.getLong((Property) ArgumentMatchers.any())).thenReturn(L(quotaFromFile));
    }

    @AfterEach
    public void tearDown() {
        staticServerConfigMock.close();
    }

    @Test
     public void testMailUploadQuotaCheckerUserSettingMail_userSettingsNull_throwException() {
        try {
            this.mailUploadQuotaChecker = new MailUploadQuotaChecker(null);
        } catch (@SuppressWarnings("unused") IllegalArgumentException e) {
            Assertions.fail("No exception was expected");
        }
    }

     @Test
     public void testMailUploadQuotaCheckerUserSettingMail_bothZero_maxQuotaZero() {
        this.mailUploadQuotaChecker = new MailUploadQuotaChecker(userSettingMail);

        long quotaMax = this.mailUploadQuotaChecker.getQuotaMax();

        Assertions.assertEquals(this.unlimitedQuota2, quotaMax);
    }

     @Test
     public void testMailUploadQuotaCheckerUserSettingMail_bothZero_maxQuotaPerFileNegativ() {
        this.mailUploadQuotaChecker = new MailUploadQuotaChecker(userSettingMail);

        long fileQuotaMax = this.mailUploadQuotaChecker.getFileQuotaMax();

        Assertions.assertEquals(this.unlimitedQuota2, fileQuotaMax);
    }

     @Test
     public void testMailUploadQuotaCheckerUserSettingMail_uploadQuotaSetInDB_setUploadQuota() {
        Mockito.when(L(userSettingMail.getUploadQuota())).thenReturn(L(this.quota));

        this.mailUploadQuotaChecker = new MailUploadQuotaChecker(userSettingMail);

        long quotaMax = this.mailUploadQuotaChecker.getQuotaMax();

        Assertions.assertEquals(this.quota, quotaMax);
    }

     @Test
     public void testMailUploadQuotaCheckerUserSettingMail_uploadQuotaPerFileSetInDB_setUploadQuotaPerFile() {
        Mockito.when(L(userSettingMail.getUploadQuotaPerFile())).thenReturn(L(this.quota));

        this.mailUploadQuotaChecker = new MailUploadQuotaChecker(userSettingMail);

        long fileQuotaMax = this.mailUploadQuotaChecker.getFileQuotaMax();

        Assertions.assertEquals(this.quota, fileQuotaMax);
    }

     @Test
     public void testMailUploadQuotaCheckerUserSettingMail_uploadQuotaNegativ_setUploadQuotaFromServerProperties() {
        Mockito.when(L(userSettingMail.getUploadQuota())).thenReturn(L(-1L));

        this.mailUploadQuotaChecker = new MailUploadQuotaChecker(userSettingMail);

        long quotaMax = this.mailUploadQuotaChecker.getQuotaMax();

        Assertions.assertEquals(this.quotaFromFile, quotaMax);
        Assertions.assertEquals(this.unlimitedQuota2, this.mailUploadQuotaChecker.getFileQuotaMax());
    }

     @Test
     public void testMailUploadQuotaCheckerUserSettingMail_uploadQuotaNegativAndServerConfigException_setUploadQuotaToZero() throws OXException {
        Mockito.when(L(userSettingMail.getUploadQuota())).thenReturn(L(-1L));
        Mockito.when(ServerConfig.getLong((Property) ArgumentMatchers.any())).thenThrow(new OXException());

        this.mailUploadQuotaChecker = new MailUploadQuotaChecker(userSettingMail);

        long quotaMax = this.mailUploadQuotaChecker.getQuotaMax();

        Assertions.assertEquals(unlimitedQuota, quotaMax);
        Assertions.assertEquals(this.unlimitedQuota2, this.mailUploadQuotaChecker.getFileQuotaMax());
    }
}
