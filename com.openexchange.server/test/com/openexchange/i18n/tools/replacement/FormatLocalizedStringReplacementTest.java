/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.i18n.tools.replacement;

import java.util.Locale;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.i18n.I18nService;
import com.openexchange.i18n.I18nServiceRegistry;
import com.openexchange.i18n.tools.TemplateToken;
import com.openexchange.server.services.ServerServiceRegistry;

/**
 * Unit tests for {@link FormatLocalizedStringReplacement}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 7.6.0
 */
public class FormatLocalizedStringReplacementTest {

    private FormatLocalizedStringReplacement formatLocalizedStringReplacement;

    private final String format = "Priority: %1$s";

    private final String formatTranslated = "Priorit\\u00e4t: %1$s";

    private final String result = "Priorit\\u00e4t: Niedrig";

    private final String replacement = "Low";

    private final Locale locale = new Locale("de_DE");

    private final TemplateToken templateToken = TemplateToken.TASK_PRIORITY;

    @Mock
    private I18nService i18nService;

    @Mock
    private I18nServiceRegistry mockRegistry;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        formatLocalizedStringReplacement = new FormatLocalizedStringReplacement(templateToken, format, replacement);
        formatLocalizedStringReplacement.setChanged(false);
        formatLocalizedStringReplacement.setLocale(locale);

        Mockito.when(mockRegistry.getI18nService(locale)).thenReturn(i18nService);
        ServerServiceRegistry.getInstance().addService(I18nServiceRegistry.class, mockRegistry);

        Mockito.when(i18nService.getLocalized(format)).thenReturn(formatTranslated);
        Mockito.when(i18nService.getLocalized(replacement)).thenReturn("Niedrig");
    }

    @AfterEach
    public void tearDown() {
        ServerServiceRegistry.getInstance().removeService(I18nServiceRegistry.class);
    }

    @Test
    public void testGetReplacement_replacementObjectStateFine_returnTranslatedPrioritySentence() {
        String replacedString = formatLocalizedStringReplacement.getReplacement();
        Assertions.assertEquals(result, replacedString, "String not localized");
    }

}
