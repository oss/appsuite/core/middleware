/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.undo.json.osgi;

import java.util.HashMap;
import java.util.Map;
import com.openexchange.ajax.requesthandler.Dispatcher;
import com.openexchange.ajax.requesthandler.ResultConverter;
import com.openexchange.ajax.requesthandler.osgiservice.AJAXModuleActivator;
import com.openexchange.capabilities.CapabilityChecker;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.settings.PreferencesItemService;
import com.openexchange.jslob.ConfigTreeEquivalent;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tools.session.ServerSessionAdapter;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.json.UndoActionFactory;
import com.openexchange.undo.json.config.EnabledPreferencesItem;
import com.openexchange.undo.json.converter.UndoOperationsConverter;

/**
 * {@link UndoJsonActivator} - The activator for "undo" JSON bundle.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class UndoJsonActivator extends AJAXModuleActivator {

    /**
     * Initializes a new {@link UndoJsonActivator}.
     */
    public UndoJsonActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { UndoService.class, Dispatcher.class, CapabilityService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        // Register AJAX module
        registerModule(new UndoActionFactory(this), "undo");

        // Register result converter
        registerService(ResultConverter.class, new UndoOperationsConverter());

        // Register CapabilityChecker
        String sCapability = "undo";
        {
            Map<String, Object> properties = HashMap.newHashMap(1);
            properties.put(CapabilityChecker.PROPERTY_CAPABILITIES, sCapability);
            registerService(CapabilityChecker.class, new CapabilityChecker() {

                @Override
                public boolean isEnabled(final String capability, final Session ses) throws OXException {
                    if (sCapability.equals(capability)) {
                        final ServerSession session = ServerSessionAdapter.valueOf(ses);
                        if (session.isAnonymous()) {
                            return false;
                        }
                        return getService(UndoService.class).isEnabledFor(session);
                    }
                    return true;
                }
            }, properties);
        }

        // Declare capability
        getService(CapabilityService.class).declareCapability(sCapability);

        EnabledPreferencesItem enabled = new EnabledPreferencesItem(this);
        registerService(ConfigTreeEquivalent.class, enabled);
        registerService(PreferencesItemService.class, enabled);
    }

}
