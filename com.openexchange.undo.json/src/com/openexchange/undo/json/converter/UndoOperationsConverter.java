/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.undo.json.converter;

import java.util.Collection;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONCoercion;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.Converter;
import com.openexchange.ajax.requesthandler.ResultConverter;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.UndoOperation;
import com.openexchange.undo.UndoOperations;

/**
 * {@link UndoOperationsConverter}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class UndoOperationsConverter implements ResultConverter {

    /**
     * Initializes a new instance of {@link UndoOperationsConverter}.
     */
    public UndoOperationsConverter() {
        super();
    }

    @Override
    public String getInputFormat() {
        return "undoOperations";
    }

    @Override
    public String getOutputFormat() {
        return "json";
    }

    @Override
    public Quality getQuality() {
        return Quality.GOOD;
    }

    @Override
    public void convert(AJAXRequestData requestData, AJAXRequestResult result, ServerSession session, Converter converter) throws OXException {
        try {
            Object object = result.getResultObject();
            if (object instanceof UndoOperations undoOperations) {
                result.setResultObject(operations2Json(undoOperations), "json");
            } else {
                @SuppressWarnings("unchecked") final Collection<UndoOperations> collection = (Collection<UndoOperations>) object;
                result.setResultObject(collection.stream().map(UndoOperationsConverter::operations2Json).collect(CollectorUtils.toJsonArray(collection.size())), "json");
            }
        } catch (RuntimeException e) {
            Throwable cause = e.getCause();
            if (cause instanceof JSONException) {
                throw AjaxExceptionCodes.JSON_ERROR.create(cause, cause.getMessage());
            }
            throw AjaxExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    private static JSONObject operations2Json(UndoOperations undoOperations) {
        try {
            JSONObject jOperations = new JSONObject(3);
            jOperations.put("token", undoOperations.getUndoToken().getToken());
            jOperations.put("ttlMillis", undoOperations.getTimeToLiveMillis());
            List<UndoOperation> operations = undoOperations.getOperations();
            JSONArray jOps = switch (operations.size()) {
                case 0 -> JSONArray.EMPTY_ARRAY;
                case 1 -> new JSONArray(1).put(operation2Json(operations.get(0)));
                default -> operations.stream().map(op -> operation2Json(op)).collect(CollectorUtils.toJsonArray(undoOperations.size()));
            };
            jOperations.put("operations", jOps);
            return jOperations;
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    private static JSONObject operation2Json(UndoOperation operation) {
        try {
            JSONObject jOperation = new JSONObject(4);
            jOperation.putSafe("module", operation.getModule());
            jOperation.putSafe("action", operation.getAction());
            jOperation.putSafe("parameters", JSONCoercion.coerceToJSON(operation.getParameters()));
            jOperation.putSafe("data", operation.getData());
            return jOperation;
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

}
