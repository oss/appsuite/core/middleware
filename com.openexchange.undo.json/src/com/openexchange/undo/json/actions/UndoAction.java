/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.undo.json.actions;

import java.util.Optional;
import org.json.ImmutableJSONObject;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestDataBuilder;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.AJAXState;
import com.openexchange.ajax.requesthandler.Dispatcher;
import com.openexchange.ajax.requesthandler.Dispatchers;
import com.openexchange.ajax.requesthandler.UndoableAJAXActionService;
import com.openexchange.exception.OXException;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.DefaultUndoToken;
import com.openexchange.undo.UndoExceptionCodes;
import com.openexchange.undo.UndoOperation;
import com.openexchange.undo.UndoOperations;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.json.UndoRequest;

/**
 * {@link UndoAction} - The undo action.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class UndoAction extends AbstractUndoAction {

    private final JSONObject jSuccess;

    /**
     * Initializes a new {@link UndoAction}.
     *
     * @param services The service look-up
     */
    public UndoAction(final ServiceLookup services) {
        super(services);
        jSuccess = ImmutableJSONObject.immutableFor(new JSONObject(1).putSafe("success", Boolean.TRUE));
    }

    @Override
    protected AJAXRequestResult perform(final UndoRequest req) throws OXException, JSONException {
        AJAXRequestData originalRequest = req.getRequest();
        long undoTimeToLive = UndoableAJAXActionService.getUndoTimeToLiveFrom(originalRequest);
        if (undoTimeToLive > 0) {
            // No undo for undo...
            throw AjaxExceptionCodes.NOT_UNDOABLE.create(originalRequest.getAction(), originalRequest.getModule());
        }

        // Acquire service
        UndoService undoService = getService(UndoService.class);
        if (null == undoService) {
            throw ServiceExceptionCode.absentService(UndoService.class);
        }

        // Get dispatcher
        Dispatcher ox = getService(Dispatcher.class);
        if (null == ox) {
            throw ServiceExceptionCode.absentService(Dispatcher.class);
        }

        // Require undo token
        String token = req.checkParameter("token");

        // Retrieve undo operation(s) for given token
        ServerSession session = req.getSession();
        Optional<UndoOperations> optUndoOperations = undoService.removeUndoOperations(new DefaultUndoToken(token), session);
        if (optUndoOperations.isEmpty()) {
            throw UndoExceptionCodes.NO_SUCH_UNDO_OPERATION.create(token);
        }

        // Undo operation(s) obtained for passed undo token
        UndoOperations undoOperations = optUndoOperations.get();
        boolean error = true;
        try {
            // Execute undo operation
            AJAXState ajaxState = null;
            try {
                ajaxState = ox.begin();
                for (UndoOperation undoOperation : undoOperations) {
                    AJAXRequestResult requestResult = null;
                    Exception exc = null;
                    try {
                        AJAXRequestData requestData = AJAXRequestDataBuilder.request().session(session).module(undoOperation.getModule()).action(undoOperation.getAction()).params(undoOperation.getParameters()).data(undoOperation.getData(), "apiResponse").build(originalRequest);
                        requestResult = ox.perform(requestData, ajaxState, session);
                    } catch (OXException | RuntimeException x) {
                        exc = x;
                        throw x;
                    } finally {
                        Dispatchers.signalDone(requestResult, exc);
                    }
                }
            } finally {
                ox.end(ajaxState);
            }

            AJAXRequestResult requestResult = new AJAXRequestResult(jSuccess, "json");
            error = false;
            return requestResult;
        } finally {
            if (error) {
                undoService.restoreUndoOperations(new DefaultUndoToken(token), undoOperations, session);
            }
        }
    }

}
