/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.undo.json.actions;

import java.util.Optional;
import org.json.JSONException;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.server.ServiceLookup;
import com.openexchange.undo.DefaultUndoToken;
import com.openexchange.undo.UndoExceptionCodes;
import com.openexchange.undo.UndoOperations;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.json.UndoRequest;

/**
 * {@link GetAction} - The get action for undo module.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class GetAction extends AbstractUndoAction {

    /**
     * Initializes a new {@link GetAction}.
     *
     * @param services The service look-up
     */
    public GetAction(final ServiceLookup services) {
        super(services);
    }

    @Override
    protected AJAXRequestResult perform(final UndoRequest req) throws OXException, JSONException {
        // Acquire service
        UndoService undoService = getService(UndoService.class);
        if (null == undoService) {
            throw ServiceExceptionCode.absentService(UndoService.class);
        }

        // Require undo token
        String token = req.checkParameter("token");

        // Get undo operation(s) for given token
        Optional<UndoOperations> optUndoOperations = undoService.getUndoOperations(new DefaultUndoToken(token), req.getSession());
        if (optUndoOperations.isEmpty()) {
            throw UndoExceptionCodes.NO_SUCH_UNDO_OPERATION.create(token);
        }

        return new AJAXRequestResult(optUndoOperations.get(), "undoOperations");
    }

}
