/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.undo.json.actions;

import org.json.ImmutableJSONObject;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.DefaultUndoToken;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.json.UndoRequest;

/**
 * {@link DeleteAction} - The delete action for undo module.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class DeleteAction extends AbstractUndoAction {

    private final JSONObject jSuccess;
    private final JSONObject jFailure;

    /**
     * Initializes a new {@link DeleteAction}.
     *
     * @param services The service look-up
     */
    public DeleteAction(final ServiceLookup services) {
        super(services);
        jSuccess = ImmutableJSONObject.immutableFor(new JSONObject(1).putSafe("success", Boolean.TRUE));
        jFailure = ImmutableJSONObject.immutableFor(new JSONObject(1).putSafe("success", Boolean.FALSE));
    }

    @Override
    protected AJAXRequestResult perform(final UndoRequest req) throws OXException, JSONException {
        // Acquire service
        UndoService undoService = getService(UndoService.class);
        if (null == undoService) {
            throw ServiceExceptionCode.absentService(UndoService.class);
        }

        // Require undo token
        String token = req.checkParameter("token");

        // Drop undo operation(s) for given token
        boolean deleted = undoService.unregisterUndoOperations(new DefaultUndoToken(token), req.getSession());
        return deleted ? new AJAXRequestResult(jSuccess, "json") : new AJAXRequestResult(jFailure, "json");
    }

}
