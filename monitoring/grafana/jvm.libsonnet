local g = import 'g.libsonnet';

local queries = import './jvmQueries.libsonnet';
local panels = import './panels.libsonnet';
local variables = import './variables.libsonnet';
local row = g.panel.row;
local timeSeries = g.panel.timeSeries;
local fieldOverride = g.panel.timeSeries.fieldOverride;

local dashboardTitle = 'App Suite / Core-Middleware / JVM';

g.dashboard.new(dashboardTitle)
+ g.dashboard.withDescription(|||
  App Suite Core Middleware JVM Dashboard
|||)
+ g.dashboard.graphTooltip.withSharedCrosshair()
+ g.dashboard.withVariables([
  variables.datasource,
  variables.namespace,
  variables.app,
  variables.pod,
  variables.mem_pools_heap,
  variables.mem_pools_nonheap,
])
+ g.dashboard.withTags(['java', 'app-suite'])
+ g.dashboard.withEditable(false)
+ g.dashboard.withTimezone('browser')
+ g.dashboard.withRefresh('1m')
+ g.dashboard.time.withFrom('now-15m')
+ g.dashboard.withUid(g.util.string.slugify(dashboardTitle))
+ g.dashboard.withPanels(
  g.util.grid.makeGrid(
    [
      row.new('Overview')
      + row.withPanels([
        panels.stat.overviewUptime('Uptime', queries.overviewUptime),
        panels.stat.percent('Heap', queries.overviewHeapUsage),
        panels.stat.percent('Non-Heap', queries.overviewNonHeapUsage),
      ]),
    ], panelWidth=3, panelHeight=3, startY=1
  )
  + g.util.grid.makeGrid(
    [
      // ----------------------------------------------------------------------------
      row.new('Memory Pools (Heap)')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.bytes(
          '$%s' % [variables.mem_pools_heap.name],
          queries.memoryPools(
            'heap',
            variables.mem_pools_heap.name
          ),
          variables.mem_pools_heap.name
        ),
      ]),
      // ----------------------------------------------------------------------------
      row.new('Memory Pools (Non-Heap)')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.bytes(
          '$%s' % [variables.mem_pools_nonheap.name],
          queries.memoryPools(
            'nonheap',
            variables.mem_pools_nonheap.name
          ),
          variables.mem_pools_nonheap.name
        ),
      ]),
      // ----------------------------------------------------------------------------
      row.new('Garbage Collector')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.seconds('GC duration', queries.gcDuration),
        panels.timeSeries.ops('Collection', queries.gcDurationCount),
      ]),
      // ----------------------------------------------------------------------------
      row.new('Classloading')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.short('Classes loaded', queries.classesLoaded),
      ]),
      // ----------------------------------------------------------------------------
      row.new('Thread')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.base('Threads', queries.threadThreads)
        + timeSeries.standardOptions.withOverrides([
          fieldOverride.byName.new('Peak')
          + fieldOverride.byName.withProperty(
            'custom.fillOpacity',
            0
          ),
        ]),
        panels.timeSeries.base('Thread States', queries.threadThreadStates),
      ]),
    ], panelWidth=12, startY=5
  )
)
