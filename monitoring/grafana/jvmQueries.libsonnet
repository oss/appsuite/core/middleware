local g = import './g.libsonnet';
local prometheusQuery = g.query.prometheus;

local variables = import './variables.libsonnet';

{
  uptime:
    prometheusQuery.new(
      '$' + variables.datasource.name,
      |||
        process_uptime_seconds{
          pod="$%s"
        }
      ||| % [variables.pod.name]
    ),
  startTime: 0,

  threadThreads:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          jvm_threads_live_threads{
            pod=~"$%s"
          }
        ||| % [variables.pod.name]

      )
      + prometheusQuery.withLegendFormat(|||
        Live
      |||),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          jvm_threads_daemon_threads{
            pod=~"$%s"
          }
        ||| % [variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('Daemon'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          jvm_threads_peak_threads{
            pod=~"$%s"
          }
        ||| % [variables.pod.name]

      )
      + prometheusQuery.withLegendFormat('Peak'),
    ],

  threadThreadStates:
    prometheusQuery.new(
      '$' + variables.datasource.name,
      |||
        jvm_threads_states_threads{
          pod=~"$%s"
        }
      ||| % [variables.pod.name]

    )
    + prometheusQuery.withLegendFormat(|||
      {{state}}
    |||),

  overviewHeapUsage:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            jvm_memory_used_bytes{
              pod=~"$%s",
              area="heap"
            }
          ) * 100 / sum(
            jvm_memory_max_bytes{
              pod=~"$%s",
              area="heap"
            }
          )
        ||| % [variables.pod.name, variables.pod.name]
      ),
    ],

  overviewNonHeapUsage:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            jvm_memory_used_bytes{
              pod=~"$%s",
              area="nonheap"
            }
          ) * 100 / sum(
            jvm_memory_max_bytes{
              pod=~"$%s",
              area="nonheap"
            }
          )
        ||| % [variables.pod.name, variables.pod.name]
      ),
    ],

  overviewUptime:
    prometheusQuery.new(
      '$' + variables.datasource.name,
      |||
        process_uptime_seconds{
          pod=~"$%s"
        }
      ||| % [variables.pod.name]
    ),

  overviewThreads:
    prometheusQuery.new(
      '$' + variables.datasource.name,
      |||
        jvm_threads_live_threads{
          pod=~"$%s"
        }
      ||| % [variables.pod.name]
    ),

  classesLoaded:
    prometheusQuery.new(
      '$' + variables.datasource.name,
      |||
        jvm_classes_loaded_classes{
          pod=~"$%s"
        }
      ||| % [variables.pod.name]
    )
    + prometheusQuery.withLegendFormat('Loaded'),

  classesUnloaded:
    prometheusQuery.new(
      '$' + variables.datasource.name,
      |||
        jvm_classes_unloaded_classes_total{
          pod=~"$%s"
        }
      ||| % [variables.pod.name]
    )
    + prometheusQuery.withLegendFormat('Unloaded'),

  gcDuration:
    prometheusQuery.new(
      '$' + variables.datasource.name,
      |||
        rate(
          jvm_gc_collection_seconds_sum{
            pod=~"$%s"
          }[$__rate_interval]
        ) / rate(
          jvm_gc_collection_seconds_count{
            pod=~"$%s"
          }[$__rate_interval]
        )
      ||| % [variables.pod.name, variables.pod.name]
    )
    + prometheusQuery.withLegendFormat(|||
      {{gc}}
    |||),

  gcDurationCount:
    prometheusQuery.new(
      '$' + variables.datasource.name,
      |||
        rate(
          jvm_gc_collection_seconds_count{
            pod=~"$%s"
          }[$__rate_interval]
        )
      ||| % [variables.pod.name]
    )
    + prometheusQuery.withLegendFormat(|||
      {{gc}}
    |||),


  memoryPools(memoryArea, memoryPool):
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            jvm_memory_used_bytes{
              pod=~"$%s",
              area="%s",
              id=~"$%s"
            }
          )
        ||| % [variables.pod.name, memoryArea, memoryPool]
      )
      + prometheusQuery.withLegendFormat('Used'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            jvm_memory_committed_bytes{
              pod=~"$%s",
              area="%s",
              id=~"$%s"
            }
          )
        ||| % [variables.pod.name, memoryArea, memoryPool]
      )
      + prometheusQuery.withLegendFormat('Committed'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            jvm_memory_max_bytes{
              pod=~"$%s",
              area="%s",
              id=~"$%s"
            }
          )
        ||| % [variables.pod.name, memoryArea, memoryPool]
      )
      + prometheusQuery.withLegendFormat('Max'),
    ],
}
