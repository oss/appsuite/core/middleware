local g = import './g.libsonnet';
local prometheusQuery = g.query.prometheus;

local variables = import './variables.libsonnet';

{
  // --------------------------------------------------------------------------

  serverVersion:
    prometheusQuery.new(
      '$' + variables.datasource.name,
      |||
        topk(1, count(
          appsuite_version_info{pod=~"$%s", namespace="$%s"}
          ) by (server_version)
        )
      ||| % [variables.pod.name, variables.namespace.name]
    )
    + prometheusQuery.withLegendFormat(|||
      {{server_version}}
    |||)
    + prometheusQuery.withRange(false)
    + prometheusQuery.withInstant(true),
  overviewUptime:
    prometheusQuery.new(
      '$' + variables.datasource.name,
      'process_uptime_seconds{pod="$%s"}' % [variables.pod.name]
    )
    + prometheusQuery.withRange(false)
    + prometheusQuery.withInstant(true),

  overviewHealthStatus:
    prometheusQuery.new(
      '$' + variables.datasource.name,
      |||
        topk(1, count(
          appsuite_health_status{
            pod="$%s"
          } > 0)
        )
      ||| % [variables.pod.name]
    ),

  // --------------------------------------------------------------------------

  sessions:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_sessions_total{
            client="all",
            namespace="$%s",
            pod=~"$%s"
          }
        ||| % [variables.namespace.name, variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('Total'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_sessions_active_total{
            client="all",
            namespace="$%s",
            pod=~"$%s"
          }
        ||| % [variables.namespace.name, variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('Active'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_sessions_short_term_total{
            client="all",
            namespace="$%s",
            pod=~"$%s"
          }
        ||| % [variables.namespace.name, variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('Short Term'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_sessions_long_term_total{
            client="all",
            namespace="$%s",
            pod=~"$%s"
          }
        ||| % [variables.namespace.name, variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('Long Term'),
    ],

  // --------------------------------------------------------------------------

  threadPool:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_executor_active_threads{
            name="main",
            pod="$%s"
          }
        ||| % [variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('ActiveCount'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_executor_pool_size_threads{
            name="main",
            pod="$%s"
          }
        ||| % [variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('PoolSize'),
    ],

  threadPoolTasks:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_executor_queue_remaining_tasks{
            name="main",
            pod=~"$%s"
          }
        ||| % [variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('Remaining'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_executor_queued_tasks{
            name="main",
            pod=~"$%s"
          }
        ||| % [variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('Queued'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          rate(
            appsuite_executor_completed_tasks_total{
              name="main",
              pod=~"$%s"
            }[$__rate_interval])
        ||| % [variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('Completed'),
    ],

  // --------------------------------------------------------------------------

  cacheHits:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum by(module) (
            rate(
              appsuite_redis_cache_hits_total{
                pod=~"$pod"
              }
              [$__rate_interval]
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('{{module}}'),
    ],

  cacheMisses:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum by(module) (
            rate(
              appsuite_redis_cache_misses_total{
                pod=~"$pod"
              }
              [$__rate_interval]
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('{{module}}'),
    ],

  cachePuts:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum by(module) (
            rate(
              appsuite_redis_cache_puts_total{
                pod=~"$pod"
              }
              [$__rate_interval]
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('{{module}}'),
    ],

  cacheRemovals:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum by(module) (
            rate(
              appsuite_redis_cache_removals_total{
                pod=~"$pod"
              }
              [$__rate_interval]
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('{{module}}'),
    ],

  cacheHitMissRatio:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            rate(
              appsuite_redis_cache_hits_total{
                pod="$pod"
              }
              [$__rate_interval]
            )
          ) / (
            sum(
              rate(
                appsuite_redis_cache_hits_total{
                  pod="$pod"
                }
                [$__rate_interval]
              )
            ) + sum(
              rate(
                appsuite_redis_cache_misses_total{
                  pod="$pod"
                }
                [$__rate_interval]
              )
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('Hit Ratio'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            rate(
              appsuite_redis_cache_misses_total{
                pod="$pod"
                }
                [$__rate_interval]
              )
            )
          / (
            sum(
              rate(
                appsuite_redis_cache_hits_total{
                  pod="$pod"
                }
                [$__rate_interval]
              )
            )
            + sum(
              rate(
                appsuite_redis_cache_misses_total{
                  pod="$pod"
                }
                [$__rate_interval]
              )
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('Miss Ratio'),
    ],

  // --------------------------------------------------------------------------

  configDBReadConnections:
    self.configDBConnections('read'),

  configDBWriteConnections:
    self.configDBConnections('write'),

  userdbConnections:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_mysql_connections_total{
            class=~"userdb",
            pod=~"$pod",
            pool=~"$%s"
          }
        ||| % variables.dbPool.name
      )
      + prometheusQuery.withLegendFormat(
        |||
          Pooled (Type: {{type}}, Pool: {{pool}})
        |||
      ),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_mysql_connections_active{
            class=~"userdb",
            pod=~"$pod",
            pool=~"$%s"
          }
        ||| % variables.dbPool.name
      )
      + prometheusQuery.withLegendFormat(
        |||
          Active (Type: {{type}}, Pool: {{pool}})
        |||
      ),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_mysql_connections_idle{
            class=~"userdb",
            pod=~"$pod",
            pool=~"$%s"
          }
        ||| % variables.dbPool.name
      )
      + prometheusQuery.withLegendFormat(
        |||
          Idle (Type: {{type}}, Pool: {{pool}})
        |||
      ),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_mysql_connections_max{
            class=~"userdb",
            pod=~"$pod",
            pool=~"$%s"
          }
        ||| % variables.dbPool.name
      )
      + prometheusQuery.withLegendFormat(
        |||
          Max (Type: {{type}}, Pool: {{pool}})
        |||
      ),
    ],

  configDBConnections(type):
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_mysql_connections_total{class="configdb",pod=~"$pod",type="%s"}
        ||| % type
      )
      + prometheusQuery.withLegendFormat('Pooled'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_mysql_connections_active{class="configdb",pod="$pod",type="%s"}
        ||| % type
      )
      + prometheusQuery.withLegendFormat('Active'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_mysql_connections_idle{class="configdb",pod=~"$pod",type="%s"}
        ||| % type
      )
      + prometheusQuery.withLegendFormat('Idle'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(appsuite_mysql_connections_max{class="configdb",pod=~"$pod",type="%s"})
        ||| % type
      )
      + prometheusQuery.withLegendFormat('Max'),
    ],

  // --------------------------------------------------------------------------

  httpClientRequestsPerSecond:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum by (client, instance) (
            rate(
              appsuite_httpclient_requests_seconds_count{
                pod=~"$pod",
                client="$httpclient"
              }[$__rate_interval]
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('Total'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum by (client, instance) (
            rate(
              appsuite_httpclient_requests_seconds_count{
                pod=~"$pod",
                client="$httpclient",
                status!~"[45][0-9]{2}"
              }[$__rate_interval]
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('OK'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum by (client, instance) (
            rate(
              appsuite_httpclient_requests_seconds_count{
                pod=~"$pod",
                client="$httpclient",
                status=~"[45][0-9]{2}"
              }[$__rate_interval]
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('KO'),
    ],

  httpClientConnections:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_httpclient_connections_pending{
            pod="$pod",
            client=~"$httpclient"
          }
        |||
      )
      + prometheusQuery.withLegendFormat('Pending'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_httpclient_connections_available{
            pod="$pod",
            client=~"$httpclient"
          }
        |||
      )
      + prometheusQuery.withLegendFormat('Available'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_httpclient_connections_leased{
            pod="$pod",
            client=~"$httpclient"
          }
        |||
      )
      + prometheusQuery.withLegendFormat('Leased'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          appsuite_httpclient_connections_max{
            pod="$pod",
            client=~"$httpclient"
          }
        |||
      )
      + prometheusQuery.withLegendFormat('Max'),
    ],

  // --------------------------------------------------------------------------

  imapRequestRate:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            rate(
              appsuite_imap_commands_seconds_count{
                pod=~"$pod"
              }
              [$__rate_interval]
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('Total'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            rate(
              appsuite_imap_commands_seconds_count{
                status=~"OK",
                pod=~"$pod"
              }
              [$__rate_interval]
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('OK'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            rate(
              appsuite_imap_commands_seconds_count{
                status!~"OK",
                pod=~"$pod"
              }[$__rate_interval]
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('KO'),
    ],

  // --------------------------------------------------------------------------

  avgRedisCommandCompletion:
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum by (command) (
            rate(
              lettuce_command_completion_seconds_sum{
                pod=~"$pod",
                namespace=~"$namespace"
              }[$__rate_interval]
            )
          ) /
          sum by (command) (
            rate(
              lettuce_command_completion_seconds_count{
                pod=~"$pod",
                namespace=~"$namespace"
              }[$__rate_interval]
            )
          )
        |||
      )
      + prometheusQuery.withLegendFormat('{{command}}'),
    ],

  // --------------------------------------------------------------------------

  provisioningApiRequestRate:
    self.requestRate('appsuite_provisioning_api_duration_seconds_count'),

  provisioningPluginRequestRate:
    self.requestRate('appsuite_provisioning_plugin_duration_seconds_count'),

  provisioningStorageRequestRate:
    self.requestRate('appsuite_provisioning_storage_duration_seconds_count'),

  requestRate(metric):
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            rate(
              %s{
                pod=~"$%s"
              }
              [$__rate_interval]
            )
          )
        ||| % [metric, variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('Total'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            rate(
              %s{
                status=~"OK",
                pod=~"$%s"
              }
              [$__rate_interval]
            )
          )
        ||| % [metric, variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('OK'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            rate(
              %s{
                status!~"OK",
                pod=~"$%s"
              }[$__rate_interval]
            )
          )
        ||| % [metric, variables.pod.name]
      )
      + prometheusQuery.withLegendFormat('KO'),
    ],

  avgProvisioningDuration(metric, groupBy):
    prometheusQuery.new(
      '$' + variables.datasource.name,
      |||
        sum by (%s, method, operation) (
          rate(
            appsuite_provisioning_%s_duration_seconds_sum{
              pod=~"$%s",
              namespace=~"$namespace"
            }[$__rate_interval]
          )
        ) /
        sum by (%s, method, operation) (
          rate(
            appsuite_provisioning_%s_duration_seconds_count{
              pod=~"$%s",
              namespace=~"$namespace"
            }[$__rate_interval]
          )
        )
      ||| % [groupBy, metric, variables.pod.name, groupBy, metric, variables.pod.name]
    )
    + prometheusQuery.withLegendFormat(
      |||
        {{%s}} - {{method}} - {{operation}}
      ||| % groupBy
    ),

  avgProvisioningApiDuration:
    self.avgProvisioningDuration('api', 'api_class'),

  avgProvisioningPluginDuration:
    self.avgProvisioningDuration('plugin', 'plugin_name'),

  avgProvisioningStorageDuration:
    self.avgProvisioningDuration('storage', 'storage_class'),

  // --------------------------------------------------------------------------

  apiRequestsPerSecond(apiName):
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            rate(
              appsuite_%s_requests_seconds_count{
                pod=~"$pod"
              }[$__rate_interval]
            )
          )
        ||| % apiName
      )
      + prometheusQuery.withLegendFormat('Total'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            rate(
              appsuite_%s_requests_seconds_count{
                status=~"OK",
                pod=~"$pod"
              }[$__rate_interval]
            )
          )
        ||| % apiName
      )
      + prometheusQuery.withLegendFormat('OK'),
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum(
            rate(
              appsuite_%s_requests_seconds_count{
                status!~"OK",
                pod=~"$pod"
              }[$__rate_interval]
            )
          )
        ||| % apiName
      )
      + prometheusQuery.withLegendFormat('KO'),
    ],

  apiResponsePercentiles(apiName):
    self.histogramQuantile(apiName),

  apiRequestsPercentilesByRequest(apiName):
    self.quantileByRequest(apiName),

  quantileByRequest(apiName):
    prometheusQuery.new(
      '$' + variables.datasource.name,
      |||
        topk(
          5,
          histogram_quantile(
            0.99, sum by (le, module, action, instance) (
              rate(
                appsuite_%s_requests_seconds_bucket{
                  pod=~"$pod"
                }[${__range_s}s]
              )
            )
          )
        )
      ||| % apiName
    )
    + prometheusQuery.withLegendFormat(|||
      {{module}}/{{action}}
    |||),

  histogramQuantile(apiName):
    [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          histogram_quantile(
            0.%s,
            sum by (namespace, le, pod) (
              rate(
                appsuite_%s_requests_seconds_bucket{
                  namespace=~"$namespace",
                  pod=~"$pod"
                }[$__rate_interval]
              )
            )
          )
        ||| % [quantile, apiName]
      )
      + prometheusQuery.withIntervalFactor(2)
      + prometheusQuery.withLegendFormat(|||
        p%s
      ||| % quantile)
      for quantile in ['90', '95', '99']
    ] + [
      prometheusQuery.new(
        '$' + variables.datasource.name,
        |||
          sum by (namespace, le, pod) (
            rate(
              appsuite_%s_requests_seconds_sum{
                  namespace=~"$namespace",
                  pod=~"$pod"
              }[$__rate_interval])
          ) /
          sum by (namespace, le, pod) (
            rate(
              appsuite_%s_requests_seconds_count{
                namespace=~"$namespace",
                pod=~"$pod"
              }[$__rate_interval]
            )
          )
        ||| % [apiName, apiName]
      )
      + prometheusQuery.withIntervalFactor(2)
      + prometheusQuery.withLegendFormat('mean'),
    ],
}
