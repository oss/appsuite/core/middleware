local g = import 'g.libsonnet';

{
  stat: {
    local stat = g.panel.stat,
    local options = stat.options,
    local fieldOverride = stat.fieldOverride,

    base(title, targets):
      stat.new(title)
      + stat.queryOptions.withTargets(targets),

    serverVersion(title, targets):
      self.base(title, targets)
      + options.withTextMode('name')
      + options.withColorMode('none')
      + options.withGraphMode('none'),

    percent(title, targets):
      self.base(title, targets)
      + options.withTextMode('auto')
      + options.withColorMode('value')
      + options.withGraphMode('none')
      + stat.standardOptions.withUnit('percent')
      + stat.standardOptions.withDecimals(0)
      + stat.standardOptions.thresholds.withSteps([
        {
          value: null,
          color: 'rgba(50, 172, 45, 0.97)',
        },
        {
          value: 70,
          color: 'rgba(237, 129, 40, 0.89)',
        },
        {
          value: 90,
          color: 'rgba(245, 54, 54, 0.9)',
        },
      ])
      + stat.standardOptions.withMappings([
        {
          type: 'special',
          options: {
            match: 'null',
            result: {
              text: 'N/A',
            },
          },
        },
        {
          type: 'range',
          options: {
            from: -1e+32,
            to: 0,
            result: {
              text: 'N/A',
            },
          },
        },
      ]),

    overviewUptime(title, targets):
      self.base(title, targets)
      + options.withTextMode('auto')
      + options.withColorMode('value')
      + options.withGraphMode('none')
      + stat.standardOptions.withUnit('s')
      + stat.standardOptions.withDecimals(1)
      + stat.standardOptions.color.withMode('thresholds')
      + stat.standardOptions.withMappings([
        {
          type: 'special',
          options: {
            match: 'null',
            result: {
              text: 'N/A',
            },
          },
        },
      ])
      + stat.standardOptions.thresholds.withSteps([
        {
          value: null,
          color: 'rgba(245, 54, 54, 0.9)',
        },
        {
          value: 300,
          color: 'rgba(237, 129, 40, 0.89)',
        },
        {
          value: 3600,
          color: 'rgba(50, 172, 45, 0.97)',
        },
      ]),

    overviewHealthStatus(title, targets):
      self.base(title, targets)
      + options.withColorMode('background')
      + options.withGraphMode('none')
      + stat.queryOptions.withMaxDataPoints(100)
      + stat.standardOptions.withMappings([
        {
          type: 'value',
          options: {
            '0': {
              text: 'Unhealthy',
            },
            '1': {
              text: 'Healthy',
            },
          },
        },
      ])
      + stat.standardOptions.thresholds.withSteps([
        {
          value: null,
          color: 'rgba(245, 54, 54, 0.9)',
        },
        {
          value: 0.1,
          color: 'rgba(237, 129, 40, 0.89)',
        },
        {
          value: 1,
          color: 'rgba(50, 172, 45, 0.97)',
        },
      ]),
  },
  timeSeries: {
    local timeSeries = g.panel.timeSeries,
    local fieldOverride = g.panel.timeSeries.fieldOverride,
    local custom = timeSeries.fieldConfig.defaults.custom,
    local options = timeSeries.options,

    base(title, targets):
      timeSeries.new(title)
      + timeSeries.queryOptions.withTargets(targets)
      + timeSeries.queryOptions.withInterval('1m')
      + options.tooltip.withMode('multi')
      + options.tooltip.withSort('desc')
      + custom.withFillOpacity(10)
      + custom.withShowPoints('never'),

    httpClientConnections(title, targets):
      self.base(title, targets)
      + timeSeries.standardOptions.withMin(0),

    ops(title, targets):
      self.base(title, targets)
      + options.legend.withDisplayMode('list')
      + options.legend.withPlacement('bottom')
      + timeSeries.standardOptions.withUnit('ops')
      + timeSeries.standardOptions.withMin(0)
      + timeSeries.standardOptions.withDecimals(2),

    cps(title, targets):
      self.base(title, targets)
      + timeSeries.standardOptions.withUnit('cps')
      + timeSeries.standardOptions.withMin(0)
      + timeSeries.standardOptions.withOverrides([
        fieldOverride.byName.new('OK')
        + fieldOverride.byName.withProperty(
          'color',
          {
            mode: 'fixed',
            fixedColor: 'dark-green',
          }
        ),
        fieldOverride.byName.new('KO')
        + fieldOverride.byName.withProperty(
          'color',
          {
            mode: 'fixed',
            fixedColor: 'dark-red',
          }
        ),
        fieldOverride.byName.new('Total')
        + fieldOverride.byName.withProperty(
          'color',
          {
            mode: 'fixed',
            fixedColor: 'dark-yellow',
          }
        ),
      ]),

    percentUnit(title, targets):
      self.base(title, targets)
      + timeSeries.standardOptions.withMin(0)
      + timeSeries.standardOptions.withMax(1)
      + timeSeries.standardOptions.withUnit('percentunit')
      + timeSeries.standardOptions.withDecimals(0),

    topk(title, targets):
      self.seconds(title, targets)
      + custom.withAxisLabel('Response Time'),

    short(title, targets):
      self.base(title, targets)
      + options.legend.withDisplayMode('list')
      + options.legend.withPlacement('bottom')
      + timeSeries.standardOptions.withUnit('short')
      + timeSeries.standardOptions.withDecimals(0),

    seconds(title, targets):
      self.base(title, targets)
      + options.legend.withDisplayMode('list')
      + options.legend.withPlacement('bottom')
      + timeSeries.standardOptions.withUnit('s')
      + timeSeries.standardOptions.withMin(0),

    bytes(title, targets, repeat):
      self.base(title, targets)
      + options.legend.withDisplayMode('list')
      + options.legend.withPlacement('bottom')
      + timeSeries.standardOptions.withMin(0)
      + timeSeries.standardOptions.withUnit('decbytes')
      + timeSeries.panelOptions.withRepeat(repeat)
      + timeSeries.panelOptions.withRepeatDirection('h')
      + timeSeries.standardOptions.withOverrides([
        fieldOverride.byName.new('Max')
        + fieldOverride.byName.withProperty(
          'color',
          {
            mode: 'fixed',
            fixedColor: 'dark-red',
          }
        )
        + fieldOverride.byName.withProperty(
          'custom.fillOpacity',
          0
        ),
      ]),

    durationQuantile(title, targets):
      self.base(title, targets)
      + timeSeries.standardOptions.withUnit('s')
      + custom.withDrawStyle('bars')

      + timeSeries.standardOptions.withOverrides([
        fieldOverride.byRegexp.new('/mean/i')
        + fieldOverride.byRegexp.withProperty(
          'custom.fillOpacity',
          0
        )
        + fieldOverride.byRegexp.withProperty(
          'custom.lineStyle',
          {
            dash: [8, 10],
            fill: 'dash',
          }
        ),
      ]),
  },
}
