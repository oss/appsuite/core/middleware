# Grafana Dashboards as Code

* `Jsonnet` is a data templating language &rightarrow; [https://github.com/google/jsonnet](https://github.com/google/jsonnet)
* `Grafonnet` is a `Jsonnet` library for generating Grafana dashboards &rightarrow; [https://github.com/grafana/grafonnet](https://github.com/grafana/grafonnet)

## Getting Started

### Prerequisites

* Grafonnet requires Jsonnet, so follow the installation instructions for [Jsonnet](https://github.com/google/jsonnet#packages) and [Grafonnet](https://github.com/grafana/grafonnet#install).

## Usage

1. Go to your `middleware/core` repository.

2. Run a `golang` container:

   ```bash
   docker run -it --rm -v $(pwd):/project golang:alpine /bin/sh
   ```

3. In the container change directory:

   ```bash
   cd /project/monitoring/grafana
   ```

4. Install `git`, `jsonnet` and `jsonnet-bundler`:

   ```bash
   apk add git
   ```

   then

   ```bash
   go install github.com/google/go-jsonnet/cmd/jsonnet@latest
   ```

   and finally

   ```bash
   go install -a github.com/jsonnet-bundler/jsonnet-bundler/cmd/jb@latest
   ```

5. Use the `jsonnet-bundler` to install the library:

   ```bash
   jb install github.com/grafana/grafonnet/gen/grafonnet-latest@main
   ```

6. Generate the dashboards:

   ```bash
   jsonnet -J vendor middleware.libsonnet -o /project/helm/core-mw/dashboards/core-mw.json
   jsonnet -J vendor jvm.libsonnet -o /project/helm/core-mw/dashboards/jvm.json
   ```

   > Note: Rendered dashboards are saved to the helm chart directory.
