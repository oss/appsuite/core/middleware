local g = import './g.libsonnet';
local var = g.dashboard.variable;

{
  datasource:
    var.datasource.new('datasource', 'prometheus'),

  namespace:
    var.query.new('namespace')
    + var.query.queryTypes.withLabelValues(
      'namespace',
      'appsuite_version_info'
    )
    + var.query.withRefresh(2)
    + var.query.withSort(i=1, type='alphabetical')
    + var.query.withDatasourceFromVariable(self.datasource),

  app:
    var.query.new('app')
    + var.query.queryTypes.withLabelValues(
      'app',
      'up{namespace="$%s"}' % [
        self.namespace.name,
      ]
    )
    + var.query.withDatasourceFromVariable(self.datasource)
    + var.query.withRefresh(2)
    + var.query.withSort(i=1, type='alphabetical')
    + var.query.withRegex('/.*core-mw.*/')
    + var.query.generalOptions.showOnDashboard.withNothing(),

  pod:
    var.query.new('pod')
    + var.query.queryTypes.withLabelValues(
      'pod',
      'appsuite_version_info{namespace="$%s", pod=~".*$%s.*"}' % [
        self.namespace.name,
        self.app.name,
      ]
    )
    + var.query.withDatasourceFromVariable(self.datasource)
    + var.query.withRefresh(2)
    + var.query.withSort(i=1, type='alphabetical'),

  dbPool:
    var.query.new('dbpool')
    + var.query.withDatasourceFromVariable(self.datasource)
    + var.query.queryTypes.withLabelValues(
      'pool',
      'appsuite_mysql_connections_total{namespace="$%s"}' % [self.namespace.name]
    )
    + var.query.withRegex('([0-9]+)')
    + var.query.withSort(i=3, type='numerical')
    + var.query.selectionOptions.withIncludeAll(customAllValue='.*'),

  httpClient:
    var.query.new('httpclient')
    + var.query.withDatasourceFromVariable(self.datasource)
    + var.query.queryTypes.withLabelValues(
      'client',
      'appsuite_httpclient_requests_seconds_count'
    )
    + var.query.withSort(i=1, type='alphabetical'),

  circuitBreaker:
    var.query.new('circuitbreaker')
    + var.query.withDatasourceFromVariable(self.datasource)
    + var.query.queryTypes.withLabelValues(
      'name',
      'appsuite_circuitbreaker_state'
    )
    + var.query.withSort(i=1, type='alphabetical'),

  mem_pools_heap:
    var.query.new('mem_pools_heap')
    + var.query.withDatasourceFromVariable(self.datasource)
    + var.query.queryTypes.withLabelValues(
      'id',
      'jvm_memory_used_bytes{pod=~"$%s", area="heap"}' % [self.pod.name]
    )
    + var.query.selectionOptions.withIncludeAll()
    + var.query.generalOptions.showOnDashboard.withNothing(),

  mem_pools_nonheap:
    var.query.new('mem_pools_nonheap')
    + var.query.withDatasourceFromVariable(self.datasource)
    + var.query.queryTypes.withLabelValues(
      'id',
      'jvm_memory_used_bytes{pod=~"$%s", area="nonheap"}' % [self.pod.name]
    )
    + var.query.selectionOptions.withIncludeAll()
    + var.query.generalOptions.showOnDashboard.withNothing(),

  toArray: [
    self.datasource,
    self.namespace,
    self.app,
    self.pod,
    self.dbPool,
    self.httpClient,
  ],
}
