local g = import 'g.libsonnet';

local row = g.panel.row;

local panels = import './panels.libsonnet';
local variables = import './variables.libsonnet';
local queries = import './queries.libsonnet';

local link = g.dashboard.link;
local stat = g.panel.stat;
local timeSeries = g.panel.timeSeries;

local dashboardTitle = 'App Suite / Core-Middleware';

g.dashboard.new(dashboardTitle)
+ g.dashboard.withDescription(|||
  App Suite Core Middleware Dashboard
|||)
+ g.dashboard.graphTooltip.withSharedCrosshair()
+ g.dashboard.withVariables(variables.toArray)
+ g.dashboard.withLinks(
  [
    link.link.new(
      'Documentation',
      'https://documentation.open-xchange.com/latest/middleware/monitoring/02_micrometer_and_prometheus.html#visualization'
    ),
  ]
)
+ g.dashboard.withTags(['java', 'app-suite'])
+ g.dashboard.withEditable(false)
+ g.dashboard.withTimezone('browser')
+ g.dashboard.withRefresh('1m')
+ g.dashboard.time.withFrom('now-15m')
+ g.dashboard.withUid(g.util.string.slugify(dashboardTitle))
// ----------------------------------------------------------------------------
+ g.dashboard.withPanels(
  g.util.grid.makeGrid(
    [
      row.new('Overview')
      + row.withPanels([
        panels.stat.serverVersion('Server Version', queries.serverVersion),
        panels.stat.overviewUptime('Uptime', queries.overviewUptime),
        panels.stat.overviewHealthStatus('Health Status', queries.overviewHealthStatus),
      ]),
    ], panelWidth=6, panelHeight=4, startY=1
  )
  // ----------------------------------------------------------------------------
  + g.util.grid.makeGrid(
    [
      row.new('Sessions')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.short('Overview', queries.sessions)
        + timeSeries.fieldConfig.defaults.custom.withAxisLabel('Sessions'),
      ]),
    ], panelWidth=24, panelHeight=8, startY=6
  )
  // ----------------------------------------------------------------------------
  + g.util.grid.makeGrid(
    [
      row.new('Redis (Lettuce)')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.durationQuantile('Average Command Completion', queries.avgRedisCommandCompletion)
        + timeSeries.options.legend.withDisplayMode('table')
        + timeSeries.options.legend.withPlacement('right')
        + timeSeries.options.legend.withSortBy('Mean')
        + timeSeries.options.legend.withSortDesc(false)
        + timeSeries.options.legend.withCalcs([
          'min',
          'mean',
          'max',
        ]),
      ]),
    ], panelWidth=24, panelHeight=8, startY=15
  )
  // ----------------------------------------------------------------------------
  + g.util.grid.makeGrid(
    [
      // ----------------------------------------------------------------------------
      row.new('ThreadPool')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.base('ThreadPool', queries.threadPool)
        + timeSeries.fieldConfig.defaults.custom.withAxisLabel('Threads'),
        panels.timeSeries.base('ThreadPool Tasks', queries.threadPoolTasks)
        + timeSeries.fieldConfig.defaults.custom.withAxisLabel('Tasks/s'),
      ]),
      row.new('Cache')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.short('Cache Hits', queries.cacheHits),
        panels.timeSeries.short('Cache Misses', queries.cacheMisses),
        panels.timeSeries.percentUnit('Cache Hit/Miss Ratio', queries.cacheHitMissRatio),
        panels.timeSeries.short('Cache Puts', queries.cachePuts),
        panels.timeSeries.short('Cache Removals', queries.cacheRemovals),
      ]),
      // ----------------------------------------------------------------------------
      row.new('DB Pool')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.short('ConfigDB Read Connections', queries.configDBReadConnections),
        panels.timeSeries.short('ConfigDB Write Connections', queries.configDBWriteConnections),
        panels.timeSeries.short('UserDB Connections', queries.userdbConnections),
      ]),
      // ----------------------------------------------------------------------------
      row.new('HTTP Client ($%s)' % variables.httpClient.name)
      + row.withPanels([
        panels.timeSeries.cps('Requests (per-second)', queries.httpClientRequestsPerSecond),
        panels.timeSeries.httpClientConnections('Connections', queries.httpClientConnections),
      ])
      + row.withCollapsed(true),
      // ----------------------------------------------------------------------------
      row.new('HTTP API')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.cps('Requests (per-second)', queries.apiRequestsPerSecond('httpapi')),
        panels.timeSeries.durationQuantile('Response Time Percentiles', queries.apiResponsePercentiles('httpapi')),
        panels.timeSeries.topk('Top 5 Requests (99th percentile)', queries.apiRequestsPercentilesByRequest('httpapi')),
      ]),
      // ----------------------------------------------------------------------------
      row.new('REST API')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.cps('Requests (per-second)', queries.apiRequestsPerSecond('restapi')),
        panels.timeSeries.durationQuantile('Response Time Percentiles', queries.apiResponsePercentiles('restapi')),
      ]),
      // ----------------------------------------------------------------------------
      row.new('WebDAV API')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.cps('Requests (per-second)', queries.apiRequestsPerSecond('webdav')),
        panels.timeSeries.durationQuantile('Response Time Percentiles', queries.apiResponsePercentiles('webdav')),
      ]),
      // ----------------------------------------------------------------------------
      row.new('SOAP API')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.cps('Requests (per-second)', queries.apiRequestsPerSecond('soapapi')),
        panels.timeSeries.durationQuantile('Response Time Percentiles', queries.apiResponsePercentiles('soapapi')),
      ]),
      // ----------------------------------------------------------------------------
      row.new('Provisioning')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.cps('API Requests (per-second)', queries.provisioningApiRequestRate),
        panels.timeSeries.cps('Plugin Requests (per-second)', queries.provisioningPluginRequestRate),
        panels.timeSeries.cps('Storage Requests (per-second)', queries.provisioningStorageRequestRate),
        panels.timeSeries.durationQuantile('Average API Duration', queries.avgProvisioningApiDuration),
        panels.timeSeries.durationQuantile('Average Plugin Duration', queries.avgProvisioningPluginDuration),
        panels.timeSeries.durationQuantile('Average Storage Duration', queries.avgProvisioningStorageDuration),
      ]),
      // ----------------------------------------------------------------------------
      row.new('IMAP')
      + row.withCollapsed(true)
      + row.withPanels([
        panels.timeSeries.cps('Requests (per-second)', queries.imapRequestRate),
      ]),
    ], panelWidth=8, startY=15
  )
)
