/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.i18n.impl;

import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import com.openexchange.i18n.I18nService;
import com.openexchange.i18n.I18nServiceRegistry;
import com.openexchange.i18n.I18nTranslator;
import com.openexchange.i18n.Translator;
import com.openexchange.i18n.TranslatorFactory;


/**
 * If strings shall be translated, you need to use an appropriate {@link I18nService},
 * according to the desired locale. The {@link I18nTranslatorFactory} helps you to
 * keep track of all {@link I18nService} instances and creates new {@link Translator}
 * instances based on given locales.
 *
 * @author <a href="mailto:steffen.templin@open-xchange.com">Steffen Templin</a>
 * @since v7.8.0
 */
public class I18nTranslatorFactory implements TranslatorFactory {

    private final ConcurrentMap<Locale, Translator> translators;
    private final I18nServiceRegistry registry;

    /**
     * Initializes a new {@link I18nTranslatorFactory}.
     *
     * @param registry The i18n service registry to use
     */
    public I18nTranslatorFactory(I18nServiceRegistry registry) {
        super();
        this.registry = registry;
        this.translators = new ConcurrentHashMap<>(16, 0.9F, 1);
    }

    @Override
    public Translator translatorFor(Locale locale) {
        if (locale == null) {
            return Translator.EMPTY;
        }

        Translator t = translators.get(locale);
        if (null == t) {
            I18nService service = registry.getI18nService(locale);
            Translator nt = new I18nTranslator(service);
            t = translators.putIfAbsent(locale, nt);
            if (null == t) {
                t = nt;
            }
        }
        return t;
    }

}
