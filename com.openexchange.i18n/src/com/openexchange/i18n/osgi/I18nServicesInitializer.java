/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.i18n.osgi;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import com.openexchange.config.ConfigurationService;
import com.openexchange.i18n.I18nService;
import com.openexchange.i18n.impl.CompositeI18nTools;
import com.openexchange.i18n.impl.I18nImpl;
import com.openexchange.i18n.impl.POTranslationsDiscoverer;
import com.openexchange.i18n.impl.ResourceBundleDiscoverer;
import com.openexchange.i18n.impl.TranslationsI18N;
import com.openexchange.i18n.parsing.Translations;
import com.openexchange.java.Functions;

/**
 * {@link I18nServicesInitializer} - Properly registers all I18n services defined through property <code>"i18n.language.path"</code>
 * when configuration service is available
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class I18nServicesInitializer implements ServiceTrackerCustomizer<ConfigurationService, ConfigurationService> {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(I18nServicesInitializer.class);

    private final BundleContext context;
    private List<ServiceRegistration<I18nService>> serviceRegistrations;

    I18nServicesInitializer(final BundleContext context) {
        super();
        this.context = context;
    }

    @Override
    public synchronized ConfigurationService addingService(ServiceReference<ConfigurationService> reference) {
        ConfigurationService config = context.getService(reference);
        unregisterAll();
        try {
            serviceRegistrations = initI18nServices(context, config);
            return config;
        } catch (Exception e) {
            LOG.error("Failed to initialize i18n services", e);
            context.ungetService(reference);
            return null;
        }
    }

    @Override
    public void modifiedService(ServiceReference<ConfigurationService> reference, ConfigurationService service) {
        // Nothing
    }

    @Override
    public synchronized void removedService(ServiceReference<ConfigurationService> reference, ConfigurationService service) {
        context.ungetService(reference);
    }

    /**
     * Unregisters all registered I18n services and resets them to <code>null</code>.
     */
    public void unregisterAll() {
        List<ServiceRegistration<I18nService>> serviceRegistrations = this.serviceRegistrations;
        if (serviceRegistrations != null) {
            this.serviceRegistrations = null;
            for (ServiceRegistration<I18nService> serviceRegistration : serviceRegistrations) {
                serviceRegistration.unregister();
            }
            LOG.info("All I18n services unregistered");
        }
    }

    /**
     * Reads in all I18n services configured through property <code>"i18n.language.path"</code>, registers them, and returns corresponding
     * service registrations for future unregistration.
     *
     * @param context The currently valid bundle context
     * @return The corresponding service registrations of registered I18n services
     * @throws FileNotFoundException If directory referenced by <code>"i18n.language.path"</code> does not exist
     */
    private static List<ServiceRegistration<I18nService>> initI18nServices(BundleContext context, ConfigurationService config) throws FileNotFoundException {
        Map<Locale, List<I18nService>> locales = collectTranslations(config);
        List<ServiceRegistration<I18nService>> serviceRegistrations = new ArrayList<>(locales.size());
        for (Map.Entry<Locale, List<I18nService>> localeEntry : locales.entrySet()) {
            List<I18nService> list = localeEntry.getValue();
            I18nService i18n = list.size() == 1 ? list.get(0) : new CompositeI18nTools(list);

            Dictionary<String, Object> prop = new Hashtable<>(1);
            prop.put(I18nService.LANGUAGE, localeEntry.getKey());
            serviceRegistrations.add(context.registerService(I18nService.class, i18n, prop));
        }

        LOG.info("All I18n services registered");
        return serviceRegistrations;
    }

    private static Map<Locale, List<I18nService>> collectTranslations(ConfigurationService config) throws FileNotFoundException {
        String value = config.getProperty("i18n.language.path");
        if (null == value) {
            throw new FileNotFoundException("Configuration property 'i18n.language.path' is not defined.");
        }
        File dir = new File(value);

        List<ResourceBundle> resourceBundles = new ResourceBundleDiscoverer(dir).getResourceBundles();
        List<Translations> translations = new POTranslationsDiscoverer(dir).getTranslations();

        Map<Locale, List<I18nService>> locales = new HashMap<Locale, List<I18nService>>();
        for (Translations tr : translations) {
            locales.computeIfAbsent(tr.getLocale(), Functions.getNewArrayListFuntion()).add(new TranslationsI18N(tr));
        }
        for (ResourceBundle rc : resourceBundles) {
            locales.computeIfAbsent(rc.getLocale(), Functions.getNewArrayListFuntion()).add(new I18nImpl(rc));
        }
        return locales;
    }

}
