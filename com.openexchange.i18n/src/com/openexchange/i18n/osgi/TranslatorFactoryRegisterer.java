/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.i18n.osgi;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import com.openexchange.i18n.I18nServiceRegistry;
import com.openexchange.i18n.TranslatorFactory;
import com.openexchange.i18n.impl.I18nTranslatorFactory;

/**
 * {@link TranslatorFactoryRegisterer} - Tracks i18n service registry and registers <code>I18nTranslatorFactory</code> once available.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class TranslatorFactoryRegisterer implements ServiceTrackerCustomizer<I18nServiceRegistry, I18nServiceRegistry> {

    private final BundleContext context;
    private ServiceRegistration<TranslatorFactory> serviceRegistration;

    /**
     * Initializes a new {@link TranslatorFactoryRegisterer}.
     *
     * @param context The bundle context
     */
    public TranslatorFactoryRegisterer(BundleContext context) {
        super();
        this.context = context;
    }

    @Override
    public synchronized I18nServiceRegistry addingService(ServiceReference<I18nServiceRegistry> reference) {
        I18nServiceRegistry registry = context.getService(reference);
        serviceRegistration = context.registerService(TranslatorFactory.class, new I18nTranslatorFactory(registry), null);
        return registry;
    }

    @Override
    public void modifiedService(ServiceReference<I18nServiceRegistry> reference, I18nServiceRegistry service) {
        // Nothing
    }

    @Override
    public synchronized void removedService(ServiceReference<I18nServiceRegistry> reference, I18nServiceRegistry service) {
        ServiceRegistration<TranslatorFactory> serviceRegistration = this.serviceRegistration;
        if (serviceRegistration != null) {
            this.serviceRegistration = null;
            serviceRegistration.unregister();
        }
        context.ungetService(reference);
    }

}
