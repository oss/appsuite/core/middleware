@Library('pipeline-library') _

import com.openexchange.jenkins.Trigger

String workspace // pwd()

// Branch name which represents the default behaviour of this pipeline. For main = main for stable-X.X = stable-X.X.
final COMMON_BRANCH = 'stable-8.35'

// Middleware Helm chart name
final CHART_NAME = 'core-mw'

def skipRemainingStages = false
def useTmpBaseLayer = true
def release = false

pipeline {
    agent none
    parameters {
        booleanParam(name: 'BLACKDUCK_SCANS', defaultValue: false, description: 'Execute Blackduck scans')
        booleanParam(name: 'COVERITY_SCANS', defaultValue: false, description: 'Execute Coverity scans')
        booleanParam(name: 'SKIP_DOC', defaultValue: false, description: 'Skip documentation build')
        booleanParam(name: 'SKIP_CORE_TEST', defaultValue: false, description: 'Skip the core-test pipeline')
        booleanParam(name: 'PRESERVE_TMP_IMAGE', defaultValue: false, description: 'Do not delete the temporary base layer image afterwards')
        string(name: 'GUARD_BRANCH', defaultValue: COMMON_BRANCH, description: 'Sets the guard branch to a different value than the common branch')
        string(name: 'WEAKFORCE_BRANCH', defaultValue: COMMON_BRANCH, description: 'Sets the weakforced branch to a different value than the common branch')
        string(name: 'CORE_INTERNAL_BRANCH', defaultValue: COMMON_BRANCH, description: 'Sets the core-internal branch to a different value than the common branch')
        string(name: 'CORE_TEST_BRANCH', defaultValue: COMMON_BRANCH, description: 'Sets the core-test branch to a different value than the common branch')
    }
    environment {
        GIT_MIRROR_HOME = '/git-mirror/jenkins/appsuite'
    }
    options {
        buildDiscarder(logRotator(daysToKeepStr: '30'))
        skipDefaultCheckout()
        disableConcurrentBuilds()
    }
    triggers {
        cron('main' == env.BRANCH_NAME ? 'H H(20-23) * * 1-5' : '')
    }
    stages {
        stage('Blackduck') {
            agent {
                kubernetes {
                    yamlFile 'blackduckPod.yaml'
                }
            }
            when {
                beforeAgent true
                allOf {
                    branch 'main'
                    anyOf {
                        triggeredBy('TimerTrigger')
                        expression { params.BLACKDUCK_SCANS == true}
                    }
                }
            }
            steps {
                script {
                    dir('backend') {
                        checkout scm
                    }
                }
                container('ant') {
                    dir('backend/com.openexchange.bundles') {
                        sh 'ant -f java-commons.xml'
                    }
                }
                dir('backend') {
                    script {
                        setCredentials()
                    }
                }
                catchError(buildResult: 'UNSTABLE', stageResult: 'FAILURE') {
                    container('blackduck') {
                        scanBlackDuckHub("backend", "Product: OX App Suite Middleware", "main", "8.5")
                    }
                }
            }
            post {
                always {
                    script {
                        dir('backend') {
                            sh 'git restore gradle.properties'
                        }
                    }
                }
            }
        }
        stage('Coverity') {
            agent {
                kubernetes {
                    yamlFile 'coverityPod.yaml'
                }
            }
            when {
                beforeAgent true
                allOf {
                    branch 'main'
                    anyOf {
                        triggeredBy('TimerTrigger')
                        expression{ params.COVERITY_SCANS == true }
                    }
                }
            }
            environment {
                coverityIntermediateDir = 'analyze-idir'
                coverityBuildDir = 'build'
            }
            steps {
                script {
                    dir('backend') {
                        checkout scm
                    }
                }
                container('ant') {
                    dir('backend/com.openexchange.bundles') {
                        sh 'ant -f java-commons.xml'
                    }
                }
                dir('backend') {
                    script {
                        setCredentials()
                    }
                }
                catchError(buildResult: 'UNSTABLE', stageResult: 'FAILURE') {
                    container('coverity') {
                        dir('backend') {
                            scanCoverity('middleware-core', 'git@gitlab.open-xchange.com:appsuite/platform/core.git', 'main', "./gradlew clean build -x test -x integrationTest -Pcoverity --no-daemon", "")
                        }
                    }
                }
            }
            post {
                always {
                    archiveArtifacts(allowEmptyArchive: true, artifacts: "${coverityIntermediateDir}/build-log.txt")
                    archiveArtifacts(allowEmptyArchive: true, artifacts: "${coverityBuildDir}/**")
                    archiveArtifacts(allowEmptyArchive: true, artifacts: "${coverityIntermediateDir}/output/analysis-log.txt")
                    archiveArtifacts(allowEmptyArchive: true, artifacts: "${coverityIntermediateDir}/output/distributor.log")
                    script {
                        dir('backend') {
                            sh 'git restore gradle.properties'
                        }
                    }
                }
            }
        }
        stage('POT') {
            agent {
                kubernetes {
                    yamlFile 'potPod.yaml'
                }
            }
            environment {
                GITLAB = credentials('e9b6b0ea-2d95-4d57-ac38-f64189c5be27')
            }
            when {
                beforeAgent true
                allOf {
                    expression { getBranchName() != COMMON_BRANCH }
                    expression { skipRemainingStages == false }
                }
            }
            stages {
                stage('Checkout') {
                    steps {
                        dir('backend') {
                            checkout scm
                        }
                    }
                }
                stage('POT Commit Check') {
                    steps {
                        dir('backend') {
                            script {
                                def lastCommitMessage = sh(returnStdout: true, script: 'git show -s --format=%s').trim()
                                def previousBuild = currentBuild.getPreviousBuild()
                                if (previousBuild != null) {
                                    if (lastCommitMessage.equals("Automatic POT generation") && previousBuild.result.equals('SUCCESS')) {
                                        echo "Skip remaining stages since the build was triggered by automatic POT generation."
                                        skipRemainingStages = true
                                    }
                                }
                            }
                        }
                    }
                }
                stage('POT Generation') {
                    when {
                        expression { skipRemainingStages == false }
                    }
                    steps {
                        checkout([
                            $class: 'GitSCM',
                            branches: [[name: '*/master']],
                            doGenerateSubmoduleConfigurations: false,
                            extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'automation']],
                            gitTool: 'Linux',
                            submoduleCfg: [],
                            userRemoteConfigs: [[credentialsId: '9a40d6b1-813a-4c46-9b0d-18320a0a4ef4', url: "git@gitlab.open-xchange.com:engineering/automation.git"]]
                        ])
                        dir('automation/backendI18N') {
                            container('toox-proxy') {
                                sh "ant -file backendPot.xml -DcheckoutDir=${env.WORKSPACE}/backend -DproductToGenerate=backend -DpotDir=${env.WORKSPACE}/backend/l10n create-server-pot"
                            }
                        }
                        dir('backend') {
                            script {
                                def gitStatus = sh script: 'git status --porcelain', returnStdout: true
                                if (gitStatus.contains('l10n/backend.pot')) {
                                    sh 'git config user.email scriptuser@open-xchange.com'
                                    sh 'git config user.name Jenkins'
                                    sh 'git add l10n/backend.pot'
                                    sh 'git commit -m "Automatic POT generation"'
                                    sh 'git show HEAD'
                                    sh "git remote set-url origin https://$GITLAB_USR:$GITLAB_PSW@gitlab.open-xchange.com/appsuite/platform/core.git"
                                    sh "git push origin HEAD:${getBranchName()}"
                                }
                            }
                        }
                    }
                    post {
                        success {
                            archiveArtifacts artifacts: 'backend/l10n/backend.pot', onlyIfSuccessful: true
                        }
                    }
                }
            }
        }
        stage('Core Image') {
            agent {
                kubernetes {
                    yamlFile 'imageBuildPod.yaml'
                }
            }
            when {
                expression { skipRemainingStages == false }
            }
            environment {
                GITLAB = credentials('e9b6b0ea-2d95-4d57-ac38-f64189c5be27')
                TOOX_SHARED_ENV_CI_REGISTRY = credentials('0fa3bb8a-cc55-4752-8d4d-636568bd8b23')
                TOOX_SHARED_ENV_GITLAB = credentials('a7a6fb6e-8131-43c5-8614-0aaf2d080afb')
            }
            stages {
                stage('Checkout') {
                    steps {
                        script {
                            dir('backend') {
                                checkout scm
                                env.GIT_COMMIT = sh(returnStdout: true, script: "git rev-parse HEAD").trim()
                            }
                        }
                    }
                }
                stage('Prepare') {
                    steps {
                        dir('backend') {
                            script {
                                dir('com.openexchange.bundles') {
                                    container('ant') {
                                        sh 'ant -f java-commons.xml'
                                    }
                                }
                                // Write access token to gradle.properties to be able to load dependencies
                                propertiesContent = readFile 'gradle.properties'
                                withCredentials([usernamePassword(credentialsId: '9a41527b-8fdc-4d4f-b48b-328ca3a84c9f', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                                    propertiesContent = propertiesContent.replaceAll("GITLAB_USERNAME", USERNAME)
                                    propertiesContent = propertiesContent.replaceAll("GITLAB_PASSWORD", PASSWORD)
                                }
                                writeFile file: "gradle.properties", text: propertiesContent
                            }
                        }
                    }
                }
                stage('Core Commit Check') {
                    when {
                        allOf {
                            expression { getBranchName() == COMMON_BRANCH }
                        }
                    }
                    steps {
                        dir('backend') {
                            script {
                                try {
                                    container('toox-proxy') {
                                        sh 'gradle testRelease'
                                        release = true
                                    }
                                } catch (Exception e) {
                                    echo "Nothing to release. Use existing core image ..."
                                    useTmpBaseLayer = false
                                }
                            }
                        }
                    }
                }
                stage('Test Helm Chart') {
                    environment {
                        CI_REGISTRY = credentials('0fa3bb8a-cc55-4752-8d4d-636568bd8b23')
                    }
                    steps{
                        container('k8s') {
                            dir('backend') {
                                script{
                                    sh 'helm registry login registry.open-xchange.com -u $CI_REGISTRY_USR -p $CI_REGISTRY_PSW'
                                    sh "helm dep up helm/${CHART_NAME}"
                                    // helm lint
                                    sh "helm lint helm/${CHART_NAME} --set mysql.auth.user=alice --set mysql.auth.password=secret --set mysql.auth.rootPassword=secret"
                                    // helm template
                                    sh "helm template helm/${CHART_NAME} --set mysql.auth.user=alice --set mysql.auth.password=secret --set mysql.auth.rootPassword=secret > /tmp/${CHART_NAME}.yaml"
                                    if (fileExists("/tmp/${CHART_NAME}yaml")) {
                                        // kubeval
                                        sh "kubeval /tmp/${CHART_NAME}.yaml"
                                        // polaris
                                        sh "polaris audit --config helm/${CHART_NAME}/tests/polaris/checks.yaml --only-show-failed-tests=true --audit-path /tmp/${CHART_NAME}.yaml --format=pretty --color=false"
                                    }
                                    // terratest
                                    dir("helm/${CHART_NAME}/tests/terratest") {
                                         sh "go test -v > test_output.log"
                                         sh "terratest_log_parser -testlog test_output.log -outputdir test_output"
                                         junit testResults: 'test_output/report.xml'
                                    }
                                }
                            }
                        }
                    }
                }
                stage('Build Core Image')  {
                    when {
                        expression { useTmpBaseLayer == true }
                    }
                    steps {
                        dir('backend') {
                            script {
                                container('toox-proxy') {
                                    retry(3) {
                                        sh 'gradle buildahPushImage-core-tmp -x integrationTest --stacktrace'
                                    }
                                }
                                junit allowEmptyResults: true, testResults: '**/build/test-results/**/TEST-*.xml'
                                if (currentBuild.currentResult == "UNSTABLE") {
                                    error 'Unit tests failed! Please check the test results.'
                                }
                                if ("${getBranchName()}" == COMMON_BRANCH) {
                                    container('toox-proxy') {
                                        sh 'gradle publishNextVersion --stacktrace'
                                    }
                                    env.VERSION_NUMBER = sh(script: 'git describe --tags --abbrev=0', returnStdout: true).trim()
                                    echo "Version number: ${env.VERSION_NUMBER}"
                                }
                            }
                        }
                    }
                }
                stage('API Tests') {
                    when {
                        expression { params.SKIP_CORE_TEST == false }
                    }
                    steps {
                        script {
                            def componentsJson = []
                            componentsJson.add([name: "backend", branch: getBranchName(), commit: env.GIT_COMMIT])
                            if ("${getBranchName()}" != COMMON_BRANCH) {
                                componentsJson.add([name: "core-internal", branch: params.CORE_INTERNAL_BRANCH])
                                componentsJson.add([name: "guard", branch: params.GUARD_BRANCH])
                                componentsJson.add([name: "weakforced", branch: params.WEAKFORCE_BRANCH])
                            }
                            def isTriggeredByTimer = !currentBuild.getBuildCauses('hudson.triggers.TimerTrigger$TimerTriggerCause').isEmpty()
                            build job: "middleware/Core-test/${params.CORE_TEST_BRANCH}",
                            parameters: [
                                string(name: 'OVERWRITE_COMPONENTS', value: writeJSON(returnText: true, json: componentsJson)),
                                booleanParam(name: 'CORE_TMP_IMAGE', value: useTmpBaseLayer),
                                booleanParam(name: 'SKIP_PERFORMANCE_TESTS', value: isTriggeredByTimer == false)
                            ]
                        }
                    }
                }
                stage('Release Image') {
                    when {
                        allOf {
                            expression { release == true }
                        }
                    }
                    steps {
                        dir('backend') {
                            script {
                                container('toox-proxy') {
                                    retry(3) {
                                        sh 'gradle buildahPushAllImages -x check --stacktrace'
                                    }
                                }
                                addSshHostKey(host: 'gitlab.open-xchange.com')
                                sshagent(['9a40d6b1-813a-4c46-9b0d-18320a0a4ef4']) {
                                    sh 'git push origin --tags'
                                }
                            }
                        }
                    }
                }
            }
            post {
                always {
                    container('toox-proxy') {
                        dir('backend') {
                            script {
                                if (useTmpBaseLayer && params.PRESERVE_TMP_IMAGE == false) {
                                    retry(3) {
                                        sh 'gradle deleteImage-core-tmp -x check --stacktrace'
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        stage('Release Helm Chart') {
            agent {
                kubernetes {
                    yamlFile 'helmPod.yaml'
                }
            }
            when {
                beforeAgent true
                allOf {
                    expression { params.CORE_TEST_BRANCH == COMMON_BRANCH }
                    expression { skipRemainingStages == false }
                    expression { release == true }
                }
            }
            environment {
                CI_REGISTRY = credentials('0fa3bb8a-cc55-4752-8d4d-636568bd8b23')
            }
            steps {
                container('k8s') {
                    script {
                        cloneCore(env.GIT_COMMIT)
                        dir('backend') {
                            def chart = readYaml file: "helm/${CHART_NAME}/Chart.yaml"
                            def registryUrl = 'registry.open-xchange.com'
                            def apiUrl = "https://${registryUrl}/api/v2.0/projects/appsuite-core-internal/repositories/charts%252Fcore-mw/artifacts/${chart.version}/tags"

                            def response = sh(script: "curl -s -w %{http_code} -o /dev/null -u ${env.CI_REGISTRY_USR}:${env.CI_REGISTRY_PSW} ${apiUrl}", returnStdout: true).trim()

                            if (response == '404') {
                                echo "Publishing Helm Chart (${chart.version})"
                                sh 'helm registry login registry.open-xchange.com -u $CI_REGISTRY_USR -p $CI_REGISTRY_PSW'
                                sh "helm dep up helm/${CHART_NAME}"
                                sh "helm package helm/${CHART_NAME} --app-version ${env.VERSION_NUMBER}"
                                sh "helm push ${CHART_NAME}-${chart.version}.tgz oci://${registryUrl}/appsuite-core-internal/charts"
                            } else {
                                echo "Skip publishing of the helm chart because ${chart.version} already exists."
                            }
                        }
                    }
                }
            }
        }
        stage('Documentation') {
            agent {
                kubernetes {
                    yamlFile 'jenkinsPod.yaml'
                }
            }
            when {
                beforeAgent true
                allOf {
                    expression { params.CORE_TEST_BRANCH == COMMON_BRANCH }
                    expression { skipRemainingStages == false }
                    expression { release == true }
                }
            }
            stages {
                stage('Prepare') {
                    agent {
                        kubernetes {
                            yamlFile 'helmPod.yaml'
                        }
                    }
                    environment {
                        CI_REGISTRY = credentials('0fa3bb8a-cc55-4752-8d4d-636568bd8b23')
                    }
                    steps {
                        script {
                            env.SYMLINK_DOC = env.BRANCH_NAME == 'main' || env.BRANCH_NAME.startsWith('stable-')
                            def versionNumber = getVersionComponents(env.VERSION_NUMBER)
                            env.MAJOR = versionNumber.major
                            env.MINOR = versionNumber.minor
                            env.PATCH = versionNumber.patch
                            if (env.BRANCH_NAME.startsWith('stable-')) {
                                container('k8s') {
                                    script {
                                        def image = "docker://registry.open-xchange.com/appsuite-core-internal/middleware"
                                        env.LATEST_STABLE_BRANCH = sh(script: "skopeo inspect --creds ${env.CI_REGISTRY_USR}:${env.CI_REGISTRY_PSW} ${image} | jq '.RepoTags | contains([\"stable-${env.MAJOR}.${Integer.parseInt(env.MINOR) + 1}\"]) | not'", returnStdout: true).trim()
                                    }
                                }
                            }
                        }
                    }
                }
                stage('Checkout') {
                    steps {
                        script {
                            cloneCore(env.GIT_COMMIT)
                        }
                        dir('backend') {
                            script {
                                setCredentials()
                            }
                        }
                    }
                }
                stage('Configuration Documentation') {
                    when {
                        expression { params.SKIP_DOC == false }
                    }
                    steps {
                        script {
                            def targetDirectory
                            dir('config-doc-processor') {
                                // Need to do some file operation in directory otherwise it is not created.
                                writeFile file: 'properties.json', text: ''
                                targetDirectory = pwd()
                            }
                            container('ant') {
                                dir('backend/com.openexchange.bundles') {
                                    sh 'ant -f java-commons.xml'
                                }
                            }
                            container('gradle') {
                                dir('backend/documentation-generic/config') {
                                    sh "gradle --stacktrace --no-daemon runConfigDocuProcessor -PtargetDirectory=${targetDirectory} -PtargetVersion=${env.VERSION_NUMBER}"
                                }
                            }
                            dir('config-doc-processor') {
                                script {
                                    publishConfigDoc()
                                }
                            }
                            build job: 'middleware/propertyDocumentationUI/master', parameters: [string(name: 'targetVersion', value: env.VERSION_NUMBER), string(name: 'targetDirectory', value: 'middleware/config')]
                        }
                    }
                    post {
                        success {
                            archiveArtifacts 'config-doc-processor/properties.json'
                        }
                    }
                }
                stage('HTTP API Documentation') {
                    when {
                        expression { params.SKIP_DOC == false }
                    }
                    steps {
                        script {
                            container('gradle') {
                                dir('backend') {
                                    sh 'gradle --stacktrace --no-daemon :http-api:generateHttpApiDoc :drive-api:generateHttpApiDoc :rest-api:generateHttpApiDoc'
                                }
                            }
                            dir('backend/documentation-generic/') {
                                script {
                                    publishHttpApiDoc()
                                }
                            }
                        }
                    }
                    post {
                        always {
                            script {
                                dir('backend') {
                                    sh 'git restore gradle.properties'
                                }
                            }
                        }
                    }
                }
                stage('Markdown Documentation') {
                    steps {
                        script {
                            buildAndPublishMarkdownDoc()
                        }
                    }
                }
            }
        }
        stage('Integration Build') {
            agent any
            when {
                allOf {
                    expression { skipRemainingStages == false }
                    expression { release == true }
                }
            }
            steps {
                dir('backend') {
                    script {
                        def buildJob = build job: "appsuite/integration/${COMMON_BRANCH}", wait: false, propagate: false
                    }
                }
            }
        }
    }
    post {
        failure {
            emailext attachLog: true,
                body: "${env.BUILD_URL} failed.\n\nFull log at: ${env.BUILD_URL}console\n\n",
                subject: "${env.JOB_NAME} (#${env.BUILD_NUMBER}) - ${currentBuild.result}",
                recipientProviders: [[$class: 'RequesterRecipientProvider'], [$class: 'UpstreamComitterRecipientProvider'], [$class: 'DevelopersRecipientProvider']],
                to: 'backend@open-xchange.com'
        }
    }
}

/**
 * Retrieves the branch name.
 * If the branch name starts with "MR-", it returns the actual branch name instead.
 *
 * @return The name of the branch as a String.
 */
String getBranchName() {
    def branchName = env.BRANCH_NAME
    if (branchName.startsWith("MR-")) {
        branchName = env.CHANGE_BRANCH
    }
    return branchName
}

/**
 * Parses a version string in the format "major.minor.patch" and extracts
 * the major, minor, and patch components.
 *
 * @param version The version string to be parsed, expected to be in the format "X.Y.Z".
 * @return A map containing the major, minor, and patch components of the version.
 *         The map has keys "major", "minor", and "patch".
 * @throws IllegalArgumentException if the version string does not match the expected format.
 */
def getVersionComponents(String version) {
    def matcher = version =~ /(\d+)\.(\d+)\.(\d+)/
    if (matcher.matches()) {
        def major = matcher[0][1]
        def minor = matcher[0][2]
        def patch = matcher[0][3]
        return [major: major, minor: minor, patch: patch]
    } else {
        throw new IllegalArgumentException("Invalid version format: ${version}")
    }
}

/**
 * Clones the core repository from a specified branch using Git.
 * The repository is checked out into a relative target directory named 'backend'.
 *
 * @param branch The name of the branch to be checked out.
 * @throws Exception if the checkout fails due to Git errors or configuration issues.
 */
def cloneCore(String branch) {
    script {
        checkout([
            $class: 'GitSCM',
            branches: [[name: branch]],
            doGenerateSubmoduleConfigurations: false,
            extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'backend'], [$class: 'CloneOption', reference: "$GIT_MIRROR_HOME/backend.git"]],
            gitTool: 'Linux',
            submoduleCfg: [],
            userRemoteConfigs: [[credentialsId: '9a40d6b1-813a-4c46-9b0d-18320a0a4ef4', url: 'git@gitlab.open-xchange.com:appsuite/platform/core.git']]
        ])
    }
}

/**
 * Builds and publishes the Markdown documentation.
 * This method processes the CHANGELOG.md file, modifies links in the documentation,
 * generates the Jekyll site, and copies the generated documentation to a specified directory.
 * 
 * The method also handles symlinking of documentation based on the branch name and
 * publishes the documentation using SSH.
 */
def buildAndPublishMarkdownDoc() {
    container('jekyll'){
        sh """
            tail -n +3 ${WORKSPACE}/backend/CHANGELOG.md >> ${WORKSPACE}/backend/documentation/index.md
            sed -i 's+https://jira.open-xchange.com/browse/SCR+https://documentation.open-xchange.com/${env.VERSION_NUMBER}/middleware/detailed_software_changes.html#scr+gI' ${WORKSPACE}/backend/documentation/index.md
            ln -s ${WORKSPACE}/backend/documentation /documentation/jekyll/_middleware
            cd /documentation
            echo branchName: "/${env.BRANCH_NAME}" >> _config.yml
            cat _config.yml
            bundle exec jekyll b --baseurl /${env.MAJOR} --config _config.yml
            cd ${WORKSPACE}
            mkdir -p doku/${env.VERSION_NUMBER}
            cp -r /documentation/dist/* doku/${env.VERSION_NUMBER}
        """
    }
    dir('doku') {
        script {
            def basePath = "/var/www/documentation"
            def execCommand = new StringBuilder()
            if (env.SYMLINK_DOC == "true") {
                if (env.BRANCH_NAME.startsWith('stable-')) {
                    execCommand.append("mkdir -p ${basePath}/${env.MAJOR} && mkdir -p ${basePath}/${env.MAJOR}.${env.MINOR}")
                    execCommand.append(" && ")
                    execCommand.append("cp -rfs ${basePath}/${env.MAJOR}.${env.MINOR}.${env.PATCH}/* ${basePath}/${env.MAJOR}.${env.MINOR}/")
                    if (env.LATEST_STABLE_BRANCH == "true") {
                        execCommand.append(" && ")
                        execCommand.append("cp -rfs ${basePath}/${env.MAJOR}.${env.MINOR}/* ${basePath}/${env.MAJOR}/")
                    }
                }
                if (env.BRANCH_NAME == 'main') {
                    execCommand.append("cp -rfs ${basePath}/${env.MAJOR}.${env.MINOR}.${env.PATCH}/* ${basePath}/main/")
                }
                execCommand.append(" | true")
            }
            sshPublisher(
                publishers: [
                    sshPublisherDesc(
                        configName: 'documentation',
                        transfers: [
                            sshTransfer(
                                cleanRemote: false,
                                excludes: '',
                                execCommand: "${execCommand.toString()}",
                                execTimeout: 120000,
                                flatten: false,
                                makeEmptyDirs: false,
                                noDefaultExcludes: false,
                                patternSeparator: '[, ]+',
                                remoteDirectorySDF: false,
                                sourceFiles: "${env.VERSION_NUMBER}/**"
                            )
                        ],
                        usePromotionTimestamp: false,
                        useWorkspaceInPromotion: false,
                        verbose: true
                    )
                ]
            )
        }
    }
    }

/**
 * Publishes the HTTP API, Drive API, and REST API documentation.
 * This method creates the necessary directories for the documentation based on the versioning scheme
 * and copies the generated documentation to the appropriate locations.
 * 
 * The method also handles symlinking of documentation based on the branch name and
 * publishes the documentation using SSH.
 */
def publishHttpApiDoc() {
    def basePath = "/var/www/documentation/components/middleware"
    def httpApiDocPath = "${basePath}/http"
    def driveApiDocPath = "${basePath}/drive"
    def restApiDocPath = "${basePath}/rest"

    def execCommandHTTP = new StringBuilder()
    def execCommandDrive = new StringBuilder()
    def execCommandREST = new StringBuilder()
    if (env.SYMLINK_DOC == "true") {
        if (env.BRANCH_NAME.startsWith('stable-')) {
            // HTTP API
            execCommandHTTP.append("mkdir -p ${httpApiDocPath}/${env.MAJOR} && mkdir -p ${httpApiDocPath}/${env.MAJOR}.${env.MINOR}")
            execCommandHTTP.append(" && ")
            execCommandHTTP.append("cp -rfs ${httpApiDocPath}/${env.MAJOR}.${env.MINOR}.${env.PATCH}/* ${httpApiDocPath}/${env.MAJOR}.${env.MINOR}/")
            // Drive API
            execCommandDrive.append("mkdir -p  ${driveApiDocPath}/${env.MAJOR} && mkdir -p ${driveApiDocPath}/${env.MAJOR}.${env.MINOR}")
            execCommandDrive.append(" && ")
            execCommandDrive.append("cp -rfs ${driveApiDocPath}/${env.MAJOR}.${env.MINOR}.${env.PATCH}/* ${driveApiDocPath}/${env.MAJOR}.${env.MINOR}/")
            // REST API
            execCommandREST.append("mkdir -p ${restApiDocPath}/${env.MAJOR} && mkdir -p ${restApiDocPath}/${env.MAJOR}.${env.MINOR}")
            execCommandREST.append(" && ")
            execCommandREST.append("cp -rfs ${restApiDocPath}/${env.MAJOR}.${env.MINOR}.${env.PATCH}/* ${restApiDocPath}/${env.MAJOR}.${env.MINOR}/")
            // Only symlink latest minor to major
            if (env.LATEST_STABLE_BRANCH == "true") {
                execCommandHTTP.append(" && ")
                execCommandHTTP.append("cp -rfs ${httpApiDocPath}/${env.MAJOR}.${env.MINOR}/* ${httpApiDocPath}/${env.MAJOR}/")
                execCommandDrive.append(" && ")
                execCommandDrive.append("cp -rfs ${driveApiDocPath}/${env.MAJOR}.${env.MINOR}/* ${driveApiDocPath}/${env.MAJOR}/")
                execCommandREST.append(" && ")
                execCommandREST.append("cp -rfs ${restApiDocPath}/${env.MAJOR}.${env.MINOR}/* ${restApiDocPath}/${env.MAJOR}/")
            }
        }
        if (env.BRANCH_NAME == 'main') {
            execCommandHTTP.append("cp -rfs ${httpApiDocPath}/${env.MAJOR}.${env.MINOR}.${env.PATCH}/* ${httpApiDocPath}/main/")
            execCommandDrive.append("cp -rfs ${driveApiDocPath}/${env.MAJOR}.${env.MINOR}.${env.PATCH}/* ${driveApiDocPath}/main/")
            execCommandREST.append("cp -rfs ${restApiDocPath}/${env.MAJOR}.${env.MINOR}.${env.PATCH}/* ${restApiDocPath}/main/")
        }
        execCommandHTTP.append(" | true")
        execCommandDrive.append(" | true")
        execCommandREST.append(" | true")
    }
    sshPublisher(
        publishers: [
            sshPublisherDesc(
                configName: 'documentation',
                transfers: [
                        sshTransfer(
                            cleanRemote: true,
                            excludes: '',
                            execCommand: "${execCommandHTTP.toString()}",
                            execTimeout: 120000,
                            flatten: false,
                            makeEmptyDirs: false,
                            noDefaultExcludes: false,
                            patternSeparator: '[, ]+',
                            remoteDirectory: "components/middleware/http/${env.VERSION_NUMBER}",
                            remoteDirectorySDF: false,
                            removePrefix: 'http_api/documents/html',
                            sourceFiles: 'http_api/documents/html/**'),
                        sshTransfer(
                            cleanRemote: true,
                            excludes: '',
                            execCommand: "${execCommandDrive.toString()}",
                            execTimeout: 120000,
                            flatten: false,
                            makeEmptyDirs: false,
                            noDefaultExcludes: false,
                            patternSeparator: '[, ]+',
                            remoteDirectory: "components/middleware/drive/${env.VERSION_NUMBER}",
                            remoteDirectorySDF: false,
                            removePrefix: 'drive_api/documents/html',
                            sourceFiles: 'drive_api/documents/html/**'),
                        sshTransfer(
                            cleanRemote: true,
                            excludes: '',
                            execCommand: "${execCommandREST.toString()}",
                            execTimeout: 120000,
                            flatten: false,
                            makeEmptyDirs: false,
                            noDefaultExcludes: false,
                            patternSeparator: '[, ]+',
                            remoteDirectory: "components/middleware/rest/${env.VERSION_NUMBER}",
                            remoteDirectorySDF: false,
                            removePrefix: 'rest_api/documents/html',
                            sourceFiles: 'rest_api/documents/html/**')
                ],
                usePromotionTimestamp: false,
                useWorkspaceInPromotion: false,
                verbose: true
            )
        ]
    )
}

/**
 * Publishes the configuration documentation.
 * This method creates the necessary directories for the configuration documentation based on the versioning scheme
 * and copies the properties.json file to the appropriate location.
 * 
 * The method also handles symlinking of documentation based on the branch name and
 * publishes the documentation using SSH.
 */
def publishConfigDoc() {
    def configDocPath = "/var/www/documentation/components/middleware/config"
    def execCommand = new StringBuilder()
    if (env.SYMLINK_DOC == "true") {
        if (env.BRANCH_NAME.startsWith('stable-')) {
            execCommand.append("mkdir -p ${configDocPath}/${env.MAJOR} && mkdir -p ${configDocPath}/${env.MAJOR}.${env.MINOR}")
            execCommand.append(" && ")
            execCommand.append("cp -rfs ${configDocPath}/${env.MAJOR}.${env.MINOR}.${env.PATCH}/* ${configDocPath}/${env.MAJOR}.${env.MINOR}/")
            if (env.LATEST_STABLE_BRANCH == "true") {
                execCommand.append(" && ")
                execCommand.append("cp -rfs ${configDocPath}/${env.MAJOR}.${env.MINOR}/* ${configDocPath}/${env.MAJOR}/")
            }
        }
        if (env.BRANCH_NAME == 'main') {
            execCommand.append("cp -rfs ${configDocPath}/${env.MAJOR}.${env.MINOR}.${env.PATCH}/* ${configDocPath}/main/")
        }
        execCommand.append(" | true")
    }
    sshPublisher(
        publishers: [
            sshPublisherDesc(
                configName: 'documentation',
                transfers: [
                    sshTransfer(
                        cleanRemote: false,
                        excludes: '',
                        execCommand: "${execCommand.toString()}",
                        execTimeout: 120000,
                        flatten: false,
                        makeEmptyDirs: false,
                        noDefaultExcludes: false,
                        patternSeparator: '[, ]+',
                        remoteDirectory: "components/middleware/config/${env.VERSION_NUMBER}",
                        remoteDirectorySDF: false,
                        removePrefix: '',
                        sourceFiles: 'properties.json'
                    )
                ],
                usePromotionTimestamp: false,
                useWorkspaceInPromotion: false,
                verbose: true
            )
        ]
    )
}

/**
 * Updates the `gradle.properties` file with GitLab credentials.
 *
 * This method reads the existing properties, appends GitLab credentials (username and password) 
 * retrieved securely, and writes the updated properties back to the file.
 *
 * @throws IOException If an error occurs while reading or writing the 
 *         `gradle.properties` file.
 */
def setCredentials() {
    def properties = readFile 'gradle.properties'
    def updatedProperties = new StringBuilder(properties)
    withCredentials([usernamePassword(credentialsId: '47690614-b089-4c42-8c52-e773a891dafd', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        updatedProperties.append("\ngitlabUsername=$USERNAME")
        updatedProperties.append("\ngitlabPassword=$PASSWORD")
    }
    writeFile file: "gradle.properties", text: updatedProperties.toString()
}