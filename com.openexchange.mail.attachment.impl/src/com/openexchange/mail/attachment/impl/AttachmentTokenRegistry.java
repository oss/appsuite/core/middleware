/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.attachment.impl;

import static com.openexchange.java.Autoboxing.I;
import java.io.InputStream;
import java.util.Set;
import com.openexchange.exception.OXException;
import com.openexchange.mail.attachment.AttachmentToken;
import com.openexchange.mail.attachment.AttachmentTokenConstants;
import com.openexchange.mail.attachment.AttachmentTokenService;
import com.openexchange.mail.attachment.impl.commands.JsonRedisStringCommands;
import com.openexchange.redis.RedisCommandsProvider;
import com.openexchange.redis.RedisConnector;
import com.openexchange.redis.RedisConnectorService;
import com.openexchange.redis.RedisOperation;
import com.openexchange.redis.RedisVoidOperation;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import io.lettuce.core.api.sync.RedisKeyCommands;
import io.lettuce.core.api.sync.RedisStringCommands;

/**
 * {@link AttachmentTokenRegistry}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class AttachmentTokenRegistry implements AttachmentTokenConstants, AttachmentTokenService {

    static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(AttachmentTokenRegistry.class);

    private static final char DELIMITER = ':';

    private static final String REDIS_SET_TOKENIDS = "ox-attachTokenIds";
    private static final String REDIS_TOKEN = "ox-attachToken";

    /**
     * Initializes the instance
     *
     * @param services The services look-up
     * @return The initialized instance
     * @throws OXException If required service is absent
     */
    public static AttachmentTokenRegistry initInstance(ServiceLookup services) throws OXException {
        RedisConnectorService connectorService = services.getServiceSafe(RedisConnectorService.class);
        RedisConnector redisConnector = connectorService.getConnectorProvider().getConnector();
        return new AttachmentTokenRegistry(redisConnector);
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    private final RedisConnector connector;

    /**
     * Initializes a new {@link AttachmentTokenRegistry}.
     */
    private AttachmentTokenRegistry(RedisConnector connector) {
        super();
        this.connector = connector;
    }

    @Override
    public void dropFor(final int userId, final int contextId) {
        try {
            connector.executeVoidOperation(new DropForRedisOperation(contextId, userId));
        } catch (Exception e) {
            LOG.warn("Failed to remove attachment tokens for user {} in context {} from Redis storage", I(userId), I(contextId), e);
        }
        LOG.debug("Cleaned attachment tokens for user {} in context {}", I(userId), I(contextId));
    }

    @Override
    public void dropFor(final Session session) {
        dropFor(session.getUserId(), session.getContextId());
    }

    @Override
    public void removeToken(final String tokenId) {
        try {
            connector.executeVoidOperation(new RemoveTokenRedisOperation(tokenId));
        } catch (Exception e) {
            LOG.warn("Failed to remove attachment token {} from Redis storage", tokenId, e);
        }
    }

    @Override
    public AttachmentToken getToken(String tokenId, boolean chunked) {
        try {
            AttachmentToken attachmentToken = connector.executeOperation(new GetTokenRedisOperation(tokenId));
            if (null == attachmentToken) {
                return null;
            }

            if (attachmentToken.isExpired()) {
                removeToken(tokenId);
                return null;
            }
            if (attachmentToken.isOneTime() && !chunked) {
                removeToken(tokenId);
                return attachmentToken;
            }
            return attachmentToken.touch();

        } catch (Exception e) {
            LOG.warn("Failed to retrieve attachment token {} from Redis storage", tokenId, e);
        }
        return null;
    }

    @Override
    public void putToken(final AttachmentToken token, final Session session) {
        try {
            connector.executeVoidOperation(new PutTokenRedisOperation(token));
        } catch (Exception e) {
            LOG.warn("Failed to put attachment token {} into Redis storage", token.getId(), e);
        }
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    protected static String getTokenKey(String tokenId) {
        return new StringBuilder(REDIS_TOKEN).append(DELIMITER).append(tokenId).toString();
    }

    protected static String getSetKey(int userId, int contextId) {
        return new StringBuilder(REDIS_SET_TOKENIDS).append(DELIMITER).append(contextId).append(DELIMITER).append(userId).toString();
    }

    /**
     * Gets the token commands from given Redis connection.
     *
     * @param commandsProvider The commands provider
     * @return The token commands
     */
    protected static RedisStringCommands<String, AttachmentToken> getTokenCommands(RedisCommandsProvider commandsProvider) {
        return new JsonRedisStringCommands(commandsProvider.getRawStringCommands());
    }

    /**
     * Redis operation to put a token.
     */
    private static class PutTokenRedisOperation implements RedisVoidOperation {

        private final AttachmentToken token;

        /**
         * Initializes a new {@link RedisVoidOperationImplementation}.
         *
         * @param token The token to put into Redis collection
         */
        private PutTokenRedisOperation(AttachmentToken token) {
            super();
            this.token = token;
        }

        @Override
        public void executeWithoutResult(RedisCommandsProvider commandsProvider) throws OXException {
            getTokenCommands(commandsProvider).psetex(getTokenKey(token.getId()), AttachmentTokenConstants.DEFAULT_TIMEOUT, token);
            commandsProvider.getSetCommands().sadd(getSetKey(token.getUserId(), token.getContextId()), token.getId());
        }
    }

    /**
     * Redis operation to retrieve the token for a specified token identifier.
     */
    private static class GetTokenRedisOperation implements RedisOperation<AttachmentToken> {

        private final String tokenId;

        /**
         * Initializes a new {@link RedisOperationImplementation}.
         *
         * @param tokenId The token identifier
         */
        private GetTokenRedisOperation(String tokenId) {
            super();
            this.tokenId = tokenId;
        }

        @Override
        public AttachmentToken execute(RedisCommandsProvider commandsProvider) throws OXException {
            AttachmentToken token = getTokenCommands(commandsProvider).get(getTokenKey(tokenId));
            if (token != null) {
                commandsProvider.getKeyCommands().pexpire(getTokenKey(tokenId), AttachmentTokenConstants.DEFAULT_TIMEOUT);
            }
            return token;
        }
    }

    /**
     * Redis operation to remove the token by a given token identifier.
     */
    private static class RemoveTokenRedisOperation implements RedisVoidOperation {

        private final String tokenId;

        /**
         * Initializes a new {@link RedisVoidOperationImplementation}.
         *
         * @param tokenId The token identifier
         */
        private RemoveTokenRedisOperation(String tokenId) {
            super();
            this.tokenId = tokenId;
        }

        @Override
        public void executeWithoutResult(RedisCommandsProvider commandsProvider) throws OXException {
            AttachmentToken removedToken = getTokenCommands(commandsProvider).getdel(getTokenKey(tokenId));
            if (removedToken != null) {
                commandsProvider.getSetCommands().srem(getSetKey(removedToken.getUserId(), removedToken.getContextId()), tokenId);
            }
        }
    }

    /**
     * Redis operation to drop entries a specified given user.
     */
    private static class DropForRedisOperation implements RedisVoidOperation {

        private final int contextId;
        private final int userId;

        /**
         * Initializes a new {@link RedisVoidOperationImplementation}.
         *
         * @param contextId The context identifier
         * @param userId The user identifier
         */
        DropForRedisOperation(int contextId, int userId) {
            super();
            this.contextId = contextId;
            this.userId = userId;
        }

        @Override
        public void executeWithoutResult(RedisCommandsProvider commandsProvider) throws OXException {
            Set<String> tokenIds = commandsProvider.getSetCommands().smembers(getSetKey(userId, contextId));
            if (tokenIds == null || tokenIds.isEmpty()) {
                return;
            }

            RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
            for (String tokenId : tokenIds) {
                keyCommands.del(getTokenKey(tokenId));
            }
            keyCommands.del(getSetKey(userId, contextId));
        }
    }

}
