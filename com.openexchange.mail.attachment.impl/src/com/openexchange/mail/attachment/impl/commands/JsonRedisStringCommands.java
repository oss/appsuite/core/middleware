/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.attachment.impl.commands;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.openexchange.java.Streams;
import com.openexchange.mail.attachment.AttachmentToken;
import io.lettuce.core.BitFieldArgs;
import io.lettuce.core.GetExArgs;
import io.lettuce.core.KeyValue;
import io.lettuce.core.SetArgs;
import io.lettuce.core.StrAlgoArgs;
import io.lettuce.core.StringMatchResult;
import io.lettuce.core.api.sync.RedisStringCommands;
import io.lettuce.core.output.KeyValueStreamingChannel;


/**
 * {@link JsonRedisStringCommands}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.0.0
 */
public class JsonRedisStringCommands implements RedisStringCommands<String, AttachmentToken> {

    private final RedisStringCommands<String, InputStream> commands;

    /**
     * Initializes a new {@link JsonRedisStringCommands}.
     */
    public JsonRedisStringCommands(RedisStringCommands<String, InputStream> commands) {
        super();
        this.commands = commands;
    }

    @Override
    public Long append(String key, AttachmentToken value) {
        InputStream stream = null;
        try {
            stream = AttachmentTokenCodec.token2Json(value).getStream(false);
            return commands.append(key, stream);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to convert session", e);
        } finally {
            Streams.close(stream);
        }
    }

    @Override
    public Long bitcount(String key) {
        return commands.bitcount(key);
    }

    @Override
    public Long bitcount(String key, long start, long end) {
        return commands.bitcount(key, start, end);
    }

    @Override
    public List<Long> bitfield(String key, BitFieldArgs bitFieldArgs) {
        return commands.bitfield(key, bitFieldArgs);
    }

    @Override
    public Long bitpos(String key, boolean state) {
        return commands.bitpos(key, state);
    }

    @Override
    public Long bitpos(String key, boolean state, long start) {
        return commands.bitpos(key, state, start);
    }

    @Override
    public Long bitpos(String key, boolean state, long start, long end) {
        return commands.bitpos(key, state, start, end);
    }

    @Override
    public Long bitopAnd(String destination, String... keys) {
        return commands.bitopAnd(destination, keys);
    }

    @Override
    public Long bitopNot(String destination, String source) {
        return commands.bitopNot(destination, source);
    }

    @Override
    public Long bitopOr(String destination, String... keys) {
        return commands.bitopOr(destination, keys);
    }

    @Override
    public Long bitopXor(String destination, String... keys) {
        return commands.bitopXor(destination, keys);
    }

    @Override
    public Long decr(String key) {
        return commands.decr(key);
    }

    @Override
    public Long decrby(String key, long amount) {
        return commands.decrby(key, amount);
    }

    @Override
    public AttachmentToken get(String key) {
        try {
            return AttachmentTokenCodec.stream2Token(commands.get(key));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to get", e);
        }
    }

    @Override
    public Long getbit(String key, long offset) {
        return commands.getbit(key, offset);
    }

    @Override
    public AttachmentToken getdel(String key) {
        try {
            return AttachmentTokenCodec.stream2Token(commands.getdel(key));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to getdel", e);
        }
    }

    @Override
    public AttachmentToken getex(String key, GetExArgs args) {
        try {
            return AttachmentTokenCodec.stream2Token(commands.getex(key, args));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to getex", e);
        }
    }

    @Override
    public AttachmentToken getrange(String key, long start, long end) {
        try {
            return AttachmentTokenCodec.stream2Token(commands.getrange(key, start, end));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to getex", e);
        }
    }

    @Override
    public AttachmentToken getset(String key, AttachmentToken value) {
        try {
            return AttachmentTokenCodec.stream2Token(commands.getset(key, AttachmentTokenCodec.token2Json(value).getStream(false)));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to getset", e);
        }
    }

    @Override
    public Long incr(String key) {
        return commands.incr(key);
    }

    @Override
    public Long incrby(String key, long amount) {
        return commands.incrby(key, amount);
    }

    @Override
    public Double incrbyfloat(String key, double amount) {
        return commands.incrbyfloat(key, amount);
    }

    @Override
    public List<KeyValue<String, AttachmentToken>> mget(String... keys) {
        List<KeyValue<String, InputStream>> entries = commands.mget(keys);

        List<KeyValue<String, AttachmentToken>> retval = new ArrayList<>(entries.size());
        for (KeyValue<String,InputStream> keyValue : entries) {
            retval.add(new KeyValue<String, AttachmentToken>() {

                @Override
                public String getKey() {
                    return keyValue.getKey();
                }

                @Override
                public AttachmentToken getValue() {
                    try {
                        return AttachmentTokenCodec.stream2Token(keyValue.getValue());
                    } catch (Exception e) {
                        throw new IllegalStateException("Failed to convert", e);
                    }
                }
            });
        }
        return retval;
    }

    @Override
    public Long mget(KeyValueStreamingChannel<String, AttachmentToken> channel, String... keys) {
        throw new UnsupportedOperationException("JsonRedisStringCommands.mget()");
    }

    @Override
    public String mset(Map<String, AttachmentToken> map) {
        try {
            Map<String, InputStream> m = LinkedHashMap.newLinkedHashMap(map.size());
            for (Map.Entry<String, AttachmentToken> e : map.entrySet()) {
                m.put(e.getKey(), AttachmentTokenCodec.token2Json(e.getValue()).getStream(false));
            }
            return commands.mset(m);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to convert", e);
        }
    }

    @Override
    public Boolean msetnx(Map<String, AttachmentToken> map) {
        try {
            Map<String, InputStream> m = LinkedHashMap.newLinkedHashMap(map.size());
            for (Map.Entry<String, AttachmentToken> e : map.entrySet()) {
                m.put(e.getKey(), AttachmentTokenCodec.token2Json(e.getValue()).getStream(false));
            }
            return commands.msetnx(m);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to convert", e);
        }
    }

    @Override
    public String set(String key, AttachmentToken value) {
        try {
            return commands.set(key, AttachmentTokenCodec.token2Json(value).getStream(false));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to convert", e);
        }
    }

    @Override
    public String set(String key, AttachmentToken value, SetArgs setArgs) {
        try {
            return commands.set(key, AttachmentTokenCodec.token2Json(value).getStream(false), setArgs);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to convert", e);
        }
    }

    @Override
    public AttachmentToken setGet(String key, AttachmentToken value) {
        try {
            return AttachmentTokenCodec.stream2Token(commands.setGet(key, AttachmentTokenCodec.token2Json(value).getStream(false)));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to convert", e);
        }
    }

    @Override
    public AttachmentToken setGet(String key, AttachmentToken value, SetArgs setArgs) {
        try {
            return AttachmentTokenCodec.stream2Token(commands.setGet(key, AttachmentTokenCodec.token2Json(value).getStream(false), setArgs));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to convert", e);
        }
    }

    @Override
    public Long setbit(String key, long offset, int value) {
        return commands.setbit(key, offset, value);
    }

    @Override
    public String setex(String key, long seconds, AttachmentToken value) {
        try {
            return commands.setex(key, seconds, AttachmentTokenCodec.token2Json(value).getStream(false));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to convert", e);
        }
    }

    @Override
    public String psetex(String key, long milliseconds, AttachmentToken value) {
        try {
            return commands.psetex(key, milliseconds, AttachmentTokenCodec.token2Json(value).getStream(false));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to convert", e);
        }
    }

    @Override
    public Boolean setnx(String key, AttachmentToken value) {
        try {
            return commands.setnx(key, AttachmentTokenCodec.token2Json(value).getStream(false));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to convert", e);
        }
    }

    @Override
    public Long setrange(String key, long offset, AttachmentToken value) {
        try {
            return commands.setrange(key, offset, AttachmentTokenCodec.token2Json(value).getStream(false));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to convert", e);
        }
    }

    @Override
    public StringMatchResult stralgoLcs(StrAlgoArgs strAlgoArgs) {
        return commands.stralgoLcs(strAlgoArgs);
    }

    @Override
    public Long strlen(String key) {
        return commands.strlen(key);
    }

}
