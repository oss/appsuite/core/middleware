/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.attachment.impl.commands;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import org.json.JSONObject;
import org.json.JSONServices;
import com.openexchange.java.Streams;
import com.openexchange.mail.attachment.AttachmentToken;
import com.openexchange.mail.attachment.AttachmentTokenConstants;

/**
 * {@link AttachmentTokenCodec} - Converts attachment tokens to JSON representation and vice versa.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class AttachmentTokenCodec {

    /**
     * Initializes a new {@link AttachmentTokenCodec}.
     */
    private AttachmentTokenCodec() {
        super();
    }

    /**
     * Gets the attachment token from given JSON stream.
     *
     * @param jsonData The JSON stream
     * @return The attachment token
     * @throws Exception If conversion fails
     */
    public static AttachmentToken stream2Token(InputStream jsonData) throws Exception {
        if (null == jsonData) {
            return null;
        }

        InputStreamReader reader = null;
        try {
            reader = new InputStreamReader(jsonData, StandardCharsets.UTF_8);
            return json2Token(JSONServices.parseObject(reader));
        } finally {
            Streams.close(reader, jsonData);
        }
    }

    /**
     * Gets the attachment token from given JSON representation.
     *
     * @param jToken The JSON representation to convert
     * @return The attachment token
     */
    public static AttachmentToken json2Token(JSONObject jToken) {
        if (jToken == null) {
            return null;
        }

        String id = jToken.optString("id", null);
        int contextId = jToken.optInt("contextId", 0);
        int userId = jToken.optInt("userId", 0);
        int accountId = jToken.optInt("accountId", 0);
        String mailId = jToken.optString("mailId", null);
        String attachmentId = jToken.optString("attachmentId", null);
        String folderPath = jToken.optString("folderPath", null);
        String sessionId = jToken.optString("sessionId", null);
        String clientIp = jToken.optString("clientIp", null);
        String client = jToken.optString("client", null);
        String userAgent = jToken.optString("userAgent", null);
        boolean oneTime = jToken.optBoolean("oneTime", false);
        boolean checkIp = jToken.optBoolean("checkIp", false);

        AttachmentToken token = new AttachmentToken(id, AttachmentTokenConstants.DEFAULT_TIMEOUT);
        token.setAccountId(accountId);
        token.setAttachmentId(attachmentId);
        token.setCheckIp(checkIp);
        token.setClient(client);
        token.setClientIp(clientIp);
        token.setContextId(contextId);
        token.setFolderPath(folderPath);
        token.setMailId(mailId);
        token.setOneTime(oneTime);
        token.setSessionId(sessionId);
        token.setUserAgent(userAgent);
        token.setUserId(userId);
        return token;
    }

    /**
     * Gets the JSON representation for given token.
     *
     * @param token The token to convert
     * @return The token's JSON representation
     * @throws Exception If conversion fails
     */
    public static JSONObject token2Json(AttachmentToken token) throws Exception {
        if (token == null) {
            return null;
        }

        JSONObject jToken = new JSONObject(16);
        jToken.putOpt("id", token.getId());
        jToken.putOpt("sessionId", token.getSessionId());
        jToken.put("contextId", token.getContextId());
        jToken.put("userId", token.getUserId());
        jToken.put("accountId", token.getAccountId());
        jToken.putOpt("client", token.getClient());
        jToken.putOpt("clientIp", token.getClientIp());
        jToken.putOpt("mailId", token.getMailId());
        jToken.putOpt("attachmentId", token.getAttachmentId());
        jToken.putOpt("folderPath", token.getFolderPath());
        jToken.putOpt("userAgent", token.getUserAgent());
        jToken.put("oneTime", token.isOneTime());
        jToken.put("checkIp", token.isCheckIp());
        return jToken;
    }

}
