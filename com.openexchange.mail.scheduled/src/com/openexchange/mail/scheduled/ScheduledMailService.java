/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.annotation.SingletonService;
import com.openexchange.session.Session;

/**
 * {@link ScheduledMailService} - The service for scheduled mails.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@SingletonService
public interface ScheduledMailService {

    /** The capability name to signal access to scheduled mail */
    public static final String CAPABILITY_SCHEDULED_MAIL = "scheduled_mail";

    /**
     * Checks if scheduled mail is available for session-associated user.
     *
     * @param session The session providing user information
     * @return <code>true</code> if available; otherwise <code>false</code> if denied
     * @throws OXException If availability check fails
     */
    boolean isAvailable(Session session) throws OXException;

    /**
     * Creates a scheduled mail from given description.
     *
     * @param optionalId The optional UUID to use or <code>null</code> to create a random one
     * @param scheduledMailDesc The scheduled mail description
     * @param session The session
     * @return The created scheduled mail
     * @throws OXException If scheduled mail cannot be created
     */
    ScheduledMail scheduleTransportFor(UUID optionalId, ScheduledMailDescription scheduledMailDesc, Session session) throws OXException;

    /**
     * Updates a scheduled mail from given description.
     *
     * @param id The scheduled mail identifier
     * @param scheduledMailDesc The scheduled mail description
     * @param session The session
     * @return The updated scheduled mail
     * @throws OXException If scheduled mail cannot be updated
     */
    ScheduledMail updateScheduledMail(UUID id, ScheduledMailDescription scheduledMailDesc, Session session) throws OXException;

    /**
     * Deletes the scheduled mail associated with given identifier.
     *
     * @param id The scheduled mail identifier
     * @param deleteAssociatedMail Whether to attempt deleting associated mail from mail storage
     * @param session The session providing user data
     * @return The deleted scheduled mail if successfully deleted; otherwise empty
     * @throws OXException If scheduled mail cannot be deleted
     */
    Optional<ScheduledMail> deleteScheduledMail(UUID id, boolean deleteAssociatedMail, Session session) throws OXException;

    /**
     * Deletes the scheduled mails associated with given identifiers belonging to session-associated user.
     *
     * @param ids The scheduled mail identifiers
     * @param deleteAssociatedMails Whether to attempt deleting associated mails from mail storage
     * @param session The session providing user data
     * @return The deleted scheduled mails if successfully deleted; otherwise <code>null</code>
     * @throws OXException If scheduled mail cannot be deleted
     */
    ScheduledMail[] deleteScheduledMails(Collection<UUID> ids, boolean deleteAssociatedMails, Session session) throws OXException;

    /**
     * Checks if such a scheduled mail associated with given identifier exists.
     *
     * @param id The scheduled mail identifier
     * @param session The session providing user data
     * @return <code>true</code> if such a scheduled mail exists; otherwise <code>false</code>
     * @throws OXException If existence check fails
     */
    boolean existsScheduledMail(UUID id, Session session) throws OXException;

    /**
     * Gets the scheduled mail associated with given identifier.
     *
     * @param id The scheduled mail identifier
     * @param session The session providing user data
     * @return The scheduled mail
     * @throws OXException If scheduled mail cannot be returned
     */
    ScheduledMail getScheduledMail(UUID id, Session session) throws OXException;

    /**
     * Gets the scheduled mail associated with given identifier.
     *
     * @param id The scheduled mail identifier
     * @param session The session providing user data
     * @return The scheduled mail or empty
     * @throws OXException If scheduled mail cannot be returned
     */
    Optional<ScheduledMail> optScheduledMail(UUID id, Session session) throws OXException;

    /**
     * Gets all available scheduled mails associated with given session.
     *
     * @param session The session
     * @return The scheduled mails
     * @throws OXException If scheduled mails cannot be returned
     */
    List<ScheduledMail> getScheduledMails(Session session) throws OXException;

}
