/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled;


/**
 * {@link ScheduledMailExceptionMessages} - Translatable exception messages for scheduled mail module.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class ScheduledMailExceptionMessages {

    /**
     * Initializes a new {@link ScheduledMailExceptionMessages}.
     */
    private ScheduledMailExceptionMessages() {
        super();
    }

    // If user hits limit on how many scheduled mails he/she is allowed to create
    public static final String TOO_MANY_SCHEDULED_MAILS_MSG = "You are only allowed to create %1$s scheduled mails at once.";

    // If user hits limit on how many scheduled mails he/she is allowed to scheduled for sending per hour
    public static final String TOO_MANY_SCHEDULED_MAILS_PER_HOUR_MSG = "You are only allowed to send %1$s scheduled mails per hour.";

    // User attempted to scheduled a mail for being sent for non-primary account
    public static final String ONLY_PRIMARY_ACCOUNT_MSG = "Mails are only allowed being scheduled for sending at a later time for primary account.";

}
