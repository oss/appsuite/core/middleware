/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled;

/**
 * {@link Schema} - Provides a representative context identifier for denoted database schema.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class Schema implements Comparable<Schema> {

    private final int representativeContextId;

    /**
     * Initializes a new {@link Schema}.
     *
     * @param representativeContextId The representative context identifier denoting database schema
     */
    public Schema(int representativeContextId) {
        super();
        this.representativeContextId = representativeContextId;
    }

    @Override
    public int hashCode() {
        return representativeContextId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Schema other = (Schema) obj;
        return representativeContextId == other.representativeContextId;
    }

    @Override
    public int compareTo(Schema o) {
        return Integer.compare(representativeContextId, o.representativeContextId);
    }

}
