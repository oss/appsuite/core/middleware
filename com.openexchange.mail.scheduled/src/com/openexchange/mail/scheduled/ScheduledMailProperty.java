/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled;

import static com.openexchange.java.Autoboxing.I;
import com.openexchange.config.lean.Property;

/**
 * {@link ScheduledMailProperty} - Property enumeration for scheduled mail feature.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public enum ScheduledMailProperty implements Property {

    /**
     * The switch to enable/disable scheduled mail feature.
     */
    ENABLED("enabled", Boolean.FALSE),
    /**
     * The max. allowed number of scheduled mails per user.
     */
    MAX_NUMBER_SCHEDULED_MAILS("maxNumberOfScheduledMails", I(1000)),
    /**
     * The max. allowed number of scheduled mails being sent per hour for a user.
     */
    MAX_NUMBER_SCHEDULED_MAILS_PER_HOUR("maxNumberOfScheduledMailsPerHour", I(100)),
    /**
     * The frequency in minutes when to check for due scheduled mails.
     */
    CHECK_FREQUENCY_MINUTES("checkFrequencyMinutes", I(30)),
    /**
     * The look-ahead in minutes specifies the extra time added to <i>now</i> when a scheduled mail is considered as due.
     */
    LOOK_AHEAD_MINUTES("lookAheadMinutes", I(35)),
    /**
     * The time in minutes when the lock marking a scheduled mail as "in processing" is considered as expired and thus may be newly
     * acquired by another process.
     */
    LOCK_EXPIRY_MINUTES("lockExpiryMinutes", I(5)),
    /**
     * The time in minutes when the lock marking a scheduled mail as "in processing" is refreshed by lock-holding process.
     */
    LOCK_REFRESH_MINUTES("lockRefreshMinutes", I(2)),

    ;

    private final Object defaultValue;
    private final String fqn;

    /**
     * Initializes a new {@link ScheduledMailProperty}.
     *
     * @param appendix The appendix for full-qualified name
     * @param defaultValue The default value
     */
    private ScheduledMailProperty(String appendix, Object defaultValue) {
        this.fqn = "com.openexchange.mail.scheduled." + appendix;
        this.defaultValue = defaultValue;
    }

    /**
     * Returns the fully qualified name for the property
     *
     * @return the fully qualified name for the property
     */
    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    /**
     * Returns the default value of this property
     *
     * @return the default value of this property
     */
    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }
}
