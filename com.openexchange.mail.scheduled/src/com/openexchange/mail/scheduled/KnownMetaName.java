/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.mail.scheduled;


/**
 * {@link KnownMetaName} - An enumeration of well-known names of meta information for a scheduled mail.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public enum KnownMetaName {

    /**
     * The meta name for login name; type is supposed to be <code>java.lang.String</code>
     */
    LOGIN("login"),
    /**
     * The meta name for password; type is supposed to be <code>java.lang.String</code>
     */
    PASSWORD("password"),
    /**
     * The meta name for OAuth access token; type is supposed to be <code>java.lang.String</code>
     */
    OAUTH_ACCESS_TOKEN("oauthAccessToken"),
    /**
     * The meta name for OAuth refresh token; type is supposed to be <code>java.lang.String</code>
     */
    OAUTH_REFRESH_TOKEN("oauthRefreshToken"),
    /**
     * The meta name for OAuth access token expiration date; type is supposed to be <code>java.lang.String</code>
     */
    OAUTH_ACCESS_TOKEN_EXPIRY_DATE("oauthAccessTokenExpiryDate"),
    /**
     * The meta name for request parameter; type is supposed to be <code>com.openexchange.mail.scheduled.Meta</code>
     */
    PARAMETERS("parameters"),
    /**
     * The meta name for remote address of requesting client; type is supposed to be <code>java.lang.String</code>
     */
    REMOTE_ADDRESS("remoteAddress"),
    /**
     * The meta name for IP address of requesting client; type is supposed to be <code>java.lang.String</code>
     */
    CLIENT_IP("clientIp"),
    ;

    private final String name;

    /**
     * Initializes a new {@link KnownMetaName}.
     *
     * @param name The name of the meta property
     */
    private KnownMetaName(String name) {
        this.name = name;
    }

    /**
     * Gets the name of the meta property.
     *
     * @return The name
     */
    public String getName() {
        return name;
    }

}
