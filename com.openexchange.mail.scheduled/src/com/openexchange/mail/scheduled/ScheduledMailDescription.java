/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled;

import java.util.Optional;

/**
 * {@link ScheduledMailDescription} - A description for a scheduled mail.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ScheduledMailDescription {

    /**
     * Generates a new instance of <code>ScheduledMailDescription</code> from given scheduled mail.
     *
     * @param scheduledMail The scheduled mail to initialize from
     * @return The new instance of <code>ScheduledMailDescription</code>
     */
    public static ScheduledMailDescription newInstanceFrom(ScheduledMail scheduledMail) {
        ScheduledMailDescription smd = new ScheduledMailDescription();
        if (scheduledMail != null) {
            smd.setDateToSend(scheduledMail.getDateToSend());
            smd.setMailPath(scheduledMail.getMailPath());
            Optional<Meta> meta = scheduledMail.getMeta();
            if (meta.isPresent()) {
                smd.setMeta(meta.get());
            }
        }
        return smd;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private String mailPath;
    private long dateToSend;
    private Meta meta;

    private boolean containsMailPath;
    private boolean containsDateToSend;
    private boolean containsMeta;

    /**
     * Initializes a new {@link ScheduledMailDescription}.
     */
    public ScheduledMailDescription() {
        super();
    }

    /**
     * Gets the mail path.
     *
     * @return The mail path
     */
    public String getMailPath() {
        return mailPath;
    }

    /**
     * Sets the mail path.
     *
     * @param mailPath The mail path
     */
    public void setMailPath(String mailPath) {
        this.mailPath = mailPath;
        containsMailPath = true;
    }

    /**
     * Checks if mail path has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsMailPath() {
        return containsMailPath;
    }

    /**
     * Removes the mail path.
     */
    public void removeMailPath() {
        mailPath = null;
        containsMailPath = false;
    }

    // ---------------------------------------------------------------------------------

    /**
     * Gets the time stamp when this scheduled mail is due for being sent, which is the number of milliseconds since January 1, 1970, 00:00:00 GMT.
     *
     * @return The date to send
     */
    public long getDateToSend() {
        return dateToSend;
    }

    /**
     * Sets the time stamp when this scheduled mail is due for being sent, which is the number of milliseconds since January 1, 1970, 00:00:00 GMT.
     *
     * @param dateToSend The date to send
     */
    public void setDateToSend(long dateToSend) {
        this.dateToSend = dateToSend;
        containsDateToSend = true;
    }

    /**
     * Checks if date to send has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsDateToSend() {
        return containsDateToSend;
    }

    /**
     * Removes the time stamp when this scheduled mail is due for being sent.
     */
    public void removeDateToSend() {
        dateToSend = 0;
        containsDateToSend = false;
    }

    // ---------------------------------------------------------------------------------

    /**
     * Gets the meta instance.
     *
     * @return The meta instance
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * Sets the meta instance for this scheduled mail.
     *
     * @param meta The meta instance
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
        containsMeta = true;
    }

    /**
     * Checks if meta instance has been set.
     *
     * @return <code>true</code> if set; otherwise <code>false</code>
     */
    public boolean containsMeta() {
        return containsMeta;
    }

    /**
     * Removes the meta instance.
     */
    public void removeMeta() {
        meta = null;
        containsMeta = false;
    }

}
