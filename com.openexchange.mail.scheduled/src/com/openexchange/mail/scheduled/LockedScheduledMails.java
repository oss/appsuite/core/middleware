/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled;

import static com.openexchange.java.Autoboxing.I;
import java.util.Collection;
import com.openexchange.java.util.UUIDs;

/**
 * {@link LockedScheduledMails} - Utility class for locked scheduled mails.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.14.0
 */
public final class LockedScheduledMails {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(LockedScheduledMails.class);

    /**
     * Initializes a new {@link LockedScheduledMails}.
     */
    private LockedScheduledMails() {
        super();
    }

    /**
     * Safely unlocks given locked scheduled mail instances.
     *
     * @param lockedScheduledMails The instances to unlock
     */
    public static void unlock(Collection<LockedScheduledMail> lockedScheduledMails) {
        if (lockedScheduledMails != null) {
            for (LockedScheduledMail lockedScheduledMail : lockedScheduledMails) {
                unlock(lockedScheduledMail);
            }
        }
    }

    /**
     * Safely unlocks given locked scheduled mail instance.
     *
     * @param lockedScheduledMail The instance to unlock
     */
    public static void unlock(LockedScheduledMail lockedScheduledMail) {
        if (lockedScheduledMail != null) {
            try {
                lockedScheduledMail.unlock();
            } catch (Exception e) {
                LOG.error("Failed to unlock scheduled mail {} of user {} in context {}", UUIDs.getUnformattedStringObjectFor(lockedScheduledMail.getId()), I(lockedScheduledMail.getUserId()), I(lockedScheduledMail.getContextId()), e);
            }
        }
    }

}
