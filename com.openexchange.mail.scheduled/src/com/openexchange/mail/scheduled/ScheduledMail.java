/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled;

import java.util.Optional;
import java.util.UUID;
import com.openexchange.session.IUserAndContext;

/**
 * {@link ScheduledMail} - A representation of a scheduled mail.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public interface ScheduledMail extends IUserAndContext {

    /**
     * Gets the identifier
     *
     * @return The identifier
     */
    UUID getId();

    /**
     * Gets the mail path for this scheduled mail.
     *
     * @return The mail path
     */
    String getMailPath();

    /**
     * Gets the time stamp when this scheduled mail is due for being sent, which is the number of milliseconds since January 1, 1970, 00:00:00 GMT.
     *
     * @return The date to send
     */
    long getDateToSend();

    /**
     * Gets arbitrary meta information of this scheduled mail.
     *
     * @return The meta information or empty
     */
    Optional<Meta> getMeta();
}
