/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled;

import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;
import com.openexchange.exception.OXExceptionStrings;

/**
 * {@link ScheduledMailExceptionCodes} - Enumeration of all {@link OXException}s known in Scheduled Mail module.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public enum ScheduledMailExceptionCodes implements DisplayableOXExceptionCode {

    /**
     * An error occurred: %1$s
     */
    UNEXPECTED_ERROR("An error occurred: %1$s", OXExceptionStrings.MESSAGE, CATEGORY_ERROR, 1),
    /**
     * An SQL error occurred: %1$s
     */
    SQL_ERROR("An SQL error occurred: %1$s", null, CATEGORY_ERROR, 2),
    /**
     * An I/O error occurred: %1$s
     */
    IO_ERROR("An I/O error occurred: %1$s", OXExceptionStrings.MESSAGE, CATEGORY_ERROR, 3),
    /**
     * Found no such scheduled mail for identifier: %1$s
     */
    NO_SUCH_SCHEDULED_MAIL("Found no such scheduled mail for identifier: %1$s", null, CATEGORY_ERROR, 4),
    /**
     * The mail path is not supported: %1$s
     */
    INVALID_MAIL_PATH("The mail path is not supported: %1$s", null, CATEGORY_ERROR, 5),
    /**
     * Too many scheduled mails. Max. allowed number of scheduled mails is %1$s.
     */
    TOO_MANY_SCHEDULED_MAILS("Too many scheduled mails. Max. allowed number of scheduled mails is %1$s.", ScheduledMailExceptionMessages.TOO_MANY_SCHEDULED_MAILS_MSG, CATEGORY_PERMISSION_DENIED, 6),
    /**
     * Too many scheduled mails per hour. Max. allowed number of scheduled mails per hour is %1$s.
     */
    TOO_MANY_SCHEDULED_MAILS_PER_HOUR("Too many scheduled mails per hour. Max. allowed number of scheduled mails per hour is %1$s.", ScheduledMailExceptionMessages.TOO_MANY_SCHEDULED_MAILS_PER_HOUR_MSG, CATEGORY_PERMISSION_DENIED, 7),
    /**
     * Mails are only allowed being scheduled for sending at a later time for primary account.
     */
    ONLY_PRIMARY_ACCOUNT("Mails are only allowed being scheduled for sending at a later time for primary account.", ScheduledMailExceptionMessages.ONLY_PRIMARY_ACCOUNT_MSG, CATEGORY_ERROR, 8),
    ;

    /**
     * The error code prefix for Scheduled Mail module.
     */
    public static final String PREFIX = "SCHEDLDMAIL";

    private final Category category;
    private final int detailNumber;
    private final String message;
    private final String displayMessage;

    private ScheduledMailExceptionCodes(final String message, final String displayMessage, final Category category, final int detailNumber) {
        this.message = message;
        this.displayMessage = displayMessage;
        this.detailNumber = detailNumber;
        this.category = category;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDisplayMessage() {
        return displayMessage;
    }

    @Override
    public int getNumber() {
        return detailNumber;
    }

    @Override
    public String getPrefix() {
        return PREFIX;
    }

    @Override
    public boolean equals(final OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Throwable cause, Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }

}
