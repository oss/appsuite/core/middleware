/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.annotation.SingletonService;
import com.openexchange.session.Session;
import com.openexchange.session.UserAndContext;

/**
 * {@link ScheduledMailStorageService} - The storage for scheduled mails.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@SingletonService
public interface ScheduledMailStorageService {

    /**
     * Acquires the processing lock for specified scheduled mail.
     *
     * @param id The scheduled mail identifier
     * @param session The session providing user data
     * @return The acquired locked scheduled mail or empty if already locked by another process
     * @throws OXException If acquiring lock fails
     */
    Optional<LockedScheduledMail> acquireProcessingLock(UUID id, Session session) throws OXException;

    /**
     * Checks if such a scheduled mail associated with given identifier exists.
     *
     * @param id The scheduled mail identifier
     * @param session The session providing user data
     * @return <code>true</code> if such a scheduled mail exists; otherwise <code>false</code>
     * @throws OXException If existence check fails
     */
    boolean existsScheduledMail(UUID id, Session session) throws OXException;

    /**
     * Gets the scheduled mail associated with given identifier.
     *
     * @param id The scheduled mail identifier
     * @param session The session providing user data
     * @return The scheduled mail
     * @throws OXException If scheduled mail cannot be returned
     */
    ScheduledMail getScheduledMail(UUID id, Session session) throws OXException;

    /**
     * Gets the scheduled mail associated with given identifier.
     *
     * @param id The scheduled mail identifier
     * @param session The session providing user data
     * @return The scheduled mail or empty
     * @throws OXException If scheduled mail cannot be returned
     */
    Optional<ScheduledMail> optScheduledMail(UUID id, Session session) throws OXException;

    /**
     * Gets all available scheduled mails associated with given session.
     *
     * @param session The session
     * @return The scheduled mails
     * @throws OXException If scheduled mails cannot be returned
     */
    List<ScheduledMail> getScheduledMails(Session session) throws OXException;

    /**
     * Gets all available scheduled mails grouped by database schema that are due for being sent.
     *
     * @param dueDate The due time stamp; the number of milliseconds since January 1, 1970, 00:00:00 GMT representing the due date
     * @return The scheduled mails due for being sent
     * @throws OXException If scheduled mails cannot be returned
     */
    Map<Schema, Map<UserAndContext, List<LockedScheduledMail>>> getDueScheduledMails(long dueDate) throws OXException;

    /**
     * Creates a scheduled mail from given description.
     *
     * @param optionalId The optional identifier to use or <code>null</code> to create a new one
     * @param scheduledMailDesc The scheduled mail description
     * @param maxNumberOfScheduledMails The max. number of scheduled mails allowed for session-associated user
     * @param maxNumberOfScheduledMailsPerHour The max. number of scheduled mails that are allowed being sent with one hour
     * @param session The session
     * @return The created scheduled mail
     * @throws OXException If scheduled mail cannot be created
     */
    ScheduledMail createScheduledMail(UUID optionalId, ScheduledMailDescription scheduledMailDesc, int maxNumberOfScheduledMails, int maxNumberOfScheduledMailsPerHour, Session session) throws OXException;

    /**
     * Updates a scheduled mail from given description.
     *
     * @param id The scheduled mail identifier
     * @param scheduledMailDesc The scheduled mail description
     * @param maxNumberOfScheduledMailsPerHour The max. number of scheduled mails that are allowed being sent with one hour (only considered if date-to-send is modified)
     * @param session The session
     * @return The updated scheduled mail
     * @throws OXException If scheduled mail cannot be updated
     */
    ScheduledMail updateScheduledMail(UUID id, ScheduledMailDescription scheduledMailDesc, int maxNumberOfScheduledMailsPerHour, Session session) throws OXException;

    /**
     * Deletes the scheduled mail associated with given identifier.
     *
     * @param id The scheduled mail identifier
     * @param session The session providing user data
     * @return The deleted scheduled mail if successfully deleted; otherwise empty
     * @throws OXException If scheduled mail cannot be deleted
     */
    Optional<ScheduledMail> deleteScheduledMail(UUID id, Session session) throws OXException;

    /**
     * Deletes the scheduled mails associated with given identifiers.
     *
     * @param ids The scheduled mail identifiers
     * @param session The session providing user data
     * @return The deleted scheduled mails if successfully deleted; otherwise <code>null</code>
     * @throws OXException If scheduled mail cannot be deleted
     */
    ScheduledMail[] deleteScheduledMails(Collection<UUID> ids, Session session) throws OXException;

    /**
     * Deletes the scheduled mail associated with given identifier.
     *
     * @param id The scheduled mail identifier
     * @param contextId The context identifier referencing target schema
     * @return <code>true</code> if successfully deleted; otherwise <code>false</code>
     * @throws OXException If scheduled mail cannot be deleted
     */
    default boolean deleteScheduledMail(UUID id, int contextId) throws OXException {
        return deleteScheduledMails(Collections.singletonList(id), contextId);
    }

    /**
     * Deletes the scheduled mails associated with given identifiers.
     *
     * @param ids The scheduled mail identifiers
     * @param contextId The context identifier referencing target schema
     * @return <code>true</code> if successfully deleted; otherwise <code>false</code>
     * @throws OXException If scheduled mails cannot be deleted
     */
    boolean deleteScheduledMails(Collection<UUID> ids, int contextId) throws OXException;

}
