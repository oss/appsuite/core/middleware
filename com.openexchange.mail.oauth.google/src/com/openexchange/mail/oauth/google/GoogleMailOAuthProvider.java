/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.oauth.google;

import static com.openexchange.google.api.client.GoogleApiClients.REFRESH_THRESHOLD;
import static com.openexchange.java.Autoboxing.I;
import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.Gmail.Builder;
import com.google.api.services.gmail.model.Profile;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.exception.OXException;
import com.openexchange.google.api.client.GoogleApiClients;
import com.openexchange.mail.autoconfig.Autoconfig;
import com.openexchange.mail.autoconfig.ImmutableAutoconfig;
import com.openexchange.mail.oauth.DefaultTokenInfo;
import com.openexchange.mail.oauth.MailOAuthProvider;
import com.openexchange.mail.oauth.TokenInfo;
import com.openexchange.oauth.KnownApi;
import com.openexchange.oauth.OAuthAccount;
import com.openexchange.oauth.OAuthExceptionCodes;
import com.openexchange.session.Session;

/**
 * {@link GoogleMailOAuthProvider}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.3
 */
public class GoogleMailOAuthProvider implements MailOAuthProvider {

    /**
     * Initializes a new {@link GoogleMailOAuthProvider}.
     */
    public GoogleMailOAuthProvider() {
        super();
    }

    @Override
    public Autoconfig getAutoconfigFor(OAuthAccount oauthAccount, Session session) throws OXException {
        try {
            // Ensure not expired
            OAuthAccount oauthAccountToUse = oauthAccount;
            OAuthAccount newAccount = GoogleApiClients.ensureNonExpiredGoogleAccount(oauthAccountToUse, session);
            if (null != newAccount) {
                oauthAccountToUse = newAccount;
            }

            // Determine E-Mail address from "user/me" end-point
            GoogleCredential credentials = com.openexchange.google.api.client.GoogleApiClients.getCredentials(oauthAccountToUse, session);
            Gmail gmail = new Builder(credentials.getTransport(), credentials.getJsonFactory(), credentials).setApplicationName(GoogleApiClients.getGoogleProductName(session)).build();
            Profile profile = gmail.users().getProfile("me").execute();
            String email = profile.getEmailAddress();

            ImmutableAutoconfig.Builder builder = ImmutableAutoconfig.builder();
            builder.username(email);
            builder.mailOAuthId(oauthAccount.getId()).mailPort(I(993)).mailProtocol("imap").mailSecure(Boolean.TRUE).mailServer("imap.gmail.com").mailStartTls(false);
            builder.transportOAuthId(oauthAccount.getId()).transportPort(I(-1)).transportProtocol("gmailsend").transportSecure(Boolean.FALSE).transportServer("www.googleapis.com");
            return builder.build();
        } catch (IOException e) {
            throw OAuthExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    /** By default the google access token has the expiry time of about 3600 seconds. */
    private static final Cache<Key, Stamp> GOOGLE_EXPIRY_CACHE = CacheBuilder.newBuilder().expireAfterWrite(Duration.ofSeconds(3600)).build();

    @Override
    public TokenInfo getTokenFor(OAuthAccount oauthAccount, Session session) throws OXException {
        // Ensure not expired
        Key key = new Key(oauthAccount.getId(), session.getUserId(), session.getContextId());

        // Query cache value
        Stamp stamp = GOOGLE_EXPIRY_CACHE.getIfPresent(key);
        if (stamp != null) {
            if ((stamp.timeMillis - System.currentTimeMillis()) >= REFRESH_THRESHOLD) {
                // More than 1 minute to go
                return DefaultTokenInfo.newXOAUTH2TokenInfoFor(oauthAccount.getToken());
            }

            // Stamp expired...
            synchronized (stamp) {
                if (GOOGLE_EXPIRY_CACHE.getIfPresent(key) != stamp) {
                    // Another thread dropped or put a new stamp. Retry...
                    return getTokenFor(oauthAccount, session);
                }

                // No intermediate modification
                Future<Integer> future = GoogleApiClients.submitGetExpiryForGoogleAccount(oauthAccount, session);
                int expirySeconds;
                try {
                    expirySeconds = future.get(10, TimeUnit.SECONDS).intValue();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    throw OXException.general("Interrupted while acquiring Google OAuth token for account " + oauthAccount.getId() + " (" + oauthAccount.getDisplayName() + ") of user " + session.getUserId() + " in context " + session.getContextId(), e);
                } catch (ExecutionException e) {
                    Throwable cause = e.getCause();
                    if (cause instanceof OXException) {
                        throw (OXException) cause;
                    }
                    throw OXException.general("Failed to acquire Google OAuth token for account " + oauthAccount.getId() + " (" + oauthAccount.getDisplayName() + ") of user " + session.getUserId() + " in context " + session.getContextId(), cause == null ? e : cause);
                } catch (TimeoutException e) {
                    // Timed out...
                    throw OXException.general("Timed out while acquiring Google OAuth token for account " + oauthAccount.getId() + " (" + oauthAccount.getDisplayName() + ") of user " + session.getUserId() + " in context " + session.getContextId(), e);
                }

                // Check if expiration holds long enough for still using the account
                if (expirySeconds >= REFRESH_THRESHOLD) {
                    // More than 1 minute to go
                    GOOGLE_EXPIRY_CACHE.put(key, new Stamp(System.currentTimeMillis() + (expirySeconds * 1000)));
                    return DefaultTokenInfo.newXOAUTH2TokenInfoFor(oauthAccount.getToken());
                }

                // Drop cache entry
                GOOGLE_EXPIRY_CACHE.invalidate(key);
            }
        } else {
            // Not cached... Query Google API
            int expirySeconds = GoogleApiClients.getExpiryForGoogleAccount(oauthAccount, session);

            // Check if expiration holds long enough for still using the account
            if (expirySeconds >= REFRESH_THRESHOLD) {
                // More than 1 minute to go
                GOOGLE_EXPIRY_CACHE.put(key, new Stamp(System.currentTimeMillis() + (expirySeconds * 1000)));
                return DefaultTokenInfo.newXOAUTH2TokenInfoFor(oauthAccount.getToken());
            }
        }

        // Expired...
        OAuthAccount oauthAccountToUse = oauthAccount;
        OAuthAccount newAccount = GoogleApiClients.ensureNonExpiredGoogleAccount(oauthAccountToUse, session);
        if (null != newAccount) {
            oauthAccountToUse = newAccount;
        }
        return DefaultTokenInfo.newXOAUTH2TokenInfoFor(oauthAccountToUse.getToken());
    }

    @Override
    public String getProviderId() {
        return KnownApi.GOOGLE.getServiceId();
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static class Key {

        private final int accountId;
        private final int userId;
        private final int contextId;
        private final int hash;

        Key(int accountId, int userId, int contextId) {
            super();
            this.accountId = accountId;
            this.userId = userId;
            this.contextId = contextId;

            int prime = 31;
            int result = 1;
            result = prime * result + accountId;
            result = prime * result + contextId;
            result = prime * result + userId;
            this.hash = result;
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Key)) {
                return false;
            }
            Key other = (Key) obj;
            if (accountId != other.accountId) {
                return false;
            }
            if (contextId != other.contextId) {
                return false;
            }
            if (userId != other.userId) {
                return false;
            }
            return true;
        }
    }

    private static class Stamp {

        final long timeMillis;

        Stamp(long timeMillis) {
            super();
            this.timeMillis = timeMillis;
        }
    }

}
