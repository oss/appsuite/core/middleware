/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.client.onboarding;

import com.google.common.collect.Maps;
import java.util.EnumMap;
import java.util.Map;

/**
 * {@link ConcreteClientDevice} - A client device backed by a concrete device.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.4
 */
public final class ConcreteClientDevice implements ClientDevice {

    private static final Map<Device, ConcreteClientDevice> UNIVERSE = Maps.immutableEnumMap(createUniverseEntries());

    private static Map<Device, ConcreteClientDevice> createUniverseEntries() {
        Device[] devices = Device.values();
        Map<Device, ConcreteClientDevice> m = new EnumMap<>(Device.class);
        for (Device device : devices) {
            m.put(device, new ConcreteClientDevice(device));
        }
        return m;
    }

    /**
     * Gets the concrete client device for specified device constant.
     *
     * @param device The device
     * @return The concrete client device or <code>null</code>
     */
    public static ConcreteClientDevice concreteClientDeviceFor(Device device) {
        return null == device ? null : UNIVERSE.get(device);
    }

    // ---------------------------------------------------------------------------

    private final Device device;

    /**
     * Initializes a new {@link ConcreteClientDevice}.
     */
    private ConcreteClientDevice(Device device) {
        super();
        this.device = device;
    }

    @Override
    public boolean implies(Device device) {
        return this.device == device;
    }

    @Override
    public boolean matches(Device device) {
        return this.device == device;
    }

}
