/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.undo;

import java.util.Map;

/**
 * {@link DefaultUndoOperation}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class DefaultUndoOperation implements UndoOperation {

    /**
     * Creates a new builder for an instance of <code>DefaultUndoOperation</code>.
     *
     * @return The newly created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /** The builder for an instance of <code>DefaultUndoOperation</code> */
    public static final class Builder {

        private String module;
        private String action;
        private Map<String, String> parameters;
        private Object data;

        /**
         * Initializes a new instance of {@link DefaultUndoOperation.Builder}.
         */
        private Builder() {
            super();
        }

        /**
         * Sets the module.
         *
         * @param module The module to set
         */
        public Builder withModule(String module) {
            this.module = module;
            return this;
        }

        /**
         * Sets the action.
         *
         * @param action The action to set
         */
        public Builder withAction(String action) {
            this.action = action;
            return this;
        }

        /**
         * Sets the parameters.
         *
         * @param parameters The parameters to set
         */
        public Builder withParameters(Map<String, String> parameters) {
            this.parameters = parameters;
            return this;
        }

        /**
         * Sets the data.
         *
         * @param data The data to set
         */
        public Builder withData(Object data) {
            this.data = data;
            return this;
        }

        /**
         * Builds the resulting instance of <code>DefaultUndoOperation</code> from this builder's arguments.
         *
         * @return The resulting instance of <code>DefaultUndoOperation</code>
         */
        public DefaultUndoOperation build() {
            return new DefaultUndoOperation(module, action, parameters, data);
        }

    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final String module;
    private final String action;
    private final Map<String, String> parameters;
    private final Object data;

    /**
     * Initializes a new instance of {@link DefaultUndoOperation}.
     *
     * @param module The module identifier
     * @param action The action identifier
     * @param parameters The request parameters
     * @param data The request's data
     */
    public DefaultUndoOperation(String module, String action, Map<String, String> parameters, Object data) {
        super();
        this.module = module;
        this.action = action;
        this.parameters = parameters == null ? null : Map.copyOf(parameters);
        this.data = data;
    }

    @Override
    public String getModule() {
        return module;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public Map<String, String> getParameters() {
        return parameters;
    }

    @Override
    public Object getData() {
        return data;
    }

}
