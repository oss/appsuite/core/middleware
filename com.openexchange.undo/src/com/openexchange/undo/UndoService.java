/*
 0000* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.undo;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.annotation.SingletonService;
import com.openexchange.session.Session;

/**
 * {@link UndoService} - The undo service.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
@SingletonService
public interface UndoService {

    /**
     * Checks if undo service is enabled for session-associated user.
     *
     * @param session The session providing user information
     * @return <code>true</code> if enabled; otherwise <code>false</code>
     * @throws OXException If check fails
     */
    boolean isEnabledFor(Session session) throws OXException;

    /**
     * Gets the listing of all available undo operation(s) for given session.
     *
     * @param session The session providing user information
     * @return The listing of all available undo operation(s) or empty collection if there is no undo operation for session-associated user
     * @throws OXException If undo operation cannot be returned for any reason
     */
    List<UndoOperations> listUndoOperations(Session session) throws OXException;

    /**
     * Gets the undo operation(s) for given undo token and session.
     *
     * @param undoToken The undo token identifying the undo operation
     * @param session The session providing user information
     * @return The undo operation(s) or empty if there is no such undo operation
     * @throws OXException If undo operation cannot be returned for any reason
     */
    Optional<UndoOperations> getUndoOperations(UndoToken undoToken, Session session) throws OXException;

    /**
     * Gets (& removes) the undo operation(s) for given undo token and session.
     *
     * @param undoToken The undo token identifying the undo operation
     * @param session The session providing user information
     * @return The removed undo operation(s) or empty if there is no such undo operation
     * @throws OXException If undo operation cannot be returned for any reason
     */
    Optional<UndoOperations> removeUndoOperations(UndoToken undoToken, Session session) throws OXException;

    /**
     * Restores the undo operation(s) for given undo token and session.
     *
     * @param undoToken The undo token identifying the undo operation
     * @param undoOperations The undo operations to restore
     * @param session The session providing user information
     * @throws OXException If restore operation fails
     */
    void restoreUndoOperations(UndoToken undoToken, UndoOperations undoOperations, Session session) throws OXException;

    /**
     * Registers given undo operation using specified session.
     *
     * @param undoOperation The undo operation to register
     * @param timeToLiveMillis The time-to-live in milliseconds
     * @param session The session providing user information
     * @return The undo token referencing the given undo operation
     * @throws OXException If registration fails
     */
    default UndoToken registerUndoOperation(UndoOperation undoOperation, long timeToLiveMillis, Session session) throws OXException {
        return registerUndoOperations(Collections.singletonList(undoOperation), timeToLiveMillis, session);
    }

    /**
     * Registers given undo operations using specified session.
     *
     * @param undoOperations The undo operations to register
     * @param timeToLiveMillis The time-to-live in milliseconds
     * @param session The session providing user information
     * @return The undo token referencing the given undo operations
     * @throws OXException If registration fails
     */
    UndoToken registerUndoOperations(List<UndoOperation> undoOperations, long timeToLiveMillis, Session session) throws OXException;

    /**
     * Unregisters the undo operation for given undo token and session.
     *
     * @param undoToken The undo token identifying the undo operation
     * @param session The session providing user information
     * @return <code>true</code> if such a undo operation has been unregistered; otherwise <code>false</code> if there was no such undo operation
     * @throws OXException If undo operation cannot be unregistered for any reason
     */
    boolean unregisterUndoOperations(UndoToken undoToken, Session session) throws OXException;

}
