/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.undo;

import java.util.Map;

/**
 * {@link UndoOperation} - Represents an undo operation that effectively renders a previously performed operation undone.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public interface UndoOperation {

    /**
     * Gets the module identifier to be used for the artificial request.
     *
     * @return The module identifier
     */
    String getModule();

    /**
     * Gets the action identifier to be used for the artificial request.
     *
     * @return The action identifier
     */
    String getAction();

    /**
     * Gets the request parameters to be used for the artificial request.
     *
     * @return The request parameters
     */
    Map<String, String> getParameters();

    /**
     * Gets the request data to be used for the artificial request.
     *
     * @return The request data
     */
    Object getData();

}
