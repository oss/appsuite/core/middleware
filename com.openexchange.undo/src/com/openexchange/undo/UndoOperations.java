/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.undo;

import java.util.List;

/**
 * {@link UndoOperations} - The result when querying for undo operations with a certain undo token.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public interface UndoOperations extends Iterable<UndoOperation> {

    /**
     * Gets the undo token referencing this collection of undo operations.
     *
     * @return The undo token
     */
    UndoToken getUndoToken();

    /**
     * Gets the undo operations.
     *
     * @return The undo operations
     */
    List<UndoOperation> getOperations();

    /**
     * Gets the remaining time-to-live in milliseconds in the time of retrieval.
     *
     * @return The remaining time-to-live in milliseconds
     */
    long getTimeToLiveMillis();

    /**
     * Checks if the list of undo operations is empty.
     *
     * @return <code>true</code> if empty; otherwise <code>false</code>
     */
    boolean isEmpty();

    /**
     * Gets the number of undo operations contained in this collection.
     *
     * @return The number of undo operations
     */
    int size();

}
