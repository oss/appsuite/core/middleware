/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.recurrence;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import com.openexchange.chronos.Event;
import com.openexchange.time.TimeTools;

/**
 * {@link OccurrencesTest}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 * @since v7.10.0
 */
public class OccurrencesTest extends AbstractSingleTimeZoneTest {

    @ParameterizedTest
    @MethodSource("timeZones")
    public void noLimits(String timeZone) throws Exception {
        super.timeZone = timeZone;
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        setStartAndEndDates(master, "01.10.2008 14:45:00", "01.10.2008 15:45:00", false, tz);

        Iterator<Event> instances = service.calculateInstances(master, null, null, null);
        int count = 0;
        outer: while (instances.hasNext()) {
            Event instance = instances.next();
            switch (++count) {
                case 1:
                    compareInstanceWithMaster(master, instance, TimeTools.D("01.10.2008 14:45:00", tz), TimeTools.D("01.10.2008 15:45:00", tz));
                    break;
                case 2:
                    compareInstanceWithMaster(master, instance, TimeTools.D("02.10.2008 14:45:00", tz), TimeTools.D("02.10.2008 15:45:00", tz));
                    break;
                case 3:
                    compareInstanceWithMaster(master, instance, TimeTools.D("03.10.2008 14:45:00", tz), TimeTools.D("03.10.2008 15:45:00", tz));
                    break;
                case 4:
                    compareInstanceWithMaster(master, instance, TimeTools.D("04.10.2008 14:45:00", tz), TimeTools.D("04.10.2008 15:45:00", tz));
                    break;
                case 5:
                    compareInstanceWithMaster(master, instance, TimeTools.D("05.10.2008 14:45:00", tz), TimeTools.D("05.10.2008 15:45:00", tz));
                    break outer;
                default:
                    fail("Too many instances.");
                    break;
            }
        }
        assertEquals(5, count, "Missing instance.");
    }

    @ParameterizedTest
    @MethodSource("timeZones")
    public void limit(String timeZone) throws Exception {
        super.timeZone = timeZone;
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        setStartAndEndDates(master, "01.10.2008 14:45:00", "01.10.2008 15:45:00", false, tz);

        Iterator<Event> instances = service.calculateInstances(master, null, null, I(3));
        int count = 0;
        while (instances.hasNext()) {
            Event instance = instances.next();
            switch (++count) {
                case 1:
                    compareInstanceWithMaster(master, instance, TimeTools.D("01.10.2008 14:45:00", tz), TimeTools.D("01.10.2008 15:45:00", tz));
                    break;
                case 2:
                    compareInstanceWithMaster(master, instance, TimeTools.D("02.10.2008 14:45:00", tz), TimeTools.D("02.10.2008 15:45:00", tz));
                    break;
                case 3:
                    compareInstanceWithMaster(master, instance, TimeTools.D("03.10.2008 14:45:00", tz), TimeTools.D("03.10.2008 15:45:00", tz));
                    break;
                default:
                    fail("Too many instances.");
                    break;
            }
        }
        assertEquals(3, count, "Missing instance.");
    }

    public static Stream<Arguments> data() {
        return Stream.of(TimeZone.getAvailableIDs())
                .flatMap(timeZone -> Stream.of(
                        Arguments.of(timeZone, "leftBoundary", "02.10.2008 14:00:00", false),
                        Arguments.of(timeZone, "leftBoundary", "02.10.2008 14:45:00", false),
                        Arguments.of(timeZone, "leftBoundary", "02.10.2008 15:00:00", false),
                        Arguments.of(timeZone, "leftBoundary", "02.10.2008 15:45:00", true),
                        Arguments.of(timeZone, "leftBoundaryWithLimit", "02.10.2008 14:00:00", false),
                        Arguments.of(timeZone, "leftBoundaryWithLimit", "02.10.2008 14:45:00", false),
                        Arguments.of(timeZone, "leftBoundaryWithLimit", "02.10.2008 15:00:00", false),
                        Arguments.of(timeZone, "leftBoundaryWithLimit", "02.10.2008 15:45:00", true),
                        Arguments.of(timeZone, "rightBoundary", "05.10.2008 14:45:00", true),
                        Arguments.of(timeZone, "rightBoundary", "05.10.2008 15:00:00", false),
                        Arguments.of(timeZone, "rightBoundary", "05.10.2008 15:45:00", false),
                        Arguments.of(timeZone, "rightBoundary", "05.10.2008 16:45:00", false),
                        Arguments.of(timeZone, "rightBoundaryWithLimit", "05.10.2008 14:45:00", true),
                        Arguments.of(timeZone, "rightBoundaryWithLimit", "05.10.2008 15:00:00", true),
                        Arguments.of(timeZone, "rightBoundaryWithLimit", "05.10.2008 15:45:00", true),
                        Arguments.of(timeZone, "rightBoundaryWithLimit", "05.10.2008 16:45:00", true)
                ));
    }

    @ParameterizedTest
    @MethodSource("data")
    public void testBoundary(String timeZone, String methodName, String timestamp, boolean expectedResult) throws Exception {
        super.timeZone = timeZone;
        switch (methodName) {
            case "leftBoundary":
                leftBoundary(timestamp, expectedResult);
                break;
            case "leftBoundaryWithLimit":
                leftBoundaryWithLimit(timestamp, expectedResult);
                break;
            case "rightBoundary":
                rightBoundary(timestamp, expectedResult);
                break;
            case "rightBoundaryWithLimit":
                rightBoundaryWithLimit(timestamp);
                break;
            default:
                throw new IllegalArgumentException("Invalid method name: " + methodName);
        }
    }

    @ParameterizedTest
    @MethodSource("timeZones")
    public void leftAndRightBoundary(String timeZone) throws Exception {
        super.timeZone = timeZone;
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        setStartAndEndDates(master, "01.10.2008 14:45:00", "01.10.2008 15:45:00", false, tz);

        Iterator<Event> instances = service.calculateInstances(master, getCal("03.10.2008 14:00:00"), getCal("07.10.2008 17:00:00"), null);
        int count = 0;
        outer: while (instances.hasNext()) {
            Event instance = instances.next();
            switch (++count) {
                case 1:
                    compareInstanceWithMaster(master, instance, TimeTools.D("03.10.2008 14:45:00", tz), TimeTools.D("03.10.2008 15:45:00", tz));
                    break;
                case 2:
                    compareInstanceWithMaster(master, instance, TimeTools.D("04.10.2008 14:45:00", tz), TimeTools.D("04.10.2008 15:45:00", tz));
                    break;
                case 3:
                    compareInstanceWithMaster(master, instance, TimeTools.D("05.10.2008 14:45:00", tz), TimeTools.D("05.10.2008 15:45:00", tz));
                    break;
                case 4:
                    compareInstanceWithMaster(master, instance, TimeTools.D("06.10.2008 14:45:00", tz), TimeTools.D("06.10.2008 15:45:00", tz));
                    break;
                case 5:
                    compareInstanceWithMaster(master, instance, TimeTools.D("07.10.2008 14:45:00", tz), TimeTools.D("07.10.2008 15:45:00", tz));
                    break outer;
                default:
                    fail("Too many instances.");
                    break;
            }
        }
        assertEquals(5, count, "Missing instance.");
    }

    @ParameterizedTest
    @MethodSource("timeZones")
    public void leftAndRightBoundaryAndLimit(String timeZone) throws Exception {
        super.timeZone = timeZone;
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        setStartAndEndDates(master, "01.10.2008 14:45:00", "01.10.2008 15:45:00", false, tz);

        Iterator<Event> instances = service.calculateInstances(master, getCal("03.10.2008 14:00:00"), getCal("07.10.2008 17:00:00"), I(3));
        int count = 0;
        outer: while (instances.hasNext()) {
            Event instance = instances.next();
            switch (++count) {
                case 1:
                    compareInstanceWithMaster(master, instance, TimeTools.D("03.10.2008 14:45:00", tz), TimeTools.D("03.10.2008 15:45:00", tz));
                    break;
                case 2:
                    compareInstanceWithMaster(master, instance, TimeTools.D("04.10.2008 14:45:00", tz), TimeTools.D("04.10.2008 15:45:00", tz));
                    break;
                case 3:
                    compareInstanceWithMaster(master, instance, TimeTools.D("05.10.2008 14:45:00", tz), TimeTools.D("05.10.2008 15:45:00", tz));
                    break outer;
                default:
                    fail("Too many instances.");
                    break;
            }
        }
        assertEquals(3, count, "Missing instance.");
    }

    private void leftBoundary(String leftBoundary, boolean atEnd) throws Exception {
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        setStartAndEndDates(master, "01.10.2008 14:45:00", "01.10.2008 15:45:00", false, tz);

        Iterator<Event> instances = service.calculateInstances(master, getCal(leftBoundary), null, null);
        int count = atEnd ? 1 : 0;
        outer: while (instances.hasNext()) {
            Event instance = instances.next();
            switch (++count) {
                case 1:
                    compareInstanceWithMaster(master, instance, TimeTools.D("02.10.2008 14:45:00", tz), TimeTools.D("02.10.2008 15:45:00", tz));
                    break;
                case 2:
                    compareInstanceWithMaster(master, instance, TimeTools.D("03.10.2008 14:45:00", tz), TimeTools.D("03.10.2008 15:45:00", tz));
                    break;
                case 3:
                    compareInstanceWithMaster(master, instance, TimeTools.D("04.10.2008 14:45:00", tz), TimeTools.D("04.10.2008 15:45:00", tz));
                    break;
                case 4:
                    compareInstanceWithMaster(master, instance, TimeTools.D("05.10.2008 14:45:00", tz), TimeTools.D("05.10.2008 15:45:00", tz));
                    break;
                case 5:
                    compareInstanceWithMaster(master, instance, TimeTools.D("06.10.2008 14:45:00", tz), TimeTools.D("06.10.2008 15:45:00", tz));
                    break outer;
                default:
                    fail("Too many instances.");
                    break;
            }
        }
        assertEquals(5, count, "Missing instance.");
    }

    private void leftBoundaryWithLimit(String leftBoundary, boolean atEnd) throws Exception {
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        setStartAndEndDates(master, "01.10.2008 14:45:00", "01.10.2008 15:45:00", false, tz);

        Iterator<Event> instances = service.calculateInstances(master, getCal(leftBoundary), null, I(1));
        int count = 0;
        while (instances.hasNext()) {
            Event instance = instances.next();
            switch (++count) {
                case 1:
                    if (atEnd) {
                        compareInstanceWithMaster(master, instance, TimeTools.D("03.10.2008 14:45:00", tz), TimeTools.D("03.10.2008 15:45:00", tz));
                    } else {
                        compareInstanceWithMaster(master, instance, TimeTools.D("02.10.2008 14:45:00", tz), TimeTools.D("02.10.2008 15:45:00", tz));
                    }
                    break;
                default:
                    fail("Too many instances.");
                    break;
            }
        }
        assertEquals(1, count, "Missing instance.");
    }

    private void rightBoundary(String rightBoundary, boolean atStart) throws Exception {
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        setStartAndEndDates(master, "01.10.2008 14:45:00", "01.10.2008 15:45:00", false, tz);

        Iterator<Event> instances = service.calculateInstances(master, null, getCal(rightBoundary), null);
        int count = 0;
        while (instances.hasNext()) {
            Event instance = instances.next();
            switch (++count) {
                case 1:
                    compareInstanceWithMaster(master, instance, TimeTools.D("01.10.2008 14:45:00", tz), TimeTools.D("01.10.2008 15:45:00", tz));
                    break;
                case 2:
                    compareInstanceWithMaster(master, instance, TimeTools.D("02.10.2008 14:45:00", tz), TimeTools.D("02.10.2008 15:45:00", tz));
                    break;
                case 3:
                    compareInstanceWithMaster(master, instance, TimeTools.D("03.10.2008 14:45:00", tz), TimeTools.D("03.10.2008 15:45:00", tz));
                    break;
                case 4:
                    compareInstanceWithMaster(master, instance, TimeTools.D("04.10.2008 14:45:00", tz), TimeTools.D("04.10.2008 15:45:00", tz));
                    break;
                case 5:
                    if (atStart) {
                        fail("Too many instances.");
                    } else {
                        compareInstanceWithMaster(master, instance, TimeTools.D("05.10.2008 14:45:00", tz), TimeTools.D("05.10.2008 15:45:00", tz));
                    }
                    break;
                default:
                    fail("Too many instances.");
                    break;
            }
        }
        assertEquals(atStart ? 4 : 5, count, "Missing instance.");
    }

    private void rightBoundaryWithLimit(String rightBoundary) throws Exception {
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        setStartAndEndDates(master, "01.10.2008 14:45:00", "01.10.2008 15:45:00", false, tz);

        Iterator<Event> instances = service.calculateInstances(master, null, getCal(rightBoundary), I(1));
        int count = 0;
        while (instances.hasNext()) {
            Event instance = instances.next();
            switch (++count) {
                case 1:
                    compareInstanceWithMaster(master, instance, TimeTools.D("01.10.2008 14:45:00", tz), TimeTools.D("01.10.2008 15:45:00", tz));
                    break;
                default:
                    fail("Too many instances.");
                    break;
            }
        }
        assertEquals(1, count, "Missing instance.");
    }

}
