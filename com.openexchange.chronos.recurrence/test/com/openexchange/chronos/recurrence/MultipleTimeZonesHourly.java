/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.recurrence;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import com.openexchange.chronos.Event;

/**
 * {@link MultipleTimeZonesHourly}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 * @since v7.10.0
 */
public class MultipleTimeZonesHourly extends RecurrenceServiceTest {

    private static final int YEAR = 2016;
    protected String startTimeZone;
    protected String endTimeZone;

    public static Stream<Arguments> timeZonesWithHours() {
        return Stream.of(
            Arguments.of("Europe/Berlin", "UTC"),
            Arguments.of("UTC", "Europe/Berlin"),
            Arguments.of("America/New_York", "UTC"),
            Arguments.of("UTC", "America/New_York"),
            Arguments.of("America/New_York", "Europe/Berlin"),
            Arguments.of("Europe/Berlin", "America/New_York")
        )
        .flatMap(pair ->
            Stream.iterate(0, hour -> hour + 1)
                  .limit(24)
                  .map(hour -> Arguments.of(pair.get()[0], pair.get()[1], hour))
        );
    }

    public static Stream<Arguments> timeZones() {
        return Stream.of(
            Arguments.of("Europe/Berlin", "UTC"),
            Arguments.of("UTC", "Europe/Berlin"),
            Arguments.of("America/New_York", "UTC"),
            Arguments.of("UTC", "America/New_York"),
            Arguments.of("America/New_York", "Europe/Berlin"),
            Arguments.of("Europe/Berlin", "America/New_York")
        );
    }

    @ParameterizedTest
    @MethodSource("timeZonesWithHours")
    public void testTimeZonesWithHours(String startTimeZone, String endTimeZone, int hour) throws Exception {
        this.startTimeZone = startTimeZone;
        this.endTimeZone = endTimeZone;
        test(hour);
    }

    @ParameterizedTest
    @MethodSource("timeZones")
    public void fullTime(String startTimeZone, String endTimeZone) throws Exception {
        this.startTimeZone = startTimeZone;
        this.endTimeZone = endTimeZone;
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone utc = TimeZone.getTimeZone("UTC");
        Calendar s = GregorianCalendar.getInstance(utc);
        s.set(YEAR, Calendar.OCTOBER, 1, 0, 0, 0);
        s.set(Calendar.MILLISECOND, 0);
        Calendar e = GregorianCalendar.getInstance(utc);
        e.setTimeInMillis(s.getTimeInMillis());
        e.add(Calendar.DAY_OF_MONTH, 1);
        master.setStartDate(DT(s.getTime(), TimeZone.getTimeZone(startTimeZone), true));
        master.setEndDate(DT(e.getTime(), TimeZone.getTimeZone(endTimeZone), true));

        Iterator<Event> instances = service.calculateInstances(master, null, null, null);

        Calendar start = GregorianCalendar.getInstance(utc);
        start.set(YEAR, Calendar.OCTOBER, 1, 0, 0, 0);
        start.set(Calendar.MILLISECOND, 0);
        Calendar end = GregorianCalendar.getInstance(utc);
        end.setTimeInMillis(start.getTimeInMillis());
        end.add(Calendar.DAY_OF_MONTH, 1);

        int count = 0;
        while (instances.hasNext() && count++ <= 365) {
            Event instance = instances.next();
            compareInstanceWithMaster(master, instance, start.getTime(), end.getTime());
            start.add(Calendar.DAY_OF_MONTH, 1);
            end.add(Calendar.DAY_OF_MONTH, 1);
            start.set(Calendar.HOUR_OF_DAY, 0);
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.SECOND, 0);
            start.set(Calendar.MILLISECOND, 0);
            end.setTimeInMillis(start.getTimeInMillis());
            end.add(Calendar.DAY_OF_MONTH, 1);
        }
    }

    private void test(int hourOfDay) throws Exception {
        oneHourLong(hourOfDay);
        moreThanOneDay(hourOfDay);
    }

    private void moreThanOneDay(int hourOfDay) throws Exception {
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone startTz = TimeZone.getTimeZone(startTimeZone);
        TimeZone endTz = TimeZone.getTimeZone(endTimeZone);
        Calendar s = GregorianCalendar.getInstance(startTz);
        s.set(YEAR, Calendar.OCTOBER, 1, hourOfDay, 0, 0);
        s.set(Calendar.MILLISECOND, 0);
        Calendar e = GregorianCalendar.getInstance(endTz);
        e.setTimeInMillis(s.getTimeInMillis() + 3600000L * 36);
        master.setStartDate(DT(s.getTime(), TimeZone.getTimeZone(startTimeZone), false));
        master.setEndDate(DT(e.getTime(), TimeZone.getTimeZone(endTimeZone), false));

        Iterator<Event> instances = service.calculateInstances(master, null, null, null);

        Calendar start = GregorianCalendar.getInstance(startTz);
        start.set(YEAR, Calendar.OCTOBER, 1, hourOfDay, 0, 0);
        start.set(Calendar.MILLISECOND, 0);
        Calendar end = GregorianCalendar.getInstance(endTz);
        end.setTimeInMillis(start.getTimeInMillis() + 3600000L * 36);

        int count = 0;
        while (instances.hasNext() && count++ <= 365) {
            Event instance = instances.next();
            compareInstanceWithMaster(master, instance, start.getTime(), end.getTime());
            start.add(Calendar.DAY_OF_MONTH, 1);
            end.add(Calendar.DAY_OF_MONTH, 1);
            start.set(Calendar.HOUR_OF_DAY, hourOfDay);
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.SECOND, 0);
            start.set(Calendar.MILLISECOND, 0);
            end.setTimeInMillis(start.getTimeInMillis() + 3600000L * 36);
        }
    }

    private void oneHourLong(int hourOfDay) throws Exception {
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone startTz = TimeZone.getTimeZone(startTimeZone);
        TimeZone endTz = TimeZone.getTimeZone(endTimeZone);
        Calendar s = GregorianCalendar.getInstance(startTz);
        s.set(YEAR, Calendar.OCTOBER, 1, hourOfDay, 0, 0);
        s.set(Calendar.MILLISECOND, 0);
        Calendar e = GregorianCalendar.getInstance(endTz);
        e.setTimeInMillis(s.getTimeInMillis() + 3600000L);
        master.setStartDate(DT(s.getTime(), TimeZone.getTimeZone(startTimeZone), false));
        master.setEndDate(DT(e.getTime(), TimeZone.getTimeZone(endTimeZone), false));

        Iterator<Event> instances = service.calculateInstances(master, null, null, null);

        Calendar start = GregorianCalendar.getInstance(startTz);
        start.set(YEAR, Calendar.OCTOBER, 1, hourOfDay, 0, 0);
        start.set(Calendar.MILLISECOND, 0);
        Calendar end = GregorianCalendar.getInstance(endTz);
        end.setTimeInMillis(start.getTimeInMillis() + 3600000L);

        int count = 0;
        while (instances.hasNext() && count++ <= 365) {
            Event instance = instances.next();
            compareInstanceWithMaster(master, instance, start.getTime(), end.getTime());
            start.add(Calendar.DAY_OF_MONTH, 1);
            end.add(Calendar.DAY_OF_MONTH, 1);
            start.set(Calendar.HOUR_OF_DAY, hourOfDay);
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.SECOND, 0);
            start.set(Calendar.MILLISECOND, 0);
            end.setTimeInMillis(start.getTimeInMillis() + 3600000L);
        }
    }

}
