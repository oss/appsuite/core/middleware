/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.recurrence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import java.util.TimeZone;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import com.openexchange.chronos.Event;

/**
 * {@link RecurrenceDatePositionTest}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 * @since v7.10.0
 */
public class RecurrenceDatePositionTest extends AbstractSingleTimeZoneTest {

    @ParameterizedTest
    @MethodSource("timeZones")
    public void dailyNoEnd(String timeZone) throws Exception {
        this.timeZone = timeZone;
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        master.setStartDate(DT("01.10.2008 14:45:00", TimeZone.getTimeZone(timeZone), false));
        assertEquals(getCal("01.10.2008 14:45:00"), service.calculateRecurrenceDatePosition(master, 1), "Wrong date position.");
        assertNull(service.calculateRecurrenceDatePosition(master, 0), "Wrong date position.");
        assertEquals(getCal("02.10.2008 14:45:00"), service.calculateRecurrenceDatePosition(master, 2), "Wrong date position.");
        assertEquals(getCal("09.01.2009 14:45:00"), service.calculateRecurrenceDatePosition(master, 101), "Wrong date position.");
    }

    @ParameterizedTest
    @MethodSource("timeZones")
    public void dailyThreeOccurrences(String timeZone) throws Exception {
        this.timeZone = timeZone;
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1;COUNT=3");
        master.setStartDate(DT("01.10.2008 14:45:00", TimeZone.getTimeZone(timeZone), false));
        assertEquals(getCal("01.10.2008 14:45:00"), service.calculateRecurrenceDatePosition(master, 1), "Wrong date position.");
        assertEquals(getCal("02.10.2008 14:45:00"), service.calculateRecurrenceDatePosition(master, 2), "Wrong date position.");
        assertEquals(getCal("03.10.2008 14:45:00"), service.calculateRecurrenceDatePosition(master, 3), "Wrong date position.");
        assertNull(service.calculateRecurrenceDatePosition(master, 4), "Wrong date position.");
    }

    @ParameterizedTest
    @MethodSource("timeZones")
    public void dailyUntil(String timeZone) throws Exception {
        this.timeZone = timeZone;
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1;UNTIL=" + getUntilZulu(getCal("12.10.2008 14:45:00")));
        master.setStartDate(DT("01.10.2008 14:45:00", TimeZone.getTimeZone(timeZone), false));
        assertEquals(getCal("01.10.2008 14:45:00"), service.calculateRecurrenceDatePosition(master, 1), "Wrong date position.");
        assertEquals(getCal("12.10.2008 14:45:00"), service.calculateRecurrenceDatePosition(master, 12), "Wrong date position.");
        assertNull(service.calculateRecurrenceDatePosition(master, 13), "Wrong date position.");
    }

    @ParameterizedTest
    @MethodSource("timeZones")
    public void weeklyNoEnd(String timeZone) throws Exception {
        this.timeZone = timeZone;
        Event master = new Event();
        master.setRecurrenceRule("FREQ=WEEKLY;BYDAY=WE;INTERVAL=1");
        master.setStartDate(DT("01.10.2008 08:00:00", TimeZone.getTimeZone(timeZone), false));
        assertEquals(getCal("01.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 1), "Wrong date position.");
        assertEquals(getCal("08.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 2), "Wrong date position.");
        assertEquals(getCal("15.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 3), "Wrong date position.");
        assertEquals(getCal("22.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 4), "Wrong date position.");
        assertEquals(getCal("29.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 5), "Wrong date position.");
        assertEquals(getCal("05.11.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 6), "Wrong date position.");
    }

    @ParameterizedTest
    @MethodSource("timeZones")
    public void weeklyThreeOccurrencesAndStartOffset(String timeZone) throws Exception {
        this.timeZone = timeZone;
        Event master = new Event();
        master.setRecurrenceRule("FREQ=WEEKLY;BYDAY=TH;INTERVAL=1;COUNT=3");
        master.setStartDate(DT("01.10.2008 08:00:00", TimeZone.getTimeZone(timeZone), false));
        assertEquals(getCal("01.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 1), "Wrong date position.");
        assertEquals(getCal("02.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 2), "Wrong date position.");
        assertEquals(getCal("09.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 3), "Wrong date position.");
        if (COUNT_DTSTART) {
            assertNull(service.calculateRecurrenceDatePosition(master, 4), "Wrong date position.");
        } else {
            assertEquals(getCal("16.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 4), "Wrong date position.");
            assertNull(service.calculateRecurrenceDatePosition(master, 5), "Wrong date position.");
        }
    }

    @ParameterizedTest
    @MethodSource("timeZones")
    public void weeklyComplex(String timeZone) throws Exception {
        this.timeZone = timeZone;
        Event master = new Event();
        master.setRecurrenceRule("FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;INTERVAL=2;UNTIL=" + getUntilZulu(getCal("27.10.2008 08:00:00")));
        master.setStartDate(DT("01.10.2008 08:00:00", TimeZone.getTimeZone(timeZone), false));
        assertEquals(getCal("01.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 1), "Wrong date position.");
        assertEquals(getCal("02.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 2), "Wrong date position.");
        assertEquals(getCal("03.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 3), "Wrong date position.");
        assertEquals(getCal("13.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 4), "Wrong date position.");
        assertEquals(getCal("14.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 5), "Wrong date position.");
        assertEquals(getCal("15.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 6), "Wrong date position.");
        assertEquals(getCal("16.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 7), "Wrong date position.");
        assertEquals(getCal("17.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 8), "Wrong date position.");
        assertEquals(getCal("27.10.2008 08:00:00"), service.calculateRecurrenceDatePosition(master, 9), "Wrong date position.");
        assertNull(service.calculateRecurrenceDatePosition(master, 10), "Wrong date position.");
    }
}
