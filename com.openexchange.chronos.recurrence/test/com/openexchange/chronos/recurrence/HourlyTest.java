/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.recurrence;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import com.openexchange.chronos.Event;

/**
 * {@link HourlyTest}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 * @since v7.10.0
 */
public class HourlyTest extends AbstractSingleTimeZoneTest {

    private static final int YEAR = new GregorianCalendar().get(Calendar.YEAR) + 1;

    public static Stream<Arguments> timeZonesAndHours() {
        return Stream.of(TimeZone.getAvailableIDs())
                .flatMap(timeZone ->
                        IntStream.range(0, 24)
                                .mapToObj(i -> Arguments.of(timeZone, i))
                );
    }

    @ParameterizedTest
    @MethodSource("timeZonesAndHours")
    public void testTimeZoneWithNumber(String timeZone, int number) throws Exception {
        super.timeZone = timeZone;
        test(number);
    }

    @ParameterizedTest
    @MethodSource("timeZones")
    public void fullTime(String timeZone) throws Exception {
        super.timeZone = timeZone;
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone utc = TimeZone.getTimeZone("UTC");
        Calendar s = GregorianCalendar.getInstance(utc);
        s.set(YEAR, Calendar.OCTOBER, 1, 0, 0, 0);
        s.set(Calendar.MILLISECOND, 0);
        Calendar e = GregorianCalendar.getInstance(utc);
        e.setTimeInMillis(s.getTimeInMillis());
        e.add(Calendar.DAY_OF_MONTH, 1);
        setStartAndEndDates(master, s.getTime(), e.getTime(), true, null);

        Iterator<Event> instances = service.calculateInstances(master, null, null, null);

        Calendar start = GregorianCalendar.getInstance(utc);
        start.set(YEAR, Calendar.OCTOBER, 1, 0, 0, 0);
        start.set(Calendar.MILLISECOND, 0);
        Calendar end = GregorianCalendar.getInstance(utc);
        end.setTimeInMillis(start.getTimeInMillis());
        end.add(Calendar.DAY_OF_MONTH, 1);

        int count = 0;
        while (instances.hasNext() && count++ <= 365) {
            Event instance = instances.next();
            compareInstanceWithMaster(master, instance, start.getTime(), end.getTime());
            start.add(Calendar.DAY_OF_MONTH, 1);
            end.add(Calendar.DAY_OF_MONTH, 1);
            start.set(Calendar.HOUR_OF_DAY, 0);
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.SECOND, 0);
            start.set(Calendar.MILLISECOND, 0);
            end.setTimeInMillis(start.getTimeInMillis());
            end.add(Calendar.DAY_OF_MONTH, 1);
        }
    }

    private void test(int hourOfDay) throws Exception {
        oneHourLong(hourOfDay);
        moreThanOneDay(hourOfDay);
    }

    private void moreThanOneDay(int hourOfDay) throws Exception {
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        Calendar s = GregorianCalendar.getInstance(tz);
        s.set(YEAR, Calendar.OCTOBER, 1, hourOfDay, 0, 0);
        s.set(Calendar.MILLISECOND, 0);
        Calendar e = GregorianCalendar.getInstance(tz);
        e.setTimeInMillis(s.getTimeInMillis() + 3600000L * 36);
        setStartAndEndDates(master, s.getTime(), e.getTime(), false, tz);

        Iterator<Event> instances = service.calculateInstances(master, null, null, null);

        // Use "neutral" calendar object for iterations since timzones may do weird stuff when incrementing fields.
        Calendar utc = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC"));
        utc.set(YEAR, Calendar.OCTOBER, 1, hourOfDay, 0, 0);
        utc.set(Calendar.MILLISECOND, 0);

        Calendar start = GregorianCalendar.getInstance(tz);
        start.set(Calendar.YEAR, utc.get(Calendar.YEAR));
        start.set(Calendar.MONTH, utc.get(Calendar.MONTH));
        start.set(Calendar.DAY_OF_MONTH, utc.get(Calendar.DAY_OF_MONTH));
        start.set(Calendar.HOUR_OF_DAY, hourOfDay);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);

        Calendar end = GregorianCalendar.getInstance(tz);
        end.setTimeInMillis(start.getTimeInMillis() + 3600000L * 36);

        int count = 0;
        while (instances.hasNext() && count++ <= 365) {
            Event instance = instances.next();
            compareInstanceWithMaster(master, instance, start.getTime(), end.getTime());
            utc.add(Calendar.DAY_OF_MONTH, 1);
            end.add(Calendar.SECOND, 3600000);
            start.set(Calendar.YEAR, utc.get(Calendar.YEAR));
            start.set(Calendar.MONTH, utc.get(Calendar.MONTH));
            start.set(Calendar.DAY_OF_MONTH, utc.get(Calendar.DAY_OF_MONTH));
            start.set(Calendar.HOUR_OF_DAY, hourOfDay);
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.SECOND, 0);
            start.set(Calendar.MILLISECOND, 0);
            end.setTimeInMillis(start.getTimeInMillis() + 3600000L * 36);
        }
    }

    private void oneHourLong(int hourOfDay) throws Exception {
        Event master = new Event();
        master.setRecurrenceRule("FREQ=DAILY;INTERVAL=1");
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        Calendar s = GregorianCalendar.getInstance(tz);
        s.set(YEAR, Calendar.OCTOBER, 1, hourOfDay, 0, 0);
        s.set(Calendar.MILLISECOND, 0);
        Calendar e = GregorianCalendar.getInstance(tz);
        e.setTimeInMillis(s.getTimeInMillis() + 3600000L);
        setStartAndEndDates(master, s.getTime(), e.getTime(), false, tz);

        Iterator<Event> instances = service.calculateInstances(master, null, null, null);

        // Use "neutral" calendar object for iterations since timzones may do weird stuff when incrementing fields.
        Calendar utc = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC"));
        utc.set(YEAR, Calendar.OCTOBER, 1, hourOfDay, 0, 0);
        utc.set(Calendar.MILLISECOND, 0);

        Calendar start = GregorianCalendar.getInstance(tz);
        start.set(Calendar.YEAR, utc.get(Calendar.YEAR));
        start.set(Calendar.MONTH, utc.get(Calendar.MONTH));
        start.set(Calendar.DAY_OF_MONTH, utc.get(Calendar.DAY_OF_MONTH));
        start.set(Calendar.HOUR_OF_DAY, hourOfDay);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);

        Calendar end = GregorianCalendar.getInstance(tz);
        end.setTimeInMillis(start.getTimeInMillis() + 3600000L);

        int count = 0;
        while (instances.hasNext() && count++ <= 365) {
            Event instance = instances.next();
            compareInstanceWithMaster(master, instance, start.getTime(), end.getTime());
            utc.add(Calendar.DAY_OF_MONTH, 1);
            end.add(Calendar.SECOND, 3600000);
            start.set(Calendar.YEAR, utc.get(Calendar.YEAR));
            start.set(Calendar.MONTH, utc.get(Calendar.MONTH));
            start.set(Calendar.DAY_OF_MONTH, utc.get(Calendar.DAY_OF_MONTH));
            start.set(Calendar.HOUR_OF_DAY, hourOfDay);
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.SECOND, 0);
            start.set(Calendar.MILLISECOND, 0);
            end.setTimeInMillis(start.getTimeInMillis() + 3600000L);
        }
    }

}
