/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.sms.tools.internal;

import static com.openexchange.java.Autoboxing.I;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.annotation.NonNull;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.ClusterMap;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.cluster.map.codec.AbstractJSONMapCodec;
import com.openexchange.cluster.map.codec.MapCodec;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.exception.OXException;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.sms.tools.SMSBucketExceptionCodes;
import com.openexchange.sms.tools.SMSBucketService;
import com.openexchange.sms.tools.SMSConstants;


/**
 * {@link SMSBucketServiceImpl}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v7.8.1
 */
public class SMSBucketServiceImpl extends BasicCoreClusterMapProvider<SMSBucket> implements SMSBucketService {

    private static MapCodec<SMSBucket> createSMSBucketCodec() {
        return new AbstractJSONMapCodec<SMSBucket>() {

            @Override
            protected @NonNull JSONObject writeJson(SMSBucket value) throws Exception {
                JSONObject j = new JSONObject();
                j.put("counter", value.getCounter());
                long[] timestamps = value.getTimestamps();
                if (timestamps != null) {
                    JSONArray jStamps = new JSONArray(timestamps.length);
                    for (long timestamp : timestamps) {
                        jStamps.put(timestamp);
                    }
                    j.put("timestamps", jStamps);
                }
                return j;
            }

            @Override
            protected @NonNull SMSBucket parseJson(JSONObject jObject) throws Exception {
                int counter = jObject.optInt("counter");
                JSONArray jStamps = jObject.optJSONArray("timestamps");

                SMSBucket bucket = new SMSBucket();
                bucket.setCounter(counter);
                if (jStamps != null) {
                    long[] timestamps = new long[jStamps.length()];
                    int i = 0;
                    for (Object o : jStamps) {
                        timestamps[i++] = ((Number) o).longValue();
                    }
                    bucket.setTimestamps(timestamps);
                }
                return bucket;
            }
        };
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    private final ServiceLookup services;

    /**
     * Initializes a new {@link SMSBucketServiceImpl}.
     *
     * @param services The service look-up to use
     */
    public SMSBucketServiceImpl(ServiceLookup services) {
        super(CoreMap.SMS_BUCKET, createSMSBucketCodec(), 259200 * 1000L, () -> services.getServiceSafe(ClusterMapService.class));
        this.services = services;
    }

    @Override
    public int getSMSToken(Session session) throws OXException {
        String userIdentifier = session.getContextId() + "/" + session.getUserId();
        int limit = getUserLimit(session);

        ClusterMap<SMSBucket> map = getMap();
        SMSBucket existingBucket = map.putIfAbsent(userIdentifier, new SMSBucket(limit));
        if (existingBucket != null && existingBucket.getBucketSize() != limit) {
            map.replace(userIdentifier, existingBucket, new SMSBucket(limit));
        }

        ConfigView view = services.getServiceSafe(ConfigViewFactory.class).getView(session.getUserId(), session.getContextId());
        int refreshInterval;
        try {
            refreshInterval = Integer.parseInt(view.get(SMSConstants.SMS_USER_LIMIT_REFRESH_INTERVAL, String.class));
        } catch (NumberFormatException e) {
            throw OXException.general("Value for property '" + SMSConstants.SMS_USER_LIMIT_REFRESH_INTERVAL + "' is not an integer", e);
        }

        for (;;) {
            SMSBucket oldBucket = map.get(userIdentifier);
            SMSBucket newBucket = oldBucket.clone();
            int amount = newBucket.removeToken(refreshInterval);

            if (amount == -1) {
                int hours = (int) Math.ceil(refreshInterval / 60d);
                throw SMSBucketExceptionCodes.SMS_LIMIT_REACHED.create(I(hours));
            }

            if (map.replace(userIdentifier, oldBucket, newBucket)) {
                return amount;
            }
        }
    }

    private int getUserLimit(Session session) throws OXException {
        ConfigViewFactory configFactory = services.getServiceSafe(ConfigViewFactory.class);
        ConfigView view = configFactory.getView(session.getUserId(), session.getContextId());
        return Integer.parseInt(view.get(SMSConstants.SMS_USER_LIMIT_PROPERTY, String.class));
    }

    @Override
    public boolean isEnabled(Session session) throws OXException {
        ConfigViewFactory configFactory = services.getServiceSafe(ConfigViewFactory.class);
        ConfigView view = configFactory.getView(session.getUserId(), session.getContextId());
        return view.get(SMSConstants.SMS_USER_LIMIT_ENABLED, boolean.class).booleanValue();
    }

    @Override
    public int getRefreshInterval(Session session) throws OXException {
        ConfigViewFactory factory = services.getServiceSafe(ConfigViewFactory.class);
        ConfigView view = factory.getView(session.getUserId(), session.getContextId());
        // Return hours
        return (int) Math.ceil(Double.parseDouble(view.property("com.openexchange.sms.userlimit.refreshInterval", String.class).get()) / 60d);
    }

}
