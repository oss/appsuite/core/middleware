/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.hostname.ldap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.notify.hostname.HostnameService;
import com.openexchange.hostname.ldap.caching.Hostnames;
import com.openexchange.hostname.ldap.caching.HostnamesCodec;

/**
 * {@link CachingLDAPHostnameService}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class CachingLDAPHostnameService implements HostnameService {

    private static final Logger LOG = LoggerFactory.getLogger(CachingLDAPHostnameService.class);

    /** Options for the cache storing hostnames loaded from LDAP per context identifier */
    private static final CacheOptions<Hostnames> OPTIONS = CacheOptions.<Hostnames> builder()
                                                                      .withCoreModuleName(CoreModuleName.LDAP_HOST_NAME)
                                                                      .withCodecAndVersion(new HostnamesCodec())
                                                                      .build();

    private final LDAPHostnameService delegate;
    private final Cache<Hostnames> cache;

    /**
     * Initializes a new {@link CachingLDAPHostnameService}.
     *
     * @param cacheService The cache service to use
     * @param delegate The LDAP host name service to delegate to
     */
    public CachingLDAPHostnameService(CacheService cacheService, LDAPHostnameService delegate) {
        super();
        this.cache = cacheService.getCache(OPTIONS);
        this.delegate = delegate;
    }

    @Override
    public String getHostname(int userId, int contextId) {
        Hostnames hostnames = getHostnames(contextId);
        return null != hostnames ? hostnames.host() : null;
    }

    @Override
    public String getGuestHostname(int userId, int contextId) {
        Hostnames hostnames = getHostnames(contextId);
        return null != hostnames ? hostnames.guestHost() : null;
    }

    // ----------------------- private methods ---------------------

    /**
     * Gets or loads the host names for a certain context identifier.
     *
     * @param contextId The context identifier
     * @return The host names, or <code>null</code> if not defined or an error is occurred
     */
    private Hostnames getHostnames(int contextId) {
        CacheKey key = cache.newKey(Integer.toString(contextId));
        try {
            return cache.get(key, k -> delegate.getHostnamesFromLDAP(contextId));
        } catch (OXException e) {
            LOG.error("Unable to load hostnames into cache", e);
            return null;
        }
    }

}
