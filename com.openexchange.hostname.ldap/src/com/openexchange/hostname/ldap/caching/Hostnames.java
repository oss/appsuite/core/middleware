/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.hostname.ldap.caching;

/**
 * {@link Hostnames} is a data holder for host names.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public record Hostnames(String host, String guestHost) {

    /**
     * Initializes a new {@link Hostnames}.
     *
     * @param host The host name
     * @param guestHost The guest host name
     */
    public Hostnames(String host, String guestHost) {
        this.host = host;
        this.guestHost = guestHost;
    }

    /**
     * Gets the host name.
     *
     * @return The host name
     */
    public String host() {
        return host;
    }

    /**
     * Gets the guest host name.
     *
     * @return The guest host name
     */
    public String guestHost() {
        return guestHost;
    }
}
