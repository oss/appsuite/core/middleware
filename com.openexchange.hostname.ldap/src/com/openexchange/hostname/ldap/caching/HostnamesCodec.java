/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.hostname.ldap.caching;

import java.io.Writer;
import java.util.UUID;
import org.json.JSONObject;
import org.json.SimpleJSONSerializer;
import com.openexchange.cache.v2.codec.json.AbstractSimpleJSONObjectCacheValueCodec;

/**
 * {@link HostnamesCodec} is a codec for host names.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class HostnamesCodec extends AbstractSimpleJSONObjectCacheValueCodec<Hostnames> {

    private static final UUID CODEC_ID = UUID.fromString("35221abf-0861-434a-882d-686570345ee3");

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected void writeJson(Hostnames value, Writer writer) throws Exception {
        writer.append('{');

        if (null != value) {
            SimpleJSONSerializer.writeJsonValue("h", writer);
            writer.append(':');
            SimpleJSONSerializer.writeJsonValue(value.host(), writer);

            writer.append(',');
            SimpleJSONSerializer.writeJsonValue("g", writer);
            writer.append(':');
            SimpleJSONSerializer.writeJsonValue(value.guestHost(), writer);
        }

        writer.append('}');
    }

    @Override
    protected Hostnames parseJson(JSONObject jObject) throws Exception {
        if (null == jObject || jObject.isEmpty()) {
            return null;
        }
        return new Hostnames(jObject.optString("h", null), jObject.optString("g", null));
    }

}
