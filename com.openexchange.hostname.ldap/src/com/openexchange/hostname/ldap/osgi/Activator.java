/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.hostname.ldap.osgi;

import com.openexchange.cache.v2.CacheService;
import com.openexchange.config.ConfigurationService;
import com.openexchange.groupware.notify.hostname.HostnameService;
import com.openexchange.hostname.ldap.CachingLDAPHostnameService;
import com.openexchange.hostname.ldap.LDAPHostnameService;
import com.openexchange.hostname.ldap.configuration.LDAPHostnameProperties;
import com.openexchange.hostname.ldap.configuration.Property;
import com.openexchange.net.ssl.SSLSocketFactoryProvider;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * The activator for <i>"com.openexchange.hostname.ldap"</i> bundle
 */
public class Activator extends HousekeepingActivator {

    /**
     * Initializes a new {@link Activator}.
     */
    public Activator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { CacheService.class, ConfigurationService.class, SSLSocketFactoryProvider.class };
    }

    @Override
    protected boolean stopOnServiceUnavailability() {
        return true;
    }

    @Override
    protected void startBundle() throws Exception {
        ConfigurationService configService = getService(ConfigurationService.class);
        LDAPHostnameProperties.check(configService, Property.values(), "com.openexchange.hostname.ldap");

        // Register host name service to modify host names in direct links, this will also initialize the cache class
        registerService(HostnameService.class, new CachingLDAPHostnameService(getServiceSafe(CacheService.class),
            new LDAPHostnameService(this)));
    }

}
