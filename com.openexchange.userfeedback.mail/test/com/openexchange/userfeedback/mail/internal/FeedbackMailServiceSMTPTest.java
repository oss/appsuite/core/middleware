/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.userfeedback.mail.internal;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import java.io.InputStream;
import java.util.HashMap;
import javax.mail.MessagingException;
import javax.mail.Transport;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.userfeedback.FeedbackService;
import com.openexchange.userfeedback.exception.FeedbackExceptionCodes;
import com.openexchange.userfeedback.export.ExportResult;
import com.openexchange.userfeedback.export.ExportResultConverter;
import com.openexchange.userfeedback.export.ExportType;
import com.openexchange.userfeedback.mail.filter.FeedbackMailFilter;
import com.openexchange.userfeedback.mail.osgi.Services;

/**
 * {@link FeedbackMailServiceSMTPTest}
 *
 * @author <a href="mailto:vitali.sjablow@open-xchange.com">Vitali Sjablow</a>
 * @since v7.8.4
 */
public class FeedbackMailServiceSMTPTest {

    @Mock
    private ConfigurationService configService;

    @Mock
    private FeedbackService feedbackService;

    @Mock
    private LeanConfigurationService leanConfigurationService;

    @Mock
    private Transport transport;

    private FeedbackMailFilter filter;

    private MockedStatic<Services> servicesMock;

    @BeforeEach
    public void setUp() throws Exception {
        filter = new FeedbackMailFilter("1", new HashMap<String, String>(),  "sub", "body", 0l, 0l, "", false);

        MockitoAnnotations.openMocks(this);
        servicesMock = Mockito.mockStatic(Services.class);
        Mockito.when(Services.getService(ConfigurationService.class)).thenReturn(configService);
        Mockito.when(Services.getService(FeedbackService.class)).thenReturn(feedbackService);
        Mockito.when(Services.getService(FeedbackService.class)).thenReturn(feedbackService);
        Mockito.when(Services.getService(LeanConfigurationService.class)).thenReturn(leanConfigurationService);

        Mockito.when(configService.getProperty(org.mockito.ArgumentMatchers.anyString(), org.mockito.ArgumentMatchers.anyString())).thenReturn("");
        Mockito.when(I(configService.getIntProperty(org.mockito.ArgumentMatchers.anyString(), org.mockito.ArgumentMatchers.anyInt()))).thenReturn(I(1));

        Mockito.when(leanConfigurationService.getProperty(UserFeedbackMailProperty.hostname)).thenReturn("example.com");

        ExportResultConverter value = new ExportResultConverter() {

            @Override
            public ExportResult get(ExportType type) {
                return new ExportResult() {
                    @Override
                    public Object getResult() {
                        String source = "This is the source of my input stream";
                        InputStream in = null;
                        try {
                            in = IOUtils.toInputStream(source, "UTF-8");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return in;
                    }
                };
            }
        };
        Mockito.when(feedbackService.export("1", filter)).thenReturn(value);
    }

    @AfterEach
    public void reset() {
        servicesMock.close();
    }

    @Test
    public void sendFeedbackMail_FailInvalidAddresses() throws Exception {
        FeedbackMailServiceSMTP service = new FeedbackMailServiceSMTP();
        FeedbackMailServiceSMTP serviceSpy = Mockito.spy(service);

        try (MockedStatic<Transport> mockedStatic = Mockito.mockStatic(Transport.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> Transport.send(any(), any())).thenThrow(new MessagingException());

            filter.getRecipients().put("dsfa", "");
            try {
                serviceSpy.sendFeedbackMail(filter);
            } catch (OXException e) {
                assertEquals(FeedbackExceptionCodes.INVALID_EMAIL_ADDRESSES, e.getExceptionCode());
                return;
            }
            // should never get here
            assertFalse(true);
        }
    }

    @Test
    public void sendFeedbackMail_FailInvalidSMTP() throws Exception {
        FeedbackMailServiceSMTP service = new FeedbackMailServiceSMTP();
        FeedbackMailServiceSMTP serviceSpy = Mockito.spy(service);

        filter.getRecipients().put("dsfa@blub.de", "");
        try {
            serviceSpy.sendFeedbackMail(filter);
        } catch (OXException e) {
            assertEquals(FeedbackExceptionCodes.INVALID_SMTP_CONFIGURATION, e.getExceptionCode());
            return;
        }
        // should never get here
        assertFalse(true);
    }
}
