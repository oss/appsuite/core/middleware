/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.undo.redis;

import java.util.Iterator;
import java.util.List;
import com.openexchange.undo.UndoOperation;
import com.openexchange.undo.UndoOperations;
import com.openexchange.undo.UndoToken;

/**
 * {@link RedisUndoOperations} - The result when querying for undo operations with a certain undo token.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class RedisUndoOperations implements UndoOperations {

    private final UndoToken undoToken;
    private final List<UndoOperation> operations;
    private final long timeToLiveMillis;

    /**
     * Initializes a new instance of {@link RedisUndoOperations}.
     *
     * @param undoToken The undo token referencing this collection of undo operations
     * @param operations The undo operations
     * @param timeToLiveMillis The remaining time-to-live in milliseconds in the time of retrieval
     */
    public RedisUndoOperations(UndoToken undoToken, List<UndoOperation> operations, long timeToLiveMillis) {
        super();
        this.undoToken = undoToken;
        this.operations = List.copyOf(operations);
        this.timeToLiveMillis = timeToLiveMillis;
    }

    @Override
    public UndoToken getUndoToken() {
        return undoToken;
    }

    @Override
    public List<UndoOperation> getOperations() {
        return operations;
    }

    @Override
    public long getTimeToLiveMillis() {
        return timeToLiveMillis;
    }

    @Override
    public boolean isEmpty() {
        return operations.isEmpty();
    }

    @Override
    public int size() {
        return operations.size();
    }

    @Override
    public Iterator<UndoOperation> iterator() {
        return operations.iterator();
    }

}
