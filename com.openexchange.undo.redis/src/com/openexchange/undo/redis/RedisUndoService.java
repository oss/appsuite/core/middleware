/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.undo.redis;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import org.json.JSONArray;
import org.json.JSONCoercion;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import com.google.common.collect.Lists;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.config.cascade.ConfigViews;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Streams;
import com.openexchange.java.util.UUIDs;
import com.openexchange.redis.RedisCommandsProvider;
import com.openexchange.redis.RedisConnector;
import com.openexchange.redis.RedisConnectorService;
import com.openexchange.session.Session;
import com.openexchange.undo.DefaultUndoOperation;
import com.openexchange.undo.DefaultUndoToken;
import com.openexchange.undo.UndoOperation;
import com.openexchange.undo.UndoOperations;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.UndoToken;
import io.lettuce.core.KeyScanCursor;
import io.lettuce.core.KeyValue;
import io.lettuce.core.ScanArgs;

/**
 * {@link RedisUndoService} - The Redis-based undo service.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class RedisUndoService implements UndoService {

    private static final char DELIMITER = ':';
    private static final String APP_NAME = "ox-undo";
    private static final int MAX_CHUNK_SIZE = 100;

    private final RedisConnector connector;
    private final ConfigViewFactory configViewFactory;

    /**
     * Initializes a new instance of {@link RedisUndoService}.
     *
     * @param connectorService The connector service
     * @param configViewFactory The config-cascade access
     * @throws OXException If initialization fails
     */
    public RedisUndoService(RedisConnectorService connectorService, ConfigViewFactory configViewFactory) throws OXException {
        super();
        this.configViewFactory = configViewFactory;
        this.connector = connectorService.getCacheConnectorProviderOrElseRegular().getConnector();
    }

    @Override
    public boolean isEnabledFor(Session session) throws OXException {
        ConfigView view = configViewFactory.getView(session.getUserId(), session.getContextId());
        return ConfigViews.getDefinedBoolPropertyFrom("com.openexchange.undo.enabled", true, view);
    }

    /**
     * Builds the fully-qualifying key; e.g.
     * <pre>
     * "ox-undo:1337:3:555134b73561466fb4c6827f4df5bcf3"
     * </pre>
     *
     * @param key The element's key; e.g. <code>"555134b73561466fb4c6827f4df5bcf3"</code>
     * @param session The session providing user information
     * @return The fully-qualifying key
     */
    public static String getFullKey(String key, Session session) {
        return getFullKey(key, session.getUserId(), session.getContextId());
    }

    /**
     * Builds the fully-qualifying key; e.g.
     * <pre>
     * "ox-undo:1337:3:555134b73561466fb4c6827f4df5bcf3"
     * </pre>
     *
     * @param key The element's key; e.g. <code>"555134b73561466fb4c6827f4df5bcf3"</code>
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The fully-qualifying key
     */
    public static String getFullKey(String key, int userId, int contextId) {
        return new StringBuilder(APP_NAME).append(DELIMITER).append(contextId).append(DELIMITER).append(userId).append(DELIMITER).append(key).toString();
    }

    @Override
    public List<UndoOperations> listUndoOperations(Session session) throws OXException {
        checkSession(session, true);

        String pattern = new StringBuilder(APP_NAME).append(DELIMITER).append(session.getContextId()).append(DELIMITER).append(session.getUserId()).append(DELIMITER).append('*').toString();
        ScanArgs scanArgs = ScanArgs.Builder.matches(pattern).limit(1000);
        return connector.executeOperation(commandsProvider -> listUndoOperations(scanArgs, commandsProvider));
    }

    /**
     * Lists existing undo operations by given SCAN arguments.
     *
     * @param scanArgs The SCAN arguments
     * @param commandsProvider The commands provider to use
     * @return The listing of existing undo operations
     * @throws OXException If retrieval from Redis storage fails
     */
    private static List<UndoOperations> listUndoOperations(ScanArgs scanArgs, RedisCommandsProvider commandsProvider) throws OXException {
        List<String> keys = null;
        KeyScanCursor<String> cursor = commandsProvider.getKeyCommands().scan(scanArgs);
        while (cursor != null) {
            // Check current keys
            if (keys == null) {
                keys = new ArrayList<>(cursor.getKeys());
            } else {
                keys.addAll(cursor.getKeys());
            }

            // Move cursor forward
            cursor = cursor.isFinished() ? null : commandsProvider.getKeyCommands().scan(cursor, scanArgs);
        }

        if (keys == null) {
            return Collections.emptyList();
        }

        int numberOfKeys = keys.size();
        if (numberOfKeys <= 0) {
            return Collections.emptyList();
        }

        // Retrieve via MGET from Redis storage
        List<KeyValue<String, InputStream>> list;
        if (numberOfKeys <= MAX_CHUNK_SIZE) {
            list = commandsProvider.getRawStringCommands().mget(keys.toArray(new String[numberOfKeys]));
        } else {
            list = new ArrayList<>(numberOfKeys);
            for (List<String> partition : Lists.partition(keys, MAX_CHUNK_SIZE)) {
                list.addAll(commandsProvider.getRawStringCommands().mget(partition.toArray(new String[partition.size()])));
            }
        }
        keys = null; // NOSONARLINT Might help GC

        List<UndoOperations> collection = new ArrayList<>(list.size());
        for (KeyValue<String, InputStream> keyValue : list) {
            InputStream data = keyValue.getValueOrElse(null);
            try {
                if (data != null) {
                    String key = keyValue.getKey();
                    Long ttlMillis = commandsProvider.getKeyCommands().pttl(key);
                    collection.add(new RedisUndoOperations(new DefaultUndoToken(key.substring(key.lastIndexOf(DELIMITER))), deserializeValue(data), ttlMillis.longValue()));
                }
            } catch (RuntimeException e) {
                throw assumeRedisError(e);
            } catch (OXException e) {
                throw e;
            } catch (Exception e) {
                throw assumeSerializationError(e);
            } finally {
                Streams.close(data);
            }
        }
        return collection;
    }

    @Override
    public Optional<UndoOperations> getUndoOperations(UndoToken undoToken, Session session) throws OXException {
        checkUndoToken(undoToken);
        checkSession(session, false);
        if (UndoToken.EMPTY.getToken().equals(undoToken.getToken())) {
            return Optional.empty();
        }

        return Optional.ofNullable(connector.executeOperation(commandsProvider -> getUndoOperation(undoToken, session, false, commandsProvider)));
    }

    @Override
    public Optional<UndoOperations> removeUndoOperations(UndoToken undoToken, Session session) throws OXException {
        checkUndoToken(undoToken);
        checkSession(session, false);
        if (UndoToken.EMPTY.getToken().equals(undoToken.getToken())) {
            return Optional.empty();
        }

        return Optional.ofNullable(connector.executeOperation(commandsProvider -> getUndoOperation(undoToken, session, true, commandsProvider)));
    }

    /**
     * Gets or removes the undo operation associated with given token.
     *
     * @param undoToken The undo token
     * @param session The session providing user information
     * @param remove <code>true</code> to remove the operation; otherwise <code>false</code> for simple get
     * @param commandsProvider The commands provider to use
     * @return The undo operation
     * @throws OXException If obtaining undo operation from Redis storage fails
     */
    private static RedisUndoOperations getUndoOperation(UndoToken undoToken, Session session, boolean remove, RedisCommandsProvider commandsProvider) throws OXException {
        InputStream data = null;
        try {
            String key = getFullKey(undoToken.getToken(), session);
            if (remove) {
                // Supposed to be removed. Obtain remaining TTL in advance
                Long ttlMillis = commandsProvider.getKeyCommands().pttl(key); // // NOSONARLINT
                data = commandsProvider.getRawStringCommands().getdel(key);
                if (data == null) {
                    return null;
                }

                RedisUndoOperations retval = new RedisUndoOperations(undoToken, deserializeValue(data), ttlMillis.longValue());
                data = null;
                return retval;
            }

            // Simple GET. Remaining TTL may be queried after existence is known
            data = commandsProvider.getRawStringCommands().get(key);
            if (data == null) {
                return null;
            }

            // Query remaining TTL
            long ttlMillis = commandsProvider.getKeyCommands().pttl(key).longValue();
            if (ttlMillis == -2) {
                // The PTTL command returns -2 if the key does not exist. Evicted in the meantime...
                return null;
            }

            RedisUndoOperations retval = new RedisUndoOperations(undoToken, deserializeValue(data), ttlMillis);
            data = null;
            return retval;
        } catch (RuntimeException e) {
            throw assumeRedisError(e);
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw assumeSerializationError(e);
        } finally {
            Streams.close(data);
        }
    }

    @Override
    public void restoreUndoOperations(UndoToken undoToken, UndoOperations undoOperations, Session session) throws OXException {
        checkUndoToken(undoToken);
        checkSession(session, true);
        if (UndoToken.EMPTY.getToken().equals(undoToken.getToken())) {
            return;
        }
        if (undoOperations == null || undoOperations.isEmpty()) {
            return;
        }

        connector.executeVoidOperation(commandsProvider -> storeUndoOperation(undoToken.getToken(), undoOperations.getOperations(), undoOperations.getTimeToLiveMillis(), session, commandsProvider));
    }

    @Override
    public UndoToken registerUndoOperations(List<UndoOperation> undoOperations, long timeToLiveMillis, Session session) throws OXException {
        checkSession(session, true);
        if (undoOperations == null || undoOperations.isEmpty()) {
            return UndoToken.EMPTY;
        }

        String token = UUIDs.getUnformattedStringFromRandom();
        connector.executeVoidOperation(commandsProvider -> storeUndoOperation(token, undoOperations, timeToLiveMillis, session, commandsProvider));
        return new DefaultUndoToken(token);
    }

    /**
     * Stores specified undo operations.
     *
     * @param token The undo token to associate with undo operations
     * @param undoOperations The undo operations to store
     * @param timeToLiveMillis The time-to-live in milliseconds
     * @param session The session providing user information
     * @param commandsProvider The commands provider to use
     * @throws OXException If store operation to Redis storage fails
     */
    private static void storeUndoOperation(String token, List<UndoOperation> undoOperations, long timeToLiveMillis, Session session, RedisCommandsProvider commandsProvider) throws OXException {
        InputStream serializedValue = null;
        try {
            serializedValue = serializeValue(undoOperations);
            if (timeToLiveMillis > 0) {
                commandsProvider.getRawStringCommands().psetex(getFullKey(token, session), timeToLiveMillis, serializedValue);
            } else {
                commandsProvider.getRawStringCommands().set(getFullKey(token, session), serializedValue);
            }
        } finally {
            Streams.close(serializedValue);
        }
    }

    @Override
    public boolean unregisterUndoOperations(UndoToken undoToken, Session session) throws OXException {
        checkUndoToken(undoToken);
        checkSession(session, false);
        if (UndoToken.EMPTY.getToken().equals(undoToken.getToken())) {
            return false;
        }

        return connector.executeOperation(commandsProvider -> Boolean.valueOf(removeUndoOperations(undoToken, session, commandsProvider))).booleanValue();
    }

    /**
     * Removes token-asosciated undo operations from registry.
     *
     * @param undoToken The undo token
     * @param session The session providing user information
     * @param commandsProvider The commands provider to use
     * @return <code>true</code> if such a collection of undo operations has been unregistered; otherwise <code>false</code> if there was no such collection
     */
    private static boolean removeUndoOperations(UndoToken undoToken, Session session, RedisCommandsProvider commandsProvider) {
        return commandsProvider.getKeyCommands().del(getFullKey(undoToken.getToken(), session)).longValue() > 0;
    }

    /**
     * Checks specified session for non-<code>null</code> and optionally checks permission for session-associated user.
     *
     * @param session The session
     * @param checkEnabled <code>true</code> to also check permission for session-associated user; otherwise <code>false</code>
     * @throws OXException If permission check fails
     */
    private void checkSession(Session session, boolean checkEnabled) throws OXException {
        if (session == null) {
            throw new IllegalArgumentException("Session must not be null");
        }
        if (checkEnabled && !isEnabledFor(session)) {
            throw OXException.noPermissionForModule("undo");
        }
    }

    /**
     * Checks specified undo token for non-<code>null</code>.
     *
     * @param undoToken The undo token
     */
    private static void checkUndoToken(UndoToken undoToken) {
        if (undoToken == null) {
            throw new IllegalArgumentException("Undo token must not be null");
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a general {@link OXException} with message <code>"Failed to de-/serialize value"</code>.
     *
     * @param e The runtime exception to wrap
     * @return The wrapping {@code OXException}
     */
    private static OXException assumeSerializationError(Exception e) {
        return OXException.general("Failed to de-/serialize value", e);
    }

    /**
     * Creates a general {@link OXException} with message <code>"Redis error"</code>.
     *
     * @param e The runtime exception to wrap
     * @return The wrapping {@code OXException}
     */
    private static OXException assumeRedisError(RuntimeException e) {
        return OXException.general("Redis error", e);
    }

    // ------------------------------------------------ Serialization ----------------------------------------------------------------------

    private static final byte[] EMPTY_ARRAY = { '[', ']' };

    private static InputStream serializeValue(List<UndoOperation> undoOperations) throws OXException {
        try {
            JSONArray json = writeJson(undoOperations);
            return json.isEmpty() ? Streams.newByteArrayInputStream(EMPTY_ARRAY) : json.getStream(false);
        } catch (Exception e) {
            throw OXException.general("Serialisation failed", e);
        }
    }

    private static JSONArray writeJson(List<UndoOperation> undoOperations) throws JSONException {
        JSONArray jOperations = new JSONArray(undoOperations.size());
        for (UndoOperation undoOperation : undoOperations) {
            JSONObject jUndoOperation = new JSONObject(5);
            jUndoOperation.putOpt("m", replace(undoOperation.getModule(), MODULE_REPLACEMENTS));
            jUndoOperation.putOpt("a", replace(undoOperation.getAction(), ACTION_REPLACEMENTS));
            jUndoOperation.putOpt("d", undoOperation.getData());
            Map<String, String> parameters = undoOperation.getParameters();
            if (parameters != null) {
                jUndoOperation.putOpt("p", JSONCoercion.coerceToJSON(parameters));
            }
            jOperations.put(jUndoOperation);
        }
        return jOperations;
    }

    // ------------------------------------------------ Deserialization --------------------------------------------------------------------

    private static List<UndoOperation> deserializeValue(InputStream data) throws OXException {
        try {
            return parseJson(JSONServices.parseArray(data));
        } catch (Exception e) {
            throw OXException.general("Deserialisation failed", e);
        } finally {
            Streams.close(data);
        }
    }

    private static final Function<Map.Entry<String, Object>, String> F_KEY_MAPPER = Map.Entry::getKey;
    private static final Function<Map.Entry<String, Object>, String> F_VALUE_MAPPER = e -> e.getValue().toString();

    private static List<UndoOperation> parseJson(JSONArray jOperations) {
        int numberOfOperations = jOperations.length();
        if (numberOfOperations <= 0) {
            return Collections.emptyList();
        }

        List<UndoOperation> operations = new ArrayList<>(numberOfOperations);
        for (Object o : jOperations) {
            JSONObject object = (JSONObject) o;
            DefaultUndoOperation.Builder undoOperation = DefaultUndoOperation.builder();
            undoOperation.withModule(unreplace(object.optString("m", null), MODULE_REPLACEMENTS));
            undoOperation.withAction(unreplace(object.optString("a", null), ACTION_REPLACEMENTS));
            undoOperation.withData(object.opt("d"));
            JSONObject jParameters = object.optJSONObject("p");
            if (jParameters != null) {
                undoOperation.withParameters(jParameters.stream().collect(CollectorUtils.toMap(F_KEY_MAPPER, F_VALUE_MAPPER, jParameters.length())));
            }
            operations.add(undoOperation.build());
        }
        return operations;
    }

    // ------------------------------------------------ Replacements-- --------------------------------------------------------------------

    /** A list of replacements for module identifiers */
    private static final List<String[]> MODULE_REPLACEMENTS = List.of(
        new String[] {"mail", "^1"},
        new String[] {"folders", "^2"},
        new String[] {"folder", "^3"},
        new String[] {"infostore", "^4"},
        new String[] {"files", "^5"},
        new String[] {"advertisement", "^6"},
        new String[] {"chronos", "^7"},
        new String[] {"calendar", "^8"},
        new String[] {"addressbooks", "^9"},
        new String[] {"tasks", "^0"},
        new String[] {"contacts", "^a"}
    );

    /** A list of replacements for action identifiers */
    private static final List<String[]> ACTION_REPLACEMENTS = List.of(
        new String[] {"delete", "^1"},
        new String[] {"clear", "^2"},
        new String[] {"archive", "^3"},
        new String[] {"archive_folder", "^4"},
        new String[] {"copy", "^5"},
        new String[] {"copy_multiple", "^6"},
        new String[] {"move_all", "^7"},
        new String[] {"move", "^8"},
        new String[] {"flags", "^9"},
        new String[] {"color_label", "^0"},
        new String[] {"update", "^a"}
    );

    /**
     * Replaces the whole string or substring of given <code>toReplace</code> with specified replacements using
     * {@link String#replace(CharSequence, CharSequence)}.
     *
     * @param toReplace The string to replace or replace in
     * @param replacements The replacements to use
     * @return The string with replacements or <code>null</code> if input string was <code>null</code>, too
     */
    private static String replace(String toReplace, Iterable<String[]> replacements) {
        if (toReplace == null) {
            return null;
        }
        String retval = toReplace;
        for (String[] replacement : replacements) {
            retval = retval.replace(replacement[0], replacement[1]);
        }
        return retval;
    }

    /**
     * Un-Replaces the whole string or substring of given <code>toReplace</code> with specified replacements using
     * {@link String#replace(CharSequence, CharSequence)}.
     *
     * @param replaced The string to unreplace or unreplace in
     * @param replacements The replacements to revert
     * @return The string with replacements
     */
    private static String unreplace(String replaced, Iterable<String[]> replacements) {
        if (replaced == null) {
            return null;
        }
        String retval = replaced;
        for (String[] replacement : replacements) {
            retval = retval.replace(replacement[1], replacement[0]);
        }
        return retval;
    }

}
