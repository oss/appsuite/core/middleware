package com.openexchange.admin.soap.request.analyzer;

import java.util.Optional;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.segment.SegmentMarker;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * {@link SessionServiceRequestAnalyzer} is a {@link RequestAnalyzer} which uses the context id, context name or session
 * to determine the marker for the request
 *
 * @author <a href="mailto:marcel.broecher@open-xchange.com">Marcel Broecher</a>
 */
public class SessionServiceRequestAnalyzer extends AbstractSOAPRequestAnalyzer {

    private static final QName QNAME_SESSIONID = new QName("http://soap.sessiond.openexchange.com", "sessionId");
    private static final QName QNAME_CONTEXTID = new QName("http://soap.sessiond.openexchange.com", "contextId");

    private final ErrorAwareSupplier<SessiondService> sessiondServiceSupplier;

    /**
     * Initializes a new {@link SessionServiceRequestAnalyzer}.
     *
     * @param services services The service lookup
     */
    public SessionServiceRequestAnalyzer(ServiceLookup services) {
        super(services, "OXSessionService", Set.of(new QName("http://soap.sessiond.openexchange.com", "clearContextsSessions")));
        this.sessiondServiceSupplier = () -> services.getServiceSafe(SessiondService.class);
    }

    @Override
    protected Optional<AnalyzeResult> analyzeXMLBody(XMLEventReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()) {
                QName qName = event.asStartElement().getName();
                if (isIgnoredOperation(qName)) {
                    return Optional.of(AnalyzeResult.UNKNOWN); // Element is part of non-supported operations, return AnalyzeResult.UNKNOWN
                }
                if (QNAME_SESSIONID.equals(qName)) {
                    return getResultFromSessionId(reader);
                }
                if (QNAME_CONTEXTID.equals(qName)) {
                    return getResultFromContextId(reader);
                }
            }
        }
        return Optional.empty();
    }

    /**
     * Reads and parses a session id from the text of the current element from the passed reader, then creates a corresponding
     * analyze result for the associated database schema.
     * 
     * @param reader The XMLEventReader to read XML events
     * @return An Optional containing the analysis result
     * @throws XMLStreamException If an error occurs while processing XML events
     */
    private Optional<AnalyzeResult> getResultFromSessionId(XMLEventReader reader) throws XMLStreamException {
        String sessionId = reader.getElementText();
        if (Strings.isEmpty(sessionId)) {
            LOGGER.debug("Unable to read session id from {}", reader.peek());
            return Optional.of(AnalyzeResult.UNKNOWN);
        }
        try {
            Session session = sessiondServiceSupplier.get().peekSession(sessionId);
            if (session == null) {
                LOGGER.debug("No session with id {} found.", sessionId);
                return Optional.of(AnalyzeResult.UNKNOWN);
            }
            String schema = (String) session.getParameter(Session.PARAM_USER_SCHEMA);
            if (schema == null) {
                LOGGER.debug("Parameter {} not found in session {}.", Session.PARAM_USER_SCHEMA, sessionId);
                return Optional.ofNullable(AnalyzeResult.UNKNOWN);
            }
            return Optional.of(new AnalyzeResult(SegmentMarker.of(schema.toString()), null));
        } catch (OXException e) {
            LOGGER.warn("Error getting schema from session {}", sessionId, e);
            return Optional.of(AnalyzeResult.UNKNOWN);
        }
    }

}
