package com.openexchange.admin.soap.request.analyzer;

import java.util.Optional;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.server.ServiceLookup;

/**
 * {@link UserCopyServiceRequestAnalyzer} is a {@link RequestAnalyzer} which uses the context id or context name
 * of the destination context element to determine the marker for the request
 *
 * @author <a href="mailto:marcel.broecher@open-xchange.com">Marcel Broecher</a>
 */
public class UserCopyServiceRequestAnalyzer extends AbstractSOAPRequestAnalyzer {

    private static final QName DEST = new QName("http://soap.copy.user.admin.openexchange.com", "dest");

    /**
     * Initializes a new {@link UserCopyServiceRequestAnalyzer}.
     *
     * @param services services The service lookup
     */
    public UserCopyServiceRequestAnalyzer(ServiceLookup services) {
        super(services, "OXUserCopyService", null);
    }

    @Override
    protected Optional<AnalyzeResult> analyzeXMLBody(XMLEventReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement() && DEST.equals(event.asStartElement().getName())) {
                return getResultFromContextElement(reader);
            }
        }
        return Optional.empty();
    }
}
