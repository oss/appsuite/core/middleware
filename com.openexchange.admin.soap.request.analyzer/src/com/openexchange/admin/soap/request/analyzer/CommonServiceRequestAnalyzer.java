package com.openexchange.admin.soap.request.analyzer;

import java.util.Optional;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.server.ServiceLookup;

/**
 * {@link CommonServiceRequestAnalyzer} is a {@link RequestAnalyzer} which uses the context id or context name
 * to determine the marker for the request
 *
 * @author <a href="mailto:marcel.broecher@open-xchange.com">Marcel Broecher</a>
 */
public class CommonServiceRequestAnalyzer extends AbstractSOAPRequestAnalyzer {

    /** <code>{http://soap.admin.openexchange.com}ctx</code> */
    private static final QName QNAME_CTX = new QName("http://soap.admin.openexchange.com", "ctx");

    /** <code>{http://soap.admin.openexchange.com}context</code> */
    private static final QName QNAME_CONTEXT = new QName("http://soap.admin.openexchange.com", "context");

    /** <code>{http://soap.admin.openexchange.com}contextId</code> */
    private static final QName QNAME_CONTEXTID = new QName("http://soap.admin.openexchange.com", "contextId");

    /**
     * Initializes a new {@link AbstractSOAPRequestAnalyzer}.
     * 
     * @param services A service lookup reference
     * @param serviceName The name of the web service to be compared with the URL of incoming analyze requests
     */
    public CommonServiceRequestAnalyzer(ServiceLookup services, String serviceName) {
        this(services, serviceName, null);
    }

    /**
     * Initializes a new {@link AbstractSOAPRequestAnalyzer}.
     * 
     * @param services A service lookup reference
     * @param serviceName The name of the web service to be compared with the URL of incoming analyze requests
     * @param ignoredOperationNames An optional set of operation names that should be ignored when present as child elements of the SOAP body
     */
    public CommonServiceRequestAnalyzer(ServiceLookup services, String serviceName, Set<QName> ignoredOperationNames) {
        super(services, serviceName, ignoredOperationNames);
    }

    @Override
    protected Optional<AnalyzeResult> analyzeXMLBody(XMLEventReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();

            if (event.isStartElement()) {
                QName qName = event.asStartElement().getName();
                if (isIgnoredOperation(qName)) {
                    return Optional.of(AnalyzeResult.UNKNOWN); // Element is part of non-supported operations, return AnalyzeResult.UNKNOWN
                }

                if (QNAME_CONTEXT.equals(qName) || QNAME_CTX.equals(qName) || QNAME_CONTEXTID.equals(qName)) {
                    return getResultFromContextElement(reader);
                }
            }
        }
        return Optional.empty();
    }

}
