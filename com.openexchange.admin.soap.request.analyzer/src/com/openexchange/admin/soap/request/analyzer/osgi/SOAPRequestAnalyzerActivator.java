/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.soap.request.analyzer.osgi;

import java.util.Set;
import javax.xml.namespace.QName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.admin.soap.request.analyzer.CommonServiceRequestAnalyzer;
import com.openexchange.admin.soap.request.analyzer.SessionServiceRequestAnalyzer;
import com.openexchange.admin.soap.request.analyzer.UserCopyServiceRequestAnalyzer;
import com.openexchange.config.ConfigurationService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.sessiond.SessiondService;

/**
 * {@link SOAPRequestAnalyzerActivator}
 *
 * @author <a href="mailto:marcel.broecher@open-xchange.com">Marcel Broecher</a>
 */
public class SOAPRequestAnalyzerActivator extends HousekeepingActivator {

    private static final Logger LOG = LoggerFactory.getLogger(SOAPRequestAnalyzerActivator.class);

    /**
     * Initializes a new {@link SOAPRequestAnalyzerActivator}.
     */
    public SOAPRequestAnalyzerActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { ConfigurationService.class, SessiondService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        LOG.info("Starting bundle {}", context.getBundle().getSymbolicName());
        try {
            registerService(RequestAnalyzer.class, new CommonServiceRequestAnalyzer(this, "OXContextService", Set.of(
                new QName("http://soap.admin.openexchange.com", "create"),
                new QName("http://soap.admin.openexchange.com", "listbyDatabase"),
                new QName("http://soap.admin.openexchange.com", "listbyFilestore"),
                new QName("http://soap.admin.openexchange.com", "listPageByDatabase"),
                new QName("http://soap.admin.openexchange.com", "listPagebyFilestore"))));
            registerService(RequestAnalyzer.class, new CommonServiceRequestAnalyzer(this, "OXGroupService"));
            registerService(RequestAnalyzer.class, new CommonServiceRequestAnalyzer(this, "OXResourceService"));
            registerService(RequestAnalyzer.class, new CommonServiceRequestAnalyzer(this, "OXSecondaryAccountService"));
            registerService(RequestAnalyzer.class, new CommonServiceRequestAnalyzer(this, "OXTaskMgmtService"));
            registerService(RequestAnalyzer.class, new CommonServiceRequestAnalyzer(this, "OXUserService"));
            registerService(RequestAnalyzer.class, new UserCopyServiceRequestAnalyzer(this));
            registerService(RequestAnalyzer.class, new SessionServiceRequestAnalyzer(this));
        } catch (Exception e) {
            LOG.error("Error starting bundle {}", context.getBundle().getSymbolicName());
            throw e;
        }
    }

    @Override
    protected void stopBundle() throws Exception {
        LOG.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        super.stopBundle();
    }
}
