/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.soap.request.analyzer;

import static com.openexchange.java.Autoboxing.I;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.apache.cxf.staxutils.StaxUtils;
import com.openexchange.admin.rmi.exceptions.NoSuchObjectException;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.storage.interfaces.OXToolStorageInterface;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.BodyData;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.request.analyzer.RequestURL;
import com.openexchange.segment.SegmentMarker;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * {@link SOAPRequestAnalyzer} - A {@link RequestAnalyzer} for requests against SOAP end-points.
 *
 * @author <a href="mailto:marcel.broecher@open-xchange.com">Marcel Broecher</a>
 */
public abstract class AbstractSOAPRequestAnalyzer implements RequestAnalyzer {

    protected static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(AbstractSOAPRequestAnalyzer.class);

    /** The factory for creating XML readers to analyze incoming SOAP bodies */
    private static final XMLInputFactory XML_INPUT_FACTORY = initXmlInputFactory();

    /** The name of the configuration property for the optional CXF alias base address */
    private static final String BASEADDRESS_PROPERTY = "com.openexchange.soap.cxf.baseAddress";

    /** The expected HTTP method name of the analyzed requests */
    private static final String HTTP_METHOD_POST = "POST";

    /** <code>{http://dataobjects.soap.admin.openexchange.com/xsd}id</code> */
    private static final QName QNAME_DATAOBJECT_ID = new QName("http://dataobjects.soap.admin.openexchange.com/xsd", "id");

    /** <code>{http://dataobjects.soap.admin.openexchange.com/xsd}contextId</code> */
    private static final QName QNAME_DATAOBJECT_CONTEXTID = new QName("http://dataobjects.soap.admin.openexchange.com/xsd", "contextId");

    /** <code>{http://dataobjects.soap.admin.openexchange.com/xsd}name</code> */
    private static final QName QNAME_DATAOBJECT_NAME = new QName("http://dataobjects.soap.admin.openexchange.com/xsd", "name");

    /** <code>{http://schemas.xmlsoap.org/soap/envelope/}Body</code> */
    private static final QName QNAME_BODY = new QName("http://schemas.xmlsoap.org/soap/envelope/", "Body");

    protected final ErrorAwareSupplier<ConfigurationService> configurationServiceSupplier;
    protected final String serviceName;
    protected final Set<QName> nullableDenySet;

    /**
     * Initializes a new {@link AbstractSOAPRequestAnalyzer}.
     * 
     * @param services A service lookup reference
     * @param serviceName The name of the web service to be compared with the URL of incoming analyze requests
     */
    protected AbstractSOAPRequestAnalyzer(ServiceLookup services, String serviceName) {
        this(() -> services.getServiceSafe(ConfigurationService.class), serviceName, null);
    }

    /**
     * Initializes a new {@link AbstractSOAPRequestAnalyzer}.
     * 
     * @param services A service lookup reference
     * @param serviceName The name of the web service to be compared with the URL of incoming analyze requests
     * @param ignoredOperationNames An optional set of operation names that should be ignored when present as child elements of the SOAP body
     */
    protected AbstractSOAPRequestAnalyzer(ServiceLookup services, String serviceName, Set<QName> ignoredOperationNames) {
        this(() -> services.getServiceSafe(ConfigurationService.class), serviceName, ignoredOperationNames);
    }

    /**
     * Initializes a new {@link AbstractSOAPRequestAnalyzer}.
     * 
     * @param configurationServiceSupplier A supplier for the configuration service
     * @param serviceName The name of the web service to be compared with the URL of incoming analyze requests
     * @param ignoredOperationNames An optional set of operation names that should be ignored when present as child elements of the SOAP body
     */
    protected AbstractSOAPRequestAnalyzer(ErrorAwareSupplier<ConfigurationService> configurationServiceSupplier, String serviceName, Set<QName> ignoredOperationNames) {
        super();
        this.nullableDenySet = ignoredOperationNames;
        this.configurationServiceSupplier = configurationServiceSupplier;
        this.serviceName = serviceName;
    }

    @Override
    public Optional<AnalyzeResult> analyze(RequestData data) throws OXException {
        if (!HTTP_METHOD_POST.equals(data.getMethod())) {
            return Optional.empty();
        }

        RequestURL url = data.getParsedURL();
        Optional<String> optPath = url.getPath();
        if (optPath.isEmpty()) {
            return Optional.empty();
        }

        if (!isValidURL(optPath.get())) {
            return Optional.empty();
        }

        Optional<BodyData> body = data.optBody();
        if (body.isEmpty()) {
            return Optional.of(AnalyzeResult.MISSING_BODY);
        }

        try (InputStream xmlData = body.get().getData()) {
            return analyzeXMLData(xmlData);
        } catch (IOException | XMLStreamException e) {
            LOGGER.error("An error occurred when parsing the XML body.", e);
            return Optional.of(AnalyzeResult.UNKNOWN);
        }
    }

    /**
     * Validates the provided {@link RequestURL} against the configured base address if set
     * The validation checks if the URL path matches the expected path based on the service name
     *
     * @param urlPath The SOAP path to be validated
     * @return {@code true} if the URL is valid, {@code false} otherwise
     * @throws OXException If there is an issue with the validation process, such as an invalid URL format
     */
    private boolean isValidURL(String urlPath) throws OXException {
        if (Strings.isEmpty(urlPath)) {
            return false;
        }
        
        // First check if path matches the default service name
        if ("/webservices/".concat(serviceName).equals(urlPath)) {
            return true;
        }
        
        // If base address is configured, construct the expected service address and validate the URL.
        String baseAddress = configurationServiceSupplier.get().getProperty(BASEADDRESS_PROPERTY);
        if (Strings.isEmpty(baseAddress)) {
            return false;
        }
        String serviceAddress = baseAddress + serviceName;
        try {
            // Parse the base address URL and compare paths.
            URL baseAddressURL = new URL(serviceAddress);
            return urlPath.equals(baseAddressURL.getPath());
        } catch (MalformedURLException e) {
            LOGGER.error("Expected URL build from baseaddress property and service name is malformed.", e);
            throw new OXException(e);
        }
    }

    /**
     * Gets a value indicating whether the supplied element name (as derived from an element below the analyzed XML body) is contained in
     * the set of ignored operations or not.
     * 
     * @param elementName The elemenet name to check
     * @return <code>true</code> if the element refers to an ignored operation, <code>false</code>, otherwise
     */
    protected boolean isIgnoredOperation(QName elementName) {
        return null != nullableDenySet && nullableDenySet.contains(elementName);
    }

    /**
     * Analyzes XML data from the provided {@link InputStream} and processes the expected body element.
     *
     * @param xmlData The {@link InputStream} containing the XML data to be analyzed.
     * @return An {@link Optional} containing the analysis result, or {@link Optional#empty()} if no valid result is found.
     * @throws XMLStreamException If an error occurs while processing XML events
     */
    private Optional<AnalyzeResult> analyzeXMLData(InputStream xmlData) throws XMLStreamException {
        XMLEventReader reader = XML_INPUT_FACTORY.createXMLEventReader(xmlData);
        try {
            // Forwards to start of SOAP body
            Optional<StartElement> bodyElement = forwardToStartElement(reader, QNAME_BODY);

            // If no body element was found return AnalyzeResult.UNKNOWN
            if (bodyElement.isEmpty()) {
                return Optional.of(AnalyzeResult.UNKNOWN);
            }

            // Continue with analysis
            return analyzeXMLBody(reader).or(() -> Optional.of(AnalyzeResult.UNKNOWN));
        } finally {
            reader.close();
        }
    }

    /**
     * Analyzes the body element using the provided XMLEventReader and processes relevant elements
     *
     * @param reader The XMLEventReader to read XML events, positioned at the SOAP body element
     * @return An Optional containing the analysis result, or empty if the analysis didn't produce a result
     * @throws XMLStreamException If an error occurs while processing XML events
     */
    protected abstract Optional<AnalyzeResult> analyzeXMLBody(XMLEventReader reader) throws XMLStreamException;

    /**
     * Processes the context element and yields an appropriate analyze result, either based on the context's id or name.
     *
     * @reader The XMLEventReader to read XML events, positioned at the context element
     * @return An Optional containing the analysis result, or empty if the analysis didn't produce a result
     * @throws XMLStreamException If an error occurs while processing XML events
     */
    protected Optional<AnalyzeResult> getResultFromContextElement(XMLEventReader reader) throws XMLStreamException {
        int parentElementDepth = 0;
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()) {

                // Probe name or id properties from context element when in depth 0 
                if (parentElementDepth == 0) {
                    QName name = event.asStartElement().getName();
                    if (QNAME_DATAOBJECT_NAME.equals(name)) {
                        return getResultFromContextName(reader);
                    }
                    if (QNAME_DATAOBJECT_ID.equals(name) || QNAME_DATAOBJECT_CONTEXTID.equals(name)) {
                        return getResultFromContextId(reader);
                    }
                }

                // Proceed reading of subsequent elements, increase depth 
                parentElementDepth++;

            } else if (event.isEndElement()) {

                // Element was consumed, decrease depth
                parentElementDepth--;
            }
        }
        return Optional.empty();
    }

    /**
     * Reads and parses a context name from the text of the current element from the passed reader, then creates a corresponding
     * analyze result for the associated database schema.
     * 
     * @param reader The XMLEventReader to read XML events
     * @return An Optional containing the analysis result
     * @throws XMLStreamException If an error occurs while processing XML events
     */
    protected Optional<AnalyzeResult> getResultFromContextName(XMLEventReader reader) throws XMLStreamException {
        String contextName = reader.getElementText();
        if (Strings.isEmpty(contextName)) {
            LOGGER.debug("Unable to read context name from {}", reader.peek());
            return Optional.of(AnalyzeResult.UNKNOWN);
        }
        String schema;
        try {
            int contextIdByName = OXToolStorageInterface.getInstance().getContextIDByContextname(contextName);
            schema = OXToolStorageInterface.getInstance().getSchemaByContextId(contextIdByName).getScheme();
        } catch (StorageException | NoSuchObjectException e) {
            LOGGER.warn("Error getting database schema from context name {}", contextName, e);
            return Optional.of(AnalyzeResult.UNKNOWN);
        }
        return Optional.of(new AnalyzeResult(SegmentMarker.of(schema), null));
    }

    /**
     * Reads and parses a context id from the text of the current element from the passed reader, then creates a corresponding
     * analyze result for the associated database schema.
     * 
     * @param reader The XMLEventReader to read XML events
     * @return An Optional containing the analysis result
     * @throws XMLStreamException If an error occurs while processing XML events
     */
    protected Optional<AnalyzeResult> getResultFromContextId(XMLEventReader reader) throws XMLStreamException {
        int contextId = Strings.parseUnsignedInt(reader.getElementText());
        if (-1 == contextId) {
            LOGGER.info("Unable to extract context id from {}", reader.peek());
            return Optional.of(AnalyzeResult.UNKNOWN);
        }
        String schema;
        try {
            schema = OXToolStorageInterface.getInstance().getSchemaByContextId(contextId).getScheme();
        } catch (StorageException | NoSuchObjectException e) {
            LOGGER.warn("Error getting database schema from context id {}", I(contextId), e);
            return Optional.of(AnalyzeResult.UNKNOWN);
        }
        return Optional.of(new AnalyzeResult(SegmentMarker.of(schema), null));
    }

    /**
     * Proceeds through the streams XML events until a {@link StartElement} is reached matching the supplied name.
     * 
     * @param reader The underlying XML reader
     * @param nameToMatch The qualified name to match
     * @return The found start element, or {@link Optional#empty()} if none could be found
     * @throws XMLStreamException if there is an error with the underlying XML
     */
    protected static Optional<StartElement> forwardToStartElement(XMLEventReader reader, QName nameToMatch) throws XMLStreamException {
        // Loop through the XML events
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();

            // Check if the current event is a start element
            if (!event.isStartElement()) {
                continue;
            }
            StartElement startElement = event.asStartElement();
            if (nameToMatch.equals(startElement.getName())) {
                return Optional.of(startElement);
            }
        }
        return Optional.empty();
    }

    /**
     * Initializes and configured the {@link XMLInputFactory} to use.
     * 
     * @return The initialized {@link XMLInputFactory}
     */
    private static XMLInputFactory initXmlInputFactory() {
        XMLInputFactory factory = StaxUtils.createXMLInputFactory(true);
        factory.setProperty(XMLInputFactory.IS_COALESCING, Boolean.valueOf(true));
        return factory;
    }

}
