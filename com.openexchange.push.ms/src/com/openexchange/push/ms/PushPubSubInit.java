/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.push.ms;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONCoercion;
import org.json.JSONObject;
import org.json.JSONServices;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.ChannelKey;
import com.openexchange.pubsub.ChannelListener;
import com.openexchange.pubsub.ChannelMessageCodec;
import com.openexchange.pubsub.PubSubExceptionCode;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.pubsub.core.CoreChannelApplicationName;
import com.openexchange.pubsub.core.CoreChannelName;

/**
 * {@link PushPubSubInit} - Initializes the messaging-based push bundle.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class PushPubSubInit {

    /** The codec for channel messages */
    private static final ChannelMessageCodec<Map<String, Object>> CODEC = new ChannelMessageCodec<>() {

        @Override
        public String serialize(Map<String, Object> message) throws Exception {
            return ((JSONObject) JSONCoercion.coerceToJSON(message)).toString();
        }

        @Override
        public Map<String, Object> deserialize(String data) throws Exception {
            return (Map<String, Object>) JSONCoercion.coerceToNative(JSONServices.parseObject(data));
        }

    };

    private final AtomicReference<Channel<Map<String, Object>>> publishChannelReference;
    private final AtomicReference<ChannelListener<Map<String, Object>>> subscriberReference;
    private final AtomicReference<DelayPushQueue> delayPushQueueReference;
    private final ChannelMessageCodec<Map<String, Object>> codec;

    /**
     * Initializes a new {@link PushPubSubInit}.
     */
    public PushPubSubInit() {
        super();
        publishChannelReference = new AtomicReference<>();
        subscriberReference = new AtomicReference<>();
        delayPushQueueReference = new AtomicReference<>();
        codec = new ChannelMessageCodec<Map<String, Object>>() {

            @Override
            public String serialize(Map<String, Object> message) throws Exception {
                return ((JSONObject) JSONCoercion.coerceToJSON(message)).toString();
            }

            @Override
            public Map<String, Object> deserialize(String data) throws Exception {
                return (Map<String, Object>) JSONCoercion.coerceToNative(JSONServices.parseObject(data));
            }

        };
    }

    /**
     * Gets the channel to publish messages to.
     *
     * @return The channel
     */
    public Channel<Map<String, Object>> getPublishChannel() {
        return publishChannelReference.get();
    }

    /**
     * Gets the channel listener receiving incoming messages.
     *
     * @return The channel listener
     */
    public ChannelListener<Map<String, Object>> getSubscriber() {
        return subscriberReference.get();
    }

    /**
     * Get the delaying push queue.
     *
     * @return The delaying push queue.
     */
    public DelayPushQueue getDelayPushQueue() {
        return delayPushQueueReference.get();
    }

    /**
     * Initializes the messaging-based push bundle.
     *
     * @throws OXException If initialization fails
     */
    public synchronized void init() throws OXException {
        Channel<Map<String, Object>> publishChannel = this.publishChannelReference.get();
        DelayPushQueue delayPushQueue = this.delayPushQueueReference.get();
        if (null != publishChannel && delayPushQueue != null) {
            // Already initialized
            return;
        }

        publishChannel = this.publishChannelReference.get();
        if (null == publishChannel) {
            try {
                PubSubService pubSubService = Services.getService(PubSubService.class);
                if (null == pubSubService) {
                    throw PubSubExceptionCode.UNEXPECTED_ERROR.create("Missing service: " + PubSubService.class.getName());
                }

                ChannelKey channelKey = pubSubService.newKey(CoreChannelApplicationName.CORE, CoreChannelName.OPEN_XCHANGE_EVENTS);
                publishChannel = pubSubService.getChannel(channelKey, CODEC);
                final PushChannelListener listener = new PushChannelListener();
                publishChannel.subscribe(listener);
                this.publishChannelReference.set(publishChannel);
                subscriberReference.set(listener);
            } catch (RuntimeException e) {
                throw PubSubExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
            }
        }

        delayPushQueue = this.delayPushQueueReference.get();
        if (delayPushQueue == null) {
            LeanConfigurationService configService = Services.getService(LeanConfigurationService.class);
            if (null == configService) {
                throw PubSubExceptionCode.UNEXPECTED_ERROR.create("Missing service: " + LeanConfigurationService.class.getName());
            }
            int delayDuration = configService.getIntProperty(PushPubSubProperties.DELAY_DURATION);
            int maxDelays = configService.getIntProperty(PushPubSubProperties.MAY_DELAY_DURATION);
            delayPushQueue = new DelayPushQueue(publishChannel, delayDuration, maxDelays).start();
            this.delayPushQueueReference.set(delayPushQueue);
        }
    }

    /**
     * Shuts-down the messaging-based push bundle.
     */
    public synchronized void close() {
        Channel<Map<String, Object>> publishChannel = this.publishChannelReference.getAndSet(null);
        if (null != publishChannel) {
            final ChannelListener<Map<String, Object>> listener = subscriberReference.get();
            if (null != listener) {
                subscriberReference.set(null);
                try {
                    publishChannel.unsubscribe(listener);
                } catch (Exception e) {
                    // Ignore
                }
            }
        }
        DelayPushQueue delayPushQueue = this.delayPushQueueReference.getAndSet(null);
        if (delayPushQueue != null) {
            delayPushQueue.close();
        }
    }

}
