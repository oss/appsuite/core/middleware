/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.push.ms;

import static com.openexchange.java.Autoboxing.I;
import java.util.Collections;
import java.util.Map;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import com.openexchange.event.EventFactoryService;
import com.openexchange.event.RemoteEvent;
import com.openexchange.groupware.Types;
import com.openexchange.pubsub.ChannelListener;
import com.openexchange.pubsub.Message;
import com.openexchange.server.ServiceLookup;

/**
 * {@link PushChannelListener} - The {@link ChannelListener channel listener} for messaging-based push bundle.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class PushChannelListener implements ChannelListener<Map<String, Object>> {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(PushChannelListener.class);

    /**
     * Initializes a new {@link PushChannelListener}.
     */
    public PushChannelListener() {
        super();
    }

    @Override
    public void onMessage(final Message<Map<String, Object>> message) {
        if (!message.isRemote()) {
            LOG.debug("Locally recieved PushMsObject. Ignore...");
            return;
        }

        final PushPubSubObject pushObj = PushPubSubObject.valueFor(message.getData());
        if (null == pushObj) {
            LOG.debug("Received null from channel. Ignore...");
            return;
        }

        LOG.debug("Remotely received PushMsObject: {}", pushObj);
        final ServiceLookup registry = Services.getServiceLookup();
        final EventAdmin eventAdmin = registry.getService(EventAdmin.class);
        if (eventAdmin != null) {
            final EventFactoryService eventFactoryService = registry.getService(EventFactoryService.class);
            if (eventFactoryService != null) {
                final int action;
                final String topicName;
                if (pushObj.getModule() == Types.FOLDER) {
                    action = RemoteEvent.FOLDER_CHANGED;
                    topicName = "com/openexchange/remote/folderchanged";
                } else {
                    action = RemoteEvent.FOLDER_CONTENT_CHANGED;
                    topicName = "com/openexchange/remote/foldercontentchanged";
                }
                for (final int user : pushObj.getUsers()) {
                    /*-
                     * Post event to current user
                     *
                     *       See com.openexchange.usm.ox_event.impl.OXEventManagerImpl.handleEvent(Event)
                     *       See com.openexchange.usm.session.impl.SessionManagerImpl.folderContentChanged(int, int, String, long)
                     */
                    final RemoteEvent remEvent = eventFactoryService.newRemoteEvent(
                        pushObj.getFolderId(),
                        user,
                        pushObj.getContextId(),
                        action,
                        pushObj.getModule(),
                        pushObj.getTimestamp());
                    final Map<String, RemoteEvent> ht = Collections.singletonMap(RemoteEvent.EVENT_KEY, remEvent);
                    eventAdmin.postEvent(new Event(topicName, ht));
                    LOG.debug("Posted remote event to user {} in context {}: {}", I(user), I(pushObj.getContextId()), remEvent);
                }
            }
        }
    }

}
