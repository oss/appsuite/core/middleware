/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.push.ms;

import com.openexchange.config.lean.Property;

/**
 * {@link PushPubSubProperties} - Properties for push events.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public enum PushPubSubProperties implements Property {

    UDP_ENABLED("udp.pushEnabled", Boolean.FALSE),

    DELAY_DURATION("ms.delayDuration", Integer.valueOf(120000)),

    MAY_DELAY_DURATION("ms.maxDelayDuration", Integer.valueOf(600000))

    ;

    private final String fqn;
    private final Object defaultValue;

    private PushPubSubProperties(String appendix, Object defaultValue) {
        this.fqn = "com.openexchange.push." + appendix;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }
}
