/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.filestore.utils;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import com.openexchange.ajax.container.ThresholdFileHolder;
import com.openexchange.exception.OXException;
import com.openexchange.java.Streams;

/**
 * {@link UploadChunk} - Represents an upload chunk.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @since v7.8.2
 */
public class UploadChunk implements Closeable {

    /** The backing file holder */
    protected final ThresholdFileHolder fileHolder;

    /** The chunk's size */
    protected final long size;

    /**
     * Initializes a new {@link UploadChunk} served by the supplied file holder.
     *
     * @param fileHolder The underlying file holder
     */
    public UploadChunk(ThresholdFileHolder fileHolder) {
        this(fileHolder, fileHolder.getCount());
    }

    /**
     * Initializes a new {@link UploadChunk} served by the supplied file holder.
     *
     * @param fileHolder The underlying file holder
     * @param size The chunk's size
     */
    public UploadChunk(ThresholdFileHolder fileHolder, long size) {
        super();
        this.fileHolder = fileHolder;
        this.size = size;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    public long getSize() {
        return size;
    }

    /**
     * Gets the data.
     *
     * @return The data
     * @throws OXException
     */
    public InputStream getData() throws OXException {
        if (fileHolder.getLength() <= 0) {
            return Streams.EMPTY_INPUT_STREAM;
        }

        ByteArrayOutputStream buffer = fileHolder.getBuffer();
        if (buffer != null) {
            return Streams.asInputStream(buffer);
        }

        File tempFile = fileHolder.getTempFile();
        if (tempFile == null) {
            IOException e = new IOException("Already closed.");
            throw OXException.general("An I/O error occurred", e);
        }

        try {
            return new FileInputStream(tempFile);
        } catch (FileNotFoundException e) {
            throw OXException.general("An I/O error occurred", e);
        }
    }

    /**
     * Gets the optional temporary file to which data has been written.
     *
     * @return The temporary file or empty
     */
    public Optional<File> getTempFile() {
        if (fileHolder.getLength() <= 0) {
            return Optional.empty();
        }
        return Optional.ofNullable(fileHolder.getTempFile());
    }

    @Override
    public void close() throws IOException {
        Streams.close(fileHolder);
    }

}
