# App Suite Middleware

All notable changes to this project will be documented in this file.




## [8.35] - 2025-02-12

### Added

- [`SCR-1486`](https://jira.open-xchange.com/browse/SCR-1486): New property `com.openexchange.carddav.addressbookMultigetLimit` [`dced5468`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dced54680b96f0144ae7f5efc832d6d34419e3d3)
- Added async provisioning framework [`35b89cf8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/35b89cf80ae1a399cb6a31c3bedf0b3b1f7f3d62)
- [`#134`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/134): Health Check for Redis Cache - /appsuite/platform/core#134 [`cb4b20fd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cb4b20fdfe89a342f046fc9913fea7448cf8e58f)
- [`#137`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/137): Expose CardDAV URL for Address Book Folders - /appsuite/platform/core#137 [`185ab623`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/185ab623038996781dbad36f5f58e17049dc4990)
- [`#138`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/138): Warm-up Thread-local Cache when Handling Client Requests - /appsuite/platform/core#138 [`59b383b7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/59b383b732564488be546410ef8f9342e7abf84e)
- [`#139`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/139): Control Sharding for Redis Cluster - /appsuite/platform/core#139 [`39594fad`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/39594fadb2c21384d018295be68a02a94a90315e)
- [`#144`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/144): Fixed existing graphs and added new ones based on redis cache metrics - /appsuite/platform/core#144 [`22e1f263`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/22e1f263f741e52da6972dc039eebb4df07cdffb)


### Changed

- Add shared-read-only flag to snippet module to have shared snippets that may be seen, but must not be modified/deleted by other users [`1c7e0b79`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1c7e0b799a8a86f0bbdffdcb1393342d4d48bc5e)
- Batch-resolve resources when handling CARDDAV:addressbook-multiget request [`e5d88cea`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e5d88ceaae60713cb5f8566f02240c7576abd14c)
- INU-4548: Support configurable custom flag for mails [`068cf122`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/068cf122c6bd44b2d0570b14b178af652868cea9)
- [`SCR-1496`](https://jira.open-xchange.com/browse/SCR-1496): Enhanced OAuthAuthorizationService#validateAccessToken with Header collection parameter - /appsuite/platform/core#74 [`e2f67eb3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e2f67eb38d442d2afa47f4982497a79e8c574cf2)
- [`SCR-1510`](https://jira.open-xchange.com/browse/SCR-1510): Changed defaults for Client-Onboarding YAML configuration file [`ff520286`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ff5202861ea951596e2ee36da2374617861d04e0)
- [`SCR-1511`](https://jira.open-xchange.com/browse/SCR-1511): Update Apache Commons Codec from v.1.17.0 to v1.17.2 [`22bc2e48`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/22bc2e489007e6ab271c0f0cdc88bb6440bc46d8)
- [`SCR-1512`](https://jira.open-xchange.com/browse/SCR-1512): Updated Apache Commons Codec library from v1.6.0 to v1.9.0 [`97cf880e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/97cf880ee67b4f8a0c4d70e398a8b72c2cffc2ef)
- [`SCR-1513`](https://jira.open-xchange.com/browse/SCR-1513): Update Apache Commons CSV from v1.6 to v1.13.0 [`b6e9162b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b6e9162bc7053f4fb9ddff64cc3c444bae2b0782)
- [`SCR-1514`](https://jira.open-xchange.com/browse/SCR-1514): Limit number of addressed keys per MGET operation (SCR-1514) - /appsuite/support#340 [`2ddb4815`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2ddb48158c387ce952f29884995b38b988c26089)
- [`SCR-1522`](https://jira.open-xchange.com/browse/SCR-1522): Updated Snappy library from v1.1.10.5 to v1.1.10.7 [`7b1667e0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7b1667e01d54e08f0a1c4e3c7573ef4a21d8f5d4)
- Added appropriate OAuth scopes to mail compose actions [`f573ffe3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f573ffe39eb5cfe4c59be7dbad01ba696712ae57)
- Allow to overwrite securityContext [`633bd9ef`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/633bd9ef3c907de0f22a196735d59a74786e0241)
- Skip capability check and don't trigger preview generation if not needed [`fc3410fd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fc3410fd00229c94403352d3d583a7966cc65274)
- Updated collabora and gotenberg dependency [`0c27b6ce`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0c27b6ce41bf1bc520aca1fe86d5b201c2d86deb)
- Changed copyright year to 2025 [`918cc6eb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/918cc6eb9e624491504817653a8969b943e03853)
- Removed db defaults from values [`0e9687e8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0e9687e8cc53f27c441e5f112df18c90b5171990)
  - Removed the db defaults so that they can be overriden by global entries
  - Also adjusted the mysql secret to use those defaults in case nothing is configured
- [`#62`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/62): Gather subfolder ids for search from condition tree map - /appsuite/platform/core#62 [`9e45d258`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9e45d2581614efc916a9228e4e53b9e89baec55d)
- [`#142`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/142): Disable Global Folder Cache by Default - /appsuite/platform/core#142 [`061688e4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/061688e486da304ed928d50195ff8fd502099f9b)
- [`#156`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/156): Redirect to configured failure redirect location after erroneous token response - /appsuite/platform/core#156 [`1da98a5a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1da98a5a366306f0b92f56a9d89bbc353157616a)
- [`#163`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/163): Updated to newest SLF4j and Logback libraries - appsuite/platform/core#163 [`350f77e9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/350f77e992d03d327d499654a3b5630496d251e4)
- [`#169`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/169): Update cxf-libraries to 3.5.10 (CVE-2025-23184) - appsuite/platform/core#169 [`32ee4852`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/32ee485283027e1ab9560dcb035a9e973fc91b40)
- [`#171`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/171): Added documentation article for database cleanup jobs - /appsuite/platform/core#171 [`584fb812`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/584fb8127f5faa8abc23c3f586f966496b0fc129)
- [`#415`](https://gitlab.open-xchange.com/appsuite/support/-/issues/415): Subscribe/unsubscribe IMAP folder if deputy permission is granted/revoked - /appsuite/support#415 [`23e516da`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/23e516daa5a6e690bd5d54f3d87952c1ea9c9bdb) [`74df2fb4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/74df2fb489d3630d6b1a1f7e3987b279de86bb9e)
- [`#697`](https://gitlab.open-xchange.com/appsuite/web-apps/ui/-/issues/697): Use more compact token format - /appsuite/web-apps/ui#697 [`d3a42a18`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d3a42a18b0b5387302dac3ad3a1eb9bd4abe9a46)


### Fixed

- Connect CLT param `access-denied-portal` to code #189 [`fc3907a8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fc3907a84567d8daf1f59691f4d3478ba9118593)
- IIJMX-554: Don't use IMAP folder's sequence number when sorting by received date is performed in application [`d0ec138c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d0ec138ca348f4e57240b93fb3b14f667ceaaa5a)
- INU-4767: Improved handling of corrupt address list [`dde1c354`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dde1c35452350e38707791788fdfcc97d67a3abc)
- Only check conflicts for added/updated attendees unless re-scheduled [`9a8cdb59`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9a8cdb59767ee02f1461082e3f6775e3efe5f58a)
- PLG-450: Continue if visible folders cannot be collected for non-default contacts account [`bed1aa6b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bed1aa6bb42360e6800ae9c7e7e80c24d75e41ad)
- Fixed main by removing commons-cli-1.6.0 from target platform [`97ecdfe4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/97ecdfe4c94db4c1e955647ae9fec726eddcd7b1)
- Adjusted documentation for 'flags' parameter [`0a200e04`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0a200e0466332ef8fff62648126bb136547afe03)
- Sorting of distribution list by first name #159 [`9c227c1c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9c227c1c587676dd02a613e243dbca2682f75696)
- Allow deletion of user attributes via json null values [`aa90258d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/aa90258d0aab3a06414695d2943182f3e9873b25)
- Cache invalidator instance to prevent excessive pub key requests [`7d0b14c5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7d0b14c53f139c4eeb665db5445cca52b1794ac5) [`e66fc312`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e66fc312a7d4a90fff693e6e1b5c1aad7b14f7c4)
- Changed docu links to oss repo [`d7d42e84`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d7d42e84c1dc6780ca1c3030a74d885a12df8822)
- Enable test on borrow for redis connection pool [`f8d5be57`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f8d5be573ad29fcba18fb10dc45d0e8872bdf505)
- Fixed AbstractAdvertisementConfigServiceTest [`f301738b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f301738b161e9c919dba56ab31041b4acc98060c)
- Made flagging documentation more independent of the client [`8f0c48b9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8f0c48b9dfcda543d4e9520c6d47a092a6dda947)
- Properly store host parameter during login [`c02c5045`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c02c50457154dc6919a8174f4f3f0379fcef1517)
- [`#62`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/62): Include system type when gathering subfolders for infostore search - /appsuite/platform/core#62 [`57f6fa30`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/57f6fa30912e5cdfe9859fa7f08ee1fdd12bd39e)
- [`#130`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/130): Skip initial reachability check for remote redis connections - /appsuite/platform/core#130 [`654a44e0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/654a44e008ec6cae926138a5df6f533dbc0c1ced)
- [`#135`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/135): Fall back to storage access if accessing cache yields errors - appsuite/platform/core#135 [`fedb75c9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fedb75c9c60327ea11e0cbedf8c09a59462e9c3b)
- [`#140`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/140): Corrected link in Contacts Provider LDAP documentation article - /appsuite/platform/core#140 [`4d0e012c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4d0e012c90e98bd406e0c92bdbd7461f99129e6c)
- [`#141`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/141): Use only one cache region for advertisment config - /appsuite/platform/core#141 [`76287ed5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/76287ed55c598de2d61a86a9dfecc884da40541b)
- [`#143`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/143): Handle empty JSON input as no available value is Redis cache - appsuite/platform/core#143 [`132c9186`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/132c9186f31c53520443677adf40f5a69fed03cd)
- [`#147`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/147): Hard-delete mail(s) if located in trash folder - /appsuite/platform/core#147 [`986b872f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/986b872f12fd4534bcd88bd034d183ac350c71f1)
- [`#155`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/155): Don't attempt to invalidate caches if no group members are set - /appsuite/platform/core#155 [`0459beba`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0459beba01d287913b3ec3da1fe1ad359dcb2a6f)
- [`#162`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/162): Explicitly use UTF-8 charset when writing vCards - /appsuite/platform/core#162 [`da0558ca`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/da0558cab689cfbcc0068d2e4700a0fbf1e9b31c)
- [`#168`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/168): Handle possibly absent password when checking subscribed mail account's status - /appsuite/platform/core#168 [`bc29ddb5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bc29ddb539e7fa9539a3646231cdb3c5c517a47d)
- [`#170`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/170): Prevent from invalidating a remote node's JSlob entry if it has been stored after local node's entry - /appsuite/platform/core#170 [`7a0b1413`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7a0b1413ea3f49a1f3b9c73f5b5c96b0679f4d09) [`764f684f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/764f684fca7719346b4a1a7dd1ed9729f5d77c2e)
- [`#175`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/175): Pick proper mail from conversation when performing sort - /appsuite/platform/core#175 [`4e951b71`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4e951b71b4603fc4d3718bb7160af8c0fc8aeeb5)
- [`#179`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/179): Ignore unmappable ACLs when updating an IMAP folder's ACLs - /appsuite/platform/core#179 [`e8b25370`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e8b25370e310b060542fd8dee4b4a6858506c17d)
- [`#180`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/180): More look-up attempts in case of missing draft - /appsuite/platform/core#180 [`907c5286`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/907c52861915f93b32263b134be9464caf402e3f)
- [`#183`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/183): Fixed SQL 'INSERT INTO ... ON DUPLICATE KEY UPDATE' statement - /appsuite/platform/core#183 [`7c290330`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7c29033094400ccc2cb06f87abaaef2fe0b7e73f)
- [`#185`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/185): Evaluate CARDDAV:limit in CARDDAV:addressbook-query Report - /appsuite/platform/core#185 [`0c0a3eb4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0c0a3eb49be250a7664e006c59d755e9ec8c05b0)
- [`#186`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/186): Consider all known 'devices' for CalDAV/CardDAV onboarding providers - /appsuite/platform/core#186 [`0de5043a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0de5043a95a69b59e0717199cc8b10936e703bed)
- [`#187`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/187): Added config switch to keep own address when replying to self-sent message - /appsuite/platform/core#187 [`2bec6d40`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2bec6d4072885f52515e1128ebb5c42904284058)


## [8.34] - 2025-01-15

### Added

- PLG-360: Added entry for new feature 'open-xchange-plugins-contact-storage-provider' [`a2388168`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a2388168ca4cb95995ba9786aeb067951217b6e8)
- [`SCR-1477`](https://jira.open-xchange.com/browse/SCR-1477): New property to select default collection for new contacts via iOS / CardDAV (SCR-1477) - /appsuite/platform/core#111 [`807f8f65`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/807f8f65ed4bb3a6d17d34dc15df658ec710e4f4)
- [`SCR-1486`](https://jira.open-xchange.com/browse/SCR-1486): New property 'com.openexchange.carddav.addressbookMultigetLimit' [`dced5468`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dced54680b96f0144ae7f5efc832d6d34419e3d3)
- [`#134`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/134): Health Check for Redis Cache - /appsuite/platform/core#134 [`cb4b20fd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cb4b20fdfe89a342f046fc9913fea7448cf8e58f)
- [`#137`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/137): Expose CardDAV URL for Address Book Folders - /appsuite/platform/core#137 [`185ab623`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/185ab623038996781dbad36f5f58e17049dc4990)
- [`#138`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/138): Warm-up Thread-local Cache when Handling Client Requests - /appsuite/platform/core#138 [`59b383b7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/59b383b732564488be546410ef8f9342e7abf84e)
- [`#139`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/139): Control Sharding for Redis Cluster - /appsuite/platform/core#139 [`39594fad`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/39594fadb2c21384d018295be68a02a94a90315e)
- [`#144`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/144): Fixed existing graphs and added new ones based on redis cache metrics - /appsuite/platform/core#144 [`22e1f263`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/22e1f263f741e52da6972dc039eebb4df07cdffb)
- [`#344`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/344): Documentation for Redis cache in "cluster" mode - /appsuite/support#344 [`053907a2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/053907a213091a0273e48a6bb772c26471e733ea)


### Changed

- Additional `add-opens` for health monitor in `JAVA_OPTS_OPENS` [`809d4d69`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/809d4d698f7543c47c07455909743476eb9e195b)
- Batch-resolve resources when handling CARDDAV:addressbook-multiget request [`e5d88cea`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e5d88ceaae60713cb5f8566f02240c7576abd14c)
- [`MW-2012`](https://jira.open-xchange.com/browse/MW-2012): Allow moving appointment series to other calendars [`9cdf4674`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9cdf46741451ee1224c6288133b5a2893761aed3)
- [`SCR-1479`](https://jira.open-xchange.com/browse/SCR-1479): Updated Netty libraries from v4.1.114 to v4.1.115 [`46bc8634`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/46bc86343e2a67c41fb26c6067ad1a8d64648962)
- [`SCR-1480`](https://jira.open-xchange.com/browse/SCR-1480): Updated lettuce library from v6.5.0 to v6.5.1 [`70d335a4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/70d335a44d89efbdbdb52c67e61670289c9eed37)
- [`SCR-1481`](https://jira.open-xchange.com/browse/SCR-1481): Updated Fabric8 libraries from v6.10.0 to v6.13.4 [`a8b2bd86`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a8b2bd86ee45070ae53e29c7a886e78907810415)
- [`SCR-1482`](https://jira.open-xchange.com/browse/SCR-1482): Added redis.tls chart value - appsuite/platform/core#123 [`fea38f75`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fea38f754e9561f04681f44067272a8e6d2ee156)
- [`SCR-1485`](https://jira.open-xchange.com/browse/SCR-1485): New options for the Redis Connector [`8604bb1d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8604bb1d5d2d6adcca67a33c3831f31e76e2405f)
- [`SCR-1496`](https://jira.open-xchange.com/browse/SCR-1496): Enhanced OAuthAuthorizationService#validateAccessToken with Header collection parameter - /appsuite/platform/core#74 [`e2f67eb3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e2f67eb38d442d2afa47f4982497a79e8c574cf2)
- Type-specific redis configuration [`ca71e1d2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ca71e1d252f656d4793df17aed1fbd99f72e9c4f)
- [Redis Cache] Added support for putting multiple key-value-pairs into cache [`f4a910da`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f4a910da8aaefbbc429106364b3e542e91ec6888)
- Added appropriate OAuth scopes to mail compose actions [`f573ffe3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f573ffe39eb5cfe4c59be7dbad01ba696712ae57)
- Skip capability check and don't trigger preview generation if not needed [`fc3410fd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fc3410fd00229c94403352d3d583a7966cc65274)
- Updated collabora and gotenberg dependency [`0c27b6ce`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0c27b6ce41bf1bc520aca1fe86d5b201c2d86deb)
- [`#62`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/62): Allow to fetch multiple values at once from Redis cache - /appsuite/platform/core#62 [`5d5061e5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5d5061e5a3f4f692882294c1f52e034039a22971) [`9df6895c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9df6895ce62c5014363767eebd55d071c84725d6) [`9e45d258`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9e45d2581614efc916a9228e4e53b9e89baec55d)
- [`#65`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/65): Allow specifying the name of the HTTP header that forwards the originating remote port - appsuite/platform/core#65 [`02bee688`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/02bee688470928d1794595d1004393efbab1e5af)
- [`#102`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/102): Make mail-related actions un-doable - /appsuite/platform/core#102 [`2c7db142`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2c7db1424305cfc2650f1349912acd78f11c02d0) [`23b032bd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/23b032bd6e93f4331c0f04341f89d6299224b5d5) [`a9853a68`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a9853a682db6120a6034d107b46b6d2349cdbb6f)
- [`#114`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/114): Added check when storing user feedback if actually enabled as per configuration - /appsuite/platform/core#114 [`8df5b5ca`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8df5b5ca924f85013a07d436eb26bba989e1ce6d)
- [`#120`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/120): Added additional example for LDAP client configurations - /appsuite/platform/core#120 [`159a0807`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/159a0807abae5af046fe5b5d9b1ae6f8f3ba947a)
- [`#142`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/142): Disable Global Folder Cache by Default - /appsuite/platform/core#142 [`061688e4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/061688e486da304ed928d50195ff8fd502099f9b)
- [`#156`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/156): Redirect to configured failure redirect location after erroneous token response - /appsuite/platform/core#156 [`1da98a5a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1da98a5a366306f0b92f56a9d89bbc353157616a)
- [`#340`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/340): Use shortcut when checking calendar/contacts provider capability - /appsuite/support#340 [`a6b56856`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a6b56856d86011686a6e072c0e4a7125ce184ee7) [`f20ea710`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f20ea710a2356cea93247140d35745022f3fd451)


### Fixed

- IIJMX-554: Don't use IMAP folder's sequence number when sorting by received date is performed in application [`d0ec138c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d0ec138ca348f4e57240b93fb3b14f667ceaaa5a)
- INU-4767: Improved handling of corrupt address list [`dde1c354`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dde1c35452350e38707791788fdfcc97d67a3abc)
- Only check conflicts for added/updated attendees unless re-scheduled [`9a8cdb59`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9a8cdb59767ee02f1461082e3f6775e3efe5f58a)
- PLG-450: Continue if visible folders cannot be collected for non-default contacts account [`bed1aa6b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bed1aa6bb42360e6800ae9c7e7e80c24d75e41ad)
- Set correct dependencies for update task - appsuite/support/350 [`f3da1e30`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f3da1e3038f7a6218c8e3bd60d579a413d83aef1)
- Improved skiptoken matching. Ref. #116 [`95642e67`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/95642e6773fb6c1e7352a4a0295f48ba742c5020)
- Ensure compatibility with mysql 8+. Ref. #131 [`9f4edcc5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9f4edcc59299e2caa1c669ed67ee4eaa7063c6e2)
  - Removed unnecessary default value from the propertyValue column
- Fixed AbstractAdvertisementConfigServiceTest [`f301738b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f301738b161e9c919dba56ab31041b4acc98060c)
- [`#113`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/113): Continue import w/o attachments on absence of 'filestore' capability - /appsuite/platform/core#113 [`60d4732e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/60d4732e61fe51f0c67d28c66af1afb35ab90ca1)
- [`#121`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/121): Re-added formerly dropped method from Cache interface through a delegate implementation - /appsuite/platform/core#121 [`e69917bb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e69917bb53a6735fd690f706b290befd6d51e10d)
- [`#122`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/122): Keep subsequent space characters through quoting - /appsuite/platform/core#122 [`0ff626f0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0ff626f032e386213da48f15e3c8e1fea2c6db26)
- [`#129`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/129): Remember upload chunk size beyond underlying file holder's validity - /appsuite/platform/core#129 [`f81de9c5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f81de9c51c0e0d2ab66b4d58ed3720d3d2271f76)
- [`#130`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/130): Skip initial reachability check for remote redis connections - /appsuite/platform/core#130 [`654a44e0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/654a44e008ec6cae926138a5df6f533dbc0c1ced)
- [`#135`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/135): Fall back to storage access if accessing cache yields errors - appsuite/platform/core#135 [`fedb75c9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fedb75c9c60327ea11e0cbedf8c09a59462e9c3b)
- [`#140`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/140): Corrected link in Contacts Provider LDAP documentation article - /appsuite/platform/core#140 [`4d0e012c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4d0e012c90e98bd406e0c92bdbd7461f99129e6c)
- [`#141`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/141): Use only one cache region for advertisment config - /appsuite/platform/core#141 [`76287ed5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/76287ed55c598de2d61a86a9dfecc884da40541b)
- [`#143`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/143): Handle empty JSON input as no available value is Redis cache - appsuite/platform/core#143 [`132c9186`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/132c9186f31c53520443677adf40f5a69fed03cd)
- [`#147`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/147): Hard-delete mail(s) if located in trash folder - /appsuite/platform/core#147 [`986b872f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/986b872f12fd4534bcd88bd034d183ac350c71f1)
- [`#155`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/155): Don't attempt to invalidate caches if no group members are set - /appsuite/platform/core#155 [`0459beba`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0459beba01d287913b3ec3da1fe1ad359dcb2a6f)


## [8.33] - 2024-12-18

### Added

- [`PLG-360`](https://jira.open-xchange.com/browse/PLG-360): Added entry for new feature 'open-xchange-plugins-contact-storage-provider' [`a2388168`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a2388168ca4cb95995ba9786aeb067951217b6e8)
- [`SCR-1477`](https://jira.open-xchange.com/browse/SCR-1477): New property to select default collection for new contacts via iOS / CardDAV (SCR-1477) - /appsuite/platform/core#111 [`807f8f65`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/807f8f65ed4bb3a6d17d34dc15df658ec710e4f4)
- [`#344`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/344): Documentation for Redis cache in "cluster" mode - /appsuite/support#344 [`053907a2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/053907a213091a0273e48a6bb772c26471e733ea)


### Changed

- Additional "add-opens" for health monitor in "JAVA_OPTS_OPENS" [`809d4d69`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/809d4d698f7543c47c07455909743476eb9e195b)
- [`MW-2012`](https://jira.open-xchange.com/browse/MW-2012): Allow moving appointment series to other calendars [`9cdf4674`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9cdf46741451ee1224c6288133b5a2893761aed3)
- [`SCR-1479`](https://jira.open-xchange.com/browse/SCR-1479): Updated Netty libraries from v4.1.114 to v4.1.115 [`46bc8634`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/46bc86343e2a67c41fb26c6067ad1a8d64648962)
- [`SCR-1480`](https://jira.open-xchange.com/browse/SCR-1480): Updated lettuce library from v6.5.0 to v6.5.1 [`70d335a4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/70d335a44d89efbdbdb52c67e61670289c9eed37)
- [`SCR-1481`](https://jira.open-xchange.com/browse/SCR-1481): Updated Fabric8 libraries from v6.10.0 to v6.13.4 [`a8b2bd86`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a8b2bd86ee45070ae53e29c7a886e78907810415)
- [`SCR-1482`](https://jira.open-xchange.com/browse/SCR-1482): Added redis.tls chart value - appsuite/platform/core#123 [`fea38f75`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fea38f754e9561f04681f44067272a8e6d2ee156)
- [`SCR-1485`](https://jira.open-xchange.com/browse/SCR-1485): New options for the Redis Connector [`8604bb1d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8604bb1d5d2d6adcca67a33c3831f31e76e2405f)
- Type-specific redis configuration [`ca71e1d2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ca71e1d252f656d4793df17aed1fbd99f72e9c4f)
- Redis Cache: Added support for putting multiple key-value-pairs into cache [`f4a910da`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f4a910da8aaefbbc429106364b3e542e91ec6888)
- Added appropriate OAuth scopes to mail compose actions [`25ca51bc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/25ca51bc3c0933aabde7d643305c566043f7b51d)
- [`#62`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/62): Allow to fetch multiple values at once from Redis cache - /appsuite/platform/core#62 [`5d5061e5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5d5061e5a3f4f692882294c1f52e034039a22971) [`9df6895c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9df6895ce62c5014363767eebd55d071c84725d6)
- [`#102`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/102): Make mail-related actions un-doable - /appsuite/platform/core#102 [`2c7db142`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2c7db1424305cfc2650f1349912acd78f11c02d0) [`23b032bd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/23b032bd6e93f4331c0f04341f89d6299224b5d5) [`a9853a68`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a9853a682db6120a6034d107b46b6d2349cdbb6f)
- [`#114`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/114): Added check when storing user feedback if actually enabled as per configuration - /appsuite/platform/core#114 [`8df5b5ca`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8df5b5ca924f85013a07d436eb26bba989e1ce6d)
- [`#120`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/120): Added additional example for LDAP client configurations - /appsuite/platform/core#120 [`159a0807`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/159a0807abae5af046fe5b5d9b1ae6f8f3ba947a)
- [`#340`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/340): Use shortcut when checking calendar/contacts provider capability - /appsuite/support#340 [`a6b56856`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a6b56856d86011686a6e072c0e4a7125ce184ee7) [`f20ea710`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f20ea710a2356cea93247140d35745022f3fd451)


### Fixed

- Set correct dependencies for update task - appsuite/support/350 [`f3da1e30`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f3da1e3038f7a6218c8e3bd60d579a413d83aef1)
- Improved skiptoken matching. Ref. #116 [`95642e67`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/95642e6773fb6c1e7352a4a0295f48ba742c5020)
- Ensure compatibility with mysql 8+. Ref. #131 [`9f4edcc5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9f4edcc59299e2caa1c669ed67ee4eaa7063c6e2)
  - Removed unnecessary default value from the propertyValue column
- [`#113`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/113): Continue import w/o attachments on absence of 'filestore' capability - /appsuite/platform/core#113 [`60d4732e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/60d4732e61fe51f0c67d28c66af1afb35ab90ca1)
- [`#121`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/121): Re-added formerly dropped method from Cache interface through a delegate implementation - /appsuite/platform/core#121 [`e69917bb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e69917bb53a6735fd690f706b290befd6d51e10d)
- [`#122`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/122): Keep subsequent space characters through quoting - /appsuite/platform/core#122 [`0ff626f0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0ff626f032e386213da48f15e3c8e1fea2c6db26)
- [`#129`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/129): Remember upload chunk size beyond underlying file holder's validity - /appsuite/platform/core#129 [`f81de9c5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f81de9c51c0e0d2ab66b4d58ed3720d3d2271f76)


## [8.32] - 2024-11-20

### Added

- [`SCR-1470`](https://jira.open-xchange.com/browse/SCR-1470): Added new lean property to control detection of inline images [`bbea0f54`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bbea0f544e3d3734142ca9c8f69f9725d8a582cb)
- [`#21`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/21); New setting for preferred Calendar User Address #21 [`ba9e4fd9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ba9e4fd97fc4e70b6accc4630d7d088d714cfbf4)
- [`#90`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/90): SSO Logout when OX Sessions are closed through "Sign out from all devices" - /appsuite/platform/core#90 [`b2940eee`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b2940eee838ea1304d7125b6fb8780e306ba0289)


### Changed

- [`MW-2254`](https://jira.open-xchange.com/browse/MW-2254): Introduced baselayer image build [`034f278f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/034f278fc1a379eca2e093d5f329a99b6a360924)
- [`MWB-2245`](https://jira.open-xchange.com/browse/MWB-2245): Avoid duplicate -XX:MaxHeapSize set via helm chart [`1500ffdd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1500ffdd58cd0af79f19e5b3a5c6054b8efc2198)
- [`MWB-2614`](https://jira.open-xchange.com/browse/MWB-2614): Use `BadPassword` exception for failed decryption [`1822715c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1822715ce4ef93ab90378c3baf3d70c23d76f94d)
    - Also, log legacy encryption warning only once per day
- [`SCR-1462`](https://jira.open-xchange.com/browse/SCR-1462): Added new property to track Redis operation taking longer than a configured threshold [`fcaa5081`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fcaa5081c9e4c35d7587f7e12a2de852a88ac32a)
- [`SCR-1472`](https://jira.open-xchange.com/browse/SCR-1472): Updated Netty libraries from v4.1.112 to v4.1.114 [`cb0e08ca`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cb0e08cac8973c7ef15bfb8cb85fe96630585582)
- [`SCR-1473`](https://jira.open-xchange.com/browse/SCR-1473): Updated lettuce library from v6.4.0 to v6.5.0 [`374b02a7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/374b02a724511820e5adec7c46af756f8bce339b) [`269f72d4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/269f72d4e735388c3d71d4d158fc3658f6390e2b)
- [`SCR-1476`](https://jira.open-xchange.com/browse/SCR-1476): Updated several libraries to update Jackson libraries from v2.16.1 to v2.18.1 [`9ccc5f1a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9ccc5f1a69ca9d3aa5769e3ca29f3cbe15844c3a)
- Adapted clearing Java's DNS cache to new supported Java version(s) [`e2b49305`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e2b49305352a8924b6e9e5a942576566b9f96cf8)
- Signal support for mail folder resynchronization & enhanced /mail?action=examine response to also include modseq if supported [`f875eba3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f875eba34253d4fa473dc0ae7a5475348baca5c3)
- Updated the gotenberg image from 8.2.0 to 8.12.0 and the chart from 1.1.0 to 1.7.0 [`476a2207`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/476a22071a98ee6083fee10d6c70e208c65e8deb)
- [`#43`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/43): Optionally skip de-registration of push listeners during user/context delete - appsuite/platform/core#43 [`6adee17c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6adee17c755c807abaaffd7eb9683b863d909ad2)
- [`#65`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/65): Allow specifying the name of the HTTP header that forwards the originating remote port - appsuite/platform/core#65 [`8944f5f7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8944f5f7327459122e0c21364a90453be2eaea24) [`94774936`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/94774936cd92034ef0b80cba8931c784d5a75604) [`4aba5203`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4aba5203f1f9279d36f1be6e4113c20e6152c0ff)
- [`#66`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/66): Chunking for S3 file storage - appsuite/platform/core#66 [`cc6ae987`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cc6ae98744abd62defc88719c77455144ec76158) [`39e84d07`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/39e84d07dcb372e9d915f7a91cd94c350a76a28e) [`8827a25a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8827a25a843fcd0a51bae806e5f3415ea8af4a62) [`42a6624f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/42a6624fc78a26edc0253fb4fa0801f7604442dc)
- [`#69`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/69): Check if tables still exist prior purging accounts - appsuite/platform/core#69 [`5db6da56`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5db6da5658ba82290605f68396e2bd9b3dc51c19)
- [`#75`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/75): Update vulnerable libraries [`827c446a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/827c446a18c4c695091efe32a48c9962ee21d5f4)
  - Update libraries xmlrpc-commonclient/server to 5.0.0 (CVE-2016-5004)
  - Update library xmlsec to 2.3.4 (CVE-2023-44483)
  - Update library nimbus-jose-jwt to 9.41.2 (CVE-2023-52428)
  - Update library ion-java to 1.11.9 (CVE-2024-21634)
  - Update library  mysql-connector-j to 8.3.0 (CVE-2023-22102)
  - Update library velocity-engine-core to 2.4 (CVE-2024-47554)
  - Update library protobuf-java to 3.25.5 (CVE-2024-7254)
  - Update library logback-classic/core to 1.2.13 (CVE-2023-6378)
  - Update cxf-libraries to 3.5.9 (CVE-2024-28752)
- [`#76`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/76): Do not perform computation intensive obfuscation/un-obfuscation of sessions' password while holding Redis connection - /appsuite/platform/core#76 [`fa7b834c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fa7b834c4489702ce6daade14fef2b9f52fb7a17) [`59f9c834`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/59f9c834df40fb77ee46452a4686907a9ecd4c2c) [`11cece02`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/11cece0293ef5b2666d49bd1e71ecfe493140aca) [`ec93381b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ec93381b1e319f8d9a2a74219e387bbaf0542bf8)
- [`#80`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/80): Updated documentation about CardDAV collection handling for iOS clients - /appsuite/platform/core#80 [`f9bfa280`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f9bfa280e23e607f29ab9a11e2ed0b901d719255)
- [`#95`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/95): Use more modern values for sun.net.inetaddr.ttl and networkaddress.cache.ttl - /appsuite/platform/core#95 [`8476484f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8476484f11615f6c9a85dba4042c9c03a5073ea5)
- [`#103`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/103): Added trace logging for authentication probes during request analysis - /appsuite/platform/core#103 [`880854f6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/880854f64e49fd182481d805392709309e8c2c23) [`f974d214`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f974d214613fcac2fe8171672cacbb8ab757d4a8)
- [`#109`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/109): Added logging in case a down-/upload request is interrupted or gets killed - /appsuite/platform/core#109 [`c84586f3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c84586f3c74bf99a07f9d9fda86d970632dc5eb3) [`bf1ebcb0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bf1ebcb03e74b847b223980f5410c63996e6d482)
- [`#227`](https://gitlab.open-xchange.com/appsuite/support/-/issues/227): Ignore possible NoSuchFileException - /appsuite/support#227 [`59ec740a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/59ec740a1065bce373dca3d519c95ad7cb883aa6)
- [`#236`](https://gitlab.open-xchange.com/appsuite/support/-/issues/236): Added reference to 'checkTopLevelDomainOnAddressValidation' property in documentation - appsuite/support#236 [`dabcbca5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dabcbca53ce6db58cc7b510bf6446aa2ecbee01a)


### Fixed

- [`MW-2371`](https://jira.open-xchange.com/browse/MW-2371): Added required Zone to DateTimeFormatter [`1d71991e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1d71991ec637b698abae0f945551aa188535ae83) [`6b77a62a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6b77a62aac04a781cae45137690a665e36b0a7ab)
- [`MWB-2435`](https://jira.open-xchange.com/browse/MWB-2435): Do not generate new random values for missing secret env variables on every update [`70ed49a4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/70ed49a4c6c45c177aa149c112499f239c908b4d)
- [`MWB-2631`](https://jira.open-xchange.com/browse/MWB-2631): Inline Metadata Missing in Syncfiles Response [`c603e68b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c603e68b4c28df2322a5e5578ad3c1ab0a6185fb)
- [`MWB-2641`](https://jira.open-xchange.com/browse/MWB-2641): Use correct target DB for moved context [`124a7bf6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/124a7bf66027515d2c096ad6bd6b2ab0d2c96302) [`4f5b8faa`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4f5b8faa884afd6d1afdbc95e50f1d14437fc804)
- Added missing update task dependency to Filestore2UserUpdateReferencesTask [`4f1f52e4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4f1f52e44d688e95bafb5141e0e7617234211a55)
  - See Ref. #55
- Expose remote debug port and use correct role selector [`d4a27d52`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d4a27d522c016c464fd91ca807acd0713793d89c)
- Use hz service name that is defined by the document role [`39de7859`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/39de78595647fa9b2304a5e18a94bb987f60718a)
- Dont run pot generation on main [`e547461e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e547461e77f9ef5630690001bfbba6e1fa30373a)
- Improved regex for password filtering [`dbb1a528`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dbb1a528c8085f49face445dd286e5275ed204b5)
- Move POT to seperate stage again [`be4098e5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/be4098e57f5895812ce201d0c9116a6969a0a1f9)
- Prevent NPE if json error contains no message [`0657a779`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0657a7796f458aa182409e3ee13ac4012ccb5d6a)
- Revert imageBuildPod.yaml changes [`48c482b5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/48c482b571a5815e7831e305fa4307b7651b8b09)
- Use ant container for pot generation [`7436e13a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7436e13ab3479db17a91492bcd90b4e2ccdf9a18)
- Use proper error [`909e279f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/909e279f11670bf13899ccf13f1da9bb87e5f093)
- [`#4`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/4): Pay attention to com.openexchange.push.dovecot.unregisterAfterDelete setting - /appsuite/platform/core#4 [`b6e249e2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b6e249e2d18003be9aafbfca9641b8a96cd171ae)
- [`#68`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/68): Un-mark schema for deletion after deletion fails, ensure to load db connection settings properly - appsuite/platform/core#68 [`e0834696`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e0834696ed351132c55a1d412053d0e6f393743e)
- [`#72`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/72): Do not try to reparse a mail when creating its JSONrepresentation - /appsuite/platform/core#72 [`2f176f3d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2f176f3d0a91442d6f922a5bc801b93095180785)
- [`#76`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/76): Establish a new connection to Redis if a timeout occurs while waiting for a connection to become available in pool - appsuite/platform/core#76 [`7ad17340`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7ad173407c1383c9ea2d700c1e28710e5431bfef) [`94f60ac3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/94f60ac3bb02fddbd7d25332b0fc1f729921c405)
- [`#77`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/77): Properly indicate support for MKCOL in OPTIONS response - /appsuite/platform/core#77 [`2e2d9bf7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2e2d9bf7eaa8d9bed5b1c0f1720f49db91fa93fd)
- [`#79`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/79): Check for valid credentials prior to store attempt - appsuite/platform/core#79 [`37aed19b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/37aed19bf40b6a347d4ff383e571bc8154776df6)
- [`#85`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/85): Determine file name by Content-Type/Content-Disposition headers if not given through Subject header - appsuite/platform/core#85 [`0fa12c8e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0fa12c8e0fd798b9d63488bef11156ce851fe7c9)
- [`#97`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/97): Sanitize UID prior using as 'id-left' in Message-ID header - /appsuite/platform/core#97 [`6f9a4417`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6f9a44170466e7c1d5c73b8d938663f38bc4bb22)
- [`#98`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/98): Fixed NoClassDefFoundError when trying to access Scribe classes - /appsuite/platform/core#98 [`77f2ba9c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/77f2ba9c513fe6808d093b373106802e12b82a4d)
- [`#100`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/100): Encode resource (in)availability in cached representation - /appsuite/platform/core#100 [`438fa052`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/438fa052b612ce51e6ff1846e09880d8c1c087b8)
- [`#101`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/101): Replace illegal characters instead of throwing an error. Ref. #101 [`b382e7cf`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b382e7cf40710d7d8f3b2d0d0cc38f0dc2058fba)
- [`#105`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/105): Don't use unreliable piped streams for task export - /appsuite/platform/core#105 [`25b188e3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/25b188e3bef8313cabc9c419fefa7e1b19f8ad15)
- [`#106`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/106): Properly check key name #106 [`782be787`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/782be78723ec9aa605774ea11065f7f707530fd7)
- [`#107`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/107): Adjusted config docu links in quota docu. Ref. #107 [`79d6963c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/79d6963cba5242756822624c05bbbacb13de40f4)
- [`#110`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/110): Removed summary from request body of folder-modifying requests - /appsuite/platform/core#110 [`84c80b60`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/84c80b608e7c81b6cddd27854a89a5f366b2564a)


## [8.31] - 2024-10-23

### Added

- New setting for preferred Calendar User Address #21 [`ba9e4fd9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ba9e4fd97fc4e70b6accc4630d7d088d714cfbf4)


### Changed

- [`MW-2254`](https://jira.open-xchange.com/browse/MW-2254): Introduced baselayer image build [`034f278f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/034f278fc1a379eca2e093d5f329a99b6a360924)
- [`MWB-2614`](https://jira.open-xchange.com/browse/MWB-2614): Use `BadPassword` exception for failed decryption [`1822715c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1822715ce4ef93ab90378c3baf3d70c23d76f94d)
    - Also, log legacy encryption warning only once per day
- [`SCR-1462`](https://jira.open-xchange.com/browse/SCR-1462): Added new property to track Redis operation taking longer than a configured threshold [`fcaa5081`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fcaa5081c9e4c35d7587f7e12a2de852a88ac32a)
- Signal support for mail folder resynchronization & enhanced `/mail?action=examine` response to also include modseq if supported [`f875eba3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f875eba34253d4fa473dc0ae7a5475348baca5c3)
- [`#43`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/43): Optionally skip de-registration of push listeners during user/context delete - appsuite/platform/core#43 [`6adee17c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6adee17c755c807abaaffd7eb9683b863d909ad2)
- [`#65`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/65): Allow specifying the name of the HTTP header that forwards the originating remote port - appsuite/platform/core#65 [`8944f5f7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8944f5f7327459122e0c21364a90453be2eaea24) [`94774936`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/94774936cd92034ef0b80cba8931c784d5a75604)
- [`#69`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/69): Check if tables still exist prior purging accounts - appsuite/platform/core#69 [`5db6da56`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5db6da5658ba82290605f68396e2bd9b3dc51c19)
- [`#80`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/80): Updated documentation about CardDAV collection handling for iOS clients - /appsuite/platform/core#80 [`f9bfa280`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f9bfa280e23e607f29ab9a11e2ed0b901d719255)
- [`#227`](https://gitlab.open-xchange.com/appsuite/support/-/issues/227): Ignore possible NoSuchFileException - /appsuite/support#227 [`59ec740a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/59ec740a1065bce373dca3d519c95ad7cb883aa6)
- [`#236`](https://gitlab.open-xchange.com/appsuite/support/-/issues/236): Added reference to `checkTopLevelDomainOnAddressValidation` property in documentation - appsuite/support#236 [`dabcbca5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dabcbca53ce6db58cc7b510bf6446aa2ecbee01a)


### Fixed

- [`MW-2371`](https://jira.open-xchange.com/browse/MW-2371): Added required Zone to DateTimeFormatter [`1d71991e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1d71991ec637b698abae0f945551aa188535ae83) [`6b77a62a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6b77a62aac04a781cae45137690a665e36b0a7ab)
- [`MWB-2435`](https://jira.open-xchange.com/browse/MWB-2435): Do not generate new random values for missing secret env variables on every update [`70ed49a4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/70ed49a4c6c45c177aa149c112499f239c908b4d)
- [`MWB-2641`](https://jira.open-xchange.com/browse/MWB-2641): Use correct target DB for moved context [`124a7bf6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/124a7bf66027515d2c096ad6bd6b2ab0d2c96302) [`4f5b8faa`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4f5b8faa884afd6d1afdbc95e50f1d14437fc804)
- Added missing update task dependency to `Filestore2UserUpdateReferencesTask` [`4f1f52e4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4f1f52e44d688e95bafb5141e0e7617234211a55)
  - See Ref. [`#55`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/55)
- Use hz service name that is defined by the document role [`39de7859`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/39de78595647fa9b2304a5e18a94bb987f60718a)
- Dont run pot generation on main [`e547461e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e547461e77f9ef5630690001bfbba6e1fa30373a)
- Move POT to seperate stage again [`be4098e5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/be4098e57f5895812ce201d0c9116a6969a0a1f9)
- Revert imageBuildPod.yaml changes [`48c482b5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/48c482b571a5815e7831e305fa4307b7651b8b09)
- Use ant container for pot generation [`7436e13a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7436e13ab3479db17a91492bcd90b4e2ccdf9a18)
- [`#68`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/#68): Un-mark schema for deletion after deletion fails, ensure to load db connection settings properly - appsuite/platform/core#68 [`e0834696`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e0834696ed351132c55a1d412053d0e6f393743e)
- [`#72`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/#72): Do not try to reparse a mail when creating its JSONrepresentation - /appsuite/platform/core#72 [`2f176f3d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2f176f3d0a91442d6f922a5bc801b93095180785)
- [`#76`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/#76): Establish a new connection to Redis if a timeout occurs while waiting for a connection to become available in pool - appsuite/platform/core#76 [`7ad17340`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7ad173407c1383c9ea2d700c1e28710e5431bfef) [`94f60ac3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/94f60ac3bb02fddbd7d25332b0fc1f729921c405)
- [`#77`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/#77): Properly indicate support for MKCOL in OPTIONS response - /appsuite/platform/core#77 [`2e2d9bf7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2e2d9bf7eaa8d9bed5b1c0f1720f49db91fa93fd)
- [`#79`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/#79): Check for valid credentials prior to store attempt - appsuite/platform/core#79 [`37aed19b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/37aed19bf40b6a347d4ff383e571bc8154776df6)


## [8.30] - 2024-09-25

### Added

- [`MW-1068`](https://jira.open-xchange.com/browse/MW-1068): Improve support for SCHEDULE-AGENT [`2fd626d0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2fd626d03a3a5f67b69e0d409b7a9cde28d6ba3c)
- [`SCR-1454`](https://jira.open-xchange.com/browse/SCR-1454): New configuration property "com.openexchange.cache.v2.redis.disableHashExpiration" [`323c4fa1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/323c4fa1227608c322512c6a4b4608772eeeafd0)


### Changed

- IIJMX-530: Don't fail if UNSUBSCRIBE fails for IMAP-backed POP3 folders [`a13200d0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a13200d07431c02e81aaaaac99bbffee3b6ef77c)
- [`MW-2191`](https://jira.open-xchange.com/browse/MW-2191): Upgraded to Java v21 [`baa040d1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/baa040d1ff9dbf73da7af7b067be949e7097ee9d)
- [`MW-2309`](https://jira.open-xchange.com/browse/MW-2309): Use new caches for schema update states and db assignments [`0c626dd2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0c626dd25139f8c10c42e17d645df56782fbc3be)
- [`MW-2330`](https://jira.open-xchange.com/browse/MW-2330): Replaced old context cache with the new v2 [`3c718354`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3c718354794908e97fff9b039cd06f7ea99f26d1)
- [`MW-2335`](https://jira.open-xchange.com/browse/MW-2335): Transform "MailAccount" Cache to Redis [`13c84cf9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/13c84cf9b8c6390e847ccc367dd9f4774673ad70)
- [`MW-2338`](https://jira.open-xchange.com/browse/MW-2338): Removed "messaging"-related functionality and APIs [`8cc8df6f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8cc8df6f4985e4361194d96374ae029bc0e95f93) [`705f5c82`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/705f5c823ced617e1de8db6e7f8351799e1c6a19)
- [`MW-2347`](https://jira.open-xchange.com/browse/MW-2347): Use new cache for advertisement config caching [`60bacf4b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/60bacf4bd80443a0749c135ab9527c5fededb6bb)
- [`MW-2363`](https://jira.open-xchange.com/browse/MW-2363): Removed legacy SessionD and Hazelcast-backed session storage [`39d5b717`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/39d5b717062f981d62efe785b22d20f1531ce072)
- [`MW-2368`](https://jira.open-xchange.com/browse/MW-2368): Disable Hazelcast by Default [`de5e5633`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/de5e56330075060a64f0737cf72c40ebbd036057)
   - *Breaking Change*
- [`SCR-1452`](https://jira.open-xchange.com/browse/SCR-1452): Moved Java Data Objects (JDO) API to target platform [`ec8ad9d1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ec8ad9d193fb536fccd73e84f9f5301c3b250e61)
- Some refactoring for the code packages in c.o.cache.v2 [`64cb03b6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/64cb03b6fc4c783ab1d6f82eae9bf801808ff678)
- [`#17`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/17): Reintroduced soap log filter for passwords - appsuite/platform/core#17 [`92cdfb97`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/92cdfb97cfc59dbed6f078d88d84fc730da3d26b)


### Fixed

- Avoid duplicate service instance for singleton LeanConfigurationService [`66a48fbf`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/66a48fbf1444b459bbc35a257fdf91d1feba6ca5)
- Perform synchronous unregistration from Dovecot HTTP Notify plugin using a lowered socket read timeout [`6a4f87bb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6a4f87bb319bc24209d0ce7befcdd0977e565fd9)
- [`#23`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/23): Reverted some checks for pretty old messages that should not occur - appsuite/platform/core#23 [`9fa35f10`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9fa35f106bd49f4e12c467b23fecddf67c84d927)
- [`#25`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/25): Restored enhanced conflict checks during attendee update operation - appsuite/platform/core#25 [`7114c95f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7114c95f68b5749e9a3c18b3e67a6d91b59707fb)
- [`#29`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/29): Check mail messages reference retrieved from cache prior iterating - appsuite/platform/core#29 [`ac674a24`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ac674a24444c1d5aa0dd30e4173be0b41f8330a0)
- [`#30`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/30): Update local file storage account reference after remembering error in metadata - appsuite/platform/core#30 [`6677a246`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6677a24687fc175d0610cf620d469604af363091)
- [`#31`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/31): Prevent NPE during shutdown. Ref. [`12413312`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1241331228fcf2f73a170f5f7874d5a2ff5427d9)
- [`#32`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/32): Added auto-expiration of possible write lock for table filestore2user - appsuite/platform/core#32 [`46c2abb6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/46c2abb66cd3af037d7569d695551ed9abf14dd0) [`52c202c9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/52c202c983d7a32c263d2a276ae0f43dad3c0802) [`53873901`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/53873901a87e0d7622cbe8bee1840531e37b0645)
- [`#33`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/33): Throw proper error if mail is not found. Ref. [`251098c8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/251098c844bfacdb47711fa9e92bae192802bb23)
- [`#36`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/36): Properly apply HTTP client config for iCal timezone updater - appsuite/platform/core#36 [`814c70a8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/814c70a81eea2fb53bb53975bcad70691dae1768)
- [`#39`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/39): Don't retry login attempt by default (w/ exponential back-off wait policy) when IMAP server signals UNAVAILABLE response code - appsuite/platform/core#39 [`2b5a3191`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2b5a31910df591513b128dff52c7de2bad305659)
- [`#40`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/40): Keep user-sensitive information for isSubscribed() and getUsedForSync() - /appsuite/platform/core#40 [`e2ae5bac`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e2ae5bacf83f554b2240474a368686b32658f839)
- [`#43`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/43): Unregister user from Dovecot HTTP Notify plugin asynchronously - appsuite/platform/core#43 [`f00456a1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f00456a18f7351e1b155923b017375dc66414633)
- [`#44`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/44): Moved creation of reseller tables to accessing bundles [`a6cad0b7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a6cad0b7527934ce4a8eebd81b50e2141180668e)
- [`#48`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/48): Retry fetching thread-sorted messages if an inconsistent received date is detected - appsuite/platform/core#48 [`8270853b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8270853b95534f32ca367b83e67f9647a92290ae) [`381f39c5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/381f39c5894a7b665e5db5d54e9b813a2164a610)
- [`#49`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/49): Prevent possible NPE when replacing organizer in a calendar event - appsuite/platform/core#49 [`58fbb18f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/58fbb18f6a2198625569cd3dbc981bc839b8fccb)
- [`#54`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/54): Added missing org.xml.sax dependency to MANIFEST.MF [`bbe6bf28`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bbe6bf2837c1f97583c46c5d9747aa7639a28349)
- [`#562`](https://gitlab.open-xchange.com/appsuite/web-apps/ui/-/issues/562): Differentiate between deputy feature being 'enabled' and 'available' - appsuite/web-apps/ui#562 [`83447b85`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/83447b85c6246a2f53fa339fa2d609c30dcedd3b)


### Removed

- [`MW-2361`](https://jira.open-xchange.com/browse/MW-2361): Remove com.openexchange.ms.MsService [`7e495202`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7e4952028de69e09172034249922168417cc8c2b)


## [8.29] - 2024-08-28

### Added

- [`MW-2319`](https://jira.open-xchange.com/browse/MW-2319): Token Login Variant for PWA Onboarding [`99832baa`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/99832baa22711648c7a7f893033d69d686f84d76)
   - *Breaking Change*: The file `tokelogin-secrets` has been *removed*. See also:
   - [`SCR-1405`](https://jira.open-xchange.com/browse/SCR-1405): Renamed parameter in `/login?action=redeemToken`
   - [`SCR-1406`](https://jira.open-xchange.com/browse/SCR-1406): Added a client defined expiration time to `/token?action=acquireToken`
   - [`SCR-1407`](https://jira.open-xchange.com/browse/SCR-1407): Replaced `tokenlogin-secrets` file with lean configuration
- [`MW-2323`](https://jira.open-xchange.com/browse/MW-2323): Adjust Helm Charts for Redis 7.4 [`9ca0b47f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9ca0b47fab6024ae725e174b43d5931b37787cfb)
- [`MW-2339`](https://jira.open-xchange.com/browse/MW-2339): Deputy-Management via Provisioning API [`c2073664`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c207366457008786606404d7bba59b9fc209c8a3)
- [`MW-2353`](https://jira.open-xchange.com/browse/MW-2353): Documentation for Redis and Active/Active [`3409b456`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3409b4561af1a52d24f80c2d8ff3de2a2793c63a)


### Changed

- [`MW-2043`](https://jira.open-xchange.com/browse/MW-2043): Upgraded scribe library to v8.3.3 [`8918dd8a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8918dd8ae44fe7ae0da99cc8b05d0a848fde057e)
- [`MW-2360`](https://jira.open-xchange.com/browse/MW-2360): Added support for a special Redis instance dedicated for cache data [`78ef57c2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/78ef57c29849fee1791cc2ca00fea5fd9b096aaf)
- [`MW-2365`](https://jira.open-xchange.com/browse/MW-2365): Adjusted hazelcast documentation [`f6780571`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f67805714c74d66d9d406555156ef39ead3af861)
- [`MWB-2698`](https://jira.open-xchange.com/browse/MWB-2698): Introduced aliases for sieve script names [`418def09`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/418def099d3a4b9b3812a1e8b3bbdbec8187df64)
- [`SCR-1425`](https://jira.open-xchange.com/browse/SCR-1425): Removed Replacement of "email 1" by "default sender address" [`1c15bd25`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1c15bd25f64e026f8d3d45c3b81820861920dd82)
- [`SCR-1428`](https://jira.open-xchange.com/browse/SCR-1428): Deprecation of JCS-based "CacheService" [`792aee72`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/792aee72fa0c13378c873f0fd11bb4597388479b)
- [`SCR-1432`](https://jira.open-xchange.com/browse/SCR-1432): Updated Netty libraries from v4.1.111 to v4.1.112 [`c19e0a6f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c19e0a6fe0626d4953f6380db0cf4d783e64b979)
- [`SCR-1433`](https://jira.open-xchange.com/browse/SCR-1433): Updated lettuce library from v6.3.2 to v6.4.0 [`173351e1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/173351e164ecc485607a3cf1020f0f09807535c3)
- [`SCR-1440`](https://jira.open-xchange.com/browse/SCR-1440): Updated OSGi target platform bundles [`5f2c4c34`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5f2c4c349f9740230631a5d4c35dfa0eb7fb8622)
- [`SCR-1441`](https://jira.open-xchange.com/browse/SCR-1441): New Configuration Property "com.openexchange.oidc.staySignedIn" [`e254b475`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e254b47589008019a9aac427240ef700336ffac9)
- Inject 'preferred' hint for user's primary mail address within [`9b32b026`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9b32b0265b10d08e92bf89a790baf966e9465468)
- [`SCR-1448`](https://jira.open-xchange.com/browse/SCR-1448): Upgraded dnsjava from v3.5.3 to v3.6.1 [`77bdeed6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/77bdeed6b9216755dde2e87820579314e1bf371b)
- [`SCR-1450`](https://jira.open-xchange.com/browse/SCR-1450): Removed org.eclipse.osgi.services helper bundle [`200225de`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/200225de96522d35ddb36d7476811b939ea17068)
- [`SCR-1451`](https://jira.open-xchange.com/browse/SCR-1451): Updated Google Guava from v33.2.1 to v33.3.0 [`94b1f970`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/94b1f97099d07cb382259e1a48e7c2e8ff687fa7)
- Avoid recursive delete calls if PluginException occurred (2) - [`194688e3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/194688e3a89bbef8e79ab3930adaec1b30757cf2)
- Use compact representation for folder user properties [`6e59f30d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6e59f30d25093636a92967f07ead681753e27c7d)
- Use reverse order in `CalDAV:calendar-user-address-set` property for Thunderbird/Lightning
[`df5ffa78`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/df5ffa784e3f19d844bdb8716b4adb2c6e81d4c3)


### Fixed

- Added checks for pretty old messages that should not occur [`#23`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/23) [`bbb60b00`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bbb60b00c210a6848368eefadb8979e0fd97539f)
- Consider possible MX records on ISPDB look-up for a possible mail account setup [`#24`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/24) [`a305df9b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a305df9b01f665efa58addbcaa346e9702de6816)
- Optimized reading schema state (update tasks) [`#10`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/10) [`8cb20be4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8cb20be4ffd08b63311f4654b1a34d25c61a7568) [`2ecd9d47`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2ecd9d475c300ba44f07ca2bed3068b7c5a345f9)
- Defer dependent capability checks [`47f8b86d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/47f8b86df424b6fcd05e8170dc5ca2aa95b559fe)
- Inject fallback display name for *DAV-based subscriptions if missing [`#22`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/22) [`a6e0cbe0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a6e0cbe069ceacce1ab33b0bca656e01c5e7f553)
- [`MWB-2512`](https://jira.open-xchange.com/browse/MWB-2512): Use 'no-reply' account for synthetic push sessions [`6a623131`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6a623131da70adcfa114ab5c8a0eecd15563b4fa)
- [`MWB-2530`](https://jira.open-xchange.com/browse/MWB-2530): Orderly compile path to a shared folder from target user's point of view [`5898d475`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5898d475c1cfa2b0e47220fe8ffc5b509c0c82f1)
- [`MWB-2667`](https://jira.open-xchange.com/browse/MWB-2667): Deny writing rules requiring unsupported capabilities [`55dfa3ba`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/55dfa3bac35f957b0690fb79c44a360f3885d60b) [`8c0c8602`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8c0c8602d398729fc87fa0984fd32afc22ba3155)
- [`MWB-2701`](https://jira.open-xchange.com/browse/MWB-2701): Replace possible space characters in URLs with appropriate URL encoded representation (%20) [`ec8a08f4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ec8a08f49a34b26f9a2980abb9b01acb24c05940)
- [`MWB-2702`](https://jira.open-xchange.com/browse/MWB-2702): Properly compare user mail aliases with punycode [`0f78cb94`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0f78cb948adcddfbe7888e403679fecbf1a71e0f)
- [`MWB-2705`](https://jira.open-xchange.com/browse/MWB-2705): Fixed writing JSlob IDs as JSON array to channel [`f8178b31`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f8178b31188ac6f5566835bcb7c6e9a529d81120)
- Orderly track new cache service in IMAP bundle [`#8`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/8)  [`6a991f77`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6a991f77c53ca1bdf6a12ccdc89eaaadff815c07)


### Removed

- [`MW-2367`](https://jira.open-xchange.com/browse/MW-2367): Remove Xing integration [`52ec3fa3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/52ec3fa3f1b96d6744f61c45500451992d53c526)
- [`SCR-1426`](https://jira.open-xchange.com/browse/SCR-1426): Removed "FilteringObjectStreamFactory" Service and parent Bundle "com.openexchange.serialization" [`3e051826`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3e051826fa244de5d9aec9870f75f03a72aadb59)


## [8.28] - 2024-07-31

### Added

- [`MW-2286`](https://jira.open-xchange.com/browse/MW-2286): Added end-point & handling for site changed event [`34bb1ebf`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/34bb1ebf2e6f75bb167a711f0e64be6f8436e06e)


### Changed

- [`App-suite-platform-1/provisioning#2`](https://gitlab.open-xchange.com/appsuite/platform/app-suite-platform-1/provisioning/-/issues/2): Avoid recursive delete calls if PluginException occurred [`d2e17ef2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d2e17ef24b42fb331a82c93ac8a0ac331e4af808)
- [`MW-1746`](https://jira.open-xchange.com/browse/MW-1746): Redis Pub/Sub for Inter-Node Communication [`b6cba2af`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b6cba2af3b9fbea0cb26601a4a33a3e8856cf981)
- [`MW-2329`](https://jira.open-xchange.com/browse/MW-2329): Use new cache for filestores [`6c414a99`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6c414a9993588803c5d8adc3178ab034494bccea)
- [`MW-2341`](https://jira.open-xchange.com/browse/MW-2341): Transform "UserSettingMail" and "UserPermissionBits" Cache to Redis [`a13444a5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a13444a5e1c6767c10fa82a21de68c03c1d90ca5)
- [`MW-2342`](https://jira.open-xchange.com/browse/MW-2342): Transform "User" Cache to Redis [`1958a34e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1958a34e131d85033e68df81d2298025d66d8b01)
- [`MW-2344`](https://jira.open-xchange.com/browse/MW-2344): Replaced jcs cache with google cache [`13b86258`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/13b862585a02ffd668a60eaff12d4287521209e0)
- [`MWB-2674`](https://jira.open-xchange.com/browse/MWB-2674): Try to add further information to created `MAIL_FILTER-0002` error to signal why authentication attempt failed [`7b659b20`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7b659b2050bf5f2d285716416bd2d1d9bf00546c)
- [`MWB-2690`](https://jira.open-xchange.com/browse/MWB-2690): Avoid "CopyPartRequest" and ensure `x-amz-meta-x-amz-unencrypted-content-length` is set when S3 client-side encryption is enabled [`d922d83a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d922d83aea5c86eb042ed069c13f78b7e2d0abb1)
- [`SCR-1415`](https://jira.open-xchange.com/browse/SCR-1415): Updated Google Guava from `v33.0.0` to `v33.2.1` [`2ba7b1d6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2ba7b1d6365b48d1031fbfd1431d8a7aec204d6e)
- [`SCR-1419`](https://jira.open-xchange.com/browse/SCR-1419): Upgraded ROME library for RSS and Atom feeds from `v1.0` to `v1.19.0` [`199a4b1f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/199a4b1fda0af0b28bf41e4220e381708b55ba86) [`1c6e5dcd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1c6e5dcd6ddc1be6201e630de88b95897f564730) [`bb4a7e40`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bb4a7e406bbed1f8a6b9799c5ce54dd667041241)
- [`SCR-1421`](https://jira.open-xchange.com/browse/SCR-1421): Deprecation of "FilteringObjectStreamFactory" Service [`2bfd73cb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2bfd73cbb8461e33d26cd85ba13246c69e04b900)
- Consolidated *DAV capability checks [`bb6bb3ce`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bb6bb3ced6ff30b92e529b70652f5731c149ddd2)
- Use logback-extension `v3.0.3` to have removal of details in superior class [`8dbdab0c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8dbdab0c758114a57ba7e181566bf03dcef7eb3a) [`db5854e9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/db5854e9b819ece244e347ea2f32acd5c729eac7)
- Added retry-after header to 429 soap errors [`4121cf95`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4121cf9523f396519e7ddec19c193a8f18f92531)
- Alternative caching approach for file storage accounts [`5416cd99`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5416cd99c206287975609c5ffdc1c6f02c372504)
- Change the color and font of property fields in the http-api documentation [`c6cbabfb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c6cbabfbfcdd4db79f392394f8be94fb506c00f1)


### Fixed

- [`IAE-59`](https://jira.open-xchange.com/browse/IAE-59): Include client IP address in meta information for a scheduled mail [`97b12d03`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/97b12d03cee476e101b562313de85125eed1f7d7) [`b6b09a38`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b6b09a384a3e7a955b2251b6b46179e4fb49759a)
- [`MWB-2645`](https://jira.open-xchange.com/browse/MWB-2645): Abort auto-login if user/context in session not found [`a808257e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a808257e08a5c4d6b43e5e8524430ccdb1ed5264)
- [`MWB-2664`](https://jira.open-xchange.com/browse/MWB-2664): Don't log job's execution duration if nothing has been performed [`63dbf1cf`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/63dbf1cf9f00eaab8a3318c59bb206c18dfc0184) [`3144c949`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3144c949e1cbbd00e50ac2a99b875d0afa6c5e12) [`0695d5df`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0695d5df2fa667be62a2b4876996f1ebc2fece35) [`54646f35`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/54646f3596bc7641b59e556090c060203c9feb0e) [`7fd6139e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7fd6139ee7ecb2755c78ad16b96247b9cfa1ced7)
- [`MWB-2681`](https://jira.open-xchange.com/browse/MWB-2681): Properly initialize hash map [`8e284bfa`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8e284bfa2dc64ad2caf672284b93a8dad199a381) [`10032ed6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/10032ed610ad16afe6a854fbb7cd7a8b372bcac7)
- [`MWB-2682`](https://jira.open-xchange.com/browse/MWB-2682): Consider configuration when writing cookie in generic *DAV servlet [`45be1d86`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/45be1d86fc3d6ea72d4296d1daa5c2ae2d85515c)
- [`MWB-2686`](https://jira.open-xchange.com/browse/MWB-2686): Restore previous behaviour in case non-existent context is passed [`2ed9d00a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2ed9d00a7e5f04cce91cbefa38b9bd9d080b4cda)
- [`MWB-2687`](https://jira.open-xchange.com/browse/MWB-2687): Extended propertyKey column to 256 chars [`5c123570`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5c12357083e873fe13ccd3136356ca8a1c964e03)
- [`MWB-2692`](https://jira.open-xchange.com/browse/MWB-2692): Check specified iCal feed URI has a valid host component [`f45df772`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f45df7726c513180c3de935bfa3096ed0256fc7c)
- [`Core#12`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/12): Added missing update tasks (and changesets) to documentation [`e723395d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e723395db510b4230d4fe0a815f84d6c3112f9ec)
- [`Core#9`](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/9): Use proper field name for configurations [`ad496e4c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ad496e4ce3917ac7432f7dca4a101ddb376d3e7b)
- Allow duplicate properties in an admin path [`93ce80ba`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/93ce80ba527862e06f0b4711aa68edaeeeb8cf2e)


### Removed

- [`SCR-1420`](https://jira.open-xchange.com/browse/SCR-1420): Removed unused xmlbeans library [`78d2c577`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/78d2c57780ecab23c65306c75745f75d965b00d7)
- Replaced 'subscription' permission availability with capability checker [`cdc1836f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cdc1836fe5e2536d455f34a7efbad6e4e6cbe0e0)


## [8.27] - 2024-07-03

### Added

- [`MW-2325`](https://jira.open-xchange.com/browse/MW-2325): Multi-Database Support for Redis [`da3e7525`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/da3e7525cb30a9ad29d4fa20a64e3fe9e72a928a)
  - [`SCR-1383`](https://jira.open-xchange.com/browse/SCR-1383): New Property `com.openexchange.redis.resilientDatabase`
- [`SCR-1402`](https://jira.open-xchange.com/browse/SCR-1402): New Column `priority` for Database Tables `calendar_event` and `calendar_event_tombstone` [`b235659d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b235659d218ef2b277b69bc1789bd46b96416054)
- [`SCR-1403`](https://jira.open-xchange.com/browse/SCR-1403): New field `priority` in Event model of HTTP API [`48e3ffef`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/48e3ffef638be7bd667bb458e1bec4e390a5d68b)


### Changed

- [`MW-2309`](https://jira.open-xchange.com/browse/MW-2309): Transform Next Caches to Redis [`58109405`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/58109405e90f178a068fc37761c1acd8b0144a3f)
  - Previous regions `FileStorageAccount` and `UserAlias` are now backed by Redis cache
- [`MW-2328`](https://jira.open-xchange.com/browse/MW-2328): Additional (configurable) logging for some SOAP API requests [`36705091`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/36705091cef2a1c92f2f5bab7c0b87fff6dd9553)
- [`MW-2337`](https://jira.open-xchange.com/browse/MW-2337): Transform `MessagingAccount`, `c.o.messaging.json.messageCache`, `LDAPHostname` and Reseller-related Caches to Redis [`114878b4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/114878b47383927a92d27bd27e72ab541ffef2b7)
- [`MW-2340`](https://jira.open-xchange.com/browse/MW-2340): Transform `FolderUserProperty` Cache to Redis [`9b1e0ff2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9b1e0ff22bae81b120173a22302f6f5576835106)
- [`MW-2651`](https://jira.open-xchange.com/browse/MW-2651): Use canonical hostname when accessing distributed managed files [`3cc5dafd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3cc5dafdae1180c22ea957029212eaec9d9123e1)
- [`MWB-2641`](https://jira.open-xchange.com/browse/MWB-2641): Enhanced logging when copying a database table row fails during 'movecontextdatabase' [`ec85db90`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ec85db906be08e99a05bf961850166a067e68275)
- [`SCR-1393`](https://jira.open-xchange.com/browse/SCR-1393): Updated Netty libraries from `v4.1.106` to `v4.1.111` [`918566ac`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/918566ac7ae35291789c4e294823472803cb3d68)
- [`SCR-1394`](https://jira.open-xchange.com/browse/SCR-1394): Updated lettuce library from `v6.3.1` to `v6.3.2` [`97520f3d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/97520f3de2270269d7ef9f0fb9b03bec6cdca1b0)
- [`SCR-1395`](https://jira.open-xchange.com/browse/SCR-1395): Updated Apache Commons IO library from `v2.15.1` to `v2.16.1` [`5add29cf`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5add29cf00568136dd5929937fd2e59d15f8ac9f)
- [`SCR-1396`](https://jira.open-xchange.com/browse/SCR-1396): Updated Apache Commons Codec library from `v1.16.1` to `v1.17.0` [`7c98d03e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7c98d03ed8607e8f42bcfcc8a5c6f5c084a92d1e)
- [`SCR-1404`](https://jira.open-xchange.com/browse/SCR-1404): Updated JCTools (Java Concurrency Tools for the JVM) from `v4.0.3` to `v4.0.5` [`986656d6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/986656d632c6404b3bd56d9b66eb7f7b22121441)
- [`SCR-1411`](https://jira.open-xchange.com/browse/SCR-1411): Added an account index to various calendar tables for improved look-up [`40aaf3b0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/40aaf3b0cc915ec3c0233038e117f602521e85b0)
- Preparations to selectively invalidate caches after changing [`2b3be06c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2b3be06cd63326a8a718aaa7b449001d1e83b316)
- Updated integrated timezone definitions to tzdata2023d [`256754a3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/256754a3ae864892a0fc0e1d63e78b9259f9790b)


### Fixed

- [`MWB-2639`](https://jira.open-xchange.com/browse/MWB-2639): Don't filter `w:sdt` elements during sanitizing [`5cf45632`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5cf45632b74626cfa752a6d7b489d8f2bd68113f)
- [`MWB-2644`](https://jira.open-xchange.com/browse/MWB-2644): Added config option to specify whether to use HTML on reply/forward to/of text-only E-Mails if HTML is chosen as preferred message format [`1fa4aa99`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1fa4aa99531ec79cf6f788e1046a4bc1d41c66bc)
- [`MWB-2649`](https://jira.open-xchange.com/browse/MWB-2649): Avoid NPE [`db5c060e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/db5c060ea046049ff500094b2ab2e9e45c28dc09)
- [`MWB-2650`](https://jira.open-xchange.com/browse/MWB-2650): Wrong FCM Push documentation [`03419906`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/034199066e7798a4afbc0172e948f83f72051441)
- [`MWB-2657`](https://jira.open-xchange.com/browse/MWB-2657): Consistent value 200 for property `AVERAGE_CONTEXT_SIZE` [`dce8905b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dce8905bf4dd24b2726f43e4cacb3d87d81cba03)
- [`MWB-2659`](https://jira.open-xchange.com/browse/MWB-2659): Use proper i18n service to use best-fitting translation for a user's locale [`06307248`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0630724809e6afd625bf77c9a2e0d442a90fa9fc)
- [`MWB-2664`](https://jira.open-xchange.com/browse/MWB-2664): Lowered periodic log message about completion status of a database clean-up job to DEBUG log level [`220ef5f9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/220ef5f9cf8f6d745a3d7442b82df0a703460820)
- [`MWB-2666`](https://jira.open-xchange.com/browse/MWB-2666): Added missing import [`5ce65bb4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5ce65bb4deab5fbea3f12b2b621385cb425663f0)
- [`MWB-2670`](https://jira.open-xchange.com/browse/MWB-2670): Redirect to configured logout page as fallback if session no longer exists [`91dc5373`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/91dc5373b28dda4f574d16266d674d9c5ca6ad9f)
- [`MWB-2676`](https://jira.open-xchange.com/browse/MWB-2676): Drop `details` (aka MDC) from JSON-formatted log message if `DropMDC` marker is present in log event [`742918c4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/742918c440caf99f323b437fa2b85376aece588e) [`8a65fcfc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8a65fcfc2ffdc82ae5dc7e30d8128a3f0b75c549)


### Removed

- [`MW-2245`](https://jira.open-xchange.com/browse/MW-2245): Web Socket Support [`20b517b4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/20b517b4e835fff7025580c26d4cf03f6791623d)
- [`MW-2265`](https://jira.open-xchange.com/browse/MW-2265): Removed PropertyMapManagement from user cc layer [`cb6f485c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cb6f485c9721e6e6efceb8f5e4293c5fc22cfe0a)
- [`MW-2318`](https://jira.open-xchange.com/browse/MW-2318): Apache Commons Lang v2.6 [`89d0be5f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/89d0be5fdb60a4cb8cdcb3e46aeea32b1bfa5ac0)
- [`SCR-1388`](https://jira.open-xchange.com/browse/SCR-1388): Removed obsolete bundle `com.google.gdata` [`0c24e04b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0c24e04b91709fbc2b00e7e7b4090d5678a280f2)
- [`SCR-1389`](https://jira.open-xchange.com/browse/SCR-1389): Remove javax.jms library [`7a7165bb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7a7165bb0ba58e661fb560be0231345826de21d5)


## [8.26] - 2024-06-05

### Added

- [`MW-2321`](https://jira.open-xchange.com/browse/MW-2321): Webhook Option for Password Change [`3c64d79b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3c64d79bc6f9e7d1a7d1482e87fac151a400292e)


### Changed

- [`MW-2326`](https://jira.open-xchange.com/browse/MW-2326): Re-implemented put-if-absent for KeyDB; e.g. replaced special SET(GET) command with PX and NX option set [`ea34eba6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ea34eba6cb796fd923480eca58f115c1a88f4ff6)
- [`MW-2332`](https://jira.open-xchange.com/browse/MW-2332): Export SimpleMeterRegistry [`4724b21f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4724b21f460567f607ae4221b0da1512b6951f5a)
   - add io.micrometer.core.instrument.simple to the list of exported
  -    packages in the bundle com.openexchange.metrics.micrometer, to allow
  -    using it in unit tests
- [`MWB-2622`](https://jira.open-xchange.com/browse/MWB-2622): Added logging when events w/o recurrence are encountered during 'needs-action' generation [`ae17204a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ae17204ac09786a0d6e47bb629f267bf054b4313)


### Fixed

- CP-514: Let authentication plugin signal to ignore the call to it - 2 [`da54dce7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/da54dce7f5336e7db2c3f94d18b86cefad06a3bb)
- [`MW-926`](https://jira.open-xchange.com/browse/MW-926): Resolve user aliases in picture search, too [`750a0a99`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/750a0a9943c5bc3b9f84fef25ac55b1eb1026892) [`9e2740d0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9e2740d0a0206840607927c45c9d3d3b4fd6be21)
- [`MWB-1582`](https://jira.open-xchange.com/browse/MWB-1582): Use term "address book" in favor of generic "folder" when sharing contacts [`32cbed8c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/32cbed8c8d7104eb9c6a494ec817ff5b759953c3)
- [`MWB-2544`](https://jira.open-xchange.com/browse/MWB-2544): Use the SMTP extension for transmission of large messages through chunking [`9d4dbeae`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9d4dbeae8be3d44c9c5e2adf69cef6ad8ae7733f) [`028fb732`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/028fb732c467b5ceadfbae514d33adeb8cf09816) [`6ffc33c7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6ffc33c74b7bae83ccf75419002ee4079a816efd) [`37f3e930`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/37f3e930c35cdd8eb0a28b29808e4041d052b0a1)
- [`MWB-2584`](https://jira.open-xchange.com/browse/MWB-2584): Corrected documentation [`df4d72a3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/df4d72a39bcd9cc29558b838d73941255088204f)
- [`MWB-2585`](https://jira.open-xchange.com/browse/MWB-2585): Updated documentation [`21c7ab76`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/21c7ab769e55480bd8b6aa3f89123d2b0f48efda)
- [`MWB-2598`](https://jira.open-xchange.com/browse/MWB-2598): Orderly load a transport-only mail account on user deletion [`e0daddd4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e0daddd4257650d4360d95f2f7877184a30987ad)
- [`MWB-2602`](https://jira.open-xchange.com/browse/MWB-2602): Unable to load FCM configuration [`ae96b8a8`](https://gitlab.open-xchange.com/appsuite/platform/core/-/commit/ae96b8a8136af003b8c4df76c0522f0f44fbe194)
- [`MWB-2607`](https://jira.open-xchange.com/browse/MWB-2607): Don't start immediate processing of data export task if not enabled on node/pod [`7c1cbf55`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7c1cbf5569b16aee1624a2a7cdd2cb9632a67478)
- [`MWB-2616`](https://jira.open-xchange.com/browse/MWB-2616): No extra `.` if chunking of SMTP data is enabled [`b2e17a6c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b2e17a6cbad89e75f84abc7960379a9393425ecb)
- [`MWB-2617`](https://jira.open-xchange.com/browse/MWB-2617): Introduced `users_per_context` table in ConfigDB to have a direct access how many users use a certain file storage [`c4813b22`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c4813b229a2d4439ca0ad90c8e9ae2505963db05) [`fca810ac`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fca810ac697503ddd5c2f52074ff1125741b9798) [`25556522`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/25556522d13855b7729a5290dff46a92b43b8ab1) [`88bf687d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/88bf687d8ffe93d40d6e4a1c689947a610eb4ace)
- [`MWB-2626`](https://jira.open-xchange.com/browse/MWB-2626): Drops foreign key from "infostore_document" table due to missing unique key in the referenced table "infostore" and adds an appropriate index instead [`51bf6cb5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/51bf6cb5fb2bfc7e82ed0a0d9f1a47f230d336a6)
- [`MWB-2630`](https://jira.open-xchange.com/browse/MWB-2630): Enhanced detection for potentially long running file operations [`07012cea`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/07012ceaabcf7f4d29749ede9e8a390f080d490b)
- [`MWB-2634`](https://jira.open-xchange.com/browse/MWB-2634): Avoid NPE when JSON data is missing [`ff700145`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ff7001452c0a2e7ab33101eb33fe62b31bc61948)
- [`MWB-2635`](https://jira.open-xchange.com/browse/MWB-2635): Throw exception on session miss [`6d5ca447`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6d5ca4471105a90d693258942a5896263073f7d4)

### Removed

- [`MW-1928`](https://jira.open-xchange.com/browse/MW-1928): Cleanup unused/legacy Servlets [`9d0232c8`](https://gitlab.open-xchange.com/appsuite/platform/core/-/commit/9d0232c8cc3f2c42e63295c5aa9dc21321d4ce35)


## [8.25] - 2024-05-08

### Added

- [`MW-2224`](https://jira.open-xchange.com/browse/MW-2224): Added smtp metric collection [`a5b212c1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a5b212c11980d743139ef14eb479a21bf641938e)
- [`MW-2246`](https://jira.open-xchange.com/browse/MW-2246): Added Special Sorting by First Name in Contacts Module [`b65da7a1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b65da7a1dc0898d6fe1a3cb880edf8e418482fc1)
- [`MW-2300`](https://jira.open-xchange.com/browse/MW-2300): Exclusive Pre-Assembly across Sites [`633f6b01`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/633f6b013e552b076209ef24dc1647992f08becb)
- [`MW-2308`](https://jira.open-xchange.com/browse/MW-2308): Logout from IDP for Sessions from `Direct Grants` [`b254b595`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b254b595e4308267c80bffa0c1f3b272218d950e)
- [`MWB-2554`](https://jira.open-xchange.com/browse/MWB-2554): Added permission documentation [`ff950193`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ff950193713c43e6ba2dc242865c4799264683d2)


### Changed

- [`MW-2222`](https://jira.open-xchange.com/browse/MW-2222): DB TLS encryption for k8s [`fc64b729`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fc64b729021f656315349b5a16e2b66fa45ab2f5)
- [`MW-2263`](https://jira.open-xchange.com/browse/MW-2263): Introduced Redis-backed cache service having its own cache event framework (based on Redis pub/sub) and refactored existent stand-alone cache invalidation classes to that new service/framework [`05ea475b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/05ea475b02f6f0ab1262f2fd6bc1adb90cc42a77)
- [`MW-2264`](https://jira.open-xchange.com/browse/MW-2264): Transform First Caches to Redis [`b04537ed`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b04537ed18664f8c1e675df5418dc87d90248151)
- [`MW-2266`](https://jira.open-xchange.com/browse/MW-2266): "Upgrading without Downtimes" in a Kubernetes cluster [`d032a637`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d032a6374da6e647de536a3aa43525c46c9e8599)
- [`MW-2314`](https://jira.open-xchange.com/browse/MW-2314): Removed dependency from logback-extensions to Apache Commons Lang 2.6 [`7b007ce4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7b007ce4a8324f96251cebc8fa50b90fb316ce42)
- Identify sproxyd clients [`95ccd0b4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/95ccd0b48ec88e39c1ab71a8393eb1c4d89061cd)

### Deprecated

- [`SCR-1373`](https://jira.open-xchange.com/browse/SCR-1373): Deprecation of Apache Commons Lang 2.6

### Fixed

- CP-514: Let authentication plugin signal to ignore the call to it [`7f3b0757`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7f3b0757879f177fab0b103e2e802af4ec043ad0)
- [`MWB-1957`](https://jira.open-xchange.com/browse/MWB-1957): Grafana dashboard shows multiple server versions [`cac3800f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cac3800ff71a27dc40cfb55e81d18ea24e3bae06)
- [`MWB-2204`](https://jira.open-xchange.com/browse/MWB-2204): Missing API documentation for oidcLogin and oidcLogout actions of Login module [`516239c2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/516239c273b83bcb2970b187b9d964182b085a3d)
- [`MWB-2435`](https://jira.open-xchange.com/browse/MWB-2435): CredstoragePasscrypt not picked up in templates/typeSpecific/secret-envvars.tpl [`1d0578df`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1d0578dfa8a0e77896281f2acf1746c9fc3e6a23)
- [`MWB-2530`](https://jira.open-xchange.com/browse/MWB-2530): Added property `com.openexchange.imap.assumeUserLocalPartForSharedFolderPath` to control if user's local part should be assumed when determining a shared folder path; e.g. assume "jane.doe" instead of "jane.doe@invalid.com" [`c33caa7e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c33caa7e01da00b5a4a1c2e61f1cbfd30d90581b)
  - Synchronize all operations on ListLsubCollection objects
  - Removed IgnoreDeprecated methods as they became obsolete with synchronous operations (see Bug#41742)
- [`MWB-2552`](https://jira.open-xchange.com/browse/MWB-2552): Decrypt mail prior if needed [`fdfa0087`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fdfa00871e04ccddab42d53c0fd66832826c661b)
- [`MWB-2555`](https://jira.open-xchange.com/browse/MWB-2555): Deny scheduling a mail for transport if Guard-protected [`aa2c61c3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/aa2c61c33cc1bd201e324483810dd0251824d5f2)
- [`MWB-2556`](https://jira.open-xchange.com/browse/MWB-2556): Fixed typo [`ac08c82f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ac08c82f94ba26589520aa90b89d0fc5d0e60cbc)
- [`MWB-2562`](https://jira.open-xchange.com/browse/MWB-2562): Avoid issuing unused events for last gone session of a context/user [`876e33b3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/876e33b37ba83b5f4a25d5bf7875c0265fae9a58)
- [`MWB-2563`](https://jira.open-xchange.com/browse/MWB-2563): Ensure JSON data is orderly flushed to output stream [`c11400a0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c11400a0661878f4d7dd9d8cd7c9fc0ccbc1dc37)
- [`MWB-2564`](https://jira.open-xchange.com/browse/MWB-2564): Corrected property names in documentation article [`f8399bd3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f8399bd37d6390cdddf69d43d228c4fd6a15de27)
- [`MWB-2567`](https://jira.open-xchange.com/browse/MWB-2567): Introduced configurable file appender for logback.xml [`60e230fb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/60e230fb7015a4bbe4ed419bbf4db41150321fad)
- [`MWB-2571`](https://jira.open-xchange.com/browse/MWB-2571): Use full-fledged HTML parser to locate possible \<img\> tags inside HTML content [`22f36c8e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/22f36c8e9b6782b03eb7443635d34fa7cb85e1e3)
- [`MWB-2577`](https://jira.open-xchange.com/browse/MWB-2577): Com.openexchange.gdpr.dataexport.impl bundle does not start without additional configuration [`b7e8e048`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b7e8e0482e57879dcc926dfa4a91a11f0049fcde)
- [`MWB-2582`](https://jira.open-xchange.com/browse/MWB-2582): Do not transform transparent GIF images [`4d9afa36`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4d9afa360815f276038342c06df5e62a5ed8825d)


### Removed

- [`MW-2042`](https://jira.open-xchange.com/browse/MW-2042): Removed Internal OAuth Authorization Server [`557f2931`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/557f293152080df562b04f8aa1db2ba41aacf226)
- [`SCR-1378`](https://jira.open-xchange.com/browse/SCR-1378): Removal of Property `com.openexchange.redis.enabled` [`c29f0fe2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c29f0fe29b119c20b1c14b8d333407b7341f32df) [`ae91dee6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ae91dee61e51775ac9f0cbf91c7ba290833f3ade)


## [8.24] - 2024-04-03

### Added

- [`MW-2174`](https://jira.open-xchange.com/browse/MW-2174): Make `com.openexchange.user.contactCollectOnMailAccess` and `com.openexchange.user.contactCollectOnMailTransport` config-cascade aware. [`624c22e1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/624c22e1a38ec5f1a79d1bfec6555638ca1c6bb2)
- [`MW-2251`](https://jira.open-xchange.com/browse/MW-2251): Added REST-API and job to create pre-assembled contexts. Expose metrics for currently existing pre-assembled contexts [`66712325`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/66712325d0dfebc1333352e877037a0e887ba7f3)
  - See also [`MW-2252`](https://jira.open-xchange.com/browse/MW-2252) and [`MW-2262`](https://jira.open-xchange.com/browse/MW-2262)
- [`MW-2261`](https://jira.open-xchange.com/browse/MW-2261): Optionally Exclude Disabled Contexts in `listcontext` [`e48282e1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e48282e13193413a4ceb65155fd7f8838214fb9d)


### Changed

- [`MW-2109`](https://jira.open-xchange.com/browse/MW-2109): Migrated from GCM to FCM for drive events and generic PNS [`a9a09231`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a9a09231a3a3b374a590cf6ad63aea11737045ac)
  - Added the Firebase SDK 
  - Removed old GCM implementation and bundles
- [`MW-2220`](https://jira.open-xchange.com/browse/MW-2220): Improve example script to create certificates [`5e87d0a8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5e87d0a8eed1d343c27c3e2ca033d0099ed53d1c)
- [`MWB-1582`](https://jira.open-xchange.com/browse/MWB-1582): Preliminary introduce new strings when sharing address books [`4e350ef4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4e350ef4c17b47f43453682bd59b83db130ccde1)
- [`MWB-1800`](https://jira.open-xchange.com/browse/MWB-1800): Enforce table order in JOINs if applicable [`e66c994f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e66c994fa26f084fd6200e73cbfcea821cee8cd1) [`3ef9f828`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3ef9f828732f38c2d619ecc8a4083393f26cc966)
- [`MWB-2488`](https://jira.open-xchange.com/browse/MWB-2488): Documentation for on behalf scheduling [`3f26516d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3f26516da2c0248dffa67aff58abe3e799dbda23)
- [`SCR-1366`](https://jira.open-xchange.com/browse/SCR-1366): Updated Spring Framework from v5.3.21 to v6.1.4 [`4edac9b1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4edac9b1e8ac97896b2279c5f92fcf17694e5da7) [`5e76a6a3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5e76a6a344ae2426c1fe96f2f0f16a9183af242b)
- [`SCR-1367`](https://jira.open-xchange.com/browse/SCR-1367): Allow implementations of BasicAuthenticatorPluginInterface to signal commencing with the next candidate [`06647235`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/06647235175004020697f5e36b8afed6857f866a)
- [`SCR-1370`](https://jira.open-xchange.com/browse/SCR-1370): Added option to Redis configuration to specify a compression method [`068abc53`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/068abc5313c64030fbe88786cc2c80cf72acc981) [`c49c7115`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c49c7115e1d7e43302f2daab7dd44dd37c293f5c) [`da2cc680`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/da2cc680522fdc20db5702e77e8d8261a3054808)


### Fixed

- [`MWB-2525`](https://jira.open-xchange.com/browse/MWB-2525): Fixed wrong imports in commons compress. Will be fixed upstream with 1.27.0 [`91d657d9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/91d657d9585867d3d71dc96d515b1a08a579bc68)
- [`MWB-2545`](https://jira.open-xchange.com/browse/MWB-2545): Use correct variable to pass number of contexts to pre-assemble [`ebd3ee38`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ebd3ee38b966b6d9bb5203e7b6d359a7d49d5723)


### Removed

- [`MW-2075`](https://jira.open-xchange.com/browse/MW-2075): Removed property allowContentDispositionInline [`d7edcbeb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d7edcbeb84aa5da052fecef2a9af4e8f819b128f)
- [`MW-2284`](https://jira.open-xchange.com/browse/MW-2284): Kerberos support [`8852db40`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8852db40833f04bbb668f5aaec2000efc6e8a94a)


## [8.23] - 2024-03-07

### Added

- [`MW-2220`](https://jira.open-xchange.com/browse/MW-2220): Add Helm chart support for running Grizzly with TLS [`531d36a6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/531d36a6c9a08639f01dd0f18025006b73c406f3)
- [`MW-2223`](https://jira.open-xchange.com/browse/MW-2223): Add weakforced package per default [`4df6655f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4df6655fa8de51c01a30d901883140e031aaf18f)
- [`MW-2225`](https://jira.open-xchange.com/browse/MW-2225): Add missing services health check [`64258357`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/642583574814b10d301cf15ea55d3bdd02a81a8d)
- [`SCR-1351`](https://jira.open-xchange.com/browse/SCR-1351): New property `com.openexchange.health.noServicesMissing.enabled` [`3097661d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3097661d04f74ab92c45330d613108b4ae2cb3a4)


### Changed

- [`MW-2144`](https://jira.open-xchange.com/browse/MW-2144): Deprecate the in-memory SessionD and Hazelcast session storage #2 [`0b5c8a91`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0b5c8a919cc149857102ba20491c534bfdac28df)
- [`MW-2238`](https://jira.open-xchange.com/browse/MW-2238): Plugin Handling for Pre-Assembled Contexts [`63e82bdf`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/63e82bdff05434fcb61e9dff2a7844ca6db69359)
- [`MW-2250`](https://jira.open-xchange.com/browse/MW-2250): Have vital login infos available throughout sites [`dcec9938`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dcec99381555421fb01148da3f62c0081ea949f4)
- [`MW-2253`](https://jira.open-xchange.com/browse/MW-2253): Use Pre-Assembled Contexts in Automated Tests [`da30530c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/da30530c43aadef392ba661961908e2f6c825b8f)
- [`MWB-2482`](https://jira.open-xchange.com/browse/MWB-2482): Specify timeout in milliseconds to avoid excessive memory usage in unit test [`ad969e29`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ad969e29329f187c9ee56e912c7112e1fd757ab4)
- [`SCR-1345`](https://jira.open-xchange.com/browse/SCR-1345): Update Google Guava from v32.1.3 to v33.0.0 [`057dbbe8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/057dbbe8491aa45705adb940b8e5b151f442b593)
- [`SCR-1348`](https://jira.open-xchange.com/browse/SCR-1348): Update Amazon Java SDK from v1.12.487 to v1.12.661 [`77de9570`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/77de95702d9cb0c34b7fa2a15717d74578b152a0) [`688017ce`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/688017ceda90b19c09a929ce65006e7fff5b4730)
- [`SCR-1354`](https://jira.open-xchange.com/browse/SCR-1354): Update Apache Commons Codec library from v1.15 to v1.16.1 [`b7eaa81b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b7eaa81bbae54f75ec98a1203a2ba225fc11c318) [`b54dc90c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b54dc90caf04536971d4e5b10e37a84255bff0e2)
- [`SCR-1355`](https://jira.open-xchange.com/browse/SCR-1355): Update Apache Commons Codec library from v1.5.0 to v1.6.0 [`ebc6b449`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ebc6b449760d481d6a90d2baec48f620c6d6073d)
- [`SCR-1356`](https://jira.open-xchange.com/browse/SCR-1356): Update Apache Commons Exec library from v1.3 to v1.4.0 [`cfe36b15`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cfe36b15a6048131bf4944209838460333c53155)
- [`SCR-1357`](https://jira.open-xchange.com/browse/SCR-1357): Update Apache Commons Codec library from v2.11.0 to v2.15.1 [`077b4d52`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/077b4d52f38a64909afe3b97c982ad11e556d56d)
- [`SCR-1358`](https://jira.open-xchange.com/browse/SCR-1358): Update Apache Commons Lang3 library from v3.12.0 to v3.14.0 [`7dda379d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7dda379ddfd74bd5bc10e5fcab76161a21f9af3f)
- [`SCR-1359`](https://jira.open-xchange.com/browse/SCR-1359): Update a bunch of bundles in target platform [`8a0d9575`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8a0d957546e96686024f378bb961747303313ddd) [`701781e3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/701781e32a7414cedc949a1dd6ac8f1d700d3d2c)
- [`SCR-1361`](https://jira.open-xchange.com/browse/SCR-1361): Update Pushy library from v0.15.2 to v0.15.4 [`53df5b64`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/53df5b640f6e60c727f364530082d3e74cfea82c)
- [`SCR-1362`](https://jira.open-xchange.com/browse/SCR-1362): Update metadata-extractor from v2.18.0 to v2.19.0 [`ed9b5d99`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ed9b5d996a89659b58fea7951efab4911c030c27)
- [`SCR-1365`](https://jira.open-xchange.com/browse/SCR-1365): Support new property to specify connection lease timeout when waiting for a free connection in pool [`f4987873`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f498787384a4d7e2dac47f83966fde9bcc17e12f)
- Update Gotenberg image to 8.1.0 [`2efb0c98`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2efb0c9865c42b76a73b3df4415c3733e59ba826)
- Update Gotenberg image to 8.2.0 and chart to 1.1.0 [`a2707740`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a270774066fb0b08ac1b72affcae56e22e9e5f94)


### Fixed

- [`MWB-2398`](https://jira.open-xchange.com/browse/MWB-2398): Periodically check and remove orphaned cookies referencing no longer existing sessions from requests [`35ec1789`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/35ec17898adfaaff65642620c11263dee72a2c44)
- [`MWB-2420`](https://jira.open-xchange.com/browse/MWB-2420): Support parsing address string with multiple opening angles '<'; e.g. "<<jane@nowhere.com>>" [`c0c6edcf`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c0c6edcf3096947a208bc8fb9974505de43aa4e3)
- [`MWB-2482`](https://jira.open-xchange.com/browse/MWB-2482): Avoid excessive HTML processing w/ Jericho HTML parser [`814cb61d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/814cb61da679ec67e8d1c7c39a3d575939550f90)
- [`MWB-2485`](https://jira.open-xchange.com/browse/MWB-2485): Specify core-mw chart resources limits and maxHeapSize [`ea29bf46`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ea29bf46a489bd6c217039846dc50ef4689cc27b)
- [`MWB-2496`](https://jira.open-xchange.com/browse/MWB-2496): Mitigate with possible `java.io.IOException: Resetting to invalid mark` when writing ZIP entries to file storage location. Added possibility to have ZIP archive compiled for a certain module being spooled to a local disk. [`6a7d3d67`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6a7d3d671ed2d018e31c5c9a119be348c267eab4)
- [`MWB-2497`](https://jira.open-xchange.com/browse/MWB-2497): Ensure schema option is not set for pre-assembled contexts [`94b73757`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/94b7375780616d8b77050ea05c3cdf76914de906)
- [`MWB-2499`](https://jira.open-xchange.com/browse/MWB-2499): Add archive + schedule to mail/folder paths [`89361c4a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/89361c4a2f515c3fd80404cc8ff99221066cf9c8)
  - This is necessary to support the permanent switch to these paths
- [`MWB-2502`](https://jira.open-xchange.com/browse/MWB-2502): Add missing archive httpi api docu [`e6aea42e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e6aea42e78d5bd2c42ef2a809e482bcbc95394e6)
- [`MWB-2504`](https://jira.open-xchange.com/browse/MWB-2504): Fix personal parts not in quotes in email address [`e9593fd5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e9593fd5b154d2afc7b570e60e73e9815be4e2bd)
- [`MWB-2509`](https://jira.open-xchange.com/browse/MWB-2509): Don't mess-up MIME structure by adding multipart/* parts through attachment API [`343cb2f8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/343cb2f81e923817257de7157ce231f7c99a969b)
- [`MWB-2511`](https://jira.open-xchange.com/browse/MWB-2511): Look-up draft mail by cached association if possible [`fd2421dc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fd2421dc1e4bfd2c6d8e0ff26cbab9d6c83b077c)
- [`MWB-2515`](https://jira.open-xchange.com/browse/MWB-2515): Better handling of aborted attachment upload when composing a mail [`92f40134`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/92f4013431d87d46a14a97aae4e6f58efccde097)
- [`MWB-2516`](https://jira.open-xchange.com/browse/MWB-2516): Track SMS provider implementation as optional service [`1c027e50`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1c027e509ce656f26156c7643600b60ac324cf16)
- [`MWB-2517`](https://jira.open-xchange.com/browse/MWB-2517): Upgraded MaxMind GeoIP Libraries (SCR-1349) [`f1b1f9be`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f1b1f9be59f8dd6ec2aefe5157c7a905925fc82c)
- [`MWB-2525`](https://jira.open-xchange.com/browse/MWB-2525): Update Apache Commons Compress library from v1.21 to v1.26.0 [`27f90d39`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/27f90d3910f795aa4eb0d07ad76cd7dfe2d73925) [`478d4606`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/478d46066662c418cfc48c5ea9846e591663a062)
- [`MWB-2528`](https://jira.open-xchange.com/browse/MWB-2528): Release acquired connection as soon as possible (e.g. prior to loading file storage data) [`2d6d4663`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2d6d4663d9b9f15006a35fe34fc7f658c08c0e7a)
- [`MWB-2531`](https://jira.open-xchange.com/browse/MWB-2531): Filter possible parent folder from subfolder listing [`70a9d207`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/70a9d20760e53e4fdbba85c4344ca73121f4a1cc) [`2440686f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2440686f6dac98d046227e3ab363a885f32532cd)
- [`MWB2503`](https://jira.open-xchange.com/browse/MWB-2503): Unmangle folder id before parsing it to int [`9c3e089a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9c3e089ac5e96e2c71e4fb4840ec0452e13e3e1b)


## [8.22] - 2024-02-07

### Added

- [`MW-2022`](https://jira.open-xchange.com/browse/MW-2022): Tests for Helm Charts [`27853eb2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/27853eb29345a20efd8c3780e2602f0410891253) [`e56b0e0a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e56b0e0a12fabcb49574545831405da9c244a8fc)
- [`MW-2088`](https://jira.open-xchange.com/browse/MW-2088): Additional Analyzers for the App Suite Advanced Routing Stack [`5d17f425`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5d17f4253c07d2a677076d28b1969992d0b046aa)
  - [`MW-2111`](https://jira.open-xchange.com/browse/MW-2111): Request Analyzer Implementation for SOAP Requests
  - [`MW-2132`](https://jira.open-xchange.com/browse/MW-2132): Request Analyzer for Chronos iMIP Push
- [`MW-2125`](https://jira.open-xchange.com/browse/MW-2125): Moved helper methods to utility class [`eed2d786`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/eed2d786b151e5abb1d45449e0fef2908bd8169c)
- UI-125: Set `plugins/upsell//driveAd` to `protected` [`7f045ea8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7f045ea84d9d13cda34264986eb6e6c049aa3189)


### Changed

- [`MW-2145`](https://jira.open-xchange.com/browse/MW-2145): Use cluster map service throughout Middleware code [`9bcfd1f9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9bcfd1f9ae7ed54975c079cf9d1d7a81c083f16e)
- [`MW-2216`](https://jira.open-xchange.com/browse/MW-2216): Dropped AJP route and need for JSESSIONID cookie (and HTTP session respectively) [`66647c56`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/66647c561955099863265772dd1455a1020d7b4e)
- [`MW-2226`](https://jira.open-xchange.com/browse/MW-2226): Remove ignore action for unknown CU [`3f1d0227`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3f1d02270b1e103e725caf85d8f37570ed5f36cb)
- [`MW-2229`](https://jira.open-xchange.com/browse/MW-2229): Use pre-assembled contexts on context creation [`814bf8ea`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/814bf8ea4d9713f14ead537fe74c00d00317683d)
  - [`SCR-1331`](https://jira.open-xchange.com/browse/SCR-1331): Added lean property `com.openexchange.admin.usePreAssembledContexts`
  - [`SCR-1332`](https://jira.open-xchange.com/browse/SCR-1332): Added table `context_lock` to configdb
  - [`SCR-1339`](https://jira.open-xchange.com/browse/SCR-1339): Added methods in 'com.openexchange.admin.storage.interfaces.OXUserStorageInterface' for using pre-assembled contexts
- [`MW-2268`](https://jira.open-xchange.com/browse/MW-2268): Change copyright headers back to Open-Xchange GmbH [`6a15c8d6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6a15c8d6b8f372e5fc073c01ac4179ea88bd57f9) [`c68c8e61`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c68c8e617b7681e0ebf29e2f56f951a719cdb49f) [`b4b7616f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b4b7616f258c2ec8441cc22069d4fd483a6757ce)
- [`MWB-2430`](https://jira.open-xchange.com/browse/MWB-2430): Don't retry deleting the same events repeatedly when clearing a folder [`dfaa4dbf`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dfaa4dbfc1a8c9284f644679d6031662e7c76510)
- [`MWB-2466`](https://jira.open-xchange.com/browse/MWB-2466): Improved error message in case cryptographic functionalities are requested, but no appropriate features/modules (OX Guard) are installed/available [`de3b459b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/de3b459b9e72cf0743ed86d997716b4c05370dbb)
- [`OXUIB-2704`](https://jira.open-xchange.com/browse/OXUIB-2704): Apply requested range when merging results from [`808821fa`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/808821fa3fca6d1714bf5de9497272ec4f579146)
- Log some repeatedly occurring messages only once per day [`0d141908`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0d14190869bc61bc6c9c1d454045a7f83603fa32) [`a8aea518`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a8aea518414b880e155a25a7cf0e54ce3414f76e) [`9fc93733`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9fc93733f64674fdd309017939c0d8a55c4dfe46)
- [`SCR-1340`](https://jira.open-xchange.com/browse/SCR-1340): Updated Jackson & Fabric8 libraries [`11643ffe`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/11643ffe322bdc638eb5deff581e6dc396434181)
- [`SCR-1341`](https://jira.open-xchange.com/browse/SCR-1341): Added new lean property to possibly add Open-Xchange server information to HTTP responses [`080a0c13`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/080a0c13b242598f990464f5cd70c840ca0d7d96)
- [`SCR-1343`](https://jira.open-xchange.com/browse/SCR-1343): Updated Netty libraries from v4.1.97 to v4.1.106 [`baa9755a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/baa9755a3fa45f7bb947a5e38467d127a1218ad2)
- [`SCR-1344`](https://jira.open-xchange.com/browse/SCR-1344): Updated lettuce library from v6.2.6 to v6.3.1 [`4a61b713`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4a61b713f4eb1ac7bab743e17207225d628f0f0e)
- Updated logback-extensions from v2.1.10 to v2.1.11 [`1c3444db`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1c3444dbe78bb86939470bc45e774152d5b53299) [`2807ac93`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2807ac933d8387944b86e7caa08d8b19781d001b)
- Updated Gotenberg image from v7.9.2 to v8.0.3 to a custom image without MS fonts and chart from v0.6.0 to v1.0.1

### Fixed

- [`MWB-2250`](https://jira.open-xchange.com/browse/MWB-2250): Send proper notification mail to user in case data export failed due to missing content in selected module(s) [`3957b8c0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3957b8c005a8eb29edbf00536fbaf542586f1cb8)
- [`MWB-2311`](https://jira.open-xchange.com/browse/MWB-2311): Include events with unset TRANSP when loading overlapping events from storage [`5bcc8210`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5bcc821074cf9a0906702c649a00707be59d81ad)
- [`MWB-2401`](https://jira.open-xchange.com/browse/MWB-2401): Send REPLY if comment is removed [`d8646ca8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d8646ca8caec5eca9b1c8a54229b84466bb055e0)
- [`MWB-2414`](https://jira.open-xchange.com/browse/MWB-2414): Use default object metadata when initializing CopyObjectRequest [`3b991f6c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3b991f6cdf86a8f6ddde5d7d4483d4323568378a)
- [`MWB-2420`](https://jira.open-xchange.com/browse/MWB-2420): Don't advertise empty address string on corrupt address [`0d2475a5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0d2475a53aabd59665e3aac82cefaef218e48ac5)
- [`MWB-2425`](https://jira.open-xchange.com/browse/MWB-2425): Improved SQL statement and added logging [`f42fe4d3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f42fe4d37828fb4298ec090044b851393d5d134c)
- [`MWB-2434`](https://jira.open-xchange.com/browse/MWB-2434): Don't empty trash folder in "fire & forget" fashion if processing takes place through (AJAX) job queue. Orderly await completion then. [`279bde05`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/279bde05d4f13169f6f92507699e0c4a5f441e6e)
- [`MWB-2439`](https://jira.open-xchange.com/browse/MWB-2439): Ensure attendee comment is set on REPLY [`50febc36`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/50febc3627702335512e40986b7cad185ef54706)
- [`MWB-2444`](https://jira.open-xchange.com/browse/MWB-2444): Support for arbitrary settings in PodSpec is missing [`bb464b2a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bb464b2a7105994684323771402f13cc65c34a24)
- [`MWB-2452`](https://jira.open-xchange.com/browse/MWB-2452): Specify reasonable timeout when checking for possible shared attachment folders or scheduled mail references [`c755a394`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c755a39493c0a8c5eda106c881de26c51b778a5a)
- [`MWB-2456`](https://jira.open-xchange.com/browse/MWB-2456): More lenient insert into database on duplicate attempt [`014115e3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/014115e3aaf1219cc0f5c34b73341bc28d486e19)
- [`MWB-2458`](https://jira.open-xchange.com/browse/MWB-2458): Properly apply UID conflict strategy [`d552d3bb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d552d3bb7ce4f80601467d891d0f3992d7b2de96)
- [`MWB-2460`](https://jira.open-xchange.com/browse/MWB-2460): Advertise proper error message to client on exceeded quota [`18a5dd56`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/18a5dd560e26cb6405c5ca5e86c485ad090997e4)
- [`MWB-2461`](https://jira.open-xchange.com/browse/MWB-2461): Updated restricted scopes in "Drive Sync App" example [`6a793fc4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6a793fc453439c7c295f7769acbefc682f0ae2f5)
- [`MWB-2464`](https://jira.open-xchange.com/browse/MWB-2464): Restored "Upgrading Without Downtimes" article [`92b32f13`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/92b32f132700f2979e917b69a27a28b2dda1727c)
- [`MWB-2467`](https://jira.open-xchange.com/browse/MWB-2467): Use context id as fall-back if no context name set [`d358aead`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d358aeadf57aa2b7f0f164668790207eb104ea38)
- [`MWB-2470`](https://jira.open-xchange.com/browse/MWB-2470): Adjust 'login2user' table when using preassembled ctx [`6aea8bb2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6aea8bb2da64864bc572904828273ba073adf427)
  - [`SCR-1339`](https://jira.open-xchange.com/browse/SCR-1339): Added methods in 'com.openexchange.admin.storage.interfaces.OXUserStorageInterface' for using pre-assembled contexts
- [`MWB-2471`](https://jira.open-xchange.com/browse/MWB-2471): No replacement of illegal Content-Id identifiers [`9a70859a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9a70859a5e057e051c899c0cc24f33858261ee7a)
- [`MWB-2482`](https://jira.open-xchange.com/browse/MWB-2482): Avoid excessive HTML processing w/ Jericho HTML parser [`f04860db`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f04860db87454f0ba01f5e4ed45f25b61a1d5593)


## [8.21] - 2024-01-10

### Added

- [`MW-2118`](https://jira.open-xchange.com/browse/MW-2118): Implementation: REST Interface for Log Configuration [`ed20e40d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ed20e40d28c22e79cfce301d75bddb42cda36579)
- [`MW-2119`](https://jira.open-xchange.com/browse/MW-2119): Extend Log Configuration with "includestacktrace" and "socketLogging" [`6776d5a2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6776d5a21d513184f2833221e261a9879f69b05c)
- [`MW-2190`](https://jira.open-xchange.com/browse/MW-2190): New metrics for provisioning aspects (PluginInterfaces, storage- and API-calls) [`b0b135a6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b0b135a6072b7b32f052521809ae55fde27a660a)
- [`MW-2226`](https://jira.open-xchange.com/browse/MW-2226): Send CANCEL Message when Declining Party Crasher [`fb9fd9e1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fb9fd9e1c54b7b3daea314dce727e2788f125d07)


### Changed

- [`DOCS-5156`](https://jira.open-xchange.com/browse/DOCS-5156): Changed registry location for pdftool [`40d95a78`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/40d95a7894483d05f3d3b88c569f569637a6b94e)
- [`MW-2143`](https://jira.open-xchange.com/browse/MW-2143): Improve user update workflow [`7bd853b9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7bd853b95a41d07c11971ba84bc5865992daf3cf)
- [`MW-2192`](https://jira.open-xchange.com/browse/MW-2192): Restrict Cleanup Jobs to Site-local DB Schemas [`3f89ea99`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3f89ea99337a2364a146ef4531b4180c50e0041d)
- [`MWB-2422`](https://jira.open-xchange.com/browse/MWB-2422): Optimistically gather updateable references in
database when moving context file storages [`9ae9d6fb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9ae9d6fb427b6e6a04f6e0dde3be188d93543ca8)


### Fixed

- [`MWB-1653`](https://jira.open-xchange.com/browse/MWB-1653): Support for ox_props debug flag [`f3b8f1ac`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f3b8f1ac779551702001e3323dadbfb70cbc5f25)
- [`MWB-2285`](https://jira.open-xchange.com/browse/MWB-2285): Avoid NPE [`fb269b44`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fb269b4421ba5a91081f6559da610d3336779476)
- [`MWB-2379`](https://jira.open-xchange.com/browse/MWB-2379): Fixed attachment references in HTTP API documentation [`1fce8b44`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1fce8b44a8101a9ddef6f8b573620ec34b42b252)
- [`MWB-2392`](https://jira.open-xchange.com/browse/MWB-2392): Avoid excessive look-up by regular expression [`3ba026f4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3ba026f4ec0ea667dc8f7b129600579ccf60c162) [`248f53fb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/248f53fb6f8a3b7389a96b55c514c72e9762fbc3)
- [`MWB-2393`](https://jira.open-xchange.com/browse/MWB-2393): No regex-based processing on invalid Content-Id [`056219b9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/056219b9abde43dbec7f9c5ec1e00261ec866964) [`d6b8f40e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d6b8f40e2b8e29a4637f13d72a20b02f3db24555)
- [`MWB-2395`](https://jira.open-xchange.com/browse/MWB-2395): Avoid setting NULL parameter for NOT NULL column in
table 'del_user' [`1fc2d875`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1fc2d8750609c5e764bf7f5f702b46a0af403c50)
- [`MWB-2399`](https://jira.open-xchange.com/browse/MWB-2399): Only announce "scheduled_mail" capability for a
composition space if all preconditions are met [`b177d6f2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b177d6f2578c80d83821074ce083a3a40073042b)
- [`MWB-2403`](https://jira.open-xchange.com/browse/MWB-2403): Ensure to only re-assign "changed_from" column upon
user deletion [`e8141bbb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e8141bbbcdf30aace0a5d15fc26d688645bc991a)
- [`MWB-2410`](https://jira.open-xchange.com/browse/MWB-2410): Added config option to avoid using IMAP entity's display name when listing shared folders [`1d05f312`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1d05f312ddb45e4ce4de1c9a41ac78fb301cb86b)
- [`MWB-2415`](https://jira.open-xchange.com/browse/MWB-2415): Correctly hand down initialized connection to database [`24f5743c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/24f5743c5c58f8099352067cfb649e1513306060)
- [`MWB-2416`](https://jira.open-xchange.com/browse/MWB-2416): Guarded access to mail structure's content-type and
-disposition [`ae507b3a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ae507b3ac02d7ecf525e4ff7c8373fc6d9d192c0)
- [`MWB-2417`](https://jira.open-xchange.com/browse/MWB-2417): Write mailbox name as UTF-8 if IMAP server advertises "UTF8=ACCEPT" capability [`8308f025`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8308f025b98dfcb240b996a202d1cefdb3483406)
- [`MWB-2421`](https://jira.open-xchange.com/browse/MWB-2421): Ensure that warnings are of exception category WARNING [`6e5c2d72`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6e5c2d7296a9a4af9b1b11987a125b32ffd4d3a3)
- [`MWB-2432`](https://jira.open-xchange.com/browse/MWB-2432): Orderly pay respect to possible UTF8 support when writing mailbox names to crafted IMAP commands [`6ba3f6d6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6ba3f6d6450210bbaa5b5ab19d3d0854c0c94c6d)
- Added missing yaml docu files [`ebfa6550`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ebfa65500117364bddf4f66b57ecd6769bc7776f)


## [8.20] - 2023-11-29

### Added

- [`MW-1994`](https://jira.open-xchange.com/browse/MW-1994): Introduced scheduled mail feature [`318e13b9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/318e13b96f1b1c00d6afe243ed87069171043280) [`40809864`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/40809864178548add13f545ff3a97b8ae330d3a6) [`1ca1ede2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1ca1ede2c0ef10b09bf93f467e50874d13ff37f6)
- [`MW-2056`](https://jira.open-xchange.com/browse/MW-2056): "Forward" Appointments via Email [`0372cb66`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0372cb6649c430e18259105beae3dcec4137afeb)
- [`MW-2088`](https://jira.open-xchange.com/browse/MW-2088): Additional Analyzers for the App Suite Advanced Routing Stack [`4b5d6d31`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4b5d6d31e9d18d1532a87954a3801214367c4a42)
  - [`MW-2112`](https://jira.open-xchange.com/browse/MW-2112): Request Analyzer Implementation for Tokens Login and Drive Jump
  - [`MW-2135`](https://jira.open-xchange.com/browse/MW-2135): Request Analyzer for "Advertisement" REST endpoint
  - [`MW-2133`](https://jira.open-xchange.com/browse/MW-2133): Request Analyzer for Dovecot Push
  - [`MW-2136`](https://jira.open-xchange.com/browse/MW-2136): Request Analyzer for config-related "Preliminary" endpoints
  - [`MW-2134`](https://jira.open-xchange.com/browse/MW-2134): Request Analyzer for "Admin" REST endpoints
  - [`SCR-1302`](https://jira.open-xchange.com/browse/SCR-1302): Added context_id field to TokenLogin json response
  - [`SCR-1284`](https://jira.open-xchange.com/browse/SCR-1284): Add parameters to drive jump redirect for request analyzing
- [`MW-2173`](https://jira.open-xchange.com/browse/MW-2173): Add logging for writeable database access to non-local segments [`6387936e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6387936ee9c6031bd43646723bc15ff023deb0f0)
  - [`SCR-1309`](https://jira.open-xchange.com/browse/SCR-1309): Added lean property `com.openexchange.database.logWritesToNonLocalSegments`


### Changed

- DOCS-4961: Adjusted default of "maxSize" for image
transformations to 20MB ([`SCR-1316`](https://jira.open-xchange.com/browse/SCR-1316)) [`ecace03a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ecace03a7decc2926bb72d04d854cdba6d3a3319)
- [`MW-1595`](https://jira.open-xchange.com/browse/MW-1595): Update documentation articles for v8 [`ff5fdb37`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ff5fdb374688dfe4d103c789a63460a3db0a3735)
- [`MW-2139`](https://jira.open-xchange.com/browse/MW-2139): Restrict Pre-Upgrade for Groupdware DB Schemas to Site-local DB Schemas [`e17a00d7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e17a00d7c24501b3686c353a9b624b4c85f0a215)
- [`MW-2144`](https://jira.open-xchange.com/browse/MW-2144): Deprecate the in-memory SessionD and Hazelcast session storage [`8aa2d519`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8aa2d519c65c5ff404f18fecd560f9c8f4f8d59f)
- [`MW-2170`](https://jira.open-xchange.com/browse/MW-2170): Updated gotenberg chart dependency and enabled read-only filesystem [`f04d8c8c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f04d8c8c6acb3474dcbbf11b8a995d45e0c586ad)
- [`MWB-2345`](https://jira.open-xchange.com/browse/MWB-2345): Reduced amount of session parameter names in log
message [`187887a7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/187887a7d353e02f5cf0b4f27bfe9ba7a110bfe1)
- Enhanced error response for *DAV requests [`0bc4811a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0bc4811a7e7f32f3195d80509b43a19f1b3bc76d)
- [`SCR-1317`](https://jira.open-xchange.com/browse/SCR-1317): Added configuration options to enable
debugging/profiling SQL queries [`535206ed`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/535206ed80490c853b2da1a456127b1904286356)
- [`SCR-1325`](https://jira.open-xchange.com/browse/SCR-1325): Updated Google Guava from v31.1.1 to v32.1.3 [`b0339982`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b0339982b406c8c52af7424534a2fc8940f4be47)
- [`SCR-1326`](https://jira.open-xchange.com/browse/SCR-1326): Updated Hazelcast from v3.5.1 to v3.5.6 [`f05c206c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f05c206cb9865908a005cbc414e267fdd545b7ac)
- [`SCR-1328`](https://jira.open-xchange.com/browse/SCR-1328): Removed CPU Resource Limit [`91efe276`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/91efe2763e336873f0e22e4e9281aed37782312e)


### Fixed

- [`MWB-1730`](https://jira.open-xchange.com/browse/MWB-1730): Orderly check if organizer event copy is targeted by
scheduling messages (2) [`cfd168d4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cfd168d4d970cf8755aebdac559d48640a5c90e1)
- [`MWB-2328`](https://jira.open-xchange.com/browse/MWB-2328): Use proper MySQL v8 compatible syntax on user creation [`a86108c0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a86108c0d9a4ef11fa6428bafeb22e1ba8b5bbe3)
- [`MWB-2354`](https://jira.open-xchange.com/browse/MWB-2354): Ordlery deal with shared folders from different owners
with the same display name [`50143d91`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/50143d91820c0f1a5d10bb43e81d1d9401bcd8c4)
- [`MWB-2358`](https://jira.open-xchange.com/browse/MWB-2358): Drop PRIMARY KEY prior to modifying column belonging to
PK, then re-create PRIMARY KEY [`6592349d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6592349d12f3a67bf78f8f87494b807ae8798f96)
- [`MWB-2360`](https://jira.open-xchange.com/browse/MWB-2360): Mailfilter module not accessible via OAuth (cherry picked from commit 49f1551cc56bbe06bf422a7a6dbeaabb50d38842) [`552a0d3f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/552a0d3feaa85810b92ac14da3ee861d03d45b8a)
- [`MWB-2366`](https://jira.open-xchange.com/browse/MWB-2366): Respond with "Search too complex" error if applying a
wildcard pattern to a mail search expression takes excessively long [`93eeba9b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/93eeba9b20b87b94c8b95b646f2a838c3cdca183)
- [`MWB-2367`](https://jira.open-xchange.com/browse/MWB-2367): Use simple glob matching for file/directory exclusions,
use guarded matcher for regex patterns sent by legacy clients [`a8f6643f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a8f6643fae25aef305931bb0644fcef3af65be3d)
- [`MWB-2368`](https://jira.open-xchange.com/browse/MWB-2368): Advertise "search_in_folder_name" and "search_by_term"
for "infostore" database folders [`c00a21b0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c00a21b0fab4c457e9a3d05f72bacb0bae068811)
- [`MWB-2370`](https://jira.open-xchange.com/browse/MWB-2370): Propagate master changes only into exception events the
user actually attends [`bafc8cd1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bafc8cd184652fc7e79850075a726c0a6f94b5e3)
- [`MWB-2372`](https://jira.open-xchange.com/browse/MWB-2372): Folder API requests are not working with "Application Specific Passwords" [`99a61a51`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/99a61a51195a156112e731c32acbaeddc0f268a5)
- [`MWB-2374`](https://jira.open-xchange.com/browse/MWB-2374): Orderly handle Unified Mail messages when examining a
message for scheduling information [`e45cabd5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e45cabd55e5766f650329316510e3492683c00ef)
- [`MWB-2376`](https://jira.open-xchange.com/browse/MWB-2376): Indicate correct part number in multipart upload to S3 [`f6e52b17`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f6e52b179413fef7e332c152337d356ac2ae1c2d)
- [`MWB-2380`](https://jira.open-xchange.com/browse/MWB-2380): Generate exception events as needed for unsolicited
REPLYs to recurring event instances [`0abda2b6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0abda2b6665939fdfa801de631d1e9919fb51ec4)
- [`MWB-2382`](https://jira.open-xchange.com/browse/MWB-2382): Select proper recipient addresses on reply to own mails [`a9f10444`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a9f104443acdeadd550ee47ec9ad65c205443d8c)
- [`SAZ-4`](https://jira.open-xchange.com/browse/SAZ-4): Use singleton connection to user database for all write accesses [`eb0836a5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/eb0836a5408776ec4500c6ac550c77463fb9bfa2)


## [8.19] - 2023-10-24

### Added

- [`MW-2088`](https://jira.open-xchange.com/browse/MW-2088): Middleware components for the App Suite Advanced Routing Stack
  - Added new bundles for the request analyzer feature ([`SCR-1241`](https://jira.open-xchange.com/browse/SCR-1241))
  - New properties for Segmenter Client Service ([`SCR-1277`](https://jira.open-xchange.com/browse/SCR-1277))
  - Upgraded the gson library from 2.9.0 to 2.10.1 ([`SCR-1266`](https://jira.open-xchange.com/browse/SCR-1266))
  - New REST endpoint exposed at `/request-analysis/v1/analyze` to analyze
client requests and associate them with segment markers
  - Added first batch of request analyzer implementations covering the
most common client requests
  - Introduced `request-analyzer` service role to deploy and scale
conainers independently
  - Implemented segmenter client API to determine active site for a
certain segment [`a2705aa2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a2705aa2236c505fcba108ccab13d8b07b65c7b4)


### Changed

- [`MW-2094`](https://jira.open-xchange.com/browse/MW-2094): Added the 'LastModified' and 'ModifiedBy' metadata to each Sieve rule. [`5197be2a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5197be2ae9634f61da5af17bd58e522e347779c7)
- [`MWB-2296`](https://jira.open-xchange.com/browse/MWB-2296): Only allow certain URI schemes for external calendar
attachments ([`SCR-1307`](https://jira.open-xchange.com/browse/SCR-1307)) [`5277863a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5277863a2ab06e42d6de80a71dea3730da383ca9)
- [`MWB-2345`](https://jira.open-xchange.com/browse/MWB-2345): Enhanced logging, added fallback for missing response
error code from auth server [`3da0018d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3da0018d7f8d5319cba1f8c7ed70f20944988430) [`e2332e6a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e2332e6aac82e0697b029f374a68410f09d383e7)
- Removed vulnerable lib sqlite-jdbc and provided needed dependencies by plain snappy-java lib [`3d9e92d3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3d9e92d35e07790c9da26517d198e7ac7213846f)
- Updated core-mw chart dependencies and enabled read-only filesystem for gotenberg [`075e07d3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/075e07d327ac728903eee757c14d927551efa001)
- Updated vulnerable lib commons-fileupload 1.4 to latest version 1.5 [`353845aa`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/353845aa92016d0da56da5db17962d2d59ebe246)
- Updated vulnerable lib jackrabbit-webdav 2.19.1 to version 2.21.19 [`408e8dd3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/408e8dd33966e5fc4d61b5e4ec39afce91b3d7f1)
- Updated vulnerable lib net.minidev:json-smart and (its dependency accessors-smart) 2.4.8 to version 2.4.11 [`3b7dae91`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3b7dae918923850bb2459512c757cd4cd92f95fd)
- Updated vulnerable lib snakeyaml 1.33 to version 2.2. Depending libraries (e. g. jackson-*) required an update too [`d48c6679`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d48c6679af751b72d6a46b77adac3e42eac9eb3e)
- Updated vulnerable okio-jvm 2.8.0 lib to latest 3.5.0 and cleaned up dependencies (added okio, updated okhttp + kotlin*, test dependencies) [`0aae47f3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0aae47f3184bd6d5076abe6b87d09c8fba768193)
- Removed default values for chart dependencies and link to source [`9861882b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9861882bbc711ab685ff660f50c3a99e00f4f463)


### Fixed

- [`MWB-2220`](https://jira.open-xchange.com/browse/MWB-2220): use existing functionality for secret properties [`3e12ce12`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3e12ce12277c577e8ebae1ceeae8ff1434aca5b4)
- [`MWB-2250`](https://jira.open-xchange.com/browse/MWB-2250): No success notification if there are no result files [`263f92eb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/263f92ebe6a0ac6eb1de2d6024e03ced55be53be)
- [`MWB-2283`](https://jira.open-xchange.com/browse/MWB-2283): Don't try to assign a new category when moving to
"general" category [`10e99977`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/10e99977a86c2fa0272969e6e26aff22caf3ffb6)
- [`MWB-2296`](https://jira.open-xchange.com/browse/MWB-2296): Check potential UID conflicts for newly added attendees [`d075d98f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d075d98ffa16a32d020db44456d48cbf7edeabc1)
- [`MWB-2297`](https://jira.open-xchange.com/browse/MWB-2297): Prefer display name for object permission validation
errors [`43e8d4b8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/43e8d4b87756c02fef5c8249f95b566ba2e2cec2)
- [`MWB-2300`](https://jira.open-xchange.com/browse/MWB-2300): Optimized moving folder (and its subtree) to trash [`8ef3f975`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8ef3f97588632b6914a20ca012491e63c83fa229) [`0b68cc47`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0b68cc4745b44747b93e83f5b4e49852d6ecfc96)
- [`MWB-2309`](https://jira.open-xchange.com/browse/MWB-2309): Cross-check resource attendees when evaluating 'all
others declined' flag in list responses [`154ae880`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/154ae8801e03b3246beffc0ea078a0282bc12fca)
- [`MWB-2310`](https://jira.open-xchange.com/browse/MWB-2310): "infostore?action=upload" fails with "EOF" error on Appsuite 8 [`269accfb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/269accfb66efefa54ebf42a201e22fdce660f58e) [`c2840ffa`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c2840ffa02b5fa80bac88b1d608eaa9e582331df)
- [`MWB-2322`](https://jira.open-xchange.com/browse/MWB-2322): Probe for name of the function for geo conversion (3) [`12d7d73f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/12d7d73f7edcaecf592a090597e5c62e88a105fa)
- [`MWB-2333`](https://jira.open-xchange.com/browse/MWB-2333): Sanitize broken/corrupt Content-Type string when parsing multipart content [`22a9393b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/22a9393b471e17d0010330f2e7736f3cb2ba0f2d)
- [`MWB-2336`](https://jira.open-xchange.com/browse/MWB-2336): Aligned naming of settings to the ones used by UI [`86bf97bd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/86bf97bde3b6b0083bed3895e990e2a461ef4ea8)
- [`MWB-2337`](https://jira.open-xchange.com/browse/MWB-2337): Ignore possible "NO [NOPERM]" response when issuing a METADATA command to retrieve deputy information from all IMAP folders [`3ce1f58a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3ce1f58aede5441ef98ef89f381bcc2c7480c2e1)
- [`MWB-2339`](https://jira.open-xchange.com/browse/MWB-2339): Ensure privisioning related log properties are dropped
once message has been logged [`35022c92`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/35022c92ac9facc9623e94ad4d55f5808ffa6d98)
- [`MWB-2343`](https://jira.open-xchange.com/browse/MWB-2343): Preferably consider 'X-MICROSOFT-CDO-INTENDEDSTATUS'
when parsing event transparency from iTIP [`2e04a819`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2e04a8193b896e348834c28fdda9c893dee38dea)
- [`MWB-2349`](https://jira.open-xchange.com/browse/MWB-2349): Orderly display plain-text mail w/ alternative text
parts [`baefd0a8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/baefd0a83df80dded87cd49aff500dc19c933c01) [`711ea55b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/711ea55b43c2b91bf84c4287466f4581010a3601)
- [`MWB-2352`](https://jira.open-xchange.com/browse/MWB-2352): More user-readable error message in case message flags
cannot be changed due to insufficient folder permissions [`91188e1e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/91188e1e0bd1f8c5047b476917fcf9db8ba6a200)
- Enhanced detection for images with data URIs [`997ed5ff`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/997ed5ff0e2549990493e1676614c734e43a05bc)
- [`MWB-2353`](https://jira.open-xchange.com/browse/MWB-2353): No global lock when initializing in-memory folder map [`f7fef269`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f7fef26956a1c5b5cf7207fb7f01124790cfa72e)


### Removed

- [`MW-2169`](https://jira.open-xchange.com/browse/MW-2169): Removed preliminary sharding extension
  - [`SCR-1303`](https://jira.open-xchange.com/browse/SCR-1303): Dropped sharding related property
  - [`SCR-1304`](https://jira.open-xchange.com/browse/SCR-1304): Dropped 'shard' query paramter from SAML request [`db10fd3a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/db10fd3a6d310a4b05e9aa945ee4f469185eb7e7)
- [`SCR-1311`](https://jira.open-xchange.com/browse/SCR-1311): Removed obsolete Rhino Scripting [`ebef2cd8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ebef2cd8e80005238b21dff3e46da0a98290f159)
- [`SCR-1312`](https://jira.open-xchange.com/browse/SCR-1312): Removed obsolete bundle [`07bc8d6c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/07bc8d6c39d23bd0f8245743940276e71e634363)


## [8.18] - 2023-09-27

### Added

- [`MW-2010`](https://jira.open-xchange.com/browse/MW-2010): Support for Webhooks [`8f93c95f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8f93c95f0b399ca9f7439731daf27a2c1b070c0c) [`79d3fc0a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/79d3fc0ad3e8b19fc9bdcde7bbe3a58169f82e6a) [`39b017d3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/39b017d346f8f64527f393f7a42fc11d03fdde64)
- [`MW-2116`](https://jira.open-xchange.com/browse/MW-2116): Added option to use session parameter as a secret source. [`0c1b50d4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0c1b50d4eb6d3b241fe31a9e6e5b63129e1b51bf)
- Mail: Support dedicated column for user flags to be queried by action=all or action=list request [`86c22aa5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/86c22aa5dc2d59f7197e3ea5e61b527237f1e614)


### Changed

- [`MW-2120`](https://jira.open-xchange.com/browse/MW-2120): Convert Mail User Flags to/from UTF-8 [`b130a436`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b130a436a0b5047eb7ea400fb99393e1ec4ebeb7)
- [`MW-2124`](https://jira.open-xchange.com/browse/MW-2124): allow subscribe/unsubscribe actions via oauth [`1df8ddf8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1df8ddf8b1044abf13b7e05a463a4a9810fb614e)
- [`MWB-2315`](https://jira.open-xchange.com/browse/MWB-2315): Remove user-specific templates [`9ef5570d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9ef5570d9e4c3cd997bf6c6bf27a5fb011d7535c)
- [`SCR-1283`](https://jira.open-xchange.com/browse/SCR-1283): Enhanced redis hosts configuration [`c6f1ef04`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c6f1ef04708ab027bf62c6e4aff8b9c4688c7629)
- [`SCR-1285`](https://jira.open-xchange.com/browse/SCR-1285): Updated Netty NIO libraries from v4.1.94 to v4.1.97 [`ca90cc77`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ca90cc77a9f0a82f2c9fdc27c5e24b9932022897) [`d1f792bf`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d1f792bf1530e6da660bcf30748220f37b729b66)
- [`SCR-1286`](https://jira.open-xchange.com/browse/SCR-1286): Updated lettuce library from v6.2.5 to v6.2.6 [`46e37ee3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/46e37ee33a36667950eadf3836d4184804502fef)
- Don't require 'infostore' module permission for mail pdf export [`1afcccae`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1afcccae82f33d17330819fa9b1114fd3a9cd535)


### Fixed

- [`MWB-1730`](https://jira.open-xchange.com/browse/MWB-1730): Process CUs without calendar access [`efce0922`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/efce092252200fa2449a682ae2a4f199b7131450)
- [`MWB-1781`](https://jira.open-xchange.com/browse/MWB-1781): Set MySQL client protocol to SOCKET for localhost connections [`f2cff023`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f2cff023e374478169de318bc688d3297b6d1a5d)
- [`MWB-2286`](https://jira.open-xchange.com/browse/MWB-2286): not very helpful error message in case features.definitions is not defined [`77afac3d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/77afac3db82920707712535e0d8a1064ff5ef5ec)
- [`MWB-2287`](https://jira.open-xchange.com/browse/MWB-2287): Orderly detect possible "mail not found" error while checking for referenced mail on reply/forward [`643153ce`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/643153ce869f7e40c23ba9effe61e035cbd540dd)
- [`MWB-2290`](https://jira.open-xchange.com/browse/MWB-2290): Ensure "INBOX" folder is translated, too [`682f34a4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/682f34a48f89171ed6a58b140a5415b7f53da72e)
- [`MWB-2294`](https://jira.open-xchange.com/browse/MWB-2294): Socket Logging not working [`108ea815`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/108ea815369627f8c58982c7c2cea2a6ed432d38)
- [`MWB-2298`](https://jira.open-xchange.com/browse/MWB-2298): Changed column 'propertyValue' of table 'subadmin_config_properties' to be of type TEXT [`aa35c6d8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/aa35c6d87d7a4e77b939290fbd834fcbde0411cc)
- [`MWB-2299`](https://jira.open-xchange.com/browse/MWB-2299): Handle unsupported image format as illegal image upload [`89635a00`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/89635a002189dbec9136ad64ef8d3161df11d2a0)
- [`MWB-2306`](https://jira.open-xchange.com/browse/MWB-2306): Extend the "login" column for "user_mail_account" and "user_transport_account" tables [`2fe842ac`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2fe842ac627685c0ea9b851b3a87b5835884b2b6)
- [`MWB-2307`](https://jira.open-xchange.com/browse/MWB-2307): Don't use config-cascade cache if scope preference has been set [`b4f7aa7b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b4f7aa7b369ed2419b1711c3681f4e0f3b1a695d)
- [`MWB-2313`](https://jira.open-xchange.com/browse/MWB-2313): Check queried in-compose draft messages against cached ones [`cb82807f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cb82807f65ef6bd03cf98a2ab86d608da02ffa1e) [`a6ed27f4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a6ed27f4256b015ab85b25ff5076e45c708ca155)
- [`MWB-2316`](https://jira.open-xchange.com/browse/MWB-2316): Broken link in "Export PDF" documentation [`f87b10e8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f87b10e8fcc2f6e740664dd4701dcaee5e4fd776)
- [`MWB-2317`](https://jira.open-xchange.com/browse/MWB-2317): Capability is missing in "Export PDF" documentation [`0e96d602`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0e96d60279d7e77c9a8fc87a9d6603b3c08969c6)
- [`MWB-2319`](https://jira.open-xchange.com/browse/MWB-2319): Don't limit POP3 server response when querying UIDLs of
available messages [`55899950`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/55899950faeb198837ae3be43a48265a20b7bb51)
- [`MWB-2320`](https://jira.open-xchange.com/browse/MWB-2320): Updated JUnit to 5.10.0 to support Eclipse 2023-09 [`e5d2942f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e5d2942f0a4b776e2df9d62573a329d3d74fd048)
- [`MWB-2321`](https://jira.open-xchange.com/browse/MWB-2321): Removed persistence section in values.yaml [`41b31013`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/41b31013dfee49867efb44ef810390b174efebc6)
- [`MWB-2324`](https://jira.open-xchange.com/browse/MWB-2324): Restored parsing of erroneous token refresh responses [`afaad652`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/afaad652e0dd71a2e2d528b97c5f450689f1472e)


### Removed

- [`MW-2093`](https://jira.open-xchange.com/browse/MW-2093): Removed Twitter integration [`8e1d53e4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8e1d53e4384b7430621dd43e1c7c76b688c9294f)


### Security

- [`MWB-2315`](https://jira.open-xchange.com/browse/MWB-2315): CVE-2023-29051 [`d7041266`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d7041266aab43f689bb7cde251b42535c8aa9be3)


## [8.17] - 2023-08-30

### Added

- [`MW-2016`](https://jira.open-xchange.com/browse/MW-2016): added deployment role 'businessmobility' for USM/EAS deployments [`01bfa6d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/01bfa6d5b9cc77ef4945d0369bcf853120a729db)

### Changed

- [`MW-2003`](https://jira.open-xchange.com/browse/MW-2003): Handle Time Transparency of Appointments per User
  * Added `transp` field to attendee
  * Handle transparencies set via CalDAV clients [`12aee31`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/12aee3165b9745ee8b32a873c5488b8b2e2b427d)
- [`SCR-1270`](https://jira.open-xchange.com/browse/SCR-1270): Updated Google API Client libraries [`800dc9f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/800dc9f9e2ed7232072303070dc15ac6f3be9603)
- [`MWB-2259`](https://jira.open-xchange.com/browse/MWB-2259): Added more DEBUG and INFO logging for GDPR data export [`39f74ae`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/39f74aeac251eb68f4c7f4d9eb5d0769c3db407a) [`ab77d3e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ab77d3e20b1ae846096bee55969b6f97a68b5c23)
- [`SCR-1275`](https://jira.open-xchange.com/browse/SCR-1275): Upgraded MySQL Connector for Java from v8.0.29 to v8.0.33 [`76146ce`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/76146cef4dd60d7d5f0bc1b9a9da9c78d18bd12f)

### Fixed

- [`MWB-2266`](https://jira.open-xchange.com/browse/MWB-2266): Extremely long-running requests are not terminated [`f9e86fc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f9e86fcf2146ea73fb00adfda3cb69d95b62a987) [`4bce9cd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4bce9cdb77513bb48d02a37655f13df64be8ad94) [`e434d32`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e434d323cead7ecccc2a363844d15652a866b202) [`4f05fa1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4f05fa1e8fd91503d7cc909bd26fa24304c8d7f6) [`cc0833b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cc0833b73d66157f673e0f3ce1829d9206422c5c) [`38fea46`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/38fea46fe2d082f2073971b3cf24090d71b7c24f) [`2be0655`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2be0655fff1c3cabb58069dc205cb73f78ac7256) [`d3bd8f2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d3bd8f2b8518053ab1d4e81c5b5210a7f7c0632a) [`f85638f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f85638f83e0bb64c3a77281799e50c194fb1e021) [`d494263`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d49426374e0824d1acedb066d1cfe1168f58cea0)
  * Hard timeout of 1h for tracked requests of any kind & hard timeout of 60 seconds for mail compose related communication with primary mail backend
  * Introduced wait time for concurrent operations. If elapsed, the operation is aborted
  * Use Apache FreeMarker template engine with safe configuration 
- [`MWB-2242`](https://jira.open-xchange.com/browse/MWB-2242): Take over selected filestore id properly during user
creation
  * [`SCR-1264`](https://jira.open-xchange.com/browse/SCR-1264): Update task to insert missing references into
'filestore2user' table [`867b465`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/867b46566664bad5111a4a42335d57c399bb874a)
- [`MWB-2249`](https://jira.open-xchange.com/browse/MWB-2249): properly disable context during filestore move [`92e1649`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/92e16494408504ebb62511de47a28124c506b432)
- [`MW-2094`](https://jira.open-xchange.com/browse/MW-2094): Backwards compatibility for extra metadata in sieve scripts [`dfee773`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dfee77322511a36e99849dae8c64689777c39c1d)
- [`MWB-2275`](https://jira.open-xchange.com/browse/MWB-2275): Yield cloned objects from Caching LDAP Contacts Access [`f4d0b36`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f4d0b36c7e7429e5d78832bf259554b052f53968)
- [`MWB-2250`](https://jira.open-xchange.com/browse/MWB-2250): Added sanity check for Task Status. [`1858544`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/18585443f2d73e14226ae52a5c680b74e5dd63e2)
- [`MWB-2272`](https://jira.open-xchange.com/browse/MWB-2272): Explicitly LIST a folder once not contained in `LIST "" "*"` queried from IMAP server [`ab95588`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ab955885ca573a037e801b06a2616321e3cb6bb2)
- [`MWB-2265`](https://jira.open-xchange.com/browse/MWB-2265): Prefer to use config-cascade-wise configured value for `com.openexchange.imap.imapSupportsACL` [`c01a70a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c01a70aa5fe900c8bfbb6abaf262f68ff1ce5967)
- [`MWB-2274`](https://jira.open-xchange.com/browse/MWB-2274): Properly encode dynamically inserted part of LDAP
folder filters [`91fe39e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/91fe39ea2e5dda8e451afb181a1092f34b9ca740)
- [`MWB-2277`](https://jira.open-xchange.com/browse/MWB-2277): Changed displayed error messages according to customer's suggestion [`7754cad`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7754cad2275a6761b6332ee3fd0900b687299ba0)
- [`MWB-2242`](https://jira.open-xchange.com/browse/MWB-2242): Corrected invocation for 'list_unassigned' in filestore [`08481d7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/08481d79bf774fa03730b2a68f13fcafbec9894f)
- [`MWB-2280`](https://jira.open-xchange.com/browse/MWB-2280): Reset attendee transparency on rescheduling [`2f13573`](https://gitlab.open-xchange.com/appsuite/platform/core/-/commit/2f135730dd474d7c1746fefe8c2e2647511a3d51)

### Security

- [`MWB-2261`](https://jira.open-xchange.com/browse/MWB-2261): CVE-2023-29048 [`cf6b2f1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cf6b2f12e2104c3f896aab9015331a8878997fae) [`1ec3707`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1ec37077ac77235bd15867dd2e80918b7f2482de)

## [8.16] - 2023-08-01

### Added

- [`ASP-131`](https://jira.open-xchange.com/browse/ASP-131): Implemented a MailExportService that converts e-mails to PDFs
  * [`SCR-1235`](https://jira.open-xchange.com/browse/SCR-1235): Introduced a new action to the 'mail' module for exporting mails as PDFs
  * [`SCR-1236`](https://jira.open-xchange.com/browse/SCR-1236): Introduced new properties for the MailExportService
  * [`SCR-1237`](https://jira.open-xchange.com/browse/SCR-1237): Introduced new properties for the CollaboraMailExportConverter
  * [`SCR-1238`](https://jira.open-xchange.com/browse/SCR-1238): Introduced new properties for the GotenbergMailExportConverter
  * [`SCR-1239`](https://jira.open-xchange.com/browse/SCR-1239): Introduced new properties for the CollaboraPDFAConverter
  * [`SCR-1240`](https://jira.open-xchange.com/browse/SCR-1240): Introduced a new capability to activate the PDF MailExportService [`4d0de04`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4d0de0467de41aa62795062d5e6e1c88e8d5eeeb)
- [`MW-2036`](https://jira.open-xchange.com/browse/MW-2036): added contact collector documentation [`5441175`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5441175fda9857746dcb0eb13ae99d18ede227f6)
- [`MW-2073`](https://jira.open-xchange.com/browse/MW-2073): Log any HTTP header [`852548b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/852548b26bca8f25b82fd339edb3bddbd77393cf)
- [`MWB-2238`](https://jira.open-xchange.com/browse/MWB-2238): allow to configure a purge folder for trash deletion
  * The property com.openexchange.imap.purgeFolder allows to configure a
parent folder for renamed trash folder. If one of those folders is
configured then the trash is not deleted by the middleware itself. [`f870dcc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f870dcc2a9b82fbdf575130d7b98bb769f189448)
- Add missing configuration for new packages [`c8651ec`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c8651ec979a9d9f2ffffef6ed24db5ac92f8949c)

### Changed

- Improve markdown for core-mw chart [`6518c42`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6518c42efb82472f3865feab5f9dcc7ceb4ec678)
- [`MW-1862`](https://jira.open-xchange.com/browse/MW-1862): Upgrade encrypted data dynamically during usage
  * Throw exception if legacy encryption is detected in CryptoService
  * Services/storages detect legacy encryption by this exception and recrypt secrets themselves (by using async task)
  * If shared item protected by secret with legacy encryption is accessed, use LegacyCrypto and log this event (not possible to recrypt here)
  * When users logs in, all items shared by him are collected and checked if secrets needs to be recrypted
- [`SCR-1233`](https://jira.open-xchange.com/browse/SCR-1233): Update encryption for passwords of anonymous guest users [`d5843c4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d5843c4734abfdffd8a8fb95379db72afe78e9bb)
- [`MW-1840`](https://jira.open-xchange.com/browse/MW-1840): Reworked the CryptoService
  * changed the encrypting algorithm to AES/GCM/NoPadding
  * deprecated the encrypt and decrypt methods with the old mechanisms
  * removed default salting - Now callers are responsible for their salts
  * introduced fallbacks for the old mechanics
  * [`MW-1894`](https://jira.open-xchange.com/browse/MW-1894): moved CryptoUtil to c.o.java, replaced all instances of SecureRandom with the centralised version [`7a3e3e5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7a3e3e542588489b1b39847c6b4ec21474d8be62)
- [`MW-1861`](https://jira.open-xchange.com/browse/MW-1861): Use Implicit Salt in CryptoService
  * Utilise argon2i for password hashing
  * Use the legacy crypto for the Key-based methods
  * Let the callers dictate the byte size for salt and iv
  * Use a 96bit key for IV
  * Re-create secure random after a specified amount of time
  * Use implicit salt and IV in CryptoService [`2faf2ca`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2faf2cadf657d152eb0124e000deb4ebaa6d784c)
- [`SCR-1252`](https://jira.open-xchange.com/browse/SCR-1252): Updated Netty NIO libraries from v4.1.89 to v4.1.94 [`8c7eb32`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8c7eb3272baff42bc2f4f2fe0498b792703cfa6e)
- [`SCR-1247`](https://jira.open-xchange.com/browse/SCR-1247): Updated pushy library from v0.15.1 to v0.15.2 [`8365cff`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8365cffa89d215a934577b4154c46d08d3dc25a2)
- [`SCR-1245`](https://jira.open-xchange.com/browse/SCR-1245): Updated metadata-extractor from v2.17.0 to v2.18.0 [`bd4e29c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bd4e29c5b5c1f0582a3b5ef34306f89bff73ed18)
- [`SCR-1253`](https://jira.open-xchange.com/browse/SCR-1253): Updated lettuce library from v6.2.3 to v6.2.5 [`731ca0b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/731ca0b9afb8bd1f9f03f9c06ef81aa573e4a931)
- [`SCR-1246`](https://jira.open-xchange.com/browse/SCR-1246): Updated Google Guava from v31.1 to v32.1.1 [`1cbe1a4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1cbe1a45f6c6078b5eada6a1d464d1c4cc4935d5)
- [`SCR-1231`](https://jira.open-xchange.com/browse/SCR-1231): Updated OSGi target platform bundles [`2a0ea4e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2a0ea4e6b361eaf87602846870dccd1bacaa0da6)
- [`MWB-2208`](https://jira.open-xchange.com/browse/MWB-2208): Do log possible IMAP protocol errors while trying
to acquire a part's content [`8867c1b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8867c1bbe795931260e5701849d5856726ccf5f4)
- [`SCR-1255`](https://jira.open-xchange.com/browse/SCR-1255): Updated Apache Tika library from v2.6.0 to v2.8.0 [`d19b0fc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d19b0fcafa6ec70314b6805821486591014795d8)
- [`SCR-1256`](https://jira.open-xchange.com/browse/SCR-1256): Upgraded Javassist to 3.29.2-GA [`6a6ac84`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6a6ac84f4e92adf56c78eb50ba1c89755b901466)
- [`SCR-1244`](https://jira.open-xchange.com/browse/SCR-1244): Updated htmlcleaner from v2.22 to v2.29 [`ee140df`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ee140df60262cbf580728f85489366d473165b04)
- [`SCR-1243`](https://jira.open-xchange.com/browse/SCR-1243): Updated dnsjava from v3.5.1 to v3.5.2 [`558227a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/558227a19aec69c24fc85142f5566e7b0ede3f8a)

### Removed

- [`SCR-1254`](https://jira.open-xchange.com/browse/SCR-1254): removed support for content_disposition=inline and delivery=view parameter [`5d8bfd4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5d8bfd43e8a1fd2bede82cbceceddf05488a2837)

### Fixed
- [`MWB-2258`](https://jira.open-xchange.com/browse/MWB-2258): Adjust 'credentials' table for enhanced crypto service
- [`SCR-1267`](https://jira.open-xchange.com/browse/SCR-1267): Extend password columns in db to store encrypted passwords [`e6cdc21`](https://gitlab.open-xchange.com/appsuite/platform/core/-/commit/e6cdc218437c4f7aed902ffc52b240d6af6aa126)
- [`MWB-2253`](https://jira.open-xchange.com/browse/MWB-2253): removed unused import  [`804a806`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/804a806dbda5ea0b0783252d8f138b66cbb0ef76)
  * to fix not working imageconverter and documentconverter
- [`MWB-2252`](https://jira.open-xchange.com/browse/MWB-2252): Keep possible HTML comment markers when examining CSS [`62add69`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/62add691dc3bb96282666a726df411ab86902d4c)
- [`MWB-2251`](https://jira.open-xchange.com/browse/MWB-2251): Prefer configured call-back URL regardless of [`df0ed24`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/df0ed24f52cb12b129bd72fca6ea1ec1b2209fe6)
  * applicable dispatcher prefix
- [`MWB-2186`](https://jira.open-xchange.com/browse/MWB-2186): The upload of big files gets slower and slower (against MW 8.x) [`d55359b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d55359b3eead71013b050c3ed3cc5bc5dd487940) [`0efa733`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0efa733b00bcd143a7e703e06fb85ee34502f1df)
- properly load reseller service on demand [`3166ed3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3166ed3d51b66010a88fe96be70d286b2184f804)
- [`MWB-2228`](https://jira.open-xchange.com/browse/MWB-2228): Move EventsContactHalo into com.openexchange.halo.chronos bundle [`8171cd7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8171cd7f2b096db29de4500ead040e520402fa25)
- [`MWB-2221`](https://jira.open-xchange.com/browse/MWB-2221): Append additionally available plain text content to existent one [`738b68c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/738b68c57164d30d7a739b22fb2d825102aca71c)
- [`MWB-2184`](https://jira.open-xchange.com/browse/MWB-2184): Add support for extraStatefulSetProperties and make use of ox-common.pods.podSpec [`92f6ce9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/92f6ce9e9c5c52b668e4727ee4e9cb28f7782c73)
- [`MWB-2240`](https://jira.open-xchange.com/browse/MWB-2240): Don't output inline images as attachment [`8de296c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8de296ce750992941ca08c3726e0b89726c33c9f)
- [`MW-2203`](https://jira.open-xchange.com/browse/MW-2203): Omit OS version for web clients [`fca25d2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fca25d2ec442de057ccfb23c3037eae84598ab3c)
- [`MWB-2228`](https://jira.open-xchange.com/browse/MWB-2228): Move contact halo into com.openxchange.server bundle [`691da48`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/691da48ec7ad8fcf15267505445bed9dafdcbbfd)
- [`MWB-2231`](https://jira.open-xchange.com/browse/MWB-2231): Confirmation buttons not working when inviting a person
to a series exception [`19f8753`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/19f87531c8c7ead6125d3fd58233eab34b99d2c7)
- [`MWB-2248`](https://jira.open-xchange.com/browse/MWB-2248): Pass proper range when querying messages from contained sub-accounts if Unified Mail [`f9c8829`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f9c8829daf658afe229d3c626f2b24b6de46263d)
- [`MWB-2227`](https://jira.open-xchange.com/browse/MWB-2227): Attendee cannot be re-invited to occurrence of event
series [`553bb34`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/553bb34c8391dcd5a26c7b57eea6b4c1ca3c53a2)
- [`MWB-2233`](https://jira.open-xchange.com/browse/MWB-2233): Removed ulimit configuration from start script [`128ba0b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/128ba0badb30393f0ccce020e01cac1747e24d77)
- [`MWB-2241`](https://jira.open-xchange.com/browse/MWB-2241): Lowered log level to DEBUG when moving active/idle
sessions to first short-term session container [`82938e9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/82938e9301c3bc0ad3b3deb1e0acf18f6c7ab3dc)
- [`MWB-2223`](https://jira.open-xchange.com/browse/MWB-2223): convert all images with CID for the html body [`c7776e1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c7776e18c6805b82a5356b0c6758cca9e38c8808)
- [`MWB-2210`](https://jira.open-xchange.com/browse/MWB-2210): Consider virtual folders when getting attachments
through chronos module [`d992d75`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d992d754f25d08290112a9883cf7ae77d9f04a81)

## [8.15] - 2023-07-05

### Added

- [`MW-2045`](https://jira.open-xchange.com/browse/MW-2045): Introduced separate bundle for parsing a schedule expression and for initiating periodic tasks. Refactored database clean-up framework to have a "maintenance" window, in which execution of general clean-up jobs is permitted. It also accepts custom clean-up jobs having their own schedule. [`8b9bb19`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8b9bb19e38f5de4bd43794147408ce3c9ea42976)
- [`MW-2020`](https://jira.open-xchange.com/browse/MW-2020): Dedicated simple HTTP liveness end-point for early access to liveness check & await availability of database during start-up [`a476d76`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a476d768fceb142c7d20199b831c4d5aa82dfab1)
- [`MW-1084`](https://jira.open-xchange.com/browse/MW-1084): Added support for segmented updates with OIDC [`2277d3a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2277d3a64587fe66b6ee38ca86bd07fd5aac1ca0)
- [`MW-2073`](https://jira.open-xchange.com/browse/MW-2073): Log any HTTP header[`6bdd0d5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6bdd0d534313135ce7b62748365def6971a8de04)

### Changed

- [`MWB-2212`](https://jira.open-xchange.com/browse/MWB-2212): Allow specifying deferrer URL with path [`cab25e7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cab25e7b4ac6775dc5039d7f03e42bad3441a8df)
- [`MWB-2200`](https://jira.open-xchange.com/browse/MWB-2200): Output JSON session representation if it becomes too big [`118f0db`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/118f0db9be9f7bb52da4e57c05403f2810562a9c)
- [`MWB-2059`](https://jira.open-xchange.com/browse/MWB-2059): Improved access to queried message range in case IMAP server does not support SORT capability [`fffe20c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fffe20c32f915db0ae2902cb3349b86d08b66ce1)
- [`DOCS-4766`](https://jira.open-xchange.com/browse/DOCS-4766): Include pdftool from docker image [`4d9d0ad`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4d9d0adfa7335e5b440a3505bbfcc6635b16e97f)
- Enhance session representation managed in Redis storage by user database schema [`3798214`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/37982143af42b3f3067c95e4f7a049c5f79083f9)
- Enhance session representation managed in Redis storage by segment marker (that is the target database schema by now) [`c008e24`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c008e24395266689ca32390435440364b20890ef)
- [`MWB-2214`](https://jira.open-xchange.com/browse/MWB-2214): Improved error handling in case a `javax.mail.FolderNotFoundException` occurs [`eb5a9f1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/eb5a9f18c6b2890320aeebb11326173c2a370891)

### Fixed

- [`MWB-2193`](https://jira.open-xchange.com/browse/MWB-2193): missed to remove deprecated servlet path to admin API.
  * removed servlet path registration for obsolete path
  * removed obvious parts related to AXIS2 [`017321e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/017321eb1015288b1cf267f284047e7724d40f7f)
- [`MW-2050`](https://jira.open-xchange.com/browse/MW-2050): Refactored message alarm delivery worker to orderly use database locks [`c99b0b5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c99b0b5c280ad7f204c77fbed5828ed1f986b921)
- [`MWB-2130`](https://jira.open-xchange.com/browse/MWB-2130): Try to perform hard-delete by delete-through-rename [`db8afce`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/db8afce5e128665a4fa5e0a164975d06d7b5a9b2)
- [`MWB-2182`](https://jira.open-xchange.com/browse/MWB-2182): Fixed typo "(E|e)xcpetion" in code [`b054b35`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b054b35e14fa7aab86137485b8b406a773d6b1c8)
- [`MWB-2130`](https://jira.open-xchange.com/browse/MWB-2130): Try to perform hard-delete by delete-through-rename [`54ac301`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/54ac301a665082498078c88397f943512b91bb24)
- [`MWB-2201`](https://jira.open-xchange.com/browse/MWB-2201): Do translate standard folders of secondary accounts as well [`b549cf4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b549cf41bbbd8dc60800686a99aaba6b83a75772)
- [`MWB-2196`](https://jira.open-xchange.com/browse/MWB-2196): Pay respect to order parameter when sorting contacts by special sorting [`1db09a3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1db09a31d2714f2016579d79adc97e5735fc8cf9)
- [`MWB-2168`](https://jira.open-xchange.com/browse/MWB-2168): Support AWS S3 IMAP role using `AWS_WEB_IDENTITY_TOKEN_FILE` environment variable [`2b35ea8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2b35ea856e1c0a81f69fac93bfa46f462736a805)[`2d9ad76`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2d9ad769cd24c179edb123b7182fd1f2109adb9f)
- [`MWB-2187`](https://jira.open-xchange.com/browse/MWB-2187): Add necessary imports [`61dd61e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/61dd61e4f850a3d4defee96218b44c275110868e) [`51eb12f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/51eb12f0200bcfd516bcd4427df960e0e8d241a8)
- [`MWB-2181`](https://jira.open-xchange.com/browse/MWB-2181): Fixed possible null dereference [`15519ca`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/15519cab778d61c5f800d9e3b4f7dbc0a10122b3) [`f059c8d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f059c8d04c74af422d150a6cd1961efbe01e2161)
- [`MWB-2187`](https://jira.open-xchange.com/browse/MWB-2187): Assume configured IMAP host for IMAP authentication does not need to be checked against blocked hosts (see `com.openexchange.mail.account.blacklist`) [`0971c88`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0971c8845fffd6469ca9e8f6bc75e00cb0937917)
- [`MWB-2189`](https://jira.open-xchange.com/browse/MWB-2189): Orderly close database statements [`083f2c3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/083f2c331fc2b5c36ccebf80b7a21e060a8dd6f3)
- [`MWB-2199`](https://jira.open-xchange.com/browse/MWB-2199): Mention the affected YAML file if an invalid format is detected [`1b4a086`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1b4a08654cf5467304faf52b63136c438d3b1c3a)
- [`MWB-2178`](https://jira.open-xchange.com/browse/MWB-2178): Handle possible null session on account retrieval [`357cc79`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/357cc79d366ebaa1803663ae7da528d1c3415fb0)
- [`MWB-2045`](https://jira.open-xchange.com/browse/MWB-2045): Omit specific OS version for macOS clients (2) [`78a60c1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/78a60c1c0452d7388255e26603496839d57736bd)
- [`MWB-2194`](https://jira.open-xchange.com/browse/MWB-2194): Fixed typo in property description [`b71221f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b71221f91b98d1b548013b110c209f03060e1674)
- [`MWB-2179`](https://jira.open-xchange.com/browse/MWB-2179): Orderly handle iTip request without method [`58fbf02`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/58fbf02aed853638ef42b982983444cde0e8603a)
- [`MWB-2180`](https://jira.open-xchange.com/browse/MWB-2180): Check for possible null return value when looking-up a user with invalid user identifier [`44c3ede`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/44c3ede50e4b445cb90522b9762a7130f958acec)
- [`MWB-2185`](https://jira.open-xchange.com/browse/MWB-2185): Use SMTP default settings when changing a user's assigned SMTP server [`d1c73cb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d1c73cba67746b7a63041877625e6712d6042106)
- [`MWB-1764`](https://jira.open-xchange.com/browse/MWB-1764): Don't check against blocked hosts/allowed ports when obtaining status for subscribed mail accounts [`2e7f30a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2e7f30afeb7a942129b72a2721b59d3483fcc19c)
- [`MWB-2214`](https://jira.open-xchange.com/browse/MWB-2214): Try to re-open folder in case a `javax.mail.FolderNotFoundException` occurs (IMAP folder not LISTed, but SELECTable) [`d60a70c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d60a70c36a1b85ea921c96b45cb3260c4df1b92e)


## [8.14] - 2023-06-06

### Added

- [`MW-1545`](https://jira.open-xchange.com/browse/MW-1545): Option to hide own Free/Busy time
  * Users can now configure whether their free/busy data is exposed to others (values `all`, `none`, `internal-only`)
  * Appointments that are visible by other means (shared folder, common participation) continue to be visible
  * Default value of setting is `all`, configurable and protectable ([`SCR-1197`](https://jira.open-xchange.com/browse/SCR-1197)), and exposed to clients in JSlob ([`SCR-1198`](https://jira.open-xchange.com/browse/SCR-1198)) [`e5d91c8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e5d91c83f05e2c7d9b810cf8c9eafe74b8b158a5)
- [`MW-1981`](https://jira.open-xchange.com/browse/MW-1981): Added caching to the resource storage [`ed81544`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ed815443de45c8e13114432bf7fb2b248c4a1ec7)
- [`SCR-1213`](https://jira.open-xchange.com/browse/SCR-1213): Introduced event flag 'all_others_declined' to indicate if one might be alone in a meeting [`ae51f2c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ae51f2c1b41e370a852c373c2e4cdecbc1b6dd41)

### Changed

- [`MW-2007`](https://jira.open-xchange.com/browse/MW-2007): Remove man pages from image [`85e335d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/85e335de270a58b8da37186efb7d8572c9ab7643)
- [`SCR-1219`](https://jira.open-xchange.com/browse/SCR-1219): Upgraded JSoup library in target platform (con.openexchange.bundles) from v1.15.3 to v1.16.1 [`4d3cbc5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4d3cbc557b2286b3fc17bb37c4722efec82d601e)
- [`INF-173`](https://jira.open-xchange.com/browse/INF-173): Disable `open-xchange-dataretention-csv` by default [`9048c7d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9048c7d215b9c21b2d687191cd56fb500610d799)

### Fixed

- [`MWB-2160`](https://jira.open-xchange.com/browse/MWB-2160): Avoid excessive parsing of E-Mail addresses possibly containing CFWS personal names; e.g. `&lt;bob@example.com&gt; (Bob Smith)` [`2fb55a6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2fb55a6edb6b8e2ceab0c3d70dc036460a177c96) [`2ed855c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2ed855c76cbe58b6e949a1f2d1209c83c2960bc3)
- [`MWB-2150`](https://jira.open-xchange.com/browse/MWB-2150): Don't expunge messages from POP3 storage that could not be added to backing primary mail storage [`6cf89a7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6cf89a7422607bc7ecea51bbc14cf33c019d7dbd)
- [`MWB-2156`](https://jira.open-xchange.com/browse/MWB-2156): Make DAV UserAgents configurable
  * Also add the new user agent part `dataaccessd` to properly recognize the Mac Calendar clients
  * Introduced new `com.openexchange.dav.useragent.*` properties, see also [`SCR-1220`](https://jira.open-xchange.com/browse/SCR-1220) for details [`e46c9a6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e46c9a6ce428f239caa6f0d0febd75e798d72b5b)
- [`MWB-2158`](https://jira.open-xchange.com/browse/MWB-2158): Allow all folder names for iCAL feeds [`94c0f36`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/94c0f36e5aa9dbe98f9a676d4bcc4b3cccd850f4)
- [`MWB-2149`](https://jira.open-xchange.com/browse/MWB-2149): Prepare refreshing of subscriptions in a blocking manner to avoid having underlying HTTP being being recycled [`1bb9343`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1bb9343bebfa1af856bb30d4ec3021f103dd13ef)
- [`MWB-2171`](https://jira.open-xchange.com/browse/MWB-2171): Split orphan instances on import [`2db7d02`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2db7d02f5296c843271157bef9b2cd4cf1276c25)
- [`MWB-2167`](https://jira.open-xchange.com/browse/MWB-2167): Offered parameter and config option for sanitizing CSV cell content on contact export [`8b1d684`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8b1d684d4b40cab8f09554e7e63019eb4b42cb50)
- [`MWB-2137`](https://jira.open-xchange.com/browse/MWB-2137): Unable to Delete Contacts Account if Implementation Missing [`883b9bd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/883b9bdce4124fd4e3cb86fe46aa69b13b7e60ad)
- **Redis Session Storage**: Use `tags` to differentiate between common and brand-specific session metrics [`6655f6f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6655f6f205aea0cb508d181c2447e524c2778354)
- [`MWB-2144`](https://jira.open-xchange.com/browse/MWB-2144): Disabled Hazelcast-based session test since Hazelcast has been replaced by Redis [`cab9736`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cab9736ee0e2de4d6b5965baa6a019ffae142293)
- [`MWB-2161`](https://jira.open-xchange.com/browse/MWB-2161): Allow relative paths in yaml file names [`9dd17f3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9dd17f370ee6ec2a1e5f90f4e10fac476b82c4b3)
- [`MWB-2162`](https://jira.open-xchange.com/browse/MWB-2162): Limit number of considered filestore candidates to a reasonable amount when determining the filestore to use for a new context/user [`eb9e0ca`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/eb9e0ca1236dcff0dccab5b5c3c9dc5cb6c1be7a) [`c9b4b4d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c9b4b4dbb77aeafac395f514c5a49adedfd37daf)
- [`MWB-2139`](https://jira.open-xchange.com/browse/MWB-2139): Check a session's origin for both - guest and application-specific authentication - prior to validating mail access' authentication data [`43229c2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/43229c21deb10a443702c2cfc59267baa7af1b5e)
- [`MWB-2153`](https://jira.open-xchange.com/browse/MWB-2153): Test for `application/x-pkcs7-signature` as well as `application/pkcs7-signature` [`e99052d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e99052d966e7d002fe5ca9a2fe812716d87f2816)
- [`MWB-2165`](https://jira.open-xchange.com/browse/MWB-2165): Keep quotes in local part of an E-Mail address when extracted from ENVELOPE fetch item [`afdece9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/afdece9846d1a9a72e718d8c084edd4b1d083ee9) [`57df52f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/57df52f0b30420b6c37a56651d0a24ec76b11202)
- Prevent invalid Resource Names for new CalDAV Collections [`c7fae63`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c7fae6381845cfa2e280aa23e9553d65443c8d7b)
- [`MWB-2143`](https://jira.open-xchange.com/browse/MWB-2143): Accept `harddelete` parameter to let client instantly delete a previously opened composition space [`ec80711`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ec8071161abcd6332fdc74bc00b965c3306dc5cb) [`8ad2a99`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8ad2a99855839bbe4cd00736de40eb6b9cb314df)
- [`MWB-2159`](https://jira.open-xchange.com/browse/MWB-2159): Avoid unnecessary error in case of attempting to remove an already dropped session [`a9e1914`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a9e1914bd58537c1711f2aabff8af9f48f1daea0) [`c4ef016`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c4ef016fa095b94c79dcb42e99009407c923e89b)
- [`MWB-2138`](https://jira.open-xchange.com/browse/MWB-2138): DAV file upload fails with redis session storage [`364df81`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/364df81e8856f5817a6e32c26534537a0f310b18)
- [`MWB-2149`](https://jira.open-xchange.com/browse/MWB-2149): Prepare refreshing of subscriptions in a blocking manner to avoid having underlying HTTP being being recycled [`e5da60b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e5da60b9641ed10afe0a3da162853371c7a96b58)
- [`MWB-2164`](https://jira.open-xchange.com/browse/MWB-2164): Use header for authorization instead of query string [`4634856`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/463485636c576323b5ac13795ed1343c0418c584)
- [`MWB-2150`](https://jira.open-xchange.com/browse/MWB-2150): Follow up, reset parameter index before re-using [`6370ec6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6370ec62332aac68deb48b0d6335a2bb8cad72dc)
- [`MWB-2145`](https://jira.open-xchange.com/browse/MWB-2145): NumberFormatException on partial file upload [`1feeed1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1feeed1472eb4d8298416c766df1ba3c6d0a766d)


## [8.13] - 2023-05-03

### Added

- [`MW-1909`](https://jira.open-xchange.com/browse/MW-1909): iTIP Analysis and Apply actions for Resource Notification Mails
  * Scheduling mails to/from booking delegates of managed resources are sent as iMIP messages
  * Introduced additional header `X-OX-ITIP` for quick identification of such mails, obeying unique server id ([`MW-1405`](https://jira.open-xchange.com/browse/MW-1405))
  * Existing iTIP analysis and apply workflows were extended accordingly
  * Consolidated notifications and scheduling messages and their transport providers
  * Introduced property `com.openexchange.calendar.useIMipForInternalUsers` to switch to full iMIP messages for internal receivers generally ([`SCR-1191`](https://jira.open-xchange.com/browse/SCR-1191)) [`91c0491`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/91c0491eaa4da26d9c3722254ccc6dd80c77fc4d)
- [`MW-1908`](https://jira.open-xchange.com/browse/MW-1908): Send Calendar Notifications to Resource Owners
  * Booking delegates now receive mails upon new, modified, deleted events with the resource
  * Organizers now receive mails upon replies for their booking requests
  * `SENT-BY` property of originator/recipient as well as mail's  `From` / `Sender` header are set appropriately [`c9b28c4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c9b28c4dc613e43c26f934e3a29be739399bf072)
- [`MW-1405`](https://jira.open-xchange.com/browse/MW-1405): Introduced a unique server identifier [`d891c9d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d891c9d6885ca6c8349df1aae6d94ce54c71f2ae)

### Changed

- [`MW-1913`](https://jira.open-xchange.com/browse/MW-1913): Changed mail push config to prevent multiple notifications
  * [`SCR-1158`](https://jira.open-xchange.com/browse/SCR-1158): Added toggle switches for mail push implementations, made existing properties reloadable [`6156818`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6156818b07bf668f13f75fa467bcabc0f0898203)

### Deprecated

- [`SCR-1208`](https://jira.open-xchange.com/browse/SCR-1208): Deprecation of Internal OAuth Authorization Server [`cc7d99a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cc7d99a9634e1f402f04f418f6ef2175da5e22a6)

### Fixed

- [`MWB-2124`](https://jira.open-xchange.com/browse/MWB-2124): Change PRIMARY KEY through creation of a temporary table if the attempt to drop PRIMARY KEY is prohibited by MySQL server [`78d6f9a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/78d6f9a34a5fecf253a0339e201dd2a83ff5b063)
- **IMAP**: Allow fast `EXPUNGE` of trash folder in "fire & forget" fashion [`29c12f9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/29c12f93f1c5e68accdfacfc8a4536d01d1767fa) [`3fc0079`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3fc0079d305bc84760aa7bd9cc92207a2e2f8968)
- [`MWB-2118`](https://jira.open-xchange.com/browse/MWB-2118): No Option to prevent creation of Guest Users with Specific Email Addresses[`595c926`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/595c926d19bd7c43850aebd041388ae8355d712b)
  * [SCR-1206](https://jira.open-xchange.com/browse/SCR-1206)
- [`MWB-2110`](https://jira.open-xchange.com/browse/MWB-2110): Proper imports of Netty IO packages [`e1a850d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e1a850d38474d47664570b4f243711ca73aeb01a)
- [`MWB-2125`](https://jira.open-xchange.com/browse/MWB-2125): Do not batch-delete more than 1,000 objects from S3 storage using DeleteObjects request [`204ef8e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/204ef8e3efbab60cbe873936c6102a0d47f25db7)
  * See https://docs.aws.amazon.com/AmazonS3/latest/API/API_DeleteObjects.html
- [`MWB-2045`](https://jira.open-xchange.com/browse/MWB-2045): Omit specific OS version for macOS clients [`b0c9b40`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b0c9b403b77281d241d0188101c0720631de3b81)
- [`MWB-2129`](https://jira.open-xchange.com/browse/MWB-2129): Orderly surround column name with backpack characters '\`' [`bfc75b7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bfc75b72cee35d13446c467ad14a839a5bd50fb7)
- [`MWB-2121`](https://jira.open-xchange.com/browse/MWB-2121): Properly check master authentication first for getData call [`dcca450`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dcca450cab27b112961f05fd94a94bc16cee4291)
- [`MWB-1893`](https://jira.open-xchange.com/browse/MWB-1893): Error when deleting appointment series with multiple different organizers [`a9dbced`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a9dbced73c4416501e078e58f0edc8315f8e8b76)
- [`MWB-2122`](https://jira.open-xchange.com/browse/MWB-2122): Update lastmodified timestamp when decrementing use count [`917e8a0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/917e8a002ed81cfd432d232f1f78858e2f0d7bce)
- [`MWB-2119`](https://jira.open-xchange.com/browse/MWB-2119): Optimized cleanup job & settings [`52068af`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/52068af937fc091656d8ccebfb11542b1bcd7939)
- [`MWB-2128`](https://jira.open-xchange.com/browse/MWB-2128): CalDAV: Unexpected runtime exception on REPORT [`f3bda8b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f3bda8bcfd077140dfeb29aa84f347e6a2def65d)
- [`MWB-2116`](https://jira.open-xchange.com/browse/MWB-2116): Correctly use commands for POP3 [`6b8749c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6b8749c1693b6f9edcc8b6c2d8d40059afb86d51)
- **IMAP**: Set proper status for IMAP `AUTHENTICATE` command [`89c0766`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/89c076676192592f84be89b18477ca9bcdc60492)
- [`MWB-2095`](https://jira.open-xchange.com/browse/MWB-2095): Conflicting folder "Userstore" exposed to Drive Clients [`29c3373`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/29c3373fa27824efd42541062a8765ff85b5f9eb)
- [`MWB-2127`](https://jira.open-xchange.com/browse/MWB-2127): Re-adding a resource leads to a permission error [`33b804d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/33b804d56b4e5bc463f34fd2e085cd6048f05ec3)
- [`MWB-2103`](https://jira.open-xchange.com/browse/MWB-2103): Missing the verb in calendar invitation email template for it_IT [`822aafc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/822aafcc22798eb61f7a87596c0452aa5a8637d4)
- [`MWB-2120`](https://jira.open-xchange.com/browse/MWB-2120): Fixed the documented default value for `com.openexchange.oidc.hosts` [`5427c82`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5427c8270c58cacb49dfef44717703c6566e320f)
- [`MWB-2134`](https://jira.open-xchange.com/browse/MWB-2134): Don't return an unmodifiable instance of java.util.Map [`186e9b1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/186e9b1520b57c29a7e4e3b059f5419724866f2a)
- [`MWB-2030`](https://jira.open-xchange.com/browse/MWB-2030): Orderly do set session- and share-cookie when resolving share link [`a417c17`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a417c17e098b76ad8cad9a98ff9e5fb9d0cb0e7e)
- [`MWB-2090`](https://jira.open-xchange.com/browse/MWB-2090): Enhanced the documentation to warn about potentially vulnerable password change scripts [`42334a4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/42334a40aa75a3cb7076cfaee1c57a92d62db8eb)
- Removed duplicate dot in internal password change notification [`c9d1baa`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c9d1baab76eb73f17c2353f21ccc5b9a2e7dc00e)

### Security

- [`MWB-1996`](https://jira.open-xchange.com/browse/MWB-1996): CVE-2023-26455 [`0c1acb4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0c1acb4c2ca9e98127b70cce326a5633395d6ebb) [`09781ae`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/09781ae9eeb5aaf214aeb86871ad16648cf77937)

## [8.12] - 2023-04-03

### Added

- [`MW-1747`](https://jira.open-xchange.com/browse/MW-1747): Introduce Redis-backed sessions service [`988cb4e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/988cb4e71aa8abb88a26b76b6747c62b2bb3057f) [`c3c7177`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c3c7177f67107c347a6ed982bbca0203dfe94e7c) [`b257128`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b257128c0109c1c7c690ea10c83fe3d0d92967bc) [`434eecd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/434eecd727773219f9284eb843e79003c2a45184)
- [`MW-2029`](https://jira.open-xchange.com/browse/MW-2029): Introduced metrics for Redis session storage [`12f8ebc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/12f8ebc2c3f43e7ec89ef1ddf828b772972e3638)
- [`MW-1841`](https://jira.open-xchange.com/browse/MW-1841): Allow enforcing 'STARTTLS' for IMAP, POP3, SMTP & sieve
  * [`SCR-1120`](https://jira.open-xchange.com/browse/SCR-1120) [`c4f90a2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c4f90a20b622d7b964d358b2a35f3903e01d8f00)
- [`MW-2029`](https://jira.open-xchange.com/browse/MW-2029): Introduced metrics for Redis session storage [`bbc8f11`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bbc8f116148c045f4ec720534b313d586d634775)
- [`MW-2023`](https://jira.open-xchange.com/browse/MW-2023): introduced possibility to block commands from apply [`59402e4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/59402e4747868ef080176a840ce283d628320511)
- [`MW-1986`](https://jira.open-xchange.com/browse/MW-1986): added login_hint and target_link_uri as parameter for oidc login [`adc2f10`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/adc2f1064236d1878ce6fd6875c620ce12508e64)
- made multiple servlet oauth capable [`91b3699`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/91b3699c60781d38648cc7976215c86e7627577a)
- [`MWB-2073`](https://jira.open-xchange.com/browse/MWB-2073): Introduced new property to disable adding a Sproxyd end-point to blacklist [`8617d91`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8617d91e8f946e0b17a40e9ed83f7e4e982010ff)
- [`SCR-1181`](https://jira.open-xchange.com/browse/SCR-1181): New Properties to Control 'used-for-sync" Behavior of
Calendar Folders [`94c4251`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/94c4251e15bed3876e62beb5785c059e3e05e24c)
- [`MW-2002`](https://jira.open-xchange.com/browse/MW-2002): Publish Changelog at documentation.open-xchange.com [`3f0b316`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3f0b316536e7161fef52d58ddd49466614c3a866)

### Changed

- [`MW-1864`](https://jira.open-xchange.com/browse/MW-1864): lost and found tests
  * fixed, refactored or deleted several tests
  * refactored SoapUserService and linked classes
  * deleted outdated indexedSearch [`7f57ae9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7f57ae9564307a947eb5691f07b87c40ded9f783)
- [`MW-1516`](https://jira.open-xchange.com/browse/MW-1516): Use IDBasedContactsAccess for CardDAV
  * [`SCR-1145`](https://jira.open-xchange.com/browse/SCR-1145): Refactored CardDAV to use IDBasedContactsAccess
  * [`SCR-1146`](https://jira.open-xchange.com/browse/SCR-1146): External contacts providers are now synced via CardDAV [`50a0416`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/50a041618d9a88a07aa61c5c7efabba55fadf3ab)
- Refactored to have gnu.trove as a bundle in target platform [`0ebe8ff`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0ebe8ffec584dfb960673b90ee36e6e17e777d93)
- [`MW-1947`](https://jira.open-xchange.com/browse/MW-1947): Updated vulnerable libraries
  * [`SCR-1176`](https://jira.open-xchange.com/browse/SCR-1176): Updated vulnerable 3rd party libraries [`4525709`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/452570967da8ed59fc8d2a977bb7df5b0ed676ef)
- [`MW-1955`](https://jira.open-xchange.com/browse/MW-1955): Hand-through possible Redis connectivity/communication errors to client during runtime & probe Redis end-point until available during start-up [`aae4f1c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/aae4f1c237e8478cdebccc0ff90181918c8f6a49)
- [`MW-1955`](https://jira.open-xchange.com/browse/MW-1955): Disable max. number of sessions by default for Redis session storage [`1b65ceb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1b65ceb220bb0392b21e0fbad417b383cf0795f6)
- [`MW-1947`](https://jira.open-xchange.com/browse/MW-1947): Updated vulnerable libraries [`cb95cbe`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cb95cbe7ac9078609bfa2a27cc8517d8f94d3627)
- [`MWB-2059`](https://jira.open-xchange.com/browse/MWB-2059): Removed corrupt sort by UID [`d316136`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d3161366c1d66e0e97659097db2533db00b222c3)
- [`MWB-2059`](https://jira.open-xchange.com/browse/MWB-2059): Fast sorting by IMAP UID in case sort by received date (INTERNALDATE) is requested [`776449b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/776449bb123cdb5cf6e6bc6b0e0e709e665cc97f)
  * Moved JCTools as bundle to traget platform & updated it from
v3.3.0 to v4.0.1 [`06f7328`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/06f7328003c15ab2d293c95686c107204fff5ee6)
  * Refactored to have gnu.trove as a bundle in target platform [`e6bf595`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e6bf59500de3e8e6d770ac69ad7d7b86c9916eb2)

### Fixed

- [`MWB-1982`](https://jira.open-xchange.com/browse/MWB-1982): Timeouts for external content do not cancel the connection
  * The fix allows to interrupt client connects and InputStream reads by having hardConnectTimeout and hardReadTimeout parameters that are used for external connections
  * Defaults to 0 (disabled)
  * The following services have a defined default of 120000 for 'hardReadTimeout' and 30000 for 'hardConnectTimeout': autoconfig-server, davsub, icalfeed, rssfeed, snippetimg, vcardphoto [`63b60eb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/63b60ebdfce79e87393d5b3f45225b4bae575d15)
- [`MWB-2040`](https://jira.open-xchange.com/browse/MWB-2040): Concurrency issue when moving a touched session to first session container. The moved session might not be "visible" for a short time. [`52069a4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/52069a4b2eec77846cce2f5034c5235dfbe497ae)
- [`MWB-2061`](https://jira.open-xchange.com/browse/MWB-2061): Organizer URI not preserved when creating Appointment [`7b3e574`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7b3e574f4d7b73df2fc59025bca9f5a38ba53706)
- [`MWB-2094`](https://jira.open-xchange.com/browse/MWB-2094): Yield a modifiable list instance from messages to copy [`3aacd7a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3aacd7a8677f4f89c9c150f08f0ab483e34afe5e)
- [`MWB-2056`](https://jira.open-xchange.com/browse/MWB-2056): Include all overridden instances in scheduling object resource [`3bd7550`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3bd7550c456633716bd86e01f88d5653202612e8)
- [`MWB-1975`](https://jira.open-xchange.com/browse/MWB-1975): start report generation in parallel to task generation [`72047d7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/72047d781bf5cb00960cdef89633b9f1833ac7a4)
- [`MWB-2101`](https://jira.open-xchange.com/browse/MWB-2101): Unnecessary Data Retrieved from Filestore when Serving [`d262bd1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d262bd1409c25b18f551ffbd18a0a531a25a3d3e)
- [`MWB-2081`](https://jira.open-xchange.com/browse/MWB-2081): Check table existence prior to deletion attempt (and recognize if developer accidentally passed the cause as last argument) [`2372064`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2372064cd2e1e4673fa32e1f6a155170d4291a12)
- [`MWB-2054`](https://jira.open-xchange.com/browse/MWB-2054): Auto-delete guests when owner of per-user filestore is
deleted ([`SCR-1193`](https://jira.open-xchange.com/browse/SCR-1193)) [`a296656`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a296656a6eefd64d75f8664366b04b8ca2b5878c)
- [`MWB-1985`](https://jira.open-xchange.com/browse/MWB-1985): delete all tasks in folders owned by deleted user [`5f26d66`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5f26d66d376b6a418e260345fae5c025ec86bc25)
- [`MWB-2055`](https://jira.open-xchange.com/browse/MWB-2055): Skip unrelated events when iterating events needing [`98b8140`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/98b814060d4d4902b79b851db32350ae1b68bf9b)
- [`MWB-2086`](https://jira.open-xchange.com/browse/MWB-2086): Potentially malicious SQL injection when using full-text autocomplete [`408fcda`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/408fcdab35207ec94cda3dfb487f4263342ed550)
- [`MWB-2022`](https://jira.open-xchange.com/browse/MWB-2022): Generate a generic error response providing SMTP server
response information in case an SMTP error code occurs while attempting
to send a message [`0d43966`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0d43966b16c703fce5e46f5a07bfdcad98e953ed)
- [`MWB-2091`](https://jira.open-xchange.com/browse/MWB-2091): Mark each messages of a multiple mail forward as forwarded [`2cde555`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2cde5558809fd888f83002b801aadd1398e4dfd4)
- [`MWB-2089`](https://jira.open-xchange.com/browse/MWB-2089): Quite old 3rd party library uses weakly accessible
sun.nio.ch package. User newer library making use of up-to-date JRE
tools instead. [`4ff5296`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4ff52960e29cbf40eec8bf82dcfc83f38f4101ac)
- Fixed reading alias from settings [`840d937`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/840d937d2049d75cae81a7ba1d28187e730b8a73)
- [`MWB-2080`](https://jira.open-xchange.com/browse/MWB-2080): Added details about 'baseDN' setting in LDAP client
configuration [`7668409`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/76684090784085fb5cdc3940d014eeafd15aaca7)
- [`MWB-2058`](https://jira.open-xchange.com/browse/MWB-2058): Populate 'uuid' column when registering a new server as [`692222c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/692222cf52a430e936cf304eb58351fa92d30d92)
- [`MWB-1982`](https://jira.open-xchange.com/browse/MWB-1982): Timeouts for external content do not cancel the connection [`75086ca`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/75086caa323f1d5c9c786f9644a171d0ef76dde3)

### Security

- [`MWB-2090`](https://jira.open-xchange.com/browse/MWB-2090): CVE-2023-26444 [`4a2b089`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4a2b089b0bcee7946dee79aa6fa5a47ee2389ce2)
- [`MWB-2102`](https://jira.open-xchange.com/browse/MWB-2102): CVE-2023-26451 [`f903cec`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f903cec7518145460a47886fd8e80a1d140e955b)


## [8.11] - 2023-03-08

### Added

- Generic watcher for input stream read processes [`85699c6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/85699c6e03f74feae952a5db51faec96da326e34) [`fd49709`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fd4970965a06adef921569e3e12aad3b410a3447) [`b8dcbad`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b8dcbadffa645332a12ca993e5029060585751da) [`129749c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/129749c5e942855601c116d15bfcb3a25dd7445e)
- Added possibility to filter mail drive files [`651999c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/651999caacae4c9cbe58bd256e0b0af8cd8f89f4)
- [`MWB-1959`](https://jira.open-xchange.com/browse/MWB-1959): added possibility to filter http api metric labels [`a75d3e0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a75d3e0d48b3326afd8982aad1f6fcba9c255721)
- Support hard timeout for processor tasks [`8f1b1b9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8f1b1b9f10b0c7284512e1912cf559de24aee8f2)
- [`SCR-1190`](https://jira.open-xchange.com/browse/SCR-1190): Added property accepting to define a timeout in
milliseconds when reading responses from IMAP server after a command has
been issued [`e2ef0ef`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e2ef0ef691a0808526fae274daab6337a4f62826) [`023c13c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/023c13cbff2d87828dd9e96f04129df94b0818c9) [`6e81751`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6e817513116da4bed86267603ac48dcace70805e)
- Add missing packages to cloud-plugins helm definition [`935005a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/935005a3b323739b15c3fd660229ea38a7c7ed4b)

### Changed

- Updated shipped VTIMEZONE resources [`4fd83de`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4fd83de896e5df3995ff60c8b39ecbff7e45a412)
- [`MWB-2049`](https://jira.open-xchange.com/browse/MWB-2049): Ensure no wrong push match has been determined for a certain push notification [`307d766`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/307d76606bfdd22a5e08fa8e91d7fb4e7122ece3) [`f314ec7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f314ec7aceb1e83d8a972646a7efb48284010dff) [`ad17da7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ad17da73d6cdc3585027ea28a9bddbb59aa53851) [`cfc57a8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cfc57a89050e95d49d0152e83b69c358614f3c52) [`9564229`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/956422992a2db193ec7f9d6348c3f3309bf656fb) [`5dadcfb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5dadcfbc1b4ebb617a66b4b43601bd19d3cee755) [`508879f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/508879f96e6b33726a79425b069c76470baf248c) [`70efa61`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/70efa6158b1c6a461f88915f8d9007dce5203019)
- [`MWB-2063`](https://jira.open-xchange.com/browse/MWB-2063): Lenient parsing for DTSTAMP property [`6401516`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/64015161c9341fc9888b4a2fca4e5dc9101d82a1)
- [`MWB-2039`](https://jira.open-xchange.com/browse/MWB-2039): Improved concurrency when loading time zone information [`2ac192a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2ac192a4118e10883f6247eb4eada3fc69fc3a31)
- [`MWB-2059`](https://jira.open-xchange.com/browse/MWB-2059): Let /mail?action=all end-point support "allow_enqueue=true" parameter [`70cf31d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/70cf31d9960f0d003cd4c347055eefc69219bde3) [`273c592`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/273c5920456cff60647d6d55614ea562ab1b9b81)
[`c7b656f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c7b656f2ee97168a15bf1c1e59224b269a9c1512)
- [`MWB-2040`](https://jira.open-xchange.com/browse/MWB-2040): Added some logging and introduced a session-list mutator lock [`c625aef`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c625aef6beab186caf909214a816fbc696c00e51) [`702e171`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/702e171b7d77c4417c1669239e91cb676d8acf6a) [`845d03c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/845d03c89a8477ab63ab20d9173a82d8b7b0ad83) [`e6938e0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e6938e01443b4c21af98433484eeb9fb7aef7f79)
- [`MW-1964`](https://jira.open-xchange.com/browse/MW-1964): optimizations referring to spectral findings [`a9ba5ed`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a9ba5eddd23ec788b37fc91917096415da53bb3e)
- [`MWB-1845`](https://jira.open-xchange.com/browse/MWB-1845): Ensure a reasonable size for buffers, which will be
allocated for writing data to a connection [`b47f248`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b47f2482146b77c08e9ff48984518484fed9a386) [`679df5a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/679df5ab86745d08775ec95b0536ae210decfd77)
- Use only one AtomicLong to generate request number [`8f34cbc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8f34cbc40ac739c7e2a84f1174e133d30eac3455)
- Uses timestamp to generate a unique name for the pre-update job so the helm chart can be applied multiple times in a row if needed. Also adds a (configurable) ttl to expire the job after 24hrs. [`cfcb71a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cfcb71a32f88330a109e27026e5c6296b425c2ad)
- [`MWB-2061`](https://jira.open-xchange.com/browse/MWB-2061): Prepare entity processor decoding for internal
organizers [`270fe7e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/270fe7e848f3b4a3eced04f40bda132dc71201cb)
- Upgraded logback-extension to 2.1.5 [`eed8bf3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/eed8bf3b9e5fd8618b844940ae272266562c3fb1)
- [`MWB-2031`](https://jira.open-xchange.com/browse/MWB-2031): Accept new property to disable black-listing of end-point for which an I/O error or HTTP protocol error was encountered [`8efbc56`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8efbc56e661b445f83c02332ec6d27e040dbe737)
- [`MWB-2039`](https://jira.open-xchange.com/browse/MWB-2039): Set missing log message argument [`d3fd63a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d3fd63ad9b7bda5b00b3a613847de251a83e6d99)
- Assume property "logback.threadlocal.put.duplicate" is "false" by default to use concurrent MDC property map [`6d84989`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6d84989472a113ec1a4ad85c0649fc6a7087d737)

### Removed

- [`MW-1974`](https://jira.open-xchange.com/browse/MW-1974): Drop Hazelcast Upgrade Packages [`46a7063`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/46a7063527acbaed2ad98c4f7a7ac240ae706527)
- [`MW-1774`](https://jira.open-xchange.com/browse/MW-1774): Removed ClusterTimeService [`940239e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/940239e0aa999d1477a01cc02f69a88142f403e9)
- [`MW-1778`](https://jira.open-xchange.com/browse/MW-1778): Disabled/deprecated the 'ramp-up' json action [`e20b7c4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e20b7c41005dab842d0117cce5eda8c950c55ac9) [`36107b9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/36107b9d90e0090fc0f3ce94483e67a5162e0dd0)
- [`MW-1767`](https://jira.open-xchange.com/browse/MW-1767): Enqueued the drop ldap ids update task [`d839294`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d83929429646338a00e17a79414c9a6cdd698732)

### Fixed
- [`MWB-2054`](https://jira.open-xchange.com/browse/MWB-2054): Auto-delete guests when owner of per-user filestore is
deleted ([`SCR-1193`](https://jira.open-xchange.com/browse/SCR-1193)) [`eaec0e9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/eaec0e9c0c53bc5cce8deacab05827c3a0fe6ec5)
- [`MWB-2048`](https://jira.open-xchange.com/browse/MWB-2048): Limit accepted POP3 server response to reasonable length/size [`478b986`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/478b9866c8e07d9f2bb8ff3ae696cf5f4ca7ffd2)
- [`MWB-1877`](https://jira.open-xchange.com/browse/MWB-1877): Avoid DNS rebinding attacks where possible (check against possible block-list on connection establishment) [`2bf40e2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2bf40e276ca54e814793ed9134923dd6b213eefe)
- [`MWB-2038`](https://jira.open-xchange.com/browse/MWB-2038): Respect possible IPV4-mapped IPv6 addresses when
checking if contained in a block-list [`e4566e4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e4566e4b799db30e3b0d8adc9018d07291d0939a) [`3a97e40`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3a97e40d6f6af2b2f63d8b5efb8fc34989aae730)
- [`MWB-2047`](https://jira.open-xchange.com/browse/MWB-2047): Limit accepted IMAP server response to reasonable
length/size [`9033774`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/90337743c35b0fdfe5e5086a752fa800c3d84e72)
- [`MWB-2037`](https://jira.open-xchange.com/browse/MWB-2037): Drop FOREIGN KEYs from several Groupware tables [`8a5ac87`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8a5ac870928aacd47b9104bf1334eea2540cbf2d)
- [`MWB-2057`](https://jira.open-xchange.com/browse/MWB-2057): Add XCLIENT extension support for sieve [`b5e1320`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b5e1320efad1e86e947f560af0a675ed50a328b7)
- [`MWB-2046`](https://jira.open-xchange.com/browse/MWB-2046): Limit accepted SMTP server response to reasonable
length/size [`1f8c5e2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1f8c5e2fa5d73da4e4f101b99083855a9eb7eee7)
- [`MWB-1395`](https://jira.open-xchange.com/browse/MWB-1395): Introduced limitation for number of queued image
transformation tasks [`9c17e53`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9c17e531a915877312860590fa2bd5963c94e08b)
- [`MWB-2020`](https://jira.open-xchange.com/browse/MWB-2020): only apply sanitizing to certain fields [`ac8c67c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ac8c67c376c15cfc73eba3b012781dcfb8104524)
- [`MWB-2019`](https://jira.open-xchange.com/browse/MWB-2019): Sanitize non whitespace control character [`5e1bf5d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5e1bf5dfbd3137174f4326f2246be32e165888e6)
- [`MWB-2025`](https://jira.open-xchange.com/browse/MWB-2025): Fixed avoidable exception on DEBUG logging [`dd4514a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dd4514a904431b0cda4c620a5cf11d7c36acc518)
- [`MWB-1967`](https://jira.open-xchange.com/browse/MWB-1967): Don't set i18n name for public IMAP namespace if there are multiple ones configured [`d26a8a5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d26a8a59708074d0bcd95064eee180425cccdc3c)
- [`MWB-2071`](https://jira.open-xchange.com/browse/MWB-2071): Indicate conflicting calendar object resource in
different collection via CALDAV:unique-scheduling-object-resource
precondition [`3e20448`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3e2044835b8cf48c8e6d80bb2e9131d884ee964f)
- [`MWB-2041`](https://jira.open-xchange.com/browse/MWB-2041): Fixed "file not exists" errors for single shared files [`c95b330`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c95b33022027e19211ad9cecb2132ffa85288e59)
- [`MWB-1790`](https://jira.open-xchange.com/browse/MWB-1790): Orderly complain about missing command-line arguments [`b0a4cf9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b0a4cf9e99a7d21844a3a4d5c69c8e428d1747bb)
- [`MWB-2068`](https://jira.open-xchange.com/browse/MWB-2068): Orderly accept connect parameters when updating a mail account's attributes [`f78c307`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f78c3074c62a167b39dc2b4023d1c04fd7d59e1f)
- [`MWB-2069`](https://jira.open-xchange.com/browse/MWB-2069): Yield "unsupported" result when analyzing links
pointing to own shares [`1dbc012`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1dbc012ffc8b30631314b9536e8ab48a38161817)
- [`MWB-2030`](https://jira.open-xchange.com/browse/MWB-2030): Orderly set session- and share-cookie when resolving
share link [`212bed8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/212bed8d4719f30bd6994b962f0bb87470485f58)
- [`MWB-2044`](https://jira.open-xchange.com/browse/MWB-2044): Only update folder last-modified if permissions are
sufficient [`f14cf42`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f14cf42f334d9a0fa83996137fcea794e9ff74fe)
- [`MW-1778`](https://jira.open-xchange.com/browse/MW-1778): Added missing annotation [`7b29de7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7b29de71fcd3af167fc5c568ef4eb1a0095634d9)

## [8.10] - 2023-02-08

### Added

- [`MW-1910`](https://jira.open-xchange.com/browse/MW-1910): Extended "needsAction" action to include Delegated
Resources
  * Lookup for events needing action is now also done for attendees the
user has delegated access to (resources and other users)
  * Introduced new parameter "includeDelegates" for
"chronos?action=needsAction" ([`SCR-1162`](https://jira.open-xchange.com/browse/SCR-1162))
  * Adjusted method signature of "getEventsNeedingAction" throughout
chronos stack ([`SCR-1163`](https://jira.open-xchange.com/browse/SCR-1163)) [`546c406`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/546c406def1520f8685a515c327f5d1f5ddc6691)
- [`MW-1898`](https://jira.open-xchange.com/browse/MW-1898): On-behalf management for Managed Resources
  * Actions 'updateAttendee' and 'update' in module 'chronos' can now be performed on behalf of a resource attendee
  * This can be indicated by targeting the virtual resource folder id
  * Added 'own_privilege' into 'resource' model to reflect the user's scheduling privilege for a certain resource ([`SCR-1154`](https://jira.open-xchange.com/browse/SCR-1154))
  * Participation status of managed resources will now be 'NEEDS-ACTION' if confirmation is pending
  * Initial hooks for subsequent notification messages are prepared [`ca32f9c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ca32f9c3b3c004d634fbb03fbc226e2f02622d0a)
- [`MW-1944`](https://jira.open-xchange.com/browse/MW-1944): New Action "getRecurrence" in Module "chronos"
  * Clients can now discover whether a change exception is considered as rescheduled or overridden
  * Introduced new action "getRecurrence" in Module "chronos" ([`SCR-1166`](https://jira.open-xchange.com/browse/SCR-1166))
  * Added corresponding "getRecurrenceInfo" implementation throughout Chronos stack ([`SCR-1167`](https://jira.open-xchange.com/browse/SCR-1167)) [`2ff537d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2ff537d219164c497eb4f5d32382446246b98229)
- [`MW-1931`](https://jira.open-xchange.com/browse/MW-1931): Extended provisioning for managed resources
  * [`SCR-1161`](https://jira.open-xchange.com/browse/SCR-1161): Extended SOAP provisioning interface for managed resources [`5af1d63`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5af1d63138b4bc1b58faacbd9ec9dbeb123e3a82)
- [`MW-1969`](https://jira.open-xchange.com/browse/MW-1969): Accept "mail" as original to add attachments to a
composition space referring to file attachments of existent mails #2 [`599a83d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/599a83d2f26567c5de2a366190abae17a0c2ba16)
- [`SCR-1181`](https://jira.open-xchange.com/browse/SCR-1181): New Properties to Control 'used-for-sync" Behavior of
Calendar Folders [`821254b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/821254be26bdbb6e6ba1a7d3172853e30dca3d16)
- [`INF-80`](https://jira.open-xchange.com/browse/INF-80): Activate additional languages in default App uite 8 installations [`b186a1d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b186a1d2d4e5c96ccf4045bce18e2dfbc9fdbbbb)
- [`MW-1969`](https://jira.open-xchange.com/browse/MW-1969): Accept "mail" as original to add attachments to a composition space referring to file attachments of existent mails [`fdbd9d6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fdbd9d649f4bca5721f4ff5ffa18fab842b6b12d)
- [`MW-1888`](https://jira.open-xchange.com/browse/MW-1888): Upgraded Socket.IO server components to support Engine.IO v4 and Socket.IO v3 [`512d654`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/512d65438ce93c9e250d47cedb47f4e0062600bb) (https://gitlab.open-xchange.com/appsuite/platform/core/commit/0cb2b2f041236ea8c90b1e5863d8bf922f14a442) [`57f4869`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/57f48697a861829d108a74c8950fc446ca46f33f)

### Changed

- [`MWB-2024`](https://jira.open-xchange.com/browse/MWB-2024): Upgraded logback-extension to 2.1.4
- [`MW-1912`](https://jira.open-xchange.com/browse/MW-1912): Allow multiple Password-Change Services [`0ad74d8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0ad74d8f6746b30bc9a242f6ad69c564a607e4b6)
- Fixed new warning since Eclipse 2022-06 "Project 'PROJECT_NAME' has no explicit encoding set" [`05797c1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/05797c1f32bce473cdc58472070753983f0d90b5)
- [`MW-1957`](https://jira.open-xchange.com/browse/MW-1957): referring to RFC5455-3.8.5.3, shift start/end date of recurrence master to the first occurrence [`1ef8fd9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1ef8fd9e9573168c9f6e6a98b276793da81688e3)
- Don't build log message if log level does not fit #2 [`35ba26f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/35ba26fbd0f3225362745fbeb4abd7c28ca6bdc2)
- [`MWB-1970`](https://jira.open-xchange.com/browse/MWB-1970): Use active database connection when loading enhanced entity data for events [`5e20d9b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5e20d9b0886bc7bd97ecf9c0068a5d67c3079dd4)
- [`MWB-1970`](https://jira.open-xchange.com/browse/MWB-1970): Don't advertise 'count' capability for database-backed folders [`cdc6973`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cdc6973049c95b934e1de46acf2ee07715bdca25)
- [`MWB-1970`](https://jira.open-xchange.com/browse/MWB-1970): Maintain cached list of file storage account identifiers per service [`9d8a301`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9d8a301cae43633dbb3d8bf4398e71945d773b9e)
- [`MWB-1970`](https://jira.open-xchange.com/browse/MWB-1970): Use active database connection when loading enhanced entity data for events (2) [`7efa8fc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7efa8fcbd45210f9ebb3370d524d6c41834ecba3)
- Added special HTTP protocol exception signaling that a certain URI is denied being accessed [`0200041`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0200041fde8759db71e563c6e8fb24f0ac103d11)
- Enrich calendar results with contact details for internal organizers if requested via 'extendedEntities=true' [`e5950b7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e5950b7fbe403d2cdcce9bad49d1550c21f8260a)
- [`MW-1830`](https://jira.open-xchange.com/browse/MW-1830): Generation of mandatory Secret Values through Helm Chart [`9dbb102`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9dbb102154e25435cd559cca8bc8d39c4945e151)
- Indicate 'optional' participants in notification mails [`e1b31f0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e1b31f0362abbc1ec446fd692ca45718ae766715)
- Fixed logging & some thread visibility issues [`8fa7246`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8fa72465ce52d77e2c7b9ed975def31cabe8d45d)
- [`MWB-1991`](https://jira.open-xchange.com/browse/MWB-1991): upgraded micrometer from 1.5.1 to 1.10.3 [`63d112c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/63d112cf2592c86bfc325c404f1ae8a2d9eb15f5)
- [`MWB-2001`](https://jira.open-xchange.com/browse/MWB-2001): Added logging for periodic attachment storage
cleaner [`55cc090`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/55cc090cf3862b43dcb85b35ad44b37ce874045c)
- Use thread-safe classes [`b606631`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b606631ee4f4cffdf259373df5f39c27d8c15bf6)
- [`MW-1985`](https://jira.open-xchange.com/browse/MW-1985): Improve DB warning/error logs [`9945242`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9945242af13d12a924ed221e5710d58eca1918c3)
- Removed unused Apache POI library from JavaMail bundle [`f42b86d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f42b86d859c6fd40b90f8dc507ed6b8c58d4e6d2)
- Fixed some issues announced by Eclipse IDE [`e1b054b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e1b054b92db0e2b083300fed371e8119249e0c9e)
- Improved logged error message [`9417579`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/94175796789a097eb274282acf0298421a583b67)
- Removed remnants [`cb9b85d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cb9b85dffa90ea48116680a02bb1e3edfa1d4b39)
- Resolved warnings [`9778c66`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9778c667fbce2fdd83a305b7e7d978dd20dbd07e) [`ba04ee4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ba04ee47ad27c11853b1750c5a2b877f1f03fe1c) [`9fea797`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9fea7976d03045de9bcbadde801c700033a0303c) [`5781986`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/578198607e8027a9f951ba2471e21e8af8c1ca7c) [`2dbdc9d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2dbdc9d3cb943b61fc36a2609f94fcaf7dc0ea0e) [`06e0f60`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/06e0f60defedc5e3881434c2733472ca98510d89) [`2f2a31f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2f2a31f31b92e147c9fc7de98860ae6b054d9b86) [`5e6de37`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5e6de3707bf4fede9f025c9e56d9d4899623704f) [`d206ac0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d206ac099e275a799a406d7bc86c4477f57b3e36) [`cf2ad17`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cf2ad170d66f4d65356208d0e5a8315b89f4832b) [`e48753a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e48753ac81934ce4071d6bc7c1414f272204b18d)
- Don't build log message if log level does not fit #3 [`b55c826`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b55c826d20437c21acf1ef65d60e43dabb24aee7)

### Removed

- [`MW-1946`](https://jira.open-xchange.com/browse/MW-1946) - removed org.apache.tika (and com.openexchange.textxtraction). The required functionality is now provided through the new bundle com.openexchange.tika.util [`f7076fa`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f7076fa15c23ab62755d24b828abc089547f07bb)
- [`MW-1930`](https://jira.open-xchange.com/browse/MW-1930): Removed direct links from notification mail [`a2e29a9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a2e29a9bc964c065ac90595f5b0e3c097a2aa445)
- Removed obsolete test [`3733b38`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3733b3830a46d7b989743034ff7ffbaf644fe7f9)

### Fixed

- [`MWB-1983`](https://jira.open-xchange.com/browse/MWB-1983): Limit line length and header count when fetching HTTP headers of an HTTP message + Replaced usage of `java.net.HttpURLConnection` with Apache HttpClient where necessary [`1d12911`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1d129118a4acce147aa72ba75af13cf37bd5a58a)
- [`MWB-2026`](https://jira.open-xchange.com/browse/MWB-2026): Try to handle possible connection loss errors during mail export operation [`6ff82b6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6ff82b66df59f91664bf9fcec7414778552cd273)
- [`MW-1840`](https://jira.open-xchange.com/browse/MW-1840)-8x-patch: Encrypt with old engine, try decrypt with new if possible [`0f8a3f3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0f8a3f37175366a025754ccc02982edc7f8d34d2)
- [`MWB-1999`](https://jira.open-xchange.com/browse/MWB-1999): impp type other than work or home is set properly [`e3f0d3c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e3f0d3cd7fab533b83e31e9a189f137bf9d8d145)
- [`MWB-2023`](https://jira.open-xchange.com/browse/MWB-2023): Fixes to pre-update job for installations with multiple complex roles [`c0bf897`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c0bf897ecca0e234ce7aadeebab2b6c003d30b22)
- [`MWB-2021`](https://jira.open-xchange.com/browse/MWB-2021): Return proper value for "com.openexchange.subscribe.subscriptionFlag" on folder retrieval [`0d186b1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0d186b15510f28acfccdff5291593ec0a5903c7b)
- [`MWB-2027`](https://jira.open-xchange.com/browse/MWB-2027): Specify missing error message argument on SQL error [`beb2904`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/beb29041734407d26ebe55674ddab733e37a40d8)
- [`OXUIB-2162`](https://jira.open-xchange.com/browse/OXUIB-2162): wrong translation for calendar change [`23ff72e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/23ff72e587dc2d4d5bfbfa9e7451e91ac4e1ac4b)
- [`MWB-1997`](https://jira.open-xchange.com/browse/MWB-1997): API access not fully restricted when requiring 2FA [`bd67a4e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bd67a4e8d03c9507323eb747775fdf27d175deec)
- [`MWB-1983`](https://jira.open-xchange.com/browse/MWB-1983): Limit line length and header count when fetching HTTP headers of an HTTP message + Replaced usage of `java.net.HttpURLConnection` with Apache HttpClient where necessary #2 [`c0e345b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c0e345b890d2a186318e5e5b37f852ba1f60b552)
- [`MWB-2005`](https://jira.open-xchange.com/browse/MWB-2005): Fixed retrieving RSS feed [`fc07069`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fc070697abe9fad0e31c428d0134d3a7b996b4ef)
- [`MWB-2028`](https://jira.open-xchange.com/browse/MWB-2028): Fixed look-up of attachments in case IMAP message has TNEF content [`5934db4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5934db4d9a8915c0b889d4cd5d6e18c8de71f9ba)
- [`MWB-2008`](https://jira.open-xchange.com/browse/MWB-2008): Don't allow to access snippets/signatures from other users if not shared [`00957b4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/00957b43ea8a21b5c8242dd1a926da7ff7566947)
- [`MWB-1991`](https://jira.open-xchange.com/browse/MWB-1991): properly remove metrics in case pool is destroyed [`38286d9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/38286d97e08e9f1b45cf0c94dfc5e9bd5dca302e)
- [`MWB-2020`](https://jira.open-xchange.com/browse/MWB-2020): added sanitizing to filter rules + improved the sanitizing regex [`21ca22e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/21ca22e22cfac06f11a51a8f3bfbe4a38506407d)
- [`MWB-1981`](https://jira.open-xchange.com/browse/MWB-1981): properly check returned ical size [`5bea149`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5bea149a40e7cceb620c4903e48f5846315e5b88)
- [`MWB-2025`](https://jira.open-xchange.com/browse/MWB-2025): Fixed avoidable exception on DEBUG logging [`cf950d6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cf950d6098add3fb4135bafea6fe0072ab2f3957)
- [`MWB-1939`](https://jira.open-xchange.com/browse/MWB-1939): Print exposure time as fraction if possible [`8de8cb3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8de8cb38c16015b9b790b6a25a4abcf6c188d7d5)
- [`MWB-2006`](https://jira.open-xchange.com/browse/MWB-2006): use owc only on feature branches [`65b1aa9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/65b1aa99719307781fb9a2a2fcd7646b0a769f98)
- [`MWB-2007`](https://jira.open-xchange.com/browse/MWB-2007): Only set "domain" parameter when dropping a cookie if value is considered as valid: Not "localhost". Not an IPv4 identifier. Not an IPv6 identifier [`22f9029`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/22f902917a18b095d24efb5eecfa3534cd6f4ee2)
- [`MWB-1928`](https://jira.open-xchange.com/browse/MWB-1928): Only check usage (space capacity) of destination storage when moving from user-associated file storage to context-associated one since no entity assignment takes place #2 [`f76537b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f76537b904b66e3620697607f27686b51f098d65)
- [`MWB-2036`](https://jira.open-xchange.com/browse/MWB-2036): Do escape column names when building database
statements for context move [`89c9a1f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/89c9a1f16999773519b360d728d2debad1e3d0dd)
- [`MWB-1991`](https://jira.open-xchange.com/browse/MWB-1991): adjusted 3rdPartyLibs.properties [`0fa654a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0fa654a7d24feabf344f215be06256b148e9ca5b)
- [`MWB-2021`](https://jira.open-xchange.com/browse/MWB-2021): Return proper value for "com.openexchange.subscribe.subscriptionFlag" on folder retrieval (2) [`a1775e7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a1775e76daca90436c480c7c8f194fc9c5776a56)
- [`MWB-2000`](https://jira.open-xchange.com/browse/MWB-2000): Only query fields necessary to construct contact image URI [`10856cc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/10856cc074065bd6b18de15d42bd1da32aceea78)
- [`MWB-2010`](https://jira.open-xchange.com/browse/MWB-2010): Set correct compression level for data exports [`fb07ee6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fb07ee6833956966d2ab435ae2a1a08ba84a399c)
- Fixed importing and exporting the same package [`db5cd45`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/db5cd45d6e2c2853992e73ba9db83e0b8a47523f)
- [`MWB-2000`](https://jira.open-xchange.com/browse/MWB-2000): Only query fields necessary to construct contact image URI (2) [`96bfe2d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/96bfe2df2540201d234ee608eaf47244026e8d1a)


## [8.9] - 2023-01-10

### Added

- [`SCR-1174`](https://jira.open-xchange.com/browse/SCR-1174): New Property
'com.openexchange.resource.simplePermissionMode' [`d48c9fc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d48c9fccdfd84cde70424003f07cdf3b587e8798)

### Changed

- Refactored context restore for better readability and maintenance [`197a237`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/197a237996885c9430ac79d4f011a4afdee8cb60)
- Change for [`MWB-1962`](https://jira.open-xchange.com/browse/MWB-1962): Upgraded Hazelcast from v5.1.2 to v5.2.1 [`bfe140b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bfe140b4ff243e86c9020f7eff2fbae40d277d8f)
- **IMAP**: Check via ID command if IMAP server appears to be a Dovecot server [`f639fa4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f639fa410e29a7d6a8a431f48c24146c9438b110)
- Avoid unnecessary creation of byte array when outputting thumbnail content to client [`6777845`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/67778458e4db8b8bbf9d1200b3554d7067b76e4f)
- Avoid unnecessary SELECT statement and use "INSERT ... ON DUPLICATE KEY UPDATE" instead [`1b47613`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1b47613e69e72a3293ec0182a53c214a4cd8a63a) [`a4f414d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a4f414d8677a6f21e2592450a861e40905a4148c)
- Direct initialisation of "AttributeChangers" instances [`6c4bf47`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6c4bf47e6033243f1141bd9d8b154cbda82895cc)
- Use singleton w/ dedicated initialisation/dropping [`48accd9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/48accd96bada48b944492568b1a59fecf26807e6)
- Thread-safe collection [`48d858c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/48d858ceeeec8433aa23b01a976f94b56cb5d974)
- Use proper URL for HttpContext when trying 2nd time [`2984c65`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2984c6562b907bf821be359f43f4807f9fb81861)
- Use singleton w/ dedicated initialisation/dropping #2 [`edeff71`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/edeff71e08b5802839218971db89c5b92d7b870e)
- Removed unnecessary variable [`749e77b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/749e77bff65f414c92e904b6dacccb66ec461495)
- bump helm chart version
  * This is for the new configurable helm chart deployment type [`0cf0eb3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0cf0eb31a52bbbb08f4656e00fb2880fe4a5ea5c)
- Cache as immutable set [`0033fd3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0033fd3a2ae40f043e28524f95643f16f453b3f8)

### Removed

- removed unnecessary join (to be compatible with guest users) [`d46976c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d46976c88777626bb7d10764bb8eba3a13a36f75)

### Fixed

- fixed some variables in the translation [`26065e5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/26065e51dda07b2df00c70f802279821be5037fd)
- [`MWB-1947`](https://jira.open-xchange.com/browse/MWB-1947):
  * Introduced map for storing/managing state during authentication flow
  * Added property `com.openexchange.oidc.mail.immediateTokenRefreshOnFailedAuth` to enable/disable immediate refresh of OIDC OAuth tokens on failed authentication against mail/transport service
  * Implemented immediate refresh of OIDC OAuth tokens in case of failed authentication against mail/transport service [`276670e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/276670ea8f46c2ae9e50e0f36259afb9df61c117)
- [`MWB-1966`](https://jira.open-xchange.com/browse/MWB-1966): Use proper error code to advertise resource exceptions
to client [`0e2e389`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0e2e389ff21e8e55bd5ca78f16422f8508f1256e)
- [`MWB-1995`](https://jira.open-xchange.com/browse/MWB-1995): Check if distribution list members are accessible prior
to adding them #2 [`8beba6a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8beba6a8f7f421db1be2137815027a8fee337133)
- [`MWB-1963`](https://jira.open-xchange.com/browse/MWB-1963): More reasonable default value of 2GB (2147483648 bytes) for `com.openexchange.servlet.maxBodySize` property, which now effectively limits file uploads (no chunked HTTP upload anymore due to omission of Apache Web Server that is replaced by Istio). Moreover, introduced new property "com.openexchange.servlet.maxFormPostSize" with default value of 2MB (2097152 bytes) to have a dedicated property to control max. size for form data sent via POST. [`bd6fe39`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bd6fe39db5eff0029eeb329cf807cbdf2028276f)
- [`MWB-1972`](https://jira.open-xchange.com/browse/MWB-1972): Correctly indicate resource type in principal resources [`1ef0a13`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1ef0a13658c230a820831e045dc768c0b447da28)
- [`MWB-1995`](https://jira.open-xchange.com/browse/MWB-1995): Check if distribution list members are accessible prior
to adding them [`153909b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/153909b2ce9a799a00657c6cb171d884ec94d09a)
- [`MWB-1936`](https://jira.open-xchange.com/browse/MWB-1936): Revisited transport checks [`8542d55`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8542d5529cbe92dda547dfdfa15cfdb035df7fd2)
- [`MW-1989`](https://jira.open-xchange.com/browse/MW-1989): Don't let delete operation fail upon malformed change
exception data while tracking changes [`3d47d7e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3d47d7e0d36766f8185d4212c45a9823bbf3c2da)
- [`MWB-1985`](https://jira.open-xchange.com/browse/MWB-1985): properly handle public tasks folder in case no-reassign
is set [`036afcc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/036afcc75cd2152917bb0d3e62a4f5aca068e117)
- [`MWB-1984`](https://jira.open-xchange.com/browse/MWB-1984): Prefer address from EMAIL parameter when deciding if
iMIP mails from iCloud are considered as 'known' sender [`543dbcc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/543dbccb07efc206d6883c913385062313c3371b)
- Change for [`DOV-4625`](https://jira.open-xchange.com/browse/DOV-4625): Detect missing space character in case of corrupt NIL value for PREVIEW fetch item; e.g. "PREVIEW NILUID 1" [`d2ca600`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d2ca600f0e35731f2df51a51b478533cb343b436)
- [`MWB-1956`](https://jira.open-xchange.com/browse/MWB-1956): Apple Mail flag taken over even though Open-Xchange color flag has been explicitly set to NONE [`9f18684`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9f186842a0b4cb1f3fb50522f336aa9fdb3029e8)
- [`MWB-1964`](https://jira.open-xchange.com/browse/MWB-1964): Let guest inherit sharing user's filestore if
applicable [`e82657b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e82657b5b948b31fb6ced7d7a8eb47360065a903)
- [`MWB-1961`](https://jira.open-xchange.com/browse/MWB-1961): throw proper error in case user is missing [`d682bf8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d682bf86e32fcbc67d7f93e1973ae40cec36a3a3)
- [`MWB-1934`](https://jira.open-xchange.com/browse/MWB-1934): Don't allow empty "From" address on mail transport [`e64de8a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e64de8afb0dd597bf25a1090bbc57e03659bd2ec)
- [`MWB-1820`](https://jira.open-xchange.com/browse/MWB-1820): only removes guests in case of real failures [`110596f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/110596f9d49a40e1a92025bb4ee4a359fc301bcf)
- [`MWB-1971`](https://jira.open-xchange.com/browse/MWB-1971): improved matching of distribution list members [`1218c53`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1218c53fb0b8d1304ca16be9e0134cf22969ff5b)
- [`MWB-1851`](https://jira.open-xchange.com/browse/MWB-1851): Return proper folder identifier when saving draft to POP3 account [`05e59fc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/05e59fc84b36edff56a7fd12679e63a665b0a6f2)
- [`MWB-1951`](https://jira.open-xchange.com/browse/MWB-1951): Use unicode address to resolve mail recipient [`7fb1c8c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7fb1c8cc0361a06c14bc53c83883039c860bead8)
- [`MWB-1986`](https://jira.open-xchange.com/browse/MWB-1986): Fixed SQL error in SELECT statement (Mixing of GROUP columns (MIN(),MAX(),COUNT(),...) with no GROUP columns is illegal if there is no GROUP BY clause) [`91105d0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/91105d0d5b66c355d97c0f7c4feab279501ec420)
- [`MWB-1978`](https://jira.open-xchange.com/browse/MWB-1978): Prevent changes of object id when generating delta
event [`7de23e6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7de23e6ec3b9c8a4a255b6a06d2b02ff836d4bef)

## [8.8] - 2022-12-14

### Added

- [`MW-1857`](https://jira.open-xchange.com/browse/MW-1857): Option to disable SMTP for 3rd party Mail Accounts [`a6d5a0b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a6d5a0b028af89ffe202bebc801376e49fba06dd)
  * Added a new middleware property `com.openexchange.mail.smtp.allowExternal` which defaults to true
  * Utilise that property to filter the transport details in the mail account POJOs
  * Introduced a new read-only JSLob entry under `io.ox/mail//features/allowExternalSMTP` which reflects the middleware's property
  * Forbid sending mail from an external SMTP server as long as the setting is set to false
  * Forbid creating/updating mail accounts with transport information as long as the setting is set to false
  * Added a new warning for preflight/validity checks which reflect this 
- [`MW-1831`](https://jira.open-xchange.com/browse/MW-1831): Push configuration for macOS drive client [`d2a9903`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d2a9903a2e4e327383c767f5d42ac6111091deac)
  * [`SCR-1157`](https://jira.open-xchange.com/browse/SCR-1157): Introduced properties for macOS client push notification configuration
- [`SCR-1165`](https://jira.open-xchange.com/browse/SCR-1165): Added options to specify socket read timeout when applying filter to existent messages [`53f3023`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/53f3023894c0e5fe0586cc9f28e7bbdf9220d8d7)
- [`MW-1938`](https://jira.open-xchange.com/browse/MW-1938): New Templates and Examples section for documentation and adapted jenkins workflow to dynamically point to the correct version of the files [`11bbcbc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/11bbcbcde5666689a583729b1437642584e475bd)

### Changed

- MAL: Enhanced [`MSG-1016`](https://jira.open-xchange.com/browse/MSG-1016) error code by actual connect timeout value [`e194eb1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e194eb18b135072084ffcde5bc2f61008aca5656)
- Mail Auto-Config: Let auto-config attempt fail immediately in case login attempt encounters failed authentication due to wrong credentials/authentication data [`f1fea90`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f1fea90a31b3f5ffb0916812e08937b2ebbdb702) [`45b68d0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/45b68d01df8954efa69a1c3b97a77667b7a3079f)
- [`MWB-1943`](https://jira.open-xchange.com/browse/MWB-1943): Apply consistent configuration to mail auto-config as used when connecting to the account during runtime [`1d682ef`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1d682efe932e8e98efd53dfdadf9a483757d5bf7)
- Don't build log message if log level does not fit [`4b55202`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4b552021198869dd2a0700978e6c3395e938945b)
- [`MW-1941`](https://jira.open-xchange.com/browse/MW-1941): Updated and re-structured documentation [`373dce4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/373dce4b3179fc752c4ebfaeeb788a495cbc9d63)
- [`OXUIB-2066`](https://jira.open-xchange.com/browse/OXUIB-2066): Propagate configured mail fetch limit via JSlob under "io.ox/mail//mailfetchlimit" [`895d606`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/895d60671419534a132ffbed75a5c03b3eda8a1b)
- Database: Utility method to re-execute DB operation on transaction roll-back error [`bb47eab`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bb47eab31c667018c4abee35db7f7d3b6da20e5f)
- [`MW-1904`](https://jira.open-xchange.com/browse/MW-1904): Adjust for Reserved Words in MariaDB 10.6 [`d713340`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d713340a42cfa810b7134d4564b69c4d0b5c7c22)
  * Using back-ticks in SQL statements to handle new reserved words in MariaDB 10.6
  * Only the keyword `OFFSET` had to be adjusted in SQL statements
- Don't build log message if log level does not fit #2 [`37dd1ad`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/37dd1ad3fc89f950b4fa0b89a81b0f391f051008)
- JavaMail: Optimized creation of FetchResponse instances through remembering if RFC8970 "PREVIEW" capability is advertised by IMAP server [`cb17cd5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cb17cd55fd7f640a3f70a210a60db8a81dbee452)
- MAL: Enhanced "[`MSG-1016`](https://jira.open-xchange.com/browse/MSG-1016)" error code by actual connect timeout value #2 [`c108082`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c108082c9159996e8429785a64be1b2a10137baf)
- [`MWB-1909`](https://jira.open-xchange.com/browse/MWB-1909): Extended information in case an error occurs [`470911d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/470911d4a4750f8f47234d5831659e796cb85069)

### Fixed

- [`MWB-1902`](https://jira.open-xchange.com/browse/MWB-1902): Use localized display name for groups towards clients [`27f0a50`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/27f0a509f008c5b0f06aa892f4b32582f405c413)
- [`MWB-1857`](https://jira.open-xchange.com/browse/MWB-1857): Incomplete response when requesting /infostore?action=list [`0d4ddce`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0d4ddce361658d34c1a9cf79dff7892210241206)
- Change for [`OXUIB-2067`](https://jira.open-xchange.com/browse/OXUIB-2067): Avoid alternative MIME part look-up by Content-Id in case no such part is contained in IMAP message's BODYSTRUCTURE information [`49f3b9e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/49f3b9e0b1adb3b81be7a52032e88cb8242b5942)
- [`MWB-1944`](https://jira.open-xchange.com/browse/MWB-1944): Don't cache user-sensitive non-file-backed properties [`e7d0385`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e7d038583e07e0be6ccbea460b898c1dc3de2864)
- [`MWB-1904`](https://jira.open-xchange.com/browse/MWB-1904): Properly indicate 'DAV:need-privilege' precondition
with HTTP 403 for PUT requests w/o sufficient privileges [`65e64e6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/65e64e6975f18a046ec315e8cd189894e3106e49)
- [`MWB-1940`](https://jira.open-xchange.com/browse/MWB-1940): Only inject a valid image URI into mail body's HTML part if such an inline image seems to exist in parental mail [`d70ce12`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d70ce12d9fb3eb6f11c5fb04a0e76214ba328bdf)
- [`MWB-1887`](https://jira.open-xchange.com/browse/MWB-1887): Delete folders chunk-wise to avoid excessively big database transaction [`244847d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/244847d700a13dc46933a33e65d6db6779b323fd)
- [`MWB-1901`](https://jira.open-xchange.com/browse/MWB-1901): Disable usage of XCLIENT SMTP extension by default [`4452098`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/44520987db7a93cf722d63cbd69b4135915e067c)
- [`MWB-1948`](https://jira.open-xchange.com/browse/MWB-1948): Perform alternative SASL long against SMTP server if initial response exceeds max. line length of 998 [`90b9477`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/90b9477f379dd1758852505b7c47a0e9d59e4471)
- [`MWB-1899`](https://jira.open-xchange.com/browse/MWB-1899): Accept escaped wild-card characters in search pattern [`141e691`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/141e691296de9387b6d33315c8aa22d26b89ff80)
- [`MWB-1912`](https://jira.open-xchange.com/browse/MWB-1912): aligned checks with documentation [`8de34a9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8de34a9fd72dfbc5855b148076cf48183b373705)
- [`USM-36`](https://jira.open-xchange.com/browse/USM-36): Re-introduce CUD actions [`e83189b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e83189bd1a33199103ad4c44b0264049389554a8)
- [`MWB-1928`](https://jira.open-xchange.com/browse/MWB-1928): Only check usage (space capacity) of destination storage when moving from user-associated file storage to context-associated one since no entity assignment takes place [`06f177b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/06f177b1701c096a34e555644cba7050df1e5202)
- [`MWB-1909`](https://jira.open-xchange.com/browse/MWB-1909): Handle possible NULL result value when querying counts [`a64eb82`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a64eb82add1b2cc2db107c6ec8ee8a5c040b1e31)
- [`MWB-1950`](https://jira.open-xchange.com/browse/MWB-1950): Do not check the user while resolving mail recipients in recipientOnly modus [`263a2b5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/263a2b561b0a855e53394b1c8a8a210f24be91af)
- [`MWB-1929`](https://jira.open-xchange.com/browse/MWB-1929): Remove sessions from remote nodes during backchannel
logout synchronously [`82d4253`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/82d42539b8789d9f93caf0d3d72c5964e98564e6)
- Fix connection leak in test clients [`a415e8e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a415e8e40d53b2797be28e312c6556b5a9558bf3)
- [`MWB-1931`](https://jira.open-xchange.com/browse/MWB-1931): Don't allow empty passwords [`d506a00`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d506a009efa4d750527c7794b6eb1eb5fc6b2753)
- [`MWB-1944`](https://jira.open-xchange.com/browse/MWB-1944): Don't cache user-sensitive non-file-backed properties [`eb74ebf`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/eb74ebfc8abf34aed63b7a2803b00b3bc423c089)
- [`MWB-1887`](https://jira.open-xchange.com/browse/MWB-1887): Don't forget to finish Infostore instance [`f1d4fc4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f1d4fc47cb0c01feb530f7ec7895c6a093c9e300)
- [`MWB-1923`](https://jira.open-xchange.com/browse/MWB-1923): Avoid premature closing of attachments [`a9a5174`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a9a5174b4d7eccefbc57ac2a2ef7027579dafa68)
- Use proper fall-back for "com.openexchange.imap.folderCacheTimeoutMillis" setting [`87d9b67`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/87d9b676fa0bc791d6f081846c962b7589219cc2)
- [`MWB-1941`](https://jira.open-xchange.com/browse/MWB-1941): Deleteuser fails with invalid CU [`035a397`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/035a397b98940c0f8ded26612eac68f0963ac5a8)
- [`MWB-1949`](https://jira.open-xchange.com/browse/MWB-1949): fixed wrong option within the documentation of the command line tool [`357d263`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/357d26344a5edbeb6bc0e7c99cb5563f4cf17c42)
- [`GUARD-391`](https://jira.open-xchange.com/browse/GUARD-391): Split lines only on newline during normalization [`8873cfd`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8873cfdc890c53755c3a1a291024f76c8d9ba5e5)

### Security

- [`OXUIB-2034`](https://jira.open-xchange.com/browse/OXUIB-2034): Deny setting certain jslob core subtrees [`a603fa8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a603fa8f9a12e4dbe8d5a85cd2f82ad57430822e) [`929b9a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/929b9a657866a180627b60d0107b764a7234e266)
  * See also [`MWB-1784`](https://jira.open-xchange.com/browse/MWB-1784)

## [8.7.0-8.7.19] - 2022-11-11

### Added

- [`MW-1877`](https://jira.open-xchange.com/browse/MW-1877): Permissions for Resources
  * Introduced resource scheduling privileges 'ask_to_book', 'book_directly' and 'delegate'
  * By default, group 0 has 'book_directly' privileges for each resource("unmanaged mode"), unless defined differently ("managed mode")
  * Extended resource model by a corresponding permissions array, storing privileges per entity
  * HTTP API is adjusted accordingly  ([`SCR-1154`](https://jira.open-xchange.com/browse/SCR-1154))
  * New database table resource_permissions to store resource privileges of users/groups ([`SCR-1153`](https://jira.open-xchange.com/browse/SCR-1153)) [`4de788f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4de788f37cf9e58343019570abd359efb46050f0)
- [`MWB-1871`](https://jira.open-xchange.com/browse/MWB-1871): added possibility to parse images of nested messages
  * Added new lean property com.openexchange.mail.handler.image.parseNested with defaults to true [`b42dfec`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b42dfecd46380c8a9cf4d8a25443c867918adae3)
- [`MW-1903`](https://jira.open-xchange.com/browse/MW-1903): introduced CORE_TEST param to Jenkinsfile [`6a4a0ba`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6a4a0ba8b72d04adbb1d74488754e04894835af1)
- [`MW-1507`](https://jira.open-xchange.com/browse/MW-1507): Calendars for Resources
  * Introduced virtual folder identifiers for resource calendars ([`SCR-1149`](https://jira.open-xchange.com/browse/SCR-1149))
  * Folder ids can be used in typical "chronos?action=all" requests to get the contained events, actions "advancedSearch", "get" and "list" are supported as well
  * Events returned under the perspective of a virtual resource folder will also have this virtual identifier assigned within the folder field
  * The requesting user will either get all details of an event in a resource folder, or only an anonymized version - depending on whether the event is visible for the user in another folder view or not. [`6fbc61a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6fbc61a3b999d146bd6ecb75825aee392cfce527)
- [`MW-1792`](https://jira.open-xchange.com/browse/MW-1792): Allow changing of "includeSubfolders" flag through link permission entity [`e326340`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e3263404b8b2659143dfebc546eeb131ee3cbb82)

### Changed

- Minor changes for mail auto-config [`8221066`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/822106647db674e48dd6fbc3515448eacadfa235)
- [`MWB-1901`](https://jira.open-xchange.com/browse/MWB-1901): Do not issue XCLIENT command if no XCLIENT parameter is supported [`c915650`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c9156501df1c64e328bd82fc08f4be708857d778)
- [`MWB-666`](https://jira.open-xchange.com/browse/MWB-666): Send "431 - Request Header Fields Too Large" HTTP error response instead of "400 - Bad Request" when HTTP packet header is too large [`a7cc43c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a7cc43c6a3b3ea9ea8aed22970465c8d70c34ed0)
- JavaMail: Check appropriate capability "SEARCH=X-MIMEPART" prior to performing a file name search [`3cc2ce8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3cc2ce80a2cccb387d3b9ed65d06d384c62647cf)
- [`OXUIB-2025`](https://jira.open-xchange.com/browse/OXUIB-2025): Added support for TEXT search term to filter messages that contain a specified string in the header or body of the message [`f775905`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/f77590578c66e1bb6965d9b207ae8f9a3d600260)
- [`OXUIB-2025`](https://jira.open-xchange.com/browse/OXUIB-2025): Added support for TEXT search term to filter messages that contain a specified string in the header or body of the message #2 [`910eb69`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/910eb692d311c2912970642ce73e7f5e1cbadf74)
- [`MW-1915`](https://jira.open-xchange.com/browse/MW-1915): Migrated helm lint/publish and docu build/publish to jenkins [`391bc2b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/391bc2bc2d50a4c127792da2402fb776c68deaf0)
- [`MW-1813`](https://jira.open-xchange.com/browse/MW-1813): New approach for centralized version information [`cf6d801`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cf6d80108b45caad7320190cfb1565d1ee41ee53)
- [`MWB-1826`](https://jira.open-xchange.com/browse/MWB-1826): Added some logging [`49c0b33`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/49c0b33375e08c18ef73b7a159dd1b630077bb81)
- [`MWB-1891`](https://jira.open-xchange.com/browse/MWB-1891): Don't validate distribution list member's mail address during user copy [`e3c0f22`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e3c0f2282b16dddebd7c74ecb3dfa6d828f551fe)
- [`MW-1914`](https://jira.open-xchange.com/browse/MW-1914): Extend Webhook integration for Jitsi Conferences
  * Renamed Switchboard Packages and Bundles ([`SCR-1151`](https://jira.open-xchange.com/browse/SCR-1151))
  * Adjusted Switchboard Configuration ([`SCR-1152`](https://jira.open-xchange.com/browse/SCR-1152))
  * Implemented new interceptor for conferences of type "jitsi"
  * Transformed switchboard calendar handler into a handler for a generic webhook target [`0593a47`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0593a47cff6496008ed9d831184622caa4120a78)
- [`INF-30`](https://jira.open-xchange.com/browse/INF-30): Use globally configured appRoot [`16853d6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/16853d600e3a3b3df274b58ea9b6b834b5b186d6)

### Removed

- Removed c.o.dav.push leftovers [`4369c69`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4369c69f5950223b93ba0e341c0afbe9afee3bfa)
- Removed c.o.mail.authenticity leftovers [`c753f59`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c753f597092c49242ca85349e1fbc1e417c95ce5)
- Removed c.o.oauth.linkedin leftovers [`638988b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/638988b333feb74be16ecad11288604ba8326e75)
- Removed c.o.halo.linkedin leftovers [`121f054`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/121f054665f2be5590b4c82023c54c9f5c206b4e)
- Removed c.o.subscribe.linkedin leftovers [`01d80d1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/01d80d159cbca0c77a936e965fb092dc7159dda2)
- Removed c.o.mail.authentication leftover [`2a846b0`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2a846b0361deb32f215b850ae3bf8f5cd33c1388)
- Removed no more required folder [`d57ee8c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d57ee8c5474497e9a18bf9951284d69347699c13)
- Removed no more required folder [`1a482ee`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1a482ee55ba80b13c08d0ff6a5d7a2cfd2d13ee7)
- Removed obsolete o-x-test-bundles [`dd513de`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/dd513deff4b9607843bace66b640ffae159c0db3)
- Removed c.o.printing leftovers [`a2f7b3e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a2f7b3e6ce0131a3930f7be4bcb746d94411c19d)
- Removed no more required folder [`5ee810f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5ee810f18a1f4ffff77e49630c638b1551ee0b8d)
- Removed redundant/obsolete folder implementations [`102032c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/102032cc6b326494adac2d248af553c98caa5320)

### Fixed

- [`MWB-1907`](https://jira.open-xchange.com/browse/MWB-1907): Restored previous SOAP behaviour by accepting individual parameters instead of a wrapping parameter object [`d1c2de4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d1c2de445da68893eb916faf00eebe6d2141d77c)
- [`MWB-1876`](https://jira.open-xchange.com/browse/MWB-1876): Check redirect location against blacklisted hosts when creating an iCal subscription. [`e219389`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e219389eda92cff087e1b17e243f8058d0591df1)
- [`MWB-1911`](https://jira.open-xchange.com/browse/MWB-1911): Do not require deputy service in case user replies to a message residing in a shared mail folder [`4377dff`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4377dffe52784e90db43f5e6ca6f573acee3c0e0)
- JavaMail: Add the ability to the API consumers to load the API implementations by using a different protection domain when the API is used with security manager enabled [`12f4647`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/12f46472a650ed4797a0b393659c02eba5011cd1)
- JavaMail: Implement equals() and hashcode() on jakarta.mail.Header (#597) [`8294cf2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8294cf2d307a4551c8f67e18664717d637bc4b4d)
- [`MWB-1908`](https://jira.open-xchange.com/browse/MWB-1908): Keep remembering OIDC -> OX session id mapping in state after auto-login [`c11a94d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c11a94de3d4cf081d5374679cce52ac11c2837d2)
- JavaMail: j.m.u.FactoryFinder.factoryFromServiceLoader needs PrivilegedAction #621 (#622) [`83d9c14`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/83d9c148692ecafe8ca5f4e79768f8eff950d936)
- [`MWB-1909`](https://jira.open-xchange.com/browse/MWB-1909): Adjusted queries issued by datamining tool to obey MySQL's ONLY_FULL_GROUP_BY mode [`a4e293e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a4e293ec9bac10c536161823bd768b1025957e82)
- JavaMail: Fix630 2 (#633) [`75b7136`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/75b71363826a9e0994d254b1c02600c3ceb29939)
- [`MWB-1893`](https://jira.open-xchange.com/browse/MWB-1893): Don't let delete operation fail upon malformed change exception data while tracking changes [`78615b9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/78615b93a48961f692a04bd019e0b1aea72f7371)
- [`MWB-1887`](https://jira.open-xchange.com/browse/MWB-1887): Fire events with a separate thread avoiding unnecessary occupation of deletion-performing main thread [`0cbd10c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0cbd10c2a993c129f54593c9b8005f8cb5912412)
- [`MWB-1887`](https://jira.open-xchange.com/browse/MWB-1887): Allow /folders?action=clear being performed as enqueuable operation [`cc226a7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cc226a763d2a7f61fbf961ecb1857ec03f99f6b5)
- [`MWB-1898`](https://jira.open-xchange.com/browse/MWB-1898): Added documentation examples for mapping context-/user-id properties to LDAP attributes properly [`3be7f84`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/3be7f84eb890564a882179a9ee66ca196dc955b4)
- [`MW-1813`](https://jira.open-xchange.com/browse/MW-1813): bug fixed by which the version was not resolved correctly [`aa0d040`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/aa0d040921afb83f0e156cbecdf1d0284ec4233c)
- [`MWB-1889`](https://jira.open-xchange.com/browse/MWB-1889): Drive mail with expiry date / with password can not be send [`7b462f4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7b462f4dfa3a0c1390cf0939c5194b0b44e04370)
- [`MWB-1892`](https://jira.open-xchange.com/browse/MWB-1892): Don't filter "com.openexchange.grizzly.serverName" property from log event [`4d342b8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4d342b833ee278c9ab659cb4bf3c9350c28c3825)
- [`MWB-1878`](https://jira.open-xchange.com/browse/MWB-1878): Handle empty Disposition-Notification-To header on delete [`cf06c47`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/cf06c478bdbf5c38081e533ede3f87ae0e8a793f)
- [`MWB-1882`](https://jira.open-xchange.com/browse/MWB-1882): Upgraded Apache Commons Text from v1.9 to v1.10.0 [`7a911be`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7a911be6102d4e3ced03557ec93b8340b1092d4d)
- [`MWB-1890`](https://jira.open-xchange.com/browse/MWB-1890): Do obey folder types restriction when constructing search term for looking up events of user [`87ec00e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/87ec00edd7a2b2a9289edc464155b3d4d7692c7e)
- [`MWB-1874`](https://jira.open-xchange.com/browse/MWB-1874): Remove references to contact in distribution list member when contact's email is cleared [`db7ef9e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/db7ef9e2d6c3f202dc824c55de73db5dd66cb5fc)
- [`MWB-1695`](https://jira.open-xchange.com/browse/MWB-1695): Introduced "requiredCapabilities" for App-specific Password Applications [`SCR-1155`](https://jira.open-xchange.com/browse/SCR-1155) [`ec439e9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/ec439e91b2d5dad46a63f1d375553fa0c0ee357b)
- [`MWB-1865`](https://jira.open-xchange.com/browse/MWB-1865): Use internal resources for image build #2 [`320b808`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/320b80873839d63ed75692a936d99378034d1b7d)
- [`MWB-1834`](https://jira.open-xchange.com/browse/MWB-1834): Check command line options before accessing the reseller service [`e94ab2a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e94ab2afd9130dbfb825ec85fbdca681250f3660)
- [`MWB-1865`](https://jira.open-xchange.com/browse/MWB-1865): Use internal resources for image build [`a48433d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a48433dbbe1726f65a78de087e7e2c8a49ef89bd)
- use proper fallback property for exclude file pattern [`0eadd7d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0eadd7dc71e6a6673364f56f416971a54aae19b9)
- [`MWB-1866`](https://jira.open-xchange.com/browse/MWB-1866): Orderly consider public folder mode when userizing event data in result tracker [`15274d9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/15274d9f281cc44f0a29e21e1716bfd35773f35a)
- [`MWB-1719`](https://jira.open-xchange.com/browse/MWB-1719): Don't forget to reassign returned Stream instance when applying filter [`a76a018`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a76a01818d6f80796d217cbe243baf29b132c1e0)
- [`MWB-1870`](https://jira.open-xchange.com/browse/MWB-1870): Multifactor Webauthn provider throws UnsupportedOperationException [`8c8a2b7`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8c8a2b712389c33b023925adba8442a28457a3f4)

## [8.5.0-8.6.3] - 2022-10-05

### Added

- [`MW-1785`](https://jira.open-xchange.com/browse/MW-1785): Introduce pre-upgrade task framework [`6396946`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/6396946cdb1062f1db5334409ec914f718ad2627)
- [`MW-1815`](https://jira.open-xchange.com/browse/MW-1815): Attach files from drive to chronos events [`fabeec5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fabeec51e0182c16bea491833c0c8740187b26b0)
- [`MW-1647`](https://jira.open-xchange.com/browse/MW-1647): Handle linked attachments for appointments [`fc5477c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fc5477c68bf3d0f1d14a5555f7e1934d094890d0)  
  * Externally hosted attachments can now be stored for appointments, with an URI pointing to the data  
  * Introduced new field `uri` for `AttachmentData` object (HTTP API), with column id `891`  
  * Added new field `uri` for `c.o.groupware.attach.AttachmentMetadata` DTO as well  
  * Adjusted interface `c.o.chronos.storage.AttachmentStorage` and implementation to reference non-managed attachments properly during deletions  
  * *Breaking Change* Update task `com.openexchange.groupware.update.tasks.AttachmentAddUriColumnTask` to add column `uri` in table `prg_attachment`  
- [`MW-1817`](https://jira.open-xchange.com/browse/MW-1817): Integrate upgrade preparation bundle into core-mw helm chart [`997fb26`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/997fb26ba685a23216e7f14514a2b0b127625d83)
- [`MW-1607`](https://jira.open-xchange.com/browse/MW-1607): Add domain support for push payload [`e924d1b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e924d1b6202f66e35a36ca28aeeccfa7cbd9eed4)
  - Drive clients can now subscribe for push notifications using domains 'myFiles', 'sharedFiles' and 'publicFiles'  
  - The domain value gets re-inserted into push payload for transport 'apn2'  
  - Removed configuration property com.openexchange.drive.events.apn2.ios.pushDomain  

### Changed

- [`MWB-1849`](https://jira.open-xchange.com/browse/MWB-1849): Improved parsing of OAuth provider error message [`31933c5`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/31933c543f5d3ed245b5d743ab829f466fc22bea)
- [`MWB-1826`](https://jira.open-xchange.com/browse/MWB-1826): Added useful DEBUG log messages when adding an image to a signature [`1f1e8f9`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1f1e8f9e011f7974d24c4711e8b788fb2e4cc454)
- [`MWB-1828`](https://jira.open-xchange.com/browse/MWB-1828): Improved handling of `javax.net.ssl.SSLException` [`5180c7b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5180c7b35c68543c949e5a3630028b6bf04900ca)
- [`MWB-1849`](https://jira.open-xchange.com/browse/MWB-1849): Improved parsing of OAuth provider error message #2 [`c950617`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c95061775fa6a406ec53859e831c8543effd5c35)
- [`MWB-1830`](https://jira.open-xchange.com/browse/MWB-1830): Improved error message in case of denied request [`e0d3c94`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e0d3c9466a99c0bb60081959f9b010a02b18411d)
- [`MWB-1759`](https://jira.open-xchange.com/browse/MWB-1759): Deny requesting large message chunk in case client queries more than only identifier fields [`8e6ddb4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8e6ddb4dbf8597b7648f49a0be06339f7db85f6f)
- [`MWB-1800`](https://jira.open-xchange.com/browse/MWB-1800): Introduced configuration option [`4e95327`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4e95327af64167e1cb2a15c69b8259ad3f27c57b)
  - "com.openexchange.calendar.storage.rangeIndexHint" to allow insertion of index hints into typical database queries of the calendar module
- [`MWB-1776`](https://jira.open-xchange.com/browse/MWB-1776): Utility method to clear DNS cache [`b9c7ff3`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b9c7ff3e9085325a57226ea62d2793aeeed4c7a3)
- [`MWB-1759`](https://jira.open-xchange.com/browse/MWB-1759): Don't query flags if not required [`24729be`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/24729beb0cf55ae1ec330b98ed9e60eb9811ea48)
- [`MWB-1716`](https://jira.open-xchange.com/browse/MWB-1716): Added some helpful logging about bundle status [`1918165`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/19181654c5412109f56726a3d0463d941804b1e7)
- [`MWB-1716`](https://jira.open-xchange.com/browse/MWB-1716): Added some helpful logging about bundle status #2 [`d056354`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d056354459de85b1d1c2b8eb534b91d79dea13be)
- [`MWB-1764`](https://jira.open-xchange.com/browse/MWB-1764): Added DEBUG logging when checking status of a mail account yields an error [`2119413`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/2119413790e38ca1f0de1a1ecd7f7ab8c9fc499d)
- [`MWB-1750`](https://jira.open-xchange.com/browse/MWB-1750): Improved handling of possible javax.net.ssl.SSLException "Unsupported or unrecognized SSL message" [`0af276a`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/0af276a9850b18c5761fb57234e6e49d8d637312)
- [`MWB-1776`](https://jira.open-xchange.com/browse/MWB-1776): Added logging when DNS cache has been cleared [`fe93ae2`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/fe93ae2ab939a1df12331c29fa63ee36874c5438)
- [`MWB-1759`](https://jira.open-xchange.com/browse/MWB-1759): Delay initialization of TLongObjectHashMap [`bbb6a9f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bbb6a9fefeb1c7b4453f7d90eafb8c99eb419e24)
- [`MWB-1759`](https://jira.open-xchange.com/browse/MWB-1759): Nullify intermediate result [`103f70f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/103f70f964c34ecb34f0c3f33f0f28c0dce9d05f)

### Removed

- [`MW-1866`](https://jira.open-xchange.com/browse/MW-1866): Remove bundle com.openexchange.quartz [`c1975fc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c1975fc1cf946e14f448e603fedc3ae6340e486d)
- [`MW-1817`](https://jira.open-xchange.com/browse/MW-1817): Remove parallel container execution for update job [`a04115f`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/a04115f90064020770f56836939a5b897e27d027)

### Fixed

- [`MWB-1842`](https://jira.open-xchange.com/browse/MWB-1842): Prophylactically decode potentially MIME-encoded strings in property values in iCalendar files from MS Exchange [`24af8ec`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/24af8ec1ebe296f428322ecb3cc12ea0f114621e)
- [`MWB-1848`](https://jira.open-xchange.com/browse/MWB-1848): removed fallback value for manifest version field [`8b468a8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8b468a8ca05b5b7ec4c407b69e9c213080853e55)
- [`MWB-1839`](https://jira.open-xchange.com/browse/MWB-1839): Use dedicated introductions for forwarded meeting requests the user is not invited to [`e03a09e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/e03a09e667e5ccb7e353f29a295e779011af863c)
- [`MWB-1608`](https://jira.open-xchange.com/browse/MWB-1608): Fixed RuntimeExceptions in calendar stack [`bd422ac`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bd422acb277e62e806c2ede7c4fac750fef7de46)
- [`MWB-1808`](https://jira.open-xchange.com/browse/MWB-1808): properly detect reminders with missing permissions [`bb3f1e6`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/bb3f1e678164564f9c9d94bff79a74be11cec347)
- [`MWB-1813`](https://jira.open-xchange.com/browse/MWB-1813): Added documentation for mail?action=expunge [`56bff39`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/56bff39664dd852d47a6cd6d0ad28e0630fc91a9)
- [`MWB-1811`](https://jira.open-xchange.com/browse/MWB-1811): Ensure internal entity is admin, prevent permission [`57ca47b`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/57ca47bbdfc1a08b858fed3225d754b4e50a64a1) [`00b7702`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/00b7702ed2b8279ae9fe89871600f9c21f3f230f) [`1358b10`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/1358b10b70bbe4216304372f765a0e99337009ae)
- [`MWB-1838`](https://jira.open-xchange.com/browse/MWB-1838): Yield no result when auto-processing REQUEST with party crasher, let client re-apply iTip independently of message status flag [`8ec6208`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/8ec62084894d19c38365c7d3973ec1a33b27f5f5)
- [`MWB-1840`](https://jira.open-xchange.com/browse/MWB-1840): Return empty ajax respone if no event was found during resolve action [`81be74c`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/81be74c8a5586ff61e2d7b7a64cd40009ef8851e)
- Add missing com.openexchange.gab import in bundle com.openexchange.admin.plugin.hosting [`9749ecb`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/9749ecba3407d48934633bdc37167ee579efb26f)
- [`MWB-1805`](https://jira.open-xchange.com/browse/MWB-1805): Use URL-decoded variant of username in Authorization header for macOS Contacts client if applicable [`7d805e8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7d805e8dd2f7386c4302c9b0301bad4904fc7cc2)
- [`MWB-1735`](https://jira.open-xchange.com/browse/MWB-1735): Fixed links in Command Line Tools articles [`b9ac1ac`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/b9ac1ac8737562b8517b62bbce2262dae3018e9a)
- [`MWB-1711`](https://jira.open-xchange.com/browse/MWB-1711): Removed obsolete ContextDbLookupPluginInterface [`d9309b1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/d9309b1f080597ffbf92d4bddcebbdbc620888bc)
- [`MWB-1721`](https://jira.open-xchange.com/browse/MWB-1721): Evaluate 'X-Device-User-Agent' and pretty print common EAS devices in active clients overview [`7b197c1`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7b197c15fa581179e2f9ec65553fdd539f2538e0)
- [`MWB-1702`](https://jira.open-xchange.com/browse/MWB-1702): Skip premature cache invalidations to prevent race conditions upon folder update [`7e643b8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7e643b89754f304919b86dac92a1420c15add557)
- [`MWB-1787`](https://jira.open-xchange.com/browse/MWB-1787): Prefix download URI with current scheme/host if no absolute URI is configured in manifest [`5424d16`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/5424d16ec165a41b50be9dace94238ee6d6a41a5)
- [`MWB-1737`](https://jira.open-xchange.com/browse/MWB-1737): Removed obsolete ETag check after HTTP 409 errors [`eac8317`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/eac8317eab2540c690ff7bef23574899fc161a45)
- [`MW-1817`](https://jira.open-xchange.com/browse/MW-1817): Proper yaml in overwrite configmap if no properties are set [`7c4f3c8`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/7c4f3c8e88e3f51df4f5cb553c037080a846d72f)
- [`MWB-1760`](https://jira.open-xchange.com/browse/MWB-1760): Properly indicate "share not found" status for invalid targets of anonymous shares [`152f332`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/152f332bcbcc34b5270fb95abd713998a77a7440)
- Apply maxHeapSize to init containers [`493c5e4`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/493c5e459c2794f6429157fa61a2adb391e73a5d)
- [`MWB-1722`](https://jira.open-xchange.com/browse/MWB-1722): Do not convert aperture value, because we already read the f-number from exif data [`c5d97dc`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/c5d97dc137e31621214d8200dbaea26a115dee1d)
- Disable hz update bundle by default [`01c5d7d`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/01c5d7d255b56dcf3fd6015a30496524432c6e82)

### Security

- [`MW-1836`](https://jira.open-xchange.com/browse/MW-1836): Updated yq version [`79fca08`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/79fca08cfe1b6a18c896894cd4f4a8e8064c5ec1)
- [`MW-1836`](https://jira.open-xchange.com/browse/MW-1836): Replaced default-mysql-client with latest mariadb-client from official repository [`44a5255`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/44a52557b1a36ab90a3235683dfe46e81e0724a0)

<!-- References -->
[8.5.0-8.6.3]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.5.0...8.6.3
[8.7.0-8.7.19]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.7.0...8.7.19
[8.8]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.7.19...8.8.0
[8.9]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.8.0...8.9.0
[8.10]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.9.0...8.10.0
[8.11]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.10.0...8.11.0
[8.12]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.11.0...8.12.0
[8.13]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.12.0...8.13.0
[8.14]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.13.0...8.14.0
[8.15]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.14.0...8.15.0
[8.16]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.15.0...8.16.0
[8.17]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.16.0...8.17.0
[8.18]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.17.0...8.18.0
[8.19]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.18.0...8.19.0
[8.20]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.19.0...8.20.0
[8.21]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.20.0...8.21.0
[8.22]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.21.0...8.22.0
[8.23]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.22.0...8.23.0
[8.24]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.23.0...8.24.0
[8.25]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.24.0...8.25.0
[8.26]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.25.0...8.26.0
[8.27]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.26.0...8.27.0
[8.28]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.27.0...8.28.0
[8.29]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.28.0...8.29.0
[8.30]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.29.0...8.30.0
[8.31]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.30.0...8.31.0
[8.32]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.31.0...8.32.0
[8.33]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.32.0...8.33.0
[8.34]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.33.0...8.34.0
[8.35]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.34.0...8.35.0
