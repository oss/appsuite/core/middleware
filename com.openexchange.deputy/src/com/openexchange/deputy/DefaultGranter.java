/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.deputy;

import java.util.Set;
import com.google.common.collect.ImmutableSet;


/**
 * {@link DefaultGranter}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.6
 */
public class DefaultGranter implements Granter {

    /**
     * Creates a new builder.
     *
     * @return The newly created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /** The builder for an instance of <code>DefaultGranter</code> */
    public static final class Builder {

        private int userId;
        private Set<String> aliases;

        /**
         * Initializes a new {@link Builder}.
         */
        Builder() {
            super();
            userId = -1;
        }

        /**
         * Sets the identifier of the granting user.
         *
         * @param userId The user identifier to set
         * @return This builder
         */
        public Builder withUserId(int userId) {
            this.userId = userId;
            return this;
        }

        /**
         * Sets the mail aliases of the granting user.
         *
         * @param userId The mail aliases to set
         * @return This builder
         */
        public Builder withAliases(Set<String> aliases) {
            this.aliases = aliases;
            return this;
        }

        /**
         * Creates the instance of <code>DefaultGranter</code> from this builder's arguments.
         *
         * @return The instance of <code>DefaultGranter</code>
         * @throws IllegalArgumentException If this builder has missing or invalid arguments
         */
        public DefaultGranter build() {
            if (userId <= 0) {
                throw new IllegalArgumentException("User identifier not set or invalid");
            }
            if (aliases == null || aliases.isEmpty()) {
                throw new IllegalArgumentException("Mail aliases not set or invalid");
            }
            return new DefaultGranter(userId, ImmutableSet.copyOf(aliases));
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final int userId;
    private final Set<String> aliases;

    /**
     * Initializes a new {@link DefaultGranter}.
     *
     * @param userId The user identifier
     * @param aliases The mail aliases
     */
    DefaultGranter(int userId, Set<String> aliases) {
        super();
        this.userId = userId;
        this.aliases = aliases;
    }

    @Override
    public int getUserId() {
        return userId;
    }

    @Override
    public Set<String> getAliases() {
        return aliases;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(userId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DefaultGranter other = (DefaultGranter) obj;
        return userId == other.userId;
    }

    @Override
    public int compareTo(Granter o) {
        return Integer.compare(userId, o.getUserId());
    }

}
