/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.reseller.osgi;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import com.openexchange.admin.reseller.storage.mysqlStorage.OXResellerMySQLStorage;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;

/**
 * {@link InvalidationCacheServiceCustomizer}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public final class InvalidationCacheServiceCustomizer implements ServiceTrackerCustomizer<InvalidationCacheService, InvalidationCacheService> {

    private final BundleContext context;

    /**
     * Initializes a new {@link InvalidationCacheServiceCustomizer}.
     * 
     * @param context The bundle context
     */
    public InvalidationCacheServiceCustomizer(BundleContext context) {
        super();
        this.context = context;
    }

    @Override
    public InvalidationCacheService addingService(ServiceReference<InvalidationCacheService> reference) {
        InvalidationCacheService service = context.getService(reference);
        OXResellerMySQLStorage.setInvalidationCacheService(service);
        return service;
    }

    @Override
    public void modifiedService(ServiceReference<InvalidationCacheService> reference, InvalidationCacheService service) {
        // Nothing to do.
    }

    @Override
    public void removedService(ServiceReference<InvalidationCacheService> reference, InvalidationCacheService service) {
        OXResellerMySQLStorage.setInvalidationCacheService(null);
        context.ungetService(reference);
    }

}
