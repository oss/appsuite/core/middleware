/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.reseller.rmi.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import com.openexchange.admin.plugins.BasicAuthenticatorPluginInterface;
import com.openexchange.admin.reseller.rmi.dataobjects.ResellerAdmin;
import com.openexchange.admin.reseller.storage.interfaces.OXResellerStorageInterface;
import com.openexchange.admin.rmi.dataobjects.Context;
import com.openexchange.admin.rmi.dataobjects.Credentials;
import com.openexchange.admin.rmi.exceptions.InvalidCredentialsException;
import com.openexchange.admin.rmi.exceptions.InvalidDataException;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.rmi.impl.OXCommonImpl;
import com.openexchange.admin.tools.GenericChecks;
import com.openexchange.java.Strings;

/**
 * @author <a href="mailto:carsten.hoeger@open-xchange.com">Carsten Hoeger</a>
 *
 */
public class ResellerAuth extends OXCommonImpl implements BasicAuthenticatorPluginInterface {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ResellerAuth.class); // NOSONARLINT
    }

    /**
     * Initializes a new {@link ResellerAuth}.
     */
    public ResellerAuth() {
        super();
    }

    /**
     * Authenticate the given credentials throwing <code>InvalidCredentialsException</code> if attempt fails.
     *
     * @param authdata The credentials to check
     * @throws InvalidCredentialsException If authentication fails
     */
    public static void authenticateElseException(Credentials authdata) throws InvalidCredentialsException {
        try {
            doNullCheck(authdata);
        } catch (InvalidDataException e) {
            LoggerHolder.LOG.error("authdata is null", e);
            throw new InvalidCredentialsException("authentication failed", e);
        }
        if (Strings.isEmpty(authdata.getLogin())) {
            LoggerHolder.LOG.error("authdata has empty login");
            throw new InvalidCredentialsException("authentication failed");
        }
        try {
            OXResellerStorageInterface oxresell = OXResellerStorageInterface.getInstance();
            ResellerAdmin adm = oxresell.getData(new ResellerAdmin[] { new ResellerAdmin(authdata.getLogin()) })[0];
            if (!GenericChecks.authByMech(adm.getPassword(), authdata.getPassword(), adm.getPasswordMech(), adm.getSalt())) {
                throw new InvalidCredentialsException("authentication failed");
            }
        } catch (StorageException e) {
            LoggerHolder.LOG.error("", e);
            throw new InvalidCredentialsException("authentication failed");
        } catch (InvalidCredentialsException e) {
            LoggerHolder.LOG.error("", e);
            throw e;
        }
    }

    @Override
    public Optional<Boolean> doAuthentication(Credentials creds, Context ctx) throws InvalidCredentialsException {
        authenticateElseException(creds);
        return Optional.of(Boolean.TRUE);
    }

    @Override
    public Optional<Boolean> isMasterOfContext(Credentials creds, Context ctx) throws InvalidCredentialsException {
        try {
            doNullCheck(creds);
        } catch (InvalidDataException e) {
            LoggerHolder.LOG.error("authdata is null", e);
            throw new InvalidCredentialsException("authentication failed", e);
        }
        if (Strings.isEmpty(creds.getLogin())) {
            LoggerHolder.LOG.error("authdata has empty login");
            throw new InvalidCredentialsException("authentication failed");
        }
        try {
            OXResellerStorageInterface oxresell = OXResellerStorageInterface.getInstance();
            if (!oxresell.existsAdmin(new ResellerAdmin(creds.getLogin()))) {
                return Optional.of(Boolean.FALSE);
            }
            ResellerAdmin adm = oxresell.getData(new ResellerAdmin[] { new ResellerAdmin(creds.getLogin()) })[0];
            return Optional.of(Boolean.valueOf(oxresell.ownsContextOrIsPidOfOwner(ctx, adm.getId().intValue())));
        } catch (StorageException e) {
            LoggerHolder.LOG.error("", e);
            throw new InvalidCredentialsException("authentication failed");
        }
    }

    @Override
    public Optional<Boolean> isOwnerOfContext(Credentials creds, Context ctx) throws InvalidCredentialsException {
        try {
            doNullCheck(creds);
        } catch (InvalidDataException e) {
            LoggerHolder.LOG.error("authdata is null", e);
            throw new InvalidCredentialsException("authentication failed", e);
        }
        if (Strings.isEmpty(creds.getLogin())) {
            LoggerHolder.LOG.error("authdata has empty login");
            throw new InvalidCredentialsException("authentication failed");
        }
        try {
            OXResellerStorageInterface oxresell = OXResellerStorageInterface.getInstance();
            if (!oxresell.existsAdmin(new ResellerAdmin(creds.getLogin()))) {
                return Optional.of(Boolean.FALSE);
            }
            ResellerAdmin adm = oxresell.getData(new ResellerAdmin[] { new ResellerAdmin(creds.getLogin()) })[0];
            return Optional.of(Boolean.valueOf(oxresell.ownsContext(ctx, adm.getId().intValue())));
        } catch (StorageException e) {
            LoggerHolder.LOG.error("", e);
            throw new InvalidCredentialsException("authentication failed");
        }
    }

    @Override
    public Optional<Boolean> isMasterOfContext(Credentials creds, List<Context> ctx) throws InvalidCredentialsException {
        try {
            doNullCheck(creds);
        } catch (InvalidDataException e) {
            LoggerHolder.LOG.error("authdata is null", e);
            throw new InvalidCredentialsException("authentication failed", e);
        }
        if (Strings.isEmpty(creds.getLogin())) {
            LoggerHolder.LOG.error("authdata has empty login");
            throw new InvalidCredentialsException("authentication failed");
        }
        try {
            OXResellerStorageInterface oxresell = OXResellerStorageInterface.getInstance();
            if (!oxresell.existsAdmin(new ResellerAdmin(creds.getLogin()))) {
                return Optional.of(Boolean.FALSE);
            }
            ResellerAdmin adm = oxresell.getData(new ResellerAdmin[] { new ResellerAdmin(creds.getLogin()) })[0];
            return Optional.of(Boolean.valueOf(oxresell.ownsContextOrIsPidOfOwner(ctx, adm.getId().intValue())));
        } catch (StorageException e) {
            LoggerHolder.LOG.error("", e);
            throw new InvalidCredentialsException("authentication failed");
        }
    }

}
