/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.version.internal;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.java.Strings;
import com.openexchange.version.Version;
import com.openexchange.version.VersionService;

/**
 * Stores the version of the Middleware.
 *
 * @author <a href="mailto:marcus.klein@open-xchange.com">Marcus Klein</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a> JavaDoc
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a> - Refactoring
 * @author <a href="mailto:marcel.broecher@open-xchange.com">Marcel Broecher</a> Refactoring
 */
public class VersionServiceImpl implements VersionService {

    private static final Logger LOG = LoggerFactory.getLogger(VersionServiceImpl.class);
    private static final String VERSION_FILENAME = "/opt/open-xchange/version.txt";
    private static final String BACKUP_VERSION_FILENAME = "/version.txt";
    private static final Pattern VERSION_PATTERN = Pattern.compile("(?:version: )?(\\d+)\\.(\\d+)\\.(\\d+)");

    private final Version serverVersion;

    /**
     * Initializes a new {@link VersionServiceImpl}.
     */
    public VersionServiceImpl(){
        super();
        Version version;
        try (FileInputStream fis = new FileInputStream(VERSION_FILENAME)) {
            version = parseVersion(Strings.asciiLowerCase(IOUtils.toString(fis, StandardCharsets.UTF_8).trim()));
        } catch (IOException | IllegalArgumentException e) {
            version = optBackupVersion().orElseGet(() -> Version.builder().build());
            LOG.warn("Unable to parse version from {}, falling back to {}.", VERSION_FILENAME, version, e);
        }
        this.serverVersion = version;
    }

    @Override
    public Version getVersion() {
        return this.serverVersion;
    }

    // -------------------------- private methods --------------------

    /**
     * Parses {@value #BACKUP_VERSION_FILENAME} as a backup version if possible.
     *
     * @return The optional backup version
     */
    private Optional<Version> optBackupVersion(){
        try (InputStream fis = getClass().getResourceAsStream(BACKUP_VERSION_FILENAME)){
            if (fis == null) {
                return Optional.empty();
            }
            return Optional.of(parseVersion(IOUtils.toString(fis, StandardCharsets.UTF_8)));
        } catch (IOException | IllegalArgumentException e) {
            LOG.warn("Unable to parse backup version.", e);
            return Optional.empty();
        }
    }

    /**
     * Parses the version string.
     *
     * @param versionString The version string to parse
     * @return The version
     * @throws IllegalArgumentException in case the version string is not a valid version
     */
    private Version parseVersion(String versionString) throws IllegalArgumentException {
        String version = versionString;
        try {
            if (version.startsWith("version:")) {
                version = version.substring(8).trim();
            }
            return Version.parse(version);
        } catch (Exception e) {
            Matcher matcher = VERSION_PATTERN.matcher(version);
            if (matcher.matches() == false) {
                throw new IllegalArgumentException("Version '%s' is not a valid version string".formatted(version));
            }
            int major = Integer.parseInt(matcher.group(1));
            int minor = Integer.parseInt(matcher.group(2));
            int patch = Integer.parseInt(matcher.group(3));
            return Version.builder()
                .withMajor(major)
                .withMinor(minor)
                .withPatch(patch)
                .build();
        }
    }

}

