/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.gdpr.dataexport.impl.utils;

import static com.openexchange.java.Autoboxing.L;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.activation.MimetypesFileTypeMap;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import com.openexchange.exception.OXException;
import com.openexchange.filestore.FileStorage;
import com.openexchange.filestore.FileStorageCodes;
import com.openexchange.gdpr.dataexport.impl.DataExportUtility;
import com.openexchange.java.Buffers;
import com.openexchange.java.Streams;
import com.openexchange.java.util.UUIDs;
import it.geosolutions.imageio.stream.eraf.EnhancedRandomAccessFile;

/**
 * {@link TmpFileFileStorage}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class TmpFileFileStorage implements FileStorage {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(TmpFileFileStorage.class);

    /**
     * The buffer size of 64K.
     */
    private static final int BUFFER_SIZE = DataExportUtility.BUFFER_SIZE;

    private final File dir;

    /**
     * Initializes a new {@link TmpFileFileStorage}.
     *
     * @param dir The directory to use
     */
    public TmpFileFileStorage(File dir) {
        super();
        this.dir = dir;
    }

    @Override
    public URI getUri() {
        return dir.toURI();
    }

    @Override
    public String saveNewFile(InputStream file) throws OXException {
        FileOutputStream fos = null;
        try {
            String uuid = UUIDs.getUnformattedStringFromRandom();
            File newFile = new File(dir, uuid);
            com.openexchange.log.LogProperties.addTempFile(newFile);

            fos = new FileOutputStream(newFile);
            byte[] buf = new byte[BUFFER_SIZE];
            for (int read; (read = file.read(buf, 0, BUFFER_SIZE)) > 0;) {
                fos.write(buf, 0, read);
            }
            fos.flush();
            return uuid;
        } catch (IOException e) {
            throw FileStorageCodes.IOERROR.create(e, e.getMessage());
        } finally {
            Streams.close(file, fos);
        }
    }

    @Override
    public InputStream getFile(String name) throws OXException {
        try {
            return new FileInputStream(new File(dir, name));
        } catch (FileNotFoundException e) {
            throw FileStorageCodes.FILE_NOT_FOUND.create(e, name);
        }
    }

    @Override
    public SortedSet<String> getFileList() {
        final SortedSet<String> allIds = new TreeSet<String>();
        listRecursively(allIds, "", dir);
        return allIds;
    }

    protected void listRecursively(final SortedSet<String> allIds, String prefix, final File file) {
        if (!prefix.isEmpty() && !prefix.endsWith("/")) {
            prefix += "/";
        }

        if (file.isFile()) {
            allIds.add(prefix + file.getName());
        } else {
            File[] files = file.listFiles(); // <-- Returns null if this abstract pathname does not denote a directory, or if an I/O error occurs.
            if (files != null) {
                for (File subfile : files) {
                    if (file.equals(dir)) {
                        listRecursively(allIds, "", subfile);
                    } else {
                        listRecursively(allIds, prefix + file.getName(), subfile);
                    }
                }
            }
        }
    }

    @Override
    public long getFileSize(String name) throws OXException {
        File file = new File(dir, name);
        // Returns the length, in bytes, of the file denoted by this abstract pathname, or 0L if the file does not exist.
        long length = file.length();
        if (length <= 0 && false == file.exists()) {
            throw FileStorageCodes.FILE_NOT_FOUND.create(new FileNotFoundException(file.getPath()), name);
        }
        return length;
    }

    @Override
    public String getMimeType(String name) throws OXException {
        MimetypesFileTypeMap map = new MimetypesFileTypeMap();
        return map.getContentType(new File(dir, name));
    }

    @Override
    public boolean deleteFile(String identifier) throws OXException {
        return delete(new String[] { identifier }).isEmpty();
    }

    @Override
    public Set<String> deleteFiles(String[] identifiers) throws OXException {
        return delete(identifiers);
    }

    /**
     * Deletes multiple files.
     *
     * @param names The file names
     * @return A set containing those file names that could not be deleted
     */
    protected Set<String> delete(final String[] names) {
        final Set<String> notDeleted = new HashSet<String>();
        for (final String name : names) {
            if (!new File(dir, name).delete()) {
                notDeleted.add(name);
            }
        }
        return notDeleted;
    }

    @Override
    public void remove() throws OXException {
        try {
            FileUtils.deleteDirectory(dir);
        } catch (IOException e) {
            throw FileStorageCodes.NOT_ELIMINATED.create(e);
        }
    }

    @Override
    public void recreateStateFile() throws OXException {
        // Nothing
    }

    @Override
    public boolean stateFileIsCorrect() throws OXException {
        return true;
    }

    @Override
    public long appendToFile(InputStream file, String name, long offset) throws OXException {
        EnhancedRandomAccessFile eraf = null;
        try {
            eraf = eraf(name, false);
            if (offset != eraf.length()) {
                throw FileStorageCodes.INVALID_OFFSET.create(L(offset), name, L(eraf.length()));
            }
            eraf.seek(eraf.length());
            byte[] buffer = new byte[Buffers.BUFFER_SIZE_8K];
            int read;
            while (0 < (read = file.read(buffer, 0, buffer.length))) {
                eraf.write(buffer, 0, read);
            }
            eraf.flush();
            return eraf.length();
        } catch (IOException e) {
            throw FileStorageCodes.IOERROR.create(e, e.getMessage());
        } finally {
            if (null != eraf) {
                try {
                    eraf.close();
                } catch (Exception e) {
                    LOG.warn("error closing random access file", e);
                }
            }
            Streams.close(file);
        }
    }

    private static final String READ = "r";

    private static final String READ_WRITE = "rw";

    /**
     * Initializes a new {@link EnhancedRandomAccessFile} reference for the supplied filename.
     *
     * @param name The filename, relative to the storage's parent directory
     * @param readOnly <code>true</code> if read-only access is sufficient, <code>false</code> to use read-write access
     * @return A new {@link EnhancedRandomAccessFile} instance
     * @throws OXException If the denoted file was not found or an I/O-error occurred
     */
    private EnhancedRandomAccessFile eraf(String name, boolean readOnly) throws OXException {
        File file = new File(dir, name);
        try {
            return new EnhancedRandomAccessFile(file, readOnly ? READ : READ_WRITE);
        } catch (FileNotFoundException e) {
            throw FileStorageCodes.FILE_NOT_FOUND.create(e, file.getAbsolutePath());
        } catch (IOException e) {
            throw FileStorageCodes.IOERROR.create(e, e.getMessage());
        }
    }

    @Override
    public void setFileLength(long length, String name) throws OXException {
        RandomAccessFile raf = null;
        try {
            raf = raf(name, false);
            if (length > raf.length()) {
                throw FileStorageCodes.INVALID_LENGTH.create(L(length), name, L(raf.length()));
            }
            raf.setLength(length);
        } catch (IOException e) {
            throw FileStorageCodes.IOERROR.create(e, e.getMessage());
        } finally {
            Streams.close(raf);
        }
    }

    /**
     * Initializes a new {@link RandomAccessFile} reference for the supplied filename.
     *
     * @param name The filename, relative to the storage's parent directory
     * @param readOnly <code>true</code> if read-only access is sufficient, <code>false</code> to use read-write access
     * @return A new {@link RandomAccessFile} instance
     * @throws OXException If the denoted file was not found
     */
    private RandomAccessFile raf(String name, boolean readOnly) throws OXException {
        File file = new File(dir, name);
        try {
            return new RandomAccessFile(file, readOnly ? READ : READ_WRITE);
        } catch (FileNotFoundException e) {
            throw FileStorageCodes.FILE_NOT_FOUND.create(e, file.getAbsolutePath());
        }
    }

    @Override
    public InputStream getFile(String name, long offset, long length) throws OXException {
        EnhancedRandomAccessFile eraf = eraf(name, true);
        boolean error = true;
        try {
            if (offset >= eraf.length() || -1 != length && length > eraf.length() - offset) {
                throw FileStorageCodes.INVALID_RANGE.create(L(offset), L(length), name, L(eraf.length()));
            }
            RandomAccessFileInputStream in = new RandomAccessFileInputStream(eraf, offset, length);
            error = false;
            return in;
        } catch (FileNotFoundException e) {
            throw FileStorageCodes.FILE_NOT_FOUND.create(e, name);
        } catch (IOException e) {
            throw FileStorageCodes.IOERROR.create(e, e.getMessage());
        } finally {
            if (error) {
                try {
                    eraf.close();
                } catch (Exception e) {
                    LOG.warn("error closing random access file", e);
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static class RandomAccessFileInputStream extends java.io.InputStream {

        private final EnhancedRandomAccessFile file;
        private final long endPosition;
        private long markedPosition;

        /**
         * Initializes a new {@link RandomAccessFileInputStream}.
         *
         * @param eraf The random access file to create the random access stream for
         * @param offset The offset to begin to read data from, or <code>0</code> to start at the beginning
         * @param length The number of bytes to read starting from the offset, or <code>-1</code> to read until the end
         * @throws IOException if the offset is less than 0 or if an I/O error occurs.
         */
        public RandomAccessFileInputStream(EnhancedRandomAccessFile eraf, long offset, long length) throws IOException {
            super();
            this.file = eraf;
            this.endPosition = -1 == length ? file.length() : Math.min(file.length(), offset + length);
            this.file.seek(offset);
        }

        @Override
        public synchronized int read() throws IOException {
            if (file.getFilePointer() >= endPosition) {
                return -1;
            }
            return file.read();
        }

        @Override
        public synchronized void close() throws IOException {
            this.file.close();
            super.close();
        }

        @Override
        public boolean markSupported() {
            return true;
        }

        @Override
        public synchronized void mark(int readlimit) {
            try {
                this.markedPosition = file.getFilePointer();
            } catch (IOException e) {
                // indicate failure in following reset() call
                this.markedPosition = Long.MIN_VALUE;
            }
        }

        @Override
        public synchronized void reset() throws IOException {
            if (Long.MIN_VALUE == this.markedPosition) {
                throw new IOException("No position marked");
            }
            file.seek(markedPosition);
        }

        @Override
        public int read(byte[] b) throws IOException {
            return this.read(b, 0, b.length);
        }

        @Override
        public synchronized int read(byte[] b, int off, int len) throws IOException {
            int available = this.available();
            if (0 >= available) {
                return -1;
            }
            return file.read(b, off, Math.min(available, len));
        }

        @Override
        public synchronized int available() throws IOException {
            return (int)(endPosition - file.getFilePointer());
        }

        @Override
        public synchronized long skip(long n) throws IOException {
            if (n > Integer.MAX_VALUE) {
                throw new IOException("can skip at maximum " + Integer.MAX_VALUE + " bytes");
            }
            return file.skipBytes((int)(n & 0xFFFFFFFF));
        }

    }

}
