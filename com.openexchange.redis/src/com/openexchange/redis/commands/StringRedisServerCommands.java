/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.redis.commands;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import io.lettuce.core.ClientListArgs;
import io.lettuce.core.FlushMode;
import io.lettuce.core.KillArgs;
import io.lettuce.core.ShutdownArgs;
import io.lettuce.core.TrackingArgs;
import io.lettuce.core.TrackingInfo;
import io.lettuce.core.UnblockType;
import io.lettuce.core.api.sync.RedisServerCommands;
import io.lettuce.core.protocol.CommandType;


/**
 * {@link StringRedisServerCommands} - The Redis server commands for string values
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public class StringRedisServerCommands implements RedisServerCommands<String, String> {

    private static final int DEFAULT_CAPACITY = 32;

    private final RedisServerCommands<String, InputStream> delegate;

    public StringRedisServerCommands(RedisServerCommands<String, InputStream> delegate) {
        super();
        this.delegate = delegate;
    }

    @Override
    public TrackingInfo clientTrackinginfo() {
        return delegate.clientTrackinginfo();
    }

    @Override
    public String bgrewriteaof() {
        return delegate.bgrewriteaof();
    }

    @Override
    public String bgsave() {
        return delegate.bgsave();
    }

    @Override
    public String clientCaching(boolean enabled) {
        return delegate.clientCaching(enabled);
    }

    @Override
    public String clientGetname() {
        return delegate.clientGetname();
    }

    @Override
    public Long clientGetredir() {
        return delegate.clientGetredir();
    }

    @Override
    public Long clientId() {
        return delegate.clientId();
    }

    @Override
    public String clientKill(String addr) {
        return delegate.clientKill(addr);
    }

    @Override
    public Long clientKill(KillArgs killArgs) {
        return delegate.clientKill(killArgs);
    }

    @Override
    public String clientList() {
        return delegate.clientList();
    }

    @Override
    public String clientList(ClientListArgs clientListArgs) {
        return delegate.clientList(clientListArgs);
    }

    @Override
    public String clientInfo() {
        return delegate.clientInfo();
    }

    @Override
    public String clientNoEvict(boolean on) {
        return delegate.clientNoEvict(on);
    }

    @Override
    public String clientPause(long timeout) {
        return delegate.clientPause(timeout);
    }

    @Override
    public String clientSetname(String name) {
        return delegate.clientSetname(name);
    }

    @Override
    public String clientSetinfo(String key, String value) {
        return delegate.clientSetinfo(key, value);
    }

    @Override
    public String clientTracking(TrackingArgs args) {
        return delegate.clientTracking(args);
    }

    @Override
    public Long clientUnblock(long id, UnblockType type) {
        return delegate.clientUnblock(id, type);
    }

    @Override
    public List<Object> command() {
        return delegate.command();
    }

    @Override
    public Long commandCount() {
        return delegate.commandCount();
    }

    @Override
    public List<Object> commandInfo(String... commands) {
        return delegate.commandInfo(commands);
    }

    @Override
    public List<Object> commandInfo(CommandType... commands) {
        return delegate.commandInfo(commands);
    }

    @Override
    public Map<String, String> configGet(String parameter) {
        return delegate.configGet(parameter);
    }

    @Override
    public Map<String, String> configGet(String... parameters) {
        return delegate.configGet(parameters);
    }

    @Override
    public String configResetstat() {
        return delegate.configResetstat();
    }

    @Override
    public String configRewrite() {
        return delegate.configRewrite();
    }

    @Override
    public String configSet(String parameter, String value) {
        return delegate.configSet(parameter, value);
    }

    @Override
    public String configSet(Map<String, String> kvs) {
        return delegate.configSet(kvs);
    }

    @Override
    public Long dbsize() {
        return delegate.dbsize();
    }

    @Override
    public String debugCrashAndRecover(Long delay) {
        return delegate.debugCrashAndRecover(delay);
    }

    @Override
    public String debugHtstats(int db) {
        return delegate.debugHtstats(db);
    }

    @Override
    public String debugObject(String key) {
        return delegate.debugObject(key);
    }

    @Override
    public void debugOom() {
        delegate.debugOom();
    }

    @Override
    public String debugReload() {
        return delegate.debugReload();
    }

    @Override
    public String debugRestart(Long delay) {
        return delegate.debugRestart(delay);
    }

    @Override
    public String debugSdslen(String key) {
        return delegate.debugSdslen(key);
    }

    @Override
    public void debugSegfault() {
        delegate.debugSegfault();
    }

    @Override
    public String flushall() {
        return delegate.flushall();
    }

    @Override
    public String flushall(FlushMode flushMode) {
        return delegate.flushall(flushMode);
    }

    @Override
    public String flushallAsync() {
        return delegate.flushallAsync();
    }

    @Override
    public String flushdb() {
        return delegate.flushdb();
    }

    @Override
    public String flushdb(FlushMode flushMode) {
        return delegate.flushdb(flushMode);
    }

    @Override
    public String flushdbAsync() {
        return delegate.flushdbAsync();
    }

    @Override
    public String info() {
        return delegate.info();
    }

    @Override
    public String info(String section) {
        return delegate.info(section);
    }

    @Override
    public Date lastsave() {
        return delegate.lastsave();
    }

    @Override
    public Long memoryUsage(String key) {
        return delegate.memoryUsage(key);
    }

    @Override
    public String replicaof(String host, int port) {
        return delegate.replicaof(host, port);
    }

    @Override
    public String replicaofNoOne() {
        return delegate.replicaofNoOne();
    }

    @Override
    public String save() {
        return delegate.save();
    }

    @Override
    public void shutdown(boolean save) {
        delegate.shutdown(save);
    }

    @Override
    public void shutdown(ShutdownArgs args) {
        delegate.shutdown(args);
    }

    @Override
    public String slaveof(String host, int port) {
        return delegate.slaveof(host, port);
    }

    @Override
    public String slaveofNoOne() {
        return delegate.slaveofNoOne();
    }

    @Override
    public List<Object> slowlogGet() {
        return delegate.slowlogGet();
    }

    @Override
    public List<Object> slowlogGet(int count) {
        return delegate.slowlogGet(count);
    }

    @Override
    public Long slowlogLen() {
        return delegate.slowlogLen();
    }

    @Override
    public String slowlogReset() {
        return delegate.slowlogReset();
    }

    @Override
    public List<String> time() {
        List<InputStream> streams = delegate.time();
        if (null == streams || streams.isEmpty()) {
            return Collections.emptyList();
        }
        List<String> result = new ArrayList<>(streams.size());
        for (InputStream stream : streams) {
            result.add(RedisCommandUtils.stream2String(stream, DEFAULT_CAPACITY));
        }
        return result;
    }
}
