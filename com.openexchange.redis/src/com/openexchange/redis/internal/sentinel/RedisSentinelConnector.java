/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.redis.internal.sentinel;

import java.io.InputStream;
import java.time.Duration;
import java.util.Optional;
import java.util.function.Supplier;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.java.CloseOnExceptionCallable;
import com.openexchange.redis.OperationMode;
import com.openexchange.redis.RedisCommandsProvider;
import com.openexchange.redis.internal.AbstractRedisConnector;
import com.openexchange.redis.internal.RedisConfiguration;
import com.openexchange.redis.internal.RedisConnectorArgs;
import com.openexchange.redis.internal.RedisPingOperation;
import com.openexchange.redis.internal.codecs.CompressionRedisCodec;
import com.openexchange.redis.internal.codecs.PubSubJSONCodec;
import com.openexchange.server.ServiceLookup;
import io.lettuce.core.ClientOptions;
import io.lettuce.core.ReadFrom;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.codec.RedisCodec;
import io.lettuce.core.masterreplica.MasterReplica;
import io.lettuce.core.masterreplica.StatefulRedisMasterReplicaConnection;
import io.lettuce.core.metrics.MicrometerCommandLatencyRecorder;
import io.lettuce.core.metrics.MicrometerOptions;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import io.lettuce.core.resource.ClientResources;
import io.micrometer.core.instrument.Metrics;
import net.jodah.failsafe.CircuitBreaker;

/**
 * {@link RedisSentinelConnector} - Connector for a Redis Sentinel.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 *
 */
public final class RedisSentinelConnector extends AbstractRedisConnector<RedisClient, StatefulRedisConnection<String, InputStream>> {

    /**
     * Creates a new connector instance.
     *
     * @param configuration The configuration to use
     * @param services The service look-up providing required services
     * @return The new connector instance
     * @throws OXException If initialization fails
     */
    public static RedisSentinelConnector newInstance(RedisConfiguration configuration, ServiceLookup services) throws OXException {
        // Get Redis URI
        RedisURI redisURI = createRedisURIsFromConfig(configuration, optVersion(services), OperationMode.SENTINEL).get(0);

        // Builder for client resources
        ClientResources.Builder resourcesBuilder = ClientResources.builder();

        // Enable metrics
        boolean enableCommandLatencyMetrics = configuration.getLatencyMetrics();
        if (enableCommandLatencyMetrics) {
            MicrometerOptions options = MicrometerOptions.create();
            resourcesBuilder.commandLatencyRecorder(new MicrometerCommandLatencyRecorder(Metrics.globalRegistry, options));
        }

        // Return connector when fully initialized
        return CloseOnExceptionCallable.execute(new CloseOnExceptionCallable<RedisSentinelConnector>() {

            @Override
            protected RedisSentinelConnector doCall() throws Exception {
                // Build client
                RedisClient redisClient = addAndReturnCloseable(RedisClient.create(resourcesBuilder.build(), redisURI));

                // Client options
                ClientOptions.Builder clientOptions = ClientOptions.builder();
                long commandTimeoutMillis = configuration.getCommandTimeoutMillis();
                long connectTimeoutMillis = configuration.getConnectTimeoutMillis();
                initClientOptions(clientOptions, commandTimeoutMillis, connectTimeoutMillis, redisURI, services);
                redisClient.setOptions(clientOptions.build());

                // Circuit breaker
                Optional<CircuitBreaker> optCircuitBreaker = initCircuitBreaker(redisURI, configuration);

                // Create connection pool supplier
                Duration commandTimeoutDuration = Duration.ofMillis(commandTimeoutMillis < 0 ? 0 : commandTimeoutMillis);
                RedisCodec<String,InputStream> codec = new CompressionRedisCodec(configuration.getCompressionType(), configuration.getMinimumCompressionSize());
                Supplier<StatefulRedisConnection<String, InputStream>> connectionSupplier = createConnectionSupplier(configuration, redisURI, redisClient, commandTimeoutDuration, codec);
                Supplier<GenericObjectPool<StatefulRedisConnection<String, InputStream>>> poolSupplier = createPoolSupplier(connectionSupplier, configuration);

                // Initialize instance, check if reachable & return
                RedisConnectorArgs<RedisClient, StatefulRedisConnection<String, InputStream>> args = RedisConnectorArgs.<RedisClient, StatefulRedisConnection<String, InputStream>> builder()
                    .withInstanceId(configuration.getInstanceId())
                    .withRedisType(configuration.getRedisType())
                    .withRedisURI(redisURI)
                    .withRedisClient(redisClient)
                    .withPoolSupplier(poolSupplier)
                    .withConnectionSupplier(connectionSupplier)
                    .withCircuitBreaker(optCircuitBreaker.orElse(null))
                    .withOperationExecutionDurationThreshold(configuration.getOperationExecutionDurationThreshold())
                    .withNewConnectionIfWaitExceeded(configuration.isConnectionPoolNewConnectionIfWaitExceeded())
                    .build();
                RedisSentinelConnector connector = new RedisSentinelConnector(args, services);
                checkReachability(RedisPingOperation.getInstance(), redisURI, connector);
                return connector;
            }
        }, ERROR_CONVERTER);
    }

    private static Supplier<StatefulRedisConnection<String, InputStream>> createConnectionSupplier(RedisConfiguration configuration, RedisURI redisURI, RedisClient redisClient, Duration commandTimeoutDuration, RedisCodec<String, InputStream> codec) {
        // Consider master-replica on connection establishment or not
        if (configuration.getSentinelMasterReplica()) {
            return () -> newMasterReplicaConnection(redisClient, commandTimeoutDuration, codec, redisURI);
        }
        return () -> newConnection(redisClient, commandTimeoutDuration, codec);
    }

    private static StatefulRedisConnection<String, InputStream> newConnection(RedisClient redisClient, Duration timeoutDuration, RedisCodec<String, InputStream> codec) {
        StatefulRedisConnection<String, InputStream> connection = redisClient.connect(codec);
        connection.setTimeout(timeoutDuration);
        return connection;
    }

    private static StatefulRedisConnection<String, InputStream> newMasterReplicaConnection(RedisClient redisClient, Duration timeoutDuration, RedisCodec<String, InputStream> codec, RedisURI redisURI) {
        StatefulRedisMasterReplicaConnection<String, InputStream> connection = MasterReplica.connect(redisClient, codec, redisURI);
        connection.setTimeout(timeoutDuration);
        // ReadFrom.REPLICA_PREFERRED vs. ReadFrom.MASTER_PREFERRED
        connection.setReadFrom(ReadFrom.REPLICA_PREFERRED);
        return connection;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Initializes a new {@link RedisSentinelConnector}.
     *
     * @param args The arguments for this Redis connector
     * @param services The service look-up
     */
    private RedisSentinelConnector(RedisConnectorArgs<RedisClient, StatefulRedisConnection<String, InputStream>> args, ServiceLookup services) {
        super(args, services);
    }

    @Override
    protected RedisCommandsProvider getCommandsProviderFor(StatefulRedisConnection<String, InputStream> connection) {
        return new RedisSentinelCommandsProvider(connection);
    }

    @Override
    public StatefulRedisPubSubConnection<String, JSONObject> newPubSubConnection() {
        return redisClient.connectPubSub(PubSubJSONCodec.getInstance());
    }

    @Override
    public OperationMode getMode() {
        return OperationMode.SENTINEL;
    }

}
