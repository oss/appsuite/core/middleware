/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.redis.internal.metrics;

import static com.openexchange.java.Autoboxing.I;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.pool2.impl.GenericObjectPool;
import io.lettuce.core.api.StatefulConnection;
import io.micrometer.core.instrument.FunctionCounter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.TimeGauge;

/**
 * {@link RedisPoolMetricHandler} - Handles metrics for Redis connection pool.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class RedisPoolMetricHandler {

    /**
     * Initializes a new {@link RedisPoolMetricHandler}.
     */
    private RedisPoolMetricHandler() {
        super();
    }

    private static final AtomicReference<List<Meter>> METERS = new AtomicReference<>();

    private static final String METRIC_NAME_PREFIX = "appsuite.redis.connections.";

    /**
     * Re-Initializes the metrics for specified Redis connection pool.
     *
     * @param <C> The type of the Redis connection
     * @param pool The pool for which to initialize the metrics
     */
    public static <C extends StatefulConnection<String, InputStream>> void reinitPoolMetrics(GenericObjectPool<C> pool) {
        stop();
        initPoolMetrics(pool);
    }

    /**
     * Initializes the metrics for specified Redis connection pool.
     *
     * @param <C> The type of the Redis connection
     * @param pool The pool for which to initialize the metrics
     */
    public static <C extends StatefulConnection<String, InputStream>> void initPoolMetrics(GenericObjectPool<C> pool) {
        if (METERS.compareAndSet(null, Collections.emptyList())) {
            // Not yet initialized. Go ahead...
            List<Meter> meters = new ArrayList<>(16);

            // @formatter:off
            List<Tag> tags = List.of(Tag.of("name", "redis-pool"));
            meters.add(Gauge.builder(METRIC_NAME_PREFIX + "num.idle", () -> I(pool.getNumIdle()))
                .description("The number of Redis connections currently idle in the pool")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(Gauge.builder(METRIC_NAME_PREFIX + "num.active", () -> I(pool.getNumActive()))
                .description("The number of Redis connections currently active in the pool")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(Gauge.builder(METRIC_NAME_PREFIX + "num.total", () -> I(pool.getNumActive() + pool.getNumIdle()))
                .description("The total number (active & idle) of Redis connections in the pool")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(Gauge.builder(METRIC_NAME_PREFIX + "num.waiters", () -> I(pool.getNumWaiters()))
                .description("The estimate of the number of threads currently blocked waiting for a Redis connection from the pool")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(TimeGauge
                .builder(METRIC_NAME_PREFIX + "max.borrow.wait", pool, TimeUnit.MILLISECONDS, p -> p.getMaxBorrowWaitDuration().toMillis())
                .description("The maximum time a thread has waited to borrow Redis connections from the pool")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(TimeGauge
                .builder(METRIC_NAME_PREFIX + "mean.active", pool, TimeUnit.MILLISECONDS, p -> p.getMeanActiveDuration().toMillis())
                .description("The mean time Redis connections are active")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(TimeGauge
                .builder(METRIC_NAME_PREFIX + "mean.idle", pool, TimeUnit.MILLISECONDS, p -> p.getMeanIdleDuration().toMillis())
                .description("The mean time Redis connections are idle")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(TimeGauge
                .builder(METRIC_NAME_PREFIX + "mean.borrow.wait", pool, TimeUnit.MILLISECONDS, p -> p.getMeanBorrowWaitDuration().toMillis())
                .description("The mean time threads wait to borrow a Redis connection")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(FunctionCounter
                .builder(METRIC_NAME_PREFIX + "created", pool, p -> p.getCreatedCount())
                .description("The total number of Redis connections created for this pool over the lifetime of the pool")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(FunctionCounter
                .builder(METRIC_NAME_PREFIX + "borrowed", pool, p -> p.getBorrowedCount())
                .description("The total number of Redis connections successfully borrowed from this pool over the lifetime of the pool")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(FunctionCounter
                .builder(METRIC_NAME_PREFIX + "returned", pool, p -> p.getReturnedCount())
                .description("The total number of Redis connections returned to this pool over the lifetime of the pool")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(FunctionCounter
                .builder(METRIC_NAME_PREFIX + "destroyed", pool, p -> p.getDestroyedCount())
                .description("The total number of Redis connections destroyed by this pool over the lifetime of the pool")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(FunctionCounter
                .builder(METRIC_NAME_PREFIX + "destroyed.by.evictor", pool, p -> p.getDestroyedByEvictorCount())
                .description("The total number of Redis connections destroyed by the evictor associated with this pool over the lifetime of the pool")
                .tags(tags)
                .register(Metrics.globalRegistry));

            meters.add(FunctionCounter
                .builder(METRIC_NAME_PREFIX + "destroyed.by.borrow.validation", pool, p -> p.getDestroyedByBorrowValidationCount())
                .description("The total number of Redis connections destroyed by this pool as a result of failing validation during borrowObject() over the lifetime of the pool")
                .tags(tags)
                .register(Metrics.globalRegistry));

            // @formatter:on

            METERS.set(List.copyOf(meters));
        }
    }

    /**
     * Removes the metrics from the metric registry
     */
    public static void stop() {
        List<Meter> meters = METERS.getAndSet(null);
        if (meters != null) {
            // Not yet dropped. So, drop 'em
            meters.forEach(Metrics.globalRegistry::remove);
        }
    }
}
