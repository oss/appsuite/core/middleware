/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.redis.internal;

import java.io.InputStream;
import java.util.Optional;
import java.util.function.Supplier;
import org.apache.commons.pool2.impl.GenericObjectPool;
import com.openexchange.redis.RedisType;
import io.lettuce.core.AbstractRedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulConnection;
import net.jodah.failsafe.CircuitBreaker;

/**
 * {@link RedisConnectorArgs} - The arguments for a Redis connector.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class RedisConnectorArgs<R extends AbstractRedisClient, C extends StatefulConnection<String, InputStream>> {

    /**
     * Creates a new builder for an instance of <code>RedisConnectorArgs</code>
     *
     * @return The new builder
     */
    public static <R extends AbstractRedisClient, C extends StatefulConnection<String, InputStream>> Builder<R, C> builder() {
        return new Builder<R, C>();
    }

    /**
     * The builder for an instance of <code>RedisConnectorArgs</code>.
     */
    public static final class Builder<R extends AbstractRedisClient, C extends StatefulConnection<String, InputStream>> {

        private String instanceId;
        private RedisType redisType;
        private RedisURI redisURI;
        private R redisClient;
        private Supplier<GenericObjectPool<C>> poolSupplier;
        private Supplier<C> connectionSupplier;
        private CircuitBreaker optCircuitBreaker;
        private long operationExecutionDurationThreshold;
        private boolean newConnectionIfWaitExceeded;

        /**
         * Initializes a new instance of {@link RedisConnectorArgs.Builder}.
         */
        private Builder() {
            super();
        }

        /**
         * Sets the identifier representing the Redis instance (e.g. remote site or cache) this instance connects to, or <code>null</code> if regular.
         *
         * @param instanceId The instance identifier to set
         */
        public Builder<R, C> withInstanceId(String instanceId) {
            this.instanceId = instanceId;
            return this;
        }

        /**
         * Sets the type of the Redis instance this instance connects to.
         *
         * @param redisType The Redis type to set
         */
        public Builder<R, C> withRedisType(RedisType redisType) {
            this.redisType = redisType;
            return this;
        }

        /**
         * Sets the Redis URI for which the connector has been created.
         *
         * @param redisURI The Redis URI to set
         */
        public Builder<R, C> withRedisURI(RedisURI redisURI) {
            this.redisURI = redisURI;
            return this;
        }

        /**
         * Sets the Redis client.
         *
         * @param redisClient The Redis client to set
         */
        public Builder<R, C> withRedisClient(R redisClient) {
            this.redisClient = redisClient;
            return this;
        }

        /**
         * Sets the connection pool supplier.
         *
         * @param poolSupplier The supplier to set
         */
        public Builder<R, C> withPoolSupplier(Supplier<GenericObjectPool<C>> poolSupplier) {
            this.poolSupplier = poolSupplier;
            return this;
        }

        /**
         * Sets the supplier for a new connection to Redis storage.
         *
         * @param connectionSupplier The supplier to set
         */
        public Builder<R, C> withConnectionSupplier(Supplier<C> connectionSupplier) {
            this.connectionSupplier = connectionSupplier;
            return this;
        }

        /**
         * Sets the circuit breaker or <code>null</code>.
         *
         * @param optCircuitBreaker The circuit breaker to set
         */
        public Builder<R, C> withCircuitBreaker(CircuitBreaker optCircuitBreaker) {
            this.optCircuitBreaker = optCircuitBreaker;
            return this;
        }

        /**
         * Sets the threshold (in milliseconds) for the execution duration for a Redis operation.
         *
         * @param operationExecutionDurationThreshold The threshold to set
         */
        public Builder<R, C> withOperationExecutionDurationThreshold(long operationExecutionDurationThreshold) {
            this.operationExecutionDurationThreshold = operationExecutionDurationThreshold;
            return this;
        }

        /**
         * Sets whether to establish a new connection if waiting for an available connection in pool is exceeded.
         *
         * @param newConnectionIfWaitExceeded <code>true</code> to establish a new connection if waiting for an available connection in pool is exceeded; otherwise <code>false</code>
         */
        public Builder<R, C> withNewConnectionIfWaitExceeded(boolean newConnectionIfWaitExceeded) {
            this.newConnectionIfWaitExceeded = newConnectionIfWaitExceeded;
            return this;
        }

        /**
         * Builds the instance of <code>RedisConnectorArgs</code> from this builder's arguments.
         *
         * @return The <code>RedisConnectorArgs</code> instance
         */
        public RedisConnectorArgs<R, C> build() {
            return new RedisConnectorArgs<R, C>(this);
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final String instanceId;
    private final RedisType redisType;
    private final RedisURI redisURI;
    private final R redisClient;
    private final Supplier<GenericObjectPool<C>> poolSupplier;
    private final Supplier<C> connectionSupplier;
    private final CircuitBreaker optCircuitBreaker;
    private final long operationExecutionDurationThreshold;
    private final boolean newConnectionIfWaitExceeded;

    /**
     * Initializes a new instance of {@link RedisConnectorArgs}.
     *
     * @param builder The builder from which to obtain arguments
     * @param services The service look-up
     */
    private RedisConnectorArgs(Builder<R, C> builder) {
        super();
        this.instanceId = builder.instanceId;
        this.redisType = builder.redisType;
        this.redisURI = builder.redisURI;
        this.redisClient = builder.redisClient;
        this.poolSupplier = builder.poolSupplier;
        this.connectionSupplier = builder.connectionSupplier;
        this.optCircuitBreaker = builder.optCircuitBreaker;
        this.operationExecutionDurationThreshold = builder.operationExecutionDurationThreshold;
        this.newConnectionIfWaitExceeded = builder.newConnectionIfWaitExceeded;
    }

    /**
     * Gets the identifier representing the Redis instance (e.g. remote site or cache) this instance connects to, or <code>null</code> if regular.
     *
     * @return The instance identifier
     */
    public String getInstanceId() {
        return instanceId;
    }

    /**
     * Gets the type of the Redis instance this instance connects to.
     *
     * @return The redis type
     */
    public RedisType getRedisType() {
        return redisType;
    }

    /**
     * Gets the Redis URI for which the connector has been created.
     *
     * @return The Redis URI
     */
    public RedisURI getRedisURI() {
        return redisURI;
    }

    /**
     * Gets the Redis client.
     *
     * @return The Redis client
     */
    public R getRedisClient() {
        return redisClient;
    }

    /**
     * Gets the connection pool supplier.
     *
     * @return The supplier
     */
    public Supplier<GenericObjectPool<C>> getPoolSupplier() {
        return poolSupplier;
    }

    /**
     * Gets the supplier for a new connection to Redis storage.
     *
     * @return The supplier
     */
    public Supplier<C> getConnectionSupplier() {
        return connectionSupplier;
    }

    /**
     * Gets the optional circuit breaker.
     *
     * @return The circuit breaker or empty
     */
    public Optional<CircuitBreaker> optCircuitBreaker() {
        return Optional.ofNullable(optCircuitBreaker);
    }

    /**
     * Gets the threshold (in milliseconds) for the execution duration for a Redis operation.
     *
     * @return The threshold
     */
    public long getOperationExecutionDurationThreshold() {
        return operationExecutionDurationThreshold;
    }

    /**
     * Checks whether to establish a new connection if waiting for an available connection in pool is exceeded.
     *
     * @return <code>true</code> to establish a new connection if waiting for an available connection in pool is exceeded; otherwise <code>false</code>
     */
    public boolean isNewConnectionIfWaitExceeded() {
        return newConnectionIfWaitExceeded;
    }

}
