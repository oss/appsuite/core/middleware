/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.redis.internal;

import com.openexchange.config.lean.Property;


/**
 * {@link SpecialInstanceRedisPropertyProvider}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class SpecialInstanceRedisPropertyProvider implements RedisPropertyProvider {

    private static final SpecialInstanceRedisPropertyProvider INSTANCE = new SpecialInstanceRedisPropertyProvider();

    /**
     * Gets the instance
     *
     * @return The instance
     */
    public static SpecialInstanceRedisPropertyProvider getInstance() {
        return INSTANCE;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Initializes a new {@link SpecialInstanceRedisPropertyProvider}.
     */
    private SpecialInstanceRedisPropertyProvider() {
        super();
    }

    @Override
    public Property getMode() {
        return SpecialInstanceRedisProperty.MODE;
    }

    @Override
    public Property getHosts() {
        return SpecialInstanceRedisProperty.HOSTS;
    }

    @Override
    public Property getSentinelMasterId() {
        return SpecialInstanceRedisProperty.SENTINEL_MASTER_ID;
    }

    @Override
    public Property getSentinelMasterReplica() {
        return SpecialInstanceRedisProperty.SENTINEL_MASTER_REPLICA;
    }

    @Override
    public Property getUserName() {
        return SpecialInstanceRedisProperty.USER_NAME;
    }

    @Override
    public Property getPassword() {
        return SpecialInstanceRedisProperty.PASSWORD;
    }

    @Override
    public Property getSsl() {
        return SpecialInstanceRedisProperty.SSL;
    }

    @Override
    public Property getStartTls() {
        return SpecialInstanceRedisProperty.STARTTLS;
    }

    @Override
    public Property getCompressionType() {
        return SpecialInstanceRedisProperty.COMPRESSION_TYPE;
    }

    @Override
    public Property getMinimumCompressionSize() {
        return SpecialInstanceRedisProperty.MINIMUM_COMPRESSION_SIZE;
    }

    @Override
    public Property getOperationExecutionDurationThreshold() {
        return SpecialInstanceRedisProperty.OPERATION_EXECUTION_DURATION_THRESHOLD;
    }

    @Override
    public Property getConnectionPoolNewConnectionIfWaitExceeded() {
        return SpecialInstanceRedisProperty.POOL_NEW_CONNECTION_IF_WAIT_EXCEEDED;
    }

    @Override
    public Property getVerifyPeer() {
        return SpecialInstanceRedisProperty.VERIFY_PEER;
    }

    @Override
    public Property getDatabase() {
        return SpecialInstanceRedisProperty.DATABASE;
    }

    @Override
    public Property getLatencyMetrics() {
        return SpecialInstanceRedisProperty.LETTUCE_COMMAND_LATENCY_METRICS;
    }

    @Override
    public Property getCommandTimeoutMillis() {
        return SpecialInstanceRedisProperty.COMMAND_TIMEOUT_MILLIS;
    }

    @Override
    public Property getConnectTimeoutMillis() {
        return SpecialInstanceRedisProperty.CONNECT_TIMEOUT_MILLIS;
    }

    @Override
    public Property getClusterTopologyPeriodicRefreshMillis() {
        return SpecialInstanceRedisProperty.CLUSTER_TOPOLOGY_PERIODIC_REFRESH_MILLIS;
    }

    @Override
    public Property getConnectionPoolMaxTotal() {
        return SpecialInstanceRedisProperty.POOL_MAX_TOTAL;
    }

    @Override
    public Property getConnectionPoolMaxIdle() {
        return SpecialInstanceRedisProperty.POOL_MAX_IDLE;
    }

    @Override
    public Property getConnectionPoolMinIdle() {
        return SpecialInstanceRedisProperty.POOL_MIN_IDLE;
    }

    @Override
    public Property getConnectionPoolMaxWaitSeconds() {
        return SpecialInstanceRedisProperty.POOL_MAX_WAIT_SECONDS;
    }

    @Override
    public Property getConnectionPoolMinIdleSeconds() {
        return SpecialInstanceRedisProperty.POOL_MIN_IDLE_SECONDS;
    }

    @Override
    public Property getConnectionPoolCleanerRunSeconds() {
        return SpecialInstanceRedisProperty.POOL_CLEANER_RUN_SECONDS;
    }

    @Override
    public Property getCircuitBreakerEnabled() {
        return SpecialInstanceRedisProperty.CIRCUIT_BREAKER_ENABLED;
    }

    @Override
    public Property getCircuitBreakerFailureThreshold() {
        return SpecialInstanceRedisProperty.CIRCUIT_BREAKER_FAILURE_THRESHOLD;
    }

    @Override
    public Property getCircuitBreakerFailureExecutions() {
        return SpecialInstanceRedisProperty.CIRCUIT_BREAKER_FAILURE_EXECUTIONS;
    }

    @Override
    public Property getCircuitBreakerSuccessThreshold() {
        return SpecialInstanceRedisProperty.CIRCUIT_BREAKER_SUCCESS_THRESHOLD;
    }

    @Override
    public Property getCircuitBreakerSuccessExecutions() {
        return SpecialInstanceRedisProperty.CIRCUIT_BREAKER_SUCCESS_EXECUTIONS;
    }

    @Override
    public Property getCircuitBreakerDelayMillis() {
        return SpecialInstanceRedisProperty.CIRCUIT_BREAKER_DELAY_MILLIS;
    }

}
