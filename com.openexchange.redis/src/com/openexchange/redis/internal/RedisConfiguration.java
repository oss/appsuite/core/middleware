/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.redis.internal;

import java.util.Collections;
import java.util.Map;
import org.slf4j.Logger;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.java.Strings;
import com.openexchange.redis.RedisType;
import com.openexchange.redis.compression.CompressionType;

/**
 * {@link RedisConfiguration} - Provides access to Redis properties.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RedisConfiguration {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(RedisConfiguration.class);
    }

    private final RedisPropertyProvider propertyProvider;
    private final LeanConfigurationService configService;
    private final Map<String, String> optionals;
    private final String instanceId;
    private final RedisType redisType;

    /**
     * Initializes a new {@link RedisConfiguration}.
     *
     * @param propertyProvider The underlying property provider
     * @param configService A reference to the configuration service
     */
    public RedisConfiguration(RedisPropertyProvider propertyProvider, LeanConfigurationService configService) {
        this(null, RedisType.REGULAR, propertyProvider, configService);
    }

    /**
     * Initializes a new {@link RedisConfiguration}.
     *
     * @param instanceId The identifier representing the Redis instance (e.g. remote site or cache) this configuration refers to, or <code>null</code> if regular
     * @param redisType The type of the Redis instance this instance connects to
     * @param propertyProvider The underlying property provider
     * @param configService A reference to the configuration service
     */
    public RedisConfiguration(String instanceId, RedisType redisType, RedisPropertyProvider propertyProvider, LeanConfigurationService configService) {
        super();
        this.instanceId = instanceId;
        this.redisType = redisType;
        optionals = Strings.isEmpty(instanceId) ? Collections.emptyMap() : Collections.singletonMap(SpecialInstanceRedisProperty.QUALIFIER_INSTANCE_ID, instanceId);
        this.propertyProvider = propertyProvider;
        this.configService = configService;
    }

    /**
     * Gets the identifier representing the Redis instance (e.g. remote site or cache) this configuration refers to.
     *
     * @return The instance identifier or <code>null</code> if regular
     */
    public String getInstanceId() {
        return instanceId;
    }

    /**
     * Gets the type of the Redis instance this instance connects to.
     *
     * @return The redis type
     */
    public RedisType getRedisType() {
        return redisType;
    }

    /**
     * Gets the mode.
     *
     * @return The mode
     */
    public String getMode() {
        return configService.getProperty(propertyProvider.getMode(), optionals);
    }

    /**
     * Gets the hosts.
     *
     * @return The hosts
     */
    public String getHosts() {
        return configService.getProperty(propertyProvider.getHosts(), optionals);
    }

    /**
     * Gets the sentinel master identifier.
     *
     * @return The sentinel master identifier
     */
    public String getSentinelMasterId() {
        return configService.getProperty(propertyProvider.getSentinelMasterId(), optionals);
    }

    /**
     * Checks whether to establish a connection to sentinel w/ or w/o master-replica awareness.
     *
     * @return The master-replica flag
     */
    public boolean getSentinelMasterReplica() {
        return configService.getBooleanProperty(propertyProvider.getSentinelMasterReplica(), optionals);
    }

    /**
     * Gets the user name.
     *
     * @return The user name
     */
    public String getUserName() {
        return configService.getProperty(propertyProvider.getUserName(), optionals);
    }

    /**
     * Gets the password property.
     *
     * @return The password property
     */
    public String getPassword() {
        return configService.getProperty(propertyProvider.getPassword(), optionals);
    }

    /**
     * Gets the SSL flag.
     *
     * @return The SSL flag
     */
    public boolean getSsl() {
        return configService.getBooleanProperty(propertyProvider.getSsl(), optionals);
    }

    /**
     * Gets the STARTTLS flag.
     *
     * @return The STARTTLS flag
     */
    public boolean getStartTls() {
        return configService.getBooleanProperty(propertyProvider.getStartTls(), optionals);
    }

    /**
     * Gets the compression type.
     *
     * @return The compression type
     */
    public CompressionType getCompressionType() {
        String sCompressionType = configService.getProperty(propertyProvider.getCompressionType(), optionals);
        CompressionType compressionType = CompressionType.compressionTypeFor(sCompressionType);
        if (compressionType != null) {
            return compressionType;
        }
        LoggerHolder.LOG.warn("No such compression type for configured value: {}. Using {} as fall-back", sCompressionType, CompressionType.NONE.getIdentifier());
        return CompressionType.NONE;
    }

    /**
     * Gets the minimum size for a data chunk in bytes for being considered for compression.
     *
     * @return The minimum compression size or <code>0</code> (zero) to compress all data regardless of size
     */
    public long getMinimumCompressionSize() {
        return configService.getLongProperty(propertyProvider.getMinimumCompressionSize(), optionals);
    }

    /**
     * Gets the threshold (in milliseconds) for the execution duration for a Redis operation.
     * <p>
     * If that threshold is exceeded a warning is written to log informing that a certain Redis operation took too long.
     * <p>
     * A threshold of equal to/less than <code>0</code> (zero) disables tracking of execution durations and effectively suppresses such warnings.
     *
     * @return The threshold in milliseconds for a operation execution duration
     */
    public long getOperationExecutionDurationThreshold() {
        return configService.getLongProperty(propertyProvider.getOperationExecutionDurationThreshold(), optionals);
    }

    /**
     * Gets the verify peer flag.
     *
     * @return The verify peer flag
     */
    public boolean getVerifyPeer() {
        return configService.getBooleanProperty(propertyProvider.getVerifyPeer(), optionals);
    }

    /**
     * Gets the database.
     *
     * @return The database
     */
    public int getDatabase() {
        return configService.getIntProperty(propertyProvider.getDatabase(), optionals);
    }

    /**
     * Gets the latency metrics flag.
     *
     * @return The latency metrics flag
     */
    public boolean getLatencyMetrics() {
        return configService.getBooleanProperty(propertyProvider.getLatencyMetrics(), optionals);
    }

    /**
     * Gets the command timeout.
     *
     * @return The command timeout
     */
    public long getCommandTimeoutMillis() {
        return configService.getLongProperty(propertyProvider.getCommandTimeoutMillis(), optionals);
    }

    /**
     * Gets the connect timeout.
     *
     * @return The connect timeout
     */
    public long getConnectTimeoutMillis() {
        return configService.getLongProperty(propertyProvider.getConnectTimeoutMillis(), optionals);
    }

    /**
     * Gets the interval for the periodic topology refresh in milliseconds.
     *
     * @return The interval for the periodic topology refresh
     */
    public long getClusterTopologyPeriodicRefreshMillis() {
        return configService.getLongProperty(propertyProvider.getClusterTopologyPeriodicRefreshMillis(), optionals);
    }

    // ------------------------------------------------------------------------------------------

    /**
     * Checks whether to establish a new connection if waiting for an available connection in pool is exceeded.
     *
     * @return <code>true</code> to establish a new connection if waiting for an available connection in pool is exceeded; otherwise <code>false</code>
     */
    public boolean isConnectionPoolNewConnectionIfWaitExceeded() {
        return configService.getBooleanProperty(propertyProvider.getConnectionPoolNewConnectionIfWaitExceeded(), optionals);
    }

    /**
     * Gets the capability on the number of objects that can be allocated by the connection pool (checked out to clients, or idle
     * awaiting checkout) at a given time.
     *
     * @return The max. number of pooled connections
     */
    public int getConnectionPoolMaxTotal() {
        return configService.getIntProperty(propertyProvider.getConnectionPoolMaxTotal(), optionals);
    }

    /**
     * Gets the capability on the number of "idle" instances in the connection pool.
     *
     * @return The max. number of idle connections
     */
    public int getConnectionPoolMaxIdle() {
        return configService.getIntProperty(propertyProvider.getConnectionPoolMaxIdle(), optionals);
    }

    /**
     * Gets the target for the minimum number of idle objects to maintain in the pool.
     *
     * @return The min. number of idle connections
     */
    public int getConnectionPoolMinIdle() {
        return configService.getIntProperty(propertyProvider.getConnectionPoolMinIdle(), optionals);
    }

    /**
     * Gets the maximum duration a borrowing caller should be blocked before throwing an exception when the pool is exhausted.
     *
     * @return The max. wait seconds
     */
    public int getConnectionPoolMaxWaitSeconds() {
        return configService.getIntProperty(propertyProvider.getConnectionPoolMaxWaitSeconds(), optionals);
    }

    /**
     * Gets the minimum amount of time an object may sit idle in the pool before it is eligible for eviction.
     *
     * @return The idle seconds
     */
    public int getConnectionPoolMinIdleSeconds() {
        return configService.getIntProperty(propertyProvider.getConnectionPoolMinIdleSeconds(), optionals);
    }

    public int getConnectionPoolCleanerRunSeconds() {
        return configService.getIntProperty(propertyProvider.getConnectionPoolCleanerRunSeconds(), optionals);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Gets the flag to enable/disable the Redis circuit breaker.
     *
     * @return The flag
     */
    public boolean getCircuitBreakerEnabled() {
        return configService.getBooleanProperty(propertyProvider.getCircuitBreakerEnabled(), optionals);
    }

    /**
     * Gets the failure threshold; which is the number of successive failures that must occur in order to open the circuit.
     *
     * @return The threshold
     */
    public int getCircuitBreakerFailureThreshold() {
        return configService.getIntProperty(propertyProvider.getCircuitBreakerFailureThreshold(), optionals);
    }

    /**
     * Gets the number of executions to measure the failures against
     *
     * @return The number
     */
    public int getCircuitBreakerFailureExecutions() {
        return configService.getIntProperty(propertyProvider.getCircuitBreakerFailureExecutions(), optionals);
    }

    /**
     * Gets the success threshold; which is the number of successive successful executions that must occur when in a half-open state in order to
     * close the circuit.
     *
     * @return The threshold
     */
    public int getCircuitBreakerSuccessThreshold() {
        return configService.getIntProperty(propertyProvider.getCircuitBreakerSuccessThreshold(), optionals);
    }

    /**
     * Gets the number of executions to measure the successes against.
     *
     * @return The number
     */
    public int getCircuitBreakerSuccessExecutions() {
        return configService.getIntProperty(propertyProvider.getCircuitBreakerSuccessExecutions(), optionals);
    }

    /**
     * Gets the delay in milliseconds; the number of milliseconds to wait in open state before transitioning to half-open.
     *
     * @return The delay in milliseconds
     */
    public long getCircuitBreakerDelayMillis() {
        return configService.getLongProperty(propertyProvider.getCircuitBreakerDelayMillis(), optionals);
    }
}
