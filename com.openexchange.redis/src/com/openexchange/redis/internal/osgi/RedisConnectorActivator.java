/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.redis.internal.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.health.MWHealthCheck;
import com.openexchange.log.DurationOutputter;
import com.openexchange.net.ssl.SSLSocketFactoryProvider;
import com.openexchange.net.ssl.config.SSLConfigurationService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.redis.RedisConnectorService;
import com.openexchange.redis.RedisType;
import com.openexchange.redis.internal.AbstractRedisConnector;
import com.openexchange.redis.internal.RedisConnectorServiceImpl;
import com.openexchange.redis.internal.RedisHealthCheck;
import com.openexchange.redis.internal.metrics.RedisPoolMetricHandler;
import com.openexchange.redis.internal.rest.RedisConnectorRESTService;
import com.openexchange.version.VersionService;

/**
 * {@link RedisConnectorActivator}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 *
 */
public class RedisConnectorActivator extends HousekeepingActivator {

    private RedisConnectorServiceImpl connectorService;

    /**
     * Initializes a new {@link RedisConnectorActivator}.
     */
    public RedisConnectorActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { LeanConfigurationService.class, VersionService.class, SSLSocketFactoryProvider.class, SSLConfigurationService.class };
    }

    @Override
    protected synchronized void startBundle() throws Exception {
        Logger logger = LoggerFactory.getLogger(RedisConnectorActivator.class);
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());

        RedisConnectorServiceImpl connectorService = null;
        try {
            long start = System.nanoTime();

            // Register services
            connectorService = new RedisConnectorServiceImpl(this);
            registerService(RedisConnectorService.class, connectorService);
            registerService(MWHealthCheck.class, new RedisHealthCheck(connectorService.getConnectorProvider().getConnector()));
            if (connectorService.supports(RedisType.CACHE)) {
                registerService(MWHealthCheck.class, new RedisHealthCheck(connectorService.getCacheConnectorProvider().get().getConnector()));
            }

            registerService(RedisConnectorRESTService.class, new RedisConnectorRESTService((AbstractRedisConnector<?, ?>) connectorService.getConnectorProvider().getConnector(), this));

            logger.info("Bundle {} started successfully after {}msec.", context.getBundle().getSymbolicName(), DurationOutputter.startNanos(start));
            this.connectorService = connectorService;
            connectorService = null;
        } catch (OXException e) {
            logger.error("Failed to initialize Redis connector", e);
        } catch (Exception e) {
            logger.error("Error starting bundle {}", context.getBundle().getSymbolicName(), e);
            throw e;
        } finally {
            if (connectorService != null) {
                // Exception path...
                cleanUp();
                connectorService.shutdown();
            }
        }
    }

    @Override
    protected synchronized void stopBundle() throws Exception {
        Logger logger = LoggerFactory.getLogger(RedisConnectorActivator.class);
        try {
            logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
            super.stopBundle();
            RedisConnectorServiceImpl connectorService = this.connectorService;
            if (null != connectorService) {
                this.connectorService = null;
                connectorService.shutdown();
            }
            RedisPoolMetricHandler.stop();
            logger.info("Bundle {} stopped successfully.", context.getBundle().getSymbolicName());
        } catch (Exception e) {
            logger.error("Error stopping bundle {}", context.getBundle().getSymbolicName(), e);
            throw e;
        }
    }

}
