/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.redis.internal.rest;

import java.util.function.Supplier;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.ImmutableJSONObject;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.redis.internal.AbstractRedisConnector;
import com.openexchange.rest.services.CommonMediaType;
import com.openexchange.rest.services.JAXRSService;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.server.ServiceLookup;

/**
 * {@link RedisConnectorRESTService}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@RoleAllowed(Role.MASTER_ADMIN_AUTHENTICATED)
@Path("/admin/v1/redis-connector")
public class RedisConnectorRESTService extends JAXRSService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisConnectorRESTService.class);

    private final AbstractRedisConnector<?, ?> service;
    private final JSONObject success;

    /**
     * Initializes a new {@link RedisConnectorRESTService}.
     *
     * @param service The Redis session service
     * @param services The {@link ServiceLookup} instance
     */
    public RedisConnectorRESTService(AbstractRedisConnector<?, ?> service, ServiceLookup services) {
        super(services);
        this.service = service;
        this.success = ImmutableJSONObject.immutableFor(new JSONObject(2).putSafe("success", Boolean.TRUE));
    }

    /**
     * Re-starts the connection pool of the Open-Xchange Redis connector.
     *
     * @param global whether to perform a cluster-wide or local clean-up. Defaults to <code>true</code>
     * @param payload the payload containing the session identifiers
     * @return
     *         <ul>
     *         <li><b>200</b>: if the connection pool has been restarted successfully</li>
     *         <li><b>400</b>: if the client issued a bad request</li>
     *         <li><b>401</b>: if the client was not authenticated</li>
     *         <li><b>403</b>: if the client was not authorised</li>
     *         <li><b>500</b>: if any server side error is occurred</li>
     *         </ul>
     */
    @GET
    @Path("/restart-connection-pool")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({ MediaType.APPLICATION_JSON, CommonMediaType.APPLICATION_PROBLEM_JSON })
    public Response restartConnectionPool() {
        return perform(this::restartPool);
    }

    /**
     * Re-starts the connection pool of the Open-Xchange Redis connector.
     *
     * @return A <code>200 OK</code> HTTP response
     */
    private Response restartPool() {
        service.recreatePool();
        return Response.ok(success, MediaType.APPLICATION_JSON).build();
    }

    /////////////////////////////////////////// HELPERS /////////////////////////////////////////////

    /**
     * Performs the action
     *
     * @param supplier The {@link Supplier} to perform the action
     * @return
     *         <ul>
     *         <li><b>200</b>: if the action was performed successfully</li>
     *         <li><b>400</b>: if the client issued a bad request</li>
     *         <li><b>401</b>: if the client was not authenticated</li>
     *         <li><b>403</b>: if the client was not authorised</li>
     *         <li><b>500</b>: if any server side error is occurred</li>
     *         </ul>
     */
    private static Response perform(Supplier<Response> supplier) {
        try {
            return supplier.get();
        } catch (IllegalArgumentException e) {
            LOGGER.debug("", e);
            return Response.status(400).type(CommonMediaType.APPLICATION_PROBLEM_JSON_TYPE).entity(parse(e, 400)).build();
        } catch (Exception e) {
            LOGGER.debug("", e);
            return Response.status(500).build();
        }
    }

    /**
     * Parses the specified {@link Exception} to a {@link JSONObject} that conforms with the
     * <code>RFC-7807</code>.
     *
     * @param e The {@link Exception} to parse
     * @return The {@link JSONObject} with the exception
     * @see <a href="https://tools.ietf.org/html/rfc7807">RFC-7807</a>
     */
    private static JSONObject parse(Exception e, int statusCode) {
        try {
            // At the moment we lack proper documentation and/or code logic
            // to either include the 'type', 'instance' or the 'detail' fields.
            // - For the 'type' and 'instance' fields the documentation framework
            // needs to be adjusted in order to consider and include generic models
            // for the problem types and publish them at doc.ox.com.
            // - For the 'detail' field maybe the display message of the OXException ought
            // to do it, though for other non-OXExceptions there isn't much that
            // can be done other than explicitly analysing them or assigning them specific extra
            // details regarding their error type: e.g.
            //   * for IOExceptions:   'An I/O error was occurred. That's all we know'
            //   * for JSONExceptions: 'A JSON error was occurred due to xyz'
            //   * etc.
            JSONObject j = new JSONObject(4);
            j.put("title", e.getMessage());
            j.put("status", statusCode);
            return j;
        } catch (JSONException x) {
            LOGGER.error("", e);
            return JSONObject.EMPTY_OBJECT;
        }
    }

}
