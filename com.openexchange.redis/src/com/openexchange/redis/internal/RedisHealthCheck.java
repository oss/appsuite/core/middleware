/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.redis.internal;

import java.util.Map;
import org.slf4j.Logger;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.health.DefaultMWHealthCheckResponse;
import com.openexchange.health.MWHealthCheck;
import com.openexchange.health.MWHealthCheckResponse;
import com.openexchange.java.Strings;
import com.openexchange.redis.RedisConnector;

/**
 * {@link RedisHealthCheck} - Performs the health check against Redis end-point.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class RedisHealthCheck implements MWHealthCheck {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(RedisHealthCheck.class);
    }

    private final RedisConnector redisConnector;
    private final String name;
    private final Map<String, Object> data;

    /**
     * Initializes a new {@link RedisHealthCheck}.
     *
     * @param redisConnector The connector
     * @param name The health check name
     */
    private RedisHealthCheck(RedisConnector redisConnector, String name) {
        super();
        this.redisConnector = redisConnector;
        this.name = name;
        data = Map.of("connector", String.valueOf(redisConnector), "mode", String.valueOf(redisConnector.getMode()), "type", String.valueOf(redisConnector.getRedisType()));
    }   

    /**
     * Initializes a new {@link RedisHealthCheck}.
     *
     * @param redisConnector The connector
     */
    public RedisHealthCheck(RedisConnector redisConnector) {
        this(redisConnector, Strings.isEmpty(redisConnector.getInstanceId()) ? "redis" : "redis." + redisConnector.getInstanceId());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public MWHealthCheckResponse call() {
        try {
            Boolean alive = redisConnector.executeOperation(RedisPingOperation.getInstance());
            return new DefaultMWHealthCheckResponse(getName(), data, alive.booleanValue());
        } catch (Exception e) {
            if (ExceptionUtils.isEitherOf(e, net.jodah.failsafe.CircuitBreakerOpenException.class)) {
                // Failed due to open circuit breaker. Assume alive then...
                return new DefaultMWHealthCheckResponse(getName(), data, true);
            }
            LoggerHolder.LOG.warn("Failed PING command to check Redis still alive", e);
            return new DefaultMWHealthCheckResponse(getName(), data, false);
        }
    }

}
