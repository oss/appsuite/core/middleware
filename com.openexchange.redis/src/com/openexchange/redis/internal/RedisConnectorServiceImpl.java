/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.redis.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReference;
import org.slf4j.Logger;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXRuntimeException;
import com.openexchange.java.Strings;
import com.openexchange.redis.OperationMode;
import com.openexchange.redis.RedisConnectorProvider;
import com.openexchange.redis.RedisConnectorService;
import com.openexchange.redis.RedisExceptionCode;
import com.openexchange.redis.RedisType;
import com.openexchange.redis.internal.cluster.RedisClusterConnector;
import com.openexchange.redis.internal.sentinel.RedisSentinelConnector;
import com.openexchange.redis.internal.standalone.RedisStandAloneConnector;
import com.openexchange.server.ServiceLookup;

/**
 * {@link RedisConnectorServiceImpl} - The implementation for Redis connector service.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RedisConnectorServiceImpl implements RedisConnectorService {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(RedisConnectorServiceImpl.class); // NOSONARLINT
    }

    private final ServiceLookup services;
    private final AtomicReference<Future<RedisConnectorProviderImpl>> connectorProvider;
    private final AtomicReference<Future<RedisConnectorProviderImpl>> cacheConnectorProvider;
    private final AtomicReference<Future<List<RedisConnectorProvider>>> remoteConnectorProvider;

    /**
     * Initializes a new {@link RedisConnectorServiceImpl}.
     *
     * @param services The service look-up
     */
    public RedisConnectorServiceImpl(ServiceLookup services) {
        super();
        this.services = services;
        this.connectorProvider = new AtomicReference<>();
        this.cacheConnectorProvider = new AtomicReference<>();
        this.remoteConnectorProvider = new AtomicReference<>();
    }

    @Override
    public boolean supports(RedisType redisType) throws OXException {
        if (redisType == null) {
            throw new IllegalArgumentException("Redis type must not be null");
        }
        switch (redisType) {
            case CACHE:
                {
                    Future<RedisConnectorProviderImpl> future = cacheConnectorProvider.get();
                    if (future != null) {
                        // Cache connector provider has already been invoked
                        return getFrom(future) != null;
                    }
                    // Check configuration as fallback
                    LeanConfigurationService configService = services.getServiceSafe(LeanConfigurationService.class);
                    return configService.getBooleanProperty(RedisProperty.CACHE_ENABLED);
                }
            case REGULAR:
                return true;
            case REMOTE:
                {
                    Future<List<RedisConnectorProvider>> future = remoteConnectorProvider.get();
                    if (future != null) {
                        // Remote connector providers have already been invoked
                        List<RedisConnectorProvider> providers = getFrom(future);
                        return providers != null && !providers.isEmpty();
                    }
                    // Check configuration as fallback: Remote sites enabled AND remote sites' identifiers specified through configuration
                    LeanConfigurationService configService = services.getServiceSafe(LeanConfigurationService.class);
                    return (configService.getBooleanProperty(RedisProperty.SITES_ENABLED) && !Strings.isEmpty(configService.getProperty(RedisProperty.SITES)));
                }
            default:
                throw new IllegalArgumentException("Unknown redis type: " + redisType);
        }
    }

    /**
     * Shuts down any previously acquired connectors.
     */
    public void shutdown() {
        shutdownQuietly(unsetReference(connectorProvider, null));
        shutdownQuietly(unsetReference(cacheConnectorProvider, null));
        for (RedisConnectorProvider remoteProvider : unsetReference(remoteConnectorProvider, Collections.emptyList())) {
            shutdownQuietly((RedisConnectorProviderImpl) remoteProvider);
        }
    }

    // ------------------------------------------ Regular connector ------------------------------------------------------------------------

    @Override
    public RedisConnectorProvider getConnectorProvider() throws OXException {
        Future<RedisConnectorProviderImpl> provider = connectorProvider.get();
        if (provider == null) {
            FutureTask<RedisConnectorProviderImpl> ft = new FutureTask<>(() -> initConnectorProvider());
            provider = connectorProvider.compareAndExchange(null, ft);
            if (provider == null) {
                ft.run();
                // Known to be computed. Fast return if successful...
                try {
                    return ft.resultNow();
                } catch (Exception e) {
                    // Ignore
                    LoggerHolder.LOG.debug("Failed to obtain Redis connector provider", e);
                }
                provider = ft;
            }
        }
        return getFrom(provider);
    }

    /**
     * Initializes the local regular Redis connector provider.
     *
     * @return The initialized Redis connector provider
     * @throws OXRuntimeException If initialization fails
     */
    private RedisConnectorProviderImpl initConnectorProvider() {
        try {
            LeanConfigurationService configService = services.getServiceSafe(LeanConfigurationService.class);
            return initConnectorProvider(new RedisConfiguration(BaseRedisPropertyProvider.getInstance(), configService));
        } catch (OXException e) {
            throw new OXRuntimeException(e);
        }
    }

    // ------------------------------------------ Cache connector --------------------------------------------------------------------------

    @Override
    public Optional<RedisConnectorProvider> getCacheConnectorProvider() throws OXException {
        Future<RedisConnectorProviderImpl> provider = cacheConnectorProvider.get();
        if (provider == null) {
            FutureTask<RedisConnectorProviderImpl> ft = new FutureTask<>(() -> initCacheConnectorProvider());
            provider = cacheConnectorProvider.compareAndExchange(null, ft);
            if (provider == null) {
                ft.run();
                // Known to be computed. Fast return if successful...
                try {
                    return Optional.ofNullable(ft.resultNow());
                } catch (Exception e) {
                    // Ignore
                    LoggerHolder.LOG.debug("Failed to obtain Redis cache connector provider", e);
                }
                provider = ft;
            }
        }
        return Optional.ofNullable(getFrom(provider));
    }

    /**
     * Initializes the special local Redis connector provider for volatile cache data.
     *
     * @return The initialized Redis connector provider or <code>null</code>
     * @throws OXRuntimeException If initialization fails
     */
    private RedisConnectorProviderImpl initCacheConnectorProvider() {
        try {
            LeanConfigurationService configService = services.getServiceSafe(LeanConfigurationService.class);
            boolean cacheEnabled = configService.getBooleanProperty(RedisProperty.CACHE_ENABLED);
            if (!cacheEnabled) {
                // Remote site NOT enabled per configuration
                return null;
            }

            return initConnectorProvider(new RedisConfiguration("cache", RedisType.CACHE, SpecialInstanceRedisPropertyProvider.getInstance(), configService));
        } catch (OXException e) {
            throw new OXRuntimeException(e);
        }
    }

    // ------------------------------------------ Remote connectors ------------------------------------------------------------------------

    @Override
    public List<RedisConnectorProvider> getRemoteConnectorProviders() throws OXException {
        Future<List<RedisConnectorProvider>> providers = remoteConnectorProvider.get();
        if (providers == null) {
            FutureTask<List<RedisConnectorProvider>> ft = new FutureTask<>(() -> initRemoteConnectorProviders());
            providers = remoteConnectorProvider.compareAndExchange(null, ft);
            if (providers == null) {
                ft.run();
                // Known to be computed. Fast return if successful...
                try {
                    return ft.resultNow();
                } catch (Exception e) {
                    // Ignore
                    LoggerHolder.LOG.debug("Failed to obtain Redis remote connector providers", e);
                }
                providers = ft;
            }
        }
        return getFrom(providers);
    }

    /**
     * Initializes any configured remote Redis connector providers.
     *
     * @return The initialized remote Redis connector providers, or an empty list if non are configured
     * @throws OXRuntimeException If initialization fails
     */
    private List<RedisConnectorProvider> initRemoteConnectorProviders() {
        List<AbstractRedisConnector<?, ?>> initializedConnectors = new ArrayList<AbstractRedisConnector<?, ?>>();
        try {
            LeanConfigurationService configService = services.getServiceSafe(LeanConfigurationService.class);
            boolean sitesEnabled = configService.getBooleanProperty(RedisProperty.SITES_ENABLED);
            if (!sitesEnabled) {
                // Remote site NOT enabled per configuration
                return Collections.emptyList();
            }

            String sSitesIds = configService.getProperty(RedisProperty.SITES);
            if (Strings.isEmpty(sSitesIds)) {
                // No remote site available in configuration
                return Collections.emptyList();
            }

            String[] sitesIds = Strings.splitByComma(sSitesIds);
            List<RedisConnectorProvider> remoteConnectorProviders = new ArrayList<>(sitesIds.length);
            for (String siteId : sitesIds) {
                RedisConfiguration configuration = new RedisConfiguration(siteId, RedisType.REMOTE, SpecialInstanceRedisPropertyProvider.getInstance(), configService);
                OperationMode operationMode = determineOperationMode(configuration);
                AbstractRedisConnector<?, ?> connector = initConnector(configuration, operationMode);
                initializedConnectors.add(connector);
                remoteConnectorProviders.add(new RedisConnectorProviderImpl(operationMode, connector));
            }
            initializedConnectors = null;
            return remoteConnectorProviders;
        } catch (OXException e) {
            throw new OXRuntimeException(e);
        } finally {
            if (null != initializedConnectors) {
                initializedConnectors.forEach(connector -> shutdownQuietly(connector));
            }
        }
    }

    // ------------------------------------------ Helpers ----------------------------------------------------------------------------------

    /**
     * Initializes a new Redis connector provider with given configuration settings.
     *
     * @param configuration The configuration to use for initialization
     * @return The initialized Redis connector provider
     * @throws OXException If initialization fails
     */
    private RedisConnectorProviderImpl initConnectorProvider(RedisConfiguration configuration) throws OXException {
        OperationMode operationMode = determineOperationMode(configuration);
        AbstractRedisConnector<?, ?> initializedConnector = null;
        try {
            initializedConnector = initConnector(configuration, operationMode);
            RedisConnectorProviderImpl connectorProvider = new RedisConnectorProviderImpl(operationMode, initializedConnector);
            initializedConnector = null;
            return connectorProvider;
        } finally {
            shutdownQuietly(initializedConnector);
        }
    }

    /**
     * Initializes a new Redis connector with given configuration settings.
     *
     * @param configuration The configuration to use for initialization
     * @param operationMode The operation mode
     * @return The initialized Redis connector
     * @throws OXException If initialization fails
     */
    private AbstractRedisConnector<?, ?> initConnector(RedisConfiguration configuration, OperationMode operationMode) throws OXException {
        return switch (operationMode) {
            case CLUSTER -> RedisClusterConnector.newInstance(configuration, services);
            case SENTINEL -> RedisSentinelConnector.newInstance(configuration, services);
            case STAND_ALONE -> RedisStandAloneConnector.newInstance(configuration, services);
            default -> throw OXException.general("Invalid Redis operation mode specified: " + operationMode.getIdentifier());
        };
    }

    /**
     * Determines the configured operation mode for the Redis connector.
     *
     * @param configuration The configuration to read property from
     * @return The operation mode
     * @throws OXException If specified operation mode is invalid/unknown
     */
    private static OperationMode determineOperationMode(RedisConfiguration configuration) throws OXException {
        OperationMode operationMode = OperationMode.operationModeFor(configuration.getMode()).orElse(null);
        if (operationMode == null) {
            throw OXException.general("Invalid Redis operation mode specified: " + configuration.getMode());
        }
        return operationMode;
    }

    /**
     * Shuts down given provider's connector; swallowing any possibly exception.
     *
     * @param connectorProvider The provider whose connector shall be shut down
     */
    private static void shutdownQuietly(RedisConnectorProviderImpl connectorProvider) {
        if (null != connectorProvider) {
            shutdownQuietly(connectorProvider.getConnector());
        }
    }

    /**
     * Shuts down given connector; swallowing any possibly exception.
     *
     * @param connector The connector to shut down
     */
    private static void shutdownQuietly(AbstractRedisConnector<?, ?> connector) {
        if (null != connector) {
            try {
                connector.shutdown();
            } catch (Exception e) {
                org.slf4j.LoggerFactory.getLogger(RedisConnectorServiceImpl.class).warn("Unexpected error shuttong down Redis connector", e);
            }
        }
    }

    /**
     * Gets the value from given <code>Future</code> instance.
     *
     * @param <V> The <code>Future</code>'s result type
     * @param future The future to get from
     * @return The value
     * @throws OXException If value cannot be returned
     */
    private static <V> V getFrom(Future<V> future) throws OXException {
        try {
            return future.get();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw RedisExceptionCode.UNEXPECTED_ERROR.create(e, "Interrupted");
        } catch (CancellationException e) {
            throw RedisExceptionCode.UNEXPECTED_ERROR.create(e, "Canceled");
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof OXRuntimeException) {
                throw ((OXRuntimeException) cause).getException();
            }
            if (cause instanceof OXException) {
                throw (OXException) cause;
            }
            cause = cause == null ? e : cause;
            String message = cause.getMessage();
            throw RedisExceptionCode.UNEXPECTED_ERROR.create(cause, message == null ? "<unknown>" : message);
        }
    }

    /**
     * Unsets the <code>Future</code> instance from given <code>AtomicReference</code> and returns <code>Future</code>'s value (if available).
     *
     * @param <V> The <code>Future</code>'s result type
     * @param reference The reference possibly holding an instance of <code>Future</code>
     * @param def The default value to return on absence or failed retrieval from <code>Future</code> instance
     * @return The <code>Future</code>'s result or given default value
     */
    private static <V> V unsetReference(AtomicReference<Future<V>> reference, V def) {
        Future<V> future = reference.getAndSet(null);
        if (future == null) {
            // Referenced instance of Future already released
            return def;
        }

        // Ensure instance of Future is done; otherwise cancel its computation
        if (!future.isDone()) {
            future.cancel(true);
        }

        // Safely obtain the value from Future instance
        try {
            return getFrom(future);
        } catch (Exception e) {
            LoggerHolder.LOG.debug("Failed to retrieve value from Future. Returning given default value instead: {}", def, e);
            return def;
        }
    }

}
