/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.redis.internal.codecs;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.InflaterInputStream;
import org.xerial.snappy.SnappyCodec;
import org.xerial.snappy.SnappyInputStream;
import org.xerial.snappy.SnappyOutputStream;
import com.openexchange.java.Streams;
import com.openexchange.redis.compression.CompressionType;
import com.openexchange.redis.compression.CompressionTypeProvider;
import com.openexchange.redis.internal.RedisProperty;
import io.lettuce.core.codec.RedisCodec;

/**
 * {@link CompressionRedisCodec} - Wraps a Redis codec and compresses values using specified compression type.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class CompressionRedisCodec implements RedisCodec<String, InputStream> {

    private static final int GZIP_MAGIC_AS_INT = 0x8b1f;

    /**
     * The first byte of Snappy magic bytes: <code>-126</code>
     * @see #SNAPPY_MAGIC
     */
    private static final byte SNAPPY_FIRST_MAGIC = -126;

    /**
     * The first byte of GZIP magic bytes: <code>31</code>
     * @see #GZIP_MAGIC
     */
    private static final byte GZIP_FIRST_MAGIC = (byte) GZIP_MAGIC_AS_INT;

    /**
     * The first byte of Deflate magic bytes: <code>-113</code>
     * @see #DEFLATE_MAGIC
     */
    private static final byte DEFLATE_FIRST_MAGIC = -113;

    /**
     * The Snappy magic bytes, which is the following 8 bytes data:
     * <pre>
     * -126, 'S', 'N', 'A', 'P', 'P', 'Y', 0
     * </pre>
     */
    private static final byte[] SNAPPY_MAGIC = SnappyCodec.getMagicHeader();

    /**
     * The GZIP magic bytes plus compression method, which is the following 3 bytes data:
     * <pre>
     * 31, -117, 8
     * </pre>
     */
    private static final byte[] GZIP_MAGIC = new byte[] { (byte) GZIP_MAGIC_AS_INT, (byte)(GZIP_MAGIC_AS_INT >> 8), Deflater.DEFLATED };

    /**
     * The Deflate magic bytes, which is the following 9 bytes data:
     * <pre>
     * -113, 'D', 'E', 'F', 'L', 'A', 'T', 'E', 0
     * </pre>
     */
    private static final byte[] DEFLATE_MAGIC = new byte[] {-113, 'D', 'E', 'F', 'L', 'A', 'T', 'E', 0};

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** The codec to delegate to */
    private final RedisCodec<String, InputStream> delegate;

    /** The compression type */
    private final CompressionType compressionType;

    /** The minimum compression size or <code>0</code> (zero) to compress all data regardless of size */
    private final long minimumCompressionSize;

    /**
     * Initializes a new {@link CompressionRedisCodec} using {@link ByteArrayRedisCodec} as delegate and default minimum compression size.
     *
     * @param compressionType The compression type to use or <code>null</code> to disable compression
     */
    public CompressionRedisCodec(CompressionType compressionType) {
        this(compressionType, ((Long) RedisProperty.MINIMUM_COMPRESSION_SIZE.getDefaultValue()).longValue());
    }

    /**
     * Initializes a new {@link CompressionRedisCodec} using {@link ByteArrayRedisCodec} as delegate.
     *
     * @param compressionType The compression type to use or <code>null</code> to disable compression
     * @param minimumCompressionSize The minimum compression size or <code>0</code> (zero) to compress all data regardless of size
     */
    public CompressionRedisCodec(CompressionType compressionType, long minimumCompressionSize) {
        this(ByteArrayRedisCodec.getInstance(), compressionType, minimumCompressionSize);
    }

    /**
     * Initializes a new {@link CompressionRedisCodec}.
     *
     * @param delegate The codec to delegate to when reading/writing uncompressed data
     * @param compressionType The compression type to use or <code>null</code> to disable compression
     * @param minimumCompressionSize The minimum compression size or <code>0</code> (zero) to compress all data regardless of size
     */
    public CompressionRedisCodec(RedisCodec<String, InputStream> delegate, CompressionType compressionType, long minimumCompressionSize) {
        super();
        this.compressionType = compressionType == null ? CompressionType.NONE : compressionType;
        this.minimumCompressionSize = minimumCompressionSize <= 0 ? 0L : minimumCompressionSize;
        this.delegate = delegate;
    }

    @Override
    public String decodeKey(ByteBuffer bytes) {
        return delegate.decodeKey(bytes);
    }

    @Override
    public ByteBuffer encodeKey(String key) {
        return delegate.encodeKey(key);
    }

    @Override
    public ByteBuffer encodeValue(InputStream value) {
        CompressionType compressionTypeToExamine = getOrElse(value instanceof CompressionTypeProvider ? ((CompressionTypeProvider) value).getCompressionType() : compressionType, CompressionType.NONE);
        if (compressionTypeToExamine == CompressionType.NONE) {
            return delegate.encodeValue(value);
        }

        try {
            return compress(delegate.encodeValue(value), compressionTypeToExamine, minimumCompressionSize);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private static ByteBuffer compress(ByteBuffer source, CompressionType compressionType, long minimumCompressionSize) throws IOException {
        int remaining = source.remaining();
        if (remaining == 0 || (minimumCompressionSize > 0 && remaining < minimumCompressionSize)) {
            // Don't compress small byte batches
            return source;
        }

        ByteArrayOutputStream outputStream = Streams.newByteArrayOutputStream(remaining / 2);
        if (compressionType == CompressionType.DEFLATE) {
            // Write artificial magic bytes to detect DEFLATE compression when reading
            outputStream.write(DEFLATE_MAGIC);
        }
        OutputStream compressor = null;
        try {
            try (ByteBufferInputStream sourceStream = new ByteBufferInputStream(source)) {
                compressor = switch (compressionType) {
                    case GZIP -> new GZIPOutputStream(outputStream);
                    case SNAPPY -> new SnappyOutputStream(outputStream, 1024);
                    case DEFLATE -> new DeflaterOutputStream(outputStream);
                    case NONE -> throw createUnsupportedCompressionTypeException(compressionType);
                    default -> throw createUnknownCompressionTypeException(compressionType);
                };
                copy(sourceStream, compressor);
            } finally {
                Streams.close(compressor);
            }

            return ByteBuffer.wrap(outputStream.toByteArray());
        } finally {
            outputStream.close();
        }
    }

    @Override
    public InputStream decodeValue(ByteBuffer bytes) {
        int available = bytes.remaining();
        if (available <= 0) {
            // No content
            return delegate.decodeValue(bytes);
        }

        // Detect possible compression through first byte
        CompressionTypeAndMagic detectedType = detectCompressionType(bytes.get());
        if (detectedType == null || available <= detectedType.magicBytes.length) {
            // No compressed content detected or not enough available bytes to read magic
            return delegate.decodeValue(bytes.position(bytes.position() - 1));
        }

        // Check if further bytes do match corresponding magic
        byte[] magic = detectedType.magicBytes;
        int read = 1;
        while (read < magic.length) {
            byte b = bytes.get();
            read++;
            int index = read - 1;
            if (index >= magic.length || b != magic[index]) {
                // No compressed content
                return delegate.decodeValue(bytes.position(bytes.position() - read));
            }
        }

        // Uncompress content and return decoded
        try {
            // Do not reset position if compression type is DEFLATE since its magic bytes are artificially inserted; see #compress()
            return delegate.decodeValue(decompress(detectedType.compressionType == CompressionType.DEFLATE ? bytes : bytes.position(bytes.position() - read), detectedType.compressionType));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private static ByteBuffer decompress(ByteBuffer source, CompressionType compressionType) throws IOException {
        if (source.remaining() == 0) {
            return source;
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(source.remaining() * 2);
        InputStream decompressor = null;
        try {
            try (ByteBufferInputStream sourceStream = new ByteBufferInputStream(source)) {
                decompressor = switch (compressionType) {
                    case SNAPPY -> new SnappyInputStream(sourceStream);
                    case GZIP -> new GZIPInputStream(sourceStream);
                    case DEFLATE -> new InflaterInputStream(sourceStream);
                    case NONE -> throw createUnsupportedCompressionTypeException(compressionType);
                    default -> throw createUnknownCompressionTypeException(compressionType);
                };
                copy(decompressor, outputStream);
            } finally {
                Streams.close(decompressor);
            }

            return ByteBuffer.wrap(outputStream.toByteArray());
        } finally {
            outputStream.close();
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** The constant for Snappy compression type with Snappy magic bytes */
    private static final CompressionTypeAndMagic SNAPPY_COMPRESSION_TYPE_AND_MAGIC = new CompressionTypeAndMagic(CompressionType.SNAPPY, SNAPPY_MAGIC);

    /** The constant for GZIP compression type with GZIP magic bytes */
    private static final CompressionTypeAndMagic GZIP_COMPRESSION_TYPE_AND_MAGIC = new CompressionTypeAndMagic(CompressionType.GZIP, GZIP_MAGIC);

    /** The constant for Deflate compression type with Deflate magic bytes */
    private static final CompressionTypeAndMagic DEFLATE_COMPRESSION_TYPE_AND_MAGIC = new CompressionTypeAndMagic(CompressionType.DEFLATE, DEFLATE_MAGIC);

    /**
     * Detects the possible compression type through given first byte.
     *
     * @param b The first byte to check
     * @return The possible compression or <code>null</code> for no compression
     */
    private static CompressionTypeAndMagic detectCompressionType(byte b) {
        return switch (b) {
            case SNAPPY_FIRST_MAGIC -> SNAPPY_COMPRESSION_TYPE_AND_MAGIC;   // Snappy
            case GZIP_FIRST_MAGIC -> GZIP_COMPRESSION_TYPE_AND_MAGIC;       // GZIP
            case DEFLATE_FIRST_MAGIC -> DEFLATE_COMPRESSION_TYPE_AND_MAGIC; // Deflate
            default -> null;                                                // No (supported) compression
        };
    }

    /**
     * Copies all bytes from the input stream to the output stream. Does not close or flush either stream.
     *
     * @param from The input stream to read from
     * @param to The output stream to write to
     * @throws IOException If an I/O error occurs
     */
    private static void copy(InputStream from, OutputStream to) throws IOException {
        byte[] buf = new byte[1024];
        for (int read; (read = from.read(buf, 0, buf.length)) > 0;) {
            to.write(buf, 0, read);
        }
    }

    /**
     * Creates an <code>IllegalArgumentException</code> with message:
     * <pre>"Unknown compression type: " + [compression-type]</pre>
     *
     * @param compressionType The unknown compression type
     * @return The created <code>IllegalArgumentException</code> instance
     */
    private static IllegalArgumentException createUnknownCompressionTypeException(CompressionType compressionType) {
        return new IllegalArgumentException("Unknown compression type: " + compressionType);
    }

    /**
     * Creates an <code>IllegalArgumentException</code> with message:
     * <pre>"Unsupported compression type: " + [compression-type]</pre>
     *
     * @param compressionType The unsupported compression type
     * @return The created <code>IllegalArgumentException</code> instance
     */
    private static IllegalArgumentException createUnsupportedCompressionTypeException(CompressionType compressionType) {
        return new IllegalArgumentException("Unsupported compression type: " + compressionType);
    }

    /** Simple tuple of compression type and associated magic bytes */
    private static class CompressionTypeAndMagic {

        /** The compression type */
        final CompressionType compressionType;

        /** The magic bytes */
        final byte[] magicBytes;

        /**
         * Initializes a new {@link CompressionTypeAndMagic}.
         *
         * @param compressionType The compression type
         * @param magicBytes The magic bytes
         */
        CompressionTypeAndMagic(CompressionType compressionType, byte[] magicBytes) {
            super();
            this.compressionType = compressionType;
            this.magicBytes = magicBytes; // NOSONARLINT
        }
    }

    /**
     * Gets the specified value or return fall-back if value is <code>null</code>.
     *
     * @param <V> The value type
     * @param value The value to check and return
     * @param fallback The fall-back value to return
     * @return The non-<code>null</code> value or given fall-back
     */
    private static <V> V getOrElse(V value, V fallback) {
        return value == null ? fallback : value;
    }

}
