/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.redis;

import java.nio.charset.StandardCharsets;

/**
 * {@link ExpireMemberUnit} - The supported time units for <code>EXPIREMEMBER</code> command.
 * <p>
 * Syntax:
 * <pre>
 *   EXPIREMEMBER &lt;key&gt; &lt;field&gt; &lt;timeout&gt; &lt;OPTIONAL:unit-time-format&gt;
 * </pre>
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public enum ExpireMemberUnit {

    /**
     * The time unit for seconds.
     */
    SECONDS("s"),
    /**
     * The time unit for milliseconds.
     */
    MILLISECONDS("ms"),
    ;

    private final String identifier;
    private final byte[] bytes;

    private ExpireMemberUnit(String identifier) {
        this.identifier = identifier;
        this.bytes = identifier.getBytes(StandardCharsets.US_ASCII);
    }

    /**
     * Gets the bytes
     *
     * @return The bytes
     */
    public byte[] getBytes() {
        return bytes;
    }

    /**
     * Gets the identifier
     *
     * @return The identifier
     */
    public String getIdentifier() {
        return identifier;
    }
}
