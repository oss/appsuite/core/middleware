/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.redis.compression;

import com.openexchange.java.Strings;

/**
 * The supported compression types when reading/writing data from/to Redis storage.
 */
public enum CompressionType {

    /**
     * No compression at all
     */
    NONE("none"),
    /**
     * Use Snappy compression
     */
    SNAPPY("snappy"),
    /**
     * Use GZIP compression
     */
    GZIP("gzip"),
    /**
     * Use Deflate compression
     */
    DEFLATE("deflate");

    private final String identifier;

    /**
     * Initializes a new {@link CompressionType}.
     *
     * @param identifier The identifier
     */
    CompressionType(String identifier) {
        this.identifier = identifier;
    }

    /**
     * Gets the identifier
     *
     * @return The identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Gets the compression type for given identifier.
     *
     * @param identifier The identifier
     * @return The associated compression type or <code>null</code>
     */
    public static CompressionType compressionTypeFor(String identifier) {
        if (Strings.isEmpty(identifier)) {
            return null;
        }

        String lc = Strings.asciiLowerCase(identifier.trim());
        for (CompressionType compressionType : values()) {
            if (lc.equals(compressionType.identifier)) {
                return compressionType;
            }
        }
        return null;
    }

}
