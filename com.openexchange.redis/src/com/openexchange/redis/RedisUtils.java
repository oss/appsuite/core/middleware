/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.redis;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import com.google.common.collect.Lists;
import com.openexchange.exception.OXException;
import io.lettuce.core.RedisFuture;
import io.lettuce.core.api.StatefulConnection;
import io.lettuce.core.api.async.RedisHashAsyncCommands;
import io.lettuce.core.api.async.RedisKeyAsyncCommands;
import io.lettuce.core.api.sync.RedisHashCommands;
import io.lettuce.core.api.sync.RedisKeyCommands;

/**
 * {@link RedisUtils} - Utility class for Redis connector stuff.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class RedisUtils {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(RedisUtils.class);

    /**
     * Initializes a new {@link RedisUtils}.
     */
    private RedisUtils() {
        super();
    }

    /**
     * A handler for a possible exception.
     */
    @FunctionalInterface
    public static interface ExceptionHandler {

        /**
         * Handles specified exception.
         *
         * @param e The exception
         * @throws OXException If handling fails or exception is simply re-thrown
         */
        void handleException(OXException e) throws OXException;
    }

    private static final ExceptionHandler RETHROWING_EXCEPTION_HANDLER = e -> {
        throw e;
    };

    /**
     * Gets the re-throwing exception handler.
     *
     * @return The re-throwing exception handler
     */
    public static ExceptionHandler getRethrowingExceptionHandler() {
        return RETHROWING_EXCEPTION_HANDLER;
    }

    /**
     * Tries to disable auto-flush behavior for issued Redis commands over given connection.
     * <p>
     * If auto-flush behavior is disabled, multiple commands can be issued without writing them actually to the transport.
     * Commands are buffered until a {@link #flushCommands()} is issued. After calling <code>flushCommands()</code> commands are sent to the
     * transport and executed by Redis.
     *
     * @param connection The connection for which auto-flush behavior shall be disabled
     * @return <code>true</code> if successfully disabled; otherwise <code>false</code> if auto-flush behavior remains enabled
     */
    public static boolean disableAutoFlushCommands(StatefulConnection<String, InputStream> connection) {
        if (connection == null) {
            throw new IllegalArgumentException("Connection must not be null");
        }

        try {
            connection.setAutoFlushCommands(false);
            return true;
        } catch (Exception e) {
            // Failed
            LOG.error("Failed to disbale auto-flush of commands. Using regular issueing with writing commands directly to the transport", e);
            return false;
        }
    }

    /**
     * (Re-)Enables auto-flush behavior for issued Redis commands over given connection.
     *
     * @param connection The connection for which auto-flush behavior shall be disabled
     */
    public static void autoFlushCommands(StatefulConnection<String, InputStream> connection) {
        if (connection != null) {
            try {
                connection.setAutoFlushCommands(true);
            } catch (Exception e) {
                // Failed
                LOG.error("Failed to re-enable auto-flush of commands", e);
            }
        }
    }

    /**
     * Deletes specified keys from Redis storage asynchronously using given chunk size. Performs regular removal of keys if asynchronous
     * approach is not possible.
     *
     * @param keysToRemove The total list of keys to remove
     * @param chunkSize The desired chunk size
     * @param commandsProvider The commands provider to access Redis storage
     * @return The number of deleted keys
     * @throws OXException If removal of keys fails
     */
    public static long deleteKeysAsyncElseSync(List<String> keysToRemove, int chunkSize, RedisCommandsProvider commandsProvider) throws OXException {
        return deleteKeysAsyncElseSync(keysToRemove, chunkSize, commandsProvider, RETHROWING_EXCEPTION_HANDLER);
    }

    /**
     * Deletes specified keys from Redis storage asynchronously using given chunk size. Performs regular removal of keys if asynchronous
     * approach is not possible.
     *
     * @param keysToRemove The total list of keys to remove
     * @param chunkSize The desired chunk size
     * @param commandsProvider The commands provider to access Redis storage
     * @param exceptionHandler The exception handler
     * @return The number of deleted keys
     * @throws OXException If removal of keys fails
     */
    public static long deleteKeysAsyncElseSync(List<String> keysToRemove, int chunkSize, RedisCommandsProvider commandsProvider, ExceptionHandler exceptionHandler) throws OXException {
        if (keysToRemove == null) {
            return 0;
        }

        int numKeys = keysToRemove.size();
        if (numKeys <= 0) {
            return 0;
        }

        if (commandsProvider == null) {
            throw new IllegalArgumentException("Commands provider must not be null");
        }
        if (chunkSize <= 0) {
            throw new IllegalArgumentException("Chunk size must not be less than/equal to 0 (zero)");
        }

        // Check against chunk size
        if (chunkSize <= 0 || numKeys <= chunkSize) {
            // Invalid chunk size or insufficient number of keys
            // TODO: Maybe use UNLINK?
            Long numDeletedKeys = commandsProvider.getKeyCommands().del(keysToRemove.toArray(new String[numKeys]));
            return numDeletedKeys == null ? 0L : numDeletedKeys.longValue();
        }

        // Try to disable auto-flush behavior
        if (disableAutoFlushCommands(commandsProvider.getConnection())) {
            // Auto-flush behavior successfully disabled: use asynchronous approach
            ExceptionHandler excHandler = exceptionHandler == null ? RETHROWING_EXCEPTION_HANDLER : exceptionHandler;
            return deleteChunksAsync(Lists.partition(keysToRemove, chunkSize), commandsProvider, excHandler);
        }

        // Auto-flush behavior could not be disabled: use regular synchronous approach
        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
        long numberOfDeletedKeys = 0;
        for (List<String> partition : Lists.partition(keysToRemove, chunkSize)) {
            int partitionSize = partition.size();
            if (partitionSize > 0) {
                // TODO: Maybe use UNLINK?
                Long numDeletedKeys = keyCommands.del(partition.toArray(new String[partitionSize]));
                if (numDeletedKeys != null) {
                    numberOfDeletedKeys += numDeletedKeys.longValue();
                }
            }
        }
        return numberOfDeletedKeys;
    }

    /**
     * Deletes keys chunk-wise using asynchronous API.
     * <p>
     * It is required that auto-flush behavior has been disabled for specified asynchronous key commands; auto-flush behavior will be
     * re-enabled by this method.
     *
     * @param keysToRemove The total list of keys to remove
     * @param chunkSize The desired chunk size
     * @param commandsProvider The commands provider to use for which auto-flush behavior has already been disabled
     * @return The number of deleted keys
     * @throws OXException If removal fails
     * @see #disableAutoFlushCommands(FlushableRedisKeyAsyncCommands)
     */
    public static long deleteKeysAsync(List<String> keysToRemove, int chunkSize, RedisCommandsProvider commandsProvider) throws OXException {
        return deleteChunksAsync(Lists.partition(keysToRemove, chunkSize), commandsProvider);
    }

    /**
     * Deletes keys chunk-wise using asynchronous API.
     * <p>
     * It is required that auto-flush behavior has been disabled for specified asynchronous key commands;
     * see {@link #disableAutoFlushCommands(FlushableRedisKeyAsyncCommands)}. Auto-flush behavior will be re-enabled by this method.
     *
     * @param chunks The chunks of keys to remove
     * @param commandsProvider The commands provider to use for which auto-flush behavior has already been disabled
     * @return The number of deleted keys
     * @throws OXException If removal fails
     * @see #disableAutoFlushCommands(FlushableRedisKeyAsyncCommands)
     */
    public static long deleteChunksAsync(List<List<String>> chunks, RedisCommandsProvider commandsProvider) throws OXException {
        return deleteChunksAsync(chunks, commandsProvider, RETHROWING_EXCEPTION_HANDLER);
    }

    /**
     * Deletes keys chunk-wise using asynchronous API.
     * <p>
     * It is required that auto-flush behavior has been disabled for specified commands provider; auto-flush behavior will be
     * re-enabled by this method.
     *
     * @param chunks The chunks of keys to remove
     * @param commandsProvider The commands provider to use for which auto-flush behavior has already been disabled
     * @return The number of deleted keys
     * @throws OXException If removal fails
     * @see #disableAutoFlushCommands(FlushableRedisKeyAsyncCommands)
     */
    public static long deleteChunksAsync(List<List<String>> chunks, RedisCommandsProvider commandsProvider, ExceptionHandler exceptionHandler) throws OXException {
        if (commandsProvider == null) {
            throw new IllegalArgumentException("Commands provider must not be null");
        }

        try {
            if (chunks == null || chunks.isEmpty()) {
                return 0L;
            }

            // Schedule delete commands
            RedisKeyAsyncCommands<String, InputStream> keyAsyncCommands = commandsProvider.getKeyAsyncCommands();
            List<RedisFuture<Long>> futures = new ArrayList<>(chunks.size());
            for (List<String> partition : chunks) {
                int partitionSize = partition.size();
                if (partitionSize > 0) {
                    futures.add(keyAsyncCommands.del(partition.toArray(new String[partitionSize])));
                }
            }

            // Any future scheduled?
            if (futures.isEmpty()) {
                return 0L;
            }

            ExceptionHandler excHandler = exceptionHandler == null ? RETHROWING_EXCEPTION_HANDLER : exceptionHandler;

            // Write all commands to the transport layer
            commandsProvider.getConnection().flushCommands();

            // Wait until all futures complete
            long numberOfDeletedKeys = 0;
            for (RedisFuture<Long> redisFuture : futures) {
                try {
                    Long numDeletedKeys = getFrom(redisFuture);
                    if (numDeletedKeys != null) {
                        numberOfDeletedKeys += numDeletedKeys.longValue();
                    }
                } catch (OXException e) {
                    excHandler.handleException(e);
                }
            }
            return numberOfDeletedKeys;
        } finally {
            // Re-enable auto-flush behavior
            autoFlushCommands(commandsProvider.getConnection());
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Deletes specified members from denoted Redis hash asynchronously using given chunk size. Performs regular removal of members if
     * asynchronous approach is not possible.
     *
     * @param hashKey The key of the Redis hash
     * @param membersToRemove The total list of members to remove
     * @param chunkSize The desired chunk size
     * @param commandsProvider The commands provider to access Redis storage
     * @return The number of deleted keys
     * @throws OXException If removal of keys fails
     */
    public static long deleteHashMembersAsyncElseSync(String hashKey, List<String> membersToRemove, int chunkSize, RedisCommandsProvider commandsProvider) throws OXException {
        return deleteHashMembersAsyncElseSync(hashKey, membersToRemove, chunkSize, commandsProvider, RETHROWING_EXCEPTION_HANDLER);
    }

    /**
     * Deletes specified members from denoted Redis hash asynchronously using given chunk size. Performs regular removal of members if
     * asynchronous approach is not possible.
     *
     * @param hashKey The key of the Redis hash
     * @param membersToRemove The total list of members to remove
     * @param chunkSize The desired chunk size
     * @param commandsProvider The commands provider to access Redis storage
     * @param exceptionHandler The exception handler
     * @return The number of deleted keys
     * @throws OXException If removal of keys fails
     */
    public static long deleteHashMembersAsyncElseSync(String hashKey, List<String> membersToRemove, int chunkSize, RedisCommandsProvider commandsProvider, ExceptionHandler exceptionHandler) throws OXException {
        if (membersToRemove == null) {
            return 0;
        }

        int numKeys = membersToRemove.size();
        if (numKeys <= 0) {
            return 0;
        }

        if (hashKey == null) {
            throw new IllegalArgumentException("Hash key must not be null");
        }
        if (commandsProvider == null) {
            throw new IllegalArgumentException("Commands provider must not be null");
        }
        if (chunkSize <= 0) {
            throw new IllegalArgumentException("Chunk size must not be less than/equal to 0 (zero)");
        }

        // Check against chunk size
        if (chunkSize <= 0 || numKeys <= chunkSize) {
            // Invalid chunk size or insufficient number of members
            Long numDeletedMembers = commandsProvider.getRawHashCommands().hdel(hashKey, membersToRemove.toArray(new String[numKeys]));
            return numDeletedMembers == null ? 0L : numDeletedMembers.longValue();
        }

        // Try to disable auto-flush behavior
        if (disableAutoFlushCommands(commandsProvider.getConnection())) {
            // Auto-flush behavior successfully disabled: use asynchronous approach
            ExceptionHandler excHandler = exceptionHandler == null ? RETHROWING_EXCEPTION_HANDLER : exceptionHandler;
            return deleteMembersChunksAsync(hashKey, Lists.partition(membersToRemove, chunkSize), commandsProvider, excHandler);
        }

        // Auto-flush behavior could not be disabled: use regular synchronous approach
        RedisHashCommands<String, InputStream> hashCommands = commandsProvider.getRawHashCommands();
        long numberOfDeletedKeys = 0;
        for (List<String> partition : Lists.partition(membersToRemove, chunkSize)) {
            int partitionSize = partition.size();
            if (partitionSize > 0) {
                Long numDeletedKeys = hashCommands.hdel(hashKey, partition.toArray(new String[partitionSize]));
                if (numDeletedKeys != null) {
                    numberOfDeletedKeys += numDeletedKeys.longValue();
                }
            }
        }
        return numberOfDeletedKeys;
    }

    /**
     * Deletes members chunk-wise from denoted Redis hash using asynchronous API.
     * <p>
     * It is required that auto-flush behavior has been disabled for specified asynchronous key commands; auto-flush behavior will be
     * re-enabled by this method.
     *
     * @param hashKey The key of the Redis hash
     * @param chunks The chunks of members to remove
     * @param commandsProvider The commands provider to use for which auto-flush behavior has already been disabled
     * @return The number of deleted keys
     * @throws OXException If removal fails
     * @see #disableAutoFlushCommands(FlushableRedisKeyAsyncCommands)
     */
    public static long deleteMembersChunksAsync(String hashKey, List<List<String>> chunks, RedisCommandsProvider commandsProvider, ExceptionHandler exceptionHandler) throws OXException {
        if (commandsProvider == null) {
            throw new IllegalArgumentException("Commands provider must not be null");
        }
        try {
            if (chunks == null || chunks.isEmpty()) {
                return 0L;
            }
            ExceptionHandler excHandler = exceptionHandler == null ? RETHROWING_EXCEPTION_HANDLER : exceptionHandler;

            // Schedule delete commands
            RedisHashAsyncCommands<String, InputStream> hashAsyncCommands = commandsProvider.getRawHashAsyncCommands();
            List<RedisFuture<Long>> futures = new ArrayList<>(chunks.size());
            for (List<String> partition : chunks) {
                int partitionSize = partition.size();
                if (partitionSize > 0) {
                    futures.add(hashAsyncCommands.hdel(hashKey, partition.toArray(new String[partitionSize])));
                }
            }

            // Write all commands to the transport layer
            commandsProvider.getConnection().flushCommands();

            // Wait until all futures complete
            long numberOfDeletedKeys = 0;
            for (RedisFuture<Long> redisFuture : futures) {
                try {
                    Long numDeletedKeys = getFrom(redisFuture);
                    if (numDeletedKeys != null) {
                        numberOfDeletedKeys += numDeletedKeys.longValue();
                    }
                } catch (OXException e) {
                    excHandler.handleException(e);
                }
            }
            return numberOfDeletedKeys;
        } finally {
            // Re-enable auto-flush behavior
            autoFlushCommands(commandsProvider.getConnection());
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Deletes specified members from denoted Redis hash asynchronously using given chunk size. Performs regular removal of members if
     * asynchronous approach is not possible.
     *
     * @param hashKey2members The mapping of key of the Redis hash to total list of members to remove
     * @param chunkSize The desired chunk size
     * @param commandsProvider The commands provider to access Redis storage
     * @return The number of deleted keys
     * @throws OXException If removal of keys fails
     */
    public static long deleteMultipleHashMembersAsyncElseSync(Collection<Map.Entry<String, List<String>>> hashKey2members, int chunkSize, RedisCommandsProvider commandsProvider) throws OXException {
        return deleteMultipleHashMembersAsyncElseSync(hashKey2members, chunkSize, commandsProvider, RETHROWING_EXCEPTION_HANDLER);
    }

    /**
     * Deletes specified members from denoted Redis hash asynchronously using given chunk size. Performs regular removal of members if
     * asynchronous approach is not possible.
     *
     * @param hashKey2members The mapping of key of the Redis hash to total list of members to remove
     * @param chunkSize The desired chunk size
     * @param commandsProvider The commands provider to access Redis storage
     * @param exceptionHandler The exception handler
     * @return The number of deleted keys
     * @throws OXException If removal of keys fails
     */
    public static long deleteMultipleHashMembersAsyncElseSync(Collection<Map.Entry<String, List<String>>> hashKey2members, int chunkSize, RedisCommandsProvider commandsProvider, ExceptionHandler exceptionHandler) throws OXException {
        if (hashKey2members == null || hashKey2members.isEmpty()) {
            return 0;
        }
        if (commandsProvider == null) {
            throw new IllegalArgumentException("Commands provider must not be null");
        }
        if (chunkSize <= 0) {
            throw new IllegalArgumentException("Chunk size must not be less than/equal to 0 (zero)");
        }

        // Try to disable auto-flush behavior
        if (disableAutoFlushCommands(commandsProvider.getConnection())) {
            ExceptionHandler excHandler = exceptionHandler == null ? RETHROWING_EXCEPTION_HANDLER : exceptionHandler;
            return deleteMultipleMembersChunksAsync(hashKey2members, chunkSize, commandsProvider, excHandler);
        }

        // Delete using synchronous API
        RedisHashCommands<String, InputStream> rawHashCommands = commandsProvider.getRawHashCommands();
        long numDeleted = 0;
        for (Map.Entry<String, List<String>> e : hashKey2members) {
            String hashKey = e.getKey();
            List<String> membersToRemove = e.getValue();
            if (!membersToRemove.isEmpty()) {
                for (List<String> partition : Lists.partition(membersToRemove, chunkSize)) {
                    int partitionSize = partition.size();
                    if (partitionSize > 0) {
                        numDeleted += rawHashCommands.hdel(hashKey, partition.toArray(new String[partitionSize])).longValue();
                    }
                }
            }
        }
        return numDeleted;
    }

    /**
     * Deletes multiple members chunk-wise from denoted Redis hash using asynchronous API.
     * <p>
     * It is required that auto-flush behavior has been disabled for specified asynchronous key commands; auto-flush behavior will be
     * re-enabled by this method.
     *
     * @param hashKey2members The mapping of key of the Redis hash to total list of members to remove
     * @param chunkSize The desired chunk size
     * @param commandsProvider The commands provider to use for which auto-flush behavior has already been disabled
     * @param exceptionHandler The exception handler
     * @return The number of deleted keys
     * @throws OXException If removal fails
     * @see #disableAutoFlushCommands(FlushableRedisKeyAsyncCommands)
     */
    public static long deleteMultipleMembersChunksAsync(Collection<Map.Entry<String, List<String>>> hashKey2members, int chunkSize, RedisCommandsProvider commandsProvider, ExceptionHandler exceptionHandler) throws OXException {
        if (commandsProvider == null) {
            throw new IllegalArgumentException("Commands provider must not be null");
        }

        try {
            if (hashKey2members == null) {
                return 0L;
            }
            if (chunkSize <= 0) {
                throw new IllegalArgumentException("Chunk size must not be less than/equal to 0 (zero)");
            }

            List<RedisFuture<Long>> futures = null;
            for (Map.Entry<String, List<String>> e : hashKey2members) {
                String hashKey = e.getKey();
                List<String> membersToRemove = e.getValue();
                if (!membersToRemove.isEmpty()) {
                    for (List<String> partition : Lists.partition(membersToRemove, chunkSize)) {
                        int partitionSize = partition.size();
                        if (partitionSize > 0) {
                            if (futures == null) {
                                futures = new ArrayList<>();
                            }
                            futures.add(commandsProvider.getRawHashAsyncCommands().hdel(hashKey, partition.toArray(new String[partitionSize])));
                        }
                    }
                }
            }

            // Any future scheduled?
            if (futures == null) {
                return 0L;
            }

            ExceptionHandler excHandler = exceptionHandler == null ? RETHROWING_EXCEPTION_HANDLER : exceptionHandler;

            // Write all commands to the transport layer
            commandsProvider.getConnection().flushCommands();

            // Wait until all futures complete
            long numberOfDeletedKeys = 0;
            for (RedisFuture<Long> redisFuture : futures) {
                try {
                    Long numDeletedKeys = getFrom(redisFuture);
                    if (numDeletedKeys != null) {
                        numberOfDeletedKeys += numDeletedKeys.longValue();
                    }
                } catch (OXException e) {
                    excHandler.handleException(e);
                }
            }
            return numberOfDeletedKeys;
        } finally {
            autoFlushCommands(commandsProvider.getConnection());
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Gets the computation result from specified future; waiting if necessary for the computation to complete.
     *
     * @param <V> The type of the result value
     * @param f The future to get the computation result from
     * @return The computation result
     * @throws OXException If computation raises an error
     */
    public static <V> V getFrom(Future<V> f) throws OXException {
        if (f == null) {
            return null;
        }

        try {
            return f.get();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw RedisExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof OXException oxe) {
                throw oxe;
            }
            Throwable t = cause == null ? e : cause;
            throw RedisExceptionCode.UNEXPECTED_ERROR.create(t, t.getMessage());
        }
    }

    /**
     * Gets the computation result from specified future; waiting if necessary for at most the given time for the computation to complete.
     *
     * @param <V> The type of the result value
     * @param f The future to get the computation result from
     * @param timeout The maximum time to wait
     * @param unit The time unit of the timeout argument
     * @return The computation result
     * @throws OXException If computation raises an error
     */
    public static <V> V getFrom(Future<V> f, long timeout, TimeUnit unit) throws OXException {
        if (f == null) {
            return null;
        }

        try {
            return f.get(timeout, unit);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw RedisExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof OXException oxe) {
                throw oxe;
            }
            Throwable t = cause == null ? e : cause;
            throw RedisExceptionCode.UNEXPECTED_ERROR.create(t, t.getMessage());
        } catch (TimeoutException e) {
            Thread.currentThread().interrupt();
            throw RedisExceptionCode.REDIS_COMMAND_TIMEOUT.create(e, "UNKNOWN");
        }
    }

}
