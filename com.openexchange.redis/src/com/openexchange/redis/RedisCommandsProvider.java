/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.redis;

import java.io.InputStream;
import java.util.Optional;
import com.openexchange.redis.commands.StringRedisStringCommands;
import io.lettuce.core.api.StatefulConnection;
import io.lettuce.core.api.async.RedisHashAsyncCommands;
import io.lettuce.core.api.async.RedisKeyAsyncCommands;
import io.lettuce.core.api.sync.BaseRedisCommands;
import io.lettuce.core.api.sync.RedisHashCommands;
import io.lettuce.core.api.sync.RedisKeyCommands;
import io.lettuce.core.api.sync.RedisListCommands;
import io.lettuce.core.api.sync.RedisServerCommands;
import io.lettuce.core.api.sync.RedisSetCommands;
import io.lettuce.core.api.sync.RedisSortedSetCommands;
import io.lettuce.core.api.sync.RedisStringCommands;
import io.lettuce.core.api.sync.RedisTransactionalCommands;

/**
 * {@link RedisCommandsProvider} - Provides Redis commands to communicate with Redis end-point.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public interface RedisCommandsProvider {

    /**
     * Gets the underlying Redis connection.
     *
     * @return The connection
     */
    StatefulConnection<String, InputStream> getConnection();

    /**
     * Gets the base commands.
     *
     * @return The base commands
     */
    BaseRedisCommands<String, InputStream> getBaseCommands();

    /**
     * Gets the key commands.
     *
     * @return The key commands
     */
    RedisKeyCommands<String, InputStream> getKeyCommands();

    /**
     * Gets the asynchronous key commands.
     *
     * @return The asynchronous key commands
     */
    RedisKeyAsyncCommands<String, InputStream> getKeyAsyncCommands();

    /**
     * Gets the (raw) string commands.
     *
     * @return The (raw) string commands
     */
    RedisStringCommands<String, InputStream> getRawStringCommands();

    /**
     * Gets the (raw) hash commands.
     *
     * @return The (raw) hash commands
     */
    RedisHashCommands<String, InputStream> getRawHashCommands();

    /**
     * Gets the (raw) asynchronous hash commands.
     *
     * @return The (raw) asynchronous hash commands
     */
    RedisHashAsyncCommands<String, InputStream> getRawHashAsyncCommands();

    /**
     * Gets the optional transactional commands.
     *
     * @return The transactional commands or empty
     */
    Optional<RedisTransactionalCommands<String, InputStream>> optTransactionalCommands();

    // ---------------------------------------------------- String values -----------------------------------------------------------------

    /**
     * Gets the string commands.
     *
     * @return The string commands
     */
    default RedisStringCommands<String, String> getStringCommands() {
        return new StringRedisStringCommands(getRawStringCommands());
    }

    /**
     * Gets the set commands.
     *
     * @return The set commands
     */
    RedisSetCommands<String, String> getSetCommands();

    /**
     * Gets the sorted set commands.
     *
     * @return The sorted set commands
     */
    RedisSortedSetCommands<String, String> getSortedSetCommands();

    /**
     * Gets the hash commands.
     *
     * @return The hash commands
     */
    RedisHashCommands<String, String> getHashCommands();

    /**
     * Gets the list commands.
     *
     * @return The list commands
     */
    RedisListCommands<String, String> getListCommands();

    /**
     * Gets the server commands.
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * <b>Note</b>: Use with caution!!!
     * </div>
     *
     * @return The server commands
     */
    RedisServerCommands<String, String> getServerCommands();

}
