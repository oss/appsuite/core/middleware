/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.redis;

import java.util.List;
import java.util.Optional;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.annotation.SingletonService;

/**
 * {@link RedisConnectorService} - The service providing access to various Redis connectors for different purposes.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
@SingletonService
public interface RedisConnectorService {

    /**
     * Checks if this service supports connector providers for specified Redis type.
     *
     * @param redisType The Redis type to check
     * @return <code>true</code> if given Redis type is supported; otherwise <code>false</code>
     * @throws OXException If support cannot be checked
     */
    boolean supports(RedisType redisType) throws OXException;

    /**
     * Gets the (regular) connector provider for the (local) regular Redis storage.
     *
     * @return The (regular) connector provider for the Redis storage
     * @throws OXException If the connector provider cannot be returned
     */
    RedisConnectorProvider getConnectorProvider() throws OXException;

    /**
     * Gets the connector provider for the special Redis storage dedicated for cache data.
     *
     * @return The connector provider for the Redis storage dedicated for cache data or empty if none configured
     * @throws OXException If the connector provider cannot be returned
     */
    Optional<RedisConnectorProvider> getCacheConnectorProvider() throws OXException;

    /**
     * Gets the connector provider for the special Redis storage dedicated for cache data if enabled; if not {@link #getConnectorProvider() the regular one} is returned.
     *
     * @return Either the connector provider for the special Redis storage dedicated for cache data or the regular one
     * @throws OXException If no connector provider can be returned
     */
    default RedisConnectorProvider getCacheConnectorProviderOrElseRegular() throws OXException {
        RedisConnectorProvider cacheConnectorProvider = getCacheConnectorProvider().orElse(null);
        return cacheConnectorProvider == null ? getConnectorProvider() : cacheConnectorProvider;
    }

    /**
     * Gets the connector providers for the remote regular Redis storages.
     * <p>
     * Exemplary usage:
     * <pre>
     *  // Delete by key in all associated remote regular Redis storages
     *  String myKey = ...;
     *  List<RedisConnectorProvider> remoteConnectorProviders = connectorService.getRemoteConnectorProviders();
     *  for (RedisConnectorProvider connectorProvider : remoteConnectorProviders) {
     *     connectorProvider.getConnector().executeVoidOperation(commandsProvider -> {
     *        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
     *        // Delete by key
     *        keyCommands.del(myKey)
     *     }
     *  }
     * </pre>
     *
     * @return The remote connector providers
     * @throws OXException If remote connector providers cannot be returned
     */
    List<RedisConnectorProvider> getRemoteConnectorProviders() throws OXException;

}
