/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.redis;

import com.openexchange.java.Strings;

/**
 * {@link RedisType} - The type of the Redis instance.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public enum RedisType {

    /**
     * The regular Redis instance on local site.
     */
    REGULAR("regular"),
    /**
     * The Redis instance especially for volatile cache data on local site.
     */
    CACHE("cache"),
    /**
     * A Redis instance running on a remote site.
     */
    REMOTE("remote"),
    ;

    private final String identifier;

    /**
     * Initializes a new instance of {@link RedisType}.
     *
     * @param identifier The identifier
     */
    private RedisType(String identifier) {
        this.identifier = identifier;
    }

    /**
     * Gets the identifier
     *
     * @return The identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Gets the Redis type for given identifier.
     *
     * @param identifier The identifier
     * @param def The default value to return if there is no matching Redis type
     * @return The Redis type or given default value
     */
    public static RedisType redisTypeFor(String identifier, RedisType def) {
        if (Strings.isEmpty(identifier)) {
            return def;
        }
        String id = Strings.asciiLowerCase(identifier.trim());
        for (RedisType redisType : values()) {
            if (id.equals(redisType.getIdentifier())) {
                return redisType;
            }
        }
        return def;
    }

}
