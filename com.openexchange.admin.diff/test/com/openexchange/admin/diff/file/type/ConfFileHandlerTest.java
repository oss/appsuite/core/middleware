/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.diff.file.type;

import java.util.List;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.openexchange.admin.diff.file.domain.ConfigurationFile;
import com.openexchange.admin.diff.file.handler.ConfFileHandler;
import com.openexchange.admin.diff.file.handler.impl.CcfHandler;
import com.openexchange.admin.diff.file.handler.impl.ConfHandler;
import com.openexchange.admin.diff.file.handler.impl.NoConfigFileHandler;
import com.openexchange.admin.diff.file.handler.impl.NoExtensionHandler;
import com.openexchange.admin.diff.file.handler.impl.PerfmapHandler;
import com.openexchange.admin.diff.file.handler.impl.PropertyHandler;
import com.openexchange.admin.diff.file.handler.impl.ShHandler;
import com.openexchange.admin.diff.file.handler.impl.TypesHandler;
import com.openexchange.admin.diff.file.handler.impl.XmlHandler;
import com.openexchange.admin.diff.file.handler.impl.YamlHandler;
import com.openexchange.admin.diff.result.DiffResult;
import com.openexchange.test.mock.MockUtils;

/**
 * {@link ConfFileHandlerTest}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 7.6.1
 */
@SuppressWarnings("unchecked")
public class ConfFileHandlerTest {

    private ConfigurationFile configurationFile = null;

    @BeforeEach
    public void setUp() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        MockUtils.injectValueIntoPrivateField(CcfHandler.class, "instance", null);
        MockUtils.injectValueIntoPrivateField(ConfHandler.class, "instance", null);
        MockUtils.injectValueIntoPrivateField(PropertyHandler.class, "instance", null);
        MockUtils.injectValueIntoPrivateField(NoExtensionHandler.class, "instance", null);
        MockUtils.injectValueIntoPrivateField(PerfmapHandler.class, "instance", null);
        MockUtils.injectValueIntoPrivateField(ShHandler.class, "instance", null);
        MockUtils.injectValueIntoPrivateField(YamlHandler.class, "instance", null);
        MockUtils.injectValueIntoPrivateField(TypesHandler.class, "instance", null);
        MockUtils.injectValueIntoPrivateField(XmlHandler.class, "instance", null);
        MockUtils.injectValueIntoPrivateField(NoConfigFileHandler.class, "instance", null);

    }

    @Test
    public void testAddConfigurationFile_noConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa.dfdsf";
        String content = "content";
        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(NoConfigFileHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_ccfConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.CCF.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        CcfHandler.getInstance().addFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(CcfHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);

    }

    @Test
    public void testAddConfigurationFile_cnfConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.CNF.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(ConfHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_confConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.CONF.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(ConfHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_inConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.PROPERTY.getFileExtension() + "." + ConfigurationFileTypes.IN.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(PropertyHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(FilenameUtils.removeExtension(fileName), string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_noExtensionConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.NO_EXTENSION.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(NoExtensionHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_perfmapConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.PERFMAP.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(PerfmapHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_propertyConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.PROPERTY.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(PropertyHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_shConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.SH.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(ShHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_typesConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.TYPES.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(TypesHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_xmlConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.XML.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(XmlHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_yamlConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.YAML.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(YamlHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_ymlConfigurationFile_fileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.YML.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, false);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> installedFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(YamlHandler.getInstance(), "installedFiles");
        Assertions.assertEquals(1, installedFiles.size());
        String string = installedFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = installedFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_ccfConfigurationFile_origFileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.CCF.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, true);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> originalFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(CcfHandler.getInstance(), "originalFiles");
        Assertions.assertEquals(1, originalFiles.size());
        String string = originalFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = originalFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_cnfConfigurationFile_origFileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.CNF.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, true);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> originalFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(ConfHandler.getInstance(), "originalFiles");
        Assertions.assertEquals(1, originalFiles.size());
        String string = originalFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = originalFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_confConfigurationFile_origFileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.CONF.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, true);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> originalFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(ConfHandler.getInstance(), "originalFiles");
        Assertions.assertEquals(1, originalFiles.size());
        String string = originalFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = originalFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_inConfigurationFile_origFileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.SH.getFileExtension() + "." + ConfigurationFileTypes.IN.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, true);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> originalFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(ShHandler.getInstance(), "originalFiles");
        Assertions.assertEquals(1, originalFiles.size());
        String string = originalFiles.get(0).getName();
        Assertions.assertEquals(FilenameUtils.removeExtension(fileName), string);
        String stringContent = originalFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_noExtensionConfigurationFile_origFileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.NO_EXTENSION.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, true);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> originalFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(NoExtensionHandler.getInstance(), "originalFiles");
        Assertions.assertEquals(1, originalFiles.size());
        String string = originalFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = originalFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_perfmapConfigurationFile_origFileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.PERFMAP.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, true);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> originalFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(PerfmapHandler.getInstance(), "originalFiles");
        Assertions.assertEquals(1, originalFiles.size());
        String string = originalFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = originalFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_propertyConfigurationFile_origFileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.PROPERTY.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, true);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> originalFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(PropertyHandler.getInstance(), "originalFiles");
        Assertions.assertEquals(1, originalFiles.size());
        String string = originalFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = originalFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_shConfigurationFile_origFileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.SH.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, true);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> originalFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(ShHandler.getInstance(), "originalFiles");
        Assertions.assertEquals(1, originalFiles.size());
        String string = originalFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = originalFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_typesConfigurationFile_origFileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.TYPES.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, true);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> originalFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(TypesHandler.getInstance(), "originalFiles");
        Assertions.assertEquals(1, originalFiles.size());
        String string = originalFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = originalFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_xmlConfigurationFile_origFileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.XML.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, true);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> originalFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(XmlHandler.getInstance(), "originalFiles");
        Assertions.assertEquals(1, originalFiles.size());
        String string = originalFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = originalFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_yamlConfigurationFile_origFileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.YAML.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, true);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> originalFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(YamlHandler.getInstance(), "originalFiles");
        Assertions.assertEquals(1, originalFiles.size());
        String string = originalFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = originalFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }

    @Test
    public void testAddConfigurationFile_ymlConfigurationFile_origFileAdded() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        String fileName = "aaaa." + ConfigurationFileTypes.YML.getFileExtension();
        String content = "content";

        configurationFile = new ConfigurationFile(fileName, "/opt/open-xchange/bundles", "/jar!/conf", content, true);

        ConfFileHandler.addConfigurationFile(new DiffResult(), configurationFile);

        List<ConfigurationFile> originalFiles = (List<ConfigurationFile>) MockUtils.getValueFromField(YamlHandler.getInstance(), "originalFiles");
        Assertions.assertEquals(1, originalFiles.size());
        String string = originalFiles.get(0).getName();
        Assertions.assertEquals(fileName, string);
        String stringContent = originalFiles.get(0).getContent();
        Assertions.assertEquals(content, stringContent);
    }
}
