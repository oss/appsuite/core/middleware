/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.diff.file.provider;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.admin.diff.file.domain.ConfigurationFile;
import com.openexchange.admin.diff.file.handler.ConfFileHandler;
import com.openexchange.admin.diff.file.type.ConfigurationFileTypes;
import com.openexchange.admin.diff.result.DiffResult;

/**
 * {@link ConfFolderFileProviderTest}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 7.6.1
 */
public class ConfFolderFileProviderTest {

    @InjectMocks
    private ConfFolderFileProvider fileProvider;

    @Mock
    private File configurationFile;

    @TempDir
    Path folder;  // Replaces TemporaryFolder

    List<File> configurationFiles = new ArrayList<>();

    private File rootFolder;

    private MockedStatic<FileUtils> staticFileUtilsMock;
    private MockedStatic<ConfFileHandler> staticConfFileHandlerMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        staticFileUtilsMock = Mockito.mockStatic(FileUtils.class);
        staticConfFileHandlerMock = Mockito.mockStatic(ConfFileHandler.class);

        configurationFiles.add(configurationFile);

        rootFolder = Mockito.mock(File.class);
        Mockito.when(rootFolder.listFiles()).thenReturn(new File[]{});
    }

    @AfterEach
    public void tearDown() {
        staticFileUtilsMock.close();
        staticConfFileHandlerMock.close();
    }

    @Test
    public void testReadConfigurationFiles_listFilesNull_returnEmptyArray() {
        Mockito.when(FileUtils.listFiles(
                ArgumentMatchers.any(File.class),
                ArgumentMatchers.any(String[].class),
                ArgumentMatchers.anyBoolean())
        ).thenReturn(null);

        List<File> readConfigurationFiles = fileProvider.readConfigurationFiles(
                new DiffResult(),
                rootFolder,
                ConfigurationFileTypes.CONFIGURATION_FILE_TYPE
        );

        assertEquals(0, readConfigurationFiles.size());
    }

    @Test
    public void testReadConfigurationFiles_fileFound_fileInList() {
        Mockito.when(FileUtils.listFiles(
                ArgumentMatchers.any(File.class),
                ArgumentMatchers.any(String[].class),
                ArgumentMatchers.anyBoolean())
        ).thenReturn(configurationFiles);

        List<File> readConfigurationFiles = fileProvider.readConfigurationFiles(
                new DiffResult(),
                rootFolder,
                ConfigurationFileTypes.CONFIGURATION_FILE_TYPE
        );

        assertEquals(1, readConfigurationFiles.size());
    }

    @Test
    public void testAddFilesToDiffQueue_filesNull_noFileAddedToQueue() {
        try (MockedStatic<ConfFileHandler> mockedStatic = Mockito.mockStatic(ConfFileHandler.class)) {
            fileProvider.addFilesToDiffQueue(new DiffResult(), rootFolder, null, true);

            mockedStatic.verify(() -> ConfFileHandler.addConfigurationFile(
                    ArgumentMatchers.any(DiffResult.class),
                    ArgumentMatchers.any(ConfigurationFile.class)
            ), Mockito.never());
        }
    }

    @Test
    public void testAddFilesToDiffQueue_filesNotInConfFolder_noFileAddedToQueue() throws IOException {
        File newFile = folder.resolve("file1.properties").toFile();
        File newFile2 = folder.resolve("file2.properties").toFile();
        List<File> files = new ArrayList<>();
        files.add(newFile);
        files.add(newFile2);

        try (MockedStatic<ConfFileHandler> mockedStatic = Mockito.mockStatic(ConfFileHandler.class)) {
            fileProvider.addFilesToDiffQueue(new DiffResult(), rootFolder, files, true);

            mockedStatic.verify(() -> ConfFileHandler.addConfigurationFile(
                    ArgumentMatchers.any(DiffResult.class),
                    ArgumentMatchers.any(ConfigurationFile.class)
            ), Mockito.never());
        }
    }
}
