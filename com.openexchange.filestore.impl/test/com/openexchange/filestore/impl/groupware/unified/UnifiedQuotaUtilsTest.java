
package com.openexchange.filestore.impl.groupware.unified;

import static com.openexchange.java.Autoboxing.B;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.config.cascade.ComposedConfigProperty;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.exception.OXException;
import com.openexchange.filestore.impl.osgi.Services;
import com.openexchange.server.ServiceExceptionCode;

/**
 * {@link UnifiedQuotaUtilsTest}
 *
 * @author <a href="mailto:vitali.sjablow@open-xchange.com">Vitali Sjablow</a>
 * @since v7.10.0
 */
public class UnifiedQuotaUtilsTest {

    private static final String COM_OPENEXCHANGE_UNIFIEDQUOTA_ENABLED = "com.openexchange.unifiedquota.enabled";

    @Mock
    ConfigViewFactory mockedConfigViewFactory;

    @Mock
    ConfigView mockedConfigView;

    @Mock
    ComposedConfigProperty<String> mockedComposedConfigProperty;

    private AutoCloseable staticServicesMock;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        staticServicesMock = Mockito.mockStatic(Services.class);

        /**
         * Happy path mocking, every test will change only those parts that are not identical with
         * the ideal path.
         */
        setUpHappyPath_isUnifiedQuotaEnabledFor();
    }

    @AfterEach
    public void tearDown() throws Exception {
        staticServicesMock.close();
    }

    private void setUpHappyPath_isUnifiedQuotaEnabledFor() {
        try {
            Mockito.when(Services.optService(ConfigViewFactory.class)).thenReturn(mockedConfigViewFactory);
            Mockito.when(mockedConfigViewFactory.getView(0, 0)).thenReturn(mockedConfigView);
            Mockito.when(mockedConfigView.property(COM_OPENEXCHANGE_UNIFIEDQUOTA_ENABLED, String.class)).thenReturn(mockedComposedConfigProperty);
            Mockito.when(B(mockedComposedConfigProperty.isDefined())).thenReturn(B(true));
            Mockito.when(mockedComposedConfigProperty.get()).thenReturn("true");
        } catch (OXException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void isUnifiedQuotaEnabledFor_MissingFactoryTest() {
        Mockito.when(Services.optService(ConfigViewFactory.class)).thenReturn(null);
        try {
            UnifiedQuotaUtils.isUnifiedQuotaEnabledFor(0, 0);
        } catch (OXException e) {
            assertTrue(e.getExceptionCode() == ServiceExceptionCode.SERVICE_UNAVAILABLE, "Wrong exception was thrown, SERVICE_UNAVAILABLE was expected");
            return;
        }
        fail("No error was thrown, but expected");
    }

    @Test
    public void isUnifiedQuotaEnabledFor_MissingProperty() throws OXException {
        Mockito.when(mockedConfigView.property(COM_OPENEXCHANGE_UNIFIEDQUOTA_ENABLED, String.class)).thenReturn(null);
        boolean unifiedQuotaEnabled = UnifiedQuotaUtils.isUnifiedQuotaEnabledFor(0, 0);
        assertTrue(unifiedQuotaEnabled == false, "false expected but got true");
    }

    @Test
    public void isUnifiedQuotaEnabledFor_EmptyPropertyTest() throws OXException {
        Mockito.when(mockedComposedConfigProperty.get()).thenReturn("");
        boolean unifiedQuotaEnabled = UnifiedQuotaUtils.isUnifiedQuotaEnabledFor(0, 0);
        assertTrue(unifiedQuotaEnabled == false, "false expected but got true");
    }

    @Test
    public void isUnifiedQuotaEnabledFor_WrongPropertyTest() throws OXException {
        Mockito.when(mockedComposedConfigProperty.get()).thenReturn("This is so wrong");
        boolean unifiedQuotaEnabled = UnifiedQuotaUtils.isUnifiedQuotaEnabledFor(0, 0);
        assertTrue(unifiedQuotaEnabled == false, "false expected but got true");
    }

    @Test
    public void isUnifiedQuotaEnabledFor_TruePropertyTest() throws OXException {
        boolean unifiedQuotaEnabled = UnifiedQuotaUtils.isUnifiedQuotaEnabledFor(0, 0);
        assertTrue(unifiedQuotaEnabled == true, "true expected but got false");
    }
}
