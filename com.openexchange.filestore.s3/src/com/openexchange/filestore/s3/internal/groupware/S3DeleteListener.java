/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.filestore.s3.internal.groupware;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.IntFunction;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.filestore.FileStorage;
import com.openexchange.filestore.FileStorage2EntitiesResolver;
import com.openexchange.filestore.FileStorages;
import com.openexchange.groupware.delete.DeleteEvent;
import com.openexchange.groupware.delete.DeleteFailedExceptionCodes;
import com.openexchange.groupware.delete.PositioningDeleteListener;
import com.openexchange.java.Functions;
import com.openexchange.java.Key;
import com.openexchange.java.util.UUIDs;

/**
 * {@link S3DeleteListener} - The delete listener for S3 file storage connector.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class S3DeleteListener implements PositioningDeleteListener {

    /**
     * Initializes a new instance of {@link S3DeleteListener}.
     */
    public S3DeleteListener() {
        super();
    }

    @Override
    public String getDirectSuccessorDeleteListener() {
        return "com.openexchange.groupware.filestore.FileStorageRemover";
    }

    private static final IntFunction<String[]> FUNCTION_STRING_ARRAY = size -> new String[size];

    @Override
    public void deletePerformed(DeleteEvent event, Connection readCon, Connection writeCon) throws OXException {
        if (event.getType() == DeleteEvent.TYPE_USER) {
            int contextId = event.getContext().getContextId();
            int userId = event.getId();

            List<ChunkInfo> chunks = selectChunksFromDatabase(userId, contextId, writeCon);
            if (chunks.isEmpty()) {
                return;
            }

            // Delete files from S3
            FileStorage2EntitiesResolver resolver = FileStorages.getFileStorage2EntitiesResolver();
            FileStorage fileStorage = resolver.getFileStorageUsedBy(contextId, userId, true);

            String[] files = chunks.stream().map(c -> UUIDs.getUnformattedString(c.s3Id)).toArray(FUNCTION_STRING_ARRAY);
            fileStorage.deleteFiles(files);

            // Cleanse from database
            dropChunksFromDatabase(userId, contextId, writeCon);
        } else if (event.getType() == DeleteEvent.TYPE_CONTEXT) {
            int contextId = event.getContext().getContextId();

            List<ChunkInfo> chunks = selectChunksFromDatabase(0, contextId, writeCon);
            if (chunks.isEmpty()) {
                return;
            }

            // Delete files from S3
            Map<Key, FileStorage> fileStorages = new HashMap<>();
            Map<Key, List<String>> files = new HashMap<>();

            for (ChunkInfo chunkInfo : chunks) {
                Key key = new Key(chunkInfo.optUserId, chunkInfo.contextId);
                FileStorage fileStorage = fileStorages.get(key);
                if (fileStorage == null) {
                    FileStorage2EntitiesResolver resolver = FileStorages.getFileStorage2EntitiesResolver();
                    fileStorage = chunkInfo.optUserId > 0 ? resolver.getFileStorageUsedBy(contextId, chunkInfo.optUserId, true) : resolver.getFileStorageUsedBy(contextId, true);
                    fileStorages.put(key, fileStorage);
                }

                files.computeIfAbsent(key, Functions.getNewArrayListFuntion()).add(UUIDs.getUnformattedString(chunkInfo.s3Id));
            }

            for (Map.Entry<Key, List<String>> e : files.entrySet()) {
                Key key = e.getKey();
                List<String> names = e.getValue();
                FileStorage fileStorage = fileStorages.get(key);
                fileStorage.deleteFiles(names.toArray(new String[names.size()]));
            }

            // Cleanse from database
            dropChunksFromDatabase(0, contextId, writeCon);
        }
    }

    private static List<ChunkInfo> selectChunksFromDatabase(int optUserId, int contextId, Connection writeCon) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // Delete account data
            if (optUserId > 0) {
                stmt = writeCon.prepareStatement("SELECT `s3_id` FROM `s3_filestore` WHERE `cid` = ? AND `user` = ?");
                stmt.setInt(1, contextId);
                stmt.setInt(2, optUserId);
            } else {
                stmt = writeCon.prepareStatement("SELECT `s3_id`, `user` FROM `s3_filestore` WHERE `cid` = ?");
                stmt.setInt(1, contextId);
            }
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return Collections.emptyList();
            }

            List<ChunkInfo> chunks = new ArrayList<>();
            do {
                if (optUserId > 0) {
                    chunks.add(new ChunkInfo(optUserId, contextId, UUIDs.toUUID(rs.getBytes(1))));
                } else {
                    int userId = rs.getInt(2);
                    chunks.add(new ChunkInfo(userId <= 0 ? 0 : userId, contextId, UUIDs.toUUID(rs.getBytes(1))));
                }
            } while (rs.next());
            return chunks;
        } catch (SQLException e) {
            throw DeleteFailedExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } catch (Exception e) {
            throw DeleteFailedExceptionCodes.ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    private static void dropChunksFromDatabase(int optUserId, int contextId, Connection writeCon) throws OXException {
        PreparedStatement stmt = null;
        try {
            // Delete account data
            if (optUserId > 0) {
                stmt = writeCon.prepareStatement("DELETE FROM `s3_filestore` WHERE `cid` = ? AND `user` = ?");
                stmt.setInt(1, contextId);
                stmt.setInt(2, optUserId);
            } else {
                stmt = writeCon.prepareStatement("DELETE FROM `s3_filestore` WHERE `cid` = ?");
                stmt.setInt(1, contextId);
            }
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw DeleteFailedExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } catch (Exception e) {
            throw DeleteFailedExceptionCodes.ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(stmt);
        }
    }

    private static class ChunkInfo {

        final UUID s3Id;
        final int contextId;
        final int optUserId;

        ChunkInfo(int optUserId, int contextId, UUID s3Id) {
            super();
            this.s3Id = s3Id;
            this.contextId = contextId;
            this.optUserId = optUserId;
        }
    }

}
