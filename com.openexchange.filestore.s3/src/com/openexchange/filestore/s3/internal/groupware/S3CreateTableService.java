/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.filestore.s3.internal.groupware;

import com.openexchange.database.AbstractCreateTableImpl;
import com.openexchange.database.CreateTableService;


/**
 * {@link S3CreateTableService} - The S3 {@link CreateTableService}.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class S3CreateTableService extends AbstractCreateTableImpl {

    private static final String TABLE_S3_FILESTORE = "s3_filestore";

    private static final String CREATE_S3_FILESTORE = "CREATE TABLE `"+TABLE_S3_FILESTORE+"` (" +
        " `cid` INT4 unsigned NOT NULL," +
        " `user` INT4 unsigned NOT NULL," +
        " `document_id` BINARY(16) NOT NULL," +
        " `s3_id` BINARY(16) NOT NULL," +
        " `offset` BIGINT(64) NOT NULL," +
        " `length` BIGINT(64) NOT NULL," +
        " PRIMARY KEY (`cid`, `user`, `document_id`, `s3_id`)," +
        " UNIQUE KEY `s3_key` (`s3_id`)," +
        " INDEX `s3_index` (`cid`, `s3_id`)" +
        ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci";

    /**
     * Gets the table names.
     *
     * @return The table names.
     */
    public static String[] getTablesToCreate() {
        return new String[] { TABLE_S3_FILESTORE };
    }

    /**
     * Gets the CREATE-TABLE statements.
     *
     * @return The CREATE statements
     */
    public static String[] getCreateStmts() {
        return new String[] { CREATE_S3_FILESTORE };
    }

    /**
     * Initializes a new {@link CapabilityCreateTableService}.
     */
    public S3CreateTableService() {
        super();
    }

    @Override
    public String[] requiredTables() {
        return new String[] { "user", "infostore_document" };
    }

    @Override
    public String[] tablesToCreate() {
        return getTablesToCreate();
    }

    @Override
    protected String[] getCreateStatements() {
        return getCreateStmts();
    }

}
