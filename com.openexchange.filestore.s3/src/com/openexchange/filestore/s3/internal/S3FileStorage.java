/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.filestore.s3.internal;

import static com.openexchange.filestore.s3.internal.AbortIfNotConsumedInputStream.closeContentStream;
import static com.openexchange.filestore.s3.internal.S3ExceptionCode.wrap;
import static com.openexchange.java.Autoboxing.L;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.function.IntFunction;
import javax.servlet.http.HttpServletResponse;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Principal;
import com.amazonaws.auth.policy.Resource;
import com.amazonaws.auth.policy.Statement;
import com.amazonaws.auth.policy.actions.S3Actions;
import com.amazonaws.auth.policy.conditions.BooleanCondition;
import com.amazonaws.auth.policy.conditions.StringCondition;
import com.amazonaws.auth.policy.conditions.StringCondition.StringComparisonType;
import com.amazonaws.services.s3.Headers;
import com.amazonaws.services.s3.internal.BucketNameUtils;
import com.amazonaws.services.s3.model.AbortMultipartUploadRequest;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.CopyPartRequest;
import com.amazonaws.services.s3.model.CopyPartResult;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.MetadataDirective;
import com.amazonaws.services.s3.model.MultiObjectDeleteException;
import com.amazonaws.services.s3.model.MultiObjectDeleteException.DeleteError;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PartETag;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.Region;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.SetBucketPolicyRequest;
import com.google.common.collect.Lists;
import com.openexchange.ajax.container.ThresholdFileHolder;
import com.openexchange.exception.LogLevel;
import com.openexchange.exception.OXException;
import com.openexchange.filestore.FileStorage;
import com.openexchange.filestore.FileStorageCodes;
import com.openexchange.filestore.s3.internal.chunkstorage.Chunk;
import com.openexchange.filestore.s3.internal.chunkstorage.ChunkStorage;
import com.openexchange.filestore.s3.internal.client.S3FileStorageClient;
import com.openexchange.filestore.utils.TempFileHelper;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Streams;
import com.openexchange.java.Strings;
import com.openexchange.java.util.UUIDs;
import com.openexchange.log.LogUtility;

/**
 * {@link S3FileStorage} - The S3 file storage implementation.
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class S3FileStorage implements FileStorage {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(S3FileStorage.class);

    /** The delimiter character (<code>'/'</code>) to separate the prefix from the keys. */
    public static final char DELIMITER = '/';

    /** The minimum part size required for a multipart upload */
    private static final int MINIMUM_MULTIPART_SIZE = 5242880; /* 5 MB */

    /** Header for the original, unencrypted size of an encrypted object: {@value Headers#UNENCRYPTED_CONTENT_LENGTH} */
    private static final String UNENCRYPTED_CONTENT_LENGTH = Headers.UNENCRYPTED_CONTENT_LENGTH;

    private static final IntFunction<String[]> FUNCTION_STRING_ARRAY = size -> new String[size];

    private final URI uri;
    private final String prefix;
    private final String bucketName;
    private final S3FileStorageClient client;
    private final ChunkStorage chunkStorage;

    /**
     * Initializes a new {@link S3FileStorage}.
     *
     * @param uri The URI that fully qualifies this file storage
     * @param prefix The prefix to use; e.g. <code>"1337ctxstore"</code>
     * @param bucketName The bucket name to use
     * @param client The file storage client
     * @param chunkStorage The underlying chunk storage
     */
    public S3FileStorage(URI uri, String prefix, String bucketName, S3FileStorageClient client, ChunkStorage chunkStorage) {
        super();
        BucketNameUtils.validateBucketName(bucketName);
        if (Strings.isEmpty(prefix) || prefix.indexOf(DELIMITER) >= 0) {
            throw new IllegalArgumentException(prefix);
        }
        this.uri = uri;
        this.prefix = prefix;
        this.bucketName = bucketName;
        this.client = client;
        this.chunkStorage = chunkStorage;
        LOG.debug("S3 file storage initialized for \"{}/{}{}\"", bucketName, prefix, Character.valueOf(DELIMITER));
    }

    /**
     * Gets the prefix
     *
     * @return The prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Executes with retry the given closure with configured retry count.
     *
     * @param <R> The result type
     * @param closure The closure to invoke
     * @return The result
     * @throws SdkClientException If any errors are encountered in the client while making the request or handling the response
     * @throws AmazonServiceException If any errors occurred in Amazon S3 while processing the request
     */
    private void withVoidRetry(S3VoidOperationClosure closure) throws SdkClientException, AmazonServiceException {
        int retryCount = client.getNumOfRetryAttemptsOnConnectionPoolTimeout();
        new RetryingS3OperationExecutor<Void>(closure, retryCount <= 0 ? 1 : (retryCount + 1)).execute(client.getSdkClient());
    }

    /**
     * Executes with retry the given closure with configured retry count.
     *
     * @param <R> The result type
     * @param closure The closure to invoke
     * @return The result
     * @throws SdkClientException If any errors are encountered in the client while making the request or handling the response
     * @throws AmazonServiceException If any errors occurred in Amazon S3 while processing the request
     */
    private <R> R withRetry(S3OperationClosure<R> closure) throws SdkClientException, AmazonServiceException {
        int retryCount = client.getNumOfRetryAttemptsOnConnectionPoolTimeout();
        return new RetryingS3OperationExecutor<R>(closure, retryCount).execute(client.getSdkClient());
    }

    @Override
    public URI getUri() {
        return uri;
    }

    @Override
    public boolean isSpooling() {
        return true;
    }

    @Override
    public String saveNewFile(InputStream input) throws OXException {
        return UUIDs.getUnformattedString(saveNewFile(input, true));
    }

    /**
     * Saves specified content as new S3 object(s).
     *
     * @param input The content for the S3 object(s)
     * @param considerSpooling Whether to consider spooling given content to a temporary file or not
     * @return The document identifier
     * @throws OXException If saving new S3 object(s) fails
     */
    private UUID saveNewFile(InputStream input, boolean considerSpooling) throws OXException {
        /*
         * perform chunked upload as needed
         */
        File tmpFile = null;
        InputStream in = input;
        try {
            /*
             * spool to file
             */
            if (considerSpooling && !(in instanceof ByteArrayInputStream) && (!(in instanceof FileInputStream))) {
                Optional<File> optionalTempFile = TempFileHelper.getInstance().newTempFile();
                if (optionalTempFile.isPresent()) {
                    tmpFile = optionalTempFile.get();
                    in = Streams.transferToFileAndCreateStream(in, tmpFile);
                }
            }
            /*
             * proceed
             */
            UUID documentId = UUID.randomUUID();
            upload(documentId, in, 0);
            return documentId;
        } catch (IOException e) {
            throw FileStorageCodes.IOERROR.create(e, e.getMessage());
        } finally {
            Streams.close(in);
            TempFileHelper.deleteQuietly(tmpFile);
        }
    }

    /**
     * Performs a chunk-wise upload of the supplied data.
     *
     * @param documentId The identifier of the document
     * @param data The document data
     * @param offset The current offset, or <code>0</code> for new files
     * @param chunkSize The chunk size to use
     * @return The upload result
     * @throws OXException If upload fails
     */
    private UploadResult upload(UUID documentId, InputStream data, long offset) throws OXException {
        List<S3Identifier> s3Ids = new LinkedList<>();
        S3ChunkedUpload chunkedUpload = new S3ChunkedUpload(data, client.getEncryptionConfig().isClientEncryptionEnabled(), client.getChunkSize());
        try { // NOSONARLINT
            Thread currentThread = Thread.currentThread();
            long off = offset;
            while (chunkedUpload.hasNext()) {
                if (currentThread.isInterrupted()) {
                    throw OXException.general("Upload to S3 aborted");
                }
                S3UploadChunk uploadChunk = chunkedUpload.next();
                try {
                    S3Identifier key = generateKey();
                    uploadSingle(key, uploadChunk);
                    // Remember created S3 object for possible clean-up later on
                    s3Ids.add(key);
                    chunkStorage.storeChunk(new Chunk(documentId, key.getId(), off, uploadChunk.getSize()));
                    off += uploadChunk.getSize();
                } finally {
                    Streams.close(uploadChunk);
                }
            }
            UploadResult result = new UploadResult(s3Ids, off);
            s3Ids = null;
            return result;
        } finally {
            Streams.close(chunkedUpload);
            if (s3Ids != null) {
                for (List<S3Identifier> partition : Lists.partition(s3Ids, MAX_NUMBER_OF_KEYS_TO_DELETE)) {
                    DeleteObjectsRequest deleteRequest = new DeleteObjectsRequest(bucketName).withKeys(partition.stream().map(i -> i.getKey()).toArray(FUNCTION_STRING_ARRAY));
                    try {
                        withRetry(c -> c.deleteObjects(deleteRequest));
                    } catch (MultiObjectDeleteException e) {
                        LOG.debug("Failed to delete S3 objects", e);
                        List<DeleteError> errors = e.getErrors();
                        if (null != errors && !errors.isEmpty()) {
                            for (DeleteError error : errors) {
                                S3Identifier tmp = S3Identifier.optS3IdentifierFor(error.getKey()).orElse(null);
                                if (tmp != null) {
                                    LOG.warn("Failed to delete S3 object {} under key {}", UUIDs.getUnformattedStringObjectFor(tmp.getId()), tmp.getKey());
                                } else {
                                    LOG.warn("Failed to delete S3 object {}", error.getKey());
                                }
                            }
                        }
                    } catch (Exception e) {
                        LOG.warn("Failed to delete S3 objects", e);
                    }
                }
                try {
                    chunkStorage.deleteChunks(s3Ids.stream().map(i -> i.getId()).collect(CollectorUtils.toList(s3Ids.size())));
                } catch (Exception e) {
                    LOG.warn("Failed to delete S3 chunks", e);
                }
            }
        }
    }

    @Override
    public long getFileSize(String name) throws OXException {
        UUID documentId = UUIDs.fromUnformattedString(name);

        Optional<Chunk> optionalLastChunk = chunkStorage.getLastChunk(documentId);
        if (optionalLastChunk.isEmpty()) {
            // No chunks in chunk storage: document identifier == S3 identifier
            S3Identifier key = new S3Identifier(documentId, prefix);
            return getContentLength(key.getKey());
        }

        Chunk lastChunk = optionalLastChunk.get();
        return lastChunk.getOffset() + lastChunk.getLength();
    }

    @Override
    public InputStream getFile(String name) throws OXException {
        UUID documentId = UUIDs.fromUnformattedString(name);

        List<Chunk> chunks = chunkStorage.optChunks(documentId).orElse(null);
        if (null == chunks || chunks.isEmpty()) {
            // Not available in chunk storage: document identifier == S3 identifier
            return loadS3ObjectContent(documentId);
        }

        if (1 == chunks.size()) {
            // There is only one chunk
            return loadS3ObjectContent(chunks.get(0).getS3Id());
        }

        return new S3BufferedInputStream(chunks, this);
    }

    /**
     * Loads the content for specified S3 object.
     *
     * @param id The identifier of the S3 object
     * @return The S3 object content
     * @throws OXException If S3 object content cannot be loaded
     */
    public InputStream loadS3ObjectContent(UUID id) throws OXException {
        S3Identifier key = new S3Identifier(id, prefix);
        S3ObjectInputStream objectContent = null;
        try {
            objectContent = getObject(key.getKey()).getObjectContent();
            InputStream wrapper = wrapperWithoutRangeSupport(objectContent, key.getKey());
            objectContent = null; // Avoid premature closing
            return wrapper;
        } catch (AmazonClientException e) {
            throw wrap(e, key.getKey());
        } finally {
            closeContentStream(objectContent);
        }
    }

    private InputStream wrapperWithoutRangeSupport(S3ObjectInputStream objectContent, String key) {
        /*
         * Consume rather than skip on invocation of skip() if client-side encryption is enabled
         * since com.amazonaws.services.s3.internal.crypto.CipherLiteInputStream.skip(long) does
         * not read next chunk if more than currently buffered bytes are supposed to be skipped
         */
        boolean consumeBytesOnSkip = client.getEncryptionConfig().isClientEncryptionEnabled();
        return new ResumableAbortIfNotConsumedInputStream(objectContent, bucketName, key, consumeBytesOnSkip, client.getSdkClient());
    }

    @Override
    public InputStream getFile(String name, long offset, long length) throws OXException {
        // Check for 0 (zero) requested bytes
        if (length == 0) {
            return Streams.EMPTY_INPUT_STREAM;
        }

        UUID documentId = UUIDs.fromUnformattedString(name);

        List<Chunk> chunks = chunkStorage.optChunks(documentId).orElse(null);
        if (null == chunks || chunks.isEmpty()) {
            // Not available in chunk storage: document identifier == S3 identifier
            long fileSize = getFileSize(name);

            // Check validity of given offset/length arguments
            if (offset >= fileSize || (length >= 0 && length > fileSize - offset)) {
                throw FileStorageCodes.INVALID_RANGE.create(L(offset), L(length), name, L(fileSize));
            }
            long rangeEnd = (length > 0 ? (offset + length) : fileSize) - 1;
            return loadS3ObjectContentRange(documentId, offset, rangeEnd);
        }

        int size = chunks.size();
        if (1 == size) {
            Chunk chunk = chunks.get(0);

            // Check validity of given offset/length arguments
            if (offset >= chunk.getLength() || (length >= 0 && length > chunk.getLength() - offset)) {
                throw FileStorageCodes.INVALID_RANGE.create(L(offset), L(length), name, L(chunk.getLength()));
            }

            long rangeStart = 0 < offset ? offset : 0;
            long rangeEnd = (0 < length ? (rangeStart + length) : chunk.getLength()) - 1;
            return loadS3ObjectContentRange(chunk.getS3Id(), rangeStart, rangeEnd);
        }

        // Download from multiple chunks
        Chunk lastChunk = chunks.get(size - 1);
        long totalLength = lastChunk.getOffset() + lastChunk.getLength();
        if (offset >= totalLength || (length >= 0 && length > totalLength - offset)) {
            throw FileStorageCodes.INVALID_RANGE.create(L(offset), L(length), name, L(totalLength));
        }

        long rangeStart = 0 < offset ? offset : 0;
        long rangeEnd = (0 < length ? (rangeStart + length) : totalLength) - 1;
        return new S3BufferedInputStream(chunks, this, rangeStart, rangeEnd);
    }

    /**
     * Loads the range from the content of specified S3 object.
     *
     * @param id The identifier of the S3 object
     * @param rangeStart The range start in S3 object's content bytes
     * @param rangeEnd The range end in S3 object's content bytes
     * @return The S3 object content
     * @throws OXException If S3 object content cannot be loaded
     */
    public InputStream loadS3ObjectContentRange(UUID id, long rangeStart, long rangeEnd) throws OXException {
        S3Identifier key = new S3Identifier(id, prefix);

        GetObjectRequest request = new GetObjectRequest(bucketName, key.getKey());
        request.setRange(rangeStart, rangeEnd);

        // Return content stream
        S3ObjectInputStream objectContent = null;
        try {
            objectContent = withRetry(c -> c.getObject(request).getObjectContent());
            long[] range = new long[] { rangeStart, rangeEnd };
            InputStream wrapper = wrapperWithRangeSupport(objectContent, range, key.getKey());
            objectContent = null; // Avoid premature closing
            return wrapper;
        } catch (AmazonClientException e) {
            if ((e instanceof AmazonServiceException) && HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE == ((AmazonServiceException) e).getStatusCode()) {
                long objectSize = getContentLength(key.getKey());
                throw FileStorageCodes.INVALID_RANGE.create(e, L(rangeStart), L(rangeEnd - rangeStart + 1), UUIDs.getUnformattedString(id), L(objectSize));
            }
            throw wrap(e, key.getKey());
        } finally {
            closeContentStream(objectContent);
        }
    }

    private InputStream wrapperWithRangeSupport(S3ObjectInputStream objectContent, long[] range, String key) {
        /*
         * Consume rather than skip on invocation of skip() if client-side encryption is enabled
         * since com.amazonaws.services.s3.internal.crypto.CipherLiteInputStream.skip(long) does
         * not read next chunk if more than currently buffered bytes are supposed to be skipped
         */
        boolean consumeBytesOnSkip = client.getEncryptionConfig().isClientEncryptionEnabled();
        return new RangeAcceptingResumableAbortIfNotConsumedInputStream(objectContent, range, bucketName, key, consumeBytesOnSkip, client.getSdkClient());
    }

    @Override
    public SortedSet<String> getFileList() throws OXException {
        // Retrieve known document identifiers from chunk storage
        Map<UUID, List<UUID>> documents = chunkStorage.getDocuments();

        // Remove from retrieved list of S3 identifiers those already known to chunk storage
        SortedSet<UUID> s3ObjectIds = loadS3ObjectIds();
        for (List<UUID> knownS3ObjectIds : documents.values()) {
            s3ObjectIds.removeAll(knownS3ObjectIds);
        }

        // Fill resulting sorted set with document identifiers from chunk storage and remaining S3 identifiers
        SortedSet<String> files = new TreeSet<String>();
        for (UUID documentId : documents.keySet()) {
            files.add(UUIDs.getUnformattedString(documentId));
        }
        files.addAll(s3ObjectIds.stream().map(UUIDs::getUnformattedString).collect(CollectorUtils.toList(s3ObjectIds.size())));

        return files;
    }

    private SortedSet<UUID> loadS3ObjectIds() {
        SortedSet<UUID> files = new TreeSet<>();
        /*
         * results may be paginated - repeat listing objects as long as result is truncated
         */
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName).withDelimiter(String.valueOf(DELIMITER)).withPrefix(new StringBuilder(prefix).append(DELIMITER).toString());
        ObjectListing objectListing;
        do {
            objectListing = withRetry(c -> c.listObjects(listObjectsRequest));
            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
                files.add(S3Identifier.s3IdentifierFor(objectSummary.getKey()).getId());
            }
            listObjectsRequest.setMarker(objectListing.getNextMarker());
        } while (objectListing.isTruncated());
        return files;
    }

    @Override
    public String getMimeType(String name) throws OXException {
        // TODO: makes no sense at storage layer
        return getMetadata(new S3Identifier(UUIDs.fromUnformattedString(name), prefix).getKey()).getContentType();
    }

    @Override
    public boolean deleteFile(String name) throws OXException {
        UUID documentId = UUIDs.fromUnformattedString(name);
        Optional<List<Chunk>> optionalChunks = chunkStorage.optChunks(documentId);
        if (optionalChunks.isPresent()) {
            List<Chunk> chunks = optionalChunks.get();
            int size = chunks.size();
            if (0 >= size) {
                // Apparently no such document exists; consider as deleted
                return true;
            }

            List<UUID> s3Ids = chunks.stream().map(Chunk::getS3Id).collect(CollectorUtils.toList(chunks.size()));
            deleteS3Objects(s3Ids);
            chunkStorage.deleteDocument(documentId);
            return true;
        }

        deleteS3Object(documentId);
        return true;
    }

    /**
     * Deletes the specified S3 object from S3 storage.
     *
     * @param id The S3 object identifier
     * @throws OXException If deletion fails
     */
    public void deleteS3Object(UUID id) throws OXException {
        S3Identifier key = new S3Identifier(id, prefix);
        try {
            withVoidRetry(c -> c.deleteObject(bucketName, key.getKey()));
        } catch (AmazonClientException e) {
            throw wrap(e, key.getKey());
        }
    }

    /**
     * The max. number of keys that are allowed being passed by a multiple delete objects request.
     * <p>
     * <a href="https://docs.aws.amazon.com/AmazonS3/latest/API/API_DeleteObjects.html">See here</a>
     */
    private static final int MAX_NUMBER_OF_KEYS_TO_DELETE = 1000;

    @Override
    public Set<String> deleteFiles(String[] names) throws OXException {
        if (null == names || 0 >= names.length) {
            return Collections.emptySet();
        }

        Set<String> notDeleted = new HashSet<String>();
        for (String identifier : names) {
            if (!deleteFile(identifier)) {
                notDeleted.add(identifier);
            }
        }
        return notDeleted;
    }

    /**
     * Deletes specified S3 objects.
     *
     * @param ids The S3 object identifiers
     * @return The identifiers of those S3 object that could not be deleted
     * @throws OXException If deletion fails
     */
    public Set<String> deleteS3Objects(List<UUID> ids) throws OXException {
        if (null == ids || ids.isEmpty()) {
            return Collections.emptySet();
        }

        Set<String> notDeleted = new HashSet<String>();
        for (List<UUID> partition : Lists.partition(ids, MAX_NUMBER_OF_KEYS_TO_DELETE)) {
            String[] keys = partition.stream().map(i -> new S3Identifier(i, prefix)).map(i -> i.getKey()).toArray(FUNCTION_STRING_ARRAY);
            DeleteObjectsRequest deleteRequest = new DeleteObjectsRequest(bucketName).withKeys(keys);
            try {
                withRetry(c -> c.deleteObjects(deleteRequest));
            } catch (MultiObjectDeleteException e) {
                LOG.debug("Failed to delete S3 objects {}", LogUtility.toStringObjectFor(keys), e);
                List<DeleteError> errors = e.getErrors();
                if (null != errors && !errors.isEmpty()) {
                    for (DeleteError error : errors) {
                        notDeleted.add(UUIDs.getUnformattedString(S3Identifier.s3IdentifierFor(error.getKey()).getId()));
                    }
                }
            } catch (AmazonClientException e) {
                throw wrap(e);
            }
        }
        return notDeleted;
    }

    @Override
    public void remove() throws OXException {
        try {
            /*
             * try and delete all contained files repeatedly
             */
            final int RETRY_COUNT = 10;
            for (int i = 0; i < RETRY_COUNT; i++) {
                SortedSet<String> fileList = getFileList();
                if (null == fileList || fileList.isEmpty()) {
                    return; // no more files found
                }
                Set<String> notDeleted = deleteFiles(fileList.toArray(new String[fileList.size()]));
                if (notDeleted != null && !notDeleted.isEmpty()) {
                    if (i < RETRY_COUNT - 1) {
                        LOG.warn("Not all files in bucket deleted yet, trying again.");
                    } else {
                        throw FileStorageCodes.NOT_ELIMINATED.create("Not all files in bucket deleted after " + i + " tries, giving up.");
                    }
                }
            }
        } catch (OXException e) {
            throw FileStorageCodes.NOT_ELIMINATED.create(e);
        } catch (AmazonClientException e) {
            throw FileStorageCodes.NOT_ELIMINATED.create(wrap(e));
        }
    }

    @Override
    public void recreateStateFile() throws OXException {
        // no
    }

    @Override
    public boolean stateFileIsCorrect() throws OXException {
        return true;
    }


    @Override
    public long appendToFile(InputStream file, String name, long offset) throws OXException {
        InputStream in = file;
        File tmpFile = null;
        try {
            /*
             * Get the size of the document to add the data to
             */
            long fileSize = getFileSize(name);
            if (fileSize != offset) {
                throw FileStorageCodes.INVALID_OFFSET.create(Long.valueOf(offset), name, Long.valueOf(fileSize));
            }

            UUID documentId = UUIDs.fromUnformattedString(name);
            Chunk lastChunk = chunkStorage.getLastChunk(documentId).orElse(null);
            if (lastChunk == null) {
                // Add existent S3 object as first chunk
                lastChunk = new Chunk(documentId, documentId, 0, fileSize);
                chunkStorage.storeChunk(lastChunk);
            }

            long currentSize = lastChunk.getOffset() + lastChunk.getLength();
            if (offset != currentSize) {
                throw FileStorageCodes.INVALID_OFFSET.create(L(offset), name, L(currentSize));
            }

            /*
             * spool to file
             */
            if (!(in instanceof ByteArrayInputStream) && !(in instanceof FileInputStream)) {
                Optional<File> optionalTempFile = TempFileHelper.getInstance().newTempFile();
                if (optionalTempFile.isPresent()) {
                    tmpFile = optionalTempFile.get();
                    in = Streams.transferToFileAndCreateStream(in, tmpFile);
                }
            }
            return upload(documentId, in, offset).totalSize;
        } catch (IOException e) {
            throw FileStorageCodes.IOERROR.create(e, e.getMessage());
        } finally {
            Streams.close(in);
            TempFileHelper.deleteQuietly(tmpFile);
        }
    }

    /**
     * Set the new file length of an existing S3 object  (shortens an object)
     * <p>
     * This method uses a <i>UploadPartCopy</i> ({@link CopyPartRequest}) within a multipart upload request in order to create a new temporary S3 object with shortened data.
     * Afterwards the temporary object replaces the original.
     * </p>
     * <p>
     * Due to S3 restrictions, a UploadPartCopy is only possible if the source file, is larger than 5 MB.
     * </p>
     *
     * @param length The new length of object
     * @param key The fully-qualifiyng key of the S3 object
     * @throws OXException
     */
    private void setFileLengthWithCopyPart(long length, String key) throws OXException {
        /*
         * Initialize the multipart upload
         */
        S3Identifier tmpKey = generateKey();
        InitiateMultipartUploadRequest initiateMultipartUploadRequest = new InitiateMultipartUploadRequest(bucketName, tmpKey.getKey()).withObjectMetadata(prepareMetadataForSSE(new ObjectMetadata()));
        String uploadID = withRetry(c -> c.initiateMultipartUpload(initiateMultipartUploadRequest).getUploadId());
        boolean uploadCompleted = false;

        try {
            /*
             * Copy the object in parts to a temporary S3 object with the desired length
             */
            long partSize = 5 * 1024 * 1024L;
            long bytePosition = 0;
            int partNum = 1;
            List<PartETag> parts = new ArrayList<PartETag>();
            while (bytePosition < length) {

                // The last part might be smaller than partSize, so check to make sure
                // that lastByte isn't beyond the end of the object.
                long lastByte = Math.min(bytePosition + partSize - 1, length - 1);

                // Copy this part.
                //@formatter:off
                CopyPartRequest copyRequest = new CopyPartRequest()
                    .withSourceBucketName(bucketName)
                    .withSourceKey(key)
                    .withDestinationBucketName(bucketName)
                    .withDestinationKey(tmpKey.getKey())
                    .withUploadId(uploadID)
                    .withFirstByte(L(bytePosition))
                    .withLastByte(L(lastByte))
                    .withPartNumber(partNum++);
                //@formatter:on
                CopyPartResult copyPartResult = withRetry(c -> c.copyPart(copyRequest));
                parts.add(copyPartResult.getPartETag());
                bytePosition += partSize;
            }

            /*
             * complete the request
             */
            withRetry(c -> c.completeMultipartUpload(new CompleteMultipartUploadRequest(bucketName, tmpKey.getKey(), uploadID, parts)));
            uploadCompleted = true;

            /*
             * replace the original
             */
            CopyObjectRequest copyObjectRequest = new CopyObjectRequest(bucketName, tmpKey.getKey(), bucketName, key);
            ObjectMetadata metadata = prepareMetadataForSSE(new ObjectMetadata());
            copyObjectRequest.setNewObjectMetadata(metadata);
            withRetry(c -> c.copyObject(copyObjectRequest));
            withVoidRetry(c -> c.deleteObject(bucketName, tmpKey.getKey()));
        } catch (AmazonClientException e) {
            throw wrap(e, key);
        } finally {
            if (!uploadCompleted) {
                try {
                    withVoidRetry(c -> c.abortMultipartUpload(new AbortMultipartUploadRequest(bucketName, tmpKey.getKey(), uploadID)));
                } catch (AmazonClientException e) {
                    LOG.warn("Error aborting multipart upload", e);
                }
            }
        }
    }

    /**
     * Set the new file length of an existing S3 object  (shortens an object)
     * <p>
     * This method downloads the existing S3 object and uploads it again, up to the desired new length.
     * </p>
     *
     * @param file The data to append as {@link InputStream}
     * @param key The fully-qualifiyng key of the S3 object
     * @return The new size of the modified object
     * @throws OXException
     */
    private void setFileLengthWithSpooling(long length, String key) throws OXException {
        /*
         * copy previous file to temporary file
         */
        S3Identifier tempKey = generateKey();
        try {
            CopyObjectRequest copyObjectRequest = new CopyObjectRequest(bucketName, key, bucketName, tempKey.getKey());
            ObjectMetadata metadata = prepareMetadataForSSE(new ObjectMetadata());
            copyObjectRequest.setNewObjectMetadata(metadata);
            withRetry(c -> c.copyObject(copyObjectRequest));
            /*
             * upload $length bytes from previous file to new current file
             */
            metadata = new ObjectMetadata();
            metadata.setContentLength(length);
            metadata = prepareMetadataForSSE(metadata);
            InputStream inputStream = getFile(tempKey.getKey(), 0, length);
            try {
                ObjectMetadata meta = metadata;
                withRetry(c -> c.putObject(bucketName, key, inputStream, meta));
            } finally {
                Streams.close(inputStream);
            }
        } catch (AmazonClientException e) {
            throw wrap(e, key);
        } finally {
            try {
                withVoidRetry(c -> c.deleteObject(bucketName, tempKey.getKey()));
            } catch (AmazonClientException e) {
                LOG.warn("Error cleaning up temporary file", e);
            }
        }
    }

    @Override
    public void setFileLength(long length, String name) throws OXException {
        UUID documentId = UUIDs.fromUnformattedString(name);
        List<Chunk> chunks = chunkStorage.optChunks(documentId).orElse(null);
        if (null == chunks || chunks.isEmpty()) {
            /*-
             * No Chunks. Assume document identifier is the S3 object identifier.
             *
             * Get the current length of the object
             */
            String key = new S3Identifier(documentId, prefix).getKey();
            long contentLength = getContentLength(key);
            if (contentLength == length) {
                // The file already has the desired length
                return;
            }

            /**
             * Set the new file length: CopyPart/UploadPartCopy is preferred, because it does not require
             * to download the S3 object. But: UploadPartCopy might not be available/possible in all situations
             */
            if (isCopyPartAvailable(contentLength)) {
                setFileLengthWithCopyPart(length, key);
            } else {
                setFileLengthWithSpooling(length, key);
            }
            return;
        }

        // Chunks available
        for (Chunk chunk : chunks) {
            if (chunk.getOffset() >= length) {
                /*
                 * delete the whole chunk
                 */
                deleteS3Object(chunk.getS3Id());
                chunkStorage.deleteChunk(chunk.getS3Id());
            } else if (chunk.getOffset() < length && length <= chunk.getOffset() + chunk.getLength()) {
                /*
                 * trim the last chunk
                 */
                long newChunkLength = chunk.getOffset() + chunk.getLength() - length;
                ThresholdFileHolder fileHolder = null;
                try {
                    fileHolder = new ThresholdFileHolder();
                    byte[] md5digest = null;
                    if (client.getEncryptionConfig().isClientEncryptionEnabled()) {
                        fileHolder.write(loadS3ObjectContentRange(chunk.getS3Id(), 0, newChunkLength));
                    } else {
                        DigestInputStream digestStream = new DigestInputStream(loadS3ObjectContentRange(chunk.getS3Id(), 0, newChunkLength), MessageDigest.getInstance("MD5"));
                        fileHolder.write(digestStream);
                        md5digest = digestStream.getMessageDigest().digest();
                    }

                    S3Identifier key = generateKey();
                    uploadSingle(key, new S3UploadChunk(fileHolder, md5digest));
                    chunkStorage.storeChunk(new Chunk(documentId, key.getId(), chunk.getOffset(), newChunkLength));
                } catch (NoSuchAlgorithmException e) {
                    throw FileStorageCodes.IOERROR.create(e, e.getMessage());
                } finally {
                    Streams.close(fileHolder);
                }
                deleteS3Object(chunk.getS3Id());
                chunkStorage.deleteChunk(chunk.getS3Id());
                break;
            }
        }
    }

    /**
     * Ensures the configured bucket exists, creating it dynamically if needed.
     *
     * @throws OXException If initialization fails
     */
    public void ensureBucket() throws OXException {
        boolean bucketExists = false;
        try {
            bucketExists = withRetry(c -> Boolean.valueOf(c.doesBucketExist(bucketName))).booleanValue();
        } catch (AmazonClientException e) {
            throw S3ExceptionCode.wrap(e);
        } catch (RuntimeException e) {
            throw S3ExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }

        if (!bucketExists) {
            // Bucket does not yet exist
            String region = client.getSdkClient().getRegionName();
            try {
                withRetry(c -> c.createBucket(new CreateBucketRequest(bucketName, Region.fromValue(region))));
                if (client.getEncryptionConfig().isServerSideEncryptionEnabled()) {
                    withVoidRetry(c -> c.setBucketPolicy(new SetBucketPolicyRequest(bucketName, getSSEOnlyBucketPolicy(bucketName))));
                }
            } catch (AmazonS3Exception e) {
                if ("InvalidLocationConstraint".equals(e.getErrorCode())) {
                    // Failed to create such a bucket
                    throw S3ExceptionCode.BUCKET_CREATION_FAILED.create(bucketName, region);
                }
                throw S3ExceptionCode.wrap(e);
            } catch (AmazonClientException e) {
                throw S3ExceptionCode.wrap(e);
            } catch (RuntimeException e) {
                throw S3ExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
            }
        }
    }

    /**
     * Gets the bucket policy for a server side encryption only bucket.
     *
     * @param bucketName The name of the bucket
     * @return The encryption only policy
     */
    private static String getSSEOnlyBucketPolicy(String bucketName) {
        Policy bucketPolicy = new Policy().withStatements(
            new Statement(Statement.Effect.Deny)
                .withId("DenyIncorrectEncryptionHeader")
                .withPrincipals(Principal.AllUsers)
                .withActions(S3Actions.PutObject)
                .withResources(new Resource("arn:aws:s3:::" + bucketName + "/*"))
                .withConditions(new StringCondition(StringComparisonType.StringNotEquals, "s3:x-amz-server-side-encryption", "AES256")),
            new Statement(Statement.Effect.Deny)
                .withId("DenyUnEncryptedObjectUploads")
                .withPrincipals(Principal.AllUsers)
                .withActions(S3Actions.PutObject)
                .withResources(new Resource("arn:aws:s3:::" + bucketName + "/*"))
                .withConditions(new BooleanCondition("s3:x-amz-server-side-encryption", true))
                );
        return bucketPolicy.toJson();
    }

    /**
     * Gets metadata for an existing file.
     *
     * @param key The (full) key for the new file; no additional prefix will be prepended implicitly
     * @return The upload ID for the multipart upload
     * @throws OXException
     */
    private ObjectMetadata getMetadata(String key) throws OXException {
        try {
            return withRetry(c -> c.getObjectMetadata(bucketName, key));
        } catch (AmazonClientException e) {
            throw wrap(e, key);
        }
    }

    /**
     * Gets a stored S3 object.
     *
     * @param key The key of the file
     * @return The S3 object
     * @throws OXException
     */
    S3Object getObject(String key) throws OXException {
        try {
            return withRetry(c -> c.getObject(bucketName, key));
        } catch (AmazonClientException e) {
            throw wrap(e, key);
        }
    }

    /**
     * Creates a new arbitrary key (an unformatted string representation of a new random UUID), optionally prepended with the configured
     * prefix and delimiter.
     *
     * @return A new UID string, optionally with prefix and delimiter, e.g. <code>[prefix]/067e61623b6f4ae2a1712470b63dff00</code>.
     */
    private S3Identifier generateKey() {
        return new S3Identifier(UUID.randomUUID(), prefix);
    }

    /**
     * Puts a single upload chunk to amazon s3.
     *
     * @param key The object key; e.g. <code>"1337ctxstore/067e61623b6f4ae2a1712470b63dff00"</code>
     * @param chunk The chunk to store
     * @return The put object result passed from the client
     */
    private PutObjectResult uploadSingle(S3Identifier key, S3UploadChunk chunk) throws OXException {
        ObjectMetadata metadata = prepareMetadataForSSE(new ObjectMetadata());
        if (client.getEncryptionConfig().isClientEncryptionEnabled()) {
            metadata.addUserMetadata(UNENCRYPTED_CONTENT_LENGTH, Long.toString(chunk.getSize()));
        } else {
            metadata.setContentLength(chunk.getSize());
            metadata.setContentMD5(chunk.getMD5Digest());
        }

        PutObjectResult result;
        InputStream data = null;
        try {
            PutObjectRequest request;
            Optional<File> optTempFile = chunk.getTempFile();
            if (optTempFile.isPresent()) {
                request = new PutObjectRequest(bucketName, key.getKey(), optTempFile.get());
                request.setMetadata(metadata);
            } else {
                // It is a ByteArrayInputStream if temporary file is absent
                data = chunk.getData();
                request = new PutObjectRequest(bucketName, key.getKey(), data, metadata);
                request.getRequestClientOptions().setReadLimit((int) chunk.getSize() + 1);
            }
            result = withRetry(c -> c.putObject(request));
        } finally {
            Streams.close(data);
        }
        applyUnencryptedContentLengthIfNeeded(key.getKey(), chunk.getSize());
        return result;
    }

    private ObjectMetadata prepareMetadataForSSE(ObjectMetadata metadata) {
        if (client.getEncryptionConfig().isServerSideEncryptionEnabled()) {
            metadata.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);
        }
        return metadata;
    }

    /**
     * Retrieves the object metadata for an existing file and extracts the effective content length, which is the length of the
     * unencrypted content if specified, or the plain content length, otherwise.
     *
     * @param key The (full) key of the file to get the actual content length for; no additional prefix will be prepended implicitly
     * @return The length of the unencrypted content if specified, or the plain content length, otherwise
     */
    private long getContentLength(String key) throws OXException {
        ObjectMetadata metadata = getMetadata(key);
        OptionalLong unencryptedContentLength = optUnencryptedContentLength(metadata);
        if (unencryptedContentLength.isPresent()) {
            return unencryptedContentLength.getAsLong();
        }
        if (client.getEncryptionConfig().isClientEncryptionEnabled() && Strings.isNotEmpty(metadata.getUserMetaDataOf(Headers.CRYPTO_IV))) {
            String logKey = UNENCRYPTED_CONTENT_LENGTH + ':' + bucketName;
            LogUtility.logOncePerDay(logKey, LOG, LogLevel.WARNING, LogLevel.DEBUG,
                "No value for \"{}\" found in metadata of object \"{}\", but client-side encryption is enabled and \"{}\" is present in metadata - content length may not be accurate.",
                UNENCRYPTED_CONTENT_LENGTH, key, Headers.CRYPTO_IV);
        }
        return metadata.getContentLength();
    }

    /**
     * Optionally gets and parses the value for special header {@value #UNENCRYPTED_CONTENT_LENGTH} from
     * the supplied object metadata.
     *
     * @param metadata The metadata to extract the value from
     * @return The value as {@link OptionalLong}, or {@link OptionalLong#empty()} if not present or parseable
     */
    private static OptionalLong optUnencryptedContentLength(ObjectMetadata metadata) {
        String value = metadata.getUserMetaDataOf(UNENCRYPTED_CONTENT_LENGTH);
        long longValue = Strings.parseUnsignedLong(value);
        return -1 == longValue ? OptionalLong.empty() : OptionalLong.of(longValue);
    }

    /**
     * Ensures that the special {@value Headers#UNENCRYPTED_CONTENT_LENGTH} header is present in the object's
     * metadata and set to the given value if required, which is when client-side encryption is enabled and the value is missing or
     * different from the expected one.
     *
     * @param key The (full) key of the file to apply the unencrypted content length for; no additional prefix will be prepended implicitly
     * @param unencryptedContentLength The value to apply
     */
    private void applyUnencryptedContentLengthIfNeeded(String key, long unencryptedContentLength) {
        if (client.getEncryptionConfig().isClientEncryptionEnabled()) {
            ObjectMetadata metadata = withRetry(c -> c.getObjectMetadata(bucketName, key));
            if (unencryptedContentLength != optUnencryptedContentLength(metadata).orElse(-1L)) {
                ObjectMetadata newMetadata = cloneMetadata(metadata);
                newMetadata.addUserMetadata(UNENCRYPTED_CONTENT_LENGTH, Long.toString(unencryptedContentLength));
                CopyObjectRequest request = new CopyObjectRequest(bucketName, key, bucketName, key)
                    .withMetadataDirective(MetadataDirective.REPLACE).withNewObjectMetadata(newMetadata);
                withVoidRetry(c -> c.copyObject(request));
                LOG.debug("Successfully applied \"{} = {}\" for object \"{}\"", UNENCRYPTED_CONTENT_LENGTH, L(unencryptedContentLength), key);
            }
        }
    }

    /**
     * Clones the given metadata, ensuring that some default headers are taken over as expected.
     *
     * @param source The metadata to clone
     * @return The cloned metadata
     */
    private static ObjectMetadata cloneMetadata(ObjectMetadata source) {
        if (null == source) {
            return null;
        }
        ObjectMetadata clonedMetadata = source.clone();
        /*
         * auto-correct case in some default headers
         */
        for (String headerName : new String[] { Headers.CONTENT_LENGTH, Headers.CONTENT_TYPE, Headers.DATE, Headers.ETAG, Headers.LAST_MODIFIED }) {
            Object value = source.getRawMetadataValue(headerName);
            if (null != value) {
                clonedMetadata.setHeader(headerName, value);
            }
        }
        return clonedMetadata;
    }

    /**
     * Checks if "CopyPart" request can be used during a multipart upload or not.
     *
     * @param contentLength The content length of the object representing the part to be copied
     * @return <code>true</code> if "CopyPart" can be used, <code>false</code> otherwise
     */
    private boolean isCopyPartAvailable(long contentLength) {
        return client.useUploadPartCopy() && !client.getEncryptionConfig().isClientEncryptionEnabled() && contentLength >= MINIMUM_MULTIPART_SIZE;
    }

    /**
     * Performs an upload of the supplied data.
     *
     * @param data The document data
     * @return The upload result
     * @throws OXException If upload fails
     */
    private UploadResult upload(InputStream data) throws OXException {
        ThresholdFileHolder fileHolder = null;
        try {
            fileHolder = new ThresholdFileHolder();
            byte[] md5digest = null;
            if (client.getEncryptionConfig().isClientEncryptionEnabled()) {
                fileHolder.write(data);
            } else {
                DigestInputStream digestStream = new DigestInputStream(data, MessageDigest.getInstance("MD5"));
                fileHolder.write(digestStream);
                md5digest = digestStream.getMessageDigest().digest();
            }

            S3Identifier key = generateKey();
            uploadSingle(key, new S3UploadChunk(fileHolder, md5digest));
            return new UploadResult(Collections.singletonList(key), fileHolder.getLength());
        } catch (NoSuchAlgorithmException e) {
            throw FileStorageCodes.IOERROR.create(e, e.getMessage());
        } finally {
            Streams.close(fileHolder);
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** The result for a (possibly chunked) upload to S3 storage */
    private static class UploadResult {

        /** The identifiers of the created S3 objects representing each chunk of the document/file */
        final List<S3Identifier> s3Identifiers;

        /** The new total size of the document/file (possibly consisting of multiple S3 objects) */
        final long totalSize;

        /**
         * Initializes a new instance of {@link UploadResult}.
         *
         * @param s3Identifiers The identifiers of the created S3 objects
         * @param totalSize The new total size of the document/file
         */
        UploadResult(List<S3Identifier> s3Identifiers, long totalSize) {
            super();
            this.s3Identifiers = s3Identifiers;
            this.totalSize = totalSize;
        }
    }

}
