/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.filestore.s3.internal;

import static com.openexchange.filestore.s3.internal.S3FileStorage.DELIMITER;
import java.util.Optional;
import java.util.UUID;
import com.openexchange.java.Strings;
import com.openexchange.java.util.UUIDs;

/**
 * {@link S3Identifier} - Represents an identifier for a certain object in S3 storage.
 * <pre>
 *   [prefix] + "/" + [object-identifier]
 * </pre>
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class S3Identifier implements Comparable<S3Identifier> {

    /**
     * (Optionally) Gets the S3 identifier for specified key; e.g. <code>"1337ctxstore/067e61623b6f4ae2a1712470b63dff00"</code>.
     *
     * @param key The key to parse
     * @return The S3 identifier or empty
     */
    public static Optional<S3Identifier> optS3IdentifierFor(String key) {
        try {
            return Optional.of(s3IdentifierFor(key));
        } catch (IllegalArgumentException e) { // NOSONARLINT
            return Optional.empty();
        }
    }

    /**
     * Gets the S3 identifier for specified key; e.g. <code>"1337ctxstore/067e61623b6f4ae2a1712470b63dff00"</code>.
     *
     * @param key The key to parse
     * @return The S3 identifier
     * @throws IllegalArgumentException If specified key is invalid
     */
    public static S3Identifier s3IdentifierFor(String key) {
        if (Strings.isEmpty(key)) {
            throw new IllegalArgumentException("Key must not be null or empty");
        }
        int pos = key.indexOf(DELIMITER);
        if (pos <= 0) {
            throw new IllegalArgumentException("Invalid key: " + key);
        }
        return new S3Identifier(UUIDs.fromUnformattedString(key.substring(pos + 1)), key.substring(0, pos));
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final UUID id;
    private final String prefix;
    private final String key;
    private int hash;

    /**
     * Initializes a new instance of {@link S3Identifier}.
     *
     * @param id The identifier of the S3 object; e.g. <code>067e61623b6f4ae2a1712470b63dff00</code>
     * @param prefix The prefix; e.g. <code>"1337ctxstore"</code>
     */
    public S3Identifier(UUID id, String prefix) {
        super();
        this.id = id;
        this.prefix = prefix;
        this.key = new StringBuilder(prefix).append(DELIMITER).append(UUIDs.getUnformattedString(id)).toString();
    }

    /**
     * Gets the (UUID) identifier of the S3 object; e.g. <code>067e61623b6f4ae2a1712470b63dff00</code>.
     *
     * @return The (UUID) identifier of the S3 object
     */
    public UUID getId() {
        return id;
    }

    /**
     * Gets the prefix; e.g. <code>"1337ctxstore"</code>.
     *
     * @return The prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Gets fully-qualifying key (incl. prefix) under which the referenced S3 object is stored.
     * <p>
     * Example:
     * <pre>
     *   "1337ctxstore/067e61623b6f4ae2a1712470b63dff00"
     * </pre>.
     *
     * @return The key
     */
    public String getKey() {
        return key;
    }

    @Override
    public int hashCode() {
        int result = hash;
        if (result == 0) {
            int prime = 31;
            result = 1;
            result = prime * result + ((id == null) ? 0 : id.hashCode());
            result = prime * result + ((prefix == null) ? 0 : prefix.hashCode());
            hash = result;
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        S3Identifier other = (S3Identifier) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (prefix == null) {
            if (other.prefix != null) {
                return false;
            }
        } else if (!prefix.equals(other.prefix)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(S3Identifier o) {
        int c = Strings.compare(prefix, o.prefix);
        return c == 0 ? id.compareTo(o.id) : c;
    }

    @Override
    public String toString() {
        return getKey();
    }

}
