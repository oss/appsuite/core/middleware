/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.password.mechanism.impl.mech;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import com.openexchange.exception.OXException;
import com.openexchange.password.mechanism.PasswordDetails;
import com.openexchange.password.mechanism.PasswordMech;
import com.openexchange.password.mechanism.stock.StockPasswordMechs;

/**
 * {@link PasswordMechTests}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v7.10.2
 */
public class PasswordMechTests {

    private static final String PASSWORD = "secret";

    // Provide PasswordMech instances for parameterized tests
    static Stream<PasswordMech> passwordMechProvider() {
        return Stream.of(
            StockPasswordMechs.BCRYPT.getPasswordMech(),
            StockPasswordMechs.CRYPT.getPasswordMech(),
            StockPasswordMechs.SHA1.getPasswordMech(),
            StockPasswordMechs.SHA256.getPasswordMech(),
            StockPasswordMechs.SHA512.getPasswordMech()
        );
    }

    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("passwordMechProvider")
    public void testEncode_withPassword_handleProperly(PasswordMech passwordMech) throws OXException {
        PasswordDetails encode = passwordMech.encode(PASSWORD);

        assertEquals(PASSWORD, encode.getPlainPassword());
        assertEquals(passwordMech.getIdentifier(), encode.getPasswordMech());
        assertNotNull(encode.getEncodedPassword());

        boolean check = passwordMech.check(PASSWORD, encode.getEncodedPassword(), encode.getSalt());
        assertTrue(check);
    }

    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("passwordMechProvider")
    public void testEncode_passwordNull_handleProperly(PasswordMech passwordMech) throws OXException {
        PasswordDetails encode = passwordMech.encode(null);

        assertNull(encode.getPlainPassword());
        assertNull(encode.getEncodedPassword());
        assertEquals(passwordMech.getIdentifier(), encode.getPasswordMech());

        boolean check = passwordMech.check(null, encode.getEncodedPassword(), encode.getSalt());
        assertTrue(check);
    }

    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("passwordMechProvider")
    public void testEncode_passwordEmpty_handleProperly(PasswordMech passwordMech) throws OXException {
        PasswordDetails encode = passwordMech.encode("");

        assertEquals("", encode.getPlainPassword());
        assertNull(encode.getEncodedPassword());
        assertEquals(passwordMech.getIdentifier(), encode.getPasswordMech());

        boolean check = passwordMech.check("", encode.getEncodedPassword(), encode.getSalt());
        assertTrue(check);
    }
}