/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.exportpdf.converters;

import java.io.IOException;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.mail.exportpdf.InternalConverterProperties;
import com.openexchange.mail.exportpdf.MailExportConverterOptions;
import com.openexchange.mail.exportpdf.MailExportExceptionCode;
import com.openexchange.mail.exportpdf.MailExportMailPartContainer;
import com.openexchange.mail.exportpdf.converter.MailExportConversionResult;
import com.openexchange.mail.exportpdf.converter.MailExportConversionResult.Status;
import com.openexchange.mail.exportpdf.converter.WriteThroughMailExportConversionResult;

/**
 * {@link ImageMailExportConverter} - Write-through image converter
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class ImageMailExportConverter extends AbstractWriteThroughMailExportConverter {

    /**
     * Initialises a new {@link ImageMailExportConverter}.
     */
    public ImageMailExportConverter(LeanConfigurationService leanConfigService) {
        super(leanConfigService, InternalConverterProperties.IMAGE_FILE_EXTENSIONS);
    }

    @Override
    public MailExportConversionResult convert(MailExportMailPartContainer mailPart, MailExportConverterOptions options) throws OXException {
        try {
            if(MailExportConverterUtil.isSupportedImageFile(mailPart.getFileName())) {
                if (MailExportConverterUtil.isCorrupt(mailPart.getInputStream())) {
                    return new WriteThroughMailExportConversionResult(Status.ATTACHMENT_CORRUPT);
                }
            }
        } catch (IOException e) {
            throw MailExportExceptionCode.IO_ERROR.create(e);
        }
        return super.convert(mailPart, options);
    }
}
