package com.openexchange.rest.services.configuration.request.analyzer;

import java.util.List;
import java.util.Optional;
import javax.ws.rs.Path;
import com.openexchange.database.ConfigDatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.request.analyzer.utils.RequestAnalyzerUtils;
import com.openexchange.rest.services.configuration.ConfigurationRESTService;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * {@link ConfigurationRESTRequestAnalyzer} is a {@link RequestAnalyzer} which uses the "{context}" path segment of the analyzed call to
 * the configuration servlet to determine the marker for the request.
 *
 * @author <a href="mailto:marcel.broecher@open-xchange.com">Marcel Broecher</a>
 */
public class ConfigurationRESTRequestAnalyzer implements RequestAnalyzer {

    private final ErrorAwareSupplier<? extends ConfigDatabaseService> dbServiceSupplier;
    private final String servletPath;

    /**
     * Initializes a new {@link ConfigurationRESTRequestAnalyzer}.
     *
     * @param dbServiceSupplier A supplier for the database service for resolving the database schema name during analysis
     */
    public ConfigurationRESTRequestAnalyzer(ErrorAwareSupplier<? extends ConfigDatabaseService> dbServiceSupplier) {
        super();
        this.dbServiceSupplier = dbServiceSupplier;
        this.servletPath = ConfigurationRESTService.class.getAnnotation(Path.class).value();
    }

    @Override
    public Optional<AnalyzeResult> analyze(RequestData data) throws OXException {
        if (false == "GET".equals(data.getMethod())) {
            // no preliminary configuration request
            return Optional.empty();
        }
        Optional<String> path = data.getParsedURL().getPath();
        if (path.isEmpty() || false == path.get().startsWith(servletPath)) {
            // no preliminary configuration request
            return Optional.empty();
        }
        List<String> pathSegments = data.getParsedURL().getPathSegments();
        if (null != pathSegments && 5 < pathSegments.size() && ("property".equals(pathSegments.get(3)) || "withPrefix".equals(pathSegments.get(3)))) {
            // /preliminary/configuration/v1/property/com.openexchange.some.property/[contextId]/[userId]
            // /preliminary/configuration/v1/withPrefix/com.openexchange.mymodule/[contextId]/[userId]
            return Optional.of(RequestAnalyzerUtils.createAnalyzeResultFromCid(pathSegments.get(5), dbServiceSupplier));
        }
        // unknown, otherwise
        return Optional.of(AnalyzeResult.UNKNOWN);
    }
}

