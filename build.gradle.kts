import com.fasterxml.jackson.annotation.JsonInclude
import com.openexchange.build.image.extensions.DockerRegistry
import com.openexchange.build.image.extensions.ImageBuildConfig
import com.openexchange.build.image.tasks.BuildahPushImage
import com.openexchange.build.image.tasks.BuildahTagImage
import com.openexchange.build.image.tasks.RegistryDeleteImage
import com.openexchange.build.install.extension.InstallExtension

plugins {
    id("com.openexchange.build.documentation-properties") version "5.0.0" apply false
    id("com.openexchange.build.gradle-git") version "6.0.4"
    id("com.openexchange.build.install") version "6.0.1" apply false
    id("com.openexchange.build.licensing") version "5.0.0"
    id("com.openexchange.build.osgi") version "4.0.0"
    id("com.openexchange.build.packaging") version "9.0.0" apply false
    id("com.openexchange.build.plugin-applier") version "5.0.0"
    id("com.openexchange.build.project-type-scanner") version "5.1.0"
    id("com.openexchange.build.image-builder") version "9.1.1"
    id("com.openexchange.autosemver.gradle-git") version "3.0.1"
}

val testRuntimeOnlyForSubProjects: Configuration by configurations.creating {
    isCanBeConsumed = true
}

dependencies {
    testRuntimeOnlyForSubProjects("org.junit.vintage:junit-vintage-engine:5.10.2")
}

autoSemVer {
    branchNameProvider = git.branchNameProvider
    versionProvider = git.tagVersionProvider
    incrementProvider = git.stableVersioningIncrementProvider
    buildMetaProvider = { null }
    preReleaseProvider = { null }
    versionPublisher.add(git.tagVersionPublisher)
    with(releaseBranches) {
        remove("master")
        add("main")
    }
}

allprojects {
    apply {
        plugin("com.openexchange.build.plugin-applier")
    }
    repositories {
        mavenCentral()
    }
    tasks.withType(AbstractTestTask::class.java).configureEach {
        // TODO all tests need to run successfully one time
        ignoreFailures = true
    }
    project.configurations.findByName("testRuntimeOnly")?.dependencies?.add(
        dependencies.create(project.dependencies.project(rootProject.path, "testRuntimeOnlyForSubProjects"))
    )
    tasks.withType<Test> {
        useJUnitPlatform()
        ignoreFailures = true
        jvmArgs = listOf(
            "--add-opens=java.base/java.lang=ALL-UNNAMED",
            "--add-opens=java.base/java.util=ALL-UNNAMED",
            "--add-opens=java.base/java.util.concurrent=ALL-UNNAMED",
            "--add-opens=java.base/java.util.regex=ALL-UNNAMED",
            "--add-opens=java.base/java.lang.reflect=ALL-UNNAMED",
            "--add-opens=java.base/java.net=ALL-UNNAMED",
            "--add-opens=java.base/java.util.concurrent.atomic=ALL-UNNAMED",
            "--add-opens=java.base/javax.net.ssl=ALL-UNNAMED",
            "--add-opens=java.base/java.util.stream=ALL-UNNAMED",
            "--add-opens=java.base/java.nio.charset=ALL-UNNAMED",
            "--add-opens=java.base/java.nio.file=ALL-UNNAMED",
            "--add-opens=java.base/java.text=ALL-UNNAMED",
            "--add-opens=java.base/java.security=ALL-UNNAMED",
            "--add-opens=java.base/sun.security.action=ALL-UNNAMED",
            "--add-opens=java.base/sun.security.jca=ALL-UNNAMED",
            "--add-opens=java.xml/jdk.xml.internal=ALL-UNNAMED",
            "--add-opens=java.base/java.io=ALL-UNNAMED"
        )
    }
}

licensing {
    licenses {
        create("server") {
            this.sourceFile = File(project.projectDir, "SERVER-LICENSE")
        }
    }
}

install {
    destDir.set(File("/"))
    prefix.set(File("/opt/open-xchange"))
    packageSeparatedInis.set(true)
}

lateinit var core: NamedDomainObjectProvider<ImageBuildConfig>
val registryHost = "registry.open-xchange.com"
imageExtension {
    images {
        // Core baselayer build
        core = registerBuildConfig("core") {
            components.addAll("backend")
            archiveFileName.set("backend_container.orig.tar.bz2")
            inputDir.set(file("docker"))
            repositoryName.set("core")
            imageTag.addAll(provider { git.releaseTags() })
            labels.set(mutableMapOf(Pair("io.ox.commit-sha.backend", git.head.name)))
            dockerFile.set(file("docker/Dockerfile"))
            buildArgs.set(mutableMapOf(Pair("REGISTRY", "registry.hub.docker.com")))
            fileOwnership {
                userId.set(1000)
                userName.set("open-xchange")
                groupId.set(1000)
                groupName.set("open-xchange")
            }
            registries {
                create("default") {
                    url.set(registryHost)
                    projectName.set("appsuite-core-internal")
                    credentialsFromEnvironment("CI_REGISTRY_USR", "CI_REGISTRY_PSW")
                    // within Jenkins sandbox variable contents get $ escaped
                    System.getenv("CI_REGISTRY_USR")?.let { login.set(it.replace("$$", "$")) }
                }
            }
        }
    }
}

tasks {
    afterEvaluate {
        val createVersionFileTask = register("createVersionFile") {
            group = "container images"
            description = "Creates a file containing the current version of the appsuite container image."

            // Set up the task inputs and outputs
            val versionFile = layout.buildDirectory.file("version.txt")
            inputs.property("versionFile", provider{ git.getReleaseVersion().versionWithoutRevision })
            outputs.file(versionFile).withPropertyName("versionFile")

            // Write the version to the version file
            doLast {
                versionFile.get().asFile.writeText(git.getReleaseVersion().versionWithoutRevision)
            }
        }
        imageExtension.images.filterIsInstance<ImageBuildConfig>().forEach { config ->
            named<com.openexchange.build.packaging.tasks.TarWithSymlinkAndRoot>("imageTar-${config.name}") {
                config.resolveProjects().forEach { subProject ->
                    dependsOn(subProject.tasks.named("build"))
                }
                with(project.copySpec {
                    from(createVersionFileTask.get().outputs)
                    into(project.extensions.getByType(InstallExtension::class.java).prefixResolve(""))
                })
            }
        }
        named("buildahPushImage-core-tmp") {
            dependsOn(named<Task>("buildahBuildImage-core"))
            dependsOn(named<Task>("tagCoreTmpImage"))
        }
        named("copyDockerInputDir-core") {
            dependsOn(named<Task>("createVersionFile"))
        }
        named("tagCoreTmpImage") {
            mustRunAfter(named<Task>("buildahBuildImage-core"))
        }
        named("createVersionFile") {
            mustRunAfter(named<Task>("publishNextVersion"))
        }
    }
    register("tagCoreTmpImage", BuildahTagImage::class.java) {
        group = "container images"
        description = "Adds a new name to an already existing image"

        imageName.set("${getFullName()}:${core.get().firstImageTag()}")
        newName.set("${getFullName("core-tmp")}:${git.head.name}")
    }
    register("buildahPushImage-core-tmp", BuildahPushImage::class.java) {
        group = "container images"
        description = "Push the intermediate image to the tmp repository."

        image.set("$registryHost/appsuite-core-internal/core-tmp:${git.head.name}")
        extraArgs.set(listOf("--compression-format=gzip", "--compression-level=9"))
    }
    register("deleteImage-core-tmp", RegistryDeleteImage::class.java) {
        group = "container images"
        description = "Delete the intermediate image from the tmp repository"

        registry.set(core.get().registries.first())
        repositoryName.set("core-tmp")
        tagName.set("${git.head.name}")
    }
}


// ------------------- HELPER -------------------

data class GitComponent(val name: String, val branch: String, val commit: String? = null)

class RegistryNotFoundException(message: String) : GradleException(message)

fun ImageBuildConfig.firstImageTag(): String = imageTag.get().first()

fun ImageBuildConfig.defaultOrFirstRegistry(): DockerRegistry =
    registries.findByName("Default") ?: registries.firstOrNull()
    ?: throw RegistryNotFoundException("No registry found for ${name}.")

fun getFullName(repositoryName: String? = null): String {
    val coreConfig = core.get()
    val registry = coreConfig.defaultOrFirstRegistry()
    return registry.buildImageName(repositoryName ?: coreConfig.repositoryName.get())
}