/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.oauth.provider.impl;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import com.openexchange.oauth.provider.resourceserver.scope.OAuthScopeProvider;


/**
 * {@link ScopeProviderRegistry} - A registry for <code>OAuthScopeProvider</code> instances.
 *
 * @author <a href="mailto:steffen.templin@open-xchange.com">Steffen Templin</a>
 * @since v7.8.0
 */
public final class ScopeProviderRegistry {

    private static final ScopeProviderRegistry INSTANCE = new ScopeProviderRegistry();

    /**
     * Gets the instance.
     *
     * @return The instance
     */
    public static ScopeProviderRegistry getInstance() {
        return INSTANCE;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final ConcurrentMap<String, OAuthScopeProvider> providers = new ConcurrentHashMap<>();

    /**
     * Initializes a new instance of {@link ScopeProviderRegistry}.
     */
    private ScopeProviderRegistry() {
        super();
    }

    /**
     * Adds given scope provider.
     *
     * @param provider The scope provider to add
     */
    public void addScopeProvider(OAuthScopeProvider provider) {
        providers.put(provider.getToken(), provider);
    }

    /**
     * Removes given scope provider.
     *
     * @param provider The scope provider to remove
     */
    public void removeScopeProvider(OAuthScopeProvider provider) {
        providers.remove(provider.getToken(), provider);
    }

    /**
     * Gets the scope provider associated with given token.
     *
     * @param token The token to look-up
     * @return The scope provider or <code>null</code>
     */
    public OAuthScopeProvider getProvider(String token) {
        return providers.get(token);
    }

    /**
     * Checks if there is a scope provider associated with given token.
     *
     * @param token The token to look-up
     * @return <code>true</code> if such a scope provider exists; otherwise <code>false</code>
     */
    public boolean hasScopeProvider(String token) {
        return providers.containsKey(token);
    }

}
