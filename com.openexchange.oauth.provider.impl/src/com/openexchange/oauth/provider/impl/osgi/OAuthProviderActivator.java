/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.oauth.provider.impl.osgi;

import org.slf4j.Logger;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.ForcedReloadable;
import com.openexchange.config.Reloadable;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.oauth.provider.authorizationserver.spi.OAuthAuthorizationService;
import com.openexchange.oauth.provider.impl.OAuthProviderMode;
import com.openexchange.oauth.provider.impl.OAuthProviderProperties;
import com.openexchange.oauth.provider.impl.OAuthResourceServiceImpl;
import com.openexchange.oauth.provider.impl.introspection.OAuthIntrospectionAuthorizationService;
import com.openexchange.oauth.provider.impl.jwt.OAuthJWTScopeService;
import com.openexchange.oauth.provider.impl.jwt.OAuthJwtAuthorizationService;
import com.openexchange.oauth.provider.impl.request.analyzer.OAuthRequestAnalyzer;
import com.openexchange.oauth.provider.resourceserver.OAuthResourceService;
import com.openexchange.oauth.provider.resourceserver.scope.OAuthScopeProvider;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.osgi.RankingAwareNearRegistryServiceTracker;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.server.ServiceLookup;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.user.UserService;

/**
 * {@link OAuthProviderActivator} - The activator for OAuth provider implementation bundle.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class OAuthProviderActivator extends HousekeepingActivator {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(OAuthProviderActivator.class);

    /**
     * Initializes a new {@link OAuthProviderActivator}.
     */
    public OAuthProviderActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { DatabaseService.class, LeanConfigurationService.class, ConfigurationService.class, ContextService.class, UserService.class, SessiondService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        final ServiceLookup serviceLookup = this;
        Services.setServiceLookup(serviceLookup);

        LeanConfigurationService leanConfigService = getServiceSafe(LeanConfigurationService.class);
        if (!leanConfigService.getBooleanProperty(OAuthProviderProperties.ENABLED)) {
            LOG.info("OAuth provider is disabled by configuration.");
            return;
        }

        track(OAuthScopeProvider.class, new OAuthScopeProviderTracker(context));

        OAuthProviderMode mode = OAuthProviderMode.getProviderMode(leanConfigService.getProperty(OAuthProviderProperties.MODE));
        switch (mode) {
            default:
            case EXPECT_JWT:
                startJWTService(leanConfigService);
                break;
            case TOKEN_INTROSPECTION:
                startTokenIntrospectionService(leanConfigService);
                break;
        }

        RankingAwareNearRegistryServiceTracker<OAuthAuthorizationService> oauthAuthorizationServiceTracker = new RankingAwareNearRegistryServiceTracker<>(context, OAuthAuthorizationService.class);
        rememberTracker(oauthAuthorizationServiceTracker);
        openTrackers();
        registerService(OAuthResourceService.class, new OAuthResourceServiceImpl(oauthAuthorizationServiceTracker, serviceLookup));

        registerService(RequestAnalyzer.class, new OAuthRequestAnalyzer(this));
    }

    @Override
    protected boolean stopOnServiceUnavailability() {
        return true;
    }

    @Override
    protected void stopBundle() throws Exception {
        Services.setServiceLookup(null);
        super.stopBundle();
    }

    private void startJWTService(LeanConfigurationService leanConfigService) throws OXException {
    	OAuthJWTScopeService scopeService = new OAuthJWTScopeService(leanConfigService);
    	registerService(Reloadable.class, scopeService);

        OAuthJwtAuthorizationService jwtAuthorizationService = new OAuthJwtAuthorizationService(leanConfigService, scopeService);
        registerService(OAuthAuthorizationService.class, jwtAuthorizationService);
        registerService(ForcedReloadable.class, jwtAuthorizationService);
    }

    private void startTokenIntrospectionService(LeanConfigurationService leanConfigService) {
        OAuthJWTScopeService scopeService = new OAuthJWTScopeService(leanConfigService);
        registerService(Reloadable.class, scopeService);

        OAuthIntrospectionAuthorizationService tokenIntrospectionAuthorizationService = new OAuthIntrospectionAuthorizationService(leanConfigService, scopeService);
        registerService(OAuthAuthorizationService.class, tokenIntrospectionAuthorizationService);
        registerService(Reloadable.class, tokenIntrospectionAuthorizationService);
    }
}
