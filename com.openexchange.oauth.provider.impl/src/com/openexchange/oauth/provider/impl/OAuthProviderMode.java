/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.oauth.provider.impl;

import com.openexchange.oauth.provider.impl.introspection.OAuthIntrospectionAuthorizationService;
import com.openexchange.oauth.provider.impl.jwt.OAuthJwtAuthorizationService;

/**
 * {@link OAuthProviderMode} - defines available modes for OAuth provider
 *
 * @author <a href="mailto:sebastian.lutz@open-xchange.com">Sebastian Lutz</a>
 * @since v7.10.5
 */
public enum OAuthProviderMode {

    /**
     * The OAuth provider expects JWT and is able to parse and validate it.
     * This mode enables {@link OAuthJwtAuthorizationService}
     */
    EXPECT_JWT("expect_jwt"),

    /**
     * The OAuthProvider uses token introspection to verify a received token.
     * This mode enables {@link OAuthIntrospectionAuthorizationService}
     */
    TOKEN_INTROSPECTION("token_introspection");

    private final String identifier;

    /**
     * Initializes a new {@link OAuthProviderMode}.
     *
     * @param identifier The identifier
     */
    private OAuthProviderMode(String identifier) {
        this.identifier = identifier;
    }

    /**
     * Gets the identifier for this OAuth provider mode.
     *
     * @return The identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Gets the corresponding OAuth provider mode or {@link OAuthProviderMode#EXPECT_JWT} in case the given mode string is unknown.
     *
     * @param input The identifier to look-up
     * @return The corresponding OAuth provider mode
     */
    public static OAuthProviderMode getProviderMode(String input) {
        return getProviderMode(input, EXPECT_JWT);
    }

    /**
     * Gets the corresponding OAuth provider mode or given default value in case the given mode string is unknown.
     *
     * @param input The identifier to look-up
     * @param defaultValue The default value to return
     * @return The corresponding OAuth provider mode or given default value
     */
    public static OAuthProviderMode getProviderMode(String input, OAuthProviderMode defaultValue) {
        if (input != null) {
            for (OAuthProviderMode mode : OAuthProviderMode.values()) {
                if (mode.getIdentifier().equals(input)) {
                    return mode;
                }
            }
        }
        return defaultValue;
    }

}
