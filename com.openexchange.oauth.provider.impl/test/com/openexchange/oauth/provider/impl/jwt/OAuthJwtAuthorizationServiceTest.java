/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.oauth.provider.impl.jwt;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.openexchange.authentication.NamePart;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.contexts.impl.ContextImpl;
import com.openexchange.java.Buffers;
import com.openexchange.oauth.provider.authorizationserver.spi.ValidationResponse;
import com.openexchange.oauth.provider.authorizationserver.spi.ValidationResponse.TokenStatus;
import com.openexchange.oauth.provider.impl.OAuthProviderProperties;
import com.openexchange.oauth.provider.impl.osgi.Services;
import com.openexchange.servlet.Headers;
import com.openexchange.user.UserService;

/**
 * {@link OAuthJwtAuthorizationServiceTest}
 *
 * @author <a href="mailto:sebastian.lutz@open-xchange.com">Sebastian Lutz</a>
 * @since 7.10.5
 */
public class OAuthJwtAuthorizationServiceTest {

    private final String subject = "anton@context1.ox.test";
    private final String issuer = "https://example.com";
    private final String scopeClaimname = "scope";
    private final String scope = "oxpim";
    private final String authorizedPartyClaimname = "azp";
    private final String authorizedParty = "contactviewer";

    private JWK jwk;

    private KeyPair keyPair;

    @Mock
    private LeanConfigurationService leanConfigurationService;

    @Mock
    private ContextService contextService;

    @Mock
    private UserService userService;

    private MockedStatic<Services> servicesMock;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        servicesMock = Mockito.mockStatic(Services.class);
        Mockito.when(Services.requireService(LeanConfigurationService.class)).thenReturn(leanConfigurationService);
        Mockito.when(Services.requireService(ContextService.class)).thenReturn(contextService);
        Mockito.when(Services.requireService(UserService.class)).thenReturn(userService);

        Mockito.when(leanConfigurationService.getProperty(OAuthJWTProperty.JWKS_URI)).thenReturn(OAuthJWTProperty.JWKS_URI.getDefaultValue().toString());
        Mockito.when(leanConfigurationService.getProperty(OAuthProviderProperties.ALLOWED_ISSUER)).thenReturn(issuer);

        Mockito.when(leanConfigurationService.getProperty(OAuthProviderProperties.CONTEXT_LOOKUP_CLAIM)).thenReturn(OAuthProviderProperties.CONTEXT_LOOKUP_CLAIM.getDefaultValue().toString());
        Mockito.when(leanConfigurationService.getProperty(OAuthProviderProperties.CONTEXT_LOOKUP_NAME_PART)).thenReturn(NamePart.DOMAIN.getConfigName());

        Mockito.when(leanConfigurationService.getProperty(OAuthProviderProperties.USER_LOOKUP_CLAIM)).thenReturn(OAuthProviderProperties.USER_LOOKUP_CLAIM.getDefaultValue().toString());
        Mockito.when(leanConfigurationService.getProperty(OAuthProviderProperties.USER_LOOKUP_NAME_PART)).thenReturn(NamePart.LOCAL_PART.getConfigName());

        Mockito.when(contextService.getContext(ArgumentMatchers.anyInt())).thenReturn(new ContextImpl(1));
        Mockito.when(I(userService.getUserId(ArgumentMatchers.anyString(), (Context) ArgumentMatchers.any()))).thenReturn(I(3));

        this.keyPair = generateKeyPair();
        this.jwk = generateJWK(keyPair);
    }

    @AfterEach
    public void reset() {
        servicesMock.close();
    }

    @Test
    public void testJwtValidationForValidToken() throws Exception {
        Mockito.mockConstruction(OAuthJwtAuthorizationService.class, withSettings(), (mock, context) -> {
            when(mock.getKeySource()).thenReturn(new ImmutableJWKSet<>(new JWKSet(jwk)));

            // Create RSA-signer with the private key
            JWSSigner signer = new RSASSASigner(keyPair.getPrivate());

            // Prepare JWT with claims set
            // @formatter:off
            JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject(subject)
                .issuer(issuer)
                .claim(authorizedPartyClaimname, "testClient")
                .claim(scopeClaimname, scope)
                .expirationTime(new Date(new Date().getTime() + 60 * 1000))
                .build();
            // @formatter:on

            SignedJWT signedJWT = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(jwk.getKeyID()).build(), claimsSet);

            // Compute the RSA signature
            signedJWT.sign(signer);

            String jwt = signedJWT.serialize();

            ValidationResponse value = mock.validateAccessToken(jwt, Headers.emptyHeaders());

            assertEquals(TokenStatus.VALID, value.getTokenStatus());
        }).close();
    }

    @Test
    public void testJWTValidationForExpiredToken() throws Exception {
        Mockito.mockConstruction(OAuthJwtAuthorizationService.class, withSettings(), (mock, context) -> {
            // Create RSA-signer with the private key
            JWSSigner signer = new RSASSASigner(keyPair.getPrivate());

            // Prepare JWT with claims set
            // @formatter:off
            JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject(subject)
                .issuer(issuer)
                .claim(authorizedPartyClaimname, authorizedParty)
                .claim(scopeClaimname, scope)
                .expirationTime(new Date(new Date().getTime() - 60 * 1000))
                .build();
            // @formatter:on

            SignedJWT signedJWT = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(jwk.getKeyID()).build(), claimsSet);

            // Compute the RSA signature
            signedJWT.sign(signer);

            String jwt = signedJWT.serialize();

            ValidationResponse value = mock.validateAccessToken(jwt, Headers.emptyHeaders());
            assertEquals(TokenStatus.INVALID, value.getTokenStatus());
        }).close();
    }

    @Test
    public void testJWTValidationForUnexpectedTokenIssuer() throws Exception {
        Mockito.mockConstruction(OAuthJwtAuthorizationService.class, withSettings(), (mock, context) -> {
            // Create RSA-signer with the private key
            JWSSigner signer = new RSASSASigner(keyPair.getPrivate());

            // Prepare JWT with claims set
            // @formatter:off
            JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject(subject)
                .issuer("https://example.de")
                .claim(authorizedPartyClaimname, authorizedParty)
                .claim(scopeClaimname, scope)
                .expirationTime(new Date(new Date().getTime() + 60 * 1000))
                .build();
            // @formatter:on

            SignedJWT signedJWT = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(jwk.getKeyID()).build(), claimsSet);

            // Compute the RSA signature
            signedJWT.sign(signer);

            String jwt = signedJWT.serialize();

            ValidationResponse value = mock.validateAccessToken(jwt, Headers.emptyHeaders());
            assertEquals(value.getTokenStatus(), TokenStatus.INVALID);
        }).close();

    }

    @Test
    public void testJWTValidationForMissingScopes() throws Exception {
        Mockito.when(leanConfigurationService.getProperty(OAuthProviderProperties.ALLOWED_ISSUER)).thenReturn("https://example.com");
        Mockito.mockConstruction(OAuthJwtAuthorizationService.class, withSettings(), (mock, context) -> {
            // Create RSA-signer with the private key
            JWSSigner signer = new RSASSASigner(keyPair.getPrivate());

            // Prepare JWT with claims set
            // @formatter:off
            JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject(subject)
                .issuer(issuer)
                .claim(authorizedPartyClaimname, authorizedParty)
                .claim(scopeClaimname, "")
                .expirationTime(new Date(new Date().getTime() + 60 * 1000))
                .build();
            // @formatter:on

            SignedJWT signedJWT = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(jwk.getKeyID()).build(), claimsSet);

            // Compute the RSA signature
            signedJWT.sign(signer);

            String jwt = signedJWT.serialize();

            ValidationResponse value = mock.validateAccessToken(jwt, Headers.emptyHeaders());
            assertEquals(TokenStatus.INVALID, value.getTokenStatus());
        }).close();
    }

    private KeyPair generateKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
        gen.initialize(Buffers.BUFFER_SIZE_2K);
        return gen.generateKeyPair();
    }

    private JWK generateJWK(KeyPair keyPair) {
        // @formatter:off
        return new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
            .privateKey((RSAPrivateKey) keyPair.getPrivate())
            .keyUse(KeyUse.SIGNATURE)
            .keyID(UUID.randomUUID().toString())
            .build();
        // @formatter:on
    }

}
