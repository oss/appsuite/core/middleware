/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.subscribe.osgi;

import java.util.Dictionary;
import com.openexchange.capabilities.CapabilityChecker;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.subscribe.SubscriptionSourceDiscoveryService;
import com.openexchange.subscribe.helpers.SubscriptionCapabilityChecker;

/**
 * {@link CapabilityActivator} - Declares the 'subscribe' capability and registers a capability checker.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class CapabilityActivator extends HousekeepingActivator {

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] { CapabilityService.class, ConfigViewFactory.class, SubscriptionSourceDiscoveryService.class };
    }

    @Override
    public void startBundle() throws Exception {
        /*
         * register capability checker
         */
        Dictionary<String, Object> properties = singletonDictionary(CapabilityChecker.PROPERTY_CAPABILITIES, SubscriptionCapabilityChecker.CAPABILITY_NAME);
        registerService(CapabilityChecker.class, new SubscriptionCapabilityChecker(this), properties);
        getServiceSafe(CapabilityService.class).declareCapability(SubscriptionCapabilityChecker.CAPABILITY_NAME);
    }

    @Override
    public void stopBundle() throws Exception {
        CapabilityService capabilityService = getService(CapabilityService.class);
        if (null != capabilityService) {
            capabilityService.undeclareCapability(SubscriptionCapabilityChecker.CAPABILITY_NAME);
        }
    }

}
