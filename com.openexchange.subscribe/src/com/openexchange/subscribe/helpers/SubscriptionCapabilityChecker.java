/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.subscribe.helpers;

import java.util.List;
import com.openexchange.capabilities.CapabilityChecker;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.userconfiguration.Permission;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.subscribe.SubscriptionSource;
import com.openexchange.subscribe.SubscriptionSourceDiscoveryService;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tools.session.ServerSessionAdapter;

/**
 * {@link SubscriptionCapabilityChecker} - Checks whether the <code>subscribe</code> capability can be indicated to clients or not.
 * 
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class SubscriptionCapabilityChecker implements CapabilityChecker {

    /** The name of the checked capability */
    public static final String CAPABILITY_NAME = Permission.SUBSCRIPTION.getCapabilityName();

    private final ServiceLookup services;

    /**
     * Initializes a new {@link SubscriptionCapabilityChecker}.
     *
     * @param services A service lookup reference
     */
    public SubscriptionCapabilityChecker(ServiceLookup services) {
        this.services = services;
    }

    @Override
    public boolean isEnabled(String capability, Session session) throws OXException {
        if (false == CAPABILITY_NAME.equals(capability) || null == session || 1 > session.getUserId()) {
            return false;
        }
        return isEnabled(ServerSessionAdapter.valueOf(session)); 
    }
    
    private boolean isEnabled(ServerSession session) {
        if (session.getUser().isGuest()) {
            return false; // never for guests
        }
        if (false == session.getUserPermissionBits().hasPermission(Permission.SUBSCRIPTION)) {
            return false; // disabled by user configuration
        }
        List<SubscriptionSource> subscriptionSources;
        try {
             subscriptionSources = services.getService(SubscriptionSourceDiscoveryService.class).filter(session.getUserId(), session.getContextId()).getSources();
        } catch (OXException e) {
            org.slf4j.LoggerFactory.getLogger(SubscriptionCapabilityChecker.class).warn("Unexpected error getting subscription sources", e);
            return false; // no subscription sources
        }
        if (null == subscriptionSources || subscriptionSources.isEmpty()) {
            return false; // no subscription sources
        }
        return true; // user has at least one subscription source enabled
    }
    
}
