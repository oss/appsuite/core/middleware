/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.smtp;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.config.ConfigurationService;
import com.openexchange.smtp.config.SMTPProperties;
import com.openexchange.smtp.services.Services;


/**
 * {@link Bug30843Test}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 * @since 7.6.0
 */
public class Bug30843Test {

    private ConfigurationService configService;

    /**
     * Initializes a new {@link Bug30843Test}.
     */
    public Bug30843Test() {
        super();
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.configService = Mockito.mock(ConfigurationService.class);
        Mockito.when(configService.getProperty(anyString())).thenReturn("");
        Mockito.when(configService.getProperty(anyString(), anyString())).thenReturn("");
        Mockito.when(I(configService.getIntProperty(anyString(), anyInt()))).thenReturn(I(0));
        Mockito.when(configService.getProperty("com.openexchange.smtp.smtpAuthEnc", "UTF-8")).thenReturn("UTF-8");
        Mockito.when(configService.getProperty(eq("com.openexchange.smtp.ssl.protocols"), anyString())).thenReturn("SSLv3 TLSv1 TLSv1.1 TLSv1.2");
        Mockito.mockStatic(Services.class);
        Mockito.when(Services.getService(ConfigurationService.class)).thenReturn(configService);
    }

     @Test
     public void testSMTPProperties_getSSLProtocols() throws Exception {
        SMTPProperties props = SMTPProperties.getInstance();
        props.loadProperties();
        String sslProtocols = props.getSSLProtocols();
        assertEquals("SSLv3 TLSv1 TLSv1.1 TLSv1.2", sslProtocols, "Wrong value loaded from ConfigurationService");
    }

}
