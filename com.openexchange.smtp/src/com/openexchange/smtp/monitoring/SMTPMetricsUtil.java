/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.smtp.monitoring;

import java.net.InetAddress;
import java.time.Duration;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.smtp.config.SMTPConfig;
import com.openexchange.smtp.services.Services;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;

/**
 * {@link SMTPMetricsUtil} is a helper class
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public final class SMTPMetricsUtil {

    private static final String EXTERNAL_GROUPING_NAME = "external";
    private static final String PRIMARY_GROUPING_NAME = "primary";
    private static final String DESCRIPCTION_TEMPLATE = "SMTP transport per target server. Status can be one of the following: %s.";
    private static final String DESCRIPTION = DESCRIPCTION_TEMPLATE.formatted(Stream.of(Status.values())
                                                                                    .map(s -> s.toString())
                                                                                    .collect(Collectors.joining(", ")));
    private final MetricConfig config;

    /**
     * Creates a new {@link SMTPMetricsUtil} instance
     *
     * @return The {@link SMTPMetricsUtil}
     * @throws OXException in case the instance couldn't be created
     */
    public static SMTPMetricsUtil createUtil() throws OXException {
        MetricConfig config = getMetricConfig(Services.getServiceSafe(LeanConfigurationService.class));
        return new SMTPMetricsUtil(config);
    }

    /**
     * Initializes a new {@link SMTPMetricsUtil}.
     *
     * @param services The service lookup
     * @throws OXException
     */
    private SMTPMetricsUtil(MetricConfig config) {
        super();
        this.config = config;
    }

    /**
     * Records smtp transport execution time with proper tags based on given input and monitoring configuration.
     *
     * @param smtpConfig The smtp configuration
     * @param isPrimaryAccount Whether the account is a primary account or not
     * @param status The response status, i.e. {@code OK}, {@code NO}, {@code BAD}, {@code BYE}
     * @param duration The duration
     * @throws OXException
     */
    public void recordStatus(SMTPConfig smtpConfig, boolean isPrimaryAccount, Status status, Duration duration) throws OXException {
        String targetHost = getHost(smtpConfig, isPrimaryAccount);
        Timer requestTimer = Timer.builder("appsuite.smtp.transport")
                                  .description(DESCRIPTION)
                                  .tags("status", status.toString(), "host", targetHost)
                                  .register(Metrics.globalRegistry);
        requestTimer.record(duration);
    }

    // ----------------- private methods ----------------------

    /**
     * Loads the metric configuration
     *
     * @param configuration The {@link LeanConfigurationService}
     * @return The metric configuration
     */
    private static MetricConfig getMetricConfig(LeanConfigurationService configuration) {
        boolean isEnabled = configuration.getBooleanProperty(SMTPMonitoringProperties.ENABLED);
        PrimaryMetricMode primary = PrimaryMetricMode.of(configuration.getProperty(SMTPMonitoringProperties.PRIMARY_MODE));
        ExternalMetricMode external = ExternalMetricMode.of(configuration.getProperty(SMTPMonitoringProperties.EXTERNAL_MODE));
        return new MetricConfig(isEnabled, primary, external);
    }

    /**
     * Whether the account needs recording or not
     *
     * @param isPrimaryAccount Whether the account is the primary account or not
     * @return <code>true</code> if metrics need to be collected, <code>false</code> otherwise
     */
    public boolean needsRecording(boolean isPrimaryAccount) {
        if (false == config.isEnabled()) {
            return false;
        }
        return isPrimaryAccount || false == ExternalMetricMode.DISABLED.equals(config.externalMode());
    }

    /**
     * Gets the host for the given smtp config
     *
     * @param smtpConfig The {@link SMTPConfig}
     * @param isPrimaryAccount Whether the account is a primary account or not
     * @return The host name
     * @throws OXException if the smtp service cannot be resolved
     */
    private String getHost(SMTPConfig smtpConfig, boolean isPrimaryAccount) throws OXException {
        if (isPrimaryAccount) {
            return switch (config.primaryMode()) {
                case ENDPOINT -> {
                    InetAddress inetAddress = smtpConfig.getSmtpServerAddress();
                    yield new StringBuilder(inetAddress.getHostAddress()).append(':')
                                                                         .append(smtpConfig.getPort())
                                                                         .toString();
                }
                case HOST -> new StringBuilder(smtpConfig.getServer()).toString();
                case SIMPLE -> PRIMARY_GROUPING_NAME;
                default -> PRIMARY_GROUPING_NAME;
            };
        }

        return switch (config.externalMode()) {
            case HOST -> new StringBuilder(smtpConfig.getServer()).append(smtpConfig.getPort())
                                                                  .toString();
            case SIMPLE -> EXTERNAL_GROUPING_NAME;
            default -> EXTERNAL_GROUPING_NAME;
        };

    }

    // -------------------- private records  --------------

    /**
     * {@link MetricConfig} contains the metric configuration
     *
     * @param isEnabled Whether metric collection is enabled or not
     * @param primaryMode The primary mode
     * @param externalMode The external mode
     * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
     */
    private record MetricConfig(boolean isEnabled, PrimaryMetricMode primaryMode, ExternalMetricMode externalMode) {

    }

}
