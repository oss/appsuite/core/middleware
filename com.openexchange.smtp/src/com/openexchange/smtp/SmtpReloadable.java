/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.smtp;

import org.slf4j.Logger;
import com.openexchange.config.AbstractCompositeReloadable;
import com.openexchange.config.ConfigurationService;
import com.openexchange.java.Strings;
import com.openexchange.smtp.config.SMTPProperties;

/**
 * {@link SmtpReloadable} - Collects reloadables for SMTP module.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since 7.6.0
 */
public final class SmtpReloadable extends AbstractCompositeReloadable {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(SmtpReloadable.class);

    private static final SmtpReloadable INSTANCE = new SmtpReloadable();

    /**
     * Gets the instance.
     *
     * @return The instance
     */
    public static SmtpReloadable getInstance() {
        return INSTANCE;
    }

    // --------------------------------------------------------------------------------------------------- //

    /**
     * Initializes a new {@link SmtpReloadable}.
     */
    private SmtpReloadable() {
        super();
    }

    @Override
    protected void preReloadConfiguration(ConfigurationService configService) {
        try {
            final SMTPProperties smtpProperties = SMTPProperties.getInstance();
            if (null != smtpProperties) {
                smtpProperties.resetProperties();
                smtpProperties.loadProperties();
            }
        } catch (Exception e) {
            LOGGER.warn("Failed to reload SMTP properties", e);
        }
    }

    @Override
    protected void postReloadConfiguration(ConfigurationService configService) {
        // Noop
    }

    @Override
    protected String[] getFileNamesOfInterest() {
        return Strings.getEmptyStrings();
    }

    @Override
    protected String[] getPropertiesOfInterest() {
        return new String[] { "com.openexchange.smtp.*" };
    }

}
