/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.smtp.config;

import static com.openexchange.java.Autoboxing.B;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import com.openexchange.config.ConfigurationService;
import com.openexchange.java.CharsetDetector;
import com.openexchange.java.Strings;
import com.openexchange.log.LogMessageBuilder;
import com.openexchange.mail.api.AbstractProtocolProperties;
import com.openexchange.mail.transport.config.ITransportProperties;
import com.openexchange.mail.transport.config.TransportProperties;
import com.openexchange.smtp.services.Services;

/**
 * {@link SMTPProperties}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class SMTPProperties extends AbstractProtocolProperties implements ISMTPProperties {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(SMTPProperties.class);

    private static final SMTPProperties instance = new SMTPProperties();

    /**
     * Gets the singleton instance of {@link SMTPProperties}
     *
     * @return The singleton instance of {@link SMTPProperties}
     */
    public static SMTPProperties getInstance() {
        return instance;
    }

    /*
     * Fields for global properties
     */

    private final ITransportProperties transportProperties;

    private String smtpLocalhost;

    private boolean smtpAuth;

    private boolean smtpEnvelopeFrom;

    private boolean sendPartial;

    private boolean allowXClient;

    private String smtpAuthEnc;

    private int smtpTimeout;

    private int smtpConnectionTimeout;

    private boolean logTransport;

    private String sslProtocols;

    private String cipherSuites;

    private String primaryAddressHeader;

    private boolean requireTls;

    private long chunkSize;

    /**
     * Initializes a new {@link SMTPProperties}
     */
    private SMTPProperties() {
        super();
        transportProperties = TransportProperties.getInstance();
    }

    @Override
    protected void loadProperties0() {
        LogMessageBuilder logBuilder = LOG.isInfoEnabled() ? LogMessageBuilder.createLogMessageBuilder(1024, 32) : LogMessageBuilder.emptyLogMessageBuilder();

        logBuilder.lfappendln("Loading global SMTP properties...");

        final ConfigurationService configuration = Services.getService(ConfigurationService.class);
        {
            final String smtpLocalhostStr = configuration.getProperty("com.openexchange.smtp.smtpLocalhost").trim();
            smtpLocalhost = (smtpLocalhostStr == null) || (smtpLocalhostStr.length() == 0) || "null".equalsIgnoreCase(smtpLocalhostStr) ? null : smtpLocalhostStr;
            logBuilder.appendln("    SMTP Localhost: {}", smtpLocalhost);
        }

        {
            final String smtpAuthStr = configuration.getProperty("com.openexchange.smtp.smtpAuthentication", "false").trim();
            smtpAuth = Boolean.parseBoolean(smtpAuthStr);
            logBuilder.appendln("    SMTP Authentication: {}", B(smtpAuth));
        }

        {
            final String sendPartialStr = configuration.getProperty("com.openexchange.smtp.sendPartial", "false").trim();
            sendPartial = Boolean.parseBoolean(sendPartialStr);
            logBuilder.appendln("    Send Partial: {}", B(sendPartial));
        }

        {
            String allowXClientStr = configuration.getProperty("com.openexchange.smtp.allowXCLIENT", "false").trim();
            allowXClient = Boolean.parseBoolean(allowXClientStr);
            logBuilder.appendln("    Allow XCLIENT: {}", B(allowXClient));
        }

        {
            final String smtpEnvFromStr = configuration.getProperty("com.openexchange.smtp.setSMTPEnvelopeFrom", "false").trim();
            smtpEnvelopeFrom = Boolean.parseBoolean(smtpEnvFromStr);
            logBuilder.appendln("    Set SMTP ENVELOPE-FROM: {}", B(smtpEnvelopeFrom));
        }

        {
            final String tmp = configuration.getProperty("com.openexchange.smtp.logTransport", "false").trim();
            logTransport = Boolean.parseBoolean(tmp);
            logBuilder.appendln("    Log transport: {}", B(logTransport));
        }

        {
            final String smtpAuthEncStr = configuration.getProperty("com.openexchange.smtp.smtpAuthEnc", "UTF-8").trim();
            if (CharsetDetector.isValid(smtpAuthEncStr)) {
                smtpAuthEnc = smtpAuthEncStr;
                logBuilder.appendln("    SMTP Auth Encoding: {}", smtpAuthEnc);
            } else {
                smtpAuthEnc = "UTF-8";
                logBuilder.appendln("    SMTP Auth Encoding: Unsupported charset \"{}\". Setting to fallback {}", smtpAuthEncStr, smtpAuthEnc);
            }
        }

        {
            final String smtpTimeoutStr = configuration.getProperty("com.openexchange.smtp.smtpTimeout", "5000").trim();
            try {
                smtpTimeout = Integer.parseInt(smtpTimeoutStr);
                logBuilder.appendln("    SMTP Timeout: {}", I(smtpTimeout));
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                smtpTimeout = 5000;
                logBuilder.appendln("    SMTP Timeout: Invalid value \"{}\". Setting to fallback {}", smtpTimeoutStr, I(smtpTimeout));

            }
        }

        {
            final String smtpConTimeoutStr = configuration.getProperty("com.openexchange.smtp.smtpConnectionTimeout", "10000").trim();
            try {
                smtpConnectionTimeout = Integer.parseInt(smtpConTimeoutStr);
                logBuilder.appendln("    SMTP Connection Timeout: {}", I(smtpConnectionTimeout));
            } catch (NumberFormatException e) {
                smtpConnectionTimeout = 10000;
                logBuilder.appendln("    SMTP Connection Timeout: Invalid value \"{}\". Setting to fallback {}", smtpConTimeoutStr, I(smtpConnectionTimeout));

            }
        }

        {
            final String chunkSizeStr = configuration.getProperty("com.openexchange.smtp.chunksize", "131072").trim();
            try {
                chunkSize = Long.parseLong(chunkSizeStr);
                logBuilder.appendln("    SMTP Chunk Size: {}", L(chunkSize));
            } catch (NumberFormatException e) {
                chunkSize = 131072L;
                logBuilder.appendln("    SMTP Chunk Size: Invalid value \"{}\". Setting to fallback {}", chunkSizeStr, L(chunkSize));

            }
        }

        {
            final String tmp = configuration.getProperty("com.openexchange.smtp.ssl.protocols", "").trim();
            this.sslProtocols = Strings.isEmpty(tmp) ? null : tmp;
            logBuilder.appendln("    Supported SSL protocols: {}", null == this.sslProtocols ? "<default>" : sslProtocols);
        }

        {
            final String tmp = configuration.getProperty("com.openexchange.smtp.ssl.ciphersuites", "").trim();
            this.cipherSuites = Strings.isEmpty(tmp) ? null : tmp;
            logBuilder.appendln("    Supported SSL cipher suites: {}", null == this.cipherSuites ? "<default>" : cipherSuites);
        }

        {
            final String tmp = configuration.getProperty("com.openexchange.smtp.setPrimaryAddressHeader", "").trim();
            primaryAddressHeader = (tmp == null) || (tmp.length() == 0) || "null".equalsIgnoreCase(tmp) ? null : tmp;
            logBuilder.appendln("    Primary address header: {}", primaryAddressHeader);
        }

        {
            String tmp = configuration.getProperty("com.openexchange.smtp.requireTls", "true").trim();
            this.requireTls = Strings.isEmpty(tmp) ? true : Boolean.parseBoolean(tmp);
            logBuilder.appendln("    Require TLS: {}", Boolean.toString(requireTls));
        }

        logBuilder.append("Global SMTP properties successfully loaded!");
        LOG.info(logBuilder.getMessage(), logBuilder.getArgumentsAsArray());
    }

    @Override
    protected void resetFields() {
        smtpLocalhost = null;
        sendPartial = false;
        allowXClient = false;
        smtpAuth = false;
        smtpEnvelopeFrom = false;
        smtpAuthEnc = null;
        smtpTimeout = 0;
        smtpConnectionTimeout = 0;
        logTransport = false;
        sslProtocols = null;
        cipherSuites = null;
        primaryAddressHeader = null;
    }

    @Override
    public String getSmtpLocalhost() {
        return smtpLocalhost;
    }

    @Override
    public boolean isSmtpAuth() {
        return smtpAuth;
    }

    @Override
    public boolean isSendPartial() {
        return sendPartial;
    }

    @Override
    public boolean isAllowXClient() {
        return allowXClient;
    }

    @Override
    public boolean isSmtpEnvelopeFrom() {
        return smtpEnvelopeFrom;
    }

    @Override
    public boolean isLogTransport() {
        return logTransport;
    }

    @Override
    public String getSmtpAuthEnc() {
        return smtpAuthEnc;
    }

    @Override
    public long getChunkSize() {
        return chunkSize;
    }

    @Override
    public int getSmtpTimeout() {
        return smtpTimeout;
    }

    @Override
    public int getSmtpConnectionTimeout() {
        return smtpConnectionTimeout;
    }

    @Override
    public int getReferencedPartLimit() {
        return transportProperties.getReferencedPartLimit();
    }

    @Override
    public String getSSLProtocols() {
        return sslProtocols;
    }

    @Override
    public String getSSLCipherSuites() {
        return cipherSuites;
    }

    @Override
    public String getPrimaryAddressHeader() {
        return primaryAddressHeader;
    }

    @Override
    public boolean isRequireTls() {
        return requireTls;
    }
}
