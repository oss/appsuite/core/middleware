/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.gdpr.dataexport;

import java.io.File;

/**
 * {@link SpoolingOptions} - The spooling options for a data export.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class SpoolingOptions {

    /** The constant spooling options signaling that no spooling shall be performed */
    public static final SpoolingOptions NO_SPOOLING = new SpoolingOptions(false, null);

    private final boolean enabled;
    private final File directory;

    /**
     * Initializes a new {@link SpoolingOptions}.
     *
     * @param enabled Whether spooling is enabled
     * @param directory The directory to use
     */
    public SpoolingOptions(boolean enabled, File directory) {
        super();
        this.enabled = enabled;
        this.directory = directory;
    }

    /**
     * Checks whether to spool collected data to a ZIP archive held on local disk or to append directly to destination file storage.
     *
     * @return <code>true</code> to spool to local disk file; otherwise <code>false</code> to write directly to file storage
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Gets the directory on disk that shall be used for spooling.
     *
     * @return The directory or <code>null</code> (to fall-back to default upload directory)
     */
    public File getDirectory() {
        return directory;
    }

}
