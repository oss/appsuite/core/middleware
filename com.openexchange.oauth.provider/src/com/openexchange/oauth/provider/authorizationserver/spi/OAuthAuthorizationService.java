/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.oauth.provider.authorizationserver.spi;

import com.openexchange.osgi.annotation.SingletonService;
import com.openexchange.servlet.Headers;

/**
 * Service provider interface to validate access tokens against the authorization server.
 * If the middleware does not act as the authorization server, a custom implementation
 * must be published as OSGi service.
 *
 * @author <a href="mailto:steffen.templin@open-xchange.com">Steffen Templin</a>
 * @since v7.8.1
 */
@SingletonService
public interface OAuthAuthorizationService {

    /**
     * Validates the given access token. Tokens are extracted from the HTTP requests
     * <code>Authorization</code> header which follows the <code>bearer</code> scheme.<br>
     * <br>
     * <b>Note:</b> This method is called on every single request. Implementors are strongly
     * advised to add some caching here for performance and load reasons.
     *
     * @param accessToken The access token
     * @return The validation response
     * @throws AuthorizationException E.g. on connection issues
     */
    ValidationResponse validateAccessToken(String accessToken) throws AuthorizationException;
    
    /**
     * Validates the given access token. Tokens are extracted from the HTTP requests
     * <code>Authorization</code> header which follows the <code>bearer</code> scheme.<br>
     * <br>
     * <b>Note:</b> This method is called on every single request. Implementors are strongly
     * advised to add some caching here for performance and load reasons.
     *
     * @param accessToken The access token
     * @param requestHeaders All headers of the HTTP servlet request that is used to trigger the authentication request
     * @return The validation response
     * @throws AuthorizationException E.g. on connection issues
     */
    default ValidationResponse validateAccessToken(String accessToken, Headers requestHeaders) throws AuthorizationException {
        return validateAccessToken(accessToken);
    }

}
