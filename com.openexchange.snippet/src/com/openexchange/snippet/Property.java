/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.snippet;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * {@link Property} - A snippet's properties.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public enum Property {

    /**
     * The snippet's properties.
     */
    PROPERTIES(null),
    /**
     * The snippet's content.
     */
    CONTENT(null),
    /**
     * The snippet's attachments.
     */
    ATTACHMENTS(null),
    /**
     * The property for the identifier.
     */
    ID("id"),
    /**
     * The property for the account identifier.
     */
    ACCOUNT_ID("accountid"),
    /**
     * The property for the type; e.g. <code>"signature"</code>.
     */
    TYPE("type"),
    /**
     * The property for the display name.
     */
    DISPLAY_NAME("displayname"),
    /**
     * The property for the module identifier; e.g. <code>"com.openexchange.mail"</code>.
     */
    MODULE("module"),
    /**
     * The property for the creator.
     */
    CREATED_BY("createdby"),
    /**
     * The property for the shared flag.
     */
    SHARED("shared"),
    /**
     * The property for the optional miscellaneous JSON data.
     */
    MISC("misc"),
    /**
     * The property for the shared-read-only flag.
     */
    SHARED_READ_ONLY("sharedreadonly"),

    ;

    private final String propName;

    private Property(final String propName) {
        this.propName = propName;
    }

    /**
     * Gets the property name
     *
     * @return The property name or <code>null</code> if no property is associated
     */
    public String getPropName() {
        return propName;
    }

    @Override
    public String toString() {
        return propName;
    }

    /**
     * Invokes appropriate switcher's method for this property.
     *
     * @param switcher The switcher
     * @return The possible result object or <code>null</code>
     */
    public Object doSwitch(final PropertySwitch switcher) {
        return switch (this) {
            case ACCOUNT_ID -> switcher.accountId();
            case ATTACHMENTS -> switcher.attachments();
            case CONTENT -> switcher.content();
            case CREATED_BY -> switcher.createdBy();
            case DISPLAY_NAME -> switcher.displayName();
            case ID -> switcher.id();
            case MISC -> switcher.misc();
            case MODULE -> switcher.module();
            case PROPERTIES -> switcher.properties();
            case SHARED -> switcher.shared();
            case SHARED_READ_ONLY -> switcher.sharedReadOnly();
            case TYPE -> switcher.type();
            default -> throw new IllegalArgumentException();
        };
    }

    private static final Set<String> PROP_NAMES = Set.copyOf(
        Arrays.stream(Property.values()).filter(p -> p.getPropName() != null).map(Property::getPropName).collect(Collectors.toSet())
    );

    /**
     * Gets an unmodifiable set containing all property names.
     *
     * @return An unmodifiable set containing all property names
     */
    public static Set<String> getPropertyNames() {
        return PROP_NAMES;
    }

}
