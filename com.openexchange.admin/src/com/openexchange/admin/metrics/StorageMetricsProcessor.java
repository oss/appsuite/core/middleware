/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.metrics;

import com.openexchange.admin.metrics.ProvisioningMetricsProcessor.Caller;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer.Sample;

/**
 * Processor to track internal storage calls
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public final class StorageMetricsProcessor extends ProvisioningMetricsProcessor {

    private static final String METRIC_PREFIX_APPSUITE_PROVISIONING_STORAGE = "appsuite.provisioning.storage.";
    private static final String METRIC_APPSUITE_PROVISIONING_STORAGE_DURATION = METRIC_PREFIX_APPSUITE_PROVISIONING_STORAGE.concat("duration");
    private static final String TAG_STORAGE_CLASS = "storage_class";

    private StorageMetricsProcessor(OperationType type) {
        super(type);
    }

    private StorageMetricsProcessor(OperationType type, Caller caller) {
        super(type, caller);
    }

    /**
     * Returns an {@link StorageMetricsProcessor} that can be used to track CREATE operations.
     *
     * @return {@link StorageMetricsProcessor} for create.
     */
    public static final StorageMetricsProcessor getForCreate() {
        return new StorageMetricsProcessor(OperationType.CREATE);
    }

    /**
     * Returns an {@link StorageMetricsProcessor} that can be used to track CREATE operations.
     * Use this method if you would like to have more detailed information about the caller of the operation.
     *
     * @param clazz The class of the object used
     * @param method The method of the the object which was called
     * 
     * @return {@link StorageMetricsProcessor} for create.
     * @see #getForCreate()
     */
    public static final StorageMetricsProcessor getForCreate(String clazz, String identifier) {
        return new StorageMetricsProcessor(OperationType.CREATE, new Caller(clazz, identifier));
    }

    /**
     * Returns an {@link StorageMetricsProcessor} that can be used to track DELETE operations.
     *
     * @return {@link StorageMetricsProcessor} for delete.
     */
    public static final StorageMetricsProcessor getForDelete() {
        return new StorageMetricsProcessor(OperationType.DELETE);
    }

    /**
     * Returns an {@link StorageMetricsProcessor} that can be used to track DELETE operations.
     * Use this method if you would like to have more detailed information about the caller of the operation.
     *
     * @param clazz The class of the object used
     * @param method The method of the the object which was called
     * 
     * @return {@link StorageMetricsProcessor} for create.
     */
    public static final StorageMetricsProcessor getForDelete(String clazz, String identifier) {
        return new StorageMetricsProcessor(OperationType.DELETE, new Caller(clazz, identifier));
    }

    /**
     * Returns an {@link StorageMetricsProcessor} that can be used to track CHANGE operations.
     *
     * @return {@link StorageMetricsProcessor} for change.
     */
    public static final StorageMetricsProcessor getForChange() {
        return new StorageMetricsProcessor(OperationType.CHANGE);
    }

    /**
     * Returns an {@link StorageMetricsProcessor} that can be used to track CHANGE operations.
     * Use this method if you would like to have more detailed information about the caller of the operation.
     *
     * @param clazz The class of the object used
     * @param method The method of the the object which was called
     * 
     * @return {@link StorageMetricsProcessor} for create.
     */
    public static final StorageMetricsProcessor getForChange(String clazz, String identifier) {
        return new StorageMetricsProcessor(OperationType.CHANGE, new Caller(clazz, identifier));
    }

    /**
     * Returns an {@link StorageMetricsProcessor} that can be used to track READ operations.
     *
     * @return {@link StorageMetricsProcessor} for read.
     */
    public static final StorageMetricsProcessor getForRead() {
        return new StorageMetricsProcessor(OperationType.READ);
    }

    /**
     * Returns an {@link StorageMetricsProcessor} that can be used to track READ operations.
     * Use this method if you would like to have more detailed information about the caller of the operation.
     *
     * @param clazz The class of the object used
     * @param method The method of the the object which was called
     * 
     * @return {@link StorageMetricsProcessor} for create.
     */
    public static final StorageMetricsProcessor getForRead(String clazz, String identifier) {
        return new StorageMetricsProcessor(OperationType.READ, new Caller(clazz, identifier));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getMetricDescription() {
        return "Measures the duration of internal storage calls that were triggered by provisioning calls.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Tags getTags(Caller caller, OperationType type) {
        return Tags.of(TAG_STORAGE_CLASS, caller.clazz(), TAG_OPERATION, type.toString(), TAG_METHOD, caller.method());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stopTimer(Sample timerSample, boolean success) {
        stopTimer(METRIC_APPSUITE_PROVISIONING_STORAGE_DURATION, timerSample, success);
    }
}
