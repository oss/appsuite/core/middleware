/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.metrics;

import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer.Sample;

/**
 * Processor to track API calls
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public final class APIMetricsProcessor extends ProvisioningMetricsProcessor {

    private static final String METRIC_PREFIX_APPSUITE_PROVISIONING_API = "appsuite.provisioning.api.";
    private static final String METRIC_APPSUITE_PROVISIONING_API_DURATION = METRIC_PREFIX_APPSUITE_PROVISIONING_API.concat("duration");
    private static final String TAG_API_CLASS = "api_class";

    private APIMetricsProcessor(OperationType type) {
        super(type);
    }

    /**
     * Returns an {@link APIMetricsProcessor} that can be used to track CREATE operations.
     *
     * @return {@link APIMetricsProcessor} for create.
     */
    public static final APIMetricsProcessor getForCreate() {
        return new APIMetricsProcessor(OperationType.CREATE);
    }

    /**
     * Returns an {@link APIMetricsProcessor} that can be used to track CREATE operations.
     *
     * @return {@link APIMetricsProcessor} for delete.
     */
    public static final APIMetricsProcessor getForDelete() {
        return new APIMetricsProcessor(OperationType.DELETE);
    }

    /**
     * Returns an {@link APIMetricsProcessor} that can be used to track CHANGE operations.
     *
     * @return {@link APIMetricsProcessor} for change.
     */
    public static final APIMetricsProcessor getForChange() {
        return new APIMetricsProcessor(OperationType.CHANGE);
    }

    /**
     * Returns an {@link APIMetricsProcessor} that can be used to track READ operations.
     *
     * @return {@link APIMetricsProcessor} for read.
     */
    public static final APIMetricsProcessor getForRead() {
        return new APIMetricsProcessor(OperationType.READ);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getMetricDescription() {
        return "Measures the duration of provisioning operations via API calls.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Tags getTags(Caller caller, OperationType type) {
        return Tags.of(TAG_API_CLASS, caller.clazz(), TAG_OPERATION, type.toString(), TAG_METHOD, caller.method());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stopTimer(Sample timerSample, boolean success) {
        stopTimer(METRIC_APPSUITE_PROVISIONING_API_DURATION, timerSample, success);
    }
}
