
package com.openexchange.admin.metrics;

import java.lang.invoke.MethodType;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.Timer.Sample;

/**
 * Abstract class to provide basic fields and methods that should be used for exposing provisioning metrics
 * 
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public abstract class ProvisioningMetricsProcessor {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ProvisioningMetricsProcessor.class);

    protected static final String TAG_METHOD = "method";
    protected static final String TAG_OPERATION = "operation";
    private static final String TAG_STATUS = "status";
    private static final String TAG_STATUS_OK = "OK";
    private static final String TAG_STATUS_ERROR = "ERROR";

    private static Map<String, Timer> timers = new ConcurrentHashMap<>();
    private final OperationType type;
    private final Caller caller;

    /**
     * Initializes a new {@link ProvisioningMetricsProcessor}.
     * 
     * @param caller The {@link Caller} that desires to use the processor.
     * @param type The {@link OperationType} the processor is used for.
     */
    protected ProvisioningMetricsProcessor(OperationType type, Caller caller) {
        this.type = type;
        this.caller = caller;
    }

    /**
     * Initializes a new {@link ProvisioningMetricsProcessor}.
     *
     * @param type The {@link OperationType} the processor is used for.
     */
    protected ProvisioningMetricsProcessor(OperationType type) {
        this(type, getCaller());
    }

    /**
     * Type (classification) of the operation that is executed
     */
    protected enum OperationType {
        /** Operation with the purpose to create data */
        CREATE,
        /** Operation with the purpose to read data */
        READ,
        /** Operation with the purpose to update data */
        CHANGE,
        /** Operation with the purpose to delete data */
        DELETE,
    }

    /**
     * Stops the timer for the metric
     *
     * @param metric The name of the metric
     * @param timerSample The {@link Timer} to stop
     * @param success if the call was successful or not
     */
    protected void stopTimer(String metric, Timer.Sample timerSample, boolean success) {
        Tags tags = getTags(this.caller, this.type);
        if (success) {
            stopTimerWithStatus(metric, tags, timerSample, TAG_STATUS_OK);
            return;
        }
        stopTimerWithStatus(metric, tags, timerSample, TAG_STATUS_ERROR);
    }

    /**
     * Returns the detailed description of the metric related to the implementing processor.
     *
     * @return {@link String} with the description of the related metric.
     */
    protected abstract String getMetricDescription();

    /**
     * Returns the tags that are relevant for the current metric.
     *
     * @param caller The {@link Caller} that desires to use the processor.
     * @param type The {@link OperationType} the processor is used for.
     * @return {@link Tags} with the image that should be used for the metric.
     */
    protected abstract Tags getTags(Caller caller, OperationType type);

    /**
     * Stops the timer for for the metric provided with the {@link Sample} parameter.
     *
     * @param timerSample the {@link Sample} to stop
     * @param success if the execution was successful or not
     */
    protected abstract void stopTimer(Sample timerSample, boolean success);

    private void stopTimerWithStatus(String metric, Tags tags, Timer.Sample timerSample, String status) {
        String timerKey = getTimerKey(metric, tags, status);
        Timer timer = timers.computeIfAbsent(timerKey, k -> {
            return Timer.builder(metric).description(getMetricDescription()).tags(tags.and(TAG_STATUS, status)).register(Metrics.globalRegistry);
        });
        timerSample.stop(timer);
    }

    /**
     * Returns the internally used key to uniquely identify the given timer based on the given metric/tags/status combination.
     *
     * @param metric The name of the metric.
     * @param tags The tags for the current metric.
     * @param status The status of the execution.
     * @return {@link String} with the unique identifier for the metric/tags/status combination.
     */
    private String getTimerKey(final String metric, final Tags tags, final String status) {
        return metric.concat(tags.and(TAG_STATUS, status).toString());
    }

    /**
     * Returns the {@link Caller} containing the class and method the metrics should be tracked for
     * 
     * @return {@link Caller} with the class and method. When the frame cannot be obtained, {@link Caller.NOT_AVAILABLE} will be returned.
     */
    private static Caller getCaller() {
        StackWalker stackWalker = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE);
        StackWalker.StackFrame frame = stackWalker.walk(stream -> stream.limit(8).filter(stackFrame -> !stackFrame.getClassName().startsWith(ProvisioningMetricsProcessor.class.getPackageName())).findFirst().orElse(null));
        if (frame != null) {
            try {
                String className = frame.getClassName();
                MethodType methodType = frame.getMethodType();
                String methodSignature = frame.getMethodName() + methodType.toString();
                return new Caller(className, methodSignature);
            } catch (Exception e) {
                LOG.error("Error while retrieving java stacktrace information. Proceed with 'NA' caller", e);
            }
        }
        return Caller.NOT_AVAILABLE;
    }

    /**
     * Record containing information about class and method the metrics should be tracked for.
     * 
     * @param clazz class name
     * @param method method name of the class
     */
    protected record Caller(String clazz, String method) {

        protected static final Caller NOT_AVAILABLE = new Caller("N/A", "N/A");

        public Caller {
            Objects.requireNonNull(clazz);
            Objects.requireNonNull(method);
        }

        @Override
        public String toString() {
            return clazz + "." + method;
        }
    }
}
