/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.metrics;

import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer.Sample;

/**
 * Processor to track PluginInterface calls
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public final class PluginMetricsProcessor extends ProvisioningMetricsProcessor {

    private static final String METRIC_PREFIX_APPSUITE_PROVISIONING_PLUGIN = "appsuite.provisioning.plugin.";
    private static final String METRIC_APPSUITE_PROVISIONING_PLUGIN_DURATION = METRIC_PREFIX_APPSUITE_PROVISIONING_PLUGIN.concat("duration");
    private static final String TAG_PLUGIN_NAME = "plugin_name";

    private PluginMetricsProcessor(OperationType type, Caller caller) {
        super(type, caller);
    }

    /**
     * Returns an {@link PluginMetricsProcessor} that can be used to track CREATE operations.
     * 
     * @param clazz The class of the object used
     * @param method The method of the the object which was called
     *
     * @return {@link PluginMetricsProcessor} for create.
     */
    public static final PluginMetricsProcessor getForCreate(String clazz, String method) {
        return new PluginMetricsProcessor(OperationType.CREATE, new Caller(clazz, method));
    }

    /**
     * Returns an {@link PluginMetricsProcessor} that can be used to track DELETE operations.
     *
     * @param clazz The class of the object used
     * @param method The method of the the object which was called
     * 
     * @return {@link PluginMetricsProcessor} for delete.
     */
    public static final PluginMetricsProcessor getForDelete(String clazz, String method) {
        return new PluginMetricsProcessor(OperationType.DELETE, new Caller(clazz, method));
    }

    /**
     * Returns an {@link PluginMetricsProcessor} that can be used to track CHANGE operations.
     *
     * @param clazz The class of the object used
     * @param method The method of the the object which was called
     * 
     * @return {@link PluginMetricsProcessor} for change.
     */
    public static final PluginMetricsProcessor getForChange(String clazz, String method) {
        return new PluginMetricsProcessor(OperationType.CHANGE, new Caller(clazz, method));
    }

    /**
     * Returns an {@link PluginMetricsProcessor} that can be used to track READ operations.
     *
     * @param clazz The class of the object used
     * @param method The method of the the object which was called
     * 
     * @return {@link PluginMetricsProcessor} for read.
     */
    public static final PluginMetricsProcessor getForRead(String clazz, String method) {
        return new PluginMetricsProcessor(OperationType.READ, new Caller(clazz, method));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getMetricDescription() {
        return "Measures the duration of operations executed by registered PluginInterfaces.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Tags getTags(Caller caller, OperationType type) {
        return Tags.of(TAG_PLUGIN_NAME, caller.clazz(), TAG_OPERATION, type.toString(), TAG_METHOD, caller.method());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stopTimer(Sample timerSample, boolean success) {
        stopTimer(METRIC_APPSUITE_PROVISIONING_PLUGIN_DURATION, timerSample, success);
    }
}
