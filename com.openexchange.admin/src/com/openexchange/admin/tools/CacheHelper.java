/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.tools;

import static com.openexchange.cache.v2.filter.CacheFilters.asHashPart;
import static com.openexchange.java.Autoboxing.b;
import static com.openexchange.java.Autoboxing.i;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import com.google.common.collect.Sets;
import com.openexchange.admin.rmi.dataobjects.User;
import com.openexchange.admin.services.AdminServiceRegistry;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;
import com.openexchange.exception.OXException;

/**
 * {@link CacheHelper} - Utility to invalidate caches after provisioning operations.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public final class CacheHelper {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CacheHelper.class);

    /** Core module names for caches holding user-related data */
    private static final CoreModuleName[] USER_MODULE_NAMES = {
        CoreModuleName.ALIAS, CoreModuleName.CAPABILITIES, CoreModuleName.USER, CoreModuleName.USER_IMAP_LOGINS,
        CoreModuleName.USER_LOGINS, CoreModuleName.USER_PERMISSION_BITS, CoreModuleName.USER_SETTING_MAIL
    };

    /** Core module names for such cache that fire events along-side with invalidations */
    private static final Set<CoreModuleName> EVENT_FIRING_MODULE_NAMES = Sets.immutableEnumSet(CoreModuleName.USER); // for com.openexchange.config.cascade.impl.UserCacheListener

    /**
     * Initializes a new {@link CacheHelper}.
     */
    private CacheHelper() {
        super();
    }

    /**
     * Invalidates user data from the following core caches:
     * <ul>
     * <li>{@link CoreModuleName#ALIAS}</li>
     * <li>{@link CoreModuleName#CAPABILITIES}</li>
     * <li>{@link CoreModuleName#USER}</li>
     * <li>{@link CoreModuleName#USER_IMAP_LOGINS}</li>
     * <li>{@link CoreModuleName#USER_LOGINS}</li>
     * <li>{@link CoreModuleName#USER_PERMISSION_BITS}</li>
     * <li>{@link CoreModuleName#USER_SETTING_MAIL}</li>
     * </ul>
     *
     * Any exceptions are caught and logged.
     *
     * @param contextId The context identifier
     * @param users The users to invalidate the caches for
     * @throws OXException If an error happens during invalidation
     */
    public static void invalidateUserCachesQuietly(int contextId, User... users) {
        try {
            invalidateUserCaches(contextId, users);
        } catch (Exception e) {
            LOG.error("Unexpected error while invalidating user caches", e);
        }
    }

    /**
     * Invalidates user data from the following core caches:
     * <ul>
     * <li>{@link CoreModuleName#ALIAS}</li>
     * <li>{@link CoreModuleName#CAPABILITIES}</li>
     * <li>{@link CoreModuleName#USER}</li>
     * <li>{@link CoreModuleName#USER_IMAP_LOGINS}</li>
     * <li>{@link CoreModuleName#USER_LOGINS}</li>
     * <li>{@link CoreModuleName#USER_PERMISSION_BITS}</li>
     * <li>{@link CoreModuleName#USER_SETTING_MAIL}</li>
     * </ul>
     *
     * @param contextId The context identifier
     * @param users The users to invalidate the caches for
     * @throws OXException If an error happens during invalidation
     */
    public static void invalidateUserCaches(int contextId, User... users) throws OXException {
        if (null == users || 0 == users.length) {
            return;
        }
        invalidateCaches(contextId, users, USER_MODULE_NAMES);
    }

    /**
     * Invalidates user data from the following core caches:
     * <ul>
     * <li>{@link CoreModuleName#ALIAS}</li>
     * <li>{@link CoreModuleName#CAPABILITIES}</li>
     * <li>{@link CoreModuleName#USER}</li>
     * <li>{@link CoreModuleName#USER_IMAP_LOGINS}</li>
     * <li>{@link CoreModuleName#USER_LOGINS}</li>
     * <li>{@link CoreModuleName#USER_PERMISSION_BITS}</li>
     * <li>{@link CoreModuleName#USER_SETTING_MAIL}</li>
     * </ul>
     *
     * Any exceptions are caught and logged.
     *
     * @param contextId The context identifier
     * @param userIds The identifiers of the users to invalidate the caches for
     * @throws OXException If an error happens during invalidation
     */
    public static void invalidateUserCachesQuietly(int contextId, int... userIds) {
        try {
            invalidateUserCaches(contextId, userIds);
        } catch (Exception e) {
            LOG.error("Unexpected error while invalidating user caches", e);
        }
    }

    /**
     * Invalidates user data from the following core caches:
     * <ul>
     * <li>{@link CoreModuleName#ALIAS}</li>
     * <li>{@link CoreModuleName#CAPABILITIES}</li>
     * <li>{@link CoreModuleName#USER}</li>
     * <li>{@link CoreModuleName#USER_IMAP_LOGINS}</li>
     * <li>{@link CoreModuleName#USER_LOGINS}</li>
     * <li>{@link CoreModuleName#USER_PERMISSION_BITS}</li>
     * <li>{@link CoreModuleName#USER_SETTING_MAIL}</li>
     * </ul>
     *
     * @param contextId The context identifier
     * @param userIds The identifiers of the users to invalidate the caches for
     * @throws OXException If an error happens during invalidation
     */
    public static void invalidateUserCaches(int contextId, int... userIds) throws OXException {
        if (null == userIds || 0 == userIds.length) {
            return;
        }
        invalidateCaches(contextId, userIds, USER_MODULE_NAMES);
    }

    /**
     * Invalidates user data from specific core caches.
     * <p/>
     * Any exceptions are caught and logged.
     *
     * @param moduleNames The core module caches to consider for invalidation
     * @param contextId The context identifier
     * @param userIds The identifiers of the users to invalidate the caches for
     * @throws OXException If an error happens during invalidation
     */
    public static void invalidateUserCachesQuietly(CoreModuleName[] moduleNames, int contextId, int... userIds) {
        try {
            invalidateUserCaches(moduleNames, contextId, userIds);
        } catch (Exception e) {
            LOG.error("Unexpected error invalidating user caches", e);
        }
    }

    /**
     * Invalidates user data from specific core caches.
     *
     * @param moduleNames The core module caches to consider for invalidation
     * @param contextId The context identifier
     * @param userIds The identifiers of the users to invalidate the caches for
     * @throws OXException If an error happens during invalidation
     */
    public static void invalidateUserCaches(CoreModuleName[] moduleNames, int contextId, int... userIds) throws OXException {
        invalidateCaches(contextId, userIds, moduleNames);
    }

    /**
     * Invalidates data from the {@link CoreModuleName#USER} cache.
     *
     * Any exceptions are caught and logged.
     *
     * @param contextId The context identifier
     * @param userIds The identifiers of the users to invalidate the cache for
     * @throws OXException If an error happens during invalidation
     */
    public static void invalidateUserCacheQuietly(int contextId, int... userIds) {
        try {
            invalidateUserCache(contextId, userIds);
        } catch (Exception e) {
            LOG.error("Unexpected error while invalidating user caches", e);
        }
    }

    /**
     * Invalidates data from the {@link CoreModuleName#USER} cache.
     *
     * @param contextId The context identifier
     * @param userIds The identifiers of the users to invalidate the cache for
     * @throws OXException If an error happens during invalidation
     */
    public static void invalidateUserCache(int contextId, int... userIds) throws OXException {
        if (null == userIds || 0 == userIds.length) {
            return;
        }
        invalidateCaches(contextId, userIds, new CoreModuleName[] { CoreModuleName.USER });
    }

    /**
     * Invalidates data from the {@link CoreModuleName#USER}, {@link CoreModuleName#USER_IMAP_LOGINS} and
     * {@link CoreModuleName#USER_LOGINS} caches.
     *
     * Any exceptions are caught and logged.
     *
     * @param contextId The context identifier
     * @param userIds The users to invalidate the cache for
     * @throws OXException If an error happens during invalidation
     */
    public static void invalidateUserCacheQuietly(int contextId, User... users) {
        try {
            invalidateUserCache(contextId, users);
        } catch (Exception e) {
            LOG.error("Unexpected error while invalidating user caches", e);
        }
    }

    /**
     * Invalidates data from the {@link CoreModuleName#USER}, {@link CoreModuleName#USER_IMAP_LOGINS} and
     * {@link CoreModuleName#USER_LOGINS} caches.
     *
     * @param contextId The context identifier
     * @param users The users to invalidate the cache for
     * @throws OXException If an error happens during invalidation
     */
    public static void invalidateUserCache(int contextId, User... users) throws OXException {
        if (null == users || 0 == users.length) {
            return;
        }
        invalidateCaches(contextId, users, new CoreModuleName[] { CoreModuleName.USER, CoreModuleName.USER_IMAP_LOGINS, CoreModuleName.USER_LOGINS });
    }

    /**
     * Invalidates data from the {@link CoreModuleName#GROUP} cache.
     * <p/>
     * Any exceptions are caught and logged.
     *
     * @param contextId The context identifier
     * @param groupIds The identifiers of the groups to invalidate the cache for
     * @throws OXException If an error happens during invalidation
     */
    public static void invalidateGroupCacheQuietly(int contextId, int... groupIds) {
        try {
            invalidateCaches(contextId, groupIds, new CoreModuleName[] { CoreModuleName.GROUP });
        } catch (Exception e) {
            LOG.error("Unexpected error invalidating caches", e);
        }
    }

    /**
     * Invalidates data from the {@link CoreModuleName#FILESTORE} cache.
     * <p/>
     * Any exceptions are caught and logged.
     *
     * @param filestoreIds The identifiers of the file storages to invalidate the cache for
     * @throws OXException If an error happens during invalidation
     */
    public static void invalidateFilestoreCacheQuietly(int... filestoreIds) {
        if (null == filestoreIds || 0 == filestoreIds.length) {
            return;
        }
        List<CacheFilter> cacheFilters = IntStream.of(filestoreIds).mapToObj(i -> CacheFilter.builder().withCoreModuleName(CoreModuleName.FILESTORE).addSuffix(i).build()).toList();
        try {
            invalidateCaches(cacheFilters);
        } catch (Exception e) {
            LOG.error("Unexpected error invalidating caches", e);
        }
    }

    private static void invalidateCaches(int contextId, User[] users, CoreModuleName[] moduleNames) throws OXException {
        if (null == users || 0 == users.length || null == moduleNames || 0 == moduleNames.length) {
            return;
        }
        invalidateCaches(buildCacheFilters(contextId, users, moduleNames));
    }

    private static void invalidateCaches(int contextId, int[] objectIds, CoreModuleName[] moduleNames) throws OXException {
        if (null == objectIds || 0 == objectIds.length || null == moduleNames || 0 == moduleNames.length) {
            return;
        }
        Map<Boolean, List<CoreModuleName>> moduleNamesByFireEvents = Stream.of(moduleNames).collect(Collectors.partitioningBy(EVENT_FIRING_MODULE_NAMES::contains));
        for (Map.Entry<Boolean, List<CoreModuleName>> entry : moduleNamesByFireEvents.entrySet()) {
            invalidateCaches(buildCacheFilters(contextId, objectIds, entry.getValue()), b(entry.getKey()));
        }
    }

    private static void invalidateCaches(List<CacheFilter> filters) throws OXException {
        invalidateCaches(filters, false);
    }

    private static void invalidateCaches(List<CacheFilter> filters, boolean fireEvents) throws OXException {
        if (null == filters || filters.isEmpty()) {
            return;
        }
        InvalidationCacheService invalidationService = AdminServiceRegistry.getInstance().getService(InvalidationCacheService.class);
        if (null == invalidationService) {
            LOG.warn("Unable to access cache invalidation service, unable to invalidate caches.");
            return;
        }
        invalidationService.invalidate(filters, fireEvents);
    }

    private static List<CacheFilter> buildCacheFilters(int contextId, User[] users, CoreModuleName[] moduleNames) {
        List<CacheFilter> filters = new LinkedList<CacheFilter>();
        for (User user : users) {
            for (CoreModuleName moduleName : moduleNames) {
                switch (moduleName) {
                    case ALIAS, USER, CAPABILITIES, USER_PERMISSION_BITS, USER_SETTING_MAIL -> filters.add(buildCacheFilter(moduleName, contextId, i(user.getId())));
                    case USER_IMAP_LOGINS -> Optional.ofNullable(user.getImapLogin()).ifPresent(s -> filters.add(buildCacheFilter(moduleName, contextId, s)));
                    case USER_LOGINS -> Optional.ofNullable(user.getName()).ifPresent(s -> filters.add(buildCacheFilter(moduleName, contextId, s)));
                    default -> { /* nothing */ }
                }
            }
        }
        return filters;
    }

    private static List<CacheFilter> buildCacheFilters(int contextId, int[] objectIds, Collection<CoreModuleName> moduleNames) {
        List<CacheFilter> filters = new LinkedList<CacheFilter>();
        for (int objectId : objectIds) {
            for (CoreModuleName moduleName : moduleNames) {
                switch (moduleName) {
                    case ALIAS, GROUP, USER, CAPABILITIES, USER_PERMISSION_BITS, USER_SETTING_MAIL -> filters.add(buildCacheFilter(moduleName, contextId, objectId));
                    default -> { /* nothing */ }
                }
            }
        }
        return filters;
    }

    /**
     * Builds a {@link CacheFilter} using the supplied module name, appending one numerical suffix as <i>hashtag</i> as well as other
     * suffix strings.
     *
     * @param moduleName The core module name to use
     * @param hashSuffix The first suffix to add as <i>hashtag</i>
     * @param suffixes More suffixes to add
     * @return The built filter
     */
    private static CacheFilter buildCacheFilter(CoreModuleName moduleName, int hashSuffix, String... suffixes) {
        CacheFilter.Builder builder = CacheFilter.builder().withCoreModuleName(moduleName).addSuffix(asHashPart(hashSuffix));
        if (null != suffixes) {
            for (String s : suffixes) {
                builder.addSuffix(s);
            }
        }
        return builder.build();
    }

    /**
     * Builds a {@link CacheFilter} using the supplied module name, appending one numerical suffix as <i>hashtag</i> as well as other
     * numerical suffixes.
     *
     * @param moduleName The core module name to use
     * @param hashSuffix The first suffix to add as <i>hashtag</i>
     * @param suffixes More suffixes to add
     * @return The built filter
     */
    private static CacheFilter buildCacheFilter(CoreModuleName moduleName, int hashSuffix, int... suffixes) {
        CacheFilter.Builder builder = CacheFilter.builder().withCoreModuleName(moduleName).addSuffix(asHashPart(hashSuffix));
        if (null != suffixes) {
            for (int suffix : suffixes) {
                builder.addSuffix(suffix);
            }
        }
        return builder.build();
    }

}
