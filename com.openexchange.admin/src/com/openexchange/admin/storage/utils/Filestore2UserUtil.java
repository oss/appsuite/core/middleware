/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.storage.utils;

import static com.openexchange.database.DBPoolingExceptionCodes.NOT_RESOLVED_SERVER;
import static com.openexchange.database.Databases.closeSQLStuff;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLNonTransientConnectionException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.OptionalLong;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.LockSupport;
import com.google.common.collect.Lists;
import com.openexchange.admin.daemons.AdminDaemon;
import com.openexchange.admin.rmi.exceptions.PoolException;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.services.AdminServiceRegistry;
import com.openexchange.admin.tools.AdminCache;
import com.openexchange.admin.tools.AdminCacheExtended;
import com.openexchange.database.DBPoolingExceptionCodes;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.database.SchemaInfo;
import com.openexchange.exception.OXException;
import com.openexchange.java.ExceptionCatchingRunnable;
import com.openexchange.java.Sets;
import com.openexchange.java.Strings;
import com.openexchange.session.UserAndContext;
import com.openexchange.threadpool.BoundedCompletionService;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.threadpool.ThreadPools;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.ScheduledTimerTasks;
import com.openexchange.timer.TimerService;
import com.openexchange.tools.sql.DBUtils;

/**
 * {@link Filestore2UserUtil}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.2
 */
public final class Filestore2UserUtil {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(Filestore2UserUtil.class);

    private static final ThreadPools.ExpectedExceptionFactory<StorageException> EXCEPTION_FACTORY = new ThreadPools.ExpectedExceptionFactory<StorageException>() {

        @Override
        public StorageException newUnexpectedError(final Throwable t) {
            return new StorageException(t);
        }

        @Override
        public Class<StorageException> getType() {
            return StorageException.class;
        }
    };

    /** The enumeration of known reasons */
    private static enum Reason { // NOSONARLINT

        /**
         * The reason identifier (<code>57234</code>) to fill the <code>"filestore2user"</code> table.
         */
        FILL_FILESTORE2USER(57234),
        /**
         * The reason identifier (<code>57235</code>) to fill the <code>"users_per_filestore"</code> table.
         */
        FILL_USERS_PER_FILESTORE(57235),
        /**
         * The reason identifier (<code>57236</code>) for <code>"users_per_filestore"</code> table's re-initialization lock.
         */
        USERS_PER_FILESTORE_CONSISTENCY_LOCK(57236),
        /**
         * The reason identifier (<code>57237</code>) for <code>"users_per_filestore"</code> table's write lock.
         */
        USERS_PER_FILESTORE_WRITE_LOCK(57237);

        private final int identifier;

        private Reason(int identifier) {
            this.identifier = identifier;
        }

        /**
         * Gets the reason identifier; e.g <code>57237</code>.
         *
         * @return The reason identifier
         */
        public int getIdentifier() {
            return identifier;
        }
    }

    private static final String TEXT_TERMINATED = "TERMINATED";
    private static final String TEXT_PROCESSING = "PROCESSING";

    private static final String TABLE_NAME_FILESTORE2USER = "filestore2user";
    private static final String TABLE_NAME_USERS_PER_FILESTORE = "users_per_filestore";
    private static final String TABLE_NAME_REASON_TEXT = "reason_text";

    /**
     * Initializes a new {@link Filestore2UserUtil}.
     */
    private Filestore2UserUtil() {
        super();
    }

    /**
     * Logs specified pooling error.
     *
     * @param e The pooling error to log
     */
    private static void logPoolingError(PoolException e) {
        if (e != null) {
            LOG.error("Pooling error", e);
        }
    }

    /**
     * Checks if either table <code>"filestore2user"</code> or table <code>"users_per_filestore"</code> are not yet initialized.
     *
     * @param cache The admin cache to use
     * @return <code>true</code> if not terminated; otherwise <code>false</code>
     * @throws StorageException If check fails
     */
    public static boolean isNotTerminated(AdminCacheExtended cache) throws StorageException {
        Connection con = null;
        try {
            con = cache.getReadConnectionForConfigDB();
            return isNotTerminated(con);
        } catch (PoolException | SQLException e) {
            throw new StorageException(e);
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            if (null != con) {
                try {
                    cache.pushReadConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    /**
     * Gets the users for specified file store.
     *
     * @param filestoreId The file store identifier
     * @param cache The admin cache to use
     * @return The users
     * @throws StorageException If users cannot be returned
     */
    public static Set<UserAndContext> getUsersFor(int filestoreId, AdminCacheExtended cache) throws StorageException {
        Connection con = null;
        try {
            con = cache.getReadConnectionForConfigDB();

            // Check if processing
            if (isNotTerminated(con)) {
                throw new StorageException("Table \"" + TABLE_NAME_FILESTORE2USER + "\" not yet initialized");
            }

            return getUsersFor(filestoreId, con);
        } catch (PoolException | SQLException e) {
            throw new StorageException(e);
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            if (null != con) {
                try {
                    cache.pushReadConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    private static Set<UserAndContext> getUsersFor(int filestoreId, Connection con) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT `cid`, `user` FROM `" + TABLE_NAME_FILESTORE2USER + "` WHERE `filestore_id` = ? ORDER BY `cid`, `user`");
            stmt.setInt(1, filestoreId);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return Collections.emptySet();
            }

            Set<UserAndContext> users = new LinkedHashSet<UserAndContext>();
            do {
                users.add(UserAndContext.newInstance(rs.getInt(2), rs.getInt(1)));
            } while (rs.next());
            return users;
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    /**
     * Gets the user count for specified file store.
     *
     * @param filestoreId The file store identifier
     * @param cache The admin cache to use
     * @return The user count
     * @throws StorageException If user count cannot be returned
     */
    public static int getUserCountFor(int filestoreId, AdminCache cache) throws StorageException {
        Connection con = null;
        try {
            con = cache.getReadConnectionForConfigDB();

            // Check if processing
            if (isNotTerminated(con)) {
                throw new StorageException("Table \"" + TABLE_NAME_FILESTORE2USER + "\" not yet initialized");
            }

            return getUserCountFor(filestoreId, con);
        } catch (PoolException | SQLException e) {
            throw new StorageException(e);
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            if (null != con) {
                try {
                    cache.pushReadConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    private static int getUserCountFor(int filestoreId, Connection con) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT COUNT(*) FROM `" + TABLE_NAME_FILESTORE2USER + "` WHERE `filestore_id`=?");
            stmt.setInt(1, filestoreId);
            rs = stmt.executeQuery();
            return rs.next() ? rs.getInt(1) : 0;
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    /**
     * Gets the sorted (lowest first) file store user counts.
     *
     * @param cache The admin cache to use
     * @return The sorted file store user counts
     * @throws StorageException If file store user counts cannot be returned
     */
    public static FilestoreCountCollection getUserCounts(AdminCacheExtended cache) throws StorageException {
        Connection con = null;
        try {
            con = cache.getReadConnectionForConfigDB();

            // Check if processing
            if (isNotTerminated(con)) {
                throw new StorageException("Table \"" + TABLE_NAME_FILESTORE2USER + "\" not yet initialized");
            }

            return getUserCounts(con);
        } catch (PoolException e) {
            throw new StorageException(e);
        } catch (SQLException e) {
            throw new StorageException(e);
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            if (null != con) {
                try {
                    cache.pushReadConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    /**
     * Gets the sorted (lowest first) file store user counts.
     *
     * @param configDbCon The connection to ConfigDB to use
     * @return The sorted file store user counts
     * @throws StorageException If file store user counts cannot be returned
     */
    public static FilestoreCountCollection getUserCounts(Connection configDbCon) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = configDbCon.prepareStatement("SELECT `filestore_id`, COUNT(*) AS user_count FROM `" + TABLE_NAME_FILESTORE2USER + "` GROUP BY `filestore_id`");
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return new FilestoreCountCollection(Collections.<Integer, FilestoreCount> emptyMap());
            }

            Map<Integer, FilestoreCount> counts = new HashMap<Integer, FilestoreCount>();
            do {
                int filestoreId = rs.getInt(1);
                counts.put(Integer.valueOf(filestoreId), new FilestoreCount(filestoreId, rs.getInt(2)));
            } while (rs.next());
            return new FilestoreCountCollection(counts);
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    /**
     * Checks if table <code>"filestore2user"</code> is accessible.
     *
     * @param cache The admin cache used to acquire a connection
     * @throws StorageException If operation fails
     */
    private static void checkBeforeAccess(AdminCache cache) throws StorageException {
        Connection con = null;
        try {
            con = cache.getReadConnectionForConfigDB();

            if (tableDoesNotExist(con, TABLE_NAME_FILESTORE2USER)) {
                LOG.warn("Table \"{}\" does not exist.", TABLE_NAME_FILESTORE2USER);
                return;
            }

            // Check if processing
            if (isNotTerminated(con)) {
                throw new StorageException("Table \"" + TABLE_NAME_FILESTORE2USER + "\" not yet initialized");
            }
        } catch (PoolException | SQLException e) {
            throw new StorageException(e);
        } finally {
            if (null != con) {
                try {
                    cache.pushReadConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    /**
     * Adds specified entry to <code>"filestore2user"</code> table.
     *
     * @param contextId The context identifier
     * @param userId The user identifier
     * @param filestoreId The file store identifier
     * @param cache The admin cache to use
     * @throws StorageException If operation fails
     */
    public static void addFilestore2UserEntry(int contextId, int userId, int filestoreId, AdminCache cache) throws StorageException {
        checkBeforeAccess(cache);

        Connection con = null;
        int rollback = 0;
        DatabaseLock databaseLock = null;
        try {
            // Acquire lock
            databaseLock = awaitWriteLock(cache);

            // Start database transaction
            con = cache.getWriteConnectionForConfigDB();
            Databases.startTransaction(con);
            rollback = 1;

            // Insert entry
            insertEntry(new FilestoreEntry(contextId, userId, filestoreId), con);

            // Commit
            con.commit();
            rollback = 2;
        } catch (PoolException | SQLException e) {
            throw new StorageException(e);
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            if (databaseLock != null) {
                databaseLock.unlock();
            }
            if (null != con) {
                if (rollback > 0) {
                    if (rollback == 1) {
                        Databases.rollback(con);
                    }
                    Databases.autocommit(con);
                }
                try {
                    cache.pushWriteConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    /**
     * Removes specified entry from <code>"filestore2user"</code> table.
     *
     * @param contextId The context identifier
     * @param userId The user identifier
     * @param cache The admin cache to use
     * @throws StorageException If operation fails
     */
    public static void removeFilestore2UserEntry(int contextId, int userId, AdminCache cache) throws StorageException {
        checkBeforeAccess(cache);

        Connection con = null;
        int rollback = 0;
        DatabaseLock databaseLock = null;
        try {
            // Acquire lock
            databaseLock = awaitWriteLock(cache);

            // Start database transaction
            con = cache.getWriteConnectionForConfigDB();
            Databases.startTransaction(con);
            rollback = 1;

            // Insert entry
            deleteEntry(new FilestoreEntry(contextId, userId, 0), con);

            // Commit
            con.commit();
            rollback = 2;
        } catch (PoolException | SQLException e) {
            throw new StorageException(e);
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            if (databaseLock != null) {
                databaseLock.unlock();
            }
            if (null != con) {
                if (rollback > 0) {
                    if (rollback == 1) {
                        Databases.rollback(con);
                    }
                    Databases.autocommit(con);
                }
                try {
                    cache.pushWriteConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    /**
     * Removes context-associated entries from <code>"filestore2user"</code> table.
     *
     * @param contextId The context identifier
     * @param cache The admin cache to use
     * @throws StorageException If operation fails
     */
    public static void removeFilestore2UserEntries(int contextId, AdminCache cache) throws StorageException {
        checkBeforeAccess(cache);

        Connection con = null;
        int rollback = 0;
        DatabaseLock databaseLock = null;
        try {
            // Acquire lock
            databaseLock = awaitWriteLock(cache);

            // Start database transaction
            con = cache.getWriteConnectionForConfigDB();
            Databases.startTransaction(con);
            rollback = 1;

            // Insert entry
            deleteEntries(contextId, con);

            // Commit
            con.commit();
            rollback = 2;
        } catch (PoolException | SQLException e) {
            throw new StorageException(e);
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            if (databaseLock != null) {
                databaseLock.unlock();
            }
            if (null != con) {
                if (rollback > 0) {
                    if (rollback == 1) {
                        Databases.rollback(con);
                    }
                    Databases.autocommit(con);
                }
                try {
                    cache.pushWriteConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    /**
     * Replaces specified entry in <code>"filestore2user"</code> table.
     *
     * @param contextId The context identifier
     * @param userId The user identifier
     * @param filestoreId The new file store identifier
     * @param cache The admin cache to use
     * @throws StorageException If operation fails
     */
    public static void replaceFilestore2UserEntry(int contextId, int userId, int filestoreId, AdminCache cache) throws StorageException {
        checkBeforeAccess(cache);

        Connection con = null;
        int rollback = 0;
        DatabaseLock databaseLock = null;
        try {
            // Acquire lock
            databaseLock = awaitWriteLock(cache);

            // Start database transaction
            con = cache.getWriteConnectionForConfigDB();
            Databases.startTransaction(con);
            rollback = 1;

            // Insert entry
            replaceEntry(new FilestoreEntry(contextId, userId, filestoreId), con);

            // Commit
            con.commit();
            rollback = 2;
        } catch (PoolException | SQLException e) {
            throw new StorageException(e);
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            if (databaseLock != null) {
                databaseLock.unlock();
            }
            if (null != con) {
                if (rollback > 0) {
                    if (rollback == 1) {
                        Databases.rollback(con);
                    }
                    Databases.autocommit(con);
                }
                try {
                    cache.pushWriteConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    /**
     * Copies specified entry in <code>"filestore2user"</code> table.
     *
     * @param contextId The context identifier
     * @param userId The user identifier
     * @param destContext The new context identifier
     * @param destUserId The new user identifier
     * @param adminCache The admin cache to use
     * @throws StorageException If operation fails
     */
    public static void copyFilestore2UserEntry(int contextId, int userId, int destContext, int destUserId, AdminCache adminCache) throws StorageException {
        checkBeforeAccess(adminCache);

        Connection con = null;
        int rollback = 0;
        DatabaseLock databaseLock = null;
        try {
            // Acquire lock
            databaseLock = awaitWriteLock(adminCache);

            // Start database transaction
            con = adminCache.getWriteConnectionForConfigDB();
            Databases.startTransaction(con);
            rollback = 1;

            int filestoreId = selectEntry(new FilestoreEntry(contextId, userId, 0), con);

            // Insert entry
            if (filestoreId > 0) {
                replaceEntry(new FilestoreEntry(destContext, destUserId, filestoreId), con);
            }

            // Commit
            con.commit();
            rollback = 2;
        } catch (PoolException | SQLException e) {
            throw new StorageException(e);
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            if (databaseLock != null) {
                databaseLock.unlock();
            }
            if (null != con) {
                if (rollback > 0) {
                    if (rollback == 1) {
                        Databases.rollback(con);
                    }
                    Databases.autocommit(con);
                }
                try {
                    adminCache.pushWriteConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    /**
     * Yields the <code>CREATE TABLE</code> statement for specified table name and optional <code>SELECT</code>
     * statement to pre-fill it from current rows contained in <code>"filestore2user"</code> table.
     *
     * @param tableName The name of the table
     * @param withSelect <code>true</code> to append the appropriate <code>SELECT</code> statement; otherwise <code>false</code>
     * @return The <code>CREATE TABLE</code> statement
     */
    private static String getCreateTableStatementForUsersPerFilestore(String tableName, boolean withSelect) {
        StringBuilder sb = new StringBuilder(withSelect ? 256 : 128);
        sb.append("CREATE TABLE `").append(tableName).append("` (");
        sb.append(" `filestore_id` int(10) unsigned NOT NULL,");
        sb.append(" `count` int(10) unsigned NOT NULL,");
        sb.append(" PRIMARY KEY (`filestore_id`)");
        sb.append(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci");
        if (withSelect) {
            sb.append(" SELECT `filestore_id`, COUNT(*) AS `count` FROM `").append(TABLE_NAME_FILESTORE2USER).append("` GROUP BY `filestore_id`");
        }
        return sb.toString();
    }

    private static final AtomicReference<ScheduledTimerTask> REINIT_TIMER_TASK_REF = new AtomicReference<>();

    /**
     * Drops the task to re-initialize the <code>"users_per_filestore"</code> table.
     *
     * @param timerService The timer service to use
     */
    public static void dropReinitTimerTask(TimerService timerService) {
        ScheduledTimerTask task = REINIT_TIMER_TASK_REF.getAndSet(null);
        if (task != null) {
            task.cancel();
            if (timerService != null) {
                timerService.purge();
            }
        }
    }

    private static final long MILLIS_2_HOURS = Duration.ofHours(2).toMillis();

    /**
     * Schedules the task to re-initialize the <code>"users_per_filestore"</code> table.
     *
     * @param timerService The timer service to use
     */
    public static void scheduleReinitTimerTask(TimerService timerService) {
        if (REINIT_TIMER_TASK_REF.get() == null) {
            synchronized (Filestore2UserUtil.class) {
                if (REINIT_TIMER_TASK_REF.get() == null) {
                    ExceptionCatchingRunnable task = () -> reinitUsersPerFilestore(AdminDaemon.getCache());
                    REINIT_TIMER_TASK_REF.set(timerService.scheduleWithFixedDelay(task, MILLIS_2_HOURS, MILLIS_2_HOURS));
                }
            }
        }
    }

    /**
     * Re-initializes the <code>"users_per_filestore"</code> table.
     *
     * @param adminCache The admin cache to use
     * @throws StorageException If operation fails
     */
    private static void reinitUsersPerFilestore(AdminCache adminCache) throws StorageException {
        checkBeforeAccess(adminCache);

        Connection con = null;
        DatabaseLock databaseLock = null;
        try {
            // Acquire lock
            databaseLock = awaitWriteLock(adminCache);

            // Acquire lock
            con = adminCache.getWriteConnectionForConfigDB();
            boolean dropConsistencyLock = false;
            if (consistencyLock(MILLIS_2_HOURS, con)) {
                dropConsistencyLock = true;
                try {
                    long st = System.nanoTime(); // NOSONARLINT

                    // Create new temporary table with most current counts
                    String tableNameNew = TABLE_NAME_USERS_PER_FILESTORE + "_new";
                    {
                        try {
                            boolean created = Databases.createTable(getCreateTableStatementForUsersPerFilestore(tableNameNew, true), true, con);
                            if (!created) {
                                // Another process already created the table
                                LOG.debug("Failed to create and pre-fill table \"{}\" since already existent. Aborting reinitialization of \"{}\" table.", tableNameNew, TABLE_NAME_USERS_PER_FILESTORE);
                                return;
                            }
                        } catch (SQLException e) {
                            // Failed to create table
                            LOG.warn("Failed to create and pre-fill table \"{}\". Aborting reinitialization of \"{}\" table.", tableNameNew, TABLE_NAME_USERS_PER_FILESTORE, e);
                            return;
                        }
                    }

                    // Swap tables
                    String tableNameOld = TABLE_NAME_USERS_PER_FILESTORE + "_old";
                    {
                        PreparedStatement stmt = null;
                        try {
                            stmt = con.prepareStatement("RENAME TABLE `" + TABLE_NAME_USERS_PER_FILESTORE + "` TO `" + tableNameOld + "`, `" + tableNameNew + "` TO `" + TABLE_NAME_USERS_PER_FILESTORE + "`");
                            stmt.executeUpdate();
                        } finally {
                            Databases.closeSQLStuff(stmt);
                        }
                    }

                    // Delete old table
                    {
                        PreparedStatement stmt = null;
                        try {
                            stmt = con.prepareStatement("DROP TABLE `" + tableNameOld + "`");
                            stmt.executeUpdate();
                        } finally {
                            Databases.closeSQLStuff(stmt);
                        }
                    }

                    // Let lock expire
                    dropConsistencyLock = false;

                    // Log information
                    long dur = System.nanoTime() - st;
                    LOG.info("Updated/Checked consistency of table \"{}\" in {}msec.", TABLE_NAME_USERS_PER_FILESTORE, L(Duration.ofNanos(dur).toMillis()));
                } finally {
                    if (dropConsistencyLock) {
                        dropConsistencyLock(con);
                    }
                }
            } else {
                // Failed to acquire lock
                LOG.info("Failed to acquire lock to update/check consistency of table \"{}\". Another process is currently running or performed it recently.", TABLE_NAME_USERS_PER_FILESTORE);
            }
        } catch (PoolException | SQLException e) {
            throw new StorageException(e);
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            if (databaseLock != null) {
                databaseLock.unlock();
            }
            if (null != con) {
                try {
                    adminCache.pushWriteConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    /**
     * Initializes the <code>"users_per_filestore"</code> table.
     *
     * @param databaseService The database service to use
     * @throws StorageException If initialization fails
     */
    public static void initUsersPerFilestore(DatabaseService databaseService) throws StorageException {
        initUsersPerFilestore(new DatabaseServiceConnectionProvider(databaseService));
    }

    /**
     * Initializes the <code>"users_per_filestore"</code> table.
     *
     * @param adminCache The admin cache to use
     * @throws StorageException If initialization fails
     */
    public static void initUsersPerFilestore(AdminCache adminCache) throws StorageException {
        initUsersPerFilestore(new AdminCacheConnectionProvider(adminCache));
    }

    /**
     * Initializes the <code>"users_per_filestore"</code> table.
     *
     * @param connectionProvider The connection provider to use
     * @throws StorageException If initialization fails
     */
    private static void initUsersPerFilestore(ConnectionProvider connectionProvider) throws StorageException {
        Connection con = null;
        int rollback = 0;
        boolean unmarkOnError = false;
        try {
            // Acquire connection
            con = connectionProvider.getConnection();

            // Try to insert marker
            boolean marked = mark(Reason.FILL_USERS_PER_FILESTORE, con);
            if (marked) {
                unmarkOnError = true;

                // Check server id availability
                if (!connectionProvider.checkServerId()) {
                    // Assume initial start and thus mark as processed as there are no entries to insert
                    if (tableDoesNotExist(con, TABLE_NAME_USERS_PER_FILESTORE)) {
                        try {
                            Databases.createTable(getCreateTableStatementForUsersPerFilestore(TABLE_NAME_USERS_PER_FILESTORE, false), true, con);
                        } catch (SQLException e) {
                            throw DBPoolingExceptionCodes.SQL_ERROR.create(e, e.getMessage());
                        }
                    }

                    // Mark as processed
                    processed(Reason.FILL_USERS_PER_FILESTORE, con);
                    unmarkOnError = false;
                    return;
                }

                if (tableDoesNotExist(con, TABLE_NAME_USERS_PER_FILESTORE)) {
                    try {
                        Databases.createTable(getCreateTableStatementForUsersPerFilestore(TABLE_NAME_USERS_PER_FILESTORE, true), false, con);
                    } catch (SQLException e) {
                        throw DBPoolingExceptionCodes.SQL_ERROR.create(e, e.getMessage());
                    }

                    // Mark as processed
                    processed(Reason.FILL_USERS_PER_FILESTORE, con);
                } else {
                    // Start database transaction
                    Databases.startTransaction(con);
                    rollback = 1;

                    // Get current counts and fill table
                    FilestoreCountCollection userCounts = getUserCounts(con);
                    if (!userCounts.isEmpty()) {
                        PreparedStatement stmt = null;
                        try {
                            for (List<FilestoreCount> partition : Lists.partition(userCounts.toList(), 1000)) {
                                stmt = con.prepareStatement("INSERT IGNORE INTO `" + TABLE_NAME_USERS_PER_FILESTORE + "` (`filestore_id`, `count`) VALUES (?, ?)");
                                for (FilestoreCount filestoreCount : partition) {
                                    stmt.setInt(1, filestoreCount.getFilestoreId());
                                    stmt.setInt(2, filestoreCount.getCount());
                                    stmt.addBatch();
                                }
                                stmt.executeBatch();
                                Databases.closeSQLStuff(stmt);
                                stmt = null;
                            }
                        } finally {
                            Databases.closeSQLStuff(stmt);
                        }
                    }

                    // Mark as processed
                    processed(Reason.FILL_USERS_PER_FILESTORE, con);

                    // Commit
                    con.commit();
                    rollback = 2;
                }

                // Success
                unmarkOnError = false;
            } else {
                // Apparently we did nothing...
                connectionProvider.releaseConnection(con, true);
                con = null;
            }
        } catch (OXException e) {
            throw StorageException.wrapForRMI(e);
        } catch (SQLException e) {
            throw new StorageException(e);
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            if (null != con) {
                if (rollback > 0) {
                    // Check whether to roll-back transactional changes
                    if (rollback == 1) {
                        Databases.rollback(con);
                    }

                    // Switch back to auto-commit
                    Databases.autocommit(con);
                }

                // Check whether to unmark
                unmarkOnError(unmarkOnError, Reason.FILL_USERS_PER_FILESTORE, con);

                // Finally, push back used connection
                connectionProvider.releaseConnection(con, false);
            }
        }
    }

    /**
     * Initializes the <code>"filestore2user"</code> table.
     *
     * @param databaseService The database service to use
     * @throws StorageException If initialization fails
     */
    public static void initFilestore2User(DatabaseService databaseService) throws StorageException {
        Connection con = null;
        int rollback = 0;
        boolean unmarkOnError = false;
        try {
            con = databaseService.getForUpdateTask();
            if (tableDoesNotExist(con, TABLE_NAME_FILESTORE2USER)) {
                throw new StorageException("Table \"" + TABLE_NAME_FILESTORE2USER + "\" does not exist.");
            }

            // Try to insert marker
            boolean marked = mark(Reason.FILL_FILESTORE2USER, con);
            if (marked) {
                unmarkOnError = true;

                // Check server id availability
                try {
                    databaseService.getServerId();
                } catch (OXException e) {
                    if (NOT_RESOLVED_SERVER.equals(e)) {
                        // Assume initial start and thus mark as processed as there are no entries to insert
                        processed(Reason.FILL_FILESTORE2USER, con);
                        unmarkOnError = false;
                        return;
                    }

                    // Re-throw...
                    throw e;
                }

                // Start database transaction
                Databases.startTransaction(con);
                rollback = 1;

                // Determine all pools/schemas
                List<SchemaInfo> schemas = getAllNonEmptySchemas(con);

                // Determine all users having an individual file store set
                Set<FilestoreEntry> allEntries = determineAllEntries(schemas, databaseService);

                // Insert entries
                insertEntries(allEntries, con);

                // Mark as processed
                processed(Reason.FILL_FILESTORE2USER, con);

                // Commit
                con.commit();
                rollback = 2;
                unmarkOnError = false;
            } else {
                // Apparently we did nothing...
                databaseService.backForUpdateTaskAfterReading(con);
                con = null;
            }
        } catch (OXException e) {
            throw StorageException.wrapForRMI(e);
        } catch (SQLException e) {
            throw new StorageException(e);
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            if (null != con) {
                if (rollback > 0) {
                    // Check whether to roll-back transactional changes
                    if (rollback == 1) {
                        Databases.rollback(con);
                    }

                    // Switch back to auto-commit
                    Databases.autocommit(con);
                }

                // Check whether to unmark
                unmarkOnError(unmarkOnError, Reason.FILL_FILESTORE2USER, con);

                // Finally, push back used connection
                databaseService.backForUpdateTask(con);
            }
        }
    }

    private static List<SchemaInfo> getAllNonEmptySchemas(Connection con) throws StorageException, SQLException {
        // Determine all DB schemas that are currently in use
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // Grab database schemas
            stmt = con.prepareStatement("SELECT DISTINCT `db_pool_id`, `schemaname` FROM `contexts_per_dbschema` WHERE `count` > 0");
            rs = stmt.executeQuery();
            if (!rs.next()) {
                // No database schema in use
                return Collections.emptyList();
            }

            List<SchemaInfo> l = new LinkedList<>();
            do {
                l.add(SchemaInfo.valueOf(rs.getInt(1), rs.getString(2)));
            } while (rs.next());
            return l;
        } catch (RuntimeException e) {
            throw StorageException.storageExceptionFor(e);
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    private static Set<FilestoreEntry> determineAllEntries(List<SchemaInfo> schemas, final DatabaseService databaseService) throws StorageException {
        // Setup completion service
        CompletionService<Set<FilestoreEntry>> completionService = new BoundedCompletionService<Set<FilestoreEntry>>(AdminServiceRegistry.getInstance().getService(ThreadPoolService.class), 10);
        int taskCount = 0;

        // Determine entries for each pool/schema
        for (final SchemaInfo schema : schemas) {
            completionService.submit(new Callable<Set<FilestoreEntry>>() {

                @Override
                public Set<FilestoreEntry> call() throws StorageException {
                    int maxRunCount = 5;
                    int runCount = 0;
                    while (runCount < maxRunCount) {
                        Connection con = null;
                        PreparedStatement stmt = null;
                        ResultSet result = null;
                        try {
                            con = databaseService.getNoTimeout(schema.getPoolId(), schema.getSchema());

                            if (!columnExists(con, "user", "filestore_id")) {
                                // This schema cannot hold users having an individual file storage assigned
                                return Collections.emptySet();
                            }

                            stmt = con.prepareStatement("SELECT `cid`, `id`, `filestore_id` FROM `user` WHERE `filestore_id` > 0 AND (`filestore_owner` = 0 OR `filestore_owner` = `id`)");
                            result = stmt.executeQuery();

                            if (false == result.next()) {
                                return Collections.emptySet();
                            }

                            Set<FilestoreEntry> entries = new LinkedHashSet<FilestoreEntry>();
                            do {
                                entries.add(new FilestoreEntry(result.getInt(1), result.getInt(2), result.getInt(3)));
                            } while (result.next());
                            return entries;
                        } catch (OXException e) {
                            boolean doThrow = true;

                            if (DBPoolingExceptionCodes.NO_CONNECTION.equals(e)) {
                                java.net.ConnectException connectException = DBUtils.extractException(java.net.ConnectException.class, e);
                                if (null != connectException) {
                                    // Database not reachable. Connection was refused remotely.
                                    doThrow = true;
                                } else {
                                    SQLException sqle = DBUtils.extractSqlException(e);
                                    if (sqle instanceof SQLNonTransientConnectionException) {
                                        SQLNonTransientConnectionException connectionException = (SQLNonTransientConnectionException) sqle;
                                        if (isTooManyConnections(connectionException) && (++runCount < maxRunCount)) {
                                            waitWithExponentialBackoff(runCount, 1000L);
                                            doThrow = false;
                                        }
                                    }
                                }
                            } else if (DBPoolingExceptionCodes.SCHEMA_FAILED.equals(e)) {
                                // Such a schema does not exist
                                Throwable t = e.getCause();
                                if (null == t) {
                                    t = e;
                                }
                                LOG.warn("Unknown schema \"{}\" on database host {}. Please check database accessibility and/or context-to-schema associations.", schema.getSchema(), Integer.valueOf(schema.getPoolId()), t);
                                return Collections.emptySet();
                            }

                            if (doThrow) {
                                LOG.error("Failed to determine user-associated file storages for schema \"{}\" in database {}", schema.getSchema(), I(schema.getPoolId()), e);
                                throw StorageException.wrapForRMI(e);
                            }
                        } catch (SQLException e) {
                            LOG.error("Failed to determine user-associated file storages for schema \"{}\" in database {}", schema.getSchema(), I(schema.getPoolId()), e);
                            throw new StorageException(e);
                        } finally {
                            Databases.closeSQLStuff(result, stmt);
                            if (null != con) {
                                databaseService.backNoTimeoout(schema.getPoolId(), con);
                            }
                        }
                    }

                    // Should not be reached
                    throw new StorageException("Failed to determine user-associated file storages for schema \"" + schema.getSchema() + "\" in database " + schema.getPoolId());
                }

                private boolean isTooManyConnections(SQLNonTransientConnectionException connectionException) {
                    String message = connectionException.getMessage();
                    return null != message && Strings.asciiLowerCase(message).indexOf("too many connections") >= 0;
                }

                private void waitWithExponentialBackoff(int retryCount, long baseMillis) {
                    long nanosToWait = TimeUnit.NANOSECONDS.convert((retryCount * baseMillis) + ((long) (Math.random() * baseMillis)), TimeUnit.MILLISECONDS); // NOSONARLINT
                    LockSupport.parkNanos(nanosToWait);
                }
            });
            taskCount++;
        }

        // Await completion
        Set<FilestoreEntry> allEntries;
        {
            List<Set<FilestoreEntry>> entries = ThreadPools.<Set<FilestoreEntry>, StorageException> takeCompletionService(completionService, taskCount, EXCEPTION_FACTORY);
            allEntries = new LinkedHashSet<FilestoreEntry>(entries.size());
            for (Set<FilestoreEntry> set : entries) {
                allEntries.addAll(set);
            }
        }
        return allEntries;
    }

    private static void insertEntries(Set<FilestoreEntry> allEntries, Connection con) throws SQLException {
        for (Set<FilestoreEntry> entries : Sets.partition(allEntries, 50)) {
            doInsertEntries(entries, con);
        }
    }

    private static void doInsertEntries(Set<FilestoreEntry> entries, Connection con) throws SQLException {
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("INSERT IGNORE INTO `" + TABLE_NAME_FILESTORE2USER + "` (`cid`, `user`, `filestore_id`) VALUES (?, ?, ?)");
            for (FilestoreEntry filestoreEntry : entries) {
                stmt.setInt(1, filestoreEntry.cid);
                stmt.setInt(2, filestoreEntry.user);
                stmt.setInt(3, filestoreEntry.filestoreId);
                stmt.addBatch();
            }
            stmt.executeBatch();
        } finally {
            Databases.closeSQLStuff(stmt);
        }
    }

    private static boolean insertEntry(FilestoreEntry filestoreEntry, Connection con) throws SQLException {
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("INSERT INTO `" + TABLE_NAME_FILESTORE2USER + "` (`cid`, `user`, `filestore_id`) VALUES (?, ?, ?)");
            stmt.setInt(1, filestoreEntry.cid);
            stmt.setInt(2, filestoreEntry.user);
            stmt.setInt(3, filestoreEntry.filestoreId);
            try {
                // Try to INSERT
                stmt.executeUpdate();
            } catch (SQLException e) {
                if (Databases.isPrimaryKeyConflictInMySQL(e)) {
                    return false;
                }
                throw e;
            }

            // INSERT succeeded: Close & drop statement and increment count
            Databases.closeSQLStuff(stmt);
            stmt = null;
            stmt = con.prepareStatement("INSERT INTO `" + TABLE_NAME_USERS_PER_FILESTORE + "` (`filestore_id`, `count`) VALUES (?, 1) ON DUPLICATE KEY UPDATE `count` = `count` + 1");
            stmt.setInt(1, filestoreEntry.filestoreId);
            stmt.executeUpdate();
            return true;
        } finally {
            Databases.closeSQLStuff(stmt);
        }
    }

    private static boolean replaceEntry(FilestoreEntry filestoreEntry, Connection con) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT `filestore_id` FROM `" + TABLE_NAME_FILESTORE2USER + "` WHERE `cid` = ? AND `user` = ?");
            stmt.setInt(1, filestoreEntry.cid);
            stmt.setInt(2, filestoreEntry.user);
            rs = stmt.executeQuery();
            int prevFilestoreId = rs.next() ? rs.getInt(1) : 0;
            Databases.closeSQLStuff(rs, stmt);
            rs = null;
            stmt = null;

            if (prevFilestoreId <= 0) {
                // No filestore associated with that user
                return insertEntry(filestoreEntry, con);
            }

            if (prevFilestoreId == filestoreEntry.filestoreId) {
                // Existent filestore identifier is equal to new one. Do nothing...
                return false;
            }

            stmt = con.prepareStatement("UPDATE `" + TABLE_NAME_FILESTORE2USER + "` SET `filestore_id` = ? WHERE `cid` = ? AND `user` = ? AND `filestore_id` = ?");
            stmt.setInt(1, filestoreEntry.filestoreId);
            stmt.setInt(2, filestoreEntry.cid);
            stmt.setInt(3, filestoreEntry.user);
            stmt.setInt(4, prevFilestoreId);
            int rows = stmt.executeUpdate();
            if (rows <= 0) {
                // Nothing done
                return false;
            }
            Databases.closeSQLStuff(stmt);
            stmt = null;

            stmt = con.prepareStatement("INSERT INTO `" + TABLE_NAME_USERS_PER_FILESTORE + "` (`filestore_id`, `count`) VALUES (?, 1) ON DUPLICATE KEY UPDATE `count` = `count` + 1");
            stmt.setInt(1, filestoreEntry.filestoreId);
            stmt.executeUpdate();
            return true;
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    private static int selectEntry(FilestoreEntry filestoreEntry, Connection con) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT `filestore_id` FROM `" + TABLE_NAME_FILESTORE2USER + "` WHERE `cid`=? AND `user`=?");
            stmt.setInt(1, filestoreEntry.cid);
            stmt.setInt(2, filestoreEntry.user);
            rs = stmt.executeQuery();
            return rs.next() ? rs.getInt(1) : 0;
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    private static void deleteEntry(FilestoreEntry filestoreEntry, Connection con) throws SQLException {
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("DELETE FROM `" + TABLE_NAME_FILESTORE2USER + "` WHERE `cid`=? AND `user`=?");
            stmt.setInt(1, filestoreEntry.cid);
            stmt.setInt(2, filestoreEntry.user);
            stmt.executeUpdate();
            Databases.closeSQLStuff(stmt);
            stmt = null;

            stmt = con.prepareStatement("UPDATE `" + TABLE_NAME_USERS_PER_FILESTORE + "` SET `count` = `count` - 1 WHERE `filestore_id` = ? AND `count` > 0");
            stmt.setInt(1, filestoreEntry.filestoreId);
            stmt.executeUpdate();
        } finally {
            Databases.closeSQLStuff(stmt);
        }
    }

    private static void deleteEntries(int contextId, Connection con) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT `filestore_id`, `user` FROM `" + TABLE_NAME_FILESTORE2USER + "` WHERE `cid`=?");
            stmt.setInt(1, contextId);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                // There is none for given context
                return;
            }
            List<FilestoreEntry> entries = new ArrayList<>();
            do {
                entries.add(new FilestoreEntry(contextId, rs.getInt(2), rs.getInt(1)));
            } while (rs.next());
            Databases.closeSQLStuff(rs, stmt);
            rs = null;
            stmt = null;

            stmt = con.prepareStatement("DELETE FROM `" + TABLE_NAME_FILESTORE2USER + "` WHERE `cid`=?");
            stmt.setInt(1, contextId);
            stmt.executeUpdate();
            Databases.closeSQLStuff(stmt);
            stmt = null;

            stmt = con.prepareStatement("UPDATE `" + TABLE_NAME_USERS_PER_FILESTORE + "` SET `count` = `count` - 1 WHERE `filestore_id` = ? AND `count` > 0");
            for (FilestoreEntry filestoreEntry : entries) {
                stmt.setInt(1, filestoreEntry.filestoreId);
                stmt.addBatch();
            }
            stmt.executeBatch();
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    private static boolean mark(Reason reason, Connection con) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT 1 FROM `" + TABLE_NAME_REASON_TEXT + "` WHERE `id`=?");
            stmt.setInt(1, reason.getIdentifier());
            rs = stmt.executeQuery();
            if (rs.next()) {
                return false;
            }

            Databases.closeSQLStuff(rs, stmt);
            rs = null;

            stmt = con.prepareStatement("INSERT INTO `" + TABLE_NAME_REASON_TEXT + "` (`id`, `text`) VALUES (?, ?)");
            stmt.setInt(1, reason.getIdentifier());
            stmt.setString(2, TEXT_PROCESSING);
            try {
                stmt.executeUpdate();
                return true;
            } catch (SQLException e) {
                if (Databases.isPrimaryKeyConflictInMySQL(e)) {
                    // Another machine is currently processing or the update has already been applied (text = TERMINATED)
                    return false;
                }
                throw e;
            }
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    private static void processed(Reason reason, Connection con) throws SQLException {
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("UPDATE `" + TABLE_NAME_REASON_TEXT + "` SET `text`=? WHERE `id`=?");
            stmt.setString(1, TEXT_TERMINATED);
            stmt.setInt(2, reason.getIdentifier());
            stmt.executeUpdate();
        } finally {
            Databases.closeSQLStuff(stmt);
        }
    }

    private static void unmarkOnError(boolean error, Reason reason, Connection con) {
        if (error) {
            PreparedStatement stmt = null;
            try {
                stmt = con.prepareStatement("DELETE FROM `" + TABLE_NAME_REASON_TEXT + "` WHERE `id`=?");
                stmt.setInt(1, reason.getIdentifier());
                stmt.executeUpdate();
            } catch (Exception e) {
                LOG.error("Failed to unmark " + TABLE_NAME_FILESTORE2USER + " table!", e);
            } finally {
                Databases.closeSQLStuff(stmt);
            }
        }
    }

    private static boolean isNotTerminated(Connection con) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT `text` FROM `" + TABLE_NAME_REASON_TEXT + "` WHERE `id`=?");
            stmt.setInt(1, Reason.FILL_FILESTORE2USER.getIdentifier());
            rs = stmt.executeQuery();
            if ((false == rs.next()) || (false == TEXT_TERMINATED.equals(rs.getString(1)))) {
                return true;
            }
            Databases.closeSQLStuff(rs, stmt);
            rs = null;
            stmt = null;

            stmt = con.prepareStatement("SELECT `text` FROM `" + TABLE_NAME_REASON_TEXT + "` WHERE `id`=?");
            stmt.setInt(1, Reason.FILL_USERS_PER_FILESTORE.getIdentifier());
            rs = stmt.executeQuery();
            return !rs.next() || !TEXT_TERMINATED.equals(rs.getString(1));
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    /** The base milliseconds (1 second) to use for exponential back-off strategy */
    private static final long MILLISECONDS_BASE = 1000L;

    /**
     * Await acquisition of write lock with exponential back-off strategy.
     *
     * @param adminCache The admin cache to use for borrowing another connection from pool
     * @return The acquired database lock
     * @throws SQLException If an SQL error occurs
     * @throws PoolException If fetching a connection from pool fails
     * @throws StorageException If lock could not be obtained
     */
    private static DatabaseLock awaitWriteLock(AdminCache adminCache) throws SQLException, PoolException, StorageException {
        int maxRetries = 5;
        long st = System.nanoTime();
        for (int retry = 1; retry <= maxRetries; retry++) {
            DatabaseLock lock = writeLock(adminCache);
            if (lock != null) {
                long duration = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - st);
                if (duration > 0) {
                    LOG.info("Acquired write lock for \"{}\" table after {}msec", TABLE_NAME_FILESTORE2USER, L(duration));
                }
                return lock;
            }

            // Exponential back-off wait
            if (retry < maxRetries) {
                long millisToWait = (retry * MILLISECONDS_BASE) + ((long) (Math.random() * MILLISECONDS_BASE)); // NOSONARLINT
                LOG.info("Failed to acquire write lock for \"{}\" table since another process holds the lock currently. Retrying in {}msec...", TABLE_NAME_FILESTORE2USER, L(millisToWait));
                LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(millisToWait));
            }
        }

        throw new StorageException("Failed to acquire write lock for \"" + TABLE_NAME_FILESTORE2USER + "\" table since another process holds the lock currently. Please try again later.");
    }

    /**
     * Acquires the write lock.
     *
     * @param adminCache The admin cache to use for borrowing another connection from pool
     * @return The acquired lock or <code>null</code>
     * @throws SQLException If an SQL error occurs
     * @throws PoolException If fetching a connection from pool fails
     */
    private static DatabaseLock writeLock(AdminCache adminCache) throws SQLException, PoolException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = adminCache.getWriteConnectionForConfigDB();
            stmt = con.prepareStatement("SELECT `text` FROM `" + TABLE_NAME_REASON_TEXT + "` WHERE `id`=" + Reason.USERS_PER_FILESTORE_WRITE_LOCK.getIdentifier());
            rs = stmt.executeQuery();
            OptionalLong stamp = rs.next() ? OptionalLong.of(Long.parseLong(rs.getString(1))) : OptionalLong.empty();
            closeSQLStuff(rs, stmt);
            rs = null;
            stmt = null;

            long now = System.currentTimeMillis();
            int rows;
            if (stamp.isEmpty()) {
                // No lock entry, yet
                stmt = con.prepareStatement("INSERT INTO `" + TABLE_NAME_REASON_TEXT + "` (`id`, `text`) VALUES (" + Reason.USERS_PER_FILESTORE_WRITE_LOCK.getIdentifier() + ", ?) ON DUPLICATE KEY UPDATE `id` = `id`");
                stmt.setString(1, Long.toString(now));
                rows = stmt.executeUpdate();
                closeSQLStuff(stmt);
                stmt = null;
            } else if (now - stamp.getAsLong() > 20000L) {
                // Lock entry expired
                stmt = con.prepareStatement("UPDATE `" + TABLE_NAME_REASON_TEXT + "` SET `text` = ? WHERE `id` = " + Reason.USERS_PER_FILESTORE_WRITE_LOCK.getIdentifier() + " AND `text` = ?");
                stmt.setString(1, Long.toString(now));
                stmt.setString(2, Long.toString(stamp.getAsLong()));
                rows = stmt.executeUpdate();
                closeSQLStuff(stmt);
                stmt = null;
            } else {
                // Still locked...
                rows = 0;
            }

            if (rows <= 0) {
                return null;
            }

            ScheduledTimerTask timerTask = null;
            boolean error = true;
            try {
                TimerService timerService = AdminServiceRegistry.getInstance().getService(TimerService.class);
                if (timerService != null) {
                    Runnable task = () -> updateWriteLock(adminCache);
                    long refreshIntervalMillis = 3000L;
                    timerTask = timerService.scheduleWithFixedDelay(task, refreshIntervalMillis, refreshIntervalMillis, TimeUnit.MILLISECONDS);
                }

                DatabaseLock lock = new DatabaseLock(timerTask, adminCache);
                error = false;
                return lock;
            } finally {
                if (error) {
                    deleteWriteLock(con);
                    if (timerTask != null) { // NOSONARLINT
                        timerTask.cancel();
                    }
                }
            }
        } finally {
            closeSQLStuff(rs, stmt);
            if (con != null) {
                try {
                    adminCache.pushWriteConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    private static void deleteWriteLock(Connection con) {
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("DELETE FROM `" + TABLE_NAME_REASON_TEXT + "` WHERE `id`=" + Reason.USERS_PER_FILESTORE_WRITE_LOCK.getIdentifier());
            stmt.executeUpdate();
            LOG.info("Released write lock for \"{}\" table", TABLE_NAME_FILESTORE2USER);
        } catch (Exception e) {
            LOG.warn("Failed to release write lock for \"{}\" table", TABLE_NAME_FILESTORE2USER, e);
        } finally {
            closeSQLStuff(stmt);
        }
    }

    private static void updateWriteLock(AdminCache adminCache) {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = adminCache.getWriteConnectionForConfigDB();
            stmt = con.prepareStatement("UPDATE `" + TABLE_NAME_REASON_TEXT + "` SET `text` = ? WHERE `id`=" + Reason.USERS_PER_FILESTORE_WRITE_LOCK.getIdentifier());
            stmt.setString(1, Long.toString(System.currentTimeMillis()));
            stmt.executeUpdate();
        } catch (Exception e) {
            LOG.warn("Failed to uodate write lock for \"{}\" table", TABLE_NAME_FILESTORE2USER, e);
        } finally {
            closeSQLStuff(stmt);
            if (con != null) {
                try {
                    adminCache.pushWriteConnectionForConfigDB(con);
                } catch (PoolException e) {
                    logPoolingError(e);
                }
            }
        }
    }

    private static class DatabaseLock {

        private final ScheduledTimerTask timerTask;
        private final AdminCache cache;

        /**
         * Initializes a new instance of {@link DatabaseLock}.
         *
         * @param timerTask The timer task updating the lock entry
         * @param cache The cache for acquiring a connection from pool
         */
        DatabaseLock(ScheduledTimerTask timerTask, AdminCache cache) {
            super();
            this.timerTask = timerTask;
            this.cache = cache;
        }

        /**
         * Unlocks this lock instance.
         */
        void unlock() {
            ScheduledTimerTasks.cancelSafely(timerTask);
            ScheduledTimerTasks.purgeSafely(AdminServiceRegistry.getInstance().getService(TimerService.class));
            deleteWriteLock();
        }

        private void deleteWriteLock() {
            Connection con = null;
            try {
                con = cache.getWriteConnectionForConfigDB();
                Filestore2UserUtil.deleteWriteLock(con);
            } catch (Exception e) {
                LOG.warn("Failed to delete write lock for \"{}\" table", TABLE_NAME_FILESTORE2USER, e);
            } finally {
                if (con != null) {
                    try {
                        cache.pushWriteConnectionForConfigDB(con);
                    } catch (PoolException e) {
                        logPoolingError(e);
                    }
                }
            }
        }
    }

    /**
     * Tries to acquire the consistency lock for re-initializing the <code>"users_per_filestore"</code> table.
     *
     * @param timeToLive The time-to-live in milliseconds for an existent lock
     * @param con The connection to use
     * @return <code>true</code> if the lock has been acquired; otherwise <code>false</code> if another process holds the lock or performed re-initialization recently
     * @throws SQLException If an SQL error occurs
     */
    private static boolean consistencyLock(long timeToLive, Connection con) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT `text` FROM `" + TABLE_NAME_REASON_TEXT + "` WHERE `id`=?");
            stmt.setInt(1, Reason.USERS_PER_FILESTORE_CONSISTENCY_LOCK.getIdentifier());
            rs = stmt.executeQuery();
            OptionalLong stamp = rs.next() ? OptionalLong.of(Long.parseLong(rs.getString(1))) : OptionalLong.empty();
            closeSQLStuff(rs, stmt);
            rs = null;
            stmt = null;

            long now = System.currentTimeMillis();
            int rows;
            if (stamp.isEmpty()) {
                // No lock entry
                stmt = con.prepareStatement("INSERT INTO `" + TABLE_NAME_REASON_TEXT + "` (`id`, `text`) VALUES (?, ?) ON DUPLICATE KEY UPDATE id=id");
                stmt.setInt(1, Reason.USERS_PER_FILESTORE_CONSISTENCY_LOCK.getIdentifier());
                stmt.setString(2, Long.toString(now));
                rows = stmt.executeUpdate();
                closeSQLStuff(stmt);
                stmt = null;
            } else if (now - stamp.getAsLong() > timeToLive) {
                // Lock entry expired
                stmt = con.prepareStatement("UPDATE `" + TABLE_NAME_REASON_TEXT + "` SET `text`=? WHERE `id`=? AND `text`=?");
                stmt.setString(1, Long.toString(now));
                stmt.setInt(2, Reason.USERS_PER_FILESTORE_CONSISTENCY_LOCK.getIdentifier());
                stmt.setString(3, Long.toString(stamp.getAsLong()));
                rows = stmt.executeUpdate();
                closeSQLStuff(stmt);
                stmt = null;
            } else {
                // Locked and not yet expired...
                rows = 0;
            }

            return rows > 0;
        } finally {
            closeSQLStuff(rs, stmt);
        }
    }

    /**
     * Drops (releases) the previously acquired consistency lock for re-initializing the <code>"users_per_filestore"</code> table.
     *
     * @param con The connection to use
     */
    private static void dropConsistencyLock(Connection con) {
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("DELETE FROM `" + TABLE_NAME_REASON_TEXT + "` WHERE `id`=?");
            stmt.setInt(1, Reason.USERS_PER_FILESTORE_CONSISTENCY_LOCK.getIdentifier());
            stmt.executeUpdate();
        } catch (Exception e) {
            LOG.error("Failed to drop consistency lock", e);
        } finally {
            closeSQLStuff(stmt);
        }
    }

    private static boolean tableDoesNotExist(final Connection con, final String table) throws SQLException {
        return tableExist(con, table) == false;
    }

    private static boolean tableExist(final Connection con, final String table) throws SQLException {
        final DatabaseMetaData metaData = con.getMetaData();
        ResultSet rs = null;
        try {
            rs = metaData.getTables(con.getCatalog(), null, table, new String[] { "TABLE" });
            return (rs.next() && rs.getString("TABLE_NAME").equalsIgnoreCase(table));
        } finally {
            Databases.closeSQLStuff(rs);
        }
    }

    static boolean columnExists(final Connection con, final String table, final String column) throws SQLException {
        final DatabaseMetaData metaData = con.getMetaData();
        ResultSet rs = null;
        boolean retval = false;
        try {
            rs = metaData.getColumns(con.getCatalog(), null, table, column);
            while (rs.next()) {
                retval = rs.getString(4).equalsIgnoreCase(column);
            }
        } finally {
            Databases.closeSQLStuff(rs);
        }
        return retval;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------

    private static final class FilestoreEntry {

        final int cid;
        final int user;
        final int filestoreId;
        private final int hash;

        FilestoreEntry(int cid, int user, int filestoreId) {
            super();
            this.cid = cid;
            this.user = user;
            this.filestoreId = filestoreId;

            int prime = 31;
            int result = prime * 1 + cid;
            result = prime * result + filestoreId;
            result = prime * result + user;
            hash = result;
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FilestoreEntry)) {
                return false;
            }
            FilestoreEntry other = (FilestoreEntry) obj;
            if (cid != other.cid) {
                return false;
            }
            if (filestoreId != other.filestoreId) {
                return false;
            }
            if (user != other.user) {
                return false;
            }
            return true;
        }
    }

    /**
     * A collection of {@link FilestoreCount} instances.
     */
    public static final class FilestoreCountCollection implements Iterable<FilestoreCount> {

        private final Map<Integer, FilestoreCount> counts;

        /**
         * Initializes a new {@link FilestoreCountCollection}.
         */
        FilestoreCountCollection(Map<Integer, FilestoreCount> counts) {
            super();
            this.counts = counts;
        }

        /**
         * Checks if this collection is empty.
         *
         * @return <code>true</code> if empty; otherwise <code>false</code>
         */
        public boolean isEmpty() {
            return counts.isEmpty();
        }

        /**
         * Gets the size of this collection
         *
         * @return The size
         */
        public int size() {
            return counts.size();
        }

        /**
         * gets the user count for specified file store identifier
         *
         * @param filestoreId The file store identifier to look-up with
         * @return The user count or <code>null</code>
         */
        public FilestoreCount get(int filestoreId) {
            return counts.get(Integer.valueOf(filestoreId));
        }

        @Override
        public Iterator<FilestoreCount> iterator() {
            return toList().iterator();
        }

        /**
         * Gets the (sorted) list view for this collection.
         *
         * @return The list containing this collection's elements
         */
        public List<FilestoreCount> toList() {
            List<FilestoreCount> l = new ArrayList<>(counts.values());
            Collections.sort(l);
            return l;
        }
    }

    /**
     * A simple object signaling the number of users using a referenced file store.
     */
    public static final class FilestoreCount implements Comparable<FilestoreCount> {

        private final int filestoreId;
        private int count;

        /**
         * Initializes a new {@link Filestore2UserUtil.FilestoreCount}.
         */
        FilestoreCount(int filestoreId, int count) {
            super();
            this.filestoreId = filestoreId;
            this.count = count;
        }

        void incrementCount() {
            count++;
        }

        /**
         * Gets the number of users using referenced file store
         *
         * @return The number of users using referenced file store
         */
        public int getCount() {
            return count;
        }

        /**
         * Gets the file store identifier
         *
         * @return The file store identifier
         */
        public int getFilestoreId() {
            return filestoreId;
        }

        @Override
        public int compareTo(FilestoreCount o) {
            int x = this.count;
            int y = o.count;
            if (x == y) {
                x = this.filestoreId;
                y = o.filestoreId;
                return (x < y) ? -1 : ((x == y) ? 0 : 1);
            }

            return (x < y) ? -1 : 1;
        }
    }

    /** Simple interface for obtaining and releasing a database connection from pool */
    private static interface ConnectionProvider {

        /**
         * Acquires a connection from pool.
         *
         * @return The connection
         * @throws StorageException If a connection cannot be returned
         */
        Connection getConnection() throws StorageException;

        /**
         * Checks the server identifier.
         *
         * @return <code>true</code> if server identifier is alright; otherwise <code>false</code>
         * @throws StorageException If check fails
         */
        boolean checkServerId() throws StorageException;

        /**
         * Puts given connection back into pool.
         *
         * @param con The connection to release
         * @param afterReading <code>true</code> if connection was only used for reading; otherwise <code>false</code> for read-write access
         * @throws StorageException If connection could not be put back into pool
         */
        void releaseConnection(Connection con, boolean afterReading) throws StorageException;
    }

    /** The connection provider backed by database service */
    private static final class DatabaseServiceConnectionProvider implements ConnectionProvider {

        private final DatabaseService databaseService;

        /**
         * Initializes a new {@link DatabaseServiceConnectionProvider}.
         *
         * @param databaseService The database service
         */
        DatabaseServiceConnectionProvider(DatabaseService databaseService) {
            super();
            this.databaseService = databaseService;
        }

        @Override
        public Connection getConnection() throws StorageException {
            try {
                return databaseService.getForUpdateTask();
            } catch (OXException e) {
                throw StorageException.wrapForRMI(e);
            }
        }

        @Override
        public boolean checkServerId() throws StorageException {
            try {
                databaseService.getServerId();
                return true;
            } catch (OXException e) {
                if (NOT_RESOLVED_SERVER.equals(e)) {
                    return false;
                }

                // Re-throw...
                throw StorageException.wrapForRMI(e);
            }
        }

        @Override
        public void releaseConnection(Connection con, boolean afterReading) throws StorageException {
            if (afterReading) {
                databaseService.backForUpdateTaskAfterReading(con);
            } else {
                databaseService.backForUpdateTask(con);
            }
        }
    }

    /** The connection provider backed by admin cache */
    private static final class AdminCacheConnectionProvider implements ConnectionProvider {

        private final AdminCache adminCache;

        /**
         * Initializes a new {@link AdminCacheConnectionProvider}.
         *
         * @param adminCache The admin cache
         */
        AdminCacheConnectionProvider(AdminCache adminCache) {
            super();
            this.adminCache = adminCache;
        }

        @Override
        public Connection getConnection() throws StorageException {
            try {
                return adminCache.getWriteConnectionForConfigDB();
            } catch (PoolException e) {
                throw new StorageException(e);
            }
        }

        @Override
        public boolean checkServerId() throws StorageException {
            return true;
        }

        @Override
        public void releaseConnection(Connection con, boolean afterReading) throws StorageException {
            try {
                adminCache.pushWriteConnectionForConfigDB(con);
            } catch (PoolException e) {
                logPoolingError(e);
            }
        }
    }

}
