/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.admin.rmi.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import com.openexchange.admin.daemons.ClientAdminThread;
import com.openexchange.admin.properties.AdminProperties;
import com.openexchange.admin.rmi.OXDeputyPermissionsInterface;
import com.openexchange.admin.rmi.dataobjects.ActiveDeputyPermission;
import com.openexchange.admin.rmi.dataobjects.Context;
import com.openexchange.admin.rmi.dataobjects.Credentials;
import com.openexchange.admin.rmi.dataobjects.DeputyPermission;
import com.openexchange.admin.rmi.dataobjects.ModulePermission;
import com.openexchange.admin.rmi.dataobjects.User;
import com.openexchange.admin.rmi.exceptions.AbstractAdminRmiException;
import com.openexchange.admin.rmi.exceptions.DatabaseUpdateException;
import com.openexchange.admin.rmi.exceptions.InvalidCredentialsException;
import com.openexchange.admin.rmi.exceptions.InvalidDataException;
import com.openexchange.admin.rmi.exceptions.NoSuchContextException;
import com.openexchange.admin.rmi.exceptions.NoSuchObjectException;
import com.openexchange.admin.rmi.exceptions.NoSuchUserException;
import com.openexchange.admin.rmi.exceptions.RemoteExceptionUtils;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.services.AdminServiceRegistry;
import com.openexchange.admin.tools.AdminCache;
import com.openexchange.admin.tools.PropertyHandler;
import com.openexchange.deputy.AdministrativeDeputyService;
import com.openexchange.deputy.DefaultDeputyPermission;
import com.openexchange.deputy.DefaultModulePermission;
import com.openexchange.deputy.DefaultPermission;
import com.openexchange.exception.OXException;

/**
 * {@link OXDeputyPermissions}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class OXDeputyPermissions extends OXCommonImpl implements OXDeputyPermissionsInterface {

    /** The logger */
    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(OXDeputyPermissions.class);

    private static final String EMPTY_STRING = "";

    private final BasicAuthenticator basicauth;
    private final AdminCache cache;
    private final PropertyHandler prop;

    /**
     * Initializes a new instance of {@link OXDeputyPermissions}.
     *
     * @throws StorageException If initialization fails
     */
    public OXDeputyPermissions() throws StorageException {
        super();
        this.cache = ClientAdminThread.cache;
        this.prop = this.cache.getProperties();
        basicauth = BasicAuthenticator.createPluginAwareAuthenticator();
    }

    /**
     * Looks-up the administrative deputy management service.
     *
     * @return The administrative deputy management service
     * @throws StorageException If administrative deputy management service cannot be returned
     */
    private static AdministrativeDeputyService getAdministrativeDeputyService() throws StorageException {
        AdministrativeDeputyService deputyService = AdminServiceRegistry.getInstance().getService(AdministrativeDeputyService.class);
        if (deputyService == null) {
            throw new StorageException("Missing administrative deputy management service");
        }
        return deputyService;
    }

    @Override
    public String grantDeputyPermission(DeputyPermission deputyPermission, Context context, User user, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, NoSuchContextException, NoSuchUserException, InvalidDataException, DatabaseUpdateException {
        try {
            checkArgPresence(deputyPermission);
            checkArgsPresence(context, user);
            Credentials auth = credentials == null ? new Credentials(EMPTY_STRING, EMPTY_STRING) : credentials;
            if (prop.getUserProp(AdminProperties.User.AUTO_LOWERCASE, false)) {
                auth.setLogin(auth.getLogin().toLowerCase());
            }
            basicauth.doAuthentication(auth, context);
            checkContextAndSchema(context);
            try { // NOSONARLINT
                setIdOrGetIDFromNameAndIdObject(context, user);
            } catch (NoSuchObjectException e) {
                throw new NoSuchUserException(e);
            }
            try { // NOSONARLINT
                AdministrativeDeputyService deputyService = getAdministrativeDeputyService();
                return deputyService.grantDeputyPermission(toData(deputyPermission), user.getId().intValue(), context.getId().intValue());
            } catch (OXException e) {
                Throwable optCause = getNonOXExceptionCauseFrom(e);
                throw new StorageException(optCause != null ? optCause : e);
            }
        } catch (Throwable e) { // NOSONARLINT
            logAndEnhanceException(e, credentials, context, user);
            throw e;
        }
    }

    @Override
    public void updateDeputyPermission(String deputyId, DeputyPermission deputyPermission, Context context, User user, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, NoSuchContextException, NoSuchUserException, InvalidDataException, DatabaseUpdateException {
        try {
            checkArgPresence(deputyPermission);
            checkArgsPresence(context, user);
            checkArgPresence(deputyId);
            Credentials auth = credentials == null ? new Credentials(EMPTY_STRING, EMPTY_STRING) : credentials;
            if (prop.getUserProp(AdminProperties.User.AUTO_LOWERCASE, false)) {
                auth.setLogin(auth.getLogin().toLowerCase());
            }
            basicauth.doAuthentication(auth, context);
            checkContextAndSchema(context);
            try { // NOSONARLINT
                setIdOrGetIDFromNameAndIdObject(context, user);
            } catch (NoSuchObjectException e) {
                throw new NoSuchUserException(e);
            }
            try { // NOSONARLINT
                AdministrativeDeputyService deputyService = getAdministrativeDeputyService();
                deputyService.updateDeputyPermission(deputyId, toData(deputyPermission), user.getId().intValue(), context.getId().intValue());
            } catch (OXException e) {
                Throwable optCause = getNonOXExceptionCauseFrom(e);
                throw new StorageException(optCause != null ? optCause : e);
            }
        } catch (Throwable e) { // NOSONARLINT
            logAndEnhanceException(e, credentials, context, user);
            throw e;
        }
    }

    @Override
    public void revokeDeputyPermission(String deputyId, Context context, User user, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, NoSuchContextException, NoSuchUserException, InvalidDataException, DatabaseUpdateException {
        try {
            checkArgsPresence(context, user);
            checkArgPresence(deputyId);
            Credentials auth = credentials == null ? new Credentials(EMPTY_STRING, EMPTY_STRING) : credentials;
            if (prop.getUserProp(AdminProperties.User.AUTO_LOWERCASE, false)) {
                auth.setLogin(auth.getLogin().toLowerCase());
            }
            basicauth.doAuthentication(auth, context);
            checkContextAndSchema(context);
            try { // NOSONARLINT
                setIdOrGetIDFromNameAndIdObject(context, user);
            } catch (NoSuchObjectException e) {
                throw new NoSuchUserException(e);
            }
            try { // NOSONARLINT
                AdministrativeDeputyService deputyService = getAdministrativeDeputyService();
                deputyService.revokeDeputyPermission(deputyId, user.getId().intValue(), context.getId().intValue());
            } catch (OXException e) {
                Throwable optCause = getNonOXExceptionCauseFrom(e);
                throw new StorageException(optCause != null ? optCause : e);
            }
        } catch (Throwable e) { // NOSONARLINT
            logAndEnhanceException(e, credentials, context, user);
            throw e;
        }
    }

    @Override
    public void revokeAllDeputyPermission(Context context, User user, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, NoSuchContextException, NoSuchUserException, InvalidDataException, DatabaseUpdateException {
        try {
            checkArgsPresence(context, user);
            Credentials auth = credentials == null ? new Credentials(EMPTY_STRING, EMPTY_STRING) : credentials;
            if (prop.getUserProp(AdminProperties.User.AUTO_LOWERCASE, false)) {
                auth.setLogin(auth.getLogin().toLowerCase());
            }
            basicauth.doAuthentication(auth, context);
            checkContextAndSchema(context);
            try { // NOSONARLINT
                setIdOrGetIDFromNameAndIdObject(context, user);
            } catch (NoSuchObjectException e) {
                throw new NoSuchUserException(e);
            }
            try { // NOSONARLINT
                AdministrativeDeputyService deputyService = getAdministrativeDeputyService();
                int contextId = context.getId().intValue();
                int grantorId = user.getId().intValue();
                for (String deputyId : deputyService.listIdsOfDeputyPermissions(grantorId, contextId)) {
                    deputyService.revokeDeputyPermission(deputyId, grantorId, contextId);
                }
            } catch (OXException e) {
                Throwable optCause = getNonOXExceptionCauseFrom(e);
                throw new StorageException(optCause != null ? optCause : e);
            }
        } catch (Throwable e) { // NOSONARLINT
            logAndEnhanceException(e, credentials, context, user);
            throw e;
        }
    }

    @Override
    public ActiveDeputyPermission getDeputyPermission(String deputyId, Context context, User user, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, NoSuchContextException, NoSuchUserException, InvalidDataException, DatabaseUpdateException {
        try {
            checkArgsPresence(context, user);
            checkArgPresence(deputyId);
            Credentials auth = credentials == null ? new Credentials(EMPTY_STRING, EMPTY_STRING) : credentials;
            if (prop.getUserProp(AdminProperties.User.AUTO_LOWERCASE, false)) {
                auth.setLogin(auth.getLogin().toLowerCase());
            }
            basicauth.doAuthentication(auth, context);
            checkContextAndSchema(context);
            try { // NOSONARLINT
                setIdOrGetIDFromNameAndIdObject(context, user);
            } catch (NoSuchObjectException e) {
                throw new NoSuchUserException(e);
            }
            try { // NOSONARLINT
                AdministrativeDeputyService deputyService = getAdministrativeDeputyService();
                return toRmi(deputyService.getDeputyPermission(deputyId, user.getId().intValue(), context.getId().intValue()));
            } catch (OXException e) {
                Throwable optCause = getNonOXExceptionCauseFrom(e);
                throw new StorageException(optCause != null ? optCause : e);
            }
        } catch (Throwable e) { // NOSONARLINT
            logAndEnhanceException(e, credentials, context, user);
            throw e;
        }
    }

    @Override
    public List<ActiveDeputyPermission> listAll(Context context, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, NoSuchContextException, InvalidDataException, DatabaseUpdateException {
        try {
            if (null == context) {
                throw new InvalidDataException("Missing context.");
            }
            Credentials auth = credentials == null ? new Credentials(EMPTY_STRING, EMPTY_STRING) : credentials;
            if (prop.getUserProp(AdminProperties.User.AUTO_LOWERCASE, false)) {
                auth.setLogin(auth.getLogin().toLowerCase());
            }
            basicauth.doAuthentication(auth, context);
            checkContextAndSchema(context);
            try { // NOSONARLINT
                AdministrativeDeputyService deputyService = getAdministrativeDeputyService();
                return permsToRmi(deputyService.listDeputyPermissions(context.getId().intValue()));
            } catch (OXException e) {
                Throwable optCause = getNonOXExceptionCauseFrom(e);
                throw new StorageException(optCause != null ? optCause : e);
            }
        } catch (Throwable e) { // NOSONARLINT
            logAndEnhanceException(e, credentials, context, null);
            throw e;
        }
    }

    // ---------------------------------------------------- Helper stuff ------------------------------------------------------------------

    private static void checkArgsPresence(Context context, User user) throws InvalidDataException {
        if (null == context) {
            throw new InvalidDataException("Missing context.");
        }
        if (null == user) {
            throw new InvalidDataException("Missing user.");
        }
    }

    private static void checkArgPresence(DeputyPermission deputyPermission) throws InvalidDataException {
        if (deputyPermission == null) {
            throw new InvalidDataException("Permission data is missing.");
        }
    }

    private static void checkArgPresence(String deputyId) throws InvalidDataException {
        if (null == deputyId) {
            throw new InvalidDataException("Missing deputy identifier.");
        }
    }

    private static void logAndEnhanceException(Throwable t, final Credentials credentials, final Context ctx, final User usr) {
        logAndEnhanceException(t, credentials, null != ctx ? ctx.getIdAsString() : null, null != usr ? String.valueOf(usr.getId()) : null);
    }

    private static void logAndEnhanceException(Throwable t, final Credentials credentials, final String contextId, String userId) {
        if (t instanceof AbstractAdminRmiException) {
            logAndReturnException(LOGGER, ((AbstractAdminRmiException) t), credentials, contextId, userId);
        } else if (t instanceof RemoteException) {
            RemoteException remoteException = (RemoteException) t;
            String exceptionId = AbstractAdminRmiException.generateExceptionId();
            RemoteExceptionUtils.enhanceRemoteException(remoteException, exceptionId);
            logAndReturnException(LOGGER, remoteException, exceptionId, credentials, contextId, userId);
        } else if (t instanceof Exception) {
            RemoteException remoteException = RemoteExceptionUtils.convertException((Exception) t);
            String exceptionId = AbstractAdminRmiException.generateExceptionId();
            RemoteExceptionUtils.enhanceRemoteException(remoteException, exceptionId);
            logAndReturnException(LOGGER, remoteException, exceptionId, credentials, contextId, userId);
        }
    }

    private static Throwable getNonOXExceptionCauseFrom(OXException e) {
        Throwable cause = e.getCause();
        return cause instanceof OXException ? getNonOXExceptionCauseFrom((OXException) cause) : cause;
    }

    // -------------------------------------------- Conversion to RMI objects -------------------------------------------------------------

    private static List<ActiveDeputyPermission> permsToRmi(List<com.openexchange.deputy.ActiveDeputyPermission> activeDeputyPermissions) {
        if (activeDeputyPermissions == null) {
            return null;
        }

        List<ActiveDeputyPermission> retval = new ArrayList<>(activeDeputyPermissions.size());
        for (com.openexchange.deputy.ActiveDeputyPermission modulePermission : activeDeputyPermissions) {
            retval.add(toRmi(modulePermission));
        }
        return retval;
    }

    private static ActiveDeputyPermission toRmi(com.openexchange.deputy.ActiveDeputyPermission activeDeputyPermission) {
        if (activeDeputyPermission == null) {
            return null;
        }

        ActiveDeputyPermission retval = new ActiveDeputyPermission();
        retval.setDeputyId(activeDeputyPermission.getDeputyId());
        retval.setEntityId(activeDeputyPermission.getEntityId());
        retval.setGroup(activeDeputyPermission.isGroup());
        retval.setModulePermissions(toRmi(activeDeputyPermission.getModulePermissions()));
        retval.setSendOnBehalfOf(activeDeputyPermission.isSendOnBehalfOf());
        retval.setGrantorId(activeDeputyPermission.getGrantorId());
        return retval;
    }

    private static List<ModulePermission> toRmi(List<com.openexchange.deputy.ModulePermission> modulePermissions) {
        if (modulePermissions == null) {
            return null;
        }

        List<ModulePermission> retval = new ArrayList<>(modulePermissions.size());
        for (com.openexchange.deputy.ModulePermission modulePermission : modulePermissions) {
            retval.add(toRmi(modulePermission));
        }
        return retval;
    }

    private static ModulePermission toRmi(com.openexchange.deputy.ModulePermission modulePermission) {
        if (modulePermission == null) {
            return null;
        }

        ModulePermission retval = new ModulePermission();
        retval.setModuleId(modulePermission.getModuleId());
        retval.setFolderIds(modulePermission.getOptionalFolderIds().orElse(null));
        retval.setAdmin(modulePermission.getPermission().isAdmin());
        retval.setFolderPermission(modulePermission.getPermission().getFolderPermission());
        retval.setReadPermission(modulePermission.getPermission().getReadPermission());
        retval.setWritePermission(modulePermission.getPermission().getWritePermission());
        retval.setDeletePermission(modulePermission.getPermission().getDeletePermission());
        return retval;
    }

    // -------------------------------------------- Conversion to data objects ------------------------------------------------------------

    private static com.openexchange.deputy.DeputyPermission toData(DeputyPermission deputyPermission) {
        if (deputyPermission == null) {
            return null;
        }

        return DefaultDeputyPermission.builder()
            .withEntityId(deputyPermission.getEntityId())
            .withGroup(deputyPermission.isGroup())
            .withSendOnBehalfOf(deputyPermission.isSendOnBehalfOf())
            .withModulePermissions(toData(deputyPermission.getModulePermissions(), deputyPermission.getEntityId(), deputyPermission.isGroup()))
            .build();
    }

    private static List<com.openexchange.deputy.ModulePermission> toData(List<ModulePermission> modulePermissions, int entityId, boolean group) {
        if (modulePermissions == null) {
            return null;
        }

        List<com.openexchange.deputy.ModulePermission> retval = new ArrayList<>(modulePermissions.size());
        for (ModulePermission modulePermission : modulePermissions) {
            retval.add(toData(modulePermission, entityId, group));
        }
        return retval;
    }

    private static com.openexchange.deputy.ModulePermission toData(ModulePermission modulePermission, int entityId, boolean group) {
        if (modulePermission == null) {
            return null;
        }

        List<String> folderIds = modulePermission.getFolderIds();
        return DefaultModulePermission.builder()
            .withFolderIds(folderIds == null || folderIds.isEmpty() ? null : folderIds)
            .withModuleId(modulePermission.getModuleId())
            .withPermission(toData(modulePermission.getFolderPermission(), modulePermission.getReadPermission(), modulePermission.getWritePermission(), modulePermission.getDeletePermission(), modulePermission.isAdmin(), entityId, group))
            .build();
    }

    private static com.openexchange.deputy.Permission toData(int fp, int rp, int wp, int dp, boolean admin, int entityId, boolean group) {
        return DefaultPermission.builder()
            .withAdmin(admin)
            .withAllPermissions(fp, rp, wp, dp)
            .withEntity(entityId)
            .withGroup(group)
            .build();
    }

}
