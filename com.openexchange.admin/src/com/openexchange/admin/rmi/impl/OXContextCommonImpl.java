/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.rmi.impl;


import static com.openexchange.admin.storage.mysqlStorage.OXContextMySQLStorageCommon.PREASSEMBLED_NAME_PREFIX;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import com.openexchange.admin.metrics.PluginMetricsProcessor;
import com.openexchange.admin.plugins.OXContextPluginInterface;
import com.openexchange.admin.plugins.PluginException;
import com.openexchange.admin.properties.ContextPreAssemblyProperties;
import com.openexchange.admin.properties.PropertyScope;
import com.openexchange.admin.rmi.dataobjects.Context;
import com.openexchange.admin.rmi.dataobjects.Credentials;
import com.openexchange.admin.rmi.dataobjects.Database;
import com.openexchange.admin.rmi.dataobjects.SchemaSelectStrategy;
import com.openexchange.admin.rmi.dataobjects.User;
import com.openexchange.admin.rmi.dataobjects.UserModuleAccess;
import com.openexchange.admin.rmi.exceptions.ContextExistsException;
import com.openexchange.admin.rmi.exceptions.EnforceableDataObjectException;
import com.openexchange.admin.rmi.exceptions.InvalidCredentialsException;
import com.openexchange.admin.rmi.exceptions.InvalidDataException;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.services.AdminServiceRegistry;
import com.openexchange.admin.services.PluginInterfaces;
import com.openexchange.admin.storage.interfaces.OXToolStorageInterface;
import com.openexchange.admin.tools.GenericChecks;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.LogLevel;
import com.openexchange.java.Strings;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;


public abstract class OXContextCommonImpl extends OXCommonImpl {

    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(OXContextCommonImpl.class);

    /** The maximum number of pre-assembled contexts that are fetched */
    private static final int PREASSEMBLED_CONTEXTS_FETCH_LIMIT = 100;

    /**
     * Initializes a new {@link OXContextCommonImpl}.
     *
     */
    protected OXContextCommonImpl() {
        super();
    }

    protected void createchecks(final Context ctx, final User admin_user, final OXToolStorageInterface tool) throws StorageException, InvalidDataException, ContextExistsException {
        createchecks(ctx, admin_user, tool, false);
    }

    /**
     * Performs various checks on the passed data prior creating a new context.
     * 
     * @param ctx The context to create
     * @param admin_user The admin user for the context
     * @param tool The storage interface utility
     * @param skipContextIdChecks <code>true</code> to skip checks of the set context identifier (when using a pre-assembled context), <code>false</code>, otherwise
     * @throws StorageException If something fails on the storage layer
     * @throws InvalidDataException If supplied data is invalid or missing mandatory fields
     * @throws ContextExistsException If the referenced context cannot be used
     */
    protected void createchecks(final Context ctx, final User admin_user, final OXToolStorageInterface tool, boolean skipContextIdChecks) throws StorageException, InvalidDataException, ContextExistsException {
        try {
            Boolean ret = null;

            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {

                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            Boolean checkMandatoryMembersContextCreate = oxContextPlugin.checkMandatoryMembersContextCreate(ctx);
                            success = true;
                            ret = checkMandatoryMembersContextCreate;
                        } finally {
                            PluginMetricsProcessor.getForRead(oxContextPlugin.getClass().getName(), "checkMandatoryMembersContextCreate").stopTimer(timerSample, success);
                        }
                    }
                }
            }

            if (ret == null || ret.booleanValue()) {
                if (!ctx.mandatoryCreateMembersSet()) {
                    throw new InvalidDataException("Mandatory fields in context not set: " + ctx.getUnsetMembers());
                }
            }
        } catch (EnforceableDataObjectException e) {
            throw new InvalidDataException(e.getMessage());
        } catch (PluginException e) {
            throw StorageException.wrapForRMI(e);
        }

        /*
         * Check that a valid identifier was set
         */
        if (false == skipContextIdChecks) {
            tool.checkContextIdentifier(ctx);
        }

        try {
            if (!admin_user.mandatoryCreateMembersSet()) {
                throw new InvalidDataException("Mandatory fields in admin user not set: " + admin_user.getUnsetMembers());
            }
        } catch (EnforceableDataObjectException e) {
            throw new InvalidDataException(e.getMessage());
        }

        GenericChecks.checkValidMailAddress(admin_user.getPrimaryEmail());
        GenericChecks.checkValidMailAddressRegex(admin_user.getPrimaryEmail(), PropertyScope.propertyScopeForServer());

        checkUserAttributes(admin_user);
    }

    protected abstract Context createmaincall(final Context ctx, final User admin_user, Database db, UserModuleAccess access, final Credentials auth, SchemaSelectStrategy schemaSelectStrategy) throws StorageException, InvalidDataException, ContextExistsException;

    protected SchemaSelectStrategy getDefaultSchemaSelectStrategy() {
        return SchemaSelectStrategy.getDefault();
    }

    protected Context createcommon(final Context ctx, final User admin_user, final Database db, final UserModuleAccess access, final Credentials auth, SchemaSelectStrategy schemaSelectStrategy) throws InvalidCredentialsException, ContextExistsException, InvalidDataException, StorageException {
        doNullCheck(ctx, admin_user, auth);
        long start = System.nanoTime();
        ClaimedContext preAssemblyClaim = null;
        try {
            BasicAuthenticator.createPluginAwareAuthenticator().doAuthentication(auth);

            log(LogLevel.DEBUG, LOGGER, auth, null, "{} - {}", ctx, admin_user);
            Context ret = ctx;

            // Check if pre-assembled context can be used, select & claim one, take over context id
            LeanConfigurationService leanConfigurationService = AdminServiceRegistry.getInstance().getService(LeanConfigurationService.class);
            if (leanConfigurationService.getBooleanProperty(ContextPreAssemblyProperties.CONTEXT_PRE_ASSEMBLY_ENABLED)) {
                checkIfPreAssembledIsUsable(ret, schemaSelectStrategy);
                preAssemblyClaim = claimPreAssembledContext(auth, getPreAssembledContexts(PREASSEMBLED_CONTEXTS_FETCH_LIMIT));
                if (null == preAssemblyClaim) {
                    throw new StorageException("No pre-assembled context associated with a local segment is available.");
                }
                log(LogLevel.INFO, LOGGER, auth, null, "Using pre-assembled context with id {} for new context {}.", I(preAssemblyClaim.contextId()), ret.getName());
                ret.setId(I(preAssemblyClaim.contextId()));

                // Check if context name was set, otherwise fall-back to context id
                if (false == ret.isNameset() || (ret.isNameset() && ret.getName().trim().isEmpty())) {
                    ret.setName(ret.getIdAsString());
                }
            }

            PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
            if (null != pluginInterfaces) {
                for (final OXContextPluginInterface contextInterface : pluginInterfaces.getContextPlugins().getServiceList()) {
                    if (null != preAssemblyClaim && isAutoContextIdPlugin(contextInterface)) {
                        continue;
                    }
                    Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                    boolean success = false;
                    try {
                        Context preCreate = contextInterface.preCreate(ret, admin_user, auth);
                        success = true;
                        ret = preCreate;
                    } finally {
                        PluginMetricsProcessor.getForCreate(contextInterface.getClass().getName(), "preCreate").stopTimer(timerSample, success);
                    }
                }
            }

            createchecks(ret, admin_user, OXToolStorageInterface.getInstance(), null != preAssemblyClaim);

            // Ensure context identifier is contained in login mappings
            final String sContextId = ret.getIdAsString();
            if (null != sContextId) {
                ret.addLoginMapping(sContextId);
            }

            // Ensure context name is contained in login mappings
            final String name = ret.getName();
            if (null != name) {
                // Add the name of the context to the login mappings and the id
                ret.addLoginMapping(name);
            }

            // Create or use pre-assembled context
            Context retval;
            if (null == preAssemblyClaim) {
                retval = createmaincall(ret, admin_user, db, access, auth, schemaSelectStrategy);
            } else {
                checkClaimIsValid(preAssemblyClaim);
                retval = usePreAssembledContext(auth, ret, admin_user, access);
            }
            if (retval.getName() == null) {
                retval.setName(String.valueOf(retval.getId()));
            }

            if (null != pluginInterfaces) {
                PluginException pe = null;
                for (OXContextPluginInterface contextInterface : pluginInterfaces.getContextPlugins().getServiceList()) {
                    try {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            retval = contextInterface.postCreate(retval, admin_user, access, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForCreate(contextInterface.getClass().getName(), "postCreate").stopTimer(timerSample, success);
                        }
                    } catch (PluginException e) {
                        pe = e;
                    }
                }
                if (null != pe) {
                    // cleanup & throw wrapped plugin exceptions
                    try {
                        cleanUpAfterPluginFailed(retval, auth);
                    } catch (Exception x) {
                        log(LogLevel.ERROR, LOGGER, auth, x, "Error cleaning up after plugin failed: {}", x.getMessage());
                    }
                    throw StorageException.wrapForRMI(pe);
                }
            }
            return retval;
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), auth, ctx.getIdAsString());
        } finally {
            releaseClaim(auth, preAssemblyClaim);
            log(LogLevel.INFO, LOGGER, auth, null, "OXContextCommonImpl#createcommon finished after {}ms.", L(TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)));
        }
    }

    private void doNullCheck(final Context ctx, final User admin_user, final Credentials auth) throws InvalidDataException {
        try {
            doNullCheck(ctx, admin_user);
        } catch (InvalidDataException e) {
            final InvalidDataException invalidDataException = new InvalidDataException("Context or user not correct", e);
            log(LogLevel.ERROR, LOGGER, auth, invalidDataException, "");
            throw invalidDataException;
        }
    }

    /**
     * Check if plugins are loaded
     *
     * @return <code>true</code> if a plugin is loaded
     */
    protected boolean isAnyPluginLoaded() {
        final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
        return null != pluginInterfaces && false == pluginInterfaces.getContextPlugins().getServiceList().isEmpty();
    }

    /**
     * Gets a value indicating whether the supplied context plugin denotes the one that automatically assigns identifiers to newly
     * created contexts.
     * 
     * @param contextInterface The context plugin to check
     * @return <code>true</code> if the plugin is the auto-context-id plugin, <code>false</code>, otherwise
     */
    protected abstract boolean isAutoContextIdPlugin(OXContextPluginInterface contextInterface);

    /**
     * Adjusts and enables a pre-assembled context and corresponding admin user entity, based on the data of the supplied context,
     * admin user, and default access combination.
     *
     * @param credentials The credentials bound to the provisioning operation (for logging purposes)
     * @param contextToCreate The context to create, referencing a pre-assembled context identifier
     * @param adminToCreate The admin user for the context
     * @param access The user module access
     * @return The passed context instance
     * @throws StorageException If a problem occurs on the storage layer
     */
    protected abstract Context usePreAssembledContext(Credentials credentials, Context contextToCreate, User adminToCreate, UserModuleAccess access) throws InvalidDataException, InvalidCredentialsException, StorageException;

    /**
     * Looks up multiple random contexts from the database that qualify as <i>pre-assembled</i> contexts, i.e.:
     * <ul>
     * <li>they're <i>disabled</i> with reason id <code>{@link #PREASSEMBLED_REASON_ID}</code></li>
     * <li>their name starts with <code>{@link #PREASSEMBLED_NAME_PREFIX}</code></li>
     * </ul>
     * 
     * @param limit The maximum number of pre-assembled contexts to return
     * @return The pre-assembled contexts, mapped to their associated database schema name, or an empty map if none were found
     * @throws StorageException If a problem occurs on the storage layer
     */
    protected abstract Map<String, List<Integer>> getPreAssembledContexts(int limit) throws StorageException;
    
    /**
     * Claims a randomly selected, pre-assembled context from the supplied collection of available candidates.
     * <p/>
     * The context is implicitly selected from a <i>local</i> segment, based on the site information obtained from the segmenter service.
     * 
     * @param credentials The credentials bound to the provisioning operation (for logging purposes)
     * @param candidateContextsPerSchema The candidate contexts, mapped to their associated database schema names
     * @return The selected and claimed pre-assembled context
     * @throws StorageException If no pre-assembled context could be claimed
     */
    protected abstract ClaimedContext claimPreAssembledContext(Credentials credentials, Map<String, List<Integer>> candidateContextsPerSchema) throws StorageException;

    /**
     * Releases a previously acquired claim for a context.
     * 
     * @param credentials The credentials bound to the provisioning operation (for logging purposes)
     * @param claimedContext The claimed context to release, or <code>null</code> for a no-op
     */
    protected abstract void releaseClaim(Credentials credentials, ClaimedContext claimedContext);

    /**
     * Checks that a previously acquired claim is still valid, i.e. a claim for the context exists in the database and matches the supplied one.
     * 
     * @param claimedContext The context claim to check validity for
     * @throws StorageException If the claim is no longer valid, or if a problem occurs on the storage layer
     */
    protected abstract void checkClaimIsValid(ClaimedContext claimedContext) throws StorageException;

    private void checkIfPreAssembledIsUsable(Context context, SchemaSelectStrategy strategy) throws StorageException {
        if (context.isIdset() && null != context.getId()) {
            throw new StorageException("Id must not be set if pre-assembled context should be used.");
        }
        if (context.isNameset() && context.getName().startsWith(PREASSEMBLED_NAME_PREFIX)) {
            throw new StorageException("A valid context name must be set. Context name must not start with \"%s\" as this is a reserved prefix.".formatted(PREASSEMBLED_NAME_PREFIX));
        }
        if (context.isFilestore_idset() || context.isFilestore_nameset()) {
            throw new StorageException("Filestore must not be set if pre-assembled context should be used.");
        }
        if (context.isReadDatabaseset() || context.isWriteDatabaseset()) {
            throw new StorageException("Database must not be set if pre-assembled context should be used.");
        }
        if (null != strategy && Strings.isNotEmpty(strategy.getSchema())) {
            throw new StorageException("Schema must not be set if pre-assembled context should be used.");
        }
    }

    /**
     * Cleans up (deletes so far created context) after a plugin failed
     *
     * @param ctx The context to clean up
     * @param credentials The credentials bound to the provisioning operation (for logging purposes)
     * @throws StorageException If a problem occurs on the storage layer
     */
    protected abstract void cleanUpAfterPluginFailed(Context ctx, Credentials credentials) throws StorageException;

}
