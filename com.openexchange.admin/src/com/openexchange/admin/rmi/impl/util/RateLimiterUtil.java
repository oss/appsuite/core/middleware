/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.rmi.impl.util;

import static com.openexchange.java.Autoboxing.l;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import com.openexchange.admin.properties.RMIProperties;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.rmi.exceptions.TooManyRequestsException;
import com.openexchange.admin.services.AdminServiceRegistry;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.ratelimit.GenericRateLimiterFactory;
import com.openexchange.ratelimit.Rate;

/**
 * {@link RateLimiterUtil} is a utility class which helps with rate limit checks
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class RateLimiterUtil {

    private static final Rate DEFAULT_RATE = Rate.create(l(RMIProperties.DEFAULT_RATE_LIMIT.getDefaultValue(Long.class)),
                                                         TimeUnit.MINUTES.toMillis(1));

    /**
     * Checks the rate limit for the given admin login
     *
     * @param admin The admin login
     * @throws StorageException in case the {@link GenericRateLimiterFactory} is missing
     * @throws TooManyRequestsException In case the admin exceeded its rate limit
     */
    public static void checkRateLimit(String admin) throws StorageException, TooManyRequestsException {
        if (Strings.isEmpty(admin)) {
            // unable to check rate limit with no admin provided
            return;
        }
        GenericRateLimiterFactory rateLimitService;
        try {
            rateLimitService = AdminServiceRegistry.getInstance().getService(GenericRateLimiterFactory.class, true);
        } catch (OXException e) {
            throw new StorageException(e);
        }
        if(false == rateLimitService.createLimiter("prov", getRate(admin), admin).acquire()) {
            throw new TooManyRequestsException("Too many requests.", TimeUnit.MINUTES.toSeconds(1));
        }
    }

    // ---------------------- private methods --------------------------

    /**
     * Gets the rate for the given admin
     *
     * @param admin The admin login
     * @return The rate
     */
    private static Rate getRate(String admin) {
        LeanConfigurationService configService = AdminServiceRegistry.getInstance().getService(LeanConfigurationService.class);
        if (configService == null || Strings.isEmpty(admin)) {
            return DEFAULT_RATE;
        }
        String prop = configService.getProperty(RMIProperties.RATE_LIMIT, Collections.singletonMap("admin", admin));
        if (prop == null) {
            return getDefaultRate(configService);
        }
        long amount = Long.parseLong(prop);
        return Rate.create(amount, TimeUnit.MINUTES.toMillis(1));
    }

    /**
     * Gets the default rate
     *
     * @param configService The {@link LeanConfigurationService} to use
     * @return The default rate
     */
    private static Rate getDefaultRate(LeanConfigurationService configService) {
        return Rate.create(configService.getLongProperty(RMIProperties.DEFAULT_RATE_LIMIT),
                           TimeUnit.MINUTES.toMillis(1));
    }

}
