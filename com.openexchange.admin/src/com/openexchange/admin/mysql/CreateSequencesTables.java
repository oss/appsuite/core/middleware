/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.mysql;

import com.openexchange.database.AbstractCreateTableImpl;


/**
 * {@link CreateSequencesTables}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public class CreateSequencesTables extends AbstractCreateTableImpl {

    private static final String SEQUENCE_ID_TABLE_NAME = "sequence_id";
    private static final String SEQUENCE_PRINCIPAL_TABLE_NAME = "sequence_principal";
    private static final String SEQUENCE_RESOURCE_TABLE_NAME = "sequence_resource";
    private static final String SEQUENCE_RESOURCE_GROUP_TABLE_NAME = "sequence_resource_group";
    private static final String SEQUENCE_FOLDER_TABLE_NAME = "sequence_folder";
    private static final String SEQUENCE_CALENDAR_TABLE_NAME = "sequence_calendar";
    private static final String SEQUENCE_CONTACT_TABLE_NAME = "sequence_contact";
    private static final String SEQUENCE_TASK_TABLE_NAME = "sequence_task";
    private static final String SEQUENCE_PROJECT_TABLE_NAME = "sequence_project";
    private static final String SEQUENCE_INFOSTORE_TABLE_NAME = "sequence_infostore";
    private static final String SEQUENCE_FORUM_TABLE_NAME = "sequence_forum";
    private static final String SEQUENCE_PINBOARD_TABLE_NAME = "sequence_pinboard";
    private static final String SEQUENCE_ATTACHMENT_TABLE_NAME = "sequence_attachment";
    private static final String SEQUENCE_GUI_SETTING_TABLE_NAME = "sequence_gui_setting";
    private static final String SEQUENCE_REMINDER_TABLE_NAME = "sequence_reminder";
    private static final String SEQUENCE_ICAL_TABLE_NAME = "sequence_ical";
    private static final String SEQUENCE_WEBDAV_TABLE_NAME = "sequence_webdav";
    private static final String SEQUENCE_MAIL_SERVICE_TABLE_NAME = "sequence_mail_service";

    private static final String CREATE_SEQUENCE_ID_TABLE = "CREATE TABLE sequence_id ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_PRINCIPAL_TABLE = "CREATE TABLE sequence_principal ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_RESOURCE_TABLE = "CREATE TABLE sequence_resource ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_RESOURCE_GROUP_TABLE = "CREATE TABLE sequence_resource_group ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_FOLDER_TABLE = "CREATE TABLE sequence_folder ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_CALENDAR_TABLE = "CREATE TABLE sequence_calendar ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_CONTACT_TABLE = "CREATE TABLE sequence_contact ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_TASK_TABLE = "CREATE TABLE sequence_task ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_PROJECT_TABLE = "CREATE TABLE sequence_project ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_INFOSTORE_TABLE = "CREATE TABLE sequence_infostore ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_FORUM_TABLE = "CREATE TABLE sequence_forum ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_PINBOARD_TABLE = "CREATE TABLE sequence_pinboard ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_ATTACHMENT_TABLE = "CREATE TABLE sequence_attachment ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_GUI_SETTING_TABLE = "CREATE TABLE sequence_gui_setting ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_REMINDER_TABLE = "CREATE TABLE sequence_reminder ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_ICAL_TABLE = "CREATE TABLE sequence_ical ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_WEBDAV_TABLE = "CREATE TABLE sequence_webdav ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    private static final String CREATE_SEQUENCE_MAIL_SERVICE_TABLE = "CREATE TABLE sequence_mail_service ("
       + "cid INT4 UNSIGNED NOT NULL,"
       + "id INT4 UNSIGNED NOT NULL,"
       + "PRIMARY KEY (cid)"
     + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

    /**
     * Initializes a new {@link CreateSequencesTables}.
     */
    public CreateSequencesTables() {
        super();
    }

    @Override
    public String[] requiredTables() {
        return NO_TABLES;
    }

    @Override
    public String[] tablesToCreate() {
        return new String[] { SEQUENCE_ID_TABLE_NAME, SEQUENCE_PRINCIPAL_TABLE_NAME, SEQUENCE_RESOURCE_TABLE_NAME,
            SEQUENCE_RESOURCE_GROUP_TABLE_NAME, SEQUENCE_FOLDER_TABLE_NAME, SEQUENCE_CALENDAR_TABLE_NAME, SEQUENCE_CONTACT_TABLE_NAME,
            SEQUENCE_TASK_TABLE_NAME, SEQUENCE_PROJECT_TABLE_NAME, SEQUENCE_INFOSTORE_TABLE_NAME, SEQUENCE_FORUM_TABLE_NAME,
            SEQUENCE_PINBOARD_TABLE_NAME, SEQUENCE_ATTACHMENT_TABLE_NAME, SEQUENCE_GUI_SETTING_TABLE_NAME, SEQUENCE_REMINDER_TABLE_NAME,
            SEQUENCE_ICAL_TABLE_NAME, SEQUENCE_WEBDAV_TABLE_NAME, SEQUENCE_MAIL_SERVICE_TABLE_NAME };
    }

    @Override
    protected String[] getCreateStatements() {
        return new String[] { CREATE_SEQUENCE_ID_TABLE, CREATE_SEQUENCE_PRINCIPAL_TABLE, CREATE_SEQUENCE_RESOURCE_TABLE,
            CREATE_SEQUENCE_RESOURCE_GROUP_TABLE, CREATE_SEQUENCE_FOLDER_TABLE, CREATE_SEQUENCE_CALENDAR_TABLE, CREATE_SEQUENCE_CONTACT_TABLE,
            CREATE_SEQUENCE_TASK_TABLE, CREATE_SEQUENCE_PROJECT_TABLE, CREATE_SEQUENCE_INFOSTORE_TABLE, CREATE_SEQUENCE_FORUM_TABLE,
            CREATE_SEQUENCE_PINBOARD_TABLE, CREATE_SEQUENCE_ATTACHMENT_TABLE, CREATE_SEQUENCE_GUI_SETTING_TABLE, CREATE_SEQUENCE_REMINDER_TABLE,
            CREATE_SEQUENCE_ICAL_TABLE, CREATE_SEQUENCE_WEBDAV_TABLE, CREATE_SEQUENCE_MAIL_SERVICE_TABLE };
    }

}
