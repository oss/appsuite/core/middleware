/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.properties;

import com.openexchange.config.lean.Property;

/**
 * {@link ContextPreAssemblyProperties} - The configuration properties for context pre-assembly.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public enum ContextPreAssemblyProperties implements Property {

    /**
     * The property to decide on whether contexts pre-assembly is enabled or not.
     * Defaults to <code>false</code>
     */
    CONTEXT_PRE_ASSEMBLY_ENABLED("enabled", Boolean.FALSE),

    ;

    private final String fqn;
    private final Object defaultValue;

    /**
     * Initializes a new {@link ContextPreAssemblyProperties}.
     *
     * @param appendix The appendix for the fully-qualifying name
     * @param defaultValue The default value
     */
    private ContextPreAssemblyProperties(String appendix, Object defaultValue) {
        this.fqn = "com.openexchange.admin.context.preassembly." + appendix;
        this.defaultValue = defaultValue;
    }

    /**
     * Returns the fully-qualifying name for the property
     *
     * @return the fully-qualifying name for the property
     */
    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    /**
     * Returns the default value of this property
     *
     * @return the default value of this property
     */
    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }
}
