/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.tools;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

/**
 * @author choeger
 *
 */
public class NetUtilTest {

    /**
     * Test method for
     * {@link com.openexchange.admin.tools.NetUtil#isValidNetmask(java.lang.String)}.
     */
    @Test
    public final void testIsValidNetmask() {
        final String[] validMasks = new String[] { "255.255.255.0", "255.255.255.252", "255.0.0.0" };
        for (final String mask : validMasks) {
            assertTrue(NetUtil.isValidNetmask(mask), "Mask " + mask + " must be valid");
        }
        final String[] invalidMasks = new String[] { "255.255.25.0", "255.255.255.242", "255.255.0.1", "42" };
        for (final String mask : invalidMasks) {
            assertFalse(NetUtil.isValidNetmask(mask), "Mask " + mask + " must be invalid");
        }
    }

    /**
     * Test method for
     * {@link com.openexchange.admin.tools.NetUtil#isValidIPAddress(java.lang.String)}.
     */
    @Test
    public final void testIsValidIPAddress() {
        final String[] validIPs = new String[] { "192.168.1.2", "192.168.245.244" };
        for (final String ip : validIPs) {
            assertTrue(NetUtil.isValidIPAddress(ip), "IP " + ip + " must be valid");
        }
        final String[] invalidIPs = new String[] { "292.168.1.2", "192.168.244" };
        for (final String ip : invalidIPs) {
            assertFalse(NetUtil.isValidIPAddress(ip), "IP " + ip + " must be invalid");
        }
    }

    /**
     * Test method for
     * {@link com.openexchange.admin.tools.NetUtil#isValidBroadcast(java.lang.String, java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testIsValidBroadcast() {
        String[] bcasts = new String[] { "192.168.0.111", "10.20.127.255" };
        String[] nets = new String[] { "192.168.0.96", "10.20.0.0" };
        String[] masks = new String[] { "255.255.255.240", "255.255.128.0" };
        for (int n = 0; n < bcasts.length; n++) {
            assertTrue(NetUtil.isValidBroadcast(bcasts[n], nets[n], masks[n]), "Broadcast " + bcasts[n] + " must be valid");
        }
        bcasts = new String[] { "192.168.0.113", "10.20.127.254" };
        nets = new String[] { "192.168.0.96", "10.20.0.0" };
        masks = new String[] { "255.255.255.240", "255.255.128.0" };
        for (int n = 0; n < bcasts.length; n++) {
            assertFalse(NetUtil.isValidBroadcast(bcasts[n], nets[n], masks[n]), "Broadcast " + bcasts[n] + " must be invalid");
        }
    }

    /**
     * Test method for
     * {@link com.openexchange.admin.tools.NetUtil#isValidIPNetmask(java.lang.String)}.
     */
    @Test
    public final void testIsValidIPNetmask() {
        final String[] validIPMasks = new String[] { "10.11.12.13/255.255.0.0", "172.16.13.14/8" };
        for (final String ipmask : validIPMasks) {
            assertTrue(NetUtil.isValidIPNetmask(ipmask), "IPMask " + ipmask + " must be valid");
        }
        final String[] invalidIPMasks = new String[] { "10.11.12.13/2.255.0.0", "172.16.13.14/255.", "" };
        for (final String ipmask : invalidIPMasks) {
            assertFalse(NetUtil.isValidIPNetmask(ipmask), "IPMask " + ipmask + " must be invalid");
        }
    }

    /**
     * Test method for
     * {@link com.openexchange.admin.tools.NetUtil#CIDR2Mask(int)}.
     */
    @Test
    public final void testCIDR2Mask() {
        final int[] cidrs = new int[] { 16, 32, 24, 30 };
        final String[] masks = new String[] { "255.255.0.0", "255.255.255.255", "255.255.255.0", "255.255.255.252", };
        for (int n = 0; n < cidrs.length; n++) {
            assertEquals(masks[n], NetUtil.CIDR2Mask(cidrs[n]), "test failed for mask " + masks[n]);
        }
    }

}
