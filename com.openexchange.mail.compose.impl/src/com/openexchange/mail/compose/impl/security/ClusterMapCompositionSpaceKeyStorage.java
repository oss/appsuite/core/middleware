/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.compose.impl.security;

import java.security.Key;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.slf4j.Logger;
import com.openexchange.capabilities.CapabilitySet;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.ClusterMap;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.cluster.map.codec.MapCodecs;
import com.openexchange.exception.OXException;
import com.openexchange.java.util.UUIDs;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;


/**
 * {@link ClusterMapCompositionSpaceKeyStorage} - The (default) key storage backed by a cluster map.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.0.0
 */
public class ClusterMapCompositionSpaceKeyStorage extends AbstractCompositionSpaceKeyStorage {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ClusterMapCompositionSpaceKeyStorage.class);
    }

    private final ConcurrentMap<String, String> inMemoryMap;
    private final Lock lock;
    private final BasicCoreClusterMapProvider<String> clusterMapProvider;
    private ClusterMapService clusterMapService; // Guarded by lock

    /**
     * Initializes a new {@link ClusterMapCompositionSpaceKeyStorage}.
     *
     * @param services The service look-up
     */
    public ClusterMapCompositionSpaceKeyStorage(ServiceLookup services) {
        super(services);
        inMemoryMap = new ConcurrentHashMap<>(128);
        lock = new ReentrantLock();
        clusterMapProvider = BasicCoreClusterMapProvider.<String> builder() //@formatter:off
            .withCoreMap(CoreMap.COMPOSITION_SPACE_KEY_STORAGE)
            .withCodec(MapCodecs.getStringCodec())
            .withExpireMillis(640000 * 1000)
            .build(); //@formatter:on
    }

    @Override
    public boolean isApplicableFor(CapabilitySet capabilities, Session session) throws OXException {
        return true;
    }

    @Override
    public Key getKeyFor(UUID compositionSpaceId, boolean createIfAbsent, Session session) throws OXException {
        if (null == compositionSpaceId) {
            return null;
        }

        // Get the map reference
        KeyStorageMap ksMap = getMap();

        String csid = UUIDs.getUnformattedString(compositionSpaceId);
        String obfuscatedBase64EncodedKey = ksMap.get(csid);
        if (null != obfuscatedBase64EncodedKey) {
            return base64EncodedString2Key(unobfuscate(obfuscatedBase64EncodedKey));
        }

        if (false == createIfAbsent) {
            return null;
        }

        Key newRandomKey = generateRandomKey();
        String newObfuscatedBase64EncodedKey = obfuscate(key2Base64EncodedString(newRandomKey));
        obfuscatedBase64EncodedKey = ksMap.putIfAbsent(csid, newObfuscatedBase64EncodedKey);
        return null == obfuscatedBase64EncodedKey ? newRandomKey : base64EncodedString2Key(unobfuscate(obfuscatedBase64EncodedKey));
    }

    @Override
    public List<UUID> deleteKeysFor(Collection<UUID> compositionSpaceIds, Session session) throws OXException {
        if (null == compositionSpaceIds || compositionSpaceIds.isEmpty()) {
            return Collections.emptyList();
        }

        // Get the map reference
        KeyStorageMap ksMap = getMap();

        List<UUID> nonDeletedKeys = null;
        for (UUID compositionSpaceId : compositionSpaceIds) {
            String csid = UUIDs.getUnformattedString(compositionSpaceId);
            if (ksMap.remove(csid) == null) {
                // Not removed from cluster map
                if (null == nonDeletedKeys) {
                    nonDeletedKeys = new ArrayList<UUID>(compositionSpaceIds.size());
                }
                nonDeletedKeys.add(compositionSpaceId);
            }
        }
        return null == nonDeletedKeys ? Collections.emptyList() : nonDeletedKeys;
    }

    private KeyStorageMap getMap() {
        lock.lock();
        try {
            ClusterMapService clusterMapService = this.clusterMapService;
            if (null == clusterMapService) {
                LoggerHolder.LOG.warn("Missing cluster map service instance. Using non-distributed map instead");
                return new MapKeyStorageMap(inMemoryMap);
            }

            try {
                // Get the cluster map reference
                return new ClusterMapKeyStorageMap(clusterMapProvider.getMap(clusterMapService));
            } catch (Exception e) {
                LoggerHolder.LOG.warn("Failed to acquire cluster map. Using non-distributed map instead", e);
                return new MapKeyStorageMap(inMemoryMap);
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * Sets the cluster map service to use.
     *
     * @param clusterMapService The cluster map service
     */
    public void setClusterMapService(ClusterMapService clusterMapService) {
        lock.lock();
        try {
            this.clusterMapService = clusterMapService;

            // Get the map reference
            ClusterMap<String> clusterMap = clusterMapService.getMap(CoreMap.COMPOSITION_SPACE_KEY_STORAGE, MapCodecs.getStringCodec());
            for (Map.Entry<String, String> csid2key : inMemoryMap.entrySet()) {
                clusterMap.put(csid2key.getKey(), csid2key.getValue());
            }
            inMemoryMap.clear();
        } catch (Exception e) {
            LoggerHolder.LOG.error("Cluster map could not be initialized", e);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Unsets the previously set cluster map service (if any).
     */
    public void unsetClusterMapService(boolean forShutDown) {
        lock.lock();
        try {
            ClusterMapService clusterMapService = this.clusterMapService;
            if (null == clusterMapService) {
                return;
            }
            this.clusterMapService = null;

            if (false == forShutDown) {
                // Get the cluster map reference
                ClusterMap<String> clusterMap = clusterMapService.getMap(CoreMap.COMPOSITION_SPACE_KEY_STORAGE, MapCodecs.getStringCodec());
                for (String csid : clusterMap.keySet()) {
                    String base64EncodedKey = clusterMap.get(csid);
                    inMemoryMap.put(csid, base64EncodedKey);
                }
            }
        } catch (Exception e) {
            LoggerHolder.LOG.error("Cluster map could not be unset", e);
        } finally {
            lock.unlock();
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static interface KeyStorageMap {

        /**
         * Gets the value to which the specified key is mapped,
         * or {@code null} if this map contains no mapping for the key.
         *
         * @param key The key whose associated value is to be returned
         * @return The value to which the specified key is mapped, or
         *         {@code null} if this map contains no mapping for the key
         * @throws OXException If operation fails
         */
        String get(String key) throws OXException;

        /**
         * Associates the specified value with the specified key in this map
         * (optional operation).
         * <p>
         * If the map previously contained a mapping for the key, the old
         * value is replaced by the specified value.
         *
         * @param key The key with which the specified value is to be associated
         * @param value The value to be associated with the specified key
         * @return The previous value associated with {@code key}, or
         *         {@code null} if there was no mapping for {@code key}.
         *         (A {@code null} return can also indicate that the map
         *         previously associated {@code null} with {@code key},
         *         if the implementation supports {@code null} values.)
         * @throws OXException If operation fails
         */
        String put(String key, String value) throws OXException;

        /**
         * Removes the mapping for a key from this map if it is present
         * (optional operation).
         *
         * @param key The key whose mapping is to be removed from the map
         * @return The previous value associated with {@code key}, or
         *         {@code null} if there was no mapping for {@code key}.
         * @throws OXException If operation fails
         */
        String remove(String key) throws OXException;

        /**
         * If the specified key is not already associated with a value (or is mapped
         * to {@code null}) associates it with the given value and returns
         * {@code null}, else returns the current value.
         *
         * @param key The key with which the specified value is to be associated
         * @param value The value to be associated with the specified key
         * @param expireTimeMillis The specified expire time, in milliseconds, or less than/equal to <code>0</code> (zero)
         * @return The previous value associated with the specified key, or
         *         {@code null} if there was no mapping for the key.
         *         (A {@code null} return can also indicate that the map
         *         previously associated {@code null} with the key,
         *         if the implementation supports null values.)
         * @throws OXException If operation fails
         */
        String putIfAbsent(String key, String value) throws OXException;
    }

    private static class MapKeyStorageMap implements KeyStorageMap {

        private final Map<String, String> map;

        /**
         * Initializes a new {@link MapKeyStorageMap}.
         */
        MapKeyStorageMap(Map<String, String> map) {
            super();
            this.map = map;
        }

        @Override
        public String get(String key) throws OXException {
            return map.get(key);
        }

        @Override
        public String put(String key, String value) throws OXException {
            return map.put(key, value);
        }

        @Override
        public String remove(String key) throws OXException {
            return map.remove(key);
        }

        @Override
        public String putIfAbsent(String key, String value) throws OXException {
            return map.putIfAbsent(key, value);
        }
    }

    private static class ClusterMapKeyStorageMap implements KeyStorageMap {

        private final ClusterMap<String> map;

        /**
         * Initializes a new {@link MapKeyStorageMap}.
         */
        ClusterMapKeyStorageMap(ClusterMap<String> map) {
            super();
            this.map = map;
        }

        @Override
        public String get(String key) throws OXException {
            return map.get(key);
        }

        @Override
        public String put(String key, String value) throws OXException {
            return map.put(key, value);
        }

        @Override
        public String remove(String key) throws OXException {
            return map.remove(key);
        }

        @Override
        public String putIfAbsent(String key, String value) throws OXException {
            return map.putIfAbsent(key, value);
        }
    }

}
