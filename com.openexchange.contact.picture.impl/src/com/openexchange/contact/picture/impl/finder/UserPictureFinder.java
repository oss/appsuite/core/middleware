/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.contact.picture.impl.finder;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.contact.picture.PictureSearchData;
import com.openexchange.contact.picture.finder.ContactPictureFinder;
import com.openexchange.contact.picture.finder.PictureResult;
import com.openexchange.contact.picture.impl.ContactPictureUtil;
import com.openexchange.context.ContextService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.java.Strings;
import com.openexchange.session.Session;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link UserPictureFinder} - Checks if user exists and set the user information like contact id etc.
 * <pre>Note: This finder is unable to find pictures itself it only provides additional data. </pre>
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @since v7.10.1
 */
public class UserPictureFinder implements ContactPictureFinder {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserPictureFinder.class);

    private final UserService userService;

    private final ContextService contextService;

    /**
     * Initializes a new {@link UserPictureFinder}.
     *
     * @param userService The {@link UserService}
     * @param contextService The {@link ContextService}
     */
    public UserPictureFinder(UserService userService, ContextService contextService) {
        super();
        this.userService = userService;
        this.contextService = contextService;
    }

    @Override
    public int getRanking() {
        return 1000;
    }

    @Override
    public PictureResult getPicture(Session session, PictureSearchData data) {
        return provideUserData(session, data);
    }

    @Override
    public PictureResult getETag(Session session, PictureSearchData data) {
        return provideUserData(session, data);
    }

    @Override
    public PictureResult getLastModified(Session session, PictureSearchData data) {
        return provideUserData(session, data);
    }

    private PictureResult provideUserData(Session session, PictureSearchData data) {
        if (data.hasUser() && Strings.isEmpty(data.getAccountId())) {
            Optional<PictureResult> optResult = findByID(session, data);
            if (optResult.isPresent()) {
                return optResult.get();
            }
        }
        if (data.hasEmail()) {
            Optional<PictureResult> optResult = findByMail(session, data);
            if (optResult.isPresent()) {
                return optResult.get();
            }
        }
        return new PictureResult(data);
    }

    private Optional<PictureResult> findByID(Session session, PictureSearchData data) {
        try {
            User user = userService.getUser(i(data.getUserId()), session.getContextId());
            return optResult(user, session);
        } catch (OXException e) {
            LOGGER.debug("Unable to find user with identifier {} in context {}", data.getUserId(), I(session.getContextId()), e);
        }
        return Optional.empty();
    }

    private Optional<PictureResult> findByMail(Session session, PictureSearchData data) {
        try {
            Context context = contextService.getContext(session.getContextId());
            for (String email : data.getEmails()) {
                User user = userService.searchUser(email, context, true, true, false);
                Optional<PictureResult> optResult = optResult(user, session);
                if (optResult.isPresent()) {
                    return optResult;
                }
            }
        } catch (OXException e) {
            LOGGER.debug("Unable to find user with mail addresses {} in context {}", data.getEmails(), I(session.getContextId()), e);
        }
        return Optional.empty();
    }

    private Optional<PictureResult> optResult(User user, Session session) {
        if (null == user) {
            return Optional.empty();
        }
        if (false == ContactPictureUtil.hasGAB(session) && user.getId() != session.getUserId()) {
            // Neither GAB access nor the user querying its own data
            return Optional.empty();
        }
        // Enrich data, so that user can be found in contact module by its contact ID
        LinkedHashSet<String> set = new LinkedHashSet<>();
        set.add(user.getMail());
        if (null != user.getAliases()) {
            set.addAll(Arrays.asList(user.getAliases()));
        }
        return Optional.of(new PictureResult(new PictureSearchData(I(user.getId()),
                                                                   null,
                                                                   user.isGuest() ? Integer.toString(FolderObject.VIRTUAL_GUEST_CONTACT_FOLDER_ID) : Integer.toString(FolderObject.SYSTEM_LDAP_FOLDER_ID),
                                                                   Integer.toString(user.getContactId()),
                                                                   set)));
    }

}
