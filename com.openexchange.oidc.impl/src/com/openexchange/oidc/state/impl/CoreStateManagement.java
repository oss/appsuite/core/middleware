/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.oidc.state.impl;

import static com.openexchange.java.Autoboxing.L;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.annotation.NonNull;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider.ClusterMapServiceSupplier;
import com.openexchange.cluster.map.ClusterMap;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.cluster.map.RemoteSiteOptions;
import com.openexchange.cluster.map.codec.AbstractJSONMapCodec;
import com.openexchange.cluster.map.codec.MapCodec;
import com.openexchange.cluster.map.codec.MapCodecs;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.oidc.OIDCExceptionCode;
import com.openexchange.oidc.state.AuthenticationRequestInfo;
import com.openexchange.oidc.state.LogoutRequestInfo;
import com.openexchange.oidc.state.StateManagement;

/**
 * Contains and manages all current client states in {@link ClusterMap}
 *
 * @author <a href="mailto:vitali.sjablow@open-xchange.com">Vitali Sjablow</a>
 * @since v7.10.0
 */
public class CoreStateManagement implements StateManagement {

    private static final Logger LOG = LoggerFactory.getLogger(CoreStateManagement.class);

    private final BasicCoreClusterMapProvider<AuthenticationRequestInfo> authRequestMapProvider;
    private final BasicCoreClusterMapProvider<LogoutRequestInfo> logoutRequestMapProvider;
    private final BasicCoreClusterMapProvider<String> sessionInfoMapProvider;

    /**
     * Initializes a new {@link CoreStateManagement}.
     *
     * @param clusterMapService The cluster map service
     */
    public CoreStateManagement(ClusterMapService clusterMapService) {
        super();
        Objects.requireNonNull(clusterMapService, "ClusterMapService must not be null");
        RemoteSiteOptions options = RemoteSiteOptions.builder() //@formatter:off
            .withConsiderRemoteSites(true)
            .withAsyncRemoteSiteCalling(false)
            .build();
        ClusterMapServiceSupplier serviceSupplier = () -> clusterMapService;
        authRequestMapProvider = BasicCoreClusterMapProvider.<AuthenticationRequestInfo> builder()
            .withCoreMap(CoreMap.OIDC_AUTH_INFOS)
            .withCodec(createAuthInfoCodec())
            .withRemoteSiteOptions(options)
            .withServiceSupplier(serviceSupplier)
            .build();
        logoutRequestMapProvider = BasicCoreClusterMapProvider.<LogoutRequestInfo> builder()
            .withCoreMap(CoreMap.OIDC_LOGOUT_INFOS)
            .withCodec(createLogoutInfoCodec())
            .withRemoteSiteOptions(options)
            .withServiceSupplier(serviceSupplier)
            .build();
        sessionInfoMapProvider = BasicCoreClusterMapProvider.<String> builder()
            .withCoreMap(CoreMap.OIDC_SESSION_INFOS)
            .withCodec(MapCodecs.getStringCodec())
            .withRemoteSiteOptions(options)
            .withServiceSupplier(serviceSupplier)
            .build(); //@formatter:on
    }

    @Override
    public void addAuthenticationRequest(AuthenticationRequestInfo authenticationRequestInfo, long ttl, TimeUnit timeUnit) throws OXException {
        LOG.trace("addAuthenticationRequest(AuthenticationRequestInfo: {})", authenticationRequestInfo.getState());
        try {
            authRequestMapProvider.getMap().put(authenticationRequestInfo.getState(), DefaultAuthenticationRequestInfo.valueOf(authenticationRequestInfo), timeUnit.toMillis(ttl));
        } catch (RuntimeException e) {
            throw OIDCExceptionCode.CLUSTER_MAP_EXCEPTION.create(e, CoreMap.OIDC_AUTH_INFOS);
        }
    }

    @Override
    public AuthenticationRequestInfo getAndRemoveAuthenticationInfo(String state) throws OXException {
        LOG.trace("getAndRemoveAuthenticationInfo(state: {})", state);
        if (Strings.isEmpty(state)) {
            return null;
        }
        AuthenticationRequestInfo cached = null;
        try {
            cached = authRequestMapProvider.getMap().remove(state);
        } catch (RuntimeException e) {
            throw OIDCExceptionCode.CLUSTER_MAP_EXCEPTION.create(e, CoreMap.OIDC_AUTH_INFOS);
        }

        return cached;
    }

    @Override
    public void addLogoutRequest(LogoutRequestInfo logoutRequestInfo, long ttl, TimeUnit timeUnit) throws OXException {
        LOG.trace("addLogoutRequest({})", logoutRequestInfo.getState());
        try {
            logoutRequestMapProvider.getMap().put(logoutRequestInfo.getState(), DefaultLogoutRequestInfo.valueOf(logoutRequestInfo), timeUnit.toMillis(ttl));
        } catch (RuntimeException e) {
            throw OIDCExceptionCode.CLUSTER_MAP_EXCEPTION.create(e, CoreMap.OIDC_LOGOUT_INFOS);
        }
    }

    @Override
    public LogoutRequestInfo getAndRemoveLogoutRequestInfo(String state) throws OXException {
        LOG.trace("getAndRemoveLogoutRequestInfo(state: {})", state);
        LogoutRequestInfo cached = null;
        try {
            cached = logoutRequestMapProvider.getMap().remove(state);
        } catch (RuntimeException e) {
            throw OIDCExceptionCode.CLUSTER_MAP_EXCEPTION.create(e, CoreMap.OIDC_LOGOUT_INFOS);
        }
        return cached;
    }

    @Override
    public String getAndRemoveOXSessionId(String oidcSid) throws OXException {
        LOG.trace("getAndRemoveOXSessionId(oidcSid: {})", oidcSid);
        try {
            return sessionInfoMapProvider.getMap().remove(oidcSid);
        } catch (RuntimeException e) {
            throw OIDCExceptionCode.CLUSTER_MAP_EXCEPTION.create(e, CoreMap.OIDC_SESSION_INFOS);
        }
    }

    @Override
    public void rememberSession(String oidcSid, String oxSessionId, long ttl, TimeUnit timeUnit) throws OXException {
        LOG.trace("rememberSession(oidcSid: {}, oxSessionId: {}, ttl: {}, timeUnit: {})", oidcSid, oxSessionId, L(ttl), timeUnit);
        try {
            sessionInfoMapProvider.getMap().put(oidcSid, oxSessionId, ttl <= 0 ? -1 : timeUnit.toMillis(ttl));
        } catch (RuntimeException e) {
            throw OIDCExceptionCode.CLUSTER_MAP_EXCEPTION.create(e, CoreMap.OIDC_SESSION_INFOS);
        }
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    private static MapCodec<AuthenticationRequestInfo> createAuthInfoCodec() {
        return new AbstractJSONMapCodec<AuthenticationRequestInfo>() {

            @Override
            protected @NonNull JSONObject writeJson(AuthenticationRequestInfo value) throws Exception {
                JSONObject j = new JSONObject(6);
                j.putOpt("state", value.getState());
                j.putOpt("domainName", value.getDomainName());
                j.putOpt("deepLink", value.getDeepLink());
                j.putOpt("nonce", value.getNonce());
                j.putOpt("uiClientID", value.getUiClientID());
                Map<String, String> additionalClientInformation = value.getAdditionalClientInformation();
                if (additionalClientInformation != null && !additionalClientInformation.isEmpty()) {
                    JSONObject cis = new JSONObject(additionalClientInformation.size());
                    for (Map.Entry<String, String> entry : additionalClientInformation.entrySet()) {
                        cis.put(entry.getKey(), entry.getValue());
                    }
                    j.putOpt("additionalClientInformation", cis);
                }
                return j;
            }

            @Override
            protected @NonNull AuthenticationRequestInfo parseJson(JSONObject jObject) throws Exception {
                String state = jObject.optString("state", null);
                String domainName = jObject.optString("domainName", null);
                String deepLink = jObject.optString("deepLink", null);
                String nonce = jObject.optString("nonce", null);
                String uiClientID = jObject.optString("uiClientID", null);
                Map<String, String> additionalClientInformation = null;
                JSONObject cis = jObject.optJSONObject("additionalClientInformation");
                if (cis != null) {
                    additionalClientInformation = LinkedHashMap.newLinkedHashMap(cis.length());
                    for (Map.Entry<String, Object> entry : cis.entrySet()) {
                        additionalClientInformation.put(entry.getKey(), entry.getValue().toString());
                    }
                }
                return new DefaultAuthenticationRequestInfo(state, domainName, deepLink, nonce, additionalClientInformation, uiClientID);
            }
        };
    }

    private static MapCodec<LogoutRequestInfo> createLogoutInfoCodec() {

        return new AbstractJSONMapCodec<LogoutRequestInfo>() {

            @Override
            protected @NonNull JSONObject writeJson(LogoutRequestInfo value) throws Exception {
                JSONObject j = new JSONObject(6);
                j.putOpt("state", value.getState());
                j.putOpt("domainName", value.getDomainName());
                j.putOpt("sessionId", value.getSessionId());
                j.putOpt("requestURI", value.getRequestURI());
                return j;
            }

            @Override
            protected @NonNull LogoutRequestInfo parseJson(JSONObject jObject) throws Exception {
                String state = jObject.optString("state", null);
                String domainName = jObject.optString("domainName", null);
                String sessionId = jObject.optString("sessionId", null);
                String requestURI = jObject.optString("requestURI", null);
                return new DefaultLogoutRequestInfo(state, domainName, sessionId, requestURI);
            }
        };
    }

}
