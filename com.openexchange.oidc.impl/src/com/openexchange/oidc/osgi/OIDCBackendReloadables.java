/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.oidc.osgi;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.ForcedReloadable;
import com.openexchange.oidc.spi.AbstractOIDCBackend;

/**
 * {@link OIDCBackendReloadables} is a {@link ForcedReloadable} which reloads all
 * {@link AbstractOIDCBackend}s.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public final class OIDCBackendReloadables implements ForcedReloadable {

    private static final OIDCBackendReloadables INSTANCE = new OIDCBackendReloadables();

    /**
     * Gets the {@link OIDCBackendReloadables} instance
     *
     * @return The instance
     */
    public static OIDCBackendReloadables getInstance() {
        return INSTANCE;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** The collection of OIDC back-ends */
    private final Queue<WeakReference<AbstractOIDCBackend>> backends;

    /**
     * Initializes a new {@link OIDCBackendReloadables}.
     */
    private OIDCBackendReloadables() {
        super();
        backends = new ConcurrentLinkedQueue<>();
    }

    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        // Invoke reloadConfiguration() for accessible OIDC back-ends
        for (Iterator<WeakReference<AbstractOIDCBackend>> it = backends.iterator(); it.hasNext(); ) {
            AbstractOIDCBackend oidcBackend = it.next().get();
            if (oidcBackend == null) {
                // OIDC back-end finalized by garbage collector. Therefore, drop WeakReferences from collection.
                it.remove();
            } else {
                oidcBackend.reloadConfiguration(configService);
            }
        }
    }

    /**
     * Adds a specified OIDC back-end to this collection.
     *
     * @param backend The OIDC back-end to add
     */
    public void addOIDCBackend(AbstractOIDCBackend backend) {
        Objects.requireNonNull(backend);
        backends.add(new WeakReference<>(backend));
    }

}
