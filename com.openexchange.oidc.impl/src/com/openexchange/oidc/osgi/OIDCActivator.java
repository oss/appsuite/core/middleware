/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.oidc.osgi;

import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.http.HttpService;
import org.slf4j.Logger;
import com.openexchange.authentication.AuthenticationService;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.ForcedReloadable;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.database.DatabaseService;
import com.openexchange.dispatcher.DispatcherPrefixService;
import com.openexchange.groupware.upgrade.SegmentedUpdateService;
import com.openexchange.lock.LockService;
import com.openexchange.login.listener.LoginListener;
import com.openexchange.mail.api.AuthenticationFailedHandler;
import com.openexchange.net.ssl.SSLSocketFactoryProvider;
import com.openexchange.oidc.OIDCBackend;
import com.openexchange.oidc.http.outbound.OIDCHttpClientConfig;
import com.openexchange.oidc.impl.OIDCAuthenticationFailedHandler;
import com.openexchange.oidc.impl.OIDCConfigImpl;
import com.openexchange.oidc.impl.OIDCPasswordGrantAuthentication;
import com.openexchange.oidc.impl.OIDCSessionEventHandler;
import com.openexchange.oidc.impl.OIDCSessionInspectorService;
import com.openexchange.oidc.impl.OIDCSessionSsoProvider;
import com.openexchange.oidc.spi.OIDCCoreBackend;
import com.openexchange.oidc.state.StateManagement;
import com.openexchange.oidc.state.impl.CoreStateManagement;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.rest.client.httpclient.HttpClientService;
import com.openexchange.rest.client.httpclient.SpecificHttpClientConfigProvider;
import com.openexchange.server.ServiceLookup;
import com.openexchange.serverconfig.ServerConfigService;
import com.openexchange.session.SessionSsoProvider;
import com.openexchange.session.inspector.SessionInspectorService;
import com.openexchange.session.oauth.SessionOAuthTokenService;
import com.openexchange.session.reservation.SessionReservationService;
import com.openexchange.sessiond.SessiondEventConstants;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.user.UserService;
import com.openexchange.version.VersionService;

/**
 * Activates the OpenID feature.
 *
 * @author <a href="mailto:vitali.sjablow@open-xchange.com">Vitali Sjablow</a>
 * @since v7.10.0
 */
public class OIDCActivator extends HousekeepingActivator{

    private OIDCBackendRegistry oidcBackendRegistry;

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[]
        {
            LeanConfigurationService.class,
            ConfigurationService.class,
            HttpService.class,
            DispatcherPrefixService.class,
            ClusterMapService.class,
            SessionReservationService.class,
            ContextService.class,
            UserService.class,
            SessiondService.class,
            ServerConfigService.class,
            SessionOAuthTokenService.class,
            SegmentedUpdateService.class
        };
    }

    @Override
    protected boolean stopOnServiceUnavailability() {
        return true;
    }

    /**
     * Initializes the oidc backend registry
     *
     * @param services The service lookup
     * @param stateManagement The state management
     */
    private void initOIDCBackendRegistry(ServiceLookup services, StateManagement stateManagement) {
        if (this.oidcBackendRegistry == null) {
            this.oidcBackendRegistry = new OIDCBackendRegistry(context, services, stateManagement);
            this.oidcBackendRegistry.open();
        }
    }

    @Override
    protected synchronized void startBundle() throws Exception {
        Services.setServices(this);
        trackService(LockService.class);
        trackService(SSLSocketFactoryProvider.class);
        trackService(VersionService.class);
        trackService(HttpClientService.class);
        trackService(DatabaseService.class);
        openTrackers();

        registerService(ForcedReloadable.class, OIDCBackendReloadables.getInstance());

        // Initialize configuration for outbound HTTP traffic
        registerService(SpecificHttpClientConfigProvider.class, new OIDCHttpClientConfig());

        OIDCConfigImpl config = new OIDCConfigImpl(this);

        Logger logger = org.slf4j.LoggerFactory.getLogger(OIDCActivator.class);
        if (config.isEnabled()) {
            logger.info("Starting core OpenID Connect support... ");
            StateManagement stateManagement = new CoreStateManagement(getService(ClusterMapService.class));
            initOIDCBackendRegistry(this, stateManagement);
            SessionOAuthTokenService sessionOAuthTokenService = getServiceSafe(SessionOAuthTokenService.class);
            registerService(SessionInspectorService.class, new OIDCSessionInspectorService(oidcBackendRegistry, sessionOAuthTokenService, stateManagement));
            registerService(AuthenticationFailedHandler.class, new OIDCAuthenticationFailedHandler(oidcBackendRegistry, sessionOAuthTokenService, stateManagement, this), 100);
            registerService(EventHandler.class, new OIDCSessionEventHandler(oidcBackendRegistry), singletonDictionary(EventConstants.EVENT_TOPIC,
                new String[] { SessiondEventConstants.TOPIC_REMOVE_SESSION, SessiondEventConstants.TOPIC_REMOVE_CONTAINER }));
            if (config.isPasswordGrantEnabled()) {
                // @formatter:off
                OIDCPasswordGrantAuthentication passwordGrantAuth = new OIDCPasswordGrantAuthentication(
                    oidcBackendRegistry,
                    getServiceSafe(ServerConfigService.class),
                    getServiceSafe(LeanConfigurationService.class));
                // @formatter:on
                registerService(AuthenticationService.class, passwordGrantAuth);
                registerService(LoginListener.class, passwordGrantAuth);
                registerService(EventHandler.class, passwordGrantAuth, singletonDictionary(EventConstants.EVENT_TOPIC,
                    new String[] { SessiondEventConstants.TOPIC_REMOVE_SESSION, SessiondEventConstants.TOPIC_REMOVE_CONTAINER }));
            }
        } else {
            logger.info("OpenID Connect support is disabled by configuration. Skipping initialization...");
        }

        //register default oidc backend if configured
        if (config.startDefaultBackend()) {
            registerService(OIDCBackend.class, new OIDCCoreBackend());
        }
        registerService(SessionSsoProvider.class, new OIDCSessionSsoProvider());
    }

    @Override
    protected synchronized void stopBundle() throws Exception {
        OIDCBackendRegistry oidcBackends = this.oidcBackendRegistry;
        if (null != oidcBackends) {
            this.oidcBackendRegistry = null;
            oidcBackends.close();
        }
        HttpClientService httpClientService = getService(HttpClientService.class);
        if (httpClientService != null) {
            httpClientService.destroyHttpClient(OIDCHttpClientConfig.getClientIdOidc());
        }
        Services.setServices(null);
        super.stopBundle();
    }
}
