/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.multifactor.provider.webauthn.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.Duration;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import org.slf4j.Logger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.multifactor.Challenge;
import com.openexchange.multifactor.ChallengeAnswer;
import com.openexchange.multifactor.MultifactorDevice;
import com.openexchange.multifactor.MultifactorRequest;
import com.openexchange.multifactor.exceptions.MultifactorExceptionCodes;
import com.openexchange.multifactor.provider.u2f.impl.MultifactorU2FProperty;
import com.openexchange.multifactor.provider.u2f.impl.U2FMultifactorDevice;
import com.openexchange.multifactor.provider.u2f.storage.U2FMultifactorDeviceStorage;
import com.openexchange.multifactor.storage.MultifactorTokenStorage;
import com.openexchange.server.ServiceLookup;
import com.yubico.webauthn.AssertionRequest;
import com.yubico.webauthn.AssertionResult;
import com.yubico.webauthn.CredentialRepository;
import com.yubico.webauthn.FinishAssertionOptions;
import com.yubico.webauthn.RelyingParty;
import com.yubico.webauthn.StartAssertionOptions;
import com.yubico.webauthn.data.AuthenticatorAssertionResponse;
import com.yubico.webauthn.data.ByteArray;
import com.yubico.webauthn.data.ClientAssertionExtensionOutputs;
import com.yubico.webauthn.data.PublicKeyCredential;
import com.yubico.webauthn.data.RelyingPartyIdentity;
import com.yubico.webauthn.exception.AssertionFailedException;
import com.yubico.webauthn.extension.appid.AppId;
import com.yubico.webauthn.extension.appid.InvalidAppIdException;

/**
 * {@link U2FWebauthnLogin} Performs login using the old U2F devices
 * This is support for prior registered U2F devices and should be
 * removed in the future.
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public class U2FWebauthnLogin {

    private static final String PARAMETER_RESPONSE = "response";
    private static final String PARAMETER_CLIENT_DATA_JSON = "clientDataJSON";
    private static final String PARAMETER_CREDENTIALS_GET_JSON_PARAMETER = "credentialsGetJson";
    private static final String PARAMETER_CHALLENGE = "challenge";

    private final ServiceLookup services;
    private final MultifactorTokenStorage<U2FWebAuthnToken> tokenStorage;

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(U2FWebauthnLogin.class);
    }

    /**
     * 
     * Initializes a new {@link U2FWebauthnLogin}.
     *
     * @param services
     * @param tokenStorage
     */
    public U2FWebauthnLogin(ServiceLookup services, MultifactorTokenStorage<U2FWebAuthnToken> tokenStorage) {
        super();
        this.services = services;
        this.tokenStorage = tokenStorage;
    }

    /**
     * Internal method to obtain the {@link U2FMultifactorDeviceStorage}
     *
     * @return The u2f storage
     * @throws OXException
     */
    private U2FMultifactorDeviceStorage getU2FStorage() throws OXException {
        return services.getServiceSafe(U2FMultifactorDeviceStorage.class);
    }

    /**
     * Internal method to obtain the {@link LeanConfigurationService}
     *
     * @return The configuration service
     * @throws OXException
     */
    private LeanConfigurationService getConfigurationService() throws OXException {
        return services.getServiceSafe(LeanConfigurationService.class);
    }

    /**
     * Gets the U2F "App ID" for backwards compatibility to U2F
     *
     * @param multifactorRequest
     * @return The U2F "App ID" set in the configuration or the host name of the request as fallback
     * @throws OXException
     */
    private String getU2FAppId(MultifactorRequest multifactorRequest) throws OXException {
        final String appId = getConfigurationService().getProperty(multifactorRequest.getUserId(), multifactorRequest.getContextId(), MultifactorU2FProperty.appId);
        return Strings.isEmpty(appId) ? "https://" + multifactorRequest.getHost() : appId;
    }

    /**
     * Returns the configured lifetime of the token lifespan
     *
     * @param multifactorRequest
     * @return
     * @throws OXException
     */
    private Duration getTokenLifeTime(MultifactorRequest multifactorRequest) throws OXException {
        final int tokenLifetime = getConfigurationService().getIntProperty(multifactorRequest.getUserId(), multifactorRequest.getContextId(), MultifactorWebAuthnProperty.tokenLifetime);
        return Duration.ofSeconds(tokenLifetime);
    }

    /**
     * Get the configured replying party id
     *
     * @param multifactorRequest
     * @return
     * @throws OXException
     */
    private RelyingPartyIdentity getRelyingPartyIdentity(MultifactorRequest multifactorRequest) throws OXException {
        String id = getConfigurationService().getProperty(multifactorRequest.getUserId(), multifactorRequest.getContextId(), MultifactorWebAuthnProperty.rpId);
        if (Strings.isEmpty(id)) {
            String appId = getConfigurationService().getProperty(multifactorRequest.getUserId(), multifactorRequest.getContextId(), MultifactorU2FProperty.appId);
            if (!Strings.isEmpty(appId)) {
                try {
                    URL appIdUrl = new URI(appId).toURL();
                    if (appIdUrl.getPath().isEmpty()) {
                        id = appIdUrl.getHost();
                    }
                } catch (MalformedURLException | URISyntaxException e) {
                    LoggerHolder.LOG.error("AppId url malformed");
                }
            }
            if (Strings.isEmpty(id)) {
                id = multifactorRequest.getHost();
            }
        }
        return RelyingPartyIdentity.builder().id(id).name(id).build();
    }

    /**
     * Create the relying party
     *
     * @param multifactorRequest
     * @param credentialRepository
     * @return
     * @throws OXException
     */
    private RelyingParty createRelyingParty(MultifactorRequest multifactorRequest, CredentialRepository credentialRepository) throws OXException {
            try {
                RelyingParty relyingParty = RelyingParty.builder()
                                           .identity(getRelyingPartyIdentity(multifactorRequest))
                                           .credentialRepository(credentialRepository)
                                           .appId(new AppId(getU2FAppId(multifactorRequest))) 
                                           .build();
                return relyingParty;
            } catch (OXException | InvalidAppIdException e) {
                throw MultifactorExceptionCodes.ERROR_CREATING_FACTOR.create("Unable to create relyingparty");
            }
            
    }

    /**
     * Internal method to increment the U2F signature counter
     *
     * @param u2fdevice The device to increment the counter for
     * @param signatureCounter The new value of the counter
     * @param multifactorRequest The request to update the counter for
     * @throws OXException
     */
    private void incrementU2fSignatureCounter(U2FMultifactorDevice u2fdevice, long signatureCounter, MultifactorRequest multifactorRequest) throws OXException {
        if (!getU2FStorage().incrementCounter(multifactorRequest.getContextId(), multifactorRequest.getUserId(), u2fdevice.getId(), signatureCounter)) {
            LoggerHolder.LOG.error("Unable to increment U2F counter");
        }
    }

    /**
     * Internal method to return a {@link U2FMultifactorDevice} from the list of given devices by specifying a Credential ID / Key Handle
     *
     * @param credentialId The credential ID (i.e the U2F Key Handle) of the device to get from the list
     * @param devices The list of devices to the device from
     * @return An Optional {@link U2FMultifactorDevice} if found in the list of given devices
     */
    private Optional<U2FMultifactorDevice> getU2FDeviceByCredentialId(ByteArray credentialId, Collection<MultifactorDevice> devices) {
        return devices.stream()
            .filter(device -> device instanceof U2FMultifactorDevice &&
                              ((U2FMultifactorDevice) device).getKeyHandle().equals(credentialId.getBase64Url()))
            .map(device -> (U2FMultifactorDevice) device)
            .findFirst();
    }


    /**
     * Get the PublicKeyCredential from answer json
     *
     * @param answer Answer returned from device
     * @return
     * @throws IOException
     */
    private PublicKeyCredential<AuthenticatorAssertionResponse, ClientAssertionExtensionOutputs> parsePublicKeyCredentials(ChallengeAnswer answer) throws IOException {
        JSONObject answerJSON = new JSONObject(answer.asMap());
        PublicKeyCredential<AuthenticatorAssertionResponse, ClientAssertionExtensionOutputs> pkc = PublicKeyCredential.parseAssertionResponseJson(answerJSON.toString());
        return pkc;
    }

    /**
     * Internal method to return the challenge from the given {@link ChallengeAnswer} instance
     *
     * @param answer The answer to get the challenge from
     * @return The challenge, or null if no challenge was found in the given answer
     * @throws OXException
     * @throws JSONException
     */
    private static String getChallenge(ChallengeAnswer answer) throws OXException, JSONException {
        JSONObject jsonAnswer = new JSONObject(answer.asMap());
        if (jsonAnswer.hasAndNotNull(PARAMETER_RESPONSE)) {
            JSONObject response = jsonAnswer.getJSONObject(PARAMETER_RESPONSE);
            if (response.hasAndNotNull(PARAMETER_CLIENT_DATA_JSON)) {
                JSONObject clientDataJSON = JSONServices.parseObject(Base64.getUrlDecoder().decode(response.getString(PARAMETER_CLIENT_DATA_JSON)));
                if (clientDataJSON.hasAndNotNull(PARAMETER_CHALLENGE)) {
                    return clientDataJSON.getString(PARAMETER_CHALLENGE);
                }
                throw MultifactorExceptionCodes.MISSING_PARAMETER.create(PARAMETER_CHALLENGE);
            }
            throw MultifactorExceptionCodes.MISSING_PARAMETER.create(PARAMETER_CLIENT_DATA_JSON);
        }
        throw MultifactorExceptionCodes.MISSING_PARAMETER.create(PARAMETER_CLIENT_DATA_JSON);
    }

    /**
     * Begin the authentication process
     *
     * @param multifactorRequest
     * @param deviceId
     * @return Challenge to return to the device for signing
     * @throws OXException
     */
    public Challenge beginAuthentication(MultifactorRequest multifactorRequest, String deviceId) throws OXException {

        RelyingParty relyingParty = createRelyingParty(multifactorRequest, new U2FCredentialRepository(getU2FStorage(), multifactorRequest));

        AssertionRequest assertionRequest = relyingParty.startAssertion(StartAssertionOptions.builder()
            .username("")
            .build());

        //Store the token; we use the challenge as key
        String challenge = assertionRequest.getPublicKeyCredentialRequestOptions().getChallenge().getBase64Url();
        tokenStorage.add(multifactorRequest, challenge, new U2FWebAuthnToken(assertionRequest, getTokenLifeTime(multifactorRequest)));

        return new Challenge() {

            @Override
            public Map<String, Object> getChallenge() throws OXException {
                HashMap<String, Object> result = new HashMap<>(1);
                try {
                    // We need to add appId for U2F compatibility with the webauthn protocol
                    JSONObject json = JSONServices.parseObject(assertionRequest.toCredentialsGetJson());
                    JSONObject pk = json.getJSONObject("publicKey");
                    pk.remove("extensions");  // AppId not added in the way U2F seems to want.
                    JSONObject extensions = new JSONObject();
                    extensions.put("appid", getU2FAppId(multifactorRequest));
                    pk.put("extensions", extensions);
                    result.put(PARAMETER_CREDENTIALS_GET_JSON_PARAMETER, json);
                } catch (JsonProcessingException | JSONException e) {
                    throw MultifactorExceptionCodes.JSON_ERROR.create(e.getMessage());
                }
                return result;
            }

        };
    }

    /**
     * Do the authentication using the U2F device
     *
     * @param multifactorRequest
     * @param answer
     * @param userDevices
     * @throws OXException
     */
    public void doAuthentication(MultifactorRequest multifactorRequest, ChallengeAnswer answer, Collection<MultifactorDevice> userDevices) throws OXException {

        try {
            //Get the temporary token from the storage by using the provided challenge
            String challenge = getChallenge(answer);
            Optional<U2FWebAuthnToken> webAuthnToken = tokenStorage.getAndRemove(multifactorRequest, challenge);
            if (!webAuthnToken.isPresent()) {
                throw MultifactorExceptionCodes.AUTHENTICATION_FAILED.create();
            }

            AssertionRequest assertionRequest = webAuthnToken.get().getValue();
            PublicKeyCredential<AuthenticatorAssertionResponse, ClientAssertionExtensionOutputs> pkc = parsePublicKeyCredentials(answer);
            RelyingParty relyingParty = createRelyingParty(multifactorRequest, new U2FCredentialRepository(getU2FStorage(), multifactorRequest));
            //@formatter:off
            AssertionResult result = relyingParty.finishAssertion(FinishAssertionOptions.builder()
                                                               .request(assertionRequest)
                                                               .response(pkc)
                                                               .build());
            //Increment the signature counter after use
            Optional<U2FMultifactorDevice> usedU2fDevice = getU2FDeviceByCredentialId(result.getCredentialId(), userDevices);
            if (usedU2fDevice.isPresent()) {
                //The device used was a U2F device
                incrementU2fSignatureCounter(usedU2fDevice.get(), result.getSignatureCount(), multifactorRequest);
            } else {
                throw MultifactorExceptionCodes.UNKNOWN_DEVICE_ID.create();
            }

        } catch (AssertionFailedException e) {
            LoggerHolder.LOG.info(e.getMessage());
            throw MultifactorExceptionCodes.AUTHENTICATION_FAILED.create();
        } catch (@SuppressWarnings("unused") IOException e) {
            throw MultifactorExceptionCodes.UNKNOWN_ERROR.create("Failed to verify WebAuthn signature.");
        } catch (@SuppressWarnings("unused") RuntimeException e) {
            //Seems like the library throws a RuntimeException in some cases
            throw MultifactorExceptionCodes.UNKNOWN_ERROR.create("Failed to verify WebAuthn signature.");
        } catch (JSONException e) {
            throw MultifactorExceptionCodes.JSON_ERROR.create(e.getMessage());
        }
    }

}
