
package com.openexchange.multifactor.provider.webauthn.impl;

import java.time.Duration;
import com.openexchange.multifactor.MultifactorToken;
import com.yubico.webauthn.AssertionRequest;

/**
 * {@link U2FWebAuthnToken} - Represents the temporary Challenge/Assertion token for WebAuthn authentication
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.6
 */
public class U2FWebAuthnToken extends MultifactorToken<AssertionRequest> {

    /**
     * Initializes a new {@link U2FWebAuthnToken}.
     *
     * @param value The {@link AssertionRequest}
     * @param lifeTime The token lifetime
     */
    public U2FWebAuthnToken(AssertionRequest value, Duration lifeTime) {
        super(value, lifeTime);
    }
}
