
package com.openexchange.multifactor.provider.webauthn.impl;

import static com.openexchange.java.Autoboxing.b;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.i18n.tools.StringHelper;
import com.openexchange.java.Strings;
import com.openexchange.multifactor.Challenge;
import com.openexchange.multifactor.ChallengeAnswer;
import com.openexchange.multifactor.MultifactorDevice;
import com.openexchange.multifactor.MultifactorProvider;
import com.openexchange.multifactor.MultifactorRequest;
import com.openexchange.multifactor.RegistrationChallenge;
import com.openexchange.multifactor.exceptions.MultifactorExceptionCodes;
import com.openexchange.multifactor.provider.u2f.impl.MultifactorU2FProperty;
import com.openexchange.multifactor.provider.u2f.storage.U2FMultifactorDeviceStorage;
import com.openexchange.multifactor.storage.MultifactorTokenStorage;
import com.openexchange.multifactor.util.DeviceNaming;
import com.openexchange.server.ServiceLookup;
import com.openexchange.webauthn.WebAuthnDevice;
import com.openexchange.webauthn.WebAuthnService;
import com.openexchange.webauthn.storage.WebAuthnStorageService;

/**
 * {@link MultifactorWebAuthnProvider}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.6
 */
public class MultifactorWebAuthnProvider implements MultifactorProvider {

    public static final String NAME = "WEB_AUTHN";

    private static final Logger LOG = LoggerFactory.getLogger(MultifactorWebAuthnProvider.class);

    private final ServiceLookup services;

    /**
     * Initializes a new {@link MultifactorWebAuthnProvider}.
     *
     * @param configurationService The {@link LeanConfigurationService} to use
     * @param u2fStorage The {@link U2FMultifactorDeviceStorage} to use for backwards compatibility
     * @param tokenStorage The {@link MultifactorTokenStorage} storage to use for storing challenges
     */
    public MultifactorWebAuthnProvider(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Internal method to obtain the {@link LeanConfigurationService}
     *
     * @return The configuration service
     * @throws OXException
     */
    private LeanConfigurationService getConfigurationService() throws OXException {
        return services.getServiceSafe(LeanConfigurationService.class);
    }

    /**
     * Internal method to obtain the webauthn service
     *
     * @return
     * @throws OXException
     */
    private WebAuthnService getWebAuthnService() throws OXException {
        return services.getServiceSafe(WebAuthnService.class);
    }

    /**
     * Internal method to obtain the {@link U2FMultifactorDeviceStorage}
     *
     * @return The u2f storage
     * @throws OXException
     */
    private U2FMultifactorDeviceStorage getU2FStorage() {
        return services.getService(U2FMultifactorDeviceStorage.class);
    }

    /**
     * Internal method to obtain the WebAuthn Storage service
     *
     * @return
     * @throws OXException
     */
    private WebAuthnStorageService getWebauthnStorage() throws OXException {
        return services.getServiceSafe(WebAuthnStorageService.class);
    }

    /**
     * Parse out the domain of a url
     *
     * @param url Url to parse
     * @return String of the domain
     */
    private String getDomain(String url) {
        try {
            URL appIdUrl = new URI(url).toURL();
            if (appIdUrl.getPath().isEmpty()) {
                return appIdUrl.getHost();
            }
        } catch (MalformedURLException | URISyntaxException e) {
            LOG.info("AppId url malformed");
        }
        return null;
    }

    /**
     * Gets the WebAuthn Identity
     *
     * @param multifactorRequest The request to create the identity for
     * @return The WebAuthn identity set in the configuration, or the host name of the request as fallback
     * @throws OXException
     */
    private String getRelyingPartyIdentity(MultifactorRequest multifactorRequest) throws OXException {
        String id = getConfigurationService().getProperty(multifactorRequest.getUserId(), multifactorRequest.getContextId(), MultifactorWebAuthnProperty.rpId);
        if (!Strings.isEmpty(id)) {
            id = getDomain(id);
        } else {  // Check U2F
            String appId = getConfigurationService().getProperty(multifactorRequest.getUserId(), multifactorRequest.getContextId(), MultifactorU2FProperty.appId);
            if (!Strings.isEmpty(appId)) {
                id = getDomain(appId);
            }
        }
        if (Strings.isEmpty(id)) {
            id = multifactorRequest.getHost();
        }
        return id;
    }

    /**
     * Gets a list of {@link MultifactorDevices}s for the given request being able to use with the WebAuthn provider
     *
     * @param multifactorRequest The request to get the devices for
     * @return A collection of devices for the given request which can be used with WebAuthn
     * @throws OXException
     */
    private Collection<MultifactorDevice> getCompatibleDevices(MultifactorRequest multifactorRequest) throws OXException {
        Collection<MultifactorDevice> ret = new ArrayList<>();
        //By now we are only checking for u2f devices since WebAuthn is not fully supported yet
        final U2FMultifactorDeviceStorage u2fStorage = getU2FStorage();
        if (u2fStorage != null) {
            ret.addAll(u2fStorage.getDevices(multifactorRequest.getContextId(), multifactorRequest.getUserId()));
        }
        final WebAuthnStorageService storage = getWebauthnStorage();
        Collection<WebAuthnDevice> devices = storage.getDevices(multifactorRequest.getContextId(), multifactorRequest.getUserId());
        if (devices.isEmpty()) {
            return ret;
        }
        devices.forEach((d) -> ret.add(new WebAuthnMultifactorDevice(d)));
        return ret;
    }


    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public boolean isEnabled(MultifactorRequest multifactorRequest) {
        try {
            LeanConfigurationService configurationService = getConfigurationService();
            boolean webAuthnEnabled = configurationService.getBooleanProperty(multifactorRequest.getUserId(), multifactorRequest.getContextId(), MultifactorWebAuthnProperty.enabled);
            if (!webAuthnEnabled) {
                //If WebAuthn is not enabled by configuration, we still enabled it if U2F is enabled.
                //This allows backwards compatibility. I.e a U2f user can login via webauthn
                return configurationService.getBooleanProperty(multifactorRequest.getUserId(), multifactorRequest.getContextId(), MultifactorU2FProperty.enabled);
            }
            return webAuthnEnabled;
        } catch (OXException e) {
            LOG.error(e.getMessage());
            return false;
        }
    }

    /*
     * Return count of devices registered to the user
     *
     * @param multifactorRequest The request
     * 
     * @return The count of registered devices for the given request
     * 
     * @throws OXException
     */
   private int getCount(MultifactorRequest multifactorRequest) throws OXException {
       return getWebauthnStorage().getCount(multifactorRequest.getContextId(), multifactorRequest.getUserId());
   }
    
    /**
     * Internal method to get the translated, default name for the device
     *
     * @param multifactorRequest The {@link MultifactorRequest}
     * @return A translated default name for devices
     * @throws OXException
     */
    private String getDefaultName(MultifactorRequest multifactorRequest) throws OXException {
        final int count = getCount(multifactorRequest) + 1;
        final String name = StringHelper.valueOf(multifactorRequest.getLocale()).getString(WebauthnStrings.AUTHENTICATION_NAME);
        return String.format(name, Integer.valueOf(count));
    }

    @Override
    public Collection<? extends MultifactorDevice> getDevices(MultifactorRequest multifactorRequest) throws OXException {
        Collection<MultifactorDevice> ret = new ArrayList<>();
        final WebAuthnStorageService storage = getWebauthnStorage();
        Collection<WebAuthnDevice> devices = storage.getDevices(multifactorRequest.getContextId(), multifactorRequest.getUserId());
        if (devices.isEmpty()) {
            return ret;
        }
        devices.forEach((d) -> ret.add(new WebAuthnMultifactorDevice(d)));
        return ret;
    }

    @Override
    public Collection<? extends MultifactorDevice> getEnabledDevices(MultifactorRequest multifactorRequest) throws OXException {
        return getDevices(multifactorRequest).stream().filter((d) -> b(d.isEnabled())).collect(Collectors.toCollection(HashSet::new));
    }

    @Override
    public Optional<? extends MultifactorDevice> getDevice(MultifactorRequest multifactorRequest, String deviceId) throws OXException {
        final WebAuthnStorageService storage = getWebauthnStorage();
        Optional<WebAuthnDevice> dev = storage.getDevice(multifactorRequest.getContextId(), multifactorRequest.getUserId(), deviceId);
        if (dev.isEmpty())
            return Optional.empty();
        return Optional.of(new WebAuthnMultifactorDevice(dev.get()));
    }

    @Override
    public RegistrationChallenge startRegistration(MultifactorRequest multifactorRequest, MultifactorDevice device) throws OXException {
        //Gets the device name from the request if present, default otherwise
        DeviceNaming.applyName(device, () -> getDefaultName(multifactorRequest));

        WebAuthnService wService = getWebAuthnService();
        
        return wService.startRegistration(multifactorRequest.getContextId(),
            multifactorRequest.getUserId(),
            multifactorRequest.getLogin(),
            device.getName(),
            getRelyingPartyIdentity(multifactorRequest),
            true);
    }

    @Override
    public MultifactorDevice finishRegistration(MultifactorRequest multifactorRequest, String deviceId, ChallengeAnswer answer) throws OXException {
        WebAuthnService wService = getWebAuthnService();
        WebAuthnDevice dev = wService.finishRegistration(multifactorRequest.getContextId(),
            multifactorRequest.getUserId(), deviceId, answer, true);
        WebAuthnMultifactorDevice wdev = new WebAuthnMultifactorDevice(dev);
        return wdev;
    }

    @Override
    public void deleteRegistration(MultifactorRequest multifactorRequest, String deviceId) throws OXException {
        getWebauthnStorage().unregisterDevice(multifactorRequest.getContextId(), multifactorRequest.getUserId(), deviceId);
    }

    @Override
    public boolean deleteRegistrations(int contextId, int userId) throws OXException {
        return getWebauthnStorage().deleteAllForUser(contextId, userId);
    }

    @Override
    public boolean deleteRegistrations(int contextId) throws OXException {
        return getWebauthnStorage().deleteAllForContext(contextId);
    }

    @Override
    public Challenge beginAuthentication(MultifactorRequest multifactorRequest, String deviceId) throws OXException {
        Collection<MultifactorDevice> devices = getCompatibleDevices(multifactorRequest);
        if (devices.isEmpty()) {
            throw MultifactorExceptionCodes.NO_DEVICES.create();
        }
        boolean isU2f = devices.stream().allMatch(d -> d.getProviderName().equals("U2F"));
        if (isU2f) {
            U2FWebauthnLogin u2f = services.getService(U2FWebauthnLogin.class);
            if (u2f != null) {
                return u2f.beginAuthentication(multifactorRequest, deviceId);
            }
            throw MultifactorExceptionCodes.UNKNOWN_PROVIDER.create();
        }
        WebAuthnService wService = getWebAuthnService();
        
        return wService.beginAuthentication(multifactorRequest.getContextId(),
            multifactorRequest.getUserId(),
            multifactorRequest.getLogin(),
            getRelyingPartyIdentity(multifactorRequest));

    }

    @Override
    public void doAuthentication(MultifactorRequest multifactorRequest, String deviceId, ChallengeAnswer answer) throws OXException {
        Collection<MultifactorDevice> devices = getCompatibleDevices(multifactorRequest);
        if (devices == null || devices.isEmpty()) {
            throw MultifactorExceptionCodes.NO_DEVICES.create();
        }
        boolean isU2f = devices.stream().allMatch(d -> d.getProviderName().equals("U2F"));
        if (isU2f) {
            U2FWebauthnLogin u2f = services.getService(U2FWebauthnLogin.class);
            if (u2f != null) {
                u2f.doAuthentication(multifactorRequest, answer, devices);
                return;
            }
            throw MultifactorExceptionCodes.UNKNOWN_PROVIDER.create();
        }
        getWebAuthnService().doAuthentication(multifactorRequest.getContextId(),
            multifactorRequest.getUserId(),
            getRelyingPartyIdentity(multifactorRequest),
            answer);
    }

    @Override
    public MultifactorDevice renameDevice(MultifactorRequest multifactorRequest, MultifactorDevice device) throws OXException {
        getWebAuthnService().renameDevice(multifactorRequest.getContextId(), multifactorRequest.getUserId(), device.getId(), device.getName());
        return device;
    }
}
