
package com.openexchange.multifactor.provider.webauthn.impl;

import com.openexchange.multifactor.AbstractMultifactorDevice;
import com.openexchange.webauthn.WebAuthnDevice;

/**
 * 
 * {@link WebAuthnMultifactorDevice} Returns the required information for a
 * webauthn device for multifactor user interface
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public class WebAuthnMultifactorDevice extends AbstractMultifactorDevice {

    private String keyId;           // Public key Id

    /**
     * Initializes a new {@link WebAuthnMultifactorDevice}.
     *
     * @param id Device Id
     * @param name Name of the device
     */
    protected WebAuthnMultifactorDevice(String id, String name) {
        super(id, MultifactorWebAuthnProvider.NAME, name);
        this.keyId = null;
    }

    /**
     * Initializes a new {@link WebAuthnMultifactorDevice} from WebAuthn Device
     *
     * @param dev Device to use for initialization
     */
    protected WebAuthnMultifactorDevice(WebAuthnDevice dev) {
        super(dev.getDeviceId(), MultifactorWebAuthnProvider.NAME, dev.getName());
        this.enable(Boolean.TRUE);
        this.keyId = dev.getKeyId();
    }

    /**
     * Return the key handle
     *
     * @return
     */
    public String getKeyHandle() {
        return this.keyId;
    }




}
