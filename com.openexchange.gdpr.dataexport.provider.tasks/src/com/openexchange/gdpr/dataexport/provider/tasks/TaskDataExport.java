/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.gdpr.dataexport.provider.tasks;

import static com.openexchange.gdpr.dataexport.DataExportProviders.getBoolOption;
import static com.openexchange.gdpr.dataexport.DataExportProviders.isPermissionDenied;
import static com.openexchange.gdpr.dataexport.DataExportProviders.isRetryableExceptionAndMayFail;
import static com.openexchange.gdpr.dataexport.provider.general.AttachmentLoader.PROPERTY_BINARY_ATTACHMENTS;
import static com.openexchange.gdpr.dataexport.provider.general.AttachmentLoader.loadAttachmentBinaries;
import static com.openexchange.gdpr.dataexport.provider.general.SavePointAndReason.savePointFor;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Strings.parsePositiveInt;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.container.ThresholdFileHolder;
import com.openexchange.ajax.fileholder.IFileHolder;
import com.openexchange.api2.TasksSQLInterface;
import com.openexchange.context.ContextService;
import com.openexchange.data.conversion.ical.ConversionError;
import com.openexchange.data.conversion.ical.ConversionWarning;
import com.openexchange.data.conversion.ical.ICalEmitter;
import com.openexchange.data.conversion.ical.ICalSession;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.ContentType;
import com.openexchange.folderstorage.FolderService;
import com.openexchange.folderstorage.FolderServiceDecorator;
import com.openexchange.folderstorage.FolderStorage;
import com.openexchange.folderstorage.Type;
import com.openexchange.folderstorage.UserizedFolder;
import com.openexchange.folderstorage.type.PrivateType;
import com.openexchange.folderstorage.type.PublicType;
import com.openexchange.folderstorage.type.SharedType;
import com.openexchange.gdpr.dataexport.DataExportAbortedException;
import com.openexchange.gdpr.dataexport.DataExportExceptionCode;
import com.openexchange.gdpr.dataexport.DataExportSink;
import com.openexchange.gdpr.dataexport.DataExportTask;
import com.openexchange.gdpr.dataexport.Directory;
import com.openexchange.gdpr.dataexport.ExportResult;
import com.openexchange.gdpr.dataexport.Item;
import com.openexchange.gdpr.dataexport.Message;
import com.openexchange.gdpr.dataexport.Module;
import com.openexchange.gdpr.dataexport.provider.general.AbstractDataExportProviderTask;
import com.openexchange.gdpr.dataexport.provider.general.SavePointAndReason;
import com.openexchange.groupware.container.DataObject;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.i18n.FolderStrings;
import com.openexchange.groupware.tasks.Task;
import com.openexchange.groupware.tasks.TasksSQLImpl;
import com.openexchange.i18n.Translator;
import com.openexchange.i18n.TranslatorFactory;
import com.openexchange.i18n.tools.StringHelper;
import com.openexchange.java.Streams;
import com.openexchange.java.Strings;
import com.openexchange.java.util.UUIDs;
import com.openexchange.notification.service.FullNameBuilderService;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.GeneratedSession;
import com.openexchange.session.Session;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.tools.TimeZoneUtils;
import com.openexchange.tools.iterator.SearchIterator;
import com.openexchange.tools.iterator.SearchIterators;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link TaskDataExport}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.3
 */
public class TaskDataExport extends AbstractDataExportProviderTask {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(TaskDataExport.class);

    private static final String JSON_FIELD_ID = "id";
    private static final String JSON_FIELD_ROOT = "root";
    private static final String JSON_FIELD_PATH = "path";
    private static final String JSON_FIELD_FOLDER = "folder";

    private static final int PRIVATE_ID = FolderObject.SYSTEM_PRIVATE_FOLDER_ID;
    private static final int PUBLIC_ID = FolderObject.SYSTEM_PUBLIC_FOLDER_ID;
    private static final int SHARED_ID = FolderObject.SYSTEM_SHARED_FOLDER_ID;
    private static final String ID_TASKS = "tasks";

    private StartInfo startInfo;

    /**
     * Initializes a new {@link TaskDataExport}.
     *
     * @param sink The sink to output to
     * @param savepoint The optional save-point previously set by this provider
     * @param task The data export task providing needed arguments
     * @param locale The locale
     * @param services The service look-up
     */
    public TaskDataExport(DataExportSink sink, Optional<JSONObject> savepoint, DataExportTask task, Locale locale, ServiceLookup services) {
        super(ID_TASKS, sink, savepoint, task, locale, services);
    }

    /**
     * Exports tasks.
     *
     * @return The export result
     * @throws OXException If export fails
     */
    @Override
    public ExportResult export() throws OXException {
        ICalEmitter icalEmitter = services.getServiceSafe(ICalEmitter.class);
        FolderService folderService = services.getServiceSafe(FolderService.class);
        UserService userService = services.getServiceSafe(UserService.class);
        ContextService contextService = services.getServiceSafe(ContextService.class);
        ThreadPoolService threadPool = services.getServiceSafe(ThreadPoolService.class);

        try {
            Session session = new GeneratedSession(task.getUserId(), task.getContextId());

            NeededServices neededServices = new NeededServices(folderService, icalEmitter, contextService, threadPool);

            Module tasksModule = getModule();
            boolean includePublicFolders = getBoolOption(TasksDataExportPropertyNames.PROP_INCLUDE_PUBLIC_FOLDERS, false, tasksModule);
            boolean includeSharedFolders = getBoolOption(TasksDataExportPropertyNames.PROP_INCLUDE_SHARED_FOLDERS, false, tasksModule);
            Options options = new Options(includePublicFolders, includeSharedFolders);

            if (savepoint.isPresent()) {
                JSONObject jSavePoint = savepoint.get();
                int taskId = jSavePoint.optInt(JSON_FIELD_ID, 0);
                startInfo = new StartInfo(taskId > 0 ? I(taskId) : null, jSavePoint.getInt(JSON_FIELD_FOLDER), jSavePoint.optString(JSON_FIELD_PATH, null), jSavePoint.getInt(JSON_FIELD_ROOT));
            } else {
                startInfo = null;
            }

            User user = userService.getUser(task.getUserId(), task.getContextId());
            TimeZone timeZone = TimeZoneUtils.getTimeZone(user.getTimeZone());
            ContentType contentType = folderService.parseContentType(ID_TASKS);
            DecoratorProvider decoratorProvider = new DecoratorProvider(locale, timeZone, contentType);

            Locale locale = this.locale;
            StringHelper helper = StringHelper.valueOf(locale);

            tryTouch();

            // Private
            if (startInfo == null || PRIVATE_ID == startInfo.root) {
                Folder folder = new Folder(PRIVATE_ID, helper.getString(FolderStrings.SYSTEM_PRIVATE_FOLDER_NAME), true);
                SavePointAndReason optSavePoint = traverseFolder(PRIVATE_ID, PrivateType.getInstance(), folder, null, options, decoratorProvider, session, neededServices);
                if (optSavePoint != null) {
                    return optSavePoint.result();
                }

                if (startInfo != null) {
                    startInfo = null;
                }
            }

            // Shared
            if (options.includeSharedFolders && (startInfo == null || SHARED_ID == startInfo.root)) {
                Folder folder = new Folder(SHARED_ID, helper.getString(FolderStrings.SYSTEM_SHARED_FOLDER_NAME), true);
                SavePointAndReason optSavePoint = traverseFolder(SHARED_ID, SharedType.getInstance(), folder, null, options, decoratorProvider, session, neededServices);
                if (optSavePoint != null) {
                    return optSavePoint.result();
                }

                if (startInfo != null) {
                    startInfo = null;
                }
            }

            // Public
            if (options.includePublicFolders && (startInfo == null || PUBLIC_ID == startInfo.root)) {
                Folder folder = new Folder(PUBLIC_ID, helper.getString(FolderStrings.SYSTEM_PUBLIC_FOLDER_NAME), true);
                SavePointAndReason optSavePoint = traverseFolder(PUBLIC_ID, PublicType.getInstance(), folder, null, options, decoratorProvider, session, neededServices);
                if (optSavePoint != null) {
                    return optSavePoint.result();
                }

                if (startInfo != null) {
                    startInfo = null;
                }
            }

            tryTouch();
            return ExportResult.completed();
        } catch (OXException e) {
            if (isRetryableExceptionAndMayFail(e, sink)) {
                return ExportResult.incomplete(savepoint, Optional.of(e));
            }
            throw e;
        } catch (JSONException e) {
            throw DataExportExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        } catch (DataExportAbortedException e) {
            return ExportResult.aborted();
        } catch (Exception e) {
            if (isRetryableExceptionAndMayFail(e, sink)) {
                return ExportResult.incomplete(savepoint, Optional.of(e));
            }
            throw OXException.general(e.getMessage(), e);
        }
    }

    private SavePointAndReason traverseFolder(int root, Type type, Folder folder, String parentPath, Options options, DecoratorProvider decoratorProvider, Session session, NeededServices neededServices) throws OXException, DataExportAbortedException {
        if (isPauseRequested()) {
            if (startInfo != null) {
                JSONObject jSavePoint = new JSONObject(4).putSafe(JSON_FIELD_FOLDER, startInfo.folderId).putSafe(JSON_FIELD_ROOT, I(root));
                if (startInfo.path != null) {
                    jSavePoint.putSafe(JSON_FIELD_PATH, startInfo.path);
                }
                if (startInfo.taskId != null) {
                    jSavePoint.putSafe(JSON_FIELD_ID, startInfo.taskId);
                }
                return savePointFor(jSavePoint);
            }

            return savePointFor(new JSONObject(2).putSafe(JSON_FIELD_FOLDER, folder.getFolderId()).putSafe(JSON_FIELD_ROOT, I(root)));
        }
        checkAborted();

        String pathOfFolder = null;
        StartInfo info = startInfo;
        if (info == null || info.folderId.equals(folder.getFolderId())) {
            if (info == null || info.path == null) {
                pathOfFolder = exportFolderInternal(folder, parentPath);
                if (pathOfFolder == null) {
                    return savePointFor(new JSONObject(2).putSafe(JSON_FIELD_FOLDER, folder.getFolderId()).putSafe(JSON_FIELD_ROOT, I(root)));
                }
                LOG.debug("Exported task directory {} for data export {} of user {} in context {}", folder.getName(), UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()));
            } else {
                pathOfFolder = info.path;
            }

            if (!folder.isRootFolder()) {
                SavePointAndReason jSavePoint = exportTasks(root, folder, pathOfFolder, info == null ? null : info.taskId, session, neededServices);
                if (jSavePoint != null) {
                    return jSavePoint;
                }
            }

            info = null;
            startInfo = null;
        }

        if (folder.isRootFolder()) {
            List<Folder> children;
            try {
                FolderService folderService = neededServices.folderService;
                UserizedFolder[] visibleFolders = folderService.getVisibleFolders(FolderStorage.REAL_TREE_ID, decoratorProvider.contentType, type, true, session, decoratorProvider.createDecorator()).getResponse();
                if (null == visibleFolders || visibleFolders.length <= 0) {
                    children = Collections.emptyList();
                } else {
                    Translator translator = null;
                    if (SharedType.getInstance().equals(type)) {
                        translator = services.getServiceSafe(TranslatorFactory.class).translatorFor(locale);
                    }

                    children = new ArrayList<>(visibleFolders.length);
                    for (UserizedFolder subfolder : visibleFolders) {
                        if (subfolder.getID().equals(folder.getFolderId())) {
                            // Skip parent folder to prevent endless recursion
                            continue;
                        }
                        String namePrefix = null;
                        if (translator != null) {
                            FullNameBuilderService fullNameBuilder = services.getServiceSafe(FullNameBuilderService.class);
                            namePrefix = generateFullNamePrefix(fullNameBuilder.buildFullName(subfolder.getCreatedBy(), task.getContextId(), translator));
                        }
                        children.add(new Folder(subfolder.getID(), namePrefix == null ? subfolder.getName(locale) : namePrefix + subfolder.getName(locale), false));
                    }
                }
            } catch (Exception e) {
                if (isRetryableExceptionAndMayFail(e, sink)) {
                    if (info != null) {
                        JSONObject jSavePoint = new JSONObject(4).putSafe(JSON_FIELD_FOLDER, info.folderId).putSafe(JSON_FIELD_ROOT, I(root));
                        if (startInfo.path != null) {
                            jSavePoint.putSafe(JSON_FIELD_PATH, startInfo.path);
                        }
                        if (info.taskId != null) {
                            jSavePoint.putSafe(JSON_FIELD_ID, info.taskId);
                        }
                        return savePointFor(jSavePoint, e);
                    }

                    return savePointFor(new JSONObject(2).putSafe(JSON_FIELD_FOLDER, folder.getFolderId()).putSafe(JSON_FIELD_ROOT, I(root)), e);
                }
                LOG.warn("Failed to retrieve subfolders of folder \"{}\" from user {} in context {}", folder.getName(), I(task.getUserId()), I(task.getContextId()), e);
                sink.addToReport(Message.builder().appendToMessage("Failed to retrieve subfolders of folder \"").appendToMessage(folder.getName()).appendToMessage("\": ").appendToMessage(e.getMessage()).withModuleId(ID_TASKS).withTimeStamp(new Date()).build());
                children = Collections.emptyList();
            }

            if (!children.isEmpty()) {
                if (pathOfFolder == null) {
                    pathOfFolder = info == null || info.path == null ? (parentPath == null ? "" : parentPath) + sanitizeNameForZipEntry(folder.getName()) + "/" : info.path;
                }
                for (Folder child : children) {
                    SavePointAndReason jSavePoint = traverseFolder(root, type, child, pathOfFolder, options, decoratorProvider, session, neededServices);
                    if (jSavePoint != null) {
                        return jSavePoint;
                    }
                }
            }
        }

        return null;
    }

    private String exportFolderInternal(Folder folderInfo, String path) throws OXException {
        return sink.export(new Directory(path, sanitizeNameForZipEntry(folderInfo.getName())));
    }

    private static final Date DATE_ZERO = new Date(0); // NOSONARLINT

    private static final int[] FIELDS_ID = new int[] { DataObject.OBJECT_ID };

    private static final Comparator<Task> COMPARATOR = (task1, task2) -> Integer.compare(task1.getObjectID(), task2.getObjectID());

    private SavePointAndReason exportTasks(int root, Folder folder, String path, Integer startTaskId, Session session, NeededServices neededServices) throws OXException, DataExportAbortedException {
        if (isPauseRequested()) {
            JSONObject jSavePoint = new JSONObject(4).putSafe(JSON_FIELD_FOLDER, folder.getFolderId()).putSafe(JSON_FIELD_PATH, path).putSafe(JSON_FIELD_ROOT, I(root));
            if (startTaskId != null) {
                jSavePoint.putSafe(JSON_FIELD_ID, startTaskId);
            }
            return savePointFor(jSavePoint);
        }
        checkAborted();

        ICalEmitter icalEmitter = neededServices.icalEmitter;
        TasksSQLInterface tasksSql = new TasksSQLImpl(session);
        Context context = neededServices.contextService.getContext(this.task.getContextId());
        ThreadPoolService threadPool = neededServices.threadPool;

        int iFolderId = parsePositiveInt(folder.getFolderId());
        List<Task> tasks;
        SearchIterator<Task> queriedTasks = null;
        try {
            queriedTasks = tasksSql.getModifiedTasksInFolder(iFolderId, FIELDS_ID, DATE_ZERO);
            if (!queriedTasks.hasNext()) {
                // No tasks in given folder
                return null;
            }

            if (isPauseRequested()) {
                JSONObject jSavePoint = new JSONObject(4);
                jSavePoint.putSafe(JSON_FIELD_FOLDER, folder.getFolderId());
                jSavePoint.putSafe(JSON_FIELD_PATH, path);
                jSavePoint.putSafe(JSON_FIELD_ROOT, I(root));
                jSavePoint.putSafe(JSON_FIELD_ID, startTaskId != null ? startTaskId : I(queriedTasks.next().getObjectID()));
                return savePointFor(jSavePoint);
            }
            checkAborted();

            tasks = SearchIterators.asList(queriedTasks);
            Collections.sort(tasks, COMPARATOR);

            if (startTaskId != null) {
                boolean found = false;
                int index = 0;
                while (!found && index < tasks.size()) {
                    Task t = tasks.get(index);
                    if (t.getObjectID() == startTaskId.intValue()) {
                        found = true;
                    } else {
                        index++;
                    }
                }

                if (found && index > 0) {
                    tasks = tasks.subList(index, tasks.size());
                }
            }

            LOG.debug("Going to export {} tasks from directory {} for data export {} of user {} in context {}", I(tasks.size()), folder.getName(), UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()));
        } catch (DataExportAbortedException e) {
            throw e;
        } catch (Exception e) {
            if (isRetryableExceptionAndMayFail(e, sink)) {
                JSONObject jSavePoint = new JSONObject(4).putSafe(JSON_FIELD_FOLDER, folder.getFolderId()).putSafe(JSON_FIELD_PATH, path).putSafe(JSON_FIELD_ROOT, I(root));
                if (startTaskId != null) {
                    jSavePoint.putSafe(JSON_FIELD_ID, startTaskId);
                }
                return savePointFor(jSavePoint, e);
            }
            if (isPermissionDenied(e)) {
                LOG.debug("Forbidden to export tasks from folder \"{}\" for user {} in context {}", folder.getName(), I(task.getUserId()), I(task.getContextId()), e);
                sink.addToReport(Message.builderWithPermissionDeniedType().appendToMessage("Insufficient permissions to export tasks from folder \"").appendToMessage(folder.getName()).appendToMessage("\": ").appendToMessage(e.getMessage()).withModuleId(ID_TASKS).withTimeStamp(new Date()).build());
            } else {
                LOG.warn("Failed to export tasks from folder \"{}\" for user {} in context {}", folder.getName(), I(task.getUserId()), I(task.getContextId()), e);
                sink.addToReport(Message.builder().appendToMessage("Failed to export tasks from folder \"").appendToMessage(folder.getName()).appendToMessage("\": ").appendToMessage(e.getMessage()).withModuleId(ID_TASKS).withTimeStamp(new Date()).build());
            }
            return null;
        } finally {
            SearchIterators.close(queriedTasks);
        }

        int batchCount = 0;
        List<ConversionError> errors = new LinkedList<>();
        List<ConversionWarning> warnings = new LinkedList<>();
        ThresholdFileHolder fileHolder = null;
        try {
            for (Task task : tasks) {
                int taskId = task.getObjectID();

                if (isPauseRequested()) {
                    JSONObject jSavePoint = new JSONObject(4);
                    jSavePoint.putSafe(JSON_FIELD_FOLDER, folder.getFolderId());
                    jSavePoint.putSafe(JSON_FIELD_PATH, path);
                    jSavePoint.putSafe(JSON_FIELD_ROOT, I(root));
                    jSavePoint.putSafe(JSON_FIELD_ID, I(taskId));
                    return savePointFor(jSavePoint);
                }
                int count = incrementAndGetCount();
                checkAborted((count % 100 == 0));
                if (count % 1000 == 0) {
                    sink.setSavePoint(new JSONObject(4).putSafe(JSON_FIELD_FOLDER, folder.getFolderId()).putSafe(JSON_FIELD_PATH, path).putSafe(JSON_FIELD_ROOT, I(root)).putSafe(JSON_FIELD_ID, I(taskId)));
                }
                batchCount++;

                List<IFileHolder> attachments = null;
                try {
                    // Query full task
                    Task t = tasksSql.getTaskById(taskId, iFolderId);

                    // Prepare attachments
                    if (t.getNumberOfAttachments() > 0) {
                        int folderId = Strings.getUnsignedInt(folder.getFolderId());
                        if (folderId > 0) {
                            Optional<List<IFileHolder>> optionalAttachments = loadAttachmentBinaries(taskId, com.openexchange.groupware.Types.TASK, folderId, session);
                            if (optionalAttachments.isPresent()) {
                                attachments = optionalAttachments.get();
                                t.setProperty(PROPERTY_BINARY_ATTACHMENTS, attachments);
                            }
                        }
                    }

                    // Write task as iCal to iCal session
                    ICalSession iCalSession = icalEmitter.createSession();
                    icalEmitter.writeTask(iCalSession, t, context, errors, warnings);

                    // Write iCal
                    InputStream iCalContent = null;
                    try {
                        // Write iCal content to file holder
                        fileHolder = initOrReset(fileHolder);
                        icalEmitter.writeSession(iCalSession, fileHolder.asOutputStream());

                        if (fileHolder.getLength() <= 0) {
                            // No bytes written
                            LOG.debug("No export for task {} ({} of {}) from directory {} for data export {} of user {} in context {}. Reason: Empty iCal written", I(taskId), I(batchCount), I(tasks.size()), folder.getName(), UUIDs.getUnformattedStringObjectFor(this.task.getId()), I(this.task.getUserId()), I(this.task.getContextId()));
                        } else {
                            // Export file holder's content
                            iCalContent = fileHolder.getStream();
                            boolean exported = sink.export(iCalContent, new Item(path, taskId + ".ics", null));
                            if (!exported) {
                                return savePointFor(new JSONObject(4).putSafe(JSON_FIELD_FOLDER, folder.getFolderId()).putSafe(JSON_FIELD_PATH, path).putSafe(JSON_FIELD_ROOT, I(root)).putSafe(JSON_FIELD_ID, I(taskId)));
                            }
                            LOG.debug("Exported task {} ({} of {}) from directory {} for data export {} of user {} in context {}", I(taskId), I(batchCount), I(tasks.size()), folder.getName(), UUIDs.getUnformattedStringObjectFor(this.task.getId()), I(this.task.getUserId()), I(this.task.getContextId()));
                        }
                    } finally {
                        Streams.close(iCalContent);
                    }
                } catch (Exception e) {
                    if (isRetryableExceptionAndMayFail(e, sink)) {
                        return savePointFor(new JSONObject(4).putSafe(JSON_FIELD_FOLDER, folder.getFolderId()).putSafe(JSON_FIELD_PATH, path).putSafe(JSON_FIELD_ROOT, I(root)).putSafe(JSON_FIELD_ID, I(taskId)), e);
                    }
                    LOG.warn("Failed to export task {} in folder \"{}\" for data export {} of user {} in context {}", I(taskId), folder.getName(), UUIDs.getUnformattedStringObjectFor(this.task.getId()), I(this.task.getUserId()), I(this.task.getContextId()), e);
                    sink.addToReport(Message.builder().appendToMessage("Failed to export task \"").appendToMessage(taskId).appendToMessage("\" in folder \"").appendToMessage(folder.getName()).appendToMessage("\": ").appendToMessage(e.getMessage()).withModuleId(ID_TASKS).withTimeStamp(new Date()).build());
                } finally {
                    Streams.close(attachments);
                }
            }
        } finally {
            Streams.close(fileHolder);
        }

        return null;
    }

    /**
     * Initializes a new file holder if passed reference is <code>null</code>; otherwise given instance is reset and returned.
     *
     * @param fileHolder The currently used file holder instance or <code>null</code>
     * @return The file holder to use
     */
    private static ThresholdFileHolder initOrReset(ThresholdFileHolder fileHolder) {
        if (fileHolder == null) {
            return new ThresholdFileHolder(false);
        }
        fileHolder.reset();
        return fileHolder;
    }

    // ------------------------------------------------------------ Helpers -------------------------------------------------------------

    private static class Folder extends com.openexchange.gdpr.dataexport.provider.general.Folder {

        Folder(int folderId, String name, boolean rootFolder) {
            this(Integer.toString(folderId), name, rootFolder);
        }

        Folder(String folderId, String name, boolean rootFolder) {
            super(folderId, name, rootFolder);
        }
    }

    private static class DecoratorProvider {

        private final Locale locale;
        private final TimeZone timeZone;
        private final List<ContentType> allowedContentTypes;
        final ContentType contentType;

        DecoratorProvider(Locale locale, TimeZone timeZone, ContentType contentTypes) {
            super();
            this.locale = locale;
            this.timeZone = timeZone;
            this.allowedContentTypes = Collections.singletonList(contentTypes);
            this.contentType = contentTypes;
        }

        FolderServiceDecorator createDecorator() {
            return new FolderServiceDecorator().setLocale(locale).setTimeZone(timeZone).setAllowedContentTypes(allowedContentTypes).put("suppressUnifiedMail", Boolean.TRUE);
        }
    }

    private static class Options {

        final boolean includePublicFolders;
        final boolean includeSharedFolders;

        Options(boolean includePublicFolders, boolean includeSharedFolders) {
            super();
            this.includePublicFolders = includePublicFolders;
            this.includeSharedFolders = includeSharedFolders;
        }
    }

    private static class StartInfo {

        final int root;
        final String folderId;
        final String path;
        final Integer taskId;

        StartInfo(Integer taskId, int folderId, String path, int root) {
            this(taskId, Integer.toString(folderId), path, root);
        }

        StartInfo(Integer taskId, String folderId, String path, int root) {
            super();
            this.root = root;
            this.taskId = taskId;
            this.folderId = folderId;
            this.path = path;
        }
    }

    private static class NeededServices {

        final ICalEmitter icalEmitter;
        final FolderService folderService;
        final ContextService contextService;
        final ThreadPoolService threadPool;

        NeededServices(FolderService folderService, ICalEmitter icalEmitter, ContextService contextService, ThreadPoolService threadPool) {
            super();
            this.icalEmitter = icalEmitter;
            this.folderService = folderService;
            this.contextService = contextService;
            this.threadPool = threadPool;
        }
    }

}
