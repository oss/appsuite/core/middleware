/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 1997-2018 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * https://oss.oracle.com/licenses/CDDL+GPL-1.1
 * or LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * If applicable, add the following below the License Header, with the fields
 * enclosed by brackets [] replaced by your own identifying information:
 * "Portions Copyright [year] [name of copyright owner]"
 *
 * Contributor(s):
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.mail.imap.protocol;

import java.util.ArrayList;
import java.util.List;
import javax.mail.Flags;
import com.sun.mail.iap.ParsingException;
import com.sun.mail.iap.Response;

/**
 * Information collected when opening a mailbox.
 *
 * @author  John Mani
 * @author  Bill Shannon
 */

public class MailboxInfo { 
    /** The available flags. */
    public Flags availableFlags = null;
    /** The permanent flags. */
    public Flags permanentFlags = null;
    /** The total number of messages. */
    public int total = -1;
    /** The number of recent messages. */
    public int recent = -1;
    /** The first unseen message. */
    public int first = -1;
    /** The UIDVALIDITY. */
    public long uidvalidity = -1;
    /** The next UID value to be assigned. */
    public long uidnext = -1;
    /** UIDs are not sticky. */
    public boolean uidNotSticky = false;	// RFC 4315
    /** The highest MODSEQ value. */
    public long highestmodseq = -1;	// RFC 4551 - CONDSTORE
    /** Folder.READ_WRITE or Folder.READ_ONLY, set by IMAPProtocol. */
    public int mode;
    /** VANISHED or FETCH responses received while opening the mailbox. */
    public List<IMAPResponse> responses;
    /** VANISHED or FETCH events received while opening the mailbox. */
    public List<MailboxInfoEvent> events;

    /**
     * Collect the information about this mailbox from the
     * responses to a <code>SELECT</code> or <code>EXAMINE</code>.
     * <p>
     * Example with <code>QRESYNC</code> parameter to <code>SELECT</code>/<code>EXAMINE</code>:
     * <pre>
     * C: A3 SELECT INBOX (QRESYNC (67890007 90060115194045000 41:211,214:541))
     * S: * OK [CLOSED]
     * S: * 100 EXISTS
     * S: * 11 RECENT
     * S: * OK [UIDVALIDITY 67890007] UIDVALIDITY
     * S: * OK [UIDNEXT 600] Predicted next UID
     * S: * OK [HIGHESTMODSEQ 90060115205545359] Highest mailbox mod-sequence
     * S: * OK [UNSEEN 7] There are some unseen messages in the mailbox
     * S: * FLAGS (\Answered \Flagged \Draft \Deleted \Seen)
     * S: * OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen \*)] Permanent flags
     * S: * VANISHED (EARLIER) 41,43:116,118,120:211,214:540
     * S: * 49 FETCH (UID 117 FLAGS (\Seen \Answered) MODSEQ (90060115194045001))
     * S: * 50 FETCH (UID 119 FLAGS (\Draft $MDNSent) MODSEQ (90060115194045308))
     * S: * 51 FETCH (UID 541 FLAGS (\Seen $Forwarded) MODSEQ (90060115194045001))
     * S: A3 OK [READ-WRITE] mailbox selected
     * </pre>
     *
     * @param	r	the responses
     * @param   decodeUserFlagsWithUTF7  Whether to decode user flags using RFC2060's UTF-7 encoding
     * @exception	ParsingException	for errors parsing the responses
     */
    public MailboxInfo(Response[] r, boolean decodeUserFlagsWithUTF7) throws ParsingException {
	for (int i = 0; i < r.length; i++) {
	    if (r[i] == null || !(r[i] instanceof IMAPResponse))
		continue;

	    IMAPResponse ir = (IMAPResponse)r[i];

	    if (ir.keyEquals("EXISTS")) {
		total = ir.getNumber();
		r[i] = null; // remove this response
	    } else if (ir.keyEquals("RECENT")) {
		recent = ir.getNumber();
		r[i] = null; // remove this response
	    } else if (ir.keyEquals("FLAGS")) {
		availableFlags = new FLAGS(ir, decodeUserFlagsWithUTF7);
		r[i] = null; // remove this response
	    } else if (ir.keyEquals("VANISHED")) {
		if (responses == null)
		    responses = new ArrayList<>();
		responses.add(ir);
		r[i] = null; // remove this response
	    } else if (ir.keyEquals("FETCH")) {
		if (responses == null)
		    responses = new ArrayList<>();
		responses.add(ir);
		r[i] = null; // remove this response
	    } else if (ir.isUnTagged() && ir.isOK()) {
		/*
		 * should be one of:
		 * 	* OK [UNSEEN 12]
		 * 	* OK [UIDVALIDITY 3857529045]
		 * 	* OK [PERMANENTFLAGS (\Deleted)]
		 * 	* OK [UIDNEXT 44]
		 * 	* OK [HIGHESTMODSEQ 103]
		 */
		ir.skipSpaces();

		if (ir.readByte() != '[') {	// huh ???
		    ir.reset();
		    continue;
		}

		boolean handled = true;
		String s = ir.readAtom();
		if (s.equalsIgnoreCase("UNSEEN"))
		    first = ir.readNumber();
		else if (s.equalsIgnoreCase("UIDVALIDITY"))
		    uidvalidity = ir.readLong();
		else if (s.equalsIgnoreCase("PERMANENTFLAGS"))
		    permanentFlags = new FLAGS(ir, decodeUserFlagsWithUTF7);
		else if (s.equalsIgnoreCase("UIDNEXT"))
		    uidnext = ir.readLong();
		else if (s.equalsIgnoreCase("HIGHESTMODSEQ"))
		    highestmodseq = ir.readLong();
		else
		    handled = false;	// possibly an ALERT

		if (handled)
		    r[i] = null; // remove this response
		else
		    ir.reset();	// so ALERT can be read
	    } else if (ir.isUnTagged() && ir.isNO()) {
		/*
		 * should be one of:
		 * 	* NO [UIDNOTSTICKY]
		 */
		ir.skipSpaces();

		if (ir.readByte() != '[') {	// huh ???
		    ir.reset();
		    continue;
		}

		boolean handled = true;
		String s = ir.readAtom();
		if (s.equalsIgnoreCase("UIDNOTSTICKY"))
		    uidNotSticky = true;
		else
		    handled = false;	// possibly an ALERT

		if (handled)
		    r[i] = null; // remove this response
		else
		    ir.reset();	// so ALERT can be read
	    }
	}

	/*
	 * The PERMANENTFLAGS response code is optional, and if
	 * not present implies that all flags in the required FLAGS
	 * response can be changed permanently.
	 */
	if (permanentFlags == null) {
	    if (availableFlags != null)
		permanentFlags = new Flags(availableFlags);
	    else
		permanentFlags = new Flags();
	}
    }

    /**
     * The interceptor for untagged FETCH and VANISHED responses.
     */
    public static class VanishedAndChangedInterceptor implements com.sun.mail.iap.ResponseInterceptor {

        private final List<MailboxInfoEvent> openEvents;

        /**
         * Initializes a new instance of {@link IMAPProtocol.VanishedAndChangedInterceptor}.
         */
        public VanishedAndChangedInterceptor() {
            super();
            openEvents = new ArrayList<MailboxInfoEvent>();
        }

        /**
         * Gets the captured events.
         *
         * @return The captured events
         */
        public List<MailboxInfoEvent> getEvents() {
            return openEvents;
        }

        @Override
        public boolean intercept(Response response) {
            if (!(response instanceof IMAPResponse ir)) {
                return false;
            }

            if (ir.keyEquals("VANISHED")) {
                // "VANISHED" SP ["(EARLIER)"] SP known-uids
                String[] s = ir.readAtomStringList();
                // check that it really is "EARLIER"
                if (s == null || s.length != 1 || !s[0].equalsIgnoreCase("EARLIER")) {
                    return false;   // it's not, what to do with it here?
                }
                String uids = ir.readAtom();
                UIDSet[] uidset = UIDSet.parseUIDSets(uids);
                if (uidset != null)
                    openEvents.add(new MailboxInfoVanishedEvent(uidset));
                return true;
            }

            if (ir.keyEquals("FETCH")) {
                FetchResponse fr = (FetchResponse) ir;
                UID uid = fr.getItem(UID.class);
                MODSEQ modseq = fr.getItem(MODSEQ.class);
                openEvents.add(new MailboxInfoChangedEvent(fr.getNumber(), uid == null ? -1L : uid.uid, modseq == null ? -1L : modseq.modseq, fr.getItem(FLAGS.class), fr.getExtensionItems()));
                return true;
            }
            return false;
        }
    }
}
