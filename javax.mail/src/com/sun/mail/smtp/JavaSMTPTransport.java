/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.sun.mail.smtp;

import java.io.OutputStream;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.URLName;
import com.openexchange.java.Buffers;

/**
 * {@link JavaSMTPTransport} - The special JavaMail SMTP transport paying respect to <code>"mail.smtp.maxMessageSize"</code> property.
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class JavaSMTPTransport extends SMTPTransport {

    /**
     * Initializes a new {@link JavaSMTPTransport}.
     *
     * @param   session the Session
     * @param   urlname the URLName of this transport
     * @param   name    the protocol name of this transport
     * @param   isSSL   use SSL to connect?
     */
    public JavaSMTPTransport(Session session, URLName urlname, String name, boolean isSSL) {
        super(session, urlname, name, isSSL);
    }

    /**
     * Initializes a new {@link JavaSMTPTransport}.
     *
     * @param   session the Session
     * @param   urlname the URLName of this transport
     */
    public JavaSMTPTransport(Session session, URLName urlname) {
        super(session, urlname);
    }

    @Override
    protected OutputStream data() throws MessagingException {
        long optMaxSize = optMaxMessageSize();
        return optMaxSize > 0 ? new CountingOutputStream(super.data(), optMaxSize) : super.data();
    }

    @Override
    protected OutputStream bdat() throws MessagingException {
        long optMaxSize = optMaxMessageSize();
        return optMaxSize > 0 ? new CountingOutputStream(super.bdat(), optMaxSize) : super.bdat();
    }

    @Override
    protected int readServerResponse() throws MessagingException {
        return readServerResponse(Buffers.BUFFER_SIZE_8K);
    }

    /**
     * Gets the max. message size allowed for transport.
     *
     * @return The max. message size or less than/equal to <code>0</code> (zero) if there is no limitation
     */
    private long optMaxMessageSize() {
        String sMaxMailSize = session.getProperty("mail.smtp.maxMessageSize");
        if (sMaxMailSize != null) {
            try {
                long maxMailSize = Long.parseLong(sMaxMailSize.trim());
                if (maxMailSize > 0) {
                    return maxMailSize;
                }
            } catch (NumberFormatException e) {
                // Not a parseable number...
            }
        }
        return 0;
    }

}
