/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import com.openexchange.admin.rmi.dataobjects.ActiveDeputyPermission;
import com.openexchange.admin.rmi.dataobjects.Context;
import com.openexchange.admin.rmi.dataobjects.Credentials;
import com.openexchange.admin.rmi.dataobjects.DeputyPermission;
import com.openexchange.admin.rmi.dataobjects.User;
import com.openexchange.admin.rmi.exceptions.DatabaseUpdateException;
import com.openexchange.admin.rmi.exceptions.InvalidCredentialsException;
import com.openexchange.admin.rmi.exceptions.InvalidDataException;
import com.openexchange.admin.rmi.exceptions.NoSuchContextException;
import com.openexchange.admin.rmi.exceptions.NoSuchUserException;
import com.openexchange.admin.rmi.exceptions.StorageException;

/**
 * This interface defines the methods of the deputy permissions management.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since 8.x
 */
public interface OXDeputyPermissionsInterface extends Remote {

    /**
     * RMI name to be used in the naming lookup.
     */
    public static final String RMI_NAME = "OXDeputyPermissions";

    /**
     * Grants given deputy permission.
     *
     * @param deputyPermission The deputy permission
     * @param context The context providing context identifier
     * @param user The user providing user identifier
     * @param credentials The credentials
     * @return The identifier of the associated deputy permission
     * @throws RemoteException If a general remote exception occurs
     * @throws StorageException If a general storage exception occurs
     * @throws InvalidCredentialsException If provided credentials are invalid
     * @throws NoSuchContextException If specified context does not exist
     * @throws NoSuchUserException If specified user does not exist
     * @throws InvalidDataException If given account data is invalid
     * @throws DatabaseUpdateException If database is currently updating
     */
    String grantDeputyPermission(DeputyPermission deputyPermission, Context context, User user, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, NoSuchContextException, NoSuchUserException, InvalidDataException, DatabaseUpdateException;

    /**
     * Updates given deputy permission.
     *
     * @param deputyId The identifier of the deputy permission
     * @param deputyPermission The new deputy permission to apply
     * @param context The context providing context identifier
     * @param user The user providing user identifier
     * @param credentials The credentials
     * @throws RemoteException If a general remote exception occurs
     * @throws StorageException If a general storage exception occurs
     * @throws InvalidCredentialsException If provided credentials are invalid
     * @throws NoSuchContextException If specified context does not exist
     * @throws NoSuchUserException If specified user does not exist
     * @throws InvalidDataException If given account data is invalid
     * @throws DatabaseUpdateException If database is currently updating
     */
    void updateDeputyPermission(String deputyId, DeputyPermission deputyPermission, Context context, User user, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, NoSuchContextException, NoSuchUserException, InvalidDataException, DatabaseUpdateException;

    /**
     * Revokes the deputy permission associated with given identifier.
     *
     * @param deputyId The identifier of the deputy permission
     * @param context The context providing context identifier
     * @param user The user providing user identifier
     * @param credentials The credentials
     * @throws RemoteException If a general remote exception occurs
     * @throws StorageException If a general storage exception occurs
     * @throws InvalidCredentialsException If provided credentials are invalid
     * @throws NoSuchContextException If specified context does not exist
     * @throws NoSuchUserException If specified user does not exist
     * @throws InvalidDataException If given account data is invalid
     * @throws DatabaseUpdateException If database is currently updating
     */
    void revokeDeputyPermission(String deputyId, Context context, User user, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, NoSuchContextException, NoSuchUserException, InvalidDataException, DatabaseUpdateException;

    /**
     * Revokes all deputy permission from specified user (grantor).
     *
     * @param context The context providing context identifier
     * @param user The user providing user identifier
     * @param credentials The credentials
     * @throws RemoteException If a general remote exception occurs
     * @throws StorageException If a general storage exception occurs
     * @throws InvalidCredentialsException If provided credentials are invalid
     * @throws NoSuchContextException If specified context does not exist
     * @throws NoSuchUserException If specified user does not exist
     * @throws InvalidDataException If given account data is invalid
     * @throws DatabaseUpdateException If database is currently updating
     */
    void revokeAllDeputyPermission(Context context, User user, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, NoSuchContextException, NoSuchUserException, InvalidDataException, DatabaseUpdateException;

    /**
     * Gets the deputy permission associated with given identifier.
     *
     * @param deputyId The identifier of the deputy permission
     * @param context The context providing context identifier
     * @param user The user providing user identifier
     * @param credentials The credentials
     * @return The deputy permission
     * @throws RemoteException If a general remote exception occurs
     * @throws StorageException If a general storage exception occurs
     * @throws InvalidCredentialsException If provided credentials are invalid
     * @throws NoSuchContextException If specified context does not exist
     * @throws NoSuchUserException If specified user does not exist
     * @throws InvalidDataException If given account data is invalid
     * @throws DatabaseUpdateException If database is currently updating
     */
    ActiveDeputyPermission getDeputyPermission(String deputyId, Context context, User user, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, NoSuchContextException, NoSuchUserException, InvalidDataException, DatabaseUpdateException;

    /**
     * List all deputy permissions of a context
     *
     * @param context The context providing context identifier
     * @param credentials The credentials
     * @return A list of all deputy permissions of a context
     * @throws RemoteException If a general remote exception occurs
     * @throws StorageException If a general storage exception occurs
     * @throws InvalidCredentialsException If provided credentials are invalid
     * @throws NoSuchContextException If specified context does not exist
     * @throws InvalidDataException If given account data is invalid
     * @throws DatabaseUpdateException If database is currently updating
     */
    List<ActiveDeputyPermission> listAll(Context context, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, NoSuchContextException, InvalidDataException, DatabaseUpdateException;

}
