/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.rmi.dataobjects;

import java.io.Serializable;
import java.util.List;

/**
 * {@link ModulePermission} - Represents a permission for a certain module (e.g. <code>"mail"</code>) for provisioning API.
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ModulePermission implements Serializable, Cloneable {

    /** The serial version UID */
    private static final long serialVersionUID = 156305089743702181L;

    private String moduleId;
    private List<String> folderIds;
    private boolean admin;
    private int folderPermission;
    private int readPermission;
    private int writePermission;
    private int deletePermission;

    /**
     * Initializes a new instance of {@link ModulePermission}.
     */
    public ModulePermission() {
        super();
    }

    @Override
    public Object clone() throws CloneNotSupportedException { // NOSONARLINT
        return super.clone();
    }

    /**
     * Gets the module identifier; e.g. <code>"mail"</code>.
     *
     * @return The module identifier
     */
    public String getModuleId() {
        return moduleId;
    }

    /**
     * Sets the module identifier.
     *
     * @param moduleId The module identifier to set
     */
    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    /**
     * Gets the folder identifiers.
     *
     * @return The folder identifiers
     */
    public List<String> getFolderIds() {
        return folderIds;
    }

    /**
     * Sets the folder identifiers.
     *
     * @param folderIds The folder identifiers to set
     */
    public void setFolderIds(List<String> folderIds) {
        this.folderIds = folderIds;
    }

    /**
     * Checks if this permission grants administrator privilege.
     *
     * @return <code>true</code> if administrator privilege is granted; otherwise <code>false</code>
     */
    public boolean isAdmin() {
        return admin;
    }

    /**
     * Sets whether this permission grants administrator privilege.
     *
     * @param admin The flag to set
     */
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    /**
     * Gets the folder permission.
     *
     * @return The folder permission
     */
    public int getFolderPermission() {
        return folderPermission;
    }

    /**
     * Sets the folder permission.
     *
     * @param folderPermission The folder permission to set
     */
    public void setFolderPermission(int folderPermission) {
        this.folderPermission = folderPermission;
    }

    /**
     * Gets the read permission.
     *
     * @return The read permission
     */
    public int getReadPermission() {
        return readPermission;
    }

    /**
     * Sets the read permission.
     *
     * @param readPermission The read permission to set
     */
    public void setReadPermission(int readPermission) {
        this.readPermission = readPermission;
    }

    /**
     * Gets the write permission.
     *
     * @return The write permission
     */
    public int getWritePermission() {
        return writePermission;
    }

    /**
     * Sets the write permission.
     *
     * @param writePermission The write permission to set
     */
    public void setWritePermission(int writePermission) {
        this.writePermission = writePermission;
    }

    /**
     * Gets the delete permission.
     *
     * @return The delete permission
     */
    public int getDeletePermission() {
        return deletePermission;
    }

    /**
     * Sets the delete permission.
     *
     * @param deletePermission The delete permission to set
     */
    public void setDeletePermission(int deletePermission) {
        this.deletePermission = deletePermission;
    }

}
