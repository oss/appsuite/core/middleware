/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.rmi.dataobjects;

import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * Object for setting/getting access informations to the different ox modules
 *
 * @author <a href="mailto:manuel.kraft@open-xchange.com">Manuel Kraft</a>
 * @author <a href="mailto:carsten.hoeger@open-xchange.com">Carsten Hoeger</a>
 * @author <a href="mailto:dennis.sieben@open-xchange.com">Dennis Sieben</a>
 *
 */
public class UserModuleAccess implements Serializable, Cloneable {

    /**
     * For serialization
     */
    private static final long serialVersionUID = -5336341908204911967L;

    // ALL ACCESS MODULES;
    // MAKE SURE YOU REWRITE THE "equals" METHOD
    // IF YOU CHANGE SOMETHING HERE!!!

    /**
     * The calendar access
     */
    private boolean calendar = true;

    /**
     * The contacts access
     */
    private boolean contacts = true;

    /**
     * The delegateTask access
     */
    private boolean delegateTask = true;

    /**
     * The editPublicFolders access
     */
    private boolean editPublicFolders = true;

    /**
     * The ical access
     */
    private boolean ical = true;

    /**
     * The infostore access
     */
    private boolean infostore = true;

    /**
     * The readCreateSharedFolders access
     */
    private boolean readCreateSharedFolders = true;

    /**
     * The Syncml access
     */
    private boolean Syncml = true;

    /**
     * The Tasks access
     */
    private boolean Tasks = true;

    /**
     * The Vcard access
     */
    private boolean Vcard = true;

    /**
     * The Webdav access
     */
    private boolean Webdav = true;

    /**
     * The WebdavXml access
     */
    @Deprecated
    private boolean WebdavXml = true;

    /**
     * The Webmail access
     */
    private boolean Webmail = true;

    /**
     * The EditGroup access
     */
    private boolean EditGroup = true;

    /**
     * The EditResource access
     */
    private boolean EditResource = true;

    /**
     * The EditPassword access
     */
    private boolean EditPassword = true;

    /**
     * The CollectEmailAddresses access
     */
    private boolean CollectEmailAddresses = true;

    /**
     * The MultipleMailAccounts access
     */
    private boolean MultipleMailAccounts = true;

    /**
     * The Subscription access
     */
    private boolean Subscription = true;

    /**
     * The Publication access
     */
    @Deprecated
    private boolean Publication = true;

    /**
     * The ActiveSync access
     */
    private boolean ActiveSync = true;

    /**
     * The USM access
     */
    private boolean USM = true;

    /**
     * The OLOX20 access
     */
    @Deprecated
    private boolean OLOX20 = true;

    /**
     * The GlobalAddressBookDisabled access
     */
    private boolean GlobalAddressBookDisabled = false;

    /**
     * The PublicFolderEditable access
     */
    private boolean PublicFolderEditable = false;

    /**
     * The deniedPortal access
     */
    private boolean deniedPortal;

    /**
     * Creates a new instance of UserModuleAccess
     */
    public UserModuleAccess() {
        super();
    }

    @Override
    public UserModuleAccess clone() {
        try {
            return (UserModuleAccess) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError("CloneNotSupportedException although Colenable is implemented");
        }
    }

    /**
     * Enable all modules
     */
    public void enableAll() {
        this.calendar = true;
        this.contacts = true;
        this.delegateTask = true;
        this.editPublicFolders = true;
        this.ical = true;
        this.infostore = true;
        this.readCreateSharedFolders = true;
        this.Syncml = true;
        this.Tasks = true;
        this.Vcard = true;
        this.Webdav = true;
        this.WebdavXml = true;
        this.Webmail = true;
        this.EditGroup = true;
        this.EditResource = true;
        this.EditPassword = true;
        this.CollectEmailAddresses = true;
        this.MultipleMailAccounts = true;
        this.Subscription = true;
        this.Publication = true;
        this.ActiveSync = true;
        this.USM = true;
        this.GlobalAddressBookDisabled = false;
        this.PublicFolderEditable = true;
        this.OLOX20 = true;
    }

    /**
     * Disable all modules
     */
    public void disableAll() {
        this.calendar = false;
        this.contacts = false;
        this.delegateTask = false;
        this.editPublicFolders = false;
        this.ical = false;
        this.infostore = false;
        this.readCreateSharedFolders = false;
        this.Syncml = false;
        this.Tasks = false;
        this.Vcard = false;
        this.Webdav = false;
        this.WebdavXml = false;
        this.Webmail = false;
        this.EditGroup = false;
        this.EditResource = false;
        this.EditPassword = false;
        this.CollectEmailAddresses = false;
        this.MultipleMailAccounts = false;
        this.Subscription = false;
        this.Publication = false;
        this.ActiveSync = false;
        this.USM = false;
        this.GlobalAddressBookDisabled = true;
        this.PublicFolderEditable = false;
        this.OLOX20 = false;
    }

    /**
     * Gets the edit-group access.
     *
     * @return The edit-group access
     */
    public boolean getEditGroup() {
        return EditGroup;
    }

    /**
     * Sets the edit-group access.
     *
     * @param editGroup The edit-group access to set
     */
    public void setEditGroup(final boolean editGroup) {
        EditGroup = editGroup;
    }

    /**
     * Gets the edit-resource access.
     *
     * @return The edit-resource access
     */
    public boolean getEditResource() {
        return EditResource;
    }

    /**
     * Sets the edit-resource access.
     *
     * @param editResource The edit-resource access to set
     */
    public void setEditResource(final boolean editResource) {
        EditResource = editResource;
    }

    /**
     * Gets the edit-password access.
     *
     * @return The edit-password access
     */
    public boolean getEditPassword() {
        return EditPassword;
    }

    /**
     * Sets the edit-password access.
     *
     * @param editPassword The edit-password access to set
     */
    public void setEditPassword(final boolean editPassword) {
        EditPassword = editPassword;
    }

    /**
     * Gets the collect-email-addresses access.
     *
     * @return The collect-email-addresses access
     */
    public boolean isCollectEmailAddresses() {
        return CollectEmailAddresses;
    }

    /**
     * Sets the collect-email-addresses access.
     *
     * @param collectEmailAddresses The collect-email-addresses access to set
     */
    public void setCollectEmailAddresses(final boolean collectEmailAddresses) {
        CollectEmailAddresses = collectEmailAddresses;
    }

    /**
     * Gets the multiple-mail-accounts access.
     *
     * @return The multiple-mail-accounts access
     */
    public boolean isMultipleMailAccounts() {
        return MultipleMailAccounts;
    }

    /**
     * Sets the multiple-mail-accounts access.
     *
     * @param multipleMailAccounts The multiple-mail-accounts access to set
     */
    public void setMultipleMailAccounts(final boolean multipleMailAccounts) {
        MultipleMailAccounts = multipleMailAccounts;
    }

    /**
     * Gets the subscription access.
     *
     * @return The subscription access to set
     */
    public boolean isSubscription() {
        return Subscription;
    }

    /**
     * Sets the subscription access.
     *
     * @param subscription The subscription access to set
     */
    public void setSubscription(final boolean subscription) {
        Subscription = subscription;
    }

    /**
     * Gets the publication access.
     *
     * @return The publication access
     * @deprecated with v7.10.2 publication has been removed
     */
    @Deprecated
    public boolean isPublication() {
        return Publication;
    }

    /**
     * Sets the publication access.
     *
     * @param publication The publication access to set
     * @deprecated with v7.10.2 publication has been removed
     */
    @Deprecated
    public void setPublication(final boolean publication) {
        Publication = publication;
    }

    /**
     * Shows if a user has access to the calendar module of ox.
     *
     * @return Returns <CODE>true</CODE> if user has access to calendar module
     *         or <CODE>false</CODE> if he has now access!
     */
    public boolean getCalendar() {
        return calendar;
    }

    /**
     * Defines if a user has access to the calendar module of ox.
     *
     * @param val
     *            Set to <CODE>true</CODE> if user should be able to access
     *            the calendar module!
     */
    public void setCalendar(final boolean val) {
        this.calendar = val;
    }

    /**
     * Shows if a user has access to the contact module of ox.
     *
     * @return Returns <CODE>true</CODE> if user has access to contact module
     *         or <CODE>false</CODE> if he has now access!
     */
    public boolean getContacts() {
        return contacts;
    }

    /**
     * Defines if a user has access to the contact module of ox.
     *
     * @param val
     *            Set to <CODE>true</CODE> if user should be able to access
     *            the contact module!
     */
    public void setContacts(final boolean val) {
        this.contacts = val;
    }

    /**
     * Shows if a user has the right to delegate tasks in the ox groupware.
     *
     * @return Returns <CODE>true</CODE> if user has the right to delegate
     *         tasks in the ox groupware. Or <CODE>false</CODE> if he has no
     *         right to delegate tasks!
     */
    public boolean getDelegateTask() {
        return delegateTask;
    }

    /**
     * Defines if a user has the right to delegate tasks in the ox groupware.
     *
     * @param val
     *            Set to <CODE>true</CODE> if user should be able to delegate
     *            tasks in the ox groupware.
     */
    public void setDelegateTask(final boolean val) {
        this.delegateTask = val;
    }

    /**
     * Gets the edit-public-folders access
     *
     * @return The edit-public-folders access
     */
    public boolean getEditPublicFolders() {
        return editPublicFolders;
    }

    /**
     * Sets the edit-public-folders access
     *
     * @param editPublicFolders The edit-public-folders access to set
     */
    public void setEditPublicFolders(final boolean editPublicFolders) {
        this.editPublicFolders = editPublicFolders;
    }

    /**
     * Gets the ical access
     *
     * @return The ical access
     */
    public boolean getIcal() {
        return ical;
    }

    /**
     * Sets the ical access
     *
     * @param ical The ical access to set
     */
    public void setIcal(final boolean ical) {
        this.ical = ical;
    }

    /**
     * Gets the infostore access
     *
     * @return The infostore access
     */
    public boolean getInfostore() {
        return infostore;
    }

    /**
     * Sets the infostore access
     *
     * @param infostore The infostore access to set
     */
    public void setInfostore(final boolean infostore) {
        this.infostore = infostore;
    }

    /**
     * Gets the read-create-shared-folders access
     *
     * @return The read-create-shared-folders access
     */
    public boolean getReadCreateSharedFolders() {
        return readCreateSharedFolders;
    }

    /**
     * Sets the read-create-shared-folders access
     *
     * @param readCreateSharedFolders The read-create-shared-folders access to set
     */
    public void setReadCreateSharedFolders(final boolean readCreateSharedFolders) {
        this.readCreateSharedFolders = readCreateSharedFolders;
    }

    /**
     * Gets the syncml access
     *
     * @return The syncml access
     */
    public boolean getSyncml() {
        return Syncml;
    }

    /**
     * Sets the syncml access
     *
     * @param syncml The syncml access to set
     */
    public void setSyncml(final boolean syncml) {
        this.Syncml = syncml;
    }

    /**
     * Gets the tasks access
     *
     * @return The tasks access
     */
    public boolean getTasks() {
        return Tasks;
    }

    /**
     * Sets the tasks access
     *
     * @param tasks The tasks access to set
     */
    public void setTasks(final boolean tasks) {
        this.Tasks = tasks;
    }

    /**
     * Gets the vcard access
     *
     * @return The vcard access
     */
    public boolean getVcard() {
        return Vcard;
    }

    /**
     * Sets the vcard access
     *
     * @param vcard The vcard access to set
     */
    public void setVcard(final boolean vcard) {
        this.Vcard = vcard;
    }

    /**
     * Gets the webdav access
     *
     * @return The webdav access
     */
    public boolean getWebdav() {
        return Webdav;
    }

    /**
     * Sets the webdav access
     *
     * @param webdav The webdav access to set
     */
    public void setWebdav(final boolean webdav) {
        this.Webdav = webdav;
    }

    /**
     * Gets the webdavxml access
     *
     * @return The webdavxml access
     */
    @Deprecated
    public boolean getWebdavXml() {
        return WebdavXml;
    }

    /**
     * Sets the webdavxml access
     *
     * @param webdavxml The webdavxml access to set
     */
    @Deprecated
    public void setWebdavXml(final boolean webdavxml) {
        this.WebdavXml = webdavxml;
    }

    /**
     * Gets the webmail access
     *
     * @return The webmail access
     */
    public boolean getWebmail() {
        return Webmail;
    }

    /**
     * Sets the webmail access
     *
     * @param webmail The webmail access to set
     */
    public void setWebmail(final boolean webmail) {
        this.Webmail = webmail;
    }

    /**
     * Gets the activesync access
     *
     * @return The activesync access
     */
    public boolean isActiveSync() {
        return ActiveSync;
    }

    /**
     * Sets the activesync access
     *
     * @param activeSync The activesync access to set
     */
    public void setActiveSync(final boolean activeSync) {
        this.ActiveSync = activeSync;
    }

    /**
     * Gets the usm access
     *
     * @return The usm access
     */
    public boolean isUSM() {
        return USM;
    }

    /**
     * Sets the usm access
     *
     * @param usm The usm access to set
     */
    public void setUSM(final boolean usm) {
        this.USM = usm;
    }

    /**
     * Gets the olox20 access
     *
     * @return The olox20 access
     */
    @Deprecated
    public boolean isOLOX20() {
        return OLOX20;
    }


    /**
     * Sets the usm access
     *
     * @param usm The usm access to set
     */
    @Deprecated
    public void setOLOX20(final boolean usm) {
        this.OLOX20 = usm;
    }

    /**
     * Sets the denied-portal access
     *
     * @param deniedPortal The denied-portal access to set
     */
    public void setDeniedPortal(final boolean deniedPortal) {
        this.deniedPortal = deniedPortal;
    }

    /**
     * Gets the denied-portal access
     *
     * @return The denied-portal access
     */
    public boolean isDeniedPortal() {
        return deniedPortal;
    }

    /**
     * Gets the global-address-book-disabled access
     *
     * @return The global-address-book-disabled access
     */
    public boolean isGlobalAddressBookDisabled() {
        return GlobalAddressBookDisabled;
    }

    /**
     * Sets the global-address-book-disabled access
     *
     * @param globalAddressBookDisabled The global-address-book-disabled access to set
     */
    public void setGlobalAddressBookDisabled(final boolean globalAddressBookDisabled) {
        this.GlobalAddressBookDisabled = globalAddressBookDisabled;
    }

    /**
     * Gets the public-folder-editable access
     *
     * @return The public-folder-editable access
     */
    public boolean isPublicFolderEditable() {
        return PublicFolderEditable;
    }

    /**
     * Sets the public-folder-editable access access
     *
     * @param publicFolderEditable The public-folder-editable access to set
     */
    public void setPublicFolderEditable(final boolean publicFolderEditable) {
        this.PublicFolderEditable = publicFolderEditable;
    }

    @Override
    public String toString() {
        final StringBuilder ret = new StringBuilder();
        ret.append("[ \n");
        for (final Field f : this.getClass().getDeclaredFields()) {
            try {
                final Object ob = f.get(this);
                final String tname = f.getName();
                if (ob != null && !tname.equals("serialVersionUID")) {
                    ret.append("  ");
                    ret.append(tname);
                    ret.append(": ");
                    ret.append(ob);
                    ret.append("\n");
                }
            } catch (IllegalArgumentException e) {
                ret.append("IllegalArgument\n");
            } catch (IllegalAccessException e) {
                ret.append("IllegalAccessException\n");
            }
        }
        ret.append(']');
        return ret.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (ActiveSync ? 1231 : 1237);
        result = prime * result + (CollectEmailAddresses ? 1231 : 1237);
        result = prime * result + (EditGroup ? 1231 : 1237);
        result = prime * result + (EditPassword ? 1231 : 1237);
        result = prime * result + (EditResource ? 1231 : 1237);
        result = prime * result + (GlobalAddressBookDisabled ? 1231 : 1237);
        result = prime * result + (MultipleMailAccounts ? 1231 : 1237);
        result = prime * result + (OLOX20 ? 1231 : 1237);
        result = prime * result + (PublicFolderEditable ? 1231 : 1237);
        result = prime * result + (Publication ? 1231 : 1237);
        result = prime * result + (Subscription ? 1231 : 1237);
        result = prime * result + (Syncml ? 1231 : 1237);
        result = prime * result + (Tasks ? 1231 : 1237);
        result = prime * result + (USM ? 1231 : 1237);
        result = prime * result + (Vcard ? 1231 : 1237);
        result = prime * result + (Webdav ? 1231 : 1237);
        result = prime * result + (WebdavXml ? 1231 : 1237);
        result = prime * result + (Webmail ? 1231 : 1237);
        result = prime * result + (calendar ? 1231 : 1237);
        result = prime * result + (contacts ? 1231 : 1237);
        result = prime * result + (delegateTask ? 1231 : 1237);
        result = prime * result + (deniedPortal ? 1231 : 1237);
        result = prime * result + (editPublicFolders ? 1231 : 1237);
        result = prime * result + (ical ? 1231 : 1237);
        result = prime * result + (infostore ? 1231 : 1237);
        result = prime * result + (readCreateSharedFolders ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof UserModuleAccess)) {
            return false;
        }
        UserModuleAccess other = (UserModuleAccess) obj;
        if (ActiveSync != other.ActiveSync) {
            return false;
        }
        if (CollectEmailAddresses != other.CollectEmailAddresses) {
            return false;
        }
        if (EditGroup != other.EditGroup) {
            return false;
        }
        if (EditPassword != other.EditPassword) {
            return false;
        }
        if (EditResource != other.EditResource) {
            return false;
        }
        if (GlobalAddressBookDisabled != other.GlobalAddressBookDisabled) {
            return false;
        }
        if (MultipleMailAccounts != other.MultipleMailAccounts) {
            return false;
        }
        if (OLOX20 != other.OLOX20) {
            return false;
        }
        if (PublicFolderEditable != other.PublicFolderEditable) {
            return false;
        }
        if (Publication != other.Publication) {
            return false;
        }
        if (Subscription != other.Subscription) {
            return false;
        }
        if (Syncml != other.Syncml) {
            return false;
        }
        if (Tasks != other.Tasks) {
            return false;
        }
        if (USM != other.USM) {
            return false;
        }
        if (Vcard != other.Vcard) {
            return false;
        }
        if (Webdav != other.Webdav) {
            return false;
        }
        if (WebdavXml != other.WebdavXml) {
            return false;
        }
        if (Webmail != other.Webmail) {
            return false;
        }
        if (calendar != other.calendar) {
            return false;
        }
        if (contacts != other.contacts) {
            return false;
        }
        if (delegateTask != other.delegateTask) {
            return false;
        }
        if (deniedPortal != other.deniedPortal) {
            return false;
        }
        if (editPublicFolders != other.editPublicFolders) {
            return false;
        }
        if (ical != other.ical) {
            return false;
        }
        if (infostore != other.infostore) {
            return false;
        }
        if (readCreateSharedFolders != other.readCreateSharedFolders) {
            return false;
        }
        return true;
    }

    /**
     * Transfers enabled permissions to <code>enabled</code> and disabled ones to <code>disabled</code>.
     *
     * @param enabled The {@code UserModuleAccess} carrying the enabled permissions
     * @param disabled The {@code UserModuleAccess} carrying the disbaled permissions
     */
    public void transferTo(UserModuleAccess enabled, UserModuleAccess disabled) {
        if (ActiveSync) {
            enabled.setActiveSync(true);
        } else {
            disabled.setActiveSync(true);
        }

        if (calendar) {
            enabled.setCalendar(true);
        } else {
            disabled.setCalendar(true);
        }

        if (CollectEmailAddresses) {
            enabled.setCollectEmailAddresses(true);
        } else {
            disabled.setCollectEmailAddresses(true);
        }

        if (contacts) {
            enabled.setContacts(true);
        } else {
            disabled.setContacts(true);
        }

        if (delegateTask) {
            enabled.setDelegateTask(true);
        } else {
            disabled.setDelegateTask(true);
        }

        if (deniedPortal) {
            enabled.setDeniedPortal(true);
        } else {
            disabled.setDeniedPortal(true);
        }

        if (EditGroup) {
            enabled.setEditGroup(true);
        } else {
            disabled.setEditGroup(true);
        }

        if (EditPassword) {
            enabled.setEditPassword(true);
        } else {
            disabled.setEditPassword(true);
        }

        if (editPublicFolders) {
            enabled.setEditPublicFolders(true);
        } else {
            disabled.setEditPublicFolders(true);
        }

        if (EditResource) {
            enabled.setEditResource(true);
        } else {
            disabled.setEditResource(true);
        }

        if (GlobalAddressBookDisabled) {
            enabled.setGlobalAddressBookDisabled(true);
        } else {
            disabled.setGlobalAddressBookDisabled(true);
        }

        if (ical) {
            enabled.setIcal(true);
        } else {
            disabled.setIcal(true);
        }

        if (infostore) {
            enabled.setInfostore(true);
        } else {
            disabled.setInfostore(true);
        }

        if (MultipleMailAccounts) {
            enabled.setMultipleMailAccounts(true);
        } else {
            disabled.setMultipleMailAccounts(true);
        }

        if (OLOX20) {
            enabled.setOLOX20(true);
        } else {
            disabled.setOLOX20(true);
        }

        if (Publication) {
            enabled.setPublication(true);
        } else {
            disabled.setPublication(true);
        }

        if (PublicFolderEditable) {
            enabled.setPublicFolderEditable(true);
        } else {
            disabled.setPublicFolderEditable(true);
        }

        if (readCreateSharedFolders) {
            enabled.setReadCreateSharedFolders(true);
        } else {
            disabled.setReadCreateSharedFolders(true);
        }

        if (Subscription) {
            enabled.setSubscription(true);
        } else {
            disabled.setSubscription(true);
        }

        if (Syncml) {
            enabled.setSyncml(true);
        } else {
            disabled.setSyncml(true);
        }

        if (Tasks) {
            enabled.setTasks(true);
        } else {
            disabled.setTasks(true);
        }

        if (USM) {
            enabled.setUSM(true);
        } else {
            disabled.setUSM(true);
        }

        if (Vcard) {
            enabled.setVcard(true);
        } else {
            disabled.setVcard(true);
        }

        if (Webdav) {
            enabled.setWebdav(true);
        } else {
            disabled.setWebdav(true);
        }

        if (WebdavXml) {
            enabled.setWebdavXml(true);
        } else {
            disabled.setWebdavXml(true);
        }

        if (Webmail) {
            enabled.setWebmail(true);
        } else {
            disabled.setWebmail(true);
        }

    }

}
