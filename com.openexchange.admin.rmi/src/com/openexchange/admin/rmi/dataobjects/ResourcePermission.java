/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.rmi.dataobjects;

import java.io.Serializable;

/**
 * {@link ResourcePermission} - Resource permission for an user/group
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 * @since v8
 */
public class ResourcePermission implements Serializable {

    private static final long serialVersionUID = 248703825368008334L;

    /**
     * The entity
     */
    private Integer entity;
    /**
     * The group
     */
    private Boolean group;
    /**
     * The privilege
     */
    private String privilege;

    /**
     * Initializes a new {@link ResourcePermission}.
     *
     * @param entity The entity
     * @param group The group
     * @param privilege The privilege
     */
    public ResourcePermission(Integer entity, Boolean group, String privilege) {
        super();
        this.entity = entity;
        this.group = group;
        this.privilege = privilege;
    }

    /**
     * Gets the entity
     *
     * @return The entity
     */
    public Integer getEntity() {
        return entity;
    }

    /**
     * Sets the entity
     *
     * @param entity The entity to set
     */
    public void setEntity(Integer entity) {
        this.entity = entity;
    }

    /**
     * Gets the group
     *
     * @return The group
     */
    public Boolean getGroup() {
        return group;
    }

    /**
     * Sets the group
     *
     * @param group The group to set
     */
    public void setGroup(Boolean group) {
        this.group = group;
    }

    /**
     * Gets the privilege
     *
     * @return The privilege
     */
    public String getPrivilege() {
        return privilege;
    }

    /**
     * Sets the privilege
     *
     * @param privilege The privilege to set
     */
    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

}