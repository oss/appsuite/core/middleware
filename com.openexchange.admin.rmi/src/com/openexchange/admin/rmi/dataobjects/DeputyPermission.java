/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.rmi.dataobjects;

import java.io.Serializable;
import java.util.List;

/**
 * {@link DeputyPermission} - Represents a deputy permission for provisioning API.
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class DeputyPermission implements Serializable, Cloneable {

    /** The serial version UID */
    private static final long serialVersionUID = 6640569896948706836L;

    private int entityId;
    private boolean group;
    private boolean sendOnBehalfOf;
    private List<ModulePermission> modulePermissions;

    /**
     * Initializes a new instance of {@link DeputyPermission}.
     */
    public DeputyPermission() {
        super();
    }

    @Override
    public Object clone() throws CloneNotSupportedException { // NOSONARLINT
        return super.clone();
    }

    /**
     * Checks if associated entity is a group.
     *
     * @return <code>true</code> if entity is a group; otherwise <code>false</code> (user)
     */
    public boolean isGroup() {
        return group;
    }

    /**
     * Sets whether associated entity is a group.
     *
     * @param group The group flag to set
     */
    public void setGroup(boolean group) {
        this.group = group;
    }


    /**
     * Gets the entity identifier.
     *
     * @return The entity identifier
     */
    public int getEntityId() {
        return entityId;
    }

    /**
     * Sets the entity identifier.
     *
     * @param entityId The entity identifier to set
     */
    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    /**
     * Gets the <code>sendOnBehalfOf</code> flag.
     *
     * @return The <code>sendOnBehalfOf</code> flag
     */
    public boolean isSendOnBehalfOf() {
        return sendOnBehalfOf;
    }

    /**
     * Sets the <code>sendOnBehalfOf</code> flag.
     *
     * @param sendOnBehalfOf The <code>sendOnBehalfOf</code> flag to set
     */
    public void setSendOnBehalfOf(boolean sendOnBehalfOf) {
        this.sendOnBehalfOf = sendOnBehalfOf;
    }

    /**
     * Gets the module permissions.
     *
     * @return The module permissions
     */
    public List<ModulePermission> getModulePermissions() {
        return modulePermissions;
    }

    /**
     * Sets the module permissions.
     *
     * @param modulePermissions The module permissions to set
     */
    public void setModulePermissions(List<ModulePermission> modulePermissions) {
        this.modulePermissions = modulePermissions;
    }

}
