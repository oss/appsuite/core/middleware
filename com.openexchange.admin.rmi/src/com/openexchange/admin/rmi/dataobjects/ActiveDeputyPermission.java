/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.rmi.dataobjects;

/**
 * {@link ActiveDeputyPermission} - Represents an active deputy permission for provisioning API.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ActiveDeputyPermission extends DeputyPermission {

    /** The serial version UID */
    private static final long serialVersionUID = 4327445555291115061L;

    private String deputyId;
    private int grantorId;

    /**
     * Initializes a new instance of {@link ActiveDeputyPermission}.
     */
    public ActiveDeputyPermission() {
        super();
    }

    /**
     * Gets the deputy identifier.
     *
     * @return The deputy identifier
     */
    public String getDeputyId() {
        return deputyId;
    }

    /**
     * Sets the deputy identifier
     *
     * @param deputyId The deputy identifier to set
     */
    public void setDeputyId(String deputyId) {
        this.deputyId = deputyId;
    }

    /**
     * Gets the identifier of the granting user
     *
     * @return The user identifier
     */
    public int getGrantorId() {
        return grantorId;
    }

    /**
     * Sets the identifier of the granting user
     *
     * @param userId The user identifier to set
     */
    public void setGrantorId(int userId) {
        this.grantorId = userId;
    }

}
