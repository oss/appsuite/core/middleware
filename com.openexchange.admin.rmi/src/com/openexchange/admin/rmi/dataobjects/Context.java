/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.rmi.dataobjects;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import com.openexchange.gab.GABMode;

/**
 * Class representing a context.
 *
 * @author <a href="mailto:manuel.kraft@open-xchange.com">Manuel Kraft</a>
 * @author <a href="mailto:carsten.hoeger@open-xchange.com">Carsten Hoeger</a>
 * @author <a href="mailto:dennis.sieben@open-xchange.com">Dennis Sieben</a>
 *
 */
public class Context extends ExtendableDataObject implements NameAndIdObject {

    private static final long serialVersionUID = -8939189372445990901L;

    /**
     * The context identifier
     */
    private Integer id;

    /**
     * Whether the context identifier has been set or not
     */
    private boolean idset;

    /**
     * The read database
     */
    private Database readDatabase;

    /**
     * Whether the read database has been set or not
     */
    private boolean readDatabaseset;

    /**
     * The write database
     */
    private Database writeDatabase;

    /**
     * Whether the write database has been set or not
     */
    private boolean writeDatabaseset;

    /**
     * The filestore identifier
     */
    private Integer filestore_id;

    /**
     * Whether the filestore identifier has been set or not
     */
    private boolean filestore_idset;

    /**
     * The filestore name
     */
    private String filestore_name;

    /**
     * Whether the filestore name has been set or not
     */
    private boolean filestore_nameset;

    /**
     * The average context size (in MB)
     */
    private Long average_size;

    /**
     * Whether the average context size has been set or not
     */
    private boolean average_sizeset;

    /**
     * The maximum quota
     */
    private Long maxQuota;

    /**
     * Whether the maximum quota has been set or not
     */
    private boolean maxQuotaset;

    /**
     * The used quota
     */
    private Long usedQuota;

    /**
     * Whether the used quota has been set or not
     */
    private boolean usedQuotaset;

    /**
     * The maintenance reason
     */
    private MaintenanceReason maintenanceReason;

    /**
     * Whether the maintenance reason has been set or not
     */
    private boolean maintenanceReasonset;

    /**
     * The enabled flag
     */
    private Boolean enabled;

    /**
     * Whether the enabled flag has been set or not
     */
    private boolean enabledset;

    /**
     * The name
     */
    private String name;

    /**
     * Whether the name has been set or not
     */
    private boolean nameset;

    /**
     * The login mappings
     */
    private HashSet<String> login_mappings;

    /**
     * The user attributes
     */
    private Map<String, Map<String, String>> userAttributes = null;

    /**
     * Whether the user attributes have been set or not
     */
    private boolean userAttribtuesset;

    /**
     * The listrun
     */
    private boolean listrun;

    /**
     * The quotas
     */
    private List<Object[]> quotas;

    /**
     * The global address book mode
     */
    private GABMode gabMode;

    /**
     * Initializes a new {@link Context}.
     */
    public Context() {
        super();
        init();
    }

    /**
     * Initializes a new {@link Context}.
     *
     * @param id The context id
     */
    public Context(final Integer id) {
        super();
        init();
        setId(id);
    }

    /**
     * Initializes a new {@link Context}.
     *
     * @param id The context id
     * @param name The context name
     */
    public Context(final int id, final String name) {
        super();
        init();
        this.id = Integer.valueOf(id);
        this.name = name;
    }

    @Override
    public final Integer getId() {
        return this.id;
    }

    /**
     * Gets the id as string
     *
     * @return The id
     */
    public final String getIdAsString() {
        return null == id ? null : id.toString();
    }

    @Override
    public final void setId(final Integer id) {
        this.id = id;
        this.idset = true;
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public final void setName(final String name) {
        this.name = name;
        this.nameset = true;
    }

    /**
     * Sets the login mappings.
     * Example:
     * If you add here a HashSet containing "mydomain.org", then you can later
     * login with &lt;username&gt;@mydomain.org OR &lt;username&gt;@&lt;context_id&gt;
     *
     * @param mappings The mappings
     */
    public final void setLoginMappings(final HashSet<String> mappings) {
        this.login_mappings = mappings;
    }

    /**
     * Add a single login mapping entry.
     *
     * @param mapping The login mapping
     */
    public final void addLoginMapping(final String mapping) {
        if (null == mapping) {
            return;
        }
        if (this.login_mappings == null) {
            this.login_mappings = HashSet.newHashSet(4);
        }
        this.login_mappings.add(mapping);
    }

    /**
     * Adds login mapping entries
     *
     * @param mapping The mapping entries
     */
    public final void addLoginMappings(final Collection<String> mapping) {
        if (null == mapping) {
            return;
        }
        if (this.login_mappings == null) {
            this.login_mappings = HashSet.newHashSet(4);
        }
        for (final String sMapping : mapping) {
            if (null != sMapping) {
                this.login_mappings.add(sMapping);
            }
        }
    }

    /**
     * Remove a login mapping.
     *
     * @param mapping The mapping entry to remove
     * @return <code>true</code> if the mapping was removed, <code>false</code> otherwise
     */
    public final boolean removeLoginMapping(final String mapping) {
        if (null == mapping) {
            return false;
        }
        return null != this.login_mappings ? this.login_mappings.remove(mapping) : false;
    }

    /**
     * Removes login mapping entries
     *
     * @param mapping The mapping entries to remove
     * @return <code>true</code> if at least one mappings was removed, <code>false</code> otherwise
     */
    public final boolean removeLoginMappings(final Collection<String> mapping) {
        if (null == mapping) {
            return false;
        }
        return null != this.login_mappings ? this.login_mappings.removeAll(mapping) : false;
    }

    /**
     * Gets the login mappings
     *
     * @return The mapping set
     */
    public final HashSet<String> getLoginMappings() {
        return this.login_mappings;
    }

    /**
     * Gets the filestore id
     *
     * @return The filestore id
     */
    public final Integer getFilestoreId() {
        return filestore_id;
    }

    /**
     * Sets the filestore id
     *
     * @param filestore_id The filestore id to set
     */
    public final void setFilestoreId(final Integer filestore_id) {
        this.filestore_id = filestore_id;
        this.filestore_idset = true;
    }

    /**
     * @return max Quota (in MB)
     */
    public final Long getMaxQuota() {
        return maxQuota;
    }

    /**
     *
     * @param maxQuota (in MB)
     */
    public final void setMaxQuota(final Long maxQuota) {
        this.maxQuota = maxQuota;
        this.maxQuotaset = true;
    }

    /**
     * @return used Quota (in MB)
     */
    public final Long getUsedQuota() {
        return usedQuota;
    }

    /**
     * Sets the used quota
     *
     * @param usedQuota The used quota to set
     */
    public final void setUsedQuota(final Long usedQuota) {
        this.usedQuota = usedQuota;
        this.usedQuotaset = true;
    }

    /**
     * Gets the maintenance reason
     *
     * @return The reason
     */
    public final MaintenanceReason getMaintenanceReason() {
        return maintenanceReason;
    }

    /**
     * Sets the maintenance reason
     *
     * @param maintenanceReason The reason to set
     */
    public final void setMaintenanceReason(final MaintenanceReason maintenanceReason) {
        this.maintenanceReason = maintenanceReason;
        this.maintenanceReasonset = true;
    }

    /**
     * Whether this context is enabled or not
     *
     * @return <code>true</code> if this context is enabled, <code>false</code> otherwise
     */
    public final Boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets the enabled status of this context
     *
     * @param enabled The enabled status
     */
    public final void setEnabled(final Boolean enabled) {
        this.enabled = enabled;
        this.enabledset = true;
    }

    /**
     * Gets the read database
     *
     * @return The read database
     */
    public final Database getReadDatabase() {
        return readDatabase;
    }

    /**
     * Sets the read database
     *
     * @param readDatabase The read database
     */
    public final void setReadDatabase(final Database readDatabase) {
        this.readDatabase = readDatabase;
        this.readDatabaseset = true;
    }

    /**
     * Gets the write database
     *
     * @return The write database
     */
    public final Database getWriteDatabase() {
        return writeDatabase;
    }

    /**
     * Sets the write database
     *
     * @param writeDatabase The write database to set
     */
    public final void setWriteDatabase(final Database writeDatabase) {
        this.writeDatabase = writeDatabase;
        this.writeDatabaseset = true;
    }

    /**
     * @return configured average size (in MB)
     */
    public final Long getAverage_size() {
        return average_size;
    }

    /**
     * The context average size can only be configured in AdminDaemon.properties
     *
     * @param average_size The average size
     */
    public final void setAverage_size(final Long average_size) {
        this.average_size = average_size;
        this.average_sizeset = true;
    }

    /**
     * Gets the filestore name
     *
     * @return The filestore name
     */
    public final String getFilestore_name() {
        return filestore_name;
    }

    /**
     * Sets the filestore name
     *
     * @param filestore_name The filestore name
     */
    public final void setFilestore_name(final String filestore_name) {
        this.filestore_name = filestore_name;
        this.filestore_nameset = true;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append("[ \n");
        for (final Field f : this.getClass().getDeclaredFields()) {
            try {
                Object ob = f.get(this);
                String tname = f.getName();
                if (ob != null && !tname.equals("serialVersionUID")) {
                    ret.append("  ");
                    ret.append(tname);
                    ret.append(": ");
                    ret.append(ob);
                    ret.append("\n");
                }
            } catch (IllegalArgumentException e) {
                ret.append("IllegalArgument\n");
            } catch (IllegalAccessException e) {
                ret.append("IllegalAccessException\n");
            }
        }
        ret.append(']');
        return ret.toString();
    }

    private void init() {
        initExtendable();
        this.id = null;
        this.name = null;
        this.enabled = Boolean.FALSE;
        this.filestore_id = null;
        this.average_size = null;
        this.maintenanceReason = null;
        this.maxQuota = null;
        this.usedQuota = null;
        this.readDatabase = null;
        this.writeDatabase = null;
        this.login_mappings = null;
        this.listrun = false;
        this.quotas = Collections.emptyList();
    }

    /**
     * At the moment no fields are defined here
     */
    @Override
    public String[] getMandatoryMembersChange() {
        return null;
    }

    /**
     * At the moment {@link #setId} and {@link #setMaxQuota} are defined here
     */
    @Override
    public String[] getMandatoryMembersCreate() {
        return new String[]{ "id", "maxQuota" };
    }

    /**
     * At the moment no fields are defined here
     */
    @Override
    public String[] getMandatoryMembersDelete() {
        return null;
    }

    /**
     * At the moment no fields are defined here
     */
    @Override
    public String[] getMandatoryMembersRegister() {
        return null;
    }

    /**
     * Gets the quotas
     * <pre>
     *  (["calendar", 1000000L], ["contact", 1000000L], ...)
     * </pre>
     *
     * @return The quotas
     */
    public List<Object[]> getQuotas() {
        return Collections.unmodifiableList(quotas);
    }

    /**
     * Sets the quotas
     * <pre>
     *  (["calendar", 1000000L], ["contact", 1000000L], ...)
     * </pre>
     *
     * @param quotas The quotas to set
     */
    public void setQuotas(List<Object[]> quotas) {
        if (null == quotas) {
            this.quotas = Collections.emptyList();
        } else {
            this.quotas = new ArrayList<>(quotas);
        }
    }

    /**
     * @return the average_sizeset
     */
    public boolean isAverage_sizeset() {
        return average_sizeset;
    }

    /**
     * @return the enabledset
     */
    public boolean isEnabledset() {
        return enabledset;
    }

    /**
     * @return the filestore_idset
     */
    public boolean isFilestore_idset() {
        return filestore_idset;
    }

    /**
     * @return the filestore_nameset
     */
    public boolean isFilestore_nameset() {
        return filestore_nameset;
    }

    /**
     * @return the idset
     */
    public boolean isIdset() {
        return idset;
    }

    /**
     * @return the maintenanceReasonset
     */
    public boolean isMaintenanceReasonset() {
        return maintenanceReasonset;
    }

    /**
     * @return the maxQuotaset
     */
    public boolean isMaxQuotaset() {
        return maxQuotaset;
    }

    /**
     * @return the nameset
     */
    public boolean isNameset() {
        return nameset;
    }

    /**
     * @return the readDatabaseset
     */
    public boolean isReadDatabaseset() {
        return readDatabaseset;
    }

    /**
     * @return the usedQuotaset
     */
    public boolean isUsedQuotaset() {
        return usedQuotaset;
    }

    /**
     * @return the writeDatabaseset
     */
    public boolean isWriteDatabaseset() {
        return writeDatabaseset;
    }

    /**
     * Sets a generic user attribute
     *
     * @param namespace The attribute namespace
     * @param name The attribute name
     * @param value The attribute value
     */
    public void setUserAttribute(String namespace, String name, String value) {
        getNamespace(namespace).put(name, value);
        userAttribtuesset = true;
    }

    /**
     * Read a generic user attribute
     *
     * @param namespace The attribute namespace
     * @param name The name of the attribute
     * @return The user attribute
     */
    public String getUserAttribute(String namespace, String name) {
        return getNamespace(namespace).get(name);
    }

    /**
     * Gets the user attributes
     *
     * @return The attributes
     */
    public Map<String, Map<String, String>> getUserAttributes() {
        if (userAttributes == null) {
            userAttributes = new HashMap<>();
        }
        return userAttributes;
    }

    /**
     * Sets the user attribute
     *
     * @param userAttributes The attributes
     */
    public void setUserAttributes(Map<String, Map<String, String>> userAttributes) {
        this.userAttribtuesset = true;
        this.userAttributes = userAttributes;
    }

    /**
     * Gets the namespace
     *
     * @param namespace The namespace to get
     * @return The namespace
     */
    public Map<String, String> getNamespace(String namespace) {
        if (userAttributes == null) {
            userAttributes = new HashMap<>();
        }
        Map<String, String> ns = userAttributes.get(namespace);
        if (ns == null) {
            ns = new HashMap<>();
            userAttributes.put(namespace, ns);
        }
        return ns;
    }

    /**
     * Used to check if the user attributes have been modified
     *
     * @return <code>true</code> if the user attributes is set, <code>false</code> otherwise
     */
    public boolean isUserAttributesset() {
        return userAttribtuesset;
    }

    /**
     * Sets the {@link GABMode}
     *
     * @param mode THe mode to use for the context
     */
    public void setGABMode(GABMode mode) {
        this.gabMode = mode;
    }

    /**
     * Gets the {@link GABMode} for the context
     *
     * @return The {@link GABMode} or <code>null</code> if not set
     */
    public GABMode getGABMode() {
        return gabMode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((average_size == null) ? 0 : average_size.hashCode());
        result = prime * result + (average_sizeset ? 1231 : 1237);
        result = prime * result + ((enabled == null) ? 0 : enabled.hashCode());
        result = prime * result + (enabledset ? 1231 : 1237);
        result = prime * result + ((filestore_id == null) ? 0 : filestore_id.hashCode());
        result = prime * result + (filestore_idset ? 1231 : 1237);
        result = prime * result + ((filestore_name == null) ? 0 : filestore_name.hashCode());
        result = prime * result + (filestore_nameset ? 1231 : 1237);
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + (idset ? 1231 : 1237);
        result = prime * result + ((login_mappings == null) ? 0 : login_mappings.hashCode());
        result = prime * result + ((maintenanceReason == null) ? 0 : maintenanceReason.hashCode());
        result = prime * result + (maintenanceReasonset ? 1231 : 1237);
        result = prime * result + ((maxQuota == null) ? 0 : maxQuota.hashCode());
        result = prime * result + (maxQuotaset ? 1231 : 1237);
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + (nameset ? 1231 : 1237);
        result = prime * result + ((readDatabase == null) ? 0 : readDatabase.hashCode());
        result = prime * result + (readDatabaseset ? 1231 : 1237);
        result = prime * result + ((usedQuota == null) ? 0 : usedQuota.hashCode());
        result = prime * result + (usedQuotaset ? 1231 : 1237);
        result = prime * result + ((writeDatabase == null) ? 0 : writeDatabase.hashCode());
        result = prime * result + (writeDatabaseset ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof final Context other)) {
            return false;
        }
        if (average_size == null) {
            if (other.average_size != null) {
                return false;
            }
        } else if (!average_size.equals(other.average_size)) {
            return false;
        }
        if (average_sizeset != other.average_sizeset) {
            return false;
        }
        if (enabled == null) {
            if (other.enabled != null) {
                return false;
            }
        } else if (!enabled.equals(other.enabled)) {
            return false;
        }
        if (enabledset != other.enabledset) {
            return false;
        }
        if (filestore_id == null) {
            if (other.filestore_id != null) {
                return false;
            }
        } else if (!filestore_id.equals(other.filestore_id)) {
            return false;
        }
        if (filestore_idset != other.filestore_idset) {
            return false;
        }
        if (filestore_name == null) {
            if (other.filestore_name != null) {
                return false;
            }
        } else if (!filestore_name.equals(other.filestore_name)) {
            return false;
        }
        if (filestore_nameset != other.filestore_nameset) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (idset != other.idset) {
            return false;
        }
        if (login_mappings == null) {
            if (other.login_mappings != null) {
                return false;
            }
        } else if (!login_mappings.equals(other.login_mappings)) {
            return false;
        }
        if (maintenanceReason == null) {
            if (other.maintenanceReason != null) {
                return false;
            }
        } else if (!maintenanceReason.equals(other.maintenanceReason)) {
            return false;
        }
        if (maintenanceReasonset != other.maintenanceReasonset) {
            return false;
        }
        if (maxQuota == null) {
            if (other.maxQuota != null) {
                return false;
            }
        } else if (!maxQuota.equals(other.maxQuota)) {
            return false;
        }
        if (maxQuotaset != other.maxQuotaset) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (nameset != other.nameset) {
            return false;
        }
        if (readDatabase == null) {
            if (other.readDatabase != null) {
                return false;
            }
        } else if (!readDatabase.equals(other.readDatabase)) {
            return false;
        }
        if (readDatabaseset != other.readDatabaseset) {
            return false;
        }
        if (usedQuota == null) {
            if (other.usedQuota != null) {
                return false;
            }
        } else if (!usedQuota.equals(other.usedQuota)) {
            return false;
        }
        if (usedQuotaset != other.usedQuotaset) {
            return false;
        }
        if (writeDatabase == null) {
            if (other.writeDatabase != null) {
                return false;
            }
        } else if (!writeDatabase.equals(other.writeDatabase)) {
            return false;
        }
        if (writeDatabaseset != other.writeDatabaseset) {
            return false;
        }
        if (gabMode == null) {
            if (other.gabMode != null) {
                return false;
            }
        } else if (!gabMode.equals(other.gabMode)) {
            return false;
        }
        return true;
    }

    /**
     * Whether the context is enabled or not
     *
     * @return <code>true</code> if the context is enabled, <code>false</code> otherwise
     */
    public Boolean getEnabled() {
        return enabled;
    }


    /**
     * This settings are only used internally, don't manipulate the setting here or rely on this methods existence
     *
     * @return the listrun value
     */
    public final boolean isListrun() {
        return listrun;
    }


    /**
     * This settings are only used internally, don't manipulate the setting here or rely on this methods existence
     *
     * @param listrun The listrun value
     */
    public final void setListrun(boolean listrun) {
        this.listrun = listrun;
    }
}
