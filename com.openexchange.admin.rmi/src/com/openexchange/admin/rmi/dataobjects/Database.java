/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.rmi.dataobjects;

import java.lang.reflect.Field;
import com.openexchange.database.DatabaseConstants;

/**
 * This class represents a database.
 *
 * @author <a href="mailto:manuel.kraft@open-xchange.com">Manuel Kraft</a>
 * @author <a href="mailto:carsten.hoeger@open-xchange.com">Carsten Hoeger</a>
 * @author <a href="mailto:dennis.sieben@open-xchange.com">Dennis Sieben</a>
 *
 */
public class Database extends EnforceableDataObject implements NameAndIdObject {

    /**
     * For serialization
     */
    private static final long serialVersionUID = -3068828009821317094L;

    /**
     * The database identified
     */
    private Integer id;

    /**
     * Whether the id has been set or not
     */
    private boolean idset;

    /**
     * The read id
     */
    private Integer read_id;

    /**
     * Whether the read id has been set or not
     */
    private boolean read_idset;

    /**
     * The URL
     */
    private String url;

    /**
     * Whether the URL has been set or not
     */
    private boolean urlset;

    /**
     * The login
     */
    private String login;

    /**
     * Whether the login has been set or not
     */
    private boolean loginset;

    /**
     * The password
     */
    private String password;

    /**
     * Whether the password has been set or not
     */
    private boolean passwordset;

    /**
     * The name
     */
    private String name;

    /**
     * Whether the name has been set or not
     */
    private boolean nameset;

    /**
     * The driver
     */
    private String driver;

    /**
     * Whether the driver has been set or not
     */
    private boolean driverset;

    /**
     * The scheme
     */
    private String scheme;

    /**
     * Whether the scheme has been set or not
     */
    private boolean schemeset;

    /**
     * The maximum units
     */
    private Integer maxUnits;

    /**
     * Whether the maximum units have been set or not
     */
    private boolean maxUnitsset;

    /**
     * The hard pool limit
     */
    private Integer poolHardLimit;

    /**
     * Whether the hard pool limit has been set or not
     */
    private boolean poolHardLimitset;

    /**
     * The initial pool size
     */
    private Integer poolInitial;

    /**
     * Whether the initial pool size has been set or not
     */
    private boolean poolInitialset;

    /**
     * The maximum pool size
     */
    private Integer poolMax;

    /**
     * Whether the maximum pool size has been set or not
     */
    private boolean poolMaxset;

    /**
     * The master id
     */
    private Integer masterId;

    /**
     * Whether the master id has been set or not
     */
    private boolean masterIdset;

    /**
     * The current units
     */
    private Integer currentUnits;

    /**
     * Whether the current units have been set or not
     */
    private boolean currentUnitsset;

    /**
     * The master flag
     */
    private Boolean master;

    /**
     * Whether the master flag has been set or not
     */
    private boolean masterset;

    /**
     * Initializes a new {@link Database}.
     *
     * @param id The database id
     */
    public Database(final int id) {
        super();
        init();
        this.id = Integer.valueOf(id);
    }

    /**
     * Initializes a new {@link Database}.
     *
     * @param id The database id
     * @param schema The database schema
     */
    public Database(final int id, final String schema) {
        super();
        init();
        this.id = Integer.valueOf(id);
        this.scheme = schema;
    }

    /**
     * The copy constructor.
     *
     * @param toCopy To copy from
     */
    public Database(final Database toCopy) {
        super();
        init();
        this.currentUnits = toCopy.currentUnits;
        this.currentUnitsset = toCopy.currentUnitsset;
        this.driver = toCopy.driver;
        this.driverset = toCopy.driverset;
        this.id = toCopy.id;
        this.idset = toCopy.idset;
        this.login = toCopy.login;
        this.loginset = toCopy.loginset;
        this.master = toCopy.master;
        this.masterId = toCopy.masterId;
        this.masterIdset = toCopy.masterIdset;
        this.masterset = toCopy.masterset;
        this.maxUnits = toCopy.maxUnits;
        this.maxUnitsset = toCopy.maxUnitsset;
        this.name = toCopy.name;
        this.nameset = toCopy.nameset;
        this.password = toCopy.password;
        this.passwordset = toCopy.passwordset;
        this.poolHardLimit = toCopy.poolHardLimit;
        this.poolHardLimitset = toCopy.poolHardLimitset;
        this.poolInitial = toCopy.poolInitial;
        this.poolInitialset = toCopy.poolInitialset;
        this.poolMax = toCopy.poolMax;
        this.poolMaxset = toCopy.poolMaxset;
        this.read_id = toCopy.read_id;
        this.read_idset = toCopy.read_idset;
        this.scheme = toCopy.scheme;
        this.schemeset = toCopy.schemeset;
        this.url = toCopy.url;
        this.urlset = toCopy.urlset;
    }

    /**
     * The copy constructor.
     *
     * @param toCopy To copy from
     * @param schema The database schema
     */
    public Database(final Database toCopy, final String schema) {
        this(toCopy);
        this.scheme = schema;
        this.schemeset = true;
    }

    /**
     * Initializes a new {@link Database}.
     */
    public Database() {
        super();
        init();
    }

    // //////////////////////////////////////////////
    // Getter and Setter
    //
    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public void setId(final Integer val) {
        this.id = val;
        this.idset = true;
    }

    /**
     * Gets the url
     *
     * @return the url
     */
    public String getUrl() {
        return this.url;
    }

    /**
     * Sets the url
     *
     * @param url The url to set
     */
    public void setUrl(final String url) {
        this.url = url;
        this.urlset = true;
    }

    /**
     * Gets the login
     *
     * @return The login
     */
    public String getLogin() {
        return this.login;
    }

    /**
     * Sets the login
     *
     * @param login The login to set
     */
    public void setLogin(final String login) {
        this.login = login;
        this.loginset = true;
    }

    /**
     * Gets the password
     *
     * @return The password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Sets the password
     *
     * @param password The password to set
     */
    public void setPassword(final String password) {
        this.password = password;
        this.passwordset = true;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(final String val) {
        this.name = val;
        this.nameset = true;
    }

    /**
     * Gets the driver for this database
     *
     * @deprecated Ignore this driver and use {@link DatabaseConstants#DRIVER} instead
     * @return The driver
     */
    @Deprecated
    public String getDriver() {
        return this.driver;
    }

    /**
     * Sets the driver for this database
     *
     * @deprecated Setting this driver has no effect
     * @param val The driver
     */
    @Deprecated
    public void setDriver(final String val) {
        this.driver = val;
        this.driverset = true;
    }

    /**
     * Gets the scheme
     *
     * @return The scheme
     */
    public String getScheme() {
        return this.scheme;
    }

    /**
     * Sets the scheme
     *
     * @param scheme The scheme to set
     */
    public void setScheme(final String scheme) {
        this.scheme = scheme;
        this.schemeset = true;
    }

    /**
     * Gets the max units
     *
     * @return The max units
     */
    public Integer getMaxUnits() {
        return this.maxUnits;
    }

    /**
     * Sets the max units
     *
     * @param maxunits The max units to set
     */
    public void setMaxUnits(final Integer maxunits) {
        this.maxUnits = maxunits;
        this.maxUnitsset = true;
    }

    /**
     * Gets the initial pool size
     *
     * @return The initial pool size
     */
    public Integer getPoolInitial() {
        return this.poolInitial;
    }

    /**
     * Sets the initial pool size
     *
     * @param poolInitial The pool size to set
     */
    public void setPoolInitial(final Integer poolInitial) {
        this.poolInitial = poolInitial;
        this.poolInitialset = true;
    }

    /**
     * Gets the maximal pool size
     *
     * @return The maximal pool size
     */
    public Integer getPoolMax() {
        return this.poolMax;
    }

    /**
     * Sets the maximal pool size
     *
     * @param poolMax The maximal pool size
     */
    public void setPoolMax(final Integer poolMax) {
        this.poolMax = poolMax;
        this.poolMaxset = true;
    }

    /**
     * Gets the master id
     *
     * @return The master id
     */
    public Integer getMasterId() {
        return this.masterId;
    }

    /**
     * Sets the identifier of the associated master database.
     * <p>
     * Implicitly marks this database as a slave (suitable for read-only accesses)
     *
     * @param masterId The identifier of the master database
     */
    public void setMasterId(final Integer masterId) {
        this.masterId = masterId;
        this.masterIdset = true;
    }

    /**
     * Signals if this database is a master (suitable for read-write accesses)
     *
     * @return <code>true</code> if master; otherwise <code>false</code>
     */
    public Boolean isMaster() {
        return this.master;
    }

    /**
     * Sets if this database is a master (suitable for read-write accesses)
     *
     * @param master <code>true</code> if master; otherwise <code>false</code>
     */
    public void setMaster(final Boolean master) {
        this.master = master;
        this.masterset = true;
    }

    /**
     * Gets the read id
     *
     * @return The read id
     */
    public Integer getRead_id() {
        return this.read_id;
    }

    /**
     * Sets the read id
     *
     * @param read_id The read id to set
     */
    public void setRead_id(final Integer read_id) {
        this.read_id = read_id;
        this.read_idset = true;
    }

    /**
     * Gets the current units
     *
     * @return The current units
     */
    public Integer getCurrentUnits() {
        return this.currentUnits;
    }

    /**
     * Sets the current units
     *
     * @param units The current units to set
     */
    public void setCurrentUnits(final Integer units) {
        this.currentUnits = units;
        this.currentUnitsset = true;
    }

    /**
     * Gets the hard limit of the pool
     *
     * @return The hard limit
     */
    public Integer getPoolHardLimit() {
        return this.poolHardLimit;
    }

    /**
     * Sets the pools hard limit
     *
     * @param poolHardLimit The hard limit to set
     */
    public void setPoolHardLimit(final Integer poolHardLimit) {
        this.poolHardLimit = poolHardLimit;
        this.poolHardLimitset = true;
    }

    @Override
    public String toString() {
        final StringBuilder ret = new StringBuilder();
        ret.append("[ \n");
        for (final Field f : this.getClass().getDeclaredFields()) {
            try {
                final Object ob = f.get(this);
                final String tname = f.getName();
                if (ob != null && !tname.equals("serialVersionUID")) {
                    ret.append("  ");
                    ret.append(tname);
                    ret.append(": ");
                    ret.append(ob);
                    ret.append("\n");
                }
            } catch (@SuppressWarnings("unused") IllegalArgumentException e) {
                ret.append("IllegalArgument\n");
            } catch (@SuppressWarnings("unused") IllegalAccessException e) {
                ret.append("IllegalAccessException\n");
            }
        }
        ret.append(" ]");
        return ret.toString();
    }

    /**
     * Initializes all members to the default values
     */
    private void init() {
        this.id = null;
        this.read_id = null;
        this.url = null;
        this.login = null;
        this.password = null;
        this.name = null;
        this.driver = null;
        this.scheme = null;
        this.maxUnits = null;
        this.poolHardLimit = null;
        this.poolInitial = null;
        this.poolMax = null;
        this.masterId = null;
        this.master = null;
        this.currentUnits = null;
    }

    /**
     * At the moment no fields are defined here
     */
    @Override
    public String[] getMandatoryMembersChange() {
        return null;
    }

    /**
     * At the moment {@link #setId}, {@link #setDriver}, {@link #setUrl} and {@link #setScheme} are defined here
     */
    @Override
    public String[] getMandatoryMembersCreate() {
        return new String[] { "id", "driver", "url", "scheme" };
    }

    /**
     * At the moment {@link #setDriver}, {@link #setUrl}, {@link #setScheme}, {@link #setPassword} and {@link #setLogin} are defined here
     */
    @Override
    public String[] getMandatoryMembersDelete() {
        return new String[] { "driver", "url", "scheme", "password", "login" };
    }

    /**
     * At the moment {@link #setPassword}, {@link #setName} and {@link #setMaster} are defined here
     */
    @Override
    public String[] getMandatoryMembersRegister() {
        return new String[] { "password", "name", "master" };
    }

    /**
     * @return the currentUnitsset
     */
    public boolean isCurrentUnitsset() {
        return currentUnitsset;
    }

    /**
     * @return the driverset
     */
    public boolean isDriverset() {
        return driverset;
    }

    /**
     * @return the idset
     */
    public boolean isIdset() {
        return idset;
    }

    /**
     * @return the loginset
     */
    public boolean isLoginset() {
        return loginset;
    }

    /**
     * @return the masterIdset
     */
    public boolean isMasterIdset() {
        return masterIdset;
    }

    /**
     * @return the masterset
     */
    public boolean isMasterset() {
        return masterset;
    }

    /**
     * @return the maxUnitsset
     */
    public boolean isMaxUnitsset() {
        return maxUnitsset;
    }

    /**
     * @return the nameset
     */
    public boolean isNameset() {
        return nameset;
    }

    /**
     * @return the passwordset
     */
    public boolean isPasswordset() {
        return passwordset;
    }

    /**
     * @return the poolHardLimitset
     */
    public boolean isPoolHardLimitset() {
        return poolHardLimitset;
    }

    /**
     * @return the poolInitialset
     */
    public boolean isPoolInitialset() {
        return poolInitialset;
    }

    /**
     * @return the poolMaxset
     */
    public boolean isPoolMaxset() {
        return poolMaxset;
    }

    /**
     * @return the read_idset
     */
    public boolean isRead_idset() {
        return read_idset;
    }

    /**
     * @return the schemeset
     */
    public boolean isSchemeset() {
        return schemeset;
    }

    /**
     * @return the urlset
     */
    public boolean isUrlset() {
        return urlset;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((currentUnits == null) ? 0 : currentUnits.hashCode());
        result = prime * result + (currentUnitsset ? 1231 : 1237);
        result = prime * result + ((driver == null) ? 0 : driver.hashCode());
        result = prime * result + (driverset ? 1231 : 1237);
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + (idset ? 1231 : 1237);
        result = prime * result + ((login == null) ? 0 : login.hashCode());
        result = prime * result + (loginset ? 1231 : 1237);
        result = prime * result + ((master == null) ? 0 : master.hashCode());
        result = prime * result + ((masterId == null) ? 0 : masterId.hashCode());
        result = prime * result + (masterIdset ? 1231 : 1237);
        result = prime * result + (masterset ? 1231 : 1237);
        result = prime * result + ((maxUnits == null) ? 0 : maxUnits.hashCode());
        result = prime * result + (maxUnitsset ? 1231 : 1237);
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + (nameset ? 1231 : 1237);
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + (passwordset ? 1231 : 1237);
        result = prime * result + ((poolHardLimit == null) ? 0 : poolHardLimit.hashCode());
        result = prime * result + (poolHardLimitset ? 1231 : 1237);
        result = prime * result + ((poolInitial == null) ? 0 : poolInitial.hashCode());
        result = prime * result + (poolInitialset ? 1231 : 1237);
        result = prime * result + ((poolMax == null) ? 0 : poolMax.hashCode());
        result = prime * result + (poolMaxset ? 1231 : 1237);
        result = prime * result + ((read_id == null) ? 0 : read_id.hashCode());
        result = prime * result + (read_idset ? 1231 : 1237);
        result = prime * result + ((scheme == null) ? 0 : scheme.hashCode());
        result = prime * result + (schemeset ? 1231 : 1237);
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        result = prime * result + (urlset ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof final Database other)) {
            return false;
        }
        if (currentUnits == null) {
            if (other.currentUnits != null) {
                return false;
            }
        } else if (!currentUnits.equals(other.currentUnits)) {
            return false;
        }
        if (currentUnitsset != other.currentUnitsset) {
            return false;
        }
        if (driver == null) {
            if (other.driver != null) {
                return false;
            }
        } else if (!driver.equals(other.driver)) {
            return false;
        }
        if (driverset != other.driverset) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (idset != other.idset) {
            return false;
        }
        if (login == null) {
            if (other.login != null) {
                return false;
            }
        } else if (!login.equals(other.login)) {
            return false;
        }
        if (loginset != other.loginset) {
            return false;
        }
        if (master == null) {
            if (other.master != null) {
                return false;
            }
        } else if (!master.equals(other.master)) {
            return false;
        }
        if (masterId == null) {
            if (other.masterId != null) {
                return false;
            }
        } else if (!masterId.equals(other.masterId)) {
            return false;
        }
        if (masterIdset != other.masterIdset) {
            return false;
        }
        if (masterset != other.masterset) {
            return false;
        }
        if (maxUnits == null) {
            if (other.maxUnits != null) {
                return false;
            }
        } else if (!maxUnits.equals(other.maxUnits)) {
            return false;
        }
        if (maxUnitsset != other.maxUnitsset) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (nameset != other.nameset) {
            return false;
        }
        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }
        if (passwordset != other.passwordset) {
            return false;
        }
        if (poolHardLimit == null) {
            if (other.poolHardLimit != null) {
                return false;
            }
        } else if (!poolHardLimit.equals(other.poolHardLimit)) {
            return false;
        }
        if (poolHardLimitset != other.poolHardLimitset) {
            return false;
        }
        if (poolInitial == null) {
            if (other.poolInitial != null) {
                return false;
            }
        } else if (!poolInitial.equals(other.poolInitial)) {
            return false;
        }
        if (poolInitialset != other.poolInitialset) {
            return false;
        }
        if (poolMax == null) {
            if (other.poolMax != null) {
                return false;
            }
        } else if (!poolMax.equals(other.poolMax)) {
            return false;
        }
        if (poolMaxset != other.poolMaxset) {
            return false;
        }
        if (read_id == null) {
            if (other.read_id != null) {
                return false;
            }
        } else if (!read_id.equals(other.read_id)) {
            return false;
        }
        if (read_idset != other.read_idset) {
            return false;
        }
        if (scheme == null) {
            if (other.scheme != null) {
                return false;
            }
        } else if (!scheme.equals(other.scheme)) {
            return false;
        }
        if (schemeset != other.schemeset) {
            return false;
        }
        if (url == null) {
            if (other.url != null) {
                return false;
            }
        } else if (!url.equals(other.url)) {
            return false;
        }
        if (urlset != other.urlset) {
            return false;
        }
        return true;
    }

    /**
     * Whether the database is the master
     *
     * @return <code>true</code> if this database is the master, <code>false</code> otherwise
     */
    public Boolean getMaster() {
        return master;
    }
}
