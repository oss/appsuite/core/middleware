/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.rmi.exceptions;

import java.util.Collections;
import java.util.Map;
import com.openexchange.exception.AbstractResponseCodeAwareException;

/**
 * {@link TooManyRequestsException} is an {@link AbstractResponseCodeAwareException} for too many requests.
 *
 * This error should be thrown in case an admin makes too many requests.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class TooManyRequestsException extends AbstractResponseCodeAwareException {

    private static final long serialVersionUID = 6835563061440115800L;

    /**
     * Initializes a new {@link TooManyRequestsException}.
     *
     * @param retryAfter The seconds to wait until the client can try again
     */
    public TooManyRequestsException(long retryAfter) {
        super(429, createHeaderMap(retryAfter));
    }

    /**
     * Initializes a new {@link TooManyRequestsException}.
     *
     * @param message The error message
     * @param retryAfter The seconds to wait until the client can try again
     */
    public TooManyRequestsException(String message, long retryAfter) {
        super(message, 429, createHeaderMap(retryAfter));

    }

    /**
     * Initializes a new {@link TooManyRequestsException}.
     *
     * @param cause The error cause
     * @param retryAfter The seconds to wait until the client can try again
     */
    public TooManyRequestsException(Throwable cause, long retryAfter) {
        super(cause, 429, createHeaderMap(retryAfter));

    }

    /**
     * Initializes a new {@link TooManyRequestsException}.
     *
     * @param message The error message
     * @param cause The error cause
     * @param retryAfter The seconds to wait until the client can try again
     */
    public TooManyRequestsException(String message, Throwable cause, long retryAfter) {
        super(message, cause, 429, createHeaderMap(retryAfter));
    }

    /**
     * Creates a header map for the given retryAfter seconds
     *
     * @param retryAfter The retryAfter seconds
     * @return The map
     */
    private static final Map<String, String> createHeaderMap(long retryAfter) {
        return Collections.singletonMap("Retry-After", String.valueOf(retryAfter));
    }

}
