/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.admin.rmi.exceptions;


/**
 * This exception is thrown when the underlying database is locked due to an update or so
 *
 * @author dsieben
 */
public class DatabaseLockedException extends AbstractAdminRmiException {

    /**
     * For serialization
     */
    private static final long serialVersionUID = -3944701775507953137L;

    /**
     * Default constructor
     */
    public DatabaseLockedException() {

    }

    /**
     * Initializes a new {@link DatabaseLockedException}.
     *
     * @param message The message
     */
    public DatabaseLockedException(String message) {
        super(message);

    }

    /**
     * Initializes a new {@link DatabaseLockedException}.
     *
     * @param cause The cause for this error
     */
    public DatabaseLockedException(Throwable cause) {
        super(cause);

    }

    /**
     * Initializes a new {@link DatabaseLockedException}.
     *
     * @param message The message
     * @param cause The cause for this error
     */
    public DatabaseLockedException(String message, Throwable cause) {
        super(message, cause);
    }

}
