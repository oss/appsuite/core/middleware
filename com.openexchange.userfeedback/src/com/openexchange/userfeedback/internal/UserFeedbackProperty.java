/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.userfeedback.internal;

import static com.openexchange.java.Autoboxing.B;
import com.openexchange.config.lean.Property;

/**
 * {@link UserFeedbackProperty} - Property enumeration for user feedback module.
 *
 * @author <a href="vitali.sjablow@open-xchange.com">Vitali Sjablow</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.4
 */
public enum UserFeedbackProperty implements Property{

    /**
     * Whether user feedback is enabled
     */
    ENABLED("enabled", B(true)),
    /**
     * The user feedback mode
     */
    MODE("mode", "star-rating-v1"),
    ;

    private final String fqn;
    private final Object defaultValue;

    /**
     * Initializes a new {@link UserFeedbackProperty}.
     *
     * @param appendix The appendix for the fully-qualifying name
     * @param defaultValue The default value
     */
    private UserFeedbackProperty(String appendix, Object defaultValue) {
        this.fqn = "com.openexchange.userfeedback." + appendix;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
