/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.gdpr.dataexport.provider.mail.internal;

import static com.openexchange.gdpr.dataexport.DataExportProviders.getBoolOption;
import static com.openexchange.gdpr.dataexport.DataExportProviders.isPermissionDenied;
import static com.openexchange.gdpr.dataexport.DataExportProviders.isRetryableExceptionAndMayFail;
import static com.openexchange.gdpr.dataexport.provider.general.SavePointAndReason.savePointFor;
import static com.openexchange.java.Autoboxing.I;
import java.io.InputStream;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.config.cascade.ComposedConfigProperty;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.exception.OXException;
import com.openexchange.gdpr.dataexport.DataExportAbortedException;
import com.openexchange.gdpr.dataexport.DataExportExceptionCode;
import com.openexchange.gdpr.dataexport.DataExportSink;
import com.openexchange.gdpr.dataexport.DataExportTask;
import com.openexchange.gdpr.dataexport.Directory;
import com.openexchange.gdpr.dataexport.ExportResult;
import com.openexchange.gdpr.dataexport.Item;
import com.openexchange.gdpr.dataexport.Message;
import com.openexchange.gdpr.dataexport.Module;
import com.openexchange.gdpr.dataexport.provider.general.AbstractDataExportProviderTask;
import com.openexchange.gdpr.dataexport.provider.general.SavePointAndReason;
import com.openexchange.gdpr.dataexport.provider.mail.MailDataExportPropertyNames;
import com.openexchange.gdpr.dataexport.provider.mail.generator.FailedAuthenticationResult;
import com.openexchange.gdpr.dataexport.provider.mail.generator.SessionGenerator;
import com.openexchange.java.Collators;
import com.openexchange.java.Streams;
import com.openexchange.java.Strings;
import com.openexchange.java.util.UUIDs;
import com.openexchange.mail.MailField;
import com.openexchange.mail.MailSortField;
import com.openexchange.mail.OrderDirection;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailFolderStorageInfoSupport;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.mail.dataobjects.MailFolder;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.mime.utils.MimeMessageUtility;
import com.openexchange.mail.service.MailService;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.GeneratedSession;

/**
 * {@link MailDataExport}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.3
 */
public class MailDataExport extends AbstractDataExportProviderTask {

    /** The logger constant */
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(MailDataExport.class);

    private static final String JSON_FIELD_ID = "id";
    private static final String JSON_FIELD_PATH = "path";
    private static final String JSON_FIELD_FOLDER = "folder";

    private static final String ID_MAIL = "mail";

    /** The full name for the virtual "all messages" folder */
    private static String allMessagesFolder(int userId, int contextId, ServiceLookup services) throws OXException {
        ConfigViewFactory factory = services.getService(ConfigViewFactory.class);
        ConfigView view = factory.getView(userId, contextId);

        ComposedConfigProperty<String> property = view.property("com.openexchange.find.basic.mail.allMessagesFolder", String.class);
        String fn = property.get();
        return Strings.isEmpty(fn) ? null : fn;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final SessionGeneratorRegistry generatorRegistry;

    /**
     * Initializes a new {@link MailDataExport}.
     *
     * @param sink The sink to output to
     * @param savepoint The optional save-point previously set by this provider
     * @param task The data export task providing needed arguments
     * @param locale The locale
     * @param generatorRegistry The generator registry
     * @param services The service look-up
     */
    public MailDataExport(DataExportSink sink, Optional<JSONObject> savepoint, DataExportTask task, Locale locale, SessionGeneratorRegistry generatorRegistry, ServiceLookup services) {
        super(ID_MAIL, sink, savepoint, task, locale, services);
        this.generatorRegistry = generatorRegistry;
    }

    /**
     * Exports mail messages.
     *
     * @return The export result
     * @throws OXException If export fails
     */
    @Override
    public ExportResult export() throws OXException {
        MailService mailService = services.getServiceSafe(MailService.class);

        MailOperationExecutor mailOperationExecutor = null;
        try {
            Module mailModule = getModule();

            SessionGenerator sessionGenerator;
            {
                String generatorId = getProperty(MailDataExportPropertyNames.PROP_SESSION_GENERATOR, mailModule);
                sessionGenerator = generatorRegistry.getGeneratorById(generatorId);
            }

            GeneratedSession session = generateSession(mailModule, sessionGenerator);

            boolean includeUnsubscribed = getBoolOption(MailDataExportPropertyNames.PROP_INCLUDE_UNSUBSCRIBED, false, mailModule);
            boolean includePublicFolders = getBoolOption(MailDataExportPropertyNames.PROP_INCLUDE_PUBLIC_FOLDERS, false, mailModule);
            boolean includeSharedFolders = getBoolOption(MailDataExportPropertyNames.PROP_INCLUDE_SHARED_FOLDERS, false, mailModule);
            boolean includeTrashFolder = getBoolOption(MailDataExportPropertyNames.PROP_INCLUDE_TRASH_FOLDER, false, mailModule);
            Options options = new Options(includeUnsubscribed, includePublicFolders, includeSharedFolders, includeTrashFolder);

            StartInfo startInfo;
            if (savepoint.isPresent()) {
                JSONObject jSavePoint = savepoint.get();
                startInfo = new StartInfo(jSavePoint.optString(JSON_FIELD_ID, null), jSavePoint.getString(JSON_FIELD_FOLDER), jSavePoint.optString(JSON_FIELD_PATH, null));
            } else {
                startInfo = null;
            }

            tryTouch();

            // Connect
            while (mailOperationExecutor == null) {
                MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess = null;
                try {
                    mailAccess = mailService.getMailAccess(session, 0);
                    mailAccess.connect();
                    mailOperationExecutor = new MailOperationExecutor(mailModule, sessionGenerator, mailAccess, task, services);
                    mailAccess = null;
                } catch (OXException e) {
                    // Check for failed authentication
                    if (!MailAccess.isAuthFailed(e)) {
                        throw e;
                    }

                    // Retry with new generated session
                    if (mailAccess != null) {
                        LOG.debug("Failed authentication against mail server '{}' during data export {} of user {} in context {}. Trying to establish a new session...", mailAccess.getMailConfig().getServer(), UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()));
                    }
                    FailedAuthenticationResult failedAuthenticationResult = sessionGenerator.onFailedAuthentication(e, session, mailModule.getProperties().get());
                    if (!failedAuthenticationResult.retry()) {
                        throw e;
                    }
                    session = failedAuthenticationResult.getOptionalSession().get();
                } finally {
                    MailAccess.closeInstance(mailAccess);
                }
            }

            String allMessagesFolder = allMessagesFolder(session.getUserId(), session.getContextId(), services);
            if (Strings.isNotEmpty(allMessagesFolder)) {
                DefaultFolder.setFullNamesToIgnore(Collections.singleton(allMessagesFolder));
            }

            MailOperationExecutor executor = mailOperationExecutor;
            Folder rootFolder =  mailOperationExecutor.executeWithRetryOnConnectionLoss(mailAccess -> {
                IMailFolderStorage folderStorage = mailAccess.getFolderStorage();
                IMailFolderStorageInfoSupport infoSupport = folderStorage.supports(IMailFolderStorageInfoSupport.class);
                if (null != infoSupport && infoSupport.isInfoSupported()) {
                    return new DefaultFolder(infoSupport.getFolderInfo(MailFolder.ROOT_FOLDER_ID), executor);
                }
                return new DefaultFolder(folderStorage.getRootFolder(), executor);
            });

            mailOperationExecutor.getCurrentMailAccess().setWaiting(true); // Mark as "waiting" to prevent from mail access watcher outputting false-positive warnings
            try {
                SavePointAndReason optSavePoint = traverseFolder(rootFolder, null, startInfo, options, mailOperationExecutor);
                if (optSavePoint != null) {
                    return optSavePoint.result();
                }
            } finally {
                mailOperationExecutor.getCurrentMailAccess().setWaiting(false); // Unset "waiting" flag
            }

            tryTouch();
            return ExportResult.completed();
        } catch (OXException e) {
            if (isRetryableExceptionAndMayFail(e, sink)) {
                LOG.debug("Encountered retryable error during mail data export {} of user {} in context {}", UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()), e);
                return ExportResult.incomplete(savepoint, Optional.of(e));
            }
            throw e;
        } catch (JSONException e) {
            throw DataExportExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        } catch (DataExportAbortedException e) {
            return ExportResult.aborted();
        } catch (Exception e) {
            if (isRetryableExceptionAndMayFail(e, sink)) {
                LOG.debug("Encountered retryable error during mail data export {} of user {} in context {}", UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()), e);
                return ExportResult.incomplete(savepoint, Optional.of(e));
            }
            throw OXException.general(e.getMessage(), e);
        } finally {
            MailAccess.closeInstance(mailOperationExecutor == null ? null : mailOperationExecutor.getCurrentMailAccess());
        }
    }

    private SavePointAndReason traverseFolder(Folder folder, String parentPath, StartInfo startInfo, Options options, MailOperationExecutor mailOperationExecutor) throws OXException, DataExportAbortedException {
        if (isPauseRequested()) {
            if (startInfo != null) {
                JSONObject jSavePoint = new JSONObject(4);
                jSavePoint.putSafe(JSON_FIELD_FOLDER, startInfo.fullName);
                if (startInfo.path != null) {
                    jSavePoint.putSafe(JSON_FIELD_PATH, startInfo.path);
                }
                if (startInfo.mailId != null) {
                    jSavePoint.putSafe(JSON_FIELD_ID, startInfo.mailId);
                }
                return savePointFor(jSavePoint);
            }

            return savePointFor(new JSONObject(2).putSafe(JSON_FIELD_FOLDER, folder.getFullname()));
        }
        checkAborted();

        String pathOfFolder = null;
        StartInfo info = startInfo;
        if (info == null || info.fullName.equals(folder.getFullname())) {
            if (info == null || info.path == null) {
                pathOfFolder = exportFolder(folder, parentPath);
                if (pathOfFolder == null) {
                    return savePointFor(new JSONObject(2).putSafe(JSON_FIELD_FOLDER, folder.getFullname()));
                }
                LOG.debug("Exported mail directory \"{}\" for data export {} of user {} in context {}", folder.getFullname(), UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()));
            } else {
                pathOfFolder = info.path;
            }

            if (folder.isHoldsMessages()) {
                SavePointAndReason jSavePoint = exportMessages(folder.getFullname(), pathOfFolder, info == null ? null : info.mailId, mailOperationExecutor);
                if (jSavePoint != null) {
                    return jSavePoint;
                }
            }

            info = null;
        }

        List<Folder> children;
        try {
            children = folder.getChildren(false == options.includeUnsubscribed);
        } catch (Exception e) {
            if (isRetryableExceptionAndMayFail(e, sink)) {
                LOG.warn("Encountered retryable error during retrieval of subfolders of directory \"{}\" from primary mail account for data export {} of user {} in context {}", folder.getFullname(), UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()), e);
                if (info != null) {
                    JSONObject jSavePoint = new JSONObject(4);
                    jSavePoint.putSafe(JSON_FIELD_FOLDER, info.fullName);
                    if (info.path != null) {
                        jSavePoint.putSafe(JSON_FIELD_PATH, info.path);
                    }
                    if (info.mailId != null) {
                        jSavePoint.putSafe(JSON_FIELD_ID, info.mailId);
                    }
                    return savePointFor(jSavePoint, e);
                }

                return savePointFor(new JSONObject(2).putSafe(JSON_FIELD_FOLDER, folder.getFullname()), e);
            }
            LOG.warn("Failed to retrieve subfolders of directory \"{}\" from primary mail account for data export {} of user {} in context {}", folder.getFullname(), UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()), e);
            sink.addToReport(Message.builder().appendToMessage("Failed to retrieve subfolders of folder \"").appendToMessage(folder.getFullname()).appendToMessage("\": ").appendToMessage(e.getMessage()).withModuleId(ID_MAIL).withTimeStamp(new Date()).build());
            children = Collections.emptyList();
        }

        if (!children.isEmpty()) {
            children = children.stream().filter(new FolderPredicate(options)).sorted(new FolderFullNameComparator(locale)).collect(Collectors.toList());

            if (pathOfFolder == null) {
                if (startInfo == null || startInfo.path == null) {
                    pathOfFolder = folder.isRootFolder() ? "" : (parentPath == null ? "" : parentPath) + sanitizeNameForZipEntry(folder.getName()) + "/";
                } else {
                    pathOfFolder = startInfo.path;
                }
            }
            for (Folder child : children) {
                SavePointAndReason jSavePoint = traverseFolder(child, pathOfFolder, info, options, mailOperationExecutor);
                if (jSavePoint != null) {
                    return jSavePoint;
                }
            }
        }

        return null;
    }

    private String exportFolder(Folder folderInfo, String path) throws OXException {
        if (folderInfo.isRootFolder()) {
            return "";
        }

        return sink.export(new Directory(path, sanitizeNameForZipEntry(folderInfo.getName())));
    }

    private static final MailField[] FIELDS_ID = new MailField[] { MailField.ID };

    private SavePointAndReason exportMessages(String fullName, String path, String startMailId, MailOperationExecutor mailOperationExecutor) throws OXException, DataExportAbortedException {
        if (isPauseRequested()) {
            JSONObject jSavePoint = new JSONObject(4);
            jSavePoint.putSafe(JSON_FIELD_FOLDER, fullName).putSafe(JSON_FIELD_PATH, path);
            if (startMailId != null) {
                jSavePoint.putSafe(JSON_FIELD_ID, startMailId);
            }
            return savePointFor(jSavePoint);
        }
        checkAborted();

        MailMessage[] messages;
        try {
            messages = mailOperationExecutor.executeWithRetryOnConnectionLoss(mailAccess -> mailAccess.getMessageStorage().getAllMessages(fullName, null, MailSortField.RECEIVED_DATE, OrderDirection.ASC, FIELDS_ID));
            if (messages.length <= 0) {
                // No messages in given folder
                LOG.debug("No mails to export in directory \"{}\" for data export {} of user {} in context {}", fullName, UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()));
                return null;
            }

            if (isPauseRequested()) {
                JSONObject jSavePoint = new JSONObject(4).putSafe(JSON_FIELD_FOLDER, fullName).putSafe(JSON_FIELD_PATH, path);
                if (startMailId != null) {
                    jSavePoint.putSafe(JSON_FIELD_ID, startMailId);
                }
                return savePointFor(jSavePoint);
            }
            checkAborted();

            if (startMailId != null) {
                boolean found = false;
                int index = 0;
                while (!found && index < messages.length) {
                    MailMessage m = messages[index];
                    if (m.getMailId().equals(startMailId)) {
                        found = true;
                    } else {
                        index++;
                    }
                }

                if (found) {
                    if (index > 0) {
                        int newlength = messages.length - index;
                        MailMessage[] tmp = new MailMessage[newlength];
                        System.arraycopy(messages, index, tmp, 0, newlength);
                        messages = tmp;
                    }
                }
            }

            LOG.debug("Going to export {} mails from directory \"{}\" for data export {} of user {} in context {}", I(messages.length), fullName, UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()));
        } catch (DataExportAbortedException e) {
            throw e;
        } catch (Exception e) {
            if (isRetryableExceptionAndMayFail(e, sink)) {
                LOG.debug("Encountered retryable error during export of messages from directory \"{}\" from primary mail account for data export {} of user {} in context {}", fullName, UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()), e);
                JSONObject jSavePoint = new JSONObject(4).putSafe(JSON_FIELD_FOLDER, fullName).putSafe(JSON_FIELD_PATH, path);
                if (startMailId != null) {
                    jSavePoint.putSafe(JSON_FIELD_ID, startMailId);
                }
                return savePointFor(jSavePoint, e);
            }
            if (MailFolder.ROOT_FOLDER_ID.equals(fullName)) {
                LOG.debug("Failed to export messages from root directory from primary mail account for data export {} of user {} in context {}", UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()), e);
            } else {
                if (isPermissionDenied(e)) {
                    LOG.debug("Forbidden to export messages from directory \"{}\" from primary mail account for data export {} of user {} in context {}", fullName, UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()), e);
                    sink.addToReport(Message.builderWithPermissionDeniedType().appendToMessage("Insufficient permissions to export messages from folder \"").appendToMessage(fullName).appendToMessage("\": ").appendToMessage(e.getMessage()).withModuleId(ID_MAIL).withTimeStamp(new Date()).build());
                } else {
                    LOG.warn("Failed to export messages from directory \"{}\" from primary mail account for data export {} of user {} in context {}", fullName, UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()), e);
                    sink.addToReport(Message.builder().appendToMessage("Failed to export messages from folder \"").appendToMessage(fullName).appendToMessage("\": ").appendToMessage(e.getMessage()).withModuleId(ID_MAIL).withTimeStamp(new Date()).build());
                }
            }
            return null;
        }

        int batchCount = 0;
        for (MailMessage message : messages) {
            String mailId = message.getMailId();

            if (isPauseRequested()) {
                JSONObject jSavePoint = new JSONObject(4);
                jSavePoint.putSafe(JSON_FIELD_FOLDER, fullName);
                jSavePoint.putSafe(JSON_FIELD_PATH, path);
                jSavePoint.putSafe(JSON_FIELD_ID, mailId);
                return savePointFor(jSavePoint);
            }
            int count = incrementAndGetCount();
            checkAborted(count % 100 == 0);
            if (count % 1000 == 0) {
                sink.setSavePoint(new JSONObject(4).putSafe(JSON_FIELD_FOLDER, fullName).putSafe(JSON_FIELD_PATH, path).putSafe(JSON_FIELD_ID, mailId));
            }
            batchCount++;

            InputStream stream = null;
            try {
                StreamFromMailPartOperation streamFromMailPartCallable = new StreamFromMailPartOperation(messages.length, batchCount, mailId, fullName, task);
                stream = mailOperationExecutor.executeWithRetryOnConnectionLoss(streamFromMailPartCallable);
                if (stream != null) {
                    boolean exported = sink.export(stream, new Item(path, sanitizeNameForZipEntry(mailId + ".eml"), streamFromMailPartCallable.getSentDate()));
                    if (!exported) {
                        return savePointFor(new JSONObject(4).putSafe(JSON_FIELD_FOLDER, fullName).putSafe(JSON_FIELD_PATH, path).putSafe(JSON_FIELD_ID, mailId));
                    }
                    LOG.debug("Exported mail {} ({} of {}) from directory \"{}\" for data export {} of user {} in context {}", mailId, I(batchCount), I(messages.length), fullName, UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()));
                }
            } catch (Exception e) {
                if (isRetryableExceptionAndMayFail(e, sink)) {
                    LOG.debug("Encountered retryable error during export of export message {} from directory \"{}\" of primary mail account for data export {} of user {} in context {}", mailId, fullName, UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()), e);
                    return savePointFor(new JSONObject(4).putSafe(JSON_FIELD_FOLDER, fullName).putSafe(JSON_FIELD_PATH, path).putSafe(JSON_FIELD_ID, mailId), e);
                }
                LOG.warn("Failed to export message {} from directory \"{}\" of primary mail account for data export {} of user {} in context {}", mailId, fullName, UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()), e);
                sink.addToReport(Message.builder().appendToMessage("Failed to export message \"").appendToMessage(mailId).appendToMessage("\" in folder \"").appendToMessage(fullName).appendToMessage("\": ").appendToMessage(e.getMessage()).withModuleId(ID_MAIL).withTimeStamp(new Date()).build());
            } finally {
                Streams.close(stream);
            }
        }

        return null;
    }

    private static class StreamFromMailPartOperation implements MailOperation<InputStream> {

        private final int messagesLength;
        private final int currentBatchCount;
        private final String mailId;
        private final String fullName;
        private final DataExportTask task;
        private Date sentDate;

        StreamFromMailPartOperation(int messagesLength, int currentBatchCount, String mailId, String fullName, DataExportTask task) {
            super();
            this.messagesLength = messagesLength;
            this.currentBatchCount = currentBatchCount;
            this.mailId = mailId;
            this.fullName = fullName;
            this.task = task;
        }

        @Override
        public InputStream execute(MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess) throws OXException {
            MailMessage m = mailAccess.getMessageStorage().getMessage(fullName, mailId, false);
            if (m == null) {
                LOG.debug("No such mail {} ({} of {}) in directory \"{}\" for data export {} of user {} in context {}", mailId, I(currentBatchCount), I(messagesLength), fullName, UUIDs.getUnformattedStringObjectFor(task.getId()), I(task.getUserId()), I(task.getContextId()));
                return null;
            }

            sentDate = m.getSentDate();
            return MimeMessageUtility.getInputStream(m);
        }

        /**
         * Gets the sent date of the message.
         *
         * @return The sent date
         */
        Date getSentDate() {
            return sentDate;
        }
    }

    // ------------------------------------------------------------ Helpers -------------------------------------------------------------

    private GeneratedSession generateSession(Module mailModule, SessionGenerator sessionGenerator) throws OXException {
        int contextId = task.getContextId();
        int userId = task.getUserId();
        return sessionGenerator.generateSession(userId, contextId, mailModule.getProperties().get());
    }

    private static String getProperty(String propName, Module mailModule) {
        Optional<Map<String, Object>> optionalProps = mailModule.getProperties();
        if (!optionalProps.isPresent()) {
            return null;
        }

        return (String) optionalProps.get().get(propName);
    }

    private static class FolderPredicate implements Predicate<Folder> {

        private final Options options;

        FolderPredicate(Options options) {
            super();
            this.options = options;
        }

        @Override
        public boolean test(Folder folder) {
            if (!options.includeSharedFolders && folder.isShared()) {
                return false;
            }
            if (!options.includePublicFolders && folder.isPublic()) {
                return false;
            }
            if (!options.includeTrashFolder && folder.isTrash()) {
                return false;
            }
            return true;
        }
    }

    private static final class FolderFullNameComparator implements Comparator<Folder> {

        private final Collator collator;

        FolderFullNameComparator(Locale locale) {
            super();
            collator = Collators.getSecondaryInstance(locale);
        }

        @Override
        public int compare(Folder o1, Folder o2) {
            /*
             * Compare by full name
             */
            return collator.compare(o1.getFullname(), o2.getFullname());
        }
    }

    private static class Options {

        final boolean includeUnsubscribed;
        final boolean includePublicFolders;
        final boolean includeSharedFolders;
        final boolean includeTrashFolder;

        Options(boolean includeUnsubscribed, boolean includePublicFolders, boolean includeSharedFolders, boolean includeTrashFolder) {
            super();
            this.includeUnsubscribed = includeUnsubscribed;
            this.includePublicFolders = includePublicFolders;
            this.includeSharedFolders = includeSharedFolders;
            this.includeTrashFolder = includeTrashFolder;
        }
    }

    private static class StartInfo {

        final String fullName;
        final String path;
        final String mailId;

        StartInfo(String mailId, String fullName, String path) {
            super();
            this.mailId = mailId;
            this.fullName = fullName;
            this.path = path;
        }
    }

}
