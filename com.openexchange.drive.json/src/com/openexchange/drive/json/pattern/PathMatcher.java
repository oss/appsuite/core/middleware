/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.drive.json.pattern;

import com.openexchange.exception.OXException;

/**
 * {@link PathMatcher}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public abstract class PathMatcher {

    protected final String pattern;
    protected final boolean caseSensitive;

    /**
     * Initializes a new {@link PathMatcher}.
     *
     * @param pattern The pattern to match
     * @param caseSensitive <code>true</code> to match in a case sensitive way, <code>false</code>, otherwise
     */
    protected PathMatcher(String pattern, boolean caseSensitive) {
        super();
        this.pattern = pattern;
        this.caseSensitive = caseSensitive;
    }

    /**
     * Gets a value indicating whether the supplied string is matched.
     *
     * @param input The input string to match
     * @return <code>true</code> if it matches, <code>false</code>, otherwise
     */
    public abstract boolean matches(String input) throws OXException;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (caseSensitive ? 1231 : 1237);
        result = prime * result + ((pattern == null) ? 0 : pattern.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PathMatcher other = (PathMatcher) obj;
        if (caseSensitive != other.caseSensitive)
            return false;
        if (pattern == null) {
            if (other.pattern != null)
                return false;
        } else if (!pattern.equals(other.pattern))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "PathMatcher [pattern=" + pattern + ", caseSensitive=" + caseSensitive + "]";
    }

}
