/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.drive.json.pattern;

import static com.openexchange.java.MatcherWatcher.DEFAULT_REGEX_TIMEOUT;
import java.util.regex.Pattern;
import com.openexchange.drive.DriveExceptionCodes;
import com.openexchange.exception.OXException;
import com.openexchange.java.MatcherException;
import com.openexchange.java.MatcherWatcher;

/**
 * {@link RegexMatcher}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class RegexMatcher extends PathMatcher {

    private final Pattern compiledPattern;

    /**
     * Initializes a new {@link RegexMatcher}.
     *
     * @param pattern The pattern to match
     * @param caseSensitive <code>true</code> to match in a case sensitive way, <code>false</code>, otherwise
     */
    RegexMatcher(String pattern, boolean caseSensitive) {
        super(pattern, caseSensitive);
        int flags = Pattern.CANON_EQ | Pattern.UNICODE_CASE;
        if (false == caseSensitive) {
            flags |= Pattern.CASE_INSENSITIVE;
        }
        this.compiledPattern = Pattern.compile(pattern, flags);
    }

    @Override
    public boolean matches(String input) throws OXException {
        try {
            return MatcherWatcher.matches(compiledPattern, input, DEFAULT_REGEX_TIMEOUT);
        } catch (MatcherException e) {
            throw DriveExceptionCodes.INVALID_PATTERN.create(e, pattern, e.getMessage());
        }
    }

}
