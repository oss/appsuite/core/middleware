/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.drive.json.pattern;

/**
 * {@link GlobMatcher}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class GlobMatcher extends PathMatcher {

    private final String glob;

    /**
     * Initializes a new {@link GlobMatcher}.
     *
     * @param pattern The pattern to match
     * @param caseSensitive <code>true</code> to match in a case sensitive way, <code>false</code>, otherwise
     */
    GlobMatcher(String pattern, boolean caseSensitive) {
        super(pattern, caseSensitive);
        this.glob = caseSensitive ? pattern : pattern.toLowerCase();
    }

    @Override
    public boolean matches(String value) {
        if (null == value) {
            return false;
        }
        /*
         * loop through pattern and input
         */
        String input = caseSensitive ? value : value.toLowerCase();
        int i = 0;
        int j = 0;
        int asteriskPositionInput = -1;
        int asteriskPositionPattern = -1;
        while (i < input.length()) {
            if (j < glob.length()) {
                /*
                 * evaluate pattern
                 */
                switch (glob.charAt(j)) {
                    case '*':
                        /*
                         * asterisk, remember current indexes and advance pattern
                         */
                        asteriskPositionInput = i;
                        j++;
                        asteriskPositionPattern = j;
                        continue;
                    case '?':
                        /*
                         * questionmark, match any character (advance indexes)
                         */
                        i++;
                        j++;
                        continue;
                    case '\\':
                        /*
                         * escaped character, advance in pattern index and fall-through
                         */
                        if (j + 1 < glob.length()) {
                            j++;
                        }
                    default:
                        /*
                         * proceed if character is matched, otherwise leave to check via asterisk
                         */
                        if (glob.charAt(j) != input.charAt(i)) {
                            break;
                        }
                        i++;
                        j++;
                        continue;
                }
            }
            /*
             * check if covered by asterisk, otherwise no match
             */
            if (asteriskPositionPattern == -1) {
                return false;
            }
            /*
             * advance input & continue matching
             */
            asteriskPositionInput++;
            i = asteriskPositionInput;
            j = asteriskPositionPattern;
        }
        /*
         * end of input string reached - consider as match if pattern was completely consumed, or only asterisk wildcards remaining
         */
        while (j < glob.length() && '*' == glob.charAt(j)) {
            j++;
        }
        return j >= glob.length();
    }

}
