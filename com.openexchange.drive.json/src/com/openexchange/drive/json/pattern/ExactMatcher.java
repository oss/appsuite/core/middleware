/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.drive.json.pattern;

import com.openexchange.java.Strings;

/**
 * {@link ExactMatcher}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class ExactMatcher extends PathMatcher {

    /**
     * Initializes a new {@link ExactMatcher}.
     *
     * @param pattern The pattern to match
     * @param caseSensitive <code>true</code> to match in a case sensitive way, <code>false</code>, otherwise
     */
    ExactMatcher(String pattern, boolean caseSensitive) {
        super(pattern, caseSensitive);
    }

    @Override
    public boolean matches(String input) {
        return caseSensitive ? Strings.equalsNormalized(pattern, input) : Strings.equalsNormalizedIgnoreCase(pattern, input);
    }

}
