/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.jsieve.export.exceptions;

import java.util.Set;
import com.google.common.collect.ImmutableSet;

/**
 * {@link OXSieveHandlerUnsupportedCapabilitiesException} - One or more SIEVE rules shall be written that require unsupported capabilities
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class OXSieveHandlerUnsupportedCapabilitiesException extends Exception {

    private static final long serialVersionUID = 7933802308349671768L;

    private final Set<String> unsupportedCapabilities;

    /**
     * Initializes a new instance of {@link OXSieveHandlerUnsupportedCapabilitiesException}.
     *
     * @param unsupportedCapabilities The capabilities that are required, but not supported
     */
    public OXSieveHandlerUnsupportedCapabilitiesException(Set<String> unsupportedCapabilities) {
        super("SIEVE server does not support all required capabilities");
        this.unsupportedCapabilities = unsupportedCapabilities == null ? null : ImmutableSet.copyOf(unsupportedCapabilities);
    }

    /**
     * Gets the unsupported capabilities.
     *
     * @return The unsupported capabilities or <code>null</code> if not specified
     */
    public Set<String> getUnsupportedCapabilities() {
        return unsupportedCapabilities;
    }

}
