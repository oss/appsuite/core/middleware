/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.jsieve.export.utils;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import com.openexchange.mailfilter.internal.CircuitBreakerInfo;
import net.jodah.failsafe.CircuitBreakerOpenException;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.FailsafeException;

/**
 * {@link FailsafeCircuitBreakerBufferedOutputStream}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @since v7.10.3
 */
@SuppressWarnings("synthetic-access")
public class FailsafeCircuitBreakerBufferedOutputStream extends BufferedOutputStream {

    private final CircuitBreakerInfo circuitBreakerInfo;

    /**
     * Initializes a new {@link FailsafeCircuitBreakerBufferedOutputStream}.
     *
     * @param out The underlying output stream
     * @param circuitBreakerInfo The circuit breaker to use
     */
    public FailsafeCircuitBreakerBufferedOutputStream(OutputStream out, CircuitBreakerInfo circuitBreakerInfo) {
        super(out);
        this.circuitBreakerInfo = circuitBreakerInfo;
    }

    /**
     * Initializes a new {@link FailsafeCircuitBreakerBufferedOutputStream}.
     *
     * @param out The underlying output stream
     * @param size The buffer size
     * @param circuitBreakerInfo The circuit breaker to use
     */
    public FailsafeCircuitBreakerBufferedOutputStream(OutputStream out, int size, CircuitBreakerInfo circuitBreakerInfo) {
        super(out, size);
        this.circuitBreakerInfo = circuitBreakerInfo;
    }

    @Override
    public void write(int b) throws IOException {
        try {
            Failsafe.with(circuitBreakerInfo.getCircuitBreaker()).get(new NetworkCommunicationErrorAdvertisingCallable<Void>() {

                @Override
                protected Void performIOOperation() throws IOException {
                    superWrite(b);
                    return null;
                }
            }).getCheckedResult();
        } catch (CircuitBreakerOpenException e) {
            ExceptionHandler.handleCircuitBreakerOpenWriteException(e, circuitBreakerInfo);
        } catch (FailsafeException e) {
            ExceptionHandler.handleFailsafeException(e);
        }
    }

    /**
     * Invoke's <code>BufferedOutputStream</code>'s method to write the specified byte.
     *
     * @param b The byte to write
     * @throws IOException If an I/O error occurs
     */
    protected void superWrite(int b) throws IOException {
        super.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        try {
            Failsafe.with(circuitBreakerInfo.getCircuitBreaker()).get(new NetworkCommunicationErrorAdvertisingCallable<Void>() {

                @Override
                protected Void performIOOperation() throws IOException {
                    superWrite(b, off, len);
                    return null;
                }
            }).getCheckedResult();
        } catch (CircuitBreakerOpenException e) {
            ExceptionHandler.handleCircuitBreakerOpenWriteException(e, circuitBreakerInfo);
        } catch (FailsafeException e) {
            ExceptionHandler.handleFailsafeException(e);
        }
    }

    /**
     * Invoke's <code>BufferedOutputStream</code>'s method to write the specified bytes.
     *
     * @param b The byte to write
     * @param off The start offset in the data
     * @param len The number of bytes to write
     * @throws IOException If an I/O error occurs
     */
    protected void superWrite(byte[] b, int off, int len) throws IOException {
        super.write(b, off, len);
    }

    @Override
    public void flush() throws IOException {
        try {
            Failsafe.with(circuitBreakerInfo.getCircuitBreaker()).get(new NetworkCommunicationErrorAdvertisingCallable<Void>() {

                @Override
                protected Void performIOOperation() throws IOException {
                    superFlush();
                    return null;
                }
            }).getCheckedResult();
        } catch (CircuitBreakerOpenException e) {
            ExceptionHandler.handleCircuitBreakerOpenWriteException(e, circuitBreakerInfo);
        } catch (FailsafeException e) {
            ExceptionHandler.handleFailsafeException(e);
        }
    }

    /**
     * Invoke's <code>BufferedOutputStream</code>'s method to flush the stream.
     *
     * @throws IOException If an I/O error occurs.
     */
    protected void superFlush() throws IOException {
        super.flush();
    }

}
