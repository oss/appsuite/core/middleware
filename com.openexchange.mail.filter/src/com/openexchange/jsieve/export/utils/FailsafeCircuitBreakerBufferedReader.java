/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.jsieve.export.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import com.openexchange.mailfilter.internal.CircuitBreakerInfo;
import net.jodah.failsafe.CircuitBreakerOpenException;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.FailsafeException;

/**
 * {@link FailsafeCircuitBreakerBufferedReader}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.3
 */
@SuppressWarnings("synthetic-access")
public class FailsafeCircuitBreakerBufferedReader extends BufferedReader {

    private final CircuitBreakerInfo circuitBreakerInfo;

    /**
     * Initializes a new {@link FailsafeCircuitBreakerBufferedReader}.
     *
     * @param in A reader
     * @param circuitBreakerInfo The circuit breaker to use
     */
    public FailsafeCircuitBreakerBufferedReader(Reader in, CircuitBreakerInfo circuitBreakerInfo) {
        super(in);
        this.circuitBreakerInfo = circuitBreakerInfo;
    }

    /**
     * Initializes a new {@link FailsafeCircuitBreakerBufferedReader}.
     *
     * @param in A reader
     * @param sz The buffer size
     * @param circuitBreakerInfo The circuit breaker to use
     */
    public FailsafeCircuitBreakerBufferedReader(Reader in, int sz, CircuitBreakerInfo circuitBreakerInfo) {
        super(in, sz);
        this.circuitBreakerInfo = circuitBreakerInfo;
    }

    @Override
    public int read() throws IOException {
        try {
            return Failsafe.with(circuitBreakerInfo.getCircuitBreaker()).get(new NetworkCommunicationErrorAdvertisingCallable<Integer>() {

                @Override
                protected Integer performIOOperation() throws IOException {
                    return Integer.valueOf(superRead());
                }
            }).getCheckedResult().intValue();
        } catch (CircuitBreakerOpenException e) {
            throw ExceptionHandler.handleCircuitBreakerOpenReadException(e, circuitBreakerInfo);
        } catch (FailsafeException e) {
            throw ExceptionHandler.handleFailsafeException(e);
        }
    }

    /**
     * Reads a single character using super class' read() method.
     *
     * @return The character read, as an integer in the range
     *         0 to 65535 ({@code 0x00-0xffff}), or -1 if the
     *         end of the stream has been reached
     * @throws IOException If an I/O error occurs
     */
    protected int superRead() throws IOException {
        return super.read();
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        try {
            return Failsafe.with(circuitBreakerInfo.getCircuitBreaker()).get(new NetworkCommunicationErrorAdvertisingCallable<Integer>() {

                @Override
                protected Integer performIOOperation() throws IOException {
                    return Integer.valueOf(superRead(cbuf, off, len));
                }
            }).getCheckedResult().intValue();
        } catch (CircuitBreakerOpenException e) {
            throw ExceptionHandler.handleCircuitBreakerOpenReadException(e, circuitBreakerInfo);
        } catch (FailsafeException e) {
            throw ExceptionHandler.handleFailsafeException(e);
        }
    }

    /**
     * Reads characters into a portion of an array using super class' read() method.
     *
     * @param cbuf The character array to write to
     * @param off The start offset in the character array
     * @param len The number of characters to write
     * @return The number of read characters
     * @throws IndexOutOfBoundsException {@inheritDoc}
     * @throws IOException {@inheritDoc}
     */
    protected int superRead(char[] cbuf, int off, int len) throws IOException {
        return super.read(cbuf, off, len);
    }

    @Override
    public String readLine() throws IOException {
        try {
            return Failsafe.with(circuitBreakerInfo.getCircuitBreaker()).get(new NetworkCommunicationErrorAdvertisingCallable<String>() {

                @Override
                protected String performIOOperation() throws IOException {
                    return superReadLine();
                }
            }).getCheckedResult();
        } catch (CircuitBreakerOpenException e) {
            throw ExceptionHandler.handleCircuitBreakerOpenReadException(e, circuitBreakerInfo);
        } catch (FailsafeException e) {
            throw ExceptionHandler.handleFailsafeException(e);
        }
    }

    /**
     * Reads a line of text using super class' read() method.
     *
     * @return The read line
     * @throws IOException If an I/O error occurs
     */
    protected String superReadLine() throws IOException {
        return super.readLine();
    }
}
