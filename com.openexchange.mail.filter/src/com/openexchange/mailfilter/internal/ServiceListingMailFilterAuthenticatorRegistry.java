/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mailfilter.internal;

import java.util.Optional;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.mailfilter.Credentials;
import com.openexchange.mailfilter.MailFilterAuthenticator;
import com.openexchange.mailfilter.MailFilterAuthenticatorRegistry;
import com.openexchange.osgi.RankingAwareNearRegistryServiceTracker;
import com.openexchange.osgi.ServiceListing;
import com.openexchange.session.Session;

/**
 * {@link MailFilterAuthenticatorRegistry} - A registry that uses a {@link ServiceListing} to track
 * {@link MailFilterAuthenticator implementations}.
 */
public class ServiceListingMailFilterAuthenticatorRegistry extends RankingAwareNearRegistryServiceTracker<MailFilterAuthenticator> implements MailFilterAuthenticatorRegistry {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceListingMailFilterAuthenticatorRegistry.class);

    /**
     * Initializes a new {@link ServiceListingMailFilterAuthenticatorRegistry}.
     *
     * @param context The bundle context
     */
    public ServiceListingMailFilterAuthenticatorRegistry(BundleContext context) {
        super(context, MailFilterAuthenticator.class);
    }

    @Override
    public Optional<Credentials> getCredentials(Session session) {
        for (MailFilterAuthenticator authenticator : this) {
            if (authenticator != null) {
                Optional<Credentials> override;
                try {
                    override = authenticator.getCredentials(session);
                    // first implementation that proposes a result wins
                    if (override.isPresent()) {
                        return override;
                    }
                } catch (Exception e) {
                    LOG.error("Failed to invoke authenticator {}", authenticator.getClass().getName(), e);
                }
            }
        }
        return Optional.empty();
    }

}
