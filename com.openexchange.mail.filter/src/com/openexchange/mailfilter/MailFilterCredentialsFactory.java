/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mailfilter;

import com.openexchange.osgi.annotation.SingletonService;
import com.openexchange.session.Session;

/**
 * {@link MailFilterCredentialsFactory} - A factory for mail filter credentials.
 */
@SingletonService
public interface MailFilterCredentialsFactory {

    /**
     * Creates the appropriate mail filter credentials from given session.
     *
     * @param session The session
     * @return The mail filter credentials
     */
    Credentials createCredentials(Session session);

    /**
     * Creates the appropriate mail filter credentials from given session advertising specified user name.
     *
     * @param session The session
     * @param userName The user name to provide on call to {@link Credentials#getUserName()}
     * @return The mail filter credentials
     */
    Credentials createCredentialsWithUserName(Session session, String userName);
}
