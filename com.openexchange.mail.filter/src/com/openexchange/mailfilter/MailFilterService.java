/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mailfilter;

import java.util.List;
import java.util.Map;
import java.util.Set;
import com.openexchange.exception.OXException;
import com.openexchange.jsieve.commands.Rule;
import com.openexchange.osgi.annotation.SingletonService;

/**
 * {@link MailFilterService}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
@SingletonService
public interface MailFilterService {

    public enum FilterType {
        antispam("antispam"), autoforward("autoforward"), vacation("vacation"), all(""), custom("custom"), category("category"), syscategory("syscategory");

        private final String flag;

        private FilterType(String flag) {
            this.flag = flag;
        }

        public String getFlag() {
            return flag;
        }
    }

    public enum DayOfWeek {
        sunday, monday, tuesday, wednesday, thursday, friday, saturday
    }

    /**
     * Creates a new mail filter rule and return it's UID.
     *
     * @param credentials The credentials
     * @param rule The new mail filter rule
     * @return The UID of the new mail filter
     * @throws OXException If rule creation fails
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public int createFilterRule(Credentials credentials, Rule rule) throws OXException;

    /**
     * Updates an already existing mail filter rule.
     *
     * @param credentials The credentials
     * @param rule The mail filter rule
     * @param uid The rule's UID
     * @throws OXException If rule update fails
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public void updateFilterRule(Credentials credentials, Rule rule, int uid) throws OXException;

    /**
     * Deletes the specified rule.
     *
     * @param credentials The credentials
     * @param uid UID of the mail filter rule to delete
     * @throws OXException If rule deletion fails
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public void deleteFilterRule(Credentials credentials, int uid) throws OXException;

    /**
     * Deletes the specified rules.
     *
     * @param credentials The credentials
     * @param uids UIDs of the mail filter rules to delete
     * @throws OXException If deletion of rules fails
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public void deleteFilterRules(Credentials credentials, int... uids) throws OXException;

    /**
     * Delete all filters for the specified user.
     *
     * @param credentials The credentials
     * @throws OXException If deletion fails
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public void purgeFilters(Credentials credentials) throws OXException;

    /**
     * Gets the filter rule uniquely identified by the specified UID.
     *
     * @param credentials The credentials
     * @param uid rule's UID
     * @return the rule or null if none found
     * @throws OXException If rule cannot be returned
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public Rule getFilterRule(Credentials credentials, int uid) throws OXException;

    /**
     * Gets a list with all mail filter rules.
     *
     * @param credentials The credentials
     * @param flag instructs the method to only rules matching the specified flag (optional, can be null)
     * @return a list with all mail filter rules
     * @throws OXException If rules cannot be returned
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public List<Rule> listRules(Credentials credentials, String flag) throws OXException;

    /**
     * Gets a list with all mail filter rules.
     *
     * @param credentials The credentials
     * @param flag instructs the method to only rules matching the specified flag (optional, can be null)
     * @return a list with all mail filter rules
     * @throws OXException If rules cannot be returned
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public List<Rule> listRules(Credentials credentials, FilterType flag) throws OXException;

    /**
     * Gets a list with all mail filters except those specified in the exclusion list.
     *
     * @param credentials The credentials
     * @param exclusionFlags a list with exclusion flags
     * @return a list with all mail filter rules except those in the exclusion list
     * @throws OXException If rules cannot be returned
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public List<Rule> listRules(Credentials credentials, List<FilterType> exclusionFlags) throws OXException;

    /**
     * Reorders the rules.
     *
     * @param credentials The credentials
     * @param uids An array of UIDs which represents how the rules should be reordered
     * @throws OXException If reordering fails
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public void reorderRules(Credentials credentials, int[] uids) throws OXException;

    /**
     * Fetches the entire active script for the specified user.
     *
     * @param credentials The credentials
     * @return the active mail filter script as a string
     * @throws OXException If active script cannot be returned
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public String getActiveScript(Credentials credentials) throws OXException;

    /**
     * Gets a set with capabilities.
     *
     * @param credentials The credentials
     * @return a Set with capabilities
     * @throws OXException If capabilities cannot be returned
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public Set<String> getCapabilities(Credentials credentials) throws OXException;

    /**
     * Gets a map with extended properties.
     *
     * @param credentials The credentials
     * @return a map with extended properties
     * @throws OXException If extended properties cannot be returned
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public Map<String, Object> getExtendedProperties(Credentials credentials) throws OXException;

    /**
     * Gets a set with static capabilities.
     *
     * @param credentials The credentials (but only used to determine host and port)
     * @return a Set with static capabilities
     * @throws OXException If static capabilities cannot be returned
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public Set<String> getStaticCapabilities(Credentials credentials) throws OXException;

    /**
     * Converts a rule to its textual representation.
     *
     * @param credentials The credentials
     * @param rule The rule to convert
     * @return The textual representation
     * @throws OXException If textual representation cannot be returned
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public String convertToString(Credentials credentials, Rule rule) throws OXException;

    /**
     * Executes given command.
     *
     * @param credentials The credentials
     * @param command The command to execute
     * @throws OXException If command execution fails
     * @see {@link com.openexchange.mailfilter.MailFilterCredentialsFactory} to acquire credentials
     */
    public void executeCommand(Credentials credentials, MailFilterCommand command) throws OXException;

}
