/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mailfilter;

/**
 * Credentials to login into the SIEVE server.
 */
public interface Credentials {

    /**
     * Gets the user name.
     *
     * @return The user name
     */
    String getUserName();

    /**
     * Gets the password.
     *
     * @return The password
     */
    String getPassword();

    /**
     * Gets the authentication name.
     *
     * @return The authentication name
     */
    String getAuthName();

    /**
     * Gets the user identifier.
     *
     * @return The user identifier
     */
    int getUserId();

    /**
     * Gets the context identifier.
     *
     * @return The context identifier
     */
    int getContextId();

    /**
     * Gets the right user name.
     * <p>
     * If user name is <code>null</code> the {@link #getAuthName() authentication name} is returned; otherwise the user name
     */
    default String getRightUserName() {
        String userName = getUserName();
        return userName == null ? getAuthName() : userName;
    }

    /**
     * Gets the string value of the context identifier if a context identifier is present; otherwise <code>"unknown"</code> is returned
     *
     * @return The string value of context identifier or <code>"unknown"</code>
     */
    default String getContextString() {
        return Integer.toString(getContextId());
    }

    /**
     * Gets the OAuth token
     *
     * @return The OAuth token or <code>null</code> if absent
     */
    String getOAuthToken();

    /**
     * Gets the IP address of the session.
     *
     * @return The IP address or <code>null</code> if absent
     */
    String getSessionIp();

    /**
     * Gets the (mail) session information
     * <p>
     * Example:
     * <pre>
     *   "6ceec6585485458eb27456ad6ec97b62-17-1337-1356782"
     * </pre>
     *
     * @return The session information
     */
    String getSessionInfo();

}
