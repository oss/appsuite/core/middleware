/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mailfilter;

import java.util.Optional;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.Ranked;
import com.openexchange.session.Session;

/**
 * Pluggable interface for implementations that wish to override some of the MailFilter
 * (ManageSieve) authentication parameters to use in App Suite.
 */
public interface MailFilterAuthenticator extends Ranked {
    /**
     * Peek the implementation for a potential set of MailFilter authentication
     * parameters to use.
     * @param session
     * @return if applicable, a set of parameter overrides to use instead; if empty,
     * then the caller should use the default {@link Credentials} instead
     * @throws OXException
     */
    Optional<Credentials> getCredentials(Session session) throws OXException;
}
