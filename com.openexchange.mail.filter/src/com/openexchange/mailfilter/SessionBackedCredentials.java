/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mailfilter;

import java.util.function.Supplier;
import com.google.common.base.Suppliers;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXRuntimeException;
import com.openexchange.mail.utils.ClientParameterUtility;
import com.openexchange.mailfilter.properties.CredentialSource;
import com.openexchange.mailfilter.properties.MailFilterProperty;
import com.openexchange.mailfilter.services.Services;
import com.openexchange.session.Session;

/**
 * {@link SessionBackedCredentials} - Immutable implementation that fetches credentials information from a {@link Session}.
 */
public final class SessionBackedCredentials implements Credentials {

    private static final String SESSION_FULL_LOGIN = CredentialSource.SESSION_FULL_LOGIN.getName();

    /**
     * Creates session-backed credentials from given session.
     *
     * @param session The session to fetch credentials information from
     * @return The session-backed credentials
     * @throws OXRuntimeException If required service is unavailable
     */
    public static SessionBackedCredentials from(Session session) {
        String authName = determineAuthName(session);
        return new SessionBackedCredentials(null, authName, null, null, session);
    }

    /**
     * Creates session-backed credentials from given session and user name.
     *
     * @param session The session to fetch credentials information from
     * @param userName The user name override
     * @return The session-backed credentials
     * @throws OXRuntimeException If required service is unavailable
     */
    public static SessionBackedCredentials withUserName(Session session, String userName) {
        String authName = determineAuthName(session);
        return new SessionBackedCredentials(userName, authName, null, null, session);
    }

    /**
     * Determines the authentication name according to configuration setting for {@link MailFilterProperty#credentialSource}.
     *
     * @param session The session to get information from
     * @return The authentication name
     * @throws OXRuntimeException If required service is unavailable
     */
    private static String determineAuthName(Session session) {
        try {
            LeanConfigurationService leanConfigurationService = Services.requireService(LeanConfigurationService.class);
            String credsrc = leanConfigurationService.getProperty(session.getUserId(), session.getContextId(), MailFilterProperty.credentialSource);
            return SESSION_FULL_LOGIN.equals(credsrc) ? session.getLogin() : session.getLoginName();
        } catch (OXException e) {
            // Should not happen
            throw new OXRuntimeException(e);
        }
    }

    /**
     * Creates session-backed credentials from given session and authentication name.
     *
     * @param session The session to fetch credentials information from
     * @param authName The authentication name override
     * @return The session-backed credentials
     */
    public static SessionBackedCredentials withAuthName(Session session, String authName) {
        return new SessionBackedCredentials(null, authName, null, null, session);
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    private final String userName;
    private final String authName;
    private final String password;
    private final String oauthToken;
    private final Supplier<String> sessionInfo;
    private final Session session;

    /**
     * Initializes a new {@link SessionBackedCredentials}.
     *
     * @param userName The user name
     * @param authName The authentication name
     * @param password The password
     * @param oauthToken The OAuth token
     * @param session The session
     */
    private SessionBackedCredentials(String userName, String authName, String password, String oauthToken, Session session) {
        super();
        this.userName = userName;
        this.authName = authName;
        this.password = password;
        this.oauthToken = oauthToken;
        this.session = session;
        this.sessionInfo = Suppliers.memoize(() -> ClientParameterUtility.generateSessionInformation(session));
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public String getPassword() {
        return password != null ? password : session.getPassword();
    }

    @Override
    public String getAuthName() {
        return authName;
    }

    @Override
    public int getUserId() {
        return session.getUserId();
    }

    @Override
    public int getContextId() {
        return session.getContextId();
    }

    @Override
    public String getOAuthToken() {
        return oauthToken != null ? oauthToken : (String) session.getParameter(Session.PARAM_OAUTH_ACCESS_TOKEN);
    }

    @Override
    public String getSessionIp() {
        return session.getLocalIp();
    }

    @Override
    public String getSessionInfo() {
        return sessionInfo.get();
    }

    @Override
    public String toString() {
        return "Username: " + getRightUserName();
    }

}
