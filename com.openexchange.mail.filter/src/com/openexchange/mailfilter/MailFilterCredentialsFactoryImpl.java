/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mailfilter;

import java.util.Optional;
import com.openexchange.session.Session;

/**
 * {@link MailFilterCredentialsFactoryImpl} - The implementation for mail filter credentials factory.
 */
public class MailFilterCredentialsFactoryImpl implements MailFilterCredentialsFactory {

    private final MailFilterAuthenticatorRegistry authenticators;

    /**
     * Initializes a new {@link MailFilterCredentialsFactoryImpl}.
     *
     * @param authenticators The authenticator registry
     */
    public MailFilterCredentialsFactoryImpl(MailFilterAuthenticatorRegistry authenticators) {
        super();
        this.authenticators = authenticators;
    }

    @Override
    public Credentials createCredentials(Session session) {
        Optional<Credentials> optCredentials = authenticators.getCredentials(session);
        return optCredentials.isPresent() ? optCredentials.get() : SessionBackedCredentials.from(session);
    }

    @Override
    public Credentials createCredentialsWithUserName(Session session, String userName) {
        Optional<Credentials> optCredentials = authenticators.getCredentials(session);
        if (optCredentials.isPresent()) {
            return new UsernameOverridingCredentials(optCredentials.get(), userName);
        }
        return SessionBackedCredentials.withUserName(session, userName);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** Simple delegating mail filter credentials accepting an override for user name */
    private static final class UsernameOverridingCredentials implements Credentials {

        private final Credentials delegate;
        private final String userName;

        /**
         * Initializes a new {@link UsernameOverridingCredentials}.
         *
         * @param delegate The credentials to delegate to
         * @param userName The overriding user name or <code>null</code>
         */
        UsernameOverridingCredentials(Credentials delegate, String userName) {
            super();
            this.delegate = delegate;
            this.userName = userName;
        }

        @Override
        public String getUserName() {
            return userName;
        }

        @Override
        public String getPassword() {
            return delegate.getPassword();
        }

        @Override
        public String getAuthName() {
            return delegate.getAuthName();
        }

        @Override
        public int getUserId() {
            return delegate.getUserId();
        }

        @Override
        public int getContextId() {
            return delegate.getContextId();
        }

        @Override
        public String getRightUserName() {
            return userName != null ? userName : delegate.getRightUserName();
        }

        @Override
        public String getContextString() {
            return delegate.getContextString();
        }

        @Override
        public String getOAuthToken() {
            return delegate.getOAuthToken();
        }

        @Override
        public String getSessionIp() {
            return delegate.getSessionIp();
        }

        @Override
        public String getSessionInfo() {
            return delegate.getSessionInfo();
        }

    }

}
