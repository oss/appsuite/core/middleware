/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.saml;

import static org.junit.jupiter.api.Assertions.assertNull;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.opensaml.security.credential.Credential;
import org.opensaml.security.crypto.SigningUtil;
import org.opensaml.xmlsec.signature.SignableXMLObject;
import org.opensaml.xmlsec.signature.Signature;
import org.opensaml.xmlsec.signature.support.SignatureException;
import org.opensaml.xmlsec.signature.support.SignatureValidator;
import com.openexchange.saml.tools.SignatureHelper;
import com.openexchange.saml.validation.ValidationError;

/**
 *
 * {@link SignatureHelperTest}
 *
 * @author <a href="mailto:vitali.sjablow@open-xchange.com">Vitali Sjablow</a>
 * @since v7.10.1
 */
public class SignatureHelperTest {

    private List<Credential> credentials;

    @Mock
    private SignableXMLObject object;

    @Mock
    private Credential credOne;

    @Mock
    private Credential credTwo;

    @Mock
    private Signature signature;

    private MockedStatic<SigningUtil> signingUtilStaticMock;
    private MockedStatic<SignatureValidator> signatureValidatorStaticMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        signingUtilStaticMock = Mockito.mockStatic(SigningUtil.class);
        signatureValidatorStaticMock = Mockito.mockStatic(SignatureValidator.class);
        credentials = new ArrayList<>();
        credentials.add(credOne);
        credentials.add(credTwo);
        Mockito.when(object.getSignature()).thenReturn(signature);
    }

    @AfterEach
    public void tearDown() {
        signingUtilStaticMock.close();
        signatureValidatorStaticMock.close();
    }

    @Test
    public void testValidateSignature_TwoSigPass() {
        signatureValidatorStaticMock
            .when(() -> SignatureValidator.validate(signature, credOne))
            .thenAnswer(invocation -> null);

        signatureValidatorStaticMock
            .when(() -> SignatureValidator.validate(signature, credTwo))
            .thenAnswer(invocation -> null);

        ValidationError error = SignatureHelper.validateSignature(object, credentials);
        assertNull(error, "Expected no validation failure, but did fail");
    }

    @Test
    public void testValidateSignature_OneSigPass() {
        signatureValidatorStaticMock
            .when(() -> SignatureValidator.validate(signature, credOne))
            .thenThrow(new SignatureException());

        signatureValidatorStaticMock
            .when(() -> SignatureValidator.validate(signature, credTwo))
            .thenAnswer(invocation -> null);

        ValidationError error = SignatureHelper.validateSignature(object, credentials);

        assertNull(error, "Expected no validation failure, but did fail");
    }
}
