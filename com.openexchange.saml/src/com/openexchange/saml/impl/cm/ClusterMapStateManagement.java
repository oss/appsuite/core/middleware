/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.saml.impl.cm;

import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;
import com.openexchange.annotation.NonNull;
import com.openexchange.cluster.map.ClusterMap;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.cluster.map.RemoteSiteOptions;
import com.openexchange.cluster.map.codec.AbstractJSONMapCodec;
import com.openexchange.cluster.map.codec.MapCodec;
import com.openexchange.cluster.map.codec.MapCodecs;
import com.openexchange.exception.OXException;
import com.openexchange.java.util.UUIDs;
import com.openexchange.saml.state.AuthnRequestInfo;
import com.openexchange.saml.state.DefaultAuthnRequestInfo;
import com.openexchange.saml.state.DefaultLogoutRequestInfo;
import com.openexchange.saml.state.LogoutRequestInfo;
import com.openexchange.saml.state.StateManagement;


/**
 * Hazelcast-based implementation of {@link StateManagement}.
 *
 * @author <a href="mailto:steffen.templin@open-xchange.com">Steffen Templin</a>
 * @since v7.6.1
 */
public class ClusterMapStateManagement implements StateManagement {

    /** The options for the used cluster maps */
    private static final RemoteSiteOptions CLUSTER_MAP_OPTIONS = RemoteSiteOptions.builder()
        .withConsiderRemoteSites(true).withAsyncRemoteSiteCalling(false).build();

    private final ClusterMapService clusterMapService;
    private final MapCodec<AuthnRequestInfo> authnRequestInfoCodec;
    private final MapCodec<LogoutRequestInfo> logoutRequestInfoCodec;

    /**
     * Initializes a new {@link ClusterMapStateManagement}.
     * @param clusterMapService
     */
    public ClusterMapStateManagement(ClusterMapService clusterMapService) {
        super();
        this.clusterMapService = clusterMapService;

        authnRequestInfoCodec = new AbstractJSONMapCodec<AuthnRequestInfo>() {

            @Override
            protected @NonNull JSONObject writeJson(AuthnRequestInfo value) throws Exception {
                JSONObject j = new JSONObject(6);
                j.putOpt("requestId", value.getRequestId());
                j.putOpt("domainName", value.getDomainName());
                j.putOpt("loginPath", value.getLoginPath());
                j.putOpt("clientId", value.getClientID());
                j.putOpt("deepLink", value.getUriFragment());
                return j;
            }

            @Override
            protected @NonNull AuthnRequestInfo parseJson(JSONObject jObject) throws Exception {
                DefaultAuthnRequestInfo authnRequestInfo = new DefaultAuthnRequestInfo();
                authnRequestInfo.setClientID(jObject.optString("clientId"));
                authnRequestInfo.setDomainName(jObject.optString("domainName"));
                authnRequestInfo.setLoginPath(jObject.optString("loginPath"));
                authnRequestInfo.setRequestId(jObject.optString("requestId"));
                authnRequestInfo.setUriFragment(jObject.optString("deepLink"));
                return authnRequestInfo;
            }
        };

        logoutRequestInfoCodec = new AbstractJSONMapCodec<LogoutRequestInfo>() {

            @Override
            protected @NonNull JSONObject writeJson(LogoutRequestInfo value) throws Exception {
                JSONObject j = new JSONObject(4);
                j.putOpt("requestId", value.getRequestId());
                j.putOpt("domainName", value.getDomainName());
                j.putOpt("sessionId", value.getSessionId());
                return j;
            }

            @Override
            protected @NonNull LogoutRequestInfo parseJson(JSONObject jObject) throws Exception {
                DefaultLogoutRequestInfo logoutRequestInfo = new DefaultLogoutRequestInfo();
                logoutRequestInfo.setDomainName(jObject.optString("domainName"));
                logoutRequestInfo.setRequestId(jObject.optString("requestId"));
                logoutRequestInfo.setSessionId(jObject.optString("sessionId"));
                return logoutRequestInfo;
            }
        };
    }

    @Override
    public String addAuthnRequestInfo(AuthnRequestInfo requestInfo, long ttl, TimeUnit timeUnit) throws OXException {
        String id = generateID();
        getAuthnRequestInfoMap().put(id, requestInfo, timeUnit.toMillis(ttl));
        return id;
    }

    @Override
    public AuthnRequestInfo removeAuthnRequestInfo(String id) throws OXException {
        return getAuthnRequestInfoMap().remove(id);
    }

    @Override
    public void addAuthnResponseID(String responseID, long ttl, TimeUnit timeUnit) throws OXException {
        getAuthnResponseIDMap().put(responseID, responseID, timeUnit.toMillis(ttl));
    }

    @Override
    public boolean hasAuthnResponseID(String responseID) throws OXException {
        return getAuthnResponseIDMap().containsKey(responseID);
    }

    @Override
    public String addLogoutRequestInfo(LogoutRequestInfo requestInfo, long ttl, TimeUnit timeUnit) throws OXException {
        String id = generateID();
        getLogoutRequestInfoMap().put(id, requestInfo, timeUnit.toMillis(ttl));
        return id;
    }

    @Override
    public LogoutRequestInfo removeLogoutRequestInfo(String id) throws OXException {
        return getLogoutRequestInfoMap().remove(id);
    }

    private ClusterMap<AuthnRequestInfo> getAuthnRequestInfoMap() throws OXException {
        return clusterMapService.getMap(CoreMap.SAML_AUTHN_REQUEST_INFOS, authnRequestInfoCodec, 3600 * 1000L, CLUSTER_MAP_OPTIONS);
    }

    private ClusterMap<LogoutRequestInfo> getLogoutRequestInfoMap() throws OXException {
        return clusterMapService.getMap(CoreMap.SAML_LOGOUT_REQUEST_INFOS, logoutRequestInfoCodec, 14400 * 1000L, CLUSTER_MAP_OPTIONS);
    }

    private ClusterMap<String> getAuthnResponseIDMap() throws OXException {
        return clusterMapService.getMap(CoreMap.SAML_AUTHN_RESPONSE_IDS, MapCodecs.getStringCodec(), 3600 * 1000L, CLUSTER_MAP_OPTIONS);
    }

    private static String generateID() {
        return UUIDs.getUnformattedString(UUID.randomUUID());
    }

}
