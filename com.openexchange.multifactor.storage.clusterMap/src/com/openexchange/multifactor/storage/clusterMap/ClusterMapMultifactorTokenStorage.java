/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.multifactor.storage.clusterMap;

import static com.openexchange.java.Autoboxing.L;
import java.util.Objects;
import java.util.Optional;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.exception.OXException;
import com.openexchange.multifactor.MultifactorRequest;
import com.openexchange.multifactor.MultifactorToken;
import com.openexchange.multifactor.storage.MultifactorTokenStorage;

/**
 * {@link ClusterMapMultifactorTokenStorage}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class ClusterMapMultifactorTokenStorage<T extends MultifactorToken<?>> implements MultifactorTokenStorage<T> {

    /**
     * Factory for mapping {@link CachedMultifactorToken} to {@link MultifactorToken}
     *
     * {@link TokenFactory}
     *
     * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
     * @since v7.10.2
     * @param <T>
     */
    public interface TokenFactory<T>{

        /**
         * Creates the {@link MultifactorToken} Token from the given{@link CachedMultifactorToken}
         *
         * @param cachedToken The cached to create the token from
         * @return The {@link MultifactorToken} created from the given portable
         */
        T create(CachedMultifactorToken cachedToken);

        /**
         * Generates the binary representation for given token.
         *
         * @param token The token
         * @return The binary representation
         * @throws OXException If binary representation cannot be generated
         */
        byte[] generateBytesFor(T token) throws OXException;
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    private final BasicCoreClusterMapProvider<CachedMultifactorToken> clusterMapProvider;
    private final TokenFactory<T> tokenFactory;

    /**
     * Initializes a new {@link ClusterMapMultifactorTokenStorage}.
     *
     * @param clusterMapService The service to use
     * @param mapName The name of the map to use
     * @param tokenFactory A factory for creating the MultifactorToken from a given portable
     */
    public ClusterMapMultifactorTokenStorage(ClusterMapService clusterMapService, CoreMap mapName, TokenFactory<T> tokenFactory) {
        super();
        this.clusterMapProvider = BasicCoreClusterMapProvider.<CachedMultifactorToken> builder() //@formatter:off
            .withCoreMap(Objects.requireNonNull(mapName, "mapName must not be null"))
            .withCodec(new MultifactorTokenMapCodec())
            .withServiceSupplier(BasicCoreClusterMapProvider.singletonClusterMapServiceSupplier(Objects.requireNonNull(clusterMapService, "ClusterMapService must not be null")))
            .build(); //@formatter:on
        this.tokenFactory = Objects.requireNonNull(tokenFactory, "tokenFactory must not be null");
    }

    private static String toPartialKey(MultifactorRequest multifactorRequests) {
        return new StringBuilder().append(multifactorRequests.getContextId()).append('_').append(multifactorRequests.getUserId()).toString();
    }

    private static String toKey(MultifactorRequest session, String key) {
        return new StringBuilder().append(toPartialKey(session)).append('_').append(key).toString();
    }

    @Override
    public Optional<T> getAndRemove(MultifactorRequest multifactorRequest, String key) throws OXException {
        String cmKey = toKey(multifactorRequest, key);
        Optional<CachedMultifactorToken> cachedToken = Optional.ofNullable(clusterMapProvider.getMap().remove(cmKey));
        return cachedToken.map(tokenFactory::create);
    }

    @Override
    public void add(MultifactorRequest multifactorRequest, String key, T token) throws OXException {
        clusterMapProvider.getMap().putIfAbsent(
            toKey(multifactorRequest,key),
            new CachedMultifactorToken(token.getLifeTime().get(), tokenFactory.generateBytesFor(token)),
            token.getLifeTime().map(d -> L(d.toMillis())).orElse(L(0L)).longValue());
    }

    @Override
    public int getTokenCount(MultifactorRequest multifactorRequest) throws OXException {
        return (int) clusterMapProvider.getMap().keySet().stream().filter(
            k -> k.startsWith(toPartialKey(multifactorRequest))
        ).count();
    }

}
