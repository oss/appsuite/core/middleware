/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.multifactor.storage.clusterMap;

import java.time.Duration;
import java.util.Optional;
import com.openexchange.multifactor.MultifactorToken;

/**
 *
 * {@link CachedMultifactorToken} - The cached representation of {@link MultifactorToken}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class CachedMultifactorToken {

    private final Optional<Duration> lifeTime;
    private final byte[] tokenValueData;

    /**
     * Initializes a new {@link CachedMultifactorToken}.
     *
     * @param lifeTime The optional life time
     * @param tokenValueData The token value's data
     */
    public CachedMultifactorToken(Duration lifeTime, byte[] tokenValueData) {
        super();
        this.lifeTime = Optional.ofNullable(lifeTime);
        this.tokenValueData = tokenValueData;
    }

    /**
     * Gets the binary data of the token value.
     *
     * @return The token value's data
     */
    public byte[] getTokenValueData() {
       return tokenValueData;
    }

    /**
     * Gets the optional life time.
     *
     * @return The optional life time
     */
    public Optional<Duration> getLifeTime(){
        return lifeTime;
    }

}
