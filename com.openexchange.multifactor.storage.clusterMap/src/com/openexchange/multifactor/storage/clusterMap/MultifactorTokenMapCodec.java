/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.multifactor.storage.clusterMap;

import java.time.Duration;
import java.util.Base64;
import java.util.Optional;
import org.json.JSONObject;
import com.openexchange.annotation.NonNull;
import com.openexchange.cluster.map.codec.AbstractJSONMapCodec;


/**
 * {@link MultifactorTokenMapCodec} - The codec to serialize/deserialize <code>CachedMultifactorToken</code> instances.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.0.0
 */
public class MultifactorTokenMapCodec extends AbstractJSONMapCodec<CachedMultifactorToken> {

    /**
     * Initializes a new {@link MultifactorTokenMapCodec}.
     */
    public MultifactorTokenMapCodec() {
        super();
    }

    @Override
    protected @NonNull JSONObject writeJson(CachedMultifactorToken value) throws Exception {
        JSONObject jToken = new JSONObject(2);

        Optional<Duration> optLifeTime = value.getLifeTime();
        if (optLifeTime.isPresent()) {
            jToken.put("lifeTime", optLifeTime.get().toMillis());
        }

        byte[] tokenValueData = value.getTokenValueData();
        if (tokenValueData != null) {
            jToken.put("value", Base64.getEncoder().encodeToString(tokenValueData));
        }

        return jToken;
    }

    @Override
    protected @NonNull CachedMultifactorToken parseJson(JSONObject jToken) throws Exception {
        long lifeTimeMillis = jToken.optLong("lifeTime", 0);

        byte[] tokenValueData = null;
        String base64 = jToken.optString("value", null);
        if (base64 != null) {
            tokenValueData = Base64.getDecoder().decode(base64);
        }

        return new CachedMultifactorToken(lifeTimeMillis == 0 ? null : Duration.ofMillis(lifeTimeMillis), tokenValueData);
    }

}
