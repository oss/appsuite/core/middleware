/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.carddav.reports;

import static com.openexchange.java.Autoboxing.I;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import com.openexchange.carddav.CardDAVProperty;
import com.openexchange.carddav.CarddavProtocol;
import com.openexchange.carddav.GroupwareCarddavFactory;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.dav.DAVProtocol;
import com.openexchange.dav.actions.PROPFINDAction;
import com.openexchange.java.Strings;
import com.openexchange.log.DurationOutputter;
import com.openexchange.webdav.action.WebdavRequest;
import com.openexchange.webdav.action.WebdavResponse;
import com.openexchange.webdav.protocol.Multistatus;
import com.openexchange.webdav.protocol.Protocol;
import com.openexchange.webdav.protocol.WebdavFactory;
import com.openexchange.webdav.protocol.WebdavPath;
import com.openexchange.webdav.protocol.WebdavProtocolException;
import com.openexchange.webdav.protocol.WebdavResource;
import com.openexchange.webdav.protocol.WebdavStatus;
import com.openexchange.webdav.xml.resources.PropertiesMarshaller;
import com.openexchange.webdav.xml.resources.ResourceMarshaller;

/**
 * A {@link CaldavMultigetReport} allows clients to retrieve properties of certain named resources. It is conceptually similar to a propfind.
 *
 * @author <a href="mailto:francisco.laguna@open-xchange.com">Francisco Laguna</a>
 */
public class AddressbookMultigetReport extends PROPFINDAction {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(AddressbookMultigetReport.class);

    public static final String NAMESPACE = CarddavProtocol.CARD_NS.getURI();
    public static final String NAME = "addressbook-multiget";

    /**
     * Initializes a new {@link AddressbookMultigetReport}.
     *
     * @param protocol The protocol
     */
    public AddressbookMultigetReport(DAVProtocol protocol) {
        super(protocol);
    }

    @Override
    public void perform(WebdavRequest request, WebdavResponse response) throws WebdavProtocolException {
        /*
         * get paths of requested resources
         */
        Document requestBody = requireRequestBody(request);
        List<WebdavPath> paths = getHrefPaths(request, requestBody.getRootElement());
        /*
         * limit requested paths as needed
         */
        int multigetLimit = getMultigetLimit(request);
        List<WebdavPath> pathsToResolve;
        List<WebdavPath> pathsToSkip;
        if (0 < multigetLimit && multigetLimit < paths.size()) {
            pathsToResolve = paths.subList(0, multigetLimit);
            pathsToSkip = paths.subList(multigetLimit, paths.size());
            LOG.debug("Excluding {} WebDAV resources in addressbook-multiget response due to configured limit of {}.", I(pathsToSkip.size()), I(multigetLimit));
        } else {
            pathsToResolve = paths;
            pathsToSkip = null;
        }
        /*
         * resolve & marshal resources
         */
        long start = System.nanoTime();
        ResourceMarshaller marshaller = getMarshaller(request, requestBody);
        PropertiesMarshaller helper = new PropertiesMarshaller(request.getURLPrefix(), request.getCharset());
        Element multistatusElement = prepareMultistatusElement();
        WebdavFactory factory = request.getFactory();
        if (factory instanceof GroupwareCarddavFactory carddavFactory) {
            /*
             * batch-resolve requested resources if possible
             */
            Multistatus<WebdavResource> multistatus = carddavFactory.resolveResources(pathsToResolve);
            for (int statusCode : multistatus.getStatusCodes()) {
                for (WebdavStatus<WebdavResource> status : multistatus.toIterable(statusCode)) {
                    if (null != status.getAdditional()) {
                        multistatusElement.addContent(marshaller.marshal(status.getAdditional()));
                    } else {
                        multistatusElement.addContent(new Element("response", Protocol.DAV_NS)
                            .addContent(helper.marshalHREF(status.getUrl(), false))
                            .addContent(helper.marshalStatus(status.getStatus()))
                        );
                    }
                }
            }
        } else {
            /*
             * resolve each requested resource individually, otherwise
             */
            for (WebdavPath webdavPath : pathsToResolve) {
                multistatusElement.addContent(marshaller.marshal(request.getFactory().resolveResource(webdavPath)));
            }
        }
        multistatusElement.addContent(marshallInsufficientStorage(helper, pathsToSkip));
        LOG.debug("CARDDAV:addressbook-multiget response for {} with {} resolved resources (from a total of {} requested paths) generated after {}.",
            request.getUrl(), I(pathsToResolve.size()), I(paths.size()), DurationOutputter.startNanos(start));
        sendMultistatusResponse(response, multistatusElement);
    }

    private static List<Element> marshallInsufficientStorage(PropertiesMarshaller marshaller, List<WebdavPath> paths) {
        if (null == paths || paths.isEmpty()) {
            return Collections.emptyList();
        }
        List<Element> elements = new ArrayList<Element>(paths.size());
        for (WebdavPath path : paths) {
            elements.add(new Element("response", Protocol.DAV_NS)
                .addContent(marshaller.marshalHREF(path, false))
                .addContent(marshaller.marshalStatus(Protocol.SC_INSUFFICIENT_STORAGE).setText("HTTP/1.1 507 Insufficient Storage"))
                .addContent(new Element("error", Protocol.DAV_NS).addContent(new Element("number-of-matches-within-limits", Protocol.DAV_NS)))
            );
        }
        return elements;
    }

    private static int getMultigetLimit(WebdavRequest request) {
        try {
            WebdavFactory factory = request.getFactory();
            if (factory instanceof GroupwareCarddavFactory carddavFactory) {
                return Strings.parseInt(carddavFactory.getServiceSafe(LeanConfigurationService.class).getProperty(
                    factory.getSession().getUserId(), factory.getSession().getContextId(), CardDAVProperty.ADDRESSBOOK_MULTIGET_LIMIT));
            }
        } catch (Exception e) {
            LOG.warn("Error getting {}, falling back to defaults.", CardDAVProperty.ADDRESSBOOK_MULTIGET_LIMIT.getFQPropertyName(), e);
        }
        return Strings.parseInt(CardDAVProperty.ADDRESSBOOK_MULTIGET_LIMIT.getDefaultValue(String.class));
    }

}
