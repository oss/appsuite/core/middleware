/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.carddav;

import java.util.ArrayList;
import java.util.List;
import com.openexchange.ajax.customizer.folder.AdditionalFolderField;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.dav.DAVCapabilityChecker;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.ContentType;
import com.openexchange.folderstorage.Folder;
import com.openexchange.folderstorage.UsedForSync;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link CardDAVURLField}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class CardDAVURLField implements AdditionalFolderField {

    private final ServiceLookup services;
    private final DAVCapabilityChecker capabilityChecker;

    /**
     * Initializes a new {@link CardDAVURLField}.
     *
     * @param services A service lookup reference
     * @param capabilityChecker The checker for the CardDAV capability
     */
    public CardDAVURLField(ServiceLookup services, DAVCapabilityChecker capabilityChecker) {
        super();
        this.services = services;
        this.capabilityChecker = capabilityChecker;
    }

    @Override
    public int getColumnID() {
        return 3221;
    }

    @Override
    public String getColumnName() {
        return "com.openexchange.carddav.url";
    }

    @Override
    public Object getValue(Folder item, ServerSession session) {
        String resourceName = getCardDAVResourceName(session, item);
        if (null == resourceName) {
            return null;
        }
        return getURLTemplate(session).replaceAll("\\[folderId\\]", resourceName);
    }

    @Override
    public List<Object> getValues(List<Folder> items, ServerSession session) {
        List<Object> values = new ArrayList<Object>(items.size());
        String urlTemplate = getURLTemplate(session);
        for (Folder item : items) {
            String resourceName = getCardDAVResourceName(session, item);
            values.add(null == resourceName ? null : urlTemplate.replaceAll("\\[folderId\\]", resourceName));
        }
        return values;
    }

    @Override
    public Object renderJSON(AJAXRequestData requestData, Object value) {
        if (null != value && value instanceof String stringValue && null != requestData && null != requestData.getHostname()) {
            return stringValue.replaceAll("\\[hostname\\]", requestData.getHostname());
        }
        return value;
    }

    private String getURLTemplate(ServerSession session) {
        try {
            return services.getServiceSafe(LeanConfigurationService.class).getProperty(session.getUserId(), session.getContextId(), CardDAVProperty.URL_TEMPLATE);
        } catch (OXException e) {
            org.slf4j.LoggerFactory.getLogger(CardDAVURLField.class).warn("Error getting property \"{}\", falling back to {}.", 
                CardDAVProperty.URL_TEMPLATE, CardDAVProperty.URL_TEMPLATE.getDefaultValue(), e);
            return CardDAVProperty.URL_TEMPLATE.getDefaultValue(String.class);
        }
    }

    private String getCardDAVResourceName(ServerSession session, Folder folder) {
        ContentType contentType = folder.getContentType();
        if (null == contentType || FolderObject.CONTACT != contentType.getModule()) {
            return null;
        }
        UsedForSync usedForSync = folder.getUsedForSync();
        if (null != usedForSync && false == usedForSync.isUsedForSync()) {
            return null;
        }
        if (false == session.getUserPermissionBits().hasContact() || false == isEnabled(session)) {
            return null;
        }
        return Tools.encodeFolderId(folder.getID());
    }

    private boolean isEnabled(ServerSession session) {
        String capabilityName = capabilityChecker.getCapbilityName();
        try {
            return capabilityChecker.isEnabled(capabilityName, session);
        } catch (OXException e) {
            org.slf4j.LoggerFactory.getLogger(CardDAVURLField.class).warn("Error checking if capability \"{}\", is enabled.", capabilityName, e);
            return false;
        }
    }

}
