/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.carddav;

import com.openexchange.config.lean.Property;

/**
 * {@link CardDAVProperty}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @since v7.10.5
 */
public enum CardDAVProperty implements Property {

    /**
     * Whether CardDAV is enabled or not.
     */
    ENABLED("enabled", Boolean.TRUE),

    /**
     * A comma-separated list of folder IDs to exclude from the synchronization.
     * Use this to disable syncing of very large folders (e.g. the global address
     * list in large contexts, which always has ID 6).
     */
    IGNORE_FOLDERS("ignoreFolders", null),

    /**
     * Configures the ID of the folder tree used by the CardDAV interface.
     */
    TREE("tree", "0"),

    /**
     * Controls which collections are exposed via the CardDAV interface. Possible
     * values are <code>0</code>, <code>1</code> and <code>2</code>. A value of <code>1</code>
     * makes each visible folder available as a resource collection, while <code>2</code> only
     * exposes an aggregated collection containing all contact resources from all visible folders. The
     * value <code>0</code> exposes either an aggregated collection or individual
     * collections for each folder, depending on the client's user-agent that is
     * matched against the pattern in [[com.openexchange.carddav.userAgentForAggregatedCollection]].
     */
    EXPOSED_COLLECTIONS("exposedCollections", "0"),

    /**
     * Regular expression to match against the client's user-agent to decide
     * whether the aggregated collection is exposed or not. The default pattern
     * matches all known varieties of the Mac OS Addressbook client, that doesn't
     * support multiple collections. Only used if [[com.openexchange.carddav.exposedCollections]]
     * is set to <code>0</code>. The pattern is used case insensitive.
     */
    USERAGENT_FOR_AGGREGATED_COLLECTION("userAgentForAggregatedCollection", ".*CFNetwork.*Darwin.*|.*AddressBook.*CardDAVPlugin.*Mac_OS_X.*|.*Mac OS X.*AddressBook.*|.*macOS.*AddressBook.*"),

    /**
     * Specifies if all visible folders are used to create the aggregated
     * collection, or if a reduced set of folders only containing the global
     * addressbook and the personal contacts folders should be used. This setting
     * only influences the aggregated collection that is used for clients that
     * don't support multiple collections. Possible values are <code>true</code> and <code>false</code>.
     */
    REDUCED_AGGREGATED_COLLECTION("reducedAggregatedCollection", Boolean.FALSE),

    /**
     * Configures which contact folders are exposed through the special aggregated collection. This setting
     * only influences the aggregated collection that is used for clients that don't support multiple collections.
     * <p/>
     * Possible values are:
     * <ul>
     * <li><code>all</code>: All visible contact folders are exposed</li>
     * <li><code>all_synced</code>: All folders that are marked to be <i>used for sync</i> are exposed</li>
     * <li><code>reduced</code>: Only the default personal and the global addressbook folders are exposed</li>
     * <li><code>reduced_synced</code>: Only the default personal and the global addressbook folders are exposed, if marked to be <i>used for sync</i></li>
     * <li><code>default_only</code>: Only the default personal contact folder is exposed</li>
     * </ul>
     * Defaults to <code>all_synced</code>.
     */
    AGGREGATED_COLLECTION_FOLDERS("aggregatedCollectionFolders", "all_synced"),

    /**
     * Option to configure whether the value of the <code>PHOTO</code>-property in vCards should be preferably exported as URI or not.
     * Possible values are <code>binary</code> to generate Base64-encoded inline data, or <code>uri</code> to generate a link pointing to
     * the <code>/photos</code>-collection on the server.
     * <p/>
     * May still be overridden by clients on a per-request basis using the <code>Prefer</code>-header.
     */
    PREFERRED_PHOTO_ENCODING("preferredPhotoEncoding", "binary"),
    /**
     * Controls which parent collection / folder is used for newly created contacts created by iOS clients.
     * <p/>
     * <p/>
     * Possible settings are:
     * <ul>
     * <li><code>always</code>: New contacts are always created within the user's default contacts folder on App Suite</li>
     * <li><code>disabled</code>: Contacts are created within the folder targeted by the client, but rejected on insufficient permissions</li>
     * <li><code>insufficient_permissions</code>: Contacts are created within the folder targeted by the client, falling back to the user's default folder on insufficient permissions</li>
     * </ul>
     * Defaults to 'always' so that contacts are created in the user's default contacts folder independently of which folder is targeted by the client.
     */
    IOS_FALLBACK_TO_DEFAULT_COLLECTION_FOR_NEW_RESOURCES("iosFallbackToDefaultCollectionForNewResources", "always"),
    /**
     * Configures the maximum number of elements included in <code>CARDDAV:addressbook-multiget</code> responses to the client. If
     * data from more elements was requested, <code>HTTP/1.1 507 Insufficient Storage</code> responses will get inserted.
     * <p/>
     * A value of <code>-1</code> disables the limit.
     */
    ADDRESSBOOK_MULTIGET_LIMIT("addressbookMultigetLimit", "1000"),
    /**
     * Tells users where to find a CardDAV folder. This can be displayed in frontends.
     * You can use the variables <code>[hostname]</code> and <code>[folderId]</code>.
     * If you chose to deploy CardDAV as a virtual host (say 'dav.example.com') use
     * <code>https://dav.example.com/carddav/[folderId]</code> as the value.
     * If you are using user-agent sniffing use <code>https://[hostname]/carddav/[folderId]</code>.
     */
    URL_TEMPLATE("url", "https://[hostname]/carddav/[folderId]"),
    ;

    private final Object defaultValue;
    private final String fqn;

    /**
     * Initializes a new {@link CardDAVProperty}.
     *
     * @param suffix The property name suffix
     * @param defaultValue The property's default value
     */
    private CardDAVProperty(String suffix, Object defaultValue) {
        this.defaultValue = defaultValue;
        this.fqn = "com.openexchange.carddav." + suffix;
    }

    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
