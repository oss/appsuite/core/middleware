
package com.openexchange.filemanagement.distributed.osgi;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.osgi.framework.ServiceReference;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.config.ConfigurationService;
import com.openexchange.filemanagement.DistributedFileManagement;
import com.openexchange.filemanagement.DistributedFileUtils;
import com.openexchange.filemanagement.distributed.DistributedFileManagementImpl;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.osgi.SimpleRegistryListener;
import com.openexchange.server.ServiceLookup;
import com.openexchange.threadpool.ThreadPoolService;

/**
 * {@link DistributedFileManagementActivator}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 */
public class DistributedFileManagementActivator extends HousekeepingActivator {

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { ConfigurationService.class, ThreadPoolService.class, DistributedFileUtils.class };
    }

    @Override
    protected void startBundle() throws Exception {
        final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DistributedFileManagementActivator.class);
        logger.info("Starting bundle: com.openexchange.filemanagement.distributed");

        final int port = getService(ConfigurationService.class).getIntProperty("com.openexchange.connector.networkListenerPort", 8009);
        final ServiceLookup services = this;
        {
            track(ClusterMapService.class, new SimpleRegistryListener<ClusterMapService>() {

                private DistributedFileManagementImpl distributedFileManagement;

                @Override
                public synchronized void added(ServiceReference<ClusterMapService> ref, ClusterMapService service) {
                    // Address
                    String address;
                    try {
                        address = InetAddress.getLocalHost().getCanonicalHostName();
                    } catch (UnknownHostException e) {
                        address = "localhost";
                    }
                    address = address + ":" + port;

                    // Clean-up task
                    Runnable shutDownTask = () -> {
                        try {
                            shutDownDistributedFileManagement();
                        } catch (Exception e) {
                            logger.error("Failed to shut-down distributed file management", e);
                        }
                    };

                    // Register distributed file management service
                    DistributedFileManagementImpl distributedFileManagement = new DistributedFileManagementImpl(services, service, address, shutDownTask);
                    this.distributedFileManagement = distributedFileManagement;
                    registerService(DistributedFileManagement.class, distributedFileManagement);
                }

                @Override
                public synchronized void removed(ServiceReference<ClusterMapService> ref, ClusterMapService service) {
                    shutDownDistributedFileManagement();
                }

                void shutDownDistributedFileManagement() {
                    DistributedFileManagementImpl distributedFileManagement = this.distributedFileManagement;
                    if (distributedFileManagement != null) {
                        this.distributedFileManagement = null;
                        unregisterService(distributedFileManagement);
                        distributedFileManagement.cleanUp();
                    }
                }
            });
        }

        openTrackers();
    }

    @Override
    protected void stopBundle() throws Exception {
        org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DistributedFileManagementActivator.class);
        logger.info("Stopping bundle: com.openexchange.filemanagement.distributed");
        super.stopBundle();
    }

    @Override
    public <S> void registerService(java.lang.Class<S> clazz, S service) {
        super.registerService(clazz, service);
    }

    @Override
    public <S> void unregisterService(S service) {
        super.unregisterService(service);
    }

}
