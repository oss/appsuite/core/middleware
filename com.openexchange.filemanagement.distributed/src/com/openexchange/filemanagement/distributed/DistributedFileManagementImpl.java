/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.filemanagement.distributed;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Duration;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.cluster.map.codec.MapCodecs;
import com.openexchange.exception.OXException;
import com.openexchange.filemanagement.DistributedFileManagement;
import com.openexchange.filemanagement.DistributedFileUtils;
import com.openexchange.filemanagement.ManagedFileExceptionErrorMessage;
import com.openexchange.server.ServiceLookup;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.threadpool.ThreadPools;

/**
 * {@link DistributedFileManagementImpl}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 */
public class DistributedFileManagementImpl extends BasicCoreClusterMapProvider<String> implements DistributedFileManagement {

    private static final int READ_TIMEOUT = 10000;
    private static final int CONNECT_TIMEOUT = 3000;


    // ---------------------------------- Member stuff -------------------------------------- //

    private final String address;
    private final ServiceLookup services;
    private final Runnable shutDownTask;

    /**
     * Initializes a new {@link DistributedFileManagementImpl}.
     *
     * @param services The service look-up
     * @param clusterMapService The cluster map service
     * @param address The address for this node
     * @param shutDownTask The task to perform on shut-down
     */
    public DistributedFileManagementImpl(ServiceLookup services, ClusterMapService clusterMapService, String address, Runnable shutDownTask) {
        super(CoreMap.DISTRIBUTED_FILES, MapCodecs.getStringCodec(), Duration.ofHours(1).toMillis(), singletonClusterMapServiceSupplier(clusterMapService));
        this.services = services;
        this.address = address;
        this.shutDownTask = shutDownTask;
    }

    /**
     * Performs necessary clean-up actions before destroying this distributed file management service.
     */
    public synchronized void cleanUp() {
        shutDownTask.run();
    }

    private String encodeId(String rawId) throws OXException {
        return services.getServiceSafe(DistributedFileUtils.class).encodeId(rawId);
    }

    @Override
    public void register(String id) throws OXException {
        try {
            getMap().put(id, getURI());
        } catch (RuntimeException e) {
            throw ManagedFileExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public void unregister(String id) throws OXException {
        try {
            getMap().remove(id);
        } catch (RuntimeException e) {
            throw ManagedFileExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public InputStream get(String id) throws OXException {
        try {
            String url = getMap().get(id);
            if (url != null) {
                try {
                    return loadFile("http://" + url + "/" + encodeId(id));
                } catch (IOException e) {
                    throw ManagedFileExceptionErrorMessage.IO_ERROR.create(e, e.getMessage());
                }
            }

            return null;
        } catch (RuntimeException e) {
            throw ManagedFileExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public void touch(String id) throws OXException {
        try {
            String url = getMap().get(id);
            if (url != null) {
                try {
                    URL remoteUrl = new URL("http://" + url + "/" + encodeId(id));
                    HttpURLConnection con = (HttpURLConnection) remoteUrl.openConnection();
                    con.setRequestMethod("POST");
                    con.setConnectTimeout(CONNECT_TIMEOUT);
                    con.connect();
                } catch (IOException e) {
                    throw ManagedFileExceptionErrorMessage.IO_ERROR.create(e, e.getMessage());
                }
            }
        } catch (RuntimeException e) {
            throw ManagedFileExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public boolean exists(final String id, long timeout, TimeUnit unit) throws OXException, TimeoutException {
        Future<Boolean> f = services.getService(ThreadPoolService.class).getExecutor().submit(new Callable<Boolean>() {

            @Override
            public Boolean call() throws OXException {
                try {
                    return Boolean.valueOf(getMap().containsKey(id));
                } catch (RuntimeException e) {
                    throw ManagedFileExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
                }
            }
        });

        try {
            return f.get(timeout, unit).booleanValue();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw ManagedFileExceptionErrorMessage.UNEXPECTED_ERROR.create(e, "Thread interrupted");
        } catch (ExecutionException e) {
            throw ThreadPools.launderThrowable(e, OXException.class);
        } catch (TimeoutException e) {
            f.cancel(true);
            throw e;
        }
    }

    @Override
    public boolean exists(String id) throws OXException {
        try {
            return getMap().containsKey(id);
        } catch (RuntimeException e) {
            throw ManagedFileExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public void remove(String id) throws OXException {
        try {
            String url = getMap().get(id);
            if (url != null) {
                try {
                    URL remoteUrl = new URL("http://" + url + "/" + encodeId(id));
                    HttpURLConnection con = (HttpURLConnection) remoteUrl.openConnection();
                    con.setRequestMethod("DELETE");
                    con.setConnectTimeout(CONNECT_TIMEOUT);
                    con.connect();
                } catch (IOException e) {
                    throw ManagedFileExceptionErrorMessage.IO_ERROR.create(e, e.getMessage());
                }
            }
        } catch (RuntimeException e) {
            throw ManagedFileExceptionErrorMessage.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    private String getURI() {
        return address + PATH;
    }

    private static InputStream loadFile(String url) throws IOException {
        URL remoteUrl = new URL(url);

        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) remoteUrl.openConnection();
            con.setRequestMethod("GET");
            con.setConnectTimeout(CONNECT_TIMEOUT);
            con.setReadTimeout(READ_TIMEOUT);
            con.connect();

            int responseCode = con.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                return null;
            }

            InputStream retval = con.getInputStream();
            con = null; // Avoid premature closing
            return retval;
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }
    }

}
