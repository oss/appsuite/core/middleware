/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.file.storage.composition.internal;

import static com.openexchange.file.storage.composition.internal.AbstractCompositingIDBasedFolderAccess.INFOSTORE_FOLDER_ID;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.CacheAware;
import com.openexchange.file.storage.DefaultFileStoragePermission;
import com.openexchange.file.storage.DefaultTypeAwareFileStorageFolder;
import com.openexchange.file.storage.FileStorageAccount;
import com.openexchange.file.storage.FileStorageFolder;
import com.openexchange.file.storage.FileStorageFolderType;
import com.openexchange.file.storage.FileStoragePermission;
import com.openexchange.file.storage.RootFolderPermissionsAware;
import com.openexchange.file.storage.composition.FolderID;
import com.openexchange.session.Session;

/**
 * {@link FileStorageRootFolder}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class FileStorageRootFolder extends DefaultTypeAwareFileStorageFolder implements CacheAware {

    /**
     * Initializes a file storage root folder for a specific file storage account.
     *
     * @param session The user session to construct the root folder for
     * @param account The account
     */
    public FileStorageRootFolder(Session session, FileStorageAccount account) throws OXException {
        this(session.getUserId(), account.getFileStorageService().getId(), account.getId(), account.getDisplayName(), getRootFolderPermissions(session, account));
    }

    /**
     * Initializes a file storage root folder for a specific file storage account.
     *
     * @param userID The user identifier to construct the root folder for
     * @param serviceID The account's service identifier
     * @param accountID The account identifier
     * @param displayName The folder name to use, usually the account's display name
     * @param rootFolderPermissions The optional root folder permissions
     */
    public FileStorageRootFolder(int userID, String serviceID, String accountID, String displayName, List<FileStoragePermission> rootFolderPermissions) {
        super();
        setParentId(INFOSTORE_FOLDER_ID);
        setId(new FolderID(serviceID, accountID, FileStorageFolder.ROOT_FULLNAME).toUniqueID());
        setName(displayName);
        setType(FileStorageFolderType.NONE);
        setSubscribed(true);
        setSubfolders(true);
        setSubscribedSubfolders(true);
        setRootFolder(true);
        setHoldsFiles(true);
        setHoldsFolders(true);
        setExists(true);
        if (null == rootFolderPermissions || rootFolderPermissions.isEmpty()) {
            FileStoragePermission permission = getDefaultPermission(userID);
            setPermissions(Collections.singletonList(permission));
            setOwnPermission(permission);
        } else {
            setPermissions(rootFolderPermissions);
            getOwnPermission(rootFolderPermissions, userID).ifPresent(p -> setOwnPermission(p));
        }
        setCreatedBy(userID);
        setModifiedBy(userID);
    }

    @Override
    public boolean cacheable() {
        return false;
    }

    private static Optional<FileStoragePermission> getOwnPermission(List<FileStoragePermission> permissions, int userId) {
        if (null != permissions) {
            for (FileStoragePermission permission : permissions) {
                if (permission.getEntity() == userId) {
                    return Optional.of(permission);
                }
            }
        }
        return Optional.empty();
    }

    private static FileStoragePermission getDefaultPermission(int userId) {
        DefaultFileStoragePermission permission = DefaultFileStoragePermission.newInstance();
        permission.setAdmin(true);
        permission.setFolderPermission(FileStoragePermission.CREATE_SUB_FOLDERS);
        permission.setEntity(userId);
        return permission;
    }

    private static List<FileStoragePermission> getRootFolderPermissions(Session session, FileStorageAccount account) throws OXException {
        if (account.getFileStorageService() instanceof RootFolderPermissionsAware rootFolderPermissionsAware) {
            return rootFolderPermissionsAware.getRootFolderPermissions(account.getId(), session);
        }
        return null;
    }

}
