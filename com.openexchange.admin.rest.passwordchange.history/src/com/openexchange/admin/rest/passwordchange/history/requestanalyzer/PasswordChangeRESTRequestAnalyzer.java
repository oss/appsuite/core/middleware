/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.rest.passwordchange.history.requestanalyzer;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.segment.SegmentMarker;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * {@link PasswordChangeRESTRequestAnalyzer} - {@link RequestAnalyzer} for passwordchange admin REST servlet
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public class PasswordChangeRESTRequestAnalyzer implements RequestAnalyzer {

    private static final Logger LOG = LoggerFactory.getLogger(PasswordChangeRESTRequestAnalyzer.class);
    private static final Pattern PASSWORD_CHANGE_PATTERN = Pattern.compile("/admin/v1/contexts/(\\d*?)/users/(\\d*?)/passwd-changes", Pattern.DOTALL);

    private final ErrorAwareSupplier<DatabaseService> dbServiceSupplier;

    public PasswordChangeRESTRequestAnalyzer(ServiceLookup services) {
        super();
        this.dbServiceSupplier = () -> services.getServiceSafe(DatabaseService.class);
    }

    @Override
    public Optional<AnalyzeResult> analyze(RequestData data) throws OXException {
        if (false == "GET".equals(data.getMethod())) {
            // not a request for the passwordchange REST service
            return Optional.empty();
        }
        Optional<String> path = data.getParsedURL().getPath();
        if (path.isEmpty()) {
            // not a request for the passwordchange REST service
            return Optional.empty();
        }
        Matcher matcher = PASSWORD_CHANGE_PATTERN.matcher(path.get());
        if (false == matcher.matches()) {
            // not a request for the passwordchange REST service
            return Optional.empty();
        }
        String sCtxId = matcher.group(1);
        int ctxId;
        try {
            ctxId = Integer.parseInt(sCtxId);
        } catch (NumberFormatException e) {
            LOG.info("Malformed request, parameter {} must be an integer.", sCtxId);
            return Optional.empty();
        }
        String schema = dbServiceSupplier.get().getSchemaName(ctxId);
        AnalyzeResult result = new AnalyzeResult(SegmentMarker.of(schema), null);
        return Optional.of(result);
    }

}
