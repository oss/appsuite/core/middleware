/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.push.imapidle.locking;

import java.time.Duration;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.ClusterMap;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.cluster.map.codec.MapCodecs;
import com.openexchange.exception.OXException;
import com.openexchange.server.ServiceLookup;


/**
 * {@link ClusterMapImapIdleClusterLock} - The cluster lock using cluster map.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ClusterMapImapIdleClusterLock extends AbstractImapIdleClusterLock {

    private final BasicCoreClusterMapProvider<String> clusterMapProvider;

    /**
     * Initializes a new {@link ClusterMapImapIdleClusterLock}.
     *
     * @param services The service look-up
     */
    public ClusterMapImapIdleClusterLock(ServiceLookup services) {
        super(services);
        clusterMapProvider = BasicCoreClusterMapProvider.<String> builder() //@formatter:off
            .withCoreMap(CoreMap.IMAPIDLE_PUSH_LOCKS)
            .withCodec(MapCodecs.getStringCodec())
            .withExpireMillis(Duration.ofMinutes(11).toMillis())
            .withServiceSupplier(() -> services.getServiceSafe(ClusterMapService.class))
            .build(); //@formatter:on
    }

    private String generateKey(SessionInfo sessionInfo) {
        return new StringBuilder(16).append(sessionInfo.getUserId()).append('@').append(sessionInfo.getContextId()).toString();
    }

    @Override
    public Type getType() {
        return Type.CLUSTER_MAP;
    }

    @Override
    public AcquisitionResult acquireLock(SessionInfo sessionInfo) throws OXException {
        ClusterMap<String> map = clusterMapProvider.getMap();
        String key = generateKey(sessionInfo);

        long now = System.currentTimeMillis();
        String previous = map.putIfAbsent(key, generateValue(now, sessionInfo));

        if (null == previous) {
            // Not present before
            return AcquisitionResult.ACQUIRED_NEW;
        }

        // Check if valid
        Validity validity = validateValue(previous, now);
        if (Validity.VALID == validity) {
            // Locked
            return AcquisitionResult.NOT_ACQUIRED;
        }

        // Invalid entry - try to replace it mutually exclusive
        boolean replaced = map.replace(key, previous, generateValue(now, sessionInfo));
        if (false == replaced) {
            return AcquisitionResult.NOT_ACQUIRED;
        }

        switch (validity) {
            case NO_SUCH_SESSION:
                return AcquisitionResult.ACQUIRED_NO_SUCH_SESSION;
            case TIMED_OUT:
                return AcquisitionResult.ACQUIRED_TIMED_OUT;
            default:
                return AcquisitionResult.ACQUIRED_NEW;
        }
    }

    @Override
    public void refreshLock(SessionInfo sessionInfo) throws OXException {
        ClusterMap<String> map = clusterMapProvider.getMap();
        map.put(generateKey(sessionInfo), generateValue(System.currentTimeMillis(), sessionInfo));
    }

    @Override
    public void releaseLock(SessionInfo sessionInfo) throws OXException {
        ClusterMap<String> map = clusterMapProvider.getMap();
        map.remove(generateKey(sessionInfo));
    }

}
