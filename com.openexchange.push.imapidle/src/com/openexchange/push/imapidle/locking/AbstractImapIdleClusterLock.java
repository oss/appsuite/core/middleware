/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.push.imapidle.locking;

import com.openexchange.server.ServiceLookup;

/**
 * {@link AbstractImapIdleClusterLock} - The abstract lock implementation.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public abstract class AbstractImapIdleClusterLock implements ImapIdleClusterLock {

    static enum Validity {
        VALID, TIMED_OUT, NO_SUCH_SESSION;
    }

    /** The arguments to control validation of session existence */
    static class SessionValidationArgs {

        static final class Builder {

            private boolean validateSessionExistence;
            private boolean tranzient;

            Builder() {
                super();
            }

            Builder withValidateSessionExistence(boolean validateSessionExistence) {
                this.validateSessionExistence = validateSessionExistence;
                return this;
            }

            Builder withTranzient(boolean tranzient) {
                this.tranzient = tranzient;
                return this;
            }

            SessionValidationArgs build() {
                return new SessionValidationArgs(validateSessionExistence, tranzient);
            }
        }

        /** The constant for <b>no(!)</b> validation of session existence */
        static final SessionValidationArgs NO_SESSION_EXISTENCE_VALIDATION = new Builder().withValidateSessionExistence(false).build();

        /** <code>true</code> to perform validation of session existence; otherwise <code>false</code> to discard validation */
        final boolean validateSessionExistence;

        /** <code>true</code> if session is not supposed to be held in session storage; otherwise <code>false</code> */
        final boolean tranzient;

        /**
         * Initializes a new {@link SessionValidationArds}.
         */
        SessionValidationArgs(boolean validateSessionExistence, boolean tranzient) {
            super();
            this.validateSessionExistence = validateSessionExistence;
            this.tranzient = tranzient;
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** The service look-up */
    protected final ServiceLookup services;

    /**
     * Initializes a new {@link AbstractImapIdleClusterLock}.
     *
     * @param services The service look-up
     */
    protected AbstractImapIdleClusterLock(ServiceLookup services) {
        super();
        this.services = services;
    }

    /**
     * Generate an appropriate value for given time stamp and session identifier pair
     *
     * @param millis The time stamp
     * @param sessionInfo The session info
     * @return The value
     */
    protected String generateValue(long millis, SessionInfo sessionInfo) {
        if (sessionInfo.isPermanent()) {
            return Long.toString(millis);
        }
        return new StringBuilder(32).append(millis).append('?').append(sessionInfo.getSessionId()).toString();
    }

    /**
     * Checks validity of passed value in comparison to given time stamp (and session).
     *
     * @param value The value to check
     * @param now The current time stamp
     * @param validationArguments The arguments to control validation of session existence
     * @return <code>true</code> if valid; otherwise <code>false</code>
     */
    protected Validity validateValue(String value, long now) {
        return now - parseMillisFromValue(value) > TIMEOUT_MILLIS ? Validity.TIMED_OUT : Validity.VALID;
    }

    /**
     * Parses the time stamp milliseconds from given value
     *
     * @param value The value
     * @return The milliseconds
     */
    private static long parseMillisFromValue(String value) {
        int pos = value.indexOf('?');
        return Long.parseLong(pos > 0 ? value.substring(0, pos) : value);
    }

}
