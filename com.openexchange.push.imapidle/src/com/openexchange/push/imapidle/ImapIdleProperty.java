/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.push.imapidle;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import com.openexchange.config.lean.Property;

/**
 * {@link ImapIdleProperty}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public enum ImapIdleProperty implements Property {

    /**
     * The full name
     */
    FOLDER("folder", "INBOX"),

    /**
     * The delay
     */
    DELAY("delay", L(5000)),

    /**
     * The cluster lock
     */
    CLUSTER_LOCK("clusterLock", "cm"),

    /**
     * The account identifier
     */
    ACCOUNT_ID("accountId", I(0)),

    /**
     * The push mode
     */
    PUSH_MODE("pushMode", "always"),

    /**
     * Whether existence of expired IMAP IDLE listeners should happen periodically or through waiting take.
     */
    CHECK_PERIOD("checkPeriodic", Boolean.FALSE),

    /**
     * Whether to support permanent listeners
     */
    SUPPORTS_PERMANENT_LISTENERS("supportsPermanentListeners", Boolean.FALSE),

    ;

    private final String fqn;
    private final Object defaultValue;

    /**
     * Initializes a new {@link ImapIdleProperty}.
     *
     * @param appendix The appendix for the fully-qualifying name
     * @param defaultValue The default value
     */
    private ImapIdleProperty(String appendix, Object defaultValue) {
        this.fqn = "com.openexchange.push.imapidle." + appendix;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
