/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.continuation.osgi;

import java.time.Duration;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.openexchange.continuation.Continuation;
import com.openexchange.continuation.ContinuationRegistryService;
import com.openexchange.continuation.internal.ContinuationRegistryServiceImpl;
import com.openexchange.continuation.internal.DeprecationAwareCache;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.session.UserAndContext;
import com.openexchange.sessiond.SessiondEventConstants;
import com.openexchange.sessiond.SessiondService;


/**
 * {@link ContinuationActivator}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since 7.6.0
 */
public final class ContinuationActivator extends HousekeepingActivator {

    /**
     * Initializes a new {@link ContinuationActivator}.
     */
    public ContinuationActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { SessiondService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        final Logger logger = org.slf4j.LoggerFactory.getLogger(ContinuationActivator.class);
        try {
            // Initialize cache
            LoadingCache<UserAndContext, DeprecationAwareCache> cache = CacheBuilder.newBuilder()
                .maximumSize(20000)
                .expireAfterAccess(Duration.ofSeconds(360))
                .removalListener(new RemovalListener<UserAndContext, DeprecationAwareCache>() {

                    @Override
                    public void onRemoval(RemovalNotification<UserAndContext, DeprecationAwareCache> notification) {
                        notification.getValue().invalidateAll();
                    }
                })
                .build(new CacheLoader<UserAndContext, DeprecationAwareCache>() {

                    @Override
                    public DeprecationAwareCache load(UserAndContext key) throws Exception {
                        Cache<UUID, Continuation<?>> userCache = CacheBuilder.newBuilder()
                            .initialCapacity(16)
                            .expireAfterAccess(5, TimeUnit.MINUTES)
                            .maximumSize(100)
                            .removalListener(new RemovalListener<UUID, Continuation<?>>() {

                                @Override
                                public void onRemoval(final RemovalNotification<UUID, Continuation<?>> notification) {
                                    final Continuation<?> continuation = notification.getValue();
                                    if (null != continuation) {
                                        try {
                                            continuation.cancel(true);
                                        } catch (Exception x) {
                                            logger.warn("Failed to cancel continuation {}", continuation.getUuid());
                                        }
                                    }
                                }
                            })
                            .build();
                        return new DeprecationAwareCache(userCache);
                    }
                });

            {
                Dictionary<String, Object> serviceProperties = new Hashtable<String, Object>(1);
                serviceProperties.put(EventConstants.EVENT_TOPIC, SessiondEventConstants.TOPIC_LAST_SESSION);
                EventHandler eventHandler = new EventHandler() {

                    @Override
                    public void handleEvent(final Event event) {
                        final String topic = event.getTopic();
                        if (SessiondEventConstants.TOPIC_LAST_SESSION.equals(topic)) {
                            Integer contextId = (Integer) event.getProperty(SessiondEventConstants.PROP_CONTEXT_ID);
                            if (null != contextId) {
                                Integer userId = (Integer) event.getProperty(SessiondEventConstants.PROP_USER_ID);
                                if (null != userId) {
                                    try {
                                        cache.invalidate(UserAndContext.newInstance(userId.intValue(), contextId.intValue()));
                                    } catch (Exception e) {
                                        // Failed handling session
                                    }
                                }
                            }
                        }
                    }
                };
                registerService(EventHandler.class, eventHandler, serviceProperties);
            }

            registerService(ContinuationRegistryService.class, new ContinuationRegistryServiceImpl(cache));

            logger.info("Bundle \"com.openexchange.continuation\" successfully started");
        } catch (Exception e) {
            logger.error("Error while starting bundle \"com.openexchange.continuation\"", e);
            throw e;
        }
    }

    @Override
    protected void stopBundle() throws Exception {
        super.stopBundle();
        org.slf4j.LoggerFactory.getLogger(ContinuationActivator.class).info("Bundle \"com.openexchange.continuation\" successfully stopped");
    }

}
