/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.continuation.internal;

import java.util.UUID;
import com.google.common.cache.LoadingCache;
import com.openexchange.continuation.Continuation;
import com.openexchange.continuation.ContinuationRegistryService;
import com.openexchange.exception.OXException;
import com.openexchange.session.Session;
import com.openexchange.session.UserAndContext;

/**
 * {@link ContinuationRegistryServiceImpl}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since 7.6.0
 */
public class ContinuationRegistryServiceImpl implements ContinuationRegistryService {

    private final LoadingCache<UserAndContext, DeprecationAwareCache> cache;

    /**
     * Initializes a new {@link ContinuationRegistryServiceImpl}.
     *
     * @param cache The local cache to use
     */
    public ContinuationRegistryServiceImpl(LoadingCache<UserAndContext, DeprecationAwareCache> cache) {
        super();
        this.cache = cache;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <V> Continuation<V> getContinuation(final UUID uuid, final Session session) throws OXException {
        if (null != uuid && null != session) {
            DeprecationAwareCache userCache = cache.getIfPresent(UserAndContext.newInstance(session));
            if (userCache != null && userCache.isNotDeprecated()) {
                return (Continuation<V>) userCache.getIfPresent(uuid);
            }
        }
        return null;
    }

    @Override
    public <V> void putContinuation(final Continuation<V> continuation, final Session session) throws OXException {
        if (null != continuation && null != session) {
            DeprecationAwareCache userCache = cache.getUnchecked(UserAndContext.newInstance(session));
            synchronized (userCache) {
                if (userCache.isDeprecated()) {
                    putContinuation(continuation, session);
                } else {
                    if (null != userCache.asMap().putIfAbsent(continuation.getUuid(), continuation)) {
                        // Already present
                    }
                }
            }
        }
    }

    @Override
    public void removeContinuation(final UUID uuid, final Session session) throws OXException {
        if (null != uuid && null != session) {
            UserAndContext key = UserAndContext.newInstance(session);
            DeprecationAwareCache userCache = cache.getIfPresent(key);
            if (userCache != null) {
                synchronized (userCache) {
                    userCache.invalidate(uuid);
                    if (userCache.asMap().isEmpty()) {
                        userCache.markDeprecated();
                        cache.invalidate(key);
                    }
                }
            }
        }
    }

}
