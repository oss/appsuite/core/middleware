/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.continuation.internal;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheStats;
import com.google.common.collect.ImmutableMap;
import com.openexchange.continuation.Continuation;

/**
 * {@link DeprecationAwareCache} - Extends Guava cache by a deprecation marker that can be checked to ensure cache validity.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class DeprecationAwareCache implements Cache<UUID, Continuation<?>> {

    private final Cache<UUID, Continuation<?>> delegate;
    private final AtomicBoolean deprecated;

    /**
     * Initializes a new {@link DeprecationAwareCache}.
     *
     * @param delegate The cache to delegate to
     */
    public DeprecationAwareCache(Cache<UUID, Continuation<?>> delegate) {
        super();
        this.delegate = delegate;
        deprecated = new AtomicBoolean(false);
    }

    /**
     * Marks this cache as deprecated. Further usages (PUTs into cache, GETs from cache) should be avoided.
     */
    public void markDeprecated() {
        deprecated.set(true);
    }

    /**
     * Checks if this cache has been marked as deprecated.
     *
     * @return <code>true</code> if deprecated; otherwise <code>false</code>
     */
    public boolean isDeprecated() {
        return deprecated.get();
    }

    /**
     * Checks if this cache has <b>NOT</b> been marked as deprecated.
     *
     * @return <code>true</code> if <b>NOT</b> deprecated; otherwise <code>false</code> (if deprecated)
     */
    public boolean isNotDeprecated() {
        return !deprecated.get();
    }

    @Override
    public Continuation<?> getIfPresent(Object key) {
        return delegate.getIfPresent(key);
    }

    @Override
    public Continuation<?> get(UUID key, Callable<? extends Continuation<?>> loader) throws ExecutionException {
        return delegate.get(key, loader);
    }

    @Override
    public ImmutableMap<UUID, Continuation<?>> getAllPresent(Iterable<?> keys) {
        return delegate.getAllPresent(keys);
    }

    @Override
    public void put(UUID key, Continuation<?> value) {
        delegate.put(key, value);
    }

    @Override
    public void putAll(Map<? extends UUID, ? extends Continuation<?>> m) {
        delegate.putAll(m);
    }

    @Override
    public void invalidate(Object key) {
        delegate.invalidate(key);
    }

    @Override
    public void invalidateAll(Iterable<?> keys) {
        delegate.invalidateAll(keys);
    }

    @Override
    public void invalidateAll() {
        delegate.invalidateAll();
    }

    @Override
    public long size() {
        return delegate.size();
    }

    @Override
    public CacheStats stats() {
        return delegate.stats();
    }

    @Override
    public ConcurrentMap<UUID, Continuation<?>> asMap() {
        return delegate.asMap();
    }

    @Override
    public void cleanUp() {
        delegate.cleanUp();
    }

}
