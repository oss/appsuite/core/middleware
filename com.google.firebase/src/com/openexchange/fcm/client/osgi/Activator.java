/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.fcm.client.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.fcm.client.impl.FCMClientFactory;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.push.clients.PushClientFactory;

/**
 * {@link Activator}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class Activator extends HousekeepingActivator {

    private static final Logger LOG = LoggerFactory.getLogger(Activator.class);

    /**
     * Initialises a new {@link Activator}.
     */
    public Activator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return EMPTY_CLASSES;
    }

    @Override
    protected void startBundle() throws Exception {
        LOG.info("Starting bundle '{}'", context.getBundle().getSymbolicName());
        try {
            registerService(PushClientFactory.class, new FCMClientFactory());
            LOG.info("Bundle '{}' started.", context.getBundle().getSymbolicName());
        } catch (Exception e) {
            LOG.error("Failed starting '{}'", context.getBundle().getSymbolicName(), e);
        }
    }

    @Override
    public void stopBundle() throws Exception {
        LOG.info("Stopping bundle '{}'", context.getBundle().getSymbolicName());
        try {
            super.stopBundle();
            LOG.info("Bundle '{}' stopped.", context.getBundle().getSymbolicName());
        } catch (Exception e) {
            LOG.error("Failed stopping '{}'", context.getBundle().getSymbolicName(), e);
        }
    }
}
