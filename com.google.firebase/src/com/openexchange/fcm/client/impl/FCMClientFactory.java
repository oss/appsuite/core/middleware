/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
* 
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.fcm.client.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.push.clients.PushClientFactory;
import com.openexchange.push.clients.PushClientsExceptionCodes;

/**
 * {@link FCMClientFactory}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class FCMClientFactory implements PushClientFactory<FirebaseMessaging> {

    private static final Logger LOG = LoggerFactory.getLogger(FCMClientFactory.class);

    private static final String SCOPES = "https://www.googleapis.com/auth/firebase.messaging";
    private static final String TYPE = "fcm";

    /**
     * Initialises a new {@link FCMClientFactory}.
     */
    public FCMClientFactory() {
        super();
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public FirebaseMessaging create(Map<String, Object> config) throws OXException {
        String keyPath = (String) config.get("keyPath");
        if (Strings.isEmpty(keyPath)) {
            throw PushClientsExceptionCodes.INVALID_CONFIGURATION.create("Key path is either missing or empty");
        }
        String id = (String) config.get("id");
        if (Strings.isEmpty(id)) {
            throw PushClientsExceptionCodes.INVALID_CONFIGURATION.create("ID either missing or empty");
        }
        try (FileInputStream keyStream = new FileInputStream(keyPath)) {
            GoogleCredentials googleCredentials = GoogleCredentials.fromStream(keyStream).createScoped(Arrays.asList(SCOPES));
            FirebaseOptions options = FirebaseOptions.builder().setCredentials(googleCredentials).build();
            return createApp(options, id);
        } catch (IOException e) {
            throw PushClientsExceptionCodes.IO_ERROR.create(e, "Unable to load FCM key");
        }
    }

    /**
     * Creates the messaging app. If another app with the same name already exists,
     * it will be deleted and re-initialised
     * 
     * @param options The firebase options
     * @param id The app's id
     * @return The firebase messaging app
     */
    private FirebaseMessaging createApp(FirebaseOptions options, String id) {
        try {
            FirebaseApp firebaseApp = FirebaseApp.initializeApp(options, id);
            return FirebaseMessaging.getInstance(firebaseApp);
        } catch (IllegalStateException e) {
            LOG.debug("", e);
            FirebaseApp.getInstance(id).delete();
            FirebaseApp app = FirebaseApp.initializeApp(options, id);
            return FirebaseMessaging.getInstance(app);
        }
    }
}
