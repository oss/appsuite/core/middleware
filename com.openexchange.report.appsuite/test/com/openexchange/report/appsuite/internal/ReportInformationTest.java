/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.report.appsuite.internal;

import static com.openexchange.java.Autoboxing.B;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
/**
 * {@link ReportInformationTest}
 *
 * @author <a href="mailto:anna.ottersbach@open-xchange.com">Anna Ottersbach</a>
 * @since v7.10.4
 */
import com.openexchange.config.ConfigurationService;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;

public class ReportInformationTest {

    private static final List<String> OUTPUT_DEBIAN_PACKAGES = Arrays.asList(
        "open-xchange/unknown,now 7.10.3-3 all [installed]",
        "open-xchange-admin/unknown,now 7.10.3-3 all [installed]");

    private static final List<String> OUTPUT_PACKAGES_EXPECTED = Arrays.asList(
        "open-xchange", "open-xchange-admin");

    private static final List<String> OUTPUT_RHEL_CENTOS_AMAZONLINUX_PACKAGES = Arrays.asList(
        "open-xchange.noarch                     7.10.3-3_21.1      @ox-appsuite-backend",
        "open-xchange-admin.noarch               7.10.3-3_21.1      @ox-appsuite-backend");

    private static final List<String> OUTPUT_SLES_PACKAGES = Arrays.asList(
        "i | open-xchange       | 7.10.3-3                                                       | package",
        "i | open-xchange-admin | 7.10.3-3                                                       | package");

    private static final List<String> AMAZON_LINUX = Arrays.asList(
        "NAME=\"Amazon Linux AMI\"",
        "VERSION=\"2015.03\"",
        "ID=\"amzn\"",
        "ID_LIKE=\"rhel fedora\"",
        "VERSION_ID=\"2015.03\"",
        "PRETTY_NAME=\"Amazon Linux AMI 2015.03\"",
        "ANSI_COLOR=\"0;33\"",
        "CPE_NAME=\"cpe:/o:amazon:linux:2015.03:ga\"",
        "HOME_URL=\"http://aws.amazon.com/amazon-linux-ami/\"");

    private static final List<String> OUTPUT_CENTOS = Arrays.asList(
        "NAME=\"CentOS Linux\"",
        "VERSION=\"7 (Core)\"",
        "ID=\"centos\"",
        "ID_LIKE=\"rhel fedora\"",
        "VERSION_ID=\"7\"",
        "PRETTY_NAME=\"CentOS Linux 7 (Core)\"",
        "ANSI_COLOR=\"0;31\"",
        "CPE_NAME=\"cpe:/o:centos:centos:7\"",
        "HOME_URL=\"https://www.centos.org/\"",
        "BUG_REPORT_URL=\"https://bugs.centos.org/\"",
        "",
        "CENTOS_MANTISBT_PROJECT=\"CentOS-7\"",
        "CENTOS_MANTISBT_PROJECT_VERSION=\"7\"",
        "REDHAT_SUPPORT_PRODUCT=\"centos\"",
        "REDHAT_SUPPORT_PRODUCT_VERSION=\"7\"");

    private static final List<String> OUTPUT_DEBIAN = Arrays.asList(
        "PRETTY_NAME=\"Debian GNU/Linux 10 (buster)\"",
        "NAME=\"Debian GNU/Linux\"",
        "VERSION_ID=\"10\"",
        "VERSION=\"10 (buster)\"",
        "VERSION_CODENAME=buster",
        "ID=debian",
        "HOME_URL=\"https://www.debian.org/\"",
        "SUPPORT_URL=\"https://www.debian.org/support\"",
        "BUG_REPORT_URL=\"https://bugs.debian.org/\"");

    private static final List<String> OUTPUT_RHEL = Arrays.asList(
        "NAME=\"Red Hat Enterprise Linux Server\"",
        "VERSION=\"7.3 (Maipo)\"",
        "ID=\"rhel\"",
        "ID_LIKE=\"fedora\"",
        "VERSION_ID=\"7.3\"",
        "PRETTY_NAME=\"Red Hat Enterprise Linux Server 7.3 (Maipo)\"",
        "ANSI_COLOR=\"0;31\"",
        "CPE_NAME=\"cpe:/o:redhat:enterprise_linux:7.3:GA:server\"",
        "HOME_URL=\"https://www.redhat.com/\"",
        "BUG_REPORT_URL=\"https://bugzilla.redhat.com/\"",
        "REDHAT_BUGZILLA_PRODUCT=\"Red Hat Enterprise Linux 7\"",
        "REDHAT_BUGZILLA_PRODUCT_VERSION=7.3",
        "REDHAT_SUPPORT_PRODUCT=\"Red Hat Enterprise Linux\"",
        "REDHAT_SUPPORT_PRODUCT_VERSION=\"7.3\"");

    private static final List<String> OUTPUT_SLES = Arrays.asList(
        "NAME=\"SLES\"",
        "VERSION=\"12\"",
        "VERSION_ID=\"12\"",
        "PRETTY_NAME=\"SUSE Linux Enterprise Server 12\"",
        "ID=\"sles\"",
        "ANSI_COLOR=\"0;32\"",
        "CPE_NAME=\"cpe:/o:suse:sles:12\"");

    @Mock
    private ConfigurationService mockedConfigurationService;

    @Mock
    private Connection mockedConnection;

    @Mock
    private DatabaseMetaData mockedDatabaseMetaData;

    @Mock
    private DatabaseService mockedDatabaseService;

    private MockedStatic<Services> staticServicesMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        staticServicesMock = mockStatic(Services.class);
        when(Services.getService(ConfigurationService.class)).thenReturn(mockedConfigurationService);
    }

    @AfterEach
    public void tearDown() {
        staticServicesMock.close();
    }

    @Test
    public void testFormatPackageList_AmazonLinux() {
        List<String> output = ReportInformation.formatPackageList(OUTPUT_RHEL_CENTOS_AMAZONLINUX_PACKAGES, Distribution.AMAZONLINUX);
        assertEquals(OUTPUT_PACKAGES_EXPECTED.size(), output.size());
        for (int i = 0; i < OUTPUT_PACKAGES_EXPECTED.size(); i++) {
            assertEquals(OUTPUT_PACKAGES_EXPECTED.get(i), output.get(i));
        }
    }

    @Test
    public void testFormatPackageList_Centos() {
        List<String> output = ReportInformation.formatPackageList(OUTPUT_RHEL_CENTOS_AMAZONLINUX_PACKAGES, Distribution.CENTOS);
        assertEquals(OUTPUT_PACKAGES_EXPECTED.size(), output.size());
        for (int i = 0; i < OUTPUT_PACKAGES_EXPECTED.size(); i++) {
            assertEquals(OUTPUT_PACKAGES_EXPECTED.get(i), output.get(i));
        }
    }

    @Test
    public void testFormatPackageList_Debian() {
        List<String> output = ReportInformation.formatPackageList(OUTPUT_DEBIAN_PACKAGES, Distribution.DEBIAN);
        assertEquals(OUTPUT_PACKAGES_EXPECTED.size(), output.size());
        for (int i = 0; i < OUTPUT_PACKAGES_EXPECTED.size(); i++) {
            assertEquals(OUTPUT_PACKAGES_EXPECTED.get(i), output.get(i));
        }
    }

    @Test
    public void testFormatPackageList_NoLinux() {
        List<String> input = Arrays.asList("Test");
        List<String> output = ReportInformation.formatPackageList(input, Distribution.NOLINUX);
        assertEquals(input, output);
    }

    @Test
    public void testFormatPackageList_OutputEmpty() {
        List<String> output = ReportInformation.formatPackageList(Arrays.asList(), null);
        assertTrue(output.isEmpty());
    }

    @Test
    public void testFormatPackageList_OutputNull() {
        assertNull(ReportInformation.formatPackageList(null, null));
    }

    @Test
    public void testFormatPackageList_RHEL() {
        List<String> output = ReportInformation.formatPackageList(OUTPUT_RHEL_CENTOS_AMAZONLINUX_PACKAGES, Distribution.RHEL);
        assertEquals(OUTPUT_PACKAGES_EXPECTED.size(), output.size());
        for (int i = 0; i < OUTPUT_PACKAGES_EXPECTED.size(); i++) {
            assertEquals(OUTPUT_PACKAGES_EXPECTED.get(i), output.get(i));
        }
    }

    @Test
    public void testFormatPackageList_SLES() {
        List<String> output = ReportInformation.formatPackageList(OUTPUT_SLES_PACKAGES, Distribution.SLES);
        assertEquals(OUTPUT_PACKAGES_EXPECTED.size(), output.size());
        for (int i = 0; i < OUTPUT_PACKAGES_EXPECTED.size(); i++) {
            assertEquals(OUTPUT_PACKAGES_EXPECTED.get(i), output.get(i));
        }
    }

    @Test
    public void testFormatPackageList_Unknown() {
        List<String> input = Arrays.asList("Test");
        List<String> output = ReportInformation.formatPackageList(input, Distribution.UNKNOWN);
        assertEquals(input, output);
    }

    @Test
    public void testGetConfiguredThirdPartyAPIsNotOAuth_Empty() {
        List<ThirdPartyAPI> configuredAPIs = ReportInformation.getConfiguredThirdPartyAPIsNotOAuth();
        assertEquals(0, configuredAPIs.size());
    }

    @Test
    public void testGetConfiguredThirdPartyAPIsNotOAuth_Filled() {
        List<ThirdPartyAPI> noOAuthAPIs = Collections.emptyList();
        for (ThirdPartyAPI api : noOAuthAPIs) {
            String apiKey = "TestKey";
            when(mockedConfigurationService.getProperty(api.getPropertyName() + ".apiKey")).thenReturn(apiKey);
        }
        List<ThirdPartyAPI> configuredAPIs = ReportInformation.getConfiguredThirdPartyAPIsNotOAuth();
        assertEquals(noOAuthAPIs.size(), configuredAPIs.size());
        for (ThirdPartyAPI api : noOAuthAPIs) {
            assertTrue(configuredAPIs.contains(api));
        }
    }

    @Test
    public void testGetConfiguredThirdPartyAPIsViaOAuth_Empty() {
        List<ThirdPartyAPI> configuredAPIs = ReportInformation.getConfiguredThirdPartyAPIsViaOAuth();
        assertEquals(0, configuredAPIs.size());
    }

    @Test
    public void testGetConfiguredThirdPartyAPIsViaOAuth_Filled() {
        List<ThirdPartyAPI> oAuthAPIs = Arrays.asList(ThirdPartyAPI.BOXCOM, ThirdPartyAPI.DROPBOX, ThirdPartyAPI.GOOGLE, ThirdPartyAPI.MICROSOFT, ThirdPartyAPI.YAHOO);
        for (ThirdPartyAPI api : oAuthAPIs) {
            String enabledValue = B(true).toString();
            String apiKey = "TestKey";
            String apiSecret = "TestSecret";
            when(mockedConfigurationService.getProperty(api.getPropertyName())).thenReturn(enabledValue);
            when(mockedConfigurationService.getProperty(api.getPropertyName() + ".apiKey")).thenReturn(apiKey);
            when(mockedConfigurationService.getProperty(api.getPropertyName() + ".apiSecret")).thenReturn(apiSecret);
        }
        List<ThirdPartyAPI> settings = Arrays.asList(ThirdPartyAPI.TUMBLR, ThirdPartyAPI.FLICKR);
        for (ThirdPartyAPI api : settings) {
            String apiKey = "TestKey";
            when(mockedConfigurationService.getProperty(api.getPropertyName())).thenReturn(apiKey);
        }

        List<ThirdPartyAPI> configuredAPIs = ReportInformation.getConfiguredThirdPartyAPIsViaOAuth();
        assertEquals(oAuthAPIs.size() + settings.size(), configuredAPIs.size());
        for (ThirdPartyAPI api : oAuthAPIs) {
            assertTrue(configuredAPIs.contains(api));
        }
    }

    @Test
    public void testGetDatabaseVersion_OXException() throws OXException {
        when(Services.getService(DatabaseService.class)).thenReturn(mockedDatabaseService);
        when(mockedDatabaseService.getReadOnly()).thenThrow(new OXException());
        ReportInformation.getDatabaseVersion();
    }

    @Test
    public void testGetDatabaseVersion_SQLException() throws OXException, SQLException {
        when(Services.getService(DatabaseService.class)).thenReturn(mockedDatabaseService);
        when(mockedDatabaseService.getReadOnly()).thenReturn(mockedConnection);
        when(mockedConnection.getMetaData()).thenThrow(new SQLException());
        ReportInformation.getDatabaseVersion();
    }

    @Test
    public void testGetDatabaseVersion_Success() throws OXException, SQLException {
        String databaseName = "MySQL";
        String databaseVersion = "5.6.x";

        when(Services.getService(DatabaseService.class)).thenReturn(mockedDatabaseService);
        when(mockedDatabaseService.getReadOnly()).thenReturn(mockedConnection);
        when(mockedConnection.getMetaData()).thenReturn(mockedDatabaseMetaData);
        when(mockedDatabaseMetaData.getDatabaseProductName()).thenReturn(databaseName);
        when(mockedDatabaseMetaData.getDatabaseProductVersion()).thenReturn(databaseVersion);

        assertEquals(databaseName + " " + databaseVersion, ReportInformation.getDatabaseVersion());
    }

    @Test
    public void testGetDistribution_AmazonLinux() throws Exception {
        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(AMAZON_LINUX);

            assertEquals(Distribution.AMAZONLINUX, ReportInformation.getDistribution());
        }
    }

    @Test
    public void testGetDistribution_Centos() throws Exception {
        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_CENTOS);

            assertEquals(Distribution.CENTOS, ReportInformation.getDistribution());
        }
    }

    @Test
    public void testGetDistribution_Debian() throws Exception {
        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_DEBIAN);

            assertEquals(Distribution.DEBIAN, ReportInformation.getDistribution());
        }
    }

    @Test
    public void testGetDistribution_NoLinux() {
        System.setProperty("os.name", "MacOS");
        assertEquals(Distribution.NOLINUX, ReportInformation.getDistribution());
    }

    @Test
    public void testGetDistribution_RHEL() throws Exception {
        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_RHEL);

            assertEquals(Distribution.RHEL, ReportInformation.getDistribution());
        }
    }

    @Test
    public void testGetDistribution_SLES() throws Exception {
        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_SLES);

            assertEquals(Distribution.SLES, ReportInformation.getDistribution());
        }
    }

    @Test
    public void testGetDistribution_Unknown() throws Exception {
        setUpLinux();
        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release"))
                        .thenReturn(Arrays.asList("Empty"));

            Distribution distribution = ReportInformation.getDistribution();
            assertEquals(Distribution.UNKNOWN, distribution);
        }
    }

    @Test
    public void testGetDistributionName_LinuxAmazon() throws Exception {
        setUpLinux();
        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(AMAZON_LINUX);

            assertEquals("Amazon Linux AMI 2015.03", ReportInformation.getDistributionName());
        }
    }

    @Test
    public void testGetDistributionName_LinuxCentOS() throws Exception {
        setUpLinux();
        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_CENTOS);

            assertEquals("CentOS Linux 7 (Core)", ReportInformation.getDistributionName());
        }
    }

    @Test
    public void testGetDistributionName_LinuxDebian() throws Exception {
        setUpLinux();
        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_DEBIAN);

            assertEquals("Debian GNU/Linux 10 (buster)", ReportInformation.getDistributionName());
        }
    }

    @Test
    public void testGetDistributionName_LinuxRHEL() throws Exception {
        setUpLinux();
        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_RHEL);

            assertEquals("Red Hat Enterprise Linux Server 7.3 (Maipo)", ReportInformation.getDistributionName());
        }
    }

    @Test
    public void testGetDistributionName_LinuxSLES() throws Exception {
        setUpLinux();
        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_SLES);

            assertEquals("SUSE Linux Enterprise Server 12", ReportInformation.getDistributionName());
        }
    }

    @Test
    public void testGetDistributionName_NoLinux() {
        System.setProperty("os.name", "MacOS");
        assertEquals("", ReportInformation.getDistributionName());
    }

    @Test
    public void testGetDistributionName_NoPrettyName() throws Exception {
        setUpLinux();
        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release"))
                        .thenReturn(Arrays.asList("Debian"));

            String distributionName = ReportInformation.getDistributionName();
            assertEquals("", distributionName);
        }
    }

    @Test
    public void testInstalledPackages_AmazonLinux() throws Exception {
        setUpLinux();

        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("/bin/sh", "-c", "yum list installed | egrep 'open-xchange|readerengine'"))
                        .thenReturn(OUTPUT_RHEL_CENTOS_AMAZONLINUX_PACKAGES);
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_RHEL);

            List<String> output = ReportInformation.getInstalledPackages();
            assertEquals(OUTPUT_PACKAGES_EXPECTED.size(), output.size());
            for (int i = 0; i < OUTPUT_PACKAGES_EXPECTED.size(); i++) {
                assertEquals(OUTPUT_PACKAGES_EXPECTED.get(i), output.get(i));
            }
        }
    }

    @Test
    public void testInstalledPackages_Centos() throws Exception {
        setUpLinux();

        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("/bin/sh", "-c", "yum list installed | egrep 'open-xchange|readerengine'"))
                        .thenReturn(OUTPUT_RHEL_CENTOS_AMAZONLINUX_PACKAGES);
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_CENTOS);


            List<String> output = ReportInformation.getInstalledPackages();
            assertEquals(OUTPUT_PACKAGES_EXPECTED.size(), output.size());
            for (int i = 0; i < OUTPUT_PACKAGES_EXPECTED.size(); i++) {
                assertEquals(OUTPUT_PACKAGES_EXPECTED.get(i), output.get(i));
            }
        }
    }

    @Test
    public void testInstalledPackages_Debian() throws Exception {
        setUpLinux();

        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("/bin/sh", "-c", "apt --installed list | egrep 'open-xchange|readerengine'"))
                        .thenReturn(OUTPUT_DEBIAN_PACKAGES);
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_DEBIAN);

            List<String> output = ReportInformation.getInstalledPackages();
            assertEquals(OUTPUT_PACKAGES_EXPECTED.size(), output.size());
            for (int i = 0; i < OUTPUT_PACKAGES_EXPECTED.size(); i++) {
                assertEquals(OUTPUT_PACKAGES_EXPECTED.get(i), output.get(i));
            }
        }
    }

    @Test
    public void testInstalledPackages_RHEL() throws Exception {
        setUpLinux();

        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("/bin/sh", "-c", "yum list installed | egrep 'open-xchange|readerengine'"))
                        .thenReturn(OUTPUT_RHEL_CENTOS_AMAZONLINUX_PACKAGES);
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_RHEL);

            List<String> output = ReportInformation.getInstalledPackages();
            assertEquals(OUTPUT_PACKAGES_EXPECTED.size(), output.size());
            for (int i = 0; i < OUTPUT_PACKAGES_EXPECTED.size(); i++) {
                assertEquals(OUTPUT_PACKAGES_EXPECTED.get(i), output.get(i));
            }
        }
    }

    @Test
    public void testInstalledPackages_SLES() throws Exception {
        setUpLinux();

        try (MockedStatic<ReportInformation> mockedStatic = mockStatic(ReportInformation.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> ReportInformation.executeCommand("/bin/sh", "-c", "zypper se -i | egrep 'open-xchange|readerengine'"))
                        .thenReturn(OUTPUT_SLES_PACKAGES);
            mockedStatic.when(() -> ReportInformation.executeCommand("cat", "/etc/os-release")).thenReturn(OUTPUT_SLES);


            List<String> output = ReportInformation.getInstalledPackages();
            assertEquals(OUTPUT_PACKAGES_EXPECTED.size(), output.size());
            for (int i = 0; i < OUTPUT_PACKAGES_EXPECTED.size(); i++) {
                assertEquals(OUTPUT_PACKAGES_EXPECTED.get(i), output.get(i));
            }
        }
    }

    @Test
    public void testInstalledPackages_NoLinux() {
        System.setProperty("os.name", "MacOS");
        List<String> output = ReportInformation.getInstalledPackages();
        assertTrue(output.isEmpty());
    }

    @Test
    public void testInstalledPackages_Unknown() {
        setUpLinux();
        List<String> output = ReportInformation.getInstalledPackages();
        assertTrue(output.isEmpty());
    }

    @Test
    public void testIsLinux_MacOS() {
        System.setProperty("os.name", "MacOS");
        assertFalse(ReportInformation.isLinux());
    }

    @Test
    public void testIsLinux_Null() {
        System.setProperty("os.name", "null");
        assertFalse(ReportInformation.isLinux());
    }

    @Test
    public void testIsLinux_True() {
        setUpLinux();
        assertTrue(ReportInformation.isLinux());
    }

    private void setUpLinux() {
        System.setProperty("os.name", "Linux");
    }

}
