/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.monitoring.impl.internal.health;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;
import com.openexchange.log.LogMessageBuilder;

/**
 * {@link HealthMonitor} - Periodically outputs JVM health information.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class HealthMonitor implements Runnable {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(HealthMonitor.class);

    private final OperatingSystemMXBean osBean;
    private final MemoryMXBean memoryBean;
    private final GarbageCollectorMXBean minorGcBean;
    private final GarbageCollectorMXBean majorGcBean;

    private final Integer availableProcessors;

    private final Method totalPhysicalMemorySize;
    private final Method freePhysicalMemorySize;

    private final Method totalSwapSpaceSize;
    private final Method freeSwapSpaceSize;

    private final Method heapMemoryUsage;

    private final Method processCpuLoad;
    private final Method systemCpuLoad;
    private final Method systemLoadAverage;

    private final LogMessageBuilder sb;
    private int count;

    /**
     * Initializes a new instance of {@link HealthMonitor}.
     */
    public HealthMonitor() {
        super();

        OperatingSystemMXBean osBean = ManagementFactory.getOperatingSystemMXBean();
        this.osBean = osBean;

        //Method committedVirtualMemorySize = getMethod(osBean, "getCommittedVirtualMemorySize");

        freePhysicalMemorySize = getMethod(osBean, "getFreePhysicalMemorySize");
        totalPhysicalMemorySize = getMethod(osBean, "getTotalPhysicalMemorySize");
        totalSwapSpaceSize = getMethod(osBean, "getTotalSwapSpaceSize");
        freeSwapSpaceSize = getMethod(osBean, "getFreeSwapSpaceSize");

        //Method processCpuTime = getMethod(osBean, "getProcessCpuTime");
        //Method maxFileDescriptorCount = getMethod(osBean, "getMaxFileDescriptorCount");
        //Method openFileDescriptorCount = getMethod(osBean, "getOpenFileDescriptorCount");

        processCpuLoad = getMethod(osBean, "getProcessCpuLoad");
        systemCpuLoad = getMethod(osBean, "getSystemCpuLoad");
        systemLoadAverage = getMethod(osBean, "getSystemLoadAverage");

        this.availableProcessors = Integer.valueOf(Runtime.getRuntime().availableProcessors());

        MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();
        this.memoryBean = memoryBean;
        heapMemoryUsage = getMethod(memoryBean, "getHeapMemoryUsage");

        List<GarbageCollectorMXBean> gcBeans = ManagementFactory.getGarbageCollectorMXBeans();
        minorGcBean = gcBeans.get(0);
        majorGcBean = gcBeans.get(1);

        this.sb = LogMessageBuilder.createLogMessageBuilder(256, 10);
        this.count = 0;
    }

    /**
     * Gets the execution count.
     *
     * @return The execution count
     */
    private synchronized int getCount() {
        count++;
        if (count <= 0) {
            // Integer overflow
            count = 1;
        }
        return count;
    }

    @Override
    public void run() {
        try {
            // Check whether log as INFO or DEBUG message
            boolean info = getCount() % 10 == 0;
            if (info || LOG.isDebugEnabled()) {
                sb.reset();
                sb.appendln("Health information:");
                renderProcessors();
                renderPhysicalMemory();
                renderHeap();
                renderGc();
                renderLoad();
                renderSwap();
                if (info) {
                    LOG.info(sb.getMessage(), sb.getArgumentsAsArray());
                } else {
                    LOG.debug(sb.getMessage(), sb.getArgumentsAsArray());
                }
            }
        } catch (Exception e) {
            LOG.error("Failed to output health information", e);
        }
    }

    private void renderProcessors() {
        sb.appendln("    processors: {}", availableProcessors);
    }

    private void renderPhysicalMemory() throws IllegalAccessException, InvocationTargetException {
        sb.appendln("    physical.memory.total: {}", numberToUnit(invokeLongMethod(totalPhysicalMemorySize, osBean)));
        sb.appendln("    physical.memory.free: {}", numberToUnit(invokeLongMethod(freePhysicalMemorySize, osBean)));
    }

    private void renderSwap() throws IllegalAccessException, InvocationTargetException {
        sb.appendln("    swap.space.total: {}", numberToUnit(invokeLongMethod(totalSwapSpaceSize, osBean)));
        sb.appendln("    swap.space.free: {}", numberToUnit(invokeLongMethod(freeSwapSpaceSize, osBean)));
    }

    private void renderHeap() throws IllegalAccessException, InvocationTargetException {
        MemoryUsage memoryUsage = (MemoryUsage) heapMemoryUsage.invoke(memoryBean);

        long max = memoryUsage.getMax();
        if (max < 0) {
            max = memoryUsage.getCommitted();
        }

        sb.appendln("    heap.memory.used: {}", numberToUnit(memoryUsage.getUsed()));
        sb.appendln("    heap.memory.free: {}", numberToUnit(memoryUsage.getCommitted() - memoryUsage.getUsed()));
        sb.appendln("    heap.memory.total: {}", numberToUnit(memoryUsage.getCommitted()));
        sb.appendln("    heap.memory.max: {}", numberToUnit(max));
        sb.appendln("    heap.memory.used/total: {}", percentageString((double) memoryUsage.getUsed() / memoryUsage.getCommitted()));
        sb.appendln("    heap.memory.used/max: {}", percentageString((double) memoryUsage.getUsed() / max));
    }

    private void renderGc() {
        sb.appendln("    minor.gc.count: {}", Long.valueOf(minorGcBean.getCollectionCount()));
        sb.appendln("    minor.gc.time: {}ms", Long.valueOf(minorGcBean.getCollectionTime()));
        sb.appendln("    major.gc.count: {}", Long.valueOf(majorGcBean.getCollectionCount()));
        sb.appendln("    major.gc.time: {}ms", Long.valueOf(majorGcBean.getCollectionTime()));
    }

    private void renderLoad() throws IllegalAccessException, InvocationTargetException {
        sb.appendln("    load.process: {}%", String.format(Locale.US, "%.2f", invokeDoubleMethod(processCpuLoad, osBean)));
        sb.appendln("    load.system=: {}%", String.format(Locale.US, "%.2f", invokeDoubleMethod(systemCpuLoad, osBean)));

        Double value = systemLoadAverage == null ? Double.valueOf(-1) : invokeDoubleMethod(systemLoadAverage, osBean);
        if (value.doubleValue() < 0) {
            sb.appendln("    load.systemAverage: n/a");
        } else {
            sb.appendln("    load.systemAverage: {}", String.format(Locale.US, "%.2f", value));
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static long invokeLongMethod(Method method, Object bean) throws IllegalAccessException, InvocationTargetException {
        return ((Long) method.invoke(bean)).longValue();
    }

    private static Double invokeDoubleMethod(Method method, Object bean) throws IllegalAccessException, InvocationTargetException {
        return (Double) method.invoke(bean);
    }

    private static final String[] UNITS = new String[] { "", "K", "M", "G", "T", "P", "E" };

    private static String numberToUnit(long number) {
        for (int i = 6; i > 0; i--) {
            // 1024 is for 1024 kb is 1 MB etc
            double step = Math.pow(1024, i);
            if (number > step) {
                return String.format(Locale.US, "%3.1f%s", Double.valueOf(number / step), UNITS[i]);
            }
        }
        return Long.toString(number);
    }

    /**
     * Given a number, returns that number as a percentage string.
     *
     * @param p the given number
     * @return a string of the given number as a format float with two decimal places and a period
     */
    private static String percentageString(double p) {
        return String.format(Locale.US, "%.2f%%", Double.valueOf(p));
    }

    /**
     * Returns a method from the given source object.
     *
     * @param source The source object.
     * @param methodName The name of the method to retrieve.
     * @return The method or <code>null</code>
     */
    private static Method getMethod(Object source, String methodName) {
        try {
            Method method = source.getClass().getMethod(methodName);
            method.setAccessible(true); // NOSONARLINT
            return method;
        } catch (Exception e) {
            LOG.error("Failed to acquire method {} from {}", methodName, source.getClass().getName(), e);
            return null;
        }
    }

}
