/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.scheduling.common;

import java.util.Locale;
import java.util.TimeZone;
import com.openexchange.chronos.CalendarUser;
import com.openexchange.chronos.CalendarUserType;
import com.openexchange.chronos.scheduling.RecipientSettings;
import com.openexchange.regional.RegionalSettings;

/**
 * {@link FallbackRecipientSettings} - Recipient settings with default values
 *
 * @author <a href="mailto:daniel.beckerlopen-xchange.com">Daniel Becker</a>
 */
public class FallbackRecipientSettings implements RecipientSettings {

    private final CalendarUser recipient;
    private final TimeZone timeZone;
    private final Locale locale;

    /**
     * Initializes a new {@link FallbackRecipientSettings}.
     *
     * @param recipient The recipient
     * @param timeZone The timezone to use
     * @param locale The locale to use
     */
    public FallbackRecipientSettings(CalendarUser recipient, TimeZone timeZone, Locale locale) {
        super();
        this.recipient = recipient;
        this.timeZone = timeZone;
        this.locale = locale;
    }

    @Override
    public TimeZone getTimeZone() {
        return timeZone;
    }

    @Override
    public RegionalSettings getRegionalSettings() {
        return null;
    }

    @Override
    public CalendarUserType getRecipientType() {
        return CalendarUserType.INDIVIDUAL;
    }

    @Override
    public CalendarUser getRecipient() {
        return recipient;
    }

    @Override
    public int getMsgFormat() {
        return 3; // See UserSettingMail.MSG_FORMAT_BOTH
    }

    @Override
    public Locale getLocale() {
        return locale;
    }
}
