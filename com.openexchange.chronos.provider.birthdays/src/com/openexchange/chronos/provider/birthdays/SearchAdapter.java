/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.provider.birthdays;

import com.openexchange.chronos.EventField;
import com.openexchange.contact.ContactFieldOperand;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contact.helpers.ContactField;
import com.openexchange.search.CompositeSearchTerm;
import com.openexchange.search.CompositeSearchTerm.CompositeOperation;
import com.openexchange.search.Operand;
import com.openexchange.search.SearchExceptionMessages;
import com.openexchange.search.SearchTerm;
import com.openexchange.search.SingleSearchTerm;
import com.openexchange.search.SingleSearchTerm.SingleOperation;
import com.openexchange.search.internal.operands.ConstantOperand;

/**
 * {@link SearchAdapter}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @since 7.10.0
 */
public class SearchAdapter {

    /** The contact fields considered for a birthday event's summary */
    private static final ContactField[] SUMMARY_FIELDS = { ContactField.DISPLAY_NAME, ContactField.SUR_NAME, ContactField.GIVEN_NAME };

    /**
     * Gets a contact search term for the supplied calendar search term.
     *
     * @param calendarTerm The calendar term to get the contact search term for
     * @return The search term, or <code>null</code> if all contacts would be matched
     */
    public static SearchTerm<?> getContactSearchTerm(SearchTerm<?> calendarTerm) throws OXException {
        if ((calendarTerm instanceof SingleSearchTerm)) {
            SingleSearchTerm singleSearchTerm = (SingleSearchTerm) calendarTerm;
            Operand<?> constantOperand = requireSingleOperand(singleSearchTerm, Operand.Type.CONSTANT);
            Operand<?> columnOperand = requireSingleOperand(singleSearchTerm, Operand.Type.COLUMN);
            if (false == (columnOperand.getValue() instanceof EventField)) {
                throw SearchExceptionMessages.PARSING_FAILED_UNSUPPORTED_OPERAND.create(columnOperand);
            }
            return getContactSearchTerm((EventField) columnOperand.getValue(), singleSearchTerm.getOperation(), constantOperand);
        }
        if ((calendarTerm instanceof CompositeSearchTerm)) {
            CompositeSearchTerm contactCompositeSearchTerm = new CompositeSearchTerm((CompositeOperation) calendarTerm.getOperation());
            for (SearchTerm<?> calendarTermOperand : ((CompositeSearchTerm) calendarTerm).getOperands()) {
                contactCompositeSearchTerm.addSearchTerm(getContactSearchTerm(calendarTermOperand));
            }
            return contactCompositeSearchTerm;
        }
        throw new IllegalArgumentException("Need either an 'SingleSearchTerm' or 'CompositeSearchTerm'.");
    }

    /**
     * Extracts a single operand of a certain type from the supplied single search term, ensuring that exactly one operand of this type is
     * present in the term.
     *
     * @param searchTerm The search term to extract the operand from
     * @param type The type of the operand to extract
     * @return The operand
     * @throws IllegalArgumentException If no or more than one operands of this type are found in the term
     */
    private static Operand<?> requireSingleOperand(SingleSearchTerm searchTerm, Operand.Type type) {
        Operand<?> singleOperand = null;
        for (Operand<?> operand : searchTerm.getOperands()) {
            if (type.equals(operand.getType())) {
                if (null != singleOperand) {
                    throw new IllegalArgumentException("Multiple operands of type " + type + " in term " + searchTerm);
                }
                singleOperand = operand;
            }
        }
        if (null == singleOperand) {
            throw new IllegalArgumentException("No operand of type " + type + " in term " + searchTerm);
        }
        return singleOperand;
    }

    /**
     * Gets a search term for looking up contacts matching a certain event field criteria.
     *
     * @param matchedField The event field to match
     * @param singleOperation The underlying search term's operation
     * @param constantOperand The underlying search term's constant operand
     * @return The contact search term
     * @throws OXException If search is not supported
     */
    private static SearchTerm<?> getContactSearchTerm(EventField matchedField, SingleOperation singleOperation, Operand<?> constantOperand) throws OXException {
        switch (matchedField) {
            case LOCATION:
            case DESCRIPTION:
            case ORGANIZER:
            case URL:
            case COLOR:
            case SEQUENCE:
            case CATEGORIES:
            case STATUS:
                return new SingleSearchTerm(singleOperation).addOperand(new ConstantOperand<String>("")).addOperand(constantOperand);
            case RECURRENCE_RULE:
                return new SingleSearchTerm(singleOperation).addOperand(new ConstantOperand<String>(EventConverter.BIRTHDAYS_RRULE)).addOperand(constantOperand);
            case TRANSP:
                return new SingleSearchTerm(singleOperation).addOperand(new ConstantOperand<String>(EventConverter.BIRTHDAYS_TRANSP.getValue())).addOperand(constantOperand);
            case CLASSIFICATION:
                return new SingleSearchTerm(singleOperation).addOperand(new ConstantOperand<String>(EventConverter.BIRTHDAYS_CLASSIFICATION.getValue())).addOperand(constantOperand);
            case TIMESTAMP:
            case DTSTAMP:
            case LAST_MODIFIED:
                return getContactFieldTerm(ContactField.LAST_MODIFIED, singleOperation, constantOperand);
            case CREATED:
                return getContactFieldTerm(ContactField.CREATION_DATE, singleOperation, constantOperand);
            case UID:
                return getContactFieldTerm(ContactField.UID, singleOperation, constantOperand);
            case SUMMARY:
                return getSummaryTerm(singleOperation, constantOperand);
            case ATTENDEES:
                return getAttendeesTerm(singleOperation, constantOperand);
            default:
                throw SearchExceptionMessages.PARSING_FAILED_UNSUPPORTED_OPERAND.create(matchedField);
        }
    }

    private static CompositeSearchTerm getSummaryTerm(SingleOperation singleOperation, Operand<?> constantOperand) {
        CompositeSearchTerm orTerm = new CompositeSearchTerm(CompositeOperation.OR);
        for (ContactField field : SUMMARY_FIELDS) {
            orTerm.addSearchTerm(getContactFieldTerm(field, singleOperation, constantOperand));
        }
        return orTerm;
    }

    private static SingleSearchTerm getAttendeesTerm(SingleOperation singleOperation, Operand<?> constantOperand) {
        if ((constantOperand.getValue() instanceof Number)) {
            /*
             * match against contact user id (as attendee entity identifier)
             */
            return new SingleSearchTerm(singleOperation).addOperand(new ContactFieldOperand(ContactField.INTERNAL_USERID)).addOperand(constantOperand);
        }
        /*
         * match against contact email1 (as attendee uri), otherwise
         */
        return new SingleSearchTerm(singleOperation).addOperand(new ContactFieldOperand(ContactField.EMAIL1)).addOperand(constantOperand);
    }

    private static SingleSearchTerm getContactFieldTerm(ContactField matchedField, SingleOperation singleOperation, Operand<?> constantOperand) {
        return new SingleSearchTerm(singleOperation).addOperand(new ContactFieldOperand(matchedField)).addOperand(constantOperand);
    }

    /**
     * Initializes a new {@link SearchAdapter}.
     */
    private SearchAdapter() {
        super();
    }

}
