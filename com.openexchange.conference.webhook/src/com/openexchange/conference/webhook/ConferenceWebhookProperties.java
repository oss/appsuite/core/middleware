/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.conference.webhook;

import com.openexchange.config.lean.Property;

/**
 * {@link ConferenceWebhookProperties}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 * @since v7.10.4
 */
public enum ConferenceWebhookProperties implements Property {

    BASE_URI("baseUri", ""),
    SECRET("secret", ""),
    ENABLE_ZOOM_INTERCEPTOR("enableZoomInterceptor", Boolean.TRUE),
    ENABLE_JITSI_INTERCEPTOR("enableJitsiInterceptor", Boolean.TRUE),
    ;

    private final String fqn;
    private final Object defaultValue;

    /**
     * Initializes a new {@link ConferenceWebhookProperties}.
     *
     * @param appendix The appendix for the fully-qualifying name
     * @param defaultValue The default value
     */
    private ConferenceWebhookProperties(String appendix, Object defaultValue) {
        this.fqn = "com.openexchange.conference.webhook." + appendix;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
