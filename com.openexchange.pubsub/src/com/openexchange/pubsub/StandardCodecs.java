/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.pubsub;

import com.openexchange.session.UserAndContext;

/**
 * {@link StandardCodecs} - Provides standard codecs for pub/sub messages.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class StandardCodecs {

    /**
     * Initializes a new {@link StandardCodecs}.
     */
    private StandardCodecs() {
        super();
    }

    /** The channel message code for a <code>boolean</code> value */
    public static final ChannelMessageCodec<Boolean> BOOLEAN = BooleanChannelMessageCodec.getInstance();

    /** The channel message code for a <code>String</code> value */
    public static final ChannelMessageCodec<String> STRING = StringChannelMessageCodec.getInstance();

    /** The channel message codec for a user-and-context identifier */
    public static final ChannelMessageCodec<UserAndContext> USER_AND_CONTEXT = UserAndContextChannelMessageCodec.getInstance();

}
