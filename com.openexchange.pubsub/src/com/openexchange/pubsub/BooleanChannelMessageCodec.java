/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.pubsub;


/**
 * {@link BooleanChannelMessageCodec} - The channel message code for a <code>boolean</code> value.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class BooleanChannelMessageCodec implements ChannelMessageCodec<Boolean> {

    private static final BooleanChannelMessageCodec INSTANCE = new BooleanChannelMessageCodec();

    /**
     * Gets the instance
     *
     * @return The instance
     * @deprecated Use {@link StandardCodecs#BOOLEAN}
     */
    @Deprecated
    public static BooleanChannelMessageCodec getInstance() {
        return INSTANCE;
    }

    // ------------------------------------------------------------------------------------------------------------------------

    /**
     * Initializes a new {@link BooleanChannelMessageCodec}.
     */
    private BooleanChannelMessageCodec() {
        super();
    }

    @Override
    public String serialize(Boolean message) throws Exception {
        return message.booleanValue() ? "true" : "false";
    }

    @Override
    public Boolean deserialize(String data) throws Exception {
        return Boolean.valueOf("true".equals(data));
    }

}
