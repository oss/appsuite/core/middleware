/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.pubsub;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 * {@link AbstractTrackingChannelListener} - The abstract channel listener that track Pub/Sub service.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @param <D> The type of message's data
 */
public abstract class AbstractTrackingChannelListener<D> implements ServiceTrackerCustomizer<PubSubService, PubSubService>, ChannelListener<D> {

    private final BundleContext context;

    /**
     * Initializes a new {@link AbstractTrackingChannelListener}.
     *
     * @param context The bundle context
     */
    protected AbstractTrackingChannelListener(BundleContext context) {
        super();
        this.context = context;
    }

    /**
     * Gets the channel to subscribe to/unsubscribe from.
     *
     * @param service The Pub/Sub service to use
     * @return The channel
     */
    protected abstract Channel<D> getChannel(PubSubService service);

    @Override
    public synchronized PubSubService addingService(ServiceReference<PubSubService> reference) {
        PubSubService service = context.getService(reference);
        try {
            Channel<D> channel = getChannel(service);
            channel.subscribe(this);
            return service;
        } catch (Exception e) {
            // Failed
            context.ungetService(reference);
            return null;
        }
    }

    @Override
    public void modifiedService(ServiceReference<PubSubService> reference, PubSubService service) {
        // Ignore
    }

    @Override
    public synchronized void removedService(ServiceReference<PubSubService> reference, PubSubService service) {
        try {
            Channel<D> channel = getChannel(service);
            channel.unsubscribe(this);
        } catch (Exception e) {
            // Failed
        } finally {
            context.ungetService(reference);
        }
    }

}
