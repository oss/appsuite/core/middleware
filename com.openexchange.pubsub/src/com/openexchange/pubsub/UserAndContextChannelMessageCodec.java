/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.pubsub;

import com.openexchange.session.UserAndContext;

/**
 * {@link UserAndContextChannelMessageCodec} - The channel message codec for a user-and-context identifier.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class UserAndContextChannelMessageCodec implements ChannelMessageCodec<UserAndContext> {

    private static final UserAndContextChannelMessageCodec INSTANCE = new UserAndContextChannelMessageCodec();

    /**
     * Gets the instance
     *
     * @return The instance
     * @deprecated Use {@link StandardCodecs#USER_AND_CONTEXT}
     */
    @Deprecated
    public static UserAndContextChannelMessageCodec getInstance() {
        return INSTANCE;
    }

    // ------------------------------------------------------------------------------------------------------------------------

    /**
     * Initializes a new {@link UserAndContextChannelMessageCodec}.
     */
    private UserAndContextChannelMessageCodec() {
        super();
    }

    @Override
    public String serialize(UserAndContext userAndContext) throws Exception {
        return userAndContext.toFormattedString();
    }

    @Override
    public UserAndContext deserialize(String data) throws Exception {
        return UserAndContext.parseFrom(data);
    }

}
