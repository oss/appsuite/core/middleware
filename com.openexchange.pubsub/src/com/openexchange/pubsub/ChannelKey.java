/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.pubsub;

import java.io.Serializable;

/**
 * {@link ChannelKey} - Represents a channel key that consists out of the application name and the channel name.
 * <p>
 * The resulting key may look like: <code>open-xchange-core:cacheevents</code>
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 *
 */
public interface ChannelKey extends Serializable, CompositeKey, Comparable<ChannelKey> {

    /**
     * Gets the channel part of the key.
     *
     * @return The channel part of the key, might be <code>null</code>
     */
    ChannelName getChannel();

    @Override
    default int compareTo(ChannelKey o) {
        return getChannel().getName().compareTo(o.getChannel().getName());
    }
}
