/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.config;

import java.util.function.Consumer;

/**
 * {@link ConsumingReloadable} - Reloadable implementation passing the reload invocation to a consumer.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class ConsumingReloadable implements Reloadable {

    private final Interests interests;
    private final Consumer<ConfigurationService> reloader;

    /**
     * Initializes a new {@link ConsumingReloadable}.
     * 
     * @param reloader The operation to invoke upon reload events
     * @param propertiesOfInterest The properties of interest
     */
    public ConsumingReloadable(Consumer<ConfigurationService> reloader, String... propertiesOfInterest) {
        this(reloader, Reloadables.interestsForProperties(propertiesOfInterest));
    }

    /**
     * Initializes a new {@link ConsumingReloadable}.
     * 
     * @param reloader The operation to invoke upon reload events
     * @param interest The interests
     */
    public ConsumingReloadable(Consumer<ConfigurationService> reloader, Interests interests) {
        super();
        this.interests = interests;
        this.reloader = reloader;
    }

    @Override
    public Interests getInterests() {
        return interests;
    }

    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        reloader.accept(configService);
    }

}
