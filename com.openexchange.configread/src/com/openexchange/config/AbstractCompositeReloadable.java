/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.config;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;
import com.openexchange.config.DefaultInterests.Builder;

/**
 * {@link AbstractCompositeReloadable} - The abstract implementation for a composite reloadable.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public abstract class AbstractCompositeReloadable implements Reloadable {

    /** The listing of reloadables to delegate to */
    protected final List<Reloadable> reloadables;

    /**
     * Initializes a new instance of {@link AbstractCompositeReloadable}.
     */
    protected AbstractCompositeReloadable() {
        super();
        reloadables = new CopyOnWriteArrayList<Reloadable>();
    }

    /**
     * Adds given {@link Reloadable} instance.
     *
     * @param reloadable The instance to add
     */
    public void addReloadable(Reloadable reloadable) {
        if (reloadable != null) {
            reloadables.add(reloadable);
        }
    }

    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        preReloadConfiguration(configService);

        for (Reloadable reloadable : reloadables) {
            reloadable.reloadConfiguration(configService);
        }

        postReloadConfiguration(configService);
    }

    @Override
    public Interests getInterests() {
        Set<String> fileNames = new TreeSet<>();
        String[] fileNamesOfInterest = getFileNamesOfInterest();
        if (fileNamesOfInterest != null && fileNamesOfInterest.length > 0) {
            fileNames.addAll(Arrays.asList(fileNamesOfInterest));
        }

        for (Reloadable reloadable : reloadables) {
            Interests interests = reloadable.getInterests();
            if (null != interests) {
                String[] configFileNames = interests.getConfigFileNames();
                if (null != configFileNames) {
                    fileNames.addAll(Arrays.asList(configFileNames));
                }
            }
        }

        Builder builder = DefaultInterests.builder();
        String[] propertiesOfInterest = getPropertiesOfInterest();
        if (propertiesOfInterest != null && propertiesOfInterest.length > 0) {
            builder.propertiesOfInterest(propertiesOfInterest);
        }
        if (!fileNames.isEmpty()) {
            builder.configFileNames(fileNames.toArray(new String[fileNames.size()]));
        }
        return builder.build();
    }

    /**
     * Invoked after reloadables were called.
     *
     * @param configService The configuration service
     */
    protected abstract void postReloadConfiguration(ConfigurationService configService);

    /**
     * Invoked before reloadables were called.
     *
     * @param configService The configuration service
     */
    protected abstract void preReloadConfiguration(ConfigurationService configService);

    /**
     * Gets the properties of interest that covers all added reloadable instances; e.g. <code>"com.openexchange.mail.*"</code>.
     *
     * @return The properties of interest
     */
    protected abstract String[] getPropertiesOfInterest();

    /**
     * Gets the file names of interest; e.g. <code>"mail.properties"</code>.
     *
     * @return The file names of interest
     */
    protected abstract String[] getFileNamesOfInterest();

}
