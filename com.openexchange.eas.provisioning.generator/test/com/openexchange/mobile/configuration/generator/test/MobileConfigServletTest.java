package com.openexchange.mobile.configuration.generator.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.SimConfigurationService;
import com.openexchange.mobile.configuration.generator.MobileConfigServlet;
import com.openexchange.mobile.configuration.generator.configuration.ConfigurationException;
import com.openexchange.mobile.configuration.generator.configuration.Property;
import com.openexchange.mobile.configuration.generator.osgi.Services;
import com.openexchange.server.SimpleServiceLookup;


@SuppressWarnings("serial")
public class MobileConfigServletTest extends MobileConfigServlet  {

     @Test
     public void testSplitUsernameAndDomain() throws ConfigurationException {
        final SimConfigurationService service = new SimConfigurationService();
        service.stringProperties.put(Property.DomainUser.getName(), "$USER@$DOMAIN");

        SimpleServiceLookup serviceLookup = new SimpleServiceLookup();
        serviceLookup.add(ConfigurationService.class, service);
        Services.setServiceLookup(serviceLookup);

        final String[] splitUsernameAndDomain = splitUsernameAndDomain("seppel@ox.de");
        Assertions.assertEquals("seppel", splitUsernameAndDomain[0], "Value at index 0 wrong");
        Assertions.assertEquals("ox.de", splitUsernameAndDomain[1], "Value at index 1 wrong");
    }

     @Test
     public void testSplitUsernameAndDomain2() throws ConfigurationException {
        final SimConfigurationService service = new SimConfigurationService();
        service.stringProperties.put(Property.DomainUser.getName(), "$DOMAIN@$USER");

        SimpleServiceLookup serviceLookup = new SimpleServiceLookup();
        serviceLookup.add(ConfigurationService.class, service);
        Services.setServiceLookup(serviceLookup);

        final String[] splitUsernameAndDomain = splitUsernameAndDomain("seppel@ox.de");
        Assertions.assertEquals("ox.de", splitUsernameAndDomain[0], "Value at index 0 wrong");
        Assertions.assertEquals("seppel", splitUsernameAndDomain[1], "Value at index 1 wrong");
    }

     @Test
     public void testSplitUsernameAndDomain3() throws ConfigurationException {
        final SimConfigurationService service = new SimConfigurationService();
        service.stringProperties.put(Property.DomainUser.getName(), "$DOMAIN|$USER");

        SimpleServiceLookup serviceLookup = new SimpleServiceLookup();
        serviceLookup.add(ConfigurationService.class, service);
        Services.setServiceLookup(serviceLookup);

        final String[] splitUsernameAndDomain = splitUsernameAndDomain("seppel@ox.de");
        Assertions.assertEquals("seppel@ox.de", splitUsernameAndDomain[0], "Value at index 0 wrong");
        Assertions.assertEquals("defaultcontext", splitUsernameAndDomain[1], "Value at index 1 wrong");
    }

     @Test
     public void testSplitUsernameAndDomain4() throws ConfigurationException {
        final SimConfigurationService service = new SimConfigurationService();
        service.stringProperties.put(Property.DomainUser.getName(), "$DOMAIN|$USER");

        SimpleServiceLookup serviceLookup = new SimpleServiceLookup();
        serviceLookup.add(ConfigurationService.class, service);
        Services.setServiceLookup(serviceLookup);

        final String[] splitUsernameAndDomain = splitUsernameAndDomain("seppel|ox.de");
        Assertions.assertEquals("ox.de", splitUsernameAndDomain[0], "Value at index 0 wrong");
        Assertions.assertEquals("seppel", splitUsernameAndDomain[1], "Value at index 1 wrong");
    }

     @Test
     public void testSplitUsernameAndDomain5() throws ConfigurationException {
        final SimConfigurationService service = new SimConfigurationService();
        service.stringProperties.put(Property.DomainUser.getName(), "$USER@$DOMAIN");

        SimpleServiceLookup serviceLookup = new SimpleServiceLookup();
        serviceLookup.add(ConfigurationService.class, service);
        Services.setServiceLookup(serviceLookup);

        final String[] splitUsernameAndDomain = splitUsernameAndDomain("seppel");
        Assertions.assertEquals("seppel", splitUsernameAndDomain[0], "Value at index 0 wrong");
        Assertions.assertEquals("defaultcontext", splitUsernameAndDomain[1], "Value at index 1 wrong");
    }

}
