/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.file.storage.mail;

import static com.openexchange.java.Autoboxing.L;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import javax.mail.MessagingException;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.FileStorageExceptionCodes;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.FileStorageFolder;
import com.openexchange.file.storage.mail.filter.MailDriveFile;
import com.openexchange.file.storage.mail.osgi.Services;
import com.openexchange.java.Strings;
import com.openexchange.mail.mime.ContentDisposition;
import com.openexchange.mail.mime.ContentType;
import com.openexchange.mail.mime.MessageHeaders;
import com.openexchange.mail.mime.MimeType2ExtMap;
import com.openexchange.mail.mime.MimeTypes;
import com.openexchange.mail.mime.utils.MimeMessageUtility;
import com.openexchange.mime.MimeTypeMap;
import com.sun.mail.imap.IMAPMessage;

/**
 * {@link MailDriveFileImpl}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.2
 */
public final class MailDriveFileImpl extends DefaultFile implements MailDriveFile {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(MailDriveFileImpl.class);

    /**
     * Parses an instance of <code>MailDriveFile</code> from specified IMAP message.
     *
     * @param message The backing IMAP message representing the attachment
     * @param folderId The folder identifier
     * @param id The file identifier
     * @param userId The user identifier
     * @param rootFolderId The identifier of the root folder
     * @return This parsed Mail Drive file <b>or <code>null</code> if specified IMAP message represents an invalid attachment</b>
     * @throws MessagingException If a messaging error occurs
     * @throws OXException If an Open-Xchange error occurs
     */
    public static MailDriveFileImpl parse(IMAPMessage message, String folderId, String id, int userId, String rootFolderId) throws MessagingException, OXException {
        return parse(message, folderId, id, userId, rootFolderId, null);
    }

    /**
     * Parses an instance of <code>MailDriveFile</code> from specified IMAP message.
     *
     * @param message The backing IMAP message representing the attachment
     * @param folderId The folder identifier
     * @param id The file identifier
     * @param userId The user identifier
     * @param rootFolderId The identifier of the root folder
     * @param fields The fields to consider
     * @return This parsed Mail Drive file <b>or <code>null</code> if specified IMAP message represents an invalid attachment</b>
     * @throws MessagingException If a messaging error occurs
     * @throws OXException If an Open-Xchange error occurs
     */
    public static MailDriveFileImpl parse(IMAPMessage message, String folderId, String id, int userId, String rootFolderId, List<Field> fields) throws MessagingException, OXException {
        return new MailDriveFileImpl(folderId, id, userId, rootFolderId).parseMessage(message, fields);
    }

    // -----------------------------------------------------------------------------------------------------------------------------

    private MailMetadataImpl metadata;

    /**
     * Initializes a new {@link MailDriveFileImpl}.
     *
     * @param folderId The folder identifier
     * @param id The file identifier
     * @param userId The user identifier
     * @param rootFolderId The identifier of the root folder
     */
    private MailDriveFileImpl(String folderId, String id, int userId, String rootFolderId) {
        super();
        setFolderId(isRootFolder(folderId, rootFolderId) ? FileStorageFolder.ROOT_FULLNAME : folderId);
        setCreatedBy(userId);
        setModifiedBy(userId);
        setId(id);
        setFileName(id);
        setVersion(FileStorageFileAccess.CURRENT_VERSION);
        setIsCurrentVersion(true);
    }

    private static boolean isRootFolder(String id, String rootFolderId) {
        return "".equals(id) || rootFolderId.equals(id);
    }

    /**
     * Gets the mail metadata for this file.
     *
     * @return The mail metadata, or <code>null</code> if not yet parsed
     */
    @Override
    public MailMetadataImpl getMetadata() {
        return metadata;
    }

    @Override
    public String toString() {
        final String url = getURL();
        return url == null ? super.toString() : url;
    }

    /**
     * Parses specified Mail Drive file.
     *
     * @param message The backing IMAP message representing the attachment
     * @param fields The fields to consider
     * @return This Mail Drive file with property set applied <b>or <code>null</code> if specified IMAP message represents an invalid attachment</b>
     * @throws MessagingException If a messaging error occurs
     * @throws OXException If parsing Mail Drive file fails
     */
    private MailDriveFileImpl parseMessage(IMAPMessage message, List<Field> fields) throws MessagingException, OXException {
        if (null != message) {
            try {
                String name = determineFileNameFor(message);
                setTitle(name);
                setFileName(name);
                boolean allFields = null == fields || fields.isEmpty();
                final Set<Field> set = allFields ? EnumSet.allOf(Field.class) : EnumSet.copyOf(fields);

                if (allFields || set.contains(Field.CREATED)) {
                    Date createdAt = message.getReceivedDate();
                    if (null != createdAt) {
                        setCreated(createdAt);
                    }
                }
                if (allFields || set.contains(Field.LAST_MODIFIED) || set.contains(Field.LAST_MODIFIED_UTC)) {
                    Date modifiedAt = message.getReceivedDate();
                    if (null != modifiedAt) {
                        setLastModified(modifiedAt);
                    }
                }
                if (allFields || set.contains(Field.FILE_MIMETYPE)) {
                    String contentType;
                    {
                        String[] tmp = message.getHeader(MessageHeaders.HDR_CONTENT_TYPE);
                        if ((tmp != null) && (tmp.length > 0)) {
                            contentType = MimeMessageUtility.decodeMultiEncodedHeader(tmp[0]);
                        } else {
                            contentType = MimeTypes.MIME_DEFAULT;
                        }
                    }
                    if (Strings.isEmpty(contentType)) {
                        MimeTypeMap map = Services.getService(MimeTypeMap.class);
                        contentType = map.getContentType(name);
                    }
                    if (null != contentType) {
                        int pos = contentType.indexOf(';');
                        if (pos > 0) {
                            contentType = contentType.substring(0, pos);
                        }
                        contentType = contentType.trim();
                    }
                    setFileMIMEType(contentType);
                }
                if (allFields || set.contains(Field.FILE_SIZE)) {
                    long size = message.getSize();
                    if (size >= 0) {
                        setFileSize(size);
                        setAccurateSize(false);
                    }
                }
                if (allFields || set.contains(Field.URL)) {
                    setURL(null);
                }
                if (allFields || set.contains(Field.COLOR_LABEL)) {
                    setColorLabel(0);
                }
                if (allFields || set.contains(Field.CATEGORIES)) {
                    setCategories(null);
                }
                if (allFields || set.contains(Field.DESCRIPTION)) {
                    setDescription(null);
                }
                if (allFields || set.contains(Field.VERSION_COMMENT)) {
                    setVersionComment(null);
                }

                // Prepare additional metadata
                this.metadata = new MailMetadataImpl(message);
            } catch (RuntimeException e) {
                throw FileStorageExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
            }
        }
        return this;
    }

    /**
     * Determines the file name to assume for specified IMAP message.
     *
     * @param message The IMAP message
     * @return The file name
     * @throws MessagingException If a suitable file name cannot be determined
     */
    private String determineFileNameFor(IMAPMessage message) throws MessagingException {
        String name = MimeMessageUtility.getSubject(message);
        if (Strings.isEmpty(name)) {
            name = extractFileNameFromContentDisposition(message);
            if (Strings.isEmpty(name)) {
                name = extractFileNameFromContentType(message);
            }
        }

        if (Strings.isEmpty(name)) {
            // File name unknown; neither by Subject, nor by Content-Type/Content-Disposition headers
            ContentType contentType = getContentTypeFromOrElse(message, ContentType.APPLICATION_OCTETSTREAM_CONTENT_TYPE);
            LOG.warn("Missing file name in IMAP message {} in folder {}. Trying to deduce from MIME type: {}", L(message.getUID()), getFolderId(), contentType);
            List<String> exts = MimeType2ExtMap.getFileExtensions(Strings.asciiLowerCase(contentType.getBaseType()));
            name = new StringBuilder(16).append("file.").append(exts == null ? "dat" : exts.get(0)).toString();
        }
        return name;
    }

    /**
     * Extracts possible file name from <code>"filename"</code> parameter of <code>"Content-Disposition"</code> header.
     * <pre>
     *  Content-Disposition: attachment; filename=letter.pdf
     * </pre>
     *
     * @param message The IMAP message to acquire the header from
     * @return The file name as specified by <code>"filename"</code> parameter of <code>"Content-Disposition"</code> header
     */
    private String extractFileNameFromContentDisposition(IMAPMessage message) {
        ContentDisposition cd = getContentDispositionFromOrElse(message, null);
        return cd == null ? null : cd.getFilenameParameter();
    }

    /**
     * Gets the <code>"Content-Disposition"</code> header from given IMAP message if such a header is present (and parseable); otherwise
     * given <code>defaultContentDisposition</code> is returned.
     *
     * @param message The IMAP message to get <code>"Content-Disposition"</code> header from
     * @param defaultContentDisposition The default disposition to return on absence or parsing failure
     * @return The disposition or <code>defaultContentDisposition</code>
     */
    private ContentDisposition getContentDispositionFromOrElse(IMAPMessage message, ContentDisposition defaultContentDisposition) {
        String hdrContentDisposition = null;
        try {
            hdrContentDisposition = message.getHeader(MessageHeaders.HDR_CONTENT_DISPOSITION, null);
            if (Strings.isNotEmpty(hdrContentDisposition)) {
                return new ContentDisposition(hdrContentDisposition);
            }
        } catch (Exception e) {
            if (hdrContentDisposition == null) {
                LOG.warn("Failed to acquire {} header from IMAP message {} in folder {}", MessageHeaders.HDR_CONTENT_TYPE, L(message.getUID()), getFolderId(), e);
            } else {
                LOG.warn("Failed to parse {} header from IMAP message {} in folder {}: {}", MessageHeaders.HDR_CONTENT_TYPE, L(message.getUID()), getFolderId(), hdrContentDisposition, e);
            }
        }
        return defaultContentDisposition;
    }

    /**
     * Extracts possible file name from <code>"name"</code> parameter of <code>"Content-Type"</code> header.
     * <pre>
     *  Content-Type: application/pdf; name=letter.pdf
     * </pre>
     *
     * @param message The IMAP message to acquire the header from
     * @return The file name as specified by <code>"name"</code> parameter of <code>"Content-Type"</code> header
     */
    private String extractFileNameFromContentType(IMAPMessage message) {
        ContentType ct = getContentTypeFromOrElse(message, null);
        return ct == null ? null : ct.getNameParameter();
    }

    /**
     * Gets the <code>"Content-Type"</code> header from given IMAP message if such a header is present (and parseable); otherwise given
     * <code>defaultContentType</code> is returned.
     *
     * @param message The IMAP message to get <code>"Content-Type"</code> header from
     * @param defaultContentType The default MIME type to return on absence or parsing failure
     * @return The MIME type or <code>defaultContentType</code>
     */
    private ContentType getContentTypeFromOrElse(IMAPMessage message, ContentType defaultContentType) {
        String hdrContentType = null;
        try {
            hdrContentType = message.getHeader(MessageHeaders.HDR_CONTENT_TYPE, null);
            if (Strings.isNotEmpty(hdrContentType)) {
                return new ContentType(hdrContentType);
            }
        } catch (Exception e) {
            if (hdrContentType == null) {
                LOG.warn("Failed to acquire {} header from IMAP message {} in folder {}", MessageHeaders.HDR_CONTENT_TYPE, L(message.getUID()), getFolderId(), e);
            } else {
                LOG.warn("Failed to parse {} header from IMAP message {} in folder {}: {}", MessageHeaders.HDR_CONTENT_TYPE, L(message.getUID()), getFolderId(), hdrContentType, e);
            }
        }
        return defaultContentType;
    }

}
