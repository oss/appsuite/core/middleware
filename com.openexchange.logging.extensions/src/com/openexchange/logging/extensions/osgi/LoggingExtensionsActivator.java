/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.logging.extensions.osgi;

import java.util.Arrays;
import java.util.List;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.DefaultInterests;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadable;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Strings;
import com.openexchange.logging.extensions.DenyAllFilter;
import com.openexchange.logging.extensions.OriginFilter;
import com.openexchange.logging.extensions.OriginFilterProperties;
import com.openexchange.logging.filter.ExtendedTurboFilter;
import com.openexchange.osgi.HousekeepingActivator;
import ch.qos.logback.classic.LoggerContext;

/**
 * {@link LoggingExtensionsActivator} - The activator for <code>"com.openexchange.logging.extensions"</code> bundle.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class LoggingExtensionsActivator extends HousekeepingActivator implements Reloadable {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(LoggingExtensionsActivator.class);

    private ExtendedTurboFilter filter;

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { LeanConfigurationService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        LOG.info("Starting logging extensions bundle.");
        addLoggingExtensionsFilter();
        registerService(Reloadable.class, this);
    }

    @Override
    protected void stopBundle() throws Exception {
        LOG.info("Stopping logging extensions bundle.");
        removeLoggingExtensionsFilter();
        super.stopBundle();
    }

    @Override
    public Interests getInterests() {
        OriginFilterProperties[] originFilterProperties = OriginFilterProperties.values();
        List<String> propertyNames = Arrays.stream(originFilterProperties).map(OriginFilterProperties::getFQPropertyName).collect(CollectorUtils.toList(originFilterProperties.length));
        return DefaultInterests.builder().propertiesOfInterest(propertyNames).build();
    }

    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        addLoggingExtensionsFilter();
    }

    /**
     * Adds (& remembers) the filter for logging extensions to logger context that is effective based on the current configuration.
     * <p>
     * Any previously added filter for logging extensions is removed from logger context.
     */
    private synchronized void addLoggingExtensionsFilter() {
        removeLoggingExtensionsFilter();
        ExtendedTurboFilter filter = getFilter();
        LoggerContext loggerContext = (LoggerContext) org.slf4j.LoggerFactory.getILoggerFactory();
        loggerContext.addTurboFilter(filter);
        this.filter = filter;
    }

    /**
     * Removes the previously added filter (if any) from logger context.
     */
    private synchronized void removeLoggingExtensionsFilter() {
        ExtendedTurboFilter filter = this.filter;
        if (filter != null) {
            this.filter = null;
            LoggerContext loggerContext = (LoggerContext) org.slf4j.LoggerFactory.getILoggerFactory();
            loggerContext.getTurboFilterList().remove(filter);
        }
    }

    /**
     * Gets the {@link ExtendedTurboFilter filter} that is effective based on the current configuration.
     *
     * @return The filter to use
     */
    private ExtendedTurboFilter getFilter() {
        LeanConfigurationService leanConfigurationService = getService(LeanConfigurationService.class);
        if (leanConfigurationService == null) {
            LOG.info("LeanConfigurationService not available. Use DenyAllFilter.");
            return new DenyAllFilter();
        }
        String authUsernames = leanConfigurationService.getProperty(OriginFilterProperties.AUTH_USERNAMES);
        String authBrandnames = leanConfigurationService.getProperty(OriginFilterProperties.AUTH_BRANDNAMES);
        String hosts = leanConfigurationService.getProperty(OriginFilterProperties.HOSTS);
        String remoteAddresses = leanConfigurationService.getProperty(OriginFilterProperties.REMOTE_ADDRESSES);

        if (Strings.isEmpty(authUsernames) && Strings.isEmpty(authBrandnames) && Strings.isEmpty(hosts) && Strings.isEmpty(remoteAddresses)) {
            LOG.debug("Did not find a configuration for extended logging.");
            return new DenyAllFilter();
        }

        LOG.debug("Configuration for extended logging found.");
        return new OriginFilter(authUsernames, authBrandnames, hosts, remoteAddresses);
    }

}
