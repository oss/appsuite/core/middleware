/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.logging.extensions;

import static com.openexchange.log.Markers.LOGGING_EXTENSIONS_MARKER;
import org.slf4j.Marker;
import com.openexchange.logging.filter.ExtendedTurboFilter;
import ch.qos.logback.classic.Logger;

/**
 * {@link AbstractLoggingExtensionsFilter} - The abstract filter for logging extensions.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public abstract class AbstractLoggingExtensionsFilter extends ExtendedTurboFilter {

    /**
     * Initializes a new {@link AbstractLoggingExtensionsFilter}.
     */
    protected AbstractLoggingExtensionsFilter() {
        super();
    }

    /**
     * Checks if specified marker does <b>not</b> signal logging extensions.
     *
     * @param marker The marker to check
     * @return <code>true</code> if marker does <b>not</b> signal logging extensions; otherwise <code>false</code>
     */
    protected static boolean isNoLoggingExtensions(Marker marker) {
        return !isLoggingExtensions(marker);
    }

    /**
     * Checks if specified marker signals logging extensions.
     *
     * @param marker The marker to check
     * @return <code>true</code> if marker signals logging extensions; otherwise <code>false</code>
     */
    protected static boolean isLoggingExtensions(Marker marker) {
        return marker != null && marker.contains(LOGGING_EXTENSIONS_MARKER);
    }

    /**
     * Checks is specified logger is <b>not</b> applicable for logging extensions.
     *
     * @param logger The logger to check
     * @return <code>true</code> if logger is <b>not</b> applicable; otherwise <code>false</code>
     */
    protected static boolean isNoApplicableLogger(Logger logger) {
        return !isApplicableLogger(logger);
    }

    /**
     * Checks is specified logger is applicable for logging extensions.
     *
     * @param logger The logger to check
     * @return <code>true</code> if logger is applicable; otherwise <code>false</code>
     */
    protected static boolean isApplicableLogger(Logger logger) {
        return logger != null && logger.getName() != null && logger.getName().startsWith("com.openexchange.logging.extensions.");
    }

}
