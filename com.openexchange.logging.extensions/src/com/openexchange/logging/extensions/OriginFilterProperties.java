/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.logging.extensions;

import com.openexchange.config.lean.Property;

/**
 * {@link OriginFilterProperties} - Properties for logging filter.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public enum OriginFilterProperties implements Property {

    /** A comma separated list of auth user names to enable logging for */
    AUTH_USERNAMES("authUsernames", ""),

    /** A comma separated list of brand names to enable logging for */
    AUTH_BRANDNAMES("authBrandnames", ""),

    /** A comma separated list of hosts or IP addresses to enable logging for */
    HOSTS("hosts", ""),

    /** A comma separated list of the clients IP addresses to enable logging for */
    REMOTE_ADDRESSES("remoteAddresses", ""),
    ;

    /** The FQN of the property */
    private final String fqn;

    /** The default value of the property */
    private final Object defaultValue;

    /**
     * Initializes a new {@link OriginFilterProperties}.
     *
     * @param name The name of the property.
     * @param defaultValue The default value
     */
    private OriginFilterProperties(String name, Object defaultValue) {
        this.fqn = "com.openexchange.logging.extensions." + name;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
