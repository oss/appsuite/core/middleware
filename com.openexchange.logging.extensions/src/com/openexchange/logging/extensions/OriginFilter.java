/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.logging.extensions;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.Marker;
import com.openexchange.java.Strings;
import com.openexchange.log.LogProperties;
import com.openexchange.log.LogProperties.Name;
import com.openexchange.net.HostList;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.core.spi.FilterReply;

/**
 * {@link OriginFilter} - The filter that applies to log events deciding if the event should be logged for the given auth username, hosts or client IP addresses.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class OriginFilter extends AbstractLoggingExtensionsFilter {

    private final Set<String> authUsernames;
    private final Set<String> authBrandnames;
    private final HostList hostlist;
    private final HostList remoteAddresses;

    /**
     * Initializes a new {@link OriginFilter}.
     *
     * @param authUsernames A comma-separated list of authUsername the request might be initiated by
     * @param authBrandnames A comma-separated list of brand names the request might be initiated by
     * @param hosts A comma-separated list of hosts the request was sent to
     * @param remoteAddresses A comma-separated list of clients IP addresses the request was sent from
     */
    public OriginFilter(String authUsernames, String authBrandnames, String hosts, String remoteAddresses) {
        super();
        this.authUsernames = Strings.isNotEmpty(authUsernames) ? new HashSet<>(Arrays.asList(Strings.splitByComma(authUsernames))) : Collections.emptySet();
        this.authBrandnames = Strings.isNotEmpty(authBrandnames) ? new HashSet<>(Arrays.asList(Strings.splitByComma(authBrandnames))) : Collections.emptySet();
        this.hostlist = Strings.isNotEmpty(hosts) ? HostList.valueOf(hosts) : HostList.of("");
        this.remoteAddresses = Strings.isNotEmpty(remoteAddresses) ? HostList.valueOf(remoteAddresses) : HostList.of("");
    }

    @Override
    public FilterReply decide(Marker marker, Logger logger, Level level, String format, Object[] params, Throwable t) {
        if (isNoApplicableLogger(logger) && isNoLoggingExtensions(marker)) {
            return FilterReply.NEUTRAL;
        }

        String originalHostname = LogProperties.get(Name.ORIGINAL_HOSTNAME);
        if (Strings.isNotEmpty(originalHostname) && hostlist.contains(originalHostname)) {
            return FilterReply.ACCEPT;
        }
        String serverName = LogProperties.get(Name.GRIZZLY_SERVER_NAME);
        if (Strings.isNotEmpty(serverName) && (hostlist.contains(serverName))) {
            return FilterReply.ACCEPT;
        }

        String originalRemoteAddressHostname = LogProperties.get(Name.GRIZZLY_ORIGINAL_REMOTE_ADDRESS);
        if (Strings.isNotEmpty(originalRemoteAddressHostname) && remoteAddresses.contains(originalRemoteAddressHostname)) {
            return FilterReply.ACCEPT;
        }
        String remoteAddressHostname = LogProperties.get(Name.GRIZZLY_REMOTE_ADDRESS);
        if (Strings.isNotEmpty(remoteAddressHostname) && remoteAddresses.contains(remoteAddressHostname)) {
            return FilterReply.ACCEPT;
        }

        String authUsername = LogProperties.get(Name.SOAP_AUTH_USERNAME);
        if (authUsernames.contains(authUsername)) {
            return FilterReply.ACCEPT;
        }

        String authBrandname = LogProperties.get(Name.SOAP_AUTH_BRANDNAME);
        if (authBrandnames.contains(authBrandname)) {
            return FilterReply.ACCEPT;
        }

        return FilterReply.DENY;
    }

}
