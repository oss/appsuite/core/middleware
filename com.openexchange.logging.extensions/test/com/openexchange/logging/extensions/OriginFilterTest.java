/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.logging.extensions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.stream.Stream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import com.openexchange.log.LogProperties;
import com.openexchange.log.LogProperties.Name;
import ch.qos.logback.core.spi.FilterReply;

/**
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class OriginFilterTest {

    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(OriginFilterTest.class);
    private static final org.slf4j.Logger LOGGER_WITH_WRONG_PACKAGE = org.slf4j.LoggerFactory.getLogger("com.openexchange.imap.IMAPMessageStorage");

    private MockedStatic<LogProperties> logPropertiesMock;

    private static Stream<Arguments> provideTestCases() {
        return Stream.of(Arguments.of(null, null, null, null),
                         Arguments.of(null, null, null, "66.249.66.3,66.249.66.4,66.249.66.222"),
                         Arguments.of(null, null, "localhost", null),
                         Arguments.of("mySuperDuperAuthUser", null, null, null),
                         Arguments.of("", "", "", ""));
    }

    @BeforeEach
    public void setUp() {
        logPropertiesMock = Mockito.mockStatic(LogProperties.class);
    }

    @AfterEach
    public void tearDown() {
        logPropertiesMock.close();
    }

    @ParameterizedTest
    @MethodSource("provideTestCases")
    public void testConstructor(String authUsername, String authBrandname, String hosts, String remoteAddresses) {
        OriginFilter filter = new OriginFilter(authUsername, authBrandname, hosts, remoteAddresses);

        FilterReply decide = filter.decide(null, null, null, null, null, null);

        assertEquals(FilterReply.NEUTRAL, decide);
    }

    @ParameterizedTest
    @MethodSource("provideTestCases")
    public void testDecide_wrongLoggerPrefix_returnNeutral(String authUsername, String authBrandname, String hosts, String remoteAddresses) {
        OriginFilter filter = new OriginFilter(authUsername, authBrandname, hosts, remoteAddresses);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER_WITH_WRONG_PACKAGE, null, null, null, null);

        assertEquals(FilterReply.NEUTRAL, decide);
    }

    @ParameterizedTest
    @MethodSource("provideTestCases")
    public void testDecide_noMatch_returnDeny(String authUsername, String authBrandname, String hosts, String remoteAddresses) {
        OriginFilter filter = new OriginFilter(authUsername, authBrandname, hosts, remoteAddresses);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);

        assertEquals(FilterReply.DENY, decide);
    }

    @Test
    public void testDecide_authUsernameMatch_returnAccept() {
        logPropertiesMock.when(() -> LogProperties.get(Name.SOAP_AUTH_USERNAME)).thenReturn("secondAuth");

        String authUsernames = "antonAuthIsAuthenticated, secondAuth";
        OriginFilter filter = new OriginFilter(authUsernames, null, null, null);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);

        assertEquals(FilterReply.ACCEPT, decide);
    }

    @Test
    public void testDecide_authUsernameNoMatch_returnDeny() {
        logPropertiesMock.when(() -> LogProperties.get(Name.SOAP_AUTH_USERNAME)).thenReturn("secondAuthdafdsfa");

        String authUsernames = "antonAuthIsAuthenticated, secondAuth";
        OriginFilter filter = new OriginFilter(authUsernames, null, null, null);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);

        assertEquals(FilterReply.DENY, decide);
    }

    @Test
    public void testDecide_authBrandnameMatch_returnAccept() {
        logPropertiesMock.when(() -> LogProperties.get(Name.SOAP_AUTH_BRANDNAME)).thenReturn("myBrand");

        String authBrandnames = "myBrand, secondBrand";
        OriginFilter filter = new OriginFilter(null, authBrandnames, null, null);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);

        assertEquals(FilterReply.ACCEPT, decide);
    }

    @Test
    public void testDecide_authBrandnameNoMatch_returnDeny() {
        logPropertiesMock.when(() -> LogProperties.get(Name.SOAP_AUTH_BRANDNAME)).thenReturn("myNotConfiguredBrand");

        String authBrandnames = "myBrand,secondBrand";
        OriginFilter filter = new OriginFilter(null, authBrandnames, null, null);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);

        assertEquals(FilterReply.DENY, decide);
    }

    @Test
    public void testDecide_destinationHostMatch_returnAccept() {
        logPropertiesMock.when(() -> LogProperties.get(Name.ORIGINAL_HOSTNAME)).thenReturn("66.249.66.222");

        String destinationHosts = "66.249.66.222, mysuperdomain.com";
        OriginFilter filter = new OriginFilter(null, null, destinationHosts, null);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);
        assertEquals(FilterReply.ACCEPT, decide);

        logPropertiesMock.when(() -> LogProperties.get(Name.ORIGINAL_HOSTNAME)).thenReturn("mysuperdomain.com");
        decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);
        assertEquals(FilterReply.ACCEPT, decide);

        logPropertiesMock.when(() -> LogProperties.get(Name.ORIGINAL_HOSTNAME)).thenReturn("mysuperdomain.de");
        decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);
        assertEquals(FilterReply.DENY, decide);
    }

    @Test
    public void testDecide_destinationHostNoMatch_returnDeny() {
        logPropertiesMock.when(() -> LogProperties.get(Name.ORIGINAL_HOSTNAME)).thenReturn("63.333.33.222");

        String destinationHosts = "66.249.66.222, mysuperdomain.com";
        OriginFilter filter = new OriginFilter(null, null, destinationHosts, null);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);

        assertEquals(FilterReply.DENY, decide);
    }

    @Test
    public void testDecide_destinationHostGrizzlyServerNameMatch_returnAccept() {
        logPropertiesMock.when(() -> LogProperties.get(Name.GRIZZLY_SERVER_NAME)).thenReturn("66.249.66.222");

        String destinationHosts = "66.249.66.222, mysuperdomain.com";
        OriginFilter filter = new OriginFilter(null, null, destinationHosts, null);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);
        assertEquals(FilterReply.ACCEPT, decide);

        logPropertiesMock.when(() -> LogProperties.get(Name.GRIZZLY_SERVER_NAME)).thenReturn("mysuperdomain.com");
        decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);
        assertEquals(FilterReply.ACCEPT, decide);

        logPropertiesMock.when(() -> LogProperties.get(Name.GRIZZLY_SERVER_NAME)).thenReturn("mysuperdomain.de");
        decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);
        assertEquals(FilterReply.DENY, decide);
    }

    @Test
    public void testDecide_destinationHostGrizzlyServerNameNoMatch_returnDeny() {
        logPropertiesMock.when(() -> LogProperties.get(Name.GRIZZLY_SERVER_NAME)).thenReturn("63.333.33.222");

        String destinationHosts = "66.249.66.222, mysuperdomain.com";
        OriginFilter filter = new OriginFilter(null, null, destinationHosts, null);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);

        assertEquals(FilterReply.DENY, decide);
    }

    @Test
    public void testDecide_clientIPMatchForOriginalRemoteAddress_returnAccept() {
        logPropertiesMock.when(() -> LogProperties.get(Name.GRIZZLY_ORIGINAL_REMOTE_ADDRESS)).thenReturn("44.249.66.220");

        String clientIPs = "66.249.66.222, 33.249.66.222, 44.249.66.200-44.249.66.222 ";
        OriginFilter filter = new OriginFilter(null, null, null, clientIPs);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);
        assertEquals(FilterReply.ACCEPT, decide);

        logPropertiesMock.when(() -> LogProperties.get(Name.GRIZZLY_ORIGINAL_REMOTE_ADDRESS)).thenReturn("33.249.66.222");
        decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);
        assertEquals(FilterReply.ACCEPT, decide);

        logPropertiesMock.when(() -> LogProperties.get(Name.GRIZZLY_ORIGINAL_REMOTE_ADDRESS)).thenReturn("33.249.66.223");
        decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);
        assertEquals(FilterReply.DENY, decide);
    }

    @Test
    public void testDecide_clientIPNoMatch_returnDeny() {
        logPropertiesMock.when(() -> LogProperties.get(Name.GRIZZLY_ORIGINAL_REMOTE_ADDRESS)).thenReturn("63.333.33.244");

        String clientIPs = "66.249.66.222, 33.249.66.222, 44.249.66.200-44.249.66.222 ";
        OriginFilter filter = new OriginFilter(null, null, null, clientIPs);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);

        assertEquals(FilterReply.DENY, decide);
    }

    @Test
    public void testDecide_badMDCEntries_returnDeny() {
        logPropertiesMock.when(() -> LogProperties.get(Name.GRIZZLY_ORIGINAL_REMOTE_ADDRESS)).thenReturn("63a.333b.33.244");
        logPropertiesMock.when(() -> LogProperties.get(Name.ORIGINAL_HOSTNAME)).thenReturn("!§$%&/()=");

        String clientIPs = "66.249.66.222, 33.249.66.222, 44.249.66.200-44.249.66.222 ";
        String destinationHosts = "66.249.66.222, mysuperdomain.com";
        OriginFilter filter = new OriginFilter(null, null, destinationHosts, clientIPs);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);

        assertEquals(FilterReply.DENY, decide);
    }

    @Test
    public void testDecide_clientIPMatchForRemoteAddress_returnAccept() {
        logPropertiesMock.when(() -> LogProperties.get(Name.GRIZZLY_REMOTE_ADDRESS)).thenReturn("44.249.66.220");

        String clientIPs = "66.249.66.222,33.249.66.222,44.249.66.200-44.249.66.222 ";
        OriginFilter filter = new OriginFilter(null, null, null, clientIPs);

        FilterReply decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);
        assertEquals(FilterReply.ACCEPT, decide);

        logPropertiesMock.when(() -> LogProperties.get(Name.GRIZZLY_REMOTE_ADDRESS)).thenReturn("33.249.66.222");
        decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);
        assertEquals(FilterReply.ACCEPT, decide);

        logPropertiesMock.when(() -> LogProperties.get(Name.GRIZZLY_REMOTE_ADDRESS)).thenReturn("33.249.66.223");
        decide = filter.decide(null, (ch.qos.logback.classic.Logger) LOGGER, null, null, null, null);
        assertEquals(FilterReply.DENY, decide);
    }
}
