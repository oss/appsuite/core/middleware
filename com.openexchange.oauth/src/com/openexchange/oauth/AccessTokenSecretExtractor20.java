/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.oauth;

import static com.openexchange.java.Autoboxing.I;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.github.scribejava.core.exceptions.OAuthException;
import com.github.scribejava.core.extractors.TokenExtractor;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.utils.OAuthEncoder;
import com.github.scribejava.core.utils.Preconditions;

/**
 * {@link AccessTokenSecretExtractor20}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @since v7.10.5
 * @deprecated Use {@link com.github.scribejava.core.extractors.OAuth2AccessTokenJsonExtractor} instead
 */
@Deprecated
public class AccessTokenSecretExtractor20 implements TokenExtractor<OAuth2AccessToken> {

    /**
     * The <code>"access_token": &lt;token&gt;</code> pattern
     */
    private static final Pattern PATTERN_ACCESS_TOKEN = Pattern.compile("\"access_token\" *: *\"([^&\"]+)\"");

    /**
     * The <code>"refresh_token": &lt;token&gt;</code> pattern
     */
    private static final Pattern PATTERN_REFRESH_TOKEN = Pattern.compile("\"refresh_token\" *: *\"([^&\"]+)\"");

    /**
     * The <code>"expires_in": &lt;number&gt;</code> pattern
     */
    private static final Pattern PATTERN_EXPIRES = Pattern.compile("\"expires_in\" *: *([0-9]+)");

    /**
     * The <code>"token_type": &lt;string&gt;</code> pattern
     */
    private static final Pattern PATTERN_TOKEN_TYPE = Pattern.compile("\"token_type\" *: *\"([^&\"]+)\"");
    
    /**
     * The <code>"scope": &lt;string&gt;</code> pattern
     */
    private static final Pattern PATTERN_SCOPE = Pattern.compile("\"scope\" *: *\"([^&\"]+)\"");

    /**
     * Initializes a new {@link AccessTokenSecretExtractor20}.
     */
    public AccessTokenSecretExtractor20() {
        super();
    }

    @Override
    public OAuth2AccessToken extract(Response response) throws IOException {
        Preconditions.checkEmptyString(response.getBody(), "Response body is incorrect. Can't extract a token from an empty string");
        String body = response.getBody();
        Matcher matcher = PATTERN_ACCESS_TOKEN.matcher(body);
        if (false == matcher.find()) {
            throw new OAuthException("Response body is incorrect. Can't extract a token from this: '" + body + "'", null);
        }
        String token = OAuthEncoder.decode(matcher.group(1));
        String refreshToken = "";
        Matcher refreshMatcher = PATTERN_REFRESH_TOKEN.matcher(body);
        if (refreshMatcher.find()) {
            refreshToken = OAuthEncoder.decode(refreshMatcher.group(1));
        }
        int expiry = 0;
        Matcher expiryMatcher = PATTERN_EXPIRES.matcher(body);
        if (expiryMatcher.find()) {
            try {
                expiry = Integer.parseInt(OAuthEncoder.decode(expiryMatcher.group(1)));
            } catch (NumberFormatException e) {
                // Ignore
            }
        }
        String tokenType = null;
        Matcher tokenTypeMatcher = PATTERN_TOKEN_TYPE.matcher(body);
        if (tokenTypeMatcher.find()) {
            tokenType = OAuthEncoder.decode(tokenTypeMatcher.group(1));
        }

        String scope = null;
        Matcher scopeMatcher = PATTERN_SCOPE.matcher(body);
        if (scopeMatcher.find()) {
            scope = OAuthEncoder.decode(scopeMatcher.group(1));
        }
        return new OAuth2AccessToken(token, tokenType, I(expiry), refreshToken, scope, body);
    }

}
