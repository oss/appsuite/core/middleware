/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.oauth;

import static com.openexchange.java.Autoboxing.i;
import java.util.HashMap;
import java.util.Map;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.Token;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;

/**
 * {@link AbstractReauthorizeClusterTask}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public abstract class AbstractReauthorizeClusterTask {

    private final String taskName;
    private final Session session;
    private final OAuthAccount cachedAccount;
    private final ServiceLookup services;
    private OAuthAccount dbAccount;
    private OAuthService oauthService;

    /**
     * Initialises a new {@link AbstractReauthorizeClusterTask} and generates the name of
     * the task.
     *
     * <p>The task name consists out of the following parts:</p>
     *
     * <pre>[USER_ID]@[CONTEXT_ID]:[ACCOUNT_ID]:[SERVICE_ID]</pre>
     * e.g.
     * <pre>SomeProviderReauthorizeClusterTask:1138@31145:34:tld.domain.provider.oauth</pre>
     *
     * @param services The {@link ServiceLookup} instance
     * @param session The groupware {@link Session}
     * @param cachedAccount The cached {@link OAuthAccount}
     */
    protected AbstractReauthorizeClusterTask(ServiceLookup services, Session session, OAuthAccount cachedAccount) {
        super();
        this.services = services;
        this.session = session;
        this.cachedAccount = cachedAccount;

        StringBuilder builder = new StringBuilder();
        builder.append(session.getUserId()).append('@');
        builder.append(session.getContextId());
        builder.append(':').append(cachedAccount.getId());
        builder.append(':').append(cachedAccount.getAPI().getDisplayName());

        taskName = builder.toString();
    }

    /**
     * Returns the context identifier
     *
     * @return the context identifier
     */
    public int getContextId() {
        return session.getContextId();
    }

    /**
     * Returns the user identifier
     *
     * @return the user identifier
     */
    public int getUserId() {
        return session.getUserId();
    }

    /**
     * Gets the task name.
     *
     * @return The task name
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Gets the session
     *
     * @return The session
     */
    public Session getSession() {
        return session;
    }

    /**
     * Gets the cachedAccount
     *
     * @return The cachedAccount
     */
    public OAuthAccount getCachedAccount() {
        return cachedAccount;
    }

    /**
     * Returns the database {@link OAuthAccount}
     *
     * @return The database {@link OAuthAccount}
     * @throws OXException if the {@link OAuthAccount} cannot be retrieved
     */
    public OAuthAccount getDBAccount() throws OXException {
        if (dbAccount == null) {
            dbAccount = getOAuthService().getAccount(session, cachedAccount.getId());
        }
        return dbAccount;
    }

    /**
     * Returns the {@link OAuthService}
     *
     * @return the {@link OAuthService}
     */
    public OAuthService getOAuthService() {
        if (oauthService == null) {
            oauthService = services.getService(OAuthService.class);
        }
        return oauthService;
    }

    /**
     * Common logic for the perform action
     *
     * @return The re-authorised OAuthAccount
     * @throws OXException if an error is occurred
     */
    public OAuthAccount perform() throws OXException {
        dbAccount = getDBAccount();

        // Cached account does not match the database account. DB account is always considered to be up-to-date, thus return it
        if (false == dbAccount.getToken().equals(cachedAccount.getToken()) || false == dbAccount.getSecret().equals(cachedAccount.getSecret())) {
            return dbAccount;
        }

        // Perform the actual re-authorise
        ScribeToken token = reauthorize();

        // Did the OAuth provider returned a new refresh token?
        String refreshToken = (Strings.isEmpty(token.getSecret())) ? dbAccount.getSecret() : token.getSecret();

        // Set the arguments for the update
        int accountId = dbAccount.getId();
        Map<String, Object> arguments = HashMap.newHashMap(2);
        arguments.put(OAuthConstants.ARGUMENT_REQUEST_TOKEN, new DefaultOAuthToken(token.getAccessToken(), refreshToken, token.getExpiry()));
        arguments.put(OAuthConstants.ARGUMENT_SESSION, session);

        // Update the account
        OAuthService oAuthService = getOAuthService();
        oAuthService.updateAccount(session, accountId, arguments);

        // Reload
        return oAuthService.getAccount(session, accountId);
    }

    /**
     * Performs the actual re-authorise task
     *
     * @return The re-authorised OAuthAccount
     * @throws OXException if an error is occurred
     */
    protected abstract ScribeToken reauthorize() throws OXException;

    public static class ScribeToken {
        private String accessToken;
        private String secret;
        private long expiry = 0;

        /**
         * Initialises a new {@link ScribeToken}
         *
         * @param token The token
         */
        public ScribeToken(Token token) {
            if (token instanceof OAuth1AccessToken t) {
                this.accessToken = t.getToken();
                this.secret = t.getTokenSecret();
            } else if (token instanceof OAuth2AccessToken t) {
                this.accessToken = t.getAccessToken();
                this.secret = t.getRefreshToken();
                this.expiry = System.currentTimeMillis() + (null == t.getExpiresIn() ? 1 : i(t.getExpiresIn()) * 1000);
            }
        }

        /**
         * Returns the access token
         *
         * @return the access token
         */
        public String getAccessToken() {
            return accessToken;
        }

        /**
         * Returns the secret/refresh token
         *
         * @return The secret/refresh token
         */
        public String getSecret() {
            return secret;
        }

        /**
         * Returns the expiration timestamp
         *
         * @return the expiration timestamp
         */
        public long getExpiry() {
            return expiry;
        }
    }
}
