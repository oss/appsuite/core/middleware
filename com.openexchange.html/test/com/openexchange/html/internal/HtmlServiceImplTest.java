/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.html.internal;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.openexchange.html.HtmlSanitizeResult;
import com.openexchange.html.internal.HtmlServiceImpl.Html2TextStats;
import com.openexchange.html.osgi.HTMLServiceActivator;
import com.openexchange.java.Strings;


/**
 * {@link HtmlServiceImplTest}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 7.6.1
 */
public class HtmlServiceImplTest {

    private HtmlServiceImpl htmlServiceImpl;

    private final String htmlContent = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head>\n" +
        "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
        " \n" +
        " </head><body>\n" +
        " \n" +
        "  <p>Tolle html mail <br /></p>\n" +
        "  <h1>\n" +
        "   <ul>\n" +
        "    <li>hgfhghdgdhdg<br /></li>\n" +
        "    <li>gfhdgd<br /></li>\n" +
        "    <li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. <br /><br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. <br /><br />Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. <br /><br />Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. <br /><br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. <br /><br />At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. <br /><br />Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus. <br /><br />Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. <br /><br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. <br /><br /><span style=\"color: rgb(255, 0, 0);\">Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </span><br /><br /><span style=\"color: rgb(255, 0, 0);\">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo</span></li>\n" +
        "    <li><br /></li>\n" +
        "   </ul></h1>\n" +
        " \n" +
        "</body></html>";

    private final String htmlDownlevelRevealed ="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\"http://www.w3.org/TR/html4/loose.dtd\"> <html> <head><title>Sky</title></head><div><![if !IE]><p>You should see this</p><![endif]><!--[if IE 6]><p>Not this</p><![endif]--></div></body></html>";

    private final String bigHtmlContent = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head>\n" + "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" + " \n" + " </head><body>\n" + " \n" + "  <ol>\n" + "   <li>fadskjfdaksjf&#246;ldksajfdkl&#246;jadksf<br /></li>\n" + "   <li>dkfj&#246;adsadjksf&#246;ladjksfl&#246;kdjsfl&#246;kadjs<br /></li>\n" + "   <li>das&#246;fjkds&#246;lkfjdsl&#246;kfjdsadls&#246;<br /></li>\n" + "   <li>\n" + "    <ul>\n" + "     <li>gfhdgd<br /></li>\n" + "     <li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. <br /><br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. <br /><br />Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te</li>\n" + "    </ul></li>\n" + "   <li>\n" + "    <ul>\n" + "     <li>gfhdgd<br /></li>\n" + "     <li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. <br /><br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. <br /><br />Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te</li>\n" + "     <li>\n" + "      <ul>\n" + "       <li>gfhdgd<br /></li>\n" + "       <li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. <br /><br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. <br /><br />Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te</li>\n" + "      </ul></li>\n" + "     <li>\n" + "      <ul>\n" + "       <li>gfhdgd<br /></li>\n" + "       <li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. <br /><br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. <br /><br />Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te</li>\n" + "      </ul></li>\n" + "    </ul></li>\n" + "   <li>\n" + "    <ul>\n" + "     <li>gfhdgd<br /></li>\n" + "     <li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. <br /><br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. <br /><br />Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te</li>\n" + "    </ul></li>\n" + "   <li>\n" + "    <ul>\n" + "     <li>gfhdgd<br /></li>\n" + "     <li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. <br /><br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. <br /><br />Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te</li>\n" + "    </ul></li>\n" + "  </ol>\n" + "  <blockquote type=\"cite\">\n" + "   ---------- Urspr&#252;ngliche Nachricht ----------\n" + "   <br />Von: Martin Schneider &#60;martin.schneider@premium&#62;\n" + "   <br />An: Martin Schneider &#60;martin.schneider@premium&#62;\n" + "   <br />Datum: 24. Juni 2014 um 14:47\n" + "   <br />Betreff: HTML-Mail\n" + "   <br />\n" + "   <br />\n" + "   <p>Tolle html mail <br /></p>\n" + "   <ul>\n" + "    <li>hgfhghdgdhdg<br /></li>\n" + "    <li>gfhdgd<br /></li>\n" + "    <li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. <br /><br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. <br /><br />Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. <br /><br />Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. <br /><br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. <br /><br />At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. <br /><br />Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus. <br /><br />Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. <br /><br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. <br /><br /><span style=\"color: rgb(255, 0, 0);\">Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </span><br /><br /><span style=\"color: rgb(255, 0, 0);\">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo</span></li>\n" + "    <li><br /></li>\n" + "   </ul>\n" + "  </blockquote>\n" + "  <p><br />&#160;</p>\n" + " \n" + "</body></html>";

    private final String cssPrefix = "ox-78f9d2a3d7";

    private final String bigPlainText = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   \n" +
        "\n" +
        "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.   \n" +
        "\n" +
        "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.   \n" +
        "\n" +
        "Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   \n" +
        "\n" +
        "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.   \n" +
        "\n" +
        "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.   \n" +
        "\n" +
        "Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus.   \n" +
        "\n" +
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   \n" +
        "\n" +
        "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.   \n" +
        "\n" +
        "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.   \n" +
        "\n" +
        "Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   \n" +
        "\n" +
        "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.   \n" +
        "\n" +
        "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.   \n" +
        "\n" +
        "Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus.   \n" +
        "\n" +
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   \n" +
        "\n" +
        "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.   \n" +
        "\n" +
        "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.   \n" +
        "\n" +
        "Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   \n" +
        "\n" +
        "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.   \n" +
        "\n" +
        "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.   \n" +
        "\n" +
        "Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus.   \n" +
        "\n" +
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero";

    private final String plainText = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   \n" + "\n" + "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.   \n";

    @BeforeEach
    public void setUp() {
        htmlServiceImpl = new HtmlServiceImpl(new HashMap<Character, String>(), new HashMap<String, Character>());
    }

     @Test
     public void testSupportForDataScheme() throws Exception {
        String html = "Sincerely Peter Pan\n" +
            "\n" +
            "<a href=\"domain.invalid\"><img alt=\"alternative\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAABQC\"></a>";

        Object[] maps = HTMLServiceActivator.getDefaultHTMLEntityMaps();

        @SuppressWarnings("unchecked")
        final Map<String, Character> htmlEntityMap = (Map<String, Character>) maps[1];
        @SuppressWarnings("unchecked")
        final Map<Character, String> htmlCharMap = (Map<Character, String>) maps[0];

        htmlEntityMap.put("apos", Character.valueOf('\''));

        HtmlServiceImpl service = new HtmlServiceImpl(htmlCharMap, htmlEntityMap);

        HtmlSanitizeResult sanitize = service.sanitize(html, null, false, new boolean[0], cssPrefix, 100);
        String content = sanitize.getContent();

        Assertions.assertTrue(content.indexOf("data:image/png;") > 0);
    }

     @Test
     public void testSanitize_contentEmtpy_returnEmptyContent() throws Exception {
        HtmlSanitizeResult sanitize = htmlServiceImpl.sanitize("", null, false, new boolean[0], cssPrefix, 100);
        Assertions.assertEquals("", sanitize.getContent());
    }

     @Test
     public void testSanitize_htmlContentWithSetMaxContentSize_returnSanitizedContent() throws Exception {
        HtmlSanitizeResult sanitize = htmlServiceImpl.sanitize(htmlContent, null, false, new boolean[0], cssPrefix, 100);
        Assertions.assertTrue(!sanitize.getContent().contains("<br />"));
    }

     @Test
     public void testSanitize_htmlContentWithSetMaxContentSize_notTruncatedDueToSmallerContent() throws Exception {
        HtmlSanitizeResult sanitize = htmlServiceImpl.sanitize(htmlContent, null, false, new boolean[0], cssPrefix, 11111);
        Assertions.assertFalse(sanitize.isTruncated());
    }

     @Test
     public void testSanitize_htmlContentWithSetMaxContentSize_truncatedDueToBigContent() throws Exception {
        HtmlSanitizeResult sanitize = htmlServiceImpl.sanitize(bigHtmlContent, null, false, new boolean[0], cssPrefix, 11111);
        Assertions.assertTrue(sanitize.isTruncated());
    }

     @Test
     public void testSanitize_htmlContentWithNoMaxContentSize_notTruncated() throws Exception {
        HtmlSanitizeResult sanitize = htmlServiceImpl.sanitize(bigHtmlContent, null, false, new boolean[0], cssPrefix, -1);
        Assertions.assertFalse(sanitize.isTruncated());
    }

     @Test
     public void testHtmlFormat_noMaxContentSize_notTruncated() {
        HtmlSanitizeResult htmlFormat = htmlServiceImpl.htmlFormat(bigPlainText, false, "", -1);
        Assertions.assertFalse(htmlFormat.isTruncated());
    }

     @Test
     public void testHtmlFormat_maxContentSizeToSmall_isTruncated() {
        HtmlSanitizeResult htmlFormat = htmlServiceImpl.htmlFormat(bigPlainText, false, "", 444);
        Assertions.assertTrue(htmlFormat.isTruncated());
    }

     @Test
     public void testHtmlFormat_maxContentSizeToSmall_truncateAt10000() {
        HtmlSanitizeResult htmlFormat = htmlServiceImpl.htmlFormat(bigPlainText, false, "", 444);
        Assertions.assertTrue(htmlFormat.getContent().length() < 10300);
        Assertions.assertTrue(htmlFormat.getContent().length() > 10000);
    }

     @Test
     public void testHtmlFormat_maxContentSizeSet_isTruncated() {
        HtmlSanitizeResult htmlFormat = htmlServiceImpl.htmlFormat(bigPlainText, false, "", 11111);
        Assertions.assertTrue(htmlFormat.isTruncated());
    }

     @Test
     public void testHtmlFormat_maxContentSizeSet_truncateAtGivenValue() {
        HtmlSanitizeResult htmlFormat = htmlServiceImpl.htmlFormat(bigPlainText, false, "", 11111);
        Assertions.assertTrue(htmlFormat.getContent().length() < 11300);
        Assertions.assertTrue(htmlFormat.getContent().length() > 11000);
    }

     @Test
     public void testHtmlFormat_maxContentSizeToSmall_isNotTruncated() {
        HtmlSanitizeResult htmlFormat = htmlServiceImpl.htmlFormat(plainText, false, "", 4444);
        Assertions.assertFalse(htmlFormat.isTruncated());
    }

     @Test
     public void testHtmlFormat_maxContentSizeSet_isNotTruncated() {
        HtmlSanitizeResult htmlFormat = htmlServiceImpl.htmlFormat(plainText, false, "", 11111);
        Assertions.assertFalse(htmlFormat.isTruncated());
    }

     @Test
     public void testDropSuperfluousDivTags() throws Exception {
        String html = "<div id=\"ox-7bf62dbb34\"><p>Some text</p></div>";
        String test = htmlServiceImpl.getConformHTML(html, "UTF-8");
        Assertions.assertTrue(test.indexOf("<div") < 0);
    }

     @Test
     public void testBug40943() throws Exception {
        String html = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "<head>\n" +
            "<meta charset=\"UTF-8\">\n" +
            "<title>Title of the document</title>\n" +
            "</head>\n" +
            "\n" +
            "<body>\n" +
            "<article>\n" +
            "  <header>\n" +
            "    <h1>Headline</h1>\n" +
            "    <p>Abstract</p>\n" +
            "  </header>\n" +
            "  <p>Content</p>\n" +
            "</article>\n" +
            "</body>\n" +
            "</html>";
        String test = htmlServiceImpl.getConformHTML(html, "UTF-8");
        Assertions.assertTrue(test.indexOf("<meta charset=\"UTF-8\">") > 0);
        Assertions.assertTrue(test.indexOf("<!doctype html>") >= 0);
    }

     @Test
     public void testBug46608() throws Exception {
        HtmlSanitizeResult test = htmlServiceImpl.sanitize(htmlDownlevelRevealed, null, false, null, "ox-36f8df7e2a", 102400);
        String content = unfoldAndTrim(test.getContent());
        Assertions.assertTrue(content.contains("<!--[if !IE]--><p>You should see this</p><!--[endif]--><!--[if IE 6]><p>Not this</p><![endif]-->"));
    }

    private String unfoldAndTrim(String str) {
        String[] lines = Strings.splitByCRLF(str);
        StringBuilder sb = new StringBuilder(str.length());
        for (String line : lines) {
            sb.append(line.trim());
        }
        return sb.toString();
    }

    @Test
    public void testHtml2TextTimeout() {
        String html = "<!doctype html>\n"
            + "<html>\n"
            + " <head> \n"
            + "  <meta charset=\"UTF-8\"> \n"
            + " </head>\n"
            + " <body>\n"
            + "  <div>\n"
            + "   Hi Jane,\n"
            + "  </div> \n"
            + "  <div class=\"default-style\">\n"
            + "   &nbsp;\n"
            + "  </div> \n"
            + "  <div class=\"default-style\">\n"
            + "   it was only an empty mail ..\n"
            + "  </div> \n"
            + "  <div class=\"default-style\">\n"
            + "   &nbsp;\n"
            + "  </div> \n"
            + "  <div class=\"default-style\">\n"
            + "   Could you send me your feedback again?\n"
            + "  </div> \n"
            + "  <div class=\"default-style\">\n"
            + "   &nbsp;\n"
            + "  </div> \n"
            + "  <div class=\"default-style\">\n"
            + "   Best\n"
            + "  </div> \n"
            + "  <div class=\"default-style\">\n"
            + "   Izabela\n"
            + "  </div> \n"
            + "  <blockquote type=\"cite\"> \n"
            + "   <div>\n"
            + "    Jane Doe &lt;j.doe@kmp.nl&gt; hat am 30.01.2024 14:11 CET geschrieben:\n"
            + "   </div> \n"
            + "   <div>\n"
            + "    &nbsp;\n"
            + "   </div> \n"
            + "   <div>\n"
            + "    &nbsp;\n"
            + "   </div> \n"
            + "   <div style=\"margin-top: 4294967294px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"font-family: Arial; color: #202124;\">Thank you for the request. the answers are next to the questions</span>\n"
            + "   </div> \n"
            + "   <br>\n"
            + "   <br>\n"
            + "   <br>\n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"font-family: Arial; color: #000000;\">Met vriendelijke groet,</span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"font-family: Arial; color: #000000;\">Jan Doe</span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"font-family: Arial; color: #162461;\"><em>Sales Co\u00F6rdinator</em></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <img src=\"cid:MzAxMjAyNF8xNDA3MDJfMC5wbmc=@degrootdruk.nl\" alt=\"3012024_140702_0.png\" width=\"153\" height=\"83\" align=\"middle\" border=\"0\" hspace=\"0\" vspace=\"0\">\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"font-family: Arial; color: #162461;\"><strong>Kontakt Somecompany</strong></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"font-family: Arial; color: #000000;\">Street 131 &nbsp;- &nbsp;1337 XE City</span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"font-family: Arial; color: #000000;\">99 99 99 99 99</span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"font-family: Arial; color: #000000;\"><a href=\"mailto:j.doe@kmp.nl\">j.doe@kmp.nl</a></span>\n"
            + "   </div> \n"
            + "   <br>\n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"font-family: Arial; color: #000000;\">Disclaimer: </span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"font-family: Arial; color: #000000;\">Dit bericht (met bijlagen) is met grote zorgvuldigheid samengesteld. Voor mogelijke onjuistheid en/of onvolledigheid van de hierin verstrekte informatie kan Kontakt Somecompany geen juridische aansprakelijkheid aanvaarden, evenmin kunnen aan de inhoud van dit bericht (met bijlagen) rechten worden ontleend. De inhoud van dit bericht (met bijlagen) kan vertrouwelijke informatie bevatten en is uitsluitend bestemd voor de geadresseerde van dit bericht. Indien u niet de beoogde ontvanger van dit bericht bent, verzoekt Kontakt Mediapartners u dit bericht terug te zenden en daarna te verwijderen. De emailserver van Kontakt Mediapartners kan niet meer dan 10mb ontvangen. Kontakt Mediapartners is statutair gevestigd in Goudriaan en ingeschreven in de KvK onder nr. 23070015.</span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"font-family: Arial; color: #000000;\"><strong>Bella Bums &lt;<a href=\"mailto:im@impulsq.de\">bb@impulsq.de</a>&gt; writes:</strong></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\"> Hi, </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp; </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">It will soon be the end of the month and our client would like to book in January, so your feedback would be very important to me. Can you please tell me something about the current conditions? </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp; </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Best regards </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Bella </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Bella Bums &lt;<a href=\"mailto:bb@impulsq.de\">im@impulsq.de</a>&gt; hat am 24.01.2024 09:54 CET geschrieben: </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp; </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp; </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Hi, </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp; </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Verdana; color: #000000;\">we would like to publish an article on goudsepost.nl. </span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Verdana; color: #000000;\">Can you quickly confirm this booking for me and tell me if the conditions are still the same: </span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Verdana; color: #000000;\">Contentplan: </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">BookingID: 999999 </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Where: city.nl </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Topic: suitable </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Link: <a href=\"https://www.techopedia.com/nl/casino/casinos-zonder-cruks\" target=\"_blank\" rel=\"noopener\">https://www.techopedia.com/nl/casino/casinos-zonder-cruks</a>&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Anchor: niet zijn aangesloten </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Words: 1000 (We write in best quality) </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Budget: 200\u20AC </span></span><span style=\"font-family: Arial; color: #000000;\">&nbsp;you must pay before we can place your article, so please sent me your compagny info</span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Verdana; color: #000000;\">&nbsp; </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Article Conditions: </span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">- Dofollow </span></span><span style=\"font-family: Arial; color: #000000;\">&nbsp;no problem</span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">- Lifetime online </span></span><span style=\"font-family: Arial; color: #000000;\">&nbsp;ok</span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">- Not labeled </span></span><span style=\"font-family: Arial; color: #000000;\">&nbsp;&nbsp;&nbsp;\"ingezonden mededeling\" label</span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">- Gambling fine </span></span><span style=\"font-family: Arial; color: #000000;\">ok</span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Can I send you our article? </span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">It would be really important if I could get a quick response. </span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">I hope you understand, but the client is putting on some pressure :D </span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">I look forward to your answer and thank you very much. </span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Kind regards </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">Bella </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp; </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #b200ff;\"><strong>Bella Bums</strong></span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\">Projekt Management</span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\"><strong>Important:</strong> Please always include our booking id in your invoice, thanks a lot! </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\"><strong>How</strong> to send us an <strong>invoice</strong>: </span></span><a href=\"https://docs.google.com/document/d/nonexistent/\"><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman;\"><strong>Link to Google Doc</strong></span></span></a>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">P.S. Sollte die Email nicht durchgehen, bitte kontaktieren Sie mich auf Skype: (live:bella_511) Vielen Dank!</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">P.S. If an email could not be sent, please contact me on Skype: (live:bella_511) Thank you!!</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\"><strong>YEAH - Tragen Sie sich in unseren Linkmarktplatz ein und erhalten Sie viele Buchungen: </strong></span></span><a href=\"https://www.ranksider.com/register/w\"><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman;\"><strong>Hier geht es zu Ranksider &gt;</strong></span></span></a>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\"><strong>E-Mail: </strong></span></span><a href=\"mailto:bb@impulsq.de\"><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman;\">im@impulsQ.de</span></span></a><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\"><strong>Internet: </strong></span></span><a href=\"https://www.bellabums.de/\"><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman;\">www.impulsQ.de</span></span></a><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <a target=\"FCIMGWIN\">DigitalPR mit impulsQ</a><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;Bella Bums I Streetname 4 I 99999 Somecity</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">234567432145678543245675432</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">5467854325678654321456786543256785432</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #b200ff;\"><strong>Unsere digitalen Schwester-Agenturen</strong></span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <a target=\"FCIMGWIN\">AFS</a> <a target=\"FCIMGWIN\">Wortrakete</a>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp; </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #b200ff;\"><strong>Bella Bums</strong></span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\">Projekt Management</span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\"><strong>Important:</strong> Please always include our booking id in your invoice, thanks a lot! </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\"><strong>How</strong> to send us an <strong>invoice</strong>: </span></span><a href=\"https://docs.google.com/document/d/nonexistent<span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman;\"><strong>Link to Google Doc</strong></span></span></a>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">P.S. Sollte die Email nicht durchgehen, bitte kontaktieren Sie mich auf Skype: (live:bella3342243) Vielen Dank!</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">P.S. If an email could not be sent, please contact me on Skype: (live:bella324235) Thank you!!</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\"><strong>YEAH - Tragen Sie sich in unseren Linkmarktplatz ein und erhalten Sie viele Buchungen: </strong></span></span><a href=\"https://www.ranksider.com/register/w\"><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman;\"><strong>Hier geht es zu Ranksider &gt;</strong></span></span></a>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\"><strong>E-Mail: </strong></span></span><a href=\"mailto:im@impulsq.de\"><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman;\">bb@comapny.de</span></span></a><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\"><strong>Internet: </strong></span></span><a href=\"https://www.impulsq.de/\"><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman;\">www.company.de</span></span></a><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <a target=\"FCIMGWIN\">DigitalPR mit impulsQ</a><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;COmapny GmbH Strretname 4 I 99999 Somecity</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\"32456743256735678432</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">45674325678543256748543</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"padding-left: 45px; text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #b200ff;\"><strong>Unsere digitalen Schwester-Agenturen</strong></span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <a target=\"FCIMGWIN\">AFS</a> <a target=\"FCIMGWIN\">Wortrakete</a>\n"
            + "   </div> \n"
            + "   <br>\n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp; </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #b200ff;\"><strong>Bella Bums</strong></span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\">Projekt Management</span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\"><strong>Important:</strong> Please always include our booking id in your invoice, thanks a lot! </span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\"><strong>How</strong> to send us an <strong>invoice</strong>: </span></span><a href=\"https://docs.google.com/document/d/nonexistent/\"><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman;\"><strong>Link to Google Doc</strong></span></span></a>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">P.S. Sollte die Email nicht durchgehen, bitte kontaktieren Sie mich auf Skype: (live:retetrztrzrtz) Vielen Dank!</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">P.S. If an email could not be sent, please contact me on Skype: (live:itrwzertzrtez) Thank you!!</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\"><strong>YEAH - Tragen Sie sich in unseren Linkmarktplatz ein und erhalten Sie viele Buchungen: </strong></span></span><a href=\"https://www.ranksider.com/register/w\"><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman;\"><strong>Hier geht es zu Ranksider &gt;</strong></span></span></a>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\"><strong>E-Mail: </strong></span></span><a href=\"mailto:im@impulsq.de\"><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman;\">im@impulsQ.de</span></span></a><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #3c3d3e;\"><strong>Internet: </strong></span></span><a href=\"https://www.impulsq.de/\"><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman;\">www.impulsQ.de</span></span></a><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <a target=\"FCIMGWIN\">DigitalPR mit impulsQ</a><span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;</span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">&nbsp;tzuritztrewtzuiotrewrrztuioozliturze</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">treuzirtztrewtzuioootrewrzuii</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #000000;\">etrzejrutilzujzdtsdrewrtzuilzktrewqrtzuii</span></span>\n"
            + "   </div> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <span style=\"background-color: #d0d0d0;\"><span style=\"font-family: Times New Roman; color: #b200ff;\"><strong>Unsere digitalen Schwester-Agenturen</strong></span></span>\n"
            + "   </div> \n"
            + "   <br> \n"
            + "   <div style=\"text-align: left;\" align=\"left\">\n"
            + "    <a target=\"FCIMGWIN\">AFS</a> <a target=\"FCIMGWIN\">Wortrakete</a>\n"
            + "   </div> \n"
            + "   <br>\n"
            + "   <br>\n"
            + "  </blockquote> \n"
            + "  <div class=\"default-style\">\n"
            + "   &nbsp;\n"
            + "  </div> \n"
            + "  <div class=\"io-ox-signature\"> \n"
            + "   <p class=\"default-style\"><span style=\"color: #b200ff;\"><strong>Bella Bums</strong></span><br><span style=\"color: #3c3d3e;\">Projekt Management</span><br><br><span style=\"color: #3c3d3e;\"><strong>Important:</strong> Please always include our booking id in your invoice, thanks a lot! <br><strong>How</strong> to send us an <strong>invoice</strong>: <strong><a href=\"https://docs.google.com/document/d/nonexistent/\">Link to Google Doc</a></strong></span></p> \n"
            + "   <p class=\"default-style\">&nbsp;</p> \n"
            + "   <p class=\"default-style\">P.S. Sollte die Email nicht durchgehen, bitte kontaktieren Sie mich auf Skype: (live:trzutrwezuirre) Vielen Dank!</p> \n"
            + "   <p class=\"default-style\">P.S. If an email could not be sent, please contact me on Skype: (live:iertzurezue) Thank you!!</p> \n"
            + "   <p class=\"default-style\"><span style=\"color: #3c3d3e;\"><strong>YEAH - Tragen Sie sich in unseren Linkmarktplatz ein und erhalten Sie viele Buchungen: <a href=\"https://www.ranksider.com/register/w\">Hier geht es zu Ranksider &gt;</a></strong></span></p> \n"
            + "   <div class=\"default-style\">\n"
            + "    <strong>E-Mail: </strong><a href=\"mailto:bb@ertertert.de\">rwetewrtztrztrztzretertzzter</a> \n"
            + "    <br><span style=\"color: #3c3d3e;\"><strong>Internet: </strong><a href=\"https://www.ezrtterwzwtreztrz.de/\">www.impulsQ.de</a></span> \n"
            + "    <br>\n"
            + "    <br><a href=\"https://etrzertztez.de/\"><img class=\"\" style=\"width: 139px; height: 35px;\" src=\"https://impulsq.de/mega/wp-content/uploads/2023/10/impulsQ_Logo_positiv_139PX.jpg\" alt=\"DigitalPR mit impulsQ\" width=\"139\" height=\"35\"></a> \n"
            + "    <br>\n"
            + "    <br style=\"font-size: 11px;\"><span style=\"font-size: 11px;\"><span style=\"font-size: 11px;\"> erzterzt erztrez I eretzjtrastertrzt<br>restzjrtasezwejr<br>Gesch\u00E4ftsf\u00FChrer: esratzeuurasezwujrr</span></span> \n"
            + "    <p class=\"default-style\"><span style=\"color: #b200ff;\"><strong>Unsere digitalen Schwester-Agenturen</strong></span><br><br><a href=\"https://www.afs-akademie.org\"><img class=\"\" style=\"width: 229px; height: 30px;\" src=\"https://impulsq.de/mega/wp-content/uploads/2023/10/AFS_Wortbildmarke-fuer-die-Signatur.jpg\" alt=\"AFS\" width=\"229\" height=\"30\"></a> <a href=\"https://www.wortrakete.de/\"><img class=\"\" style=\"width: 133px; height: 40px;\" src=\"https://impulsq.de/mega/wp-content/uploads/2022/01/Wortrakete_Logo_80px-Schwarz.jpg\" alt=\"Wortrakete\" width=\"133\" height=\"40\"></a></p> \n"
            + "   </div> \n"
            + "  </div>\n"
            + " </body>\n"
            + "</html>";

        Html2TextStats stats = new Html2TextStats();
        int timeout = 100;
        String text = htmlServiceImpl.html2text(html, true, true, timeout, stats);
        Assertions.assertTrue(stats.isTimeoutOccurred());
        if (stats.isConvertedWithJSoup()) {
            Assertions.assertNotNull(text);
            Assertions.assertTrue(text.length() > 0);
        }
    }
}
