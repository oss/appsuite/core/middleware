/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.html.internal.jericho.handler;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.openexchange.html.internal.HtmlServiceImpl;
import com.openexchange.html.internal.jericho.handler.FilterJerichoHandler.CellPadding;
import com.openexchange.test.mock.MockUtils;

/**
 * {@link FilterJerichoHandlerTest}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 7.6.1
 */
public class FilterJerichoHandlerTest {

    @Mock
    private HtmlServiceImpl htmlServiceImpl;

    private FilterJerichoHandler filterJerichoHandler;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        filterJerichoHandler = new FilterJerichoHandler(100000, htmlServiceImpl);
    }

    @Test
    public void testSetMaxContentSize_infiniteWithMinusOne_setInfinite() {
        filterJerichoHandler.setMaxContentSize(-1);

        int valueFromField = ((Integer) MockUtils.getValueFromField(filterJerichoHandler, "maxContentSize")).intValue();
        Assertions.assertEquals(-1, valueFromField);
    }

    @Test
    public void testSetMaxContentSize_infiniteWithMinusZero_setInfinite() {
        filterJerichoHandler.setMaxContentSize(0);

        int valueFromField = ((Integer) MockUtils.getValueFromField(filterJerichoHandler, "maxContentSize")).intValue();
        Assertions.assertEquals(0, valueFromField);
    }

    @Test
    public void testSetMaxContentSize_lessThanMinimum_setMinimum10000() {
        filterJerichoHandler.setMaxContentSize(555);

        int valueFromField = ((Integer) MockUtils.getValueFromField(filterJerichoHandler, "maxContentSize")).intValue();
        Assertions.assertEquals(10000, valueFromField);
    }

    @Test
    public void testSetMaxContentSize_validMaxSize_setMaxSize() {
        filterJerichoHandler.setMaxContentSize(22222);

        int valueFromField = ((Integer) MockUtils.getValueFromField(filterJerichoHandler, "maxContentSize")).intValue();
        Assertions.assertEquals(22222, valueFromField);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHandleTableCellpaddingAttribute_mapNull_return() {
        filterJerichoHandler.handleTableCellpaddingAttribute(null);

        LinkedList<CellPadding> tablePaddings = (LinkedList<CellPadding>) MockUtils.getValueFromField(filterJerichoHandler, "tablePaddings");
        Assertions.assertEquals(0, tablePaddings.size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHandleTableCellpaddingAttribute_emptyMap_addEmptyCellpadding() {
        Map<String, String> map = new LinkedHashMap<String, String>();

        filterJerichoHandler.handleTableCellpaddingAttribute(map);

        LinkedList<CellPadding> tablePaddings = (LinkedList<CellPadding>) MockUtils.getValueFromField(filterJerichoHandler, "tablePaddings");
        Assertions.assertEquals(1, tablePaddings.size());
        Assertions.assertEquals(null, tablePaddings.getFirst().cellPadding);
        Assertions.assertEquals(0, map.size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHandleTableCellpaddingAttribute_cellpaddingInAttributes_addCellpadding() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("cellpadding", "10");

        filterJerichoHandler.handleTableCellpaddingAttribute(map);

        LinkedList<CellPadding> tablePaddings = (LinkedList<CellPadding>) MockUtils.getValueFromField(filterJerichoHandler, "tablePaddings");
        Assertions.assertEquals(1, tablePaddings.size());
        Assertions.assertEquals("10", tablePaddings.getFirst().cellPadding);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHandleTableCellpaddingAttribute_cellpaddingZero_addCellpadding() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("cellpadding", "0");

        filterJerichoHandler.handleTableCellpaddingAttribute(map);

        LinkedList<CellPadding> tablePaddings = (LinkedList<CellPadding>) MockUtils.getValueFromField(filterJerichoHandler, "tablePaddings");
        Assertions.assertEquals(1, tablePaddings.size());
        Assertions.assertEquals("0", tablePaddings.getFirst().cellPadding);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHandleTableCellpaddingAttribute_cellsapcingZero_addCellpaddingAndBorderCollapse() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("cellpadding", "0");
        map.put("cellspacing", "0");

        filterJerichoHandler.handleTableCellpaddingAttribute(map);

        LinkedList<CellPadding> tablePaddings = (LinkedList<CellPadding>) MockUtils.getValueFromField(filterJerichoHandler, "tablePaddings");
        Assertions.assertEquals(1, tablePaddings.size());
        Assertions.assertEquals("0", tablePaddings.getFirst().cellPadding);
        Assertions.assertEquals(3, map.size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHandleTableCellpaddingAttribute_cellpaddingInStyle_addCellpadding() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("style", "border-collapse:collapse;margin:2px;padding:20;width:546px;background-color:#dbefff;");

        filterJerichoHandler.handleTableCellpaddingAttribute(map);

        LinkedList<CellPadding> tablePaddings = (LinkedList<CellPadding>) MockUtils.getValueFromField(filterJerichoHandler, "tablePaddings");
        Assertions.assertEquals(1, tablePaddings.size());
        Assertions.assertEquals("20", tablePaddings.getFirst().cellPadding);
        Assertions.assertEquals(1, map.size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHandleTableCellpaddingAttribute_styleAvailableButNoCellpadding_doNotAdd() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("style", "border-collapse:collapse;margin:2px;width:546px;background-color:#dbefff;");

        filterJerichoHandler.handleTableCellpaddingAttribute(map);

        LinkedList<CellPadding> tablePaddings = (LinkedList<CellPadding>) MockUtils.getValueFromField(filterJerichoHandler, "tablePaddings");
        Assertions.assertEquals(1, tablePaddings.size());
        Assertions.assertEquals(null, tablePaddings.getFirst().cellPadding);
        Assertions.assertEquals(1, map.size());
    }

}
