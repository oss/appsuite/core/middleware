/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.html;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Queue;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;

/**
 * {@link AssertionHelper}
 *
 * @author <a href="mailto:lars.hoogestraat@open-xchange.com">Lars Hoogestraat</a>
 */
public class AssertionHelper {

    public static void assertSanitizedEquals(HtmlService service, String html, String expected, boolean ignoreCase) {
        try {
            String sanitized = service.sanitize(html, null, false, null, null);
            if (ignoreCase) {
                if (Strings.isNotEmpty(sanitized)) {
                    sanitized = sanitized.toLowerCase();
                }
                assertEquals(expected.toLowerCase(), sanitized, "Unexpected HTML sanitize result");
            }
            assertEquals(expected, sanitized, "Unexpected HTML sanitize result");
        } catch (OXException e) {
            Assertions.fail(e.getMessage());
        }
    }

    public static void assertSanitizedDoesNotContain(HtmlService service, String html, String... mailiciousParams) {
        assertSanitized(service, html, mailiciousParams, AssertExpression.NOT_CONTAINED);
    }

    public static void assertSanitizedEmpty(HtmlService service, String html) {
        assertSanitized(service, html, (String) null, AssertExpression.EMPTY);
    }

    public static void assertSanitized(HtmlService service, String html, String maliciousParam, AssertExpression ae) {
        assertSanitized(service, html, null == maliciousParam ? null : new String[] { maliciousParam }, ae);
    }

    public static void assertSanitized(HtmlService service, String html, String[] maliciousParams, AssertExpression ae) {
        try {
            String sanitized = service.sanitize(html, null, false, null, null);
            if (Strings.isNotEmpty(sanitized)) {
                sanitized = sanitized.toLowerCase();
            }
            if (AssertExpression.NOT_CONTAINED.equals(ae)) {
                if (null != maliciousParams) {
                    for (String maliciousParam : maliciousParams) {
                        assertFalse(StringUtils.containsIgnoreCase(sanitized, maliciousParam), "sanitized output: " + sanitized + " contains " + maliciousParam);
                    }
                }
            } else if (AssertExpression.EQUALS.equals(ae)) {
                if (null != maliciousParams && maliciousParams.length > 0) {
                    String maliciousParam = maliciousParams[0];
                    assertEquals(maliciousParam, sanitized, "sanitized output: " + sanitized + " is not equals " + maliciousParam);
                }
            } else if (AssertExpression.EMPTY.equals(ae)) {
                assertTrue(Strings.isEmpty(sanitized), "expected html: " + html + " after sanitizing to be empty but contains " + sanitized);
            }
        } catch (OXException e) {
            Assertions.fail(e.getMessage());
        }
    }

    public static void assertBlockingQuote(final Queue<String> quotedText, String[] quotedLines) {
        int line = 0;
        while (!quotedText.isEmpty()) {
            String qt = quotedText.poll();
            for (int i = line; i < quotedLines.length; i++) {
                if (quotedLines[i].contains(qt)) {
                    assertTrue(quotedLines[i].startsWith(">"), "The HTML <blockquote> tag is not properly converted to '>'");
                    line = i;
                    break;
                }
            }
        }
    }

    public static void assertTag(String tag, String actual, boolean closing) {
        assertTrue(actual.contains(tag), tag + " is missing");
        if (closing) {
            String closingTag = tag.substring(1);
            closingTag = "</" + closingTag;
            assertTag(closingTag, actual, false);
        }
    }
}
