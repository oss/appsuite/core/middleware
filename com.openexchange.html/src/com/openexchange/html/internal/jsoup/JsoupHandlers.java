/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.html.internal.jsoup;

import static com.openexchange.java.Strings.toLowerCase;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import com.openexchange.java.Strings;

/**
 * {@link JsoupHandlers} - Utility class for {@link JsoupHandler Jsoup handlers}.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.2
 */
public final class JsoupHandlers {

    /**
     * Initializes a new {@link JsoupHandlers}.
     */
    private JsoupHandlers() {
        super();
    }

    // -------------------------------------- Image check --------------------------------------------- //

    private static final String CID = "cid:";
    private static final String DATA = "data:";
    private static final Pattern PATTERN_FILENAME = Pattern.compile("([\\p{L}\\p{N}-_&&[^.\\s>\"]]+\\.[\\p{L}\\p{N}&&[^.\\s>\"]]+)");

    /**
     * Checks if specified value from &lt;img&gt; tag's <code>"src"</code> attribute appears to be an inline/embedded image.
     *
     * @param src The value of the <code>"src"</code> attribute to examine
     * @param exactCheckForEmbeddedImage Whether an exact check for an embedded image should be performed
     * @return <code>true</code> for an inline/embedded image; otherwise <code>false</code>
     */
    public static boolean isInlineImage(String src, boolean exactCheckForEmbeddedImage) {
        if (Strings.isEmpty(src)) {
            return false;
        }
        String tmp = toLowerCase(src.trim());
        return tmp.startsWith(CID) || (tmp.startsWith(DATA) && (exactCheckForEmbeddedImage ? isEmbeddedImage(tmp) : true)) || PATTERN_FILENAME.matcher(tmp).matches();
    }

    /** Simple class to delay initialization until needed */
    private static class DataBase64PatternHolder {

        /**
         * Pattern to match data URIs like <code>data:image/jpeg;base64,...</code> or <code>data:image/svg+xml;base64,...</code>
         */
        static final Pattern PATTERN_DATA_BASE64 = Pattern.compile("data:([\\p{L}_0-9-]+(?:/([\\p{L}_0-9-\\+\\.]+))?)?;base64,");
    }

    private static boolean isEmbeddedImage(String val) {
        Matcher m = DataBase64PatternHolder.PATTERN_DATA_BASE64.matcher(val);
        return m.find() && (m.start() == 0);
    }

    // ----------------------------------------------------------------------------------------------- //

    /**
     * Creates a new list containing given attributes to avoid {@link ConcurrentModificationException} when altering attributes during traversal.
     *
     * @param attributes The attributes to add
     * @return The newly created list
     */
    public static List<Attribute> listFor(Attributes attributes) {
        List<Attribute> copy = new ArrayList<>(attributes.size());
        for (Attribute attribute : attributes) {
            copy.add(attribute);
        }
        return copy;
    }

    /** The HTML elements that use <a href="https://www.w3schools.com/tags/att_href.asp">the href attribute</a> */
    private static final Set<String> HREF_TAGS = Set.of("a","area","base","link");

    /**
     * Checks if denoted tag may hold a <code>"href"</code> attribute according to <a href="http://www.w3schools.com/tags/att_href.asp">this specification</a>.
     *
     * @param tagName The name of the tag to check
     * @return <code>true</code> if tag possibly holds a <code>"href"</code> attribute; otherwise <code>false</code>
     */
    public static boolean isHrefTag(String tagName) {
        return HREF_TAGS.contains(tagName);
    }

    /** The HTML elements that use <a href="https://www.w3schools.com/tags/att_src.asp">the src attribute</a> to possibly specify an image file */
    private static final Set<String> SRC_IMAGE_TAGS = Set.of("embed","img","input");

    /**
     * Checks if denoted tag may hold a <code>"src"</code> attribute to specify an image file according to <a href="http://www.w3schools.com/tags/src_href.asp">this specification</a>.
     *
     * @param tagName The name of the tag to check
     * @return <code>true</code> if tag possibly holds a <code>"src"</code> attribute; otherwise <code>false</code>
     */
    public static boolean isSrcImageTag(String tagName) {
        return SRC_IMAGE_TAGS.contains(tagName);
    }

}
