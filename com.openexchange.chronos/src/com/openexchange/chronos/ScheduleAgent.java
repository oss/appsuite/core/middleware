/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos;

import com.openexchange.java.EnumeratedProperty;

/**
 * {@link ScheduleAgent}
 * 
 * To specify the agent expected to deliver scheduling messages to the corresponding <i>Organizer</i> or <i>Attendee</i>.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @see <a href="https://tools.ietf.org/html/rfc5545#section-3.2.16">RFC 5545, section 3.2.16</a>
 */
public class ScheduleAgent extends EnumeratedProperty {

    /**
     * The server handles scheduling.
     */
    public static final ScheduleAgent SERVER = new ScheduleAgent("SERVER");

    /**
     * The client handles scheduling.
     */
    public static final ScheduleAgent CLIENT = new ScheduleAgent("CLIENT");

    /**
     * No scheduling.
     */
    public static final ScheduleAgent NONE = new ScheduleAgent("NONE");

    /**
     * Initializes a new {@link ScheduleAgent}.
     *
     * @param value The property value
     */
    public ScheduleAgent(String value) {
        super(value);
    }

    @Override
    public String getDefaultValue() {
        return SERVER.getValue();
    }

    @Override
    protected String[] getStandardValues() {
        return getValues(SERVER, CLIENT, NONE);
    }

}
