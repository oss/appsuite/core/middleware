/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.advertisement.impl.services;

import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import org.json.JSONObject;
import org.json.JSONValue;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.advertisement.AdvertisementConfigService;
import com.openexchange.advertisement.impl.caching.Constants;
import com.openexchange.advertisement.impl.osgi.Services;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheLoader;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.exception.OXException;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;

/**
 * {@link AbstractAdvertisementConfigServiceTest}
 *
 * @author <a href="mailto:vitali.sjablow@open-xchange.com">Vitali Sjablow</a>
 * @since v7.8.4
 */
public class AbstractAdvertisementConfigServiceTest {

    @Mock
    Session session;

    @Mock
    CacheService cacheService;

    @Mock
    Cache<JSONValue> userCache;

    @Mock
    CacheKey cacheKey;

    private final int userId = 1;
    private final int contextId = 1;
    private MockedStatic<Services> staticServicesMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        ServiceLookup services = mock(ServiceLookup.class);
        Mockito.when(services.getService(CacheService.class)).thenReturn(cacheService);

        staticServicesMock = Mockito.mockStatic(Services.class);
        Mockito.when(Services.getService(CacheService.class)).thenReturn(cacheService);

        Mockito.when(I(session.getContextId())).thenReturn(I(contextId));
        Mockito.when(I(session.getUserId())).thenReturn(I(userId));

        Mockito.when(cacheService.getCache(AbstractAdvertisementConfigService.CACHE_OPTIONS)).thenReturn(userCache);
    }

    @AfterEach
    public void tearDown() {
        staticServicesMock.close();
    }

    private final AbstractAdvertisementConfigService configService = new AbstractAdvertisementConfigService() {

        @Override
        public String getSchemeId() {
            return null;
        }

        @Override
        protected String getReseller(int contextId) throws OXException {
            return RESELLER_ALL;
        }

        @Override
        protected String getPackage(Session session) throws OXException {
            return PACKAGE_ALL;
        }
    };

    @Test
    public void getConfigTest_withoutCacheUserInternal() throws Exception {
        try (MockedStatic<AbstractAdvertisementConfigService> mockedStatic = Mockito.mockStatic(AbstractAdvertisementConfigService.class)) {
            Mockito.when(Services.getService(CacheService.class)).thenReturn(null);
            JSONValue result = new JSONObject();
            mockedStatic.when(() -> AbstractAdvertisementConfigService.getConfigByUserInternal(session)).thenReturn(result);
            JSONValue config = configService.getConfig(session);
            assertTrue(config != null);
        }


    }

    @Test
    public void getConfigTest_withoutCacheInternal() throws Exception {
        Mockito.when(Services.getService(CacheService.class)).thenReturn(null);
        JSONValue result = new JSONObject();

        try (MockedStatic<AbstractAdvertisementConfigService> mockedStatic = Mockito.mockStatic(AbstractAdvertisementConfigService.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> AbstractAdvertisementConfigService.getConfigByUserInternal(session)).thenReturn(null);
            mockedStatic.when(() -> AbstractAdvertisementConfigService.getConfigInternal(AdvertisementConfigService.RESELLER_ALL, AdvertisementConfigService.PACKAGE_ALL)).thenReturn(result);

            JSONValue config = configService.getConfig(session);
            assertTrue(config != null);
        }
    }

    @Test
    public void getConfigTest_userCache() throws Exception {
        Mockito.when(userCache.newGroupMemberKey(Constants.DEFAULT_CACHE_GROUP, String.valueOf(contextId), String.valueOf(userId))).thenReturn(cacheKey);
        JSONObject result = new JSONObject();
        result.put("someValue", true);
        Mockito.when(userCache.get(eq(cacheKey), (CacheLoader<JSONValue>) any())).thenReturn(result);

        JSONValue config = configService.getConfig(session);
        assertNotNull(config);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void getConfigTest_normalCache() throws Exception {
        Mockito.when(userCache.newKey(String.valueOf(contextId), String.valueOf(userId))).thenReturn(cacheKey);
        Mockito.when(userCache.newKey(Constants.RESELLER_KEY_PREFIX, AdvertisementConfigService.RESELLER_ALL, AdvertisementConfigService.PACKAGE_ALL)).thenReturn(cacheKey);
        JSONObject result = new JSONObject();
        result.put("test", "test"); // Add value to avoid empty object
        Mockito.when(userCache.get(any())).thenReturn(null);
        Mockito.when(userCache.get(any(), any(CacheLoader.class))).thenReturn(result);

        try (MockedStatic<AbstractAdvertisementConfigService> mockedStatic = Mockito.mockStatic(AbstractAdvertisementConfigService.class, Mockito.CALLS_REAL_METHODS)) {
            mockedStatic.when(() -> AbstractAdvertisementConfigService.getConfigByUserInternal(session)).thenReturn(null);
            mockedStatic.when(() -> AbstractAdvertisementConfigService.getConfigInternal(AdvertisementConfigService.RESELLER_ALL, AdvertisementConfigService.PACKAGE_ALL)).thenReturn(result);

            JSONValue config = configService.getConfig(session);
            assertTrue(config != null);
        }
    }

}
