/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.advertisement.impl.services;

import static com.openexchange.advertisement.AdvertisementExceptionCodes.CONFIG_NOT_FOUND;
import static com.openexchange.java.Autoboxing.I;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import org.json.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.advertisement.AdvertisementConfigService;
import com.openexchange.advertisement.AdvertisementExceptionCodes;
import com.openexchange.advertisement.ConfigResult;
import com.openexchange.advertisement.impl.caching.Constants;
import com.openexchange.advertisement.impl.osgi.Services;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.codec.json.GenericJSONValueCacheValueCodec;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.config.cascade.ConfigProperty;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.config.cascade.ConfigViewScope;
import com.openexchange.context.ContextService;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.java.Strings;
import com.openexchange.session.Session;
import com.openexchange.user.UserService;

/**
 * {@link AbstractAdvertisementConfigService}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v7.8.3
 */
public abstract class AbstractAdvertisementConfigService implements AdvertisementConfigService {

    private static final JSONValue NULL_VALUE = JSONObject.EMPTY_OBJECT;

    private static final Logger LOG = LoggerFactory.getLogger(AdvertisementConfigService.class);

    protected static final CacheOptions<JSONValue> CACHE_OPTIONS = CacheOptions.<JSONValue> builder()
                                                                             .withCoreModuleName(CoreModuleName.ADVERTISEMENT_CONFIG)
                                                                             .withCodecAndVersion(GenericJSONValueCacheValueCodec.getInstance())
                                                                             .build();

    private static final String SQL_INSERT_MAPPING = "REPLACE INTO advertisement_mapping (reseller,package,configId) VALUES (?,?,?)";
    private static final String SQL_INSERT_CONFIG = "INSERT INTO advertisement_config (reseller,config) VALUES (?,?)";
    private static final String SQL_UPDATE_CONFIG = "UPDATE advertisement_config SET config=? WHERE configId=?";
    private static final String SQL_UPDATE_MAPPING = "UPDATE advertisement_mapping SET configId=? WHERE reseller=? and package=?";
    private static final String SQL_DELETE_CONFIG = "DELETE FROM advertisement_config WHERE configId=?";
    private static final String SQL_DELETE_MAPPING = "DELETE FROM advertisement_mapping WHERE configId=?";
    private static final String SQL_SELECT_MAPPING_SIMPLE = "SELECT configId FROM advertisement_mapping WHERE reseller=? AND package=?";
    private static final String SQL_SELECT_CONFIG = "SELECT config FROM advertisement_config WHERE configId=?";
    private static final String PREVIEW_CONFIG = "com.openexchange.advertisement.preview";

    /**
     * Initializes a new {@link AbstractAdvertisementConfigService}.
     */
    protected AbstractAdvertisementConfigService() {
        super();
    }

    /**
     * Gets the reseller name associated with specified contextId.
     *
     * @param contextId The context identifier
     * @return The reseller name
     * @throws OXException If reseller name cannot be resolved
     */
    protected abstract String getReseller(int contextId) throws OXException;

    /**
     * Gets the package name associated with specified session.
     *
     * @param session The session
     * @return The package name
     * @throws OXException If package name cannot be resolved
     */
    protected abstract String getPackage(Session session) throws OXException;

    @Override
    public boolean isAvailable(Session session) {
        try {
            return getConfig(session) != null;
        } catch (OXException e) {
            if (false == CONFIG_NOT_FOUND.equals(e)) {
                LOG.error("Error while getting the advertisement configuration", e);
            }
            return false;
        }
    }

    @Override
    public JSONValue getConfig(Session session) throws OXException {
        CacheService cacheService = Services.getService(CacheService.class);
        if (cacheService == null) {
            // Get config w/o cache
            JSONValue optInternalConfiguration = optInternalConfiguration(session);
            if (optInternalConfiguration == null) {
                throw CONFIG_NOT_FOUND.create(I(session.getUserId()), I(session.getContextId()));
            }
            return optInternalConfiguration;
        }

        // Get config w/ cache
        Cache<JSONValue> cache = cacheService.getCache(CACHE_OPTIONS);
        CacheKey key = createKey(cache, session.getContextId(), session.getUserId());
        JSONValue config = cache.get(key, k -> optInternalConfigurationForCache(session));
        if (NULL_VALUE.isEqualTo(config)) {
            throw AdvertisementExceptionCodes.CONFIG_NOT_FOUND.create(I(session.getUserId()), I(session.getContextId()));
        }
        return config;
    }

    /**
     * Gets the cache-ready applicable advertisement configuration for given session.
     *
     * @param session The session providing user information
     * @return The advertisement configuration or {@link #NULL_VALUE}, but never <code>null</code>
     * @throws OXException If advertisement configuration cannot be returned
     */
    private JSONValue optInternalConfigurationForCache(Session session) throws OXException {
        JSONValue result = optInternalConfiguration(session);
        return result == null ? NULL_VALUE : result;
    }

    /**
     * Gets the applicable advertisement configuration for given session.
     *
     * @param session The session providing user information
     * @return The advertisement configuration or <code>null</code>
     * @throws OXException If advertisement configuration cannot be returned
     */
    private JSONValue optInternalConfiguration(Session session) throws OXException {
        JSONValue result = getConfigByUserInternal(session);
        if (result == null) {
            String reseller = null;
            String pack = null;
            try {
                reseller = getReseller(session.getContextId());
            } catch (OXException e) {
                LOG.debug("Error while retrieving the reseller for user {} in context {}.", I(session.getUserId()), I(session.getContextId()), e);
            }
            try {
                pack = getPackage(session);
            } catch (OXException e) {
                LOG.debug("Error while retrieving the package for user {} in context {}.", I(session.getUserId()), I(session.getContextId()), e);
            }
            // fallback to defaults in case reseller or package is empty
            if (Strings.isEmpty(reseller)) {
                reseller = RESELLER_ALL;
            }
            if (Strings.isEmpty(pack)) {
                pack = PACKAGE_ALL;
            }
            result = getConfigInternal(reseller, pack);
        }
        return result;
    }

    protected static JSONValue getConfigByUserInternal(Session session) throws OXException {
        ConfigViewFactory factory = Services.getService(ConfigViewFactory.class);
        ConfigView view = factory.getView(session.getUserId(), session.getContextId());
        String id = view.get(PREVIEW_CONFIG, String.class);
        if (Strings.isEmpty(id)) {
            return null;
        }

        int configId = Integer.parseInt(id);
        DatabaseService dbService = Services.getService(DatabaseService.class);
        Connection con = dbService.getReadOnly();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = con.prepareStatement(SQL_SELECT_CONFIG);
            stmt.setInt(1, configId);
            result = stmt.executeQuery();
            if (result.next()) {
                return JSONServices.parse(result.getString(1));
            }
        } catch (SQLException e) {
            throw AdvertisementExceptionCodes.UNEXPECTED_DATABASE_ERROR.create(e, e.getMessage());
        } catch (JSONException e) {
            LOG.warn("Invalid advertisement configuration data with id {}. Falling back to reseller and package based determination of advertisement configuration", I(configId), e);
            // Invalid advertisement data. Fallback to reseller and package
        } finally {
            Databases.closeSQLStuff(result, stmt);
            dbService.backReadOnly(con);
        }

        return null;
    }

    protected static JSONValue getConfigInternal(String reseller, String pack) throws OXException {
        DatabaseService dbService = Services.getService(DatabaseService.class);
        Connection con = dbService.getReadOnly();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            int x = 1;
            stmt = con.prepareStatement(SQL_SELECT_MAPPING_SIMPLE);
            stmt.setString(x++, reseller);
            stmt.setString(x++, pack);
            result = stmt.executeQuery();
            if (result.next()) {
                try {
                    int configId = result.getInt(1);
                    Databases.closeSQLStuff(result, stmt);
                    stmt = con.prepareStatement(SQL_SELECT_CONFIG);
                    stmt.setInt(1, configId);
                    result = stmt.executeQuery();
                    if (result.next()) {
                        return JSONServices.parse(result.getString(1));
                    }
                } catch (JSONException e) {
                    LOG.error("Invalid advertisement configuration data for reseller {} and package {}", reseller, pack, e);
                }
            }
            return null;
        } catch (SQLException e) {
            throw AdvertisementExceptionCodes.UNEXPECTED_DATABASE_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(result, stmt);
            dbService.backReadOnly(con);
        }
    }

    @Override
    public ConfigResult setConfigByName(String name, int contextId, String config) {
        try {
            ContextService contextService = Services.getService(ContextService.class);
            Context ctx = contextService.getContext(contextId);
            UserService userService = Services.getService(UserService.class);
            int userId = userService.getUserId(name, ctx);
            return setConfig(userId, contextId, config);
        } catch (OXException e) {
            return new ConfigResult(ConfigResultType.ERROR, e);
        }
    }

    @Override
    public ConfigResult setConfig(int userId, int ctxId, String config) {
        try {
            ConfigViewFactory factory = Services.getService(ConfigViewFactory.class);
            ConfigView view = factory.getView(userId, ctxId);
            ConfigProperty<String> property = view.property(ConfigViewScope.USER.getScopeName(), PREVIEW_CONFIG, String.class);

            DatabaseService dbService = Services.getService(DatabaseService.class);
            Connection con = dbService.getWritable();
            PreparedStatement stmt = null;
            ResultSet result = null;
            boolean setProperty = false;
            String newPropertyValue = null;

            ConfigResult configResult = null;
            try {
                String value = property.get();
                if (value != null) {
                    int configId = Integer.parseInt(value);
                    if (config == null) {
                        stmt = con.prepareStatement(SQL_DELETE_CONFIG);
                        stmt.setInt(1, configId);
                        stmt.execute();
                        setProperty = true;
                        configResult = new ConfigResult(ConfigResultType.DELETED, null);
                    } else {
                        stmt = con.prepareStatement(SQL_UPDATE_CONFIG);
                        stmt.setString(1, config);
                        stmt.setInt(2, configId);
                        if (stmt.executeUpdate() == 0) {
                            // Nothing to update. Insert new row instead.
                            Databases.closeSQLStuff(stmt);
                            String reseller = null;
                            try {
                                reseller = getReseller(ctxId);
                            } catch (@SuppressWarnings("unused") OXException e) {
                                // no reseller found
                            }
                            if (reseller == null) {
                                reseller = RESELLER_ALL;
                            }
                            stmt = con.prepareStatement(SQL_INSERT_CONFIG, Statement.RETURN_GENERATED_KEYS);
                            stmt.setString(1, reseller);
                            stmt.setString(2, config);
                            stmt.execute();
                            result = stmt.getGeneratedKeys();
                            if (!result.next()) {
                                throw createInsertFailedException();
                            }
                            int resultConfigId = result.getInt(1);
                            setProperty = true;
                            newPropertyValue = String.valueOf(resultConfigId);
                            configResult = new ConfigResult(ConfigResultType.CREATED, null);
                        } else {
                            configResult = new ConfigResult(ConfigResultType.UPDATED, null);
                        }
                    }
                } else {
                    if (config == null) {
                        return new ConfigResult(ConfigResultType.IGNORED, null);
                    }
                    String reseller = null;
                    try {
                        reseller = getReseller(ctxId);
                    } catch (@SuppressWarnings("unused") OXException e) {
                        // no reseller found
                    }
                    if (reseller == null) {
                        reseller = RESELLER_ALL;
                    }
                    stmt = con.prepareStatement(SQL_INSERT_CONFIG, Statement.RETURN_GENERATED_KEYS);
                    stmt.setString(1, reseller);
                    stmt.setString(2, config);
                    stmt.execute();
                    result = stmt.getGeneratedKeys();
                    if (!result.next()) {
                        throw createInsertFailedException();
                    }
                    int resultConfigId = result.getInt(1);
                    setProperty = true;
                    newPropertyValue = String.valueOf(resultConfigId);
                    configResult = new ConfigResult(ConfigResultType.CREATED, null);
                }
            } catch (SQLException e) {
                return new ConfigResult(ConfigResultType.ERROR, AdvertisementExceptionCodes.UNEXPECTED_DATABASE_ERROR.create(e, e.getMessage()));
            } finally {
                Databases.closeSQLStuff(result, stmt);
                dbService.backWritable(con);
            }

            if (setProperty) {
                property.set(newPropertyValue);
            }

            // Remove entry from cache
            CacheService cacheService = Services.getService(CacheService.class);
            if (cacheService != null) {
                Cache<JSONValue> cache = cacheService.getCache(CACHE_OPTIONS);
                cache.invalidate(createKey(cache, ctxId, userId));
            }

            return configResult;
        } catch (OXException e) {
            return new ConfigResult(ConfigResultType.ERROR, e);
        }
    }

    @Override
    public ConfigResult setConfig(String reseller, String pack, String config) {
        try {
            ConfigResultType type = setConfigInternal(reseller, pack, config);
            return new ConfigResult(type, null);
        } catch (OXException e) {
            return new ConfigResult(ConfigResultType.ERROR, e);
        }
    }

    private static ConfigResultType setConfigInternal(String reseller, String pack, String config) throws OXException {
        ConfigResultType status = ConfigResultType.IGNORED;
        String resellerToUse = reseller;
        if (Strings.isEmpty(resellerToUse)) {
            resellerToUse = RESELLER_ALL;
        }
        String packToUse = pack;
        if (Strings.isEmpty(packToUse)) {
            packToUse = PACKAGE_ALL;
        }

        DatabaseService dbService = Services.getService(DatabaseService.class);
        Connection con = dbService.getWritable();
        PreparedStatement stmt = null;
        ResultSet result = null;
        boolean readOnly = false;
        try {

            stmt = con.prepareStatement(SQL_SELECT_MAPPING_SIMPLE);
            stmt.setString(1, resellerToUse);
            stmt.setString(2, packToUse);
            result = stmt.executeQuery();

            if (result.next()) {

                int configId = Integer.parseInt(result.getString(1));
                Databases.closeSQLStuff(result, stmt);
                if (config == null) {
                    stmt = con.prepareStatement(SQL_DELETE_CONFIG);
                    stmt.setInt(1, configId);
                    stmt.execute();
                    Databases.closeSQLStuff(stmt);
                    stmt = con.prepareStatement(SQL_DELETE_MAPPING);
                    stmt.setInt(1, configId);
                    stmt.execute();
                    status = ConfigResultType.DELETED;
                } else {
                    stmt = con.prepareStatement(SQL_UPDATE_CONFIG);
                    stmt.setString(1, config);
                    stmt.setInt(2, configId);
                    if (0 == stmt.executeUpdate()) {
                        // Nothing to update. Create new one instead
                        Databases.closeSQLStuff(stmt);
                        stmt = con.prepareStatement(SQL_INSERT_CONFIG, Statement.RETURN_GENERATED_KEYS);
                        stmt.setString(1, resellerToUse);
                        stmt.setString(2, config);
                        stmt.execute();
                        result = stmt.getGeneratedKeys();
                        if (!result.next()) {
                            throw createInsertFailedException();
                        }
                        int resultConfigId = result.getInt(1);
                        Databases.closeSQLStuff(stmt);
                        stmt = con.prepareStatement(SQL_UPDATE_MAPPING);
                        stmt.setInt(1, resultConfigId);
                        stmt.setString(2, resellerToUse);
                        stmt.setString(3, packToUse);
                        stmt.execute();
                        status = ConfigResultType.CREATED;
                    } else {
                        status = ConfigResultType.UPDATED;
                    }
                }

            } else {
                Databases.closeSQLStuff(result, stmt);
                if (config == null) {
                    readOnly = true;
                    return ConfigResultType.IGNORED;
                }
                stmt = con.prepareStatement(SQL_INSERT_CONFIG, Statement.RETURN_GENERATED_KEYS);
                stmt.setString(1, resellerToUse);
                stmt.setString(2, config);
                stmt.execute();
                result = stmt.getGeneratedKeys();
                if (!result.next()) {
                    throw createInsertFailedException();
                }
                int resultConfigId = result.getInt(1);
                Databases.closeSQLStuff(stmt);
                stmt = con.prepareStatement(SQL_INSERT_MAPPING);
                stmt.setString(1, resellerToUse);
                stmt.setString(2, packToUse);
                stmt.setInt(3, resultConfigId);
                stmt.execute();
                status = ConfigResultType.CREATED;
            }

            // clear read cache
            CacheService cacheService = Services.getService(CacheService.class);
            if (cacheService != null) {
                Cache<JSONValue> cache = cacheService.getCache(CACHE_OPTIONS);
                cache.invalidateGroup(createGroupKey(cache));
            }
        } catch (SQLException e) {
            throw AdvertisementExceptionCodes.UNEXPECTED_DATABASE_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(result, stmt);
            if (readOnly) {
                dbService.backWritableAfterReading(con);
            } else {
                dbService.backWritable(con);
            }
        }
        return status;
    }

    @Override
    public List<ConfigResult> setConfig(String reseller, Map<String, String> configs) {
        if (configs == null) {
            return Collections.emptyList();
        }

        int size = configs.size();
        if (size <= 0) {
            return Collections.emptyList();
        }

        List<ConfigResult> resultList = new ArrayList<>(size);
        for (Map.Entry<String, String> entry : configs.entrySet()) {
            try {
                ConfigResultType status = setConfigInternal(reseller, entry.getKey(), entry.getValue());
                resultList.add(new ConfigResult(status, null));
            } catch (OXException e) {
                resultList.add(new ConfigResult(ConfigResultType.ERROR, e));
            }
        }

        return resultList;
    }

    // -------------------------------- Helper methods ---------------------

    private static OXException createInsertFailedException() {
        return AdvertisementExceptionCodes.UNEXPECTED_DATABASE_ERROR.create("Insert operation failed to retrieve generated key.");
    }

    private static <V> CacheKey createKey(Cache<V> cache, int contextId, int userId) {
        return cache.newGroupMemberKey(Constants.DEFAULT_CACHE_GROUP, Integer.toString(contextId), Integer.toString(userId));
    }

    private static <V> CacheKey createGroupKey(Cache<V> cache) {
        return cache.newGroupKey(Constants.DEFAULT_CACHE_GROUP);
    }
}
