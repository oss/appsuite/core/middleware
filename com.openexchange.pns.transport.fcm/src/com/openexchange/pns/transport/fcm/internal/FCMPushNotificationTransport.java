/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
* 
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.pns.transport.fcm.internal;

import static com.openexchange.java.Autoboxing.I;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.ObjIntConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidConfig.Priority;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.SendResponse;
import com.openexchange.config.cascade.ComposedConfigProperty;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.exception.OXException;
import com.openexchange.pns.DefaultToken;
import com.openexchange.pns.EnabledKey;
import com.openexchange.pns.KnownTransport;
import com.openexchange.pns.PushExceptionCodes;
import com.openexchange.pns.PushMatch;
import com.openexchange.pns.PushMessageGenerator;
import com.openexchange.pns.PushMessageGeneratorRegistry;
import com.openexchange.pns.PushNotification;
import com.openexchange.pns.PushNotificationTransport;
import com.openexchange.pns.PushNotifications;
import com.openexchange.pns.PushSubscriptionDescription;
import com.openexchange.pns.PushSubscriptionRegistry;
import com.openexchange.pns.PushSubscriptionRestrictions;
import com.openexchange.pns.SuffixAwarePushClientProvider;
import com.openexchange.push.clients.PushClientProvider;
import com.openexchange.push.clients.PushClientProviderFactory;

/**
 * {@link FCMPushNotificationTransport}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class FCMPushNotificationTransport implements PushNotificationTransport {

    private static final Logger LOG = LoggerFactory.getLogger(FCMPushNotificationTransport.class);

    private static final String ID = KnownTransport.FCM.getTransportId();

    /**
     * Multicast size of registration tokens. Even though the online documentation states that are 1000 registration
     * ids allowed, the client API states that the limit is set to 500. We are going with the client API.
     * 
     * @see <a href="https://firebase.google.com/docs/cloud-messaging/js/topic-messaging">Topic Messaging</a>
     */
    private static final int MULTICAST_LIMIT = 500;
    private static final int MAX_PAYLOAD_SIZE = 4096;

    private final Cache<EnabledKey, Boolean> cacheAvailability = CacheBuilder.newBuilder().maximumSize(65536).expireAfterWrite(30, TimeUnit.MINUTES).build();

    private final ConfigViewFactory configViewFactory;
    private final PushClientProvider<FirebaseMessaging> clientProvider;
    private final PushMessageGeneratorRegistry generatorRegistry;
    private final PushSubscriptionRegistry pushSubscriptionRegistry;

    /**
     * Initialises a new {@link FCMPushNotificationTransport}.
     * 
     * @param pushSubscriptionRegistry
     */
    public FCMPushNotificationTransport(ConfigViewFactory configViewFactory, PushClientProviderFactory clientProviderFactory, PushMessageGeneratorRegistry generatorRegistry, PushSubscriptionRegistry pushSubscriptionRegistry) {
        super();
        this.configViewFactory = configViewFactory;
        this.generatorRegistry = generatorRegistry;
        this.pushSubscriptionRegistry = pushSubscriptionRegistry;
        this.clientProvider = new SuffixAwarePushClientProvider<>(clientProviderFactory.createProvider(FirebaseMessaging.class), "-android");
    }

    @Override
    public boolean isEnabled(String topic, String client, int userId, int contextId) throws OXException {
        EnabledKey key = new EnabledKey(topic, client, userId, contextId);
        Boolean result = cacheAvailability.getIfPresent(key);
        if (null == result) {
            result = Boolean.valueOf(doCheckEnabled(topic, client, userId, contextId));
            cacheAvailability.put(key, result);
        }
        return result.booleanValue();
    }

    @Override
    public void transport(PushNotification notification, Collection<PushMatch> matches) throws OXException {
        if (null == notification || null == matches) {
            return;
        }
        Map<String, List<PushMatch>> clientMatches = categorize(matches);
        for (Map.Entry<String, List<PushMatch>> entry : clientMatches.entrySet()) {
            transport(entry.getKey(), notification, entry.getValue());
        }
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public boolean servesClient(String client) throws OXException {
        return clientProvider.optClient(client).isPresent();
    }

    /**
     * Invalidates the <i>enabled cache</i>.
     */
    public void invalidateEnabledCache() {
        cacheAvailability.invalidateAll();
    }

    /**
     * Checks if the transport is enabled for the specified topic, client, user in context
     * 
     * @param topic The topic
     * @param client The client
     * @param userId the user identifier
     * @param contextId The context identifier
     * @return <code>true</code> if the transport is enabled; <code>false</code> otherwise
     * @throws OXException if an error is occurred
     */
    private boolean doCheckEnabled(String topic, String client, int userId, int contextId) throws OXException {
        ConfigView view = configViewFactory.getView(userId, contextId);

        String basePropertyName = "com.openexchange.pns.transport.fcm.enabled";

        ComposedConfigProperty<Boolean> property;
        property = null == topic || null == client ? null : view.property(basePropertyName + "." + client + "." + topic, boolean.class);
        if (null != property && property.isDefined()) {
            return property.get().booleanValue();
        }

        property = null == client ? null : view.property(basePropertyName + "." + client, boolean.class);
        if (null != property && property.isDefined()) {
            return property.get().booleanValue();
        }

        property = view.property(basePropertyName, boolean.class);
        if (null != property && property.isDefined()) {
            return property.get().booleanValue();
        }

        return false;
    }

    /**
     * Categorises the push subscriptions per client
     * 
     * @param matches The push subscriptions
     * @return The categorised push subscriptions
     * @throws OXException if an error is occurred
     */
    private Map<String, List<PushMatch>> categorize(Collection<PushMatch> matches) throws OXException {
        Map<String, List<PushMatch>> clientMatches = LinkedHashMap.newLinkedHashMap(matches.size());
        for (PushMatch match : matches) {
            PushMessageGenerator generator = generatorRegistry.getGenerator(match);
            if (null == generator) {
                throw PushExceptionCodes.NO_SUCH_GENERATOR.create(match.getClient(), match.getTransportId());
            }
            clientMatches.computeIfAbsent(match.getClient(), c -> new LinkedList<>()).add(match);
        }
        return clientMatches;
    }

    /**
     * Sends the push notification to the specified devices
     * 
     * @param client The client
     * @param notification The notification to send
     * @param matches The devices to send it to
     * @throws OXException if an error is occurred
     */
    private void transport(String client, PushNotification notification, List<PushMatch> matches) throws OXException {
        if (null == notification || null == matches) {
            return;
        }

        LOG.debug("Going to send notification '{}' via transport '{}' for user {} in context {}", notification.getTopic(), ID, I(notification.getUserId()), I(notification.getContextId()));
        int size = matches.size();
        if (size <= 0) {
            return;
        }

        Optional<FirebaseMessaging> optSender = optSender(client);
        if (optSender.isEmpty()) {
            return;
        }

        List<String> registrationIDs = new ArrayList<>(size);
        for (int i = 0; i < size; i += MULTICAST_LIMIT) {
            registrationIDs.clear();

            int length = Math.min(size, i + MULTICAST_LIMIT) - i;
            //@formatter:off
            List<String> chunk = IntStream.range(i, i + length)
                    .mapToObj(j -> matches.get(j).getToken().getValue())
                    .toList();
            //@formatter:on
            registrationIDs.addAll(chunk);

            // Send chunk
            if (registrationIDs.isEmpty()) {
                continue;
            }
            try {
                BatchResponse batchResponse = optSender.get().sendEachForMulticast(createMessage(client, notification, registrationIDs));
                // Might contain a partial failure
                processResponse(notification, registrationIDs, batchResponse);
            } catch (FirebaseMessagingException e) {
                // Total failure, no notification was sent at all
                LOG.warn("Error publishing push notification", e);
                handleException(notification, registrationIDs, e);
            }
        }
    }

    /**
     * Creates the multicast message to send to the devices
     * 
     * @param client The client
     * @param notification The notification to send
     * @param registrtationIDs The registration ids of the devices
     * @return The multicast message
     * @throws OXException when no message generator exists or there is an unsupported message class
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private MulticastMessage createMessage(String client, PushNotification notification, List<String> registrtationIDs) throws OXException {
        PushMessageGenerator generator = generatorRegistry.getGenerator(client, ID);
        if (null == generator) {
            throw PushExceptionCodes.NO_SUCH_GENERATOR.create(client);
        }

        com.openexchange.pns.Message<?> message = generator.generateMessageFor(ID, notification);
        Object object = message.getMessage();
        if (object instanceof MulticastMessage m) {
            return checkMessage(m);
        }
        if (object instanceof Map m) {
            return checkMessage(toMessage(m, registrtationIDs));
        }
        throw PushExceptionCodes.UNSUPPORTED_MESSAGE_CLASS.create(null == object ? "null" : object.getClass().getName());
    }

    /**
     * Checks whether the message's payload size exceeds the limits
     * 
     * @param message The message
     * @return The message for chained calls
     * @throws OXException if the message is too big
     */
    private MulticastMessage checkMessage(MulticastMessage message) throws OXException {
        int currentLength = PushNotifications.getPayloadLength(message.toString());
        if (currentLength > MAX_PAYLOAD_SIZE) {
            throw PushExceptionCodes.MESSAGE_TOO_BIG.create(I(MAX_PAYLOAD_SIZE), I(currentLength));
        }
        return message;
    }

    /**
     * Converts the specified map to a multicast message
     * Note: 'contentAvailable', 'delayWhileIdle' and 'badge' properties are not available in FCM
     * 
     * @param message the map containing the message information
     * @param registrationIds the registration ids for the devices
     * @return The multicast message
     * @throws OXException if the icon element is missing
     */
    private MulticastMessage toMessage(Map<String, Object> message, List<String> registrationIds) throws OXException {
        Map<String, Object> source = new HashMap<>(message);

        AndroidNotification.Builder notificationBuilder = AndroidNotification.builder();
        setValue(source, "icon", notificationBuilder, AndroidNotification.Builder::setIcon);
        setValue(source, "body", notificationBuilder, AndroidNotification.Builder::setBody);
        setValue(source, "click_action", notificationBuilder, AndroidNotification.Builder::setClickAction);
        setValue(source, "color", notificationBuilder, AndroidNotification.Builder::setColor);
        setValue(source, "sound", notificationBuilder, AndroidNotification.Builder::setSound);
        setValue(source, "tag", notificationBuilder, AndroidNotification.Builder::setTag);
        setValue(source, "title", notificationBuilder, AndroidNotification.Builder::setTitle);

        AndroidConfig.Builder androidConfigBuilder = AndroidConfig.builder();
        setValue(source, "collapse_key", androidConfigBuilder, AndroidConfig.Builder::setCollapseKey);
        setValue(source, "restricted_package_name", androidConfigBuilder, AndroidConfig.Builder::setRestrictedPackageName);
        setIntValue(source, "time_to_live", androidConfigBuilder, AndroidConfig.Builder::setTtl);
        setPriority(source, androidConfigBuilder);
        androidConfigBuilder.setNotification(notificationBuilder.build());

        MulticastMessage.Builder builder = MulticastMessage.builder();
        builder.setAndroidConfig(androidConfigBuilder.build());
        builder.addAllTokens(registrationIds);

        // Add the remaining data as raw data
        for (Map.Entry<String, Object> entry : message.entrySet()) {
            String value = String.valueOf(entry.getValue());
            builder.putData(entry.getKey(), value);
        }

        return builder.build();
    }

    /**
     * Sets the value to the specified builder
     * 
     * @param source The source containing the value
     * @param key the key for the value
     * @param builder The builder
     * @param valueSetter The value setter
     * @throws OXException if a mandatory value is empty
     */
    private void setValue(Map<String, Object> source, String key, AndroidNotification.Builder builder, BiConsumer<AndroidNotification.Builder, String> valueSetter) throws OXException {
        String value = (String) source.remove(key);
        if (value != null) {
            valueSetter.accept(builder, value);
            return;
        }
        throw PushExceptionCodes.MESSAGE_GENERATION_FAILED.create("Missing '" + key + "' element.");
    }

    /**
     * Sets the value to the specified builder
     * 
     * @param source The source containing the value
     * @param key the key for the value
     * @param builder The builder
     * @param valueSetter The value setter
     */
    private void setValue(Map<String, Object> source, String key, AndroidConfig.Builder builder, BiConsumer<AndroidConfig.Builder, String> valueSetter) {
        String value = (String) source.remove(key);
        if (value == null) {
            return;
        }
        valueSetter.accept(builder, value);
    }

    /**
     * Sets the integer value to the specified builder
     * 
     * @param source The source containing the value
     * @param key the key for the value
     * @param builder The builder
     * @param valueSetter The value setter
     */
    private void setIntValue(Map<String, Object> source, String key, AndroidConfig.Builder builder, ObjIntConsumer<AndroidConfig.Builder> valueSetter) {
        Integer value = (Integer) source.remove(key);
        if (value == null) {
            return;
        }
        valueSetter.accept(builder, value.intValue());
    }

    /**
     * Sets the priority to the specified builder
     * 
     * @param source The source containing the value
     * @param key the key for the value
     * @param builder The builder
     * @param valueSetter The value setter
     */
    private void setPriority(Map<String, Object> source, AndroidConfig.Builder builder) {
        String sPriority = (String) source.remove("priority");
        if (null == sPriority) {
            return;
        }
        if ("high".equalsIgnoreCase(sPriority)) {
            builder.setPriority(Priority.HIGH);
        } else if ("normal".equalsIgnoreCase(sPriority)) {
            builder.setPriority(Priority.NORMAL);
        }
    }

    /**
     * Gets the optional FCM client for the given client id
     *
     * @param client The client id
     * @return The optional FCM sender
     */
    private Optional<FirebaseMessaging> optSender(String client) {
        return clientProvider.optClient(client);
    }

    /**
     * Processes the response
     * 
     * @param notification the notification
     * @param registrationIDs The registration ids
     * @param batchResponse the batch response
     */
    private void processResponse(PushNotification notification, List<String> registrationIDs, BatchResponse batchResponse) {
        if (null == registrationIDs) {
            LOG.warn("Unable to process empty results");
            return;
        }
        String registrationIDsString = registrationIDs.stream().collect(Collectors.joining(","));
        if (batchResponse == null) {
            LOG.info("Failed to send notification '{}' via transport '{}' for user {} in context {} to registration ID(s): {}", notification.getTopic(), ID, I(notification.getUserId()), I(notification.getContextId()), registrationIDsString);
            return;
        }
        if (batchResponse.getFailureCount() == 0) {
            LOG.debug("Sent notification '{}' via transport '{}' for user {} in context {} to registration ID(s): {}", notification.getTopic(), ID, I(notification.getUserId()), I(notification.getContextId()), registrationIDsString);
            LOG.debug("{}", batchResponse);
            return;
        }
        List<SendResponse> responses = batchResponse.getResponses();
        if (responses == null || responses.isEmpty()) {
            return;
        }

        int numOfResults = batchResponse.getSuccessCount() + batchResponse.getFailureCount();
        for (int i = 0; i < numOfResults; i++) {
            // Assuming results' order is the same as in the sent order...
            SendResponse response = responses.get(i);
            String registrationId = registrationIDs.get(i);
            if (response.getException() == null) {
                LOG.debug("Sent notification '{}' via transport '{}' for user {} in context {} to registration ID: {}", notification.getTopic(), ID, I(notification.getUserId()), I(notification.getContextId()), registrationId);
                continue;
            }
            handleException(notification, List.of(registrationId), response.getException());
        }
    }

    /**
     * Handles the specified {@link FirebaseMessagingException}
     * 
     * @param subscriptionStore The subscription store
     * @param contextID The context identifier
     * @param token The registration id, i.e. the token
     * @param e the exception
     */
    private void handleException(PushNotification notification, List<String> registrationIDs, FirebaseMessagingException e) {
        switch (e.getErrorCode()) {
            case ABORTED:
                LOG.warn("Push message could not be sent because the request was aborted.");
                return;
            case ALREADY_EXISTS:
                LOG.warn("Push message could not be sent because the resource the client tried to created already exists.");
                return;
            case CANCELLED:
                LOG.warn("Push message could not be sent because the request was cancelled.");
                return;
            case CONFLICT:
                LOG.warn("Push message could not be sent because there was a concurrency conflict.");
                return;
            case DATA_LOSS:
                LOG.warn("Push message could not be sent because there was an unrecoverable data loss or data corruption.");
                return;
            case DEADLINE_EXCEEDED:
                LOG.warn("Push message could not be sent because the request's deadline was exceeded.");
                return;
            case FAILED_PRECONDITION:
                LOG.warn("Push message could not be sent because a precondition failed.");
                return;
            case INTERNAL:
                LOG.warn("Push message could not be sent because an internal error was occurred in the FCM servers.");
                return;
            case INVALID_ARGUMENT:
                LOG.warn("Push message could not be sent because an invalid argument was provided for registrationIds '{}' : '{}'", registrationIDs, e.getMessage());
                return;
            case NOT_FOUND:
                LOG.warn("Received error '{}' when sending push message to '{}', removing registration ID.", e.getErrorCode(), notification.getSourceToken());
                registrationIDs.forEach(r -> removeRegistrations(notification, r));
                return;
            case OUT_OF_RANGE:
                LOG.warn("Push message could not be sent because an invalid range was specified.");
                return;
            case PERMISSION_DENIED:
                LOG.warn("Push message could not be sent due to insufficient permissions. This can happen because the OAuth token does not have the right scopes, the client doesn't have permission, or the API has not been enabled for the client project.");
                return;
            case RESOURCE_EXHAUSTED:
                LOG.warn("Push message could not be sent due to the resource either being out of quota or rate limited.");
                return;
            case UNAUTHENTICATED:
                LOG.warn("Push message could not be sent because the request was not authenticated due to missing, invalid or expired OAuth token.");
                return;
            case UNAVAILABLE:
                LOG.warn("Push message could not be sent because the FCM servers were not available.");
                return;
            case UNKNOWN:
                LOG.warn("Push message could not be sent because an unknown error was occurred in the FCM servers.");
                return;
            default:
                LOG.warn("", e);
        }
    }

    /**
     * Removes the registrations for the specified notification
     * 
     * @param notification The notification
     * @param registrationID the registration id
     */
    private void removeRegistrations(PushNotification notification, String registrationID) {
        try {
            //@formatter:off
            PushSubscriptionDescription description = new PushSubscriptionDescription()
                .setContextId(notification.getContextId())
                .setToken(DefaultToken.tokenFor(registrationID))
                .setTransportId(ID)
                .setUserId(notification.getUserId());
            //@formatter:on

            boolean success = pushSubscriptionRegistry.unregisterSubscription(description, new PushSubscriptionRestrictions());
            if (success) {
                LOG.info("Successfully removed registration ID {}.", registrationID);
                return;
            }
        } catch (OXException e) {
            LOG.error("Error removing subscription", e);
        }
        LOG.warn("Registration ID {} not removed.", registrationID);
    }
}
