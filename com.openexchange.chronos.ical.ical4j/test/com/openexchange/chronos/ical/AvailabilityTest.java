/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.ical;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.openexchange.chronos.Availability;
import com.openexchange.chronos.Available;

/**
 * {@link AvailabilityTest}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class AvailabilityTest extends ICalTest {

    /**
     * Test the availability of a user, always available Monday through Friday,
     * 9:00 am to 5:00 pm in the America/Montreal time zone.
     */
    @Test
    public void testImportSingleVAvailability() throws Exception {
        //@formatter:off
        String iCal = "BEGIN:VCALENDAR\n" +
            "BEGIN:VAVAILABILITY\n" +
            "ORGANIZER:mailto:bernard@example.com\n" +
            "UID:0428C7D2-688E-4D2E-AC52-CD112E2469DF\n" +
            "DTSTAMP:20111005T133225Z\n" +
            "BEGIN:AVAILABLE\n" +
            "UID:34EDA59B-6BB1-4E94-A66C-64999089C0AF\n" +
            "SUMMARY:Monday to Friday from 9:00 to 17:00\n" +
            "DTSTART;TZID=America/Montreal:20111002T090000\n" +
            "DTEND;TZID=America/Montreal:20111002T170000\n" +
            "RRULE:FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR\n" +
            "END:AVAILABLE\n" +
            "END:VAVAILABILITY\n" +
            "END:VCALENDAR\n";
        //@formatter:on

        ImportedCalendar importICal = importICal(iCal);
        assertNotNull(importICal.getAvailability(), "No availability components found");

        Availability availability = importICal.getAvailability();
        assertEquals("mailto:bernard@example.com", availability.getOrganizer().getUri(), "The organizer uri does not match");
        assertEquals("0428C7D2-688E-4D2E-AC52-CD112E2469DF", availability.getUid(), "The uid does not match");

        List<Available> freeSlots = availability.getAvailable();
        assertNotNull(freeSlots, "No 'available' sub-components found");
        assertEquals(1, freeSlots.size(), "Expected 1 'available' sub-component");

        Available freeSlot = freeSlots.get(0);
        assertEquals("Monday to Friday from 9:00 to 17:00", freeSlot.getSummary(), "The summary does not match");
        assertEquals("34EDA59B-6BB1-4E94-A66C-64999089C0AF", freeSlot.getUid(), "The uid does not match");
        assertEquals("FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR", freeSlot.getRecurrenceRule(), "The recurrence rule does not match");
        assertEquals(java.util.TimeZone.getTimeZone("America/Montreal").getRawOffset(), freeSlot.getStartTime().getTimeZone().getRawOffset(), "The start timezone does not match");
        assertEquals(java.util.TimeZone.getTimeZone("America/Montreal").getRawOffset(), freeSlot.getEndTime().getTimeZone().getRawOffset(), "The end timezone does not match");
    }

    /**
     * Test the availability of a user available Monday through Thursday,
     * 9:00 am to 5:00 pm, at the main office, and Friday, 9:00 am to 12:00 pm,
     * in the branch office in the America/Montreal time zone between
     * October 2nd and December 2nd 2011
     */
    @Test
    public void testImportMultipleAvailableBlocks() throws Exception {
        //@formatter:off
        String iCal ="BEGIN:VCALENDAR\n" +
            "BEGIN:VAVAILABILITY\n" +
            "ORGANIZER:mailto:bernard@example.com\n" +
            "UID:84D0F948-7FC6-4C1D-BBF3-BA9827B424B5\n" +
            "DTSTAMP:20111005T133225Z\n" +
            "DTSTART;TZID=America/Montreal:20111002T000000\n" +
            "DTEND;TZID=America/Montreal:20111202T000000\n" +
            "BEGIN:AVAILABLE\n" +
            "UID:7B33093A-7F98-4EED-B381-A5652530F04D\n" +
            "SUMMARY:Monday to Thursday from 9:00 to 17:00\n" +
            "DTSTART;TZID=America/Montreal:20111002T090000\n" +
            "DTEND;TZID=America/Montreal:20111002T170000\n" +
            "RRULE:FREQ=WEEKLY;BYDAY=MO,TU,WE,TH\n" +
            "LOCATION:Main Office\n" +
            "END:AVAILABLE\n" +
            "BEGIN:AVAILABLE\n" +
            "UID:DF39DC9E-D8C3-492F-9101-0434E8FC1896\n" +
            "SUMMARY:Friday from 9:00 to 12:00\n" +
            "DTSTART;TZID=America/Montreal:20111006T090000\n" +
            "DTEND;TZID=America/Montreal:20111006T120000\n" +
            "RRULE:FREQ=WEEKLY\n" +
            "LOCATION:Branch Office\n" +
            "END:AVAILABLE\n" +
            "END:VAVAILABILITY\n" +
            "END:VCALENDAR\n";
        //@formatter:on

        ImportedCalendar importICal = importICal(iCal);
        assertNotNull(importICal.getAvailability(), "No availability components found");

        Availability availability = importICal.getAvailability();
        assertEquals("mailto:bernard@example.com", availability.getOrganizer().getUri(), "The organizer uri does not match");
        assertEquals("84D0F948-7FC6-4C1D-BBF3-BA9827B424B5", availability.getUid(), "The uid does not match");

        List<Available> freeSlots = availability.getAvailable();
        assertNotNull(freeSlots, "No 'available' sub-components found");
        assertEquals(2, freeSlots.size(), "Expected 2 'available' sub-components");

        Available freeSlot = freeSlots.get(0);
        assertEquals("Monday to Thursday from 9:00 to 17:00", freeSlot.getSummary(), "The summary does not match");
        assertEquals("7B33093A-7F98-4EED-B381-A5652530F04D", freeSlot.getUid(), "The uid does not match");
        assertEquals("FREQ=WEEKLY;BYDAY=MO,TU,WE,TH", freeSlot.getRecurrenceRule(), "The recurrence rule does not match");
        assertEquals("Main Office", freeSlot.getLocation(), "The location does not match");

        freeSlot = freeSlots.get(1);
        assertEquals("Friday from 9:00 to 12:00", freeSlot.getSummary(), "The summary does not match");
        assertEquals("DF39DC9E-D8C3-492F-9101-0434E8FC1896", freeSlot.getUid(), "The uid does not match");
        assertEquals("FREQ=WEEKLY", freeSlot.getRecurrenceRule(), "The recurrence rule does not match");
        assertEquals("Branch Office", freeSlot.getLocation(), "The location does not match");
    }

    /**
     * <p>Tests the availability of a user with multiple VAvailability blocks.</p>
     *
     * <p>
     * The base availability is from Monday through Friday, 8:00 am to 6:00 pm each day with a "VAVAILABILITY"
     * with default "PRIORITY" (there is no "DTEND" property so that this availability is unbounded). For the
     * week the calendar user is working in Denver (October 23rd through October 30th), the availability is
     * represented with a "VAVAILABILITY" component with priority 1, which overrides the base availability.
     * There is also a two hour meeting starting at 12:00 pm (in the America/Denver time zone).
     * </p>
     */
    public void testImportMultipleAvailabilityBlocks() throws Exception {
        //@formatter:off
        String iCal = "BEGIN:VCALENDAR\n" +
            "BEGIN:VAVAILABILITY\n" +
            "ORGANIZER:mailto:bernard@example.com\n" +
            "UID:BE082249-7BDD-4FE0-BDBA-DE6598C32FC9\n" +
            "DTSTAMP:20111005T133225Z\n" +
            "DTSTART;TZID=America/Montreal:20111002T000000\n" +
            "DTEND;TZID=America/Montreal:20111023T030000\n" +
            "BEGIN:AVAILABLE\n" +
            "UID:54602321-CEDB-4620-9099-757583263981\n" +
            "SUMMARY:Monday to Friday from 9:00 to 17:00\n" +
            "DTSTART;TZID=America/Montreal:20111002T090000\n" +
            "DTEND;TZID=America/Montreal:20111002T170000\n" +
            "RRULE:FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR\n" +
            "LOCATION:Montreal\n" +
            "END:AVAILABLE\n" +
            "END:VAVAILABILITY\n" +
            "BEGIN:VAVAILABILITY\n" +
            "ORGANIZER:mailto:bernard@example.com\n" +
            "UID:A1FF55E3-555C-433A-8548-BF4864B5621E\n" +
            "DTSTAMP:20111005T133225Z\n" +
            "DTSTART;TZID=America/Denver:20111023T000000\n" +
            "DTEND;TZID=America/Denver:20111030T000000\n" +
            "BEGIN:AVAILABLE\n" +
            "UID:57DD4AAF-3835-46B5-8A39-B3B253157F01\n" +
            "SUMMARY:Monday to Friday from 9:00 to 17:00\n" +
            "DTSTART;TZID=America/Denver:20111023T090000\n" +
            "DTEND;TZID=America/Denver:20111023T170000\n" +
            "RRULE:FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR\n" +
            "LOCATION:Denver\n" +
            "END:AVAILABLE\n" +
            "END:VAVAILABILITY\n" +
            "BEGIN:VAVAILABILITY\n" +
            "ORGANIZER:mailto:bernard@example.com\n" +
            "UID:1852F9E1-E0AA-4572-B4C4-ED1680A4DA40\n" +
            "DTSTAMP:20111005T133225Z\n" +
            "DTSTART;TZID=America/Montreal:20111030T030000\n" +
            "BEGIN:AVAILABLE\n" +
            "UID:D27C421F-16C2-4ECB-8352-C45CA352C72A\n" +
            "SUMMARY:Monday to Friday from 9:00 to 17:00\n" +
            "DTSTART;TZID=America/Montreal:20111030T090000\n" +
            "DTEND;TZID=America/Montreal:20111030T170000\n" +
            "RRULE:FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR\n" +
            "LOCATION:Montreal\n" +
            "END:AVAILABLE\n" +
            "END:VAVAILABILITY\n" +
            "END:VCALENDAR\n";
        //@formatter:on

        ImportedCalendar importICal = importICal(iCal);
        assertNotNull(importICal.getAvailability(), "No availability components found");

        // Availability 1
        Availability availability = importICal.getAvailability();
        assertEquals("mailto:bernard@example.com", availability.getOrganizer().getUri(), "The organizer uri does not match");
        assertEquals("BE082249-7BDD-4FE0-BDBA-DE6598C32FC9", availability.getUid(), "The uid does not match");
        assertEquals(java.util.TimeZone.getTimeZone("America/Montreal"), availability.getStartTime().getTimeZone(), "The start timezone does not match");
        assertEquals(java.util.TimeZone.getTimeZone("America/Montreal"), availability.getEndTime().getTimeZone(), "The end timezone does not match");

        List<Available> freeSlots = availability.getAvailable();
        assertNotNull(freeSlots, "No 'available' sub-components found");
        assertEquals(1, freeSlots.size(), "Expected 1 'available' sub-component");

        Available freeSlot = freeSlots.get(0);
        assertEquals("Monday to Friday from 9:00 to 17:00", freeSlot.getSummary(), "The summary does not match");
        assertEquals("54602321-CEDB-4620-9099-757583263981", freeSlot.getUid(), "The uid does not match");
        assertEquals("FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR", freeSlot.getRecurrenceRule(), "The recurrence rule does not match");
        assertEquals("Montreal", freeSlot.getLocation(), "The location does not match");
        assertEquals(java.util.TimeZone.getTimeZone("America/Montreal"), freeSlot.getStartTime().getTimeZone(), "The start timezone does not match");
        assertEquals(java.util.TimeZone.getTimeZone("America/Montreal"), freeSlot.getEndTime().getTimeZone(), "The end timezone does not match");

        // Availability 2
        //availability = importICal.getAvailability().get(1);
        assertEquals("mailto:bernard@example.com", availability.getOrganizer().getUri(), "The organizer uri does not match");
        assertEquals("A1FF55E3-555C-433A-8548-BF4864B5621E", availability.getUid(), "The uid does not match");
        assertEquals(java.util.TimeZone.getTimeZone("America/Denver"), availability.getStartTime().getTimeZone(), "The start timezone does not match");
        assertEquals(java.util.TimeZone.getTimeZone("America/Denver"), availability.getEndTime().getTimeZone(), "The end timezone does not match");

        freeSlots = availability.getAvailable();
        assertNotNull(freeSlots, "No 'available' sub-components found");
        assertEquals(1, freeSlots.size(), "Expected 1 'available' sub-component");

        freeSlot = freeSlots.get(0);
        assertEquals("Monday to Friday from 9:00 to 17:00", freeSlot.getSummary(), "The summary does not match");
        assertEquals("57DD4AAF-3835-46B5-8A39-B3B253157F01", freeSlot.getUid(), "The uid does not match");
        assertEquals("FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR", freeSlot.getRecurrenceRule(), "The recurrence rule does not match");
        assertEquals("Denver", freeSlot.getLocation(), "The location does not match");
        assertEquals(java.util.TimeZone.getTimeZone("America/Denver"), freeSlot.getStartTime().getTimeZone(), "The start timezone does not match");
        assertEquals(java.util.TimeZone.getTimeZone("America/Denver"), freeSlot.getEndTime().getTimeZone(), "The end timezone does not match");

        // Availability 3
        //availability = importICal.getAvailability().get(2);
        assertEquals("The organizer uri does not match", "mailto:bernard@example.com", availability.getOrganizer().getUri());
        assertEquals("The uid does not match", "1852F9E1-E0AA-4572-B4C4-ED1680A4DA40", availability.getUid());
        assertEquals(java.util.TimeZone.getTimeZone("America/Montreal"), availability.getStartTime().getTimeZone(), "The start timezone does not match");
        assertNull(availability.getEndTime(), "The end timezone does not match");

        freeSlots = availability.getAvailable();
        assertNotNull(freeSlots, "No 'available' sub-components found");
        assertEquals(1, freeSlots.size(), "Expected 1 'available' sub-component");

        freeSlot = freeSlots.get(0);
        assertEquals("Monday to Friday from 9:00 to 17:00", freeSlot.getSummary(), "The summary does not match");
        assertEquals("D27C421F-16C2-4ECB-8352-C45CA352C72A", freeSlot.getUid(), "The uid does not match");
        assertEquals("FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR", freeSlot.getRecurrenceRule(), "The recurrence rule does not match");
        assertEquals("Montreal", freeSlot.getLocation(), "The location does not match");
        assertEquals(java.util.TimeZone.getTimeZone("America/Montreal"), freeSlot.getStartTime().getTimeZone(), "The start timezone does not match");
        assertEquals(java.util.TimeZone.getTimeZone("America/Montreal"), freeSlot.getEndTime().getTimeZone(), "The end timezone does not match");
    }
}
