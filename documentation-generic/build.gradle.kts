install {
    target("configDocs") {
        from("config") {
            include("*.yml")
            include("*.yaml")
            exclude("template.yml")
        }
        into(prefixResolve("documentation/etc"))
    }
}
