feature_name: DAV
properties:
    - key: com.openexchange.dav.prefixPath
      description: |
         Configures a static prefix for *DAV-servlets. Defaults to "/servlet/dav" for backwards compatibility.
         
         The prefix will be considered when the *DAV servlets are registered, as well when constructing paths to 
         *DAV resources and collections that are indicated to clients. The value should be set in accordance with 
         "com.openexchange.dav.proxyPrefixPath" in case the load balancer / proxy in front of the application server 
         forwards or rewrites client requests to a certain path. 
         
         Otherwise, when no special handling for *DAV requests is configured on the proxy-side, this value may also 
         be set to an arbitrary value like "/dav". 
      defaultValue: /servlet/dav/
      version: 7.10.3
      reloadable: false
      configcascadeAware: false
      related: com.openexchange.dav.proxyPrefixPath
      file:
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV"]
    - key: com.openexchange.dav.proxyPrefixPath
      description: |
         Configures the prefix path that is used by the proxy / load balancer when forwarding requests to this 
         application server. Defaults to "/servlet/dav" for backwards compatibility. 
         
         For example, if a ProxyPass directive like the following is configured: 
            ProxyPass / balancer://oxserver-sync/servlet/dav/
         
         Or, if a RewriteRule has been set up to modify incoming queries like this:
            RewriteRule (.*)  http://localhost:8009/servlet/dav$1
         
         Then, the value "/servlet/dav" should be specified here. 
         
         The correct value is necessary to indicate the correct paths to *DAV clients in responses, which in turn 
         avoids duplicated prefix paths in case the proxy forwards or rewrites requests to a subpath, too. 
         
         Also, in case no special handling for *DAV requests is configured on the proxy-side, this value should be 
         set "/", meaning that client requests will be routed as-is to the application server.
      defaultValue: /servlet/dav/
      version: 7.10.3
      reloadable: false
      configcascadeAware: false
      related: com.openexchange.dav.prefixPath
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV"]
    - key: com.openexchange.dav.useragent.mac_calendar
      description: Defines a pattern to match certain user agent strings to calendar application on Mac OS
      defaultValue: (?=.*(?:Mac OS|macOS|Mac\\+OS))(?=.*(?:CalendarStore|CalendarAgent|dataaccessd))
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.mac_contacts
      description: Defines a pattern to match certain user agent strings to the contacts application on MacOS
      defaultValue: (?:Mac OS|macOS|Mac_OS).*AddressBook.*
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.ios
      description: Defines a pattern to match certain user agent strings to the iOS platform
      defaultValue: iOS.*dataaccessd(?!.*Android)(?<!Android)
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.ios_reminders
      description: Defines a pattern to match certain user agent strings to the reminder app on iOS
      defaultValue: iOS.*remindd(?!.*Android)(?<!Android)
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.thunderbird_lightning
      description: Defines a pattern to match certain user agent strings to the Thunderbird lighting plugin
      defaultValue: (?=.*Thunderbird).*Mozilla
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.thunderbird_cardbook
      description: Defines a pattern to match certain user agent strings to the Thunderbird contacts plugin
      defaultValue: (?=.*Thunderbird).*CardBook
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.em_client
      description: Defines a pattern to match certain user agent strings to the eMClient
      defaultValue: (?:eM(\\s)?Client)
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.ox_sync
      description: Defines a pattern to match certain user agent strings to the OX Sync App for Android
      defaultValue: com\\.openexchange\\.mobile\\.syncapp\\.enterprise
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.caldav_sync
      description: Defines a pattern to match certain user agent strings for the CalDAV Sync on Android
      defaultValue: org\\.dmfs\\.caldav\\.sync
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.carddav_sync
      description: Defines a pattern to match certain user agent strings for the CardDAV Sync on Android
      defaultValue: org\\.dmfs\\.carddav\\.sync
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.smooth_sync
      description: Defines a pattern to match certain user agent strings to the Smooth Sync App on Android
      defaultValue: SmoothSync)
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.davdroid
      description: Defines a pattern to match certain user agent strings to the DAVdroid App on Android
      defaultValue: DAVdroid
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.davx5
      description: Defines a pattern to match certain user agent strings to the DAVx5 App on Android
      defaultValue: DAVx5
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.outlook_caldav_synchronizer
      description: Defines a pattern to match certain user agent strings to the Outlook Synchronizer
      defaultValue: CalDavSynchronizer
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.windows_phone
      description: Defines a pattern to match certain user agent strings to the Windows Phone platform
      defaultValue: MSFT-WP
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
    - key: com.openexchange.dav.useragent.windows
      description: Defines a pattern to match certain user agent strings to the Windows platform
      defaultValue: MSFT-WIN
      reloadable: true
      configcascadeAware: false
      related:
      file: dav.properties
      packageName: open-xchange-dav
      tags: ["CalDAV", "CardDAV", "DAV", "User Agent"]
      
