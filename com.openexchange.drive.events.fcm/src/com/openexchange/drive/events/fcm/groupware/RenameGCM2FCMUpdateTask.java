/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.drive.events.fcm.groupware;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.tools.update.Tools.tableExists;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.openexchange.database.Databases;
import com.openexchange.database.Databases.ConnectionStatus;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.update.PerformParameters;
import com.openexchange.groupware.update.UpdateExceptionCodes;
import com.openexchange.groupware.update.UpdateTaskAdapter;

/**
 * {@link RenameGCM2FCMUpdateTask} - Renames service identifiers in 'driveEventSubscriptions' from 'gcm' to 'fcm'.
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RenameGCM2FCMUpdateTask extends UpdateTaskAdapter {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(RenameGCM2FCMUpdateTask.class);

    /**
     * Initialises a new {@link RenameGCM2FCMUpdateTask}.
     */
    public RenameGCM2FCMUpdateTask() {
        super();
    }

    @Override
    public void perform(PerformParameters params) throws OXException {
        ConnectionStatus status = ConnectionStatus.INITIALISED;
        Connection connection = params.getConnection();
        try {
            if (!tableExists(connection, "driveEventSubscriptions")) {
                return;
            }

            connection.setAutoCommit(false);
            status = ConnectionStatus.FAILED; // pessimistic

            int updated = changeRows(connection);

            connection.commit();
            status = ConnectionStatus.SUCCEEDED;

            LOG.info("Renamed service identifiers in 'driveEventSubscriptions' from 'gcm' to 'fcm'. Total rows affected: {}", I(updated));
        } catch (SQLException e) {
            throw UpdateExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw UpdateExceptionCodes.OTHER_PROBLEM.create(e, e.getMessage());
        } finally {
            if (status != ConnectionStatus.INITIALISED) {
                if (status == ConnectionStatus.FAILED) {
                    Databases.rollback(connection);
                }
                Databases.autocommit(connection);
            }
        }
    }

    /**
     * Changes the service identifiers in 'driveEventSubscriptions' from 'gcm' to 'fcm'
     *
     * @param connection The connection to use
     * @return The number of rows that were changed
     * @throws SQLException If SQL statement fails
     */
    private static int changeRows(Connection connection) throws SQLException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("UPDATE driveEventSubscriptions SET service='fcm' WHERE service='gcm'");
            return statement.executeUpdate();
        } finally {
            Databases.closeSQLStuff(statement);
        }
    }

    @Override
    public String[] getDependencies() {
        return new String[] { "com.openexchange.drive.events.subscribe.rdb.DriveEventSubscriptionsCreateTableTask" };
    }

}
