/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
* 
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.drive.events.fcm.internal;

import static com.openexchange.java.Autoboxing.I;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.drive.events.DriveContentChange;
import com.openexchange.drive.events.DriveEvent;
import com.openexchange.drive.events.DriveEventPublisher;
import com.openexchange.drive.events.subscribe.DriveSubscriptionStore;
import com.openexchange.drive.events.subscribe.Subscription;
import com.openexchange.drive.events.subscribe.SubscriptionMode;
import com.openexchange.drive.events.subscribe.SubscriptionTransport;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.push.clients.PushClientProvider;

/**
 * {@link FCMDriveEventPublisher}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class FCMDriveEventPublisher implements DriveEventPublisher {

    private static final Logger LOG = LoggerFactory.getLogger(FCMDriveEventPublisher.class);
    private static final String SERVICE_ID = SubscriptionTransport.FCM.name().toLowerCase();
    private static final String[] SERVICE_IDS = new String[] { SERVICE_ID };

    private final PushClientProvider<FirebaseMessaging> provider;
    private final DriveSubscriptionStore subscriptionStore;
    private final LeanConfigurationService leanConfigService;

    /**
     * Initialises a new {@link FCMDriveEventPublisher}.
     * 
     * @param provider the push client provider
     * @param subscriptionStore The drive subscription store
     * @param leanConfigService The lean configuration service
     */
    public FCMDriveEventPublisher(PushClientProvider<FirebaseMessaging> provider, DriveSubscriptionStore subscriptionStore, LeanConfigurationService leanConfigService) {
        super();
        this.provider = provider;
        this.subscriptionStore = subscriptionStore;
        this.leanConfigService = leanConfigService;
    }

    @Override
    public void publish(DriveEvent event) {
        for (Entry<String, List<Subscription>> entry : getSubscriptionsPerClientId(event).entrySet()) {
            processEntry(entry, event);
        }
    }

    @Override
    public boolean isLocalOnly() {
        return true;
    }

    /**
     * Processes the specified subscription entry
     * 
     * @param entry the entry
     * @param event The drive event
     */
    private void processEntry(Entry<String, List<Subscription>> entry, DriveEvent event) {
        //@formatter:off
        provider.optClient(entry.getKey()).ifPresentOrElse(
            sender -> entry.getValue().forEach(subscription -> processSubscription(subscription, sender, event)),
            () -> LOG.debug("No push client configured for push via FCM with client id '{}', skipping notifications.", entry.getKey())
        );
        //@formatter:on
    }

    /**
     * Generates and sends a push message for the specified subscription
     * 
     * @param subscription The subscription
     * @param sender The sender instance
     * @param event The event
     * @param subscriptionStore The subscription store
     */
    private void processSubscription(Subscription subscription, FirebaseMessaging sender, DriveEvent event) {
        try {
            Message message = createMessage(event, subscription);
            String messageId = sender.send(message);
            LOG.debug("Successfully sent push message with id: '{}'", messageId != null ? messageId : "");
        } catch (FirebaseMessagingException e) {
            LOG.warn("Error publishing drive event", e);
            handleException(subscriptionStore, event.getContextID(), subscription.getToken(), e);
        }
    }

    /**
     * Returns a map with all subscriptions per client id
     * 
     * @param subscriptions The subscriptions as a list
     * @param event The drive event
     * @return The map with all subscriptions per client id
     */
    private Map<String, List<Subscription>> getSubscriptionsPerClientId(DriveEvent event) {
        List<Subscription> subscriptions = getSubscriptions(event);
        if (subscriptions.isEmpty()) {
            return Collections.emptyMap();
        }
        if (null == leanConfigService) {
            LOG.warn("Unable to get configuration service to determine push client configuration.");
            return Collections.emptyMap();
        }
        Map<String, List<Subscription>> subscriptionsPerClientId = new HashMap<>();
        subscriptions.forEach(subscription -> processSubscription(subscription, event, subscriptionsPerClientId));
        return subscriptionsPerClientId;
    }

    /**
     * Processes the specified subscription and adds it to the specified map
     * 
     * @param subscription The subscription to process
     * @param event The drive event for the subscription
     * @param subscriptionsPerClientId the map to put the subscription and separate it by client id
     */
    private void processSubscription(Subscription subscription, DriveEvent event, Map<String, List<Subscription>> subscriptionsPerClientId) {
        // Check if applicable & enabled for user
        if (null != event.getPushTokenReference() && subscription.matches(event.getPushTokenReference())) {
            LOG.trace("Skipping notification due to matching push token for subscription: {}", subscription);
            return;
        }
        int userId = subscription.getUserID();
        int contextId = subscription.getContextID();
        if (!event.getFolderIDs(userId).contains(subscription.getRootFolderID())) {
            LOG.trace("Skipping notification due to mismatching folder for subscription: {}", subscription);
            return;
        }
        if (!leanConfigService.getBooleanProperty(userId, contextId, DriveEventsFCMProperty.ENABLED)) {
            LOG.trace("Push via FCM is disabled for user {} in context {}, skipping notification.", I(userId), I(contextId));
            return;
        }
        // Get configured push client id
        String clientId = leanConfigService.getProperty(userId, contextId, DriveEventsFCMProperty.CLIENT_ID);
        if (Strings.isEmpty(clientId)) {
            LOG.debug("No push client configured for push via FCM for subscription {}, skipping notification.", subscription);
            return;
        }
        LOG.trace("Using configured client id {} for push via FCM for user {} in context {}.", clientId, I(userId), I(contextId));
        com.openexchange.tools.arrays.Collections.put(subscriptionsPerClientId, clientId, subscription);
    }

    /**
     * Returns a list with all subscriptions for the specified drive event
     * 
     * @param event The drive event
     * @return a list with all relevant subscriptions or an empty list if no such subscriptions exist, or an error is occurred
     */
    private List<Subscription> getSubscriptions(DriveEvent event) {
        try {
            return subscriptionStore.getSubscriptions(event.getContextID(), SERVICE_IDS, event.getUserIDs());
        } catch (OXException e) {
            LOG.error("Unable to get subscriptions for service '{}'", SERVICE_ID, e);
            return Collections.emptyList();
        }
    }

    /**
     * Creates the message to send
     * 
     * @param event The drive event
     * @param subscription The subscription
     * @return The message
     */
    private static Message createMessage(DriveEvent event, Subscription subscription) {
        AndroidConfig androidConfig = AndroidConfig.builder().setCollapseKey("TRIGGER_SYNC").build();
        //@formatter:off
        Message.Builder builder = Message.builder().setToken(subscription.getToken()).
                                    setAndroidConfig(androidConfig).
                                    putData("root", subscription.getRootFolderID()).
                                    putData("action", "sync");
        //@formatter:on

        if (!event.isContentChangesOnly() || !SubscriptionMode.SEPARATE.equals(subscription.getMode())) {
            return builder.build();
        }

        IntStream.range(0, event.getContentChanges().size()).filter(index -> {
            DriveContentChange contentChange = event.getContentChanges().get(index);
            return contentChange.isSubfolderOf(subscription.getUserID(), subscription.getRootFolderID());
        }).forEach(index -> {
            DriveContentChange contentChange = event.getContentChanges().get(index);
            builder.putData("path_" + index, contentChange.getPath(subscription.getUserID(), subscription.getRootFolderID()));
        });
        return builder.build();
    }

    /**
     * Removes the registration with the specified registration id
     * 
     * @param subscriptionStore The subscription store
     * @param contextID the context identifier
     * @param registrationID the registration identifier
     */
    private static void removeRegistrations(DriveSubscriptionStore subscriptionStore, int contextID, String registrationID) {
        try {
            if (0 < subscriptionStore.removeSubscriptions(contextID, SERVICE_ID, registrationID)) {
                LOG.info("Successfully removed registration ID '{}'.", registrationID);
            } else {
                LOG.warn("Registration ID '{}' not removed.", registrationID);
            }
        } catch (OXException e) {
            LOG.error("Error removing registrations", e);
        }
    }

    /**
     * Handles the specified {@link FirebaseMessagingException}
     * 
     * @param subscriptionStore The subscription store
     * @param contextID The context identifier
     * @param token The registration id, i.e. the token
     * @param e the exception
     */
    private void handleException(DriveSubscriptionStore subscriptionStore, int contextID, String token, FirebaseMessagingException e) {
        switch (e.getErrorCode()) {
            case ABORTED:
                LOG.warn("Push message could not be sent because the request was aborted.");
                return;
            case ALREADY_EXISTS:
                LOG.warn("Push message could not be sent because the resource the client tried to created already exists.");
                return;
            case CANCELLED:
                LOG.warn("Push message could not be sent because the request was cancelled.");
                return;
            case CONFLICT:
                LOG.warn("Push message could not be sent because there was a concurrency conflict.");
                return;
            case DATA_LOSS:
                LOG.warn("Push message could not be sent because there was an unrecoverable data loss or data corruption.");
                return;
            case DEADLINE_EXCEEDED:
                LOG.warn("Push message could not be sent because the request's deadline was exceeded.");
                return;
            case FAILED_PRECONDITION:
                LOG.warn("Push message could not be sent because a precondition failed.");
                return;
            case INTERNAL:
                LOG.warn("Push message could not be sent because an internal error was occurred in the FCM servers.");
                return;
            case INVALID_ARGUMENT:
                LOG.warn("Push message could not be sent because an invalid argument was provided.");
                return;
            case NOT_FOUND:
                LOG.warn("Received error '{}' when sending push message to '{}', removing registration ID.", e.getErrorCode(), token);
                removeRegistrations(subscriptionStore, contextID, token);
                return;
            case OUT_OF_RANGE:
                LOG.warn("Push message could not be sent because an invalid range was specified.");
                return;
            case PERMISSION_DENIED:
                LOG.warn("Push message could not be sent due to insufficient permissions. This can happen because the OAuth token does not have the right scopes, the client doesn't have permission, or the API has not been enabled for the client project.");
                return;
            case RESOURCE_EXHAUSTED:
                LOG.warn("Push message could not be sent due to the resource either being out of quota or rate limited.");
                return;
            case UNAUTHENTICATED:
                LOG.warn("Push message could not be sent because the request was not authenticated due to missing, invalid or expired OAuth token.");
                return;
            case UNAVAILABLE:
                LOG.warn("Push message could not be sent because the FCM servers were not available.");
                return;
            case UNKNOWN:
                LOG.warn("Push message could not be sent because an unknown error was occurred in the FCM servers.");
                return;
            default:
                LOG.warn("", e);
        }
    }
}
