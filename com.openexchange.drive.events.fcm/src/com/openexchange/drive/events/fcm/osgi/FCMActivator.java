/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.drive.events.fcm.osgi;

import com.google.firebase.messaging.FirebaseMessaging;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.drive.events.DriveEventService;
import com.openexchange.drive.events.fcm.groupware.RenameGCM2FCMUpdateTask;
import com.openexchange.drive.events.fcm.internal.FCMDriveEventPublisher;
import com.openexchange.drive.events.subscribe.DriveSubscriptionStore;
import com.openexchange.groupware.update.DefaultUpdateTaskProviderService;
import com.openexchange.groupware.update.UpdateTaskProviderService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.push.clients.PushClientProviderFactory;

/**
 * {@link FCMActivator}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class FCMActivator extends HousekeepingActivator {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(FCMActivator.class);

    /**
     * Initializes a new {@link FCMActivator}.
     */
    public FCMActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { DriveEventService.class, DriveSubscriptionStore.class, LeanConfigurationService.class, PushClientProviderFactory.class };
    }

    @Override
    protected void startBundle() throws Exception {
        LOG.info("Starting bundle: {}", context.getBundle().getSymbolicName());

        registerService(UpdateTaskProviderService.class, new DefaultUpdateTaskProviderService(new RenameGCM2FCMUpdateTask()));

        //@formatter:off
        getServiceSafe(DriveEventService.class).registerPublisher(new FCMDriveEventPublisher(
                        getServiceSafe(PushClientProviderFactory.class).createProvider(FirebaseMessaging.class),
                        getServiceSafe(DriveSubscriptionStore.class),
                        getServiceSafe(LeanConfigurationService.class)));
        //@formatter:on
    }

    @Override
    protected void stopBundle() throws Exception {
        LOG.info("Stopping bundle: {}", context.getBundle().getSymbolicName());
        super.stopBundle();
    }
}
