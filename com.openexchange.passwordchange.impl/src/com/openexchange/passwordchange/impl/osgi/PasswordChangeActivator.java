/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.passwordchange.impl.osgi;

import java.util.Dictionary;
import java.util.Hashtable;
import org.osgi.service.event.EventAdmin;
import com.openexchange.authentication.AuthenticationServiceRegistry;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;
import com.openexchange.capabilities.CapabilityChecker;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.guest.GuestService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.password.mechanism.PasswordMechRegistry;
import com.openexchange.passwordchange.PasswordChangeRegistry;
import com.openexchange.passwordchange.PasswordChangeService;
import com.openexchange.passwordchange.impl.EditPasswordCapabilityChecker;
import com.openexchange.passwordchange.impl.PasswordChangeRegistryImpl;
import com.openexchange.passwordchange.impl.database.DatabasePasswordChangeService;
import com.openexchange.passwordchange.impl.script.ScriptPasswordChangeService;
import com.openexchange.passwordchange.impl.webhook.WebhookPasswordChangeService;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.user.UserService;
import com.openexchange.userconf.UserPermissionService;

/**
 * {@link PasswordChangeActivator}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class PasswordChangeActivator extends HousekeepingActivator {

    /**
     * Initializes a new {@link PasswordChangeActivator}
     */
    public PasswordChangeActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] { UserPermissionService.class, UserService.class, GuestService.class, PasswordMechRegistry.class, AuthenticationServiceRegistry.class, //
            SessiondService.class, EventAdmin.class, LeanConfigurationService.class, CapabilityService.class, InvalidationCacheService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        /*
         * Register service tracker and register registry
         */
        PasswordChangeRegistryImpl registry = new PasswordChangeRegistryImpl(context);
        rememberTracker(registry);
        openTrackers();
        registerService(PasswordChangeRegistry.class, registry);
        /*
         * Register core implementations
         */
        registerService(PasswordChangeService.class, new DatabasePasswordChangeService(this));
        registerService(PasswordChangeService.class, new ScriptPasswordChangeService(this));
        registerService(PasswordChangeService.class, new WebhookPasswordChangeService(this));
        /*
         * Register capability check
         */
        {
            Dictionary<String, Object> properties = new Hashtable<String, Object>(1);
            properties.put(CapabilityChecker.PROPERTY_CAPABILITIES, EditPasswordCapabilityChecker.EDIT_PASSWORD_CAP);
            registerService(CapabilityChecker.class, new EditPasswordCapabilityChecker(this, registry), properties);

            getServiceSafe(CapabilityService.class).declareCapability(EditPasswordCapabilityChecker.EDIT_PASSWORD_CAP);
        }
    }

}
