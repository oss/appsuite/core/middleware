/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.passwordchange.impl.webhook;

import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;
import com.openexchange.exception.OXExceptionStrings;

/**
 * {@link WebhookPasswordChangeExceptionCodes}
 *
 * @author <a href="mailto:philipp.schumacher@open-xchange.com">Philipp Schumacher</a>
 */
public enum WebhookPasswordChangeExceptionCodes implements DisplayableOXExceptionCode {

    /**
     * The password change failed: %1$s
     */
    PASSWORD_CHANGE_FAILED_WITH_REASON("The password change failed: %1$s", CATEGORY_USER_INPUT, WebhookPasswordChangeExceptionMessage.PASSWORD_CHANGE_FAILED_WITH_REASON_MSG),

    /**
     * The password change failed.
     */
    PASSWORD_CHANGE_FAILED_WITHOUT_REASON("The password change failed.", CATEGORY_USER_INPUT, WebhookPasswordChangeExceptionMessage.PASSWORD_CHANGE_FAILED_MSG),

    /**
     * Unknown response from the external password change service (%1$s).
     */
    UNKNOWN_RESPONSE("Unknown response from the external password change service (%1$s).", CATEGORY_USER_INPUT, WebhookPasswordChangeExceptionMessage.PASSWORD_CHANGE_FAILED_MSG),

    /**
     * No webhook endpoint specified. Please set 'com.openexchange.passwordchange.webhook.endpoint' configuration option.
     */
    NO_ENDPOINT_SPECIFIED("No webhook endpoint specified. Please set 'com.openexchange.passwordchange.webhook.endpoint' configuration option.", CATEGORY_ERROR),

    /**
     * The specified webhook endpoint '%1$s' is not valid.
     */
    INVALID_ENDPOINT("The specified webhook endpoint '%1$s' is not valid.", CATEGORY_ERROR),

    /**
     * The specified webhook endpoint '%1$s' is not secured by TLS. Please use the URI scheme 'https' in 'com.openexchange.passwordchange.webhook.endpoint'.
     */
    UNSECURED_ENDPOINT("The specified webhook endpoint '%1$s' is not secured by TLS. Please use the URI scheme 'https' in 'com.openexchange.passwordchange.webhook.endpoint'.", CATEGORY_ERROR),

    /**
     * Authentication failed: %1$s.
     */
    AUTHENTICATION_FAILED("Authentication failed: %1$s.", CATEGORY_ERROR),

    /**
     * An I/O error occurred: %1$s
     */
    IO_ERROR("An I/O error occurred: %1$s", CATEGORY_USER_INPUT, WebhookPasswordChangeExceptionMessage.PASSWORD_CHANGE_FAILED_MSG),
    ;

    private static final String PREFIX = "PSW";

    private final String message;
    private final Category category;
    private final String displayMessage;

    /**
     * Initializes a new {@link DataExportExceptionCode}.
     *
     * @param message
     * @param category
     */
    private WebhookPasswordChangeExceptionCodes(final String message, final Category category) {
        this(message, category, null);
    }

    /**
     * Initializes a new {@link DataExportExceptionCode}.
     *
     * @param message
     * @param category
     * @param displayMessage
     */
    private WebhookPasswordChangeExceptionCodes(final String message, final Category category, final String displayMessage) {
        this.message = message;
        this.category = category;
        this.displayMessage = displayMessage != null ? displayMessage : OXExceptionStrings.MESSAGE;
    }

    @Override
    public String getPrefix() {
        return PREFIX;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public int getNumber() {
        return ordinal() + 1;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(final OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    @Override
    public String getDisplayMessage() {
        return this.displayMessage;
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(final Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(final Throwable cause, final Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }
}
