/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.passwordchange.impl.webhook;

import static com.openexchange.java.Autoboxing.I;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.passwordchange.PasswordChangeEvent;
import com.openexchange.passwordchange.common.ConfigAwarePasswordChangeService;
import com.openexchange.rest.client.httpclient.HttpClientService;
import com.openexchange.rest.client.httpclient.HttpClients;
import com.openexchange.rest.client.httpclient.ManagedHttpClient;
import com.openexchange.server.ServiceLookup;
import com.openexchange.user.User;

public class WebhookPasswordChangeService extends ConfigAwarePasswordChangeService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(WebhookPasswordChangeService.class);

    private static final int RANKING = 750;

    private static final String HTTP_CLIENT_ID = "passwordchange";
    private static final String PROVIDER_ID = "webhook";

    private final HttpClientService httpClientService;

    /**
     * Initializes a new {@link WebhookPasswordChangeService}
     *
     * @param services The service lookup
     * @throws OXException If services are missing
     */
    public WebhookPasswordChangeService(ServiceLookup services) throws OXException {
        super(services);
        httpClientService = services.getServiceSafe(HttpClientService.class);
    }

    @Override
    protected String getProviderId() {
        return PROVIDER_ID;
    }

    @Override
    public int getRanking() {
        return RANKING;
    }

    @Override
    protected void update(PasswordChangeEvent event) throws OXException {
        int userId = event.getSession().getUserId();
        int contextId = event.getSession().getContextId();

        String endpoint = configService.getProperty(userId, contextId, WebhookPasswordChangeProperties.ENDPOINT);
        if (Strings.isEmpty(endpoint)) {
            LOG.error(endpoint);
        }
        send(endpoint, userId, contextId, event);
    }

    /**
     * Sends the password change event via POST request to the given webhook URI.
     *
     * @param endpoint The webhook URI
     * @param userId The user id
     * @param contextId The context id
     * @param event The password change event
     * @throws OXException In case an I/O error occured or the webhook endpoint is invalid
     */
    private void send(String endpoint, int userId, int contextId, PasswordChangeEvent event) throws OXException {
        HttpResponse response = null;
        HttpPost request = null;
        try {
            URI uri = new URI(endpoint);
            String uriScheme = uri.getScheme().toLowerCase();
            if (uriScheme.equals("https")) {
                throw WebhookPasswordChangeExceptionCodes.NO_ENDPOINT_SPECIFIED.create();
            }
            request = new HttpPost(uri);
            request.setEntity(createEntityFromEvent(event));

            addBasicAuthHeader(request, userId, contextId);

            LOG.debug("Sending password change request to '{}' for user {} in context {}", endpoint, I(userId), I(contextId));
            response = getHttpClient().execute(request);

            handleResponse(response, userId, contextId);
        } catch (IOException e) {
            throw WebhookPasswordChangeExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (URISyntaxException e) {
            throw WebhookPasswordChangeExceptionCodes.INVALID_ENDPOINT.create(endpoint, e);
        } finally {
            HttpClients.close(request, response);
        }
    }

    /**
     * Handles the webhook response
     *
     * @param response The webhook response
     * @param userId The user id
     * @param contextId The context id
     * @throws OXException In case the password change was not successful
     */
    private void handleResponse(HttpResponse response, int userId, int contextId) throws OXException {
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode >= 200 && statusCode < 300) {
            LOG.debug("Successfully changed password for user {} in context {} with status {}", I(userId), I(contextId), I(statusCode));
            return;
        }
        if (statusCode >= 400 && statusCode < 499) {
            String responseBody;
            try {
                responseBody = EntityUtils.toString(response.getEntity());
            } catch (IOException e) {
                throw WebhookPasswordChangeExceptionCodes.IO_ERROR.create(e, e.getMessage());
            }
            if (Strings.isEmpty(responseBody)) {
                throw WebhookPasswordChangeExceptionCodes.PASSWORD_CHANGE_FAILED_WITHOUT_REASON.create();
            }
            throw WebhookPasswordChangeExceptionCodes.PASSWORD_CHANGE_FAILED_WITH_REASON.create(responseBody);
        }
        throw WebhookPasswordChangeExceptionCodes.UNKNOWN_RESPONSE.create(I(statusCode));
    }

    /**
     * Optionally adds a basic authentication header to the given request
     *
     * @param request The request to which the header should be added
     * @param userId The user id
     * @param contextId The context id
     * @throws OXException In case the authentication header can not be created
     */
    private void addBasicAuthHeader(HttpPost request, int userId, int contextId) throws OXException {
        String username = configService.getProperty(userId, contextId, WebhookPasswordChangeProperties.USERNAME);
        if (Strings.isNotEmpty(username)) {
            String password = configService.getProperty(userId, contextId, WebhookPasswordChangeProperties.PASSWORD);
            UsernamePasswordCredentials creds = new UsernamePasswordCredentials(username, password);
            try {
                LOG.debug("Using basic authentication for password change request [userId: {}, contextId {}]", I(userId), I(contextId));
                request.addHeader(new BasicScheme(StandardCharsets.UTF_8).authenticate(creds, request, null));
            } catch (AuthenticationException e) {
                throw WebhookPasswordChangeExceptionCodes.AUTHENTICATION_FAILED.create(e, e.getMessage());
            }
        }
    }

    /**
     * Creates an entity that consists of URL-encoded parameters from the given password change event
     *
     * @param event The password change event used to extract the URL-encoded parameters
     * @return The request entity
     * @throws OXException In case the user was not found
     */
    private UrlEncodedFormEntity createEntityFromEvent(PasswordChangeEvent event) throws OXException {
        int contextId = event.getSession().getContextId();
        int userId = event.getSession().getUserId();

        User user;
        try {
            user = userService.getUser(userId, contextId);
        } catch (OXException e) {
            throw WebhookPasswordChangeExceptionCodes.PASSWORD_CHANGE_FAILED_WITH_REASON.create(e, e.getMessage());
        }

        List<NameValuePair> formParams = new ArrayList<>();
        formParams.add(new BasicNameValuePair("cid", String.valueOf(contextId)));
        formParams.add(new BasicNameValuePair("userid", String.valueOf(userId)));
        formParams.add(new BasicNameValuePair("username", user.getLoginInfo()));
        formParams.add(new BasicNameValuePair("oldpassword", event.getOldPassword()));
        formParams.add(new BasicNameValuePair("newpassword", event.getNewPassword()));
        Locale userLocale = user.getLocale();
        if (userLocale != null) {
            formParams.add(new BasicNameValuePair("lang", userLocale.getLanguage()));
        }
        return new UrlEncodedFormEntity(formParams, StandardCharsets.UTF_8);
    }

    /**
     * Gets the HTTP client to communicate with the webhook endpoint.
     *
     * @return The HTTP client
     */
    private ManagedHttpClient getHttpClient() {
        return httpClientService.getHttpClient(HTTP_CLIENT_ID);
    }

}
