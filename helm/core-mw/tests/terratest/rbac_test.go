package test

import (
	"testing"

	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"

	"github.com/gruntwork-io/terratest/modules/helm"
	"github.com/stretchr/testify/assert"
)

func TestRBACDefault(t *testing.T) {
	options := &helm.Options{
		SetValues: mandatoryValues(),
	}

	chartPath := getChartPath(t)

	output := helm.RenderTemplate(t, options, chartPath, helmReleaseName, []string{roleTplPath})
	var role rbacv1.Role
	helm.UnmarshalK8SYaml(t, output, &role)

	output = helm.RenderTemplate(t, options, chartPath, helmReleaseName, []string{roleBindingTplPath})
	var roleBinding rbacv1.RoleBinding
	helm.UnmarshalK8SYaml(t, output, &roleBinding)

	output = helm.RenderTemplate(t, options, chartPath, helmReleaseName, []string{serviceAccountTplPath})
	var serviceAccount corev1.ServiceAccount
	helm.UnmarshalK8SYaml(t, output, &serviceAccount)

	assert.Equal(t, "Role", role.Kind)
	assert.Equal(t, "RoleBinding", roleBinding.Kind)
	assert.Equal(t, "ServiceAccount", serviceAccount.Kind)
}

func TestRBACDisabled(t *testing.T) {
	values := mandatoryValues()
	values["serviceAccount.create"] = "false"
	values["rbac.create"] = "false"

	options := &helm.Options{
		SetValues: values,
	}

	chartPath := getChartPath(t)

	output, _ := helm.RenderTemplateE(t, options, chartPath, helmReleaseName, []string{roleTplPath})
	var role rbacv1.Role
	helm.UnmarshalK8SYaml(t, output, &role)
	assert.Empty(t, output)

	output, _ = helm.RenderTemplateE(t, options, chartPath, helmReleaseName, []string{roleBindingTplPath})
	var roleBinding rbacv1.RoleBinding
	helm.UnmarshalK8SYaml(t, output, &roleBinding)
	assert.Empty(t, output)

	output, _ = helm.RenderTemplateE(t, options, chartPath, helmReleaseName, []string{serviceAccountTplPath})
	var serviceAccount corev1.ServiceAccount
	helm.UnmarshalK8SYaml(t, output, &serviceAccount)
	assert.Empty(t, output)
}

func TestRBACEnabled(t *testing.T) {
	values := mandatoryValues()
	values["serviceAccount.create"] = "true"
	values["rbac.create"] = "true"

	options := &helm.Options{
		SetValues: values,
	}

	chartPath := getChartPath(t)

	output := helm.RenderTemplate(t, options, chartPath, helmReleaseName, []string{roleTplPath})
	var role rbacv1.Role
	helm.UnmarshalK8SYaml(t, output, &role)

	output = helm.RenderTemplate(t, options, chartPath, helmReleaseName, []string{roleBindingTplPath})
	var roleBinding rbacv1.RoleBinding
	helm.UnmarshalK8SYaml(t, output, &roleBinding)

	output = helm.RenderTemplate(t, options, chartPath, helmReleaseName, []string{serviceAccountTplPath})
	var serviceAccount corev1.ServiceAccount
	helm.UnmarshalK8SYaml(t, output, &serviceAccount)

	assert.Equal(t, "Role", role.Kind)
	assert.Equal(t, "RoleBinding", roleBinding.Kind)
	assert.Equal(t, "ServiceAccount", serviceAccount.Kind)
}
