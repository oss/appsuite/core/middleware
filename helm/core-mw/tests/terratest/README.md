# Terratest

This projects contains automated tests for the `core-mw` Helm Chart. Tests are written in [Go](https://go.dev/) using the [Terratest](https://github.com/gruntwork-io/terratest) framework.

## Prerequisites

Make sure you have Go installed on your system. You can download and install Go from [https://go.dev/dl/](https://go.dev/dl/) or by using your favorite package manager.

## Running Tests

To run tests in this project, follow these steps:

1. Open a terminal and navigate to the project directory.

2. Run the following command to execute the tests:

    ```shell
    go test -v | grep -v logger.go
    ```

    This command will run all tests in your project and display the results on the console.
    
    Verbose logging can be enabled by omitting the `grep` command.
    
    ```shell
    go test -v
    ```

    You can also run tests using the `-run` flag to execute tests with a specific pattern in the name:

    ```shell
    go test -v -run <TEST_FUCTION> | grep -v logger.go
    ```
    The `-run` flag accepts regular expressions.

    Test results will be displayed on the console. Successful tests will be marked as `PASS`, and failing tests will be marked as `FAIL`.