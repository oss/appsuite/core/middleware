package test

import (
	"testing"

	corev1 "k8s.io/api/core/v1"

	"github.com/gruntwork-io/terratest/modules/helm"
	"github.com/stretchr/testify/assert"
)

func TestCreateGuardSecretTrue(t *testing.T) {
	values := mandatoryValues()
	values["enableInitialization"] = "true"

	options := &helm.Options{
		SetValues: values,
	}

	output := helm.RenderTemplate(t, options, getChartPath(t), helmReleaseName, []string{guardSecretTplPath})

	var secret corev1.Secret
	helm.UnmarshalK8SYaml(t, output, &secret)
	assert.Equal(t, "Secret", secret.Kind)
}

func TestCreateGuardSecretFalse(t *testing.T) {
	values := mandatoryValues()
	values["enableInitialization"] = "false"

	options := &helm.Options{
		SetValues: values,
	}

	output, _ := helm.RenderTemplateE(t, options, getChartPath(t), helmReleaseName, []string{guardSecretTplPath})
	assert.Empty(t, output)
}
