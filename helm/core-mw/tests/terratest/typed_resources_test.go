package test

import (
	"slices"
	"testing"

	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"

	"github.com/gruntwork-io/terratest/modules/helm"
	"github.com/stretchr/testify/assert"
)

func TestTypeSpecificResourcesDefault(t *testing.T) {
	options := &helm.Options{
		SetValues: mandatoryValues(),
	}

	expectedResources := []string{
		"core-mw-default",
		"core-mw-contextsets-secret",
		"core-mw-etc-secrets-secret",
		"core-mw-mysql",
		"core-mw-properties-secret",
		"core-mw-secret-envvars",
		"core-mw-ui-settings-secret",
		"core-mw-yaml-secrets-secret",
		"core-mw-as-config-configmap",
		"core-mw-contextsets-configmap",
		"core-mw-etc-files-configmap",
		"core-mw-hook-before-apply-configmap",
		"core-mw-hook-before-appsuite-start-configmap",
		"core-mw-hook-start-configmap",
		"core-mw-meta-configmap",
		"core-mw-default-properties-configmap",
		"core-mw-properties-lean-configmap",
		"core-mw-default-properties-overwrite-configmap",
		"core-mw-ui-settings-configmap",
		"core-mw-yaml-files-configmap",
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	assert.Equal(t, 20, len(renderedResources))
	verifyRenderedResources(t, expectedResources, renderedResources)
}

func TestTypeSpecificProperties(t *testing.T) {
	values := mandatoryValues()
	values["roles.role1.values.properties.foobar"] = "true"
	values["roles.role2.values.properties.foobar"] = "false"
	values["scaling.nodes.type1.roles[0]"] = "role1"
	values["scaling.nodes.type1.replicas"] = "1"
	values["scaling.nodes.type2.roles[0]"] = "role2"
	values["scaling.nodes.type2.replicas"] = "1"

	options := &helm.Options{
		SetValues: values,
	}

	expectedResources := []string{
		"core-mw-type1",
		"core-mw-type2",
		"core-mw-default",
		"core-mw-contextsets-secret",
		"core-mw-etc-secrets-secret",
		"core-mw-mysql",
		"core-mw-properties-secret",
		"core-mw-secret-envvars",
		"core-mw-ui-settings-secret",
		"core-mw-yaml-secrets-secret",
		"core-mw-as-config-configmap",
		"core-mw-contextsets-configmap",
		"core-mw-etc-files-configmap",
		"core-mw-hook-before-apply-configmap",
		"core-mw-hook-before-appsuite-start-configmap",
		"core-mw-hook-start-configmap",
		"core-mw-meta-configmap",
		"core-mw-type1-properties-configmap",
		"core-mw-type2-properties-configmap",
		"core-mw-properties-lean-configmap",
		"core-mw-type1-properties-overwrite-configmap",
		"core-mw-type2-properties-overwrite-configmap",
		"core-mw-ui-settings-configmap",
		"core-mw-yaml-files-configmap",
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	verifyRenderedResources(t, expectedResources, renderedResources)
}

func TestDeployment(t *testing.T) {
	values := mandatoryValues()
	values["scaling.nodes.hazelcast.roles[0]"] = "admin"
	values["scaling.nodes.hazelcast.replicas"] = "1"

	options := &helm.Options{
		SetValues: values,
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	rawDeployment := getResourceByName(renderedResources, "core-mw-hazelcast")
	assert.NotNil(t, rawDeployment)

	var deployment appsv1.Deployment
	convertResource(t, rawDeployment, &deployment)
	assert.Equal(t, "Deployment", deployment.Kind)
}

func TestStatefulSetDefault(t *testing.T) {
	options := &helm.Options{
		SetValues: mandatoryValues(),
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	rawDeployment := getResourceByName(renderedResources, "core-mw-default")
	assert.NotNil(t, rawDeployment)

	var deployment appsv1.Deployment
	convertResource(t, rawDeployment, &deployment)
	assert.Equal(t, "StatefulSet", deployment.Kind)
}

func TestPreUpgradeTrue(t *testing.T) {
	values := mandatoryValues()
	values["update.enabled"] = "true"

	options := &helm.Options{
		SetValues: values,
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	rawDeployment := getResourceByName(renderedResources, "core-mw-default")
	assert.Empty(t, rawDeployment)
}

func TestPreUpgradeFalse(t *testing.T) {
	values := mandatoryValues()
	values["update.enabled"] = "false"

	options := &helm.Options{
		SetValues: values,
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	rawDeployment := getResourceByName(renderedResources, "core-mw-default")
	assert.NotNil(t, rawDeployment)

	var deployment appsv1.Deployment
	convertResource(t, rawDeployment, &deployment)
	assert.Equal(t, "StatefulSet", deployment.Kind)
}

func TestUpdateJobEnabled(t *testing.T) {
	values := mandatoryValues()
	values["update.enabled"] = "true"

	options := &helm.Options{
		SetValues: values,
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	rawJob := getResourceByRegex(renderedResources, "core-mw-update.*")
	assert.NotNil(t, rawJob)

	var job batchv1.Job
	convertResource(t, rawJob, &job)
	assert.Equal(t, "Job", job.Kind)
}

func TestUpdateJobDisabled(t *testing.T) {
	values := mandatoryValues()
	values["update.enabled"] = "false"

	options := &helm.Options{
		SetValues: values,
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	rawJob := getResourceByRegex(renderedResources, "core-mw-update.*")
	assert.Empty(t, rawJob)
}

func TestUpdateJobDefault(t *testing.T) {
	values := mandatoryValues()

	options := &helm.Options{
		SetValues: values,
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	rawJob := getResourceByRegex(renderedResources, "core-mw-update.*")
	assert.Empty(t, rawJob)
}

func TestUpdateJobDefaultImage(t *testing.T) {
	values := mandatoryValues()
	values["defaultRegistry"] = "custom-registry.open-xchange.com"
	values["update.enabled"] = "true"
	values["image.repository"] = "my/custom/image"
	values["image.tag"] = "mytag"

	options := &helm.Options{
		SetValues: values,
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	rawJob := getResourceByRegex(renderedResources, "core-mw-update.*")
	assert.NotNil(t, rawJob)

	var job batchv1.Job
	convertResource(t, rawJob, &job)

	assert.Equal(t, "Job", job.Kind)
	assert.Equal(t, "custom-registry.open-xchange.com/my/custom/image:mytag", job.Spec.Template.Spec.Containers[0].Image)
}

func TestUpdateJobCustomImage(t *testing.T) {
	values := mandatoryValues()
	values["defaultRegistry"] = "custom-registry.open-xchange.com"
	values["update.enabled"] = "true"
	values["update.image.repository"] = "my/custom/image"
	values["update.image.tag"] = "mytag"

	options := &helm.Options{
		SetValues: values,
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	rawJob := getResourceByRegex(renderedResources, "core-mw-update.*")
	assert.NotNil(t, rawJob)

	var job batchv1.Job
	convertResource(t, rawJob, &job)

	assert.Equal(t, "Job", job.Kind)
	assert.Equal(t, "custom-registry.open-xchange.com/my/custom/image:mytag", job.Spec.Template.Spec.Containers[0].Image)
}

func TestUpdateJobContainerSequence(t *testing.T) {
	values := mandatoryValues()
	values["update.enabled"] = "true"
	values["roles.role1.values.packages.status.open-xchange-admin-autocontextid"] = "enabled"
	values["roles.role2.values.packages.status.open-xchange-authentication-ldap"] = "enabled"
	values["scaling.nodes.type1.roles"] = "{role1}"
	values["scaling.nodes.type1.replicas"] = "1"
	values["scaling.nodes.type11.roles"] = "{role1}"
	values["scaling.nodes.type11.replicas"] = "1"
	values["scaling.nodes.type2.roles"] = "{role2}"
	values["scaling.nodes.type2.replicas"] = "1"
	values["scaling.nodes.groupware.replicas"] = "1"

	options := &helm.Options{
		SetValues: values,
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	rawJob := getResourceByRegex(renderedResources, "core-mw-update.*")
	assert.NotNil(t, rawJob)

	var job batchv1.Job
	convertResource(t, rawJob, &job)

	var initContainerNames []string
	for _, c := range job.Spec.Template.Spec.InitContainers {
		initContainerNames = append(initContainerNames, c.Name)
	}

	assert.True(t, slices.Contains(initContainerNames, "update-groupware"))
	assert.True(t, slices.Contains(initContainerNames, "update-type11"))
	assert.False(t, slices.Contains(initContainerNames, "update-type2"))

	var containerNames []string
	for _, c := range job.Spec.Template.Spec.Containers {
		containerNames = append(containerNames, c.Name)
	}

	assert.False(t, slices.Contains(containerNames, "update-groupware"))
	assert.False(t, slices.Contains(containerNames, "update-type11"))
	assert.True(t, slices.Contains(containerNames, "update-type2"))
}

func TestRemoteDebugContainerPorts(t *testing.T) {
	values := mandatoryValues()
	values["remoteDebug.enabled"] = "true"

	options := &helm.Options{
		SetValues: values,
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	rawDeployment := getResourceByName(renderedResources, "core-mw-default")
	assert.NotNil(t, rawDeployment)

	var deployment appsv1.Deployment
	convertResource(t, rawDeployment, &deployment)

	remoteDebugPortFound := false
	for _, port := range deployment.Spec.Template.Spec.Containers[0].Ports {
		if port.ContainerPort == 8102 {
			remoteDebugPortFound = true
			break
		}
	}
	assert.True(t, remoteDebugPortFound)
}
