package test

import (
	"fmt"
	"testing"

	corev1 "k8s.io/api/core/v1"

	"github.com/gruntwork-io/terratest/modules/helm"
	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/stretchr/testify/assert"
)

func TestServiceDefault(t *testing.T) {
	values := mandatoryValues()
	values["remoteDebug.enabled"] = "true"

	options := &helm.Options{
		SetValues: values,
	}

	expectedResources := []string{
		"core-mw-hazelcast-headless",
		"core-mw-http-api",
		"core-mw-sync",
		"core-mw-admin",
		"core-mw-businessmobility",
		"core-mw-request-analyzer",
		"core-mw-remote-debug",
	}

	renderedResources := renderResources(t, serviceTplPath, options)
	assert.Equal(t, 7, len(renderedResources))

	verifyRenderedResources(t, expectedResources, renderedResources)

	var service corev1.Service
	for _, resource := range renderedResources {
		convertResource(t, resource, &service)
		var serviceName = trimResourceName(service.Name)
		switch serviceName {
		case "core-mw-hazelcast-headless":
			AssertRoleExist(t, "documents", service.Spec.Selector)
			AssertPortExists(t, "tcp-hazelcast", 5701, "TCP", "", service.Spec.Ports)
		case "core-mw-http-api":
			AssertRoleExist(t, "http-api", service.Spec.Selector)
			AssertPortExists(t, "http", 80, "TCP", "http", service.Spec.Ports)
		case "core-mw-sync":
			AssertRoleExist(t, "sync", service.Spec.Selector)
			AssertPortExists(t, "http", 80, "TCP", "http", service.Spec.Ports)
		case "core-mw-admin":
			AssertRoleExist(t, "admin", service.Spec.Selector)
			AssertPortExists(t, "http", 80, "TCP", "http", service.Spec.Ports)
		case "core-mw-businessmobility":
			AssertRoleExist(t, "businessmobility", service.Spec.Selector)
			AssertPortExists(t, "http", 80, "TCP", "http", service.Spec.Ports)
		case "core-mw-request-analyzer":
			AssertRoleExist(t, "request-analyzer", service.Spec.Selector)
			AssertPortExists(t, "http", 80, "TCP", "http", service.Spec.Ports)
		case "core-mw-remote-debug":
			AssertRoleExist(t, "debug", service.Spec.Selector)
			AssertPortExists(t, "remote-debug", 8102, "TCP", "", service.Spec.Ports)
		default:
			assert.Fail(t, fmt.Sprintf("Unexpected service '%s'found.", serviceName))
		}
	}
}

func TestRoleServices(t *testing.T) {
	values := mandatoryValues()
	values["roles.role1.services[0].type"] = "ClusterIP"
	values["roles.role1.services[0].ports[0].port"] = "80"
	values["roles.role1.services[0].ports[0].name"] = "role1"
	values["roles.role2.services[0].type"] = "ClusterIP"
	values["roles.role2.services[0].ports[0].port"] = "8080"
	values["roles.role2.services[0].ports[0].name"] = "role2"
	values["roles.role3.values.properties.foobar"] = "true"
	values["scaling.nodes.type1.roles"] = "{role1}"
	values["scaling.nodes.type1.replicas"] = "1"
	values["scaling.nodes.type2A.roles"] = "{role2}"
	values["scaling.nodes.type2A.replicas"] = "1"
	values["scaling.nodes.type2B.roles"] = "{role2}"
	values["scaling.nodes.type2B.replicas"] = "1"
	values["scaling.nodes.type3.roles"] = "{role3}"
	values["scaling.nodes.type3.replicas"] = "1"

	options := &helm.Options{
		SetValues: values,
	}

	expectedResources := []string{
		"core-mw-role1",
		"core-mw-role2",
	}

	renderedResources := renderResources(t, serviceTplPath, options)
	assert.Equal(t, 2, len(renderedResources))

	verifyRenderedResources(t, expectedResources, renderedResources)

	var service corev1.Service
	for _, resource := range renderedResources {
		convertResource(t, resource, &service)
		var serviceName = trimResourceName(service.Name)
		switch serviceName {
		case "core-mw-role1":
			AssertRoleExist(t, "role1", service.Spec.Selector)
			AssertPortExists(t, "role1", 80, "", "", service.Spec.Ports)
		case "core-mw-role2":
			AssertRoleExist(t, "role2", service.Spec.Selector)
			AssertPortExists(t, "role2", 8080, "", "", service.Spec.Ports)
		default:
			assert.Fail(t, fmt.Sprintf("Unexpected service '%s'found.", serviceName))
		}
	}
}

func AssertPortExists(t *testing.T, name string, port int32, protocol string, targetPort string, ports []corev1.ServicePort) {
	for _, p := range ports {
		if name == "" || p.Name == name {
			if port == 0 || p.Port == port {
				if protocol == "" || string(p.Protocol) == protocol {
					if targetPort == "" || p.TargetPort.StrVal == targetPort {
						return
					}
				}
			}
		}
		logger.Log(t, "DEBUG: Port [name=%s, port=%d, protocol=%s, targetPort=%s] does not match [name=%s, port=%d, protocol=%s, targetPort=%s].", name, port, protocol, targetPort, p.Name, p.Port, p.Protocol, p.TargetPort.StrVal)
	}
	assert.Fail(t, fmt.Sprintf("Port [name=%s, port=%d, protocol=%s, targetPort=%s] not found.", name, port, protocol, targetPort))
}

func AssertRoleExist(t *testing.T, role string, selector map[string]string) {
	assert.Equal(t, "true", selector["roles.middleware.open-xchange.com/"+role])
}
