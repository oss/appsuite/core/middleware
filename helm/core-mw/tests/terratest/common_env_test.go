package test

import (
	"testing"

	corev1 "k8s.io/api/core/v1"

	"github.com/gruntwork-io/terratest/modules/helm"
	"github.com/stretchr/testify/assert"
)

func TestCreateCommonEnvSecretTrue(t *testing.T) {
	values := mandatoryValues()
	values["createCommonEnv"] = "true"

	options := &helm.Options{
		SetValues: values,
	}

	output := helm.RenderTemplate(t, options, getChartPath(t), helmReleaseName, []string{commonEnvTplPath})

	var secret corev1.Secret
	helm.UnmarshalK8SYaml(t, output, &secret)
	assert.Equal(t, "Secret", secret.Kind)
}

func TestCreateCommonEnvFalse(t *testing.T) {
	values := mandatoryValues()

	values["createCommonEnv"] = "false"

	options := &helm.Options{
		SetValues: values,
	}

	output, _ := helm.RenderTemplateE(t, options, getChartPath(t), helmReleaseName, []string{commonEnvTplPath})
	assert.Empty(t, output)
}
