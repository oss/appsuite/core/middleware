package test

import (
	"testing"

	corev1 "k8s.io/api/core/v1"

	"github.com/gruntwork-io/terratest/modules/helm"
	"github.com/stretchr/testify/assert"
)

func TestMWB2435(t *testing.T) {
	values := mandatoryValues()

	options := &helm.Options{
		SetValues: values,
	}

	renderedResources := renderResources(t, typedResourcesTplPath, options)
	rawSecret := getResourceByName(renderedResources, "core-mw-secret-envvars")
	assert.NotNil(t, rawSecret)

	var evnvarSecret corev1.Secret
	convertResource(t, rawSecret, &evnvarSecret)
	assert.Contains(t, evnvarSecret.Data, "CREDSTORAGE_PASSCRYPT")
}
