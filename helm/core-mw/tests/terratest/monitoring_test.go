package test

import (
	"testing"

	corev1 "k8s.io/api/core/v1"

	"github.com/gruntwork-io/terratest/modules/helm"
	"github.com/stretchr/testify/assert"
)

func TestMonitoringServiceEnabled(t *testing.T) {
	values := mandatoryValues()
	values["extras.monitoring.enabled"] = "true"

	options := &helm.Options{
		SetValues: values,
	}

	output := helm.RenderTemplate(t, options, getChartPath(t), helmReleaseName, []string{monitoringSvcTplPath})

	var service corev1.Service
	helm.UnmarshalK8SYaml(t, output, &service)
	assert.Equal(t, "Service", service.Kind)
}

func TestMonitoringServiceDisabled(t *testing.T) {
	values := mandatoryValues()
	values["extras.monitoring.enabled"] = "false"

	options := &helm.Options{
		SetValues: values,
	}

	output, _ := helm.RenderTemplateE(t, options, getChartPath(t), helmReleaseName, []string{monitoringSvcTplPath})
	assert.Empty(t, output)
}

func TestDashboardConfigMapEnabled(t *testing.T) {
	values := mandatoryValues()
	values["extras.monitoring.enabled"] = "true"

	options := &helm.Options{
		SetValues: values,
	}

	output := helm.RenderTemplate(t, options, getChartPath(t), helmReleaseName, []string{dashboardsCmTplPath})

	var configmap corev1.ConfigMap
	helm.UnmarshalK8SYaml(t, output, &configmap)
	assert.Equal(t, "ConfigMap", configmap.Kind)
}

func TestDashboardConfigMapDisabled(t *testing.T) {
	values := mandatoryValues()
	values["extras.monitoring.enabled"] = "false"

	options := &helm.Options{
		SetValues: values,
	}

	output, _ := helm.RenderTemplateE(t, options, getChartPath(t), helmReleaseName, []string{dashboardsCmTplPath})
	assert.Empty(t, output)
}
