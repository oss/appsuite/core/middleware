package test

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"slices"
	"strings"
	"testing"

	"github.com/gruntwork-io/terratest/modules/helm"
	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v2"
)

const (
	// The release name used to render the helm chart
	helmReleaseName string = "terratest"

	// The helm chart path relative to the middleware project directory
	helmChartPathRelative string = "helm/core-mw"

	// The mandatory mysql values
	mysqlUser         string = "alice"
	mysqlPassword     string = "secret"
	mysqlRootPassword string = "secret"

	// The template file paths
	commonEnvTplPath      = "templates/common-env-secret.yaml"
	dashboardsCmTplPath   = "templates/dashboards-cm.yaml"
	guardSecretTplPath    = "templates/guard-secret.yaml"
	monitoringSvcTplPath  = "templates/monitoring-svc.yaml"
	roleBindingTplPath    = "templates/rolebinding.yaml"
	roleTplPath           = "templates/role.yaml"
	serviceAccountTplPath = "templates/serviceaccount.yaml"
	serviceTplPath        = "templates/service.yaml"
	typedResourcesTplPath = "templates/typed-resources.yaml"
)

// Gets the absolute helm chart path
//
// Parameters:
//   - t: The testing object
//
// Return:
//   - string: The helm chart path
func getChartPath(t *testing.T) string {
	path := determineChartPath(t)
	validatePath(t, path)
	return path
}

// Determines the absolute helm chart path by using the MIDDLEWARE_PROJECT_DIR environment variable
//
// If MIDDLEWARE_PROJECT_DIR is empty, the current working directory is used as a fallback
//
// Parameters:
//   - t: The testing object
//
// Return:
//   - string: The helm chart path
func determineChartPath(t *testing.T) string {
	projectDir := os.Getenv("MIDDLEWARE_PROJECT_DIR")

	if len(projectDir) > 0 {
		return resolvePath(t, fmt.Sprintf("%s/%s", projectDir, helmChartPathRelative))
	}

	workingDir, err := os.Getwd()
	if err != nil {
		t.Fatal(err.Error())
	}
	return resolvePath(t, fmt.Sprintf("%s/../..", workingDir))
}

// Resolves a given path to it's absolute representation
//
// Parameters:
//   - t: The testing object
//   - string: The path to resolve
//
// Return:
//   - string: The absolute path
func resolvePath(t *testing.T, path string) string {
	absolutePath, err := filepath.Abs(path)
	if err != nil {
		t.Fatal(err.Error())
	}
	return absolutePath
}

// Validates the given helm chart path
//
// Parameters:
//   - t: The testing object
//   - string: The helm chart path to validate
func validatePath(t *testing.T, path string) {
	if strings.HasSuffix(path, helmChartPathRelative) == false {
		t.Fatalf("'%s' is not a valid path", path)
	}
	chartDir, err := os.Stat(path)
	if err != nil {
		t.Fatal(err.Error())
	}
	if chartDir.IsDir() == false {
		t.Fatalf("'%s' is not a directory", path)
	}
}

// Gets mandatory values needed to render the helm chart
//
// Return:
//   - map[string]string: The map of mandatory values
func mandatoryValues() map[string]string {
	var values = make(map[string]string)
	values["mysql.auth.user"] = mysqlUser
	values["mysql.auth.password"] = mysqlPassword
	values["mysql.auth.rootPassword"] = mysqlRootPassword
	return values
}

// Renders the given template with given helm values
//
// Parameters:
//   - t: The testing object
//   - yamlPath: The path to the yaml template
//   - options: The Helm values
//
// Return:
//   - []map[interface{}]interface{}: The rendered resources as a slice of maps
func renderResources(t *testing.T, yamlPath string, options *helm.Options) []map[interface{}]interface{} {
	output := helm.RenderTemplate(t, options, getChartPath(t), helmReleaseName, []string{yamlPath})
	sOutput := strings.Split(output, "---")
	resources := make([]map[interface{}]interface{}, 0)
	for _, rawOutput := range sOutput[1:] {
		resource := make(map[interface{}]interface{})
		err := yaml.Unmarshal([]byte(rawOutput), &resource)
		assert.Nil(t, err)
		if len(resource) > 0 {
			resources = append(resources, resource)
		}
	}
	return resources
}

// Verifies that the given resources were rendered
//
// Parameters:
//   - t: The testing object
//   - expectedResources: Slice of resources that should be rendered
//   - renderedResources: Slice of rendered resources to verify
func verifyRenderedResources(t *testing.T, expectedResources []string, renderedResources []map[interface{}]interface{}) {
	for _, resource := range renderedResources {
		resourceName := fmt.Sprint(resource["metadata"].(map[interface{}]interface{})["name"])
		isPresent := slices.Contains(expectedResources, trimResourceName(resourceName))
		assert.True(t, isPresent, fmt.Sprintf("Found unexpected resource %s", resourceName))
	}
}

// Converts the given resources into the specified type
//
// Parameters:
//   - t: The testing object
//   - resource: The resource as map
//   - renderedResources: The object in which the resource will be converted
//
// Example:
//
//	var service corev1.Service
//	convertResource(t, resource, &service)
//	fmt.Println(service.APIVersion)
func convertResource(t *testing.T, resource map[interface{}]interface{}, resourceType interface{}) {
	yamlBytes, _ := yaml.Marshal(resource)
	yamlString := string(yamlBytes)
	helm.UnmarshalK8SYaml(t, yamlString, &resourceType)
}

// Removes the release name prefix for a resource name
//
// Parameters:
//   - resourceName: The resource name including the prefix
//
// Return:
//   - string: The resource name without prefix
func trimResourceName(resourceName string) string {
	return strings.TrimPrefix(resourceName, fmt.Sprintf("%s-", helmReleaseName))
}

// Searches the given resources by name
//
// Parameters:
//   - renderedResources: Slice of rendered resources to search
//   - resourceName: The resource to search
//
// Return:
//   - map[interface{}]interface{}: The resource or nil if the resource was not found
func getResourceByName(renderedResources []map[interface{}]interface{}, resourceName string) map[interface{}]interface{} {
	for _, resource := range renderedResources {
		if resource["metadata"].(map[interface{}]interface{})["name"] == fmt.Sprintf("%s-%s", helmReleaseName, resourceName) {
			return resource
		}
	}
	return nil
}

// Searches the given resources by name with a regex string
//
// Parameters:
//   - renderedResources: Slice of rendered resources to search
//   - regexStr: The regex string to search resources by name
//
// Return:
//   - map[interface{}]interface{}: The resource or nil if the resource was not found
func getResourceByRegex(renderedResources []map[interface{}]interface{}, regexStr string) map[interface{}]interface{} {
	r := regexp.MustCompile(regexStr)
	for _, resource := range renderedResources {
		resourceName := resource["metadata"].(map[interface{}]interface{})["name"].(string)
		if r.MatchString(resourceName) {
			return resource
		}
	}
	return nil
}
