#!/bin/bash

set -e

export REGISTRY_URL="registry.open-xchange.com"
export REGISTRY_PROJECT="appsuite-core-internal"

function determine_latest_image_version {
    if [ -z "$IMAGE_VERSION" ]; then
        export IMAGE_VERSION=$(curl -sSL https://api.github.com/repos/gotenberg/gotenberg/releases/latest | jq -r .name)
        if [ -z "$IMAGE_VERSION" ]; then
            echo -n "Please enter the image version: "
            read IMAGE_VERSION
        fi
    fi
    echo "Latest official image version: $IMAGE_VERSION"
}

function determine_chart_version {
    if [ -z "$CHART_VERSION" ]; then
        response=$(curl -sSL https://api.github.com/repos/MaikuMori/helm-charts/releases/latest | jq -r .name)
        export CHART_VERSION="${response#gotenberg-}"
        if [ -z "$CHART_VERSION" ]; then
            echo -n "Please enter the Gotenberg chart version: "
            read CHART_VERSION
        fi
    fi
    echo "Using chart version: $CHART_VERSION"
}

function update_image {
    determine_latest_image_version
    export image_url="https://$REGISTRY_URL/api/v2.0/projects/$REGISTRY_PROJECT/repositories/3rdparty%2Fgotenberg/artifacts/$IMAGE_VERSION/tags"
    echo $image_url
    docker login $REGISTRY_URL -u $REGISTRY_USER -p $REGISTRY_PASSWORD
    response=$(curl -u $REGISTRY_USER:$REGISTRY_PASSWORD -s -o /dev/null -w "%{http_code}" $image_url)
    if [ "$response" == "200" ]; then
        echo "Image already exists. Skipping ..."
    elif [ "$response" == "404" ]; then
        echo "Build the Gotenberg image ..."
        rm -rf /tmp/gotenberg
        git clone https://gitlab.open-xchange.com/appsuite/platform/gotenberg /tmp/gotenberg
        if ! git -C /tmp/gotenberg tag | grep -q "v$IMAGE_VERSION"; then
            echo "Image version does not exist ..."
            exit 1
        fi
        make -C /tmp/gotenberg build

        docker tag docker.io/gotenberg/gotenberg:snapshot registry.open-xchange.com/$REGISTRY_PROJECT/3rdparty/gotenberg:$IMAGE_VERSION
        docker push registry.open-xchange.com/$REGISTRY_PROJECT/3rdparty/gotenberg:$IMAGE_VERSION

        echo "Updating image reference in helm/core-mw/values.yaml ..."
        diff -U0 -w -b --ignore-blank-lines ../values.yaml <(yq eval ".gotenberg.image.tag= \"$IMAGE_VERSION\"" ../values.yaml) > values.diff | true
        if [ -s values.diff ]; then
            patch  ../values.yaml < values.diff
        fi
        rm values.diff

        export IMAGE_UPDATED=true
    else
        echo "An unknown error occured while accessing $image_url ($response)"
        exit 1
    fi
}

function update_chart {
    determine_chart_version
    export GOTENBERG_CHART_URL="https://$REGISTRY_URL/api/v2.0/projects/$REGISTRY_PROJECT/repositories/charts%252F3rdparty%252Fgotenberg/artifacts/$CHART_VERSION/tags"

    response=$(curl -u $REGISTRY_USER:$REGISTRY_PASSWORD -s -o /dev/null -w "%{http_code}" $GOTENBERG_CHART_URL)
    if [ "$response" == "200" ]; then
        echo "Chart already exists. Skipping ..."
    elif [ "$response" == "404" ]; then
        helm repo add --force-update maikumori https://maikumori.github.io/helm-charts/
        helm repo update
        helm pull maikumori/gotenberg --version $CHART_VERSION
        helm registry login $REGISTRY_URL -u $REGISTRY_USER -p $REGISTRY_PASSWORD
        helm push gotenberg-$CHART_VERSION.tgz oci://$REGISTRY_URL/$REGISTRY_PROJECT/charts/3rdparty
        rm gotenberg-$CHART_VERSION.tgz

        git restore ../Chart.yaml

        echo "Updating chart reference in helm/core-mw/Chart.yaml ..."
        yq -i '(.dependencies[] | select(.name == "gotenberg") | .version) = env(CHART_VERSION)' ../Chart.yaml

        export CHART_UPDATED=true
    else
        echo "An unknown error occured while accessing $GOTENBERG_CHART_URL ($response)"
        exit 1
    fi
}

function increment_chart_version {
    echo "Increment chart version in helm/core-mw/Chart.yaml ..."
    yq -i '.version |= (split(".") | .[-1] |= ((. tag = "!!int") + 1) | join("."))' ../Chart.yaml

    echo "Updating README.md ..."
    git restore ../README.md
    cd .. && helm-docs
}

function main {
    if [ -z "$REGISTRY_USER" ]; then
        echo -n "Please enter your registry user: "
        read REGISTRY_USER
    fi

    if [ -z "$REGISTRY_PASSWORD" ]; then
        echo -n "Please enter your registry password: "
        read -s REGISTRY_PASSWORD
        echo
    fi

    update_image
    update_chart
    if [ "$IMAGE_UPDATED" = "true" ] || [ "$CHART_UPDATED" = "true" ]; then
        increment_chart_version
    fi
}

main