/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.http.grizzly.util;

import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import javax.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.http.grizzly.eas.EASCommandCodes;

/**
 * {@link RequestToolsTest}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 7.6.2
 */
public class RequestToolsTest {

    private static final String SYNC = EASCommandCodes.SYNC.getCommandName().toLowerCase();

    private static final String PING = EASCommandCodes.PING.getCommandName().toLowerCase();

    private static final String SYNC_UPDATE_PATH = "/syncUpdate".toLowerCase();

    private static final String PING_PATH = "/Ping".toLowerCase();

    @Mock
    private HttpServletRequest servletRequest;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        RequestTools.USM_PATH_CACHE.invalidateAll();
    }

     @Test
     public void testIsIgnoredEasRequest_noEASRequest_ReturnFalse() {
        Mockito.when(servletRequest.getRequestURI()).thenReturn("/No_EAS_Request");

        Set<String> ignoredEasCommands = new CopyOnWriteArraySet<String>(Arrays.asList(SYNC, PING));

        boolean ignoredEasRequest = RequestTools.isIgnoredEasRequest(servletRequest, ignoredEasCommands);
        Assertions.assertFalse(ignoredEasRequest, "Non EAS request should return false for definition of 'ignore eas request'");
    }

     @Test
     public void testIsIgnoredEasRequest_syncCommandThatShouldBeIgnored_ReturnTrue() {
        Mockito.when(servletRequest.getRequestURI()).thenReturn(RequestTools.EAS_URI_DEFAULT);
        Mockito.when(servletRequest.getParameter(RequestTools.EAS_CMD)).thenReturn("Sync");

        Set<String> ignoredEasCommands = new CopyOnWriteArraySet<String>(Arrays.asList(SYNC, PING));

        boolean ignoredEasRequest = RequestTools.isIgnoredEasRequest(servletRequest, ignoredEasCommands);
        Assertions.assertTrue(ignoredEasRequest, "Given command is defined as beeing ignored!");
    }

     @Test
     public void testIsIgnoredEasRequest_syncCommandThatShouldNotBeIgnored_ReturnFalse() {
        Mockito.when(servletRequest.getRequestURI()).thenReturn(RequestTools.EAS_URI_DEFAULT);
        Mockito.when(servletRequest.getParameter(RequestTools.EAS_CMD)).thenReturn("Sync");

        Set<String> ignoredEasCommands = new CopyOnWriteArraySet<String>(Arrays.asList(PING));

        boolean ignoredEasRequest = RequestTools.isIgnoredEasRequest(servletRequest, ignoredEasCommands);
        Assertions.assertFalse(ignoredEasRequest, "Given command is defined as not beeing ignored but was not ignored!");
    }

     @Test
     public void testIsIgnoredEasRequest_easRequestWithUnknownAction_ReturnFalse() {
        Mockito.when(servletRequest.getRequestURI()).thenReturn(RequestTools.EAS_URI_DEFAULT);
        Mockito.when(servletRequest.getParameter(RequestTools.EAS_CMD)).thenReturn("thisIsNotAValidRequest");

        Set<String> ignoredEasCommands = new CopyOnWriteArraySet<String>(Arrays.asList(PING));

        boolean ignoredEasRequest = RequestTools.isIgnoredEasRequest(servletRequest, ignoredEasCommands);
        Assertions.assertFalse(ignoredEasRequest, "Given command is defined as not beeing ignored but was not ignored!");
    }

     @Test
     public void testIsIgnoredEasRequest_base64encodedSyncCommandThatShouldNotBeIngored_returnFalse() {
        Mockito.when(servletRequest.getRequestURI()).thenReturn(RequestTools.EAS_URI_DEFAULT);
        Mockito.when(servletRequest.getQueryString()).thenReturn("jAAJBAp2MTQwRGV2aWNlAApTbWFydFBob25l");

        Set<String> ignoredEasCommands = new CopyOnWriteArraySet<String>(Arrays.asList(PING));

        boolean ignoredEasRequest = RequestTools.isIgnoredEasRequest(servletRequest, ignoredEasCommands);
        Assertions.assertFalse(ignoredEasRequest);
    }

     @Test
     public void testIsIgnoredEasRequest_base64encodedSyncCommandThatsShouldBeIgnored_returnTrue() {
        Mockito.when(servletRequest.getRequestURI()).thenReturn(RequestTools.EAS_URI_DEFAULT);
        Mockito.when(servletRequest.getQueryString()).thenReturn("jAAJBAp2MTQwRGV2aWNlAApTbWFydFBob25l");

        Set<String> ignoredEasCommands = new CopyOnWriteArraySet<String>(Arrays.asList(SYNC));

        boolean ignoredEasRequest = RequestTools.isIgnoredEasRequest(servletRequest, ignoredEasCommands);
        Assertions.assertTrue(ignoredEasRequest);
    }

     @Test
     public void testIsIgnoredEasRequest_base64encodedSyncWithoutARelationToBase64EncodedRequest_returnFalse() {
        Mockito.when(servletRequest.getRequestURI()).thenReturn(RequestTools.EAS_URI_DEFAULT);
        Mockito.when(servletRequest.getQueryString()).thenReturn("ZGZhZHNmYWRzZHNmYWRzZmRmYWRz");

        Set<String> ignoredEasCommands = new CopyOnWriteArraySet<String>(Arrays.asList(SYNC));

        boolean ignoredEasRequest = RequestTools.isIgnoredEasRequest(servletRequest, ignoredEasCommands);
        Assertions.assertFalse(ignoredEasRequest);
    }

     @Test
     public void testIsIgnoredUSMRequest_noUsmRequest_ReturnFalse() {
        Mockito.when(servletRequest.getRequestURI()).thenReturn("/No_USM_Request");

        Set<String> ignoredUsmCommands = new CopyOnWriteArraySet<String>(Arrays.asList(SYNC_UPDATE_PATH));

        boolean ignoredUsmRequest = RequestTools.isIgnoredUsmRequest(servletRequest, ignoredUsmCommands);
        Assertions.assertFalse(ignoredUsmRequest, "Non USM request should return false for definition of 'ignore usm request'");
    }

     @Test
     public void testIsIgnoredUsmRequest_syncCommandThatShouldBeIgnored_ReturnTrue() {
        Mockito.when(servletRequest.getRequestURI()).thenReturn(RequestTools.USM_URI_DEFAULT);
        Mockito.when(servletRequest.getPathInfo()).thenReturn(SYNC_UPDATE_PATH);

        Set<String> ignoredUsmCommands = new CopyOnWriteArraySet<String>(Arrays.asList(SYNC_UPDATE_PATH, PING_PATH));

        boolean ignoredUsmRequest = RequestTools.isIgnoredUsmRequest(servletRequest, ignoredUsmCommands);
        Assertions.assertTrue(ignoredUsmRequest, "Given command is defined as beeing ignored but was not!");
    }

     @Test
     public void testIsIgnoredUsmRequest_syncCommandThatShouldNotBeIgnored_ReturnFalse() {
        Mockito.when(servletRequest.getRequestURI()).thenReturn(RequestTools.USM_URI_DEFAULT);
        Mockito.when(servletRequest.getPathInfo()).thenReturn(SYNC_UPDATE_PATH);

        Set<String> ignoredUsmCommands = new CopyOnWriteArraySet<String>(Arrays.asList(PING_PATH));

        boolean ignoredUsmRequest = RequestTools.isIgnoredUsmRequest(servletRequest, ignoredUsmCommands);
        Assertions.assertFalse(ignoredUsmRequest, "Given command is defined as not beeing ignored but was not ignored!");
    }

     @Test
     public void testIsIgnoredUsmRequest_usmRequestWithUnknownPath_ReturnFalse() {
        Mockito.when(servletRequest.getRequestURI()).thenReturn(RequestTools.USM_URI_DEFAULT);
        Mockito.when(servletRequest.getPathInfo()).thenReturn("/thisIsNotAValidPath");

        Set<String> ignoredUsmCommands = new CopyOnWriteArraySet<String>(Arrays.asList(SYNC_UPDATE_PATH, PING_PATH));

        boolean ignoredUsmRequest = RequestTools.isIgnoredUsmRequest(servletRequest, ignoredUsmCommands);
        Assertions.assertFalse(ignoredUsmRequest, "Given command is defined as not beeing ignored but was not ignored!");
    }

     @Test
     public void testIsIgnoredUsmRequest_pathIsNull_ReturnFalse() {
        Mockito.when(servletRequest.getRequestURI()).thenReturn(RequestTools.USM_URI_DEFAULT);
        Mockito.when(servletRequest.getPathInfo()).thenReturn(null);

        Set<String> ignoredUsmCommands = new CopyOnWriteArraySet<String>(Arrays.asList(SYNC_UPDATE_PATH, PING_PATH));

        boolean ignoredUsmRequest = RequestTools.isIgnoredUsmRequest(servletRequest, ignoredUsmCommands);
        Assertions.assertFalse(ignoredUsmRequest, "Given command is defined as not beeing ignored but was not ignored!");
    }

     @Test
     public void testIsIgnoredUsmRequest_cacheUsedForTwoPaths_returnTrue() {
         Mockito.when(servletRequest.getRequestURI()).thenReturn(RequestTools.USM_URI_DEFAULT);
         Mockito.when(servletRequest.getPathInfo()).thenReturn(SYNC_UPDATE_PATH, PING_PATH);

         Set<String> ignoredUsmCommands = new CopyOnWriteArraySet<>(Arrays.asList(SYNC_UPDATE_PATH, PING_PATH));

         boolean ignoredUsmRequest = RequestTools.isIgnoredUsmRequest(servletRequest, ignoredUsmCommands);
         Assertions.assertTrue(ignoredUsmRequest, "Given command is defined as not beeing ignored but was not ignored!");

         boolean ignoredUsmRequest2 = RequestTools.isIgnoredUsmRequest(servletRequest, ignoredUsmCommands);
         Assertions.assertTrue(ignoredUsmRequest2, "Given command is defined as not beeing ignored but was not ignored!");

         Assertions.assertEquals(2, RequestTools.USM_PATH_CACHE.size(), "Received wrong cache size. Should only contain one path");
         Assertions.assertNotNull(RequestTools.USM_PATH_CACHE.getIfPresent(SYNC_UPDATE_PATH.toLowerCase()), "Cannot find path for syncupdate in cache");
         Assertions.assertNotNull(RequestTools.USM_PATH_CACHE.getIfPresent(PING_PATH.toLowerCase()), "Cannot find path for ping in cache");
     }

     @Test
     public void testIsIgnoredUsmRequest_cacheUsedToReturnValue_returnTrue() {
         Mockito.when(servletRequest.getRequestURI()).thenReturn(RequestTools.USM_URI_DEFAULT);
         Mockito.when(servletRequest.getPathInfo()).thenReturn(SYNC_UPDATE_PATH);

         Set<String> ignoredUsmCommands = new CopyOnWriteArraySet<>(Arrays.asList(SYNC_UPDATE_PATH));

         boolean ignoredUsmRequest = RequestTools.isIgnoredUsmRequest(servletRequest, ignoredUsmCommands);
         Assertions.assertTrue(ignoredUsmRequest, "Given command is defined as not beeing ignored but was not ignored!");

         boolean ignoredUsmRequest2 = RequestTools.isIgnoredUsmRequest(servletRequest, ignoredUsmCommands);
         Assertions.assertTrue(ignoredUsmRequest2, "Given command is defined as not beeing ignored but was not ignored!");

         Assertions.assertEquals(1, RequestTools.USM_PATH_CACHE.size(), "Received wrong cache size. Should only contain one path");
     }
}
