/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package org.glassfish.grizzly.http.server;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.glassfish.grizzly.http.Cookie;
import org.glassfish.grizzly.http.server.util.Globals;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Expiry;
import com.openexchange.http.grizzly.GrizzlyConfig;
import com.openexchange.log.LogProperties;
import com.openexchange.tools.servlet.http.Cookies;


/**
 * {@link OXSessionManager} - Open-Xchange HTTP session manager.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.4
 */
public class OXSessionManager implements SessionManager {

    private final GrizzlyConfig grizzlyConfig;
    private final Cache<String, OXSession> sessions;
    private final Random rnd;
    private final AtomicReference<String> sessionCookieName;

    /**
     * Initializes a new {@link OXSessionManager}.
     */
    public OXSessionManager(GrizzlyConfig grizzlyConfig) {
        super();
        this.grizzlyConfig = grizzlyConfig;
        this.sessionCookieName = new AtomicReference<String>(Globals.SESSION_COOKIE_NAME);

        Caffeine<Object, Object> cacheBuilder = Caffeine.newBuilder();
        int max = grizzlyConfig.getMaxNumberOfHttpSessions();
        if (max > 0) {
            cacheBuilder.maximumSize(max);
        }
        cacheBuilder.expireAfter(new Expiry<String, OXSession>() {

            @Override
            public long expireAfterCreate(String key, OXSession value, long currentTime) {
                return Long.MAX_VALUE;
            }

            @Override
            public long expireAfterUpdate(String key, OXSession value, long currentTime, long currentDuration) {
                return Long.MAX_VALUE;
            }

            @Override
            public long expireAfterRead(String key, OXSession value, long currentTime, long currentDuration) {
                long timeoutMillis = value.getSessionTimeout();
                return timeoutMillis > 0 ? TimeUnit.MILLISECONDS.toNanos(timeoutMillis) : Long.MAX_VALUE;
            }

        });
        sessions = cacheBuilder.build();
        this.rnd = new Random();
    }

    /**
     * Disposes this instance
     */
    public void destroy() {
        sessions.invalidateAll();
    }

    @Override
    public void setSessionCookieName(final String name) {
        if (name != null && !name.isEmpty()) {
            sessionCookieName.set(name);
        }
    }

    @Override
    public String getSessionCookieName() {
        return sessionCookieName.get();
    }

    @Override
    public Session getSession(Request request, String requestedSessionId) {
        if (requestedSessionId == null) {
            return null;
        }

        OXSession session = sessions.getIfPresent(requestedSessionId);
        if (session == null) {
            removeInvalidSessionCookie(request, requestedSessionId);
            return null;
        }

        if (session.isValid()) {
            if (request.isRequestedSessionIdFromURL() && !hasSessionCookie(request, requestedSessionId)) {
                Response response = request.response;
                response.addCookie(createSessionCookie(request, session.getIdInternal()));
            }
            return session;
        }

        removeInvalidSessionCookie(request, requestedSessionId);
        return null;
    }

    @Override
    public Session createSession(Request request) {
        String requestedSessionId = createSessionID();
        OXSession session = new OXSession(requestedSessionId, sessions);
        sessions.put(requestedSessionId, session);
        LogProperties.put(LogProperties.Name.GRIZZLY_HTTP_SESSION, requestedSessionId);
        return session;
    }

    @Override
    public String changeSessionId(Request request, Session session) {
        String oldSessionId = session.getIdInternal();
        String newSessionId = String.valueOf(generateRandomLong());

        session.setIdInternal(newSessionId);

        sessions.invalidate(oldSessionId);
        sessions.put(newSessionId, (OXSession) session);
        return oldSessionId;
    }

    @Override
    public void configureSessionCookie(Request request, Cookie cookie) {
        cookie.setPath("/");

        String serverName  = request.getServerName();
        String domain = Cookies.getDomainValue(null == serverName ? determineServerNameByLogProperty() : serverName);
        if (domain != null) {
            cookie.setDomain(domain);
        }

        /*
         * Toggle the security of the cookie on when we are dealing with a https request or the forceHttps config option is true e.g. when A
         * proxy in front of apache terminates ssl. The exception from forced https is a request from the local LAN.
         */
        {
            boolean isCookieSecure = request.isSecure() || (grizzlyConfig.isForceHttps() && !Cookies.isLocalLan(request.getServerName()));
            cookie.setSecure(isCookieSecure);
        }

        /*
         * Always set cookie max age to configured maximum value, otherwise it may break 'stay signed in' sessions
         */
        cookie.setMaxAge(grizzlyConfig.getCookieMaxAge());
    }

    private static String determineServerNameByLogProperty() {
        return LogProperties.getLogProperty(LogProperties.Name.GRIZZLY_SERVER_NAME);
    }

    /**
     * Create a new JSessioID String that consists of a:
     * <pre>
     *  &lt;random&gt; + ("-" + &lt;the urlencoded domain of this server&gt;)? + "." + &lt;backendRoute&gt;
     * </pre>
     * Example:
     * <pre>
     *  367190855121044669-open%2Dxchange%2Ecom.OX0
     * </pre>
     *
     * @return A new JSessionId value as String
     */
    private String createSessionID() {
        return Long.toString(generateRandomLong());
    }

    /**
     * Creates a new JSessionIdCookie based on a sessionID and the server configuration.
     *
     * @param sessionID The sessionId to use for cookie generation
     * @return The new JSessionId Cookie
     */
    private Cookie createSessionCookie(Request request, String sessionID) {
        Cookie jSessionIdCookie = new Cookie(request.obtainSessionCookieName(), sessionID);
        configureSessionCookie(request, jSessionIdCookie);
        return jSessionIdCookie;
    }

    /**
     * Checks if this request has a session cookie associated with specified session identifier.
     *
     * @param sessionID The session identifier to use for look-up
     * @return <code>true</code> if this request contains such a session cookie; otherwise <code>false</code>
     */
    private static boolean hasSessionCookie(Request request, String sessionID) {
        Cookie[] cookies = request.getCookies();
        if (null == cookies || cookies.length <= 0) {
            return false;
        }

        String sessionCookieName = request.obtainSessionCookieName();
        for (Cookie cookie : cookies) {
            if (sessionCookieName.equals(cookie.getName()) && sessionID.equals(cookie.getValue())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Remove invalid JSession cookie used in the Request. Cookies are invalid when:
     *
     * @param invalidSessionId The invalid sessionId requested by the browser/cookie
     */
    private static void removeInvalidSessionCookie(Request request, String invalidSessionId) {
        Cookie[] cookies = request.getCookies();
        if (null == cookies || cookies.length <= 0) {
            return;
        }

        String sessionCookieName = request.obtainSessionCookieName();
        Response response = request.response;
        for (Cookie cookie : cookies) {
            if (cookie.getName().startsWith(sessionCookieName)) {
                if (cookie.getValue().equals(invalidSessionId)) {
                    response.addCookie(createinvalidationCookie(cookie));

                    String domain = Cookies.getDomainValue(request.getServerName());
                    response.addCookie(createinvalidationCookie(cookie, null == domain ? request.getServerName() : domain));
                    break;
                }
            }
        }
    }

    /**
     * Generate a invalidation Cookie that can be added to the response to prompt the browser to remove that cookie.
     *
     * @param invalidCookie The invalid Cookie from the incoming request
     * @return an invalidation Cookie that can be added to the response to prompt the browser to remove that cookie.
     */
    private static Cookie createinvalidationCookie(Cookie invalidCookie) {
        Cookie invalidationCookie = new Cookie(invalidCookie.getName(), invalidCookie.getValue());
        invalidationCookie.setPath("/");
        invalidationCookie.setMaxAge(0);
        return invalidationCookie;
    }

    /**
     * Generate a invalidation Cookie with domain that can be added to the response to prompt the browser to remove that cookie. The domain
     * is needed for IE to change/remove cookies.
     *
     * @param invalidCookie The invalid Cookie from the incoming request
     * @param domain The domain to set in the invalidation cookie
     * @return an invalidation Cookie that can be added to the response to prompt the browser to remove that cookie.
     */
    private static Cookie createinvalidationCookie(Cookie invalidCookie, String domain) {
        Cookie invalidationCookieWithDomain = createinvalidationCookie(invalidCookie);
        invalidationCookieWithDomain.setDomain(domain);
        return invalidationCookieWithDomain;
    }

    /**
     * Returns pseudorandom positive long value.
     */
    private long generateRandomLong() {
        return (rnd.nextLong() & 0x7FFFFFFFFFFFFFFFL);
    }

}
