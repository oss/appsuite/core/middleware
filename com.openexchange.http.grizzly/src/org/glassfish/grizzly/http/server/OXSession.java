/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package org.glassfish.grizzly.http.server;

import com.github.benmanes.caffeine.cache.Cache;

/**
 * {@link OXSession} - An HTTP session.
 * <p>
 * Extends simple {@link Session} by letting session collection when a session is set to invalid.
 *
 * @author Thorben Betten
 */
public class OXSession extends Session {

    private final Cache<String, OXSession> sessions;

    /**
     * Initializes a new {@link OXSession}.
     *
     * @param sessions The session collection maintained by session manager
     */
    public OXSession(Cache<String, OXSession> sessions) {
        super();
        this.sessions = sessions;
    }

    /**
     * Initializes a new {@link OXSession}.
     *
     * @param id The session identifier
     * @param sessions The session collection maintained by session manager
     */
    public OXSession(String id, Cache<String, OXSession> sessions) {
        super(id);
        this.sessions = sessions;
    }

    @Override
    public void setValid(boolean isValid) {
        super.setValid(isValid);
        if (!isValid) {
            sessions.invalidate(getIdInternal());
        }
    }

}
