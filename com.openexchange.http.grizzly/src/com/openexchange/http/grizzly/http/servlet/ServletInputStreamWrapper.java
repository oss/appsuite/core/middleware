/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.http.grizzly.http.servlet;

import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import com.openexchange.exception.ExceptionUtils;

/**
 * {@link ServletInputStreamWrapper} - A wrapper for an instance of <code>ServletInputStream</code>.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ServletInputStreamWrapper extends ServletInputStream {

    private final ServletInputStream inputStream;

    /**
     * Initializes a new instance of {@link ServletInputStreamWrapper}.
     *
     * @param inputStream The instance of <code>ServletInputStream</code>
     */
    public ServletInputStreamWrapper(ServletInputStream inputStream) {
        super();
        this.inputStream = inputStream;
    }

    /**
     * Handles specified instance of <code>java.io.IOException</code>.
     * <p>
     * Checks for a possible <code>java.util.concurrent.TimeoutException</code> in exception chain and if present a read timeout is assumed
     * while awaiting incoming data from client.
     *
     * @param e The instance of <code>java.io.IOException</code>
     * @return The appropriate instance of <code>java.io.IOException</code>
     */
    private static IOException handleIOException(IOException e)  {
        if (ExceptionUtils.extractFrom(e, java.util.concurrent.TimeoutException.class) != null) {
            // Assume read timeout while reading client data
            return new IOException("Read timed out. Waited too long for incoming data from client. Please check property \"com.openexchange.http.grizzly.readTimeoutMillis\" if it specifies a sufficient read timeout in milliseconds.", e);
        }
        return e;
    }

    @Override
    public int readLine(byte[] b, int off, int len) throws IOException {
        try {
            return inputStream.readLine(b, off, len);
        } catch (IOException e) {
            throw handleIOException(e);
        }
    }

    @Override
    public int read() throws IOException {
        try {
            return inputStream.read();
        } catch (IOException e) {
            throw handleIOException(e);
        }
    }

    @Override
    public boolean isFinished() {
        return inputStream.isFinished();
    }

    @Override
    public boolean isReady() {
        return inputStream.isReady();
    }

    @Override
    public int read(byte[] b) throws IOException {
        try {
            return inputStream.read(b);
        } catch (IOException e) {
            throw handleIOException(e);
        }
    }

    @Override
    public void setReadListener(ReadListener readListener) {
        inputStream.setReadListener(readListener);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        try {
            return inputStream.read(b, off, len);
        } catch (IOException e) {
            throw handleIOException(e);
        }
    }

    @Override
    public String toString() {
        return inputStream.toString();
    }

    @Override
    public byte[] readAllBytes() throws IOException {
        try {
            return inputStream.readAllBytes();
        } catch (IOException e) {
            throw handleIOException(e);
        }
    }

    @Override
    public byte[] readNBytes(int len) throws IOException {
        try {
            return inputStream.readNBytes(len);
        } catch (IOException e) {
            throw handleIOException(e);
        }
    }

    @Override
    public int readNBytes(byte[] b, int off, int len) throws IOException {
        try {
            return inputStream.readNBytes(b, off, len);
        } catch (IOException e) {
            throw handleIOException(e);
        }
    }

    @Override
    public long skip(long n) throws IOException {
        try {
            return inputStream.skip(n);
        } catch (IOException e) {
            throw handleIOException(e);
        }
    }

    @Override
    public void skipNBytes(long n) throws IOException {
        try {
            inputStream.skipNBytes(n);
        } catch (IOException e) {
            throw handleIOException(e);
        }
    }

    @Override
    public int available() throws IOException {
        try {
            return inputStream.available();
        } catch (IOException e) {
            throw handleIOException(e);
        }
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }

    @Override
    public void mark(int readlimit) {
        inputStream.mark(readlimit);
    }

    @Override
    public void reset() throws IOException {
        inputStream.reset();
    }

    @Override
    public boolean markSupported() {
        return inputStream.markSupported();
    }

    @Override
    public long transferTo(OutputStream out) throws IOException {
        try {
            return inputStream.transferTo(out);
        } catch (IOException e) {
            throw handleIOException(e);
        }
    }

}
