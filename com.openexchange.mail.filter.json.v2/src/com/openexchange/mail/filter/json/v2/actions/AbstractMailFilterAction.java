/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.filter.json.v2.actions;

import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.EnqueuableAJAXActionService;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.jsieve.commands.Rule;
import com.openexchange.jsieve.commands.RuleComment;
import com.openexchange.mailfilter.Credentials;
import com.openexchange.mailfilter.MailFilterCredentialsFactory;
import com.openexchange.mailfilter.exceptions.MailFilterExceptionCode;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link AbstractMailFilterAction}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v7.8.4
 */
public abstract class AbstractMailFilterAction implements EnqueuableAJAXActionService {

    private static final String PARAM_USERNAME = "username";

    /** The module as used in restricted scopes for OAuth, aligned to <code>com.openexchange.mail.json.actions.AbstractMailAction.MODULE</code> */
    protected static final String RESTRICTED_MODULE = "mail";

    /** The service look-up */
    protected final ServiceLookup services;

    /**
     * Initializes a new {@link AbstractMailFilterAction}.
     */
    protected AbstractMailFilterAction(ServiceLookup services) {
        super();
        this.services = services;
    }

    protected static JSONObject getJSONBody(Object data) throws OXException {

        if (!(data instanceof JSONObject)) {
            throw AjaxExceptionCodes.INVALID_REQUEST_BODY.create(JSONObject.class.getSimpleName(), data.getClass().getSimpleName());
        }

        return (JSONObject) data;
    }

    protected static JSONArray getJSONArrayBody(Object data) throws OXException {

        if (!(data instanceof JSONArray)) {
            throw AjaxExceptionCodes.INVALID_REQUEST_BODY.create(JSONObject.class.getSimpleName(), data.getClass().getSimpleName());
        }

        return (JSONArray) data;
    }

    protected static Integer getUniqueId(final JSONObject json) throws OXException {
        int id = json.optInt("id", -1);
        if (id < 0) {
            throw MailFilterExceptionCode.MISSING_PARAMETER.create("id");
        }
        return Integer.valueOf(id);
    }

    /**
     * Acquires the mail filter credentials for given session and request.
     *
     * @param session The session
     * @param request The mail filter request
     * @return The mail filter credentials
     * @throws OXException If required service is absent
     */
    protected Credentials getCredentials(Session session, AJAXRequestData request) throws OXException {
        String userName = getUserName(request);
        MailFilterCredentialsFactory credentialsFactory = services.getServiceSafe(MailFilterCredentialsFactory.class);
        return Strings.isNotEmpty(userName) ? credentialsFactory.createCredentialsWithUserName(session, userName) : credentialsFactory.createCredentials(session);
    }

    private static String getUserName(AJAXRequestData request) {
        return request.getParameter(PARAM_USERNAME);
    }

    @Override
    public Result isEnqueueable(AJAXRequestData request, ServerSession session) throws OXException {
        return EnqueuableAJAXActionService.resultFor(false);
    }

    /**
     * Sets the rule's metadata, i.e. the update timestamp and the source of update (client IP)
     *
     * @param rule The rule
     * @param session The session
     */
    static void setMetadata(Rule rule, ServerSession session) {
        if (rule.getRuleComment() == null) {
            rule.setRuleComments(new RuleComment(""));
        }
        rule.getRuleComment().setUpdateTimestamp(System.currentTimeMillis());
        rule.getRuleComment().setSourceOfUpdate(session.getLocalIp());
    }

}
