/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.provider.composition.impl;

import static com.openexchange.chronos.provider.CalendarProviders.getCapabilityName;
import static com.openexchange.chronos.provider.CalendarProviders.getEnabledPropertyName;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.capabilities.CapabilityChecker;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.chronos.provider.CalendarProvider;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.config.cascade.ConfigViews;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.RankingAwareNearRegistryServiceTracker;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tools.session.ServerSessionAdapter;

/**
 * {@link CalendarProviderTracker}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @since v7.10.0
 */
public class CalendarProviderTracker extends RankingAwareNearRegistryServiceTracker<CalendarProvider> {

    private static final Logger LOG = LoggerFactory.getLogger(CalendarProviderTracker.class);

    private final ConcurrentMap<String, ServiceRegistration<CapabilityChecker>> checkerRegistrations;
    private final ConcurrentMap<String, CapabilityChecker> checkersByCapabilityName;
    final ServiceLookup services;

    /**
     * Initializes a new {@link CalendarProviderTracker}.
     *
     * @param context The bundle context
     * @param services A service lookup reference
     */
    public CalendarProviderTracker(BundleContext context, ServiceLookup services) {
        super(context, CalendarProvider.class);
        this.checkerRegistrations = new ConcurrentHashMap<String, ServiceRegistration<CapabilityChecker>>();
        this.checkersByCapabilityName = new ConcurrentHashMap<>();
        this.services = services;
    }

    /**
     * Gets a value indicating whether the session's user has the required capability for a specific calendar provider or not.
     *
     * @param session The session to check
     * @param providerId The identifier of the calendar provider to check the capabilities for
     * @return <code>true</code> if the user has the required capability, <code>false</code>, otherwise
     */
    public boolean hasCapability(Session session, String providerId) throws OXException {
        String capabilityName = getCapabilityName(providerId);
        CapabilityChecker capabilityChecker = checkersByCapabilityName.get(capabilityName);
        if (null == capabilityChecker) {
            LOG.debug("No capability checker known for \"{}\", assuming absent.", capabilityName);
            return false;
        }
        return capabilityChecker.isEnabled(capabilityName, session);
    }

    @Override
    protected void onServiceAdded(CalendarProvider provider) {
        /*
         * declare capability for calendar provider
         */
        String capabilityName = getCapabilityName(provider);
        services.getService(CapabilityService.class).declareCapability(capabilityName);
        /*
         * register an appropriate capability checker
         */
        CapabilityChecker capabilityChecker = (capability, session) -> {
            if (capabilityName.equals(capability)) {
                ServerSession serverSession = ServerSessionAdapter.valueOf(session);
                if (serverSession.isAnonymous()) {
                    return false;
                }
                if (false == serverSession.getUserPermissionBits().hasCalendar()) {
                    return false;
                }
                ConfigView configView = services.getServiceSafe(ConfigViewFactory.class).getView(session.getUserId(), session.getContextId());
                return ConfigViews.getDefinedBoolPropertyFrom(getEnabledPropertyName(provider), provider.getDefaultEnabled(), configView) && provider.isAvailable(session);
            }
            return true;
        };
        Dictionary<String, Object> serviceProperties = new Hashtable<String, Object>(1);
        serviceProperties.put(CapabilityChecker.PROPERTY_CAPABILITIES, capabilityName);
        ServiceRegistration<CapabilityChecker> checkerRegistration = context.registerService(CapabilityChecker.class, capabilityChecker, serviceProperties);
        if (null != checkerRegistrations.putIfAbsent(provider.getId(), checkerRegistration)) {
            checkerRegistration.unregister();
        } else {
            checkersByCapabilityName.put(capabilityName, capabilityChecker);
        }
    }

    @Override
    protected void onServiceRemoved(CalendarProvider provider) {
        /*
         * unregister capability checker for calendar provider
         */
        String capabilityName = getCapabilityName(provider);
        ServiceRegistration<CapabilityChecker> checkerRegistration = checkerRegistrations.remove(provider.getId());
        if (null != checkerRegistration) {
            checkerRegistration.unregister();
            checkersByCapabilityName.remove(capabilityName);
        }
        /*
         * undeclare capability for calendar provider
         */
        services.getService(CapabilityService.class).undeclareCapability(capabilityName);
    }

}
