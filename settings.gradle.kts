pluginManagement {
    repositories {
        maven {
            url = uri("https://artifactory.production.cloud.oxoe.io/artifactory/libs-release/")
        }
        gradlePluginPortal()
        maven {
            name = "openapiResolver"
            url = uri("https://gitlab.open-xchange.com/api/v4/projects/2126/packages/maven")
        }
        maven {
            name = "openapiProcessor"
            url = uri("https://gitlab.open-xchange.com/api/v4/projects/4019/packages/maven")
        }
        maven {
            name = "openapiReplacer"
            url = uri("https://gitlab.open-xchange.com/api/v4/projects/4014/packages/maven")
        }
        maven {
            // Until the resolution of https://gitlab.com/gitlab-org/gitlab/-/issues/383537,
            // it is necessary to configure an access token for artifacts from the engineering/gradle package registry
            name = "gitlab"
            url = uri("https://gitlab.open-xchange.com/api/v4/groups/101/-/packages/maven")
            credentials(PasswordCredentials::class.java)
            authentication {
                create("basic", BasicAuthentication::class.java)
            }
        }
    }
}

rootProject.name = "backend"

buildscript {
    repositories {
        mavenCentral()
        maven {
            // Until the resolution of https://gitlab.com/gitlab-org/gitlab/-/issues/383537,
            // it is necessary to configure an access token for artifacts from the engineering/gradle package registry
            name = "gitlab"
            url = uri("https://gitlab.open-xchange.com/api/v4/groups/225/-/packages/maven")
            credentials(PasswordCredentials::class.java)
            authentication {
                create("basic", BasicAuthentication::class.java)
            }
        }
    }
    dependencies {
        classpath("com.openexchange.build:projectset:3.0.0")
    }
}

// Just for testing some newly developed plugin feature
//includeBuild("../../engineering/gradle/<plugin>")
apply {
    plugin("com.openexchange.build.projectset")
}

include("com.openexchange.test")

include(":drive-api")
project(":drive-api").projectDir = File("http-api/drive_api")
include("http-api")
project(":http-api").projectDir = File("http-api/http_api")
include("rest-api")
project(":rest-api").projectDir = File("http-api/rest_api")
include("openexchange-test")

include("documentation") // subdirectory command_line_tools provides man pages

include("properties-documentation")
project(":properties-documentation").projectDir = File("documentation-generic/config")

// TODO enable later when JARs will be removed from com.openexchange.bundles
// includeBuild("target-platform")
