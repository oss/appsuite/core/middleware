/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl.tasks.provider;

import com.openexchange.admin.async.AsyncTask;
import com.openexchange.admin.async.impl.ExecutableAsyncTask;
import com.openexchange.admin.async.impl.storage.AsyncTaskStorage;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.config.lean.Property;
import com.openexchange.exception.OXException;
import static com.openexchange.java.Autoboxing.B;
import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * {@link AsyncTaskServiceImplTest} contains unit tests for the {@link AsyncTaskServiceImpl}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class AsyncTaskServiceImplTest {

    @Mock
    LeanConfigurationService configMock;

    @SuppressWarnings("boxing")
    @BeforeEach
    private void setup() {
        MockitoAnnotations.openMocks(this);
        Mockito.when(configMock.getIntProperty(any())).thenAnswer(invocation -> {
            Property argument = invocation.getArgument(0);
            return argument.getDefaultValue(Integer.class);
        });
    }

    @Test
    public void testCreate() throws OXException {
        AsyncTaskStorage mockStorage = Mockito.mock(AsyncTaskStorage.class);
        AsyncTaskServiceImpl service = new AsyncTaskServiceImpl(mockStorage, configMock);
        when(mockStorage.createTask(any())).thenReturn(Optional.of(I(1)));

        service.createTask(AsyncTask.builder()
                                    .withType("abc")
                                    .withEntityId("a@b")
                                    .build());

        Mockito.verify(mockStorage).createTask(any());
    }

    @Test
    public void testGetDueTasks() throws OXException {
        AsyncTaskStorage mockStorage = Mockito.mock(AsyncTaskStorage.class);
        AsyncTaskServiceImpl service = new AsyncTaskServiceImpl(mockStorage, configMock);
        when(mockStorage.getDueTasks(anyInt(), anyInt())).thenReturn(Collections.emptyList());
        List<ExecutableAsyncTask> tasks = service.getDueTasks();
        assertTrue(tasks.isEmpty());
        Mockito.verify(mockStorage).getDueTasks(anyInt(), anyInt());
    }

    @Test
    public void testUpdateAndRefresh() throws OXException {
        AsyncTaskStorage mockStorage = Mockito.mock(AsyncTaskStorage.class);
        AsyncTaskServiceImpl service = new AsyncTaskServiceImpl(mockStorage, configMock);

        // Test update
        when(B(mockStorage.updateTask(any(), anyBoolean()))).thenReturn(Boolean.TRUE);
        boolean updated = service.updateTask(AsyncTask.builder()
                                                      .withType("abc")
                                                      .withEntityId("a@b")
                                                      .build());
        assertTrue(updated);
        Mockito.verify(mockStorage).updateTask(any(), anyBoolean());

        // Test refresh
        service.refreshTask(I(1));
        Mockito.verify(mockStorage).refreshClaim(I(1));

    }

    @Test
    public void testCreateWithRunning() throws OXException {
        AsyncTaskStorage mockStorage = Mockito.mock(AsyncTaskStorage.class);
        // Make sure the filler can run but is noop
        when(mockStorage.getDueTasks(anyInt(), anyInt())).thenReturn(Collections.emptyList());
        when(mockStorage.createTask(any())).thenReturn(Optional.of(I(1)));

        AsyncTaskServiceImpl service = new AsyncTaskServiceImpl(mockStorage, configMock);
        service.start();
        service.createTask(AsyncTask.builder()
                                    .withType("abc")
                                    .withEntityId("a@b")
                                    .build());
        Mockito.verify(mockStorage).createTask(any());
        assertNotNull(service.getNextTask());
        service.stop();
    }

}
