/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl.storage;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import com.openexchange.admin.async.AsyncTask;
import com.openexchange.admin.async.AsyncTask.Builder;
import com.openexchange.admin.async.AsyncTaskState;
import com.openexchange.admin.async.impl.AsyncTaskExceptionCodes;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;

/**
 * {@link AsyncTaskStorage} is a sql based storage for async tasks
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class AsyncTaskStorage {

    private static final byte[] CLAIM = createClaim();

    // ------------- The prepared sql statements -------------

    private static final String INSERT_STMT = "INSERT INTO async_tasks (`entityId`, `type`, `args`, `state`) "
        + "VALUES (?, ?, ?, ?)";

    private static final String UPDATE_STMT = "UPDATE async_tasks "
        + "SET `state`=?, `claim`=?, `retryCount`=?, `error`=? "
        + "WHERE `entityId`=? AND `type`=? AND (`claim` IS NULL OR `claim`=?)";

    private static final String UPDATE_FORCE_STMT = "UPDATE async_tasks "
        + "SET `state`=?, `claim`=?, `retryCount`=?, `error`=? "
        + "WHERE `entityId`=? AND `type`=?";

    private static final String WHERE_NORMAL_DUE = "`claim` IS NULL AND `state`='%s'".formatted(AsyncTaskState.SCHEDULED.name());
    private static final String WHERE_OVERDUE = "`claim` IS NOT NULL AND "
        + "`state`<>'" + AsyncTaskState.FINISHED.name() + "' AND "
        + "`retryCount` <= ? AND "
        + "lastUpdate < now() - INTERVAL ? MINUTE";

    private static final String CLAIM_STMT = "UPDATE async_tasks SET `claim`=?, `state`=? "
        + "WHERE `id`=? "
        + "AND ((" + WHERE_NORMAL_DUE + ") "
        + "OR (" + WHERE_OVERDUE + "))";

    private static final String REFRESH_STMT = "UPDATE async_tasks SET `lastUpdate`=NOW() "
        + "WHERE `id`=? AND `claim`=?";

    private static final String SELECT_DUE_STMT = "SELECT `id` "
        + "FROM async_tasks "
        + "WHERE (" + WHERE_NORMAL_DUE + ") "
        + "OR (" + WHERE_OVERDUE + ")";

    private static final String SELECT_BY_ID_STMT = "SELECT `id`, `entityId`, `type`, `args`, `state`, `claim`, `retryCount`, `error` "
        + "FROM async_tasks "
        + "WHERE `id`=?";

    private static final String DELETE_OLD_ENTRIES = "DELETE FROM async_tasks WHERE DATEDIFF(NOW(), lastUpdate) > ?";

    // ------------- End of prepared sql statements -------------

    private final DatabaseService service;

    /**
     * Initializes a new {@link AsyncTaskStorage}.
     *
     * @param service The database service to use
     */
    public AsyncTaskStorage(DatabaseService service) {
        super();
        this.service = service;
    }

    /**
     * Creates a new task for the given entity id and type if possible.
     *
     * The task will be created without a claim and with the {@link AsyncTaskState#SCHEDULED} state.
     *
     * @param task The task to create
     * @return The optional id of the new task
     * @throws OXException in case of errors
     */
    public Optional<Integer> createTask(AsyncTask task) throws OXException {
        Connection con = service.getWritable();
        boolean readOnly = true;
        try {
            Optional<Integer> result = insert(con, task);
            readOnly = result.isEmpty();
            return result;
        } catch (SQLException e) {
            if (Databases.isPrimaryKeyConflictInMySQL(e)) {
                return Optional.empty();
            }
            throw AsyncTaskExceptionCodes.SQL_ERROR.create(e);
        } finally {
            if (readOnly) {
                service.backWritableAfterReading(con);
            } else {
                service.backWritable(con);
            }
        }
    }

    /**
     * Updates an existing task.
     *
     * Fields that can be updated are:
     * - The claim (if not claimed yet)
     * - The state
     * - The retry count
     * - The error
     *
     * @param task The task containing the updated fields
     * @param forceClaim Whether to force claim or not
     * @return <code>true</code> if the task was updated successfully, <code>false</code> if no tasks exists or if it was already claimed by another node
     * @throws OXException
     */
    public boolean updateTask(AsyncTask task, boolean forceClaim) throws OXException {
        Connection con = service.getWritable();
        boolean readOnly = true;
        try {
            boolean result = update(con, task, forceClaim);
            readOnly = false == result;
            return result;
        } catch (SQLException e) {
            throw AsyncTaskExceptionCodes.SQL_ERROR.create(e);
        } finally {
            if (readOnly) {
                service.backWritableAfterReading(con);
            } else {
                service.backWritable(con);
            }
        }
    }

    /**
     * Get the id of all task with a retry count lower than the given maximum and are due to the given retry interval
     *
     * @param maxRetries Excludes tasks with a retry count greater than this
     * @param retryIntervalInMinutes The retry interval in minutes which excludes tasks with a timestamp earlier than now - this
     * @return The list of ids of due task
     * @throws OXException in case of errors
     */
    public List<Integer> getDueTasks(int maxRetries, int retryIntervalInMinutes) throws OXException {
        Connection con = service.getReadOnly();
        try {
            return getDueTasks(con, maxRetries, retryIntervalInMinutes);
        } catch (SQLException e) {
            throw AsyncTaskExceptionCodes.SQL_ERROR.create(e);
        } finally {
            service.backReadOnly(con);
        }

    }

    /**
     * Tries to claim a given task
     *
     * @param id The id of the task to claim
     * @param maxRetries Only claims the task if the retry count is lower than this
     * @param retryIntervalInMinutes Only claims the task if it is still older than now + this interval in minutes
     * @return The claimed task if successful
     * @throws OXException in case of errors
     */
    public Optional<AsyncTask> claimTask(Integer id, int maxRetries, int retryIntervalInMinutes) throws OXException {
        Connection con = service.getWritable();
        boolean readOnly = true;
        try {
            Optional<AsyncTask> result = claimTask(con, id, maxRetries, retryIntervalInMinutes);
            readOnly = result.isEmpty();
            return result;
        } catch (SQLException e) {
            throw AsyncTaskExceptionCodes.SQL_ERROR.create(e);
        } finally {
            if (readOnly) {
                service.backWritableAfterReading(con);
            } else {
                service.backWritable(con);
            }
        }
    }

    /**
     * Updates the timestamped of an already claimed task
     *
     * @param id The id of the task
     * @throws OXException in case of errors
     */
    public void refreshClaim(Integer id) throws OXException {
        Connection con = service.getWritable();
        boolean readOnly = true;
        try {
            refreshClaim(con, id);
        } catch (SQLException e) {
            throw AsyncTaskExceptionCodes.SQL_ERROR.create(e);
        } finally {
            if (readOnly) {
                service.backWritableAfterReading(con);
            } else {
                service.backWritable(con);
            }
        }
    }

    /**
     * Deletes old entries
     *
     * @param retentionDays The maximum retention days for old entries
     * @return The number of deleted tasks
     */
    public int deleteOldTasks(int retentionDays) throws OXException {
        Connection con = service.getWritable();
        boolean readOnly = true;
        try {
            int deleteTasks = deleteOldTasks(con, retentionDays);
            readOnly = deleteTasks > 0;
            return deleteTasks;
        } catch (SQLException e) {
            throw AsyncTaskExceptionCodes.SQL_ERROR.create(e);
        } finally {
            if (readOnly) {
                service.backWritableAfterReading(con);
            } else {
                service.backWritable(con);
            }
        }
    }

    // --------------------- private methods -------------------

    /**
     * Deletes old entries
     *
     * @param con The write connection to use
     * @param retentionDays The maximum retention days for old entries
     * @return the number of deleted tasks
     */
    private int deleteOldTasks(Connection con, int retentionDays) throws SQLException {
        try (PreparedStatement stmt = con.prepareStatement(DELETE_OLD_ENTRIES)) {
            stmt.setInt(1, retentionDays);
            return stmt.executeUpdate();
        }
    }

    /**
     * Refreshes the timer of a task
     *
     * @param con The connnection
     * @param id The id of the task to refresh
     */
    private void refreshClaim(Connection con, Integer id) throws SQLException {
        try (PreparedStatement stmt = con.prepareStatement(REFRESH_STMT)) {
            int index = 1;
            stmt.setInt(index++, i(id));
            // Set claim as condition and updated value
            stmt.setBytes(index++, CLAIM);
            stmt.executeUpdate();
        }
    }

    /**
     * Tries to claim the given task.
     *
     * The task is only claimed in case it is not claimed yet or if it is overdue.
     * A task is overdue if the last update was longer ago than the retryIntervalInMinutes and its retry count is smaller than maxRetries.
     *
     * @param con The connection to use
     * @param id The of the task to claim
     * @param maxRetries The maximum number of retries
     * @param retryIntervalInMinutes The retry interval in minutes
     * @return The task if claimed
     * @throws SQLException in case of errors
     */
    private Optional<AsyncTask> claimTask(Connection con, Integer id, int maxRetries, int retryIntervalInMinutes) throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement stmt = con.prepareStatement(CLAIM_STMT)) {
            int index = 1;
            stmt.setBytes(index++, CLAIM);
            stmt.setString(index++, AsyncTaskState.RUNNING.name());
            stmt.setInt(index++, i(id));
            stmt.setInt(index++, maxRetries <= 0 ? Integer.MAX_VALUE : maxRetries);
            stmt.setInt(index++, retryIntervalInMinutes);
            int updated = stmt.executeUpdate();
            if(updated<=0) {
                // Claim failed
                return Optional.empty();
            }
            return optTask(con, id);
        } finally {
            Databases.closeSQLStuff(rs);
        }
    }

    /**
     * Gets the task with the given id if possible
     *
     * @param con The connection to use
     * @param id The id of the task
     * @return The optional task
     * @throws SQLException in case of errors
     */
    private Optional<AsyncTask> optTask(Connection con, Integer id) throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID_STMT)) {
            stmt.setInt(1, i(id));
            rs = stmt.executeQuery();
            if (rs.next() == false) {
                return Optional.empty();
            }
            return Optional.of(fromResultSet(rs));
        } finally {
            Databases.closeSQLStuff(rs);
        }
    }

    /**
     * Gets due tasks by using the given {@link Connection}
     *
     * See {@link #getDueTasks(int, int)}
     *
     * @param con The connection to use
     * @param maxRetries The maximum number of retries
     * @param retryIntervalInMinutes The retry interval in minutes
     * @return The ids of dues tasks
     * @throws SQLException in case of errors
     */
    private List<Integer> getDueTasks(Connection con, int maxRetries, int retryIntervalInMinutes) throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement stmt = con.prepareStatement(SELECT_DUE_STMT)) {
            int index = 1;
            stmt.setInt(index++, maxRetries <= 0 ? Integer.MAX_VALUE : maxRetries);
            stmt.setInt(index++, retryIntervalInMinutes);
            rs = stmt.executeQuery();
            List<Integer> result = new ArrayList<>(rs.getFetchSize());
            while (rs.next()) {
                result.add(I(rs.getInt("id")));
            }
            return result;
        } finally {
            Databases.closeSQLStuff(rs);
        }
    }

    /**
     * Parses an async task from a given result set
     *
     * @param resultSet The result set
     * @return The parsed task
     * @throws SQLException
     */
    private AsyncTask fromResultSet(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String entityId = resultSet.getString("entityId");
        String type = resultSet.getString("type");
        String args = resultSet.getString("args");
        String state = resultSet.getString("state");
        int retryCount = resultSet.getInt("retryCount");
        String error = resultSet.getString("error");
        UUID claim = fromBytes(resultSet.getBytes("claim"));

        Builder builder = AsyncTask.builder();
        builder.withId(id);
        builder.withEntityId(entityId);
        builder.withType(type);
        builder.withArgs(args);
        builder.withState(AsyncTaskState.parse(state));
        builder.withRetryCount(retryCount);
        builder.withClaim(claim);
        builder.withError(error);
        return builder.build();
    }

    /**
     * Inserts a task
     *
     * @param con The connection to use
     * @param task The task to insert
     * @return The id of the task if successful
     * @throws SQLException in case of errors
     */
    private Optional<Integer> insert(Connection con, AsyncTask task) throws SQLException {
        try (PreparedStatement stmt = con.prepareStatement(INSERT_STMT, Statement.RETURN_GENERATED_KEYS)) {
            int index = 1;
            stmt.setString(index++, task.getEntityId());
            stmt.setString(index++, task.getType());
            stmt.setString(index++, task.getArgs());
            stmt.setString(index++, AsyncTaskState.SCHEDULED.name());
            int executeUpdate = stmt.executeUpdate();
            if(executeUpdate > 0) {
                return optKey(stmt);
            }
            return Optional.empty();
        }
    }

    /**
     * Updates an existing task
     *
     * @param con The connection to use
     * @param task The task to update
     * @param forceClaim Whether to force claim a task or not
     * @return <code>true</code> if the operation was successful, <code>false</code> otherwise
     * @throws SQLException in case of errors
     */
    private boolean update(Connection con, AsyncTask task, boolean forceClaim) throws SQLException {
        try (PreparedStatement stmt = con.prepareStatement(forceClaim ? UPDATE_FORCE_STMT : UPDATE_STMT)) {
            int index = 1;
            // update values
            stmt.setString(index++, task.getState().name());
            stmt.setBytes(index++, CLAIM);
            stmt.setInt(index++, task.getRetryCount());
            stmt.setString(index++, task.getError());

            // condition
            stmt.setString(index++, task.getEntityId());
            stmt.setString(index++, task.getType());
            if (forceClaim == false) {
                stmt.setBytes(index++, CLAIM);
            }
            int executeUpdate = stmt.executeUpdate();
            return executeUpdate > 0;
        }
    }

    /**
     * Gets the generated key of the given {@link Statement} if possible
     *
     * @param stmt The statement to get the generated key for
     * @return The optional key
     * @throws SQLException
     */
    private Optional<Integer> optKey(PreparedStatement stmt) throws SQLException {
        ResultSet keys = stmt.getGeneratedKeys();
        if (keys.next()) {
            return Optional.of(I(keys.getInt(1)));
        }
        return Optional.empty();
    }

    /**
     * Creates a new claim for this node
     *
     * @return The claim
     */
    private static byte[] createClaim() {
        UUID id = UUID.randomUUID();
        return ByteBuffer.allocate(16)
                         .putLong(id.getMostSignificantBits())
                         .putLong(id.getLeastSignificantBits())
                         .array();
    }

    /**
     * Creates a {@link UUID} from the given bytes
     *
     * @param uuidBytes The bytes to convert
     * @return The {@link UUID}
     */
    private static UUID fromBytes(byte[] uuidBytes) {
        try {
            ByteBuffer bb = ByteBuffer.wrap(uuidBytes);
            return new UUID(bb.getLong(), bb.getLong());
        } catch (BufferUnderflowException e) {
            // Should not happen
            throw new IllegalStateException(e);
        }
    }

}
