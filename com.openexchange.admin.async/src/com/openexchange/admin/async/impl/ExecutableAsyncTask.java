/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl;

import java.util.Optional;
import com.openexchange.admin.async.AsyncTask;
import com.openexchange.exception.OXException;

/**
 * {@link ExecutableAsyncTask} is a wrapper for {@link AsyncTask}s which allows to claim the task.
 *
 * This is useful for tasks which are not claimed yet
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public interface ExecutableAsyncTask {

    /**
     * Gets the id of this task
     *
     * @return The id
     */
    Integer getId();

    /**
     * Claims the task if possible
     *
     * @return The optionally claimed task
     * @throws OXException in case of errors
     */
    Optional<AsyncTask> claimTask() throws OXException;

}
