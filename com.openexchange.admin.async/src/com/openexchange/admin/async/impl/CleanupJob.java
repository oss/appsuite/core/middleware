/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.admin.async.impl.config.AdminAsyncProperties;
import com.openexchange.admin.async.impl.storage.AsyncTaskStorage;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;

/**
 * {@link CleanupJob} deletes old task entries. See {@link AdminAsyncProperties#CLEANUP_RETENTION_DAYS} for more infos.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class CleanupJob implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(CleanupJob.class);
    private final AsyncTaskStorage storage;
    private final LeanConfigurationService configurationService;

    /**
     * Initializes a new {@link CleanupJob}.
     *
     * @param storage The storage to use
     * @param configurationService The {@link LeanConfigurationService}
     */
    public CleanupJob(AsyncTaskStorage storage, LeanConfigurationService configurationService) {
        super();
        this.storage = storage;
        this.configurationService = configurationService;
    }

    @Override
    public void run() {
        try {
            LOG.info("Starting async task clean-up job");
            int deletedTasks = storage.deleteOldTasks(getRetentionDays());
            LOG.info("Async task clean-up job finished and cleaned-up {} tasks", deletedTasks);
        } catch (OXException e) {
            LOG.error("Unable to clean-up async tasks", e);
        }
    }

    /**
     * Gets the maximum retention days for tasks
     *
     * @return The maximum retention days
     */
    private int getRetentionDays() {
        return configurationService.getIntProperty(AdminAsyncProperties.CLEANUP_RETENTION_DAYS);
    }

}
