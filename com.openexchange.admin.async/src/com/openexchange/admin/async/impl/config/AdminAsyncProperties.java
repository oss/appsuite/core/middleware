/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl.config;

import static com.openexchange.java.Autoboxing.I;
import com.openexchange.config.lean.Property;

/**
 * {@link AdminAsyncProperties} contains properties for the async framework
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public enum AdminAsyncProperties implements Property {

    SCHEDULE("schedule", null),
    POOL_SIZE("pool.size", I(10)),
    REFRESH_INTERVAL("refresh.interval", I(5)),
    RETRY_LIMIT("retry.limit", I(5)),
    RETRY_INTERVAL("retry.interval", I(15)),
    REFILL_INTERVAL("refill.interval", I(5)),
    CLEANUP_RETENTION_DAYS("cleanup.retentionDays", I(365 * 2)),
    CLEANUP_INTERVAL_IN_DAYS("cleanup.interval", I(7))
    ;

    private static final String PREFIX = "com.openexchange.admin.async.";

    private final String fqn;
    private final Object defaultValue;

    /**
     * Initializes a new {@link AdminAsyncProperties}.
     *
     * @param name
     * @param defaultValue
     */
    private AdminAsyncProperties(String name, Object defaultValue) {
        this.fqn = PREFIX + name;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
