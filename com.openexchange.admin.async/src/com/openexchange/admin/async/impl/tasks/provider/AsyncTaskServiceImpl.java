/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl.tasks.provider;

import static com.openexchange.admin.async.impl.AsyncTaskExceptionCodes.TASK_ALREADY_EXISTS;
import com.openexchange.admin.async.AsyncTaskService;
import com.openexchange.admin.async.impl.ExecutableAsyncTask;
import com.openexchange.admin.async.impl.config.AdminAsyncProperties;
import com.openexchange.admin.async.impl.storage.AsyncTaskStorage;
import com.openexchange.admin.async.impl.tasks.DueTaskProvider;
import com.openexchange.admin.async.impl.tasks.ExecutableAsyncTaskImpl;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import java.util.List;
import com.openexchange.admin.async.AsyncTask;

/**
 * {@link AsyncTaskServiceImpl} is an implementation of the {@link AsyncTaskService} which also
 * provides the {@link DueTaskProvider} interface
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class AsyncTaskServiceImpl extends AbstractAsyncTaskQueue implements AsyncTaskService {

    private final AsyncTaskStorage storage;

    /**
     * Initializes a new {@link AsyncTaskServiceImpl}.
     *
     * @param storage The task storage
     * @param configurationService The configuration service
     */
    public AsyncTaskServiceImpl(AsyncTaskStorage storage, LeanConfigurationService configurationService) {
        super(configurationService);
        this.storage = storage;
    }

    @Override
    public Integer createTask(AsyncTask task) throws OXException {
        Integer result = storage.createTask(task)
                                .orElseThrow(() -> TASK_ALREADY_EXISTS.create(task.getEntityId(),
                                                                              task.getType()));

        // Try offering the task to the queue in case it is currently active
        offer(toExecuteTask(result));
        return result;
    }

    @Override
    public List<ExecutableAsyncTask> getDueTasks() throws OXException {
        return storage.getDueTasks(getRetryLimit(), getRetryIntervalInMinutes())
                      .stream()
                      .map(taskId -> toExecuteTask(taskId))
                      .toList();
    }

    /**
     * Updates the given task.
     *
     * This update will fail in case the task is already claimed
     *
     * @param task The task to update
     * @return <code>true</code> if the task was updated, <code>false</code> otherwise
     * @throws OXException in case of errors
     */
    public boolean updateTask(AsyncTask task) throws OXException {
        return updateTask(task, false);
    }

    /**
     * Updates the given task
     *
     * @param task The task to update
     * @param forceClaim Whether to force claim or not
     * @return <code>true</code> if the task was updated, <code>false</code> otherwise
     * @throws OXException in case of errors
     */
    public boolean updateTask(AsyncTask task, boolean forceClaim) throws OXException {
        return storage.updateTask(task, forceClaim);
    }

    /**
     * Refreshes the given task
     *
     * @param id The task id
     * @throws OXException in case of errors
     */
    public void refreshTask(Integer id) throws OXException {
        storage.refreshClaim(id);
    }

    // ----------------------- private methods -----------------------

    /**
     * Converts the task id to an {@link ExecutableAsyncTask}
     *
     * @param taskId The task id
     * @return The task
     */
    private ExecutableAsyncTask toExecuteTask(Integer taskId) {
        return new ExecutableAsyncTaskImpl(storage, taskId, getRetryLimit(), getRetryIntervalInMinutes());
    }

    /**
     * Gets the retry interval in minutes
     *
     * @return The interval
     */
    private int getRetryIntervalInMinutes() {
        return configurationService.getIntProperty(AdminAsyncProperties.RETRY_INTERVAL);
    }

    /**
     * Gets the max number of retries
     *
     * @return The max number of retries
     */
    private int getRetryLimit() {
        return configurationService.getIntProperty(AdminAsyncProperties.RETRY_LIMIT);
    }

}
