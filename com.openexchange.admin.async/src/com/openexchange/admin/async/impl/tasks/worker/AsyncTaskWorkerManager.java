/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl.tasks.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import com.openexchange.admin.async.impl.ExecutorRegistry;
import com.openexchange.admin.async.impl.config.AdminAsyncProperties;
import com.openexchange.admin.async.impl.tasks.provider.AsyncTaskServiceImpl;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.timer.TimerService;

/**
 * {@link AsyncTaskWorkerManager} manages a thread pool of {@link AsyncTaskWorker}s
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class AsyncTaskWorkerManager {

    private final AsyncTaskServiceImpl taskService;
    private final ExecutorRegistry registry;
    private final TimerService timerService;
    private final long refreshIntervalInMilliseconds;
    private final int threadPoolSize;
    private ExecutorService pool;
    private final List<AsyncTaskWorker> workers = new ArrayList<>();

    /**
     * Initializes a new {@link AsyncTaskWorkerManager}.
     *
     * @param configurationService The configuration service
     * @param registry The {@link ExecutorRegistry}
     * @param taskService The {@link AsyncTaskServiceImpl}
     * @param timerService The {@link TimerService}
     */
    public AsyncTaskWorkerManager(LeanConfigurationService configurationService,
                                  ExecutorRegistry registry,
                                  AsyncTaskServiceImpl taskService,
                                  TimerService timerService) {
        super();
        this.registry = registry;
        this.taskService = taskService;
        this.timerService = timerService;
        this.threadPoolSize = configurationService.getIntProperty(AdminAsyncProperties.POOL_SIZE);
        this.pool = Executors.newFixedThreadPool(threadPoolSize);
        int intervalInMinutes = configurationService.getIntProperty(AdminAsyncProperties.REFRESH_INTERVAL);
        this.refreshIntervalInMilliseconds = TimeUnit.MINUTES.toMillis(intervalInMinutes);
    }

    /**
     * Starts the worker threads
     */
    public synchronized void start() {
        if (workers.isEmpty() == false) {
            // Already started
            return;
        }
        if (pool.isShutdown()) {
            // Recreate pool after shutdown
            pool = Executors.newFixedThreadPool(threadPoolSize);
        }
        for (int i = threadPoolSize; i > 0; i--) {
            AsyncTaskWorker worker = new AsyncTaskWorker(taskService,
                                                         registry,
                                                         timerService,
                                                         refreshIntervalInMilliseconds);
            pool.execute(worker);
            workers.add(worker);
        }

    }

    /**
     * Stops the worker threads
     */
    public void stop() {
        workers.forEach(w -> w.stop());
        workers.clear();
        pool.shutdown();
    }

}
