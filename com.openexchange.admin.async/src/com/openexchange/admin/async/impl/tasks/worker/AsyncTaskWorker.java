/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl.tasks.worker;

import static com.openexchange.java.Autoboxing.I;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.admin.async.AsyncTask;
import com.openexchange.admin.async.AsyncTaskExecutor;
import com.openexchange.admin.async.AsyncTaskState;
import com.openexchange.admin.async.impl.ExecutableAsyncTask;
import com.openexchange.admin.async.impl.ExecutorRegistry;
import com.openexchange.admin.async.impl.tasks.provider.AsyncTaskServiceImpl;
import com.openexchange.exception.OXException;
import com.openexchange.timer.TimerService;

/**
 * {@link AsyncTaskWorker} is a worker which claims new {@link AsyncTask}s and executes them
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class AsyncTaskWorker implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(AsyncTaskWorker.class);

    private final AsyncTaskServiceImpl taskService;
    private final ExecutorRegistry registry;
    private final TimerService timerService;
    private final long refreshIntervalInMilliseconds;

    private boolean running = true;

    /**
     * Initializes a new {@link AsyncTaskWorker}.
     *
     * @param taskService The task service
     * @param registry The executor registry
     * @param timerService The timer service
     * @param refreshIntervalInMilliseconds The refresh interval in milliseconds
     */
    public AsyncTaskWorker(AsyncTaskServiceImpl taskService,
                           ExecutorRegistry registry,
                           TimerService timerService,
                           long refreshIntervalInMilliseconds) {
        super();
        this.taskService = taskService;
        this.registry = registry;
        this.timerService = timerService;
        this.refreshIntervalInMilliseconds = refreshIntervalInMilliseconds;
    }

    @Override
    public void run() {
        while (running) {
            ExecutableAsyncTask task = taskService.getNextTask();
            if (task == null) {
                // Task Provider is shutting down
                continue;
            }
            try {
                task.claimTask().ifPresent(t -> findExecutorAndExecute(t));
            } catch (OXException e) {
                LOG.error("Error while claiming task with id '{}'", task.getId(), e);
            }
        }
    }

    /**
     * Stops the worker
     */
    public void stop() {
        running = false;
    }

    // ------------------- private methods ----------------------

    /**
     * Finds the relevant {@link AsyncTaskExecutor} and executes the given task with it
     *
     * @param task The task to execute
     */
    private void findExecutorAndExecute(AsyncTask task) {
        Optional<AsyncTaskExecutor> optExecutor = registry.optExecutor(task.getType());
        if (optExecutor.isEmpty()) {
            LOG.warn("Missing task executor for task type '%s'".formatted(task.getType()));
            return;
        }
        execute(optExecutor.get(), task);
    }

    /**
     * Executes the given task with the given {@link AsyncTaskExecutor}
     *
     * @param executor The {@link AsyncTaskExecutor} to use
     * @param task The task to execute
     */
    private void execute(AsyncTaskExecutor executor, AsyncTask task) {
        Refresher refresher = new Refresher(timerService, taskService, I(task.getId()), refreshIntervalInMilliseconds);
        refresher.start();
        try {
            executor.execute(task);
            updateTaskAsFinished(task);
        } catch (OXException e) {
            LOG.error("Failure during execution of task type '{}' with id '{}'", task.getType(), I(task.getId()), e);
            updateTaskWithError(task, e);
        } finally {
            refresher.stop();
        }
    }

    /**
     * Updates the task as finished
     *
     * @param task The finished task
     */
    private void updateTaskAsFinished(AsyncTask task) {
        try {
            task.setState(AsyncTaskState.FINISHED);
            task.setError(null);
            taskService.updateTask(task);
        } catch (OXException updateError) {
            LOG.error("Unable to store finished task status for task with id '{}'", I(task.getId()), updateError);
        }
    }

    /**
     * Updates a task with an error
     *
     * @param task The failed task
     * @param e The error
     */
    private void updateTaskWithError(AsyncTask task, OXException e) {
        try {
            task.setError(e.getMessage());
            task.setState(AsyncTaskState.FAILURE);
            task.increaseRetryCount();
            taskService.updateTask(task);
        } catch (OXException updateError) {
            LOG.error("Unable to store failed task status for task with id '{}'", I(task.getId()), updateError);
        }
    }

}
