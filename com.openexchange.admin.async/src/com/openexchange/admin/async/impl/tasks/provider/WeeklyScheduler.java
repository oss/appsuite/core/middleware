/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl.tasks.provider;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.admin.async.AsyncTaskExecutor;
import com.openexchange.admin.async.impl.ExecutorRegistry;
import com.openexchange.admin.async.impl.config.AdminAsyncProperties;
import com.openexchange.admin.async.impl.tasks.worker.AsyncTaskWorkerManager;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.java.Strings;
import com.openexchange.osgi.NearRegistryServiceTracker;
import com.openexchange.schedule.DayOfWeekTimeRanges;
import com.openexchange.schedule.RangesOfTheWeek;
import com.openexchange.schedule.ScheduleExpressionParser;
import com.openexchange.schedule.TimeOfTheDay;
import com.openexchange.schedule.TimeRange;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

/**
 * {@link WeeklyScheduler} schedules {@link AsyncTaskWorkerManager} for every configured window in a week
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class WeeklyScheduler extends NearRegistryServiceTracker<AsyncTaskExecutor> implements ExecutorRegistry, Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(WeeklyScheduler.class);

    private static final AtomicBoolean IS_RUNNING = new AtomicBoolean(false);

    private final TimerService timer;
    private final LeanConfigurationService configService;
    private final List<ScheduledTimerTask> tasks = new ArrayList<>();
    private final ScheduleWindowChanger starter;
    private final ScheduleWindowChanger stopper;

    private ScheduledTimerTask weeklyTask;

    /**
     * Initializes a new {@link WeeklyScheduler}.
     *
     * @param context The bundle context
     * @param timer The timer service to use
     * @param configService The configuration service
     * @param queue The {@link AbstractAsyncTaskQueue}
     */
    public WeeklyScheduler(BundleContext context,
                           TimerService timer,
                           LeanConfigurationService configService,
                           AbstractAsyncTaskQueue queue) {
        super(context, AsyncTaskExecutor.class);
        this.timer = timer;
        this.configService = configService;
        this.starter = new ScheduleWindowChanger(queue, true);
        this.stopper = new ScheduleWindowChanger(queue, false);
    }

    /**
     * Starts scheduling
     */
    public void start() {
        if (isCurrentlyRunning()) {
            throw new IllegalStateException("Scheduler is already running");
        }
        weeklyTask = timer.scheduleAtFixedRate(this, 0, TimeUnit.DAYS.toMillis(7));
    }

    @Override
    public void run() {
        LocalDateTime now = getNow();
        // Get the schedule and create
        Optional<RangesOfTheWeek> optSchedule = optRangesOfTheWeek();
        if (optSchedule.isEmpty()) {
            // No schedule
            return;
        }
        for (DayOfWeek day : DayOfWeek.values()) {
            handleDayScheduleWindows(now, day, optSchedule.get());
        }
    }

    /**
     * Stops the weekly task and all already scheduled but not yet running tasks
     */
    public void stop() {
        weeklyTask.cancel(true);
        tasks.forEach(t -> t.cancel());
        IS_RUNNING.set(false);
    }

    // ------------------------ registry -----------------------

    private final Map<String, AsyncTaskExecutor> executors = new HashMap<>();

    @Override
    protected AsyncTaskExecutor onServiceAvailable(AsyncTaskExecutor service) {
        executors.putIfAbsent(service.getType(), service);
        return service;
    }

    @Override
    public void removedService(ServiceReference<AsyncTaskExecutor> reference, AsyncTaskExecutor service) {
        super.removedService(reference, service);
        executors.remove(service.getType());
    }

    @Override
    public Optional<AsyncTaskExecutor> optExecutor(String type) {
        return Optional.ofNullable(executors.get(type));
    }

    // ------------------------ private helper methods ------------------------

    /**
     * Handles all schedule windows for a day
     *
     * @param now The current time
     * @param day The day to handle
     * @param schedule The schedule
     */
    private void handleDayScheduleWindows(LocalDateTime now, DayOfWeek day, RangesOfTheWeek schedule) {
        DayOfWeekTimeRanges dailyRanges = schedule.getDayOfWeekTimeRangesFor(day);
        if (dailyRanges == null) {
            // No ranges on that day
            return;
        }
        for (TimeRange range : dailyRanges.getRanges()) {
            handleScheduleWindow(now, day, range);
        }
    }

    /**
     * Handles a schedule window.
     *
     * It creates a new task for it at the given time.
     *
     * @param now The current time
     * @param day The day of the schedule windows
     * @param range The timerange of the schedule window
     */
    private void handleScheduleWindow(LocalDateTime now, DayOfWeek day, TimeRange range) {
        TimeOfTheDay start = range.getStart();
        if (isInRange(now, day, range)) {
            // Schedule immediately
            timer.schedule(starter, 0);
        } else {
            // Calculate distance and schedule
            long distance = getDistance(now, day, start);
            timer.schedule(starter, distance);
        }

        TimeOfTheDay end = range.getEnd();
        long distanceEnd = getDistance(now, day, end);
        tasks.add(timer.schedule(stopper, distanceEnd));
    }

    /**
     * Whether the schedule windows is now
     *
     * @param now The current time
     * @param day The day of the schedule window
     * @param range The timerange of the schedule window
     * @return <code>true</code> if now is in the schedule window, <code>false</code> otherwise
     */
    private boolean isInRange(LocalDateTime now, DayOfWeek day, TimeRange range) {
        return now.getDayOfWeek().equals(day) &&
               range.contains(new TimeOfTheDay(now.getHour(),
                                               now.getMinute(),
                                               now.getSecond()));
    }

    /**
     * Gets the distance in milliseconds between now and the schedule time
     *
     * @param now The current time
     * @param day The day of the schedule window
     * @param time The start or end time of the schedule window
     * @return The distance from now to the start time of the schedule window in milliseconds
     */
    private static long getDistance(LocalDateTime now, DayOfWeek day, TimeOfTheDay time) {
        LocalDateTime target = getTargetTime(now, day, time);
        ZonedDateTime nowZoned = now.atZone(ZoneId.systemDefault());
        ZonedDateTime targetZoned = target.atZone(ZoneId.systemDefault());
        return ChronoUnit.MILLIS.between(nowZoned, targetZoned);
    }

    /**
     * Gets the target time
     *
     * @param now The current time
     * @param day The day of the schedule window
     * @param time The start or end time of the schedule window
     * @return The target time
     */
    private static LocalDateTime getTargetTime(LocalDateTime now, DayOfWeek day, TimeOfTheDay time) {
        if (time.getHour() == 24) {
            // Special handling for 24h
            return now.withHour(0)
                      .withMinute(0)
                      .withSecond(0)
                      .plusDays(getDaysUntil(day, now) + 1);

        }
        return now.withHour(time.getHour())
                  .withMinute(time.getMinute())
                  .withSecond(time.getSecond())
                  .plusDays(getDaysUntil(day, now));
    }

    /**
     * Gets the days until the given weekday
     *
     * @param day The weekday
     * @param now The current time
     * @return The number of days until the given weekday
     */
    private static int getDaysUntil(DayOfWeek day, LocalDateTime now) {
        return (day.getValue() - now.getDayOfWeek().getValue() + 7) % 7;
    }

    private static LocalDateTime getNow() {
        return LocalDateTime.now();
    }

    private boolean isCurrentlyRunning() {
        return false == IS_RUNNING.compareAndSet(false, true);
    }

    /**
     * Gets the ranges of the week if configured
     *
     * @return The optional ranges
     */
    private Optional<RangesOfTheWeek> optRangesOfTheWeek() {
        String ranges = configService.getProperty(AdminAsyncProperties.SCHEDULE);
        if (Strings.isEmpty(ranges)) {
            return Optional.empty();
        }
        try {
            return Optional.of(ScheduleExpressionParser.parse(ranges));
        } catch (IllegalStateException e) {
            LOG.error("Unable to schedule async framework jobs. The configured schedule is invalid", e);
            return Optional.empty();
        }
    }

}
