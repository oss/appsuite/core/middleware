/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl.tasks;

import java.util.Optional;
import com.openexchange.admin.async.AsyncTask;
import com.openexchange.admin.async.impl.ExecutableAsyncTask;
import com.openexchange.admin.async.impl.storage.AsyncTaskStorage;
import com.openexchange.exception.OXException;

/**
 * {@link ExecutableAsyncTaskImpl} is a {@link ExecutableAsyncTask} which uses
 * the {@link AsyncTaskStorage} to claim the task
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class ExecutableAsyncTaskImpl implements ExecutableAsyncTask {

    private final AsyncTaskStorage storage;
    private final Integer id;
    private final int maxRetries;
    private final int retryIntervalMinutes;

    /**
     * Initializes a new {@link ExecutableAsyncTaskImpl}.
     *
     * @param storage The task storage
     * @param id The task id
     * @param maxRetries The maximum number of retries
     * @param retryIntervalMinutes The retry interval in minutes
     */
    public ExecutableAsyncTaskImpl(AsyncTaskStorage storage, Integer id, int maxRetries, int retryIntervalMinutes) {
        super();
        this.storage = storage;
        this.id = id;
        this.maxRetries = maxRetries;
        this.retryIntervalMinutes = retryIntervalMinutes;
    }

    @Override
    public Optional<AsyncTask> claimTask() throws OXException {
        return storage.claimTask(id, maxRetries, retryIntervalMinutes);
    }

    @Override
    public Integer getId() {
        return id;
    }
}
