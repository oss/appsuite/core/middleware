/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl.tasks.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.admin.async.impl.tasks.provider.AsyncTaskServiceImpl;
import com.openexchange.exception.OXException;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

/**
 * A {@link Refresher} is used to updates the task claim for as long as the task is properly running.
 * If the tasks fails - e.g. because the node crashed - the claim is no longer refreshed and can be picked up again by another node during the next window.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class Refresher {

    private static final Logger LOG = LoggerFactory.getLogger(Refresher.class);

    private final AsyncTaskServiceImpl taskService;
    private final Integer id;
    private final TimerService timer;
    private final long interval;
    private volatile ScheduledTimerTask current;

    /**
     * Initializes a new {@link Refresher}.
     *
     * @param timer The timer service used to start this refresher
     * @param taskService The task service needed to udpate the given task
     * @param id The task id
     * @param intervalInMilliseconds The interval in milliseconds between refreshes
     */
    public Refresher(TimerService timer, AsyncTaskServiceImpl taskService, Integer id, long intervalInMilliseconds) {
        super();
        this.timer = timer;
        this.taskService = taskService;
        this.id = id;
        this.interval = intervalInMilliseconds;
    }

    /**
     * Starts refreshing the claim
     */
    void start() {
        current = timer.scheduleAtFixedRate(() -> refresh(), interval, interval);
    }

    /**
     * Stops refreshing the claim
     */
    void stop() {
        if (current != null) {
            current.cancel(true);
        }
    }

    /**
     * Refreshes the claim safely
     */
    private void refresh() {
        try {
            taskService.refreshTask(id);
        } catch (OXException e) {
            LOG.warn("Unable to refresh claim for task '{}'", id, e);
        }
    }

}
