/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl.tasks.provider;

import static com.openexchange.java.Autoboxing.I;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.admin.async.impl.ExecutableAsyncTask;
import com.openexchange.admin.async.impl.config.AdminAsyncProperties;
import com.openexchange.admin.async.impl.tasks.DueTaskProvider;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;

/**
 * {@link AbstractAsyncTaskQueue} is an abstract {@link DueTaskProvider} which uses
 * a {@link LinkedBlockingQueue} to provide the next tasks. The tasks are automatically filled once
 * the {@link #start()} method is called.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public abstract class AbstractAsyncTaskQueue implements DueTaskProvider {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractAsyncTaskQueue.class);

    protected final LeanConfigurationService configurationService;
    private final long workerTimeout;
    private final LinkedBlockingQueue<ExecutableAsyncTask> queue = new LinkedBlockingQueue<>();

    private Thread thread;
    private volatile boolean running = false;

    /**
     * Initializes a new {@link AbstractAsyncTaskQueue}.
     *
     * @param configurationService The configuration service
     */
    public AbstractAsyncTaskQueue(LeanConfigurationService configurationService) {
        super();
        this.configurationService = configurationService;
        this.workerTimeout = getRefillIntervalInMilliseconds();
    }

    @Override
    public ExecutableAsyncTask getNextTask() {
        try {
            return queue.take();
        } catch (@SuppressWarnings("unused") InterruptedException e) {
            return null;
        }
    }

    /**
     * Add a new task to this queue
     *
     * @param task The task to add
     */
    public void offer(ExecutableAsyncTask task) {
        if (running == false) {
            return;
        }
        queue.offer(task);
    }

    /**
     * Starts adding due tasks to this queue
     */
    public final synchronized void start() {
        if (thread != null && thread.isAlive()) {
            // Already running
            return;
        }

        running = true;
        thread = new Thread(() -> fillQueueUntilStopped());
        thread.start();
        LOG.info("Started async task processing");
    }

    /**
     * Stops adding new tasks to the queue
     */
    public final void stop() {
        if (thread == null || thread.isAlive() == false) {
            // Already stopped
            return;
        }
        running = false;
        thread.interrupt();
        // Wait until thread stopped
        try {
            thread.join();
        } catch (@SuppressWarnings("unused") InterruptedException unused) {
            // Should not happen
        }
        int remainingTasks = queue.size();
        queue.clear(); // Clear queue so workers stop working
        LOG.info("Stopped async task processing with {} tasks left in the queue.", I(remainingTasks));
    }

    // ---------------------- abstract methods ------------------

    /**
     * Gets all due tasks
     *
     * @return The due tasks
     * @throws OXException in case of errors
     */
    protected abstract List<ExecutableAsyncTask> getDueTasks() throws OXException;

    // ---------------------- private methods ------------------

    /**
     * Fills the queue with new due tasks every {@link #workerTimeout} milliseconds
     */
    private void fillQueueUntilStopped() {
        while (running) {
            fillQueue();
            try {
                Thread.sleep(workerTimeout);
            } catch (@SuppressWarnings("unused") InterruptedException unused) {
                // Thread stopped
                return;
            }
        }
    }

    /**
     * Fills the queue with new due tasks
     */
    private void fillQueue() {
        try {
            List<ExecutableAsyncTask> tasks = getDueTasks();
            if(tasks.isEmpty() == false) {
                queue.addAll(tasks);
            }
        } catch (OXException e) {
            LOG.error("Error while loading async tasks", e);
        }
    }

    /**
     * Gets the configured refill interval in milliseconds
     *
     * @return The refill time in milliseconds
     */
    private long getRefillIntervalInMilliseconds() {
        int intervalInMinutes = configurationService.getIntProperty(AdminAsyncProperties.REFILL_INTERVAL);
        return TimeUnit.MINUTES.toMillis(intervalInMinutes);
    }


}
