/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.impl.tasks.provider;


/**
 * {@link ScheduleWindowChanger} starts or stops the {@link AbstractAsyncTaskQueue}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class ScheduleWindowChanger implements Runnable {

    private final AbstractAsyncTaskQueue queue;
    private final boolean start;

    /**
     * Initializes a new {@link ScheduleWindowChanger}.
     *
     * @param queue The queue
     * @param start Whether to start or stop the queue
     */
    public ScheduleWindowChanger(AbstractAsyncTaskQueue queue, boolean start) {
        super();
        this.queue = queue;
        this.start = start;
    }

    @Override
    public void run() {
        if (start) {
            queue.start();
            return;
        }
        queue.stop();
    }

}
