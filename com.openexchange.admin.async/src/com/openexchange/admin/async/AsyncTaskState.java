/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async;

import java.util.stream.Stream;

/**
 * The {@link AsyncTaskState} describes the state of tasks in the async framework
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public enum AsyncTaskState {
    /**
     * The task has finished successfully.
     *
     * E.g. the context has been deleted.
     */
    FINISHED,
    /**
     * The task run has failed because of some error. It maybe get executed again.
     */
    FAILURE,
    /**
     * The task is scheduled to run in the next window
     */
    SCHEDULED,
    /**
     * The task is currently running
     */
    RUNNING;

    /**
     * Parses the given state into a {@link AsyncTaskState}
     *
     * @param state The state to parse
     * @return The {@link AsyncTaskState}
     * @throws IllegalArgumentException in case the state is unknown
     */
    public static AsyncTaskState parse(String state) throws IllegalArgumentException {
        return Stream.of(values())
                     .filter(s -> s.name().equalsIgnoreCase(state))
                     .findAny()
                     .orElseThrow(() -> new IllegalArgumentException("Unknown task state"));
    }
}
