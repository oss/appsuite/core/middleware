/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async;

import java.util.Objects;
import java.util.UUID;

/**
 * {@link AsyncTask} defines an executable task for the async framework.
 * E.g. a context deletion
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class AsyncTask {

    private final int id;
    private final String entityId;
    private final String type;
    private final String args;
    private AsyncTaskState state;
    private final UUID claim;
    private int retryCount;
    private String error;

    /**
     * Initializes a new {@link AsyncTask}.
     *
     * @param builder
     */
    private AsyncTask(Builder builder) {
        this.id = builder.id;
        this.entityId = builder.entityId;
        this.type = builder.type;
        this.args = builder.args;
        this.state = builder.state;
        this.claim = builder.claim;
        this.retryCount = builder.retryCount;
        this.error = builder.error;
    }

    /**
     * Gets the unique identifier for this task.
     *
     * @return the task's unique identifier.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the entity ID associated with the async task.
     *
     * @return the entity ID.
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * Gets the type of this task.
     *
     * @return the type of the task.
     */
    public String getType() {
        return type;
    }

    /**
     * Gets the arguments related to this task.
     *
     * @return the arguments of the task, or null if not set.
     */
    public String getArgs() {
        return args;
    }

    /**
     * Gets the current state of this task.
     *
     * @return the state of this tasl.
     */
    public AsyncTaskState getState() {
        return state;
    }

    /**
     * Sets the state of this task.
     *
     * @param state the new state of the task.
     */
    public void setState(AsyncTaskState state) {
        this.state = state;
    }

    /**
     * Gets the claim of this task.
     *
     * @return the claim or null if not set.
     */
    public UUID getClaim() {
        return claim;
    }

    /**
     * Gets the retry count for the async task.
     *
     * @return the number of retries attempted for this task.
     */
    public int getRetryCount() {
        return retryCount;
    }

    /**
     * Increases the retry count
     */
    public void increaseRetryCount() {
        retryCount++;
    }

    /**
     * Gets the error message associated with this task, if any.
     *
     * @return the error message, or null if no error occurred.
     */
    public String getError() {
        return error;
    }

    /**
     * Sets the error message for this task
     *
     * @param error the new error message for the task.
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * Gets a new builder for this {@link AsyncTask}
     *
     * @return The builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * {@link Builder} a builder for {@link AsyncTask}s
     *
     * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
     */
    public static final class Builder {

        private int id;
        private String entityId;
        private String type;
        private String args;
        private AsyncTaskState state;
        private UUID claim;
        private int retryCount;
        private String error;

        /**
         * Sets the id
         *
         * @param id The id
         * @return this builder
         */
        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        /**
         * Sets the entity id
         *
         * @param entityId The entity id
         * @return this builder
         */
        public Builder withEntityId(String entityId) {
            this.entityId = entityId;
            return this;
        }

        /**
         * Sets the type
         *
         * @param type The type
         * @return this builder
         */
        public Builder withType(String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the arguments
         *
         * @param args The arguments
         * @return this builder
         */
        public Builder withArgs(String args) {
            this.args = args;
            return this;
        }

        /**
         * Sets the state
         *
         * @param state The state
         * @return this builder
         */
        public Builder withState(AsyncTaskState state) {
            this.state = state;
            return this;
        }

        /**
         * Sets the claim
         *
         * @param claim The claim
         * @return this builder
         */
        public Builder withClaim(UUID claim) {
            this.claim = claim;
            return this;
        }

        /**
         * Sets the current retry count
         *
         * @param retryCount The retry count
         * @return this builder
         */
        public Builder withRetryCount(int retryCount) {
            this.retryCount = retryCount;
            return this;
        }

        /**
         * Sets the error
         *
         * @param error The error
         * @return this builder
         */
        public Builder withError(String error) {
            this.error = error;
            return this;
        }

        /**
         * Builds the {@link AsyncTask}
         *
         * @return the task
         */
        public AsyncTask build() {
            Objects.requireNonNull(this.entityId);
            Objects.requireNonNull(this.type);
            return new AsyncTask(this);
        }
    }

    @Override
    public String toString() {
        return "AsyncTask{" +
            "id=" + id +
            ", entityId='" + entityId + "'" +
            ", type='" + type + "'" +
            ", args='" + args + "'" +
            ", state='" + state + "'" +
            ", claim='" + claim + "'" +
            ", retryCount=" + retryCount +
            ", error='" + error + "'" +
            "}";
    }
}

