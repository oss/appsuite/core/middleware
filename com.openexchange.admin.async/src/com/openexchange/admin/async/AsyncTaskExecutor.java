/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async;

import com.openexchange.exception.OXException;

/**
 * {@link AsyncTaskExecutor} defines executor for tasks. These implementation do the actual work.
 *
 * There can only be one executor per type. If more than one executor is registered only the first to registered is used.
 * Tasks performed by an executor must be idempotent.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public interface AsyncTaskExecutor {

    /**
     * Gets the type this executor is responsible for.
     *
     * @return The task type
     */
    String getType();

    /**
     * Executes the given task
     *
     * @param task The task
     * @throws OXException in case of errors
     */
    void execute(AsyncTask task) throws OXException;

}
