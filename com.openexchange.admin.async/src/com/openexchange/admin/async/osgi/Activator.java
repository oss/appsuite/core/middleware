/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.async.osgi;

import java.util.concurrent.TimeUnit;
import com.openexchange.admin.async.AsyncTaskExecutor;
import com.openexchange.admin.async.AsyncTaskService;
import com.openexchange.admin.async.impl.CleanupJob;
import com.openexchange.admin.async.impl.config.AdminAsyncProperties;
import com.openexchange.admin.async.impl.storage.AsyncTaskStorage;
import com.openexchange.admin.async.impl.tasks.provider.AsyncTaskServiceImpl;
import com.openexchange.admin.async.impl.tasks.provider.WeeklyScheduler;
import com.openexchange.admin.async.impl.tasks.worker.AsyncTaskWorkerManager;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.database.DatabaseService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

/**
 * {@link Activator}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class Activator extends HousekeepingActivator{

    private WeeklyScheduler scheduler;
    private AsyncTaskWorkerManager workerManager;
    private ScheduledTimerTask cleanupTask;

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] { LeanConfigurationService.class, TimerService.class, DatabaseService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        LeanConfigurationService configurationService = getServiceSafe(LeanConfigurationService.class);
        AsyncTaskStorage storage = new AsyncTaskStorage(getServiceSafe(DatabaseService.class));
        AsyncTaskServiceImpl taskService = new AsyncTaskServiceImpl(storage, configurationService);
        registerService(AsyncTaskService.class, taskService);

        TimerService timerService = getServiceSafe(TimerService.class);
        scheduler = new WeeklyScheduler(context,
                                        timerService,
                                        configurationService,
                                        taskService);
        track(AsyncTaskExecutor.class, scheduler);
        openTrackers();
        workerManager = new AsyncTaskWorkerManager(configurationService, scheduler, taskService, timerService);
        workerManager.start();
        scheduler.start();

        int cleanIntervalInDays = configurationService.getIntProperty(AdminAsyncProperties.CLEANUP_INTERVAL_IN_DAYS);
        if (cleanIntervalInDays > 0) {
            cleanupTask = timerService.scheduleAtFixedRate(new CleanupJob(storage, configurationService), 0, TimeUnit.DAYS.toMillis(cleanIntervalInDays));
        }
    }

    @Override
    protected void stopBundle() throws Exception {
        super.stopBundle();
        scheduler.stop();
        workerManager.stop();
        cleanupTask.cancel();
    }

}
