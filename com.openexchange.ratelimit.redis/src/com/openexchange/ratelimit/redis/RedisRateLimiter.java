/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.ratelimit.redis;

import static com.openexchange.java.Autoboxing.L;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.ratelimit.Rate;
import com.openexchange.ratelimit.RateLimiter;
import com.openexchange.redis.RedisCommandsProvider;
import com.openexchange.redis.RedisConnector;
import io.lettuce.core.api.sync.RedisKeyCommands;

/**
 * {@link RedisRateLimiter} is a {@link RateLimiter} which uses redis as its backend
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class RedisRateLimiter implements RateLimiter {

    private static final Logger LOG = LoggerFactory.getLogger(RedisRateLimiter.class);

    private final RedisConnector connector;
    private final String key;

    private final Rate rate;

    /**
     * Initializes a new {@link RedisRateLimiter}.
     *
     * @param connector The redis connector
     * @param rate The rate
     * @param key The key to use
     */
    public RedisRateLimiter(RedisConnector connector, Rate rate, String key) {
        super();
        this.connector = connector;
        this.rate = rate;
        this.key = key;
    }

    @Override
    public boolean acquire() {
        return acquire(1);
    }

    @Override
    public boolean acquire(long permits) {
        if (rate.getAmount() <= 0) {
            return true;
        }
        if (permits > rate.getAmount()) {
            return false;
        }

        String key = getKey();
        try {
            long currentVal = connector.executeOperation(provider -> increment(provider, key, permits)).longValue();
            if (currentVal > rate.getAmount()) {
                return false;
            }
            connector.executeOperation(provider -> expire(provider, key));
        } catch (OXException e) {
            LOG.error("Unable to check current usage", e);
        }
        return true;
    }

    @Override
    public void reset() {
        try {
            connector.executeOperation(provider -> reset(provider));
        } catch (OXException e) {
            LOG.error("Unable to reset rate limit", e);
        }
    }

    @Override
    public boolean exceeded() {
        if (rate.getAmount() <= 0) {
            return false;
        }
        try {
            return rate.getAmount() <= connector.executeOperation(provider -> get(provider)).longValue();
        } catch (OXException e) {
            LOG.error("Unable to check current usage", e);
        }
        return false;
    }

    // ----------------- private methods ----------------------

    /**
     * Increments the key by the given amount
     *
     * @param provider The provider to use
     * @param key The key
     * @param amount The amount to increase
     * @return The new value
     */
    private Long increment(RedisCommandsProvider provider, String key, long amount) {
        return provider.getStringCommands().incrby(key, amount);
    }

    /**
     * Add an expiration to the key
     *
     * @param provider The provider to use
     * @param key The key
     * @return null
     */
    private Void expire(RedisCommandsProvider provider, String key) {
        // Compute the next time slot
        long nextSlotMillis = (getCurrentSlot() + 1) * rate.getTimeframe();
        LocalDateTime nexSlot = LocalDateTime.now().toLocalDate().atStartOfDay().plus(nextSlotMillis, ChronoUnit.MILLIS);
        // Expire at the next time slot
        provider.getKeyCommands().expireat(key, nexSlot.atZone(ZoneId.systemDefault()).toInstant());
        return null;
    }

    /**
     * Gets the current value for the key
     *
     * @param provider The provider to use
     * @return The value
     */
    private Long get(RedisCommandsProvider provider) {
        return Long.valueOf(provider.getStringCommands().get(getKey()));
    }

    /**
     * Resets the value
     *
     * @param provider The provider to use
     * @return null
     */
    private Void reset(RedisCommandsProvider provider) {
        RedisKeyCommands<String, InputStream> commands = provider.getKeyCommands();
        commands.del(getKey());
        return null;
    }

    /**
     * Gets the key for the current slot
     *
     * @return The redis key
     */
    private String getKey() {
        return "%s:%s".formatted(this.key, L(getCurrentSlot()));
    }

    /**
     * Gets the current slot
     *
     * @return The slot
     */
    private long getCurrentSlot() {
        long timeframe = rate.getTimeframe();
        long millis = LocalDateTime.now().getLong(ChronoField.MILLI_OF_DAY);
        return millis / timeframe;
    }


}
