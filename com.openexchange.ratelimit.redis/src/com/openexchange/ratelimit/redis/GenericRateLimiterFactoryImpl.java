/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.ratelimit.redis;

import java.util.concurrent.TimeUnit;
import com.openexchange.exception.OXException;
import com.openexchange.ratelimit.GenericRateLimiterFactory;
import com.openexchange.ratelimit.Rate;
import com.openexchange.ratelimit.RateLimiter;
import com.openexchange.redis.RedisConnector;
import com.openexchange.redis.RedisConnectorService;

/**
 * {@link GenericRateLimiterFactoryImpl} is a {@link GenericRateLimiterFactory} which uses redis as its backend
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class GenericRateLimiterFactoryImpl implements GenericRateLimiterFactory {

    private final RedisConnector connector;

    /**
     * Initializes a new {@link GenericRateLimiterFactoryImpl}.
     *
     * @param redisService The {@link RedisConnectorService}
     * @throws OXException If the redis connector couldn't be provided
     */
    public GenericRateLimiterFactoryImpl(RedisConnectorService redisService) throws OXException {
        super();
        connector = redisService.getConnectorProvider().getConnector();
    }

    @Override
    public RateLimiter createLimiter(String id, Rate rate, String entity) {
        if (rate.getTimeframe() < TimeUnit.SECONDS.toMillis(10)) {
            throw new IllegalArgumentException("Timeframe must be at least 10s");
        }
        if (rate.getTimeframe() > TimeUnit.HOURS.toMillis(12)) {
            throw new IllegalArgumentException("Timeframe must be smaller than 12h");
        }
        String key = "rlimit:%s:%s".formatted(id, entity);
        return new RedisRateLimiter(connector, rate, key);
    }

}
