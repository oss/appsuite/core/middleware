/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.http.requestwatcher.internal;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.http.requestwatcher.osgi.services.RequestRegistryEntry;
import com.openexchange.http.requestwatcher.osgi.services.RequestTrace;
import com.openexchange.http.requestwatcher.osgi.services.RequestWatcherService;
import com.openexchange.log.HumanTimeOutputter;
import com.openexchange.log.LogMessageBuilder;
import com.openexchange.log.LogProperties;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

/**
 * {@link RequestWatcherServiceImpl}
 *
 * @author <a href="mailto:marc.arens@open-xchange.com">Marc Arens</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RequestWatcherServiceImpl implements RequestWatcherService {

    /** The logger. */
    protected static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(RequestWatcherServiceImpl.class);

    /** The request number */
    private static final AtomicLong NUMBER = new AtomicLong();

    /** The attribute name for Grizzly's request number */
    private static final String ATTRIBUTE_REQUEST_NUMBER = "grizzly.reqnum";

    // --------------------------------------------------------------------------------------------------------------------------

    /** Navigable set, entries ordered by age (youngest first), weakly consistent iterator */
    private final ConcurrentSkipListSet<RequestRegistryEntry> requestRegistry;

    /** The watcher task */
    private final AtomicReference<ScheduledTimerTask> requestWatcherTask;

    /**
     * Initializes a new {@link RequestWatcherServiceImpl}
     *
     * @param configService The configuration service used for initialization
     * @param timerService The timer service used for initialization
     * @throws OXException If initialization fails
     */
    public RequestWatcherServiceImpl(ConfigurationService configService, TimerService timerService) throws OXException {
        super();
        // Create set
        ConcurrentSkipListSet<RequestRegistryEntry> requestRegistry = new ConcurrentSkipListSet<RequestRegistryEntry>();
        this.requestRegistry = requestRegistry;

        // Create ScheduledTimerTask to watch requests
        int requestMaxAge = configService.getIntProperty("com.openexchange.requestwatcher.maxRequestAge", 60_000);
        int requestExpiredAge = configService.getIntProperty("com.openexchange.requestwatcher.expiredRequestAge", 360_000);
        if (requestExpiredAge > 0 && requestMaxAge > requestExpiredAge) {
            throw OXException.general("Request expiration age (" + requestExpiredAge + ") must not be less than request max. age (" + requestMaxAge + ")");
        }
        int downloadUploadExpiredAge = configService.getIntProperty("com.openexchange.requestwatcher.expiredDownloadUploadAge", 43_200_000);
        if (downloadUploadExpiredAge > 0 && requestMaxAge > downloadUploadExpiredAge) {
            throw OXException.general("Request expiration age for down-/uploads (" + downloadUploadExpiredAge + ") must not be less than request max. age (" + requestMaxAge + ")");
        }
        int watcherFrequency = configService.getIntProperty("com.openexchange.requestwatcher.frequency", 30_000);
        int interruptedThreshold = configService.getIntProperty("com.openexchange.requestwatcher.interruptedThreshold", 1);
        if (interruptedThreshold < 0) {
            interruptedThreshold = 0;
        }
        Watcher task = new Watcher(this.requestRegistry, requestMaxAge, requestExpiredAge, downloadUploadExpiredAge, interruptedThreshold);
        ScheduledTimerTask requestWatcherTask = timerService.scheduleAtFixedRate(task, requestMaxAge, watcherFrequency);
        this.requestWatcherTask = new AtomicReference<>(requestWatcherTask);
    }

    @Override
    public RequestRegistryEntry registerRequest(HttpServletRequest request, HttpServletResponse response, Thread thread, Map<String, String> propertyMap) {
        RequestRegistryEntry registryEntry = new RequestRegistryEntry(getRequestNumber(request), request, thread, propertyMap);
        requestRegistry.add(registryEntry);
        return registryEntry;
    }

    /**
     * Gets the request number for given HTTP request.
     *
     * @param request The HTTP request
     * @return The request number
     */
    private static long getRequestNumber(HttpServletRequest request) {
        Long requestNumber = (Long) request.getAttribute(ATTRIBUTE_REQUEST_NUMBER);
        return requestNumber == null ? NUMBER.incrementAndGet() : requestNumber.longValue();
    }

    @Override
    public boolean unregisterRequest(RequestRegistryEntry registryEntry) {
        return requestRegistry.remove(registryEntry);
    }

    @Override
    public boolean stopWatching() {
        ScheduledTimerTask requestWatcherTask = this.requestWatcherTask.getAndSet(null);
        return null == requestWatcherTask || requestWatcherTask.cancel();
    }

    // ----------------------------------------------------------------------------------------------------------------------- //

    /** The result when handling an entry */
    private static enum Result {

        /**
         * Exceeded entry has been logged.
         */
        LOGGED,
        /**
         * Expired entry has been interrupted.
         */
        INTERRUPTED,
        /**
         * Expired entry has been hard-killed.
         */
        STOPPED;
    }

    private static final class Watcher implements Runnable {

        private final ConcurrentSkipListSet<RequestRegistryEntry> requestRegistry;
        private final int requestMaxAge;
        private final int requestExpiredAge;
        private final int downloadUploadExpiredAge;
        private final int interruptedThreshold;
        private final Lock runLock;

        /**
         * Initializes a new {@link Watcher}.
         */
        Watcher(ConcurrentSkipListSet<RequestRegistryEntry> requestRegistry, int requestMaxAge, int requestExpiredAge, int downloadUploadExpiredAge, int interruptedThreshold) {
            super();
            this.runLock = new ReentrantLock();
            this.interruptedThreshold = interruptedThreshold;
            this.requestRegistry = requestRegistry;
            this.requestMaxAge = requestMaxAge;
            this.requestExpiredAge = requestExpiredAge;
            this.downloadUploadExpiredAge = downloadUploadExpiredAge;
        }

        /**
         * Performs a request watcher run:
         * <ol>
         * <li>Start at the tail of the <code>java.util.NavigableSet</code> to get the oldest request first.
         * <li>Then proceed to the younger requests.
         * <li>Stop processing at the first yet valid request.
         * </ol>
         */
        @Override
        public void run() {
            if (!runLock.tryLock()) {
                // Lock NOT acquired --> Another run is being performed. Leave...
                return;
            }

            // Locked...
            try {
                Thread runner = Thread.currentThread();
                boolean debugEnabled = LOG.isDebugEnabled();
                LogMessageBuilder logBuilder = LogMessageBuilder.createLogMessageBuilder(256, 8);
                boolean stillOldRequestsLeft = true;
                for (Iterator<RequestRegistryEntry> descendingEntryIterator = requestRegistry.descendingIterator(); !runner.isInterrupted() && stillOldRequestsLeft && descendingEntryIterator.hasNext();) {
                    // Debug logging
                    if (debugEnabled) {
                        logBuilder.reset();
                        logBuilder.lfappendln("RegisteredThreads:");
                        for (RequestRegistryEntry entry : requestRegistry) {
                            logBuilder.appendln("    age: {} ms, thread: {}", Long.valueOf(entry.getAge()), entry.getThreadInfo());
                        }
                        LOG.debug(logBuilder.getMessage(), logBuilder.getArgumentsAsArray());
                    }

                    // Check entry's age
                    RequestRegistryEntry entry = descendingEntryIterator.next();
                    if (entry.getAge() > requestMaxAge) {
                        logBuilder.reset();
                        switch (handleEntry(entry, logBuilder)) {
                            case INTERRUPTED:
                                entry.markInterrupted();
                                break;
                            case STOPPED:
                                requestRegistry.remove(entry);
                                break;
                            case LOGGED:
                                // fall-through
                            default:
                                // Nothing
                                break;

                        }
                    } else {
                        stillOldRequestsLeft = false;
                    }
                }
            } catch (Exception e) {
                LOG.error("Request watcher run failed", e);
            } finally {
                runLock.unlock();
            }
        }

        private Result handleEntry(RequestRegistryEntry entry, LogMessageBuilder logBuilder) {
            // Age info
            AgeInfo ageInfo = newAgeInfo(entry.getAge(), requestMaxAge);

            // Get trace for associated thread's trace
            Throwable trace = new RequestTrace(ageInfo.sAge, ageInfo.sMaxAge, entry.getThread().getName());
            StackTraceElement[] stackTrace = entry.getStackTrace();
            trace.setStackTrace(stackTrace);

            // Examine trace if request is a download or upload
            boolean downloadOrUploadRequest = Utils.isDownloadOrUploadRequest(stackTrace);

            // ... check if request shall be interrupted (if not already done) or even killed
            int effectiveExpirationAge = downloadOrUploadRequest ? downloadUploadExpiredAge : requestExpiredAge;
            Result result = determineResult(entry, effectiveExpirationAge);

            // Log request info
            logRequest(entry, result, effectiveExpirationAge, !downloadOrUploadRequest, ageInfo, logBuilder, trace);

            // Return result
            return result;
        }

        /**
         * Determines whether given result shall be interrupted, killed or simply logged.
         *
         * @param entry The entry to examine
         * @param effectiveExpirationAge The effective expiration age
         * @return The result
         */
        private Result determineResult(RequestRegistryEntry entry, int effectiveExpirationAge) {
            if (entry.isInterrupted()) {
                // Already interrupted but still running
                if (interruptedThreshold <= 0) {
                    return Result.LOGGED;
                }

                int expiredCount = entry.incrementAndGetExpiredCounter();
                return expiredCount >= interruptedThreshold ? Result.STOPPED : Result.LOGGED;
            }

            return shallInterrupt(entry, effectiveExpirationAge) ? Result.INTERRUPTED : Result.LOGGED;
        }

        /** The log property for session identifier: <code>"com.openexchange.session.sessionId"</code> */
        private static final String LOG_PROPERTY_SESSION_ID = LogProperties.Name.SESSION_SESSION_ID.getName();

        /**
         * Checks if the request associated with given entry shall be interrupted.
         *
         * @param entry The entry providing information about tracked request
         * @param expiredAge The expired age to check against
         * @param downloadOrUploadRequest <code>true</code> for download/upload request; otherwise <code>false</code>
         * @return <code>true</code> if request shall be interrupted; otherwise <code>false</code>
         */
        private static boolean shallInterrupt(RequestRegistryEntry entry, int expiredAge) {
            if (expiredAge > 0 && entry.getAge() > expiredAge) {
                // Expiration time elapsed
                return true;
            }

            // Check request's log properties for inactive session
            Map<String, String> propertyMap = entry.getPropertyMap();
            if (null != propertyMap && Utils.isInvalidSession(propertyMap.get(LOG_PROPERTY_SESSION_ID))) { // NOSONARLINT
                return true;
            }

            return false;
        }

        private void logRequest(RequestRegistryEntry entry, Result result, int expirationAge, boolean withCommonLogging, AgeInfo ageInfo, LogMessageBuilder logBuilder, Throwable trace) {
            // Do the common logging for long-running requests: "Request with age XYZ exceeds..."
            if (withCommonLogging) {
                if (entry.isInterrupted()) {
                    logBuilder.append("#{} Interrupted request with age {}ms ({}) exceeds max. age of {}ms ({}).", Integer.valueOf(entry.getAndIncrementExceededCounter()), ageInfo.sAge, new HumanTimeOutputter(entry.getAge(), true), ageInfo.sMaxAge, new HumanTimeOutputter(requestMaxAge, true));
                } else {
                    logBuilder.append("#{} Request with age {}ms ({}) exceeds max. age of {}ms ({}).", Integer.valueOf(entry.getAndIncrementExceededCounter()), ageInfo.sAge, new HumanTimeOutputter(entry.getAge(), true), ageInfo.sMaxAge, new HumanTimeOutputter(requestMaxAge, true));
                }
            }

            // Special logging in case of interrupt or kill
            switch (result) {
              case Result.INTERRUPTED -> interruptAndLogRequest(entry, expirationAge, logBuilder, trace);
              case Result.STOPPED -> killAndLogRequest(entry, logBuilder, trace);
              default -> {
                  if (withCommonLogging) {
                      appendLogPropertiesAndLog(entry, logBuilder, trace);
                  }
              }
            }
        }

        /**
         * Kills and appends appropriate information to log builder.
         *
         * @param entry The request entry
         * @param logBuilder The log builder
         * @param trace The request's trace
         */
        private static void killAndLogRequest(RequestRegistryEntry entry, LogMessageBuilder logBuilder, Throwable trace) {
            boolean lf = logBuilder.length() > 0;
            if (lf) {
                logBuilder.lfappend("Going to hard-kill request's thread since already interrupted, but still running!");
            } else {
                logBuilder.append("Going to hard-kill request's thread since already interrupted, but still running!");
            }

            // Kill request
            String info = killWithInfoOnFailure(entry.getThread());
            if (info == null) {
                logBuilder.lfappend("Request's thread killed.");
            } else {
                logBuilder.lfappend("Request's thread could not be killed: {}.", info);
            }

            appendLogPropertiesAndLog(entry, logBuilder, trace);
        }

        /**
         * Kills given request's thread and given an information on failure.
         *
         * @param t The thread
         * @return The failure information or <code>null</code> on successful kill
         */
        private static String killWithInfoOnFailure(Thread t) {
            if (t != null) {
                try {
                    t.stop();
                    return null;
                } catch (Exception e) {
                    LOG.error("Failed to kill thread", e);
                    return e.getClass().getName() + ": " + e.getMessage();
                }
            }
            return "Thread is null";
        }

        /**
         * Interrupts and appends appropriate information to log builder.
         *
         * @param entry The request entry
         * @param expirationAge The expiration age
         * @param logBuilder The log builder
         * @param trace The request's trace
         */
        private static void interruptAndLogRequest(RequestRegistryEntry entry, int expirationAge, LogMessageBuilder logBuilder, Throwable trace) {
            boolean lf = logBuilder.length() > 0;
            if (lf) {
                logBuilder.lfappend("Going to interrupt request's thread since expiration threshold {}ms ({}) is exceeded!", formatDecimal(expirationAge), new HumanTimeOutputter(expirationAge, true));
            } else {
                logBuilder.append("Going to interrupt request's thread since expiration threshold {}ms ({}) is exceeded!", formatDecimal(expirationAge), new HumanTimeOutputter(expirationAge, true));
            }

            // Interrupt request
            String info = interruptWithInfoOnFailure(entry.getThread());
            if (info == null) {
                logBuilder.lfappend("Request's thread interrupted.");
            } else {
                logBuilder.lfappend("Request's thread could not be interrupted: {}.", info);
            }

            appendLogPropertiesAndLog(entry, logBuilder, trace);
        }

        /**
         * Interrupts given request's thread and given an information on failure.
         *
         * @param t The thread
         * @return The failure information or <code>null</code> on successful interruption
         */
        private static String interruptWithInfoOnFailure(Thread t) {
            if (t != null) {
                try {
                    t.interrupt();
                    return null;
                } catch (Exception e) {
                    LOG.error("Failed to interrupt thread", e);
                    return e.getClass().getName() + ": " + e.getMessage();
                }
            }
            return "Thread is null";
        }

        /**
         * Appends request's log properties and does the final <code>INFO</code> logging.
         *
         * @param entry The request entry
         * @param logBuilder The log builder
         * @param trace The request's trace
         */
        private static void appendLogPropertiesAndLog(RequestRegistryEntry entry, LogMessageBuilder logBuilder, Throwable trace) {
            // Append log properties from the ThreadLocal to logBuilder
            appendLogProperties(entry, logBuilder);

            // Add stack trace of tracked request
            logBuilder.add(trace);
            LOG.info(logBuilder.getMessage(), logBuilder.getArgumentsAsArray()); // NOSONARLINT
        }

        /**
         * Appends request's log properties to given log builder.
         *
         * @param entry The request entry
         * @param logBuilder The log builder
         */
        private static void appendLogProperties(RequestRegistryEntry entry, LogMessageBuilder logBuilder) {
            Map<String, String> propertyMap = entry.getPropertyMap();
            if (null != propertyMap) {
                // Sort the properties for readability and add them to the logBuilder (if any)
                Iterator<Map.Entry<String, String>> it = new TreeMap<String, String>(propertyMap).entrySet().iterator();
                if (it.hasNext()) {
                    logBuilder.append(" Request's properties:");
                    do {
                        Map.Entry<String, String> propertyEntry = it.next();
                        logBuilder.lfappend("  {}={}", propertyEntry.getKey(), propertyEntry.getValue());
                    } while (it.hasNext());
                }
            }
        }

    } // End of class Watcher

    /**
     * Creates a new age info for given arguments.
     *
     * @param age The current age
     * @param requestMaxAge The age threshold
     * @return The age info
     */
    protected static AgeInfo newAgeInfo(long age, int requestMaxAge) {
        return new AgeInfo(formatDecimal(age), formatDecimal(requestMaxAge));
    }

    private static String formatDecimal(long number) {
        StringBuilder builder = new StringBuilder(Long.toString(number));
        for (int offset = builder.length() - 3; offset > 0; offset -= 3) {
            builder.insert(offset, ',');
        }
        return builder.toString();
    }

    private static final class AgeInfo {

        final String sAge;
        final String sMaxAge;

        AgeInfo(String sAge, String sMaxAge) {
            super();
            this.sAge = sAge;
            this.sMaxAge = sMaxAge;
        }
    }

}
