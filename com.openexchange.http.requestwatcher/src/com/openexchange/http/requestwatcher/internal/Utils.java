/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.http.requestwatcher.internal;

import com.openexchange.sessiond.SessiondService;

/**
 * {@link Utils} - Utilities for request watcher.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class Utils {

    /**
     * Initializes a new instance of {@link Utils}.
     */
    private Utils() {
        super();
    }

    /**
     * Checks if specified session identifier refers to an invalid session (e.g. there is no such active session).
     *
     * @param sessionId The session identifier to check for
     * @return <code>true</code> if session identifier refers to an invalid session; otherwise <code>false</code>
     */
    public static boolean isInvalidSession(String sessionId) {
        if (sessionId == null) {
            // Cannot check for null session identifier. Signal as valid then...
            return false;
        }

        SessiondService sessiondService = SessiondService.SERVICE_REFERENCE.get();
        return sessiondService != null && !sessiondService.isActive(sessionId);
    }

    /**
     * Checks if specified stack trace hints to a file up- or download.
     *
     * @param trace The stack trace to examine
     * @return <code>true</code> for a file up- or download; otherwise <code>false</code>
     */
    public static boolean isDownloadOrUploadRequest(StackTraceElement[] trace) {
        for (StackTraceElement ste : trace) {
            String className = ste.getClassName();
            if (null != className) {
                if (className.startsWith("org.apache.commons.fileupload.MultipartStream$ItemInputStream") ||
                    className.startsWith("com.openexchange.tools.file.FileStreamAction")) {
                    // A long-running file upload
                    return true;
                }
                if (className.startsWith("com.openexchange.ajax.requesthandler.responseRenderers.actions.OutputBinaryContentAction")) {
                    // A long-running file download
                    return true;
                }
                if (className.startsWith("com.openexchange.admin.tools.filestore.FilestoreDataMover")) {
                    // A long-running filestore data move
                    return true;
                }
            }
        }
        return false;
    }

}
