/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.snippet.json.action;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import com.openexchange.java.Strings;

/**
 * An enumeration for HTTP methods.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public enum Method {

    /**
     * The GET method
     */
    GET,
    /**
     * The PUT method
     */
    PUT,
    /**
     * The POST method
     */
    POST,
    /**
     * The DELETE method
     */
    DELETE;

    private static final Map<String, Method> MAP = Map.copyOf(Arrays.stream(Method.values()).collect(Collectors.toMap(m -> Strings.asciiLowerCase(m.name()), m -> m)));

    /**
     * Gets the appropriate method.
     *
     * @param method The method identifier
     * @return The appropriate method or <code>null</code>
     */
    public static Method methodFor(String method) {
        return null == method ? null : MAP.get(Strings.asciiLowerCase(method.trim()));
    }
}
