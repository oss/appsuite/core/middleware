/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.metrics;

/**
 * {@link CacheMetricName} - Names for cache metrics.
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public enum CacheMetricName {

    PUT("put", "Put operation runtime"),
    GET("get", "Get operation runtime"),
    INVALIDATION("invalidation", "Invalidation operation runtime"),
    KEY_RETRIEVAL("keys.retrieval", "Keys retrieval runtime"),

    ERROR("error", "Number of errors"),
    PUTS_TOTAL("puts.total", "Number of total puts"),
    HITS_TOTAL("hits.total", "Number of cache hits"),
    REMOVALS_TOTAL("removals.total", "Number of total remove from cache operations"),
    MISSES_TOTAL("misses.total", "Number of cache misses"),

    THREAD_LOCAL_PUT("threadlocal.put", "Thread-local put operation runtime"),
    THREAD_LOCAL_GET("threadlocal.get", "Thread-local get operation runtime"),

    THREAD_LOCAL_HITS_TOTAL("threadlocal.hits.total", "Number of thread-local cache hits"),
    THREAD_LOCAL_MISSES_TOTAL("threadlocal.misses.total", "Number of thread-local cache misses"),
    ;

    private final String name;
    private final String fqn;
    private final String description;

    /**
     * Initializes a new {@link CacheMetricName}.
     */
    private CacheMetricName(String name, String description) {
        this.name = name;
        this.fqn = "appsuite.redis.cache." + name;
        this.description = description;
    }

    /**
     * Gets the name; e.g. <code>"puts.total"</code>.
     *
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the description
     *
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the fully qualified name of the metric; e.g. <code>"appsuite.redis.cache.put"</code>.
     *
     * @return The fully qualified name of the metric
     */
    public String getFqn() {
        return fqn;
    }

}
