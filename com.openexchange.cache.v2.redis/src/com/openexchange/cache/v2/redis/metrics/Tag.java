/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.metrics;

/**
 * {@link Tag} - An enumeration of known tags used for cache metrics.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public enum Tag {

    /**
     * The tag name for application.
     */
    APP("app"),
    /**
     * The tag name for module.
     */
    MODULE("module");

    private final String id;

    /**
     * Initializes a new {@link Tag}.
     *
     * @param tag the tag
     */
    private Tag(String tag) {
        this.id = tag;
    }

    /**
     * Gets the tag
     *
     * @return The tag
     */
    public String getId() {
        return id;
    }
}
