/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.metrics;

import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.cache.v2.filter.ApplicationName;
import com.openexchange.cache.v2.redis.RedisCacheProperty;
import com.openexchange.cache.v2.redis.key.RedisCacheKey;
import com.openexchange.cache.v2.redis.key.RedisCacheKeyUtil;
import com.openexchange.cache.v2.redis.osgi.Services;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.ForcedReloadable;
import com.openexchange.config.lean.LeanConfigurationService;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;

/**
 * {@link CacheMetricsCollector}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public final class CacheMetricsCollector implements ForcedReloadable {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(CacheMetricsCollector.class); // NOSONARLINT
    }

    private static final CacheMetricsCollector INSTANCE = new CacheMetricsCollector();

    /**
     * Gets the metrics collector instance.
     *
     * @return The instance
     */
    public static CacheMetricsCollector getInstance() {
        return INSTANCE;
    }

    private static volatile Boolean collectThreadLocalMetrics;

    /**
     * Checks whether to collect thread-local metrics.
     *
     * @return <code>true</code> to collect thread-local metrics; otherwise <code>false</code>
     */
    public static boolean collectThreadLocalMetrics() {
        Boolean b = collectThreadLocalMetrics;
        if (b == null) {
            RedisCacheProperty property = RedisCacheProperty.METRICS_THREAD_LOCAL;

            LeanConfigurationService configService = Services.optService(LeanConfigurationService.class);
            if (configService == null) {
                return property.getDefaultValue(Boolean.class).booleanValue();
            }

            b = Boolean.valueOf(configService.getBooleanProperty(property));
            collectThreadLocalMetrics = b;
        }
        return b.booleanValue();
    }

    // -------------------------------------------------------- MEMBERS/CONSTRUCTOR --------------------------------------------------------

    private final Cache<CacheMetricName, Timer> timers;
    private final Cache<String, Counter> counters;

    /**
     * Initializes a new {@link CacheMetricsCollector}.
     */
    private CacheMetricsCollector() {
        super();
        timers = CacheBuilder.newBuilder().build();
        counters = CacheBuilder.newBuilder().build();
    }

    // -------------------------------------------------------- RELOADABLE -----------------------------------------------------------------

    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        collectThreadLocalMetrics = null; // NOSONARLINT
    }

    // -------------------------------------------------------- TIMERS ---------------------------------------------------------------------

    /**
     * Measures the running time of a put operation
     *
     * @param timeNanos The time in nanoseconds
     */
    public void timePut(long timeNanos) {
        recordTime(timeNanos, CacheMetricName.PUT);
    }

    /**
     * Measures the running time of a get operation
     *
     * @param timeNanos The time in nanoseconds
     */
    public void timeGet(long timeNanos) {
        recordTime(timeNanos, CacheMetricName.GET);
    }

    /**
     * Measures the running time of an invalidation operation
     *
     * @param timeNanos The time in nanoseconds
     */
    public void timeInvalidation(long timeNanos) {
        recordTime(timeNanos, CacheMetricName.INVALIDATION);
    }

    /**
     * Measures the running time of a keys retrieval operation
     *
     * @param timeNanos The time in nanoseconds
     */
    public void timeKeyRetrieval(long timeNanos) {
        recordTime(timeNanos, CacheMetricName.KEY_RETRIEVAL);
    }

    /**
     * Measures the running time of a thread-local put operation
     *
     * @param timeNanos The time in nanoseconds
     */
    public void timeThreadLocalPut(long timeNanos) {
        recordTime(timeNanos, CacheMetricName.THREAD_LOCAL_PUT);
    }

    /**
     * Measures the running time of a thread-local get operation
     *
     * @param timeNanos The time in nanoseconds
     */
    public void timeThreadLocalGet(long timeNanos) {
        recordTime(timeNanos, CacheMetricName.THREAD_LOCAL_GET);
    }

    // -------------------------------------------------------- COUNTERS -------------------------------------------------------------------

    /**
     * Increments the amount of errors.
     */
    public void incrementErrors() {
        incrementCounter(null, CacheMetricName.ERROR);
    }

    /**
     * Increments the amount of puts
     * for the module denoted by the specified key
     *
     * @param key The key
     */
    public void incrementPuts(RedisCacheKey key) {
        incrementCounter(key, CacheMetricName.PUTS_TOTAL);
    }

    /**
     * Increments the amount of gets
     * for the module denoted by the specified key
     *
     * @param key The key
     */
    public void incrementGets(RedisCacheKey key) {
        incrementCounter(key, CacheMetricName.HITS_TOTAL);
    }

    /**
     * Increments the amount of invalidations
     * for the module denoted by the specified key
     *
     * @param key The key
     */
    public void incrementInvalidations(RedisCacheKey key) {
        incrementCounter(key, CacheMetricName.REMOVALS_TOTAL);
    }

    /**
     * Increments the amount of misses
     * for the module denoted by the specified key
     *
     * @param key The key
     */
    public void incrementMisses(RedisCacheKey key) {
        incrementCounter(key, CacheMetricName.MISSES_TOTAL);
    }

    /**
     * Increments the amount of thread-local gets
     * for the module denoted by the specified key
     *
     * @param key The key
     */
    public void incrementThreadLocalGets(RedisCacheKey key) {
        incrementCounter(key, CacheMetricName.THREAD_LOCAL_HITS_TOTAL);
    }

    /**
     * Increments the amount of thread-local misses
     * for the module denoted by the specified key
     *
     * @param key The key
     */
    public void incrementThreadLocalMisses(RedisCacheKey key) {
        incrementCounter(key, CacheMetricName.THREAD_LOCAL_MISSES_TOTAL);
    }

    // -------------------------------------------------------- HELPERS --------------------------------------------------------------------

    /**
     * Ticks the metrics for the specified metric name.
     *
     * @param metricName The metric name
     * @param now The time stamp when operation began
     */
    public void tickMetrics(CacheMetricName metricName, long now) {
        switch (metricName) {
            case GET:
                timeGet(System.nanoTime() - now);
                break;
            case INVALIDATION:
                timeInvalidation(System.nanoTime() - now);
                break;
            case PUT:
                timePut(System.nanoTime() - now);
                break;
            case KEY_RETRIEVAL:
                timeKeyRetrieval(System.nanoTime() - now);
                break;
            case THREAD_LOCAL_GET:
                timeThreadLocalGet(System.nanoTime() - now);
                break;
            case THREAD_LOCAL_PUT:
                timeThreadLocalPut(System.nanoTime() - now);
                break;
            default:
                break;
        }
    }

    /**
     * Records the run time of the specified metric
     *
     * @param timeNanos The run time in nanoseconds
     * @param metricName The metric name
     */
    private void recordTime(long timeNanos, CacheMetricName metricName) {
        try {
            Timer timer = timers.getIfPresent(metricName);
            if (timer == null) {
                // @formatter:off
                timer = timers.get(metricName, () -> Timer.builder(metricName.getFqn())
                    .description(metricName.getDescription())
                    .serviceLevelObjectives(registerSLOBoundaries())
                    .register(Metrics.globalRegistry));
                // @formatter:on
            }
            timer.record(timeNanos, TimeUnit.NANOSECONDS);
        } catch (ExecutionException e) {
            LoggerHolder.LOGGER.debug("Error while collecting metric for {}", metricName.getDescription().toLowerCase(), e);
        }
    }

    /**
     * Increments the specified metric's value for the module denoted by the specified key
     *
     * @param optKey The key or <code>null</code>
     * @param metric The metric name
     */
    private void incrementCounter(RedisCacheKey optKey, CacheMetricName metricName) {
        try {
            String moduleMetricName = optKey == null ? metricName.getName() : new StringBuilder(RedisCacheKeyUtil.createModulePortion(optKey)).append(metricName.getName()).toString();
            Counter counter = counters.getIfPresent(moduleMetricName);
            if (counter == null) {
                // @formatter:off
                if (optKey == null) {
                    counter = counters.get(moduleMetricName, () -> Counter.builder(metricName.getFqn())
                        .description(metricName.getDescription())
                        .register(Metrics.globalRegistry));
                } else {
                    counter = counters.get(moduleMetricName, () -> Counter.builder(metricName.getFqn())
                        .description(metricName.getDescription())
                        .tags(Tag.APP.getId(), ApplicationName.getInstance().getName(), Tag.MODULE.getId(), optKey.getModule().getName())
                        .register(Metrics.globalRegistry));
                }
                // @formatter:on
            }
            counter.increment();
        } catch (ExecutionException e) {
            if (optKey == null) {
                LoggerHolder.LOGGER.debug("Error while incrementing counter", e);
            } else {
                LoggerHolder.LOGGER.debug("Error while incrementing counter for '{}'", optKey.getFQN(), e);
            }
        }
    }

    /**
     * The SLO boundaries
     *
     * @return the SLO boundaries
     */
    private Duration[] registerSLOBoundaries() {
        // @formatter:off
        return new Duration[] {
            Duration.ofNanos(100000),
            Duration.ofNanos(120000),
            Duration.ofNanos(125000),
            Duration.ofNanos(130000),
            Duration.ofNanos(140000),
            Duration.ofNanos(150000),
            Duration.ofNanos(160000),
            Duration.ofNanos(170000),
            Duration.ofNanos(180000),
            Duration.ofNanos(200000),
            Duration.ofNanos(250000),
            Duration.ofNanos(500000),
            Duration.ofNanos(1000000),
            Duration.ofNanos(2000000),
            Duration.ofNanos(2500000),
            Duration.ofNanos(5000000)
        };
        // @formatter:on
    }
}
