/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.osgi;

import static com.openexchange.cache.v2.redis.events.CacheEventChannelImpl.CACHE_EVENT_CHANNEL_KEY;
import static com.openexchange.cache.v2.redis.events.CacheEventChannelImpl.LISTENER_CHANNEL_KEY;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.events.CacheEvent;
import com.openexchange.cache.v2.events.CacheEventService;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;
import com.openexchange.cache.v2.redis.GlobalRedisCacheOptions;
import com.openexchange.cache.v2.redis.RedisCacheProperty;
import com.openexchange.cache.v2.redis.RedisCacheService;
import com.openexchange.cache.v2.redis.events.CacheEventChannelImpl;
import com.openexchange.cache.v2.redis.events.CacheEventChannelMessageCodec;
import com.openexchange.cache.v2.redis.events.CacheListenerChannelMessageCodec;
import com.openexchange.cache.v2.redis.events.CacheListenerName;
import com.openexchange.cache.v2.redis.inmemory.InMemoryCacheOptions;
import com.openexchange.cache.v2.redis.inmemory.Mode;
import com.openexchange.cache.v2.redis.metrics.CacheMetricsCollector;
import com.openexchange.cache.v2.redis.threadlocal.ThreadLocalAwareCacheService;
import com.openexchange.config.ForcedReloadable;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.lock.LockService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.redis.RedisConnectorService;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.timer.TimerService;

/**
 * {@link RedisCacheServiceActivator}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RedisCacheServiceActivator extends HousekeepingActivator {

    private CacheEventChannelImpl cacheEventChannel;
    private RedisCacheService redisCacheService;

    /**
     * Initializes a new {@link RedisCacheServiceActivator}.
     */
    public RedisCacheServiceActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { RedisConnectorService.class, PubSubService.class, LeanConfigurationService.class, TimerService.class, ThreadPoolService.class };
    }

    @Override
    protected boolean stopOnServiceUnavailability() {
        return true;
    }

    @Override
    protected synchronized void startBundle() throws Exception {
        Logger logger = LoggerFactory.getLogger(RedisCacheServiceActivator.class);
        try {
            logger.info("Starting bundle {}", context.getBundle().getSymbolicName());

            Services.setServiceLookup(this);
            trackService(LockService.class);
            openTrackers();

            registerService(ForcedReloadable.class, CacheMetricsCollector.getInstance());

            PubSubService pubSubService = getService(PubSubService.class);
            Channel<CacheEvent> channel = pubSubService.getChannel(CACHE_EVENT_CHANNEL_KEY, CacheEventChannelMessageCodec.getInstance());
            Channel<CacheListenerName> listenerChannel = pubSubService.getChannel(LISTENER_CHANNEL_KEY, CacheListenerChannelMessageCodec.getInstance());
            CacheEventChannelImpl cacheEventChannel = new CacheEventChannelImpl(channel, listenerChannel);
            this.cacheEventChannel = cacheEventChannel;

            LeanConfigurationService configService = getService(LeanConfigurationService.class);

            InMemoryCacheOptions inMemoryCacheOptions = initInMemoryCacheOptions(configService);
            boolean withMetrics = configService.getBooleanProperty(RedisCacheProperty.WITH_METRICS);
            boolean useThreadLocalCache = configService.getBooleanProperty(RedisCacheProperty.THREAD_LOCAL);
            boolean trackPutsInThreadLocalCache = configService.getBooleanProperty(RedisCacheProperty.TRACK_PUTS_IN_THREAD_LOCAL_CACHE);
            logger.info("Successfully initialized in-memory intermediate cache using: {}", inMemoryCacheOptions);

            GlobalRedisCacheOptions globalCacheOptions = GlobalRedisCacheOptions.builder()
                .withDisableHashExpiration(configService.getBooleanProperty(RedisCacheProperty.DISABLE_HASH_EXPIRATION))
                .withOrchestrateConcurrentRetrieval(configService.getBooleanProperty(RedisCacheProperty.ORCHESTRATE_CONCURRENT_RETRIEVAL))
                .withMultiKeyLimit(configService.getIntProperty(RedisCacheProperty.MULTI_KEY_LIMIT))
                .build();
            if (globalCacheOptions.isDisableHashExpiration()) {
                logger.warn("Hash expiration is disabled via configuration. Cached values may remain in Redis indefinitely, ensure a proper maxmemory-policy is configured.");
            }

            RedisCacheService redisCacheService;
            if (useThreadLocalCache && inMemoryCacheOptions.isDisabled()) {
                // Thread-local cache is supposed being used and no in-memory cache
                redisCacheService = initRedisCacheService(getService(RedisConnectorService.class), cacheEventChannel, false, withMetrics, globalCacheOptions, inMemoryCacheOptions);
                ThreadLocalAwareCacheService threadLocalAwareCacheService = new ThreadLocalAwareCacheService(redisCacheService, trackPutsInThreadLocalCache);
                registerService(CacheEventService.class, cacheEventChannel);
                registerService(CacheService.class, threadLocalAwareCacheService);
                registerService(InvalidationCacheService.class, threadLocalAwareCacheService);
                logger.info("Successfully initialized Redis cache with thread-local intermediate cache using: {}", globalCacheOptions);
            } else {
                // Either no thread-local cache is supposed being used or there is an in-memory cache
                redisCacheService = initRedisCacheService(getService(RedisConnectorService.class), cacheEventChannel, true, withMetrics, globalCacheOptions, inMemoryCacheOptions);
                registerService(CacheEventService.class, cacheEventChannel);
                registerService(CacheService.class, redisCacheService);
                registerService(InvalidationCacheService.class, redisCacheService);
                logger.info("Successfully initialized Redis cache with {} in-memory intermediate cache using: {}", inMemoryCacheOptions.getMode().getIdentifier(), globalCacheOptions);
            }

            this.redisCacheService = redisCacheService;
            logger.info("Bundle {} started successfully.", context.getBundle().getSymbolicName());
        } catch (Exception e) { // NOSONARLINT
            logger.error("Error starting bundle {}", context.getBundle().getSymbolicName(), e);
            throw e;
        }
    }

    /**
     * Initializes the options for in-memory cache using given configuration service.
     *
     * @param configService The configuration service to read properties from
     * @return The in-memory cache options to use for individual cache regions
     */
    private static InMemoryCacheOptions initInMemoryCacheOptions(LeanConfigurationService configService) {
        boolean useInMemoryCache = configService.getBooleanProperty(RedisCacheProperty.INMEMORY_ENABLED);
        if (!useInMemoryCache) {
            // Check deprecated property
            useInMemoryCache = configService.getBooleanProperty(RedisCacheProperty.USE_INMEMORY_CACHE_OLD);
        }
        if (!useInMemoryCache) {
            return InMemoryCacheOptions.DISABLED;
        }

        // Compile in-memory cache configuration
        InMemoryCacheOptions.Builder optionsBuilder = InMemoryCacheOptions.builder();
        optionsBuilder.withEnabled(true);
        optionsBuilder.withTimeToLoveMillis(configService.getLongProperty(RedisCacheProperty.INMEMORY_TTL_MILLIS));
        String sMode = configService.getProperty(RedisCacheProperty.INMEMORY_MODE);
        if (Strings.isEmpty(sMode)) {
            // Check deprecated property
            sMode = configService.getProperty(RedisCacheProperty.INMEMORY_CACHE_MODE_OLD);
        }
        optionsBuilder.withMode(Mode.modeFor(sMode, Mode.VOLATILE));
        return optionsBuilder.build();
    }

    /**
     * Initializes the Redis cache service.
     *
     * @param connectorService The Redis connector service used to establish connections against Redis storage
     * @param cacheEventChannel The channel to distribute cache events
     * @param cacheViews Whether to cache individual Redis cache instances representing a certain cache region
     * @param withMetrics Whether to set/track cache metrics
     * @param globalCacheOptions The global options for Redis-backed cache
     * @param inMemoryCacheOptions The options for in-memory cache
     * @return The Redis cache service using specified parameters
     * @throws OXException If initializing the Redis cache service fails
     */
    private RedisCacheService initRedisCacheService(RedisConnectorService connectorService, CacheEventChannelImpl cacheEventChannel, boolean cacheViews, boolean withMetrics, GlobalRedisCacheOptions globalCacheOptions, InMemoryCacheOptions inMemoryCacheOptions) throws OXException {
        RedisCacheService cacheService = new RedisCacheService(connectorService.getCacheConnectorProviderOrElseRegular().getConnector(), cacheEventChannel, cacheViews, withMetrics, globalCacheOptions, inMemoryCacheOptions, this);
        cacheEventChannel.setCacheService(cacheService);
        return cacheService;
    }

    @Override
    protected synchronized void stopBundle() throws Exception {
        Logger logger = LoggerFactory.getLogger(RedisCacheServiceActivator.class);
        try {
            logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
            super.stopBundle();

            RedisCacheService redisCacheService = this.redisCacheService;
            if (redisCacheService != null) {
                this.redisCacheService = null;
                redisCacheService.stop();
            }

            CacheEventChannelImpl cacheEventChannel = this.cacheEventChannel;
            if (cacheEventChannel != null) {
                this.cacheEventChannel = null;
                cacheEventChannel.stop();
            }

            logger.info("Bundle {} stopped successfully.", context.getBundle().getSymbolicName());
        } catch (Exception e) { // NOSONARLINT
            logger.error("Error stopping bundle {}", context.getBundle().getSymbolicName(), e);
            throw e;
        } finally {
            Services.setServiceLookup(null);
        }
    }
}
