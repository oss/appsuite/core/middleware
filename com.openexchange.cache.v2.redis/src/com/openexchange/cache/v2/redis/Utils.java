/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2.redis;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import org.slf4j.Logger;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyType;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.codec.CacheValueCodec;
import com.openexchange.cache.v2.redis.key.RedisCacheKey;

/**
 * {@link Utils} - Utility class for Redis cache.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class Utils {

    /**
     * Predicate to check that a {@link RedisCacheKey} is not of type {@link CacheKeyType#GROUP} - otherwise, an
     * {@link IllegalArgumentException} is raised.
     */
    public static final Predicate<? super RedisCacheKey> PREDICATE_NO_GROUP_KEY = redisKey -> {
        if (redisKey.getType() == CacheKeyType.GROUP) {
            throw createIsGroupKeyException();
        }
        return true;
    };

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(Utils.class); // NOSONARLINT
    }

    /**
     * Initializes a new instance of {@link Utils}.
     */
    private Utils() {
        super();
    }

    /**
     * Adds given collection to specified target list, optionally creating the list if <code>null</code>.
     *
     * @param <E> The element type
     * @param toAdd The chunk to add
     * @param sink The target list to add to; may be <code>null</code>
     * @return The list containing the elements
     */
    public static <E> List<E> addOrCreate(Collection<E> toAdd, List<E> sink) {
        if (sink == null) {
            return new ArrayList<>(toAdd);
        }
        sink.addAll(toAdd);
        return sink;
    }

    /**
     * Adds given element to specified target list, optionally creating the list if <code>null</code>.
     *
     * @param <E> The element type
     * @param toAdd The element to add
     * @param sink The target list to add to; may be <code>null</code>
     * @param numElements The desired number of elements in case needs to be newly created or <code>-1</code> to use default capacity
     * @return The list containing the elements
     */
    public static <E> List<E> addOrCreate(E toAdd, List<E> sink, int numElements) {
        List<E> list = sink;
        if (list == null) {
            list = numElements >= 0 ? new ArrayList<>(numElements) : new ArrayList<>();
        }
        list.add(toAdd);
        return list;
    }

    /**
     * Returns given {@link Map} if non-<code>null</code>; otherwise creates a new {@link HashMap} and returns it.
     *
     * @param <K> The type of the key
     * @param <V> The type of the value
     * @param map The map to check for <code>null</code>
     * @param numMappings The desired number of mappings in case needs to be newly created or <code>-1</code> to use default capacity
     * @return The given map or a newly created one
     */
    public static <K, V> Map<K, V> returnOrCreateHashMap(Map<K, V> map, int numMappings) {
        return map == null ? (numMappings >= 0 ? HashMap.newHashMap(numMappings) : new HashMap<>()): map;
    }

    /**
     * Returns given {@link Map} if non-<code>null</code>; otherwise creates a new {@link LinkedHashMap} and returns it.
     *
     * @param <K> The type of the key
     * @param <V> The type of the value
     * @param map The map to check for <code>null</code>
     * @param numMappings The desired number of mappings in case needs to be newly created or <code>-1</code> to use default capacity
     * @return The given map or a newly created one
     */
    public static <K, V> Map<K, V> returnOrCreateLinkedHashMap(Map<K, V> map, int numMappings) {
        return map == null ? (numMappings >= 0 ? LinkedHashMap.newLinkedHashMap(numMappings) : new LinkedHashMap<>()) : map;
    }

    /**
     * Creates a new <code>StringBuilder</code> in case given reference is <code>null</code>; otherwise resets & returns given instance.
     *
     * @param sb The <code>StringBuilder</code> reference
     * @return The <code>StringBuilder</code> instance to use
     */
    public static StringBuilder createOrReset(StringBuilder sb) {
        if (sb == null) {
            return new StringBuilder(32);
        }
        sb.setLength(0);
        return sb;
    }

    /**
     * Checks if given key is an instance of {@link RedisCacheKey}.
     *
     * @param key The cache key to check
     * @return The expected instance of <code>RedisCacheKey</code>
     * @throws IllegalArgumentException If given cache key is <b>not</b> an instance of <code>RedisCacheKey</code>
     */
    public static RedisCacheKey keyCheck(CacheKey key) {
        if (key instanceof RedisCacheKey rkc) {
            return rkc;
        }
        if (key == null) {
            throw new IllegalArgumentException("Cache key must not be null");
        }
        throw new IllegalArgumentException("Unsupported cache key instance: " + key.getFQN() + " (" + key.getClass().getName() + ")");
    }

    /**
     * Tries to deserialize specified data to appropriate Java object using the option's codec.
     *
     * @param <V> The type to deserialize to
     * @param data The data to read
     * @param options The cache options to use
     * @return The Java object or <code>null</code> if deserialization failed
     */
    public static <V> V deserializeSafe(InputStream data, CacheOptions<V> options) {
        return deserializeSafe(data, options.getCodec());
    }

    /**
     * Tries to deserialize specified data to appropriate Java object using the supplied codec.
     *
     * @param <V> The type to deserialize to
     * @param data The data to read
     * @param codec The cache codec to use
     * @return The Java object or <code>null</code> if deserialization fails
     */
    public static <V> V deserializeSafe(InputStream data, CacheValueCodec<V> codec) {
        try {
            return codec.deserialize(data);
        } catch (Exception e) {
            LoggerHolder.LOG.warn("Failed to deserialize data for cache codec {}", codec, e);
            return null;
        }
    }

    /**
     * Creates a new <code>IllegalArgumentException</code> signaling that specified cache key denotes a group, but shouldn't.
     *
     * @return The newly created <code>IllegalArgumentException</code>
     */
    public static IllegalArgumentException createIsGroupKeyException() {
        return new IllegalArgumentException("Specified cache key denotes a group, but should be a key or group member key");
    }

}
