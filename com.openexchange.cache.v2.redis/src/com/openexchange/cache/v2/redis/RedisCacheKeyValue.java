/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis;

import java.util.Objects;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;

/**
 * {@link RedisCacheKeyValue} - A Redis pair of a cache key and its value.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class RedisCacheKeyValue<V> implements CacheKeyValue<V> {

    /**
     * Creates a {@link RedisCacheKeyValue} from a {@code key} and{@code value}. The resulting value contains the value if the
     * {@code value} is not null.
     *
     * @param key the key, must not be {@code null}.
     * @param value the value. May be {@code null}.
     * @return the {@link RedisCacheKeyValue}
     */
    public static <T extends V, V> RedisCacheKeyValue<V> fromNullable(CacheKey key, T value) {
        return value == null ? empty(key) : new RedisCacheKeyValue<V>(key, value);
    }

    /**
     * Returns an empty {@code RedisCacheKeyValue} instance with the {@code key} set. No value is present for this instance.
     *
     * @param key the key, must not be {@code null}.
     * @return the {@link RedisCacheKeyValue}
     */
    public static <V> RedisCacheKeyValue<V> empty(CacheKey key) {
        return new RedisCacheKeyValue<V>(key, null);
    }

    /**
     * Creates a {@link RedisCacheKeyValue} from a {@code key} and {@code value}. The resulting value contains the value.
     *
     * @param key the key. Must not be {@code null}.
     * @param value the value. Must not be {@code null}.
     * @return the {@link RedisCacheKeyValue}
     */
    public static <T extends V, V> RedisCacheKeyValue<V> just(CacheKey key, T value) {
        Objects.requireNonNull(value, "Value must not be null");
        return new RedisCacheKeyValue<V>(key, value);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final CacheKey key;
    private final V value;

    /**
     * Initializes a new instance of {@link RedisCacheKeyValue}.
     *
     * @param key The Redis cache key
     * @param value The associated value
     */
    private RedisCacheKeyValue(CacheKey key, V value) {
        super();
        Objects.requireNonNull(key, "Key must not be null");
        this.key = key;
        this.value = value;
    }

    @Override
    public CacheKey getKey() {
        return key;
    }

    @Override
    public V getValueOrElse(V other) {
        return value == null ? other : value;
    }

}
