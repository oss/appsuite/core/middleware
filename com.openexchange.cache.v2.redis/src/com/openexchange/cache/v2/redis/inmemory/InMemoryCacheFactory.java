/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.inmemory;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import com.openexchange.cache.v2.filter.ModuleName;
import com.openexchange.cache.v2.filter.VersionName;
import com.openexchange.cache.v2.redis.inmemory.impl.UpdateAfterFetchInMemoryCache;
import com.openexchange.cache.v2.redis.inmemory.impl.VolatileInMemoryCache;

/**
 * {@link InMemoryCacheFactory} - The factory for in-memory cache instances.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class InMemoryCacheFactory {

    private static final InMemoryCacheFactory INSTANCE = new InMemoryCacheFactory();

    /**
     * Gets the instance
     *
     * @return The instance
     */
    public static InMemoryCacheFactory getInstance() {
        return INSTANCE;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final ConcurrentMap<Key, InMemoryCache<?>> caches;

    /**
     * Initializes a new instance of {@link InMemoryCacheFactory}.
     */
    private InMemoryCacheFactory() {
        super();
        caches = new ConcurrentHashMap<>();
    }

    /**
     * Gets the in-meory cache for given module and version name.
     *
     * @param <V> The type of the cached value
     * @param moduleName The module name of the associated cache region
     * @param versionName The version name of the associated cache region
     * @return The in-meory cache or empty
     */
    public <V> Optional<InMemoryCache<V>> getInMemoryCache(ModuleName moduleName, VersionName versionName) {
        return Optional.ofNullable((InMemoryCache<V>) caches.get(new Key(moduleName, versionName)));
    }

    /**
     * Gets the in-meory cache for given module and version name.
     *
     * @param <V> The type of the cached value
     * @param moduleName The module name of the associated cache region
     * @param versionName The version name of the associated cache region
     * @return The in-meory cache or empty
     */
    @SuppressWarnings("rawtypes")
    public Optional<InMemoryCache> getInMemoryCacheRaw(ModuleName moduleName, VersionName versionName) {
        return Optional.ofNullable(caches.get(new Key(moduleName, versionName)));
    }

    /**
     * Gets all known in-memory caches.
     *
     * @return All known in-memory caches
     */
    public Iterable<InMemoryCache<?>> getInMemoryCaches() {
        return caches.values();
    }

    /**
     * Creates the appropriate in-meory cache from this options.
     *
     * @param <V> The type of the cached value
     * @param options The options to use to build the in-memory cache
     * @param moduleName The module name of the associated cache region
     * @param versionName The version name of the associated cache region
     * @return The in-meory cache or empty
     */
    public <V> Optional<InMemoryCache<V>> getOrCreateInMemoryCache(InMemoryCacheOptions options, ModuleName moduleName, VersionName versionName) {
        if (options.isDisabled()) {
            return Optional.empty();
        }

        Key key = new Key(moduleName, versionName);
        InMemoryCache<V> inMemoryCache = (InMemoryCache<V>) caches.get(key);
        if (inMemoryCache == null) {
            InMemoryCache<V> newInMemoryCache = switch (options.getMode()) {
                case UPDATE_AFTER_FETCH -> new UpdateAfterFetchInMemoryCache<V>(options.getTimeToLoveMillis());
                case VOLATILE -> new VolatileInMemoryCache<V>(options.getTimeToLoveMillis());
                default -> throw new IllegalArgumentException("Unknown mode: " + options.getMode().getIdentifier());
            };
            inMemoryCache = (InMemoryCache<V>) caches.putIfAbsent(key, newInMemoryCache);
            if (inMemoryCache == null) {
                inMemoryCache = newInMemoryCache;
            }
        }
        return Optional.of(inMemoryCache);
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    private static final class Key implements Comparable<Key> {

        private final String module;
        private final String version;
        private final int hash;

        /**
         * Initializes a new instance of {@link Key}.
         *
         * @param moduleName The module name of the associated cache region
         * @param versionName The version name of the associated cache region
         */
        Key(ModuleName moduleName, VersionName versionName) {
            super();
            this.module = moduleName.getName();
            this.version = versionName.getName();
            int prime = 31;
            int result = 1;
            result = prime * result + ((module == null) ? 0 : module.hashCode());
            result = prime * result + ((version == null) ? 0 : version.hashCode());
            this.hash = result;
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Key other = (Key) obj;
            if (module == null) {
                if (other.module != null) {
                    return false;
                }
            } else if (!module.equals(other.module)) {
                return false;
            }
            if (version == null) {
                if (other.version != null) {
                    return false;
                }
            } else if (!version.equals(other.version)) {
                return false;
            }
            return true;
        }

        @Override
        public int compareTo(Key o) {
            int c = module.compareTo(o.module);
            return c == 0 ? version.compareTo(o.version) : c;
        }
    }

}
