/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2.redis.inmemory.impl;

import java.time.Duration;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.slf4j.Logger;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheLoader;
import com.openexchange.cache.v2.redis.RedisCache;
import com.openexchange.cache.v2.redis.inmemory.InMemoryCache;
import com.openexchange.cache.v2.redis.inmemory.Mode;
import com.openexchange.cache.v2.redis.key.RedisCacheKey;
import com.openexchange.cache.v2.redis.osgi.Services;
import com.openexchange.threadpool.Task;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.threadpool.ThreadPools;
import com.openexchange.threadpool.ThreadRenamer;
import com.openexchange.threadpool.behavior.CallerRunsBehavior;

/**
 * {@link UpdateAfterFetchInMemoryCache} - The in-memory cache for Redis-backed cache that updates a fetched value in the background with a
 * fresh read from Redis storage.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class UpdateAfterFetchInMemoryCache<V> implements InMemoryCache<V> {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(UpdateAfterFetchInMemoryCache.class); // NOSONARLINT
    }

    /** The constant signaling ongoing cache update for a certain key w/o a loader */
    private static final Object PRESENT_NO_LOADER = new Object();

    /** The constant signaling ongoing cache update for a certain key w/ a loader */
    private static final Object PRESENT_LOADER = new Object();

    private final Cache<CacheKey, V> cache;
    private final ConcurrentMap<CacheKey, Object> updateSynchronizer;

    /**
     * Initializes a new {@link UpdateAfterFetchInMemoryCache}.
     *
     * @param timeToLoveMillis The time-to-live in volatile cache in milliseconds
     * @throws IllegalArgumentException If time-to-live milliseconds are equal to or less than <code>0</code> (zero)
     */
    public UpdateAfterFetchInMemoryCache(long timeToLoveMillis) {
        super();
        if (timeToLoveMillis <= 0) {
            throw new IllegalArgumentException("The time-to-live milliseconds must not be equal to or less than 0 (zero)");
        }
        this.cache = CacheBuilder.newBuilder().expireAfterWrite(Duration.ofMillis(timeToLoveMillis)).build();
        this.updateSynchronizer = new ConcurrentHashMap<>();
    }

    @Override
    public Mode getMode() {
        return Mode.UPDATE_AFTER_FETCH;
    }

    /**
     * Associates {@code value} with {@code redisKey} in this cache. If the cache previously contained a
     * value associated with {@code redisKey}, the old value is replaced by {@code value}.
     *
     * @param <V> The type of the value
     * @param redisKey The key
     * @param value The value
     */
    @Override
    public void put(CacheKey redisKey, V value) {
        cache.put(redisKey, value);
    }

    /**
     * Discards any cached value for key {@code redisKey}.
     *
     * @param redisKey The key
     */
    @Override
    public void invalidate(CacheKey redisKey) {
        cache.invalidate(redisKey);
    }

    @Override
    public void invalidateAll(Iterable<? extends CacheKey> keys) {
        cache.invalidateAll(keys);
    }

    /**
     * Gets the value associated with {@code redisKey} in this cache, or <code>null</code> if there is no cached value for {@code redisKey}.
     *
     * @param <V> The type of the value
     * @param redisKey The key
     * @return The associated value or <code>null</code>
     */
    @Override
    public V getIfPresent(CacheKey redisKey) {
        return cache.getIfPresent(redisKey);
    }

    /**
     * Submits invalidating given keys from in-memory cache.
     * <p>
     * If not possible to submit invalidation, the calling thread performs the invalidation.
     *
     * @param redisKeys The keys to invalidate
     */
    @Override
    public void submitInvalidationElseRun(Collection<? extends CacheKey> redisKeys) {
        if (redisKeys == null || redisKeys.isEmpty()) {
            return;
        }

        ThreadPoolService threadPool = Services.optService(ThreadPoolService.class);
        if (threadPool != null) {
            // Submit to thread pool
            threadPool.submit(ThreadPools.task(new InvalidateKeys<>(redisKeys, cache)), CallerRunsBehavior.getInstance());
        } else {
            // Perform with running thread
            cache.invalidateAll(redisKeys);
        }
    }

    /**
     * Submits updating the value associated with {@code redisKey} in this in-memory cache using given thread pool.
     *
     * @param redisKey The cache key
     * @param optLoader The loader to yield the appropriate value or <code>null</code>
     * @param redisCache The Redis cache to fetch recent value from
     * @param threadPool The thread pool to submit to
     */
    @Override
    public void submitUpdateInMemoryCache(CacheKey redisKey, CacheLoader<V> optLoader, RedisCache<V> redisCache, ThreadPoolService threadPool) {
        if (threadPool == null) {
            return;
        }

        boolean error = true; // Define here to avoid possible StackOverflowError preventing try-finally clause

        // Check either no concurrent update ongoing or w/ loader "overwrites" w/o loader
        if (optLoader == null ? (updateSynchronizer.putIfAbsent(redisKey, PRESENT_NO_LOADER) == null) : (updateSynchronizer.putIfAbsent(redisKey, PRESENT_LOADER) == null || updateSynchronizer.replace(redisKey, PRESENT_NO_LOADER, PRESENT_LOADER))) {
            try {
                threadPool.submit(new UpdateInMemoryCache<>(redisKey, optLoader, redisCache, this, updateSynchronizer), CallerRunsBehavior.getInstance());
                error = false;
            } finally {
                if (error) {
                    // Ensure entry is dropped if ThreadPoolService.submit() was not successfully invoked
                    if (optLoader == null) {
                        updateSynchronizer.remove(redisKey, PRESENT_NO_LOADER);
                    } else {
                        updateSynchronizer.remove(redisKey);
                    }
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** Simple task to update a certain in-memory cache entry with most recent value fetched from Redis storage */
    private static class UpdateInMemoryCache<V> implements Task<Void> {

        private final CacheKey redisKey;
        private final CacheLoader<V> optLoader;
        private final RedisCache<V> redisCache;
        private final InMemoryCache<V> inMemoryCache;
        private final ConcurrentMap<CacheKey, Object> updateSynchronizer;

        /**
         * Initializes a new {@link UpdateInMemoryCache}.
         *
         * @param redisKey The Redis cache key
         * @param optLoader The loader to yield the appropriate value or <code>null</code>
         * @param redisCache The Redis cache to fetch from
         * @param inMemoryCache The in-memory cache to update with newest value from Redis storage
         * @param updateSynchronizer The map used for synchronizing update operations
         */
        UpdateInMemoryCache(CacheKey redisKey, CacheLoader<V> optLoader, RedisCache<V> redisCache, InMemoryCache<V> inMemoryCache, ConcurrentMap<CacheKey, Object> updateSynchronizer) {
            super();
            this.redisKey = redisKey;
            this.optLoader = optLoader;
            this.redisCache = redisCache;
            this.inMemoryCache = inMemoryCache;
            this.updateSynchronizer = updateSynchronizer; // NOSONARLINT
        }

        @Override
        public Void call() throws Exception {
            try {
                V loaded = optLoader == null ? redisCache.getFromRedis((RedisCacheKey) redisKey) : redisCache.getFromRedis((RedisCacheKey) redisKey, optLoader);
                if (loaded == null) {
                    inMemoryCache.invalidate(redisKey);
                } else {
                    inMemoryCache.put(redisKey, loaded);
                }
            } catch (Exception e) {
                LoggerHolder.LOG.error("Failed to load value from Redis storage for key '{}'", redisKey, e);
                inMemoryCache.invalidate(redisKey);
            } finally {
                // Drop the entry from synchronizing map
                if (optLoader == null) {
                    updateSynchronizer.remove(redisKey, PRESENT_NO_LOADER);
                } else {
                    updateSynchronizer.remove(redisKey);
                }
            }
            return null;
        }

        @Override
        public void setThreadName(ThreadRenamer threadRenamer) {
            // Nothing
        }

        @Override
        public void beforeExecute(Thread t) {
            // Nothing
        }

        @Override
        public void afterExecute(Throwable t) {
            // Nothing
        }
    }

    /**
     * Simple task to invalidate specified collection of Redis cache keys.
     */
    private static class InvalidateKeys<V> implements Runnable {

        private final Collection<? extends CacheKey> redisKeys;
        private final Cache<CacheKey, V> cache;

        /**
         * Initializes a new {@link InvalidateKeys}.
         *
         * @param redisKeys The Redis cache keys to invalidate
         * @param inMemoryCache The in-memory cache
         */
        InvalidateKeys(Collection<? extends CacheKey> redisKeys, Cache<CacheKey, V> cache) {
            super();
            this.redisKeys = redisKeys;
            this.cache = cache;
        }

        @Override
        public void run() {
            cache.invalidateAll(redisKeys);
        }
    }

}
