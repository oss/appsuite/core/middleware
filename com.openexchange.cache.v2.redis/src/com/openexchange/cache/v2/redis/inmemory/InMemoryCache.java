/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.inmemory;

import java.util.Collection;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheLoader;
import com.openexchange.cache.v2.redis.RedisCache;
import com.openexchange.threadpool.ThreadPoolService;

/**
 * {@link InMemoryCache} - The in-memory cache for Redis-backed cache.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 * @param <V> The type of the value
 */
public interface InMemoryCache<V> {

    /**
     * Gets the mode this in-memory cache operates in.
     *
     * @return The operation mode
     */
    Mode getMode();

    /**
     * Associates {@code value} with {@code redisKey} in this cache. If the cache previously contained a
     * value associated with {@code redisKey}, the old value is replaced by {@code value}.
     *
     * @param <V> The type of the value
     * @param redisKey The key
     * @param value The value
     */
    void put(CacheKey redisKey, V value);

    /**
     * Discards any cached value for key {@code redisKey}.
     *
     * @param redisKey The key
     */
    void invalidate(CacheKey redisKey);

    /**
     * Discards any cached values for keys.
     *
     * @param keys The keys to invalidate
     */
    void invalidateAll(Iterable<? extends CacheKey> keys);

    /**
     * Gets the value associated with {@code redisKey} in this cache, or <code>null</code> if there is no cached value for {@code redisKey}.
     *
     * @param <V> The type of the value
     * @param redisKey The key
     * @return The associated value or <code>null</code>
     */
    V getIfPresent(CacheKey redisKey);

    /**
     * Submits invalidating given keys from in-memory cache.
     * <p>
     * If not possible to submit invalidation, the calling thread performs the invalidation.
     *
     * @param redisKeys The keys to invalidate
     */
    void submitInvalidationElseRun(Collection<? extends CacheKey> redisKeys);

    /**
     * Submits updating the value associated with {@code redisKey} in this in-memory cache using given thread pool.
     *
     * @param <V> The type of the value
     * @param redisKey The cache key
     * @param optLoader The loader to yield the appropriate value or <code>null</code>
     * @param redisCache The Redis cache to fetch recent value from
     * @param threadPool The thread pool to submit to
     */
    void submitUpdateInMemoryCache(CacheKey redisKey, CacheLoader<V> optLoader, RedisCache<V> redisCache, ThreadPoolService threadPool);

}
