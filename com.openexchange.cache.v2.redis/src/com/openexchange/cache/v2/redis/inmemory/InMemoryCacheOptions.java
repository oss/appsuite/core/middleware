/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.inmemory;

import com.openexchange.cache.v2.redis.RedisCacheProperty;

/**
 * {@link InMemoryCacheOptions} - The configured options for in-memory cache.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class InMemoryCacheOptions {

    /**
     * Creates a new builder for an instance of <code>InMemoryCacheOptions</code>.
     *
     * @return The newly created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /** The builder for an instance of <code>InMemoryCacheOptions</code> */
    public static final class Builder {

        private boolean enabled;
        private Mode mode;
        private long timeToLoveMillis;

        /**
         * Initializes a new instance of {@link InMemoryCacheOptions.Builder}.
         */
        private Builder() {
            super();
            enabled = RedisCacheProperty.INMEMORY_ENABLED.getDefaultValue(Boolean.class).booleanValue();
            mode = Mode.modeFor(RedisCacheProperty.INMEMORY_MODE.getDefaultValue(String.class), Mode.VOLATILE);
            timeToLoveMillis = RedisCacheProperty.INMEMORY_TTL_MILLIS.getDefaultValue(Long.class).longValue();
        }

        /**
         * Sets the enabled flag.
         *
         * @param enabled The enabled flag to set
         */
        public Builder withEnabled(boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the mode
         *
         * @param mode The mode to set
         */
        public Builder withMode(Mode mode) {
            this.mode = mode;
            return this;
        }

        /**
         * Sets the time-to-live in volatile cache in milliseconds
         *
         * @param timeToLoveMillis The time-to-live in volatile cache in milliseconds to set
         */
        public Builder withTimeToLoveMillis(long timeToLoveMillis) {
            this.timeToLoveMillis = timeToLoveMillis;
            return this;
        }

        /**
         * Builds the instance of <code>InMemoryCacheOptions</code> from this builder's arguments.
         *
         * @return The instance of <code>InMemoryCacheOptions</code>.
         */
        public InMemoryCacheOptions build() {
            return new InMemoryCacheOptions(enabled, mode, timeToLoveMillis);
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** The constant for disabled in-memory cache */
    public static final InMemoryCacheOptions DISABLED = builder().withEnabled(false).build();

    private final boolean enabled;
    private final Mode mode;
    private final long timeToLoveMillis;

    /**
     * Initializes a new instance of {@link InMemoryCacheOptions}.
     *
     * @param enabled The enabled flag
     * @param mode The mode
     * @param timeToLoveMillis The time-to-live in volatile cache in milliseconds
     */
    private InMemoryCacheOptions(boolean enabled, Mode mode, long timeToLoveMillis) {
        super();
        this.enabled = enabled;
        this.mode = mode;
        this.timeToLoveMillis = timeToLoveMillis;
    }

    /**
     * Gets the enabled flag
     *
     * @return <code>true</code> if in-memory cache is enabled; otherwise <code>false</code>
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Gets the disabled flag
     *
     * @return <code>true</code> if in-memory cache is disabled; otherwise <code>false</code>
     */
    public boolean isDisabled() {
        return !enabled;
    }

    /**
     * Gets the mode
     *
     * @return The mode
     */
    public Mode getMode() {
        return mode;
    }

    /**
     * Gets the time-to-live in volatile cache in milliseconds
     *
     * @return The time-to-live in volatile cache in milliseconds
     */
    public long getTimeToLoveMillis() {
        return timeToLoveMillis;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append("enabled=").append(enabled).append(", ");
        if (mode != null) {
            sb.append("mode=").append(mode).append(", ");
        }
        sb.append("timeToLoveMillis=").append(timeToLoveMillis);
        return sb.toString();
    }



}
