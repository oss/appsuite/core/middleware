/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2.redis.inmemory.impl;

import java.time.Duration;
import java.util.Collection;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheLoader;
import com.openexchange.cache.v2.redis.RedisCache;
import com.openexchange.cache.v2.redis.inmemory.InMemoryCache;
import com.openexchange.cache.v2.redis.inmemory.Mode;
import com.openexchange.cache.v2.redis.osgi.Services;
import com.openexchange.threadpool.AbstractTask;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.threadpool.behavior.CallerRunsBehavior;

/**
 * {@link VolatileInMemoryCache} - The volatile in-memory cache for Redis-backed cache.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class VolatileInMemoryCache<V> implements InMemoryCache<V> {

    private final Cache<CacheKey, V> cache;

    /**
     * Initializes a new {@link VolatileInMemoryCache}.
     *
     * @param timeToLoveMillis The time-to-live in volatile cache in milliseconds
     * @throws IllegalArgumentException If time-to-live milliseconds are equal to or less than <code>0</code> (zero)
     */
    public VolatileInMemoryCache(long timeToLoveMillis) {
        super();
        if (timeToLoveMillis <= 0) {
            throw new IllegalArgumentException("The time-to-live milliseconds must not be equal to or less than 0 (zero)");
        }
        this.cache = CacheBuilder.newBuilder().expireAfterWrite(Duration.ofMillis(timeToLoveMillis)).build();
    }

    @Override
    public Mode getMode() {
        return Mode.VOLATILE;
    }

    /**
     * Associates {@code value} with {@code redisKey} in this cache. If the cache previously contained a
     * value associated with {@code redisKey}, the old value is replaced by {@code value}.
     *
     * @param <V> The type of the value
     * @param redisKey The key
     * @param value The value
     */
    @Override
    public void put(CacheKey redisKey, V value) {
        cache.put(redisKey, value);
    }

    /**
     * Discards any cached value for key {@code redisKey}.
     *
     * @param redisKey The key
     */
    @Override
    public void invalidate(CacheKey redisKey) {
        cache.invalidate(redisKey);
    }

    @Override
    public void invalidateAll(Iterable<? extends CacheKey> keys) {
        cache.invalidateAll(keys);
    }

    /**
     * Gets the value associated with {@code redisKey} in this cache, or <code>null</code> if there is no cached value for {@code redisKey}.
     *
     * @param <V> The type of the value
     * @param redisKey The key
     * @return The associated value or <code>null</code>
     */
    @Override
    public V getIfPresent(CacheKey redisKey) {
        return cache.getIfPresent(redisKey);
    }

    /**
     * Submits invalidating given keys from in-memory cache.
     * <p>
     * If not possible to submit invalidation, the calling thread performs the invalidation.
     *
     * @param redisKeys The keys to invalidate
     */
    @Override
    public void submitInvalidationElseRun(Collection<? extends CacheKey> redisKeys) {
        if (redisKeys == null || redisKeys.isEmpty()) {
            return;
        }

        ThreadPoolService threadPool = Services.optService(ThreadPoolService.class);
        if (threadPool != null) {
            // Submit to thread pool
            threadPool.submit(new InvalidateKeys<>(redisKeys, cache), CallerRunsBehavior.getInstance());
        } else {
            // Perform with running thread
            cache.invalidateAll(redisKeys);
        }
    }

    /**
     * Submits updating the value associated with {@code redisKey} in this in-memory cache using given thread pool.
     *
     * @param redisKey The cache key
     * @param optLoader The loader to yield the appropriate value or <code>null</code>
     * @param redisCache The Redis cache to fetch recent value from
     * @param threadPool The thread pool to submit to
     */
    @Override
    public void submitUpdateInMemoryCache(CacheKey redisKey, CacheLoader<V> optLoader, RedisCache<V> redisCache, ThreadPoolService threadPool) {
        // No background update here. Let key expire in in-memory cache instead...
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Simple task to invalidate specified collection of Redis cache keys.
     */
    private static class InvalidateKeys<V> extends AbstractTask<Void> {

        private final Collection<? extends CacheKey> redisKeys;
        private final Cache<CacheKey, V> cache;

        /**
         * Initializes a new {@link InvalidateKeys}.
         *
         * @param redisKeys The Redis cache keys to invalidate
         * @param inMemoryCache The in-memory cache
         */
        InvalidateKeys(Collection<? extends CacheKey> redisKeys, Cache<CacheKey, V> cache) {
            super();
            this.redisKeys = redisKeys;
            this.cache = cache;
        }

        @Override
        public Void call() {
            cache.invalidateAll(redisKeys);
            return null;
        }
    }

}
