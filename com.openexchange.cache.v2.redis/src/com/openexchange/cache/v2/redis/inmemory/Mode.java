/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2.redis.inmemory;

import com.openexchange.java.Strings;

/**
 * {@link Mode} - The enumeration of known modes for in-memory cache.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public enum Mode {

    /**
     * The common volatile in-memory cache keeping values in cache for a very short amount of time (10 seconds),
     */
    VOLATILE("volatile"),
    /**
     * The in-memory cache that re-fetches any read value from Redis storage and updates it in the background so that a second fetch is
     * ensured to provide the newest value.
     */
    UPDATE_AFTER_FETCH("updateAfterFetch"),
    ;

    private final String identifier;

    private Mode(String identifier) {
        this.identifier = identifier;
    }

    /**
     * Gets the identifier
     *
     * @return The identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Gets the known mode for given identifier.
     *
     * @param identifier The identifier to look-up
     * @param defaultValue The default value to return in case there is no such mode for given identifier
     * @return The mode or <code>defaultValue</code> if there is no such mode for given identifier
     */
    public static Mode modeFor(String identifier, Mode defaultValue) {
        if (identifier == null) {
            return defaultValue;
        }

        String id = Strings.asciiLowerCase(identifier);
        for (Mode mode : Mode.values()) {
            if (id.equals(mode.identifier)) {
                return mode;
            }
        }
        return defaultValue;
    }

}
