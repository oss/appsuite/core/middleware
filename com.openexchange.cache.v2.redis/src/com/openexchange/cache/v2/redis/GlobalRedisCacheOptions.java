/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis;

/**
 * {@link GlobalRedisCacheOptions} - The configured global options for Redis cache.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class GlobalRedisCacheOptions { // NOSONARLINT

    /**
     * Creates a new builder for an instance of <code>GlobalRedisCacheOptions</code>.
     *
     * @return The newly created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /** The builder for an instance of <code>GlobalRedisCacheOptions</code> */
    public static final class Builder {

        private boolean disableHashExpiration;
        private boolean orchestrateConcurrentRetrieval;
        private int multiKeyLimit;

        /**
         * Initializes a new instance of {@link GlobalRedisCacheOptions.Builder}.
         */
        private Builder() {
            super();
            disableHashExpiration = RedisCacheProperty.DISABLE_HASH_EXPIRATION.getDefaultValue(Boolean.class).booleanValue();
            orchestrateConcurrentRetrieval = RedisCacheProperty.ORCHESTRATE_CONCURRENT_RETRIEVAL.getDefaultValue(Boolean.class).booleanValue();
            multiKeyLimit = RedisCacheProperty.MULTI_KEY_LIMIT.getDefaultValue(Integer.class).intValue();
        }

        /**
         * Sets whether expiration of hash fields is supported or not.
         *
         * @param disableHashExpiration <code>true</code> if expiration of hash fields is supported; otherwise <code>false</code>
         */
        public Builder withDisableHashExpiration(boolean disableHashExpiration) {
            this.disableHashExpiration = disableHashExpiration;
            return this;
        }

        /**
         * Sets whether concurrent read accesses from Redis shall be synchronized/orchestrated.
         *
         * @param orchestrateConcurrentRetrieval <code>true</code> to orchestrate; otherwise <code>false</code>
         */
        public Builder withOrchestrateConcurrentRetrieval(boolean orchestrateConcurrentRetrieval) {
            this.orchestrateConcurrentRetrieval = orchestrateConcurrentRetrieval;
            return this;
        }

        /**
         * Sets the maximum number of keys to address per <code>MGET</code>, <code>MSET</code>, <code>MHGET</code> or <code>MHSET</code> command.
         *
         * @param multiKeyLimit The maximum number of keys to address per <code>MGET</code>, <code>MSET</code>, <code>MHGET</code> or <code>MHSET</code> command
         */
        public Builder withMultiKeyLimit(int multiKeyLimit) {
            this.multiKeyLimit = multiKeyLimit;
            return this;
        }

        /**
         * Builds the instance of <code>GlobalRedisCacheOptions</code> from this builder's arguments.
         *
         * @return The instance of <code>GlobalRedisCacheOptions</code>.
         */
        public GlobalRedisCacheOptions build() {
            return new GlobalRedisCacheOptions(disableHashExpiration, orchestrateConcurrentRetrieval, multiKeyLimit);
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final boolean disableHashExpiration;
    private final boolean orchestrateConcurrentRetrieval;
    private final int multiKeyLimit;

    /**
     * Initializes a new instance of {@link GlobalRedisCacheOptions}.
     *
     * @param disableHashExpiration <code>true</code> to disable expiration for hash (i.e. group member) keys, otherwise <code>false</code>
     * @param orchestrateConcurrentRetrieval <code>true</code> to orchestrate concurrent retrievals of the same key; otherwise <code>false</code>
     * @param multiKeyLimit The maximum number of keys to address per <code>MGET</code>, <code>MSET</code>, <code>MHGET</code> or <code>MHSET</code> command
     */
    public GlobalRedisCacheOptions(boolean disableHashExpiration, boolean orchestrateConcurrentRetrieval, int multiKeyLimit) {
        super();
        this.disableHashExpiration = disableHashExpiration;
        this.orchestrateConcurrentRetrieval = orchestrateConcurrentRetrieval;
        this.multiKeyLimit = multiKeyLimit;
    }

    /**
     * Checks whether to disable expiration for hash (i.e. group member) key.
     *
     * @return <code>true</code> to disable expiration for hash (i.e. group member) keys, otherwise <code>false</code>
     */
    public boolean isDisableHashExpiration() {
        return disableHashExpiration;
    }

    /**
     * Checks whether to orchestrate concurrent retrievals of the same key.
     *
     * @return code>true</code> to orchestrate concurrent retrievals of the same key; otherwise <code>false</code>
     */
    public boolean isOrchestrateConcurrentRetrieval() {
        return orchestrateConcurrentRetrieval;
    }

    /**
     * Gets the maximum number of keys to address per <code>MGET</code>, <code>MSET</code>, <code>MHGET</code> or <code>MHSET</code> command.
     *
     * @return The maximum number of keys to address per <code>MGET</code>, <code>MSET</code>, <code>MHGET</code> or <code>MHSET</code> command
     */
    public int getMultiKeyLimit() {
        return multiKeyLimit;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        sb.append("disableHashExpiration=").append(disableHashExpiration);
        sb.append(", orchestrateConcurrentRetrieval=").append(orchestrateConcurrentRetrieval);
        sb.append(", multiKeyLimit=").append(multiKeyLimit);
        return sb.toString();
    }

}
