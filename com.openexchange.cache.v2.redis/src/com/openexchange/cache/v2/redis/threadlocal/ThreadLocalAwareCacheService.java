/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.threadlocal;

import static com.openexchange.cache.v2.redis.Utils.keyCheck;
import static com.openexchange.cache.v2.redis.metrics.CacheMetricsCollector.collectThreadLocalMetrics;
import java.time.Duration;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import com.google.common.cache.CacheBuilder;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.codec.CacheValueCodec;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.cache.v2.filter.ModuleName;
import com.openexchange.cache.v2.filter.VersionName;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;
import com.openexchange.cache.v2.redis.RedisCacheKeyValue;
import com.openexchange.cache.v2.redis.RedisCacheService;
import com.openexchange.cache.v2.redis.key.RedisCacheKey;
import com.openexchange.cache.v2.redis.metrics.CacheMetricName;
import com.openexchange.cache.v2.redis.metrics.CacheMetricsCollector;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.IReference;
import com.openexchange.java.ImmutableReference;
import com.openexchange.marker.OXThreadMarkers;

/**
 * {@link ThreadLocalAwareCacheService} - The thread-local cache service.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ThreadLocalAwareCacheService implements InvalidationCacheService {

    private final com.google.common.cache.Cache<CacheOptions<?>, ThreadLocalAwareCache<?>> cacheViews;
    private final RedisCacheService cacheService;
    private final CacheMetricsCollector metricsCollector;
    private final boolean trackPuts;

    /**
     * Initializes a new {@link ThreadLocalAwareCacheService}.
     *
     * @param cacheService The cache service to delegate to
     * @param trackPuts Whether to also track PUTs within thread-local collection
     */
    public ThreadLocalAwareCacheService(RedisCacheService cacheService, boolean trackPuts) {
        super();
        this.cacheService = cacheService;
        this.trackPuts = trackPuts;
        this.cacheViews = CacheBuilder.newBuilder().expireAfterAccess(Duration.ofMinutes(10)).build();
        this.metricsCollector = CacheMetricsCollector.getInstance();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <V> Cache<V> getCache(CacheOptions<V> options) {
        if (options == null) {
            throw new IllegalArgumentException("Options must not be null");
        }

        // Try acquire from cache
        ThreadLocalAwareCache<V> cacheView = (ThreadLocalAwareCache<V>) cacheViews.getIfPresent(options);
        if (cacheView != null) {
            return cacheView;
        }

        // Acquire using "if cached, return; otherwise create, cache and return"
        try {
            return (ThreadLocalAwareCache<V>) cacheViews.get(options, () -> newThreadLocalCacheView(options));
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof RuntimeException) {
                throw (RuntimeException) cause;
            }
            throw new IllegalStateException("Failed retrieving cache", cause == null ? e : cause);
        }
    }

    @Override
    public Map<CacheKey, CacheKeyValue<Object>> mget(Map<CacheKey, CacheValueCodec<?>> keysAndCodecs) throws OXException {
        boolean collectThreadLocalMetrics = collectThreadLocalMetrics();

        Map<CacheKey, CacheKeyValue<Object>> mergedValues = null;
        Map<CacheKey, CacheValueCodec<?>> toLoad = null;
        for (Map.Entry<CacheKey, CacheValueCodec<?>> e : keysAndCodecs.entrySet()) {
            CacheKey key = e.getKey();
            IReference<?> ref = fetchThreadLocalValue(key, collectThreadLocalMetrics);
            if (ref != null) {
                if (mergedValues == null) {
                    mergedValues = HashMap.newHashMap(keysAndCodecs.size());
                }
                mergedValues.put(key, RedisCacheKeyValue.fromNullable(key, ref.getValue()));
            } else {
                if (toLoad == null) {
                    toLoad = HashMap.newHashMap(keysAndCodecs.size());
                }
                toLoad.put(key, e.getValue());
            }
        }

        if (toLoad != null) {
            Map<CacheKey, CacheKeyValue<Object>> cachedValues = cacheService.mget(toLoad);

            if (mergedValues == null) {
                // Not a single value was loaded from thread-local cache: nothing to merge then
                for (Map.Entry<CacheKey, CacheKeyValue<Object>> entry : cachedValues.entrySet()) {
                    Object value = entry.getValue().getValueOrElse(null);
                    CacheKey key = entry.getKey();
                    if (collectThreadLocalMetrics) {
                        metricsCollector.incrementThreadLocalMisses(keyCheck(entry.getKey()));
                        long now = System.nanoTime();
                        OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
                        metricsCollector.tickMetrics(CacheMetricName.THREAD_LOCAL_PUT, now);
                    } else {
                        OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
                    }
                }
                return cachedValues;
            }

            // Put loaded values into merged map
            for (Map.Entry<CacheKey, CacheKeyValue<Object>> entry : cachedValues.entrySet()) {
                Object value = entry.getValue().getValueOrElse(null);
                CacheKey key = entry.getKey();
                if (collectThreadLocalMetrics) {
                    metricsCollector.incrementThreadLocalMisses(keyCheck(entry.getKey()));
                    long now = System.nanoTime();
                    OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
                    metricsCollector.tickMetrics(CacheMetricName.THREAD_LOCAL_PUT, now);
                } else {
                    OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
                }
                mergedValues.put(key, RedisCacheKeyValue.fromNullable(key, value));
            }
        }

        return mergedValues == null ? Map.of() : mergedValues;
    }

    /**
     * Fetches the thread-local value associated with given key.
     *
     * @param key The key to load
     * @param collectThreadLocalMetrics Whether to track thread-local metrics
     * @return The reference for thread-local value or <code>null</code>
     */
    private IReference<?> fetchThreadLocalValue(CacheKey key, boolean collectThreadLocalMetrics) {
        if (!collectThreadLocalMetrics) {
            // Track no metrics
            return OXThreadMarkers.getThreadLocalValue(key.getFQN());
        }

        long now = System.nanoTime();
        IReference<?> ref = OXThreadMarkers.getThreadLocalValue(key.getFQN());
        metricsCollector.tickMetrics(CacheMetricName.THREAD_LOCAL_GET, now);
        return ref;
    }

    private <V> ThreadLocalAwareCache<V> newThreadLocalCacheView(CacheOptions<V> options) {
        try {
            return new ThreadLocalAwareCache<>(cacheService.getCache(options), metricsCollector, trackPuts);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new IllegalStateException("Failed retrieving cache", e);
        }
    }

    @Override
    public void invalidate(Collection<CacheFilter> filters, boolean fireCacheEvents) throws OXException {
        List<RedisCacheKey> keys = cacheService.invalidateAndReturnKeys(filters, fireCacheEvents);
        if (keys != null && !keys.isEmpty()) {
            OXThreadMarkers.removeThreadLocalValues(keys.stream().map(CacheKey::getFQN).collect(CollectorUtils.toList(keys.size())));
        }
    }

    @Override
    public void invalidate(CacheFilter filter, boolean fireCacheEvents) throws OXException {
        List<RedisCacheKey> keys = cacheService.invalidateAndReturnKeys(filter, fireCacheEvents);
        if (keys != null && !keys.isEmpty()) {
            OXThreadMarkers.removeThreadLocalValues(keys.stream().map(CacheKey::getFQN).collect(CollectorUtils.toList(keys.size())));
        }
    }

    @Override
    public void invalidateGroup(ModuleName module, VersionName version, String group, boolean fireCacheEvents) throws OXException {
        List<? extends CacheKey> keys = cacheService.invalidateGroupAndReturnKeys(module, version, group, fireCacheEvents);
        if (keys != null && !keys.isEmpty()) {
            OXThreadMarkers.removeThreadLocalValues(keys.stream().map(CacheKey::getFQN).collect(CollectorUtils.toList(keys.size())));
        }
    }

    @Override
    public void invalidateGroupMember(ModuleName module, VersionName version, String group, boolean fireCacheEvents, Object... suffix) throws OXException {
        CacheKey key = cacheService.invalidateGroupMemberAndReturnKey(module, version, group, fireCacheEvents, suffix);
        if (key != null) {
            OXThreadMarkers.removeThreadLocalValue(key.getFQN());
        }
    }

}
