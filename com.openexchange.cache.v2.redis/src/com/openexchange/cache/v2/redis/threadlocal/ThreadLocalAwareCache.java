/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.threadlocal;

import static com.openexchange.cache.v2.redis.Utils.keyCheck;
import static com.openexchange.cache.v2.redis.metrics.CacheMetricsCollector.collectThreadLocalMetrics;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.CacheLoader;
import com.openexchange.cache.v2.SuffixBuilder;
import com.openexchange.cache.v2.redis.RedisCacheKeyValue;
import com.openexchange.cache.v2.redis.metrics.CacheMetricName;
import com.openexchange.cache.v2.redis.metrics.CacheMetricsCollector;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.IReference;
import com.openexchange.java.ImmutableReference;
import com.openexchange.marker.OXThreadMarkers;

/**
 * {@link ThreadLocalAwareCache} - The cache that stores values in a thread-local collection for faster look-up.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ThreadLocalAwareCache<V> implements Cache<V> {

    private final CacheMetricsCollector metricsCollector;
    private final Cache<V> cache;
    private final boolean trackPuts;

    /**
     * Initializes a new {@link ThreadLocalAwareCache}.
     *
     * @param cache The cache to delegate to
     * @param metricsCollector The cache metrics collector to use
     * @param trackPuts Whether to also track PUTs within thread-local collection
     */
    public ThreadLocalAwareCache(Cache<V> cache, CacheMetricsCollector metricsCollector, boolean trackPuts) {
        super();
        this.cache = cache;
        this.trackPuts = trackPuts;
        this.metricsCollector = metricsCollector;
    }

    @Override
    public CacheKey newKey(Object... parts) {
        return cache.newKey(parts);
    }

    @Override
    public CacheKey newGroupMemberKey(String group, Object... suffix) {
        return cache.newGroupMemberKey(group, suffix);
    }

    @Override
    public CacheKey newGroupKey(String group) {
        return cache.newGroupKey(group);
    }

    @Override
    public CacheKey newKey(SuffixBuilder suffix) {
        return cache.newKey(suffix);
    }

    @Override
    public CacheKey newGroupMemberKey(String group, SuffixBuilder suffix) {
        return cache.newGroupMemberKey(group, suffix);
    }

    @Override
    public SuffixBuilder suffixBuilder(int capacity) {
        return cache.suffixBuilder(capacity);
    }

    @Override
    public boolean exists(CacheKey key) throws OXException {
        IReference<V> ref = OXThreadMarkers.getThreadLocalValue(key.getFQN());
        return ref != null ? (ref.getValue() != null) : cache.exists(key);
    }

    @Override
    public List<CacheKeyValue<V>> mget(Collection<? extends CacheKey> keys) throws OXException {
        return keys == null || keys.isEmpty() ? Collections.emptyList() : mgetInternal(keys);
    }

    @Override
    public List<CacheKeyValue<V>> mget(CacheKey... keys) throws OXException {
        return keys == null || keys.length <= 0 ? Collections.emptyList() : mgetInternal(Arrays.asList(keys));
    }

    /**
     * Gets multiple keys from either thread-local or Redis cache.
     *
     * @param keys The keys
     * @return The key-value-pairs
     * @throws OXException If operation fails
     */
    private List<CacheKeyValue<V>> mgetInternal(Collection<? extends CacheKey> keys) throws OXException {
        boolean collectThreadLocalMetrics = collectThreadLocalMetrics();

        int numKeys = keys.size();
        Map<CacheKey, CacheKeyValue<V>> key2value = HashMap.newHashMap(numKeys);
        List<CacheKey> toLoad = null;
        for (CacheKey key : keys) {
            IReference<V> ref;
            if (collectThreadLocalMetrics) {
                long now = System.nanoTime();
                ref = OXThreadMarkers.getThreadLocalValue(key.getFQN());
                metricsCollector.tickMetrics(CacheMetricName.THREAD_LOCAL_GET, now);
            } else {
                ref = OXThreadMarkers.getThreadLocalValue(key.getFQN());
            }

            if (ref != null) {
                // This thread already obtained that value from cache
                if (collectThreadLocalMetrics) {
                    metricsCollector.incrementThreadLocalGets(keyCheck(key));
                }
                key2value.put(key, RedisCacheKeyValue.fromNullable(key, ref.getValue()));
            } else {
                if (toLoad == null) {
                    toLoad = new ArrayList<>(numKeys);
                }
                toLoad.add(key);
            }
        }

        if (toLoad != null) {
            if (collectThreadLocalMetrics) {
                for (CacheKey key : toLoad) {
                    metricsCollector.incrementThreadLocalMisses(keyCheck(key));
                }
            }

            List<CacheKeyValue<V>> keyValues = cache.mget(toLoad);
            toLoad = null; // NOSONARLINT Might help GC
            for (CacheKeyValue<V> keyValue : keyValues) {
                CacheKey key = keyValue.getKey();
                key2value.put(key, keyValue);

                V value = keyValue.getValueOrElse(null);
                if (collectThreadLocalMetrics) {
                    long now = System.nanoTime();
                    if (value != null) {
                        OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
                    } else {
                        // Remember MISS
                        OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(null));
                    }
                    metricsCollector.tickMetrics(CacheMetricName.THREAD_LOCAL_PUT, now);
                } else {
                    if (value != null) {
                        OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
                    } else {
                        // Remember MISS
                        OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(null));
                    }
                }
            }
        }

        List<CacheKeyValue<V>> retval = new ArrayList<>(numKeys);
        for (CacheKey key : keys) { // NOSONARLINT
            CacheKeyValue<V> cacheKeyValue = key2value.get(key);
            retval.add(cacheKeyValue == null ? RedisCacheKeyValue.empty(key) : cacheKeyValue);
        }
        key2value = null; // NOSONARLINT Might help GC
        return retval;
    }

    @Override
    public V get(CacheKey key) throws OXException {
        boolean collectThreadLocalMetrics = collectThreadLocalMetrics();

        IReference<V> ref;
        if (collectThreadLocalMetrics) {
            long now = System.nanoTime();
            ref = OXThreadMarkers.getThreadLocalValue(key.getFQN());
            metricsCollector.tickMetrics(CacheMetricName.THREAD_LOCAL_GET, now);
        } else {
            ref = OXThreadMarkers.getThreadLocalValue(key.getFQN());
        }

        if (ref != null) {
            // This thread already obtained that value from cache
            if (collectThreadLocalMetrics) {
                metricsCollector.incrementThreadLocalGets(keyCheck(key));
            }
            return ref.getValue(); // Might be null
        }

        if (collectThreadLocalMetrics) {
            metricsCollector.incrementThreadLocalMisses(keyCheck(key));
        }
        V value = cache.get(key);

        if (collectThreadLocalMetrics) {
            long now = System.nanoTime();
            if (value != null) {
                OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
            } else {
                // Remember MISS
                OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(null));
            }
            metricsCollector.tickMetrics(CacheMetricName.THREAD_LOCAL_PUT, now);
        } else {
            if (value != null) {
                OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
            } else {
                // Remember MISS
                OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(null));
            }
        }

        return value;
    }

    @Override
    public V get(CacheKey key, CacheLoader<V> loader) throws OXException {
        boolean collectThreadLocalMetrics = collectThreadLocalMetrics();

        IReference<V> ref;
        if (collectThreadLocalMetrics) {
            long now = System.nanoTime();
            ref = OXThreadMarkers.getThreadLocalValue(key.getFQN());
            metricsCollector.tickMetrics(CacheMetricName.THREAD_LOCAL_GET, now);
        } else {
            ref = OXThreadMarkers.getThreadLocalValue(key.getFQN());
        }

        if (ref != null) {
            // This thread already obtained that value from cache
            if (collectThreadLocalMetrics) {
                metricsCollector.incrementThreadLocalGets(keyCheck(key));
            }
            V value = ref.getValue();
            if (value != null) {
                // This thread already obtained that value from cache
                return value;
            }
        }

        if (collectThreadLocalMetrics) {
            metricsCollector.incrementThreadLocalMisses(keyCheck(key));
        }
        V value = cache.get(key, loader);

        if (collectThreadLocalMetrics) {
            long now = System.nanoTime();
            OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
            metricsCollector.tickMetrics(CacheMetricName.THREAD_LOCAL_PUT, now);
        } else {
            OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
        }

        return value;
    }

    @Override
    public void mput(Map<CacheKey, V> map) throws OXException {
        cache.mput(map);

        if (trackPuts && !map.isEmpty()) {
            if (collectThreadLocalMetrics()) {
                long now = System.nanoTime();
                for (Map.Entry<CacheKey, V> e : map.entrySet()) {
                    OXThreadMarkers.putThreadLocalValue(e.getKey().getFQN(), ImmutableReference.immutableReferenceFor(e.getValue()));
                }
                metricsCollector.tickMetrics(CacheMetricName.THREAD_LOCAL_PUT, now);
            } else {
                for (Map.Entry<CacheKey, V> e : map.entrySet()) {
                    OXThreadMarkers.putThreadLocalValue(e.getKey().getFQN(), ImmutableReference.immutableReferenceFor(e.getValue()));
                }
            }
        }
    }

    @Override
    public boolean put(CacheKey key, V value) throws OXException {
        boolean replaced = cache.put(key, value);

        if (trackPuts) {
            if (collectThreadLocalMetrics()) {
                long now = System.nanoTime();
                OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
                metricsCollector.tickMetrics(CacheMetricName.THREAD_LOCAL_PUT, now);
            } else {
                OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
            }
        }

        return replaced;
    }

    @Override
    public boolean putIfAbsent(CacheKey key, V value) throws OXException {
        boolean put = cache.putIfAbsent(key, value);

        if (put && trackPuts) {
            if (collectThreadLocalMetrics()) {
                long now = System.nanoTime();
                OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
                metricsCollector.tickMetrics(CacheMetricName.THREAD_LOCAL_PUT, now);
            } else {
                OXThreadMarkers.putThreadLocalValue(key.getFQN(), ImmutableReference.immutableReferenceFor(value));
            }
        }

        return put;
    }

    @Override
    public boolean invalidate(CacheKey key) throws OXException {
        boolean deleted = cache.invalidate(key);
        // Remove from thread-local cache regardless of result
        OXThreadMarkers.removeThreadLocalValue(key.getFQN());
        return deleted;
    }

    @Override
    public void invalidate(List<CacheKey> keys) throws OXException {
        cache.invalidate(keys);
        if (keys != null) {
            OXThreadMarkers.removeThreadLocalValues(keys.stream().map(CacheKey::getFQN).collect(CollectorUtils.toList(keys.size())));
        }
    }

    @Override
    public List<? extends CacheKey> invalidateGroup(CacheKey groupKey) throws OXException {
        List<? extends CacheKey> invalidatedKeys = cache.invalidateGroup(groupKey);
        if (invalidatedKeys != null) {
            OXThreadMarkers.removeThreadLocalValues(invalidatedKeys.stream().map(CacheKey::getFQN).collect(CollectorUtils.toList(invalidatedKeys.size())));
        }
        return invalidatedKeys;
    }

}
