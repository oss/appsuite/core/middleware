/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis;

import static com.openexchange.cache.v2.redis.Utils.addOrCreate;
import static com.openexchange.cache.v2.redis.Utils.createOrReset;
import static com.openexchange.cache.v2.redis.Utils.returnOrCreateHashMap;
import static com.openexchange.cache.v2.redis.Utils.returnOrCreateLinkedHashMap;
import static com.openexchange.cache.v2.redis.key.RedisCacheKeyUtil.CORE_START;
import static com.openexchange.java.Autoboxing.l;
import static com.openexchange.redis.RedisUtils.autoFlushCommands;
import static com.openexchange.redis.RedisUtils.deleteKeysAsyncElseSync;
import static com.openexchange.redis.RedisUtils.disableAutoFlushCommands;
import static com.openexchange.redis.RedisUtils.getFrom;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;
import com.openexchange.cache.v2.CacheExceptionCode;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.events.CacheEvent;
import com.openexchange.cache.v2.redis.events.CacheEventChannelImpl;
import com.openexchange.cache.v2.redis.key.RedisCacheKey;
import com.openexchange.cache.v2.redis.key.RedisCacheKeyUtil;
import com.openexchange.cache.v2.redis.metrics.CacheMetricName;
import com.openexchange.cache.v2.redis.metrics.CacheMetricsCollector;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Functions;
import com.openexchange.java.Predicates;
import com.openexchange.java.Streams;
import com.openexchange.java.Strings;
import com.openexchange.redis.RedisCommandsProvider;
import com.openexchange.redis.RedisConnector;
import com.openexchange.redis.RedisOperation;
import com.openexchange.redis.RedisUtils;
import com.openexchange.server.ServiceLookup;
import io.lettuce.core.KeyScanCursor;
import io.lettuce.core.KeyValue;
import io.lettuce.core.RedisException;
import io.lettuce.core.RedisFuture;
import io.lettuce.core.ScanArgs;
import io.lettuce.core.SetArgs;
import io.lettuce.core.api.async.RedisHashAsyncCommands;
import io.lettuce.core.api.async.RedisKeyAsyncCommands;
import io.lettuce.core.api.sync.RedisHashCommands;
import io.lettuce.core.api.sync.RedisKeyCommands;

/**
 * {@link NativeRedisCache} - The native Redis-backed cache.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class NativeRedisCache { // NOSONARLINT

    /** The hash type */
    private static final String TYPE_HASH = "hash";

    private static final String OK = "OK";

    private static final short SCAN_RESULTS_LIMIT = 1000;

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final CacheMetricsCollector metricsCollector;
    private final ServiceLookup services;
    private final RedisConnector connector;
    private final CacheEventChannelImpl cacheEventChannel;
    private final ReentrantLock jvmGlobalCacheLock;

    /**
     * Initializes a new {@link NativeRedisCache}.
     *
     * @param cacheEventChannel The cache event channel
     * @param connector The Redis connector
     * @param withMetrics Whether to track metrics or not
     * @param services The service look-up
     */
    public NativeRedisCache(CacheEventChannelImpl cacheEventChannel, RedisConnector connector, boolean withMetrics, ServiceLookup services) {
        super();
        this.cacheEventChannel = cacheEventChannel;
        this.connector = connector;
        this.services = services;
        this.metricsCollector = withMetrics ? CacheMetricsCollector.getInstance() : null;
        this.jvmGlobalCacheLock = new ReentrantLock();
    }

    /**
     * Executes specified tagged cache operation.
     *
     * @param <R> The result type
     * @param metricName The metric name
     * @param operation The operation to execute
     * @return The result
     * @throws OXException If result cannot be returned
     */
    protected <R> R executeOperation(CacheMetricName metricName, RedisOperation<R> operation) throws OXException {
        if (metricsCollector == null) {
            try {
                return connector.executeOperation(operation);
            } catch (OXException e) {
                throw e;
            } catch (RuntimeException e) {
                throw CacheExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
            }
        }

        // With metrics
        boolean error = true; // pessimistic
        long now = System.nanoTime();
        try {
            R result = connector.executeOperation(operation);
            error = false;
            return result;
        } catch (OXException e) {
            throw e;
        } catch (RuntimeException e) {
            throw CacheExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (error) {
                metricsCollector.incrementErrors();
            } else {
                metricsCollector.tickMetrics(metricName, now);
            }
        }
    }

    /**
     * Gets the service look-up.
     *
     * @return The service look-up
     */
    public ServiceLookup getServices() {
        return services;
    }

    /**
     * Gets the global cache lock for this JVM.
     *
     * @return The global cache lock
     */
    public ReentrantLock getGlobalCacheLock() {
        return jvmGlobalCacheLock;
    }

    ////////////////////////////// CACHE METHODS /////////////////////////////

    public void mput(Map<RedisCacheKey, InputStream> map, long ttlSeconds, boolean fireCacheEvent) throws OXException {
        if (map == null) {
            throw new IllegalArgumentException("The key-to-value map must not be null");
        }
        executeOperation(CacheMetricName.PUT, commandsProvider -> doMPut(map.entrySet(), ttlSeconds, commandsProvider));
        if (fireCacheEvent) {
            List<CacheEvent> events = map.keySet().stream().map(CacheEvent::create).collect(CollectorUtils.toList(map.size()));
            cacheEventChannel.publish(events);
        }
    }

    public void mput(Collection<Map.Entry<RedisCacheKey, InputStream>> entries, long ttlSeconds, boolean fireCacheEvent) throws OXException {
        if (entries == null) {
            throw new IllegalArgumentException("The key-to-value collection must not be null");
        }
        executeOperation(CacheMetricName.PUT, commandsProvider -> doMPut(entries, ttlSeconds, commandsProvider));
        if (fireCacheEvent) {
            List<CacheEvent> events = entries.stream().map(e -> CacheEvent.create(e.getKey())).collect(CollectorUtils.toList(entries.size()));
            cacheEventChannel.publish(events);
        }
    }

    private Void doMPut(Collection<Map.Entry<RedisCacheKey, InputStream>> entries, long ttlSeconds, RedisCommandsProvider commandsProvider) throws OXException {
        int numElements = entries.size();
        if (numElements == 1) {
            Map.Entry<RedisCacheKey, InputStream> entry = entries.iterator().next();
            doPut(entry.getKey(), entry.getValue(), ttlSeconds, commandsProvider);
            return null;
        }

        Map<String, InputStream> key2Value = null;
        Map<String, Map<String, InputStream>> field2Value = null;

        boolean withMetrics = metricsCollector != null;
        List<RedisCacheKey> keys = null;
        List<RedisCacheKey> members = null;

        for (Map.Entry<RedisCacheKey, InputStream> e : entries) {
            RedisCacheKey key = e.getKey();
            InputStream data = e.getValue();
            nullCheck(key, data);

            String groupFqn = key.getGroupFQNIfValidSuffixElseNull();
            if (groupFqn != null) {
                field2Value = returnOrCreateHashMap(field2Value, 2);
                field2Value.computeIfAbsent(groupFqn, Functions.getNewHashMapFuntion()).put(key.getSuffix(), data);
                if (withMetrics) {
                    members = addOrCreate(key, members, numElements);
                }
            } else {
                key2Value = returnOrCreateHashMap(key2Value, numElements);
                key2Value.put(key.getFQN(), data);
                if (withMetrics) {
                    keys = addOrCreate(key, keys, numElements);
                }
            }
        }

        if (key2Value != null) {
            msetKeys(key2Value, keys, ttlSeconds, commandsProvider);
        }

        if (field2Value != null) {
            msetMembers(field2Value, members, ttlSeconds, commandsProvider);
        }

        // Return void
        return null;
    }

    /**
     * Performs MSET for given regular keys.
     *
     * @param key2Value The key-value-pairs to put
     * @param keys The associated cache keys
     * @param ttlSeconds The time-to-live seconds to apply
     * @param commandsProvider The command provider to use
     * @throws OXException If operation fails
     */
    private void msetKeys(Map<String, InputStream> key2Value, List<RedisCacheKey> keys, long ttlSeconds, RedisCommandsProvider commandsProvider) throws OXException {
        // MSet
        commandsProvider.getRawStringCommands().mset(key2Value);
        if (metricsCollector != null && keys != null) {
            for (RedisCacheKey redisKey : keys) {
                metricsCollector.incrementPuts(redisKey);
            }
        }

        // TTL
        if (ttlSeconds > 0) {
            if (disableAutoFlushCommands(commandsProvider.getConnection())) {
                // Successfully disabled auto-flush behavior
                try {
                    RedisKeyAsyncCommands<String, InputStream> keyAsyncCommands = commandsProvider.getKeyAsyncCommands();
                    List<RedisFuture<Boolean>> futures = new ArrayList<>(key2Value.size());
                    for (String key : key2Value.keySet()) {
                        futures.add(keyAsyncCommands.expire(key, ttlSeconds));
                    }

                    // Write all commands to the transport layer
                    commandsProvider.getConnection().flushCommands();

                    // Wait until all futures complete
                    for (RedisFuture<Boolean> f : futures) {
                        getFrom(f);
                    }
                    futures = null; // NOSONARLINT No more needed
                }  finally {
                    // Re-enable auto-flush behavior
                    autoFlushCommands(commandsProvider.getConnection());
                }
            } else {
                // Auto-flush behavior could not be disabled. Do it using regular synchronous API.
                RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
                for (String key : key2Value.keySet()) {
                    keyCommands.expire(key, ttlSeconds);
                }
            }
        }
    }

    /**
     * Performs MSET for given members.
     *
     * @param field2Value The mapping of hash key to field-value-pairs to put
     * @param members The associated cache keys
     * @param ttlSeconds The time-to-live seconds to apply
     * @param commandsProvider The command provider to use
     * @throws OXException If operation fails
     */
    private void msetMembers(Map<String, Map<String, InputStream>> field2Value, List<RedisCacheKey> members, long ttlSeconds, RedisCommandsProvider commandsProvider) throws OXException {
        // MSet
        RedisHashCommands<String, InputStream> rawHashCommands = commandsProvider.getRawHashCommands();
        for (Map.Entry<String, Map<String, InputStream>> e : field2Value.entrySet()) {
            rawHashCommands.hmset(e.getKey(), e.getValue());
        }
        if (metricsCollector != null && members != null) {
            for (RedisCacheKey redisKey : members) {
                metricsCollector.incrementPuts(redisKey);
            }
        }

        // TTL
        if (ttlSeconds > 0) {
            if (disableAutoFlushCommands(commandsProvider.getConnection())) {
                // Successfully disabled auto-flush behavior
                try {
                    RedisHashAsyncCommands<String, InputStream> hashAsyncCommands = commandsProvider.getRawHashAsyncCommands();
                    List<RedisFuture<List<Long>>> futures = new ArrayList<>(field2Value.size());
                    for (Map.Entry<String, Map<String, InputStream>> e : field2Value.entrySet()) {
                        String groupFqn = e.getKey();
                        futures.add(hashAsyncCommands.hexpire(groupFqn, ttlSeconds, e.getValue().keySet().toArray(Functions.getNewStringArrayIntFunction())));
                    }

                    // Write all commands to the transport layer
                    commandsProvider.getConnection().flushCommands();

                    // Wait until all futures complete
                    for (RedisFuture<List<Long>> f : futures) {
                        getFrom(f);
                    }
                    futures = null; // NOSONARLINT No more needed
                } finally {
                    // Re-enable auto-flush behavior
                    autoFlushCommands(commandsProvider.getConnection());
                }
            } else {
                // Auto-flush behavior could not be disabled. Do it using regular synchronous API.
                for (Map.Entry<String, Map<String, InputStream>> e : field2Value.entrySet()) {
                    String groupFqn = e.getKey();
                    rawHashCommands.hexpire(groupFqn, ttlSeconds, e.getValue().keySet().toArray(Functions.getNewStringArrayIntFunction()));
                }
            }
        }
    }

    /**
     * Puts a new object in the cache associated with the specified key and the specified time-to-live seconds.
     * <p>
     * If there is currently another object associated with the specified key, the old object will be replaced with the current one.
     * <p>
     * The object will be automatically invalidated after the specified time-to-live is elapsed.
     *
     * @param key The key
     * @param data The data to put into cache
     * @param ttlSeconds The time to live seconds
     * @param fireCacheEvent Whether to fire a cache event if existent value is replaced or not
     * @return <code>true</code> if an existent value has been replaced by put; otherwise <code>false</code>
     * @throws OXException If the put operation fails
     * @throws IllegalArgumentException If given key is <code>null</code>
     */
    public boolean put(RedisCacheKey key, InputStream data, long ttlSeconds, boolean fireCacheEvent) throws OXException {
        nullCheck(key, data);
        boolean replaced = executeOperation(CacheMetricName.PUT, commandsProvider -> doPut(key, data, ttlSeconds, commandsProvider)).booleanValue();
        if (replaced && fireCacheEvent) {
            cacheEventChannel.publish(CacheEvent.create(key));
        }
        return replaced;
    }

    /**
     * Puts a new object in the cache associated with the specified key and the specified time-to-live seconds.
     *
     * @param key The key
     * @param data The data to put into cache
     * @param ttlSeconds The time to live seconds
     * @param commandsProvider The commands provider to access Redis storage
     * @return <code>Boolean.TRUE</code> if an existent value has been replaced by put; otherwise <code>Boolean.FALSE</code>
     */
    private Boolean doPut(RedisCacheKey key, InputStream data, long ttlSeconds, RedisCommandsProvider commandsProvider) {
        String groupFqn = key.getGroupFQNIfValidSuffixElseNull();
        if (groupFqn == null) {
            boolean replaced;
            if (ttlSeconds > 0) {   // With time-to-live seconds
                replaced = commandsProvider.getRawStringCommands().setGet(key.getFQN(), data, SetArgs.Builder.ex(ttlSeconds)) != null;
            } else {                // Without time-to-live seconds
                replaced = commandsProvider.getRawStringCommands().setGet(key.getFQN(), data) != null;
            }
            if (metricsCollector != null) {
                metricsCollector.incrementPuts(key);
            }
            return Boolean.valueOf(replaced);
        }

        // true if field is a new field in the hash and value was set. false if field already exists in the hash and the value was updated
        RedisHashCommands<String, InputStream> rawHashCommands = commandsProvider.getRawHashCommands();
        boolean replaced = !rawHashCommands.hset(groupFqn, key.getSuffix(), data).booleanValue();
        if (metricsCollector != null) {
            metricsCollector.incrementPuts(key);
        }
        if (ttlSeconds > 0) {
            rawHashCommands.hexpire(groupFqn, ttlSeconds, key.getSuffix());
        }
        return Boolean.valueOf(replaced);
    }

    /**
     * Puts a new object in the cache associated with the specified key with given time-to-live seconds <b>only</b> if the specified key
     * is not already associated with a value.
     *
     * @param key The key
     * @param data The data to put into cache
     * @param ttlSeconds The time to live seconds
     * @return <code>true</code> if given value has been put into cache; otherwise <code>false</code>
     * @throws OXException If the put operation fails
     * @throws IllegalArgumentException If given key is <code>null</code>
     */
    public boolean putIfAbsent(RedisCacheKey key, InputStream data, long ttlSeconds) throws OXException {
        nullCheck(key, data);
        return executeOperation(CacheMetricName.PUT, commandsProvider -> doPutIfAbsent(key, data, ttlSeconds, commandsProvider)).booleanValue();
    }

    /**
     * Puts a new object in the cache associated with the specified key with given time-to-live seconds <b>only</b> if the specified key
     * is not already associated with a value.
     *
     * @param key The key
     * @param data The data to put into cache
     * @param ttlSeconds The time to live seconds
     * @param commandsProvider The commands provider to access Redis storage
     * @return <code>Boolean.TRUE</code> if given value has been put into cache; otherwise <code>Boolean.FALSE</code>
     * @throws OXException If operation fails
     */
    private Boolean doPutIfAbsent(RedisCacheKey key, InputStream data, long ttlSeconds, RedisCommandsProvider commandsProvider) throws OXException {
        String groupFqn = key.getGroupFQNIfValidSuffixElseNull();
        if (groupFqn == null) {
            SetArgs setArgs = ttlSeconds > 0 ? SetArgs.Builder.nx().ex(ttlSeconds) : SetArgs.Builder.nx();
            boolean put = OK.equals(commandsProvider.getRawStringCommands().set(key.getFQN(), data, setArgs));
            if (put && metricsCollector != null) {
                metricsCollector.incrementPuts(key);
            }
            return Boolean.valueOf(put);
        }

        Boolean put = getFrom(commandsProvider.getRawHashAsyncCommands().hsetnx(groupFqn, key.getSuffix(), data), 5, TimeUnit.SECONDS);
        if (put.booleanValue()) {
            // Successfully set field in the hash
            if (ttlSeconds > 0) {
                commandsProvider.getRawHashCommands().hexpire(groupFqn, ttlSeconds, key.getSuffix());
            }
            if (metricsCollector != null) {
                metricsCollector.incrementPuts(key);
            }
            return Boolean.TRUE;
        }
        // Such a hash field already exists in the hash and no operation was performed
        return Boolean.FALSE;
    }

    /**
     * Checks existence of the value associated with specified key in cache.
     *
     * @param key The key
     * @return <code>true</code>if existent; otherwise <code>false</code>
     * @throws OXException If the exists operation fails
     */
    public boolean exists(RedisCacheKey key) throws OXException {
        nullCheck(key);
        return executeOperation(CacheMetricName.GET, commandsProvider -> doExists(key, commandsProvider)).booleanValue();
    }

    /**
     * Checks existence of the value associated with specified key in Redis cache.
     *
     * @param key The key
     * @param commandsProvider The commands provider to access Redis storage
     * @return <code>Boolean.TRUE</code> if existent; otherwise <code>Boolean.FALSE</code>
     */
    private static Boolean doExists(RedisCacheKey key, RedisCommandsProvider commandsProvider) {
        String groupFqn = key.getGroupFQNElseNull();
        if (groupFqn == null) {
            return commandsProvider.getKeyCommands().exists(key.getFQN()).longValue() > 0 ? Boolean.TRUE : Boolean.FALSE;
        }

        String suffix = key.getSuffix();
        if (suffix == null) {
            // Check for existence of such a Redis hash
            return commandsProvider.getKeyCommands().exists(groupFqn).longValue() > 0 ? Boolean.TRUE : Boolean.FALSE;
        }

        // Check for field existence in hash
        return commandsProvider.getRawHashCommands().hexists(groupFqn, suffix);
    }

    /**
     * Gets the values associated with specified keys from cache.
     *
     * @param keys The keys
     * @return The values; individual values are empty if there is no object bound to the appropriate key
     * @throws OXException If the mget operation fails
     */
    public List<CacheKeyValue<InputStream>> mget(Collection<RedisCacheKey> keys) throws OXException {
        if (keys == null || keys.isEmpty()) {
            return Collections.emptyList();
        }
        return executeOperation(CacheMetricName.GET, commandsProvider -> doMGet(keys, commandsProvider));
    }

    /**
     * Gets the values associated with specified keys from cache.
     *
     * @param keys The keys
     * @return The values; individual values are empty if there is no object bound to the appropriate key
     * @throws OXException If the mget operation fails
     */
    public List<CacheKeyValue<InputStream>> mget(RedisCacheKey... keys) throws OXException {
        if (keys == null || keys.length <= 0) {
            return Collections.emptyList();
        }
        return executeOperation(CacheMetricName.GET, commandsProvider -> doMGet(Arrays.asList(keys), commandsProvider));
    }

    /**
     * Gets the values associated with specified keys from cache.
     *
     * @param keys The keys
     * @param commandsProvider The commands provider to access Redis storage
     * @return The values; individual values are empty if there is no object bound to the appropriate key
     */
    private List<CacheKeyValue<InputStream>> doMGet(Collection<RedisCacheKey> keys, RedisCommandsProvider commandsProvider) {
        List<List<KeyValue<String, InputStream>>> cleanUp = new ArrayList<>(2);
        try {
            int numKeys = keys.size();
            if (numKeys == 1) {
                RedisCacheKey key = keys.iterator().next();
                InputStream value = doGet(key, commandsProvider);
                return Collections.singletonList(RedisCacheKeyValue.fromNullable(key, value));
            }

            // MGET multiple keys
            Map<String, RedisCacheKey> stringkey2key = HashMap.newHashMap(numKeys);
            List<RedisCacheKey> regularKeys = null;
            Map<String, List<RedisCacheKey>> groupMemberKeys = null;
            StringBuilder hashFqn = null;
            for (RedisCacheKey key : keys) {
                String groupFqn = key.getGroupFQNIfValidSuffixElseNull();
                if (groupFqn == null) {
                    regularKeys = addOrCreate(key, regularKeys, numKeys);
                    stringkey2key.put(key.getFQN(), key);
                } else {
                    groupMemberKeys = returnOrCreateLinkedHashMap(groupMemberKeys, 2);
                    groupMemberKeys.computeIfAbsent(groupFqn, Functions.getNewArrayListFuntion()).add(key);
                    hashFqn = createOrReset(hashFqn).append(groupFqn).append('@').append(key.getSuffix());
                    stringkey2key.put(hashFqn.toString(), key);
                }
            }

            Map<CacheKey, CacheKeyValue<InputStream>> key2value = HashMap.newHashMap(numKeys);
            if (regularKeys != null) {
                // MGET for collected regular keys
                try {
                    List<KeyValue<String, InputStream>> keyValues = commandsProvider.getRawStringCommands().mget(regularKeys.stream().map(CacheKey::getFQN).toArray(Functions.getNewStringArrayIntFunction()));
                    cleanUp.add(keyValues);
                    for (KeyValue<String, InputStream> keyValue : keyValues) {
                        RedisCacheKey key = stringkey2key.get(keyValue.getKey());
                        if (key != null) {
                            InputStream value = keyValue.getValueOrElse(null);
                            addToMetricsSafe(key, value != null);
                            key2value.put(key, RedisCacheKeyValue.fromNullable(key, value));
                        }
                    }
                    regularKeys = null; // NOSONARLINT Might help GC
                } catch (RedisException e) {
                    if (Strings.asciiLowerCase(e.getMessage()).indexOf("wrongtype") >= 0) {
                        throw new IllegalArgumentException("One or more of the specified cache keys denote a group and not a plain value", e);
                    }
                    throw e;
                }
            }

            if (groupMemberKeys != null) {
                // Iterate the mapping of group FQN to group member keys
                RedisHashCommands<String, InputStream> rawHashCommands = commandsProvider.getRawHashCommands();
                for (Map.Entry<String, List<RedisCacheKey>> e : groupMemberKeys.entrySet()) {
                    // HMGET for collected group member keys
                    String groupFqn = e.getKey();
                    List<KeyValue<String, InputStream>> keyValues = rawHashCommands.hmget(groupFqn, e.getValue().stream().map(RedisCacheKey::getSuffix).toArray(Functions.getNewStringArrayIntFunction()));
                    cleanUp.add(keyValues);
                    for (KeyValue<String, InputStream> keyValue : keyValues) {
                        String field = keyValue.getKey();
                        hashFqn = createOrReset(hashFqn).append(groupFqn).append('@').append(field);
                        RedisCacheKey key = stringkey2key.get(hashFqn.toString());
                        if (key != null) {
                            InputStream value = keyValue.getValueOrElse(null);
                            addToMetricsSafe(key, value != null);
                            key2value.put(key, RedisCacheKeyValue.fromNullable(key, value));
                        }
                    }
                }
                groupMemberKeys = null; // NOSONARLINT Might help GC
            }
            stringkey2key = null; // NOSONARLINT Might help GC

            List<CacheKeyValue<InputStream>> retval = new ArrayList<>(numKeys);
            for (RedisCacheKey key : keys) { // NOSONARLINT
                CacheKeyValue<InputStream> cacheKeyValue = key2value.get(key);
                retval.add(cacheKeyValue == null ? RedisCacheKeyValue.empty(key) : cacheKeyValue);
            }
            key2value = null; // NOSONARLINT Might help GC
            cleanUp = null; // NOSONARLINT Avoid premature closing
            return retval;
        } finally {
            if (cleanUp != null) {
                for (List<KeyValue<String,InputStream>> keyValues : cleanUp) {
                    for (KeyValue<String,InputStream> keyValue : keyValues) {
                        Streams.close(keyValue.getValueOrElse(null));
                    }
                }
            }
        }
    }

    /**
     * Gets the value associated with specified key from cache.
     *
     * @param key The key
     * @return The value or <code>null</code>
     * @throws OXException If the get operation fails
     */
    public InputStream get(RedisCacheKey key) throws OXException {
        nullCheck(key);
        return executeOperation(CacheMetricName.GET, commandsProvider -> doGet(key, commandsProvider));
    }

    /**
     * Gets the value associated with specified key from Redis cache.
     *
     * @param key The key
     * @param commandsProvider The commands provider to access Redis storage
     * @return The value or <code>null</code>
     */
    private InputStream doGet(RedisCacheKey key, RedisCommandsProvider commandsProvider) {
        boolean error = true; // pessimistic
        InputStream value = null;
        try {
            String groupFqn = key.getGroupFQNIfValidSuffixElseNull();
            if (groupFqn == null) {
                try {
                    value = commandsProvider.getRawStringCommands().get(key.getFQN());
                } catch (RedisException e) {
                    if (Strings.asciiLowerCase(e.getMessage()).indexOf("wrongtype") >= 0) {
                        throw new IllegalArgumentException("Specified cache key denotes a group and not a plain value: " + key, e);
                    }
                    throw e;
                }
            } else {
                value = commandsProvider.getRawHashCommands().hget(groupFqn, key.getSuffix());
            }
            addToMetricsSafe(key, value != null);
            error = false; // Avoid premature closing
            return value;
        } finally {
            if (error) {
                Streams.close(value);
            }
        }
    }

    private void addToMetricsSafe(RedisCacheKey key, boolean hit) {
        if (metricsCollector != null) {
            try {
                if (hit) {
                    metricsCollector.incrementGets(key);
                } else {
                    metricsCollector.incrementMisses(key);
                }
            } catch (Exception e) {
                // Ignore
            }
        }
    }

    /**
     * Invalidates the cache entry associated with specified key.
     *
     * @param key The cache key
     * @param fireCacheEvent Whether to fire a cache event if existent value is invalidated or not
     * @return <code>true</code> if such a key has been invalidated; otherwise <code>false</code> if there was no such key
     * @throws OXException If invalidation fails
     */
    public boolean invalidate(RedisCacheKey key, boolean fireCacheEvent) throws OXException {
        nullCheck(key);
        Boolean result = executeOperation(CacheMetricName.INVALIDATION, commandsProvider -> doInvalidate(key, commandsProvider));
        if (fireCacheEvent && result.booleanValue()) {
            cacheEventChannel.publish(CacheEvent.create(key));
        }
        return result.booleanValue();
    }

    /**
     * Invalidates specified key from Redis cache.
     *
     * @param key The key to invalidate
     * @param commandsProvider The commands provider to access Redis storage
     * @return <code>true</code> if such a key has been dropped from cache; otherwise <code>false</code>
     */
    private Boolean doInvalidate(RedisCacheKey key, RedisCommandsProvider commandsProvider) {
        String groupFqn = key.getGroupFQNIfValidSuffixElseNull();
        boolean deleted;
        if (groupFqn == null) {
            deleted = l(commandsProvider.getKeyCommands().del(key.getFQN())) == 1;
        } else {
            deleted = l(commandsProvider.getRawHashCommands().hdel(groupFqn, key.getSuffix())) == 1;
        }
        if (deleted && metricsCollector != null) {
            metricsCollector.incrementInvalidations(key);
        }
        return Boolean.valueOf(deleted);
    }

    /**
     * Invalidates the cache entries associated with specified keys.
     *
     * @param keys The cache keys
     * @param fireCacheEvent Whether to fire a cache event for invalidated existing values or not
     * @throws OXException If invalidation fails
     */
    public void invalidate(List<RedisCacheKey> keys, boolean fireCacheEvent) throws OXException {
        nullCheck(keys);
        if (keys.isEmpty()) {
            return;
        }
        Long numberOfDeletedKeys = executeOperation(CacheMetricName.INVALIDATION, commandsProvider -> doInvalidate(keys, commandsProvider));
        if (fireCacheEvent && numberOfDeletedKeys != null && numberOfDeletedKeys.longValue() > 0) {
            List<CacheKey> eventKeys = keys.stream().filter(Predicates.isNotNullPredicate()).collect(CollectorUtils.toList(keys.size()));
            if (!eventKeys.isEmpty()) {
                cacheEventChannel.publish(CacheEvent.create(eventKeys));
            }
        }
    }

    /**
     * Invalidates the cache entries associated with specified keys from Redis cache.
     *
     * @param keys The cache keys
     * @param commandsProvider The commands provider to access Redis storage
     * @throws OXException If invalidation fails
     */
    private Long doInvalidate(List<RedisCacheKey> keys, RedisCommandsProvider commandsProvider) throws OXException {
        Map<String, List<RedisCacheKey>> groupKeys = null;
        List<RedisCacheKey> regularKeys = null;
        int numElements = keys.size();
        for (RedisCacheKey key : keys) {
            if (key != null) {
                String groupFqn = key.getGroupFQNIfValidSuffixElseNull();
                if (groupFqn == null) {
                    regularKeys = addOrCreate(key, regularKeys, numElements);
                } else {
                    groupKeys = returnOrCreateLinkedHashMap(groupKeys, 2);
                    groupKeys.computeIfAbsent(groupFqn, Functions.getNewArrayListFuntion()).add(key);
                }
            }
        }

        long numDeleted = 0;
        if (regularKeys != null) {
            List<String> strKeys = keys.stream().map(this::validateAndGetFqn).collect(CollectorUtils.toList(keys.size()));
            numDeleted = deleteKeysAsyncElseSync(strKeys, SCAN_RESULTS_LIMIT, commandsProvider);
        }

        if (groupKeys != null) {
            List<Map.Entry<String, List<String>>> hashKey2members = groupKeys.entrySet().stream()
                    .map(e -> Map.entry(e.getKey(), e.getValue().stream().map(this::validateAndGetSuffix).collect(CollectorUtils.toList(e.getValue().size()))))
                    .collect(CollectorUtils.toList(groupKeys.size()));
            numDeleted = RedisUtils.deleteMultipleHashMembersAsyncElseSync(hashKey2members, SCAN_RESULTS_LIMIT, commandsProvider);
        }

        return Long.valueOf(numDeleted);
    }

    private String validateAndGetFqn(RedisCacheKey key) {
        nullCheck(key);
        if (metricsCollector != null) {
            metricsCollector.incrementInvalidations(key);
        }
        return key.getFQN();
    }

    private String validateAndGetSuffix(RedisCacheKey key) {
        nullCheck(key);
        if (metricsCollector != null) {
            metricsCollector.incrementInvalidations(key);
        }
        return key.getSuffix();
    }

    /**
     * Invalidates specified key's group.
     *
     * @param groupKey The key providing the group to invalidate
     * @param fireCacheEvent Whether to fire a cache event or not
     * @throws OXException If invalidation fails
     */
    public List<RedisCacheKey> invalidateGroup(RedisCacheKey groupKey, boolean fireCacheEvent) throws OXException {
        List<RedisCacheKey> keys = executeOperation(CacheMetricName.INVALIDATION, commandsProvider -> doInvalidateGroup(groupKey, commandsProvider));
        if (fireCacheEvent && keys != null && !keys.isEmpty()) {
            for (CacheKey key : keys) {
                cacheEventChannel.publish(CacheEvent.create(key));
            }
        }
        return keys;
    }

    /**
     * Invalidates specified key's group.
     *
     * @param groupKey The key providing the group to invalidate
     * @param commandsProvider The commands provider to access Redis storage
     * @return The keys that were dropped through deleting the group
     */
    private static List<RedisCacheKey> doInvalidateGroup(RedisCacheKey groupKey, RedisCommandsProvider commandsProvider) {
        String groupFqn = groupKey.getGroupFQNElseNull();
        if (groupFqn == null) {
            throw new IllegalArgumentException("Specified cache key does not denote a group");
        }
        if (groupKey.getSuffix() != null) {
            throw new IllegalArgumentException("Specified cache key does not denote a group, but a member of a group");
        }

        // Determine the keys that are dropped when deleting Redis hash data collection
        List<String> fields = commandsProvider.getRawHashCommands().hkeys(groupFqn);

        // Delete the Redis hash & return affected keys
        commandsProvider.getKeyCommands().del(groupFqn);
        return fields == null || fields.isEmpty() ? Collections.emptyList() : fields.stream().map(field -> toKey(groupKey, field)).collect(CollectorUtils.toList(fields.size()));
    }

    /**
     * Creates the fully-qualifying cache key for given group key and field name.
     *
     * @param groupKey The group key; e.g. <code>"ox-cache:user:v1:1337"</code>
     * @param field The field name; e.g. <code>"1337:3"</code>
     * @return The cache key
     */
    private static RedisCacheKey toKey(RedisCacheKey groupKey, String field) {
        return RedisCacheKey.builder()
                            .withModuleName(groupKey.getModule())
                            .withVersionName(groupKey.getVersion())
                            .withGroup(groupKey.getGroup())
                            .withSuffix(field)
                            .build();
    }

    /**
     * Invalidates all values that match specified pattern.
     *
     * @param pattern The pattern; e.g. <code>"ox-cache:users:v1:1337:*"</code>
     * @param returnKeys Whether to return keys of invalidated elements
     * @param fireCacheEvent Whether to fire a cache event or not
     * @return The response possible containing keys of invalidated elements dependent on given argument
     * @throws OXException If mass invalidation fails
     */
    public MassInvalidationResponse massInvalidation(String pattern, boolean returnKeys, boolean fireCacheEvent) throws OXException {
        if (pattern == null || !pattern.startsWith(CORE_START)) {
            return new MassInvalidationResponse(returnKeys ? Collections.emptyList() : null, 0);
        }
        ScanArgs scanArgs = ScanArgs.Builder.matches(pattern).limit(SCAN_RESULTS_LIMIT);
        return executeOperation(CacheMetricName.INVALIDATION, commandsProvider -> doMassInvalidation(scanArgs, returnKeys, fireCacheEvent, commandsProvider));
    }

    /** The predicate to ensure Redis key appears to be a cache key; that is key starts with <code>"ox-cache:"</code> */
    private static final Predicate<? super String> IS_CACHE_KEY = key -> key != null && key.startsWith(CORE_START);

    /**
     * Invalidates all values using given <code>SCAN</code> argument.
     *
     * @param scanArgs The <code>SCAN</code> argument to use
     * @param returnKeys Whether to return keys of invalidated elements
     * @param fireCacheEvent Whether to fire a cache event or not
     * @param commandsProvider The commands provider to access Redis storage
     * @return The response possible containing keys of invalidated elements dependent on given argument
     * @throws OXException If mass invalidation fails
     */
    private MassInvalidationResponse doMassInvalidation(ScanArgs scanArgs, boolean returnKeys, boolean fireCacheEvent, RedisCommandsProvider commandsProvider) throws OXException {
        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();

        // Collect affected keys
        List<String> keys = null;
        KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
        while (cursor != null) {
            // Obtain current key chunk
            List<String> list = cursor.getKeys();
            List<String> chunk = list.stream().filter(IS_CACHE_KEY).collect(CollectorUtils.toList(list.size()));
            list = null; // NOSONARLINT
            if (!chunk.isEmpty()) {
                keys = addOrCreate(chunk, keys);
            }
            // Move cursor forward
            cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
        }
        if (keys == null || keys.isEmpty()) {
            // None available
            return new MassInvalidationResponse(returnKeys ? Collections.emptyList() : null, 0);
        }

        // Convert string keys to CacheKey instances for event and/or return value
        CollectedCacheKeys collectedCacheKeys = null;
        if (fireCacheEvent || returnKeys) {
            collectedCacheKeys = collectCacheKeys(keys, commandsProvider);
        }

        // Delete collected keys...
        long numberOfDeletedKeys = deleteKeysAsyncElseSync(keys, SCAN_RESULTS_LIMIT, commandsProvider);
        keys = null; // NOSONARLINT No more needed

        // Fire events and/or set invalidated keys
        List<RedisCacheKey> invalidatedKeys = null;
        if (collectedCacheKeys != null) {
            List<RedisCacheKey> cacheKeys = collectedCacheKeys.regularKeys == null ? new ArrayList<>() : new ArrayList<>(collectedCacheKeys.regularKeys);
            if (collectedCacheKeys.groupKeys2CacheKeys != null) {
                for (List<RedisCacheKey> subkeys : collectedCacheKeys.groupKeys2CacheKeys.values()) {
                    cacheKeys.addAll(subkeys);
                }
            }
            if (cacheKeys.isEmpty()) {
                if (returnKeys) {
                    invalidatedKeys = Collections.emptyList();
                }
            } else {
                if (fireCacheEvent) {
                    cacheEventChannel.publish(CacheEvent.create(cacheKeys));
                }
                if (returnKeys) {
                    invalidatedKeys = cacheKeys;
                }
            }
        }
        collectedCacheKeys = null; // NOSONARLINT No more needed
        return new MassInvalidationResponse(invalidatedKeys, numberOfDeletedKeys);
    }

    private static CollectedCacheKeys collectCacheKeys(List<String> keys, RedisCommandsProvider commandsProvider) throws OXException {
        List<RedisCacheKey> regularKeys = null;
        Map<String, List<RedisCacheKey>> groupKeys2CacheKeys = null;
        if (disableAutoFlushCommands(commandsProvider.getConnection())) {
            // Successfully disabled auto-flush behavior
            try {
                RedisKeyAsyncCommands<String, InputStream> keyAsyncCommands = commandsProvider.getKeyAsyncCommands();
                Map<String, RedisFuture<String>> futures = LinkedHashMap.newLinkedHashMap(keys.size());
                for (String key : keys) {
                    futures.put(key, keyAsyncCommands.type(key));
                }

                // Write all commands to the transport layer
                commandsProvider.getConnection().flushCommands();

                // Wait until all futures complete
                for (Map.Entry<String, RedisFuture<String>> entry : futures.entrySet()) {
                    if (TYPE_HASH.equals(getFrom(entry.getValue()))) {
                        groupKeys2CacheKeys = returnOrCreateLinkedHashMap(groupKeys2CacheKeys, 4);
                        // Placeholder
                        groupKeys2CacheKeys.put(entry.getKey(), Collections.emptyList());
                    } else {
                        regularKeys = addOrCreate(RedisCacheKeyUtil.parseKey(entry.getKey()), regularKeys, -1);
                    }
                }
                futures = null; // NOSONARLINT No more needed

                // Resolve members
                if (groupKeys2CacheKeys != null) {
                    RedisHashAsyncCommands<String, InputStream> hashAsyncCommands = commandsProvider.getRawHashAsyncCommands();
                    Map<String, RedisFuture<List<String>>> membersFutures = LinkedHashMap.newLinkedHashMap(groupKeys2CacheKeys.size());
                    for (String key : groupKeys2CacheKeys.keySet()) {
                        membersFutures.put(key, hashAsyncCommands.hkeys(key));
                    }

                    // Write all commands to the transport layer
                    commandsProvider.getConnection().flushCommands();

                    // Wait until all futures complete
                    for (Map.Entry<String, RedisFuture<List<String>>> entry : membersFutures.entrySet()) {
                        String key = entry.getKey();
                        List<String> hkeys = getFrom(entry.getValue());
                        groupKeys2CacheKeys.put(key, toCacheKeys(key, hkeys));
                    }
                    membersFutures = null; // NOSONARLINT No more needed
                }
            } finally {
                // Re-enable auto-flush behavior
                autoFlushCommands(commandsProvider.getConnection());
            }
        } else {
            // Auto-flush behavior could not be disabled. Do it using regular synchronous API.
            RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
            RedisHashCommands<String, InputStream> hashCommands = commandsProvider.getRawHashCommands();
            for (String key : keys) {
                if (TYPE_HASH.equals(keyCommands.type(key))) {
                    // Key identifies a Redis hash
                    groupKeys2CacheKeys = returnOrCreateLinkedHashMap(groupKeys2CacheKeys, 4);
                    List<String> hkeys = hashCommands.hkeys(key);
                    groupKeys2CacheKeys.put(key, toCacheKeys(key, hkeys));
                } else {
                    regularKeys = addOrCreate(RedisCacheKeyUtil.parseKey(key), regularKeys, -1);
                }
            }
        }
        return new CollectedCacheKeys(regularKeys, groupKeys2CacheKeys);
    }

    /**
     * Converts given group members to cache keys.
     *
     * @param groupKey The group (or hash) key; e.g. <code>"ox-cache:user:v1:1337"</code>
     * @param members The group members aka hash fields
     * @return The cache keys
     */
    private static List<RedisCacheKey> toCacheKeys(String groupKey, List<String> members) {
        RedisCacheKey tmp = RedisCacheKeyUtil.parseKey(groupKey);
        RedisCacheKey.Builder builder = RedisCacheKey.builder()
                    .withModuleName(tmp.getModule())
                    .withVersionName(tmp.getVersion())
                    .withGroup(tmp.getSuffix());
        List<RedisCacheKey> subkeys = new ArrayList<>(members.size());
        for (String field : members) {
            subkeys.add(builder.withSuffix(field).build());
        }
        return subkeys;
    }

    /**
     * Lists all keys that match specified pattern.
     *
     * @param pattern The pattern; e.g. <code>"ox-cache:users:v1:1337:*"</code>
     * @return The keys
     * @throws OXException If retrieval of keys fails
     */
    public List<RedisCacheKey> listKeys(String pattern) throws OXException {
        if (pattern == null || !pattern.startsWith(CORE_START)) {
            return Collections.emptyList();
        }
        ScanArgs scanArgs = ScanArgs.Builder.matches(pattern).limit(SCAN_RESULTS_LIMIT);
        return executeOperation(CacheMetricName.KEY_RETRIEVAL, commandsProvider -> doListKeys(scanArgs, commandsProvider));
    }

    /**
     * Lists all keys that serve given <code>SCAN</code> argument.
     *
     * @param scanArgs The <code>SCAN</code> argument.
     * @param commandsProvider The commands provider to access Redis storage
     * @return The collected keys
     */
    private static List<RedisCacheKey> doListKeys(ScanArgs scanArgs, RedisCommandsProvider commandsProvider) {
        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();

        List<RedisCacheKey> cacheKeys = new ArrayList<>();
        KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
        while (cursor != null) {
            // Obtain current keys...
            List<String> list = cursor.getKeys();
            List<String> keys = list.stream().filter(IS_CACHE_KEY).collect(CollectorUtils.toList(list.size()));
            list = null; // NOSONARLINT

            // ... and add them
            for (String skey : keys) {
                RedisCacheKey cacheKey = RedisCacheKeyUtil.parseKey(skey);
                if (cacheKey != null) {
                    cacheKeys.add(cacheKey);
                }
            }

            // Move cursor forward
            cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
        }
        return cacheKeys;
    }

    // --------------------------------------------- Classes -------------------------------------------------------------------

    static class MassInvalidationResponse {

        /** The keys that were requested being invalidated/removed */
        final List<RedisCacheKey> invalidatedKeys;

        /** The actual number of keys that have been removed */
        final long numberOfDeletedKeys;

        /**
         * Initializes a new {@link MassInvalidationResponse}.
         *
         * @param invalidatedKeys The keys that were requested being invalidated/removed
         * @param numberOfDeletedKeys The actual number of keys that have been removed
         */
        MassInvalidationResponse(List<RedisCacheKey> invalidatedKeys, long numberOfDeletedKeys) {
            super();
            this.invalidatedKeys = invalidatedKeys;
            this.numberOfDeletedKeys = numberOfDeletedKeys;
        }
    }

    private static class CollectedCacheKeys {

        /** The regular fully-qualifying cache keys not belonging to a group or <code>null</code> */
        final List<RedisCacheKey> regularKeys;

        /** The fully-qualifying cache keys belonging to a group or <code>null</code> */
        final Map<String, List<RedisCacheKey>> groupKeys2CacheKeys;

        /**
         * Initializes a new {@link CollectedCacheKeys}.
         *
         * @param regularKeys The regular cache keys
         * @param groupKeys2CacheKeys The cache keys belonging to a group
         */
        CollectedCacheKeys(List<RedisCacheKey> regularKeys, Map<String, List<RedisCacheKey>> groupKeys2CacheKeys) {
            super();
            this.regularKeys = regularKeys;
            this.groupKeys2CacheKeys = groupKeys2CacheKeys;
        }
    }

    ////////////////////////////// HELPERS /////////////////////////////

    /**
     * Performs a null check against the key and the object
     *
     * @param key The key
     * @param object The object
     * @throws IllegalArgumentException If either the key or the object are <code>null</code>
     */
    protected static void nullCheck(CacheKey key, InputStream object) {
        nullCheck(key);
        nullCheck(object);
    }

    /**
     * Performs a null check against the specified key
     *
     * @param key The key
     * @throws IllegalArgumentException If the key is <code>null</code>
     */
    protected static void nullCheck(CacheKey key) {
        if (key == null) {
            throw new IllegalArgumentException("Key must not be null");
        }
    }

    /**
     * Performs a null check against the specified keys
     *
     * @param keys The keys
     * @throws IllegalArgumentException If keys is <code>null</code>
     */
    protected static <C extends Collection<CacheKey>> void nullCheck(C keys) {
        if (keys == null) {
            throw new IllegalArgumentException("Keys must not be null");
        }
    }

    /**
     * Performs a null check against the specified object
     *
     * @param object The object
     * @throws IllegalArgumentException If the object is <code>null</code>
     */
    protected static <V> void nullCheck(V object) {
        if (object == null) {
            throw new IllegalArgumentException("Value must not be null");
        }
    }

}
