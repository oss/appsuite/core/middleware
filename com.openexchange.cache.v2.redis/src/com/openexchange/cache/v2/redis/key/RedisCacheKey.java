/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.key;

import static com.openexchange.cache.v2.filter.CacheFilter.DELIMITER;
import static com.openexchange.cache.v2.redis.key.RedisCacheKeyUtil.containsInvalidPartCharacter;
import static com.openexchange.cache.v2.redis.key.RedisCacheKeyUtil.containsInvalidSuffixCharacter;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyType;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.SuffixBuilder;
import com.openexchange.cache.v2.filter.ApplicationName;
import com.openexchange.cache.v2.filter.ModuleName;
import com.openexchange.cache.v2.filter.VersionName;
import com.openexchange.java.Strings;

/**
 * {@link RedisCacheKey} - The Redis cache key implementation.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RedisCacheKey implements CacheKey {

    private static final long serialVersionUID = -6756658532265285785L;

    /**
     * Creates builder to build {@link RedisCacheKey}.
     *
     * @return The newly created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Creates builder to build {@link RedisCacheKey}.
     *
     * @param options The options to initialize builder from
     * @return The newly created builder
     * @throws IllegalArgumentException If passed options are <code>null</code>
     */
    public static <V> Builder builder(CacheOptions<V> options) {
        if (options == null) {
            throw new IllegalArgumentException("Cache options must not be null");
        }
        return new Builder().withModuleName(options.getModuleName()).withVersionName(options.getVersionName());
    }

    /**
     * Builder to build {@link RedisCacheKey}.
     */
    public static final class Builder {

        private ModuleName moduleName;
        private VersionName versionName;
        private final StringBuilder suffixBuilder = new StringBuilder(16);
        private String group;

        /**
         * Initializes a new {@link Builder}.
         *
         * @param delimiter The delimiter character
         */
        Builder() {
            super();
        }

        /**
         * Sets given module name.
         * <p>
         * <code>"ox-cache:<b>context</b>:v1:1337"</code>
         *
         * @param moduleName The module name to set; e.g. <code>"context"</code>
         * @return This builder
         */
        public Builder withModuleName(ModuleName moduleName) {
            this.moduleName = moduleName;
            return this;
        }

        /**
         * Sets given version name.
         * <p>
         * <code>"ox-cache:context:<b>v1</b>:1337"</code>
         *
         * @param versionName The version name to set; e.g. <code>"v1"</code>
         * @return This builder
         */
        public Builder withVersionName(VersionName versionName) {
            this.versionName = versionName;
            return this;
        }

        /**
         * Sets the group identifier.
         * <p>
         * <code>"ox-cache:user:v1:<b>1337</b>[1]"</code>
         *
         * @param group The group identifier to set; e.g. <code>"1337"</code>
         * @return This builder
         */
        public Builder withGroup(String group) {
            if (Strings.isEmpty(group)) {
                return this;
            }
            if (containsInvalidPartCharacter(group)) {
                this.group = RedisCacheKeyUtil.escapeKeyComponent(group.trim());
            } else {
                this.group = group.trim();
            }
            return this;
        }

        /**
         * Adds given suffix portion; either for a regular key or a group member.
         * <p>
         * <ul>
         * <li><code>"ox-cache:context:v1:<b>1337</b>"</code>
         * <li><code>"ox-cache:user:v1:1337[<b>12</b>]"</code>
         * </ul>
         *
         * @param suffix The suffix to add; e.g. <code>"1337"</code>
         * @return This builder
         */
        public Builder addSuffix(String suffix) {
            if (Strings.isEmpty(suffix)) {
                return this;
            }
            if (containsInvalidPartCharacter(suffix)) {
                suffixBuilder.append(RedisCacheKeyUtil.escapeKeyComponent(suffix).trim());
            } else {
                suffixBuilder.append(suffix.trim());
            }
            // Append trailing delimiter character
            suffixBuilder.append(DELIMITER);
            return this;
        }

        /**
         * Sets given complete suffix. Any previously added suffix part is overwritten.
         * <p>
         * <code>"ox-cache:attributes:v1:<b>1337:4711</b>"</code>
         *
         * @param completeSuffix The complete suffix to set; e.g. <code>"1337:4711"</code>
         * @return This builder
         */
        public Builder withSuffix(String completeSuffix) {
            if (Strings.isEmpty(completeSuffix)) {
                return this;
            }
            if (containsInvalidSuffixCharacter(completeSuffix)) {
                throw new IllegalArgumentException("Invalid complete suffix: " + completeSuffix);
            }
            suffixBuilder.setLength(0);
            suffixBuilder.append(completeSuffix);
            // Append trailing delimiter character
            suffixBuilder.append(DELIMITER);
            return this;
        }

        /**
         * Sets given complete suffix. Any previously added suffix part is overwritten.
         * <p>
         * <code>"ox-cache:attributes:v1:<b>1337:4711</b>"</code>
         *
         * @param builder The complete suffix to set; e.g. <code>"1337:4711"</code>
         * @return This builder
         */
        public Builder withSuffix(SuffixBuilder builder) {
            String completeSuffix = builder.toString();
            if (Strings.isEmpty(completeSuffix)) {
                return this;
            }
            suffixBuilder.setLength(0);
            suffixBuilder.append(completeSuffix);
            // Append trailing delimiter character
            suffixBuilder.append(DELIMITER);
            return this;
        }

        /**
         * Builds the resulting instance of <code>RedisCacheKey</code> from this builder.
         *
         * @return The resulting instance of <code>RedisCacheKey</code>
         */
        public RedisCacheKey build() {
            if (versionName != null && VersionName.DEFAULT_VERSION_NAME != versionName) {
                if (moduleName == null) { // NOSONARLINT
                    throw new IllegalArgumentException("Module name must not be null if a version name is present");
                }
            }

            int suffixLength = suffixBuilder.length();
            if (suffixLength <= 0) {
                // No suffix set
                return new RedisCacheKey(moduleName, versionName, null, group);
            }

            // Suffix available
            if (moduleName == null) {
                throw new IllegalArgumentException("Module name must not be null if a suffix is present");
            }
            if (versionName == null) {
                throw new IllegalArgumentException("Version name must not be null if a suffix is present");
            }

            // Discard trailing delimiter character
            suffixBuilder.setLength(suffixLength - 1);
            return new RedisCacheKey(moduleName, versionName, suffixBuilder.toString(), group);
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final ModuleName moduleName;
    private final VersionName versionName;
    private final String suffix;
    private final String group;
    private final String fqn;
    private final String groupFqn;
    private final CacheKeyType type;

    private final int hashCode;

    /**
     * Initializes a new {@link RedisCacheKey}.
     *
     * @param moduleName The module name
     * @param versionName The version name
     * @param suffix The suffix
     * @param group The group identifier or <code>null</code>
     */
    RedisCacheKey(ModuleName moduleName, VersionName versionName, String suffix, String group) {
        super();
        this.moduleName = moduleName;
        this.versionName = versionName;
        this.suffix = suffix;
        this.group = group;

        // Build prefix; e.g. "ox-cache:users:v1"
        StringBuilder sb = new StringBuilder(64);
        sb.append(ApplicationName.getInstance().getName());
        if (moduleName != null) {
            sb.append(DELIMITER).append(moduleName.getName());
        }
        if (versionName != null) {
            sb.append(DELIMITER).append(versionName.getName());
        }

        // Build group FQN; e.g. "ox-cache:users:v1:1337"
        CacheKeyType type = CacheKeyType.KEY;
        if (group == null) {
            groupFqn = null;
        } else {
            sb.append(DELIMITER).append(group);
            groupFqn = sb.toString();
            type = CacheKeyType.GROUP;
        }

        // Build FQN; e.g. "ox-cache:users:v1:1337:3" or "ox-cache:users:v1:1337[3]"
        if (group == null) {
            if (suffix != null) {
                sb.append(DELIMITER).append(suffix);
            }
        } else {
            sb.append('[');
            if (suffix != null) {
                sb.append(suffix);
                type = CacheKeyType.MEMBER;
            }
            sb.append(']');
        }
        this.fqn = sb.toString();
        this.type = type;
        sb = null; // No more needed

        int prime = 31;
        int result = 1;
        result = prime * result + ((fqn == null) ? 0 : fqn.hashCode());
        hashCode = result;
    }

    @Override
    public String getFQN() {
        return fqn;
    }

    @Override
    public String getGroup() {
        return group;
    }

    @Override
    public CacheKeyType getType() {
        return type;
    }

    /**
     * Gets the fully-qualifying name for the group if group identifier has been set; otherwise <code>null</code>.
     *
     * @return The group fully-qualifying name (e.g. <code>"ox-cache:user:v1:1337"</code>) or <code>null</code>
     */
    public String getGroupFQNElseNull() {
        return groupFqn;
    }

    /**
     * Gets the fully-qualifying name for the group if group identifier AND a valid suffix have been set; otherwise <code>null</code>.
     *
     * @return The group fully-qualifying name (e.g. <code>"ox-cache:user:v1:1337"</code>) or <code>null</code>
     * @throws IllegalStateException If this instance is a group cache key, but has no suffix portion specified
     */
    public String getGroupFQNIfValidSuffixElseNull() {
        if (groupFqn == null) {
            return null;
        }
        if (suffix == null) {
            throw new IllegalStateException("No suffix portion specified in group member key: " + this);
        }
        return groupFqn;
    }

    /**
     * Gets the module name.
     *
     * @return The module name or <code>null</code>
     */
    public ModuleName getModule() {
        return moduleName;
    }

    /**
     * Gets the version name.
     *
     * @return The version name or <code>null</code>
     */
    public VersionName getVersion() {
        return versionName;
    }

    /**
     * Gets the suffix.
     *
     * @return The suffix or <code>null</code>
     */
    public String getSuffix() {
        return suffix;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        CacheKey other = (CacheKey) obj;
        if (fqn == null) {
            if (other.getFQN() != null) {
                return false;
            }
        } else if (!fqn.equals(other.getFQN())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getFQN();
    }

}
