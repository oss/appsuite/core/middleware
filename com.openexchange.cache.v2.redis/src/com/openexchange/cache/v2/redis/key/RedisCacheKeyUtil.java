/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.key;

import static com.openexchange.cache.v2.filter.CacheFilter.DELIMITER;
import static com.openexchange.cache.v2.filter.CacheFilters.moduleNameFor;
import static com.openexchange.cache.v2.filter.CacheFilters.versionNameFor;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.filter.ApplicationName;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.java.Strings;

/**
 * {@link RedisCacheKeyUtil} - Utilities for Redis cache keys.
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class RedisCacheKeyUtil {

    /**
     * Initializes a new {@link RedisCacheKeyUtil}.
     */
    private RedisCacheKeyUtil() {
        super();
    }

    private static IllegalArgumentException createModuleNameAbsentException(CacheKey key) {
        return new IllegalArgumentException(new StringBuilder("The module name of the specified key '").append(key).append("' is absent").toString());
    }

    /**
     * Creates the module portion from the specified cache key ending with delimiter character.
     *
     * @param key The key
     * @return The module portion; e.g. <code>"ox-cache:mailaccount:"</code>
     */
    public static String createModulePortion(RedisCacheKey key) {
        if (null == key.getModule()) {
            throw createModuleNameAbsentException(key);
        }
        StringBuilder b = new StringBuilder(32);
        b.append(ApplicationName.getInstance().getName()).append(DELIMITER);
        b.append(key.getModule().getName()).append(DELIMITER);
        return b.toString();
    }

    /** The expected start of a fully-qualified name for a cache key: <code>"ox-cache:"</code> */
    public static final String CORE_START = ApplicationName.getInstance().getName() + DELIMITER;

    /**
     * Parses given fully-qualified name to a cache key instance.
     *
     * @param fqn The fully-qualified name
     * @return The cache key instance
     */
    public static RedisCacheKey parseKey(String fqn) { // NOSONARLINT
        if (fqn == null) {
            throw new IllegalArgumentException("Fully-qualified name for a cache key must not be null");
        }

        if (fqn.endsWith(String.valueOf(DELIMITER)) || !fqn.startsWith(CORE_START)) {
            throw new IllegalArgumentException("Invalid fully-qualified name for a cache key: " + fqn);
        }

        int mBegin = fqn.indexOf('[');
        if (mBegin < 0) {
            // A non-group FQN; e.g. "ox-cache:context:v1:1337"
            return parseRegularFqn(fqn);
        }

        // Assume group FQN ending with ']'; e.g. "ox-cache:user:v1:1337[3]"
        if (mBegin == 0 || fqn.charAt(fqn.length() - 1) != ']') {
            throw new IllegalArgumentException("Invalid fully-qualified name for a group cache key: " + fqn);
        }

        // Tokenize until opening bracket; e.g. "ox-cache:user:v1:1337"
        String[] parts = Strings.splitBy(fqn.substring(0, mBegin), DELIMITER, true);
        for (String part : parts) {
            if (Strings.isEmpty(part)) {
                throw new IllegalArgumentException("Invalid fully-qualified name for a group cache key (infix part must not be empty): " + fqn);
            }
        }

        // Expect four tokens; e.g.: "ox-cache", "user", "v1", "1337"
        if (parts.length != 4) {
            throw new IllegalArgumentException("Invalid fully-qualified name for a group cache key: " + fqn);
        }

        // Interpret everything inside parenthesis as member identifier (aka. field)
        RedisCacheKey.Builder builder = builderWith(parts[1], parts[2]);
        builder.withGroup(parts[3]);
        String[] suffixes = Strings.splitBy(fqn.substring(mBegin + 1, fqn.length() - 1), DELIMITER, true);
        for (String suffix : suffixes) {
            builder.addSuffix(suffix);
        }
        return builder.build();
    }

    /**
     * Parses a regular fully-qualified name for a cache key; e.g. <code>"ox-cache:context:v1:1337"</code>.
     *
     * @param fqn The regular fully-qualified name to parse
     * @return The parsed key
     */
    private static RedisCacheKey parseRegularFqn(String fqn) {
        String[] parts = Strings.splitBy(fqn, DELIMITER, true);
        for (String part : parts) {
            if (Strings.isEmpty(part)) {
                throw new IllegalArgumentException("Invalid fully-qualified name for a cache key (infix part must not be empty): " + fqn);
            }
        }
        switch (parts.length) {
            case 0, 1:
                // "ox-cache"
                return RedisCacheKey.builder().build();
            case 2:
                // "ox-cache:context"
                return builderWith(parts[1]).build();
            case 3:
                // "ox-cache:context:v1"
                return builderWith(parts[1], parts[2]).build();
            default:
                // "ox-cache:context:v1:1337"
                RedisCacheKey.Builder builder = builderWith(parts[1], parts[2]);
                for (int i = 3; i < parts.length; i++) {
                    builder.addSuffix(parts[i]);
                }
                return builder.build();
        }
    }

    /**
     * Creates a key builder with given application and module name.
     *
     * @param moduleName The module name
     * @return The newly created key builder
     */
    private static RedisCacheKey.Builder builderWith(String moduleName) {
        return RedisCacheKey.builder().withModuleName(moduleNameFor(moduleName));
    }

    /**
     * Creates a key builder with given application, module and version name.
     *
     * @param moduleName The module name
     * @param versionName The version name
     * @return The newly created key builder
     */
    private static RedisCacheKey.Builder builderWith(String moduleName, String versionName) {
        return RedisCacheKey.builder().withModuleName(moduleNameFor(moduleName)).withVersionName(versionNameFor(versionName));
    }

    /**
     * Replaces invalid chars in key components with a replacement
     *
     * @param component The component to adjust
     * @return The adjusted component
     */
    static String escapeKeyComponent(String component) {
        return URLEncoder.encode(component, StandardCharsets.UTF_8);
    }

    private static final char[] INVALIDS = { DELIMITER, CacheFilter.WILDCARD, '[', ']' };

    /**
     * Checks if specified key part contains invalid characters that need to be escaped.
     *
     * @param part The key part to check
     * @return <code>true</code> if key part contains invalid characters; otherwise <code>false</code>
     */
    static boolean containsInvalidPartCharacter(String part) {
        for (char invalid : INVALIDS) {
            if (part.indexOf(invalid) >= 0) {
                return true;
            }
        }
        return false;
    }

    private static final char[] INVALIDS_SUFFIX = { CacheFilter.WILDCARD, '[', ']' };

    /**
     * Checks if specified key part contains invalid characters that need to be escaped.
     *
     * @param part The key part to check
     * @return <code>true</code> if key part contains invalid characters; otherwise <code>false</code>
     */
    static boolean containsInvalidSuffixCharacter(String part) {
        for (char invalid : INVALIDS_SUFFIX) {
            if (part.indexOf(invalid) >= 0) {
                return true;
            }
        }
        return false;
    }

}
