/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2.redis.key;

import static com.openexchange.cache.v2.filter.CacheFilter.DELIMITER;
import com.openexchange.cache.v2.SuffixBuilder;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.java.Strings;

/**
 * {@link RedisSuffixBuilder} - The Redis suffix builder.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class RedisSuffixBuilder implements SuffixBuilder {

    private final StringBuilder suffixBuilder;

    /**
     * Initializes a new instance of {@link RedisSuffixBuilder}.
     *
     * @param capacity The initial capacity.
     */
    public RedisSuffixBuilder(int capacity) {
        super();
        suffixBuilder = new StringBuilder(capacity);
    }

    @Override
    public boolean isEmpty() {
        return suffixBuilder.isEmpty();
    }

    @Override
    public SuffixBuilder reset() {
        suffixBuilder.setLength(0);
        return this;
    }

    @Override
    public SuffixBuilder append(Object... suffixes) {
        if (suffixes != null && suffixes.length > 0) {
            if (suffixBuilder.length() > 0 && suffixBuilder.charAt(suffixBuilder.length() - 1) != DELIMITER) {
                // May happen if toString() has been called in between
                suffixBuilder.append(DELIMITER);
            }

            if (suffixes.length == 1) {
                String suffix = suffixes[0] == null ? null : suffixes[0].toString();
                if (Strings.isNotEmpty(suffix)) {
                    if (containsInvalidCharacter(suffix)) {
                        suffix = RedisCacheKeyUtil.escapeKeyComponent(suffix);
                    }
                    suffixBuilder.append(suffix.trim()).append(DELIMITER);
                }
            } else {
                for (Object o : suffixes) {
                    String suffix = o == null ? null : o.toString();
                    if (Strings.isNotEmpty(suffix)) {
                        if (containsInvalidCharacter(suffix)) {
                            suffixBuilder.append(RedisCacheKeyUtil.escapeKeyComponent(suffix).trim());
                        } else {
                            suffixBuilder.append(suffix.trim());
                        }

                        // Append trailing delimiter character
                        suffixBuilder.append(DELIMITER);
                    }
                }
            }
        }
        return this;
    }

    @Override
    public SuffixBuilder append(int suffix) {
        if (suffixBuilder.length() > 0 && suffixBuilder.charAt(suffixBuilder.length() - 1) != DELIMITER) {
            // May happen if toString() has been called in between
            suffixBuilder.append(DELIMITER);
        }
        suffixBuilder.append(suffix).append(DELIMITER);
        return this;
    }

    @Override
    public SuffixBuilder append(int... suffixes) {
        if (suffixes != null && suffixes.length > 0) {
            if (suffixBuilder.length() > 0 && suffixBuilder.charAt(suffixBuilder.length() - 1) != DELIMITER) {
                // May happen if toString() has been called in between
                suffixBuilder.append(DELIMITER);
            }

            if (suffixes.length == 1) {
                suffixBuilder.append(suffixes[0]).append(DELIMITER);
            } else {
                for (int i : suffixes) {
                    suffixBuilder.append(i).append(DELIMITER);
                }
            }
        }
        return this;
    }

    @Override
    public SuffixBuilder appendHashPart(Object... hashParts) {
        if (hashParts != null && hashParts.length > 0) {
            checkNoHashPartYet();
            if (suffixBuilder.length() > 0 && suffixBuilder.charAt(suffixBuilder.length() - 1) != DELIMITER) {
                // May happen if toString() has been called in between
                suffixBuilder.append(DELIMITER);
            }

            if (hashParts.length == 1) {
                String hashPart = hashParts[0] == null ? null : hashParts[0].toString();
                if (Strings.isNotEmpty(hashPart)) {
                    if (containsInvalidCharacter(hashPart)) {
                        hashPart = RedisCacheKeyUtil.escapeKeyComponent(hashPart);
                    }
                    suffixBuilder.append('{').append(hashPart.trim()).append('}').append(DELIMITER);
                }
            } else {
                boolean append = false;
                for (Object o : hashParts) {
                    String hashPart = o == null ? null : o.toString();
                    if (Strings.isNotEmpty(hashPart)) {
                        if (!append) {
                            suffixBuilder.append('{');
                            append = true;
                        }
                        if (containsInvalidCharacter(hashPart)) {
                            hashPart = RedisCacheKeyUtil.escapeKeyComponent(hashPart);
                        }
                        suffixBuilder.append(hashPart.trim());

                        // Append trailing delimiter character
                        suffixBuilder.append(DELIMITER);
                    }
                }
                if (append) {
                    // Discard trailing delimiter character
                    suffixBuilder.setLength(suffixBuilder.length() - 1);
                    suffixBuilder.append('}');
                    suffixBuilder.append(DELIMITER);
                }
            }
        }
        return this;
    }

    @Override
    public SuffixBuilder appendHashPart(int hashPart) {
        checkNoHashPartYet();
        if (suffixBuilder.length() > 0 && suffixBuilder.charAt(suffixBuilder.length() - 1) != DELIMITER) {
            // May happen if toString() has been called in between
            suffixBuilder.append(DELIMITER);
        }
        suffixBuilder.append('{').append(hashPart).append('}').append(DELIMITER);
        return this;
    }

    @Override
    public SuffixBuilder appendHashPart(int... hashParts) {
        if (hashParts != null && hashParts.length > 0) {
            checkNoHashPartYet();
            if (suffixBuilder.length() > 0 && suffixBuilder.charAt(suffixBuilder.length() - 1) != DELIMITER) {
                // May happen if toString() has been called in between
                suffixBuilder.append(DELIMITER);
            }
            if (hashParts.length == 1) {
                suffixBuilder.append('{').append(hashParts[0]).append('}').append(DELIMITER);
            } else {
                suffixBuilder.append('{');
                for (int i : hashParts) {
                    suffixBuilder.append(i);

                    // Append trailing delimiter character
                    suffixBuilder.append(DELIMITER);
                }
                // Discard trailing delimiter character
                suffixBuilder.setLength(suffixBuilder.length() - 1);
                suffixBuilder.append('}');
                suffixBuilder.append(DELIMITER);
            }
        }
        return this;
    }

    /**
     * Checks that this builder does not yet contain a hash part.
     */
    private void checkNoHashPartYet() {
        if (suffixBuilder.indexOf("{") >= 0) {
            throw new IllegalArgumentException("A hash part has already been specified for the suffix: " + this);
        }
    }

    @Override
    public String toString() {
        int suffixLength = suffixBuilder.length();
        if (suffixLength <= 0) {
            // No suffix set
            return null;
        }

        if (suffixBuilder.charAt(suffixLength - 1) == DELIMITER) {
            suffixBuilder.setLength(suffixLength - 1);
        }
        return suffixBuilder.toString();
    }

    // ---------------------------------------------------------------------------------------------------------------------------------

    private static final char[] INVALIDS = { DELIMITER, CacheFilter.WILDCARD, '[', ']', '{', '}' };

    /**
     * Checks if specified key part contains invalid characters that need to be escaped.
     *
     * @param part The key part to check
     * @return <code>true</code> if key part contains invalid characters; otherwise <code>false</code>
     */
    private static boolean containsInvalidCharacter(String part) {
        for (char invalid : INVALIDS) {
            if (part.indexOf(invalid) >= 0) {
                return true;
            }
        }
        return false;
    }

}
