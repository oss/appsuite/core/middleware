/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis;

import static com.openexchange.cache.v2.redis.Utils.PREDICATE_NO_GROUP_KEY;
import static com.openexchange.cache.v2.redis.Utils.addOrCreate;
import static com.openexchange.cache.v2.redis.Utils.createIsGroupKeyException;
import static com.openexchange.cache.v2.redis.Utils.deserializeSafe;
import static com.openexchange.cache.v2.redis.Utils.keyCheck;
import static com.openexchange.cache.v2.redis.Utils.returnOrCreateHashMap;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheExceptionCode;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyType;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.CacheLoader;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.SignalingCacheLoader;
import com.openexchange.cache.v2.SuffixBuilder;
import com.openexchange.cache.v2.codec.CacheValueCodec;
import com.openexchange.cache.v2.codec.CompressionType;
import com.openexchange.cache.v2.redis.inmemory.InMemoryCache;
import com.openexchange.cache.v2.redis.inmemory.InMemoryCacheFactory;
import com.openexchange.cache.v2.redis.inmemory.InMemoryCacheOptions;
import com.openexchange.cache.v2.redis.inmemory.Mode;
import com.openexchange.cache.v2.redis.key.RedisCacheKey;
import com.openexchange.cache.v2.redis.key.RedisSuffixBuilder;
import com.openexchange.cache.v2.redis.osgi.Services;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.IReference;
import com.openexchange.java.ImmutableReference;
import com.openexchange.java.Predicates;
import com.openexchange.java.Strings;
import com.openexchange.lock.AccessControl;
import com.openexchange.lock.AccessControls;
import com.openexchange.lock.LockService;
import com.openexchange.lock.ReentrantLockAccessControl;
import com.openexchange.redis.compression.CompressionTypeProvidingInputStream;
import com.openexchange.threadpool.ThreadPoolService;

/**
 * {@link RedisCache} - The cache backed by Redis storage.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @param <V> The type of the cache values
 */
public class RedisCache<V> implements Cache<V> {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(RedisCache.class); // NOSONARLINT
    }

    /** The mapping from cache compression type to Redis compression type */
    private static final Map<CompressionType, com.openexchange.redis.compression.CompressionType> COMPRESSION_TYPE_MAPPING = Map.of(
        CompressionType.NONE, com.openexchange.redis.compression.CompressionType.NONE,
        CompressionType.DEFLATE, com.openexchange.redis.compression.CompressionType.DEFLATE,
        CompressionType.GZIP, com.openexchange.redis.compression.CompressionType.GZIP,
        CompressionType.SNAPPY, com.openexchange.redis.compression.CompressionType.SNAPPY
    );

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final CacheOptions<V> options;
    private final InMemoryCache<V> inMemoryCache;
    private final boolean volatileInMemoryCache;
    private final NativeRedisCache nativeCache;
    private final GlobalRedisCacheOptions globalCacheOptions;
    private final ThreadPoolService threadPool;

    /**
     * Initializes a new {@link RedisCache}.
     *
     * @param options The options for the cache view
     * @param nativeCache The native cache
     * @param globalCacheOptions The global options for Redis-backed cache
     * @param inMemoryCacheOptions The options for in-memory cache
     */
    public RedisCache(CacheOptions<V> options, NativeRedisCache nativeCache, GlobalRedisCacheOptions globalCacheOptions, InMemoryCacheOptions inMemoryCacheOptions) {
        super();
        this.options = options;
        this.globalCacheOptions = globalCacheOptions;
        this.nativeCache = nativeCache;
        this.inMemoryCache = InMemoryCacheFactory.getInstance().<V> getOrCreateInMemoryCache(inMemoryCacheOptions, options.getModuleName(), options.getVersionName()).orElse(null);
        this.volatileInMemoryCache = inMemoryCache != null && Mode.VOLATILE == inMemoryCache.getMode();
        this.threadPool = Services.optService(ThreadPoolService.class);
    }

    /**
     * Serializes given cache value to appropriate byte stream using the option's codec.
     *
     * @param <V> The type to serialize from
     * @param object The cache value to serialize
     * @param options The cache options to use
     * @return The appropriate byte stream
     * @throws Exception If serialization fails
     */
    private static <V> InputStream serializeValue(V object, CacheOptions<V> options) throws Exception {
        CacheValueCodec<V> codec = options.getCodec();
        CompressionType compressionType = codec.getCompressionType();
        if (compressionType == CompressionType.NONE) {
            return codec.serialize(object);
        }
        return compressionType == null ? codec.serialize(object) : new CompressionTypeProvidingInputStream(codec.serialize(object), COMPRESSION_TYPE_MAPPING.get(compressionType));
    }

    /**
     * Fetches the value for given key from in-memory cache.
     *
     * @param redisKey The key to fetch the value for
     * @param loader The loader or <code>null</code>
     * @return The cached value or <code>null</code>
     */
    private V fetchFromInMemoryCache(RedisCacheKey redisKey, CacheLoader<V> loader) {
        if (volatileInMemoryCache) {
            return inMemoryCache.getIfPresent(redisKey);
        }

        // Update-After-Fetch: Only fetch if thread pool is available since a background update is supposed being submitted
        V object = threadPool == null ? null : inMemoryCache.getIfPresent(redisKey);
        if (object != null) {
            inMemoryCache.submitUpdateInMemoryCache(redisKey, loader, this, threadPool);
        }
        return object;
    }

    @Override
    public RedisCacheKey newKey(Object... suffix) {
        if (suffix == null || suffix.length <= 0) {
            throw new IllegalArgumentException("Suffix portion must not be null or empty");
        }
        return newCacheKey0(null, suffix);
    }

    @Override
    public RedisCacheKey newGroupMemberKey(String group, Object... suffix) {
        if (suffix == null || suffix.length <= 0) {
            throw new IllegalArgumentException("Suffix portion must not be null or empty");
        }
        if (Strings.isEmpty(group)) {
            throw new IllegalArgumentException("Group identifier must not be null or empty");
        }
        return newCacheKey0(group, suffix);
    }

    /**
     * Creates a new fully-qualifying cache key for given arguments.
     *
     * @param optGroup The group identifier or <code>null</code>
     * @param suffix The suffix portion
     * @return The fully-qualifying cache key
     * @throws IllegalArgumentException If a suffix part is <code>null</code> or its <code>toString()</code> representation is empty
     */
    private RedisCacheKey newCacheKey0(String optGroup, Object... suffix) {
        RedisCacheKey.Builder builder = RedisCacheKey.builder(options);
        if (optGroup != null) {
            builder.withGroup(optGroup);
        }
        for (Object part : suffix) {
            String s = part == null ? null : part.toString();
            if (Strings.isEmpty(s)) {
                throw new IllegalArgumentException("Suffix part must not be null or empty");
            }
            builder.addSuffix(s);
        }
        return builder.build();
    }

    @Override
    public CacheKey newKey(SuffixBuilder suffix) {
        if (suffix == null || suffix.isEmpty()) {
            throw new IllegalArgumentException("Suffix portion must not be null or empty");
        }
        return newCacheKey0(null, suffix);
    }

    @Override
    public CacheKey newGroupMemberKey(String group, SuffixBuilder suffix) {
        if (suffix == null || suffix.isEmpty()) {
            throw new IllegalArgumentException("Suffix portion must not be null or empty");
        }
        if (Strings.isEmpty(group)) {
            throw new IllegalArgumentException("Group identifier must not be null or empty");
        }
        return newCacheKey0(group, suffix);
    }

    /**
     * Creates a new fully-qualifying cache key for given arguments.
     *
     * @param optGroup The group identifier or <code>null</code>
     * @param suffix The suffix portion
     * @return The fully-qualifying cache key
     * @throws IllegalArgumentException If a suffix part is <code>null</code> or its <code>toString()</code> representation is empty
     */
    private RedisCacheKey newCacheKey0(String optGroup, SuffixBuilder suffix) {
        RedisCacheKey.Builder builder = RedisCacheKey.builder(options);
        if (optGroup != null) {
            builder.withGroup(optGroup);
        }
        builder.withSuffix(suffix);
        return builder.build();
    }

    @Override
    public CacheKey newGroupKey(String group) {
        if (Strings.isEmpty(group)) {
            throw new IllegalArgumentException("Group identifier must not be null or empty");
        }
        return RedisCacheKey.builder(options).withGroup(group).build();
    }

    @Override
    public SuffixBuilder suffixBuilder(int capacity) {
        return new RedisSuffixBuilder(capacity);
    }

    @Override
    public void mput(Map<CacheKey, V> map) throws OXException {
        if (map == null) {
            throw new IllegalArgumentException("The key-to-value map must not be null");
        }
        if (map.isEmpty()) {
            return;
        }

        int numMappings = map.size();
        if (numMappings == 1) {
            Map.Entry<CacheKey, V> entry = map.entrySet().iterator().next();
            put(entry.getKey(), entry.getValue());
            return;
        }

        try {
            long keyExpiration = CacheOptions.getEffectiveExpirationSecondsFor(options.getExpirationSeconds());
            if (keyExpiration < 0L) {
                // No expiration
                keyExpiration = 0L;
            }
            long memberExpiration = globalCacheOptions.isDisableHashExpiration() ? 0L : keyExpiration;
            if (keyExpiration != memberExpiration) {
                // Different expiration seconds for regular keys and hash fields
                Map<RedisCacheKey, InputStream> keyValues = null;
                Map<RedisCacheKey, InputStream> memberValues = null;
                for (Map.Entry<CacheKey, V> e : map.entrySet()) {
                    nullCheck(e.getValue());
                    CacheKeyType keyType = e.getKey().getType();
                    if (keyType == CacheKeyType.GROUP) {
                        throw createIsGroupKeyException();
                    }
                    RedisCacheKey redisKey = keyCheck(e.getKey());
                    if (keyType == CacheKeyType.MEMBER) {
                        memberValues = returnOrCreateHashMap(memberValues, numMappings);
                        memberValues.put(redisKey, serializeValue(e.getValue(), options));
                    } else {
                        keyValues = returnOrCreateHashMap(keyValues, numMappings);
                        keyValues.put(redisKey, serializeValue(e.getValue(), options));
                    }
                }

                if (keyValues != null) {
                    putIntoRedis(keyValues, keyExpiration);
                }
                if (memberValues != null) {
                    putIntoRedis(memberValues, memberExpiration);
                }
            } else {
                Map<RedisCacheKey, InputStream> values = HashMap.newHashMap(numMappings);
                for (Map.Entry<CacheKey, V> e : map.entrySet()) {
                    nullCheck(e.getValue());
                    CacheKeyType keyType = e.getKey().getType();
                    if (keyType == CacheKeyType.GROUP) {
                        throw createIsGroupKeyException();
                    }
                    values.put(keyCheck(e.getKey()), serializeValue(e.getValue(), options));
                }

                putIntoRedis(values, keyExpiration); // Don't care since disableHashExpiration is false
            }

            if (inMemoryCache != null) {
                for (Map.Entry<CacheKey, V> e : map.entrySet()) {
                    inMemoryCache.put(e.getKey(), e.getValue());
                }
            }
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw CacheExceptionCode.FAILED_MPUT.create(e);
        }
    }

    /**
     * Puts specified values into Redis storage.
     *
     * @param values The key-to-value map
     * @param effectiveExpiration The effective expiration seconds to apply
     * @throws OXException If operation fails
     */
    private void putIntoRedis(Map<RedisCacheKey, InputStream> values, long effectiveExpiration) throws OXException {
        try {
            int multiKeyLimit = globalCacheOptions.getMultiKeyLimit();
            if (multiKeyLimit > 0 && values.size() > multiKeyLimit) {
                // Exceeds multi-key limit
                for (Iterator<List<Map.Entry<RedisCacheKey, InputStream>>> partitions = Iterators.partition(values.entrySet().iterator(), multiKeyLimit); partitions.hasNext();) {
                    nativeCache.mput(partitions.next(), effectiveExpiration, options.isFireCacheEvents());
                }
            } else {
                nativeCache.mput(values, effectiveExpiration, options.isFireCacheEvents());
            }
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw CacheExceptionCode.FAILED_MPUT.create(e);
        }
    }

    @Override
    public boolean put(CacheKey key, V object) throws OXException {
        nullCheck(object);
        RedisCacheKey redisKey = keyCheck(key);
        if (redisKey.getType() == CacheKeyType.GROUP) {
            throw createIsGroupKeyException();
        }

        boolean replaced = putIntoRedis(redisKey, object);
        if (inMemoryCache != null) {
            inMemoryCache.put(redisKey, object);
        }
        return replaced;
    }

    /**
     * Puts specified value into Redis storage.
     *
     * @param redisKey The cache key
     * @param object The value to put
     * @return <code>true</code> if an existent value has been replaced by put; otherwise <code>false</code>
     * @throws OXException If operation fails
     */
    private boolean putIntoRedis(RedisCacheKey redisKey, V object) throws OXException {
        try {
            return nativeCache.put(redisKey, serializeValue(object, options), getEffectiveExpiration(redisKey), options.isFireCacheEvents());
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw CacheExceptionCode.FAILED_PUT.create(e, redisKey.getFQN());
        }
    }

    @Override
    public boolean putIfAbsent(CacheKey key, V object) throws OXException {
        nullCheck(object);
        RedisCacheKey redisKey = keyCheck(key);
        if (redisKey.getType() == CacheKeyType.GROUP) {
            throw createIsGroupKeyException();
        }

        try {
            boolean putIntoRedis = nativeCache.putIfAbsent(redisKey, serializeValue(object, options), getEffectiveExpiration(redisKey));
            if (putIntoRedis && inMemoryCache != null) {
                inMemoryCache.put(redisKey, object);
            }
            return putIntoRedis;
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw CacheExceptionCode.FAILED_PUT.create(e, redisKey.getFQN());
        }
    }

    @Override
    public boolean exists(CacheKey key) throws OXException {
        RedisCacheKey redisKey = keyCheck(key);
        try {
            boolean exists = nativeCache.exists(redisKey);
            if (!exists && inMemoryCache != null) {
                inMemoryCache.invalidate(redisKey);
            }
            return exists;
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw CacheExceptionCode.FAILED_GET.create(e, redisKey.getFQN());
        }
    }

    @Override
    public List<CacheKeyValue<V>> mget(Collection<? extends CacheKey> keys) throws OXException {
        if (keys == null || keys.isEmpty()) {
            return Collections.emptyList();
        }

        return mget(keys.stream().map(k -> keyCheck(k)).filter(PREDICATE_NO_GROUP_KEY).collect(CollectorUtils.toList(keys.size())));
    }

    @Override
    public List<CacheKeyValue<V>> mget(CacheKey... keys) throws OXException {
        if (keys == null || keys.length <= 0) {
            return Collections.emptyList();
        }

        return mget(Arrays.stream(keys).map(k -> keyCheck(k)).filter(PREDICATE_NO_GROUP_KEY).collect(CollectorUtils.toList(keys.length)));
    }

    /**
     * Gets multiple keys from Redis storage.
     *
     * @param redisKeys The keys to get
     * @return The key-value-pairs
     * @throws OXException If operation fails
     */
    private List<CacheKeyValue<V>> mget(List<RedisCacheKey> redisKeys) throws OXException {
        if (inMemoryCache == null) {
            return getMultipleFromRedis(redisKeys);
        }


        int numKeys = redisKeys.size();
        Map<CacheKey, CacheKeyValue<V>> key2value = HashMap.newHashMap(numKeys);
        List<RedisCacheKey> toLoad = null;
        for (RedisCacheKey redisKey : redisKeys) {
            V object = fetchFromInMemoryCache(redisKey, null);
            if (object != null) {
                key2value.put(redisKey, RedisCacheKeyValue.just(redisKey, object));
            } else {
                toLoad = addOrCreate(redisKey, toLoad, numKeys);
            }
        }

        if (toLoad != null) {
            List<CacheKeyValue<V>> keyValues = getMultipleFromRedis(toLoad);
            toLoad = null; // NOSONARLINT Might help GC
            for (CacheKeyValue<V> keyValue : keyValues) {
                RedisCacheKey redisKey = keyCheck(keyValue.getKey());
                key2value.put(redisKey, keyValue);

                V object = keyValue.getValueOrElse(null);
                if (object == null) {
                    inMemoryCache.invalidate(redisKey);
                } else {
                    inMemoryCache.put(redisKey, object);
                }
            }
        }

        List<CacheKeyValue<V>> retval = new ArrayList<>(numKeys);
        for (RedisCacheKey redisKey : redisKeys) { // NOSONARLINT
            CacheKeyValue<V> cacheKeyValue = key2value.get(redisKey);
            retval.add(cacheKeyValue == null ? RedisCacheKeyValue.empty(redisKey) : cacheKeyValue);
        }
        key2value = null; // NOSONARLINT Might help GC
        return retval;
    }

    /**
     * <b>Only internally invoked</b>
     * <p>
     * Gets the values associated with specified keys from Redis storage.
     *
     * @param redisKeys The cache keys
     * @return The cache objects; individual values are empty if there is no object bound to the appropriate key
     * @throws OXException If the mget operation fails
     */
    public List<CacheKeyValue<V>> getMultipleFromRedis(List<RedisCacheKey> redisKeys) throws OXException {
        try {
            List<CacheKeyValue<V>> values = new ArrayList<>(redisKeys.size());
            int multiKeyLimit = globalCacheOptions.getMultiKeyLimit();
            if (multiKeyLimit > 0 && redisKeys.size() > multiKeyLimit) {
                // Exceeds multi-key limit
                for (List<RedisCacheKey> partition : Lists.partition(redisKeys, multiKeyLimit)) {
                    getChunkFromRedis(partition, values);
                }
            } else {
                getChunkFromRedis(redisKeys, values);
            }
            return values;
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw CacheExceptionCode.FAILED_MGET.create(e);
        }
    }

    /**
     * Gets the values associated with specified keys from Redis storage.
     *
     * @param redisKeys The cache keys
     * @param values The list to add retrieved values to
     * @throws OXException If retrieval fails
     */
    private void getChunkFromRedis(List<RedisCacheKey> redisKeys, List<CacheKeyValue<V>> values) throws OXException {
        for (CacheKeyValue<InputStream> keyValue : nativeCache.mget(redisKeys)) {
            InputStream data = keyValue.getValueOrElse(null);
            V object = data == null ? null : deserializeSafe(data, options);
            values.add(RedisCacheKeyValue.fromNullable((RedisCacheKey) keyValue.getKey(), object));
        }
    }

    @Override
    public V get(CacheKey key) throws OXException {
        RedisCacheKey redisKey = keyCheck(key);
        if (redisKey.getType() == CacheKeyType.GROUP) {
            throw createIsGroupKeyException();
        }

        if (inMemoryCache == null) {
            return getFromRedis(redisKey);
        }

        V object = fetchFromInMemoryCache(redisKey, null);
        if (object != null) {
            return object;
        }

        object = getFromRedis(redisKey);
        if (object == null) {
            inMemoryCache.invalidate(redisKey);
        } else {
            inMemoryCache.put(redisKey, object);
        }
        return object;
    }

    /**
     * <b>Only internally invoked</b>
     * <p>
     * Gets the value associated with specified key from Redis storage.
     *
     * @param redisKey The cache key
     * @return The value or <code>null</code>
     * @throws OXException If the get operation fails
     */
    public V getFromRedis(RedisCacheKey redisKey) throws OXException {
        try {
            AccessControl optAccessControl = globalCacheOptions.isOrchestrateConcurrentRetrieval() ? getAccessControlFor(redisKey, false) : null;
            if (optAccessControl == null) {
                InputStream data = nativeCache.get(redisKey);
                return data == null ? null : deserializeSafe(data, options);
            }

            // AccessControl available...
            boolean acquired = false; // NOSONARLINT
            try {
                // Acquire exclusive access
                boolean waited = false;
                acquired = optAccessControl.tryAcquireGrant();
                if (!acquired) {
                    acquired = optAccessControl.tryAcquireGrant(5, TimeUnit.SECONDS);
                    if (acquired) {
                        waited = true;
                    } else {
                        // Time elapsed...
                        throw CacheExceptionCode.TIMEOUT.create();
                    }
                }

                // Check for previous concurrent attempt from another thread in case current thread had to wait
                if (waited && optAccessControl.valueSupported()) {
                    @SuppressWarnings("unchecked")
                    ImmutableReference<V> prev = ((ImmutableReference<V>) optAccessControl.getValue());
                    if (prev != null) {
                        // Fetch attempt already concurrently performed by another thread
                        V loadedByAnotherThread = prev.getValue();
                        return loadedByAnotherThread == null ? null : cloneIfPossible(loadedByAnotherThread);
                    }
                }

                // Fetch from cache
                InputStream data = nativeCache.get(redisKey);
                V object = data == null ? null : deserializeSafe(data, options);
                if (optAccessControl.valueSupported()) {
                    optAccessControl.setValue(new ImmutableReference<V>(object));
                }
                return object;
            } finally {
                AccessControls.release(optAccessControl, acquired);
            }
        } catch (OXException e) {
            throw e;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw OXException.general("Interrupted", e);
        } catch (Exception e) {
            throw CacheExceptionCode.FAILED_GET.create(e, redisKey.getFQN());
        }
    }

    @Override
    public V get(CacheKey key, CacheLoader<V> loader) throws OXException {
        if (loader == null) {
            throw new IllegalArgumentException("Loader must not be null");
        }

        RedisCacheKey redisKey = keyCheck(key);
        if (redisKey.getType() == CacheKeyType.GROUP) {
            throw createIsGroupKeyException();
        }

        if (inMemoryCache == null) {
            return getFromRedis(redisKey, loader);
        }

        V object = fetchFromInMemoryCache(redisKey, loader);
        if (object != null) {
            return object;
        }

        object = getFromRedis(redisKey, loader);
        if (object == null) {
            inMemoryCache.invalidate(redisKey);
        } else {
            inMemoryCache.put(redisKey, object);
        }
        return object;
    }

    /**
     * <b>Only internally invoked</b>
     * <p>
     * Retrieves an object from the Redis storage that is associated with the specified key, obtaining that value from <code>loader</code>
     * if necessary. If loader yields <code>null</code>, then <code>null</code> is returned as well and nothing is put into cache.
     *
     * @param redisKey The cache key
     * @param loader The loader to yield the appropriate value, which is then put into cache for future access
     * @return The (possibly loaded) value
     * @throws OXException If operation fails
     */
    public V getFromRedis(RedisCacheKey redisKey, CacheLoader<V> loader) throws OXException {
        try {
            AccessControl accessControl = globalCacheOptions.isOrchestrateConcurrentRetrieval() ? getAccessControlFor(redisKey, true) : null;
            if (accessControl == null) {
                // Check for such a value in Redis storage
                InputStream dataFromCache = nativeCache.get(redisKey);
                V valueFromCache = dataFromCache == null ? null : deserializeSafe(dataFromCache, options);
                if (valueFromCache != null) {
                    return valueFromCache;
                }

                // No such key-value-pair in cache. Need to load...
                V loaded = loadValue(redisKey, loader);
                if (null == loaded) {
                    return null;
                }

                // ... and put into cache (if absent)
                long expirationSecs = getEffectiveExpiration(redisKey);
                do {
                    if (nativeCache.putIfAbsent(redisKey, serializeValue(loaded, options), expirationSecs)) {
                        // Successfully put into cache
                        if (loader instanceof SignalingCacheLoader signalingLoader) { // NOSONARLINT
                            signalingLoader.setValuePutToCache(true);
                        }
                        return loaded;
                    }

                    // Re-fetch from Redis storage since "put if absent" failed
                    dataFromCache = nativeCache.get(redisKey);
                    valueFromCache = dataFromCache == null ? null : deserializeSafe(dataFromCache, options);
                } while (valueFromCache == null);

                return valueFromCache;
            }

            // AccessControl available...
            boolean acquired = false;
            try {
                // Acquire exclusive access
                boolean waited = false;
                acquired = accessControl.tryAcquireGrant();
                if (!acquired) {
                    acquired = accessControl.tryAcquireGrant(5, TimeUnit.SECONDS);
                    if (acquired) {
                        waited = true;
                    } else {
                        // Time elapsed...
                        throw CacheExceptionCode.TIMEOUT.create();
                    }
                }

                // Check for previous concurrent attempt from another thread in case current thread had to wait
                if (waited && accessControl.valueSupported()) {
                    @SuppressWarnings("unchecked")
                    ImmutableReference<V> prev = ((ImmutableReference<V>) accessControl.getValue());
                    if (prev != null) {
                        // Fetch attempt already concurrently performed by another thread
                        V loadedByAnotherThread = prev.getValue();
                        if (loadedByAnotherThread != null) {
                            return cloneIfPossible(loadedByAnotherThread);
                        }
                    }
                }

                // Check for such a value in Redis storage
                InputStream dataFromCache = nativeCache.get(redisKey);
                V valueFromCache = dataFromCache == null ? null : deserializeSafe(dataFromCache, options);
                if (valueFromCache != null) {
                    if (accessControl.valueSupported()) {
                        accessControl.setValue(new ImmutableReference<V>(valueFromCache));
                    }
                    return valueFromCache;
                }

                // No such key-value-pair in cache. Need to load...
                V loaded = loadValue(redisKey, loader);
                if (null == loaded) {
                    return null;
                }

                // ... and put into cache (if absent)
                long expirationSecs = getEffectiveExpiration(redisKey);
                do {
                    if (nativeCache.putIfAbsent(redisKey, serializeValue(loaded, options), expirationSecs)) {
                        // Successfully put into cache
                        if (loader instanceof SignalingCacheLoader signalingLoader) { // NOSONARLINT
                            signalingLoader.setValuePutToCache(true);
                        }
                        if (accessControl.valueSupported()) {
                            accessControl.setValue(new ImmutableReference<V>(loaded));
                        }
                        return loaded;
                    }

                    // Re-fetch from Redis storage since "put if absent" failed
                    dataFromCache = nativeCache.get(redisKey);
                    valueFromCache = dataFromCache == null ? null : deserializeSafe(dataFromCache, options);
                } while (valueFromCache == null);

                if (accessControl.valueSupported()) {
                    accessControl.setValue(new ImmutableReference<V>(valueFromCache));
                }
                return valueFromCache;
            } finally {
                AccessControls.release(accessControl, acquired);
            }
        } catch (OXException e) {
            throw e;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw OXException.general("Interrupted", e);
        } catch (Exception e) {
            throw CacheExceptionCode.FAILED_GET.create(e, redisKey.getFQN());
        }
    }

    /**
     * Loads the value from given loader.
     *
     * @param <V> The type of the value
     * @param redisKey The cache key to yield the value for
     * @param loader The loader to load from
     * @return The loaded value
     * @throws Exception If loading value fails for any reason
     */
    private static <V> V loadValue(RedisCacheKey redisKey, CacheLoader<V> loader) throws Exception {
        V loaded = loader.load(redisKey);
        if (loaded == null) {
            LoggerHolder.LOG.debug("Specified loader '{}' yielded null value!", loader.getClass().getName());
        }
        return loaded;
    }

    /**
     * Gets the access control for specified cache key.
     *
     * @param redisKey The cache key to get the access control for
     * @param required <code>true</code> to require an access control being returned; otherwise <code>false</code> to return <code>null</code> on absence of {@link LockService lock service}
     * @return The access control or <code>null</code> (if <code>required</code> set to <code>false</code> and {@link LockService lock service} is absent)
     * @throws OXException If access control cannot be returned
     */
    private AccessControl getAccessControlFor(RedisCacheKey redisKey, boolean required) throws OXException {
        LockService optionalService = nativeCache.getServices().getOptionalService(LockService.class);
        if (optionalService != null) {
            return optionalService.getAccessControlFor(redisKey.getFQN(), 1, 1, 1);
        }
        return required ? new ReentrantLockAccessControl(nativeCache.getGlobalCacheLock()) : null;
    }

    @Override
    public boolean invalidate(CacheKey key) throws OXException {
        if (key == null) {
            return false;
        }

        RedisCacheKey redisKey = keyCheck(key);
        if (redisKey.getType() == CacheKeyType.GROUP) {
            throw createIsGroupKeyException();
        }
        boolean invalidated = nativeCache.invalidate(redisKey, options.isFireCacheEvents());
        if (inMemoryCache != null) {
            inMemoryCache.invalidate(redisKey);
        }
        return invalidated;
    }

    @Override
    public void invalidate(List<CacheKey> keys) throws OXException {
        if (keys != null && !keys.isEmpty()) {
            List<RedisCacheKey> redisKeys = keys.stream()
                                           .filter(Predicates.isNotNullPredicate())
                                           .map(Utils::keyCheck)
                                           .collect(CollectorUtils.toList(keys.size()));
            if (redisKeys.stream().anyMatch(redisKey -> (redisKey.getType() == CacheKeyType.GROUP))) {
                throw new IllegalArgumentException("A specified cache key denotes a group, but should be a key or group member key");
            }
            nativeCache.invalidate(redisKeys, options.isFireCacheEvents());
            if (inMemoryCache != null) {
                inMemoryCache.invalidateAll(redisKeys);
            }
        }
    }

    @Override
    public List<? extends CacheKey> invalidateGroup(CacheKey groupKey) throws OXException {
        if (groupKey == null) {
            return Collections.emptyList();
        }
        if (groupKey.getType() != CacheKeyType.GROUP) {
            throw new IllegalArgumentException("Specified cache key does not denote a group");
        }
        List<RedisCacheKey> invalidatedKeys = nativeCache.invalidateGroup(keyCheck(groupKey), options.isFireCacheEvents());
        if (inMemoryCache != null) {
            inMemoryCache.invalidateAll(invalidatedKeys);
        }
        return invalidatedKeys;
    }

    /**
     * Gets the effective expiration seconds to use for the supplied cache key, based on the underlying cache options and type of
     * supplied key along with defined {@link #disableHashExpiration}.
     *
     * @param redisKey The cache key to evaluate the effective expiration for
     * @return The effective expiration in seconds, or <code>0</code> for no expiration
     */
    private long getEffectiveExpiration(RedisCacheKey redisKey) {
        if (globalCacheOptions.isDisableHashExpiration() && CacheKeyType.MEMBER == redisKey.getType()) {
            return 0L;
        }
        long expirationSeconds = CacheOptions.getEffectiveExpirationSecondsFor(options.getExpirationSeconds());
        return expirationSeconds > 0 ? expirationSeconds : 0L;
    }

    /**
     * Performs a <code>null</code> check against the specified object.
     *
     * @param object The object to check
     * @throws IllegalArgumentException If the object is <code>null</code>
     */
    private static <V> void nullCheck(V object) {
        if (object == null) {
            throw new IllegalArgumentException("Value must not be null");
        }
    }

    private static final com.google.common.cache.Cache<Class<?>, IReference<Method>> CLONE_METHODS = CacheBuilder.newBuilder().expireAfterAccess(Duration.ofHours(2)).build();

    /**
     * Clones specified value if possible.
     *
     * @param <V> The type of the value
     * @param valueToClone The value to clone
     * @return The cloned value or given value as-is
     */
    private static <V> V cloneIfPossible(V valueToClone) {
        Class<?> clazz = valueToClone.getClass();
        IReference<Method> cachedMethod = CLONE_METHODS.getIfPresent(clazz);
        if (cachedMethod != null) {
            Method cloneMethod = cachedMethod.getValue();
            return cloneMethod == null ? valueToClone : invokeElse(cloneMethod, valueToClone);
        }

        // Check for int[]
        if (valueToClone instanceof int[]) {
            int[] ints = (int[]) valueToClone;
            int[] newInts = new int[ints.length];
            System.arraycopy(ints, 0, newInts, 0, ints.length);
            return (V) newInts;
        }

        // Check for java.lang.Cloneable
        if (!(valueToClone instanceof java.lang.Cloneable)) {
            // java.lang.Cloneable not implemented
            CLONE_METHODS.put(clazz, ImmutableReference.empty());
            return valueToClone;
        }

        // Implements java.lang.Cloneable
        try {
            // Retrieve clone() method (if any)
            Method method = valueToClone.getClass().getDeclaredMethod("clone");
            if (Modifier.isPublic(method.getModifiers())) {
                CLONE_METHODS.put(clazz, new ImmutableReference<>(method));
                return invokeElse(method, valueToClone);
            }

            // Not publicly accessible
            CLONE_METHODS.put(clazz, ImmutableReference.empty());
        } catch (NoSuchMethodException e) {
            // No such clone() method
            CLONE_METHODS.put(clazz, ImmutableReference.empty());
        } catch (Exception e) {
            LoggerHolder.LOG.warn("Failed to fetch clone() method from class {}. Returning value as-is instead.", valueToClone.getClass().getName(), e);
            CLONE_METHODS.put(clazz, ImmutableReference.empty());
        }

        return valueToClone;
    }

    /**
     * Invokes specified clone() method on given value. Returns the value as-is if invocation fails.
     *
     * @param <V> The type of the value
     * @param cloneMethod The clone() method to invoke
     * @param valueToClone The value to clone
     * @return The cloned value or given value as-is
     */
    private static <V> V invokeElse(Method cloneMethod, V valueToClone) {
        try {
            return (V) cloneMethod.invoke(valueToClone);
        } catch (Exception e) {
            LoggerHolder.LOG.warn("Failed to invoke clone() method of class {}. Returning value as-is instead.", valueToClone.getClass().getName(), e);
        }
        return valueToClone;
    }

}
