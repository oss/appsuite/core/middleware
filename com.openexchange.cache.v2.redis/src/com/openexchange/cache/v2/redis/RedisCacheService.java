/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis;

import static com.openexchange.cache.v2.redis.Utils.PREDICATE_NO_GROUP_KEY;
import static com.openexchange.cache.v2.redis.Utils.deserializeSafe;
import java.io.InputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.codec.CacheValueCodec;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.cache.v2.filter.ModuleName;
import com.openexchange.cache.v2.filter.VersionName;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;
import com.openexchange.cache.v2.redis.events.CacheEventChannelImpl;
import com.openexchange.cache.v2.redis.inmemory.InMemoryCache;
import com.openexchange.cache.v2.redis.inmemory.InMemoryCacheFactory;
import com.openexchange.cache.v2.redis.inmemory.InMemoryCacheOptions;
import com.openexchange.cache.v2.redis.key.RedisCacheKey;
import com.openexchange.cache.v2.redis.key.RedisCacheKeyUtil;
import com.openexchange.cache.v2.redis.osgi.Services;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.redis.RedisConnector;
import com.openexchange.server.ServiceLookup;
import com.openexchange.threadpool.AbstractTask;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.threadpool.behavior.CallerRunsBehavior;

/**
 * {@link RedisCacheService} - The Redis cache service.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RedisCacheService implements InvalidationCacheService {

    private final com.google.common.cache.Cache<CacheOptions<?>, RedisCache<?>> optViews;
    private final NativeRedisCache nativeCache;
    private final InMemoryCacheOptions inMemoryCacheOptions;
    private final GlobalRedisCacheOptions globalCacheOptions;

    /**
     * Initializes a new {@link RedisCacheService}.
     *
     * @param connector The Redis connector
     * @param cacheEventChannel The cache event channel
     * @param cacheInstances Whether instances of <code>RedisCache</code> shall be cached or not
     * @param withMetrics Whether to track metrics or not
     * @param globalCacheOptions The global options for Redis-backed cache
     * @param inMemoryCacheOptions The options for in-memory cache
     * @param services The service look-up
     */
    public RedisCacheService(RedisConnector connector, CacheEventChannelImpl cacheEventChannel, boolean cacheInstances, boolean withMetrics, GlobalRedisCacheOptions globalCacheOptions, InMemoryCacheOptions inMemoryCacheOptions, ServiceLookup services) {
        super();
        this.optViews = cacheInstances ? CacheBuilder.newBuilder().expireAfterAccess(Duration.ofMinutes(10)).build() : null;
        this.nativeCache = new NativeRedisCache(cacheEventChannel, connector, withMetrics, services);
        this.inMemoryCacheOptions = inMemoryCacheOptions;
        this.globalCacheOptions = globalCacheOptions;
    }

    /**
     * Gets the native Redis cache.
     *
     * @return The native Redis cache
     */
    public NativeRedisCache getNativeCache() {
        return nativeCache;
    }

    /**
     * Stops this service.
     */
    public void stop() {
        // Maybe future shut-down things to come here
    }

    @SuppressWarnings("unchecked")
    @Override
    public <V> RedisCache<V> getCache(CacheOptions<V> options) {
        if (options == null) {
            throw new IllegalArgumentException("Options must not be null");
        }

        if (optViews == null) {
            // No caching enabled
            return newRedisCacheView(options);
        }

        // Try acquire from cache
        RedisCache<V> redisCacheView = (RedisCache<V>) optViews.getIfPresent(options);
        if (redisCacheView != null) {
            return redisCacheView;
        }

        // Acquire using "if cached, return; otherwise create, cache and return"
        try {
            return (RedisCache<V>) optViews.get(options, () -> newRedisCacheView(options));
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof RuntimeException) {
                throw (RuntimeException) cause;
            }
            throw new IllegalStateException("Failed retrieving cache", cause == null ? e : cause);
        }
    }

    @Override
    public Map<CacheKey, CacheKeyValue<Object>> mget(Map<CacheKey, CacheValueCodec<?>> keysAndCodecs) throws OXException {
        if (keysAndCodecs == null) {
            return Map.of();
        }

        int numMappings = keysAndCodecs.size();
        if (numMappings <= 0) {
            return Map.of();
        }

        // The mapping to fill with fetched values from Redis storage
        Map<CacheKey, CacheKeyValue<Object>> valuesPerKey = HashMap.newHashMap(numMappings);

        // Determine the keys to fetch
        List<RedisCacheKey> redisKeys = keysAndCodecs.keySet().stream().map(Utils::keyCheck).filter(PREDICATE_NO_GROUP_KEY).collect(CollectorUtils.toList(numMappings));

        // Fetch either by batch or completely
        int multiKeyLimit = globalCacheOptions.getMultiKeyLimit();
        if (multiKeyLimit > 0 && redisKeys.size() > multiKeyLimit) {
            List<List<RedisCacheKey>> chunks = Lists.partition(redisKeys, multiKeyLimit);
            for (List<RedisCacheKey> chunk : chunks) {
                getChunkFromRedis(chunk, keysAndCodecs, valuesPerKey);
            }
        } else {
            getChunkFromRedis(redisKeys, keysAndCodecs, valuesPerKey);
        }
        return valuesPerKey;
    }

    /**
     * Retrieves given chunk of Redis keys from Redis storage.
     *
     * @param redisKeys The Reis keys to retrieve
     * @param keysAndCodecs The mapping of keys to codecs to know what codec to apply
     * @param valuesPerKey The mapping to fill with loaded values
     * @throws OXException If retrieval fails
     */
    private void getChunkFromRedis(List<RedisCacheKey> redisKeys, Map<CacheKey, CacheValueCodec<?>> keysAndCodecs, Map<CacheKey, CacheKeyValue<Object>> valuesPerKey) throws OXException {
        for (CacheKeyValue<InputStream> keyValue : nativeCache.mget(redisKeys)) {
            CacheKey key = keyValue.getKey();
            InputStream data = keyValue.getValueOrElse(null);
            valuesPerKey.put(key, RedisCacheKeyValue.fromNullable(key, data == null ? null : deserializeSafe(data, keysAndCodecs.get(key))));
        }
    }

    private <V> RedisCache<V> newRedisCacheView(CacheOptions<V> options) {
        try {
            return new RedisCache<>(options, nativeCache, globalCacheOptions, inMemoryCacheOptions);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new IllegalStateException("Failed retrieving cache", e);
        }
    }

    @Override
    public void invalidateGroup(ModuleName module, VersionName version, String group, boolean fireCacheEvents) throws OXException {
        invalidateGroupAndReturnKeys(module, version, group, fireCacheEvents);
    }

    /**
     * Invalidates specified key's group.
     *
     * @param module The module of the cache group
     * @param version The version of the cache group
     * @param group The group identifier
     * @param fireCacheEvents <code>true</code> to fire cache events; otherwise <code>false</code>
     * @return The keys that were invalidated
     * @throws OXException If invalidation fails
     */
    public List<? extends CacheKey> invalidateGroupAndReturnKeys(ModuleName module, VersionName version, String group, boolean fireCacheEvents) throws OXException {
        RedisCacheKey key = RedisCacheKey.builder().withModuleName(module).withVersionName(version).withGroup(group).build();
        List<RedisCacheKey> invalidatedKeys = nativeCache.invalidateGroup(key, fireCacheEvents);
        if (inMemoryCacheOptions.isEnabled()) {
            InMemoryCacheFactory.getInstance().getInMemoryCache(module, version).ifPresent(new SubmitInvalidationConsumer(invalidatedKeys));
        }
        return invalidatedKeys;
    }

    @Override
    public void invalidateGroupMember(ModuleName module, VersionName version, String group, boolean fireCacheEvents, Object... suffix) throws OXException {
        invalidateGroupMemberAndReturnKey(module, version, group, fireCacheEvents, suffix);
    }

    /**
     * Invalidates specified key's group member.
     *
     * @param module The module of the cache group
     * @param version The version of the cache group
     * @param group The group identifier
     * @param fireCacheEvents <code>true</code> to fire cache events; otherwise <code>false</code>
     * @return The key that was invalidated or <code>null</code> if no such group member was invalidated
     * @throws OXException If invalidation fails
     */
    public RedisCacheKey invalidateGroupMemberAndReturnKey(ModuleName module, VersionName version, String group, boolean fireCacheEvents, Object... suffix) throws OXException {
        RedisCacheKey.Builder keyBuilder = RedisCacheKey.builder().withModuleName(module).withVersionName(version).withGroup(group);
        for (Object suffixPart : suffix) {
            keyBuilder.addSuffix(suffixPart == null ? null : suffixPart.toString());
        }
        RedisCacheKey key = keyBuilder.build();
        boolean invalidated = nativeCache.invalidate(key, fireCacheEvents);
        return invalidated ? key : null;
    }

    @Override
    public void invalidate(CacheFilter filter, boolean fireCacheEvents) throws OXException {
        invalidateAndReturnKeys(filter, fireCacheEvents);
    }

    @Override
    public void invalidate(Collection<CacheFilter> filters, boolean fireCacheEvents) throws OXException {
        invalidateAndReturnKeys(filters, fireCacheEvents);
    }

    private static final String FILTER_EXPRESSION_MATCH_ALL = CacheFilter.matchAll().getFilterExpression();

    /**
     * Invalidates all keys which apply to the given filters.
     *
     * @param cacheFilters The filters to apply
     * @param fireCacheEvents <code>true</code> to fire cache events; otherwise <code>false</code>
     * @return The keys that were invalidated
     * @throws OXException If invalidation fails
     */
    public List<RedisCacheKey> invalidateAndReturnKeys(Collection<CacheFilter> cacheFilters, boolean fireCacheEvents) throws OXException {
        if (cacheFilters == null || cacheFilters.isEmpty()) {
            return Collections.emptyList();
        }

        // Fold filters
        Collection<CacheFilter> filters = fold(cacheFilters);

        // Check for special match-all filter
        if (MATCH_ALL == filters) {
            // Invalidate 'em all
            List<RedisCacheKey> invalidatedKeys = nativeCache.massInvalidation(FILTER_EXPRESSION_MATCH_ALL, true, fireCacheEvents).invalidatedKeys;
            if (inMemoryCacheOptions.isEnabled()) {
                invalidateKeysFromInMemoryCaches(invalidatedKeys);
            }
            return invalidatedKeys;
        }

        // Invalidate matching keys
        List<RedisCacheKey> invalidatedKeys = invalidateCollectingKeys(filters, fireCacheEvents);
        if (inMemoryCacheOptions.isEnabled()) {
            invalidateKeysFromInMemoryCaches(invalidatedKeys);
        }
        return invalidatedKeys;
    }

    /**
     * Invalidates all keys which apply to the given filters.
     *
     * @param filters The filters to apply
     * @param fireCacheEvents Whether to issue cache events for invalidated keys
     * @return The keys that were invalidated
     * @throws OXException If invalidation fails
     */
    private List<RedisCacheKey> invalidateCollectingKeys(Collection<CacheFilter> filters, boolean fireCacheEvents) throws OXException {
        List<RedisCacheKey> invalidatedKeys = null;
        for (CacheFilter filter : filters) {
            String filterExpression = filter.getFilterExpression();
            if (filterExpression.endsWith(CacheFilter.WILDCARD_ENDING)) {
                // A wild-card expression
                List<RedisCacheKey> keys = nativeCache.massInvalidation(filterExpression, true, fireCacheEvents).invalidatedKeys;
                if (!keys.isEmpty()) {
                    invalidatedKeys = createIfNullAndAdd(invalidatedKeys, keys);
                }
            } else {
                // A fully-qualifying key
                RedisCacheKey key = RedisCacheKeyUtil.parseKey(filterExpression);
                if (nativeCache.invalidate(key, fireCacheEvents)) {
                    // Such a key has been invalidated
                    invalidatedKeys = createIfNullAndAdd(invalidatedKeys, key);
                }
            }
        }
        return invalidatedKeys == null ? Collections.emptyList() : invalidatedKeys;
    }

    /**
     * Invalidates all keys which apply to the given filter.
     *
     * @param cacheFilter The filter to apply
     * @param fireCacheEvents <code>true</code> to fire cache events; otherwise <code>false</code>
     * @return The keys that were invalidated
     * @throws OXException If invalidation fails
     */
    public List<RedisCacheKey> invalidateAndReturnKeys(CacheFilter cacheFilter, boolean fireCacheEvents) throws OXException {
        if (cacheFilter == null) {
            throw new IllegalArgumentException("Filter must not be null");
        }

        String filterExpression = cacheFilter.getFilterExpression();
        if (FILTER_EXPRESSION_MATCH_ALL.equals(filterExpression)) {
            // Invalidate 'em all
            List<RedisCacheKey> invalidatedKeys = nativeCache.massInvalidation(filterExpression, true, fireCacheEvents).invalidatedKeys;
            if (inMemoryCacheOptions.isEnabled()) {
                invalidateKeysFromInMemoryCaches(invalidatedKeys);
            }
            return invalidatedKeys;
        }

        if (filterExpression.endsWith(CacheFilter.WILDCARD_ENDING)) {
            // A wild-card expression
            List<RedisCacheKey> invalidatedKeys = nativeCache.massInvalidation(filterExpression, true, fireCacheEvents).invalidatedKeys;
            if (inMemoryCacheOptions.isEnabled()) {
                invalidateKeysFromInMemoryCaches(invalidatedKeys);
            }
            return invalidatedKeys;
        }

        // A fully-qualifying key
        RedisCacheKey key = RedisCacheKeyUtil.parseKey(filterExpression);
        boolean invalidated = nativeCache.invalidate(key, fireCacheEvents);
        if (invalidated && inMemoryCacheOptions.isEnabled()) {
            InMemoryCacheFactory.getInstance().getInMemoryCache(key.getModule(), key.getVersion()).ifPresent(new SubmitInvalidationConsumer(key));
        }
        return invalidated ? Collections.singletonList(key) : Collections.emptyList();
    }

    /**
     * (Asynchronously) Invalidates given keys from all in-memory caches.
     *
     * @param invalidatedKeys The keys to invalidate
     */
    private static void invalidateKeysFromInMemoryCaches(List<RedisCacheKey> invalidatedKeys) {
        ThreadPoolService threadPool = Services.optService(ThreadPoolService.class);
        if (threadPool != null) {
            // Submit to thread pool
            threadPool.submit(new InvalidateKeysForEachInMemoryCache(invalidatedKeys), CallerRunsBehavior.getInstance());
        } else {
            // Perform with running thread
            InMemoryCacheFactory.getInstance().getInMemoryCaches().forEach(new SubmitInvalidationConsumer(invalidatedKeys));
        }
    }

    /**
     * Simple task to invalidate specified collection of Redis cache keys for each in-memory cache.
     */
    private static class InvalidateKeysForEachInMemoryCache extends AbstractTask<Void> {

        private final Consumer<InMemoryCache<?>> invalidator;

        /**
         * Initializes a new {@link InvalidateKeys}.
         *
         * @param redisKeys The Redis cache keys to invalidate
         */
        InvalidateKeysForEachInMemoryCache(Collection<? extends CacheKey> redisKeys) {
            super();
            this.invalidator = inMemoryCache -> inMemoryCache.invalidateAll(redisKeys);
        }

        @Override
        public Void call() throws Exception {
            InMemoryCacheFactory.getInstance().getInMemoryCaches().forEach(invalidator);
            return null;
        }
    }

    /**
     * The consumer submitting invalidation of certain keys to an in-memory cache.
     */
    private static class SubmitInvalidationConsumer implements Consumer<InMemoryCache<?>> {

        /** The keys to invalidate */
        private final Collection<? extends CacheKey> invalidatedKeys;

        /**
         * Initializes a new instance of {@link ConsumerImplementation}.
         *
         * @param invalidatedKey The key to invalidate
         */
        SubmitInvalidationConsumer(RedisCacheKey invalidatedKey) {
            super();
            this.invalidatedKeys = Collections.singletonList(invalidatedKey);
        }

        /**
         * Initializes a new instance of {@link ConsumerImplementation}.
         *
         * @param invalidatedKeys The keys to invalidate
         */
        SubmitInvalidationConsumer(Collection<? extends CacheKey> invalidatedKeys) {
            super();
            this.invalidatedKeys = invalidatedKeys; // NOSONARLINT
        }

        @Override
        public void accept(InMemoryCache<?> inMemoryCache) {
            inMemoryCache.submitInvalidationElseRun(invalidatedKeys);
        }
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates the list to add if <code>null</code> and adds specified element.
     *
     * @param <E> The element type
     * @param list The list to add to (may be <code>null</code>)
     * @param element The element to add
     * @return The list to which the element has been added
     */
    private static <E> List<E> createIfNullAndAdd(List<E> list, E element) {
        List<E> l = list;
        if (l == null) {
            l = new ArrayList<>();
        }
        l.add(element);
        return l;
    }

    /**
     * Creates the list to add if <code>null</code> and adds specified elements.
     *
     * @param <E> The element type
     * @param list The list to add to (may be <code>null</code>)
     * @param elements The elements to add
     * @return The list to which the elements has been added
     */
    private static <E> List<E> createIfNullAndAdd(List<E> list, Collection<E> elements) {
        List<E> l = list;
        if (l == null) {
            l = new ArrayList<>(elements);
        } else {
            l.addAll(elements);
        }
        return l;
    }

    /** The special constant to invalidate every cache key */
    private static final Collection<CacheFilter> MATCH_ALL = Collections.singletonList(CacheFilter.matchAll());

    /**
     * Folds the collection of cache filters.
     *
     * @param filters The filters to fold
     * @return The folded filters or special {@link #MATCH_ALL} constant to invalidate every cache key
     * @throws IllegalArgumentException If a cache filter is <code>null</code> in specified collection
     */
    private static Collection<CacheFilter> fold(Collection<CacheFilter> filters) {
        if (filters.size() <= 1) {
            return foldSingleton(filters);
        }

        CacheFilterMaps maps = buildCacheFilterMaps(filters);
        if (maps == null) {
            return MATCH_ALL;
        }

        Map<String, CacheFilter> matchingPrefix = maps.matchingPrefix;
        if (matchingPrefix == null) {
            Map<String, CacheFilter> matchingKey = maps.matchingKey;
            return matchingKey == null ? Collections.emptyList() : matchingKey.values();
        }

        // Use shortest wild-card expression
        foldPrefixes(matchingPrefix);

        Map<String, CacheFilter> matchingKey = maps.matchingKey;
        if (matchingKey == null) {
            return matchingPrefix.values();
        }

        // Drop all keys covered by a wild-card expression
        dropCoveredKeys(matchingKey, matchingPrefix);

        List<CacheFilter> retval = new ArrayList<>(matchingPrefix.values());
        retval.addAll(matchingKey.values());
        return retval;
    }

    /**
     * Folds wild-card cache filters;<br>
     * e.g. prefer <code>"ox-cache:users:v1:*"</code> over <code>"ox-cache:users:v1:1337:*"</code>.
     *
     * @param matchingPrefix The map of wild-card expressions
     */
    private static void foldPrefixes(Map<String, CacheFilter> matchingPrefix) {
        dropCoveredKeys(matchingPrefix, matchingPrefix);
    }

    /**
     * Drops those keys or wild-card expressions that are already covered by a (shorter) wild-card expression;<br>
     * e.g. drop <code>"ox-cache:users:v1:1337:3"</code> when there <code>"ox-cache:users:v1:1337:*"</code> or <br>
     * e.g. drop <code>"ox-cache:users:v1:1337:*"</code> when there <code>"ox-cache:users:v1:*"</code>.
     *
     * @param matchingKey The map of cache filters to check for a possible wild-card expression
     * @param matchingPrefix The map of wild-card expressions
     */
    private static void dropCoveredKeys(Map<String, CacheFilter> matchingKey, Map<String, CacheFilter> matchingPrefix) {
        for (Iterator<String> keyIter = matchingKey.keySet().iterator(); keyIter.hasNext();) {
            String key = keyIter.next();
            boolean covered = false;
            for (Iterator<String> inner = matchingPrefix.keySet().iterator(); !covered && inner.hasNext();) {
                String prefix = inner.next();
                if (key.length() > prefix.length() && key.startsWith(prefix)) {
                    // Drop the concrete cache key since covered by a wild-card expression
                    covered = true;
                }
            }
            if (covered) {
                keyIter.remove();
            }
        }
    }

    /**
     * "Folds" given singleton collection.
     *
     * @param filters The singleton collection
     * @return The singleton collection or special {@link #MATCH_ALL} constant to invalidate every cache key
     */
    private static Collection<CacheFilter> foldSingleton(Collection<CacheFilter> filters) {
        for (CacheFilter filter : filters) {
            if (filter == null) {
                throw new IllegalArgumentException("Filter must not be null");
            }

            String filterExpression = filter.getFilterExpression();
            if (CacheFilter.matchAll().getFilterExpression().equals(filterExpression)) {
                return MATCH_ALL;
            }
        }
        return filters;
    }

    /**
     * Builds the cache filter maps.
     *
     * @param filters The cache filters
     * @return The maps or <code>null</code> to signal match-all
     * @throws IllegalArgumentException If a cache filter is <code>null</code> in specified collection
     */
    private static CacheFilterMaps buildCacheFilterMaps(Collection<CacheFilter> filters) {
        Map<String, CacheFilter> matchingPrefix = null;
        Map<String, CacheFilter> matchingKey = null;

        for (CacheFilter filter : filters) {
            if (filter == null) {
                throw new IllegalArgumentException("Filter must not be null");
            }

            String filterExpression = filter.getFilterExpression();
            if (CacheFilter.matchAll().getFilterExpression().equals(filterExpression)) {
                // Signal match-all through null
                return null;
            }

            if (filterExpression.endsWith(CacheFilter.WILDCARD_ENDING)) {
                // Exclude wild-card character for prefix; e.g. "ox-cache:users:v1:1337:"
                String prefix = filterExpression.substring(0, filterExpression.length() - 1);
                if (matchingPrefix == null) {
                    matchingPrefix = new HashMap<>();
                }
                matchingPrefix.put(prefix, filter);
            } else {
                if (matchingKey == null) {
                    matchingKey = new HashMap<>();
                }
                matchingKey.put(filterExpression, filter);
            }
        }

        return new CacheFilterMaps(matchingPrefix, matchingKey);
    }

    /** Helper class for cache filter maps */
    private static class CacheFilterMaps {

        /** The map for wild-card cache filters grouped by prefix */
        final Map<String, CacheFilter> matchingPrefix;

        /** The map for key-based cache filters grouped by key */
        final Map<String, CacheFilter> matchingKey;

        /**
         * Initializes a new instance.
         *
         * @param matchingPrefix The map for wild-card cache filters grouped by prefix
         * @param matchingKey The map for key-based cache filters grouped by key
         */
        CacheFilterMaps(Map<String, CacheFilter> matchingPrefix, Map<String, CacheFilter> matchingKey) {
            super();
            this.matchingPrefix = matchingPrefix;
            this.matchingKey = matchingKey;
        }
    }

}
