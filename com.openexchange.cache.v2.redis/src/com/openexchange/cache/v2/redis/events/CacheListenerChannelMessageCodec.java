/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.events;

import org.json.JSONObject;
import org.json.JSONServices;
import com.openexchange.pubsub.ChannelMessageCodec;

/**
 * {@link CacheListenerChannelMessageCodec} - The channel message codec for cache events.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class CacheListenerChannelMessageCodec implements ChannelMessageCodec<CacheListenerName> { // NOSONARLINT

    private static final CacheListenerChannelMessageCodec INSTANCE = new CacheListenerChannelMessageCodec();

    private static final String NAME = "name";

    /**
     * Gets the instance.
     *
     * @return The instance
     */
    public static CacheListenerChannelMessageCodec getInstance() {
        return INSTANCE;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Initializes a new {@link CacheListenerChannelMessageCodec}.
     */
    private CacheListenerChannelMessageCodec() {
        super();
    }

    @Override
    public String serialize(CacheListenerName message) throws Exception {
        return new JSONObject(2).put(NAME, message.getName()).toString();
    }

    @Override
    public CacheListenerName deserialize(String data) throws Exception {
        return new CacheListenerName(JSONServices.parseObject(data).getString(NAME));
    }

}
