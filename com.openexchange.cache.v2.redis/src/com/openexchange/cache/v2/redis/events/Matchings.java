/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2.redis.events;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.openexchange.cache.v2.events.CacheEventInterest;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.java.Functions;

/**
 * {@link Matchings} - The matches for a registered cache event listener.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
final class Matchings {

    private static final String WILDCARD = "*";

    /**
     * Creates a new builder.
     *
     * @param delimiter The delimiter character
     * @return The newly created builder
     */
    static MatchingsBuilder newBuilder(char delimiter) {
        return new MatchingsBuilder(delimiter);
    }

    /** The builder for an instance of <code>Matchings</code> */
    static final class MatchingsBuilder {

        private final char delimiter;
        private final ImmutableList.Builder<CacheListenerName> matchingAllListeners;
        private final Map<String, List<CacheListenerName>> matchingPrefixListeners;
        private final Map<String, List<CacheListenerName>> matchingKeyListeners;

        private MatchingsBuilder(char delimiter) {
            super();
            this.delimiter = delimiter;
            matchingAllListeners = ImmutableList.builder();
            matchingPrefixListeners = new HashMap<>();
            matchingKeyListeners = new HashMap<>();
        }

        /**
         * Adds specified listener entry.
         *
         * @param name The listener name
         * @param interest The listener's interest
         * @return This builder
         */
        public MatchingsBuilder addListener(CacheListenerName name, CacheEventInterest interest) {
            CacheFilter filter = interest.getFilter();
            if (filter == null) {
                matchingAllListeners.add(name);
                return this;
            }

            String filterExpression = filter.getFilterExpression();
            if (CacheFilter.matchAll().getFilterExpression().equals(filterExpression)) {
                matchingAllListeners.add(name);
                return this;
            }

            if (filterExpression.endsWith(new StringBuilder(2).append(delimiter).append(WILDCARD).toString())) {
                String prefix = filterExpression.substring(0, filterExpression.length() - 2);
                addListener(matchingPrefixListeners, name, prefix);
                return this;
            }

            addListener(matchingKeyListeners, name, filterExpression);
            return this;
        }

        private static void addListener(Map<String, List<CacheListenerName>> listeners, CacheListenerName name, String key) {
            listeners.computeIfAbsent(key, Functions.getNewArrayListFuntion()).add(name);
        }

        /**
         * Builds the resulting instance of <code>Matchings</code>.
         *
         * @return The instance of <code>Matchings</code>
         */
        Matchings build() {
            return new Matchings(matchingAllListeners.build(), ImmutableMap.copyOf(matchingPrefixListeners), ImmutableMap.copyOf(matchingKeyListeners));
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** The listing of listeners that are interested in all cache keys */
    final List<CacheListenerName> matchingAllListeners;

    /** The listing of listeners that are interested in certain cache keys by prefix */
    final Map<String, List<CacheListenerName>> matchingPrefixListeners;

    /** The listing of listeners that are interested in certain concrete cache keys */
    final Map<String, List<CacheListenerName>> matchingKeyListeners;

    /** The flag whether there are any listeners that are interested in certain concrete cache keys */
    final boolean matchingKeyListenersIsEmpty;

    /**
     * Initializes a new {@link Matchings}.
     *
     * @param matchingAllListeners The listing of listeners that are interested in all cache keys
     * @param matchingPrefixListeners The listing of listeners that are interested in certain cache keys by prefix
     * @param matchingKeyListeners The listing of listeners that are interested in certain concrete cache keys
     */
    Matchings(List<CacheListenerName> matchingAllListeners, Map<String, List<CacheListenerName>> matchingPrefixListeners, Map<String, List<CacheListenerName>> matchingKeyListeners) {
        super();
        this.matchingAllListeners = matchingAllListeners;
        this.matchingPrefixListeners = matchingPrefixListeners;
        this.matchingKeyListeners = matchingKeyListeners;
        this.matchingKeyListenersIsEmpty = matchingKeyListeners.isEmpty();
    }

}
