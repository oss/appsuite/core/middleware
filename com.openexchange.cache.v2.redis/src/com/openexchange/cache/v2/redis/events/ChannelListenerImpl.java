/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2.redis.events;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.events.CacheEvent;
import com.openexchange.cache.v2.events.CacheEventInterest;
import com.openexchange.cache.v2.events.CacheEventListener;
import com.openexchange.pubsub.ChannelListener;
import com.openexchange.pubsub.Message;

/**
 * {@link ChannelListenerImpl} - The channel listener wrapping a cache event listener.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
class ChannelListenerImpl implements ChannelListener<CacheEvent> {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ChannelListenerImpl.class); // NOSONARLINT
    }

    private final CacheEventListener listener;

    ChannelListenerImpl(CacheEventListener listener) {
        super();
        this.listener = listener;
    }

    @Override
    public void onMessage(Message<CacheEvent> cacheEventMessage) {
        CacheEvent cacheEventOfinterest = isInterested(cacheEventMessage);
        if (cacheEventOfinterest != null) {
            listener.onCacheEvent(cacheEventOfinterest);
        }
    }

    private CacheEvent isInterested(Message<CacheEvent> cacheEventMessage) {
        CacheEventInterest interest = listener.getInterest();
        if (interest == null) {
            // Listener advertises no interest at all. Assume cache event shall be fed to listener.
            LoggerHolder.LOG.warn("Cache listener \"{}\" advertises no interest at all. Assume any cache event shall be fed to listener.", listener.getClass().getName());
            return cacheEventMessage.getData();
        }

        if (interest.onlyRemote() && !cacheEventMessage.isRemote()) {
            // No remote cache event, but only remote ones are of interest. Discard...
            return null;
        }

        CacheEvent cacheEvent = cacheEventMessage.getData();
        Set<CacheKey> keysOfInterest = getInterestingKeys(interest, cacheEvent.getKeys());

        if (keysOfInterest == null || keysOfInterest.isEmpty()) {
            return null;
        }

        return CacheEvent.create(new ArrayList<>(keysOfInterest));
    }

    static Set<CacheKey> getInterestingKeys(CacheEventInterest interest, Collection<CacheKey> keys) {
        return keys.stream()
                   .filter(key -> interest.getFilter().matches(key))
                   .collect(Collectors.toSet());
    }

}
