/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.events;

import com.openexchange.cache.v2.events.CacheEventInterest;

/**
 * {@link InterestAndName} - A tuple for cache event interest and listener name.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class InterestAndName {

    private final CacheListenerName name;
    private final CacheEventInterest interest;

    /**
     * Initializes a new {@link InterestAndName}.
     *
     * @param name The listener name
     * @param interest The listener's interest
     */
    public InterestAndName(CacheListenerName name, CacheEventInterest interest) {
        super();
        this.name = name;
        this.interest = interest;
    }

    /**
     * Gets the listener name.
     *
     * @return The listener name
     */
    public CacheListenerName getName() {
        return name;
    }

    /**
     * Gets the interest.
     *
     * @return The interest
     */
    public CacheEventInterest getInterest() {
        return interest;
    }

}
