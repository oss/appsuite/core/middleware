/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.events;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONServices;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.events.CacheEvent;
import com.openexchange.cache.v2.redis.key.RedisCacheKeyUtil;
import com.openexchange.pubsub.ChannelMessageCodec;

/**
 * {@link CacheEventChannelMessageCodec} - The channel message codec for cache events.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class CacheEventChannelMessageCodec implements ChannelMessageCodec<CacheEvent> { // NOSONARLINT

    private static final CacheEventChannelMessageCodec INSTANCE = new CacheEventChannelMessageCodec();

    private static final String KEYS = "keys";

    /**
     * Gets the instance.
     *
     * @return The instance
     */
    public static CacheEventChannelMessageCodec getInstance() {
        return INSTANCE;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Initializes a new {@link CacheEventChannelMessageCodec}.
     */
    private CacheEventChannelMessageCodec() {
        super();
    }

    @Override
    public String serialize(CacheEvent message) throws Exception {
        JSONArray jKeys = new JSONArray(message.getKeys().size());
        for (CacheKey key : message.getKeys()) {
            jKeys.put(key.getFQN());
        }
        return new JSONObject(2).put(KEYS, jKeys).toString();
    }

    @Override
    public CacheEvent deserialize(String data) throws Exception {
        JSONArray jKeys = JSONServices.parseObject(data).getJSONArray(KEYS);
        int length = jKeys.length();
        List<CacheKey> keys = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            keys.add(RedisCacheKeyUtil.parseKey(jKeys.getString(i)));
        }
        return CacheEvent.create(keys);
    }

}
