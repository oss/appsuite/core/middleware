/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis.events;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.slf4j.Logger;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.cache.v2.events.CacheEvent;
import com.openexchange.cache.v2.events.CacheEventCollectionId;
import com.openexchange.cache.v2.events.CacheEventListener;
import com.openexchange.cache.v2.events.CacheEventService;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.cache.v2.redis.NativeRedisCache;
import com.openexchange.cache.v2.redis.RedisCache;
import com.openexchange.cache.v2.redis.RedisCacheService;
import com.openexchange.cache.v2.redis.events.Matchings.MatchingsBuilder;
import com.openexchange.cache.v2.redis.key.RedisCacheKey;
import com.openexchange.cache.v2.redis.key.RedisCacheKeyUtil;
import com.openexchange.cache.v2.redis.osgi.Services;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.ChannelKey;
import com.openexchange.pubsub.ChannelListener;
import com.openexchange.pubsub.DefaultChannelKey;
import com.openexchange.pubsub.MessageCollectionId;
import com.openexchange.pubsub.core.CoreChannelApplicationName;
import com.openexchange.pubsub.core.CoreChannelName;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

/**
 * {@link CacheEventChannelImpl} - The cache event channel backed by Pub-Sub channel.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class CacheEventChannelImpl implements CacheEventService {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(CacheEventChannelImpl.class); // NOSONARLINT
    }

    /** The channel key for cache events */
    public static final ChannelKey CACHE_EVENT_CHANNEL_KEY = DefaultChannelKey.builder().withChannelApplicationName(CoreChannelApplicationName.CORE).withChannelName(CoreChannelName.CACHE_EVENTS).build();

    /** The channel key for cache listener registration/unregistration */
    public static final ChannelKey LISTENER_CHANNEL_KEY = DefaultChannelKey.builder().withChannelApplicationName(CoreChannelApplicationName.CORE).withChannelName(CoreChannelName.CACHE_LISTENER).build();

    /** The cache options for cache event listener entries. */
    private static final CacheOptions<InterestAndName> CACHE_EVENT_LISTENERS_CACHE_OPTIONS = CacheOptions.<InterestAndName> builder()
            .withCoreModuleName(CoreModuleName.CACHE_EVENT_LISTENERS)
            .withCodecAndVersion(new CacheEventInterestCodec())
            .withFireCacheEvents(true)
            .build();

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final Channel<CacheEvent> channel;
    private final Channel<CacheListenerName> listenerChannel;
    private final ChannelListener<CacheListenerName> listenerForCacheListenerChanges;
    private final ConcurrentMap<CacheEventListener, ChannelListenerImpl> listeners;
    private final char delimiter;
    private final AtomicReference<Matchings> matchingsReference;
    private final AtomicReference<RedisCacheService> cacheServiceReference;
    private final ScheduledTimerTask listenerCacheRefresher;

    /**
     * Initializes a new {@link CacheEventChannelImpl}.
     *
     * @param channel The pub/sub channel for cache events
     * @param listenerChannel The pub/sub channel for listener changes
     * @throws OXException If initialization fails
     */
    public CacheEventChannelImpl(Channel<CacheEvent> channel, Channel<CacheListenerName> listenerChannel) throws OXException {
        super();
        this.channel = channel;
        this.listenerChannel = listenerChannel;
        this.delimiter = CacheFilter.DELIMITER;
        this.listeners = new ConcurrentHashMap<>();
        AtomicReference<Matchings> matchingsReference = new AtomicReference<>();
        this.matchingsReference = matchingsReference;
        this.cacheServiceReference = new AtomicReference<>(null);
        listenerForCacheListenerChanges = message -> {
            if (message.isRemote()) {
                matchingsReference.set(null);
            }
        };
        listenerChannel.subscribe(listenerForCacheListenerChanges);

        // Initialize timer task to refresh the content of the cache for cache event listeners
        TimerService timerService = Services.getServiceLookup().getServiceSafe(TimerService.class);
        long delaySeconds = CacheOptions.getEffectiveExpirationSecondsFor(CoreModuleName.CACHE_EVENT_LISTENERS.getExpirationSeconds()) >> 1;
        listenerCacheRefresher = timerService.scheduleWithFixedDelay(this::refreshListenerCacheContent, delaySeconds, delaySeconds, TimeUnit.SECONDS);
    }

    /**
     * Refreshes the content of the cache containing the names of current cache event listeners.
     */
    private void refreshListenerCacheContent() {
        try {
            RedisCacheService cacheService = cacheServiceReference.get();
            if (cacheService != null) {
                RedisCache<InterestAndName> cacheView = cacheService.getCache(CACHE_EVENT_LISTENERS_CACHE_OPTIONS);
                Thread currentThread = Thread.currentThread();
                for (CacheEventListener listener : listeners.keySet()) {
                    if (!currentThread.isInterrupted()) {
                        CacheListenerName cacheListenerName = new CacheListenerName(listener.getName());
                        CacheKey key = cacheView.newKey(Strings.asciiLowerCase(listener.getClass().getSimpleName()));
                        cacheView.putIfAbsent(key, new InterestAndName(cacheListenerName, listener.getInterest()));
                    }
                }
            }
        } catch (Exception e) {
            LoggerHolder.LOG.warn("Failed to update listener registry", e);
        }
    }

    /**
     * Stops this cache event channel.
     */
    public void stop() {
        unsubscribeSafe(listenerForCacheListenerChanges, listenerChannel);
        for (ChannelListenerImpl l : listeners.values()) {
            unsubscribeSafe(l, channel);
        }
        listeners.clear();
        listenerCacheRefresher.cancel(true);
        TimerService timerService = Services.getServiceLookup().getOptionalService(TimerService.class);
        if (timerService != null) {
            timerService.purge();
        }
    }

    /**
     * Safely unsubscribes specified listener from given channel.
     *
     * @param <D> The channel message type
     * @param listener The listener to unsubscribe
     * @param channel The channel to unsubscribe from
     */
    private static <D> void unsubscribeSafe(ChannelListener<D> listener, Channel<D> channel) {
        try {
            channel.unsubscribe(listener);
        } catch (Exception e) {
            LoggerHolder.LOG.warn("Failed to unsubscribe listener {}", listener, e);
        }
    }

    /**
     * Gets the matchings.
     *
     * @return The matchings or <code>null</code>
     */
    private Matchings optMatchings() {
        Matchings matchings = matchingsReference.get();
        if (matchings != null) {
            return matchings;
        }

        synchronized (this) {
            matchings = matchingsReference.get();
            if (matchings != null) {
                return matchings;
            }

            matchings = initMatchings();
            if (matchings != null) {
                matchingsReference.set(matchings);
            }
            return matchings;
        }
    }

    /**
     * Initializes the matchings by registered cache event listeners.
     *
     * @return The matchings
     */
    private Matchings initMatchings() {
        RedisCacheService cacheService = cacheServiceReference.get();
        if (cacheService == null) {
            // No cache service available
            return null;
        }

        NativeRedisCache theRedisCache = cacheService.getNativeCache();
        RedisCache<InterestAndName> cache = cacheService.getCache(CACHE_EVENT_LISTENERS_CACHE_OPTIONS);
        try {
            // Get keys for all registered cache event listeners
            CacheFilter cacheFilter = CacheFilter.builder().withModuleName(CoreModuleName.CACHE_EVENT_LISTENERS).build();
            List<RedisCacheKey> keys = getMatchingKeysFor(cacheFilter, theRedisCache);

            // Grab each cache event listener and initialize matchings with it
            MatchingsBuilder builder = Matchings.newBuilder(delimiter);
            for (RedisCacheKey key : keys) {
                InterestAndName interestAndName = cache.get(cache.newKey(key.getSuffix()));
                if (interestAndName != null) {
                    builder.addListener(interestAndName.getName(), interestAndName.getInterest());
                }
            }
            return builder.build();
        } catch (Exception e) {
            // Failed to build matchings
            LoggerHolder.LOG.warn("Failed to build matchings", e);
            return null;
        }
    }

    private static final String FILTER_EXPRESSION_MATCH_ALL = CacheFilter.matchAll().getFilterExpression();

    private static List<RedisCacheKey> getMatchingKeysFor(CacheFilter cacheFilter, NativeRedisCache theRedisCache) throws OXException {
        String filterExpression = cacheFilter.getFilterExpression();
        if (FILTER_EXPRESSION_MATCH_ALL.equals(filterExpression)) {
            // Invalidate 'em all
            return theRedisCache.listKeys(filterExpression);
        }

        if (filterExpression.endsWith(CacheFilter.WILDCARD_ENDING)) {
            // A wild-card expression
            return theRedisCache.listKeys(cacheFilter.getFilterExpression());
        }

        // A fully-qualifying key
        return Collections.singletonList(RedisCacheKeyUtil.parseKey(filterExpression));
    }

    /**
     * Sets the cache service.
     *
     * @param cacheService The cache service to set
     */
    public void setCacheService(RedisCacheService cacheService) {
        cacheServiceReference.set(cacheService);
    }

    @Override
    public void publish(Collection<CacheEvent> cacheEvents) throws OXException {
        channel.publish(cacheEvents);
    }

    @Override
    public void subscribe(CacheEventListener listener) throws OXException {
        ChannelListenerImpl channelListener = new ChannelListenerImpl(listener);
        if (listeners.putIfAbsent(listener, channelListener) != null) {
            // Already added
            return;
        }

        try {
            channel.subscribe(channelListener);
        } catch (Exception e) {
            // Subscription failed
            LoggerHolder.LOG.error("Failed to subscribe cache event listener {}", listener, e);
            listeners.remove(listener);
            throw e;
        }

        CacheListenerName cacheListenerName = new CacheListenerName(listener.getName());
        RedisCacheService cacheService = cacheServiceReference.get();
        if (cacheService != null) {
            RedisCache<InterestAndName> cacheView = cacheService.getCache(CACHE_EVENT_LISTENERS_CACHE_OPTIONS);
            RedisCacheKey key = cacheView.newKey(Strings.asciiLowerCase(listener.getClass().getSimpleName()));
            cacheView.put(key, new InterestAndName(cacheListenerName, listener.getInterest()));
        }

        matchingsReference.set(null);
        listenerChannel.publish(cacheListenerName);
    }

    @Override
    public void unsubscribe(CacheEventListener listener) throws OXException {
        if (listener == null) {
            return;
        }

        ChannelListenerImpl channelListener = listeners.remove(listener);
        if (channelListener == null) {
            // No such listener
            return;
        }

        unsubscribeSafe(channelListener, channel);
    }

    /**
     * Checks if there is a locally available listener that is interested in given key.
     *
     * @param key The key to examine
     * @return <code>true</code> if there is such a listener; otherwise <code>false</code>
     */
    public boolean hasInterestedListenersForKey(CacheKey key) {
        Matchings matchings = optMatchings();
        if (matchings == null) {
            // Don't know better
            return true;
        }

        if (!matchings.matchingAllListeners.isEmpty()) {
            return true;
        }

        return hasInterestedListenersForSingleKey(key, matchings);
    }

    /**
     * Checks if there is a locally available listener that is interested in any of the given keys.
     *
     * @param keys The keys to examine
     * @return <code>true</code> if there is such a listener; otherwise <code>false</code>
     */
    public boolean hasInterestedListenersForKeys(Iterable<CacheKey> keys) {
        Matchings matchings = optMatchings();
        if (matchings == null) {
            // Don't know better
            return true;
        }

        if (!matchings.matchingAllListeners.isEmpty()) {
            return true;
        }

        for (CacheKey key : keys) {
            if (hasInterestedListenersForSingleKey(key, matchings)) {
                return true;
            }
        }

        return false;
    }

    private boolean hasInterestedListenersForSingleKey(CacheKey key, Matchings matchings) {
        String fqn = key.getFQN();

        // Check for exact match
        List<CacheListenerName> matchingKeyQueue = matchings.matchingKeyListenersIsEmpty ? null : matchings.matchingKeyListeners.get(fqn);
        if (matchingKeyQueue != null && !matchingKeyQueue.isEmpty()) {
            return true;
        }

        // Check for prefixed match
        int pos = fqn.lastIndexOf(delimiter);
        while (pos > 0) {
            String prefix = fqn.substring(0, pos);
            List<CacheListenerName> queue = matchings.matchingPrefixListeners.get(prefix);
            if (queue != null && !queue.isEmpty()) {
                return true;
            }
            pos = prefix.lastIndexOf(delimiter);
        }

        return false;
    }

    // ------------------------------------------------------ Event collections -----------------------------------------------------------

    @Override
    public CacheEventCollectionId startCacheEventCollection(CacheEvent optionalInitialCacheEvent) throws OXException {
        return new CacheEventCollectionId(channel.startMessageCollection(optionalInitialCacheEvent).getId());
    }

    @Override
    public void addToCacheEventollection(CacheEvent cacheEvent, CacheEventCollectionId collectionId) throws OXException {
        channel.addToMessageCollection(cacheEvent, new MessageCollectionId(collectionId.getId(), CACHE_EVENT_CHANNEL_KEY));
    }

    @Override
    public void releaseCacheEventCollection(CacheEventCollectionId collectionId) throws OXException {
        channel.releaseMessageCollection(new MessageCollectionId(collectionId.getId(), CACHE_EVENT_CHANNEL_KEY));
    }

    @Override
    public void dropCacheEventCollection(CacheEventCollectionId collectionId) throws OXException {
        channel.dropMessageCollection(new MessageCollectionId(collectionId.getId(), CACHE_EVENT_CHANNEL_KEY));
    }

}
