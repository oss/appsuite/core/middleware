/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.redis;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import com.openexchange.cache.v2.redis.inmemory.Mode;
import com.openexchange.config.lean.Property;

/**
 * {@link RedisCacheProperty} - Properties enumeration for Redis-backed cache.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public enum RedisCacheProperty implements Property {

    /**
     * Defines whether to use thread-local pre-cache.
     */
    THREAD_LOCAL("threadLocal", TRUE),
    /**
     * Whether to track cache metrics or not
     */
    WITH_METRICS("withMetrics", FALSE),

    // ----------------------------------------------- In-Memory Cache -----------------------------------------------------------------

    /**
     * Whether to to use volatile in-memory cache as simple facade prior to accessing Redis storage
     * <p>
     * In-memory cache gets updated with most recent value fetched from Redis storage in any case using a separate thread.
     */
    @Deprecated
    USE_INMEMORY_CACHE_OLD("useInMemoryCache", FALSE),
    /**
     * Whether to to use volatile in-memory cache as simple facade prior to accessing Redis storage
     * <p>
     * In-memory cache gets updated with most recent value fetched from Redis storage in any case using a separate thread.
     */
    @Deprecated
    INMEMORY_CACHE_MODE_OLD("inMemoryCacheMode", Mode.VOLATILE.getIdentifier()),
    /**
     * Whether to to use volatile in-memory cache as simple facade prior to accessing Redis storage
     */
    INMEMORY_ENABLED("inmemory.enabled", FALSE),
    /**
     * Configures the operation mode for the volatile in-memory cache. Supported values are:
     * <p>
     * Supported values are <code>volatile</code> (keeping values in cache for a very short amount of time as per
     * <code>com.openexchange.cache.v2.redis.inmemory.timeToLiveMillis</code>), and <code>updateAfterFetch</code> (re-fetches any read
     * value from Redis storage and updates it in the background so that a second fetch is ensured to provide the newest value).
     */
    INMEMORY_MODE("inmemory.mode", Mode.VOLATILE.getIdentifier()),
    /**
     * Configures the time-to-live of elements in the in-memory caching layer, when operating in <code>volatile</code> mode.
     */
    INMEMORY_TTL_MILLIS("inmemory.timeToLiveMillis", L(10_000)),

    // ---------------------------------------------------------------------------------------------------------------------------------

    /**
     * Whether to collect metrics for thread-local cache.
     */
    METRICS_THREAD_LOCAL("metrics.threadLocal", FALSE),
    /**
     * Optionally disables field expiration of hash keys for increased compatibility. If set to <code>true</code>, no <code>HEXPIRE</code>
     * commands are invoked after setting values in hashes, resulting in <i>persistent</i> values that will only be removed explicitly by
     * the application, or due to a general <code>maxmemory</code> eviction policy of Redis like <code>allkeys-lru</code>.
     * <p/>
     * Should therefore only be enabled if a dedicated Redis instance for cache-purposes is used (see
     * <code>com.openexchange.redis.cache.enabled</code>)!
     */
    DISABLE_HASH_EXPIRATION("disableHashExpiration", FALSE),
    /**
     * Whether to also track PUTs within thread-local collection
     */
    TRACK_PUTS_IN_THREAD_LOCAL_CACHE("threadLocal.trackPuts", FALSE),
    /**
     * Whether to orchestrate concurrent retrievals of the same key. E.g. another thread would wait for a thread that currently loads the
     * same key - thus also using the already loaded object.
     */
    ORCHESTRATE_CONCURRENT_RETRIEVAL("orchestrateConcurrentRetrieval", TRUE),
    /**
     * The maximum number of keys to address per <code>MGET</code>, <code>MSET</code>, <code>MHGET</code> or <code>MHSET</code> command.
     */
    MULTI_KEY_LIMIT("multiKeyLimit", I(100)),
    ;

    private final Object defaultValue;
    private final String fqn;

    /**
     * Initializes a new {@link RedisCacheProperty}.
     *
     * @param appendix The appendix for full-qualified name
     * @param defaultValue The default value
     */
    private RedisCacheProperty(String appendix, Object defaultValue) {
        this.fqn = "com.openexchange.cache.v2.redis." + appendix;
        this.defaultValue = defaultValue;
    }

    /**
     * Returns the fully qualified name for the property
     *
     * @return the fully qualified name for the property
     */
    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    /**
     * Returns the default value of this property
     *
     * @return the default value of this property
     */
    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
