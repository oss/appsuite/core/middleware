/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.audit.impl;

import static com.openexchange.java.Autoboxing.B;
import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.util.Collections;
import org.dmfs.rfc5545.DateTime;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.osgi.service.event.Event;
import com.openexchange.chronos.Attendee;
import com.openexchange.chronos.CalendarUser;
import com.openexchange.event.CommonEvent;
import com.openexchange.file.storage.FileStorageEventConstants;
import com.openexchange.groupware.Types;
import com.openexchange.groupware.container.Contact;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.contexts.impl.ContextStorage;
import com.openexchange.groupware.infostore.DocumentMetadata;
import com.openexchange.groupware.ldap.UserStorage;
import com.openexchange.groupware.tasks.Task;
import com.openexchange.session.Session;
import com.openexchange.test.mock.MockUtils;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * Unit tests for {@link AuditEventHandler}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 7.4.1
 */
public class AuditEventHandlerTest {

    private AuditEventHandler auditEventHandler;

    @Mock
    private Event event;

    @Mock
    private CommonEvent commonEvent;

    @Mock
    private Context context;

    @Mock
    private org.slf4j.Logger log;

    @Mock
    private UserService userService;

    @Mock
    private Contact contact;

    private CalendarUser calendarUser;
    private StringBuilder stringBuilder;

    private final int userId = 9999;
    private final int contextId = 111111;
    private final int objectId = 555555555;
    private final String objectTitle = "theObjectTitle";
    private final DateTime date = new DateTime(System.currentTimeMillis());

    private MockedStatic<UserStorage> userStorageStaticMock;
    private MockedStatic<ContextStorage> contextStorageStaticMock;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);

        // Mock static methods
        userStorageStaticMock = Mockito.mockStatic(UserStorage.class);
        contextStorageStaticMock = Mockito.mockStatic(ContextStorage.class);

        // Configure mocks for static methods
        User user = Mockito.mock(com.openexchange.user.User.class);
        Mockito.when(user.getDisplayName()).thenReturn(this.objectTitle);
        Mockito.when(userService.getUser(ArgumentMatchers.anyInt(), ArgumentMatchers.any(Context.class))).thenReturn(user);

        // Initialize non-mock objects
        this.calendarUser = new CalendarUser();
        this.calendarUser.setEntity(userId);
        this.calendarUser.setCn("user name");

        this.stringBuilder = new StringBuilder();
    }

    @AfterEach
    public void tearDown() {
        userStorageStaticMock.close();
        contextStorageStaticMock.close();
    }

    @Test
    public void testGetInstance_Fine_ReturnInstance() {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        Assertions.assertNotNull(this.auditEventHandler);
    }

    @Test
    public void testHandleEvent_InfoLoggingDisabled_Return() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        Mockito.when(B(log.isInfoEnabled())).thenReturn(Boolean.FALSE);
        MockUtils.injectValueIntoPrivateField(this.auditEventHandler, "logger", log);

        this.auditEventHandler.handleEvent(event);

        Mockito.verify(log, Mockito.never()).info(Mockito.anyString());
    }

    @Test
    public void testHandleEvent_InfoLoggingEnabledButWrongEvent_NothingToWrite() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        Mockito.when(B(log.isInfoEnabled())).thenReturn(Boolean.TRUE);
        MockUtils.injectValueIntoPrivateField(this.auditEventHandler, "logger", log);
        Mockito.when(this.event.getTopic()).thenReturn("topicOfAnyOtherEvent");

        this.auditEventHandler.handleEvent(event);

        Mockito.verify(log, Mockito.never()).info(Mockito.anyString());
    }

    @Test
    public void testHandleEvent_IsInfoStoreEventButLogEmpty_NotLogged() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected void handleInfostoreEvent(Event event, StringBuilder log) {
                return;
            }
        };

        Mockito.when(B(log.isInfoEnabled())).thenReturn(Boolean.TRUE);
        MockUtils.injectValueIntoPrivateField(this.auditEventHandler, "logger", log);
        Mockito.when(this.event.getTopic()).thenReturn("com/openexchange/groupware/infostore/");

        this.auditEventHandler.handleEvent(event);

        Mockito.verify(log, Mockito.never()).info(Mockito.anyString());
    }

    @Test
    public void testHandleEvent_IsGroupwareEventButLogEmpty_NotLogged() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected void handleGroupwareEvent(Event event, StringBuilder log) {
                return;
            }
        };

        Mockito.when(B(log.isInfoEnabled())).thenReturn(Boolean.TRUE);
        MockUtils.injectValueIntoPrivateField(this.auditEventHandler, "logger", log);
        Mockito.when(this.event.getTopic()).thenReturn("com/openexchange/groupware/");

        this.auditEventHandler.handleEvent(event);

        Mockito.verify(log, Mockito.never()).info(Mockito.anyString());
    }

    @Test
    public void testHandleEvent_IsInfoStoreEventAndLogNotEmpty_Logged() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected void handleInfostoreEvent(Event event, StringBuilder log) {
                log.append("isInfostoreEvent");
                return;
            }
        };

        Mockito.when(B(log.isInfoEnabled())).thenReturn(Boolean.TRUE);
        MockUtils.injectValueIntoPrivateField(this.auditEventHandler, "logger", log);
        Mockito.when(this.event.getTopic()).thenReturn("com/openexchange/groupware/infostore/");

        this.auditEventHandler.handleEvent(event);

        Mockito.verify(log, Mockito.times(1)).info("isInfostoreEvent");
    }

    @Test
    public void testHandleEvent_IsGroupwareEventAndLogNotEmpty_Logged() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected void handleGroupwareEvent(Event event, StringBuilder log) {
                log.append("isGroupwareEvent");
                return;
            }
        };

        Mockito.when(B(log.isInfoEnabled())).thenReturn(Boolean.TRUE);
        MockUtils.injectValueIntoPrivateField(this.auditEventHandler, "logger", log);
        Mockito.when(this.event.getTopic()).thenReturn("com/openexchange/groupware/");

        this.auditEventHandler.handleEvent(event);

        Mockito.verify(log, Mockito.times(1)).info("isGroupwareEvent");
    }

    @Test
    public void testHandleMainCommmonEvent_CommonEventNull_ThrowException() {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleMainCommmonEvent(null, stringBuilder);
        });
    }

    @Test
    public void testHandleMainCommmonEvent_StringBuilderNull_ThrowException() {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleMainCommmonEvent(commonEvent, null);
        });
    }

    @Test
    public void testHandleMainCommmonEvent_EventInsert_AddInsertToLog() {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        Mockito.when(I(commonEvent.getAction())).thenReturn(I(CommonEvent.INSERT));

        this.auditEventHandler.handleMainCommmonEvent(commonEvent, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().startsWith("EVENT TYPE: INSERT; "));
    }

    @Test
    public void testHandleMainCommmonEvent_EventDelete_AddDeleteToLog() {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        Mockito.when(I(commonEvent.getAction())).thenReturn(I(CommonEvent.DELETE));

        this.auditEventHandler.handleMainCommmonEvent(commonEvent, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().startsWith("EVENT TYPE: DELETE; "));
    }

    @Test
    public void testHandleMainCommmonEvent_EventUpdate_AddUpdateToLog() {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        Mockito.when(I(commonEvent.getAction())).thenReturn(I(CommonEvent.UPDATE));

        this.auditEventHandler.handleMainCommmonEvent(commonEvent, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().startsWith("EVENT TYPE: UPDATE; "));
    }

    @Test
    public void testHandleAppointmentCommmonEvent_CommonEventNull_ThrowException() {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleAppointmentCommonEvent(null, context, stringBuilder);
        });
    }

    @Test
    public void testHandleAppointmentCommmonEvent_StringBuilderNull_ThrowException() {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleAppointmentCommonEvent(commonEvent, context, null);
        });
    }

    @Test
    public void testHandleAppointmentCommonEvent_EverythingFine_LogStartWithCorrect() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected String getPathToRoot(int folderId, Session sessionObj) {
                return "";
            }
        };

        Mockito.when(userService.getUser(userId, context).getDisplayName()).thenReturn("TestUser");

        com.openexchange.chronos.Event event = Mockito.mock(com.openexchange.chronos.Event.class);
        Mockito.when(I(commonEvent.getAction())).thenReturn(I(CommonEvent.INSERT));
        Mockito.when(commonEvent.getActionObj()).thenReturn(event);
        Mockito.when(I(commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(commonEvent.getUserId())).thenReturn(I(this.userId));
        Mockito.when(event.getId()).thenReturn(String.valueOf(this.objectId));
        Mockito.when(event.getCreatedBy()).thenReturn(this.calendarUser);
        Mockito.when(event.getModifiedBy()).thenReturn(this.calendarUser);
        Mockito.when(event.getSummary()).thenReturn(this.objectTitle);
        Mockito.when(event.getStartDate()).thenReturn(this.date);
        Mockito.when(event.getEndDate()).thenReturn(this.date);
        Mockito.when(event.getAttendees()).thenReturn(Collections.singletonList(new Attendee()));

        this.auditEventHandler.handleAppointmentCommonEvent(commonEvent, context, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().startsWith("OBJECT TYPE: EVENT; "));
    }

    @Test
    public void testHandleAppointmentCommonEvent_EverythingFine_ContainsAllInformation() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected String getPathToRoot(int folderId, Session sessionObj) {
                return "";
            }
        };

        Mockito.when(userService.getUser(userId, context).getDisplayName()).thenReturn("TestUser");

        com.openexchange.chronos.Event event = Mockito.mock(com.openexchange.chronos.Event.class);
        Mockito.when(I(commonEvent.getAction())).thenReturn(I(CommonEvent.INSERT));
        Mockito.when(commonEvent.getActionObj()).thenReturn(event);
        Mockito.when(I(commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(commonEvent.getUserId())).thenReturn(I(this.userId));
        Mockito.when(event.getId()).thenReturn(String.valueOf(this.objectId));
        Mockito.when(event.getCreatedBy()).thenReturn(this.calendarUser);
        Mockito.when(event.getModifiedBy()).thenReturn(this.calendarUser);
        Mockito.when(event.getSummary()).thenReturn(this.objectTitle);
        Mockito.when(event.getStartDate()).thenReturn(this.date);
        Mockito.when(event.getEndDate()).thenReturn(this.date);
        Attendee attendee = new Attendee();
        attendee.setCn("InvitedTestUser");
        Mockito.when(event.getAttendees()).thenReturn(Collections.singletonList(attendee));

        this.auditEventHandler.handleAppointmentCommonEvent(commonEvent, context, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().contains("CONTEXT ID: " + this.contextId));
        Assertions.assertTrue(stringBuilder.toString().contains("END DATE: " + this.date));
    }

    @Test
    public void testHandleAppointmentCommonEvent_EverythingFine() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected String getPathToRoot(int folderId, Session sessionObj) {
                return "";
            }
        };

        Mockito.when(userService.getUser(userId, context).getDisplayName()).thenReturn("TestUser");

        com.openexchange.chronos.Event event = Mockito.mock(com.openexchange.chronos.Event.class);
        Mockito.when(I(commonEvent.getAction())).thenReturn(I(CommonEvent.DELETE));
        Mockito.when(commonEvent.getActionObj()).thenReturn(event);
        Mockito.when(I(commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(commonEvent.getUserId())).thenReturn(I(this.userId));
        Mockito.when(event.getId()).thenReturn(String.valueOf(this.objectId));
        Mockito.when(event.getCreatedBy()).thenReturn(this.calendarUser);
        Mockito.when(event.getModifiedBy()).thenReturn(this.calendarUser);
        Mockito.when(event.getSummary()).thenReturn(this.objectTitle);
        Mockito.when(event.getStartDate()).thenReturn(this.date);
        Mockito.when(event.getEndDate()).thenReturn(this.date);
        Attendee attendee = new Attendee();
        attendee.setCn("InvitedTestUser");
        Mockito.when(event.getAttendees()).thenReturn(Collections.singletonList(attendee));

        this.auditEventHandler.handleAppointmentCommonEvent(commonEvent, context, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().contains("CONTEXT ID: " + this.contextId));
        Assertions.assertTrue(stringBuilder.toString().contains("END DATE: " + this.date));
    }

    @Test
    public void testHandleContactCommmonEvent_CommonEventNull_ThrowException() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleContactCommonEvent(null, context, stringBuilder);
        });
    }

    @Test
    public void testHandleContactCommmonEvent_StringBuilderNull_ThrowException() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleContactCommonEvent(commonEvent, context, null);
        });
    }

    @Test
    public void testHandleContactCommonEvent_EverythingFine_LogStartWithCorrect() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected String getPathToRoot(int folderId, Session sessionObj) {
                return "";
            }
        };

        Mockito.when(commonEvent.getActionObj()).thenReturn(contact);
        Mockito.when(I(commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(commonEvent.getUserId())).thenReturn(I(this.userId));
        Mockito.when(I(contact.getObjectID())).thenReturn(I(this.objectId));
        Mockito.when(I(contact.getCreatedBy())).thenReturn(I(this.userId));
        Mockito.when(I(contact.getModifiedBy())).thenReturn(I(this.userId));
        Mockito.when(contact.getTitle()).thenReturn(this.objectTitle);

        this.auditEventHandler.handleContactCommonEvent(commonEvent, context, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().startsWith("OBJECT TYPE: CONTACT; "));
    }

    @Test
    public void testHandleContactCommonEvent_EverythingFine_ContainsDesiredInformation() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected String getPathToRoot(int folderId, Session sessionObj) {
                return "";
            }
        };

        Mockito.when(commonEvent.getActionObj()).thenReturn(contact);
        Mockito.when(I(commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(commonEvent.getUserId())).thenReturn(I(this.userId));
        Mockito.when(I(contact.getObjectID())).thenReturn(I(this.objectId));
        Mockito.when(I(contact.getCreatedBy())).thenReturn(I(this.userId));
        Mockito.when(I(contact.getModifiedBy())).thenReturn(I(this.userId));
        Mockito.when(contact.getDisplayName()).thenReturn(this.objectTitle);

        this.auditEventHandler.handleContactCommonEvent(commonEvent, context, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().contains("OBJECT ID: " + this.objectId));
        Assertions.assertTrue(stringBuilder.toString().contains("CONTACT FULLNAME: " + this.objectTitle));
        Assertions.assertFalse(stringBuilder.toString().contains("MODIFIED BY: " + this.objectTitle));
    }

    @Test
    public void testHandleContactCommonEvent_EverythingFine_ContainsAllInformation() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected String getPathToRoot(int folderId, Session sessionObj) {
                return "";
            }
        };

        Mockito.when(commonEvent.getActionObj()).thenReturn(contact);
        Mockito.when(I(commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(commonEvent.getUserId())).thenReturn(I(this.userId));
        Mockito.when(I(contact.getObjectID())).thenReturn(I(this.objectId));
        Mockito.when(I(contact.getCreatedBy())).thenReturn(I(this.userId));
        Mockito.when(I(contact.getModifiedBy())).thenReturn(I(this.userId));
        Mockito.when(contact.getDisplayName()).thenReturn(this.objectTitle);
        Mockito.when(B(contact.containsCreatedBy())).thenReturn(Boolean.TRUE);
        Mockito.when(B(contact.containsModifiedBy())).thenReturn(Boolean.TRUE);

        this.auditEventHandler.handleContactCommonEvent(commonEvent, context, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().contains("OBJECT ID: " + this.objectId));
        Assertions.assertTrue(stringBuilder.toString().contains("CONTACT FULLNAME: " + this.objectTitle));
        Assertions.assertTrue(stringBuilder.toString().contains("CREATED BY: " + this.objectTitle));
        Assertions.assertTrue(stringBuilder.toString().contains("MODIFIED BY: " + this.objectTitle));
    }

    @Test
    public void testHandleTaskCommmonEvent_CommonEventNull_ThrowException() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleTaskCommonEvent(null, context, stringBuilder);
        });
    }

    @Test
    public void testHandleTaskCommmonEvent_StringBuilderNull_ThrowException() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleTaskCommonEvent(commonEvent, context, null);
        });
    }

    @Test
    public void testHandleTaskCommonEvent_EverythingFine_LogStartWithCorrect() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected String getPathToRoot(int folderId, Session sessionObj) {
                return "";
            }
        };

        Task task = Mockito.mock(Task.class);
        Mockito.when(commonEvent.getActionObj()).thenReturn(task);
        Mockito.when(I(commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(commonEvent.getUserId())).thenReturn(I(this.userId));
        Mockito.when(I(task.getObjectID())).thenReturn(I(this.objectId));
        Mockito.when(I(task.getCreatedBy())).thenReturn(I(this.userId));
        Mockito.when(I(task.getModifiedBy())).thenReturn(I(this.userId));
        Mockito.when(task.getTitle()).thenReturn(this.objectTitle);

        this.auditEventHandler.handleTaskCommonEvent(commonEvent, context, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().startsWith("OBJECT TYPE: TASK; "));
    }

    @Test
    public void testHandleTaskCommonEvent_EverythingFine_ContainsAllInformation() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected String getPathToRoot(int folderId, Session sessionObj) {
                return "";
            }
        };

        Task task = Mockito.mock(Task.class);
        Mockito.when(commonEvent.getActionObj()).thenReturn(task);
        Mockito.when(I(commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(commonEvent.getUserId())).thenReturn(I(this.userId));
        Mockito.when(I(task.getObjectID())).thenReturn(I(this.objectId));
        Mockito.when(I(task.getCreatedBy())).thenReturn(I(this.userId));
        Mockito.when(I(task.getModifiedBy())).thenReturn(I(this.userId));
        Mockito.when(task.getTitle()).thenReturn(this.objectTitle);

        this.auditEventHandler.handleTaskCommonEvent(commonEvent, context, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().contains("OBJECT ID: " + this.objectId));
        Assertions.assertTrue(stringBuilder.toString().contains("TITLE: " + this.objectTitle));
        Assertions.assertFalse(stringBuilder.toString().contains("MODIFIED BY: " + this.userId));
    }

    @Test
    public void testHandleInfostoreCommmonEvent_CommonEventNull_ThrowException() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleInfostoreCommonEvent(null, context, stringBuilder);
        });
    }

    @Test
    public void testHandleInfostoreCommmonEvent_StringBuilderNull_ThrowException() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleInfostoreCommonEvent(commonEvent, context, null);
        });
    }


    @Test
    public void testHandleInfostoreCommonEvent_EverythingFine_LogStartWithCorrect() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected String getPathToRoot(int folderId, Session sessionObj) {
                return "";
            }
        };

        DocumentMetadata documentMetadata = Mockito.mock(DocumentMetadata.class);
        Mockito.when(commonEvent.getActionObj()).thenReturn(documentMetadata);
        Mockito.when(I(commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(commonEvent.getUserId())).thenReturn(I(this.userId));
        Mockito.when(I(documentMetadata.getCreatedBy())).thenReturn(I(this.userId));
        Mockito.when(I(documentMetadata.getModifiedBy())).thenReturn(I(this.userId));
        Mockito.when(documentMetadata.getTitle()).thenReturn(this.objectTitle);

        this.auditEventHandler.handleInfostoreCommonEvent(commonEvent, context, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().startsWith("OBJECT TYPE: INFOSTORE; "));
    }

    @Test
    public void testHandleInfostoreCommonEvent_EverythingFine_ContainsAllInformation() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected String getPathToRoot(int folderId, Session sessionObj) {
                return "";
            }
        };

        DocumentMetadata documentMetadata = Mockito.mock(DocumentMetadata.class);
        Mockito.when(commonEvent.getActionObj()).thenReturn(documentMetadata);
        Mockito.when(I(commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(commonEvent.getUserId())).thenReturn(I(this.userId));
        Mockito.when(I(documentMetadata.getCreatedBy())).thenReturn(I(this.userId));
        Mockito.when(I(documentMetadata.getModifiedBy())).thenReturn(I(this.userId));
        Mockito.when(documentMetadata.getTitle()).thenReturn(this.objectTitle);
        Mockito.when(I(documentMetadata.getId())).thenReturn(I(this.objectId));

        this.auditEventHandler.handleInfostoreCommonEvent(commonEvent, context, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().contains("OBJECT ID: " + this.objectId));
        Assertions.assertTrue(stringBuilder.toString().contains("TITLE: " + this.objectTitle));
        Assertions.assertFalse(stringBuilder.toString().contains("MODIFIED BY: " + this.userId));
    }

    @Test
    public void testHandleInfostoreEvent_EventNull_ThrowException() {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleInfostoreEvent(null, stringBuilder);
        });
    }

    @Test
    public void testHandleInfostoreEvent_StringBuilderNull_ThrowException() {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleInfostoreEvent(event, null);
        });
    }

    @Test
    public void testHandleInfostoreEvent_TopicNotRelevant_OnlyAppendDefault() {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        Mockito.when(event.getTopic()).thenReturn(this.objectTitle);
        Session session = Mockito.mock(Session.class);
        Mockito.when(session.getParameter(ArgumentMatchers.anyString())).thenReturn(Boolean.FALSE);
        Mockito.when(event.getProperty(FileStorageEventConstants.SESSION)).thenReturn(session);
        Mockito.when(event.getProperty(FileStorageEventConstants.OBJECT_ID)).thenReturn(I(this.objectId));
        Mockito.when(event.getProperty(FileStorageEventConstants.SERVICE)).thenReturn(this.objectTitle);
        Mockito.when(event.getProperty(FileStorageEventConstants.ACCOUNT_ID)).thenReturn(I(this.userId));
        Mockito.when(event.getProperty(FileStorageEventConstants.FOLDER_ID)).thenReturn(this.objectTitle);

        this.auditEventHandler.handleInfostoreEvent(event, stringBuilder);

        Assertions.assertFalse(stringBuilder.toString().startsWith("EVENT TYPE:"));
        Assertions.assertFalse(stringBuilder.toString().contains("PUBLISH: "));
        Assertions.assertTrue(stringBuilder.toString().startsWith("EVENT TIME: "));
        Assertions.assertTrue(stringBuilder.toString().contains("FOLDER: " + this.objectTitle));
        Assertions.assertTrue(stringBuilder.toString().contains("SERVICE ID: " + this.objectTitle));
    }

    @Test
    public void testHandleInfostoreEvent_AccessTopic_AppendLocalIp() {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        Mockito.when(event.getTopic()).thenReturn(FileStorageEventConstants.ACCESS_TOPIC);
        Session session = Mockito.mock(Session.class);
        Mockito.when(session.getParameter(ArgumentMatchers.anyString())).thenReturn(Boolean.TRUE);
        Mockito.when(event.getProperty(FileStorageEventConstants.SESSION)).thenReturn(session);
        Mockito.when(event.getProperty(FileStorageEventConstants.OBJECT_ID)).thenReturn(I(this.objectId));
        Mockito.when(event.getProperty(FileStorageEventConstants.SERVICE)).thenReturn(this.objectTitle);
        Mockito.when(event.getProperty(FileStorageEventConstants.ACCOUNT_ID)).thenReturn(I(this.userId));
        Mockito.when(event.getProperty(FileStorageEventConstants.FOLDER_ID)).thenReturn(this.objectTitle);
        Mockito.when(event.getProperty("remoteAddress")).thenReturn("172.16.13.71");

        this.auditEventHandler.handleInfostoreEvent(event, stringBuilder);

        Assertions.assertTrue(stringBuilder.toString().startsWith("EVENT TYPE: ACCESS; "));
    }

    @Test
    public void testHandleGroupwareEvent_EventNull_ThrowException() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleGroupwareEvent(null, stringBuilder);
        });
    }

    @Test
    public void testHandleGroupwareEvent_StringBuilderNull_ThrowException() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true);

        assertThrows(NullPointerException.class, () -> {
            this.auditEventHandler.handleGroupwareEvent(event, null);
        });
    }

    @Test
    public void testHandleGroupwareEvent_CommonEventNull_Return() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected void handleMainCommmonEvent(CommonEvent commonEvent, StringBuilder log) {
                return;
            }
        };

        Mockito.when(event.getProperty(CommonEvent.EVENT_KEY)).thenReturn(null);

        this.auditEventHandler.handleGroupwareEvent(event, stringBuilder);
    }

    @Test
    public void testHandleGroupwareEvent_TypeAppointment_InvokeAppointment() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected void handleMainCommmonEvent(CommonEvent commonEvent, StringBuilder log) {
                return;
            }

            @Override
            protected void handleAppointmentCommonEvent(CommonEvent commonEvent, Context context, StringBuilder log) {
                return;
            }
        };

        Mockito.when(event.getProperty(CommonEvent.EVENT_KEY)).thenReturn(this.commonEvent);
        Mockito.when(I(this.commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(this.commonEvent.getModule())).thenReturn(I(Types.APPOINTMENT));
        ContextStorage contextStorage = Mockito.mock(ContextStorage.class);
        Mockito.when(contextStorage.getContext(this.contextId)).thenReturn(this.context);

        Mockito.when(ContextStorage.getInstance()).thenReturn(contextStorage);

        this.auditEventHandler.handleGroupwareEvent(event, stringBuilder);
    }

    @Test
    public void testHandleGroupwareEvent_TypeContact_InvokeContact() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected void handleMainCommmonEvent(CommonEvent commonEvent, StringBuilder log) {
                return;
            }

            @Override
            protected void handleContactCommonEvent(CommonEvent commonEvent, Context context, StringBuilder log) {
                return;
            }
        };

        Mockito.when(event.getProperty(CommonEvent.EVENT_KEY)).thenReturn(this.commonEvent);
        Mockito.when(I(this.commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(this.commonEvent.getModule())).thenReturn(I(Types.CONTACT));
        ContextStorage contextStorage = Mockito.mock(ContextStorage.class);
        Mockito.when(contextStorage.getContext(this.contextId)).thenReturn(this.context);

        Mockito.when(ContextStorage.getInstance()).thenReturn(contextStorage);

        this.auditEventHandler.handleGroupwareEvent(event, stringBuilder);
    }

    @Test
    public void testHandleGroupwareEvent_TypeTask_InvokeTask() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected void handleMainCommmonEvent(CommonEvent commonEvent, StringBuilder log) {
                return;
            }

            @Override
            protected void handleTaskCommonEvent(CommonEvent commonEvent, Context context, StringBuilder log) {
                return;
            }
        };

        Mockito.when(event.getProperty(CommonEvent.EVENT_KEY)).thenReturn(this.commonEvent);
        Mockito.when(I(this.commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(this.commonEvent.getModule())).thenReturn(I(Types.TASK));
        ContextStorage contextStorage = Mockito.mock(ContextStorage.class);
        Mockito.when(contextStorage.getContext(this.contextId)).thenReturn(this.context);

        Mockito.when(ContextStorage.getInstance()).thenReturn(contextStorage);

        this.auditEventHandler.handleGroupwareEvent(event, stringBuilder);
    }

    @Test
    public void testHandleGroupwareEvent_TypeInfostore_InvokeInfostore() throws Exception {
        this.auditEventHandler = new AuditEventHandler(userService, true, true) {

            @Override
            protected void handleMainCommmonEvent(CommonEvent commonEvent, StringBuilder log) {
                return;
            }

            @Override
            protected void handleInfostoreCommonEvent(CommonEvent commonEvent, Context context, StringBuilder log) {
                return;
            }
        };

        Mockito.when(event.getProperty(CommonEvent.EVENT_KEY)).thenReturn(this.commonEvent);
        Mockito.when(I(this.commonEvent.getContextId())).thenReturn(I(this.contextId));
        Mockito.when(I(this.commonEvent.getModule())).thenReturn(I(Types.INFOSTORE));
        ContextStorage contextStorage = Mockito.mock(ContextStorage.class);
        Mockito.when(contextStorage.getContext(this.contextId)).thenReturn(this.context);

        Mockito.when(ContextStorage.getInstance()).thenReturn(contextStorage);

        this.auditEventHandler.handleGroupwareEvent(event, stringBuilder);
    }

}
