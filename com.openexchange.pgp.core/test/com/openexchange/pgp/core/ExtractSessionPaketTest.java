/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.pgp.core;

import static com.openexchange.java.Autoboxing.B;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.List;
import java.util.stream.Stream;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPKeyRingGenerator;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import com.openexchange.pgp.core.packethandling.ExtractSessionProcessorHandler;
import com.openexchange.pgp.core.packethandling.ExtractSessionProcessorHandler.EncryptedSession;
import com.openexchange.pgp.core.packethandling.PacketProcessor;
import com.openexchange.tools.encoding.Base64;

/**
 * {@link ExtractSessionPaketTest} tests to extract the session packets from a PGP Stream.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class ExtractSessionPaketTest extends AbstractPGPTest {

    private boolean armored;
    private Identity testIdentity;

    @BeforeEach
    public void setup() throws NoSuchAlgorithmException, NoSuchProviderException, PGPException {
        //Setting up a keypair for the first identity
        PGPKeyRingGenerator keyGenerator = createPGPKeyPairGenerator();
        testIdentity = new Identity(TEST_IDENTITY_NAME, getPublicKeyFromGenerator(keyGenerator), getSecretKeyFromGenerator(keyGenerator), TEST_IDENTITY_PASSWORD);
    }

    static Stream<Boolean> parameters() {
        return Stream.of(true, false);
    }

    private String getKeyData(PGPPublicKey publicKey) throws IOException {
        try(
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ArmoredOutputStream arm = new ArmoredOutputStream(out);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();) {

            publicKey.encode(bout);
            arm.write(bout.toByteArray());
            arm.close();
            return new String(out.toByteArray());
        }
    }

    private String getKeyData(PGPSecretKey secretKey) throws IOException {
        try(
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ArmoredOutputStream arm = new ArmoredOutputStream(out);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();) {

            secretKey.encode(bout);
            arm.write(bout.toByteArray());
            arm.close();
            return new String(out.toByteArray());
        }
    }

    /**
     * Tests to extract the symmetric session key from encrypted PGP data and use it for decryption.
     *
     * @throws Exception
     */
    @ParameterizedTest
    @MethodSource("parameters")
    public void testExtractSymmetricSessionKeyAndDecrypt(boolean armored) throws Exception {
        this.armored = armored;

        //Encrypt the key data into a test PGP Stream
        byte[] testData = generateTestData();
        ByteArrayInputStream testDataStream = new ByteArrayInputStream(testData);
        ByteArrayOutputStream encryptedDataStream = new ByteArrayOutputStream();
        PGPEncrypter encrypter = new PGPEncrypter();
        encrypter.encrypt(testDataStream, encryptedDataStream, armored, testIdentity.getPublicKey());
        byte[] encryptedTestData  = encryptedDataStream.toByteArray();

        //Extracting the session data out of the PGP Stream
        PacketProcessor packetProcessor = new PacketProcessor();
        ExtractSessionProcessorHandler extractSessionProcessorHandler = new ExtractSessionProcessorHandler();
        packetProcessor.process(new ByteArrayInputStream(encryptedTestData),
            null,
            extractSessionProcessorHandler,
            armored);
        EncryptedSession encryptedPgpSession =
            extractSessionProcessorHandler.getEncryptedSession(testIdentity.getPublicKey().getKeyID());
        assertNotNull(encryptedPgpSession, "The session for the given identity should have been decrypted");

        //-------------------------------------------------------------------------------------------------------------
        // Decrypting the symmetric encryption key from the obtained session data

        PGPSessionKeyExtractor pgpSessionDecrypter = new PGPSessionKeyExtractor();
        PGPPrivateKey privateKey = decodePrivateKey(testIdentity.getSecretKey(), TEST_IDENTITY_PASSWORD);
        byte[] symmetricKey = pgpSessionDecrypter.decryptSymmetricSessionKey(encryptedPgpSession, privateKey);
        assertTrue(symmetricKey.length > 0, "The session key should have at least some data");

        //-------------------------------------------------------------------------------------------------------------

        //Now trying to decrypt the PGP Data just with the knowledge of the session key
        PGPSessionKeyDecrypter symmetricDecrypter = new PGPSessionKeyDecrypter(symmetricKey);
        ByteArrayOutputStream decryptedData = new ByteArrayOutputStream();
        PGPDecryptionResult result = symmetricDecrypter.decrypt(new ByteArrayInputStream(encryptedTestData), decryptedData);
        List<SignatureVerificationResult> verifyResults = result.getSignatureVerificationResults();
        assertNotNull(B(verifyResults.isEmpty()), "Verification results should be empty for non signed data");
        assertArrayEquals(decryptedData.toByteArray(), testData, "Decrypted data should be equals to plaintext data");

        //-------------------------------------------------------------------------------------------------------------

        //Test output
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ NEW TEST RUN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("------------------------------------PGP-PUBLIC-KEY-----------------------------------------");
        System.out.println(getKeyData(testIdentity.getPublicKey()));
        System.out.println("------------------------------------PGP-PRIVATE-KEY-----------------------------------------");
        System.out.println("Password: " + new String(testIdentity.getPassword()));
        System.out.println(getKeyData(testIdentity.getSecretKey()));
        System.out.println("------------------------------------Encoded Session Packet-----------------------------------------");
        System.out.println(Base64.encode(encryptedPgpSession.getEncoded()));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        //-------------------------------------------------------------------------------------------------------------
    }
}
