/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.pgp.core;

import static org.junit.jupiter.api.Assertions.assertThrows;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.stream.Stream;
import org.bouncycastle.openpgp.PGPException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * {@link PGPSymmetricEncrypterDecrypterTest}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class PGPSymmetricEncrypterDecrypterTest {

    private boolean armored;
    private static final byte[] TEST_DATA = "Hello World!".getBytes();
    private static final char[] TEST_PASSWORD = new char[] { 'S', 'E', 'C', 'R', 'E', 'T' };
    private static final int AES_256 = 9; //see RFC 4880
    private static final int ZLIB = 2; //see RFC 4880

    static Stream<Boolean> parameters() {
        return Stream.of(true, false);
    }

    @ParameterizedTest
    @MethodSource("parameters")
    public void testEncryptDecrypt(boolean armored) throws Exception {
        this.armored = armored;
        //Encrypt
        ByteArrayInputStream plaintextStream = new ByteArrayInputStream(TEST_DATA);
        ByteArrayOutputStream encryptedData = new ByteArrayOutputStream();
        new PGPSymmetricEncrypter().encrypt(plaintextStream, encryptedData, armored, TEST_PASSWORD);

        //Decrypt
        ByteArrayOutputStream decryptedStream = new ByteArrayOutputStream();
        new PGPSymmetricDecrypter().decrypt(new ByteArrayInputStream(encryptedData.toByteArray()), decryptedStream, TEST_PASSWORD);

        //Verify
        Assertions.assertArrayEquals(TEST_DATA, decryptedStream.toByteArray());
    }

    @ParameterizedTest
    @MethodSource("parameters")
    public void testEncryptDecryptCompressed(boolean armored) throws Exception {
        this.armored = armored;
        //Encrypt
        ByteArrayInputStream plaintextStream = new ByteArrayInputStream(TEST_DATA);
        ByteArrayOutputStream encryptedData = new ByteArrayOutputStream();
        new PGPSymmetricEncrypter(AES_256, ZLIB).encrypt(plaintextStream, encryptedData, armored, TEST_PASSWORD);

        //Decrypt
        ByteArrayOutputStream decryptedStream = new ByteArrayOutputStream();
        new PGPSymmetricDecrypter().decrypt(new ByteArrayInputStream(encryptedData.toByteArray()), decryptedStream, TEST_PASSWORD);

        //Verify
        Assertions.assertArrayEquals(TEST_DATA, decryptedStream.toByteArray());
    }

    public void testDecryptWithWrongKeyShouldFail() throws Exception {
        //Encrypt
        ByteArrayInputStream plaintextStream = new ByteArrayInputStream(TEST_DATA);
        ByteArrayOutputStream encryptedData = new ByteArrayOutputStream();
        new PGPSymmetricEncrypter().encrypt(plaintextStream, encryptedData, armored, TEST_PASSWORD);

        //Decrypt with wrong password should throw a PGPException
        final char[] WRONG_PASSWORD = new char[] { 'W', 'R', 'O', 'N', 'G', '!' };
        ByteArrayOutputStream decryptedStream = new ByteArrayOutputStream();
        assertThrows(PGPException.class, () -> new PGPSymmetricDecrypter().decrypt(new ByteArrayInputStream(encryptedData.toByteArray()), decryptedStream, WRONG_PASSWORD));
    }
}
