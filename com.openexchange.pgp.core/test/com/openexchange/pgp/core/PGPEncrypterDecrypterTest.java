/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.pgp.core;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Stream;
import org.bouncycastle.openpgp.PGPKeyRingGenerator;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import com.openexchange.exception.OXException;

/**
 * {@link PGPEncrypterDecrypterTest}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.4
 */
public class PGPEncrypterDecrypterTest extends AbstractPGPTest {

    private boolean armored;
    private PGPPublicKey publicKey;
    private PGPSecretKey secretKey;
    private PGPKeyRetrievalStrategy keyRetrievalStrategy;

    static Stream<Boolean> parameters() {
        return Stream.of(true, false);
    }

    @BeforeEach
    public void setup() throws Exception {
        PGPKeyRingGenerator keyGenerator = createPGPKeyPairGenerator();
        publicKey = getPublicKeyFromGenerator(keyGenerator);
        secretKey = getSecretKeyFromGenerator(keyGenerator);

        //Setting up a strategy for key retrieving, this is used when decrypting data
        keyRetrievalStrategy = mock(PGPKeyRetrievalStrategy.class);
        when(keyRetrievalStrategy.getPublicKey(anyLong())).thenReturn(publicKey);
        when(keyRetrievalStrategy.getSecretKey(anyLong(),anyString(),any(char[].class))).thenReturn(decodePrivateKey(secretKey, TEST_IDENTITY_PASSWORD));
    }

    @ParameterizedTest
    @MethodSource("parameters")
    public void testEncryptDecrypt(boolean armored) throws Exception {
        this.armored = armored;

        byte[] testData = "test".getBytes();
        InputStream plainTextData = new ByteArrayInputStream(testData);

        //Encrypting the data
        ByteArrayOutputStream encryptedData = new ByteArrayOutputStream();
        new PGPEncrypter().encrypt(plainTextData, encryptedData, armored, publicKey);

        //Decrypting the data
        InputStream encryptedDataInput = new ByteArrayInputStream(encryptedData.toByteArray());
        ByteArrayOutputStream decryptedData = new ByteArrayOutputStream();
        PGPDecryptionResult result = new PGPDecrypter(keyRetrievalStrategy).decrypt(encryptedDataInput, decryptedData, TEST_IDENTITY_NAME,
            TEST_IDENTITY_PASSWORD);
        List<SignatureVerificationResult> verifyResults = result.getSignatureVerificationResults();
        Assertions.assertTrue(verifyResults.isEmpty(), "Verification results should be empty for non signed data");
        Assertions.assertArrayEquals(decryptedData.toByteArray(), testData, "Decrypted data should be equals to plaintext data");

        //Verify that the key has been retrieved
        verify(keyRetrievalStrategy).getSecretKey(anyLong(),anyString(),any(char[].class));
    }

    @ParameterizedTest
    @MethodSource("parameters")
    public void testEncryptSignedDecryptVerify(boolean armored) throws Exception {
        this.armored = armored;

        byte[] testData = "test".getBytes();
        InputStream plainTextData = new ByteArrayInputStream(testData);

        //Encrypting the data
        ByteArrayOutputStream encryptedData = new ByteArrayOutputStream();
        new PGPEncrypter().encryptSigned(plainTextData, encryptedData, armored, secretKey, TEST_IDENTITY_PASSWORD, publicKey);

        //Decrypting the data
        InputStream encryptedDataInput = new ByteArrayInputStream(encryptedData.toByteArray());
        ByteArrayOutputStream decryptedData = new ByteArrayOutputStream();
        PGPDecryptionResult result =
                new PGPDecrypter(keyRetrievalStrategy).decrypt(encryptedDataInput, decryptedData, TEST_IDENTITY_NAME, TEST_IDENTITY_PASSWORD);
        List<SignatureVerificationResult> verifyResults = result.getSignatureVerificationResults();
        Assertions.assertEquals(1, verifyResults.size(), "We should have obtained one verification result");
        Assertions.assertTrue(verifyResults.get(0).isVerified(), "Signature should have been verified");
        Assertions.assertArrayEquals(decryptedData.toByteArray(), testData, "Decrypted data should be equals to plaintext data");

        //Verify that both keys have been retrieved
        verify(keyRetrievalStrategy).getPublicKey(anyLong());
        verify(keyRetrievalStrategy).getSecretKey(anyLong(),anyString(),any(char[].class));
    }

    @ParameterizedTest
    @MethodSource("parameters")
    public void testDecryptNonPGPDataShouldThrowException(boolean armored) throws Exception {
        this.armored = armored;

        //Trying to decrypt emtpy data should result in an OXException
        InputStream nonPGPData = new ByteArrayInputStream(new byte[] {});
        assertThrows(OXException.class, () -> new PGPDecrypter(keyRetrievalStrategy).decrypt(nonPGPData, new ByteArrayOutputStream(), TEST_IDENTITY_NAME, TEST_IDENTITY_PASSWORD), "PGP-CORE-0001");
    }

    @ParameterizedTest
    @MethodSource("parameters")
    public void testDecryptWithoutKnownPrivateKeyShouldThrowException(boolean armored) throws Exception {
        this.armored = armored;

        //A strategy which does not find any keys in order to test error handling
        PGPKeyRetrievalStrategy noKeyFoundRetrievalStrategy = mock(PGPKeyRetrievalStrategy.class);
        when(noKeyFoundRetrievalStrategy.getPublicKey(anyLong())).thenReturn(null);

        byte[] testData = "test".getBytes();
        InputStream plainTextData = new ByteArrayInputStream(testData);

        //Encrypting the data
        ByteArrayOutputStream encryptedData = new ByteArrayOutputStream();
        new PGPEncrypter().encryptSigned(plainTextData, encryptedData, armored, secretKey, TEST_IDENTITY_PASSWORD, publicKey);

        //Decrypting the data
        InputStream encryptedDataInput = new ByteArrayInputStream(encryptedData.toByteArray());
        ByteArrayOutputStream decryptedData = new ByteArrayOutputStream();
        assertThrows(OXException.class, () ->  new PGPDecrypter(noKeyFoundRetrievalStrategy).decrypt(encryptedDataInput, decryptedData, TEST_IDENTITY_NAME, TEST_IDENTITY_PASSWORD), "PGP-CORE-0002");
    }

    @ParameterizedTest
    @MethodSource("parameters")
    public void testVerifySignatureWithoutKnownPublicKeyShouldReturnFalseSignatureVerificationResult(boolean armored) throws Exception{
        this.armored = armored;
        //A strategy which does find the secret key, but not the public key in order to test verification result
        PGPKeyRingGenerator keyGenerator = createPGPKeyPairGenerator();
        secretKey = getSecretKeyFromGenerator(keyGenerator);
        publicKey = getPublicKeyFromGenerator(keyGenerator);
        PGPKeyRetrievalStrategy onlyFindSecretKeyStrategy = mock(PGPKeyRetrievalStrategy.class);
        when(onlyFindSecretKeyStrategy.getSecretKey(anyLong(),anyString(),any(char[].class))).thenReturn(decodePrivateKey(secretKey, TEST_IDENTITY_PASSWORD));
        when(onlyFindSecretKeyStrategy.getPublicKey(anyLong())).thenReturn(null);

        byte[] testData = "test".getBytes();
        InputStream plainTextData = new ByteArrayInputStream(testData);

        //Encrypting the data
        ByteArrayOutputStream encryptedData = new ByteArrayOutputStream();
        new PGPEncrypter().encryptSigned(plainTextData, encryptedData, armored, secretKey, TEST_IDENTITY_PASSWORD, publicKey);

        //Decrypting the data
        InputStream encryptedDataInput = new ByteArrayInputStream(encryptedData.toByteArray());
        ByteArrayOutputStream decryptedData = new ByteArrayOutputStream();
        PGPDecryptionResult result =
            new PGPDecrypter(onlyFindSecretKeyStrategy).decrypt(encryptedDataInput, decryptedData, TEST_IDENTITY_NAME, TEST_IDENTITY_PASSWORD);
        List<SignatureVerificationResult> verifyResults = result.getSignatureVerificationResults();

        Assertions.assertEquals(1, verifyResults.size(), "We should have obtained one verification result");
        Assertions.assertFalse(verifyResults.get(0).isVerified(), "Signature should NOT have been verified due to missing public key");
        Assertions.assertArrayEquals(decryptedData.toByteArray(), testData, "Decrypted data should be equals to plaintext data");
    }

}
