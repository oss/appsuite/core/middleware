/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.compose.mailstorage;

import java.io.File;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import org.slf4j.Logger;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.ForcedReloadable;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.config.cascade.ConfigViews;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.mail.compose.CompositionSpaceProperties;
import com.openexchange.server.ServiceLookup;
import com.openexchange.uploaddir.UploadDirService;

/**
 * {@link MailStorageCompositionSpaceConfig} - Configuration for mail storage backed composition spaces.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.5
 */
public final class MailStorageCompositionSpaceConfig implements ForcedReloadable {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(MailStorageCompositionSpaceConfig.class);
    }

    private static final AtomicReference<MailStorageCompositionSpaceConfig> INSTANCE_REFERENCE = new AtomicReference<>(null);

    /**
     * Initializes the instance.
     *
     * @param services The service look-up to use
     * @return The newly created instance
     */
    public static MailStorageCompositionSpaceConfig initInstance(ServiceLookup services) {
        MailStorageCompositionSpaceConfig instance = new MailStorageCompositionSpaceConfig(services);
        INSTANCE_REFERENCE.set(instance);
        return instance;
    }

    /**
     * Drops the instance.
     */
    public static void dropInstance() {
        INSTANCE_REFERENCE.set(null);
    }

    /**
     * Gets the instance.
     *
     * @return The instance
     * @throws OXException If not yet initialized
     */
    public static MailStorageCompositionSpaceConfig getInstance() throws OXException {
        MailStorageCompositionSpaceConfig config = INSTANCE_REFERENCE.get();
        if (config == null) {
            throw OXException.general("Bundle \"com.openexchange.mail.compose.mailstorage\" not yet initialized");
        }
        return config;
    }

    /**
     * Gets the optional instance.
     *
     * @return The instance or empty if not yet initialized
     */
    public static Optional<MailStorageCompositionSpaceConfig> optInstance() {
        return Optional.ofNullable(INSTANCE_REFERENCE.get());
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final ServiceLookup services;
    private volatile File spoolDir = null;
    private volatile File fileCacheDir = null;

    /**
     * Initializes a new {@link MailStorageCompositionSpaceConfig}.
     *
     * @param services The service look-up
     */
    private MailStorageCompositionSpaceConfig(ServiceLookup services) {
        super();
        this.services = services;
    }

    /**
     * Checks if draft messages are supposed to be iterated if there is not hit for a composition space via direct search.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> if iteration is demanded; otherwise <code>false</code>
     * @throws OXException If check fails
     */
    public boolean isIterateIfNotFound(int userId, int contextId) throws OXException {
        ConfigViewFactory viewFactory = services.getServiceSafe(ConfigViewFactory.class);
        ConfigView view = viewFactory.getView(userId, contextId);
        boolean def = CompositionSpaceProperties.MAILSTORAGE_ITERATE_IF_NOT_FOUND.getDefaultValue(Boolean.class).booleanValue();
        return ConfigViews.getDefinedBoolPropertyFrom(CompositionSpaceProperties.MAILSTORAGE_ITERATE_IF_NOT_FOUND.getFQPropertyName(), def, view);
    }

    /**
     * Checks if mail storage backed composition spaces are enabled/available for given user.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> if enabled/available; otherwise <code>false</code>
     * @throws OXException If availability check fails
     */
    public boolean isEnabled(int userId, int contextId) throws OXException {
        ConfigViewFactory viewFactory = services.getServiceSafe(ConfigViewFactory.class);
        ConfigView view = viewFactory.getView(userId, contextId);
        boolean def = CompositionSpaceProperties.MAILSTORAGE_ENABLED.getDefaultValue(Boolean.class).booleanValue();
        return ConfigViews.getDefinedBoolPropertyFrom(CompositionSpaceProperties.MAILSTORAGE_ENABLED.getFQPropertyName(), def, view);
    }

    /**
     * Gets the in-memory threshold for composition space data. Messages exceeding that threshold are spooled to disk.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The in-memory threshold in bytes
     * @throws OXException If the in-memory threshold cannot be returned
     */
    public int getInMemoryThreshold(int userId, int contextId) throws OXException {
        ConfigViewFactory viewFactory = services.getServiceSafe(ConfigViewFactory.class);
        ConfigView view = viewFactory.getView(userId, contextId);
        int def = CompositionSpaceProperties.MAILSTORAGE_IN_MEMORY_THRESHOLD.getDefaultValue(Long.class).intValue();
        return ConfigViews.getDefinedIntPropertyFrom(CompositionSpaceProperties.MAILSTORAGE_IN_MEMORY_THRESHOLD.getFQPropertyName(), def, view);
    }

    /**
     * Gets the dedicated spool directory used for temporary composition space data that is too big to be held in memory.
     *
     * @throws OXException If spool directory cannot be returned
     */
    public File getSpoolDirectory() throws OXException {
        return getSpoolDirectory(Optional.empty());
    }

    /**
     * Gets the dedicated spool directory used for temporary composition space data that is too big to be held in memory.
     *
     * @param optionalConfig The optional configuration service
     * @throws OXException If spool directory cannot be returned
     */
    public File getSpoolDirectory(Optional<ConfigurationService> optionalConfig) throws OXException {
        File spoolDir = this.spoolDir;
        if (spoolDir == null) {
            ConfigurationService config = optionalConfig.isPresent() ? optionalConfig.get() : services.getServiceSafe(ConfigurationService.class);
            String spoolDirPath = config.getProperty(CompositionSpaceProperties.MAILSTORAGE_SPOOL_DIR.getFQPropertyName());
            if (Strings.isEmpty(spoolDirPath)) {
                UploadDirService uploadDirService = services.getServiceSafe(UploadDirService.class);
                spoolDir = uploadDirService.getUploadDir();
            } else {
                spoolDir = new File(spoolDirPath);
                if (!spoolDir.exists()) {
                    if (!spoolDir.mkdirs()) {
                        throw OXException.general("Spool directory " + spoolDir + " does not exist and cannot be created.");
                    }
                    LoggerHolder.LOG.info("Spool directory {} configured through {} did not exist, but could be created.", spoolDir, CompositionSpaceProperties.MAILSTORAGE_SPOOL_DIR.getFQPropertyName());
                }
                if (!spoolDir.isDirectory()) {
                    throw OXException.general(spoolDir + " is not a directory.");
                }
            }

            this.spoolDir = spoolDir;
        }

        return spoolDir;
    }

    /**
     * Checks whether the local file cache is enabled.
     *
     * @param optionalConfig The optional configuration service
     * @return <code>true</code> if cache is enabled; otherwise <code>false</code>
     * @throws OXException If according configuration setting cannot be examined
     */
    public boolean isFileCacheEnabled() throws OXException {
        ConfigurationService config = services.getServiceSafe(ConfigurationService.class);
        boolean defaultValue = CompositionSpaceProperties.MAILSTORAGE_FILE_CACHE_ENABLED.getDefaultValue(Boolean.class).booleanValue();
        return config.getBoolProperty(CompositionSpaceProperties.MAILSTORAGE_FILE_CACHE_ENABLED.getFQPropertyName(), defaultValue);
    }

    /**
     * Gets the file cache directory.
     *
     * @throws OXException If file cache directory cannot be returned
     */
    public File getFileCacheDirectory() throws OXException {
        return getFileCacheDirectory(Optional.empty());
    }

    /**
     * Gets the file cache directory.
     *
     * @param optionalConfig The optional configuration service
     * @throws OXException If file cache directory cannot be returned
     */
    public File getFileCacheDirectory(Optional<ConfigurationService> optionalConfig) throws OXException {
        File fileCacheDir = this.fileCacheDir;
        if (fileCacheDir == null) {
            ConfigurationService config = optionalConfig.isPresent() ? optionalConfig.get() : services.getServiceSafe(ConfigurationService.class);
            String cacheDirPath = config.getProperty(CompositionSpaceProperties.MAILSTORAGE_FILE_CACHE_DIR.getFQPropertyName());
            if (Strings.isEmpty(cacheDirPath)) {
                // fall back to spool directory by default
                fileCacheDir = getSpoolDirectory(optionalConfig);
            } else {
                fileCacheDir = new File(cacheDirPath);
                if (!fileCacheDir.exists()) {
                    if (!fileCacheDir.mkdirs()) {
                        throw OXException.general("File cache directory " + fileCacheDir + " does not exist and cannot be created.");
                    }
                    LoggerHolder.LOG.info("File cache directory {} configured through {} did not exist, but could be created.", fileCacheDir, CompositionSpaceProperties.MAILSTORAGE_FILE_CACHE_DIR.getFQPropertyName());
                }
                if (!fileCacheDir.isDirectory()) {
                    throw OXException.general(fileCacheDir + " is not a directory.");
                }
            }

            this.fileCacheDir = fileCacheDir;
        }

        return fileCacheDir;
    }


    /**
     * Gets the max. time a MIME message file might stay in local cache without being read.
     *
     * @return The max. idle time in seconds
     * @throws OXException If property cannot be read
     */
    public int getFileCacheMaxIdleSeconds() throws OXException {
        ConfigurationService config = services.getServiceSafe(ConfigurationService.class);
        int def = CompositionSpaceProperties.MAILSTORAGE_FILE_CACHE_MAX_IDLE_SECONDS.getDefaultValue(Integer.class).intValue();
        return config.getIntProperty(CompositionSpaceProperties.MAILSTORAGE_FILE_CACHE_MAX_IDLE_SECONDS.getFQPropertyName(), def);
    }

    /**
     * Gets the max. time that metadata records about open composition spaces might stay in an in-memory cache without being accessed.
     *
     * @return The max. idle time in seconds
     * @throws OXException If property cannot be read
     */
    public long getInMemoryCacheMaxIdleSeconds() throws OXException {
        ConfigurationService config = services.getServiceSafe(ConfigurationService.class);
        int def = CompositionSpaceProperties.MAILSTORAGE_IN_MEMORY_CACHE_MAX_IDLE_SECONDS.getDefaultValue(Integer.class).intValue();
        return config.getIntProperty(CompositionSpaceProperties.MAILSTORAGE_IN_MEMORY_CACHE_MAX_IDLE_SECONDS.getFQPropertyName(), def);
    }

    /**
     * Gets whether eager upload checks for max. mail size and quota are enabled. With enabled checks, incoming requests that
     * violate these (for example by exceeding it with an attachment upload) are rejected with an according error
     * response before the request content was fully consumed and thus allowing to fail early and not forcing the client to upload
     * large contents that would lead to limit restriction errors anyway.
     *
     * @return <code>true</code> for eager uploads; otherwise <code>false</code>
     * @throws OXException If property cannot be read
     */
    public boolean isEagerUploadChecksEnabled() throws OXException {
        ConfigurationService config = services.getServiceSafe(ConfigurationService.class);
        boolean def = CompositionSpaceProperties.MAILSTORAGE_EAGER_UPLOAD_CHECKS_ENABLED.getDefaultValue(Boolean.class).booleanValue();
        return config.getBoolProperty(CompositionSpaceProperties.MAILSTORAGE_EAGER_UPLOAD_CHECKS_ENABLED.getFQPropertyName(),def);
    }

    /**
     * Gets the default timeout for reading responses from mail server.
     *
     * @return The default timeout for reading responses
     * @throws OXException If property cannot be read
     */
    public long getDefaultReadResponseTimeout() throws OXException {
        ConfigurationService config = services.getServiceSafe(ConfigurationService.class);
        long def = CompositionSpaceProperties.MAILSTORAGE_DEFAULT_READ_RESPONSE_TIMEOUT.getDefaultValue(Long.class).longValue();
        return config.getLongProperty(CompositionSpaceProperties.MAILSTORAGE_DEFAULT_READ_RESPONSE_TIMEOUT.getFQPropertyName(), def);
    }

    /**
     * Gets the long timeout for reading responses from mail server.
     *
     * @return The long timeout for reading responses
     * @throws OXException If property cannot be read
     */
    public long getLongReadResponseTimeout() throws OXException {
        ConfigurationService config = services.getServiceSafe(ConfigurationService.class);
        long def = CompositionSpaceProperties.MAILSTORAGE_LONG_READ_RESPONSE_TIMEOUT.getDefaultValue(Long.class).longValue();
        return config.getLongProperty(CompositionSpaceProperties.MAILSTORAGE_LONG_READ_RESPONSE_TIMEOUT.getFQPropertyName(), def);
    }

    /**
     * Gets the operation wait time when waiting for concurrent operations to complete.
     *
     * @return The  operation wait time
     * @throws OXException If property cannot be read
     */
    public long getOperationWaitTime() throws OXException {
        ConfigurationService config = services.getServiceSafe(ConfigurationService.class);
        long def = CompositionSpaceProperties.MAILSTORAGE_OPERATION_WAIT_TIME.getDefaultValue(Long.class).longValue();
        return config.getLongProperty(CompositionSpaceProperties.MAILSTORAGE_OPERATION_WAIT_TIME.getFQPropertyName(), def);
    }

    // --------------------------------------------------- Reloadable stuff ----------------------------------------------------------------

    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        this.spoolDir = null;
        this.fileCacheDir = null;
    }

}
