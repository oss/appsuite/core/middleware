apiVersion: v1
kind: Pod
metadata:
  labels:
    name: image-build
spec:
  automountServiceAccountToken: false
  containers:
    - name: gradle
      image: docker.io/library/gradle:8.7-jdk21 # docker.io is used for the hetzner build
      command:
        - /toox/server
      args:
        - gradle
      env:
        - name: TOOX_TOOLS
          value: buildah
      resources:
        limits:
          memory: 10Gi
        requests:
          cpu: 3
          memory: 10Gi
      volumeMounts:
        - mountPath: /git-mirror
          name: git-mirror
        - mountPath: /toox
          name: toox
    - name: ant
      image: docker.io/frekele/ant:1.9.11-jdk8 # docker.io is used for the hetzner build
      command:
        - sleep
      args:
        - infinity
      resources:
        limits:
          memory: 512Mi
        requests:
          memory: 512Mi
    - name: jnlp
      env:
        - name: JAVA_OPTS
          value: "-XX:MaxRAMPercentage=50"
      resources:
        requests:
          cpu: 100m
          memory: 1Gi
        limits:
          memory: 2Gi
      volumeMounts:
        - mountPath: /git-mirror
          name: git-mirror
    - name: buildah
      image: quay.io/containers/buildah:v1.37.0
      command:
        - /toox/server
      args:
        - buildah
      env:
        - name: REGISTRY_AUTH_FILE
          value: /auth/config.json
      resources:
        limits:
          memory: 2Gi
        requests:
          cpu: 1
          memory: 2Gi
      volumeMounts:
        - mountPath: /toox
          name: toox
        - name: appsuite-core-internal-cred
          mountPath: /auth
      securityContext:
        privileged: true
    - name: toox-proxy
      image: registry.gitlab.open-xchange.com/engineering/tools/toox:0.15.4
      command:
        - /toox/proxy
      env:
        - name: TOOX_TOOLS
          value: gradle,buildah
      resources:
        requests:
          cpu: 100m
          memory: 32Mi
        limits:
          memory: 32Mi
      volumeMounts:
        - mountPath: /toox
          name: toox
    - name: jekyll
      image: registry.gitlab.open-xchange.com/engineering/documentation:latest
      command:
        - sleep
      args:
        - 365d
      resources:
        limits:
          memory: 512Mi
        requests:
          memory: 512Mi
      securityContext:
        allowPrivilegeEscalation: false
        capabilities:
          drop:
            - all
    - name: gettext
      image: registry.gitlab.open-xchange.com/docker/gettext:2.0
      command:
        - /toox/server
      args:
        - msgcat
        - xgettext
      env:
        - name: TOOX_TOOLS
          value: ant
      resources:
        limits:
          memory: 16Mi
        requests:
          cpu: 100m
          memory: 16Mi
      securityContext:
        allowPrivilegeEscalation: false
        capabilities:
          drop:
            - all
      volumeMounts:
        - mountPath: /toox
          name: toox
    - name: k8s
      image: registry.gitlab.open-xchange.com/appsuite/platform/docker/k8s-cloud-toolkit:latest
      imagePullPolicy: Always
      command:
        - sleep
      args:
        - infinity
      resources:
        limits:
          memory: 2048Mi
        requests:
          cpu: 100m
          memory: 2048Mi
      securityContext:
        allowPrivilegeEscalation: false
        capabilities:
          drop:
            - all
      volumeMounts:
        - mountPath: /git-mirror
          name: git-mirror
  imagePullSecrets:
    - name: gitlab
  initContainers:
    - name: toox-init
      image: registry.gitlab.open-xchange.com/engineering/tools/toox:0.15.4
      command:
        - /toox/init
      volumeMounts:
        - mountPath: /init
          name: toox
      workingDir: /init
  volumes:
    - name: git-mirror
      persistentVolumeClaim:
        claimName: middleware-core-pvc
    - name: toox
      emptyDir: {}
    - name: appsuite-core-internal-cred
      projected:
        sources:
          - secret:
              name: appsuite-core-internal-cred
              items:
                - key: .dockerconfigjson
                  path: config.json