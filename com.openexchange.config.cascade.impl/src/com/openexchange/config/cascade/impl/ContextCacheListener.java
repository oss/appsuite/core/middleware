/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.config.cascade.impl;

import java.util.List;
import org.osgi.framework.BundleContext;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.cache.v2.events.AbstractTrackingCacheEventListener;
import com.openexchange.cache.v2.events.CacheEvent;
import com.openexchange.cache.v2.events.CacheEventInterest;
import com.openexchange.cache.v2.events.CacheEvents;
import com.openexchange.cache.v2.events.DefaultCacheEventInterest;
import com.openexchange.cache.v2.filter.CacheFilter;

/**
 * {@link ContextCacheListener} - Listens to events from the {@link CoreModuleName#CONTEXT} cache to invalidate the config-cascade cache as needed.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class ContextCacheListener extends AbstractTrackingCacheEventListener {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextCacheListener.class);

    /** The interest for all events in the "context" core module cache */
    private static final CacheEventInterest EVENT_INTEREST = DefaultCacheEventInterest.builder()
                                                                                      .withOnlyRemote(false)
                                                                                      .withFilter(CacheFilter.builder()
                                                                                                             .withCoreModuleName(CoreModuleName.CONTEXT)
                                                                                                             .build())
                                                                                      .build();

    /**
     * Initializes a new {@link ContextCacheListener}.
     *
     * @param context The bundle context
     */
    public ContextCacheListener(BundleContext context) {
        super(context);
    }

    @Override
    public CacheEventInterest getInterest() {
        return EVENT_INTEREST;
    }

    @Override
    public void onCacheEvent(CacheEvent cacheEvent) {
        LOG.trace("Received cache event: {}", cacheEvent);
        if (null == cacheEvent) {
            return;
        }
        List<CacheKey> keys = cacheEvent.getKeys();
        if (null == keys || keys.isEmpty()) {
            return;
        }
        for (CacheKey key : keys) {
            CacheEvents.extractIntSuffixes(key, CoreModuleName.CONTEXT, 1).ifPresent(ContextCacheListener::clearCachedValuesOfContext);
        }
    }

    private static void clearCachedValuesOfContext(int[] cacheKeySuffixes) {
        if (null != cacheKeySuffixes && 1 == cacheKeySuffixes.length) {
            ConfigCascade.clearCachedValuesOfContext(cacheKeySuffixes[0]);
        }
    }

}
