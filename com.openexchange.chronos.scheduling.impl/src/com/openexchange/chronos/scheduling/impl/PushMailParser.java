/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.chronos.scheduling.impl;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.chronos.scheduling.PushedIMipResolveMode;
import com.openexchange.chronos.scheduling.common.PushMail;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.ldap.LdapExceptionCode;
import com.openexchange.java.Strings;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.login.resolver.MailLoginResolverService;
import com.openexchange.mail.login.resolver.ResolverResult;
import com.openexchange.mail.mime.MimeDefaultSession;
import com.openexchange.mail.mime.converters.MimeMessageConverter;
import com.openexchange.mailmapping.MailResolverService;
import com.openexchange.mailmapping.ResolvedMail;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.functions.ErrorAwareSupplier;
import com.openexchange.user.UserExceptionCode;
import com.openexchange.user.UserService;

/**
 * {@link PushMailParser}
 *
 * @author <a href="mailto:martin.herfurthr@open-xchange.com">Martin Herfurth</a>
 *
 */
public class PushMailParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(PushMailParser.class);

    public static boolean isValid(JSONObject json) {
        try {
            if (json.has("event") && !json.getString("event").equalsIgnoreCase("messageNew")) {
                return false;
            }
        } catch (JSONException e) {
            LOGGER.debug("Invalid json: {}", e.getMessage(), e);
            return false;
        }
        if (!json.has("body") || !json.has("folder")) {
            return false;
        }
        return true;
    }

    public static PushMail parse(JSONObject json) throws OXException {
        isValid(json);
        PushMail pushMail = new PushMail();
        try {
            if (json.has("event")) {
                pushMail.setEvent(json.getString("event"));
            }
            try (ByteArrayInputStream is = new ByteArrayInputStream(json.getString("body").getBytes(StandardCharsets.UTF_8))) {
                MimeMessage msg = new MimeMessage(MimeDefaultSession.getDefaultSession(), is);
                MailMessage mailMessage = MimeMessageConverter.convertMessage(msg);
                mailMessage.setFolder(json.getString("folder"));
                pushMail.setMail(mailMessage);
                if (json.has("user")) {
                    pushMail.setUser(json.getString("user"));
                }
                return pushMail;
            }
        } catch (JSONException | MessagingException | IOException e) {
            LOGGER.debug("Bad request: {}", e.getMessage(), e);
        }
        return null;
    }

    /**
     * Resolves a User depending on {@link PushedIMipResolveMode}
     *
     * @param user The pushed User
     * @param services A {@link ServiceLookup}
     * @param recipientsSupplier An {@link ErrorAwareSupplier} to provide a List of recipients
     * @return A list of {@link ResolvedMail}s
     * @throws OXException In case of an error
     */
    public static List<ResolvedMail> resolveUser(ServiceLookup services, String user, ErrorAwareSupplier<List<String>> recipientsSupplier) throws OXException {
        LeanConfigurationService configService = services.getService(LeanConfigurationService.class);
        if (configService == null) {
            LOGGER.debug("Missing service {}.", LeanConfigurationService.class.getSimpleName());
            return emptyList();
        }

        PushedIMipResolveMode mode = PushedIMipResolveMode.getConfiguredValue(configService);
        switch (mode) {
            case SYNTHETIC:
                return synthetic(user);
            case LOGININFO:
                return loginInfo(user, services);
            case MAILLOGIN:
                return mailLogin(user, services);
            case RECIPIENTONLY:
            default:
                return recipientOnly(recipientsSupplier, services);
        }
    }

    private static List<ResolvedMail> recipientOnly(ErrorAwareSupplier<List<String>> recipientsSupplier, ServiceLookup services) throws OXException {
        List<String> recipients = recipientsSupplier.get();
        if (recipients == null || recipients.isEmpty()) {
            return emptyList();
        }
        int size = recipients.size();
        ArrayList<ResolvedMail> result = new ArrayList<>(size);
        MailResolverService mailResolver = services.getServiceSafe(MailResolverService.class);
        for (ResolvedMail resolvedMail : mailResolver.resolveMultiple(recipients.toArray(new String[size]))) {
            if (null != resolvedMail && resolvedMail.getUserID() > 0 && resolvedMail.getContextID() > 0) {
                result.add(resolvedMail);
            }
        }
        return result;
    }

    private static List<ResolvedMail> mailLogin(String user, ServiceLookup services) throws OXException {
        MailLoginResolverService mailLoginResolver = services.getServiceSafe(MailLoginResolverService.class);
        ResolverResult result = mailLoginResolver.resolveMailLogin(-1, user);
        return singletonList(new ResolvedMail(result.getUserId(), result.getContextId()));
    }

    private static List<ResolvedMail> synthetic(String user) {
        if (Strings.isEmpty(user) || user.indexOf('@') < 0) {
            LOGGER.debug("Invalid user format for systhetic mode.");
            return emptyList();
        }
        String[] split = Strings.splitBy(user, '@', false);
        if (split.length != 2) {
            LOGGER.debug("Invalid user format for systhetic mode.");
            return emptyList();
        }
        try {
            return singletonList(new ResolvedMail(Integer.parseInt(split[0].trim()), Integer.parseInt(split[1].trim())));
        } catch (NumberFormatException nfe) {
            LOGGER.debug("Invalid user format for systhetic mode. {}", nfe.getMessage(), nfe);
            return emptyList();
        }
    }

    private static List<ResolvedMail> loginInfo(String user, ServiceLookup services) throws OXException {
        ContextService contextService = services.getServiceSafe(ContextService.class);
        UserService userService = services.getServiceSafe(UserService.class);
        if (Strings.isEmpty(user)) {
            return emptyList();
        }
        String[] logininfo = split(user);
        int ctxId = contextService.getContextId(logininfo[1]);
        if (ctxId == -1) {
            LOGGER.debug("Missing context mapping for context \"{}\". Login failed.", logininfo[1]);
            return emptyList();
        }
        Context ctx = contextService.getContext(ctxId);

        int userId;
        try {
            userId = userService.getUserId(logininfo[0], ctx);
        } catch (OXException e) {
            if (e.equalsCode(LdapExceptionCode.USER_NOT_FOUND.getNumber(), UserExceptionCode.PROPERTY_MISSING.getPrefix())) {
                LOGGER.debug("Missing user mapping for user \"{}\". Login failed.", logininfo[0]);
                return emptyList();
            }
            throw e;
        }
        return singletonList(new ResolvedMail(userId, ctxId));
    }

    /**
     * Splits user name and context.
     *
     * @param loginInfo the composite login information separated by an <code>'@'</code> sign
     * @return A String Array providing context and user name
     * @throws OXException If no separator is found
     */
    private static String[] split(final String loginInfo) {
        int pos = loginInfo.lastIndexOf('@');
        String[] retval = new String[2];
        if (pos < 0) {
            retval[0] = loginInfo;
            retval[1] = "defaultcontext";
        } else {
            retval[0] = loginInfo.substring(0, pos);
            retval[1] = loginInfo.substring(pos + 1);
        }
        return retval;
    }
}
