/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.scheduling.impl.request.analyzer;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.tools.arrays.Arrays.isNullOrEmpty;
import static java.util.Collections.emptyList;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.IDN;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.Path;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.chronos.scheduling.impl.PushMailParser;
import com.openexchange.chronos.scheduling.impl.push.PushMailREST;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.mail.mime.MessageHeaders;
import com.openexchange.mail.mime.MimeDefaultSession;
import com.openexchange.mail.mime.MimeMailException;
import com.openexchange.mail.mime.utils.MimeMessageUtility;
import com.openexchange.mailmapping.ResolvedMail;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.request.analyzer.UserInfo;
import com.openexchange.segment.SegmentMarker;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 *
 * {@link IMipPushRequestAnalyzer}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 */
public class IMipPushRequestAnalyzer implements RequestAnalyzer {

    Logger LOG = LoggerFactory.getLogger(IMipPushRequestAnalyzer.class);

    private final ServiceLookup services;
    private final ErrorAwareSupplier<DatabaseService> dbServiceSupplier;
    private final String servletPath;

    public IMipPushRequestAnalyzer(ServiceLookup services) {
        super();
        this.services = services;
        dbServiceSupplier = () -> services.getServiceSafe(DatabaseService.class);
        this.servletPath = PushMailREST.class.getAnnotation(Path.class).value();
    }

    @Override
    public Optional<AnalyzeResult> analyze(RequestData data) throws OXException {
        if (!"PUT".equals(data.getMethod())) {
            return Optional.empty();
        }
        Optional<String> path = data.getParsedURL().getPath();
        if (path.isEmpty() || false == path.get().startsWith(servletPath)) {
            return Optional.empty();
        }
        if (!data.optBody().isPresent()) {
            return Optional.of(AnalyzeResult.MISSING_BODY);
        }

        final JSONObject jsonBody;
        try {
            jsonBody = JSONServices.parseObject(data.optBody().get().getDataAsString());
            if (!jsonBody.has("user")) {
                return Optional.of(AnalyzeResult.UNKNOWN);
            }
            String user = jsonBody.getString("user");
            List<ResolvedMail> resolvedMails = PushMailParser.resolveUser(services, user, () -> getRecipients(jsonBody));
            if (resolvedMails.size() < 1) {
                return Optional.of(AnalyzeResult.UNKNOWN);
            }
            if (resolvedMails.size() > 1) {
                LOG.info("Found {} users, use first.", I(resolvedMails.size()));
                Set<String> schemata = new HashSet<>();
                for (ResolvedMail resolvedMail : resolvedMails) {
                    int contextId = resolvedMail.getContextID();
                    schemata.add(dbServiceSupplier.get().getSchemaName(contextId));
                }
                if (schemata.size() > 1) {
                    LOG.warn("Found multiple schemata: {}", schemata);
                }
            }

            ResolvedMail resolvedMail = resolvedMails.get(0);
            int userId = resolvedMail.getUserID();
            int contextId = resolvedMail.getContextID();
            String schema = dbServiceSupplier.get().getSchemaName(contextId);
            return Optional.of(new AnalyzeResult(SegmentMarker.of(schema), UserInfo.builder(contextId).withUserId(userId).build()));
        } catch (JSONException | IOException e) {
            LOG.debug("Unable to analyze request.", e);
            return Optional.of(AnalyzeResult.UNKNOWN);
        }
    }

    private List<String> getRecipients(JSONObject jsonBody) throws OXException {
        MimeMessage message = null;
        try (ByteArrayInputStream is = new ByteArrayInputStream(jsonBody.getString("body").getBytes(StandardCharsets.UTF_8))) {
            message = new MimeMessage(MimeDefaultSession.getDefaultSession(), is);
        } catch (JSONException | MessagingException | IOException e) {
            LOG.debug("Bad request: {}", e.getMessage(), e);
        }
        if (message == null) {
            return emptyList();
        }
        InternetAddress[] to = null;
        try {
            to = MimeMessageUtility.getAddressHeader(MessageHeaders.HDR_TO, message);
        } catch (MessagingException e) {
            throw MimeMailException.handleMessagingException(e);
        }
        if (isNullOrEmpty(to)) {
            return emptyList();
        }
        int size = to.length;
        ArrayList<String> recipients = new ArrayList<>(size);
        for (InternetAddress internetAddress : to) {
            recipients.add(IDN.toUnicode(internetAddress.getAddress()));
        }
        return recipients;
    }

}
