/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.scheduling.impl.push;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.chronos.scheduling.common.PushMail;
import com.openexchange.chronos.scheduling.impl.PushMailParser;
import com.openexchange.chronos.scheduling.impl.incoming.IncomingSchedulingMailListener;
import com.openexchange.exception.OXException;
import com.openexchange.mail.api.MailConfig;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.threadpool.AbstractTask;
import com.openexchange.threadpool.ThreadPools;

/**
 *
 * {@link PushMailREST}
 *
 * @author <a href="mailto:martin.herfurthr@open-xchange.com">Martin Herfurth</a>
 * @since v7.10.6
 */
@Path("/chronos/v1/itip/pushmail")
@RoleAllowed(Role.BASIC_AUTHENTICATED)
public class PushMailREST {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(PushMailREST.class);
    }

    private final IncomingSchedulingMailListener mailPushListener;

    /**
     * Initializes a new {@link PushMailREST}.
     *
     * @param mailPushListener The mail listener to forward iMIP notifications to
     */
    public PushMailREST(IncomingSchedulingMailListener mailPushListener) {
        super();
        this.mailPushListener = mailPushListener;
    }

    /**
     * Notifies about a new iTIP or rather iMIP mail
     *
     * @param json The payload as JSON
     * @return <code>200</code> if the payload was paresed sucessfully, <code>400</code> if not
     * @throws OXException In case the message can't be parsed
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response notify(JSONObject json) throws OXException {
        if (!PushMailParser.isValid(json)) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        MailConfig.setPushMail(json.toString());
        PushMail pushMail = PushMailParser.parse(json);
        notify(pushMail);
        return Response.ok().build();
    }

    private void notify(PushMail pushMail) {
        ThreadPools.submitElseExecute(new AbstractTask<Void>() {

            @Override
            public Void call() throws Exception {
                try {
                    mailPushListener.pushMail(pushMail);
                } catch (OXException e) {
                    LoggerHolder.LOGGER.debug("Error while processing: {}", e.getMessage(), e);
                    throw e;
                }
                return null;
            }
        });
    }

}
