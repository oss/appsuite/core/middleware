/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.xml.util;

import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Utility class to provide securely configured DocumentBuilderFactory.
 *
 * @author <a href="mailto:alexander.schulze-ardey@open-xchange.com">Alexander Schulze-Ardey</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.5
 */
public final class XMLUtils {

    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(XMLUtils.class);

    /**
     * Initializes a new {@link XMLUtils}.
     */
    private XMLUtils() {
        super();
    }

    /**
     * Initializes a new instance of <code>DocumentBuilderFactory</code> with configuration settings suggested by OWASP.
     * <ul>
     * <li><a href="https://owasp.org/www-community/vulnerabilities/XML_External_Entity_(XXE)_Processing">XML External Entity (XXE) Processing</a>
     * <li><a href="https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html">XML External Entity Prevention Cheat Sheet</a>
     * </ul>
     *
     * @return A newly created <code>DocumentBuilderFactory</code> instance with a safe configuration applied
     */
    public static DocumentBuilderFactory newSafeDbf() {
        return safeDbf(DocumentBuilderFactory.newInstance()); // NOSONARLINT
    }

    /**
     * Configures specified <code>DocumentBuilderFactory</code> instance as suggested by OWASP.
     * <ul>
     * <li><a href="https://owasp.org/www-community/vulnerabilities/XML_External_Entity_(XXE)_Processing">XML External Entity (XXE) Processing</a>
     * <li><a href="https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html">XML External Entity Prevention Cheat Sheet</a>
     * </ul>
     *
     * @param dbf The <code>DocumentBuilderFactory</code> instance to configure
     * @return The given <code>DocumentBuilderFactory</code> instance with a safe configuration applied
     */
    public static DocumentBuilderFactory safeDbf(DocumentBuilderFactory dbf) {
        if (null == dbf) {
            return dbf;
        }

        // This is the PRIMARY defense. If DTDs (doctypes) are disallowed, almost all
        // XML entity attacks are prevented
        // Xerces 2 only - http://xerces.apache.org/xerces2-j/features.html#disallow-doctype-decl
        String name = "http://apache.org/xml/features/disallow-doctype-decl";
        setFeatureSafe(name, true, dbf);

        // If you can't completely disable DTDs, then at least do the following:
        // Xerces 1 - http://xerces.apache.org/xerces-j/features.html#external-general-entities
        // Xerces 2 - http://xerces.apache.org/xerces2-j/features.html#external-general-entities
        // JDK7+ - http://xml.org/sax/features/external-general-entities
        // This feature has to be used together with the following one, otherwise it will not protect you from XXE for sure
        name = "http://xml.org/sax/features/external-general-entities";
        setFeatureSafe(name, false, dbf);

        // Xerces 1 - http://xerces.apache.org/xerces-j/features.html#external-parameter-entities
        // Xerces 2 - http://xerces.apache.org/xerces2-j/features.html#external-parameter-entities
        // JDK7+ - http://xml.org/sax/features/external-parameter-entities
        // This feature has to be used together with the previous one, otherwise it will not protect you from XXE for sure
        name = "http://xml.org/sax/features/external-parameter-entities";
        setFeatureSafe(name, false, dbf);

        // Disable external DTDs as well
        name = "http://apache.org/xml/features/nonvalidating/load-external-dtd";
        setFeatureSafe(name, false, dbf);

        // and these as well, per Timothy Morgan's 2014 paper: "XML Schema, DTD, and Entity Attacks"
        dbf.setXIncludeAware(false);
        dbf.setExpandEntityReferences(false);

        return dbf;
    }

    private static void setFeatureSafe(String name, boolean value, DocumentBuilderFactory dbf) {
        try {
            dbf.setFeature(name, value);
        } catch (Exception e) {
            LOGGER.error("Failed to set feature '{}' for DocumentBuilderFactory instance. That feature is probably not supported by your XML processor.", name, e);
        }
    }

}
