/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.plugin.hosting.rmi.impl.async;

import static com.openexchange.java.Autoboxing.I;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import com.openexchange.admin.async.AsyncTask;
import com.openexchange.admin.async.AsyncTaskExecutor;
import com.openexchange.admin.metrics.PluginMetricsProcessor;
import com.openexchange.admin.plugin.hosting.rmi.impl.OXContext;
import com.openexchange.admin.plugin.hosting.services.PluginInterfaces;
import com.openexchange.admin.plugin.hosting.storage.interfaces.OXContextStorageInterface;
import com.openexchange.admin.plugins.OXContextPluginInterface;
import com.openexchange.admin.plugins.OXContextPluginInterfaceExtended;
import com.openexchange.admin.plugins.PluginException;
import com.openexchange.admin.rmi.dataobjects.Context;
import com.openexchange.admin.rmi.dataobjects.Credentials;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.rmi.impl.BasicAuthenticator;
import com.openexchange.admin.storage.interfaces.OXToolStorageInterface;
import com.openexchange.exception.OXException;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;

/**
 * {@link AsyncDeletionExecutor}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class AsyncDeletionExecutor implements AsyncTaskExecutor {

    /** The logger */
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(OXContext.class);

    @Override
    public String getType() {
        return "ctx_deletion";
    }

    @Override
    public void execute(AsyncTask task) throws OXException {
        String args = task.getArgs();
        try {
            Credentials creds = CredentialsParser.convert(args);
            Context context = toContext(task);
            delete(context, creds);
        } catch (JSONException e) {
            LOG.error("Invalid arguments found for task '{}'", I(task.getId()));
            throw OXException.general(e.getMessage(), e);
        } catch (StorageException e) {
            throw OXException.general(e.getMessage(), e);
        } catch (PluginException e) {
            throw OXException.general(e.getMessage(), e);
        }
    }

    /**
     * Deletes the given context
     *
     * @param ctx The context to delete
     * @param auth The credentials
     * @throws StorageException in case a storage error occurs
     * @throws PluginException in case a plugin error occurs
     */
    public void delete(final Context ctx, Credentials auth) throws StorageException, PluginException {
        if (OXToolStorageInterface.getInstance().existsContext(ctx) == false) {
            LOG.debug("The context {} does not exist. It has probably already been deleted.", ctx.getId());
            return;
        }

        final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
        // Trigger plug-in extensions for pre-deletion
        Map<OXContextPluginInterfaceExtended, Map<String, Object>> undeleteInfos = null;
        {
            final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
            if (null != pluginInterfaces) {
                List<OXContextPluginInterface> plugins = pluginInterfaces.getContextPlugins().getServiceList();
                for (final OXContextPluginInterface oxContextPlugin : plugins) {
                    undeleteInfos = triggerPluginDeletion(ctx, auth, undeleteInfos, plugins, oxContextPlugin);
                }
            }
        }

        if (undeleteInfos == null) {
            oxcox.delete(ctx);
        } else {
            boolean deleted = false;
            try {
                oxcox.delete(ctx);
                deleted = true;
            } finally {
                if (!deleted) {
                    undoDelete(ctx, undeleteInfos);
                }
            }
        }

        BasicAuthenticator.createPluginAwareAuthenticator().removeFromAuthCache(ctx);
    }

    private Map<OXContextPluginInterfaceExtended, Map<String, Object>> triggerPluginDeletion(final Context ctx, Credentials auth, Map<OXContextPluginInterfaceExtended, Map<String, Object>> undeleteInfos, List<OXContextPluginInterface> plugins, final OXContextPluginInterface oxContextPlugin) throws PluginException {
        Map<OXContextPluginInterfaceExtended, Map<String, Object>> result = undeleteInfos;
        if (oxContextPlugin instanceof OXContextPluginInterfaceExtended extended) {
            try {
                result = triggerExtendedPluginDeletion(ctx, auth, plugins, oxContextPlugin, result, extended);
            } catch (PluginException e) {
                undoDelete(ctx, result);
                throw e;
            }
            return result;
        }

        try {
            Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
            boolean success = false;
            try {
                oxContextPlugin.delete(ctx, auth);
                success = true;
            } finally {
                PluginMetricsProcessor.getForDelete(oxContextPlugin.getClass().getName(), "delete").stopTimer(timerSample, success);
            }
        } catch (PluginException e) {
            undoDelete(ctx, result);
            throw e;
        }
        return result;
    }

    private Map<OXContextPluginInterfaceExtended, Map<String, Object>> triggerExtendedPluginDeletion(final Context ctx, Credentials auth, List<OXContextPluginInterface> plugins, final OXContextPluginInterface oxContextPlugin, Map<OXContextPluginInterfaceExtended, Map<String, Object>> undeleteInfos, OXContextPluginInterfaceExtended extended) throws PluginException {
        Map<OXContextPluginInterfaceExtended, Map<String, Object>> result = undeleteInfos;
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Map<String, Object> undoInfo = extended.undoableDelete(ctx, auth);
            if (undoInfo != null) {
                if (result == null) {
                    result = LinkedHashMap.newLinkedHashMap(plugins.size());
                }
                result.put(extended, undoInfo);
            }
            success = true;
        } finally {
            PluginMetricsProcessor.getForDelete(oxContextPlugin.getClass().getName(), "undoableDelete").stopTimer(timerSample, success);
        }
        return result;
    }

    /**
     * Performs an undo
     *
     * @param ctx The context
     * @param undeleteInformation The undelete information
     */
    private void undoDelete(final Context ctx, Map<OXContextPluginInterfaceExtended, Map<String, Object>> undeleteInformation) {
        if (undeleteInformation == null) {
            return;
        }
        for (Map.Entry<OXContextPluginInterfaceExtended, Map<String, Object>> undeleteInfo : undeleteInformation.entrySet()) {
            try {
                Map<String, Object> undoInfo = undeleteInfo.getValue();
                OXContextPluginInterfaceExtended key = undeleteInfo.getKey();
                Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                boolean success = false;
                try {
                    key.undelete(ctx, undoInfo);
                    success = true;
                } finally {
                    PluginMetricsProcessor.getForCreate(key.getClass().getName(), "undelete").stopTimer(timerSample, success);
                }
            } catch (PluginException x) {
                LOG.warn("Undeletion failed", x);
            }
        }
    }

    private Context toContext(AsyncTask task) throws OXException {
        String id = task.getEntityId();
        try {
            return new Context(Integer.valueOf(id));
        } catch (NumberFormatException e) {
            throw OXException.general("Unexpected entity format error", e);
        }
    }

}
