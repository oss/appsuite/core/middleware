/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.admin.plugin.hosting.rmi.impl.async;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import com.openexchange.admin.rmi.dataobjects.Credentials;

/**
 * {@link CredentialsParser} is a helper class which helps to
 * convert {@link Credentials} to arguments and back
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class CredentialsParser {

    // Credential fields
    private static final String CRED = "cred";
    private static final String LOGIN = "login";


    /**
     * Converts the given args into a {@link Credentials} object
     *
     * @param args The arguments to parse
     * @return The {@link Credentials}
     * @throws JSONException in case of errors
     */
    public static Credentials convert(String args) throws JSONException {
        JSONObject json = JSONServices.parseObject(args);
        return parseCredentials(json.getJSONObject(CRED));
    }

    /**
     * Converts the given {@link Credentials} to an arguments string
     *
     * @param credentials The {@link Credentials} to convert
     * @return The converted arguments
     */
    public static String convert(Credentials credentials) {
        try {
            JSONObject result = new JSONObject();
            result.put(CRED, parseCredentials(credentials));
            return result.toString();
        } catch (JSONException e) {
            // Should not happen
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    // -------------------- private methods ------------------

    /**
     * parseCredentials
     *
     * @param credentials
     * @return
     * @throws JSONException
     */
    private static JSONObject parseCredentials(Credentials credentials) throws JSONException {
        JSONObject result = new JSONObject();
        result.put(LOGIN, credentials.getLogin());
        return result;
    }

    /**
     * Parses the credentials for this delete operation
     *
     * @param json The json object containing the login field
     * @return The credentials
     * @throws JSONException in case of errors
     */
    private static Credentials parseCredentials(JSONObject json) throws JSONException {
        String login = json.getString(LOGIN);
        return Credentials.alwaysAcceptFor(login);
    }

}
