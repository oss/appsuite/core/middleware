/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.plugin.hosting.rmi.impl;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.I2i;
import static com.openexchange.java.Autoboxing.i;
import static com.openexchange.java.Autoboxing.i2I;
import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.RejectedExecutionException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.openexchange.admin.async.AsyncTask;
import com.openexchange.admin.async.AsyncTaskService;
import com.openexchange.admin.daemons.ClientAdminThread;
import com.openexchange.admin.daemons.ClientAdminThreadExtended;
import com.openexchange.admin.metrics.APIMetricsProcessor;
import com.openexchange.admin.metrics.PluginMetricsProcessor;
import com.openexchange.admin.plugin.hosting.rmi.impl.async.AsyncDeletionExecutor;
import com.openexchange.admin.plugin.hosting.rmi.impl.async.CredentialsParser;
import com.openexchange.admin.plugin.hosting.rmi.impl.async.config.AsyncContextProvisioningProperties;
import com.openexchange.admin.plugin.hosting.services.AdminServiceRegistry;
import com.openexchange.admin.plugin.hosting.services.PluginInterfaces;
import com.openexchange.admin.plugin.hosting.storage.interfaces.OXContextStorageInterface;
import com.openexchange.admin.plugin.hosting.tools.DatabaseDataMover;
import com.openexchange.admin.plugins.OXContextPluginInterface;
import com.openexchange.admin.plugins.OXContextPluginInterfaceExtended;
import com.openexchange.admin.plugins.PluginException;
import com.openexchange.admin.rmi.OXContextInterface;
import com.openexchange.admin.rmi.dataobjects.Context;
import com.openexchange.admin.rmi.dataobjects.Credentials;
import com.openexchange.admin.rmi.dataobjects.Database;
import com.openexchange.admin.rmi.dataobjects.Filestore;
import com.openexchange.admin.rmi.dataobjects.MaintenanceReason;
import com.openexchange.admin.rmi.dataobjects.NameAndIdObject;
import com.openexchange.admin.rmi.dataobjects.Quota;
import com.openexchange.admin.rmi.dataobjects.SchemaSelectStrategy;
import com.openexchange.admin.rmi.dataobjects.User;
import com.openexchange.admin.rmi.dataobjects.UserModuleAccess;
import com.openexchange.admin.rmi.exceptions.AbstractAdminRmiException;
import com.openexchange.admin.rmi.exceptions.ContextExistsException;
import com.openexchange.admin.rmi.exceptions.DatabaseUpdateException;
import com.openexchange.admin.rmi.exceptions.DuplicateExtensionException;
import com.openexchange.admin.rmi.exceptions.EnforceableDataObjectException;
import com.openexchange.admin.rmi.exceptions.InvalidCredentialsException;
import com.openexchange.admin.rmi.exceptions.InvalidDataException;
import com.openexchange.admin.rmi.exceptions.NoSuchContextException;
import com.openexchange.admin.rmi.exceptions.NoSuchDatabaseException;
import com.openexchange.admin.rmi.exceptions.NoSuchFilestoreException;
import com.openexchange.admin.rmi.exceptions.NoSuchObjectException;
import com.openexchange.admin.rmi.exceptions.NoSuchReasonException;
import com.openexchange.admin.rmi.exceptions.OXContextException;
import com.openexchange.admin.rmi.exceptions.RemoteExceptionUtils;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.rmi.extensions.OXCommonExtension;
import com.openexchange.admin.rmi.impl.BasicAuthenticator;
import com.openexchange.admin.rmi.impl.ClaimedContext;
import com.openexchange.admin.rmi.impl.OXContextCommonImpl;
import com.openexchange.admin.storage.interfaces.OXUserStorageInterface;
import com.openexchange.admin.storage.interfaces.OXUtilStorageInterface;
import com.openexchange.admin.taskmanagement.TaskManager;
import com.openexchange.admin.tools.AdminCache;
import com.openexchange.admin.tools.CacheHelper;
import com.openexchange.admin.tools.filestore.FilestoreDataMover;
import com.openexchange.config.cascade.ConfigProperty;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.config.cascade.ConfigViewScope;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.LogLevel;
import com.openexchange.exception.OXException;
import com.openexchange.filestore.FileStorages;
import com.openexchange.groupware.contexts.impl.ContextStorage;
import com.openexchange.java.Strings;
import com.openexchange.java.Suppliers.OXSupplier;
import com.openexchange.log.Constants;
import com.openexchange.log.extensions.ParsableLogCreator;
import com.openexchange.log.extensions.ParsableLogCreator.ObjectType;
import com.openexchange.log.extensions.ParsableLogCreator.Operation;
import com.openexchange.segment.SegmentMarker;
import com.openexchange.segmenter.client.SegmenterService;
import com.openexchange.tools.pipesnfilters.Filter;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;

public class OXContext extends OXContextCommonImpl implements OXContextInterface {

    /** The logger */
    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(OXContext.class);

    /** The logger for logging extensions */
    private static final org.slf4j.Logger EXTENSIONS_LOGGER = org.slf4j.LoggerFactory.getLogger("com.openexchange.logging.extensions.OXContext");

    private final AdminCache cache;

    private final AsyncDeletionExecutor deletionExecutor;

    /**
     * Initializes a new {@link OXContext}.
     *
     * @param deletionExecutor The {@link AsyncDeletionExecutor} used to delete contexts
     */
    public OXContext(AsyncDeletionExecutor deletionExecutor) {
        super();
        this.deletionExecutor = deletionExecutor;
        cache = ClientAdminThread.cache;
        log(LogLevel.DEBUG, LOGGER, null, null, "Class loaded: {}", this.getClass().getName());
    }

    private void logAndEnhanceException(Throwable t, final Credentials credentials) {
        logAndEnhanceException(t, credentials, (String) null);
    }

    private void logAndEnhanceException(Throwable t, final Credentials credentials, final Context ctx) {
        logAndEnhanceException(t, credentials, null != ctx ? ctx.getIdAsString() : null);
    }

    private void logAndEnhanceException(Throwable t, final Credentials credentials, final String contextId) {
        if (t instanceof AbstractAdminRmiException) {
            logAndReturnException(LOGGER, ((AbstractAdminRmiException) t), credentials, contextId);
        } else if (t instanceof RemoteException remoteException) {
            String exceptionId = AbstractAdminRmiException.generateExceptionId();
            RemoteExceptionUtils.enhanceRemoteException(remoteException, exceptionId);
            logAndReturnException(LOGGER, remoteException, exceptionId, credentials, contextId);
        } else if (t instanceof Exception) {
            RemoteException remoteException = RemoteExceptionUtils.convertException((Exception) t);
            String exceptionId = AbstractAdminRmiException.generateExceptionId();
            RemoteExceptionUtils.enhanceRemoteException(remoteException, exceptionId);
            logAndReturnException(LOGGER, remoteException, exceptionId, credentials, contextId);
        }
    }

    @Override
    public Quota[] listQuotas(Context ctx, Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Quota[] listQuotasInternal = listQuotasInternal(ctx, credentials);
            success = true;
            return listQuotasInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private Quota[] listQuotasInternal(Context ctx, Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        if (null == ctx) {
            InvalidDataException invalidDataException = new InvalidDataException("Context is null");
            log(LogLevel.ERROR, LOGGER, auth, invalidDataException, "");
            throw invalidDataException;
        }

        try {
            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            BasicAuthenticator.createNonPluginAwareAuthenticator().doAuthentication(auth);

            checkExistence(ctx);
            OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
            return oxcox.listQuotas(ctx);
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public void changeQuota(final Context ctx, final String sModule, final long quotaValue, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            changeQuotaInternal(ctx, sModule, quotaValue, credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CHANGE)
                .withObjectType(ObjectType.QUOTA)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private void changeQuotaInternal(final Context ctx, final String sModule, final long quotaValue, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        try {
            final Credentials auth = credentials == null ? new Credentials("", "") : credentials;
            if (com.openexchange.java.Strings.isEmpty(sModule)) {
                throw new InvalidDataException("No valid module specified.");
            }
            final String[] mods = Strings.splitByComma(sModule);
            final Set<String> modules = new LinkedHashSet<>(mods.length);
            for (final String mod : mods) {
                modules.add(mod);
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            final long quota;
            if (quotaValue < 0) {
                quota = -1L;
            } else {
                // MySQL int(10) unsigned: the allowable range is from 0 to 4294967295
                if (quotaValue > 4294967295L) {
                    throw new InvalidDataException("Quota value is out of range (allowable range is from 0 to 4294967295): " + quotaValue);
                }
                quota = quotaValue;
            }

            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, "{} - {} - {}", ctx, modules, Long.valueOf(quota));
            checkExistence(ctx);

            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            oxContextPlugin.changeQuota(ctx, sModule, quotaValue, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForChange(oxContextPlugin.getClass().getName(), "changeQuota").stopTimer(timerSample, success);
                        }
                    }
                }
            }

            final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
            oxcox.changeQuota(ctx, new ArrayList<>(modules), quota, auth);
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public Set<String> getCapabilities(final Context ctx, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Set<String> capabilitiesInternal = getCapabilitiesInternal(ctx, credentials);
            success = true;
            return capabilitiesInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private Set<String> getCapabilitiesInternal(final Context ctx, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        if (null == ctx) {
            throw new InvalidDataException("Missing context.");
        }

        try {
            final Credentials auth = credentials == null ? new Credentials("", "") : credentials;

            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e.getMessage());
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);
            checkExistence(ctx);
            final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
            return oxcox.getCapabilities(ctx);
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public void changeCapabilities(final Context ctx, Set<String> capsToAdd, Set<String> capsToRemove, Set<String> capsToDrop, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            changeCapabilitiesInternal(ctx, capsToAdd, capsToRemove, capsToDrop, credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CHANGE)
                .withObjectType(ObjectType.CAPABILITY)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private void changeCapabilitiesInternal(final Context ctx, Set<String> capsToAdd, Set<String> capsToRemove, Set<String> capsToDrop, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        if ((null == capsToAdd || capsToAdd.isEmpty()) && (null == capsToRemove || capsToRemove.isEmpty()) && (null == capsToDrop || capsToDrop.isEmpty())) {
            throw new InvalidDataException("No capabilities specified.");
        }

        try {
            final Set<String> capasToAdd = capsToAdd != null ? capsToAdd : Collections.emptySet();
            final Set<String> capasToRemove = capsToRemove != null ? capsToRemove : Collections.emptySet();
            final Set<String> capasToDrop = capsToDrop != null ? capsToDrop : Collections.emptySet();

            if (null == ctx) {
                throw new InvalidDataException("Missing context.");
            }
            final Credentials auth = credentials == null ? new Credentials("", "") : credentials;

            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, "{} - {} | {}", ctx, capasToAdd.toString(), capasToRemove.toString());
            checkExistence(ctx);
            checkCapabilities(Optional.ofNullable(capsToAdd), Optional.ofNullable(capsToRemove));

            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            oxContextPlugin.changeCapabilities(ctx, capasToAdd, capasToRemove, capasToDrop, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForChange(oxContextPlugin.getClass().getName(), "changeCapabilities").stopTimer(timerSample, success);
                        }
                    }
                }
            }

            final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
            oxcox.changeCapabilities(ctx, capasToAdd, capasToRemove, capasToDrop, auth);
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public void change(final Context ctx, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            changeInternal(ctx, credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CHANGE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private void changeInternal(final Context ctx, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        if (null == ctx) {
            InvalidDataException x = new InvalidDataException("context is null");
            log(LogLevel.ERROR, LOGGER, auth, x, "Context is invalid");
            throw x;
        }

        try {
            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            checkUserAttributes(ctx.getUserAttributes());

            validateloginmapping(ctx);

            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, ctx.toString());
            Set<String> loginMappings = null; // used for invalidating old login mappings in the cache
            if (ctx.getName() == null || Strings.isEmpty(ctx.getName())) {
                ctx.setName(null);
                checkExistence(ctx);
            } else {
                if (false == tool.checkContextName(ctx)) {
                    // Holds the same name
                    ctx.setName(null);
                }
            }

            // check if he wants to change the filestore id, if yes, make sure filestore with this id exists in the system
            if (ctx.getFilestoreId() != null) {
                if (!tool.existsStore(ctx.getFilestoreId().intValue())) {
                    final InvalidDataException inde = new InvalidDataException("No such filestore with id " + ctx.getFilestoreId());
                    throw logAndReturnException(LOGGER, inde, credentials, ctx.getIdAsString());
                }
            }

            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            oxContextPlugin.change(ctx, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForChange(oxContextPlugin.getClass().getName(), "change").stopTimer(timerSample, success);
                        }
                    }
                }
            }

            OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();

            // Check if login-mappings are supposed to be changed
            if (null != ctx.getLoginMappings()) {
                // Load old ones for invalidation purpose
                loginMappings = oxcox.getLoginMappings(ctx);
            }

            oxcox.change(ctx);
            try {
                final ContextStorage cs = ContextStorage.getInstance();
                cs.invalidateContext(ctx.getId().intValue());
                if (loginMappings != null && !loginMappings.isEmpty()) {
                    for (String loginMapping : loginMappings) {
                        cs.invalidateLoginInfo(loginMapping);
                    }
                }
            } catch (OXException e) {
                log(LogLevel.ERROR, LOGGER, credentials, ctx.getIdAsString(), null, e, "Error invalidating cached infos of context {} in context storage", ctx.getId());
            }
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public Context create(final Context ctx, final User admin_user, final Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, ContextExistsException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Context createInternal = createInternal(ctx, admin_user, credentials);
            success = true;
            return createInternal;
        } finally {
            APIMetricsProcessor.getForCreate().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CREATE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private Context createInternal(final Context ctx, final User admin_user, final Credentials credentials) throws StorageException, InvalidCredentialsException, InvalidDataException, ContextExistsException {
        try {
            return createcommon(ctx, admin_user, null, null, credentials, getDefaultSchemaSelectStrategy());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public Context create(final Context ctx, final User admin_user, final Credentials credentials, SchemaSelectStrategy schemaSelectStrategy) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, ContextExistsException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Context createInternal = createInternal(ctx, admin_user, credentials, schemaSelectStrategy);
            success = true;
            return createInternal;
        } finally {
            APIMetricsProcessor.getForCreate().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CREATE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private Context createInternal(final Context ctx, final User admin_user, final Credentials credentials, SchemaSelectStrategy schemaSelectStrategy) throws StorageException, InvalidCredentialsException, InvalidDataException, ContextExistsException {
        try {
            return createcommon(ctx, admin_user, null, null, credentials, schemaSelectStrategy);
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public Context create(final Context ctx, final User admin_user, final String access_combination_name, final Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, ContextExistsException {
        return create(ctx, admin_user, access_combination_name, credentials, getDefaultSchemaSelectStrategy());
    }

    @Override
    public Context create(final Context ctx, final User admin_user, final String access_combination_name, final Credentials credentials, SchemaSelectStrategy schemaSelectStrategy) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, ContextExistsException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Context createInternal = createInternal(ctx, admin_user, access_combination_name, credentials, schemaSelectStrategy);
            success = true;
            return createInternal;
        } finally {
            APIMetricsProcessor.getForCreate().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CREATE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private Context createInternal(final Context ctx, final User adminUser, final String access_combination_name, final Credentials credentials, SchemaSelectStrategy schemaSelectStrategy) throws StorageException, InvalidCredentialsException, InvalidDataException, ContextExistsException {
        try {
            final Credentials auth = credentials == null ? new Credentials("", "") : credentials;
            // Resolve access rights by name
            try {
                doNullCheck(adminUser, access_combination_name);
                if (access_combination_name.trim().isEmpty()) {
                    throw new InvalidDataException("Invalid access combination name");
                }
            } catch (InvalidDataException e3) {
                log(LogLevel.ERROR, LOGGER, credentials, e3, "One of the given arguments for create is null");
                throw e3;
            }

            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), "", null, "{} - {} - {} - {}", ctx, adminUser, access_combination_name, auth);

            UserModuleAccess access = cache.getNamedAccessCombination(access_combination_name.trim(), true);
            if (access == null) {
                // no such access combination name defined in configuration
                // throw error!
                throw new InvalidDataException("No such access combination name \"" + access_combination_name.trim() + "\"");
            }
            access = access.clone();

            return createcommon(ctx, adminUser, null, access, auth, schemaSelectStrategy);
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public Context create(final Context ctx, final User admin_user, final UserModuleAccess access, final Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, ContextExistsException {
        return create(ctx, admin_user, access, credentials, getDefaultSchemaSelectStrategy());
    }

    @Override
    public Context create(final Context ctx, final User admin_user, final UserModuleAccess access, final Credentials credentials, SchemaSelectStrategy schemaSelectStrategy) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, ContextExistsException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Context createInternal = createInternal(ctx, admin_user, access, credentials, schemaSelectStrategy);
            success = true;
            return createInternal;
        } finally {
            APIMetricsProcessor.getForCreate().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CREATE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private Context createInternal(final Context ctx, final User admin_user, final UserModuleAccess access, final Credentials credentials, SchemaSelectStrategy schemaSelectStrategy) throws StorageException, InvalidCredentialsException, InvalidDataException, ContextExistsException {
        try {
            final Credentials auth = credentials == null ? new Credentials("", "") : credentials;

            try {
                doNullCheck(admin_user, access);
            } catch (InvalidDataException e3) {
                log(LogLevel.ERROR, LOGGER, credentials, e3, "One of the given arguments for create is null");
                throw e3;
            }

            return createcommon(ctx, admin_user, null, access, auth, schemaSelectStrategy);
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public void delete(final Context ctx, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, DatabaseUpdateException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            deleteInternal(ctx, credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForDelete().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.DELETE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private void deleteInternal(final Context ctx, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, DatabaseUpdateException, InvalidDataException {
        try {
            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            Credentials auth = credentials == null ? new Credentials("", "") : credentials;
            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            log(LogLevel.DEBUG, LOGGER, credentials, null, ctx.toString());
            checkExistence(ctx);

            try {
                if (tool.checkAndUpdateSchemaIfRequired(ctx)) {
                    throw tool.generateDatabaseUpdateException(ctx.getId().intValue());
                }
            } catch (StorageException e) {
                // Context deletion should be a robust process. Therefore not failing if the schema is not up
                log(LogLevel.DEBUG, LOGGER, credentials, e, "Error while checking/updating schema");
            }

            performDeletion(ctx, auth);
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    /**
     * Performs the deletion either synchronously or asynchronously
     *
     * @param ctx The context to delete
     * @param auth The credentials
     * @throws StorageException in case of storage errors
     * @throws PluginException in case a plugin error occurs
     */
    private void performDeletion(final Context ctx, Credentials auth) throws StorageException, PluginException {
        boolean useAsync = useAsync(ctx);
        if (false == useAsync) {
            // Handle synchronously
            deletionExecutor.delete(ctx, auth);
            return;
        }
        // Schedule asynchronously
        AsyncTaskService asyncService = getAsyncService();
        if (asyncService == null) {
            LOGGER.warn("ASynchronous deletion is enabled but no AsyncTaskService is available. Falling back to synchronous execution.");
            deletionExecutor.delete(ctx, auth);
            return;
        }

        try {
            AsyncTask task = AsyncTask.builder()
                                      .withEntityId(ctx.getIdAsString())
                                      .withArgs(CredentialsParser.convert(auth))
                                      .withType(deletionExecutor.getType())
                                      .build();
            Integer taskId = asyncService.createTask(task);
            LOGGER.info("Created a new deletion task with id '{}' for context '{}'", taskId, ctx.getIdAsString());
        } catch (OXException e) {
            LOGGER.error("Unable to create a task for context deletion for context id '{}'. Falling back to synchronous execution.", ctx.getIdAsString(), e);
            deletionExecutor.delete(ctx, auth);
            return;
        }
    }

    /**
     * Whether to use async or not
     *
     * @param ctx The Context to delete
     * @return <code>true</code> if contexts should be deleted asynchronously, <code>false</code> otherwise
     */
    private boolean useAsync(Context ctx) {
        LeanConfigurationService service = AdminServiceRegistry.getInstance().getService(LeanConfigurationService.class);
        if (service == null) {
            // No service. Use fallback
            return AsyncContextProvisioningProperties.ENABLED.getDefaultValue(Boolean.class).booleanValue();
        }

        return service.getBooleanProperty(-1, i(ctx.getId()), AsyncContextProvisioningProperties.ENABLED);
    }

    private AsyncTaskService getAsyncService() {
        return AdminServiceRegistry.getInstance().getService(AsyncTaskService.class);
    }

    @Override
    public void disable(final Context ctx, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException, NoSuchReasonException, OXContextException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            disableInternal(ctx, credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.DEACTIVATE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    @Override
    public void disable(final Context ctx, final MaintenanceReason reason, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException, NoSuchReasonException, OXContextException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            disableInternal(ctx, reason, credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.DEACTIVATE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private void disableInternal(final Context ctx, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        final MaintenanceReason reason = new MaintenanceReason(Integer.valueOf(42));
        disableInternal(ctx, reason, credentials);
    }

    private void disableInternal(final Context ctx, final MaintenanceReason reason, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        final Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        try {
            doNullCheck(ctx, reason);
            doNullCheck(reason.getId());
        } catch (InvalidDataException e1) {
            log(LogLevel.ERROR, LOGGER, credentials, e1, "Invalid data sent by client!");
            throw e1;
        }

        try {
            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, "{} - {}", ctx, reason);
            checkExistence(ctx);

            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            oxContextPlugin.disable(ctx, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForChange(oxContextPlugin.getClass().getName(), "disable").stopTimer(timerSample, success);
                        }
                    }
                }
            }

            final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
            oxcox.disable(ctx, reason);
            log(LogLevel.INFO, LOGGER, credentials, ctx.getIdAsString(), null, "Context {} successfully disabled", ctx.getId());

            try {
                ContextStorage.getInstance().invalidateContext(ctx.getId().intValue());
                log(LogLevel.INFO, LOGGER, credentials, ctx.getIdAsString(), null, "Context {} successfully invalidated", ctx.getId());
            } catch (OXException e) {
                log(LogLevel.ERROR, LOGGER, credentials, ctx.getIdAsString(), e, "Error invalidating context {} in ox context storage", ctx.getId());
            }
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public void disableAll(final Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, NoSuchReasonException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            final MaintenanceReason reason = new MaintenanceReason(Integer.valueOf(42));
            disableAllInternal(reason, credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.DEACTIVATE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private void disableAllInternal(final MaintenanceReason reason, final Credentials credentials) throws StorageException, InvalidCredentialsException, InvalidDataException {
        final Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        try {
            doNullCheck(reason);
            doNullCheck(reason.getId());
        } catch (InvalidDataException e1) {
            log(LogLevel.ERROR, LOGGER, credentials, e1, "Invalid data sent by client!");
            throw e1;
        }
        try {
            BasicAuthenticator.createPluginAwareAuthenticator().doAuthentication(auth);
            Integer reason_id = reason.getId();
            log(LogLevel.DEBUG, LOGGER, credentials, null, "{}", reason_id);
            final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
            if (ClientAdminThreadExtended.cache.isMasterAdmin(auth)) {
                oxcox.disableAll(reason);
            } else {
                // Trigger plugin extensions
                {
                    final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                    if (null != pluginInterfaces) {
                        for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                            Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                            boolean success = false;
                            try {
                                oxContextPlugin.disableAll(auth);
                                success = true;
                            } finally {
                                PluginMetricsProcessor.getForChange(oxContextPlugin.getClass().getName(), "disableAll").stopTimer(timerSample, success);
                            }
                        }
                    }
                }
            }
            // Clear context cache
            try {
                ContextStorage.getInstance().invalidateAll();
            } catch (@SuppressWarnings("unused") OXException e) {
                log(LogLevel.ERROR, LOGGER, credentials, null, "{}", reason_id);
            }
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials);
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials);
            throw e;
        }
    }

    @Override
    public void enable(final Context ctx, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            enableInternal(ctx, credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.ACTIVATE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private void enableInternal(final Context ctx, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        if (null == ctx) {
            InvalidDataException invalidDataException = new InvalidDataException("Context is null");
            log(LogLevel.ERROR, LOGGER, auth, invalidDataException, "");
            throw invalidDataException;
        }

        try {

            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, ctx.toString());
            checkExistence(ctx);

            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            oxContextPlugin.enable(ctx, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForChange(oxContextPlugin.getClass().getName(), "enable").stopTimer(timerSample, success);
                        }
                    }
                }
            }

            final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
            oxcox.enable(ctx);

            try {
                ContextStorage.getInstance().invalidateContext(ctx.getId().intValue());
            } catch (OXException e) {
                log(LogLevel.ERROR, LOGGER, credentials, ctx.getIdAsString(), e, "Error invalidating context {} in ox context storage", ctx.getId());
            }
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public void enableAll(final Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            enableAllInternal(credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.ACTIVATE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private void enableAllInternal(final Credentials credentials) throws StorageException, InvalidCredentialsException {
        try {
            final Credentials auth = credentials == null ? new Credentials("", "") : credentials;

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthentication(auth);

            final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
            if (ClientAdminThreadExtended.cache.isMasterAdmin(auth)) {
                oxcox.enableAll();
            } else {
                // Trigger plugin extensions
                {
                    final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                    if (null != pluginInterfaces) {
                        for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                            Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                            boolean success = false;
                            try {
                                oxContextPlugin.enableAll(auth);
                                success = true;
                            } finally {
                                PluginMetricsProcessor.getForChange(oxContextPlugin.getClass().getName(), "enableAll").stopTimer(timerSample, success);
                            }
                        }
                    }
                }
            }

            // Clear context cache
            try {
                ContextStorage.getInstance().invalidateAll();
            } catch (OXException e) {
                log(LogLevel.ERROR, LOGGER, credentials, e, "");
            }
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials);
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials);
            throw e;
        }
    }

    @Override
    public Context getOwnData(Context ctx, Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Context ownDataInternal = getOwnDataInternal(ctx, credentials);
            success = true;
            return ownDataInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private Context getOwnDataInternal(Context ctx, Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        if (null == ctx) {
            InvalidDataException invalidDataException = new InvalidDataException("Context is null");
            log(LogLevel.ERROR, LOGGER, auth, invalidDataException, "");
            throw invalidDataException;
        }

        try {
            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
            return oxcox.getData(new Context[] { ctx })[0];
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public Context[] getData(final Context[] ctxs, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Context[] dataInternal = getDataInternal(ctxs, credentials);
            success = true;
            return dataInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private Context[] getDataInternal(final Context[] ctxs, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        final Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        try {
            try {
                doNullCheck((Object[]) ctxs);
            } catch (InvalidDataException e1) {
                log(LogLevel.ERROR, LOGGER, credentials, e1, "One of the given arguments for getData is null");
                throw e1;
            }

            for (Context ctx : ctxs) {
                try {
                    setIdOrGetIDFromNameAndIdObject(null, ctx);
                } catch (NoSuchObjectException e) {
                    throw new NoSuchContextException(e);
                }
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthentication(auth, Arrays.asList(ctxs));

            boolean filled = true;
            for (final Context ctx : ctxs) {
                if (!ctx.isListrun()) {
                    filled = false;
                }
                log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, ctx.toString());
                try {
                    checkExistence(ctx);
                } catch (NoSuchContextException e) {
                    log(LogLevel.ERROR, LOGGER, credentials, ctx.getIdAsString(), e, "");
                    throw e;
                }
            }
            final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();

            final List<Context> retval = new ArrayList<>(ctxs.length);
            if (filled) {
                final List<Context> callGetDataPlugins = callGetDataPlugins(Arrays.asList(ctxs), auth, oxcox);
                if (null != callGetDataPlugins) {
                    retval.addAll(callGetDataPlugins);
                } else {
                    retval.addAll(Arrays.asList(ctxs));
                }
            } else {
                final Context[] ret = oxcox.getData(ctxs);
                final List<Context> callGetDataPlugins = callGetDataPlugins(Arrays.asList(ret), auth, oxcox);
                if (null != callGetDataPlugins) {
                    retval.addAll(callGetDataPlugins);
                } else {
                    retval.addAll(Arrays.asList(ret));
                }
            }
            return retval.toArray(new Context[retval.size()]);
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, getObjectIds(ctxs));
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, getObjectIds(ctxs));
            throw e;
        }
    }

    @Override
    public Context getData(final Context ctx, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        return getData(new Context[] { ctx }, credentials)[0];
    }

    @Override
    public Context[] list(String search_pattern, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException {
        return list(search_pattern, -1, -1, credentials);
    }

    @Override
    public Context[] list(String search_pattern, boolean exclude_disabled, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException {
        return list(search_pattern, -1, -1, exclude_disabled, credentials);
    }

    @Override
    public Context[] list(String searchPattern, int offset, int length, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException {
        return list(searchPattern, offset, length, false, credentials);
    }

    @Override
    public Context[] list(String searchPattern, int offset, int length, boolean excludeDisabled, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Context[] listInternal = listInternal(searchPattern, offset, length, excludeDisabled, credentials);
            success = true;
            return listInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }


    private Context[] listInternal(String searchPattern, int offset, int length, boolean excludeDisabled, Credentials credentials) throws StorageException, InvalidCredentialsException, InvalidDataException {
        if (null == searchPattern) {
            InvalidDataException invalidDataException = new InvalidDataException("Search pattern is null");
            log(LogLevel.ERROR, LOGGER, credentials, invalidDataException, "");
            throw invalidDataException;
        }

        try {
            Credentials auth = credentials == null ? new Credentials("", "") : credentials;

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthentication(auth);

            log(LogLevel.DEBUG, LOGGER, credentials, null, "{}", searchPattern);

            final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
            Filter<Context, Context> loader = null;
            Filter<Integer, Integer> filter = null;
            final ArrayList<Filter<Context, Context>> loaderFilter = new ArrayList<>();
            final ArrayList<Filter<Integer, Integer>> contextFilter = new ArrayList<>();
            List<OXSupplier<List<Integer>, StorageException>> contextSearchers = new ArrayList<>();

            final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
            if (null != pluginInterfaces) {
                for (OXContextPluginInterface oxctx : pluginInterfaces.getContextPlugins().getServiceList()) {
                    log(LogLevel.DEBUG, LOGGER, credentials, null, "Calling list for plugin: {}", oxctx.getClass().getName());
                    Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                    boolean success = false;
                    try {
                        filter = oxctx.filter(auth);
                        success = true;
                    } finally {
                        PluginMetricsProcessor.getForRead(oxctx.getClass().getName(), "filter").stopTimer(timerSample, success);
                    }
                    if (null != filter) {
                        contextFilter.add(filter);
                    }
                    Timer.Sample timerSample2 = Timer.start(Metrics.globalRegistry);
                    boolean success2 = false;
                    try {
                        loader = oxctx.list(searchPattern, auth);
                        success2 = true;
                    } finally {
                        PluginMetricsProcessor.getForRead(oxctx.getClass().getName(), "list").stopTimer(timerSample2, success2);
                    }
                    if (null != loader) {
                        loaderFilter.add(loader);
                    }
                    if (oxctx instanceof OXContextPluginInterfaceExtended oxctxe) {
                        OXSupplier<List<Integer>, StorageException> contextSearcher;
                        Timer.Sample timerSample3 = Timer.start(Metrics.globalRegistry);
                        boolean success3 = false;
                        try {
                            contextSearcher = oxctxe.search(searchPattern, auth, excludeDisabled);
                            success3 = true;
                        } finally {
                            PluginMetricsProcessor.getForRead(oxctx.getClass().getName(), "search").stopTimer(timerSample3, success3);
                        }
                        if (null != contextSearcher) {
                            contextSearchers.add(contextSearcher);
                        }
                    }
                }
            }

            Context[] newRetval = oxcox.listContext(searchPattern, contextSearchers, contextFilter, loaderFilter, offset, length, excludeDisabled);
            return newRetval;
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials);
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials);
            throw e;
        }
    }

    @Override
    public Context[] listAll(final Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException {
        return listAll(-1, -1, credentials);
    }

    @Override
    public Context[] listAll(int offset, int length, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException {
        return list("*", offset, length, credentials);
    }

    @Override
    public Context[] listByDatabase(Database db, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, NoSuchDatabaseException {
        return listByDatabase(db, -1, -1, credentials);
    }

    @Override
    public Context[] listByDatabase(Database db, boolean excludeDisabled, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, NoSuchDatabaseException {
        return listByDatabase(db, -1, -1, excludeDisabled, credentials);
    }

    @Override
    public Context[] listByDatabase(Database db, int offset, int length, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, NoSuchDatabaseException {
        return listByDatabase(db, offset, length, false, credentials);
    }

    @Override
    public Context[] listByDatabase(Database db, int offset, int length, boolean excludeDisabled, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, NoSuchDatabaseException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Context[] listByDatabaseInternal = listByDatabaseInternal(db, offset, length, excludeDisabled, credentials);
            success = true;
            return listByDatabaseInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private Context[] listByDatabaseInternal(Database db, int offset, int length, boolean excludeDisabled, Credentials credentials) throws StorageException, InvalidCredentialsException, InvalidDataException, NoSuchDatabaseException {
        if (null == db) {
            InvalidDataException invalidDataException = new InvalidDataException("Database is null");
            log(LogLevel.ERROR, LOGGER, credentials, invalidDataException, "");
            throw invalidDataException;
        }

        try {
            Credentials auth = credentials == null ? new Credentials("", "") : credentials;

            try {
                setIdOrGetIDFromNameAndIdObject(null, db);
            } catch (NoSuchObjectException e) {
                throw new NoSuchDatabaseException(e);
            }

            BasicAuthenticator.createNonPluginAwareAuthenticator().doAuthentication(auth);

            log(LogLevel.DEBUG, LOGGER, credentials, null, db.toString());
            if (!tool.existsDatabase(db.getId().intValue())) {
                throw new NoSuchDatabaseException();
            }
            final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();

            final List<Context> retval = new ArrayList<>();
            final Context[] ret = oxcox.searchContextByDatabase(db, offset, length, excludeDisabled);
            final List<Context> callGetDataPlugins = callGetDataPlugins(Arrays.asList(ret), auth, oxcox);
            if (null != callGetDataPlugins) {
                retval.addAll(callGetDataPlugins);
            } else {
                retval.addAll(Arrays.asList(ret));
            }

            Context[] newRetval = retval.toArray(new Context[retval.size()]);
            return newRetval;
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials);
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials);
            throw e;
        }
    }

    @Override
    public Context[] listByFilestore(Filestore filestore, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, NoSuchFilestoreException {
        return listByFilestore(filestore, false, credentials);
    }

    @Override
    public Context[] listByFilestore(Filestore filestore, boolean excludeDisabled, Credentials credentials) throws RemoteException, StorageException, InvalidCredentialsException, InvalidDataException, NoSuchFilestoreException {
        return listByFilestore(filestore, -1, -1, excludeDisabled, credentials);
    }

    @Override
    public Context[] listByFilestore(Filestore filestore, int offset, int length, Credentials credentials) throws RemoteException, StorageException, InvalidDataException, InvalidCredentialsException, NoSuchFilestoreException {
        return listByFilestore(filestore, offset, length, false, credentials);
    }

    @Override
    public Context[] listByFilestore(Filestore filestore, int offset, int length, boolean excludeDisabled, Credentials credentials) throws RemoteException, StorageException, InvalidDataException, InvalidCredentialsException, NoSuchFilestoreException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Context[] listByFilestoreInternal = listByFilestoreInternal(filestore, offset, length, excludeDisabled, credentials);
            success = true;
            return listByFilestoreInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private Context[] listByFilestoreInternal(Filestore filestore, int offset, int length, boolean excludeDisabled, Credentials credentials) throws StorageException, InvalidDataException, InvalidCredentialsException, NoSuchFilestoreException {
        if (null == filestore) {
            InvalidDataException invalidDataException = new InvalidDataException("Filestore is null");
            log(LogLevel.ERROR, LOGGER, credentials, invalidDataException, "");
            throw invalidDataException;
        }
        if (null == filestore.getId()) {
            InvalidDataException invalidDataException = new InvalidDataException("Filestore ID is null");
            log(LogLevel.ERROR, LOGGER, credentials, invalidDataException, "");
            throw invalidDataException;
        }

        try {
            Credentials auth = credentials == null ? new Credentials("", "") : credentials;

            BasicAuthenticator.createNonPluginAwareAuthenticator().doAuthentication(auth);

            log(LogLevel.DEBUG, LOGGER, credentials, null, filestore.toString());
            if (!tool.existsStore(filestore.getId().intValue())) {
                throw new NoSuchFilestoreException();
            }
            final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
            final List<Context> retval = new ArrayList<>();
            final Context[] ret = oxcox.searchContextByFilestore(filestore, offset, length, excludeDisabled);
            final List<Context> callGetDataPlugins = callGetDataPlugins(Arrays.asList(ret), auth, oxcox);
            if (null != callGetDataPlugins) {
                retval.addAll(callGetDataPlugins);
            } else {
                retval.addAll(Arrays.asList(ret));
            }

            Context[] newRetval = retval.toArray(new Context[retval.size()]);
            return newRetval;
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials);
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials);
            throw e;
        }
    }

    /**
     * @see com.openexchange.admin.plugin.hosting.rmi.OXContextInterface#moveContextDatabase(com.openexchange.admin.plugin.hosting.rmi.dataobjects.Context, com.openexchange.admin.plugin.hosting.rmi.dataobjects.Database,
     *      com.openexchange.admin.plugin.hosting.rmi.dataobjects.Credentials)
     */
    @Override
    public int moveContextDatabase(final Context ctx, final Database db, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException, DatabaseUpdateException, OXContextException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            int moveContextDatabaseInternal = moveContextDatabaseInternal(ctx, db, new MaintenanceReason(Integer.valueOf(42)), credentials);
            success = true;
            return moveContextDatabaseInternal;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CHANGE)
                .withObjectType(ObjectType.CONTEXT_DATABASE)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private int moveContextDatabaseInternal(final Context ctx, final Database db, final MaintenanceReason reason, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException, DatabaseUpdateException, OXContextException {
        final Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        try {
            doNullCheck(ctx, db, reason);
            doNullCheck(reason.getId());
        } catch (InvalidDataException e1) {
            log(LogLevel.ERROR, LOGGER, credentials, e1, "Invalid data sent by client!");
            throw e1;
        }

        try {
            BasicAuthenticator.createNonPluginAwareAuthenticator().doAuthentication(auth);

            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            try {
                setIdOrGetIDFromNameAndIdObject(null, db);
            } catch (NoSuchObjectException e) {
                // FIXME normally NoSuchDatabaseException needs to be thrown here. Unfortunately it is not already in the throws declaration.
                throw new StorageException(e);
            }
            Integer reason_id = reason.getId();
            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, "{} - {} - {}", ctx, db, reason_id);
            checkExistence(ctx);
            if (tool.checkAndUpdateSchemaIfRequired(ctx)) {
                throw tool.generateDatabaseUpdateException(ctx.getId().intValue());
            }
            if (!tool.isContextEnabled(ctx)) {
                throw new OXContextException(OXContextException.CONTEXT_DISABLED);
            }
            final Integer dbid = db.getId();
            if (!tool.isMasterDatabase(dbid.intValue())) {
                throw new OXContextException("Database with id " + dbid + " is NOT a master!");
            }
            {
                // Check if target database is already source database
                OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
                Context storageVersion = oxcox.getData(new Context(ctx.getId()));
                if (storageVersion.getWriteDatabase().getId().intValue() == dbid.intValue()) {
                    throw new OXContextException("Context with id " + ctx.getId() + " already exists in database with id " + dbid);
                }
            }
            final DatabaseDataMover ddm = new DatabaseDataMover(ctx, db, reason);

            return TaskManager.getInstance().addJob(ddm, "movedatabase", "move context " + ctx.getIdAsString() + " to database " + dbid, ctx.getId().intValue());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public int moveContextFilestore(final Context ctx, final Filestore dst_filestore, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException, NoSuchFilestoreException, NoSuchReasonException, OXContextException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            final MaintenanceReason reason = new MaintenanceReason(I(42));
            int moveContextFilestoreInternal = moveContextFilestoreInternal(ctx, dst_filestore, reason, credentials);
            success = true;
            return moveContextFilestoreInternal;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CHANGE)
                .withObjectType(ObjectType.FILESTORE)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private int moveContextFilestoreInternal(final Context ctx, final Filestore dst_filestore, final MaintenanceReason reason, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException, NoSuchFilestoreException, OXContextException {
        final Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        try {
            doNullCheck(ctx, dst_filestore, reason);
            doNullCheck(dst_filestore.getId(), reason.getId());
        } catch (InvalidDataException e) {
            log(LogLevel.ERROR, LOGGER, credentials, e, "Invalid data sent by client!");
            throw e;
        }

        try {
            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            BasicAuthenticator.createNonPluginAwareAuthenticator().doAuthentication(auth);

            Context retval = null;

            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, "{} - {}", ctx, dst_filestore);

            final OXContextStorageInterface oxcox;
            try {
                oxcox = OXContextStorageInterface.getInstance();
            } catch (StorageException e) {
                log(LogLevel.ERROR, LOGGER, credentials, ctx.getIdAsString(), e, "");
                throw new StorageException(e.getMessage());
            }
            checkExistence(ctx);
            if (!tool.existsStore(dst_filestore.getId().intValue())) {
                throw new NoSuchFilestoreException();
            } else if (!tool.isContextEnabled(ctx)) {
                throw new OXContextException("Unable to disable Context " + ctx.getIdAsString());
            }

            retval = oxcox.getData(ctx);

            // Check equality
            int srcStore_id = retval.getFilestoreId().intValue();
            ctx.setFilestoreId(Integer.valueOf(srcStore_id));
            if (srcStore_id == dst_filestore.getId().intValue()) {
                throw new OXContextException("The identifiers for the source and destination storage are equal: " + dst_filestore);
            }

            // Check storage name
            String ctxdir = retval.getFilestore_name();
            if (ctxdir == null) {
                throw new OXContextException("Unable to get filestore directory " + ctx.getIdAsString());
            }

            OXUtilStorageInterface oxu = OXUtilStorageInterface.getInstance();
            Filestore destFilestore = oxu.getFilestore(dst_filestore.getId().intValue(), false);

            // Check capacity
            if (!oxu.hasSpaceForAnotherContext(destFilestore)) {
                throw new StorageException("Destination filestore does not have enough space for another context.");
            }

            // Load it to ensure validity
            String baseUri = destFilestore.getUrl();
            try {
                URI uri = FileStorages.getFullyQualifyingUriForContext(ctx.getId().intValue(), new java.net.URI(baseUri));
                FileStorages.getFileStorageService().getFileStorage(uri);
            } catch (OXException e) {
                throw StorageException.wrapForRMI(e);
            } catch (URISyntaxException e) {
                throw new StorageException("Invalid file storage URI: " + baseUri, e);
            }

            try {
                // Initialize mover instance
                FilestoreDataMover fsdm = FilestoreDataMover.newContextMover(oxu.getFilestore(srcStore_id, false), destFilestore, ctx);

                // Enable context after processing
                fsdm.addPostProcessTask(executionError -> {
                    if (null == executionError) {
                        oxcox.enable(ctx);
                        invalidateContextCache(ctx, credentials);
                    } else {
                        log(LogLevel.WARNING, LOGGER, credentials, ctx.getIdAsString(), null, "An execution error occurred during \"movefilestore\" for context {}. Context will stay disabled.", ctx.getId(), executionError.getCause());
                    }
                });

                // Schedule task
                oxcox.disable(ctx, reason);
                invalidateContextCache(ctx, credentials);
                return TaskManager.getInstance().addJob(fsdm, "movefilestore", "move context " + ctx.getIdAsString() + " to filestore " + dst_filestore.getId(), ctx.getId().intValue());
            } catch (StorageException e) {
                throw new StorageException(e.getMessage());
            } catch (RejectedExecutionException e) {
                oxcox.enable(ctx);
                throw e;
            }
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public boolean hasAutoContextIdPlugin() throws RemoteException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            boolean hasAutoContextIdPluginInternal = hasAutoContextIdPluginInternal();
            success = true;
            return hasAutoContextIdPluginInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private boolean hasAutoContextIdPluginInternal() {
        if (isAnyPluginLoaded() == false) {
            return false;
        }

        PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
        if (null == pluginInterfaces) {
            return false;
        }

        for (OXContextPluginInterface contextInterface : pluginInterfaces.getContextPlugins().getServiceList()) {
            if (isAutoContextIdPlugin(contextInterface)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean isAutoContextIdPlugin(OXContextPluginInterface contextInterface) {
        return null != contextInterface && "OXAutoCIDContextImpl".equals(contextInterface.getClass().getSimpleName());
    }

    @Override
    protected Context createmaincall(final Context ctx, final User admin_user, final Database db, final UserModuleAccess access, final Credentials credentials, SchemaSelectStrategy schemaSelectStrategy) throws StorageException, InvalidDataException, ContextExistsException {
        validateloginmapping(ctx);
        OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();

        // If not defined or access combination name does NOT exist, use hardcoded fallback!
        UserModuleAccess createaccess = getModuleAccessOrDefault(access);
        ctx.setEnabled(Boolean.TRUE);
        Context ret = oxcox.create(ctx, admin_user, createaccess, schemaSelectStrategy == null ? getDefaultSchemaSelectStrategy() : schemaSelectStrategy);
        final ConfigViewFactory viewFactory = AdminServiceRegistry.getInstance().getService(ConfigViewFactory.class);
        if (viewFactory != null) {
            ConfigView view;
            try {
                view = viewFactory.getView(admin_user.getId().intValue(), ret.getId().intValue());
                Boolean check = view.opt("com.openexchange.imap.initWithSpecialUse", Boolean.class, Boolean.TRUE);
                if (check != null && check.booleanValue()) {
                    ConfigProperty<Boolean> prop = view.property(ConfigViewScope.USER.getScopeName(), "com.openexchange.mail.specialuse.check", Boolean.class);
                    prop.set(Boolean.TRUE);
                }
            } catch (OXException e) {
                log(LogLevel.ERROR, LOGGER, credentials, ret.getIdAsString(), e, "Unable to set special use check property!");
            }
        }

        return ret;
    }

    private void validateloginmapping(final Context ctx) throws InvalidDataException {
        Set<String> loginMappings = ctx.getLoginMappings();
        if (null != loginMappings) {
            String login_regexp = ClientAdminThreadExtended.cache.getProperties().getProp("CHECK_CONTEXT_LOGIN_MAPPING_REGEXP", "[$%\\.+a-zA-Z0-9_-]");
            Pattern pattern = Pattern.compile(login_regexp);
            for (String mapping : loginMappings) {
                String illegal = pattern.matcher(mapping).replaceAll("");
                if (!illegal.isEmpty()) {
                    throw new InvalidDataException("Illegal chars: \"" + illegal + "\"" + " in login mapping");
                }
            }
        }
    }

    @Override
    public void changeModuleAccess(final Context ctx, final UserModuleAccess access, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            changeModuleAccessInternal(ctx, access, credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CHANGE)
                .withObjectType(ObjectType.MODULE_ACCESS)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private void changeModuleAccessInternal(final Context ctx, final UserModuleAccess access, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        final Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        if (null == ctx) {
            InvalidDataException invalidDataException = new InvalidDataException("Context is null");
            log(LogLevel.ERROR, LOGGER, auth, invalidDataException, "");
            throw invalidDataException;
        }

        try {
            doNullCheck(access);
        } catch (InvalidDataException e3) {
            log(LogLevel.ERROR, LOGGER, credentials, e3, "One of the given arguments for create is null");
            throw e3;
        }

        try {
            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, "{} - {}", ctx, access);
            checkExistence(ctx);

            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            oxContextPlugin.changeModuleAccess(ctx, access, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForChange(oxContextPlugin.getClass().getName(), "changeModuleAccess").stopTimer(timerSample, success);
                        }
                    }
                }
            }

            final OXUserStorageInterface oxu = OXUserStorageInterface.getInstance();

            // change rights for all users in context to specified one in access
            if (access.isPublicFolderEditable()) {
                // publicFolderEditable can only be applied to the context administrator.
                Integer[] userIds = i2I(oxu.getAll(ctx));
                final int adminId = tool.getAdminForContext(ctx);
                userIds = com.openexchange.tools.arrays.Arrays.remove(userIds, I(adminId));
                oxu.changeModuleAccess(ctx, adminId, access);
                access.setPublicFolderEditable(false);
                oxu.changeModuleAccess(ctx, I2i(userIds), access);
            } else {
                oxu.changeModuleAccess(ctx, oxu.getAll(ctx), access);
            }
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public void changeModuleAccess(final Context ctx, final String access_combination_name, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            changeModuleAccessInternal(ctx, access_combination_name, credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CHANGE)
                .withObjectType(ObjectType.MODULE_ACCESS)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private void changeModuleAccessInternal(final Context ctx, final String access_combination_name, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        final Credentials auth = credentials == null ? new Credentials("", "") : credentials;

        if (null == ctx) {
            InvalidDataException invalidDataException = new InvalidDataException("Context is null");
            log(LogLevel.ERROR, LOGGER, auth, invalidDataException, "");
            throw invalidDataException;
        }

        try {
            doNullCheck(access_combination_name);
            if (access_combination_name.trim().isEmpty()) {
                throw new InvalidDataException("Invalid access combination name");
            }
        } catch (InvalidDataException e3) {
            log(LogLevel.ERROR, LOGGER, credentials, ctx.getIdAsString(), e3, "One of the given arguments for create is null");
            throw e3;
        }

        OXUserStorageInterface oxu;
        Integer[] userIds;
        int adminId;
        try {
            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, "{} - {}", ctx, access_combination_name);

            checkExistence(ctx);

            UserModuleAccess accessAdmin = cache.getNamedAccessCombination(access_combination_name.trim(), true);
            UserModuleAccess accessUser = cache.getNamedAccessCombination(access_combination_name.trim(), false);
            if (null == accessAdmin || null == accessUser) {
                // no such access combination name defined in configuration
                // throw error!
                throw new InvalidDataException("No such access combination name \"" + access_combination_name.trim() + "\"");
            }
            accessAdmin = accessAdmin.clone();
            accessUser = accessUser.clone();

            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            oxContextPlugin.changeModuleAccess(ctx, access_combination_name, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForChange(oxContextPlugin.getClass().getName(), "changeModuleAccess").stopTimer(timerSample, success);
                        }
                    }
                }
            }

            oxu = OXUserStorageInterface.getInstance();

            // change rights for all users in context to specified one in access combination name
            userIds = i2I(oxu.getAll(ctx));
            adminId = tool.getAdminForContext(ctx);
            userIds = com.openexchange.tools.arrays.Arrays.remove(userIds, I(adminId));
            oxu.changeModuleAccess(ctx, adminId, accessAdmin);
            oxu.changeModuleAccess(ctx, I2i(userIds), accessUser);
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }

        // invalidate caches of all affected users
        Set<Integer> distinctUserIds = Stream.concat(Stream.of(Integer.valueOf(adminId)), Arrays.stream(userIds)).collect(Collectors.toSet());
        CacheHelper.invalidateUserCachesQuietly(i(ctx.getId()), I2i(distinctUserIds));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void downgrade(final Context ctx, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, DatabaseUpdateException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            downgradeInternal(ctx, credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.DOWNGRADE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(ctx != null ? ctx.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private void downgradeInternal(final Context ctx, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        if (null == ctx) {
            InvalidDataException invalidDataException = new InvalidDataException("Context is null");
            log(LogLevel.ERROR, LOGGER, auth, invalidDataException, "");
            throw invalidDataException;
        }

        try {
            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, ctx.toString());

            checkExistence(ctx);

            final OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        try {
                            Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                            boolean success = false;
                            try {
                                oxContextPlugin.downgrade(ctx, auth);
                                success = true;
                            } finally {
                                PluginMetricsProcessor.getForChange(oxContextPlugin.getClass().getName(), "downgrade").stopTimer(timerSample, success);
                            }
                        } catch (PluginException e) {
                            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
                        }
                    }
                }
            }
            oxcox.downgrade(ctx);

            try {
                ContextStorage.getInstance().invalidateContext(ctx.getId().intValue());
            } catch (OXException e) {
                log(LogLevel.ERROR, LOGGER, credentials, ctx.getIdAsString(), e, "Error invalidating context {} in ox context storage", ctx.getId());
            }
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public String getAccessCombinationName(final Context ctx, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            String accessCombinationNameInternal = getAccessCombinationNameInternal(ctx, credentials);
            success = true;
            return accessCombinationNameInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private String getAccessCombinationNameInternal(final Context ctx, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        if (null == ctx) {
            InvalidDataException invalidDataException = new InvalidDataException("Context is null");
            log(LogLevel.ERROR, LOGGER, auth, invalidDataException, "");
            throw invalidDataException;
        }

        try {
            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, ctx.toString());

            // Resolve admin user and get the module access from db and query cache for access combination name
            checkExistence(ctx);

            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            oxContextPlugin.getAccessCombinationName(ctx, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForRead(oxContextPlugin.getClass().getName(), "getAccessCombinationName").stopTimer(timerSample, success);
                        }
                    }
                }
            }

            // Get admin id and fetch current access object and query cache for its name!
            final OXUserStorageInterface oxu = OXUserStorageInterface.getInstance();

            return cache.getNameForAccessCombination(oxu.getModuleAccess(ctx, tool.getAdminForContext(ctx)));
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public UserModuleAccess getModuleAccess(final Context ctx, final Credentials credentials) throws RemoteException, InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            UserModuleAccess moduleAccessInternal = getModuleAccessInternal(ctx, credentials);
            success = true;
            return moduleAccessInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private UserModuleAccess getModuleAccessInternal(final Context ctx, final Credentials credentials) throws InvalidCredentialsException, NoSuchContextException, StorageException, InvalidDataException {
        Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        if (null == ctx) {
            InvalidDataException invalidDataException = new InvalidDataException("Context is null");
            log(LogLevel.ERROR, LOGGER, auth, invalidDataException, "");
            throw invalidDataException;
        }

        try {
            try {
                setIdOrGetIDFromNameAndIdObject(null, ctx);
            } catch (NoSuchObjectException e) {
                throw new NoSuchContextException(e);
            }

            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            log(LogLevel.DEBUG, LOGGER, credentials, ctx.getIdAsString(), null, ctx.toString());

            checkExistence(ctx);

            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            oxContextPlugin.getModuleAccess(ctx, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForRead(oxContextPlugin.getClass().getName(), "getModuleAccess").stopTimer(timerSample, success);
                        }
                    }
                }
            }

            // Get admin id and fetch current access object and return it to the client!
            final OXUserStorageInterface oxu = OXUserStorageInterface.getInstance();
            return oxu.getModuleAccess(ctx, tool.getAdminForContext(ctx));
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    /**
     * @param ctxs
     * @param auth
     * @param oxcox
     * @return null if no extensions available, contexts filled with extensions otherwise
     * @throws StorageException
     */
    private List<Context> callGetDataPlugins(final List<Context> ctxs, final Credentials auth, @SuppressWarnings("unused") final OXContextStorageInterface oxcox) throws PluginException {
        List<OXCommonExtension> retval = null;
        boolean extensionsFound = false;

        PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
        if (null != pluginInterfaces) {
            for (final OXContextPluginInterface oxctx : pluginInterfaces.getContextPlugins().getServiceList()) {
                extensionsFound = true;
                final String bundlename = oxctx.getClass().getName();
                log(LogLevel.DEBUG, LOGGER, auth, getObjectIds(ctxs.toArray(new NameAndIdObject[ctxs.size()])), null, "Calling getData for plugin: {}", bundlename);
                Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                boolean success = false;
                try {
                    retval = oxctx.getData(ctxs, auth);
                    success = true;
                } finally {
                    PluginMetricsProcessor.getForRead(oxctx.getClass().getName(), "getData").stopTimer(timerSample, success);
                }
                addExtensionToContext(ctxs, retval, bundlename);
            }
        }

        return extensionsFound ? ctxs : null;
    }

    private void addExtensionToContext(final List<Context> ctxs, final List<OXCommonExtension> retval, final String bundlename) throws PluginException {
        if (null != retval) {
            if (retval.size() != ctxs.size()) {
                throw new PluginException("After the call of plugin: " + bundlename + " the size of the context and the extensions differ");
            }
            for (int i = 0; i < retval.size(); i++) {
                try {
                    ctxs.get(i).addExtension(retval.get(i));
                } catch (DuplicateExtensionException e) {
                    throw new PluginException(e);
                }
            }
        }
    }

    @Override
    public int getAdminId(final Context ctx, final Credentials credentials) throws RemoteException, InvalidCredentialsException, StorageException, NoSuchContextException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            int adminIdInternal = getAdminIdInternal(ctx, credentials);
            success = true;
            return adminIdInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private int getAdminIdInternal(final Context ctx, final Credentials credentials) throws RemoteException, InvalidCredentialsException, StorageException, NoSuchContextException {
        try {
            setIdOrGetIDFromNameAndIdObject(null, ctx);
        } catch (NoSuchObjectException e) {
            throw new NoSuchContextException(e.getMessage());
        } catch (InvalidDataException e) {
            logAndEnhanceException(e, credentials, ctx);
            throw new RemoteException(e.getMessage());
        }

        try {
            final Credentials auth = credentials == null ? new Credentials("", "") : credentials;
            BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx);

            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            oxContextPlugin.getAdminId(ctx, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForRead(oxContextPlugin.getClass().getName(), "getAdminId").stopTimer(timerSample, success);
                        }
                    }
                }
            }
            checkExistence(ctx);

            return tool.getAdminForContext(ctx);
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public boolean exists(final Context ctx, final Credentials credentials) throws RemoteException, InvalidDataException, StorageException, InvalidCredentialsException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            boolean existsInternal = existsInternal(ctx, credentials);
            success = true;
            return existsInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private boolean existsInternal(final Context ctx, final Credentials credentials) throws InvalidDataException, StorageException, InvalidCredentialsException {
        if (ctx == null) {
            throw new InvalidDataException("Given context is invalid");
        }

        try {
            setIdOrGetIDFromNameAndIdObject(null, ctx);
        } catch (NoSuchObjectException e) {
            return false;
        }

        final Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx, true);

        try {
            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            oxContextPlugin.exists(ctx, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForRead(oxContextPlugin.getClass().getName(), "exists").stopTimer(timerSample, success);
                        }
                    }
                }
            }

            if (null != ctx.getId()) {
                return tool.existsContext(ctx);
            } else if (null != ctx.getName()) {
                return tool.existsContextName(ctx.getName());
            } else {
                throw new InvalidDataException("neither id or name is set in supplied context object");
            }
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public boolean existsInServer(Context ctx, Credentials credentials) throws RemoteException, InvalidDataException, StorageException, InvalidCredentialsException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            boolean existsInServerInternal = existsInServerInternal(ctx, credentials);
            success = true;
            return existsInServerInternal;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private boolean existsInServerInternal(Context ctx, Credentials credentials) throws InvalidDataException, StorageException, InvalidCredentialsException {
        if (ctx == null) {
            throw new InvalidDataException("Given context is invalid");
        }

        try {
            setIdOrGetIDFromNameAndIdObject(null, ctx);
        } catch (NoSuchObjectException e) {
            return false;
        }

        final Credentials auth = credentials == null ? new Credentials("", "") : credentials;
        BasicAuthenticator.createPluginAwareAuthenticator().doAuthenticationContextMasters(auth, ctx, true);

        try {
            // Trigger plugin extensions
            {
                final PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
                if (null != pluginInterfaces) {
                    for (final OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                        boolean success = false;
                        try {
                            oxContextPlugin.existsInServer(ctx, auth);
                            success = true;
                        } finally {
                            PluginMetricsProcessor.getForRead(oxContextPlugin.getClass().getName(), "existsInServer").stopTimer(timerSample, success);
                        }
                    }
                }
            }

            if (null != ctx.getId()) {
                return tool.existsContextInServer(ctx);
            } else if (null != ctx.getName()) {
                return tool.existsContextNameInServer(ctx.getName());
            } else {
                throw new InvalidDataException("neither id or name is set in supplied context object");
            }
        } catch (PluginException e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, ctx.getIdAsString());
        } catch (Throwable e) {
            logAndEnhanceException(e, credentials, ctx);
            throw e;
        }
    }

    @Override
    public boolean checkExists(final Context ctx, final Credentials credentials) throws RemoteException, InvalidDataException, StorageException, InvalidCredentialsException {
        return exists(ctx, credentials);
    }

    @Override
    protected Context usePreAssembledContext(Credentials credentials, Context contextToCreate, User adminToCreate, UserModuleAccess access) throws InvalidDataException, InvalidCredentialsException, StorageException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Context result = usePreAssembledContextInternal(credentials, contextToCreate, adminToCreate, access);
            success = true;
            return result;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CHANGE)
                .withObjectType(ObjectType.CONTEXT)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(contextToCreate != null ? contextToCreate.getId() : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private Context usePreAssembledContextInternal(Credentials credentials, Context contextToCreate, User adminToCreate, UserModuleAccess access) throws InvalidDataException, StorageException {
        doNullCheck(contextToCreate, adminToCreate);
        validateloginmapping(contextToCreate);
        UserModuleAccess createaccess = getModuleAccessOrDefault(access);
        OXContextStorageInterface contextStorage = OXContextStorageInterface.getInstance();
        try {
            return contextStorage.usePreAssembledContext(contextToCreate, adminToCreate, createaccess);
        } catch (EnforceableDataObjectException e) {
            log(LogLevel.WARNING, LOGGER, credentials, e, "Exception occured when checking for pre-assembled context.");
            return null;
        }
    }

    @Override
    protected void cleanUpAfterPluginFailed(Context context, Credentials credentials) throws StorageException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            cleanUpAfterPluginFailedInternal(context, credentials);
            success = true;
        } finally {
            APIMetricsProcessor.getForDelete().stopTimer(timerSample, success);
        }
    }

    private void cleanUpAfterPluginFailedInternal(Context context, Credentials credentials) throws StorageException {
        try {
            // Trigger plugin extensions
            PluginInterfaces pluginInterfaces = PluginInterfaces.getInstance();
            if (null != pluginInterfaces) {
                for (OXContextPluginInterface oxContextPlugin : pluginInterfaces.getContextPlugins().getServiceList()) {
                    Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
                    boolean success = false;
                    try {
                        oxContextPlugin.delete(context, credentials);
                        success = true;
                    } finally {
                        PluginMetricsProcessor.getForDelete(oxContextPlugin.getClass().getName(), "delete").stopTimer(timerSample, success);
                    }
                }
            }

            // Delete context data
            OXContextStorageInterface.getInstance().delete(context);
        } catch (Exception e) {
            throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials, context.getIdAsString());
        }
    }

    @Override
    protected Map<String, List<Integer>> getPreAssembledContexts(int limit) throws StorageException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Map<String, List<Integer>> result = getPreAssembledContextsInternal(limit);
            success = true;
            return result;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private Map<String, List<Integer>> getPreAssembledContextsInternal(int limit) throws StorageException {
        return OXContextStorageInterface.getInstance().getPreAssembledContexts(limit);
    }

    @Override
    protected ClaimedContext claimPreAssembledContext(Credentials credentials, Map<String, List<Integer>> candidateContextsPerSchema) throws StorageException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        ClaimedContext result;
        try {
            result = claimPreAssembledContextInternal(credentials, candidateContextsPerSchema);
            success = true;
            return result;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CHANGE)
                .withObjectType(ObjectType.CLAIM)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private ClaimedContext claimPreAssembledContextInternal(Credentials credentials, Map<String, List<Integer>> candidateContextsPerSchema) throws StorageException {
        if (candidateContextsPerSchema == null || candidateContextsPerSchema.isEmpty()) {
            throw logAndReturnException(LOGGER, new StorageException("Unable to claim any context from empty candidates list."), credentials);
        }
        SegmenterService segmenterService = AdminServiceRegistry.getInstance().getService(SegmenterService.class);
        if (null == segmenterService) {
            throw logAndReturnException(LOGGER, new StorageException("SegmenterService unavailable when trying to find context from local segment."), credentials);
        }
        log(LogLevel.DEBUG, LOGGER, credentials, null, "Attempting to select a context from local segment using a list of {} candidate schemas...", I(candidateContextsPerSchema.size()));
        for (Entry<String, List<Integer>> entry : toShuffledList(candidateContextsPerSchema.entrySet())) {
            try {
                if (false == segmenterService.isLocal(SegmentMarker.of(entry.getKey()))) {
                    continue;
                }
            } catch (OXException e) {
                throw logAndReturnException(LOGGER, StorageException.wrapForRMI(e), credentials);
            }
            for (Integer contextId : toShuffledList(entry.getValue())) {
                log(LogLevel.DEBUG, LOGGER, credentials, null, "Trying to acquire claim for context {} on database schema {}...", contextId, entry.getKey());
                ClaimedContext claimedContext = OXContextStorageInterface.getInstance().claim(i(contextId));
                if (null != claimedContext) {
                    log(LogLevel.INFO, LOGGER, credentials, null, "Successfully claimed context {} on database schema {}.", contextId, entry.getKey());
                    return claimedContext;
                }
                log(LogLevel.DEBUG, LOGGER, credentials, null, "Claim for context {} on database schema {} could not be acquired.", contextId, entry.getKey());
            }
        }
        throw logAndReturnException(LOGGER, new StorageException("Unable to claim any context from local segment."), credentials);
    }

    @Override
    protected void releaseClaim(Credentials credentials, ClaimedContext claimedContext) {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            releaseClaimInternal(credentials, claimedContext);
            success = true;
        } finally {
            APIMetricsProcessor.getForChange().stopTimer(timerSample, success);
            // @formatter:off
            ParsableLogCreator log = ParsableLogCreator.builder("ExtensionLogs")
                .withOperation(Operation.CHANGE)
                .withObjectType(ObjectType.CLAIM)
                .withSuccessful(success)
                .withOperationExecutor(credentials != null ? credentials.getLogin() : "n/a")
                .withContextId(claimedContext != null ? Integer.valueOf(claimedContext.contextId()) : null)
                .build();
            // @formatter:on
            EXTENSIONS_LOGGER.trace(Constants.DROP_MDC_MARKER, "{}", log);
        }
    }

    private void releaseClaimInternal(Credentials credentials, ClaimedContext claimedContext) {
        if (null == claimedContext) {
            return;
        }
        try {
            OXContextStorageInterface.getInstance().releaseClaim(claimedContext);
        } catch (StorageException e) {
            log(LogLevel.WARNING, LOGGER, credentials, e, "Unexpected error releasing claim for context {}", I(claimedContext.contextId()));
        }
    }

    @Override
    protected void checkClaimIsValid(ClaimedContext claimedContext) throws StorageException {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            checkClaimIsValidInternal(claimedContext);
            success = true;
        } finally {
            APIMetricsProcessor.getForRead().stopTimer(timerSample, success);
        }
    }

    private void checkClaimIsValidInternal(ClaimedContext claimedContext) throws StorageException {
        OXContextStorageInterface.getInstance().checkClaimIsValid(claimedContext);
    }

    // ------------------------- private methods -------------------------

    /**
     * Invalidate the context cache
     *
     * @param ctx The context
     * @param credentials The credentials
     */
    private void invalidateContextCache(Context ctx, Credentials credentials) {
        try {
            ContextStorage.getInstance().invalidateContext(ctx.getId().intValue());
            log(LogLevel.INFO, LOGGER, credentials, ctx.getIdAsString(), null, "Context {} successfully invalidated", ctx.getId());
        } catch (OXException e) {
            log(LogLevel.ERROR, LOGGER, credentials, ctx.getIdAsString(), e, "Error invalidating context {} in ox context storage", ctx.getId());
        }
    }

    /**
     * Initializes a list containing the elements of the supplied collection in random order.
     *
     * @param <T> The type of the elements in the collection
     * @param collection The collection to create a shuffled list for
     * @return A new list containing the elements of the supplied collection in random order
     */
    private static <T> List<T> toShuffledList(Collection<T> collection) {
        if (null == collection || collection.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList<T> list = new ArrayList<>(collection);
        Collections.shuffle(list);
        return list;
    }

    /**
     * Gets the user module access when creating a context, falling back to the default access combination as needed.
     *
     * @param access The parsed user module access from the incoming provisioning operation
     * @return The user module access, falling back to the default user module access if needed
     */
    private UserModuleAccess getModuleAccessOrDefault(UserModuleAccess access) {
        String DEFAULT_ACCESS_COMBINATION_NAME = cache.getProperties().getProp("NEW_CONTEXT_DEFAULT_ACCESS_COMBINATION_NAME", "NOT_DEFINED");

        // If not defined or access combination name does NOT exist, use hardcoded fallback!
        UserModuleAccess createaccess;
        if (access == null) {
            if (DEFAULT_ACCESS_COMBINATION_NAME.equals("NOT_DEFINED") || cache.getNamedAccessCombination(DEFAULT_ACCESS_COMBINATION_NAME, true) == null) {
                createaccess = AdminCache.getDefaultUserModuleAccess().clone();
            } else {
                createaccess = cache.getNamedAccessCombination(DEFAULT_ACCESS_COMBINATION_NAME, true).clone();
            }
        } else {
            createaccess = access.clone();
        }
        return createaccess;
    }
}
