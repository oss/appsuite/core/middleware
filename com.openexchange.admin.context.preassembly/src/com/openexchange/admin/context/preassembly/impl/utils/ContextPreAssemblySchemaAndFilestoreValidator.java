/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.context.preassembly.impl.utils;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import com.openexchange.admin.context.preassembly.exception.ContextPreAssemblyExceptionCodes;
import com.openexchange.admin.context.preassembly.impl.ContextPreAssemblyServiceImpl;
import com.openexchange.admin.rmi.dataobjects.Filestore;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.storage.interfaces.OXToolStorageInterface;
import com.openexchange.admin.storage.interfaces.OXUtilStorageInterface;
import com.openexchange.exception.OXException;

/**
 * {@link ContextPreAssemblySchemaAndFilestoreValidator} - Utility class providing static methods used in {@link ContextPreAssemblyServiceImpl}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class ContextPreAssemblySchemaAndFilestoreValidator {

    private ContextPreAssemblySchemaAndFilestoreValidator() {
        super();
    }

    /**
     * Validates the given schema and filestore.
     *
     * @param databaseId The database id to check schema for
     * @param schema The schema to check
     * @param numberOfContexts the number of contexts that should be created
     * @throws OXException If an unexpected error occurs or the schema does not exist
     */
    public static void checkSchema(int databaseId, String schema, int numberOfContexts) throws OXException {
        checkSchemaAndFilestore(databaseId, schema, numberOfContexts, null);
    }

    /**
     * Validates the given schema and filestore.
     *
     * @param databaseId The database id to check schema for
     * @param schema The schema to check
     * @param numberOfContexts the number of contexts that should be created
     * @param filestoreId The filestore id to check, or <code>null</code> if not specified explicitly
     * @throws OXException If an unexpected error occurs or the schema/filestore does not exist
     */
    public static void checkSchemaAndFilestore(int databaseId, String schema, int numberOfContexts, Integer filestoreId) throws OXException {
        checkSchema(databaseId, schema);
        if (null != filestoreId) {
            checkFilestore(i(filestoreId), numberOfContexts);
        }
    }

    private static void checkSchema(int databaseId, String schema) throws OXException {
        boolean schemaBeingLockedOrNeedsUpdate;
        try {
            schemaBeingLockedOrNeedsUpdate = OXToolStorageInterface.getInstance().schemaBeingLockedOrNeedsUpdate(databaseId, schema);
        } catch (StorageException e) {
            throw ContextPreAssemblyExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
        if (schemaBeingLockedOrNeedsUpdate) {
            throw ContextPreAssemblyExceptionCodes.SCHEMA_NOT_UP_TO_DATE.create();
        }
    }

    private static void checkFilestore(int filestoreId, int numberOfContexts) throws OXException {
        Filestore filestore;
        try {
            filestore = OXUtilStorageInterface.getInstance().getFilestore(filestoreId);
        } catch (StorageException e) {
            throw ContextPreAssemblyExceptionCodes.FILESTORE_DOES_NOT_EXIST.create(e, I(filestoreId));
        }
        Integer maxContexts = filestore.getMaxContexts();
        Integer currentContexts = filestore.getCurrentContexts();
        if (maxContexts == null || currentContexts == null) {
            throw ContextPreAssemblyExceptionCodes.UNEXPECTED_ERROR.create("Unable to check availability of contexts for filestore " + filestoreId + " due to missing information.");
        }
        if (i(currentContexts) + numberOfContexts > i(maxContexts)) {
            throw ContextPreAssemblyExceptionCodes.NOT_ENOUGH_SPACE_LEFT_IN_FILESTORE.create(I(filestoreId), I(numberOfContexts));
        }
    }

}
