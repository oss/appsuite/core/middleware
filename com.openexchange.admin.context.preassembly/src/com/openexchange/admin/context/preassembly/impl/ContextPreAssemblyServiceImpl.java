/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.context.preassembly.impl;

import static com.openexchange.java.Autoboxing.I;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import com.openexchange.admin.context.id.ContextIDGenerator;
import com.openexchange.admin.context.preassembly.ContextPreAssemblyService;
import com.openexchange.admin.context.preassembly.dao.ContextPreAssemblyResult;
import com.openexchange.admin.context.preassembly.exception.ContextPreAssemblyExceptionCodes;
import com.openexchange.admin.context.preassembly.impl.utils.ContextPreAssemblyContextCreator;
import com.openexchange.admin.context.preassembly.impl.utils.ContextPreAssemblyDatabaseUtils;
import com.openexchange.admin.context.preassembly.impl.utils.ContextPreAssemblySchemaAndFilestoreValidator;
import com.openexchange.admin.plugin.hosting.storage.interfaces.OXContextStorageInterface;
import com.openexchange.admin.rmi.dataobjects.Database;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.storage.interfaces.OXToolStorageInterface;
import com.openexchange.admin.storage.interfaces.OXUtilStorageInterface;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.segment.SegmentMarker;
import com.openexchange.segmenter.client.SegmenterService;
import com.openexchange.segmenter.client.Site;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * {@link ContextPreAssemblyServiceImpl} - The service interface implementation to pre-assemble contexts.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class ContextPreAssemblyServiceImpl implements ContextPreAssemblyService {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextPreAssemblyServiceImpl.class);

    private final ErrorAwareSupplier<? extends SegmenterService> segmenterServiceSupplier;
    private final ContextIDGenerator contextIDGenerator;

    /**
     * Initializes a new {@link ContextPreAssemblyServiceImpl}.
     *
     * @param segmenterServiceSupplier The {@link ErrorAwareSupplier} for the {@link SegmenterService}
     * @param contextIDGenerator The {@link ContextIDGenerator} to generate the next available context id
     */
    public ContextPreAssemblyServiceImpl(ErrorAwareSupplier<? extends SegmenterService> segmenterServiceSupplier, ContextIDGenerator contextIDGenerator) {
        super();
        this.segmenterServiceSupplier = segmenterServiceSupplier;
        this.contextIDGenerator = contextIDGenerator;
    }

    @Override
    public ContextPreAssemblyResult preAssemble(String schema, int numContexts, Integer filestoreId) throws OXException {
        if (Strings.isEmpty(schema)) {
            throw ContextPreAssemblyExceptionCodes.SCHEMA_NOT_PROVIDED.create();
        }
        int databaseId = ContextPreAssemblyDatabaseUtils.getDatabaseId(schema);
        ContextPreAssemblySchemaAndFilestoreValidator.checkSchemaAndFilestore(databaseId, schema, numContexts, filestoreId);
        return ContextPreAssemblyContextCreator.doPreAssemble(contextIDGenerator, databaseId, schema, numContexts, filestoreId);
    }

    @Override
    public Map<String, ContextPreAssemblyResult> preAssemble(Set<String> schemas, int contextsPerSchema, float limitFactor) {
        if (schemas == null || schemas.isEmpty()) {
            return Collections.emptyMap();
        }
        Set<String> schemasFromLocalSite;
        try {
            schemasFromLocalSite = getDatabaseSchemasFromLocalSite(new ArrayList<String>(schemas));
        } catch (OXException e) {
            LOG.error("", e);
            return getErrorResults(schemas, e);
        }
        Map<String, ContextPreAssemblyResult> resultsPerSchema = HashMap.newHashMap(schemas.size());
        for (String schema : schemas) {
            if (schemasFromLocalSite.contains(schema)) {
                resultsPerSchema.put(schema, preAssemble(schema, contextsPerSchema, limitFactor));
            } else {
                resultsPerSchema.put(schema, new ContextPreAssemblyResult(Collections.emptyList(),
                    Collections.singletonList(ContextPreAssemblyExceptionCodes.SCHEMA_NOT_ON_LOCAL_SITE.create(schema))));
            }
        }
        return resultsPerSchema;
    }

    /**
     * Pre-assembles contexts for the given schemas as necessary, based on the supplied parameters.
     *
     * @param schema The schema to pre-assemble contexts for
     * @param contextsPerSchema The number of pre-assembled contexts to create
     * @param limitFactor The factor that limits the max contexts per schema
     * @return {@link ContextPreAssemblyResult} with the result of the pre-assembling operation
     */
    private ContextPreAssemblyResult preAssemble(String schema, int contextsPerSchema, float limitFactor) {
        try {
            int databaseId = ContextPreAssemblyDatabaseUtils.getDatabaseId(schema);
            int numberOfContextsToCreate = getNumberOfContextsToPreAssemble(schema, contextsPerSchema, limitFactor, databaseId);
            if (numberOfContextsToCreate <= 0) {
                return new ContextPreAssemblyResult(Collections.emptyList(), Collections.emptyList());
            }
            ContextPreAssemblySchemaAndFilestoreValidator.checkSchema(databaseId, schema, numberOfContextsToCreate);
            return ContextPreAssemblyContextCreator.doPreAssemble(contextIDGenerator, databaseId, schema, numberOfContextsToCreate, null);
        } catch (StorageException | OXException e) {
            return getAndLogErrorResult(schema, e);
        }
    }

    /**
     * Logs the exception that occurred during pre-assembly of a certain schema and prepares a {@link ContextPreAssemblyResult} using the
     * supplied exception as error in the result.
     *
     * @param schema The scheme where the error occurred during pre-assembly
     * @param e The exception to log and use for the result
     * @return The pre-assembly result
     */
    private static ContextPreAssemblyResult getAndLogErrorResult(String schema, Exception e) {
        if (LOG.isDebugEnabled()) {
            LOG.warn("Error during pre-assembly of schema \"{}\": {}", schema, e.getMessage(), e);
        } else {
            LOG.warn("Error during pre-assembly of schema \"{}\": {}", schema, e.getMessage());
        }
        OXException oxe = e instanceof OXException ? (OXException) e : ContextPreAssemblyExceptionCodes.PRE_ASSEMBLY_FAILED.create(e, e.getMessage());
        return new ContextPreAssemblyResult(Collections.emptyList(), Collections.singletonList(oxe));
    }

    /**
     * Prepares a map of {@link ContextPreAssemblyResult} per schema, using the supplied exception as error in each result.
     *
     * @param schemas The schemas to create the error results for
     * @param error The exception to use for the result
     * @return The results per schema
     */
    private static Map<String, ContextPreAssemblyResult> getErrorResults(Set<String> schemas, OXException error) {
        if (schemas == null || schemas.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<String, ContextPreAssemblyResult> resultsPerSchema = HashMap.newHashMap(schemas.size());
        for (String schema : schemas) {
            resultsPerSchema.put(schema, new ContextPreAssemblyResult(Collections.emptyList(), Collections.singletonList(error)));
        }
        return resultsPerSchema;
    }

    /**
     * Gets the local database schemas from given list of database schemas.
     *
     * @param databaseSchemas The database schemas for which the local ones should be determined
     * @return The local database schemas
     * @throws OXException If determining local database schemas fails
     * @throws IllegalArgumentException If the list of schemas itself or any schema in the list is <code>null</code>
     */
    private Set<String> getDatabaseSchemasFromLocalSite(List<String> databaseSchemas) throws OXException {
        SegmenterService segmenterService = segmenterServiceSupplier.get();
        if (segmenterService == null) {
            LOG.warn("SegmenterService not available. Cannot identify schemas from local site to pre-assemble contexts. Nothing will be done.");
            return Set.of();
        }

        Set<String> localSchemas = null;
        List<Site> sites = segmenterService.getSites(SegmentMarker.of(databaseSchemas));
        Site localSite = segmenterService.getLocalSite();
        for (int i = 0; i < sites.size(); i++) {
            Site site = sites.get(i);
            if (localSite.matches(site)) {
                if (localSchemas == null) {
                    localSchemas = new HashSet<>();
                }
                // The associated schema has the same index in input list
                localSchemas.add(databaseSchemas.get(i));
            } else {
                LOG.debug("Site {} does not match local site {}. Will not consider this schema for pre-assembled contexts.", site, localSite);
            }
        }
        return localSchemas == null ? Collections.<String> emptySet() : localSchemas;
    }

    private int getNumberOfContextsToPreAssemble(String schema, int contextsPerSchema, float limitFactor, int databaseId) throws StorageException {
        int numberOfContextsToPreAssemble = Math.max(0, contextsPerSchema - OXContextStorageInterface.getInstance().getNumberOfPreAssembledContextsPerSchema(schema));
        if (numberOfContextsToPreAssemble <= 0) {
            LOG.debug("Expected number of pre-assembled contexts ({}) in schema {} already available.", I(contextsPerSchema), schema);
            return numberOfContextsToPreAssemble;
        }

        int numberOfContextsToPreAssembleWithFactor = getNumberOfContextsToPreAssembleWithFactor(databaseId, schema, numberOfContextsToPreAssemble, limitFactor);
        if (numberOfContextsToPreAssembleWithFactor <= 0) {
            LOG.warn("No space left for pre-assembled contexts in schema {}.", schema);
        }
        return numberOfContextsToPreAssembleWithFactor;
    }

    private int getNumberOfContextsToPreAssembleWithFactor(int databaseId, String schema, int numberOfMissingContextsToPreAssemble, float factor) throws StorageException {
        Database database = OXToolStorageInterface.getInstance().loadDatabaseById(databaseId);
        database.setScheme(schema);

        Integer maxContextsPerSchema = database.getMaxUnits();
        if (maxContextsPerSchema == null) {
            LOG.debug("Unable to check availability of contexts due to missing maximal number of contexts.");
            return 0;
        }

        int currentlyEnabledContexts = OXUtilStorageInterface.getInstance().readDBSchemaCounter(database);
        int maxContextsPerSchemaWithFactor = (int) (maxContextsPerSchema.intValue() * factor);

        return defineNumberOfContextsToAssemble(numberOfMissingContextsToPreAssemble, currentlyEnabledContexts, maxContextsPerSchemaWithFactor);
    }

    protected int defineNumberOfContextsToAssemble(int numberOfMissingContextsToPreAssemble, int currentlyEnabledContexts, int maxContextsPerSchemaWithFactor) {
        int availableContexts = maxContextsPerSchemaWithFactor - currentlyEnabledContexts;
        int contextsToAssemble = Math.min(numberOfMissingContextsToPreAssemble, availableContexts);
        return Math.max(contextsToAssemble, 0);
    }

}
