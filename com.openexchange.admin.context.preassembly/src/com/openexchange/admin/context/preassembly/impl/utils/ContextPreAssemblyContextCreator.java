/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.context.preassembly.impl.utils;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import com.openexchange.admin.context.id.ContextIDGenerator;
import com.openexchange.admin.context.preassembly.dao.ContextPreAssemblyResult;
import com.openexchange.admin.context.preassembly.exception.ContextPreAssemblyExceptionCodes;
import com.openexchange.admin.context.preassembly.impl.ContextPreAssemblyServiceImpl;
import com.openexchange.admin.plugin.hosting.storage.interfaces.OXContextStorageInterface;
import com.openexchange.admin.rmi.dataobjects.Context;
import com.openexchange.admin.rmi.dataobjects.Database;
import com.openexchange.admin.rmi.dataobjects.MaintenanceReason;
import com.openexchange.admin.rmi.dataobjects.SchemaSelectStrategy;
import com.openexchange.admin.rmi.dataobjects.UserModuleAccess;
import com.openexchange.admin.rmi.exceptions.ContextExistsException;
import com.openexchange.admin.rmi.exceptions.InvalidDataException;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.tools.AdminCache;
import com.openexchange.exception.OXException;
import com.openexchange.java.Autoboxing;

/**
 * {@link ContextPreAssemblyContextCreator} - Utility class providing static methods used in {@link ContextPreAssemblyServiceImpl}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class ContextPreAssemblyContextCreator {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextPreAssemblyContextCreator.class);

    private ContextPreAssemblyContextCreator() {
        super();
    }

    private static Context buildContextObjectToPreAssemble(ContextIDGenerator idGenerator, int databaseId, Integer filestoreId) throws OXException {
        int contextId = idGenerator.generateContextId();
        Context context = new Context(Autoboxing.I(contextId));
        context.setName("preassembled-" + UUID.randomUUID().toString());
        context.setMaintenanceReason(new MaintenanceReason(Integer.valueOf(666)));
        context.setEnabled(Boolean.FALSE);
        context.setMaxQuota(L(0));
        context.setWriteDatabase(new Database(databaseId));
        final String sContextId = context.getIdAsString();
        if (null != sContextId) {
            context.addLoginMapping(sContextId);
        }
        if (filestoreId != null) {
            context.setFilestoreId(filestoreId);
        }
        return context;
    }

    /**
     * Pre-assembles multiple contexts within the given database and schema.
     *
     * @param idGenerator The service to retrieve the next context id
     * @param databaseId The database id for the context
     * @param schema The schema the context should be created in
     * @param numContexts The number of pre-assembled contexts to create
     * @param filestoreId The optional filestore id for the context, or <code>null</code> if unspecified
     * @return The pre-assembly result containing the ids of the pre-assembled contexts and any errors that occurred
     */
    public static ContextPreAssemblyResult doPreAssemble(ContextIDGenerator idGenerator, int databaseId, String schema, int numContexts, Integer filestoreId) {
        List<OXException> errors = new ArrayList<>();
        List<Integer> contextIds = new ArrayList<>(numContexts);
        for (int i = 0; i < numContexts; i++) {
            try {
                contextIds.add(doPreAssemble(idGenerator, databaseId, schema, filestoreId));
            } catch (OXException e) {
                errors.add(e);
                LOG.warn("Context pre-assembly failed for schema \"{}\" in database {}: {}", schema, I(databaseId), e.getMessage(), e);
            }
        }
        return new ContextPreAssemblyResult(contextIds, errors);
    }

    /**
     * Pre-assembles a single context within the given database and schema.
     *
     * @param idGenerator The service to retrieve the next context id
     * @param databaseId The database id for the context
     * @param schema The schema the context should be created in
     * @param filestoreId The optional filestore id for the context, or <code>null</code> if unspecified
     * @return The identifier of the created context
     * @throws OXException if context pre-assembly fails for any reason
     */
    public static Integer doPreAssemble(ContextIDGenerator idGenerator, int databaseId, String schema, Integer filestoreId) throws OXException {
        Context contextData = ContextPreAssemblyContextCreator.buildContextObjectToPreAssemble(idGenerator, databaseId, filestoreId);
        try {
            return preAssembleContext(schema, contextData).getId();
        } catch (StorageException | InvalidDataException | ContextExistsException e) {
            throw ContextPreAssemblyExceptionCodes.PRE_ASSEMBLY_FAILED.create(e, e.getMessage());
        }
    }

    private static Context preAssembleContext(String schema, Context contextToPreAssemble) throws StorageException, InvalidDataException, ContextExistsException {
        com.openexchange.admin.rmi.dataobjects.User adminDummy = buildDummyAdmin(contextToPreAssemble);
        UserModuleAccess createaccess = AdminCache.getDefaultUserModuleAccess();
        OXContextStorageInterface oxcox = OXContextStorageInterface.getInstance();
        return oxcox.create(contextToPreAssemble, adminDummy, createaccess, SchemaSelectStrategy.schema(schema));
    }

    private static com.openexchange.admin.rmi.dataobjects.User buildDummyAdmin(Context ctx) {
        com.openexchange.admin.rmi.dataobjects.User adminDummy = new com.openexchange.admin.rmi.dataobjects.User();
        adminDummy.setPassword("secret");
        adminDummy.setName("oxadmin");
        adminDummy.setDisplay_name("display_name");
        adminDummy.setGiven_name("given_name");
        adminDummy.setSur_name("sur_name");
        String mail = "oxadmin@" + ctx.getIdAsString();
        adminDummy.setPrimaryEmail(mail);
        adminDummy.setEmail1(mail);
        return adminDummy;
    }

}
