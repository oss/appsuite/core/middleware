/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.context.preassembly.impl.utils;

import static com.openexchange.java.Autoboxing.i;
import org.slf4j.Logger;
import com.openexchange.admin.context.preassembly.exception.ContextPreAssemblyExceptionCodes;
import com.openexchange.admin.context.preassembly.impl.ContextPreAssemblyServiceImpl;
import com.openexchange.admin.rmi.dataobjects.Database;
import com.openexchange.admin.rmi.exceptions.NoSuchObjectException;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.storage.interfaces.OXToolStorageInterface;
import com.openexchange.admin.storage.interfaces.OXUtilStorageInterface;
import com.openexchange.exception.OXException;

/**
 * {@link ContextPreAssemblyDatabaseUtils} - Utility class providing static methods used in {@link ContextPreAssemblyServiceImpl}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class ContextPreAssemblyDatabaseUtils {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextPreAssemblyDatabaseUtils.class);

    private ContextPreAssemblyDatabaseUtils() {
        super();
    }

    /**
     * Returns the database id for the given schema.
     *
     * @param schema The schema to get the database id for
     * @return The database for the given schema
     * @throws OXException If the schema does not exist, or an error occurs when while getting the database id
     */
    public static int getDatabaseId(String schema) throws OXException {
        try {
            return OXToolStorageInterface.getInstance().getDatabaseIDByDatabaseSchema(schema);
        } catch (StorageException e) {
            throw ContextPreAssemblyExceptionCodes.UNEXPECTED_ERROR.create(e.getMessage(), e);
        } catch (NoSuchObjectException e) {
            LOG.debug("No database id for schema {} found, checking if schema exists.", schema, e);
        }
        Database database;
        try {
            database = searchForDatabase(schema);
        } catch (StorageException e) {
            throw ContextPreAssemblyExceptionCodes.UNEXPECTED_ERROR.create(e.getMessage(), e);
        }
        if (null == database) {
            throw ContextPreAssemblyExceptionCodes.SCHEMA_DOES_NOT_EXIST.create(schema);
        }
        return i(database.getId());
    }

    private static Database searchForDatabase(String schema) throws StorageException {
        for (Database database : OXUtilStorageInterface.getInstance().searchForDatabaseSchema(schema, false)) {
            if (schema.equals(database.getScheme())) {
                return database;
            }
        }
        return null;
    }

}
