/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.context.preassembly.metrics;

import static com.openexchange.java.Autoboxing.I;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.slf4j.Logger;
import com.openexchange.admin.plugin.hosting.storage.interfaces.OXContextStorageInterface;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.Meter.Id;
import io.micrometer.core.instrument.Meter.Type;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;

/**
 * {@link ContextPreAssemblyMetricsProvider} - The class that provides metrics for existing pre-assembled contexts
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class ContextPreAssemblyMetricsProvider {

    private static class LoggerHolder {

        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextPreAssemblyMetricsProvider.class); // NOSONARLINT
    }

    /** The identifier / name of the registered gauge */
    private static final String GAUGE_NAME = "appsuite.provisioning.preassembled.available.total";

    /** The interval in milliseconds after which the value gets re-evaluated */
    private static final long UPDATE_INTERVAL = TimeUnit.SECONDS.toMillis(30L);

    private final AtomicLong lastUpdated;
    private volatile int value;

    /**
     * Initializes a new {@link ContextPreAssemblyMetricsProvider}.
     */
    public ContextPreAssemblyMetricsProvider() {
        super();
        this.lastUpdated = new AtomicLong(0L);
        this.value = -1;
    }

    /**
     * Registers the metrics relevant for pre-assembled contexts within the Micrometer framework.
     */
    public void register() {
        /*
         * Remove data that may reference to an old client
         */
        String description = "The number of available pre-assembled contexts for the deployment.";
        Id id = new Meter.Id(GAUGE_NAME, Tags.empty(), null, description, Type.GAUGE);
        Metrics.globalRegistry.remove(id);

        /*
         * Register new gauge
         */
        Gauge.builder(GAUGE_NAME, () -> getNumberOfPreAssembledContexts()).tags(Tags.empty()).description(description).register(Metrics.globalRegistry);
    }

    /**
     * Returns the overall number of pre-assembled contexts for the deployment. The value will be updated every 30 seconds.
     *
     * @return The overall number of pre-assembled contexts.
     */
    private Number getNumberOfPreAssembledContexts() {
        long now = System.currentTimeMillis();
        long lastUpdatedMillis = lastUpdated.get();
        if (lastUpdatedMillis + UPDATE_INTERVAL < now && lastUpdated.compareAndSet(lastUpdatedMillis, now)) {
            try {
                int newValue = OXContextStorageInterface.getInstance().getNumberOfPreAssembledContexts();
                value = newValue;
                return I(newValue);
            } catch (Exception e) {
                LoggerHolder.LOG.error("Unexpected error occurred while retrieving the number of pre-assembled contexts.", e);
            }
        }
        return I(value);
    }
}
