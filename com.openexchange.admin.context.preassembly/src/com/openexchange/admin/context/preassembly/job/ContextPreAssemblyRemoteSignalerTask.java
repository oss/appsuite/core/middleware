/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.context.preassembly.job;

import com.openexchange.cluster.lock.ClusterTask;
import com.openexchange.cluster.lock.AbstractUnboundClusterTask;
import com.openexchange.exception.OXException;

/**
 * {@link ContextPreAssemblyRemoteSignalerTask} - {@link ClusterTask} to signal ongoing pre-assembly operations to remote sites.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class ContextPreAssemblyRemoteSignalerTask extends AbstractUnboundClusterTask<Void> {

    /**
     * Initializes a new {@link ContextPreAssemblyRemoteSignalerTask}.
     */
    public ContextPreAssemblyRemoteSignalerTask() {
        super();
    }

    @Override
    public String getTaskName() {
        return ContextPreAssemblyRemoteSignalerTask.class.getCanonicalName();
    }

    @Override
    public Void perform() throws OXException {
        return null; // nothing to perform
    }

}
