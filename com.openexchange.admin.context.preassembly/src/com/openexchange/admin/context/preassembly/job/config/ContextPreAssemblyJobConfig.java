/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.context.preassembly.job.config;

import com.openexchange.java.Strings;
import com.openexchange.schedule.RangesOfTheWeek;
import com.openexchange.schedule.ScheduleExpressionParser;

/**
 * {@link ContextPreAssemblyJobConfig} - The configuration for the pre-assembly job.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class ContextPreAssemblyJobConfig {

    /**
     * Gets a new builder instance.
     *
     * @return The new builder instance
     */
    public static Builder builder() {
        return new Builder();
    }

    /** The builder for a {@link ContextPreAssemblyJobConfig} instance. */
    public static final class Builder {

        private RangesOfTheWeek rangesOfTheWeek;
        private int contextsPerSchema;
        private float limitFactor;
        private long checkForTasksFrequency;
        private long executionDelay;

        /**
         * Initializes a new {@link ContextPreAssemblyJobConfig.Builder}.
         */
        private Builder() {
            super();
        }

        /**
         * Sets the number of pre-assembled contexts that should be available after the job execution.
         *
         * @param contextsPerSchema The number of contexts
         * @return This builder
         */
        public Builder withContextsPerSchema(int contextsPerSchema) {
            this.contextsPerSchema = contextsPerSchema;
            return this;
        }

        /**
         * Sets the factor that limits the max contexts per schema
         *
         * @param limitFactor The factor that limits the max contexts per schema
         * @return This builder
         */
        public Builder withLimitFactor(float limitFactor) {
            this.limitFactor = limitFactor;
            return this;
        }

        /**
         * Sets the frequency in milliseconds when to check for further tasks to process.
         *
         * @param checkForTasksFrequency The frequency to set
         * @return This builder
         * @throws IllegalArgumentException If frequency is less than/equal to 0 (zero)
         */
        public Builder withCheckForTasksFrequency(long checkForTasksFrequency) {
            if (checkForTasksFrequency <= 0) {
                throw new IllegalArgumentException("checkForTasksFrequency must not be less than or equal to 0 (zero).");
            }
            this.checkForTasksFrequency = checkForTasksFrequency;
            return this;
        }

        /**
         * Parses the given configuration; e.g. <code>"Mon 0:12-6:45; Tue-Thu 0-7:15; Fri 0-6,22:30-24; Sat,Sun 0-8"</code>.
         *
         * @param config The configuration to parse
         * @return This builder
         * @throws IllegalArgumentException If schedule information is invalid
         */
        public Builder parse(String schedule) {
            if (Strings.isEmpty(schedule)) {
                return this;
            }
            this.rangesOfTheWeek = ScheduleExpressionParser.parse(schedule);
            return this;
        }

        /**
         * Sets the (minimum) delay between repeated executions of the pre-assembly job.
         *
         * @param executionDelay The (minimum) delay between repeated executions of the pre-assembly job in milliseconds
         * @return This builder
         */
        public Builder withExecutionDelay(long executionDelay) {
            this.executionDelay = executionDelay;
            return this;
        }

        /**
         * Creates the <code>PreAssembleJobConfig</code> instance from this builder's arguments.
         *
         * @return The <code>PreAssembleJobConfig</code> instance
         */
        public ContextPreAssemblyJobConfig build() {
            return new ContextPreAssemblyJobConfig(rangesOfTheWeek, contextsPerSchema, limitFactor, checkForTasksFrequency, executionDelay);
        }

    } // End of Builder class

    // -----------------------------------------------------------------------------------------------------------------------------

    private final RangesOfTheWeek rangesOfTheWeek;
    private final int contextsPerSchema;
    private final float limitFactor;
    private final long checkForTasksFrequency;
    private final long executionDelay;

    /**
     * Initializes a new {@link ContextPreAssemblyJobConfig}.
     *
     * @param rangesOfTheWeek The ranges of week defining the allowed schedule
     * @param contextsPerSchema The number of pre-assembled contexts after the execution
     * @param limitFactor The factor schemas should be filled up to.
     * @param checkForTasksFrequency The frequency when to check for due data export tasks
     * @param executionDelay The (minimum) delay between repeated executions of the pre-assembly job in milliseconds
     */
    private ContextPreAssemblyJobConfig(RangesOfTheWeek rangesOfTheWeek, int contextsPerSchema, float limitFactor, long checkForTasksFrequency, long executionDelay) {
        super();
        this.rangesOfTheWeek = rangesOfTheWeek == null ? RangesOfTheWeek.EMPTY_SCHEDULE : rangesOfTheWeek;
        this.contextsPerSchema = contextsPerSchema;
        this.limitFactor = limitFactor;
        this.checkForTasksFrequency = checkForTasksFrequency;
        this.executionDelay = executionDelay;
    }

    /**
     * Gets the ranges of the week.
     *
     * @return The ranges of the week.
     */
    public RangesOfTheWeek getRangesOfTheWeek() {
        return rangesOfTheWeek;
    }

    /**
     * Gets the number of pre-assembled contexts that should be available after the job execution.
     *
     * @return The number of pre-assembled contexts.
     */
    public int getContextsPerSchema() {
        return contextsPerSchema;
    }

    /**
     * Gets the factor that limits the max contexts per schema
     *
     * @return The factor that limits the max contexts per schema
     */
    public float getLimitFactor() {
        return limitFactor;
    }

    /**
     * Gets the frequency in milliseconds when to check for further tasks to process.
     *
     * @return The frequency
     */
    public long getCheckForTasksFrequency() {
        return checkForTasksFrequency;
    }

    /**
     * Gets the (minimum) delay between repeated executions of the pre-assembly job.
     *
     * @return The (minimum) delay between repeated executions of the pre-assembly job in milliseconds
     */
    public long getExecutionDelay() {
        return executionDelay;
    }

}
