/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.context.preassembly.job;

import org.slf4j.Logger;
import com.openexchange.admin.context.preassembly.ContextPreAssemblyService;
import com.openexchange.admin.context.preassembly.job.config.ContextPreAssemblyJobConfig;
import com.openexchange.cluster.lock.ClusterTask;
import com.openexchange.cluster.lock.CrossSiteClusterLockService;
import com.openexchange.exception.OXException;
import com.openexchange.java.ExceptionCatchingRunnable;
import com.openexchange.log.HumanTimeOutputter;
import com.openexchange.schedule.TaskScheduler;
import com.openexchange.server.ServiceLookup;
import com.openexchange.timer.TimerService;

/**
 * {@link ContextPreAssemblyJobScheduler} - Job scheduler for context pre-assembling.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class ContextPreAssemblyJobScheduler {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextPreAssemblyJobScheduler.class);

    private final ServiceLookup serviceLookup;
    private final TaskScheduler taskScheduler;
    private final ContextPreAssemblyService contextPreAssemblyService;
    private final ContextPreAssemblyJobConfig contextPreAssemblyJobConfig;

    /**
     * Initializes a new {@link ContextPreAssemblyJobScheduler}.
     *
     * @param serviceLookup The service lookup
     * @param contextPreAssemblyService The service to pre-assemble contexts
     * @param contextPreAssemblyJobConfig The configuration for the pre-assemble job
     */
    public ContextPreAssemblyJobScheduler(ServiceLookup serviceLookup, ContextPreAssemblyService contextPreAssemblyService, ContextPreAssemblyJobConfig contextPreAssemblyJobConfig) {
        super();
        this.serviceLookup = serviceLookup;
        this.contextPreAssemblyService = contextPreAssemblyService;
        this.contextPreAssemblyJobConfig = contextPreAssemblyJobConfig;

        ExceptionCatchingRunnable periodicTask = this::startProcessingTasks;

        // @formatter:off
        this.taskScheduler = TaskScheduler.builder()
            .withFrequencyMillis(contextPreAssemblyJobConfig.getCheckForTasksFrequency())
            .withPeriodicTask(periodicTask)
            .withRangesOfTheWeek(contextPreAssemblyJobConfig.getRangesOfTheWeek())
            .withTimerServiceSupplier(() -> serviceLookup.getService(TimerService.class)).build();
        // @formatter:on
    }

    /**
     * Starts scheduling pre-assembly jobs.
     */
    public synchronized void startScheduling() {
        // Initialize scheduling stuff
        taskScheduler.start();
        LOG.info("Started scheduling of pre-assemble contexts jobs");
    }

    private synchronized void startProcessingTasks() throws OXException {
        CrossSiteClusterLockService clusterLockService = serviceLookup.getService(CrossSiteClusterLockService.class);
        if (clusterLockService == null) {
            LOG.warn("CrossSiteClusterLockService not available. Cannot run task to pre-assemble contexts.");
            return;
        }
        long executionDelay = contextPreAssemblyJobConfig.getExecutionDelay();
        ContextPreAssemblyClusterTask preAssemblyTask = new ContextPreAssemblyClusterTask(contextPreAssemblyService, contextPreAssemblyJobConfig.getContextsPerSchema(), contextPreAssemblyJobConfig.getLimitFactor());
        ContextPreAssemblyRemoteSignalerTask remoteSignalerTask = new ContextPreAssemblyRemoteSignalerTask();

        String crossSiteLock = null;
        String clusterLock = null;
        try {
            /*
             * first acquire cross-site lock (considering remote sites as applicable) during runtime
             */
            crossSiteLock = clusterLockService.acquireClusterLock(executionDelay, remoteSignalerTask, true);
            if (null == crossSiteLock) {
                LOG.debug("Cross-site signaling lock for pre-assembly job not acquired, task is already performed elsewhere).");
                return; // task is already performed elsewhere
            }
            LOG.info("Successfully acquired cross-site signaling lock \"{}\" for pre-assembly.", crossSiteLock);
            /*
             * proceed with site-local cluster lock
             */
            clusterLock = clusterLockService.acquireClusterLock(executionDelay, preAssemblyTask);
            if (null == clusterLock) {
                LOG.debug("Cluster lock for pre-assembly job not acquired, nothing to do (task is or has already been performed recently).");
                return; // task is or has already been performed recently
            }
            LOG.info("Successfully acquired cluster lock \"{}\" ({}), starting pre-assembly job on this node.", clusterLock, new HumanTimeOutputter(executionDelay, true));
            /*
             * perform pre-assembly
             */
            preAssemblyTask.perform();
        } finally {
            /*
             * release acquired locks - immediately for cross-site lock, deferred expiration for local cluster
             */
            if (releaseClusterLockSafely(clusterLockService, crossSiteLock, true, remoteSignalerTask, true)) {
                LOG.info("Cross-site signaling lock \"{}\" for pre-assembly has been released.", crossSiteLock);
            }
            if (releaseClusterLockSafely(clusterLockService, clusterLock, false, preAssemblyTask, false)) {
                LOG.info("Pre-assembly job finished, letting cluster lock \"{}\" expire after {} for next acquisition.", clusterLock, new HumanTimeOutputter(executionDelay, true));
            }
        }
    }

    private static boolean releaseClusterLockSafely(CrossSiteClusterLockService clusterLockService, String lockValue, boolean releaseLock, ClusterTask<?> clusterTask, boolean considerRemoteSites) {
        if (null != lockValue) {
            try {
                clusterLockService.releaseClusterLock(lockValue, releaseLock, clusterTask, considerRemoteSites);
                return true;
            } catch (Exception e) {
                LOG.warn("Unepxected error releasing cluster lock \"{}\": {}", lockValue, e.getMessage(), e);
            }
        }
        return false;
    }

}
