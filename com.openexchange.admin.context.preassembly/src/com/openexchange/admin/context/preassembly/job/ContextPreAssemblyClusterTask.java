/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.context.preassembly.job;

import static com.openexchange.java.Autoboxing.F;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.log.LogUtility.toStringObjectFor;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.slf4j.Logger;
import com.openexchange.admin.context.preassembly.ContextPreAssemblyService;
import com.openexchange.admin.context.preassembly.dao.ContextPreAssemblyResult;
import com.openexchange.admin.rmi.dataobjects.Database;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.storage.interfaces.OXUtilStorageInterface;
import com.openexchange.cluster.lock.ClusterTask;
import com.openexchange.cluster.lock.AbstractUnboundClusterTask;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.log.DurationOutputter;

/**
 * {@link ContextPreAssemblyClusterTask} - {@link ClusterTask} to pre-assemble contexts
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class ContextPreAssemblyClusterTask extends AbstractUnboundClusterTask<Void> {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextPreAssemblyClusterTask.class);

    private final ContextPreAssemblyService contextPreAssemblyService;
    private final int contextsPerSchema;
    private final float limitFactor;

    /**
     * Initializes a new {@link ContextPreAssemblyClusterTask}.
     *
     * @param contextPreAssemblyService The underlying pre-assembly service
     * @param contextsPerSchema The number of pre-assembled contexts per schema to create
     * @param limitFactor The factor that limits the max contexts per schema
     */
    public ContextPreAssemblyClusterTask(ContextPreAssemblyService contextPreAssemblyService, int contextsPerSchema, float limitFactor) {
        super();
        this.contextPreAssemblyService = contextPreAssemblyService;
        this.contextsPerSchema = contextsPerSchema;
        this.limitFactor = limitFactor;
    }

    @Override
    public String getTaskName() {
        return ContextPreAssemblyClusterTask.class.getCanonicalName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void perform() throws OXException {
        long start = System.nanoTime();
        LOG.info("Starting context pre-assembly job [contextsPerSchema={}, limitFactor={}].", I(contextsPerSchema), F(limitFactor));
        Map<String, ContextPreAssemblyResult> resultsPerSchema = contextPreAssemblyService.preAssemble(getAllSchemas(), contextsPerSchema, limitFactor);
        int preAssembledContextsCount = 0;
        int preAssembledSchemasCount = 0;
        for (Entry<String, ContextPreAssemblyResult> entry : resultsPerSchema.entrySet()) {
            String schema = entry.getKey();
            List<Integer> contextIds = entry.getValue().contextIds();
            if (null != contextIds && 0 < contextIds.size()) {
                preAssembledContextsCount += contextIds.size();
                preAssembledSchemasCount++;
                LOG.debug("Created {} pre-assembled contexts in schema \"{}\" with the following context ids: {}", I(contextIds.size()), schema, toStringObjectFor(contextIds));
            }
            List<OXException> errors = entry.getValue().errors();
            if (null != errors && 0 < errors.size()) {
                LOG.debug("The following errors were encountered during pre-assembly for schema {}: {}", schema, toStringObjectFor(errors, ',' + Strings.getLineSeparator(), 50));
            }
        }
        if (0 < preAssembledContextsCount) {
            LOG.info("Created a total of {} pre-assembled contexts in {} different schemas, {} elapsed.", I(preAssembledContextsCount), I(preAssembledSchemasCount), DurationOutputter.startNanos(start));
        } else {
            LOG.info("No pre-assembled contexts were created, {} elapsed.", DurationOutputter.startNanos(start));
        }
        return null;
    }

    /**
     * Returns all schemas
     *
     * @return {@link Set} with the name of all (non-global) database schemas
     */
    private Set<String> getAllSchemas() {
        Database[] allDBSchemas;
        try {
            allDBSchemas = OXUtilStorageInterface.getInstance().searchForDatabaseSchema("*", false);
        } catch (StorageException e) {
            LOG.error("Unable to get existing db schemas. No pre-assembled contexts will be created.", e);
            return Collections.emptySet();
        }
        Set<String> allSchemas = HashSet.newHashSet(allDBSchemas.length);
        for (Database database : allDBSchemas) {
            String schemaName = database.getScheme();
            if (!schemaName.endsWith("_global")) {
                allSchemas.add(schemaName);
            }
        }
        return allSchemas;
    }

}
