/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.context.preassembly.job.config;

import java.util.concurrent.TimeUnit;
import com.openexchange.config.lean.Property;

/**
 * {@link ContextPreAssemblyJobProperties} - The properties for the pre-assembly job.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public enum ContextPreAssemblyJobProperties implements Property {

    /**
     * Configures if the job to pre-assemble contexts is enabled and should create the contexts by considering the given parameter.
     */
    ENABLED("enabled", Boolean.FALSE),

    /**
     * The schedule when to pre-assemble contexts. The creation process will only run once in this period.
     */
    SCHEDULE("schedule", "Mon-Sun 0-4"),

    /**
     * Configures the number of contexts to pre-assemble that should exist per schema after the job was executed. This number will never be exceeded by using the job.
     */
    PRE_ASSEMBLED_CONTEXTS_PER_SCHEMA("contextsPerSchema", Integer.valueOf(100)),

    /**
     * Configures the maximum filling level of schemas when adding pre-assembled contexts via periodic background job, as a factor of CONTEXTS_PER_SCHEMA.
     * I. e. pre-assembly is only performed until the total number of contexts exceeds <code>&lt;factor&gt; * &lt;contexts_per_schema&gt;</code>.
     */
    CONTEXT_LIMIT_FACTOR("contextLimitFactor", Float.valueOf(0.9F)),

    /**
     * The frequency in milliseconds when to check for new job executions within configured schedule. Defaults to <code>3600000</code> (1 hour).
     */
    CHECK_FOR_TASKS_FREQUENCY("frequency", Long.valueOf(TimeUnit.HOURS.toMillis(1))),

    /**
     * The (minimum) delay between repeated executions of the pre-assembly job in milliseconds. Defaults to <code>86400000</code> (one day)
     * to only run the job once per day within configured schedule.
     */
    EXECUTION_DELAY("executionDelay", Long.valueOf(TimeUnit.DAYS.toMillis(1))),

    ;

    private final String fqn;
    private final Object defaultValue;

    /**
     * Initializes a new {@link ContextPreAssemblyJobProperties}.
     *
     * @param appendix The appendix for the fully-qualifying name
     * @param defaultValue The default value
     */
    private ContextPreAssemblyJobProperties(String appendix, Object defaultValue) {
        this.fqn = "com.openexchange.admin.context.preassembly.job." + appendix;
        this.defaultValue = defaultValue;
    }

    /**
     * Returns the fully-qualifying name for the property
     *
     * @return the fully-qualifying name for the property
     */
    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    /**
     * Returns the default value of this property
     *
     * @return the default value of this property
     */
    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }
}
