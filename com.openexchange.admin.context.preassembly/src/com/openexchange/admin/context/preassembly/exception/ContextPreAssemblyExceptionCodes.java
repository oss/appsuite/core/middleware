/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.context.preassembly.exception;

import com.openexchange.exception.Category;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionCode;
import com.openexchange.exception.OXExceptionFactory;

/**
 * {@link ContextPreAssemblyExceptionCodes} The exception codes for the pre-assembly of contexts.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public enum ContextPreAssemblyExceptionCodes implements OXExceptionCode {

    /**
     * An unexpected error occurred while pre-assemble contexts: %s
     */
    UNEXPECTED_ERROR("An unexpected error occurred while pre-assemble contexts: %s", Category.CATEGORY_ERROR, 1),

    /**
     * The provided schema does not exist.
     */
    SCHEMA_DOES_NOT_EXIST("The provided schema %s does not exist.", Category.CATEGORY_USER_INPUT, 2),

    /**
     * The provided filestore with id %s does not exist.
     */
    FILESTORE_DOES_NOT_EXIST("The provided filestore with id %s does not exist.", Category.CATEGORY_USER_INPUT, 3),

    /**
     * There was an issue with the database. Schema not up to date. Unable to pre-assemble contexts.
     */
    SCHEMA_NOT_UP_TO_DATE("There was an issue with the database. Schema not up to date. Unable to pre-assemble contexts.", Category.CATEGORY_ERROR, 4),

    /**
     * The provided filestore with id %s does not have enough space left for %s contexts.
     */
    NOT_ENOUGH_SPACE_LEFT_IN_FILESTORE("The provided filestore with id %s does not have enough space left for %s contexts.", Category.CATEGORY_USER_INPUT, 5),

    /**
     * The provided schema %s does not belong to the local site.
     */
    SCHEMA_NOT_ON_LOCAL_SITE("The provided schema %s does not belong to the local site.", Category.CATEGORY_USER_INPUT, 6),

    /**
     * No schema provided to pre-assemble contexts for.
     */
    SCHEMA_NOT_PROVIDED("No schema provided to pre-assemble contexts for.", Category.CATEGORY_USER_INPUT, 7),

    /**
     * Pre-assembled context could not be created: %s
     */
    PRE_ASSEMBLY_FAILED("Pre-assembled context could not be created: %s", Category.CATEGORY_ERROR, 8),

    ;

    private static final String PREFIX = "PAC".intern();
    private final String message;
    private final Category category;
    private final int number;

    private ContextPreAssemblyExceptionCodes(String message, Category category, int number) {
        this.message = message;
        this.category = category;
        this.number = number;
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getPrefix() {
        return PREFIX;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public boolean equals(OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(final Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(final Throwable cause, final Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }
}
