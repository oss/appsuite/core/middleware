/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.context.preassembly;

import java.util.Map;
import java.util.Set;
import com.openexchange.admin.context.preassembly.dao.ContextPreAssemblyResult;
import com.openexchange.exception.OXException;

/**
 * {@link ContextPreAssemblyService} - The service interface for context pre-assembling.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public interface ContextPreAssemblyService {

    /**
     * Pre-assembles contexts for all given schemas (if on local site).
     *
     * @param schemas The schemas to pre-assemble contexts for.
     * @param contextsPerSchema The number of pre-assembled contexts to create.
     * @param limitFactor The factor that limits the max contexts per schema
     * @return A map holding the individual {@link ContextPreAssemblyResult}s per schema.
     */
    Map<String, ContextPreAssemblyResult> preAssemble(Set<String> schemas, int contextsPerSchema, float limitFactor);

    /**
     * Pre-assembles contexts for a specific schema.
     *
     * @param schema The schema to pre-assemble contexts for.
     * @param contextsPerSchema The number of pre-assembled contexts to create.
     * @param filestoreId Optional filestore id to use for the pre-assembled contexts. Can be null
     * @return {@link ContextPreAssemblyResult} with the result of the pre-assembling operation.
     * @throws OXException in case of a (fatal) error, or in case of unmet preconditions
     */
    ContextPreAssemblyResult preAssemble(String schema, int contextsPerSchema, Integer filestoreId) throws OXException;

}
