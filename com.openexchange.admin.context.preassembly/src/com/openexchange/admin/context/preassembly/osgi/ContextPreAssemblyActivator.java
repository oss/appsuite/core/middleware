/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.context.preassembly.osgi;

import java.util.Optional;
import com.openexchange.admin.context.id.ContextIDGenerator;
import com.openexchange.admin.context.preassembly.ContextPreAssemblyService;
import com.openexchange.admin.context.preassembly.impl.ContextPreAssemblyServiceImpl;
import com.openexchange.admin.context.preassembly.job.ContextPreAssemblyJobScheduler;
import com.openexchange.admin.context.preassembly.job.config.ContextPreAssemblyJobConfig;
import com.openexchange.admin.context.preassembly.job.config.ContextPreAssemblyJobProperties;
import com.openexchange.admin.context.preassembly.metrics.ContextPreAssemblyMetricsProvider;
import com.openexchange.cluster.lock.CrossSiteClusterLockService;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.database.DatabaseService;
import com.openexchange.java.Strings;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.segmenter.client.SegmenterService;
import com.openexchange.startup.SignalStartedService;
import com.openexchange.timer.TimerService;

/**
 * {@link ContextPreAssemblyActivator} The activator of the pre-assemble context bundle.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public final class ContextPreAssemblyActivator extends HousekeepingActivator {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextPreAssemblyActivator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { DatabaseService.class, LeanConfigurationService.class, TimerService.class, SegmenterService.class, CrossSiteClusterLockService.class, ContextIDGenerator.class };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        LOG.info("Starting context pre-assembly bundle");

        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(() -> getService(SegmenterService.class), getService(ContextIDGenerator.class));
        registerService(ContextPreAssemblyService.class, preAssembleContextServiceImpl);

        Optional<ContextPreAssemblyJobConfig> jobConfig = getJobConfig(getService(LeanConfigurationService.class));
        if (jobConfig.isPresent()) {
            ContextPreAssemblyJobScheduler preAssembleContextJobScheduler = new ContextPreAssemblyJobScheduler(this, preAssembleContextServiceImpl, jobConfig.get());
            track(SignalStartedService.class, new ContextPreAssemblySchedulingStarter(preAssembleContextJobScheduler, context));
        }

        openTrackers();
        new ContextPreAssemblyMetricsProvider().register();
    }

    private Optional<ContextPreAssemblyJobConfig> getJobConfig(LeanConfigurationService leanConfigurationService) {
        boolean enabled = leanConfigurationService.getBooleanProperty(ContextPreAssemblyJobProperties.ENABLED);
        if (!enabled) {
            LOG.debug("Context pre-assembly is disabled per configuration. Will not start the job.");
            return Optional.empty();
        }
        int preAssembledContextsPerSchema = leanConfigurationService.getIntProperty(ContextPreAssemblyJobProperties.PRE_ASSEMBLED_CONTEXTS_PER_SCHEMA);
        if (preAssembledContextsPerSchema <= 0) {
            LOG.info("Number of contexts to pre-assemble smaller or equal 0. Will not start auto pre-assembly job.");
            return Optional.empty();
        }
        ContextPreAssemblyJobConfig.Builder configBuilder = ContextPreAssemblyJobConfig.builder();
        configBuilder.withContextsPerSchema(preAssembledContextsPerSchema);

        String schedule = leanConfigurationService.getProperty(ContextPreAssemblyJobProperties.SCHEDULE);
        if (Strings.isEmpty(schedule)) {
            LOG.error("Pre-assembled contexts enabled but no schedule defined! Cannot start auto pre-assemble job!");
            return Optional.empty();
        }
        configBuilder.parse(schedule);
        configBuilder.withLimitFactor(leanConfigurationService.getFloatProperty(ContextPreAssemblyJobProperties.CONTEXT_LIMIT_FACTOR));
        configBuilder.withCheckForTasksFrequency(leanConfigurationService.getLongProperty(ContextPreAssemblyJobProperties.CHECK_FOR_TASKS_FREQUENCY));
        configBuilder.withExecutionDelay(leanConfigurationService.getLongProperty(ContextPreAssemblyJobProperties.EXECUTION_DELAY));

        return Optional.of(configBuilder.build());
    }
}
