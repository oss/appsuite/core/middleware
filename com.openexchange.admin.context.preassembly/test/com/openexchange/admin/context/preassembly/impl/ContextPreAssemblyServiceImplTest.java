/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.context.preassembly.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
class ContextPreAssemblyServiceImplTest {

    @Test
    void testDefineNumberOfContextsToAssemble() {
        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(null, null);
        int defineNumberOfContextsToAssemble = preAssembleContextServiceImpl.defineNumberOfContextsToAssemble(0, 0, 0);

        assertEquals(0, defineNumberOfContextsToAssemble);
    }

    @Test
    void testDefineNumberOfContextsToAssemble_overLimitButNothingRequested() {
        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(null, null);
        int defineNumberOfContextsToAssemble = preAssembleContextServiceImpl.defineNumberOfContextsToAssemble(0, 100, 90);

        assertEquals(0, defineNumberOfContextsToAssemble);
    }

    @Test
    void testDefineNumberOfContextsToAssemble_inLimitButNothingRequested() {
        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(null, null);
        int defineNumberOfContextsToAssemble = preAssembleContextServiceImpl.defineNumberOfContextsToAssemble(0, 20, 90);

        assertEquals(0, defineNumberOfContextsToAssemble);
    }

    @Test
    void testDefineNumberOfContextsToAssemble_overLimitAnd20Requested() {
        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(null, null);
        int defineNumberOfContextsToAssemble = preAssembleContextServiceImpl.defineNumberOfContextsToAssemble(20, 100, 90);

        assertEquals(0, defineNumberOfContextsToAssemble);
    }

    @Test
    void testDefineNumberOfContextsToAssemble_noneRequested_nonExistend() {
        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(null, null);
        int defineNumberOfContextsToAssemble = preAssembleContextServiceImpl.defineNumberOfContextsToAssemble(0, 0, 90);

        assertEquals(0, defineNumberOfContextsToAssemble);
    }

    @Test
    void testDefineNumberOfContextsToAssemble_enoughAvailable() {
        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(null, null);
        int defineNumberOfContextsToAssemble = preAssembleContextServiceImpl.defineNumberOfContextsToAssemble(20, 0, 90);

        assertEquals(20, defineNumberOfContextsToAssemble);
    }

    @Test
    void testDefineNumberOfContextsToAssemble_limitFactorReached() {
        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(null, null);
        int defineNumberOfContextsToAssemble = preAssembleContextServiceImpl.defineNumberOfContextsToAssemble(200, 0, 90);

        assertEquals(90, defineNumberOfContextsToAssemble);
    }

    @Test
    void testDefineNumberOfContextsToAssemble_requestedOverLimitAndExisting() {
        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(null, null);
        int defineNumberOfContextsToAssemble = preAssembleContextServiceImpl.defineNumberOfContextsToAssemble(150, 100, 90);

        assertEquals(0, defineNumberOfContextsToAssemble);
    }

    @Test
    void testDefineNumberOfContextsToAssemble_requestedOverLimit_returnAvailable() {
        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(null, null);
        int defineNumberOfContextsToAssemble = preAssembleContextServiceImpl.defineNumberOfContextsToAssemble(150, 20, 90);

        assertEquals(70, defineNumberOfContextsToAssemble);
    }

    @Test
    void testDefineNumberOfContextsToAssemble_limitZero_returnZero() {
        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(null, null);
        int defineNumberOfContextsToAssemble = preAssembleContextServiceImpl.defineNumberOfContextsToAssemble(150, 20, 0);

        assertEquals(0, defineNumberOfContextsToAssemble);
    }

    @Test
    void testDefineNumberOfContextsToAssemble_limitZeroAndEmpty_returnZero() {
        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(null, null);
        int defineNumberOfContextsToAssemble = preAssembleContextServiceImpl.defineNumberOfContextsToAssemble(150, 0, 0);

        assertEquals(0, defineNumberOfContextsToAssemble);
    }

    @Test
    void testDefineNumberOfContextsToAssemble_limitZeroAndNothingRequested_returnZero() {
        ContextPreAssemblyServiceImpl preAssembleContextServiceImpl = new ContextPreAssemblyServiceImpl(null, null);
        int defineNumberOfContextsToAssemble = preAssembleContextServiceImpl.defineNumberOfContextsToAssemble(0, 50, 0);

        assertEquals(0, defineNumberOfContextsToAssemble);
    }
}
