# Baselayer Build 

The middleware baselayer build serves as a blueprint for our image release procedures, providing a clear framework for creating and distributing images.
This process ensures consistency and quality across releases, enabling us to deliver reliable results to other teams/cells.

For all of the following steps, for more detailed information please refer to the `build.gradle.kts`, `Jenkinsfile` and `jenkinsPod.yaml`.
Beside the fact that we placed our workflow inside our Jenkinsfile due to automation purposes,
it is still possible to build the baselayer locally, implied the right tools are installed.

## General Preparations

- Define an `ImageExtension` for your image, as demonstrated in the `build.gradle.kts`.
For detailed information on defining an ImageExtension and utilizing the image-builder plugin, refer to the [image-builder documentation](https://gitlab.open-xchange.com/engineering/gradle/image-builder).

- For the middleware baselayer build, the correct tags for our image are determined using `git.ReleaseTags()`.
This method could be applicable to other release processes, streamlining the tagging process.

- To ensure git tags are released properly, you need to define an `AutoSemVerExtension`. Please refer to the [documentation](https://gitlab.open-xchange.com/engineering/auto-semver/gradle-git).
There are several other projects inside the engineering gitlab workspace which already make use of auto-semver.
The specific configuration varies based on your release process preferences. For the middleware baselayer build, we utilize the `stableVersioningIncrementProvider`.
This provider increments the `patch` version for every commit and increments `minor` only if the commit message contains the keyword `VERSION BUMP`.
We designate `main` and `stable` as `releaseBranches` to restrict tag writing to these repositories.

- Depending on your workflow, ensure correct task dependencies and register additional tasks as needed, as demonstrated for the middleware build.
This is driven by the complexity of the middleware image build and the inclusion of testing in our workflow.

## Workflow

The following provides a quick overview of our workflow outlined in the `Jenkinsfile`. However, all essential tasks are implemented within Gradle to ensure platform independence, enabling local building and pushing.

We will not delve into Jenkins-specific or environmental details.

1. Check out the core repository along with tags.
2. Execute the Gradle task `publishNextVersion`. This task determines the last version tag on the repository and tags the repository with the new version tag based on the configuration of `AutoSemVerExtension`.
3. Run the Gradle task `buildahPushImage-core-tmp`. This initiates the building of a baselayer image, which is pushed to a temporary repository for subsequent testing.
Ensure correct task dependencies and additional tasks are registered based on your objectives. For the baselayer build, tasks such as `createVersionFile` or `tagCoreTmpImage` will be executed first. 
The latter ensures that our image is not pushed to the final repository with the final tags defined; at this stage, it is only pushed to a temporary registry with a temporary tag.
4. Once the baselayer is pushed to the registry, `core-test` is triggered as usual.
5. If `core-test` is successful, tags are pushed to Git via `git push origin --tags`, and our baselayer is pushed to its official repository containing all the release tags (`buildahPushAllImages`).
6. After these steps, a `post` stage runs regardless of whether preceding stages failed or not. Its purpose is to clean up the temporary repository (`deleteImage-core-tmp`).

Please note: This workflow is tailored to our specific needs related to the middleware and its characteristics. However, in general, this concept can be adapted for creating other image/release workflows.




