/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.rmi.util;

import static com.openexchange.java.Autoboxing.B;
import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.admin.rmi.dataobjects.Group;
import com.openexchange.admin.rmi.dataobjects.Resource;
import com.openexchange.admin.rmi.dataobjects.User;
import com.openexchange.admin.rmi.dataobjects.UserModuleAccess;
import com.openexchange.admin.rmi.extensions.OXCommonExtensionInterface;
import com.openexchange.java.util.TimeZones;

/**
 * {@link AssertUtil}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @since v7.10.1
 */
public class AssertUtil {

    private static final Logger LOG = LoggerFactory.getLogger(AssertUtil.class);

    /**
     * Asserts that the <code>expected</code> user is equal to the <code>actual</code>.
     * Only the mandatory fields are asserted.
     *
     * @param expected The expected {@link User}
     * @param actual The actual {@link User}
     */
    public static void assertUserEquals(User expected, User actual) {
        Assertions.assertEquals(expected.getName(), actual.getName(), "Name should match");
        Assertions.assertEquals(expected.getDisplay_name(), actual.getDisplay_name(), "Display name should match");
        Assertions.assertEquals(expected.getGiven_name(), actual.getGiven_name(), "Given name should match");
        Assertions.assertEquals(expected.getSur_name(), actual.getSur_name(), "Surname should match");
        Assertions.assertEquals(expected.getPrimaryEmail(), actual.getPrimaryEmail(), "Primary E-Mail should match");
        Assertions.assertEquals(expected.getEmail1(), actual.getEmail1(), "E-Mail #1 should match");
    }

    /**
     * Compares the {@link User} A with {@link User} B
     *
     * @param a The {@link User} A
     * @param b The {@link User} B
     */
    public static void assertUser(User a, User b) {
        LOG.info("USER A: {}", a.toString());
        LOG.info("USER B: {}", b.toString());

        Assertions.assertEquals(a.getName(), b.getName(), "username not equal");
        Assertions.assertEquals(a.getMailenabled(), b.getMailenabled(), "enabled not equal");
        Assertions.assertEquals(a.getPrimaryEmail(), b.getPrimaryEmail(), "primaryemail not equal");
        Assertions.assertEquals(a.getDisplay_name(), b.getDisplay_name(), "display name not equal");
        Assertions.assertEquals(a.getGiven_name(), b.getGiven_name(), "firstname not equal");
        Assertions.assertEquals(a.getSur_name(), b.getSur_name(), "lastname not equal");
        Assertions.assertEquals(a.getLanguage(), b.getLanguage(), "language not equal");
        // test aliasing comparing the content of the hashset
        Assertions.assertEquals(a.getAliases(), b.getAliases());
        AssertUtil.compareNonCriticFields(a, b);
    }

    /**
     * Asserts that the groups are equal
     *
     * @param expected The expected {@link Group}
     * @param actual The actual {@link Group}
     */
    public static void assertGroupEquals(Group expected, Group actual) {
        Assertions.assertEquals(expected.getDisplayname(), actual.getDisplayname(), "Display name should match");
        Assertions.assertEquals(expected.getName(), actual.getName(), "Name should match");
    }

    /**
     * Asserts that the expected {@link Resource} is equal the actual {@link Resource}
     *
     * @param expected The expected {@link Resource}
     * @param actual The actual {@link Resource}
     */
    public static void assertResourceEquals(Resource expected, Resource actual) {
        Assertions.assertEquals(expected.getDisplayname(), actual.getDisplayname(), "Display name should match");
        Assertions.assertEquals(expected.getName(), actual.getName(), "Name should match");
        Assertions.assertEquals(expected.getEmail(), actual.getEmail(), "E-Mail should match");
    }

    /**
     * Compares two user arrays by retrieving all the IDs they contain
     * an checking if they match. Ignores duplicate entries, ignores
     * users without an ID at all.
     *
     * @param arr1 the first array
     * @param arr2 the second array
     */
    public static void assertIDsAreEqual(User[] arr1, User[] arr2) {
        Set<Integer> set1 = new HashSet<Integer>();
        for (User element : arr1) {
            set1.add(element.getId());
        }
        Set<Integer> set2 = new HashSet<Integer>();
        for (int i = 0; i < arr1.length; i++) {
            set2.add(arr2[i].getId());
        }

        assertEquals(set1, set2, "Both arrays should return the same IDs");
    }

    /**
     * Asserts that both {@link UserModuleAccess} objects are equal
     *
     * @param a The {@link UserModuleAccess} a
     * @param b The {@link UserModuleAccess} b
     */
    public static void compareUserAccess(UserModuleAccess a, UserModuleAccess b) {
        assertEquals(B(a.getCalendar()), B(b.getCalendar()), "access calendar not equal");
        assertEquals(B(a.getContacts()), B(b.getContacts()), "access contacts not equal");
        assertEquals(B(a.getDelegateTask()), B(b.getDelegateTask()), "access delegatetasks not equal");
        assertEquals(B(a.getEditPublicFolders()), B(b.getEditPublicFolders()), "access edit public folders not equal");
        assertEquals(B(a.getIcal()), B(b.getIcal()), "access ical not equal");
        assertEquals(B(a.getInfostore()), B(b.getInfostore()), "access infostore not equal");
        assertEquals(B(a.getReadCreateSharedFolders()), B(b.getReadCreateSharedFolders()), "access ReadCreateSharedFolders not equal");
        assertEquals(B(a.getSyncml()), b.getSyncml() ? Boolean.TRUE : Boolean.FALSE, "access syncml not equal");
        assertEquals(B(a.getTasks()), B(b.getTasks()), "access tasks not equal");
        assertEquals(B(a.getVcard()), B(b.getVcard()), "access vcard not equal");
        assertEquals(B(a.getWebdav()), B(b.getWebdav()), "access webdav not equal");
        assertEquals(B(a.getWebdavXml()), B(b.getWebdavXml()), "access webdav xml not equal");
        assertEquals(B(a.getWebmail()), B(b.getWebmail()), "access webmail not equal");
    }

    /**
     * Asserts that the {@link User} <code>b</code> does not have any of
     * its mandatory fields set to <code>null</code> and then asserts
     * that is equal to {@link User} <code>a</code>
     *
     * @param a The {@link User} a
     * @param b The {@link User} b
     */
    public static void compareUserSpecialForNulledAttributes(User a, User b) {
        LOG.info("USER A: {}", a.toString());
        LOG.info("USER B: {}", b.toString());

        // all these attributes cannot be null | cannot be changed by the server to null/empty
        Assertions.assertNotNull(b.getName(), "username cannot be null");
        Assertions.assertNotNull(b.getMailenabled(), "enabled cannot be null");
        Assertions.assertNotNull(b.getPrimaryEmail(), "primary email cannot be null");
        Assertions.assertNotNull(b.getDisplay_name(), "display name cannot be null");
        Assertions.assertNotNull(b.getGiven_name(), "first name cannot be null");
        Assertions.assertNotNull(b.getSur_name(), "last name cannot be null");
        Assertions.assertNotNull(b.getLanguage(), "language cannot be null");

        // can alias be null?
        // Assertions.assertEquals(a.getAliases(), b.getAliases());
        compareNonCriticFields(a, b);
    }

    private static void compareNonCriticFields(User a, User b) {
        assertDatesAreEqualsAtYMD("aniversary not equal", a.getAnniversary(), b.getAnniversary());
        Assertions.assertEquals(a.getAssistant_name(), b.getAssistant_name(), "assistant's name not equal");
        assertDatesAreEqualsAtYMD("birthday not equal", a.getBirthday(), b.getBirthday());
        Assertions.assertEquals(a.getBranches(), b.getBranches(), "branches not equal");
        Assertions.assertEquals(a.getBusiness_category(), b.getBusiness_category(), "BusinessCategory not equal");
        Assertions.assertEquals(a.getCity_business(), b.getCity_business(), "BusinessCity not equal");
        Assertions.assertEquals(a.getCountry_business(), b.getCountry_business(), "BusinessCountry not equal");
        Assertions.assertEquals(a.getPostal_code_business(), b.getPostal_code_business(), "BusinessPostalCode not equal");
        Assertions.assertEquals(a.getState_business(), b.getState_business(), "BusinessState not equal");
        Assertions.assertEquals(a.getStreet_business(), b.getStreet_business(), "BusinessStreet not equal");
        Assertions.assertEquals(a.getTelephone_callback(), b.getTelephone_callback(), "callback not equal");
        Assertions.assertEquals(a.getCommercial_register(), b.getCommercial_register(), "CommercialRegister not equal");
        Assertions.assertEquals(a.getCompany(), b.getCompany(), "Company not equal");
        Assertions.assertEquals(a.getCountry_home(), b.getCountry_home(), "Country not equal");
        Assertions.assertEquals(a.getDepartment(), b.getDepartment(), "Department not equal");
        Assertions.assertEquals(a.getEmployeeType(), b.getEmployeeType(), "EmployeeType not equal");
        Assertions.assertEquals(a.getFax_business(), b.getFax_business(), "FaxBusiness not equal");
        Assertions.assertEquals(a.getFax_home(), b.getFax_home(), "FaxHome not equal");
        Assertions.assertEquals(a.getFax_other(), b.getFax_other(), "FaxOther not equal");
        Assertions.assertEquals(a.getImapServerString(), b.getImapServerString(), "ImapServer not equal");
        Assertions.assertEquals(a.getInstant_messenger1(), b.getInstant_messenger1(), "InstantMessenger not equal");
        Assertions.assertEquals(a.getInstant_messenger2(), b.getInstant_messenger2(), "InstantMessenger2 not equal");
        Assertions.assertEquals(a.getTelephone_ip(), b.getTelephone_ip(), "IpPhone not equal");
        Assertions.assertEquals(a.getTelephone_isdn(), b.getTelephone_isdn(), "Isdn not equal");
        Assertions.assertEquals(a.getMail_folder_drafts_name(), b.getMail_folder_drafts_name(), "MailFolderDrafts not equal");
        Assertions.assertEquals(a.getMail_folder_sent_name(), b.getMail_folder_sent_name(), "MailFolderSent not equal");
        Assertions.assertEquals(a.getMail_folder_spam_name(), b.getMail_folder_spam_name(), "MailFolderSpam not equal");
        Assertions.assertEquals(a.getMail_folder_trash_name(), b.getMail_folder_trash_name(), "MailFolderTrash not equal");
        Assertions.assertEquals(a.getMail_folder_archive_full_name(), b.getMail_folder_archive_full_name(), "MailFolderArchiveFull not equal");
        Assertions.assertEquals(a.getMail_folder_scheduled_full_name(), b.getMail_folder_scheduled_full_name(), "MailFolderScheduledFull not equal");
        Assertions.assertEquals(a.getManager_name(), b.getManager_name(), "Manager's Name not equal");
        Assertions.assertEquals(a.getMarital_status(), b.getMarital_status(), "MaritalStatus not equal");
        Assertions.assertEquals(a.getCellular_telephone1(), b.getCellular_telephone1(), "Mobile1 not equal");
        Assertions.assertEquals(a.getCellular_telephone2(), b.getCellular_telephone2(), "Mobile2 not equal");
        Assertions.assertEquals(a.getInfo(), b.getInfo(), "MoreInfo not equal");
        Assertions.assertEquals(a.getNickname(), b.getNickname(), "NickName not equal");
        Assertions.assertEquals(a.getNote(), b.getNote(), "Note not equal");
        Assertions.assertEquals(a.getNumber_of_children(), b.getNumber_of_children(), "NumberOfChildren not equal");
        Assertions.assertEquals(a.getNumber_of_employee(), b.getNumber_of_employee(), "NumberOfEmployee not equal");
        Assertions.assertEquals(a.getTelephone_pager(), b.getTelephone_pager(), "Pager not equal");
        Assertions.assertEquals(a.getPassword_expired(), b.getPassword_expired(), "PasswordExpired not equal");
        Assertions.assertEquals(a.getTelephone_assistant(), b.getTelephone_assistant(), "PhoneAssistant not equal");
        Assertions.assertEquals(a.getTelephone_business1(), b.getTelephone_business1(), "PhoneBusiness not equal");
        Assertions.assertEquals(a.getTelephone_business2(), b.getTelephone_business2(), "PhoneBusiness2 not equal");
        Assertions.assertEquals(a.getTelephone_car(), b.getTelephone_car(), "PhoneCar not equal");
        Assertions.assertEquals(a.getTelephone_company(), b.getTelephone_company(), "PhoneCompany not equal");
        Assertions.assertEquals(a.getTelephone_home1(), b.getTelephone_home1(), "PhoneHome not equal");
        Assertions.assertEquals(a.getTelephone_home2(), b.getTelephone_home2(), "PhoneHome2 not equal");
        Assertions.assertEquals(a.getTelephone_other(), b.getTelephone_other(), "PhoneOther not equal");
        Assertions.assertEquals(a.getPosition(), b.getPosition(), "Position not equal");
        Assertions.assertEquals(a.getPostal_code_home(), b.getPostal_code_home(), "PostalCode not equal");
        Assertions.assertEquals(a.getEmail2(), b.getEmail2(), "Email2 not equal");
        Assertions.assertEquals(a.getEmail3(), b.getEmail3(), "Email3 not equal");
        Assertions.assertEquals(a.getProfession(), b.getProfession(), "Profession not equal");
        Assertions.assertEquals(a.getTelephone_radio(), b.getTelephone_radio(), "Radio not equal");
        Assertions.assertEquals(a.getRoom_number(), b.getRoom_number(), "RoomNumber not equal");
        Assertions.assertEquals(a.getSales_volume(), b.getSales_volume(), "SalesVolume not equal");
        Assertions.assertEquals(a.getCity_other(), b.getCity_other(), "SecondCity not equal");
        Assertions.assertEquals(a.getCountry_other(), b.getCountry_other(), "SecondCountry not equal");
        Assertions.assertEquals(a.getMiddle_name(), b.getMiddle_name(), "SecondName not equal");
        Assertions.assertEquals(a.getPostal_code_other(), b.getPostal_code_other(), "SecondPostalCode not equal");
        Assertions.assertEquals(a.getState_other(), b.getState_other(), "SecondState not equal");
        Assertions.assertEquals(a.getStreet_other(), b.getStreet_other(), "SecondStreet not equal");
        Assertions.assertEquals(a.getSmtpServerString(), b.getSmtpServerString(), "SmtpServer not equal");
        Assertions.assertEquals(a.getSpouse_name(), b.getSpouse_name(), "SpouseName not equal");
        Assertions.assertEquals(a.getState_home(), b.getState_home(), "State not equal");
        Assertions.assertEquals(a.getStreet_home(), b.getStreet_home(), "Street not equal");
        Assertions.assertEquals(a.getSuffix(), b.getSuffix(), "Suffix not equal");
        Assertions.assertEquals(a.getTax_id(), b.getTax_id(), "TaxId not equal");
        Assertions.assertEquals(a.getTelephone_telex(), b.getTelephone_telex(), "Telex not equal");
        Assertions.assertEquals(a.getTimezone(), b.getTimezone(), "Timezone not equal");
        Assertions.assertEquals(a.getTitle(), b.getTitle(), "Title not equal");
        Assertions.assertEquals(a.getTelephone_ttytdd(), b.getTelephone_ttytdd(), "TtyTdd not equal");
        Assertions.assertEquals(a.getUrl(), b.getUrl(), "Url not equal");
        Assertions.assertEquals(a.getUserfield01(), b.getUserfield01(), "Userfield01 not equal");
        Assertions.assertEquals(a.getUserfield02(), b.getUserfield02(), "Userfield02 not equal");
        Assertions.assertEquals(a.getUserfield03(), b.getUserfield03(), "Userfield03 not equal");
        Assertions.assertEquals(a.getUserfield04(), b.getUserfield04(), "Userfield04 not equal");
        Assertions.assertEquals(a.getUserfield05(), b.getUserfield05(), "Userfield05 not equal");
        Assertions.assertEquals(a.getUserfield06(), b.getUserfield06(), "Userfield06 not equal");
        Assertions.assertEquals(a.getUserfield07(), b.getUserfield07(), "Userfield07 not equal");
        Assertions.assertEquals(a.getUserfield08(), b.getUserfield08(), "Userfield08 not equal");
        Assertions.assertEquals(a.getUserfield09(), b.getUserfield09(), "Userfield09 not equal");
        Assertions.assertEquals(a.getUserfield10(), b.getUserfield10(), "Userfield10 not equal");
        Assertions.assertEquals(a.getUserfield11(), b.getUserfield11(), "Userfield11 not equal");
        Assertions.assertEquals(a.getUserfield12(), b.getUserfield12(), "Userfield12 not equal");
        Assertions.assertEquals(a.getUserfield13(), b.getUserfield13(), "Userfield13 not equal");
        Assertions.assertEquals(a.getUserfield14(), b.getUserfield14(), "Userfield14 not equal");
        Assertions.assertEquals(a.getUserfield15(), b.getUserfield15(), "Userfield15 not equal");
        Assertions.assertEquals(a.getUserfield16(), b.getUserfield16(), "Userfield16 not equal");
        Assertions.assertEquals(a.getUserfield17(), b.getUserfield17(), "Userfield17 not equal");
        Assertions.assertEquals(a.getUserfield18(), b.getUserfield18(), "Userfield18 not equal");
        Assertions.assertEquals(a.getUserfield19(), b.getUserfield19(), "Userfield19 not equal");
        Assertions.assertEquals(a.getUserfield20(), b.getUserfield20(), "Userfield20 not equal");

        Hashtable<String, OXCommonExtensionInterface> aexts = a.getAllExtensionsAsHash();
        Hashtable<String, OXCommonExtensionInterface> bexts = b.getAllExtensionsAsHash();
        if (aexts.size() == bexts.size()) {
            Assertions.assertTrue(aexts.values().containsAll(bexts.values()),
                "Extensions not equal: " + aexts.toString() + ",\n" + bexts.toString());
            for (int i = 0; i < aexts.size(); i++) {
                OXCommonExtensionInterface aext = aexts.get(I(i));
                OXCommonExtensionInterface bext = bexts.get(I(i));
                Assertions.assertTrue(aext.equals(bext),
                    "Extensions not equal: " + aext.toString() + ",\n" + bext.toString());
            }
        }
    }

    private static void assertDatesAreEqualsAtYMD(String message, Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance(TimeZones.UTC);
        Calendar cal2 = Calendar.getInstance(TimeZones.UTC);
        if (date1 != null && date2 != null) {
            cal1.setTime(date1);
            cal2.setTime(date2);
            assertEquals(cal1.get(Calendar.YEAR), cal2.get(Calendar.YEAR), message);
            assertEquals(cal1.get(Calendar.MONTH), cal2.get(Calendar.MONTH), message);
            assertEquals(cal1.get(Calendar.DAY_OF_MONTH), cal2.get(Calendar.DAY_OF_MONTH), message);
        }
    }
}
