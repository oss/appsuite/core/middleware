/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.reseller.rmi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.openexchange.admin.reseller.rmi.dataobjects.ResellerAdmin;
import com.openexchange.admin.reseller.rmi.dataobjects.Restriction;
import com.openexchange.admin.rmi.dataobjects.Context;
import com.openexchange.admin.rmi.dataobjects.Credentials;
import com.openexchange.admin.rmi.exceptions.InvalidCredentialsException;
import com.openexchange.admin.rmi.exceptions.InvalidDataException;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.openexchange.admin.rmi.factory.ResellerAdminFactory;

public class OXResellerContextTest extends AbstractOXResellerTest {

    private static Context ownedContext = null;

    private ResellerAdmin randomAdmin;

    /**
     * Initialises a new {@link OXResellerContextTest}.
     */
    public OXResellerContextTest() {
        super();
    }

    @Override
    @BeforeEach
    public final void setUp() throws Exception {
        super.setUp();
        randomAdmin = ResellerAdminFactory.createRandomResellerAdmin();
        getResellerManager().create(randomAdmin);
    }

    @Override
    @AfterEach
    public final void tearDown() throws Exception {
        final ResellerAdmin[] adms = getResellerManager().search(randomAdmin.getName());
        for (final ResellerAdmin adm : adms) {
            getResellerManager().delete(adm);
        }
    }

    @Test
    public void testListAllContextInvalidAuthNoUser() throws Exception {
        ResellerAdmin second = ResellerAdminFactory.createRandomResellerAdmin();
        assertThrows(InvalidCredentialsException.class, () -> getContextManager().listAll(ResellerRandomCredentials(second.getName())));
    }

    @Test
    public void testListAllContextInvalidAuthWrongpasswd() throws Exception {
        Credentials creds = ResellerRandomCredentials(randomAdmin.getName());
        creds.setPassword("wrongpass");
        assertThrows(InvalidCredentialsException.class, () -> getContextManager().listAll(creds));
    }

    @Test
    public void testListAllContextValidAuth() throws Exception {
        Credentials creds = ResellerRandomCredentials(randomAdmin.getName());
        getContextManager().listAll(creds);
    }

    @Test
    public void testCreateTooManyContexts() throws Exception {
        getResellerManager().delete(randomAdmin); // Delete normally created FooAdminUser

        randomAdmin.setRestrictions(new Restriction[] { MaxContextRestriction() });
        getResellerManager().create(randomAdmin);

        Credentials creds = ResellerRandomCredentials(randomAdmin.getName());
        Context ctx1 = createContext(creds);
        Context ctx2 = createContext(creds);
        Context ctx3 = null;
        boolean failed_ctx3 = false;
        try {
            ctx3 = createContext(creds);
        } catch (StorageException e) {
            failed_ctx3 = true;
        }

        deleteContext(ctx1, creds);
        deleteContext(ctx2, creds);
        if (ctx3 != null) {
            deleteContext(ctx3, creds);
        }

        assertTrue(failed_ctx3, "creation of ctx3 must fail");

        getResellerManager().delete(randomAdmin);
    }

    @Test
    public void testCreateContextNoQuota() throws Exception {
        getResellerManager().delete(randomAdmin); // Delete normally created FooAdminUser

        randomAdmin.setRestrictions(new Restriction[] { MaxOverallUserRestriction(2) });
        getResellerManager().create(randomAdmin);

        Credentials contextAdmin = ResellerRandomCredentials(randomAdmin.getName());

        Context ctx1 = null;
        boolean failed_ctx1 = false;
        try {
            ctx1 = createContextNoQuota(contextAdmin);
        } catch (InvalidDataException e) {
            failed_ctx1 = true;
        }

        if (ctx1 != null) {
            deleteContext(ctx1, contextAdmin);
        }
        assertTrue(failed_ctx1, "creation of ctx1 must fail");

        getResellerManager().delete(randomAdmin);
    }

    @Test
    public void testCreateTooManyOverallUser() throws Exception {
        getResellerManager().delete(randomAdmin); // Delete normally created FooAdminUser

        randomAdmin.setRestrictions(new Restriction[] { MaxOverallUserRestriction(2) });
        getResellerManager().create(randomAdmin);

        Credentials resellerRandomCredentials = ResellerRandomCredentials(randomAdmin.getName());
        Context ctx1 = createContext(resellerRandomCredentials);
        Context ctx2 = createContext(resellerRandomCredentials);
        Context ctx3 = null;
        boolean failed_ctx3 = false;
        try {
            ctx3 = createContext(resellerRandomCredentials);
        } catch (StorageException e) {
            failed_ctx3 = true;
        }
        deleteContext(ctx1, resellerRandomCredentials);
        deleteContext(ctx2, resellerRandomCredentials);
        if (ctx3 != null) {
            deleteContext(ctx3, resellerRandomCredentials);
        }
        assertTrue(failed_ctx3, "creation of ctx3 must fail");

        getResellerManager().delete(randomAdmin);
    }

    @Test
    public void testListContextOwnedByReseller() throws Exception {
        ResellerAdmin second = ResellerAdminFactory.createRandomResellerAdmin();

        getResellerManager().create(second);

        Credentials resellerRandomCredentials = ResellerRandomCredentials(randomAdmin.getName());
        ownedContext = createContext(resellerRandomCredentials);
        try {
            Context[] ret = getContextManager().listAll(resellerRandomCredentials);
            assertEquals(1, ret.length, "listAll must return one entry");

            ret = getContextManager().listAll(ResellerRandomCredentials(second.getName()));
            assertEquals(0, ret.length, "listAll must return no entries");
        } finally {
            deleteContext(ownedContext, resellerRandomCredentials);
            ownedContext = null;
        }

        getResellerManager().delete(second);

    }

    @Test
    public void testGetDataContextOwnedByReseller() throws Exception {
        Credentials resellerRandomCredentials = ResellerRandomCredentials(randomAdmin.getName());
        ownedContext = createContext(resellerRandomCredentials);
        try {
            boolean fail = false;
            try {
                ResellerAdmin second = ResellerAdminFactory.createRandomResellerAdmin();
                Credentials secondCreds = ResellerRandomCredentials(second.getName());

                getContextManager().getData(ownedContext, secondCreds);
            } catch (Exception e) {
                fail = true;
            }
            assertTrue(fail, "getData on an unowned context must fail");

            Context ctx = getContextManager().getData(ownedContext, resellerRandomCredentials);
            assertEquals(ownedContext.getId(), ctx.getId(), "getData must return context with same id as ownedContext");
        } finally {
            deleteContext(ownedContext, resellerRandomCredentials);
            ownedContext = null;
        }
    }

}
