/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.dataretention.csv;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.config.ConfigurationService;
import com.openexchange.dataretention.DataRetentionExceptionCodes;
import com.openexchange.exception.OXException;
import com.openexchange.log.LogMessageBuilder;

/**
 * {@link CSVDataRetentionConfig} - The configuration for CSV data retention.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class CSVDataRetentionConfig {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CSVDataRetentionConfig.class);

    private static final AtomicReference<CSVDataRetentionConfig> INSTANCE_REF = new AtomicReference<>();

    /**
     * Gets the singleton instance of {@link CSVDataRetentionConfig}.
     *
     * @return The singleton instance of {@link CSVDataRetentionConfig}.
     */
    public static CSVDataRetentionConfig getInstance() {
        CSVDataRetentionConfig tmp = INSTANCE_REF.get();
        if (null == tmp) {
            CSVDataRetentionConfig newInstance = new CSVDataRetentionConfig();
            tmp = INSTANCE_REF.compareAndExchange(null, newInstance);
            if (tmp == null) {
                tmp = newInstance;
            }
        }
        return tmp;
    }

    /**
     * Releases the singleton instance of {@link CSVDataRetentionConfig}.
     */
    public static void releaseInstance() {
        INSTANCE_REF.set(null);
    }

    /*-
     * ######################### MEMBERS #########################
     */

    private File directory;

    private int versionNumber;

    private String clientId;

    private String sourceId;

    private String location;

    private TimeZone timeZone;

    private long rotateLength;

    /**
     * Initializes a new {@link CSVDataRetentionConfig}.
     */
    private CSVDataRetentionConfig() {
        super();
    }

    /**
     * Initializes this configuration with specified configuration service.
     *
     * @param configurationService The configuration service
     * @throws OXException If initialization fails
     */
    public void init(final ConfigurationService configurationService) throws OXException {
        LogMessageBuilder logBuilder = LOG.isInfoEnabled() ? LogMessageBuilder.createLogMessageBuilder(512, 8) : LogMessageBuilder.emptyLogMessageBuilder();
        logBuilder.lfappendln("CSV data retention configuration:");
        // Directory
        {
            final String directoryStr = configurationService.getProperty("com.openexchange.dataretention.dir", "/var/log/open-xchange").trim();
            directory = new File(directoryStr);
            if (!directory.exists()) {
                throw DataRetentionExceptionCodes.IO.create("Directory \"" + directoryStr + "\" does not exist.");
            } else if (!directory.isDirectory()) {
                throw DataRetentionExceptionCodes.IO.create("Pathname \"" + directoryStr + "\" does not denote a directoy.");
            } else if (!directory.canWrite()) {
                throw DataRetentionExceptionCodes.IO.create("Directory \"" + directoryStr + "\" does not grant write permission.");
            }
            logBuilder.appendln("    com.openexchange.dataretention.dir={}", directory.getPath());
        }
        // Version number
        {
            final String versionNumberStr = configurationService.getProperty("com.openexchange.dataretention.versionNumber", "1").trim();
            try {
                versionNumber = Integer.parseInt(versionNumberStr);
            } catch (NumberFormatException e) {
                LOG.error("Property \"com.openexchange.dataretention.versionNumber\" is not a number: {}.Using fallback \"1\" instead.", versionNumberStr);
                versionNumber = 1;
            }
            logBuilder.appendln("    com.openexchange.dataretention.versionNumber={}", Integer.valueOf(versionNumber));
        }
        // Client ID
        {
            final String clientIDStr = configurationService.getProperty("com.openexchange.dataretention.clientID", "").trim();
            if (clientIDStr.isEmpty()) {
                LOG.warn("Missing client ID. Using empty string.");
            }
            clientId = clientIDStr;
            logBuilder.appendln("    com.openexchange.dataretention.clientID={}", clientId);
        }
        // Source ID
        {
            final String srcIDStr = configurationService.getProperty("com.openexchange.dataretention.sourceID", "").trim();
            if (srcIDStr.isEmpty()) {
                LOG.warn("Missing source ID. Using empty string.");
            }
            sourceId = srcIDStr;
            logBuilder.appendln("    com.openexchange.dataretention.sourceID={}", sourceId);
        }
        // Location
        {
            final String locationStr = configurationService.getProperty("com.openexchange.dataretention.location", "").trim();
            if (locationStr.isEmpty()) {
                LOG.warn("Missing location. Using empty string.");
            }
            location = locationStr;
            logBuilder.appendln("    com.openexchange.dataretention.location={}", location);
        }
        // Time zone
        {
            String tzStr = configurationService.getProperty("com.openexchange.dataretention.timeZone", "").trim();
            if (tzStr.isEmpty()) {
                LOG.warn("Missing time zone. Using \"GMT\" as fallback.");
                tzStr = "GMT";
            }
            // Get all available IDs
            final Set<String> ids = new HashSet<String>(Arrays.asList(TimeZone.getAvailableIDs()));
            if (!ids.contains(tzStr)) {
                LOG.error("Time zone ID \"{}\" is not supported. Using \"GMT\" as fallback.", tzStr);
                tzStr = "GMT";
            }
            timeZone = TimeZone.getTimeZone(tzStr);
            logBuilder.appendln("    com.openexchange.dataretention.timeZone={}", timeZone.getID());
        }
        // Rotate length
        {
            String rl = configurationService.getProperty("com.openexchange.dataretention.rotateLength", "0").trim();
            if (rl.isEmpty()) {
                LOG.warn("Missing rotation length. Using \"0\" as fallback.");
                rl = "0";
            }
            try {
                rotateLength = Long.parseLong(rl);
            } catch (NumberFormatException e) {
                LOG.error("Property \"com.openexchange.dataretention.rotateLength\" is not a number: {}.Using fallback \"0\" instead.", rl);
                rotateLength = 0L;
            }
            logBuilder.appendln("    com.openexchange.dataretention.rotateLength={}", Long.valueOf(rotateLength));
        }
        LOG.info(logBuilder.getMessage(), logBuilder.getArgumentsAsArray());
    }

    /**
     * Gets the parent directory of the CSV file.
     *
     * @return The parent directory of the CSV file.
     */
    public File getDirectory() {
        return directory;
    }

    /**
     * Gets the version number which identifies the format version of the file.
     *
     * @return The version number
     */
    public int getVersionNumber() {
        return versionNumber;
    }

    /**
     * Gets the client ID. A string identifying the tenant; e.g. <code>&quot;1UND1&quot;</code>.
     *
     * @return The client ID
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Gets the source ID. Any string identifying the data source; e.g. <code>&quot;GMX_mail_01&quot;</code>.
     *
     * @return The sourceId
     */
    public String getSourceId() {
        return sourceId;
    }

    /**
     * Gets the location of the system generating the CSV file; e.g. <code>&quot;DE/Karlsruhe;</code>.
     *
     * @return The location
     */
    public String getLocation() {
        return location;
    }

    /**
     * Gets the time zone of the location.
     *
     * @return The time zone of the location.
     */
    public TimeZone getTimeZone() {
        return timeZone;
    }

    /**
     * Gets the rotate length.
     *
     * @return The rotate length.
     */
    public long getRotateLength() {
        return rotateLength;
    }
}
