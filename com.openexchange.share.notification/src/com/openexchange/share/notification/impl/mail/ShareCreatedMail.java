/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.share.notification.impl.mail;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MailDateFormat;
import com.openexchange.context.ContextService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.i18n.Translator;
import com.openexchange.i18n.TranslatorFactory;
import com.openexchange.java.Strings;
import com.openexchange.mail.mime.utils.MimeMessageUtility;
import com.openexchange.notification.FullNameBuilder;
import com.openexchange.notification.mail.MailData;
import com.openexchange.notification.mail.MailData.Builder;
import com.openexchange.server.ServiceLookup;
import com.openexchange.serverconfig.ServerConfig;
import com.openexchange.serverconfig.ServerConfigService;
import com.openexchange.session.Session;
import com.openexchange.share.ShareTarget;
import com.openexchange.share.groupware.ModuleSupport;
import com.openexchange.share.groupware.TargetProxy;
import com.openexchange.share.groupware.TargetProxyType;
import com.openexchange.share.notification.impl.ShareCreatedNotification;
import com.openexchange.share.notification.impl.TextSnippets;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link ShareCreatedMail}
 *
 * @author <a href="mailto:steffen.templin@open-xchange.com">Steffen Templin</a>
 * @since v7.8.0
 */
public class ShareCreatedMail extends ShareNotificationMail {

    static final String HAS_SHARED_ITEMS = "has_shared_items";
    static final String USER_MESSAGE = "user_message";
    static final String VIEW_ITEMS_LINK = "view_items_link";
    static final String VIEW_ITEMS_LABEL = "view_items_label";

    protected ShareCreatedMail(MailData data, ServiceLookup services) {
        super(services, data);
    }

    static class CollectVarsData {
        ShareCreatedNotification<InternetAddress> notification;
        String email;

        TextSnippets textSnippets;
        String shareOwnerName;
        HashMap<ShareTarget, TargetProxy> targetProxies;
    }

    /**
     * Initializes this share mail
     *
     * @param notification The notification to send
     * @param services The service lookup
     * @return A {@link ShareCreatedMail}
     * @throws OXException In case of error
     */
    public static ShareCreatedMail init(ShareCreatedNotification<InternetAddress> notification, ServiceLookup services) throws OXException {
        ContextService contextService = services.getServiceSafe(ContextService.class);
        UserService userService = services.getServiceSafe(UserService.class);
        ServerConfigService serverConfigService = services.getServiceSafe(ServerConfigService.class);
        TranslatorFactory translatorFactory = services.getServiceSafe(TranslatorFactory.class);
        ModuleSupport moduleSupport = services.getServiceSafe(ModuleSupport.class);

        Context context = contextService.getContext(notification.getContextID());
        User sharingUser = userService.getUser(notification.getSession().getUserId(), context);
        User targetUser = userService.getUser(notification.getTargetUserID(), context);
        Translator translator = translatorFactory.translatorFor(targetUser.getLocale());
        TextSnippets textSnippets = new TextSnippets(translator);

        List<ShareTarget> shareTargets = notification.getShareTargets();
        HashMap<ShareTarget, TargetProxy> targetProxies = HashMap.newHashMap(shareTargets.size());
        HashSet<TargetProxyType> targetProxyTypes = HashSet.newHashSet(shareTargets.size());
        for (ShareTarget target : shareTargets) {
            TargetProxy targetProxy = moduleSupport.load(target, notification.getSession());
            TargetProxyType proxyType = targetProxy.getProxyType();
            targetProxies.put(target, targetProxy);
            targetProxyTypes.add(proxyType);
        }

        CollectVarsData data = new CollectVarsData();
        data.notification = notification;
        data.shareOwnerName = FullNameBuilder.buildFullName(sharingUser, translator);
        data.targetProxies = targetProxies;
        data.textSnippets = textSnippets;
        data.email = getEmail(context, sharingUser);

        ServerConfig serverConfig = serverConfigService.getServerConfig(
            notification.getHostData().getHost(),
            targetUser.getId(),
            context.getContextId());

        Map<String, Object> vars = prepareShareCreatedVars(data);
        String date = formatCurrentDate(notification.getSession());
        Builder mailData = MailData.newBuilder()
            .setSendingUser(sharingUser)
            .setRecipient(notification.getTransportInfo())
            .setHtmlTemplate("notify.share.create.mail.html.tmpl")
            .setTemplateVars(vars)
            .setMailConfig(serverConfig.getNotificationMailConfig())
            .setContext(context)
            .addMailHeader("Date", date)
            .addMailHeader("X-Open-Xchange-Share-Type", notification.getType().getId())
            .addMailHeader("X-Open-Xchange-Share-URL", notification.getShareUrl());
        if (data.notification.getTargetGroup() == null) {
            mailData.setSubject(textSnippets.shareStatementShort(data.shareOwnerName, data.targetProxies.values()));
        } else {
            mailData.setSubject(textSnippets.shareStatementGroupShort(data.shareOwnerName, data.notification.getTargetGroup().getDisplayName(), data.targetProxies.values()));
        }

        return new ShareCreatedMail(mailData.build(), services);
    }

    private static String formatCurrentDate(Session session) throws OXException {
        MailDateFormat mdf = MimeMessageUtility.getMailDateFormat(session);
        synchronized (mdf) {
            return mdf.format(new Date());
        }
    }

    /**
     * Prepares a mapping from template keywords to actual textual values that will be used during template rendering.
     *
     * @param data The data containing infos about the created share
     * @return A mapping from template keywords to actual textual values
     */
    private static Map<String, Object> prepareShareCreatedVars(CollectVarsData data) {
        Map<String, Object> vars = new HashMap<String, Object>();
        boolean hasMessage = Strings.isNotEmpty(data.notification.getMessage());
        String shareUrl = data.notification.getShareUrl();
        String email = data.email;
        String fullName = data.shareOwnerName;

        String shareStatementLong;
        if (data.notification.getTargetGroup() == null) {
            shareStatementLong = data.textSnippets.shareStatementLong(fullName, email, data.targetProxies.values(), hasMessage);
        } else {
            shareStatementLong = data.textSnippets.shareStatementGroupLong(fullName, email, data.notification.getTargetGroup().getDisplayName(), data.targetProxies.values(), hasMessage);
        }
        vars.put(HAS_SHARED_ITEMS, shareStatementLong);
        if (hasMessage) {
            vars.put(USER_MESSAGE, data.notification.getMessage());
        }

        vars.put(VIEW_ITEMS_LINK, shareUrl);
        vars.put(VIEW_ITEMS_LABEL, data.textSnippets.linkLabel(data.targetProxies.values()));
        return vars;
    }

}
