/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.servlet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.servlet.http.HttpServletRequest;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.openexchange.java.Strings;

/**
 * {@link Headers} - Represents an immutable collection of headers from an HTTP request.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class Headers implements Iterable<Header> {

    /** The function constant to create a new <code>ImmutableList.Builder</code> instance */
    private static final Function<? super String, ? extends ImmutableList.Builder<Header>> FUNCTION_NEW_BUILDER = n -> ImmutableList.builderWithExpectedSize(2);

    /** The function constant to create a new <code>ArrayList</code> instance */
    private static final Function<? super String, ? extends List<Header>> FUNCTION_NEW_ARRAY_LIST = h -> new ArrayList<>(2);

    /** The constant for an empty headers collection */
    private static final Headers EMPTY_HEADERS = new Headers();

    /**
     * Gets the empty collection of headers.
     *
     * @return The empty headers collection
     */
    public static Headers emptyHeaders() {
        return EMPTY_HEADERS;
    }

    /**
     * Gets the headers from given HTTP request.
     *
     * @param req The HTTP request
     * @return The headers
     */
    public static Headers headersFor(HttpServletRequest req) {
        if (null == req) {
            return emptyHeaders();
        }

        Headers headers = (Headers) req.getAttribute("__headers.map");
        if (headers != null) {
            return headers;
        }

        Map<String, List<Header>> result = null;
        for (Enumeration<String> en = req.getHeaderNames(); en.hasMoreElements();) {
            String headerName = en.nextElement();
            if (headerName != null) {
                if (result == null) {
                    result = new LinkedHashMap<>();
                }
                applyHeaderValues(req.getHeaders(headerName), headerName, result);
            }
        }
        Headers retval = result == null ? Headers.emptyHeaders() : new Headers(result);
        req.setAttribute("__headers.map", retval);
        return retval;
    }

    /**
     * Maps a header value enumeration to a list of headers and adds them to a given list
     *
     * @param headerValues The enumeration of header values
     * @param name The header name
     * @param headers The header collection to add the headers to
     */
    private static void applyHeaderValues(Enumeration<String> headerValues, String name, Map<String, List<Header>> headers) {
        String lcn = Strings.asciiLowerCase(name);
        while (headerValues.hasMoreElements()) {
            String value = headerValues.nextElement();
            if (value != null) {
                headers.computeIfAbsent(lcn, FUNCTION_NEW_ARRAY_LIST).add(new Header(name, value));
            }
        }
    }

    // ----------------------------------------------------------------------------------------------------------------

    /**
     * Creates a new builder.
     *
     * @return The newly created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Creates a new builder with specified expected size.
     *
     * @param size The expected size
     * @return The newly created builder
     */
    public static Builder builderWithExpectedSize(int size) {
        return new Builder(size);
    }

    /** The builder for an instance of <code>Headers</code> */
    public static final class Builder {

        private final Map<String, List<Header>> map;

        /**
         * Initializes a new {@link Headers.Builder}.
         */
        Builder() {
            super();
            map = new LinkedHashMap<>();
        }

        /**
         * Initializes a new {@link Headers.Builder}.
         *
         * @param size The expected size
         */
        Builder(int size) {
            super();
            map = LinkedHashMap.newLinkedHashMap(size);
        }

        /**
         * Adds given header to this builder.
         *
         * @param header The header to add
         * @return This builder
         */
        public Builder addHeader(Header header) {
            if (header != null) {
                map.computeIfAbsent(Strings.asciiLowerCase(header.name()), FUNCTION_NEW_ARRAY_LIST).add(header);
            }
            return this;
        }

        /**
         * Adds given name-value-pair as header to this builder.
         *
         * @param name The header name
         * @param value The header value
         * @return This builder
         */
        public Builder addHeader(String name, String value) {
            if (name != null && value != null) {
                map.computeIfAbsent(Strings.asciiLowerCase(name), FUNCTION_NEW_ARRAY_LIST).add(new Header(name, value));
            }
            return this;
        }

        /**
         * Builds the headers collection from this builder's arguments.
         *
         * @return The headers collection
         */
        public Headers build() {
            return map == null ? Headers.emptyHeaders() : new Headers(map);
        }
    }

    // ----------------------------------------------------------------------------------------------------------------

    /** The name to headers mapping */
    private final Map<String, List<Header>> mapping;

    /** The number of headers*/
    private final int size;

    /**
     * Initializes a new empty headers listing.
     */
    public Headers() {
        super();
        this.mapping = Collections.emptyMap();
        this.size = 0;
    }

    /**
     * Initializes a new headers listing from given list.
     *
     * @param headers The initial headers
     */
    public Headers(List<Header> headers) {
        super();
        Map<String, List<Header>> m = Collections.emptyMap();
        int numOfHdrs = 0;
        if (headers != null) {
            Map<String, ImmutableList.Builder<Header>> map = null;
            for (Header header : headers) {
                if (header != null) {
                    if (map == null) {
                        map = LinkedHashMap.newLinkedHashMap(headers.size());
                    }
                    map.computeIfAbsent(Strings.asciiLowerCase(header.name()), FUNCTION_NEW_BUILDER).add(header);
                    numOfHdrs++;
                }
            }
            if (map != null) {
                ImmutableMap.Builder<String, List<Header>> builder = ImmutableMap.builderWithExpectedSize(map.size());
                for (Map.Entry<String, ImmutableList.Builder<Header>> entry : map.entrySet()) {
                    builder.put(entry.getKey(), entry.getValue().build());
                }
                m = builder.build();
            }
        }
        this.mapping = m;
        this.size = numOfHdrs;
    }

    /**
     * Initializes a new headers listing from given mapping.
     *
     * @param headers The initial headers
     */
    public Headers(Map<String, List<Header>> headers) {
        super();
        if (headers == null) {
            this.mapping = Collections.emptyMap();
            this.size = 0;
        } else {
            int sz = headers.size();
            if (sz <= 0) {
                this.mapping = Collections.emptyMap();
                this.size = 0;
            } else {
                ImmutableMap.Builder<String, List<Header>> builder = ImmutableMap.builderWithExpectedSize(sz);
                int numOfHdrs = 0;
                for (Map.Entry<String, List<Header>> headerEntry : headers.entrySet()) {
                    List<Header> values = headerEntry.getValue();
                    builder.put(Strings.asciiLowerCase(headerEntry.getKey()), ImmutableList.copyOf(values));
                    numOfHdrs += values.size();
                }
                this.mapping = builder.build();
                this.size = numOfHdrs;
            }
        }
    }

    /**
     * Initializes a new headers listing from another headers object.
     *
     * @param headers The headers object to copy
     */
    public Headers(Headers headers) {
        super();
        if (headers != null && !headers.isEmpty()) {
            this.mapping = Map.copyOf(headers.mapping);
            this.size = headers.size;
        } else {
            this.mapping = Collections.emptyMap();
            this.size = 0;
        }
    }

    /**
     * Gets the {@link Map} view for this header collection.
     *
     * @return The headers' map
     */
    public Map<String, List<String>> getMapping() {
        int numMappings = size;
        if (numMappings <= 0) {
            return Collections.emptyMap();
        }

        ImmutableMap.Builder<String, List<String>> builder = ImmutableMap.builderWithExpectedSize(numMappings);
        List<Header> headers;
        Header firstHdr;
        for (Map.Entry<String, List<Header>> entry : mapping.entrySet()) {
            headers = entry.getValue();
            firstHdr = headers.get(0);
            builder.put(firstHdr.name(), headers.size() == 1 ? Collections.singletonList(firstHdr.value()) : headers.stream().map(Header::value).toList());
        }
        return builder.build();
    }

    /**
     * Checks if this headers collection is empty.
     *
     * @return <code>true</code> if empty; otherwise <code>false</code>
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Gets the number of headers in this collection.
     *
     * @return The number of headers
     */
    public int size() {
        return size;
    }

    /**
     * Gets the iterator for all headers contained in this collection.
     * <p>
     * The returned instance does not support {@link Iterator#remove()}
     *
     * @return The iterator
     */
    @Override
    public Iterator<Header> iterator() {
        return new IteratorImpl(this.mapping.values().iterator());
    }

    /**
     * Gets the first encountered header for given name.
     *
     * @param name The header name to look-up by
     * @return The first header or <code>null</code> if there is no such header
     */
    public Header getFirstHeader(String name) {
        if (Strings.isEmpty(name)) {
            return null;
        }
        List<Header> list = mapping.get(Strings.asciiLowerCase(name));
        return list == null ? null : list.get(0);
    }

    /**
     * Gets the headers for given name.
     *
     * @param name The header name to look-up by
     * @return The headers or empty list if there is no such header
     */
    public List<Header> getHeader(String name) {
        if (Strings.isEmpty(name)) {
            return Collections.emptyList();
        }
        List<Header> list = mapping.get(Strings.asciiLowerCase(name));
        return list == null ? Collections.emptyList() : new ArrayList<>(list);
    }

    /**
     * Gets the value of the first encountered header for specified header name.
     * <p>
     * Returns <code>null</code> if no headers with the specified name exist.
     *
     * @param name The header name
     * @return The value or <code>null</code>
     */
    public String getFirstHeaderValue(String name) {
        return getHeaderValue(name, null);
    }

    /**
     * Gets all the headers for specified header name, returned as a single String, with headers separated by the delimiter.
     * <p>
     * If the delimiter is <code>null</code>, only the first header is returned.
     * <p>
     * Returns <code>null</code> if no headers with the specified name exist.
     *
     * @param name The header name
     * @param delimiter The delimiter
     * @return The value fields for all headers with given name, or <code>null</code> if none
     */
    public String getHeaderValue(String name, String delimiter) {
        List<Header> s = getHeader(name);

        int numOfHeaders = s.size();
        if (numOfHeaders <= 0) {
            return null;
        }

        if (delimiter == null || numOfHeaders == 1) {
            return s.get(0).value();
        }

        return s.stream()
                .map(Header::value)
                .collect(Collectors.joining(delimiter));
    }

    @Override
    public Spliterator<Header> spliterator() {
        return Spliterators.spliterator(iterator(), size, 0);
    }

    /**
     * Returns a sequential stream of the headers contained in this collection.
     *
     * @return The stream of headers
     */
    public Stream<Header> stream() {
        return StreamSupport.stream(spliterator() , false);
    }

    // ---------------------------------------------------------------------------------------------------

    /** The iterator for headers collection */
    private static class IteratorImpl implements Iterator<Header> {

        private final Iterator<List<Header>> iterator;
        private Iterator<Header> headers;

        /**
         * Initializes a new {@link IteratorImplementation}.
         *
         * @param iterator The backing iterator
         */
        IteratorImpl(Iterator<List<Header>> iterator) {
            super();
            this.iterator = iterator;
            headers = iterator.hasNext() ? iterator.next().iterator() : null;
        }

        @Override
        public Header next() {
            if (headers == null || !headers.hasNext()) {
                throw new NoSuchElementException("No next header available");
            }
            Header header = headers.next();
            if (!headers.hasNext()) {
                headers = iterator.hasNext() ? iterator.next().iterator() : null;
            }
            return header;
        }

        @Override
        public boolean hasNext() {
            return headers != null && headers.hasNext();
        }
    }

}
