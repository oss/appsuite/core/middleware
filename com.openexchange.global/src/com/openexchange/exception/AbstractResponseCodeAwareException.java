/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * {@link AbstractResponseCodeAwareException} is an {@link Exception} which is aware of its status code
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class AbstractResponseCodeAwareException extends Exception {

    private static final long serialVersionUID = 3226223198724955446L;

    private final int code;
    private final Map<String, String> headers = new HashMap<>();

    // --------------------- code only constructors -------------------------

    /**
     * Initializes a new {@link AbstractResponseCodeAwareException}.
     *
     * @param code
     */
    protected AbstractResponseCodeAwareException(int code) {
        this((String) null, code);
    }

    /**
     * Initializes a new {@link AbstractResponseCodeAwareException}.
     *
     * @param message
     * @param code
     */
    protected AbstractResponseCodeAwareException(String message, int code) {
        this(message, null, code);
    }

    /**
     * Initializes a new {@link AbstractResponseCodeAwareException}.
     *
     * @param message
     * @param throwable
     * @param code
     */
    protected AbstractResponseCodeAwareException(String message, Throwable throwable, int code) {
        super(message, throwable);
        this.code = code;
    }

    /**
     * Initializes a new {@link AbstractResponseCodeAwareException}.
     *
     * @param throwable
     * @param code
     */
    protected AbstractResponseCodeAwareException(Throwable throwable, int code) {
        this(null, throwable, code);
    }

    // ------------------------ code and header constructors ---------------------------

    /**
     * Initializes a new {@link AbstractResponseCodeAwareException}.
     *
     * @param code
     * @param headers The additional response headers
     */
    protected AbstractResponseCodeAwareException(int code, Map<String, String> headers) {
        this((String) null, code);
        this.headers.putAll(headers);
    }

    /**
     * Initializes a new {@link AbstractResponseCodeAwareException}.
     *
     * @param message
     * @param code
     * @param headers The additional response headers
     */
    protected AbstractResponseCodeAwareException(String message, int code, Map<String, String> headers) {
        this(message, null, code);
        this.headers.putAll(headers);
    }

    /**
     * Initializes a new {@link AbstractResponseCodeAwareException}.
     *
     * @param message
     * @param throwable
     * @param code
     * @param headers The additional response headers
     */
    protected AbstractResponseCodeAwareException(String message, Throwable throwable, int code, Map<String, String> headers) {
        super(message, throwable);
        this.code = code;
        this.headers.putAll(headers);
    }

    /**
     * Initializes a new {@link AbstractResponseCodeAwareException}.
     *
     * @param throwable
     * @param code
     * @param headers The additional response headers
     */
    protected AbstractResponseCodeAwareException(Throwable throwable, int code, Map<String, String> headers) {
        this(null, throwable, code);
        this.headers.putAll(headers);
    }

    // ---------------------------- getter methods -------------------

    /**
     * Gets the code
     *
     * @return The code
     */
    public int getCode() {
        return code;
    }

    /**
     * Gets additional headers to add to the response
     *
     * @return The headers
     */
    public Map<String, String> getAdditionalHeaders() {
        return headers;
    }

}
