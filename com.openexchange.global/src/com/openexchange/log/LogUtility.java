/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.log;

import static com.openexchange.java.Autoboxing.I;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.exception.LogLevel;

/**
 * {@link LogUtility} - Utility class for logging.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.5
 */
public final class LogUtility {

    /**
     * Initializes a new {@link LogUtility}.
     */
    private LogUtility() {
        super();
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static class IntArrayString {

        private final int[] integers;
        private final CharSequence optDelimiter;

        IntArrayString(int[] integers, CharSequence optDelimiter) {
            super();
            this.integers = integers;
            this.optDelimiter = optDelimiter;
        }

        @Override
        public String toString() {
            int iMax = integers.length - 1;
            StringBuilder b = new StringBuilder();

            if (optDelimiter != null) {
                for (int i = 0; ; i++) {
                    b.append(integers[i]);
                    if (i == iMax) {
                        return b.toString();
                    }
                    b.append(optDelimiter);
                }
            }

            b.append('[');
            for (int i = 0; ; i++) {
                b.append(integers[i]);
                if (i == iMax) {
                    return b.append(']').toString();
                }
                b.append(", ");
            }
        }
    }

    /**
     * Creates a {@link #toString()} object for given integer array.
     *
     * @param integers The integer array
     * @return The object providing content of given array if {@link #toString()} is invoked
     */
    public static Object toStringObjectFor(int[] integers) {
        return toStringObjectFor(integers, null);
    }

    /**
     * Creates a {@link #toString()} object for given integer array.
     *
     * @param integers The integer array
     * @param optDelimiter The delimiter to be used between each element or <code>null</code> for element-wise output
     * @return The object providing content of given array if {@link #toString()} is invoked
     */
    public static Object toStringObjectFor(int[] integers, CharSequence optDelimiter) {
        if (integers == null) {
            return "null";
        }

        if (integers.length <= 0) {
            return optDelimiter == null ? "[]" : "";
        }

        return new IntArrayString(integers, optDelimiter);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static class ObjectArrayString<O> {

        private final O[] objects;
        private final CharSequence optDelimiter;

        ObjectArrayString(O[] objects, CharSequence optDelimiter) {
            super();
            this.objects = objects;
            this.optDelimiter = optDelimiter;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + Arrays.deepHashCode(objects);
            result = prime * result + ((optDelimiter == null) ? 0 : optDelimiter.hashCode());
            return result;
        }

        @Override
        public String toString() {
            return optDelimiter == null ? Arrays.toString(objects) : Arrays.stream(objects).map(String::valueOf).collect(Collectors.joining(", "));
        }
    }

    /**
     * Creates a {@link #toString()} object for given object array.
     *
     * @param <O> The element type
     * @param objects The object array
     * @return The object providing content of given array if {@link #toString()} is invoked
     */
    public static <O> Object toStringObjectFor(O[] objects) {
        return toStringObjectFor(objects, null);
    }

    /**
     * Creates a {@link #toString()} object for given object array.
     *
     * @param <O> The element type
     * @param objects The object array
     * @param optDelimiter The delimiter to be used between each element or <code>null</code> for element-wise output
     * @return The object providing content of given array if {@link #toString()} is invoked
     */
    public static <O> Object toStringObjectFor(O[] objects, CharSequence optDelimiter) {
        if (objects == null) {
            return "null";
        }

        if (objects.length <= 0) {
            return optDelimiter == null ? "[]" : "";
        }

        return new ObjectArrayString<O>(objects, optDelimiter);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static class ObjectCollectionString<O> {

        private final Collection<O> objects;
        private final CharSequence optDelimiter;
        private final int maxSize;

        ObjectCollectionString(Collection<O> objects, CharSequence optDelimiter, int maxSize) {
            super();
            this.objects = objects;
            this.optDelimiter = optDelimiter;
            this.maxSize = maxSize;
        }

        @Override
        public String toString() {
            if (-1 < maxSize && maxSize < objects.size()) {
                if (null != optDelimiter) {
                    return objects.stream().limit(maxSize).map(String::valueOf).collect(Collectors.joining(optDelimiter)) + ", ...";
                }
                return '[' + objects.stream().limit(maxSize).map(String::valueOf).collect(Collectors.joining(", ")) + ", ...]";
            }
            return optDelimiter == null ? objects.toString() : objects.stream().map(String::valueOf).collect(Collectors.joining(", "));
        }
    }

    /**
     * Creates a {@link #toString()} object for given object collection.
     *
     * @param <O> The element type
     * @param objects The object collection
     * @return The object providing content of given collection if {@link #toString()} is invoked
     */
    public static <O> Object toStringObjectFor(Collection<O> objects) {
        return toStringObjectFor(objects, null);
    }

    /**
     * Creates a {@link #toString()} object for given object collection.
     *
     * @param <O> The element type
     * @param objects The object collection
     * @param optDelimiter The delimiter to be used between each element or <code>null</code> for element-wise output
     * @return The object providing content of given collection if {@link #toString()} is invoked
     */
    public static <O> Object toStringObjectFor(Collection<O> objects, CharSequence optDelimiter) {
        return toStringObjectFor(objects, optDelimiter, -1);
    }

    /**
     * Creates a {@link #toString()} object for given object collection.
     *
     * @param <O> The element type
     * @param objects The object collection
     * @param optDelimiter The delimiter to be used between each element or <code>null</code> for element-wise output
     * @param maxSize The maximum number of elements in the collection to consider in the string representation, or <code>-1</code> for no limitation
     * @return The object providing content of given collection if {@link #toString()} is invoked
     */
    public static <O> Object toStringObjectFor(Collection<O> objects, CharSequence optDelimiter, int maxSize) {
        if (objects == null) {
            return "null";
        }

        if (objects.isEmpty()) {
            return optDelimiter == null ? "[]" : "";
        }

        return new ObjectCollectionString<O>(objects, optDelimiter, maxSize);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static class ObjectString<O> {

        private final Supplier<O> supplier;

        ObjectString(Supplier<O> supplier) {
            super();
            this.supplier = supplier;
        }

        @Override
        public String toString() {
            O stringForMe = supplier.get();
            return stringForMe == null ? "null" : stringForMe.toString();
        }
    }

    /**
     * Creates a {@link #toString()} object for given supplier.
     *
     * @param <O> The element type
     * @param supplier The supplier
     * @return The object providing content of given supplier if {@link #toString()} is invoked
     */
    public static <O> Object toStringObjectFor(Supplier<O> supplier) {
        if (supplier == null) {
            return "null";
        }

        return new ObjectString<O>(supplier);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static class IdentityHashCodeString {

        private final Object object;

        IdentityHashCodeString(Object object) {
            super();
            this.object = object;
        }

        @Override
        public String toString() {
            return object == null ? "null" : Integer.toString(System.identityHashCode(object));
        }
    }

    /**
     * Creates a {@link #toString()} for identity hash code for given object.
     *
     * @param object The object
     * @return The object providing identity hash code if {@link #toString()} is invoked
     */
    public static Object toIdentityHashCodeFor(Object object) {
        if (object == null) {
            return "null";
        }

        return new IdentityHashCodeString(object);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** Cache to remember when a certain message has been logged with increased log level */
    private static final Cache<Object, Boolean> INCREASED_LOG_INVOCATIONS = CacheBuilder.newBuilder().maximumSize(1 << 20).expireAfterWrite(1, TimeUnit.DAYS).build();

    /**
     * Logs a (possibly frequently occurring) message using an increased log level only once per day while silencing all further other
     * occurrences, to raise awareness on the one hand, but reduce repeated/unnecessary noise on the other hand.
     * <p/>
     * The log message itself (including the format string and arguments, ignoring {@link Throwable}s) is used implicitly as key for
     * {@link #logOncePerDay(Object, org.slf4j.Logger, LogLevel, LogLevel, String, Object...)}.
     *
     * @param logger The logger to use for outputting
     * @param increasedLevel The increased log level to use (once per day), typically {@link LogLevel#INFO}, {@link LogLevel#WARNING} or {@link LogLevel#ERROR}
     * @param silentLevel The "silent" log level to use otherwise, typically {@link LogLevel#DEBUG} or {@link LogLevel#TRACE}
     * @param format The log format string to pass to the logger
     * @param arguments The log arguments to pass to the logger
     */
    public static void logOncePerDay(org.slf4j.Logger logger, LogLevel increasedLevel, LogLevel silentLevel, String format, Object... arguments) {
        logOncePerDay(getKey(format, arguments), logger, increasedLevel, silentLevel, format, arguments);
    }

    /**
     * Logs a (possibly frequently occurring) message using an increased log level only once per day while silencing all further other
     * occurrences, to raise awareness on the one hand, but reduce repeated/unnecessary noise on the other hand.
     *
     * @param key An arbitrary key representing the log event
     * @param logger The logger to use for outputting
     * @param increasedLevel The increased log level to use (once per day), typically {@link LogLevel#INFO}, {@link LogLevel#WARNING} or {@link LogLevel#ERROR}
     * @param silentLevel The "silent" log level to use otherwise, typically {@link LogLevel#DEBUG} or {@link LogLevel#TRACE}
     * @param format The log format string to pass to the logger
     * @param arguments The log arguments to pass to the logger
     */
    public static void logOncePerDay(Object key, org.slf4j.Logger logger, LogLevel increasedLevel, LogLevel silentLevel, String format, Object... arguments) {
        LogLevel logLevel = getEffectiveLogLevel(key, increasedLevel, silentLevel);
        if (logLevel.appliesTo(logger)) {
            switch (logLevel) {
                case TRACE:
                    logger.trace(format, arguments);
                    break;
                case DEBUG:
                    logger.debug(format, arguments);
                    break;
                case INFO:
                    logger.info(format, arguments);
                    break;
                case WARNING:
                    logger.warn(format, arguments);
                    break;
                case ERROR:
                    logger.error(format, arguments);
                    break;
                default:
                    org.slf4j.LoggerFactory.getLogger(LogUtility.class).warn("Unknown log level \"{}\", falling back to {}", logLevel, LogLevel.INFO);
                    logger.info(format, arguments);
                    break;
            }
        }
    }

    /**
     * Calculates a key for the given log message and arguments.
     *
     * @param format The format string to use for key calculation
     * @param arguments The log arguments to use for key calculation
     * @return A key representing the log message and arguments
     */
    private static Object getKey(String format, Object... arguments) {
        int hash = Objects.hashCode(format);
        if (null != arguments) {
            for (Object argument : arguments) {
                if (argument instanceof Throwable) {
                    continue;
                }
                hash = 31 * hash + Objects.hash(argument);
            }
        }
        return I(hash);
    }

    /**
     * Determines whether the increased, or the silent log level should be used for the next log event.
     *
     * @param key An arbitrary key representing the log event
     * @param increasedLevel The increased log level to use (once per day), typically {@link LogLevel#INFO}, {@link LogLevel#WARNING} or {@link LogLevel#ERROR},
     * @param silentLevel The "silent" log level to use otherwise, typically {@link LogLevel#DEBUG} or {@link LogLevel#TRACE}
     * @return The effective log level to use
     */
    private static LogLevel getEffectiveLogLevel(Object key, LogLevel increasedLevel, LogLevel silentLevel) {
        // Avoid creating instances of AtomicReference and Callable when most of the time there is a non-expired cache entry available
        if (null != INCREASED_LOG_INVOCATIONS.getIfPresent(key)) {
            // Cache entry available. Thus, silent log level should be used
            return silentLevel;
        }

        // Well, check for effective log level in a thread-safe manner
        AtomicReference<LogLevel> levelToUse = new AtomicReference<LogLevel>(silentLevel);
        try {
            INCREASED_LOG_INVOCATIONS.get(key, () -> {
                levelToUse.set(increasedLevel);
                return Boolean.TRUE;
            });
        } catch (ExecutionException e) {
            org.slf4j.LoggerFactory.getLogger(LogUtility.class).warn("Unexpected error checking whether increased log level is due", e);
        }
        return levelToUse.get();
    }

}
