/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.log;

import static com.eaio.util.text.HumanTime.exactly;
import java.time.Duration;

/**
 * {@link DurationOutputter} - Simple class to output a duration in a human-readable format.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class DurationOutputter {

    /**
     * Initializes a new {@link DurationOutputter} with given start time.
     * <p>
     * Invoking its {@link #toString()} method will determine the time delta or duration by substracting from {@link System#nanoTime()}
     *
     * @param startNanos The start time taken from {@link System#nanoTime()}
     * @return The outputter instance
     */
    public static DurationOutputter startNanos(long startNanos) {
        return new DurationOutputter(startNanos, true, true);
    }

    /**
     * Initializes a new {@link DurationOutputter} with given duration in nanoseconds.
     *
     * @param durationNanos The duration in nanoseconds
     * @return The outputter instance
     */
    public static DurationOutputter durationNanos(long durationNanos) {
        return new DurationOutputter(durationNanos, false, true);
    }

    /**
     * Initializes a new {@link DurationOutputter} with given start time.
     * <p>
     * Invoking its {@link #toString()} method will determine the time delta or duration by substracting from {@link System#currentTimeMillis()}
     *
     * @param startMillis The start time taken from {@link System#currentTimeMillis()}
     * @return The outputter instance
     */
    public static DurationOutputter startMillis(long startMillis) {
        return new DurationOutputter(startMillis, true, false);
    }

    /**
     * Initializes a new {@link DurationOutputter} with given duration in milliseconds.
     *
     * @param durationMillis The duration in milliseconds
     * @return The outputter instance
     */
    public static DurationOutputter durationMillis(long durationMillis) {
        return new DurationOutputter(durationMillis, false, false);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final long units;
    private final boolean start;
    private final boolean nanos;

    /**
     * Initializes a new {@link DurationOutputter}.
     *
     * @param untis The time units; either nanoseconds or milliseconds
     * @param start <code>true</code> if given nanoseconds are a start time or <code>false</code> for the duration
     * @param nanos <code>true</code> if units refer to nanoseconds; otherwise <code>false</code> if units refer to milliseconds
     */
    private DurationOutputter(long units, boolean start, boolean nanos) {
        super();
        this.units = units;
        this.start = start;
        this.nanos = nanos;
    }

    @Override
    public String toString() {
        long durMillis;
        if (nanos) {
            long durNanos = start ? (System.nanoTime() - units) : units;
            if (1000000 > durNanos) {
                // smaller than one millisecond, format as microseconds
                return String.format("%d\u00b5s", Integer.valueOf((int) durNanos / 1000));
            }
            durMillis = Duration.ofNanos(durNanos).toMillis();
        } else {
            durMillis = start ? (System.currentTimeMillis() - units) : units;
        }
        return exactly(durMillis, true);
    }

}
