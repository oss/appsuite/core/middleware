/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.log.extensions;

import java.util.Collections;
import java.util.List;
import com.openexchange.java.Strings;

/**
 * Creates a parsable string for logging. The result will have the following structure:
 * <pre>
 * {@code
 * <prefix>:<operation>:<objectType>:<result>:<operationExecutor>:<contextId>:<userId(s)>
 * }
 * {@code
 *    myextrauserlogger:changed:user:ok:admin@context2.oxoe.int:2:5
 *    ExtensionLogs:created:context:ok:masteradmin@ox.int:1111
 * }
 * </pre>
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public final class ParsableLogCreator {

    private final String prefix;
    private final Operation operation;
    private final ObjectType objectType;
    private final ResultType resultType;
    private final String operationExecutor;
    private final Integer contextId;
    private final List<Integer> userIds;

    /**
     *
     * Initializes a new {@link ParsableLogCreator}.
     *
     * @param prefix - The prefix that will be used for logging
     * @param operation - The operation to log
     * @param objectType - The type of object that was involved
     * @param resultType - The result of the operation
     * @param operationExecutor - The one execution the operation
     * @param contextId - The context id (if available) the operation relates to
     * @param userIds - The user id(s) the operation relates to
     */
    private ParsableLogCreator(String prefix, Operation operation, ObjectType objectType, ResultType resultType, String operationExecutor, Integer contextId, List<Integer> userIds) {
        super();
        this.prefix = prefix;
        this.operation = operation;
        this.objectType = objectType;
        this.resultType = resultType;
        this.operationExecutor = operationExecutor;
        this.contextId = contextId;
        this.userIds = userIds;
    }

    @Override
    public String toString() {
        StringBuilder log = new StringBuilder(prefix);
        log.append(':').append(operation.getIdentifier());
        log.append(':').append(objectType.getIdentifier());
        log.append(':').append(resultType.getIdentifier());
        log.append(':').append(Strings.isNotEmpty(operationExecutor) ? operationExecutor : "n/a");
        log.append(':').append((contextId != null && contextId.intValue() > 0) ? Integer.toString(contextId.intValue()) : "n/a");
        if (userIds != null && !userIds.isEmpty()) {
            log.append(':');
            for (Integer userId : userIds) {
                log.append(userId.intValue()).append(',');
            }
            log.setLength(log.length() - 1); // Cut off last ','
        }
        return log.toString();
    }

    // ------------------------------------------------- Builder stuff ---------------------------------------------------------------------

    /**
     * Creates a new builder for an instance of <code>ParsableLogCreator</code>.
     *
     * @return The builder
     */
    public static Builder builder(String prefix) {
        return new Builder(prefix);
    }

    /**
     * Builder class to create {@link ParsableLogCreator}
     */
    public static final class Builder {

        private final String prefix;
        private Operation operation;
        private ObjectType objectType;
        private ResultType resultType;
        private String operationExecutor;
        private Integer contextId;
        private List<Integer> userIds;

        /**
         * Initializes a new {@link Builder}.
         *
         * @param prefix The prefix that will be used for logging
         * @throws {@link IllegalArgumentException} if prefix is <code>null</code> or empty
         */
        Builder(String prefix) {
            super();
            if (Strings.isEmpty(prefix)) {
                throw new IllegalArgumentException("Prefix cannot be null or empty.");
            }
            this.prefix = prefix;
        }

        /**
         * Sets the operation
         *
         * @param type The operation
         * @return This builder
         */
        public Builder withOperation(Operation operation) {
            this.operation = operation;
            return this;
        }

        /**
         * Sets the objectType
         *
         * @param type The objectType
         * @return This builder
         */
        public Builder withObjectType(ObjectType objectType) {
            this.objectType = objectType;
            return this;
        }

        /**
         * Sets if successful
         *
         * @param type The successful
         * @return This builder
         */
        public Builder withSuccessful(boolean successful) {
            this.resultType = successful ? ResultType.OK : ResultType.KO;
            return this;
        }

        /**
         * Sets the user that triggered the action
         *
         * @param type The user
         * @return This builder
         */
        public Builder withOperationExecutor(String operationExecutor) {
            this.operationExecutor = operationExecutor;
            return this;
        }

        /**
         * Sets the contextId
         *
         * @param type The contextId
         * @return This builder
         */
        public Builder withContextId(Integer contextId) {
            this.contextId = contextId;
            return this;
        }

        /**
         * Sets the userIds
         *
         * @param type The userIds
         * @return This builder
         */
        public Builder withUserIds(List<Integer> userIds) {
            this.userIds = userIds;
            return this;
        }

        /**
         * Sets the userIds
         *
         * @param type The userIds
         * @return This builder
         */
        public Builder withUserId(Integer userId) {
            return withUserIds(Collections.singletonList(userId));
        }

        /**
         * Creates the instance of <code>ParsableLogCreator</code> from this builder's arguments
         *
         * @return The <code>ParsableLogCreator</code> instance
         * @throws {@link IllegalArgumentException} if operation, objectType or resultType are not set
         */
        public ParsableLogCreator build() {
            if (operation == null) {
                throw new IllegalArgumentException("Operation needs to be set.");
            }
            if (objectType == null) {
                throw new IllegalArgumentException("Object type needs to be set.");
            }
            if (resultType == null) {
                throw new IllegalArgumentException("Result type needs to be set.");
            }
            return new ParsableLogCreator(prefix, operation, objectType, resultType, operationExecutor, contextId, userIds);
        }
    }

    // ------------------------------------------------ Enums ------------------------------------------------------------------------------

    /**
     * Defines the type of the result.
     */
    public enum ResultType {
        /**
         * The executed operation was successful
         */
        OK("ok"),
        /**
         * The executed operation ended with an error
         */
        KO("ko");
        ;

        private final String identifier;

        private ResultType(String identifier) {
            this.identifier = identifier;
        }

        /**
         * Gets the identifier.
         *
         * @return The identifier
         */
        public String getIdentifier() {
            return identifier;
        }
    }

    /**
     * Defines the operation that relates to the log event.
     */
    public enum Operation {
        /**
         * The operation relates to a 'create' operation
         */
        CREATE("create"),
        /**
         * The operation relates to a 'delete' operation
         */
        DELETE("delete"),
        /**
         * The operation relates to a 'change' operation
         */
        CHANGE("change"),
        /**
         * The operation relates to a 'deactivate' operation
         */
        DEACTIVATE("deactivate"),
        /**
         * The operation relates to a an 'activate' operation
         */
        ACTIVATE("activate"),
        /**
         * The operation relates to a 'downgrade' operation
         */
        DOWNGRADE("downgrade"),
        ;

        private final String identifier;

        private Operation(String identifier) {
            this.identifier = identifier;
        }

        /**
         * Gets the identifier.
         *
         * @return The identifier
         */
        public String getIdentifier() {
            return identifier;
        }
    }

    /**
     * Defines the object that relates to the log event.
     */
    public enum ObjectType {
        CONTEXT("context"),
        FILESTORE("filestore"),
        QUOTA("quota"),
        CAPABILITY("capability"),
        CONTEXT_DATABASE("context_database"),
        MODULE_ACCESS("module_access"),
        CLAIM("claim"),
        MAIL_ADDRESS("mail_address"),
        USER("user"),
        ;

        private final String identifier;

        private ObjectType(String identifier) {
            this.identifier = identifier;
        }

        /**
         * Gets the identifier.
         *
         * @return The identifier
         */
        public String getIdentifier() {
            return identifier;
        }
    }
}
