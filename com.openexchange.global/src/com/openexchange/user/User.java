/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.user;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;
import com.openexchange.groupware.contexts.FileStorageInfo;
import com.openexchange.java.Strings;

/**
 * Interface for the user object.
 *
 * @author <a href="mailto:marcus@open-xchange.org">Marcus Klein</a>
 */
public interface User extends FileStorageInfo, Serializable {

    /**
     * Gets the user password.
     *
     * @return The user password
     */
    String getUserPassword();

    /**
     * Gets the mechanism for encrypting the password.
     *
     * @return The mechanism for encrypting the password
     */
    String getPasswordMech();

    /**
     * Gets the salt used for encrypting the password.
     *
     * @return The salt used for encrypting the password
     */
    byte[] getSalt();

    /**
     * Gets the user identifier.
     *
     * @return The user identifier
     */
    int getId();

    /**
     * Gets the identifier of the creator if this user is a guest.
     *
     * @return The user identifier of the creator or <code>0</code> if this instance is a fully fledged user.
     */
    int getCreatedBy();

    /**
     * Checks if this user is a guest.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>.
     */
    boolean isGuest();

    /**
     * Checks if this user is enabled.
     *
     * @return <code>true</code> if user is enabled; otherwise <code>false</code>
     */
    boolean isMailEnabled();

    /**
     * Getter for shadowLastChange.
     * @return Days since Jan 1, 1970 that password was last changed.
     */
    int getShadowLastChange();

    /**
     * Gets the host name of the primary IMAP server.
     *
     * @return The host name of the primary IMAP server
     */
    String getImapServer();

    /**
     * Gets the login for the primary IMAP server.
     * <p>
     * This field must not be filled. Check IMAP configuration.
     * @return The login for the IMAP server.
     */
    String getImapLogin();

    /**
     * Gets the host name of the primary SMTP server.
     *
     * @return The host name of the primary SMTP server
     */
    String getSmtpServer();

    /**
     * Gets the mail domain.
     *
     * @return The mail domain.
     */
    String getMailDomain();

    /**
     * Gets the given name.
     *
     * @return The given name
     */
    String getGivenName();

    /**
     * Gets the sure name.
     *
     * @return The sure name
     */
    String getSurname();

    /**
     * Gets the primary mail address.
     *
     * @return The mail address.
     */
    String getMail();

    /**
     * Gets the alias mail addresses.
     *
     * @return The mail aliases
     */
    String[] getAliases();

    /**
     * Gets the user attributes as an unmodifiable map.
     *
     * @return The user attributes
     */
    Map<String, String> getAttributes();

    /**
     * Gets the display name.
     *
     * @return The display name.
     */
    String getDisplayName();

    /**
     * Gets the identifier of the time zone for this user.
     *
     * @return The time zone identifier
     */
    String getTimeZone();

    /**
     * Gets the preferred language of the user.
     * <p>
     * According to RFC 2798 and 2068 it should be something like <code>"de-de"</code>, <code>"en-gb"</code> or <code>"en"</code>.
     *
     * @return The preferred language.
     */
    String getPreferredLanguage();

    /**
     * Gets the locale deduced from preferred language.
     *
     * @see #getPreferredLanguage()
     * @return The locale
     */
    Locale getLocale();

    /**
     * Gets the identifiers of the groups this user is member of.
     *
     * @return The group identifiers
     */
    int[] getGroups();

    /**
     * Gets the identifier of the contact associated with this user.
     *
     * @return The contact identifier
     */
    int getContactId();

    /**
     * Gets the identifier for the owner of the file storage associated with this user.
     * <p>
     * Provided that {@link #getFilestoreId()} returns a positive, valid file storage identifier:<br>
     * If a value less than/equal to zero is returned, then {@link #getId()} is supposed to be considered as the file storage owner;
     * otherwise the returned value itself
     *
     * @return The identifier of the owner or a value less than/equal to zero if there is none
     */
    int getFileStorageOwner();

    /**
     * TODO a user can have multiple logins.
     * @return the login information of the user.
     */
    String getLoginInfo();

    /**
     * Checks if this user is an anonymous guest user or not.
     *
     * @return <code>true</code> if this user is an anonymous guest user, <code>false</code> otherwise
     */
    default boolean isAnonymousGuest() {
        return this.isGuest() && Strings.isEmpty(this.getMail());
    }
}
