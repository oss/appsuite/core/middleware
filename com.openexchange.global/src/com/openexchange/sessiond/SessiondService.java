/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.sessiond;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.osgi.annotation.SingletonService;
import com.openexchange.session.RemovalReason;
import com.openexchange.session.Session;
import com.openexchange.session.SessionAttributes;

/**
 * {@link SessiondService} - The SessionD service.
 *
 * @author <a href="mailto:sebastian.kauss@open-xchange.com">Sebastian Kauss</a>
 * @author <a href="mailto:marcus.klein@open-xchange.com">Marcus Klein</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@SingletonService
public interface SessiondService {

    /**
     * The reference to {@link SessiondService} instance.
     */
    public static final AtomicReference<SessiondService> SERVICE_REFERENCE = new AtomicReference<>();

    /**
     * (Safely) stores specified session.
     *
     * @param session The session
     * @param addIfAbsent Adds the session to storage only if absent
     * @param logger The optional logger to use; may be <code>null</code>
     * @see #storeSessionSafe(String, boolean, org.slf4j.Logger)
     * @throw {@link IllegalArgumentException} If given session is <code>null</code>
     */
    public static void storeSessionSafe(Session session, boolean addIfAbsent, org.slf4j.Logger logger) {
        if (session == null) {
            throw new IllegalArgumentException("Session must not be null");
        }

        SessiondService sessiondService = SERVICE_REFERENCE.get();
        if (sessiondService == null) {
            // No such service
            return;
        }

        try {
            sessiondService.storeSession(session, addIfAbsent);
        } catch (Exception e) {
            if (logger != null) {
                logger.error("Failed to store session {}", session.getSessionID(), e);
            }
        }
    }

    /**
     * (Safely) stores specified session.
     *
     * @param sessionId The session identifier
     * @param addIfAbsent Adds the session to storage only if absent
     * @param logger The optional logger to use; may be <code>null</code>
     */
    public static void storeSessionSafe(String sessionId, boolean addIfAbsent, org.slf4j.Logger logger) {
        SessiondService sessiondService = SERVICE_REFERENCE.get();
        if (sessiondService == null) {
            // No such service
            return;
        }

        try {
            sessiondService.storeSession(sessionId, addIfAbsent);
        } catch (Exception e) {
            if (logger != null) {
                logger.error("Failed to store session {}", sessionId, e);
            }
        }
    }

    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Checks if this SessionD service is of central or distributed (node-local) nature.
     *
     * @return <code>true</code> if central; otherwise <code>false</code>
     */
    default boolean isCentral() {
        return false;
    }

    /**
     * Creates a new session object in the SessionD storage with the given session parameters.
     * <p>
     *
     * @param parameterObject The parameter object describing the session to create
     * @return The session object interface of the newly created session.
     * @throws OXException If creating the session fails
     */
    Session addSession(AddSessionParameter parameterObject) throws OXException;

    /**
     * Stores the session associated with given identifier into session storage (if such a session exists).
     *
     * @param sessionId The session identifier
     * @return <code>true</code> if stored; otherwise <code>false</code>
     * @throws OXException If storing the session fails
     */
    default boolean storeSession(String sessionId) throws OXException {
        return storeSession(sessionId, false);
    }

    /**
     * Stores the session associated with given identifier into session storage (if such a session exists).
     *
     * @param sessionId The session identifier
     * @param addIfAbsent Adds the session to storage only if absent
     * @return <code>true</code> if stored; otherwise <code>false</code>
     * @throws OXException If storing the session fails
     */
    boolean storeSession(String sessionId, boolean addIfAbsent) throws OXException;

    /**
     * Stores the specified session into session storage (if such a session exists).
     *
     * @param session The session
     * @param addIfAbsent Adds the session to storage only if absent
     * @return <code>true</code> if stored; otherwise <code>false</code>
     * @throws OXException If storing the session fails
     */
    default boolean storeSession(Session session, boolean addIfAbsent) throws OXException {
        return session != null && storeSession(session.getSessionID(), addIfAbsent);
    }

    /**
     * Replaces the currently stored password in session identified through given session identifier with specified <code>newPassword</code>.
     * <p>
     * Invalidates all other <b>local-only</b> user sessions known by local session containers.
     *
     * @param sessionId The session identifier
     * @param newPassword The new password to apply
     * @throws OXException If new password cannot be applied or corresponding session does not exist or is expired
     */
    void changeSessionPassword(String sessionId, String newPassword) throws OXException;

    /**
     * Removes the session with the given session identifier.
     *
     * @param sessionId The Session identifier
     * @return <code>true</code> if the session was removed or <code>false</code> if the session identifier doesn't exist
     */
    default boolean removeSession(String sessionId) {
        return removeSession(sessionId, RemovalReason.UNKNOWN);
    }

    /**
     * Removes the session with the given session identifier.
     *
     * @param sessionId The Session identifier
     * @param removalReason The reason why the sessions are removed
     * @return <code>true</code> if the session was removed or <code>false</code> if the session identifier doesn't exist
     */
    boolean removeSession(String sessionId, RemovalReason removalReason);

    /**
     * Removes all sessions belonging to given user in specified context.
     *
     * @param userId The user identifier
     * @param ctx The context
     * @return The number of removed session or zero if no session was removed
     */
    default int removeUserSessions(int userId, Context ctx) {
        if (ctx == null) {
            throw new IllegalArgumentException("Context must not be null");
        }
        return removeUserSessions(userId, ctx.getContextId());
    }

    /**
     * Removes all sessions belonging to given user in specified context.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The number of removed session or zero if no session was removed
     */
    default int removeUserSessions(int userId, int contextId) {
        return removeUserSessions(userId, contextId, RemovalReason.UNKNOWN);
    }

    /**
     * Removes all sessions belonging to given user in specified context.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param removalReason The reason why the sessions are removed
     * @return The number of removed session or zero if no session was removed
     */
    int removeUserSessions(int userId, int contextId, RemovalReason removalReason);

    /**
     * Removes all sessions belonging to given context.
     *
     * @param contextId The context identifier
     */
    default void removeContextSessions(int contextId) {
        removeContextSessions(contextId, RemovalReason.UNKNOWN);
    }

    /**
     * Removes all sessions belonging to given context.
     *
     * @param contextId The context identifier
     * @param removalReason The reason why the sessions are removed
     */
    void removeContextSessions(int contextId, RemovalReason removalReason);

    /**
     * Removes all sessions belonging to given contexts from this and all other cluster nodes.
     *
     * @param contextIds - Set with the context identifiers to remove sessions for
     * @throws OXException - if removing session fails on one of the remote nodes
     */
    default void removeContextSessions(Set<Integer> contextIds) throws OXException {
        removeContextSessions(contextIds, RemovalReason.UNKNOWN);
    }

    /**
     * Removes all sessions belonging to given contexts from this and all other cluster nodes.
     *
     * @param contextIds - Set with the context identifiers to remove sessions for
     * @param removalReason The reason why the sessions are removed
     * @throws OXException - if removing session fails on one of the remote nodes
     */
    void removeContextSessions(Set<Integer> contextIds, RemovalReason removalReason) throws OXException;

    /**
     * Removes all sessions which match the given {@link SessionFilter}
     *
     * @param filter The filter
     * @return The IDs of the removed sessions, possibly empty but never <code>null</code>
     * @throws OXException If an error occurs while removing
     */
    default Collection<String> removeSessions(SessionFilter filter) throws OXException {
        return removeSessions(filter, RemovalReason.UNKNOWN);
    }

    /**
     * Removes all sessions which match the given {@link SessionFilter}
     *
     * @param filter The filter
     * @param removalReason The reason why the sessions are removed
     * @return The IDs of the removed sessions, possibly empty but never <code>null</code>
     * @throws OXException If an error occurs while removing
     */
    Collection<String> removeSessions(SessionFilter filter, RemovalReason removalReason) throws OXException;

    /**
     * Removes <b>all</b> sessions.
     *
     * @param removalReason The reason why the sessions are removed
     */
    default void removeAllSessions(RemovalReason removalReason) throws OXException {
        removeSessions(SessionFilter.ALL, removalReason);
    }

    /**
     * Gets the number of active sessions belonging to given user in specified context.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The number of active sessions belonging to given user in specified context
     */
    int getUserSessions(int userId, int contextId);

    /**
     * Checks for any active session for specified context.
     *
     * @param contextId The context identifier
     * @return <code>true</code> if at least one active session is found; otherwise <code>false</code>
     */
    boolean hasForContext(int contextId);

    /**
     * Gets the <b>local-only</b> sessions associated with specified user in given context.
     * <p>
     * <b>local-only</b> is considered as those sessions that are currently "cached" on this node.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The <b>local-only</b> sessions associated with specified user in given context
     * @see #getSessions(int, int, boolean)
     */
    default Collection<Session> getLocalSessions(int userId, int contextId) {
        return getSessions(userId, contextId, false);
    }

    /**
     * Gets the sessions associated with specified user in given context.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param considerSessionStorage <code>true</code> to also consider session storage; otherwise <code>false</code> for <b>local-only</b> sessions
     * @return The sessions associated with specified user in given context
     */
    Collection<Session> getSessions(int userId, int contextId, boolean considerSessionStorage);

    /**
     * Gets a list of <i>active</i> sessions since last 24 hours.
     *
     * @return The identifiers of all active sessions in a list
     */
    List<String> getActiveSessionIDs();

    /**
     * Finds the first session of the specified user that matches the give criterion.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param matcher The session matcher
     * @return The first matching session or <code>null</code> if none matches
     */
    Session findFirstMatchingSessionForUser(int userId, int contextId, SessionMatcher matcher);

    /**
     * Checks if denoted session is existent.
     *
     * @param sessionId The session identifier
     * @return <code>true</code> if <code>locally</code> active; otherwise <code>false</code>
     */
    boolean isActive(String sessionId);

    /**
     * Get the session object related to the given session identifier.
     *
     * @param sessionId The Session identifier
     * @return The session or <code>null</code> if no session exists for the given identifier or if the session is expired
     */
    default Session getSession(String sessionId) {
        return getSession(sessionId, true);
    }

    /**
     * Get the session object related to the given session identifier.
     *
     * @param sessionId The session identifier
     * @param considerLocalStorage <code>true</code> to consider locally held sessions; otherwise <code>false</code> to only consider storage
     * @return The session or <code>null</code> if no session exists for the given identifier or if the session is expired
     */
    Session getSession(String sessionId, boolean considerLocalStorage);

    /**
     * Get the session object related to the given session identifier.
     * <p>
     * If session has been fetched from session storage, that session will not be added to local SessionD. Moreover, session will not be
     * moved to first session container.
     *
     * @param sessionId The session identifier
     * @return Returns the session or <code>null</code> if no session exists for the given identifier or if the session is expired
     */
    Session peekSession(String sessionId);

    /**
     * Get the session object related to the given alternative identifier.
     *
     * @param altId The alternative identifier
     * @return Return the session object or <code>null</code> if no session exists for the given alternative identifier or if the session is expired
     */
    Session getSessionByAlternativeId(String altId);

    /**
     * Get the session object related to the given alternative identifier.
     * <p>
     * If session has been fetched from session storage, that session will not be added to local SessionD. Moreover, session will not be
     * moved to first session container.
     *
     * @param altId The alternative identifier
     * @return Return the session object or <code>null</code> if no session exists for the given alternative identifier or if the session is expired
     */
    Session peekSessionByAlternativeId(String altId);

    /**
     * Get the session object related to the given random token.
     *
     * @param randomToken The random token of the session
     * @param localIp The new local IP to apply to session; pass <code>null</code> to not replace existing IP in session
     * @return The session object or <code>null</code> if no session exists for the given random token or if the random token is already expired
     * @deprecated Will be removed
     */
    @Deprecated
    Session getSessionByRandomToken(String randomToken, String localIp);

    /**
     * Get the session object related to the given random token.
     *
     * @param randomToken The random token of the session
     * @return The session object or <code>null</code> if no session exists for the given random token or if the random token is already expired
     * @deprecated Will be removed
     */
    @Deprecated
    default Session getSessionByRandomToken(String randomToken) {
        return getSessionByRandomToken(randomToken, null);
    }

    /**
     * Picks up the session associated with the given client and server token. If a session exists for the given tokens and both tokens
     * match, the session object is put into the normal session container and into the session storage. It is removed from the session
     * container with tokens so a second request with the same tokens will fail.
     *
     * @param clientToken Client side token passed within the {@link #addSession(AddSessionParameter)} call.
     * @param serverToken Server side token returned inside the session from the {@link #addSession(AddSessionParameter)} call.
     * @return the matching session
     * @throws OXException if one of the tokens does not match.
     */
    Session getSessionWithTokens(String clientToken, String serverToken) throws OXException;

    /**
     * Returns all session IDs whose sessions match the given {@link SessionFilter}. The filter is applied to all sessions maintained in
     * <b>local</b> containers.
     *
     * @param filter The filter
     * @return The IDs of the found sessions, possibly empty but never <code>null</code>
     * @throws OXException If an error occurs while filtering
     */
    Collection<String> findSessions(SessionFilter filter) throws OXException;

    /**
     * Gets the number of active sessions.
     *
     * @return The number of active sessions
     */
    int getNumberOfActiveSessions();

    /**
     * Gets the first session that matches the given userId and contextId.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @deprecated Subject for being removed in future versions
     */
    @Deprecated
    Session getAnyActiveSessionForUser(int userId, int contextId);

    /**
     * Applies given attributes to denoted session.
     *
     * @param sessionId The session identifier
     * @param attrs The attributes to set
     * @throws OXException If arguments cannot be set
     */
    void setSessionAttributes(String sessionId, SessionAttributes attrs) throws OXException;

    /**
     * Gets the last-active time stamp for denoted session.
     *
     * @param sessionId The identifier or the session for which to obtain last-active time stamp
     * @return The last-active time stamp or <code>0</code> (zero) if there is none
     * @throws OXException If last-active time stamp cannot be returned
     */
    default long getLastActive(String sessionId) throws OXException {
        Session session = peekSession(sessionId);
        if (session == null) {
            // No such session
            throw SessionExceptionCodes.SESSION_EXPIRED.create(sessionId);
        }

        Object oNumber = session.getParameter(Session.PARAM_LOCAL_LAST_ACTIVE);
        if (null == oNumber) {
            return 0L;
        }

        if (oNumber instanceof Number) {
            return ((Number) oNumber).longValue();
        }

        try {
            return Long.parseLong(oNumber.toString());
        } catch (@SuppressWarnings("unused") NumberFormatException e) {
            // Cannot be parsed to a long value
            return 0L;
        }
    }
}
