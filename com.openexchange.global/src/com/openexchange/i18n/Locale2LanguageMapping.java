/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.i18n;

import java.util.Locale;
import java.util.Map;
import com.google.common.collect.ImmutableMap;

/**
 * {@link Locale2LanguageMapping} - A mapping to retrieve the well-formed IETF BCP 47 language tag of the fall-back locale for a given locale.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v7.10.3
 */
public final class Locale2LanguageMapping {

    /**
     * Initializes a new {@link Locale2LanguageMapping}.
     */
    private Locale2LanguageMapping() {
        super();
    }

    //@formatter:off
    private static final Map<String, Locale> MAPPING = ImmutableMap.<String, Locale> builderWithExpectedSize(150).
            put("sq-AL", Locale.forLanguageTag("sq-AL")).
            put("ar-DZ", Locale.forLanguageTag("en-US")).
            put("es-AR", Locale.forLanguageTag("es-MX")).
            put("en-AU", Locale.forLanguageTag("en-GB")).
            put("de-AT", Locale.forLanguageTag("de-DE")).
            put("az-Latn-AZ", Locale.forLanguageTag("az-Latn-AZ")).
            put("ar-BH", Locale.forLanguageTag("en-US")).
            put("bn-BD", Locale.forLanguageTag("en-GB")).
            put("be-BY", Locale.forLanguageTag("en-US")).
            put("fr-BE", Locale.forLanguageTag("fr-FR")).
            put("nl-BE", Locale.forLanguageTag("nl-BE")).
            put("en-BZ", Locale.forLanguageTag("en-GB")).
            put("es-VE", Locale.forLanguageTag("es-MX")).
            put("es-BO", Locale.forLanguageTag("es-MX")).
            put("hr-BA", Locale.forLanguageTag("hr-HR")).
            put("sr-Latn-BA", Locale.forLanguageTag("sr-Latn-RS")).
            put("pt-BR", Locale.forLanguageTag("pt-BR")).
            put("ms-BN", Locale.forLanguageTag("ms-MY")).
            put("bg-BG", Locale.forLanguageTag("en-US")).
            put("km-KH", Locale.forLanguageTag("en-US")).
            put("fr-CM", Locale.forLanguageTag("fr-FR")).
            put("en-CA", Locale.forLanguageTag("en-US")).
            put("fr-CA", Locale.forLanguageTag("fr-CA")).
            put("es-CL", Locale.forLanguageTag("es-MX")).
            put("zh-CN", Locale.forLanguageTag("zh-CN")).
            put("es-CO", Locale.forLanguageTag("es-MX")).
            put("es-CR", Locale.forLanguageTag("es-MX")).
            put("fr-CI", Locale.forLanguageTag("fr-FR")).
            put("hr-HR", Locale.forLanguageTag("hr-HR")).
            put("cs-CZ", Locale.forLanguageTag("cs-CZ")).
            put("da-DK", Locale.forLanguageTag("da-DK")).
            put("es-DO", Locale.forLanguageTag("es-MX")).
            put("es-EC", Locale.forLanguageTag("es-MX")).
            put("ar-EG", Locale.forLanguageTag("en-US")).
            put("es-SV", Locale.forLanguageTag("es-MX")).
            put("et-EE", Locale.forLanguageTag("et-EE")).
            put("am-ET", Locale.forLanguageTag("am-ET")).
            put("fi-FI", Locale.forLanguageTag("fi-FI")).
            put("sv-FI", Locale.forLanguageTag("sv-SE")).
            put("fr-FR", Locale.forLanguageTag("fr-FR")).
            put("de-DE", Locale.forLanguageTag("de-DE")).
            put("el-GR", Locale.forLanguageTag("en-US")).
            put("es-GT", Locale.forLanguageTag("es-MX")).
            put("fr-HT", Locale.forLanguageTag("fr-FR")).
            put("es-HN", Locale.forLanguageTag("es-MX")).
            put("zh-HK", Locale.forLanguageTag("en-GB")).
            put("en-HK", Locale.forLanguageTag("en-GB")).
            put("hu-HU", Locale.forLanguageTag("hu-HU")).
            put("is-IS", Locale.forLanguageTag("is-IS")).
            put("en-IN", Locale.forLanguageTag("en-IN")).
            put("hi-IN", Locale.forLanguageTag("en-IN")).
            put("id-ID", Locale.forLanguageTag("id-ID")).
            put("fa-IR", Locale.forLanguageTag("en-US")).
            put("ar-IQ", Locale.forLanguageTag("en-US")).
            put("en-IE", Locale.forLanguageTag("en-GB")).
            put("he-IL", Locale.forLanguageTag("en-US")).
            put("it-IT", Locale.forLanguageTag("it-IT")).
            put("en-JM", Locale.forLanguageTag("en-GB")).
            put("ja-JP", Locale.forLanguageTag("ja-JP")).
            put("ar-JO", Locale.forLanguageTag("en-US")).
            put("kk-KZ", Locale.forLanguageTag("en-US")).
            put("sw-KE", Locale.forLanguageTag("sw-KE")).
            put("ko-KR", Locale.forLanguageTag("en-US")).
            put("ar-KW", Locale.forLanguageTag("en-US")).
            put("lo-LA", Locale.forLanguageTag("en-US")).
            put("lv-LV", Locale.forLanguageTag("lv-LV")).
            put("ar-LB", Locale.forLanguageTag("en-US")).
            put("de-LI", Locale.forLanguageTag("de-DE")).
            put("lt-LT", Locale.forLanguageTag("lt-LT")).
            put("fr-LU", Locale.forLanguageTag("fr-FR")).
            put("de-LU", Locale.forLanguageTag("de-DE")).
            put("zh-MO", Locale.forLanguageTag("en-GB")).
            put("mk-MK", Locale.forLanguageTag("en-US")).
            put("en-MY", Locale.forLanguageTag("en-GB")).
            put("ms-MY", Locale.forLanguageTag("ms-MY")).
            put("es-MX", Locale.forLanguageTag("es-MX")).
            put("ro-MD", Locale.forLanguageTag("ro-RO")).
            put("fr-MC", Locale.forLanguageTag("fr-FR")).
            put("sr-Latn-ME", Locale.forLanguageTag("sr-Latn-CS")).
            put("ar-MA", Locale.forLanguageTag("fr-FR")).
            put("fr-MA", Locale.forLanguageTag("fr-FR")).
            put("nl-NL", Locale.forLanguageTag("nl-NL")).
            put("en-NZ", Locale.forLanguageTag("en-GB")).
            put("es-NI", Locale.forLanguageTag("es-MX")).
            put("ha-Latn-NG", Locale.forLanguageTag("ha-Latn-NG")).
            put("nb-NO", Locale.forLanguageTag("nb-NO")).
            put("ar-OM", Locale.forLanguageTag("en-US")).
            put("es-PA", Locale.forLanguageTag("es-MX")).
            put("es-PY", Locale.forLanguageTag("es-MX")).
            put("es-PE", Locale.forLanguageTag("es-MX")).
            put("fil-PH", Locale.forLanguageTag("en-US")).
            put("en-PH", Locale.forLanguageTag("en-US")).
            put("pl-PL", Locale.forLanguageTag("pl-PL")).
            put("pt-PT", Locale.forLanguageTag("pt-PT")).
            put("es-PR", Locale.forLanguageTag("es-MX")).
            put("ar-QA", Locale.forLanguageTag("en-US")).
            put("fr-RE", Locale.forLanguageTag("fr-FR")).
            put("ro-RO", Locale.forLanguageTag("ro-RO")).
            put("ru-RU", Locale.forLanguageTag("en-US")).
            put("ar-SA", Locale.forLanguageTag("en-US")).
            put("fr-SN", Locale.forLanguageTag("fr-FR")).
            put("sr-Latn-RS", Locale.forLanguageTag("sr-Latn-CS")).
            put("zh-SG", Locale.forLanguageTag("en-GB")).
            put("en-SG", Locale.forLanguageTag("en-GB")).
            put("sk-SK", Locale.forLanguageTag("sk-SK")).
            put("sl-SI", Locale.forLanguageTag("sl-SI")).
            put("af-ZA", Locale.forLanguageTag("af-ZA")).
            put("en-ZA", Locale.forLanguageTag("en-GB")).
            put("eu-ES", Locale.forLanguageTag("eu-ES")).
            put("ca-ES", Locale.forLanguageTag("ca-ES")).
            put("gl-ES", Locale.forLanguageTag("gl-ES")).
            put("es-ES", Locale.forLanguageTag("es-ES")).
            put("sv-SE", Locale.forLanguageTag("sv-SE")).
            put("fr-CH", Locale.forLanguageTag("fr-CH")).
            put("de-CH", Locale.forLanguageTag("de-DE")).
            put("it-CH", Locale.forLanguageTag("it-IT")).
            put("ar-SY", Locale.forLanguageTag("en-US")).
            put("zh-TW", Locale.forLanguageTag("en-US")).
            put("th-TH", Locale.forLanguageTag("en-US")).
            put("en-TT", Locale.forLanguageTag("en-GB")).
            put("ar-TN", Locale.forLanguageTag("en-US")).
            put("tr-TR", Locale.forLanguageTag("tr-TR")).
            put("uk-UA", Locale.forLanguageTag("en-US")).
            put("ar-AE", Locale.forLanguageTag("en-US")).
            put("en-GB", Locale.forLanguageTag("en-GB")).
            put("en-US", Locale.forLanguageTag("en-US")).
            put("es-US", Locale.forLanguageTag("es-MX")).
            put("es-UY", Locale.forLanguageTag("es-MX")).
            put("uz-Latn-UZ", Locale.forLanguageTag("uz-Latn-UZ")).
            put("vi-VN", Locale.forLanguageTag("vi-VN")).
            put("ar-YE", Locale.forLanguageTag("en-US")).
            put("en-ZW", Locale.forLanguageTag("en-GB")).
            build();
    //@formatter:on

    /**
     * Retrieves the {@link Locale#toLanguageTag() well-formed IETF BCP 47 language tag} of the fall-back locale for given locale.
     *
     * @param locale The locale to get the fall-back for
     * @return The locale representing the well-formed IETF BCP 47 language tag for the fall-back locale (e.g. <code>"en-US"</code>)
     *         or <code>null</code> if there is no fall-back one.
     */
    public static Locale getLanguageForLocale(String locale) {
        return MAPPING.get(locale);
    }

}
