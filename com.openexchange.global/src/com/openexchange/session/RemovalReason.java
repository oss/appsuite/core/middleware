/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.session;

/**
 * {@link RemovalReason} - Enumeration of different reasons for session removal
 * 
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public enum RemovalReason {

    /**
     * The generic/undefined placeholder if no other reason has been specified.
     */
    UNKNOWN,
    /**
     * The session is removed after being idle/unused for a certain duration.
     */
    EXPIRED,
    /**
     * Session is removed after being considered invalid.
     */
    INVALIDATED,
    /**
     * Removal through a regular, user-initiated logout operation.
     */
    USER_LOGOUT,
    /**
     * Another session is removed explicitly by the user (usually via session management API).
     */
    USER_CLOSED,
    /**
     * Session is removed explicitly via an administrative interface (e.g. <i>close sessions</i> commandline utility or REST API).
     */
    ADMIN_CLOSED,

}
