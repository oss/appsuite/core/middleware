/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.session;

import com.openexchange.java.Strings;

/**
 * {@link UserAndContext} - An immutable pair of user and context identifier.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.3
 */
public final class UserAndContext implements Comparable<UserAndContext>, IUserAndContext {

    /**
     * Creates a new instance
     *
     * @param session The session providing user data
     * @return The new instance
     */
    public static UserAndContext newInstance(Session session) {
        return newInstance(session.getUserId(), session.getContextId());
    }

    /**
     * Creates a new instance
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The new instance
     */
    public static UserAndContext newInstance(int userId, int contextId) {
        return new UserAndContext(userId, contextId);
    }

    /**
     * Parses the appropriate <code>UserAndContext</code> instance from given identifier, which is expected to be of format:
     * <pre>[user-id] + "@" + [context-id]</pre>
     *
     * @param identifier The user-and-context identifier; e.g. <code>"3@1337"</code>
     * @return The appropriate <code>UserAndContext</code> instance
     * @throws IllegalArgumentException If specified identifier is invalid
     */
    public static UserAndContext parseFrom(String identifier) {
        if (Strings.isEmpty(identifier)) {
            throw new IllegalArgumentException("User-and-context identifier must not be null or empty");
        }
        String s = identifier.trim();
        int pos = s.indexOf('@');
        if (pos <= 0) {
            throw new IllegalArgumentException("Invalid user-and-context identifier: " + identifier);
        }
        return new UserAndContext(Strings.parseUnsignedInt(s.substring(0, pos)), Strings.parseUnsignedInt(s.substring(pos + 1)));
    }

    // ---------------------------------------------------------------

    private final int contextId;
    private final int userId;
    private final int hash;

    /**
     * Initializes a new {@link UserAndContext}.
     */
    private UserAndContext(int userId, int contextId) {
        super();
        this.contextId = contextId;
        this.userId = userId;
        int prime = 31;
        int result = prime * 1 + contextId;
        result = prime * result + userId;
        this.hash = result;
    }

    @Override
    public int getUserId() {
        return userId;
    }

    @Override
    public int getContextId() {
        return contextId;
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof UserAndContext)) {
            return false;
        }
        UserAndContext other = (UserAndContext) obj;
        if (contextId != other.contextId) {
            return false;
        }
        if (userId != other.userId) {
            return false;
        }
        return true;
    }

    /**
     * Gets the string representation for this instance in format:
     * <pre>[user-id] + "@" + [context-id]</pre>
     *
     * @return The formatted string; e.g. <code>"3@1337"</code>
     */
    public String toFormattedString() {
        return new StringBuilder(16).append(userId <= 0 ? 0 : userId).append('@').append(contextId).toString();
    }

    @Override
    public String toString() {
        return new StringBuilder(32).append("[userId=").append(userId).append(", contextId=").append(contextId).append(']').toString();
    }

    @Override
    public int compareTo(UserAndContext o) {
        int c = Integer.compare(contextId, o.contextId);
        return c == 0 ? Integer.compare(userId, o.userId) : c;
    }

}
