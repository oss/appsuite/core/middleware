/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.authentication;

import java.util.Optional;
import com.openexchange.osgi.annotation.SingletonService;

/**
 * {@link AuthenticationServiceRegistry} - The registry for authentication services and special basic authentication service.
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 * @since v8.0.0
 */
@SingletonService
public interface AuthenticationServiceRegistry extends AuthenticationPerformer {

    /**
     * Gets the optional basic authentication service.
     *
     * @return An {@link Optional} object containing the {@link BasicAuthenticationService}, if available; otherwise empty
     */
    Optional<BasicAuthenticationService> optBasicAuthenticationService();

}
