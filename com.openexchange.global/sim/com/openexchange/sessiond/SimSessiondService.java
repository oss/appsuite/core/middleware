/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.sessiond;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import com.openexchange.authentication.SessionEnhancement;
import com.openexchange.exception.OXException;
import com.openexchange.java.util.UUIDs;
import com.openexchange.session.RemovalReason;
import com.openexchange.session.Session;
import com.openexchange.session.SessionAttributes;
import com.openexchange.session.SimSession;

/**
 * {@link SimSessiondService}
 *
 * @author <a href="mailto:steffen.templin@open-xchange.com">Steffen Templin</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.6.1
 */
public class SimSessiondService implements SessiondService {

    private final ConcurrentMap<String, SimSession> sessionsInCache = new ConcurrentHashMap<String, SimSession>();
    private final ConcurrentMap<String, SimSession> sessionsInStorage = new ConcurrentHashMap<String, SimSession>();

    @Override
    public Session addSession(AddSessionParameter param) throws OXException {
        SimSession session = new SimSession(param.getUserId(), param.getContext().getContextId());
        session.setClient(param.getClient());
        session.setHash(param.getHash());
        session.setLoginName(param.getFullLogin());
        session.setPassword(param.getPassword());
        session.setLocalIp(param.getClientIP());
        session.setSecret(UUIDs.getUnformattedString(UUID.randomUUID()));
        Session existing = null;
        do {
            session.setSessionID(UUIDs.getUnformattedString(UUID.randomUUID()));
            existing = sessionsInStorage.putIfAbsent(session.getSessionID(), session);
        } while (existing != null);

        List<SessionEnhancement> enhancements = param.getEnhancements();
        if (null != enhancements) {
            for (SessionEnhancement enhancement: enhancements) {
                enhancement.enhanceSession(session);
            }
        }
        SimSession cachedSession = new SimSession(session);
        sessionsInCache.put(session.getSessionID(), cachedSession);
        return cachedSession;
    }

    @Override
    public void changeSessionPassword(String sessionId, String newPassword) throws OXException {
        SimSession session = sessionsInStorage.get(sessionId);
        if (session == null) {
            throw SessionExceptionCodes.PASSWORD_UPDATE_FAILED.create();
        }

        session.setPassword(newPassword);
        session = sessionsInCache.get(sessionId);
        if (session != null) {
            session.setPassword(newPassword);
        }
    }

    @Override
    public boolean removeSession(String sessionId, RemovalReason removalReason) {
        boolean removed = sessionsInStorage.remove(sessionId) != null;
        sessionsInCache.remove(sessionId);
        return removed;
    }

    @Override
    public void removeContextSessions(int contextId, RemovalReason removalReason) {
        SessionFilter filter = SessionFilter.createContextIdFilter(contextId);
        filterSessionIds(filter, true);
    }

    @Override
    public void removeContextSessions(Set<Integer> contextIds, RemovalReason removalReason) throws OXException {
        for (Integer contextId : contextIds) {
            removeContextSessions(contextId.intValue(), removalReason);
        }
    }

    @Override
    public int removeUserSessions(int userId, int contextId, RemovalReason removalReason) {
        SessionFilter filter = SessionFilter.createUserAndContextIdFilter(userId, contextId);
        List<String> removed = filterSessionIds(filter, true);
        return removed.size();
    }

    @Override
    public Collection<String> removeSessions(SessionFilter filter, RemovalReason removalReason) throws OXException {
        return filterSessionIds(filter, true);
    }

    private List<String> filterSessionIds(SessionFilter filter, boolean remove) {
        List<String> sessions = new LinkedList<String>();
        Iterator<SimSession> iterator = sessionsInStorage.values().iterator();
        while (iterator.hasNext()) {
            SimSession session = iterator.next();
            if (filter.apply(session)) {
                if (remove) {
                    iterator.remove();
                }
                sessions.add(session.getSessionID());
            }
        }

        if (remove) {
            for (String sessionId : sessions) {
                sessionsInCache.remove(sessionId);
            }
        }

        return sessions;
    }

    private List<Session> filterSessions(SessionFilter filter, boolean remove, boolean localOnly) {
        List<Session> sessions = new LinkedList<>();
        if (localOnly) {
            Iterator<SimSession> iterator = sessionsInCache.values().iterator();
            while (iterator.hasNext()) {
                SimSession session = iterator.next();
                if (filter.apply(session)) {
                    if (remove) {
                        iterator.remove();
                    }
                    sessions.add(session);
                }
            }

            if (remove) {
                for (Session session : sessions) {
                    sessionsInStorage.remove(session.getSessionID());
                }
            }

            return sessions;
        }

        Iterator<SimSession> iterator = sessionsInStorage.values().iterator();
        while (iterator.hasNext()) {
            SimSession session = iterator.next();
            if (filter.apply(session)) {
                if (remove) {
                    iterator.remove();
                }
                SimSession fromCache = sessionsInCache.get(session.getSessionID());
                sessions.add(fromCache == null ? session : fromCache);
            }
        }

        if (remove) {
            for (Session session : sessions) {
                sessionsInCache.remove(session.getSessionID());
            }
        }

        return sessions;
    }

    @Override
    public List<String> getActiveSessionIDs() {
        return sessionsInCache.values().stream().map(s -> s.getSessionID()).toList();
    }

    @Override
    public int getUserSessions(int userId, int contextId) {
        SessionFilter filter = SessionFilter.createUserAndContextIdFilter(userId, contextId);
        List<String> sessionIds = filterSessionIds(filter, false);
        return sessionIds.size();
    }

    @Override
    public boolean hasForContext(int contextId) {
        SessionFilter filter = SessionFilter.createContextIdFilter(contextId);
        List<String> sessionIds = filterSessionIds(filter, false);
        return sessionIds.size() > 0;
    }

    @Override
    public Collection<Session> getLocalSessions(int userId, int contextId) {
        SessionFilter filter = SessionFilter.createUserAndContextIdFilter(userId, contextId);
        return filterSessions(filter, false, true);
    }

    @Override
    public Collection<Session> getSessions(int userId, int contextId, boolean considerSessionStorage) {
        SessionFilter filter = SessionFilter.createUserAndContextIdFilter(userId, contextId);
        return filterSessions(filter, false, !considerSessionStorage);
    }

    @Override
    public Session findFirstMatchingSessionForUser(int userId, int contextId, SessionMatcher matcher) {
        SessionFilter filter = SessionFilter.createUserAndContextIdFilter(userId, contextId);
        List<Session> userSessions = filterSessions(filter, false, false);
        for (Session session : userSessions) {
            if (matcher.accepts(session)) {
                sessionsInCache.put(session.getSessionID(), (SimSession) session);
                return session;
            }
        }

        return null;
    }

    @Override
    public Session getSession(String sessionId, boolean considerLocalStorage) {
        if (!considerLocalStorage) {
            return sessionsInStorage.get(sessionId);
        }
        SimSession session = sessionsInCache.get(sessionId);
        if (session != null) {
            return session;
        }

        session = sessionsInStorage.get(sessionId);
        if (session != null) {
            // Put into cache if fetched from storage
            sessionsInCache.put(sessionId, new SimSession(session));
        }
        return session;
    }

    @Override
    public boolean isActive(String sessionId) {
        return sessionsInStorage.get(sessionId) != null;
    }

    @Override
    public Session peekSession(String sessionId) {
        SimSession session = sessionsInCache.get(sessionId);
        return session != null ? session : sessionsInStorage.get(sessionId);
    }

    @Override
    public Session getSessionByAlternativeId(String altId) {
        SessionFilter filter = SessionFilter.create("(" + Session.PARAM_ALTERNATIVE_ID + "=" + altId + ")");
        List<Session> sessions = filterSessions(filter, false, false);
        if (sessions.isEmpty()) {
            return null;
        }

        Session session = sessions.get(0);
        sessionsInCache.put(session.getSessionID(), (SimSession) session);
        return session;
    }

    @Override
    public Session peekSessionByAlternativeId(String altId) {
        SessionFilter filter = SessionFilter.create("(" + Session.PARAM_ALTERNATIVE_ID + "=" + altId + ")");
        List<Session> sessions = filterSessions(filter, false, false);
        if (sessions.isEmpty()) {
            return null;
        }

        return sessions.get(0);
    }

    @Override
    public Session getSessionByRandomToken(String randomToken, String localIp) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Session getSessionByRandomToken(String randomToken) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Session getSessionWithTokens(String clientToken, String serverToken) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getNumberOfActiveSessions() {
        return sessionsInStorage.size();
    }

    @Override
    public Session getAnyActiveSessionForUser(int userId, int contextId) {
        SessionFilter filter = SessionFilter.createUserAndContextIdFilter(userId, contextId);
        List<Session> sessions = filterSessions(filter, false, false);
        if (sessions.isEmpty()) {
            return null;
        }

        Session session = sessions.get(0);
        sessionsInCache.put(session.getSessionID(), (SimSession) session);
        return session;
    }

    @Override
    public boolean storeSession(String sessionId) throws OXException {
        return storeSession(sessionId, false);
    }

    @Override
    public boolean storeSession(String sessionId, boolean addIfAbsent) throws OXException {
        SimSession session = sessionsInCache.get(sessionId);
        if (session == null) {
            return false;
        }

        if (addIfAbsent) {
            sessionsInStorage.putIfAbsent(sessionId, new SimSession(session));
        } else {
            sessionsInStorage.put(sessionId, new SimSession(session));
        }
        return true;
    }

    @Override
    public Collection<String> findSessions(SessionFilter filter) {
        return filterSessionIds(filter, false);
    }

    @Override
    public void setSessionAttributes(String sessionId, SessionAttributes attrs) throws OXException {
        SimSession session = sessionsInStorage.get(sessionId);
        if (session != null) {
            if (attrs.getLocalIp().isSet()) {
                session.setLocalIp(attrs.getLocalIp().get());
            }
            if (attrs.getClient().isSet()) {
                session.setClient(attrs.getClient().get());
            }
            if (attrs.getHash().isSet()) {
                session.setHash(attrs.getHash().get());
            }
            if (attrs.getUserAgent().isSet()) {
                session.setParameter(Session.PARAM_USER_AGENT, attrs.getUserAgent().get());
            }
        }
        session = sessionsInCache.get(sessionId);
        if (session != null) {
            if (attrs.getLocalIp().isSet()) {
                session.setLocalIp(attrs.getLocalIp().get());
            }
            if (attrs.getClient().isSet()) {
                session.setClient(attrs.getClient().get());
            }
            if (attrs.getHash().isSet()) {
                session.setHash(attrs.getHash().get());
            }
            if (attrs.getUserAgent().isSet()) {
                session.setParameter(Session.PARAM_USER_AGENT, attrs.getUserAgent().get());
            }
        }
    }

}
