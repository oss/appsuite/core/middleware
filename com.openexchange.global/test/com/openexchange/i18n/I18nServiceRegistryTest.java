/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.i18n;

import java.util.Locale;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;
import com.openexchange.i18n.internal.I18nServiceRegistryImpl;

/**
 * {@link I18nServiceRegistryTest}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v7.10.3
 */
public class I18nServiceRegistryTest {

    private I18nService en_US;
    private I18nService es_ES;
    private I18nService es_MX;
    private I18nServiceRegistryImpl registry;

    @BeforeEach
    public void setUp() {
        // Reset or create a new instance of the singleton to avoid side effects across tests
        registry = new I18nServiceRegistryImpl();

        en_US = Mockito.mock(I18nService.class);
        Mockito.when(en_US.getLocale()).thenReturn(new Locale("en", "US"));

        es_ES = Mockito.mock(I18nService.class);
        Mockito.when(es_ES.getLocale()).thenReturn(new Locale("es", "ES"));

        es_MX = Mockito.mock(I18nService.class);
        Mockito.when(es_MX.getLocale()).thenReturn(new Locale("es", "MX"));

        registry.addI18nService(en_US);
        registry.addI18nService(es_ES);
        registry.addI18nService(es_MX);
    }

    @AfterEach
    public void tearDown() {
        // Clean up or reset the singleton after each test
        registry.clear(); // Assuming there's a method to clear the services in the registry
    }

    @Test
    public void testMapping() {
        I18nService bestFittingI18nService = registry.getBestFittingI18nService(new Locale("es", "US"));
        Assertions.assertEquals(es_MX, bestFittingI18nService);
    }

}
