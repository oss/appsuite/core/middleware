/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tokenlogin.json;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link TokenLoginRequest}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public class TokenLoginRequest {

    private final AJAXRequestData request;
    private final ServerSession session;

    /**
     * Initializes a new {@link TokenLoginRequest}.
     * 
     * @param request The {@link AJAXRequestData}
     * @param session The user {@link Session}
     */
    public TokenLoginRequest(final AJAXRequestData request, final ServerSession session) {
        super();
        this.request = request;
        this.session = session;
    }

    /**
     * Get the users session
     *
     * @return The session
     */
    public ServerSession getSession() {
        return session;
    }

    /**
     * Get the request
     *
     * @return The {@link AJAXRequestData}
     */
    public AJAXRequestData getRequest() {
        return request;
    }

}
