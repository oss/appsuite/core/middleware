package com.openexchange.advertisement.json;

import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.createAnalyzeResultFromCid;
import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.isQualifiedPath;
import java.util.Optional;
import java.util.Set;
import com.openexchange.database.ConfigDatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.request.analyzer.RequestURL;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * {@link AdvertisementRequestAnalyzer} is a {@link RequestAnalyzer} which uses the "contextId" parameter of the analyzed call to the
 * advertisement servlet to determine the marker for the request.
 *
 * @author <a href="mailto:marcel.broecher@open-xchange.com">Marcel Broecher</a>
 */
public class AdvertisementRequestAnalyzer implements RequestAnalyzer {

    /** The path prefixes this analyzer is responsible for */
    private static final Set<String> PATH_PREFIXES = com.openexchange.tools.arrays.Collections.unmodifiableSet(
        "/advertisement/v1/config/user", "/advertisement/v1/config/name");

    private final ErrorAwareSupplier<? extends ConfigDatabaseService> dbServiceSupplier;

    /**
     * Initializes a new {@link AdvertisementRequestAnalyzer}.
     *
     * @param dbServiceSupplier A supplier for the database service for resolving the database schema name during analysis
     */
    public AdvertisementRequestAnalyzer(ErrorAwareSupplier<? extends ConfigDatabaseService> dbServiceSupplier) {
        super();
        this.dbServiceSupplier = dbServiceSupplier;
    }

    @Override
    public Optional<AnalyzeResult> analyze(RequestData data) throws OXException {
        if (false == "PUT".equals(data.getMethod()) && false == "DELETE".equals(data.getMethod())) {
            // not a request for the advertisement REST service
            return Optional.empty();
        }

        RequestURL url = data.getParsedURL();
        if (url.getPath().isEmpty() || !isQualifiedPath(url.getPath().get(), PATH_PREFIXES)) {
            // not a request for the advertisement REST service
            return Optional.empty();
        }

        Optional<String> cid = url.optParameter("contextId");
        if (cid.isEmpty()) {
            // no context id, yield "unknown" result
            return Optional.of(AnalyzeResult.UNKNOWN);
        }

        return Optional.of(createAnalyzeResultFromCid(cid.get(), dbServiceSupplier));
    }
}
