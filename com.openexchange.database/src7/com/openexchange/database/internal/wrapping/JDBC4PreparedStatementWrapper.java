/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.database.internal.wrapping;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.DefaultInterests;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadable;
import com.openexchange.database.Databases;
import com.openexchange.database.SpecificSQLException;
import com.openexchange.database.internal.Initialization;
import com.openexchange.database.internal.reloadable.GenericReloadable;

/**
 * {@link JDBC4PreparedStatementWrapper}
 *
 * @author <a href="mailto:marcus.klein@open-xchange.com">Marcus Klein</a>
 */
public abstract class JDBC4PreparedStatementWrapper extends JDBC4StatementWrapper implements PreparedStatement {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(JDBC4PreparedStatementWrapper.class);

    private static final AtomicReference<Integer> statementDurationThreshold = new AtomicReference<>(null);
    private static int statementDurationThreshold() {
        Integer tmp = statementDurationThreshold.get();
        if (null == tmp) {
            int defaultValue = 0;
            ConfigurationService confService = Initialization.getConfigurationService();
            if (null == confService) {
                return defaultValue;
            }

            Integer i = Integer.valueOf(confService.getIntProperty("com.openexchange.database.statementDurationThreshold", defaultValue));
            tmp = statementDurationThreshold.compareAndExchange(null, i);
            if (tmp == null) {
                tmp = i;
            }
        }
        return tmp.intValue();
    }

    static {
        GenericReloadable.getInstance().addReloadable(new Reloadable() {

            @Override
            public void reloadConfiguration(ConfigurationService configService) {
                statementDurationThreshold.set(null);
            }

            @Override
            public Interests getInterests() {
                return DefaultInterests.builder().propertiesOfInterest("com.openexchange.database.statementDurationThreshold").build();
            }
        });
    }

    /** Simple interface wrapping the execution of an SQL statement */
    private static interface StatementExecution<V> {

        /**
         * Executes the statement.
         *
         * @return The result
         * @throws SQLException If an SQL error occurs
         */
        V execute() throws SQLException;
    }

    /** Exception path handler in case execution of an SQL statement failed */
    private static interface ExceptionHandler<V> {

        /**
         * Handles given result on exception path.
         *
         * @return The result
         */
        void onException(V result);
    }

    /**
     * Executes specified statement execution with respect to possible DEBUG logging and/or statement duration tracing.
     *
     * @param <V> The type of the result
     * @param execution The statement execution to execute
     * @param debugEnabled <code>true</code> if DEBUG logging is enables; otherwise <code>false</code>
     * @param executionTimeThreshold The execution time threshold; if exceeded the statement is logged as long-running
     * @return The result
     * @throws SQLException If a database access error occurs
     */
    private <V> V executeStatement(StatementExecution<V> execution,  boolean debugEnabled, int executionTimeThreshold) throws SQLException {
        return executeStatement(execution, null, debugEnabled, executionTimeThreshold);
    }

    /**
     * Executes specified statement execution with respect to possible DEBUG logging and/or statement duration tracing.
     *
     * @param <V> The type of the result
     * @param execution The statement execution to execute
     * @param exceptionHandler The exception handler or <code>null</code>
     * @param debugEnabled <code>true</code> if DEBUG logging is enables; otherwise <code>false</code>
     * @param executionTimeThreshold The execution time threshold; if exceeded the statement is logged as long-running
     * @return The result
     * @throws SQLException If a database access error occurs
     */
    private <V> V executeStatement(StatementExecution<V> execution, ExceptionHandler<V> optExceptionHandler, boolean debugEnabled, int executionTimeThreshold) throws SQLException {
        // Log statement if DEBUG enabled
        String stmtStr = null;
        if (debugEnabled) {
            stmtStr = Databases.getSqlStatement(preparedStatementDelegate, "<unknown>");
            LOG.debug("{} executes: {}", Thread.currentThread(), stmtStr);
        }

        // Check if duration shall be tracked
        if (executionTimeThreshold <= 0) {
            return execution.execute();
        }

        // Track statement execution duration w/ or w/o exception handler invocation
        if (optExceptionHandler == null) {
            // Without exception handler invocation
            long start = System.nanoTime();
            V retval = execution.execute();
            logIfLongRunning(executionTimeThreshold, start, stmtStr);
            return retval;
        }

        // With exception handler invocation
        V tmp = null;
        try {
            long start = System.nanoTime();
            tmp = execution.execute();
            logIfLongRunning(executionTimeThreshold, start, stmtStr);
            V retval = tmp;
            tmp = null; // Avoid premature closing
            return retval;
        } finally {
            if (tmp != null) {
                optExceptionHandler.onException(tmp);
            }
        }
    }

    /**
     * Logs executed statement if its execution time exceeds specified threshold.
     *
     * @param executionTimeThreshold The execution time threshold that might be exceeded
     * @param start The start nanoseconds captured via {@link System#nanoTime()}
     * @param optStmtStr The statement's string representation or <code>null</code>
     */
    private void logIfLongRunning(int executionTimeThreshold, long start, String optStmtStr) {
        long durMillis = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start);
        if (durMillis > executionTimeThreshold) {
            LOG.warn("The following query took {} milliseconds: {}", Long.valueOf(durMillis), optStmtStr == null ? Databases.getSqlStatement(preparedStatementDelegate, "<unknown>") : optStmtStr);
        }
    }

    /**
     * Closes given result set safely.
     *
     * @param resultSet The result set to close
     */
    private static void closeSafely(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (Exception e) {
                LOG.error("Failed to close result set", e);
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    protected final PreparedStatement preparedStatementDelegate;

    /**
     * Initializes a new {@link JDBC4PreparedStatementWrapper}.
     *
     * @param delegate The delegate statement
     * @param con The connection returner instance
     */
    protected JDBC4PreparedStatementWrapper(final PreparedStatement delegate, final JDBC4ConnectionReturner con) {
        super(delegate, con);
        this.preparedStatementDelegate = delegate;
    }

    @Override
    public void addBatch() throws SQLException {
        preparedStatementDelegate.addBatch();
    }

    @Override
    public void clearParameters() throws SQLException {
        preparedStatementDelegate.clearParameters();
    }

    @Override
    public boolean execute() throws SQLException {
        try {
            boolean retval;
            boolean debugEnabled = LOG.isDebugEnabled();
            int durationThreshold = statementDurationThreshold();
            if (debugEnabled || durationThreshold > 0) {
                retval = executeStatement(preparedStatementDelegate::execute, debugEnabled, durationThreshold).booleanValue();
            } else {
                retval = preparedStatementDelegate.execute();
            }
            con.updatePerformed();
            return retval;
        } catch (java.sql.SQLSyntaxErrorException syntaxError) {
            logSyntaxError(syntaxError, preparedStatementDelegate, con);
            throw syntaxError;
        } catch (SQLException sqlException) {
            SQLException specific = SpecificSQLException.getSpecificSQLExceptionFor(sqlException);
            if (null != specific) {
                throw specific;
            }
            logReadTimeoutError(sqlException, preparedStatementDelegate, con);
            throw sqlException;
        }
    }

    /** The exception handler for a result set */
    private static final ExceptionHandler<JDBC41ResultSetWrapper> RESULT_SET_EXC_HANDLER = rs -> closeSafely(rs);

    @Override
    public ResultSet executeQuery() throws SQLException {
        JDBC41ResultSetWrapper tmp = null;
        try {
            boolean debugEnabled = LOG.isDebugEnabled();
            int durationThreshold = statementDurationThreshold();
            if (debugEnabled || durationThreshold > 0) {
                tmp = executeStatement(this::doExecuteQuery, RESULT_SET_EXC_HANDLER, debugEnabled, durationThreshold);
            } else {
                tmp = doExecuteQuery();
            }
            con.touch();
            JDBC41ResultSetWrapper retval = tmp;
            tmp = null; // Avoid premature closing
            return retval;
        } catch (java.sql.SQLSyntaxErrorException syntaxError) {
            logSyntaxError(syntaxError, preparedStatementDelegate, con);
            throw syntaxError;
        } catch (SQLException sqlException) {
            SQLException specific = SpecificSQLException.getSpecificSQLExceptionFor(sqlException);
            if (null != specific) {
                throw specific;
            }
            logReadTimeoutError(sqlException, preparedStatementDelegate, con);
            throw sqlException;
        } finally {
            closeSafely(tmp);
        }
    }

    /**
     * Executes the SQL query in this statement and returns the {@code ResultSet} object generated by the query.
     * <p>
     * Ensures that originally generated result set is closed on exception path.
     *
     * @return The result set
     * @throws SQLException If a database access error occurs
     */
    private JDBC41ResultSetWrapper doExecuteQuery() throws SQLException {
        ResultSet rs = null;
        try {
            rs = preparedStatementDelegate.executeQuery();
            JDBC41ResultSetWrapper wrapper = new JDBC41ResultSetWrapper(rs, this);
            rs = null; // Avoid premature closing
            return wrapper;
        } finally {
            closeSafely(rs);
        }
    }

    @Override
    public int executeUpdate() throws SQLException {
        try {
            int retval;
            boolean debugEnabled = LOG.isDebugEnabled();
            int durationThreshold = statementDurationThreshold();
            if (debugEnabled || durationThreshold > 0) {
                retval = executeStatement(preparedStatementDelegate::executeUpdate, debugEnabled, durationThreshold).intValue();
            } else {
                retval = preparedStatementDelegate.executeUpdate();
            }
            con.updatePerformed();
            return retval;
        } catch (java.sql.SQLSyntaxErrorException syntaxError) {
            logSyntaxError(syntaxError, preparedStatementDelegate, con);
            throw syntaxError;
        } catch (SQLException sqlException) {
            SQLException specific = SpecificSQLException.getSpecificSQLExceptionFor(sqlException);
            if (null != specific) {
                throw specific;
            }
            logReadTimeoutError(sqlException, preparedStatementDelegate, con);
            throw sqlException;
        }
    }

    @Override
    public ResultSetMetaData getMetaData() throws SQLException {
        return preparedStatementDelegate.getMetaData();
    }

    @Override
    public ParameterMetaData getParameterMetaData() throws SQLException {
        return preparedStatementDelegate.getParameterMetaData();
    }

    @Override
    public void setArray(final int i, final Array x) throws SQLException {
        preparedStatementDelegate.setArray(i, x);
    }

    @Override
    public void setAsciiStream(final int parameterIndex, final InputStream x, final int length) throws SQLException {
        preparedStatementDelegate.setAsciiStream(parameterIndex, x, length);
    }

    @Override
    public void setBigDecimal(final int parameterIndex, final BigDecimal x) throws SQLException {
        preparedStatementDelegate.setBigDecimal(parameterIndex, x);
    }

    @Override
    public void setBinaryStream(final int parameterIndex, final InputStream x, final int length) throws SQLException {
        preparedStatementDelegate.setBinaryStream(parameterIndex, x, length);
    }

    @Override
    public void setBlob(final int i, final Blob x) throws SQLException {
        preparedStatementDelegate.setBlob(i, x);
    }

    @Override
    public void setBoolean(final int parameterIndex, final boolean x) throws SQLException {
        preparedStatementDelegate.setBoolean(parameterIndex, x);
    }

    @Override
    public void setByte(final int parameterIndex, final byte x) throws SQLException {
        preparedStatementDelegate.setByte(parameterIndex, x);
    }

    @Override
    public void setBytes(final int parameterIndex, final byte[] x) throws SQLException {
        preparedStatementDelegate.setBytes(parameterIndex, x);
    }

    @Override
    public void setCharacterStream(final int parameterIndex, final Reader reader, final int length) throws SQLException {
        preparedStatementDelegate.setCharacterStream(parameterIndex, reader, length);
    }

    @Override
    public void setClob(final int i, final Clob x) throws SQLException {
        preparedStatementDelegate.setClob(i, x);
    }

    @Override
    public void setDate(final int parameterIndex, final Date x) throws SQLException {
        preparedStatementDelegate.setDate(parameterIndex, x);
    }

    @Override
    public void setDate(final int parameterIndex, final Date x, final Calendar cal) throws SQLException {
        preparedStatementDelegate.setDate(parameterIndex, x, cal);
    }

    @Override
    public void setDouble(final int parameterIndex, final double x) throws SQLException {
        preparedStatementDelegate.setDouble(parameterIndex, x);
    }

    @Override
    public void setFloat(final int parameterIndex, final float x) throws SQLException {
        preparedStatementDelegate.setFloat(parameterIndex, x);
    }

    @Override
    public void setInt(final int parameterIndex, final int x) throws SQLException {
        preparedStatementDelegate.setInt(parameterIndex, x);
    }

    @Override
    public void setLong(final int parameterIndex, final long x) throws SQLException {
        preparedStatementDelegate.setLong(parameterIndex, x);
    }

    @Override
    public void setNull(final int parameterIndex, final int sqlType) throws SQLException {
        preparedStatementDelegate.setNull(parameterIndex, sqlType);
    }

    @Override
    public void setNull(final int paramIndex, final int sqlType, final String typeName) throws SQLException {
        preparedStatementDelegate.setNull(paramIndex, sqlType, typeName);
    }

    @Override
    public void setObject(final int parameterIndex, final Object x) throws SQLException {
        preparedStatementDelegate.setObject(parameterIndex, x);
    }

    @Override
    public void setObject(final int parameterIndex, final Object x, final int targetSqlType) throws SQLException {
        preparedStatementDelegate.setObject(parameterIndex, x, targetSqlType);
    }

    @Override
    public void setObject(final int parameterIndex, final Object x, final int targetSqlType, final int scale) throws SQLException {
        preparedStatementDelegate.setObject(parameterIndex, x, targetSqlType, scale);
    }

    @Override
    public void setRef(final int i, final Ref x) throws SQLException {
        preparedStatementDelegate.setRef(i, x);
    }

    @Override
    public void setShort(final int parameterIndex, final short x) throws SQLException {
        preparedStatementDelegate.setShort(parameterIndex, x);
    }

    @Override
    public void setString(final int parameterIndex, final String x) throws SQLException {
        preparedStatementDelegate.setString(parameterIndex, x);
    }

    @Override
    public void setTime(final int parameterIndex, final Time x) throws SQLException {
        preparedStatementDelegate.setTime(parameterIndex, x);
    }

    @Override
    public void setTime(final int parameterIndex, final Time x, final Calendar cal) throws SQLException {
        preparedStatementDelegate.setTime(parameterIndex, x, cal);
    }

    @Override
    public void setTimestamp(final int parameterIndex, final Timestamp x) throws SQLException {
        preparedStatementDelegate.setTimestamp(parameterIndex, x);
    }

    @Override
    public void setTimestamp(final int parameterIndex, final Timestamp x, final Calendar cal) throws SQLException {
        preparedStatementDelegate.setTimestamp(parameterIndex, x, cal);
    }

    @Override
    public void setURL(final int parameterIndex, final URL x) throws SQLException {
        preparedStatementDelegate.setURL(parameterIndex, x);
    }

    @Override
    @Deprecated
    public void setUnicodeStream(final int parameterIndex, final InputStream x, final int length) throws SQLException {
        preparedStatementDelegate.setUnicodeStream(parameterIndex, x, length);
    }

    @Override
    public String toString() {
        return preparedStatementDelegate.toString();
    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
        preparedStatementDelegate.setAsciiStream(parameterIndex, x);
    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
        preparedStatementDelegate.setAsciiStream(parameterIndex, x, length);
    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
        preparedStatementDelegate.setBinaryStream(parameterIndex, x);
    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {
        preparedStatementDelegate.setBinaryStream(parameterIndex, x, length);
    }

    @Override
    public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
        preparedStatementDelegate.setBlob(parameterIndex, inputStream);
    }

    @Override
    public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
        preparedStatementDelegate.setBlob(parameterIndex, inputStream, length);
    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
        preparedStatementDelegate.setCharacterStream(parameterIndex, reader);
    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {
        preparedStatementDelegate.setCharacterStream(parameterIndex, reader, length);
    }

    @Override
    public void setClob(int parameterIndex, Reader reader) throws SQLException {
        preparedStatementDelegate.setClob(parameterIndex, reader);
    }

    @Override
    public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
        preparedStatementDelegate.setClob(parameterIndex, reader, length);
    }

    @Override
    public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
        preparedStatementDelegate.setNCharacterStream(parameterIndex, value);
    }

    @Override
    public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
        preparedStatementDelegate.setNCharacterStream(parameterIndex, value, length);
    }

    @Override
    public void setNClob(int parameterIndex, NClob value) throws SQLException {
        preparedStatementDelegate.setNClob(parameterIndex, value);
    }

    @Override
    public void setNClob(int parameterIndex, Reader reader) throws SQLException {
        preparedStatementDelegate.setNClob(parameterIndex, reader);
    }

    @Override
    public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
        preparedStatementDelegate.setNClob(parameterIndex, reader, length);
    }

    @Override
    public void setNString(int parameterIndex, String value) throws SQLException {
        preparedStatementDelegate.setNString(parameterIndex, value);
    }

    @Override
    public void setRowId(int parameterIndex, RowId x) throws SQLException {
        preparedStatementDelegate.setRowId(parameterIndex, x);
    }

    @Override
    public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
        preparedStatementDelegate.setSQLXML(parameterIndex, xmlObject);
    }

    @Override
    public boolean isClosed() throws SQLException {
        return preparedStatementDelegate.isClosed();
    }

    @Override
    public boolean isPoolable() throws SQLException {
        return preparedStatementDelegate.isPoolable();
    }

    @Override
    public void setPoolable(boolean poolable) throws SQLException {
        preparedStatementDelegate.setPoolable(poolable);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) {
        return iface.isAssignableFrom(preparedStatementDelegate.getClass());
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        if (iface.isAssignableFrom(preparedStatementDelegate.getClass())) {
            return iface.cast(preparedStatementDelegate);
        }
        throw new SQLException("Not a wrapper for: " + iface.getName());
    }
}
