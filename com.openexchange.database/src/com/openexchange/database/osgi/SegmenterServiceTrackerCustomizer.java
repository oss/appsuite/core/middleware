/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.database.osgi;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import com.openexchange.database.internal.Initialization;
import com.openexchange.segmenter.client.SegmenterService;


/**
 * {@link SegmenterServiceTrackerCustomizer} - Injects the {@link SegmenterService}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public class SegmenterServiceTrackerCustomizer implements ServiceTrackerCustomizer<SegmenterService, SegmenterService> {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(SegmenterServiceTrackerCustomizer.class);

    private final BundleContext context;

    public SegmenterServiceTrackerCustomizer(BundleContext context) {
        super();
        this.context = context;
    }

    @Override
    public SegmenterService addingService(ServiceReference<SegmenterService> reference) {
        SegmenterService service = context.getService(reference);
        LOG.info("Injecting segmenter service into database bundle.");
        Initialization.setSegmenterService(service);
        return service;
    }

    @Override
    public void modifiedService(ServiceReference<SegmenterService> reference, SegmenterService service) {
        // nothing to do
    }

    @Override
    public void removedService(ServiceReference<SegmenterService> reference, SegmenterService service) {
        LOG.info("Removing segmenter service from database bundle.");
        Initialization.setSegmenterService(null);
        context.ungetService(reference);
    }

}
