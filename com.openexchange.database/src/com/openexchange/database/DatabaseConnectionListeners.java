/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.function.Consumer;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.java.Consumers.OXConsumer;

/**
 * {@link DatabaseConnectionListeners}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @since v8.0.0
 */
public final class DatabaseConnectionListeners {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(DatabaseConnectionListeners.class);

    /**
     * Tries to add a callback routine that'll be invoked after the supplied database connection has been committed. If this was not
     * possible or applicable, the callback is executed directly inline.
     * <p/>
     * Exceptions during deferred handling in <i>after-commit</i> phase are caught and logged (critical ones are still re-thrown).
     * When the callback is invoked directly, exceptions are not handled.
     *
     * @param connection The connection to add the callback routine for, or <code>null</code> for immediate execution of the callback
     * @param callback The callback routine to add
     * @see ExceptionUtils#handleException(Exception)
     */
    public static <E extends Exception> void addAfterCommitCallbackElseExecute(Connection connection, OXConsumer<Connection, E> callback) throws E {
        if (addAfterCommitListener(connection, new OXConsumerCallingAfterCommitDatabaseConnectionListener<E>(callback))) {
            LOG.debug("Callback for after-commit phase registered successfully.");
        } else {
            LOG.debug("Callback could not be registered for after-commit phase, invoking directly.");
            callback.accept(connection);
        }
    }


    /**
     * Tries to add a callback routine that'll be invoked after the supplied database connection has been committed.
     *
     * @param connection The connection to add the callback routine for, or <code>null</code> for a no-op
     * @param callback The callback routine to add
     * @return <code>true</code> if the callback routine could be added, <code>false</code>, otherwise
     */
    public static boolean addAfterCommitCallback(Connection connection, Consumer<Connection> callback) {
        return addAfterCommitListener(connection, new ConsumerCallingAfterCommitDatabaseConnectionListener(callback));
    }

    /**
     * Tries to add a listener routine that'll be invoked after the supplied database connection has been committed.
     *
     * @param connection The connection to add the callback routine for, or <code>null</code> for a no-op
     * @param listener The listener to add
     * @return <code>true</code> if the listener could be added, otherwise <code>false</code>
     */
    public static boolean addAfterCommitListener(Connection connection, AfterCommitDatabaseConnectionListener listener) {
        if (null == connection) {
            return false;
        }

        try {
            if (Databases.isNotInTransaction(connection)) {
                return false;
            }
        } catch (SQLException e) {
            LOG.warn("", e);
            return false;
        }

        DatabaseConnectionListenerAnnotatable listenerAnnotatable = optDatabaseConnectionListenerAnnotatable(connection);
        if (null == listenerAnnotatable) {
            return false;
        }
        listenerAnnotatable.addListener(listener);
        return true;
    }

    /**
     * Obtains a reference to the connection listener implemented by the supplied database connection if possible.
     *
     * @param connection The connection to get the connection listener annotatable for, or <code>null</code> for a no-op
     * @return A reference to the connection listener implemented by the supplied database connection, or <code>null</code> if not available
     */
    public static DatabaseConnectionListenerAnnotatable optDatabaseConnectionListenerAnnotatable(Connection connection) {
        if (null != connection) {
            if ((connection instanceof DatabaseConnectionListenerAnnotatable)) {
                return (DatabaseConnectionListenerAnnotatable) connection;
            }
            try {
                if (connection.isWrapperFor(DatabaseConnectionListenerAnnotatable.class)) {
                    return connection.unwrap(DatabaseConnectionListenerAnnotatable.class);
                }
            } catch (SQLException e) {
                LOG.warn("", e);
            }
        }
        return null;
    }

    /**
     * Initializes a new {@link DatabaseConnectionListeners}.
     */
    private DatabaseConnectionListeners() {
        super();
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    /**
     * Simple after-commit database connection listener that invokes given call-back.
     */
    private static final class ConsumerCallingAfterCommitDatabaseConnectionListener extends AfterCommitDatabaseConnectionListener {

        /** The call-back to call */
        private final Consumer<Connection> callback;

        /**
         * Initializes a new {@link AfterCommitDatabaseConnectionListenerExtension}.
         *
         * @param The call-back to call
         */
        ConsumerCallingAfterCommitDatabaseConnectionListener(Consumer<Connection> callback) {
            super();
            this.callback = callback;
        }

        @Override
        public void onAfterCommitPerformed(Connection connection) {
            try {
                callback.accept(connection);
            } catch (Exception e) {
                LOG.warn("Error invoking callback after commit", e);
            }
        }
    }

    /**
     * Simple after-commit database connection listener that invokes given call-back.
     */
    private static final class OXConsumerCallingAfterCommitDatabaseConnectionListener<E extends Exception> extends AfterCommitDatabaseConnectionListener {

        /** The call-back to call */
        private final OXConsumer<Connection, E> callback;

        /**
         * Initializes a new {@link OXConsumerCallingAfterCommitDatabaseConnectionListener}.
         *
         * @param callback The call-back to call
         */
        OXConsumerCallingAfterCommitDatabaseConnectionListener(OXConsumer<Connection, E> callback) {
            super();
            this.callback = callback;
        }

        @Override
        public void onAfterCommitPerformed(Connection connection) {
            try {
                callback.accept(connection);
            } catch (Exception e) {
                ExceptionUtils.handleException(e);
                LOG.warn("Error invoking callback after commit", e);
            }
        }
    }

}
