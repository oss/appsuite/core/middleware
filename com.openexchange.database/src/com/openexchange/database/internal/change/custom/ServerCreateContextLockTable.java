/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.database.internal.change.custom;

import static com.openexchange.database.Databases.tableExists;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.openexchange.database.Databases;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.DatabaseConnection;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;


/**
 * {@link ServerCreateContextLockTable} - Custom task to add context_lock table used to claim pre-provisioned contexts
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public class ServerCreateContextLockTable implements CustomTaskChange {

    private static final String TABLE_NAME = "context_lock";
    private static final String TABLE_CREATE_SQL = "CREATE TABLE `" + TABLE_NAME + "` ("
                                                 + "    `cid` INT(10) UNSIGNED NOT NULL,"
                                                 + "    `claim` BINARY(16) NOT NULL,"
                                                 + "    `timestamp` BIGINT(20) UNSIGNED NOT NULL,"
                                                 + "    PRIMARY KEY(`cid`)"
                                                 + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci";

    public ServerCreateContextLockTable() {
        super();
    }

    @Override
    public String getConfirmationMessage() {
        return "Table \"context_lock\" successful created.";
    }

    @Override
    public void setUp() throws SetupException {
        // nothing to do
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        // nothing to do
    }

    @Override
    public ValidationErrors validate(Database database) {
        return new ValidationErrors();
    }

    @Override
    public void execute(Database database) throws CustomChangeException {
        DatabaseConnection databaseConnection = database.getConnection();
        if (!(databaseConnection instanceof JdbcConnection)) {
            throw new CustomChangeException("Cannot get underlying connection because database connection is not of type " + JdbcConnection.class.getName() + ", but of type: " + databaseConnection.getClass().getName());
        }

        org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ServerAddUuidColumnCustomTaskChange.class);
        int rollback = 0;
        Connection configDbCon = ((JdbcConnection) databaseConnection).getUnderlyingConnection();
        try {
            if (!tableExists(configDbCon, TABLE_NAME)) {
                Databases.startTransaction(configDbCon);
                rollback = 1;

                try (PreparedStatement stmt = configDbCon.prepareStatement(TABLE_CREATE_SQL)) {
                    stmt.execute();
                }

                configDbCon.commit();
                rollback = 2;
            }
        } catch (SQLException e) {
            logger.error("Failed to add table \"context_lock\"", e);
            throw new CustomChangeException("SQL error", e);
        } catch (RuntimeException e) {
            logger.error("Failed to add table \"context_lock\"", e);
            throw new CustomChangeException("Runtime error", e);
        } finally {
            if (rollback > 0) {
                if (rollback == 1) {
                    Databases.rollback(configDbCon);
                }
                Databases.autocommit(configDbCon);
            }
        }
    }

}
