/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.database.internal.reloadable;

import com.openexchange.config.AbstractCompositeReloadable;
import com.openexchange.config.ConfigurationService;

/**
 * {@link GenericReloadable} - Collects contributed reloadables for server bundle.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since 7.6.0
 */
public final class GenericReloadable extends AbstractCompositeReloadable {

    private static final GenericReloadable INSTANCE = new GenericReloadable();

    /**
     * Gets the instance.
     *
     * @return The instance
     */
    public static GenericReloadable getInstance() {
        return INSTANCE;
    }

    // --------------------------------------------------------------------------------------------------- //

    /**
     * Initializes a new {@link GenericReloadable}.
     */
    private GenericReloadable() {
        super();
    }

    @Override
    protected void preReloadConfiguration(ConfigurationService configService) {
        // Nothing
    }

    @Override
    protected void postReloadConfiguration(ConfigurationService configService) {
        // Nothing
    }

    @Override
    protected String[] getFileNamesOfInterest() {
        return new String[] { "configdb.properties" };
    }

    @Override
    protected String[] getPropertiesOfInterest() {
        return new String[] { "com.openexchange.database.*" };
    }

}
