/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.database;

import java.util.Optional;

/**
 * {@link SqlUpdateBuilder} - Builds the prepared SQL statement for updating a table.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class SqlUpdateBuilder {

    private final String tableName;
    private StringBuilder sb;

    /**
     * Initializes a new {@link SqlUpdateBuilder}.
     *
     * @param tableName The name of the table without surrounding back-tick characters (<code>'`'</code>)
     */
    public SqlUpdateBuilder(String tableName) {
        super();
        this.tableName = tableName;
    }

    /**
     * Adds specified column name to updated columns.
     *
     * @param columnName The name of the column without surrounding back-tick characters (<code>'`'</code>)
     * @return This builder
     */
    public SqlUpdateBuilder addColumnToSet(String columnName) {
        if (columnName != null) {
            if (sb == null) {
                sb = new StringBuilder("UPDATE `").append(tableName).append("` SET ");
            } else {
                sb.append(", ");
            }
            sb.append('`').append(columnName).append("`=?");
        }
        return this;
    }

    /**
     * Finishes this SQL builder.
     *
     * @param whereClause The optional <code>WHERE</code> clause
     * @return The optional statement; empty if there is nothing to update
     */
    public Optional<String> finish(String whereClause) {
        if (sb == null) {
            return Optional.empty();
        }

        if (whereClause != null) {
            sb.append(whereClause);
        }
        return Optional.of(sb.toString());
    }

    @Override
    public String toString() {
        return sb == null ? "<empty>" : sb.toString();
    }

}
