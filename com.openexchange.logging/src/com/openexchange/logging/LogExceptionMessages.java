/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.logging;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link LogExceptionMessages}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public class LogExceptionMessages implements LocalizableStrings {

    public static final String NOT_FOUND_MSG = "Cannot find context %d.";

    public static final String USER_NOT_FOUND_MSG = "Cannot find user with identifier %1$s in context %2$d.";

    public static final String SESSION_NOT_FOUND = "Cannot find session with identifier %1$s";

    /**
     * Initializes a new {@link LogExceptionMessages}.
     */
    private LogExceptionMessages() {}
}
