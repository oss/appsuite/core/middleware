/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.logging;

import java.util.Collection;
import java.util.function.Supplier;
import com.openexchange.exception.LogLevel;

/**
 * {@link LogUtility} - Utility class for logging.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.5
 * @deprecated Please use <code>com.openexchange.log.LogUtility</code>
 */
@Deprecated
public final class LogUtility {

    /**
     * Initializes a new {@link LogUtility}.
     */
    private LogUtility() {
        super();
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a {@link #toString()} object for given integer array.
     *
     * @param integers The integer array
     * @return The object providing content of given array if {@link #toString()} is invoked
     */
    public static Object toStringObjectFor(int[] integers) {
        return com.openexchange.log.LogUtility.toStringObjectFor(integers);
    }

    /**
     * Creates a {@link #toString()} object for given integer array.
     *
     * @param integers The integer array
     * @param optDelimiter The delimiter to be used between each element or <code>null</code> for element-wise output
     * @return The object providing content of given array if {@link #toString()} is invoked
     */
    public static Object toStringObjectFor(int[] integers, CharSequence optDelimiter) {
        return com.openexchange.log.LogUtility.toStringObjectFor(integers, optDelimiter);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a {@link #toString()} object for given object array.
     *
     * @param <O> The element type
     * @param objects The object array
     * @return The object providing content of given array if {@link #toString()} is invoked
     */
    public static <O> Object toStringObjectFor(O[] objects) {
        return com.openexchange.log.LogUtility.toStringObjectFor(objects);
    }

    /**
     * Creates a {@link #toString()} object for given object array.
     *
     * @param <O> The element type
     * @param objects The object array
     * @param optDelimiter The delimiter to be used between each element or <code>null</code> for element-wise output
     * @return The object providing content of given array if {@link #toString()} is invoked
     */
    public static <O> Object toStringObjectFor(O[] objects, CharSequence optDelimiter) {
        return com.openexchange.log.LogUtility.toStringObjectFor(objects, optDelimiter);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a {@link #toString()} object for given object collection.
     *
     * @param <O> The element type
     * @param objects The object collection
     * @return The object providing content of given collection if {@link #toString()} is invoked
     */
    public static <O> Object toStringObjectFor(Collection<O> objects) {
        return com.openexchange.log.LogUtility.toStringObjectFor(objects);
    }

    /**
     * Creates a {@link #toString()} object for given object collection.
     *
     * @param <O> The element type
     * @param objects The object collection
     * @param optDelimiter The delimiter to be used between each element or <code>null</code> for element-wise output
     * @return The object providing content of given collection if {@link #toString()} is invoked
     */
    public static <O> Object toStringObjectFor(Collection<O> objects, CharSequence optDelimiter) {
        return com.openexchange.log.LogUtility.toStringObjectFor(objects, optDelimiter);
    }

    /**
     * Creates a {@link #toString()} object for given object collection.
     *
     * @param <O> The element type
     * @param objects The object collection
     * @param optDelimiter The delimiter to be used between each element or <code>null</code> for element-wise output
     * @param maxSize The maximum number of elements in the collection to consider in the string representation, or <code>-1</code> for no limitation
     * @return The object providing content of given collection if {@link #toString()} is invoked
     */
    public static <O> Object toStringObjectFor(Collection<O> objects, CharSequence optDelimiter, int maxSize) {
        return com.openexchange.log.LogUtility.toStringObjectFor(objects, optDelimiter, maxSize);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a {@link #toString()} object for given supplier.
     *
     * @param <O> The element type
     * @param supplier The supplier
     * @return The object providing content of given supplier if {@link #toString()} is invoked
     */
    public static <O> Object toStringObjectFor(Supplier<O> supplier) {
        return com.openexchange.log.LogUtility.toStringObjectFor(supplier);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Creates a {@link #toString()} for identity hash code for given object.
     *
     * @param object The object
     * @return The object providing identity hash code if {@link #toString()} is invoked
     */
    public static Object toIdentityHashCodeFor(Object object) {
        return com.openexchange.log.LogUtility.toIdentityHashCodeFor(object);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Logs a (possibly frequently occurring) message using an increased log level only once per day while silencing all further other
     * occurrences, to raise awareness on the one hand, but reduce repeated/unnecessary noise on the other hand.
     * <p/>
     * The log message itself (including the format string and arguments, ignoring {@link Throwable}s) is used implicitly as key for
     * {@link #logOncePerDay(Object, org.slf4j.Logger, LogLevel, LogLevel, String, Object...)}.
     *
     * @param logger The logger to use for outputting
     * @param increasedLevel The increased log level to use (once per day), typically {@link LogLevel#INFO}, {@link LogLevel#WARNING} or {@link LogLevel#ERROR}
     * @param silentLevel The "silent" log level to use otherwise, typically {@link LogLevel#DEBUG} or {@link LogLevel#TRACE}
     * @param format The log format string to pass to the logger
     * @param arguments The log arguments to pass to the logger
     */
    public static void logOncePerDay(org.slf4j.Logger logger, LogLevel increasedLevel, LogLevel silentLevel, String format, Object... arguments) {
        com.openexchange.log.LogUtility.logOncePerDay(logger, increasedLevel, silentLevel, format, arguments);
    }

    /**
     * Logs a (possibly frequently occurring) message using an increased log level only once per day while silencing all further other
     * occurrences, to raise awareness on the one hand, but reduce repeated/unnecessary noise on the other hand.
     *
     * @param key An arbitrary key representing the log event
     * @param logger The logger to use for outputting
     * @param increasedLevel The increased log level to use (once per day), typically {@link LogLevel#INFO}, {@link LogLevel#WARNING} or {@link LogLevel#ERROR}
     * @param silentLevel The "silent" log level to use otherwise, typically {@link LogLevel#DEBUG} or {@link LogLevel#TRACE}
     * @param format The log format string to pass to the logger
     * @param arguments The log arguments to pass to the logger
     */
    public static void logOncePerDay(Object key, org.slf4j.Logger logger, LogLevel increasedLevel, LogLevel silentLevel, String format, Object... arguments) {
        com.openexchange.log.LogUtility.logOncePerDay(key, logger, increasedLevel, silentLevel, format, arguments);
    }

}
