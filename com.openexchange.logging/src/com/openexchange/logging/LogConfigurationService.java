/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.logging;

import java.util.List;
import java.util.Map;
import java.util.Set;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.annotation.SingletonService;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

/**
 * {@link LogConfigurationService}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @since v7.10.3
 */
@SingletonService
public interface LogConfigurationService {

    /**
     * Represents a collection of loggers grouped together with a common id
     */
    public record LoggerCollection(String id, Map<String, Level> loggers) {}

    /**
     * Creates a logging filter for the specified loggers in the given context.
     *
     * @param contextId The context identifier
     * @param loggers The logger names and their {@link Level}s
     * @return A {@link LogResponse} with information about the outcome of the operation
     * @throws OXException
     */
    LogResponse createContextFilter(int contextId, Map<String, Level> loggers) throws OXException;

    /**
     * Creates a logging filter for the specified loggers for the given user.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param loggers The logger names and their {@link Level}s
     * @return A {@link LogResponse} with information about the outcome of the operation
     * @throws OXException
     */
    LogResponse createUserFilter(int userId, int contextId, Map<String, Level> loggers) throws OXException;

    /**
     * Creates a logging filter for the specified loggers for the given session
     *
     * @param sessionId The session identifier
     * @param loggers The logger names and their {@link Level}s
     * @return A {@link LogResponse} with information about the outcome of the operation
     * @throws OXException
     */
    LogResponse createSessionFilter(String sessionId, Map<String, Level> loggers) throws OXException;

    /**
     * Gets an unmodifiable {@link Set} with all logging filters
     *
     * @return An unmodifiable {@link Set} with all logging filters
     */
    Set<String> listFilters();

    /**
     * Checks whether any filter exists. Tries the following three combinations (in that order):
     * <ul>
     * <li>user session filter (sessionId)</li>
     * <li>user filter (contextId/userId)</li>
     * <li>context filter (contextId)</li>
     * </ul>
     *
     * @return <code>true</code> if any of the possible combinations yield to a logging filter;
     *         <code>false</code> otherwise.
     */
    boolean anyFilterExists(int contextId, int userId, String sessionId);

    /**
     * Gets the log levels set for session based filters
     *
     * @param sessionId the session
     * @return A map of logger- names and level for the given session
     */
    Map<String, Level> getSessionLogLevels(String sessionId);

    /**
     * Returns all session based filters/loggers
     *
     * @return A collection of all session based filters/loggers
     * @throws OXException
     */
    List<LoggerCollection> getSessionsLogLevels() throws OXException;

    /**
     * Gets the log levels set for context based filters
     *
     * @param contextId the context ID
     * @return A map of logger- names and level for the given context
     * @throws OXException
     */
    Map<String, Level> getContextLogLevels(int contextId) throws OXException;

    /**
     * Returns all context based filters/loggers
     *
     * @return A collection of all context based filters/loggers
     * @throws OXException
     */
    List<LoggerCollection> getContextLogLevels() throws OXException;

    /**
     * Gets the log levels set for user based filters
     *
     * @param contextId the context ID
     * @param userId the user ID
     * @return A map of logger- names and level for the given user
     * @throws OXException
     */
    Map<String, Level> getUserLogLevels(int contextId, int userId) throws OXException;

    /**
     * Returns all user based filters/loggers for a given context
     *
     * @return A collection of all user based filters/loggers within the given context
     * @throws OXException
     */
    List<LoggerCollection> getUserLogLevels(int contextId) throws OXException;

    /**
     * Removes the logging filter for the specified context and for the specified loggers
     *
     * @param contextId The context identifier
     * @param loggers The logger names
     * @return A {@link LogResponse} with information about the outcome of the operation
     * @throws OXException
     */
    LogResponse removeContextFilter(int contextId, List<String> loggers) throws OXException;

    /**
     * Removes the logging filter for the specified user in the specified context and for the specified loggers
     *
     * @param contextId The context identifier
     * @param userId The user identifier
     * @param loggers The logger names
     * @return A {@link LogResponse} with information about the outcome of the operation
     * @throws OXException
     */
    LogResponse removeUserFilter(int contextId, int userId, List<String> loggers) throws OXException;

    /**
     * Removes the logging filter for the specified session and for the specified loggers
     *
     * @param sessionId The session identifier
     * @param loggers The logger names
     * @return A {@link LogResponse} with information about the outcome of the operation
     */
    LogResponse removeSessionFilter(String sessionId, List<String> loggers);

    /**
     * Removes all logging filters
     *
     * @return A {@link LogResponse} with information about the outcome of the operation
     */
    LogResponse clearFilters();

    /**
     * Modifies the specified level for the specified loggers
     *
     * @param loggers The loggers for which to modify the levels
     * @return A {@link LogResponse} with information about the outcome of the operation
     */
    LogResponse modifyLogLevels(Map<String, Level> loggers);

    /**
     * Overrides {@link Exception} categories to be suppressed (comma separated)
     *
     * @param categories The categories to suppress
     * @return A {@link LogResponse} with information about the outcome of the operation
     */
    LogResponse overrideExceptionCategories(String categories);

    /**
     * Overrides {@link Exception} categories to be suppressed
     *
     * @param categories The categories to suppress
     * @return A {@link LogResponse} with information about the outcome of the operation
     */
    LogResponse overrideExceptionCategories(Set<String> categories);

    /**
     * Returns a {@link Set} with all {@link Exception} categories
     *
     * @return a {@link Set} with all {@link Exception} categories
     */
    Set<String> listExceptionCategories();

    /**
     * Returns a {@link Set} with the names of all known loggers in the system
     *
     * @return a {@link Set} with the names of all known loggers in the system
     */
    Set<String> listLoggers();

    /**
     * Returns a {@link Set} with all known loggers in the system
     *
     * @return a {@link Set} with all known loggers in the system
     */
    Set<Logger> getLoggers();

    /**
     * Returns a {@link Set} with the names of all loggers along with their level that were dynamically modified.
     *
     * @return a {@link Set} with the names of all loggers along with their level that were dynamically modified.
     */
    Set<String> listDynamicallyModifiedLoggers();

    /**
     * Returns a {@link Set} with the level of the specified loggers
     *
     * @param loggers The logger names
     * @return a {@link Set} with the level of the specified loggers
     */
    Set<String> getLevelForLoggers(String[] loggers);

    /**
     * Sets whether to include stack-traces in HTTP API JSON responses for the specified user
     * in the specified context
     *
     * @param contextId The context identifier
     * @param userId The user identifier
     * @param enable whether or not to enable the stack-traces in the HTTP API
     * @throws OXException
     */
    void includeStackTraceForUser(int contextId, int userId, boolean enable) throws OXException;

    /**
     * Gets whether the stack-trace is included in the HTTP API JSON response for a specified user
     * in the specified context
     *
     * @param contextId The context identifier
     * @param userId The user identifier
     * @return <code>True</code> if the stack trace is included in the HTTP response for the specified user, <code>False</code> otherwise
     * @throws OXException
     */
    boolean getIncludeStackTraceForUser(int contextId, int userId) throws OXException;

    /**
     * Returns an information string about all root appenders.
     *
     * @return an information string about all root appenders.
     */
    String getRootAppenderStats();
}
