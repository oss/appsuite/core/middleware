/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.context.preassembly.rest.osgi;

import com.openexchange.admin.context.preassembly.ContextPreAssemblyService;
import com.openexchange.admin.context.preassembly.rest.ContextPreAssemblyAPI;
import com.openexchange.admin.context.preassembly.rest.requestanalyzer.ContextPreAssemblyRequestAnalyzer;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.request.analyzer.RequestAnalyzer;

/**
 * {@link ContextPreAssemblyRestActivator} The activator for the admin rest API
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public final class ContextPreAssemblyRestActivator extends HousekeepingActivator {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextPreAssemblyRestActivator.class);

    /**
     * Initializes a new {@link ContextPreAssemblyRestActivator}
     */
    public ContextPreAssemblyRestActivator() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { ContextPreAssemblyService.class };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        LOG.info("Starting context pre-assembly REST API bundle");

        // Register the different services for this bundle
        registerService(ContextPreAssemblyAPI.class, new ContextPreAssemblyAPI(getServiceSafe(ContextPreAssemblyService.class)));

        // Register request analyzer
        registerService(RequestAnalyzer.class, new ContextPreAssemblyRequestAnalyzer());
    }
}
