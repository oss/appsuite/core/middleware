/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.context.preassembly.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import com.openexchange.admin.context.preassembly.ContextPreAssemblyService;
import com.openexchange.admin.context.preassembly.dao.ContextPreAssemblyResult;
import com.openexchange.admin.context.preassembly.exception.ContextPreAssemblyExceptionCodes;
import com.openexchange.admin.context.preassembly.rest.exception.ContextPreAssemblyAPIExceptionCodes;
import com.openexchange.admin.context.preassembly.rest.requestanalyzer.ContextPreAssemblyRequestAnalyzer;
import com.openexchange.admin.metrics.APIMetricsProcessor;
import com.openexchange.exception.Category;
import com.openexchange.exception.OXException;
import com.openexchange.java.Autoboxing;
import com.openexchange.rest.services.RESTUtils;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;

/**
 * {@link ContextPreAssemblyRequestAnalyzer} - The REST endpoint for context pre-assembling.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
@Path("admin/v1/contexts/pre-assemble")
@RoleAllowed(Role.MASTER_ADMIN_AUTHENTICATED)
public class ContextPreAssemblyAPI {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextPreAssemblyAPI.class);
    private static final String PARAM_NUMBER = "number";
    private static final String PARAM_SCHEMA = "schema";
    private static final String PARAM_FILESTORE_ID = "filestore_id";

    private final ContextPreAssemblyService contextPreAssemblyService;

    /**
     * Initializes a new {@link ContextPreAssemblyAPI}.
     *
     * @param contextPreAssemblyService The {@link ContextPreAssemblyService} to pre-assemble contexts
     */
    public ContextPreAssemblyAPI(ContextPreAssemblyService contextPreAssemblyService) {
        super();
        this.contextPreAssemblyService = contextPreAssemblyService;
    }

    /**
     * Pre-assembles contexts based on the given payload.
     *
     * @param The payload containing information about the pre-assembly operation (e. g. number of contexts to create, schema name and filestore id).
     * @return If the operation was successful, a {@link Response} with status 200 and the ids of the pre-assembled contexts. Otherwise a {@link Response} with status 400.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response preAssembleContexts(JSONObject payload) {
        Timer.Sample timerSample = Timer.start(Metrics.globalRegistry);
        boolean success = false;
        try {
            Response preAssembleContexts = preAssembleContextsInternal(payload);
            if (preAssembleContexts.getStatus() >= 200 && preAssembleContexts.getStatus() < 300) {
                success = true;
            }
            return preAssembleContexts;
        } finally {
            APIMetricsProcessor.getForCreate().stopTimer(timerSample, success);
        }
    }

    private Response preAssembleContextsInternal(JSONObject payload) {
        List<String> missingParams = getMissingParams(payload);
        if (!missingParams.isEmpty()) {
            String missingParamsString = missingParams.stream().map(Object::toString).collect(java.util.stream.Collectors.joining(","));
            return Response.status(Status.BAD_REQUEST).entity("Required parameter missing: " + missingParamsString).build();
        }

        try {
            int numberOfContextsToPreAssemble = payload.getInt(PARAM_NUMBER);
            checkNumberOfContextsToPreAssemble(numberOfContextsToPreAssemble);

            Integer filestoreId = payload.has(PARAM_FILESTORE_ID) ? Autoboxing.I(payload.getInt(PARAM_FILESTORE_ID)) : null;
            String schema = payload.getString(PARAM_SCHEMA);

            ContextPreAssemblyResult preAssemblyResult = contextPreAssemblyService.preAssemble(schema, numberOfContextsToPreAssemble, filestoreId);
            JSONObject resultObject = new JSONObject(2);
            if (null != preAssemblyResult.contextIds()) {
                resultObject.put("contextIds", new JSONArray(preAssemblyResult.contextIds()));
            }
            if (null != preAssemblyResult.errors()) {
                resultObject.put("errors", new JSONArray(preAssemblyResult.errors().stream().map((e) -> RESTUtils.generateError(e)).toList()));
            }
            return Response.ok().entity(resultObject).build();
        } catch (OXException e) {
            JSONObject jsonError = RESTUtils.generateError(e);
            if (e.getExceptionCode() instanceof ContextPreAssemblyExceptionCodes && Category.CATEGORY_USER_INPUT.equals(e.getCategory())) {
                return RESTUtils.respondBadRequest(jsonError);
            } else if (e.getExceptionCode() instanceof ContextPreAssemblyAPIExceptionCodes && Category.CATEGORY_USER_INPUT.equals(e.getCategory())) {
                return RESTUtils.respondBadRequest(jsonError);
            } else if (e.getExceptionCode() instanceof ContextPreAssemblyExceptionCodes) {
                return RESTUtils.respondNotFound(jsonError);
            }
            LOG.error("Error while creating pre-assembled contexts: " + e.getMessage(), e);
            return RESTUtils.respondServerError(jsonError);
        } catch (JSONException e) {
            LOG.debug("Error while reading client parameters: " + e.getMessage(), e);
            return Response.status(Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage()).build();
        }
    }

    private void checkNumberOfContextsToPreAssemble(int number) throws OXException {
        if (number <= 0) {
            throw ContextPreAssemblyAPIExceptionCodes.INVALID_NUMBER_OF_CONTEXTS.create();
        }
        if (number > 500) {
            throw ContextPreAssemblyAPIExceptionCodes.NUMBER_OF_CONTEXTS_TOO_LARGE.create();
        }
    }

    private List<String> getMissingParams(JSONObject payload) {
        if (payload == null || payload.isEmpty()) {
            return Arrays.asList(PARAM_NUMBER, PARAM_SCHEMA);
        }
        List<String> missing = new ArrayList<>();
        if (!payload.has(PARAM_NUMBER)) {
            missing.add(PARAM_NUMBER);
        }
        if (!payload.has(PARAM_SCHEMA)) {
            missing.add(PARAM_SCHEMA);
        }
        return missing;
    }

}
