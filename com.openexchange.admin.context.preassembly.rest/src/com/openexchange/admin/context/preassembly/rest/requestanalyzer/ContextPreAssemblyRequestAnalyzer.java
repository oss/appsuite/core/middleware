/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.admin.context.preassembly.rest.requestanalyzer;

import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.optParameterValue;
import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.parseBodyParameters;
import java.util.Optional;
import com.openexchange.exception.OXException;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.BodyData;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.segment.SegmentMarker;

/**
 * {@link ContextPreAssemblyRequestAnalyzer} - {@link RequestAnalyzer} for the context pre-assembly REST servlet
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class ContextPreAssemblyRequestAnalyzer implements RequestAnalyzer {

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<AnalyzeResult> analyze(RequestData data) throws OXException {
        if (!"POST".equals(data.getMethod())) {
            // not a request for the contest pre-assemble REST service
            return Optional.empty();
        }
        Optional<String> path = data.getParsedURL().getPath();
        if (path.isEmpty() || !path.get().startsWith("/admin/v1/contexts/pre-assemble")) {
            // not a request for the contest pre-assemble REST service
            return Optional.empty();
        }
        Optional<BodyData> requestBody = data.optBody();
        if (requestBody.isEmpty()) {
            return Optional.of(AnalyzeResult.MISSING_BODY);
        }
        Optional<String> schemaOpt = optParameterValue(parseBodyParameters(requestBody.get()), "schema");
        if (!schemaOpt.isEmpty()) {
            return Optional.of(AnalyzeResult.UNKNOWN);
        }

        AnalyzeResult result = new AnalyzeResult(SegmentMarker.of(schemaOpt.get()), null);
        return Optional.of(result);
    }
}
