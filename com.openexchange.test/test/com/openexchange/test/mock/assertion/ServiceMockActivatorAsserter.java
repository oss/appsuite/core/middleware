/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.test.mock.assertion;

import java.util.List;
import org.junit.jupiter.api.Assertions;
import com.google.common.collect.Multimap;
import com.openexchange.test.mock.InjectionFieldConstants;
import com.openexchange.test.mock.MockUtils;

/**
 * {@link ServiceMockActivatorAsserter} activates mocking for the provided classes.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 7.4
 */
public class ServiceMockActivatorAsserter {

    /**
     * Verifies if all services associated with the bundle are registered
     *
     * @param activator - the activator the services should be checked for
     * @param expectedNumberOfRegisteredTrackers - int with the number of expected registered services
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static void verifyAllServicesRegistered(Object activator, int expectedNumberOfRegisteredServices) throws Exception {
        Multimap<Object, Object> serviceRegistrations = (Multimap<Object, Object>) MockUtils.getValueFromField(activator,
                                                                                                               InjectionFieldConstants.SERVICE_REGISTRATIONS);

        Assertions.assertEquals(expectedNumberOfRegisteredServices, serviceRegistrations.size(), "Registered services do not match the number of expected ones");
    }

    /**
     * Verifies if all services associated with the bundle are unregistered
     *
     * @param activator - the activator the services should be checked for
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws NoSuchFieldException
     */
    @SuppressWarnings("unchecked")
    public static void verifyAllServicesUnregistered(Object activator) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Multimap<Object, Object> serviceRegistrations = (Multimap<Object, Object>) MockUtils.getValueFromField(
            activator,
            InjectionFieldConstants.SERVICE_REGISTRATIONS);

        Assertions.assertEquals(0, serviceRegistrations.size(), "Not all services deregistered!");
    }

    /**
     * Verifies if all trackers associated with the bundle are registered
     *
     * @param activator - the activator the trackers should be checked for
     * @param expectedNumberOfRegisteredTrackers - int with the number of expected registered trackers
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws NoSuchFieldException
     */
    public static void verifyAllServiceTrackersRegistered(Object activator, int expectedNumberOfRegisteredTrackers) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        List<?> serviceTrackers = (List<?>) MockUtils.getValueFromField(
            activator,
            InjectionFieldConstants.SERVICE_TRACKERS);

        Assertions.assertEquals(expectedNumberOfRegisteredTrackers, serviceTrackers.size(), "Registered trackers do not match the number of expected ones");
    }

    /**
     * Verifies if all trackers associated with the bundle are closed
     *
     * @param activator - the activator the trackers should be checked for
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws NoSuchFieldException
     */
    public static void verifyAllServiceTrackersClosed(Object activator) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        List<?> serviceTrackers = (List<?>) MockUtils.getValueFromField(
            activator,
            InjectionFieldConstants.SERVICE_TRACKERS);

        Assertions.assertEquals(0, serviceTrackers.size(), "Not all trackers closed!");
    }
}
