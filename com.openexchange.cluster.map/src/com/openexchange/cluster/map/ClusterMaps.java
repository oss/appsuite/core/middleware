/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cluster.map;

import com.openexchange.session.IUserAndContext;

/**
 * {@link ClusterMaps} - Utility class for cluster maps.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class ClusterMaps {

    /**
     * Initializes a new {@link ClusterMaps}.
     */
    private ClusterMaps() {
        super();
    }

    /**
     * Builds a composite key for specified arguments.
     * <pre>
     *  [contextId] + ":" + [userId] + ":" + [key]
     * </pre>
     *
     * @param key The key name
     * @param userAndContext The object providing user and context identifiers
     * @return The composite key
     * @see #buildCompositeKey(String, int, int)
     */
    public static String buildCompositeKey(String key, IUserAndContext userAndContext) {
        return buildCompositeKey(key, userAndContext.getUserId(), userAndContext.getContextId());
    }

    /**
     * Builds a composite key for specified arguments.
     * <pre>
     *  [contextId] + ":" + [userId] + ":" + [key]
     * </pre>
     *
     * @param key The key name
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The composite key
     */
    public static String buildCompositeKey(String key, int userId, int contextId) {
        return new StringBuilder(key.length() + 16).append(contextId).append(':').append(userId).append(':').append(key).toString();
    }

}
