/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cluster.map;

/**
 * {@link RemoteSiteOptions} - Options for calling remote sites.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RemoteSiteOptions {

    /**
     * Creates a new builder.
     *
     * @return The newly created biilder
     */
    public static Builder builder() {
        return new Builder();
    }

    /** The builder for an instance of <code>RemoteSiteOptions</code> */
    public static final class Builder {

        private boolean considerRemoteSites;
        private boolean asyncRemoteSiteCalling;

        private Builder() {
            super();
        }

        /**
         * Sets the considerRemoteSites
         *
         * @param considerRemoteSites The considerRemoteSites to set
         * @return This builder
         */
        public Builder withConsiderRemoteSites(boolean considerRemoteSites) {
            this.considerRemoteSites = considerRemoteSites;
            return this;
        }

        /**
         * Sets the asyncRemoteSiteCalling
         *
         * @param asyncRemoteSiteCalling The asyncRemoteSiteCalling to set
         * @return This builder
         */
        public Builder withAsyncRemoteSiteCalling(boolean asyncRemoteSiteCalling) {
            this.asyncRemoteSiteCalling = asyncRemoteSiteCalling;
            return this;
        }

        /**
         * Builds the instance of <code>RemoteSiteOptions</code> from this builder's arguments.
         *
         * @return The instance of <code>RemoteSiteOptions</code>
         */
        public RemoteSiteOptions build() {
            return new RemoteSiteOptions(considerRemoteSites, asyncRemoteSiteCalling);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** The empty remote site options; no remote sites are called */
    public static final RemoteSiteOptions EMPTY = builder().withConsiderRemoteSites(false).build();

    /** The remote site options for calling remote sites asynchronously */
    public static final RemoteSiteOptions ASYNC = builder().withConsiderRemoteSites(true).withAsyncRemoteSiteCalling(true).build();

    /** The remote site options for calling remote sites synchronously */
    public static final RemoteSiteOptions SYNC = builder().withConsiderRemoteSites(true).withAsyncRemoteSiteCalling(false).build();

    private final boolean considerRemoteSites;
    private final boolean asyncRemoteSiteCalling;

    /**
     * Initializes a new {@link RemoteSiteOptions}.
     *
     * @param considerRemoteSites <code>true</code> to consider remote sites (meaning changes are reflected at remote sites); otherwise <code>false</code> to ignore them
     * @param asyncRemoteSiteCalling <code>true</code> to call remote sites asynchronously; otherwise <code>false</code> to await termination of remote site calls
     */
    private RemoteSiteOptions(boolean considerRemoteSites, boolean asyncRemoteSiteCalling) {
        super();
        this.considerRemoteSites = considerRemoteSites;
        this.asyncRemoteSiteCalling = asyncRemoteSiteCalling;
    }

    /**
     * Checks whether to consider remote sites at all.
     *
     * @return <code>true</code> to consider remote sites (meaning changes are reflected at remote sites); otherwise <code>false</code> to ignore them
     */
    public boolean isConsiderRemoteSites() {
        return considerRemoteSites;
    }

    /**
     * Checks whether remote sites shall be called asynchronously.
     *
     * @return <code>true</code> to call remote sites asynchronously; otherwise <code>false</code> to await termination of remote site calls
     */
    public boolean isAsyncRemoteSiteCalling() {
        return asyncRemoteSiteCalling;
    }

}
