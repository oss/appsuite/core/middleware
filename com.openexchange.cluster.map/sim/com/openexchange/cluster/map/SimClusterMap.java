/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cluster.map;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import com.openexchange.exception.OXException;

/**
 * {@link SimClusterMap}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class SimClusterMap<V> implements ClusterMap<V> {

    private final ConcurrentMap<String, V> map;

    /**
     * Initializes a new {@link SimClusterMap}.
     */
    public SimClusterMap() {
        super();
        map = new ConcurrentHashMap<>();
    }

    @Override
    public boolean containsKey(String key) throws OXException {
        return map.containsKey(key);
    }

    @Override
    public V get(String key) throws OXException {
        return map.get(key);
    }

    @Override
    public V put(String key, V value, long expireMillis) throws OXException {
        return map.put(key, value);
    }

    @Override
    public V remove(String key) throws OXException {
        return map.remove(key);
    }

    @Override
    public boolean replace(String key, V oldValue, V newValue, long expireMillis) throws OXException {
        return map.replace(key, oldValue, newValue);
    }

    @Override
    public V putIfAbsent(String key, V value, long expireMillis) throws OXException {
        return map.putIfAbsent(key, value);
    }

    @Override
    public Set<String> keySet() throws OXException {
        return map.keySet();
    }

}
