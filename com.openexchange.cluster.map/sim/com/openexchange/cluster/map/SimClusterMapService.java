/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cluster.map;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import com.openexchange.cluster.map.codec.MapCodec;


/**
 * {@link SimClusterMapService}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class SimClusterMapService implements ClusterMapService {

    private final ConcurrentMap<Key, ClusterMap<?>> maps;

    /**
     * Initializes a new {@link SimClusterMapService}.
     */
    public SimClusterMapService() {
        super();
        maps = new ConcurrentHashMap<>();
    }

    @Override
    public <V> ClusterMap<V> getMap(ApplicationName appName, MapName mapName, MapCodec<V> codec) throws IllegalStateException {
        Key key = new Key(appName, mapName, codec);
        ClusterMap<V> map = (ClusterMap<V>) maps.get(key);
        if (map == null) {
            ClusterMap<V> newMap = new SimClusterMap<>();
            map = (ClusterMap<V>) maps.putIfAbsent(key, newMap);
            if (map == null) {
                map = newMap;
            }
        }
        return map;
    }

    @Override
    public <V> ClusterMap<V> getMap(ApplicationName appName, MapName mapName, MapCodec<V> codec, long expireMillis) {
        return getMap(appName, mapName, codec);
    }

    @Override
    public <V> ClusterMap<V> getMap(ApplicationName appName, MapName mapName, MapCodec<V> codec, long expireMillis, RemoteSiteOptions remoteSiteOptions) {
        return getMap(appName, mapName, codec);
    }

    // --------------------------------------------------------------------------------------

    private static class Key {

        private final ApplicationName appName;
        private final MapName mapName;
        private final MapCodec<?> codec;
        private final int hash;

        /**
         * Initializes a new {@link Key}.
         *
         * @param appName The application name
         * @param mapName The map name
         * @param codec The codec
         */
        Key(ApplicationName appName, MapName mapName, MapCodec<?> codec) {
            super();
            this.appName = appName;
            this.mapName = mapName;
            this.codec = codec;

            int prime = 31;
            int result = 1;
            result = prime * result + ((appName == null) ? 0 : appName.hashCode());
            result = prime * result + ((mapName == null) ? 0 : mapName.hashCode());
            result = prime * result + ((codec == null) ? 0 : codec.hashCode());
            this.hash = result;
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Key other = (Key) obj;
            if (appName == null) {
                if (other.appName != null) {
                    return false;
                }
            } else if (!appName.equals(other.appName)) {
                return false;
            }
            if (mapName == null) {
                if (other.mapName != null) {
                    return false;
                }
            } else if (!mapName.equals(other.mapName)) {
                return false;
            }
            if (codec == null) {
                if (other.codec != null) {
                    return false;
                }
            } else if (!codec.equals(other.codec)) {
                return false;
            }
            return true;
        }
    }

}
