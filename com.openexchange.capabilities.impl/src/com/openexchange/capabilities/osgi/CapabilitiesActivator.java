/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.capabilities.osgi;

import static com.openexchange.capabilities.internal.AbstractCapabilityService.getCapability;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import com.openexchange.capabilities.Capability;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.capabilities.groupware.CapabilityConvertUtf8ToUtf8mb4Task;
import com.openexchange.capabilities.groupware.CapabilityCreateTableService;
import com.openexchange.capabilities.groupware.CapabilityCreateTableTask;
import com.openexchange.capabilities.groupware.CapabilityDeleteListener;
import com.openexchange.capabilities.groupware.MakeNotNullUpdateTask;
import com.openexchange.capabilities.internal.CapabilityServiceImpl;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.context.ContextService;
import com.openexchange.database.CreateTableService;
import com.openexchange.database.DatabaseService;
import com.openexchange.groupware.delete.DeleteListener;
import com.openexchange.groupware.update.DefaultUpdateTaskProviderService;
import com.openexchange.groupware.update.UpdateTaskProviderService;
import com.openexchange.groupware.userconfiguration.PermissionConfigurationChecker;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.osgi.SimpleRegistryListener;
import com.openexchange.serverconfig.ServerConfigService;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.user.UserService;
import com.openexchange.userconf.UserPermissionService;

public class CapabilitiesActivator extends HousekeepingActivator {

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { UserService.class, ContextService.class, ConfigurationService.class, ConfigViewFactory.class, UserPermissionService.class, DatabaseService.class, SessiondService.class };
    }

    @Override
    protected Class<?>[] getOptionalServices() {
        return new Class<?>[] { com.openexchange.cache.v2.CacheService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        final BundleContext context = this.context;

        final CapabilityCheckerRegistry capCheckers = new CapabilityCheckerRegistry(context);
        rememberTracker(capCheckers);

        /*
         * Create & register CapabilityService
         */
        final CapabilityServiceImpl capService = new CapabilityServiceImpl(this, capCheckers);
        registerService(CapabilityService.class, capService);

        track(Capability.class, new SimpleRegistryListener<Capability>() {

            @Override
            public void added(ServiceReference<Capability> ref, Capability capability) {
                getCapability(capability.getId());
            }

            @Override
            public void removed(ServiceReference<Capability> ref, Capability service) {
                // Nothing
            }

        });

        trackService(ServerConfigService.class);
        trackService(PermissionConfigurationChecker.class);

        /*
         * Register update task, create table job and delete listener
         */
        registerService(CreateTableService.class, new CapabilityCreateTableService());
        registerService(UpdateTaskProviderService.class, new DefaultUpdateTaskProviderService(new CapabilityCreateTableTask(), new MakeNotNullUpdateTask(), new CapabilityConvertUtf8ToUtf8mb4Task()));
        registerService(DeleteListener.class, new CapabilityDeleteListener());

        openTrackers();
    }

    @Override
    protected void stopBundle() throws Exception {
        super.stopBundle();
    }

}
