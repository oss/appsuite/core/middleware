/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.capabilities.internal;

import static com.openexchange.java.Strings.isEmpty;
import static com.openexchange.java.Strings.toLowerCase;
import static com.openexchange.marker.OXThreadMarkers.getThreadLocalValue;
import static com.openexchange.marker.OXThreadMarkers.putThreadLocalValue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.openexchange.capabilities.Capability;
import com.openexchange.capabilities.CapabilityChecker;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.capabilities.CapabilitySet;
import com.openexchange.capabilities.ConfigurationProperty;
import com.openexchange.capabilities.DependentCapabilityChecker;
import com.openexchange.capabilities.FailureAwareCapabilityChecker;
import com.openexchange.capabilities.storage.CachingCapabilityStorage;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.cascade.ComposedConfigProperty;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.context.ContextService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.userconfiguration.Permission;
import com.openexchange.groupware.userconfiguration.PermissionConfigurationChecker;
import com.openexchange.groupware.userconfiguration.UserPermissionBits;
import com.openexchange.java.Strings;
import com.openexchange.log.LogProperties;
import com.openexchange.mail.usersetting.UserSettingMail;
import com.openexchange.mail.usersetting.UserSettingMailStorage;
import com.openexchange.reseller.ResellerService;
import com.openexchange.reseller.data.ResellerCapability;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.GeneratedSession;
import com.openexchange.session.Session;
import com.openexchange.share.core.tools.ShareTool;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tools.session.ServerSessionAdapter;
import com.openexchange.user.User;
import com.openexchange.user.UserService;
import com.openexchange.userconf.UserPermissionService;

/**
 * {@link AbstractCapabilityService}
 *
 * @author <a href="mailto:francisco.laguna@open-xchange.com">Francisco Laguna</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public abstract class AbstractCapabilityService implements CapabilityService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(AbstractCapabilityService.class);

    private static final String PERMISSION_PROPERTY = "permissions";

    /** The property name prefix for configured capabilities */
    private static final String PREFIX_CAPABILITY = "com.openexchange.capability.";

    // ------------------------------------------------------------------------------------------------ //

    private static final ConcurrentMap<String, Capability> CAPABILITIES = new ConcurrentHashMap<>(96, 0.9f, 1);

    /** Maps permissions to their corresponding capabilities  */
    private static final Map<Permission, Capability> P2CAPABILITIES = Stream.of(Permission.values())
        .collect(Maps.toImmutableEnumMap(p -> p, p -> getCapability(p.getCapabilityName())));

    /**
     * Gets the singleton capability for given identifier
     *
     * @param id The identifier
     * @return The singleton capability
     */
    public static Capability getCapability(String id) {
        if (null == id) {
            return null;
        }
        ConcurrentMap<String, Capability> capabilities = CAPABILITIES;
        Capability capability = capabilities.get(id);
        if (capability != null) {
            return capability;
        }
        Capability existingCapability = capabilities.putIfAbsent(id, capability = new Capability(id));
        return existingCapability == null ? capability : existingCapability;
    }

    // ------------------------------------------------------------------------------------------------ //

    private final Set<DeclaredCapability> declaredCapabilities;
    private final ServiceLookup services;
    private final CachingCapabilityStorage capabilityStorage;

    /**
     * Initializes a new {@link AbstractCapabilityService}.
     *
     * @param services A service lookup reference
     */
    protected AbstractCapabilityService(ServiceLookup services) {
        super();
        this.services = services;
        this.declaredCapabilities = new ConcurrentSkipListSet<DeclaredCapability>();
        this.capabilityStorage = new CachingCapabilityStorage(services);
    }

    @Override
    public CapabilitySet getCapabilities(int userId, int contextId, boolean allowCache) throws OXException {
        return getCapabilities(userId, contextId, null, allowCache);
    }

    @Override
    public CapabilitySet getCapabilities(Session session) throws OXException {
        return getCapabilities(session.getUserId(), session.getContextId(), session, true);
    }

    /**
     * Gets the capabilities.
     *
     * @param userId The user ID or <code>-1</code>
     * @param contextId The context ID or <code>-1</code>
     * @param session The session; must either belong to the user and context ID or be <code>null</code>
     * @param allowCache Whether the capabilities may be looked up from cache
     * @return
     * @throws OXException
     */
    private CapabilitySetImpl getCapabilities(int userId, int contextId, Session session, boolean allowCache) throws OXException {
        // Try fetch from cache is allowed
        if (allowCache && 0 < contextId && 0 < userId) {
            Optional<CapabilitySetImpl> optCachedCapabilitySet = optThreadLocalCapabilitySet(contextId, userId);
            if (optCachedCapabilitySet.isPresent()) {
                return optCachedCapabilitySet.get();
            }
        }

        // Load or get context, user & permission bits
        Context context = null;
        User user = null;
        UserPermissionBits userPermissionBits = null;
        if (contextId > 0) {
            if (session instanceof ServerSession serverSession) {
                context = serverSession.getContext();
                if (userId > 0) {
                    user = serverSession.getUser();
                    userPermissionBits = serverSession.getUserPermissionBits();
                }
            } else {
                context = services.getServiceSafe(ContextService.class).getContext(contextId);
                if (userId > 0) {
                    user = services.getServiceSafe(UserService.class).getUser(userId, context);
                    userPermissionBits = services.getServiceSafe(UserPermissionService.class).getUserPermissionBits(userId, context);
                }
            }
        }

        /*-
         * Compile set of effective capabilities
         *
         * NOTE: Never ever re-order the apply methods!
         */
        CapabilitySetImpl capabilities = new CapabilitySetImpl(64, LOG.isDebugEnabled());
        Map<Capability, ValueAndScope> forcedCapabilities = HashMap.newHashMap(4);
        applyUserPermissions(capabilities, context, user, userPermissionBits);
        applyConfiguredCapabilities(capabilities, forcedCapabilities, user, context, allowCache);
        if (session == null) {
            applyDeclaredCapabilities(capabilities, ServerSessionAdapter.valueOf(userId, contextId)); // Code smell level: Ninja...
        } else {
            applyDeclaredCapabilities(capabilities, session);
        }
        applyGuestFilter(capabilities, user);
        for (Map.Entry<Capability, ValueAndScope> forcedCapability : forcedCapabilities.entrySet()) {
            Capability capability = forcedCapability.getKey();
            ValueAndScope vas = forcedCapability.getValue();
            if (vas.value.booleanValue()) {
                capabilities.add(capability, CapabilitySource.CONFIGURATION, "Forced through \"com.openexchange.capability.forced." + capability.getId() + "\" with final scope " + vas.scope);
            } else {
                capabilities.remove(capability, CapabilitySource.CONFIGURATION, "Forced through \"com.openexchange.capability.forced." + capability.getId() + "\" with final scope " + vas.scope);
            }
        }

        if (allowCache && 0 < contextId && 0 < userId) {
            // Put in cache
            putCapabilitySetInCache(contextId, userId, capabilities);
        }

        if (LOG.isDebugEnabled()) {
            capabilities.printHistoryFor(userId, contextId, LOG);
        }

        return capabilities;
    }

    /**
     * Adds the capabilities that represent user permissions to the passed set, if a valid user and context are given.
     *
     * @param capabilities The capability set
     * @param context The context; if <code>null</code>, calling this method is a no-op
     * @param user The user; if <code>null</code>, calling this method is a no-op
     * @param userPermissionBits The user permission bits; if <code>null</code>, calling this method is a no-op
     */
    private void applyUserPermissions(CapabilitySetImpl capabilities, Context context, User user, UserPermissionBits userPermissionBits) {
        if (null == context || null == userPermissionBits || null == user) {
            return;
        }

        // Capabilities by user permission bits
        for (Permission p : Permission.byBits(userPermissionBits.getPermissionBits())) {
            capabilities.add(P2CAPABILITIES.get(p), CapabilitySource.PERMISSIONS);
        }

        // Webmail
        if (user.getId() == context.getMailadmin()) {
            Capability webmailCapability = P2CAPABILITIES.get(Permission.WEBMAIL);
            if (capabilities.contains(webmailCapability) && false == services.getService(ConfigurationService.class).getBoolProperty("com.openexchange.mail.adminMailLoginEnabled", false)) {
                capabilities.remove(webmailCapability, CapabilitySource.PERMISSIONS, "Through property \"com.openexchange.mail.adminMailLoginEnabled\"");
            }
        }

        // Portal - stick to positive "portal" capability only
        if (false == capabilities.remove("denied_portal")) {
            capabilities.add(getCapability("portal"), CapabilitySource.PERMISSIONS);
        }
        // Free-Busy
        if (userPermissionBits.hasFreeBusy()) {
            capabilities.add(getCapability("freebusy"), CapabilitySource.PERMISSIONS);
        }
        // Conflict-Handling
        if (userPermissionBits.hasConflictHandling()) {
            capabilities.add(getCapability("conflict_handling"), CapabilitySource.PERMISSIONS);
        }
        // Participants-Dialog
        if (userPermissionBits.hasParticipantsDialog()) {
            capabilities.add(getCapability("participants_dialog"), CapabilitySource.PERMISSIONS);
        }
        // Group-ware
        if (userPermissionBits.hasGroupware()) {
            capabilities.add(getCapability("groupware"), CapabilitySource.PERMISSIONS);
        }
        // PIM
        if (userPermissionBits.hasPIM()) {
            capabilities.add(getCapability("pim"), CapabilitySource.PERMISSIONS);
        }
        // Spam
        if (userPermissionBits.hasWebMail()) {
            UserSettingMail mailSettings = UserSettingMailStorage.getInstance().getUserSettingMail(user.getId(), context);
            if (null != mailSettings && mailSettings.isSpamEnabled()) {
                capabilities.add(getCapability("spam"), CapabilitySource.PERMISSIONS);
            }
        }
        // Global Address Book
        if (userPermissionBits.isGlobalAddressBookEnabled()) {
            capabilities.add(getCapability("gab"), CapabilitySource.PERMISSIONS);
        }
    }

    /**
     * Adds all capabilities that have been specified via any mechanism of configuration to the passed set. Such capabilities are:
     *
     * <ul>
     * <li>capabilities specified via the <code>permissions</code> property - looked up via config cascade</li>
     * <li>capabilities specified as properties with a <code>com.openexchange.capability.</code> prefix - looked up via config cascade</li>
     * <li>reseller-specific capabilities contained in the subadmin_capabilities database table</li>
     * <li>context-specific capabilities contained in the context_capabilities database table</li>
     * <li>user-specific capabilities contained in the user_capabilities database table</li>
     * </ul>
     *
     * @param capabilities The capability set
     * @param forcedCapabilities The forcibly configured capabilities
     * @param user The user; if <code>null</code>, all config cascade lookups are performed with user ID <code>-1</code>;
     *            if the user is a guest, the configure guest capability mode is considered.
     * @param context The context; if <code>null</code>, all config cascade lookups are performed with context ID <code>-1</code>
     * @param allowCache Whether caching of loaded capabilities is allowed
     * @throws OXException
     */
    private void applyConfiguredCapabilities(CapabilitySetImpl capabilities, Map<Capability, ValueAndScope> forcedCapabilities, User user, Context context, boolean allowCache) throws OXException {
        final int contextId = context == null ? -1 : context.getContextId();
        final int userId = user == null ? -1 : user.getId();
        FilteringCapabilities filteredCaps = new FilteringCapabilities(forcedCapabilities, capabilities, getFilterPredicate(contextId, userId));
        if (user == null) {
            applyConfigCascade(filteredCaps, userId, contextId);
            applyResellerCapabilities(filteredCaps, contextId);
            applyContextCapabilities(filteredCaps, contextId, allowCache);
            return;
        }

        if (user.isGuest()) {
            applyGuestCapabilities(filteredCaps, capabilities, user, context, allowCache);
            return;
        }

        // Normal user
        applyConfigCascade(filteredCaps, userId, contextId);
        applyResellerCapabilities(filteredCaps, contextId);
        applyContextAndUserCapabilities(filteredCaps, contextId, userId, allowCache);
    }

    /**
     * Applies the guest capabilities based on the guest capability mode
     *
     * @param filteredCaps The filtered capabilities to add to
     * @param capabilities The capabilities to add to
     * @param user The user
     * @param context The context
     * @param allowCache Whether caching of loaded capabilities is allowed
     * @throws OXException
     */
    private void applyGuestCapabilities(FilteringCapabilities filteredCaps, CapabilitySetImpl capabilities, User user, Context context, boolean allowCache) throws OXException {
        int userId = user.getId();
        int contextId = context.getContextId();
        GuestCapabilityMode capMode = getGuestCapabilityMode(user, context);
        switch (capMode) {
            case DENY_ALL:
                applyUserCapabilities(filteredCaps, userId, contextId, allowCache);
                return;
            case INHERIT:
                applyConfigCascade(filteredCaps, user.getCreatedBy(), contextId);
                applyResellerCapabilities(filteredCaps, contextId);
                applyContextAndUserCapabilities(filteredCaps, contextId, user.getCreatedBy(), allowCache);
                applyUserCapabilities(filteredCaps, userId, contextId, allowCache);
                return;
            case STATIC:
            default:
                applyStaticGuestCapabilities(capabilities, user, context);
                applyUserCapabilities(filteredCaps, userId, contextId, allowCache);
        }
    }

    /**
     * Checks whether the capability should be applied or not
     *
     * @param capName The name of the capability to check
     * @param userId The user identifier or <code>-1</code>
     * @param ctxId The context identifier or <code>-1</code>
     * @return <code>true</code> if the capability should be applied, <code>false</code> otherwise
     */
    private final boolean isLegal(String capName, int userId, int ctxId) {
        try {
            return services.getServiceSafe(PermissionConfigurationChecker.class).isLegal(capName, userId, ctxId);
        } catch (OXException e) {
            LOG.error("", e);
            return true;
        }
    }

    /**
     * Prepares a function to check whether the capability should be applied or not.
     *
     * @param userId The user identifier or <code>-1</code>
     * @param contextId The context identifier or <code>-1</code>
     * @return The checker function
     */
    private Predicate<String> getFilterPredicate(int contextId, int userId) {
        PermissionConfigurationChecker configurationChecker;
        try {
            configurationChecker = services.getServiceSafe(PermissionConfigurationChecker.class);
        } catch (OXException e) {
            LOG.error("No permission configuration service available, cannot filter illegal capabilities.", e);
            return capName -> true;
        }
        return capName -> configurationChecker.isLegal(capName, userId, contextId);
    }

    /**
     * Adds all capabilities that have been specified via config-cascade properties to the passed set.
     *
     * <ul>
     * <li>capabilities specified via the <code>permissions</code> property - looked up via config-cascade</li>
     * <li>capabilities specified as properties with a <code>com.openexchange.capability.</code> prefix - looked up via config-cascade</li>
     * </ul>
     *
     * @param capabilities The {@link FilteringCapabilities}
     * @param userId The user ID for config-cascade lookups
     * @param contextId The context ID for config-cascade lookups
     * @throws OXException
     */
    private void applyConfigCascade(FilteringCapabilities capabilities, int userId, int contextId) throws OXException {
        // Permission properties
        ConfigViewFactory configViews = services.getService(ConfigViewFactory.class);
        if (configViews == null) {
            return;
        }
        ConfigView view = configViews.getView(userId, contextId);
        /*
         * check special "permissions" property within all scopes
         */
        ComposedConfigProperty<String> permissionProperty = view.property(PERMISSION_PROPERTY);
        for (String scope : configViews.getSearchPath()) {
            String permissions = permissionProperty.precedence(scope).get();
            if (permissions == null) {
                continue;
            }
            for (String permissionModifier : Strings.splitByComma(permissions)) {
                applyCapability(capabilities, permissionModifier, CapabilitySource.CONFIGURATION, "Through \"permissions\" property in file 'permissions.properties'");
            }
        }

        Map<String, ComposedConfigProperty<String>> all = view.all(PREFIX_CAPABILITY);
        for (Map.Entry<String, ComposedConfigProperty<String>> entry : all.entrySet()) {
            String propName = entry.getKey();
            ComposedConfigProperty<String> configProperty = entry.getValue();
            /*
             * don't apply undefined capabilities
             */
            String value = configProperty.get();
            if (value == null) {
                LOG.debug("Ignoring undefined capability property for user {} in context {}: {}", Integer.valueOf(userId), Integer.valueOf(contextId), propName);
                continue;
            }
            /*
             * apply capability
             */
            String name = toLowerCase(propName.substring(28));
            String scope = configProperty.getScope();
            if (name.startsWith("forced.", 0)) {
                name = name.substring(7);
                capabilities.addForced(getCapability(name), new ValueAndScope(Boolean.valueOf(value), scope));
            } else {
                if (Boolean.parseBoolean(value)) {
                    capabilities.add(getCapability(name), CapabilitySource.CONFIGURATION, "scope: " + scope);
                } else {
                    capabilities.remove(name, CapabilitySource.CONFIGURATION, "scope: " + scope);
                }
            }
        }
    }

    /**
     * Adds all capabilities that are specified as <code>reseller_capabilities</code> DB entries to the passed set.
     *
     * @param capabilities The {@link FilteringCapabilities}
     * @param contextId The context ID; if negative calling this method is a no-op
     * @param allowCache Whether caching of loaded capabilities is allowed
     * @throws OXException
     */
    private void applyResellerCapabilities(FilteringCapabilities capabilities, int contextId) throws OXException {
        if (contextId <= 0) {
            return;
        }
        ResellerService resellerService = services.getOptionalService(ResellerService.class);
        if (resellerService == null) {
            return;
        }
        if (false == resellerService.isEnabled()) {
            return;
        }
        applySet(capabilities, getResellerCaps(contextId).stream().map(r -> r.getId()).collect(Collectors.toSet()), CapabilitySource.RESELLER);
    }

    /**
     * Adds all capabilities that are specified as <code>context_capabilities</code> DB entries to the passed set.
     *
     * @param capabilities The {@link FilteringCapabilities}
     * @param contextId The context ID; if negative calling this method is a no-op
     * @param userId The user ID; if negative calling this method is a no-op
     * @param allowCache Whether caching of loaded capabilities is allowed
     * @throws OXException If getting capabilities fails
     */
    private void applyContextAndUserCapabilities(FilteringCapabilities capabilities, int contextId, int userId, boolean allowCache) throws OXException {
        if (allowCache && contextId > 0 && userId > 0) {
            capabilityStorage.getContextAndUserCapabilities(contextId, userId).entrySet().forEach(e -> applySet(capabilities, e.getValue(), e.getKey()));
        } else {
            applyContextCapabilities(capabilities, contextId, allowCache);
            applyUserCapabilities(capabilities, userId, contextId, allowCache);
        }
    }

    /**
     * Adds all capabilities that are specified as <code>context_capabilities</code> DB entries to the passed set.
     *
     * @param capabilities The {@link FilteringCapabilities}
     * @param contextId The context ID; if negative calling this method is a no-op
     * @param allowCache Whether caching of loaded capabilities is allowed
     * @throws OXException
     */
    private void applyContextCapabilities(FilteringCapabilities capabilities, int contextId, boolean allowCache) throws OXException {
        if (contextId > 0) {
            Set<String> contextCapabilities = allowCache ? capabilityStorage.getCapabilities(contextId) : capabilityStorage.loadCapabilities(contextId);
            applySet(capabilities, contextCapabilities, CapabilitySource.CONTEXT);
        }
    }

    /**
     * Adds all capabilities that are specified as <code>user_capabilities</code> DB entries to the passed set.
     *
     * @param capabilities The {@link FilteringCapabilities}
     * @param userId The user ID; if negative calling this method is a no-op
     * @param contextId The context ID; if negative calling this method is a no-op
     * @param allowCache Whether caching of loaded capabilities is allowed
     * @throws OXException
     */
    private void applyUserCapabilities(FilteringCapabilities capabilities, int userId, int contextId, boolean allowCache) throws OXException {
        if (contextId > 0 && userId > 0) {
            Set<String> userCapabilities = allowCache ? capabilityStorage.getCapabilities(contextId, userId) : capabilityStorage.loadCapabilities(contextId, userId);
            applySet(capabilities, userCapabilities, CapabilitySource.USER);
        }
    }

    /**
     * Applies the given capabilities to the {@link FilteringCapabilities} set
     *
     * @param capabilities The {@link FilteringCapabilities}
     * @param capabilitiesToApply The capabilities to apply
     */
    private static void applySet(FilteringCapabilities capabilities, Set<String> capabilitiesToApply, CapabilitySource source) {
        for (String sCap : capabilitiesToApply) {
            applyCapability(capabilities, sCap, source, null);
        }
    }

    /**
     * Applies a capability parsed from a given permission modifier string to the given filtering capability set.
     *
     * @param capabilities The filtering capability set to apply the permission modifier
     * @param permissionModifier The permission modifier
     * @param source The capability source
     * @param optReason An optional reason
     */
    private static void applyCapability(FilteringCapabilities capabilities, String permissionModifier, CapabilitySource source, String optReason) {
        if (Strings.isEmpty(permissionModifier)) {
            return;
        }
        char firstChar = permissionModifier.charAt(0);
        if ('-' == firstChar) {
            capabilities.remove(permissionModifier.substring(1), source, optReason);
        } else {
            String name = '+' == firstChar ? permissionModifier.substring(1) : permissionModifier;
            capabilities.add(getCapability(name), source, permissionModifier);
        }
    }

    /**
     * Adds all capabilities to the passed set, which are specified via {@link CapabilityService#declareCapability(String)}.
     * Every declared capability is checked against the registered {@link CapabilityChecker}s.
     *
     * @param capabilities The capability set
     * @param session The session; either a real one or a synthetic one, but never <code>null</code>
     */
    private void applyDeclaredCapabilities(CapabilitySetImpl capabilities, Session session) {
        for (DeclaredCapability declaredCapability : declaredCapabilities) {
            if (check(declaredCapability.getId(), session, capabilities)) {
                capabilities.add(declaredCapability.getCapability(), CapabilitySource.DECLARED);
            } else {
                capabilities.remove(declaredCapability.getCapability(), CapabilitySource.DECLARED);
            }
        }
    }

    /**
     * If the passed user is a guest user, the <code>guest</code> capability is set. Additionally
     * the capabilities <code>share_links</code> and <code>invite_guests</code> are removed.
     *
     * @param capabilities The capability set
     * @param user The user; if <code>null</code>, calling this method is a no-op
     */
    private void applyGuestFilter(CapabilitySetImpl capabilities, User user) {
        if (user == null) {
            return;
        }

        if (user.isGuest()) {
            capabilities.add(getCapability("guest"));
            if (ShareTool.isAnonymousGuest(user)) {
                capabilities.add(getCapability("anonymous"));
            }
            capabilities.remove("share_links");
            capabilities.remove("invite_guests");
            if (Strings.isNotEmpty(user.getMail())) {
                capabilities.add(getCapability("edit_password"));
            }
            capabilities.remove("mailfilter_v2");
            capabilities.remove("mailfilter");
        }
    }

    /**
     * Applies the statically configured capabilities to the passed capability set.
     *
     * @param capabilities The capability set
     * @param user The guest user
     * @param context The context
     * @throws OXException
     */
    private void applyStaticGuestCapabilities(CapabilitySetImpl capabilities, User user, Context context) throws OXException {
        ConfigViewFactory configViews = services.getService(ConfigViewFactory.class);
        if (configViews == null) {
            return;
        }
        ConfigView view = configViews.getView(user.getId(), context.getContextId());
        String propertyName = "com.openexchange.share.staticGuestCapabilities";
        ComposedConfigProperty<String> property = view.property(propertyName);
        String value = property.get();
        if (Strings.isEmpty(value)) {
            return;
        }
        List<String> staticCapabilities = Strings.splitAndTrim(value, ",");
        for (String cap : staticCapabilities) {
            capabilities.add(getCapability(cap), CapabilitySource.CONFIGURATION, "Through property \"com.openexchange.share.staticGuestCapabilities\"");
        }
    }

    /**
     * Gets the capability mode for the given guest user.
     *
     * @param user The guest user
     * @param context The context
     * @return The mode
     * @throws OXException
     */
    private GuestCapabilityMode getGuestCapabilityMode(User user, Context context) throws OXException {
        ConfigViewFactory configViews = services.getService(ConfigViewFactory.class);
        if (configViews == null) {
            return GuestCapabilityMode.STATIC;
        }
        ConfigView view = configViews.getView(user.getId(), context.getContextId());
        String value = view.opt("com.openexchange.share.guestCapabilityMode", String.class, "static");
        for (GuestCapabilityMode mode : GuestCapabilityMode.values()) {
            if (mode.name().toLowerCase().equals(value)) {
                return mode;
            }
        }

        return GuestCapabilityMode.STATIC;
    }

    private static enum GuestCapabilityMode {
        DENY_ALL, STATIC, INHERIT;
    }

    private boolean check(String cap, Session session, CapabilitySet allCapabilities) {
        Map<String, List<CapabilityChecker>> checkers = getCheckers();
        return performCapabilityCheck(checkers.get(cap.toLowerCase()), cap, session, allCapabilities) &&
            performCapabilityCheck(checkers.get("*"), cap, session, allCapabilities);
    }

    private static boolean performCapabilityCheck(List<CapabilityChecker> checkers, String cap, Session session, CapabilitySet allCapabilities) {
        if (null != checkers && false == checkers.isEmpty()) {
            for (CapabilityChecker checker : checkers) {
                if (false == performCapabilityCheck(checker, cap, session, allCapabilities)) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean performCapabilityCheck(CapabilityChecker checker, String cap, Session session, CapabilitySet allCapabilities) {
        try {
            if (checker instanceof DependentCapabilityChecker dependentChecker) {
                return dependentChecker.isEnabled(cap, session, allCapabilities);
            }
            if (checker instanceof FailureAwareCapabilityChecker failureAwareChecker) {
                return switch (failureAwareChecker.checkEnabled(cap, session)) {
                    case DISABLED, FAILURE -> false;
                    case ENABLED -> true;
                    default -> throw new IllegalArgumentException("Unexpected value: " + failureAwareChecker.checkEnabled(cap, session));
                };
            }
            return checker.isEnabled(cap, session);
        } catch (Exception e) {
            LOG.warn("Could not check availability for capability '{}'. Assuming as absent this time.", cap, e);
            return false;
        }
    }

    @Override
    public boolean declareCapability(String capability, int ranking) {
        return declaredCapabilities.add(new DeclaredCapability(capability, ranking));
    }

    @Override
    public boolean undeclareCapability(String capability) {
        return declaredCapabilities.removeIf(r -> r.getId().equals(capability));
    }

    @Override
    public List<ConfigurationProperty> getConfigurationSource(int userId, int contextId, String searchPattern) throws OXException {
        ConfigViewFactory configViews = services.getService(ConfigViewFactory.class);
        if (configViews == null) {
            return Collections.emptyList();
        }

        ConfigView view = configViews.getView(userId, contextId);
        if (view == null) {
            return Collections.emptyList();
        }

        Map<String, ComposedConfigProperty<String>> all = view.all();
        List<ConfigurationProperty> properties = new ArrayList<>(all.size());
        for (Map.Entry<String, ComposedConfigProperty<String>> entry : all.entrySet()) {
            String key = entry.getKey();
            if (StringUtils.containsIgnoreCase(key, searchPattern)) {
                ComposedConfigProperty<String> property = entry.getValue();
                String value = property.get();
                if ((property.getScope() == null) && (value == null)) {
                    LOG.info("Scope and value for property '{}' are null. Going to ignore it", key);
                } else {
                    List<String> metadataNames = property.getMetadataNames();
                    if (metadataNames.isEmpty()) {
                        properties.add(new ConfigurationProperty(property.getScope(), key, value, ImmutableMap.of()));
                    } else {
                        ImmutableMap.Builder<String, String> metadata = ImmutableMap.builderWithExpectedSize(metadataNames.size());
                        for (String metadataName : property.getMetadataNames()) {
                            metadata.put(metadataName, property.get(metadataName));
                        }
                        properties.add(new ConfigurationProperty(property.getScope(), key, value, metadata.build()));
                    }
                }
            }
        }
        return properties;
    }

    @Override
    public Map<String, Map<String, Set<String>>> getCapabilitiesSource(int userId, int contextId) throws OXException {
        if (userId <= 0 || contextId <= 0) {
            return doGetCapabilitiesSource(userId, contextId);
        }

        // User and context identifier available
        LogProperties.putUserProperties(userId, contextId);
        try {
            return doGetCapabilitiesSource(userId, contextId);
        } finally {
            LogProperties.removeUserProperties();
        }
    }

    private Map<String, Map<String, Set<String>>> doGetCapabilitiesSource(int userId, int contextId) throws OXException {
        if (userId > 0 && contextId > 0 && LOG.isDebugEnabled()) {
            getCapabilities(userId, contextId, null, false).printHistoryFor(userId, contextId, LOG);
        }

        Map<String, Map<String, Set<String>>> sets = LinkedHashMap.newLinkedHashMap(6);

        /*
         * Add capabilities based on user permissions
         */
        {
            User user = null;
            Context context = null;
            UserPermissionBits userPermissionBits = null;
            if (contextId > 0) {
                context = services.getServiceSafe(ContextService.class).getContext(contextId);
                if (userId > 0) {
                    user = services.getServiceSafe(UserService.class).getUser(userId, context);
                    userPermissionBits = services.getServiceSafe(UserPermissionService.class).getUserPermissionBits(userId, context);
                }
            }

            Set<String> capabilities = new TreeSet<>();
            CapabilitySetImpl capabilitySet = new CapabilitySetImpl(64);
            applyUserPermissions(capabilitySet, context, user, userPermissionBits);
            for (Iterator<Capability> iterator = capabilitySet.iterator(); iterator.hasNext();) {
                Capability capability = iterator.next();
                if (null != capability) {
                    capabilities.add(capability.getId());
                }
            }
            Map<String, Set<String>> arr = LinkedHashMap.newLinkedHashMap(3);
            arr.put("granted", capabilities);
            arr.put("denied", HashSet.newHashSet(0));
            sets.put("permissions", arr);
        }

        {
            CapabilitySetImpl grantedCapabilities = new CapabilitySetImpl(16);
            CapabilitySetImpl deniedCapabilities = new CapabilitySetImpl(16);
            final ConfigViewFactory configViews = services.getService(ConfigViewFactory.class);
            if (configViews != null) {
                final ConfigView view = configViews.getView(userId, contextId);
                final String property = PERMISSION_PROPERTY;
                for (final String scope : configViews.getSearchPath()) {
                    final String permissions = view.property(property, String.class).precedence(scope).get();
                    if (permissions != null) {
                        for (String permissionModifier : Strings.splitByComma(permissions)) {
                            if (!isEmpty(permissionModifier)) {
                                permissionModifier = permissionModifier.trim();
                                final char firstChar = permissionModifier.charAt(0);
                                if ('-' == firstChar) {
                                    String name = permissionModifier.substring(1);
                                    if (isLegal(name, userId, contextId) == false) {
                                        continue;
                                    }
                                    deniedCapabilities.add(getCapability(name));
                                } else {
                                    String name = ('+' == firstChar) ? permissionModifier.substring(1) : permissionModifier;
                                    if (isLegal(name, userId, contextId) == false) {
                                        continue;
                                    }
                                    grantedCapabilities.add(getCapability(name));
                                }
                            }
                        }
                    }
                }

                final Map<String, ComposedConfigProperty<String>> all = view.all(PREFIX_CAPABILITY);
                for (Map.Entry<String, ComposedConfigProperty<String>> entry : all.entrySet()) {
                    final String propName = entry.getKey();
                    ComposedConfigProperty<String> configProperty = entry.getValue();
                    String bValue = configProperty.get();
                    if (bValue == null) {
                        LOG.debug("Ignoring undefined capability property for user {} in context {}: {}", Integer.valueOf(userId), Integer.valueOf(contextId), propName);
                        continue;
                    }
                    boolean value = Boolean.parseBoolean(bValue);
                    String name = toLowerCase(propName.substring(28));
                    if (isLegal(name, userId, contextId) == false) {
                        continue;
                    }
                    if (value) {
                        grantedCapabilities.add(getCapability(name));
                    } else {
                        deniedCapabilities.add(getCapability(name));
                    }
                }

                Map<String, Set<String>> arr = LinkedHashMap.newLinkedHashMap(3);
                {
                    Set<String> set = new TreeSet<>();
                    for (Capability cap : grantedCapabilities) {
                        set.add(cap.getId());
                    }
                    arr.put("granted", set);
                }
                {
                    Set<String> set = new TreeSet<>();
                    for (Capability cap : deniedCapabilities) {
                        set.add(cap.getId());
                    }
                    arr.put("denied", set);
                }
                sets.put("configuration", arr);
            }
        }

        {
            if (contextId > 0) {
                Set<String> set = new HashSet<>();
                Set<String> removees = new HashSet<>();

                // Reseller-sensitive
                for (ResellerCapability resellerCapability : getResellerCaps(contextId)) {
                    if (null == resellerCapability) {
                        continue;
                    }
                    String sCap = resellerCapability.getId();
                    if (isEmpty(sCap)) {
                        continue;
                    }
                    char firstChar = sCap.charAt(0);
                    if ('-' == firstChar) {
                        String name = toLowerCase(sCap.substring(1));
                        if (isLegal(name, userId, contextId) == false) {
                            continue;
                        }
                        set.remove(name);
                        removees.add(concatResellerId(name, resellerCapability.getResellerId()));
                    } else {
                        String name = ('+' == firstChar) ? sCap.substring(1) : sCap;
                        if (isLegal(name, userId, contextId) == false) {
                            continue;
                        }
                        set.add(concatResellerId(name, resellerCapability.getResellerId()));
                    }
                }

                // Context-sensitive
                for (final String sCap : capabilityStorage.loadCapabilities(contextId)) {
                    if (!isEmpty(sCap)) {
                        final char firstChar = sCap.charAt(0);
                        if ('-' == firstChar) {
                            final String name = toLowerCase(sCap.substring(1));
                            if (isLegal(name, userId, contextId) == false) {
                                continue;
                            }
                            set.remove(name);
                            removees.add(name);
                        } else {
                            String name = ('+' == firstChar) ? sCap.substring(1) : sCap;
                            if (isLegal(name, userId, contextId) == false) {
                                continue;
                            }
                            set.add(name);
                        }
                    }
                }
                // User-sensitive
                if (userId > 0) {
                    for (final String sCap : capabilityStorage.loadCapabilities(contextId, userId)) {
                        if (!isEmpty(sCap)) {
                            final char firstChar = sCap.charAt(0);
                            if ('-' == firstChar) {
                                final String name = toLowerCase(sCap.substring(1));
                                if (isLegal(name, userId, contextId) == false) {
                                    continue;
                                }
                                set.remove(name);
                                removees.add(name);
                            } else {
                                String name = ('+' == firstChar) ? sCap.substring(1) : sCap;
                                if (isLegal(name, userId, contextId) == false) {
                                    continue;
                                }
                                set.add(name);
                                removees.remove(name);
                            }
                        }
                    }
                }

                // Merge them into result set
                Map<String, Set<String>> arr = LinkedHashMap.newLinkedHashMap(3);
                arr.put("granted", set);
                arr.put("denied", removees);
                sets.put("provisioning", arr);
            }
        }

        {
            // Now the declared ones
            CapabilitySetImpl grantedCapabilities = new CapabilitySetImpl(16);
            Session fakeSession = new GeneratedSession(userId, contextId);
            for (DeclaredCapability declaredCapability : declaredCapabilities) {
                if (check(declaredCapability.getId(), fakeSession, grantedCapabilities)) {
                    grantedCapabilities.add(declaredCapability.getCapability());
                }
            }

            Map<String, Set<String>> arr = LinkedHashMap.newLinkedHashMap(3);
            {
                Set<String> set = new TreeSet<>();
                for (Capability cap : grantedCapabilities) {
                    set.add(cap.getId());
                }
                arr.put("granted", set);
            }
            arr.put("denied", HashSet.newHashSet(0));
            sets.put("programmatic", arr);
        }

        return sets;
    }

    /**
     * Concatenates the reseller id
     *
     * @param name The cap/prop/tax name
     * @param resellerId The reseller id
     * @return The concatenated string
     */
    private String concatResellerId(String name, int resellerId) {
        return name + " (ResellerId:" + resellerId + ")";
    }

    /**
     * Gets the available capability checkers.
     *
     * @return The checkers
     */
    protected abstract Map<String, List<CapabilityChecker>> getCheckers();

    /**
     * Loads the reseller capabilities for the specified context.
     *
     * @param contextId The context identifier
     * @return A Set with all reseller capabilities.
     * @throws OXException
     */
    private Set<ResellerCapability> getResellerCaps(int contextId) throws OXException {
        if (contextId <= 0) {
            return ImmutableSet.of();
        }
        ResellerService resellerService = services.getOptionalService(ResellerService.class);
        if (resellerService == null) {
            return ImmutableSet.of();
        }
        return resellerService.getCapabilitiesByContext(contextId);
    }

    /**
     * Optionally gets a previously evaluated capability set from the thread-local cache.
     *
     * @param contextId The context identifier
     * @param userId The user identifier
     * @return The cached capability set, or {@link Optional#empty()} in case there is none
     */
    private static Optional<CapabilitySetImpl> optThreadLocalCapabilitySet(int contextId, int userId) {
        if (0 >= contextId || 0 >= userId) {
            return Optional.empty();
        }
        return Optional.ofNullable((CapabilitySetImpl) getThreadLocalValue(getKey(contextId, userId)));
    }

    /**
     * Caches a previously evaluated capability set as thread-local value.
     *
     * @param contextId The context identifier
     * @param userId The user identifier
     * @param value The capability set to put into the cache
     */
    private static void putCapabilitySetInCache(int contextId, int userId, CapabilitySetImpl value) {
        if (0 < contextId && 0 < userId) {
            putThreadLocalValue(getKey(contextId, userId), value);
        }
    }

    /**
     * Constructs a key to store and retrieve a previously calculated capability set to/from a thread-local value.
     *
     * @param contextId The context identifier
     * @param userId The user identifier
     * @return The key
     */
    private static String getKey(int contextId, int userId) {
        return CapabilitySetImpl.class.getName() + ':' + contextId + ':' + userId;
    }

}
