/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.capabilities.internal;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import com.openexchange.capabilities.Capability;
import com.openexchange.capabilities.CapabilitySet;

/**
 * {@link DefaultCapabilitySet} - A capability set.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class DefaultCapabilitySet implements CapabilitySet {

    private static final long serialVersionUID = 5308759077364925549L;

    /** The capability map */
    private final Map<String, Capability> capabilities;

    /**
     * Initializes a new {@link DefaultCapabilitySet}.
     *
     * @param capacity The initial capacity
     */
    public DefaultCapabilitySet(int capacity) {
        super();
        capabilities = new ConcurrentHashMap<String, Capability>(capacity);
    }

    protected DefaultCapabilitySet(DefaultCapabilitySet source) {
        super();
        Map<String, Capability> m = source.capabilities;
        capabilities = null == m ? null : new ConcurrentHashMap<String, Capability>(m);
    }

    @Override
    public DefaultCapabilitySet clone() {
        return new DefaultCapabilitySet(this);
    }

    @Override
    public int size() {
        return capabilities.size();
    }

    @Override
    public boolean isEmpty() {
        return capabilities.isEmpty();
    }

    @Override
    public boolean contains(Capability capability) {
        return null == capability ? false : capabilities.containsKey(capability.getId());
    }

    @Override
    public boolean contains(String id) {
        return null == id ? false : capabilities.containsKey(id);
    }

    @Override
    public Capability get(String id) {
        return null == id ? null : capabilities.get(id);
    }

    @Override
    public Iterator<Capability> iterator() {
        return capabilities.values().iterator();
    }

    @Override
    public boolean add(Capability capability) {
        return null != capability && null == capabilities.put(capability.getId(), capability);
    }

    @Override
    public boolean remove(Capability capability) {
        return null != capability && remove(capability.getId());
    }

    @Override
    public boolean remove(String id) {
        return null != id && null != capabilities.remove(id);
    }

    @Override
    public void clear() {
        capabilities.clear();
    }

    @Override
    public Set<Capability> asSet() {
        return new HashSet<Capability>(capabilities.values());
    }

    @Override
    public String toString() {
        return new TreeSet<Capability>(capabilities.values()).toString();
    }

}
