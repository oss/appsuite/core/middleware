/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.capabilities.internal;

import com.openexchange.capabilities.Capability;

/**
 * {@link DeclaredCapability} - Capability along with an optional ranking determining the order of capability checks.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class DeclaredCapability implements Comparable<DeclaredCapability> {
    
    private final Capability capability;
    private final int ranking;
    
    /**
     * Initializes a new {@link DeclaredCapability}.
     * 
     * @param capability The id of the declared capability
     * @param ranking The ranking via which the capability is registered
     */
    public DeclaredCapability(String capability, int ranking) {
        this(AbstractCapabilityService.getCapability(capability), ranking);
    }

    /**
     * Initializes a new {@link DeclaredCapability}.
     * 
     * @param capability The capability
     * @param ranking The ranking via which the capability is registered
     */
    public DeclaredCapability(Capability capability, int ranking) {
        super();
        this.capability = capability;
        this.ranking = ranking;
    }

    /**
     * Gets this capability's identifier
     *
     * @return The identifier
     */
    public String getId() {
        return capability.getId();
    }

    /**
     * Gets the underlying capability.
     * 
     * @return The capability
     */
    public Capability getCapability() {
        return capability;
    }

    @Override
    public int hashCode() {
        return capability.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DeclaredCapability other = (DeclaredCapability) obj;
        if (capability == null) {
            if (other.capability != null)
                return false;
        } else if (!capability.equals(other.capability))
            return false;
        return true;
    }

    @Override
    public int compareTo(DeclaredCapability other) {
        int result = Integer.compare(ranking, other.ranking);
        if (0 != result) {
            return result;
        }
        return capability.compareTo(other.capability);
    }

    @Override
    public String toString() {
        return "DeclaredCapability [capability=" + capability + ", ranking=" + ranking + "]";
    }

}
