/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.capabilities.storage;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheKeyValue;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.capabilities.CapabilityExceptionCodes;
import com.openexchange.capabilities.internal.CapabilitySource;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.server.ServiceLookup;

/**
 * {@link CachingCapabilityStorage}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class CachingCapabilityStorage {

    /** Options for the cache holding capabilities per context-/user id */
    private static final CacheOptions<Set<String>> CACHE_OPTIONS_CAPABILITIES = CacheOptions.<Set<String>> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.CAPABILITIES)
        .withCodecAndVersion(new StringSetCodec())
    .build(); // @formatter:on

    private final ServiceLookup services;

    /**
     * Initializes a new {@link CachingCapabilityStorage}.
     *
     * @param services A service lookup reference
     */
    public CachingCapabilityStorage(ServiceLookup services) {
        super();
        this.services = services;
    }

    /**
     * Gets or loads the capabilities for a certain context and a user in this context
     * 
     * @param contextId The context identifier
     * @param userId The identifier of the user to get the capabilities for
     * @return The context and user capabilities, mapped by {@link CapabilitySource#CONTEXT} and {@link CapabilitySource#USER}
     * @throws OXException If getting capabilities fails
     */
    public Map<CapabilitySource, Set<String>> getContextAndUserCapabilities(int contextId, int userId) throws OXException {
        Optional<Cache<Set<String>>> optCache = optCache();
        if (optCache.isEmpty()) {
            return Map.of(CapabilitySource.CONTEXT, loadCapabilities(contextId), CapabilitySource.USER, loadCapabilities(contextId, userId));
        }
        Cache<Set<String>> cache = optCache.get();
        List<CacheKeyValue<Set<String>>> cacheKeyValues = cache.mget(getCacheKey(cache, contextId), getCacheKey(cache, contextId, userId));
        if (2 != cacheKeyValues.size()) {
            throw OXException.general("Unexpected result for mget operation");
        }
        Set<String> contextCapabilities;
        Set<String> userCapabilities;
        Map<CacheKey, Set<String>> loadedValues = null;
        if (cacheKeyValues.get(0).hasValue()) {
            contextCapabilities = cacheKeyValues.get(0).getValue();
        } else {
            contextCapabilities = loadCapabilities(contextId);
            loadedValues = HashMap.newHashMap(2);
            loadedValues.put(cacheKeyValues.get(0).getKey(), contextCapabilities);
        }
        if (cacheKeyValues.get(1).hasValue()) {
            userCapabilities = cacheKeyValues.get(1).getValue();
        } else {
            userCapabilities = loadCapabilities(contextId, userId);
            if (null == loadedValues) {
                loadedValues = HashMap.newHashMap(2);
            }
            loadedValues.put(cacheKeyValues.get(1).getKey(), userCapabilities);
        }
        if (null != loadedValues) {
            cache.mput(loadedValues);
        }
        return Map.of(CapabilitySource.CONTEXT, contextCapabilities, CapabilitySource.USER, userCapabilities);
    }

    /**
     * Gets or loads the capabilities of a certain context.
     *
     * @param contextId The identifier of the context to get the capabilities for
     * @return The capabilities, or an empty set if none are stored
     * @throws OXException If getting capabilities fails
     */
    public Set<String> getCapabilities(int contextId) throws OXException {
        Optional<Cache<Set<String>>> optCache = optCache();
        if (optCache.isEmpty()) {
            return loadCapabilities(contextId);
        }
        Cache<Set<String>> cache = optCache.get();
        return cache.get(getCacheKey(cache, contextId), k -> loadCapabilities(contextId));
    }

    /**
     * Invalidates the internal cache for a certain context.
     *
     * @param contextId The identifier of the context to invalidate the cache for
     * @throws OXException If cache invalidation fails
     */
    public void invalidateCache(int contextId) throws OXException {
        Optional<Cache<Set<String>>> optCache = optCache();
        if (optCache.isEmpty()) {
            return;
        }
        Cache<Set<String>> cache = optCache.get();
        cache.invalidate(getCacheKey(cache, contextId));
    }

    /**
     * Loads the capabilities of a certain context.
     *
     * @param contextId The identifier of the context to load the capabilities for
     * @return The capabilities, or an empty set if none are stored
     * @throws OXException If loading capabilities fails
     */
    public Set<String> loadCapabilities(int contextId) throws OXException {
        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        Connection connection = databaseService.getReadOnly(contextId);
        try {
            return selectCapabilities(connection, contextId);
        } catch (SQLException e) {
            throw CapabilityExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            databaseService.backReadOnly(connection);
        }
    }

    /**
     * Gets or loads the capabilities of a certain user.
     *
     * @param contextId The context identifier
     * @param userId The identifier of the user to get the capabilities for
     * @return The capabilities, or an empty set if none are stored
     * @throws OXException If getting capabilities fails
     */
    public Set<String> getCapabilities(int contextId, int userId) throws OXException {
        Optional<Cache<Set<String>>> optCache = optCache();
        if (optCache.isEmpty()) {
            return loadCapabilities(contextId, userId);
        }
        Cache<Set<String>> cache = optCache.get();
        return cache.get(getCacheKey(cache, contextId, userId), k -> loadCapabilities(contextId, userId));
    }

    /**
     * Invalidates the internal cache for a certain user.
     *
     * @param contextId The context identifier
     * @param userId The identifier of the user to invalidate the cache for
     * @throws OXException If cache invalidation fails
     */
    public void invalidateCache(int contextId, int userId) throws OXException {
        Optional<Cache<Set<String>>> optCache = optCache();
        if (optCache.isEmpty()) {
            return;
        }
        Cache<Set<String>> cache = optCache.get();
        cache.invalidate(getCacheKey(cache, contextId, userId));
    }

    /**
     * Loads the capabilities of a certain user.
     *
     * @param contextId The context identifier
     * @param userId The identifier of the user to load the capabilities for
     * @return The capabilities, or an empty set if none are stored
     * @throws OXException If loading capabilities fails
     */
    public Set<String> loadCapabilities(int contextId, int userId) throws OXException {
        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        Connection connection = databaseService.getReadOnly(contextId);
        try {
            return selectCapabilities(connection, contextId, userId);
        } catch (SQLException e) {
            throw CapabilityExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            databaseService.backReadOnly(connection);
        }
    }

    private Optional<Cache<Set<String>>> optCache() {
        CacheService cacheService = services.getService(CacheService.class);
        if (null == cacheService) {
            return Optional.empty();
        }
        return Optional.of(cacheService.getCache(CACHE_OPTIONS_CAPABILITIES));
    }

    private static CacheKey getCacheKey(Cache<Set<String>> cache, int contextId) {
        return cache.newKey(asHashPart(contextId));
    }

    private static CacheKey getCacheKey(Cache<Set<String>> cache, int contextId, int userId) {
        return cache.newKey(asHashPart(contextId), Integer.toString(userId));
    }

    private static Set<String> selectCapabilities(Connection connection, int contextId) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT cap FROM capability_context WHERE cid=?");
            stmt.setInt(1, contextId);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return Collections.emptySet();
            }
            Set<String> set = new HashSet<>();
            do {
                set.add(rs.getString(1));
            } while (rs.next());
            return set;
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    private static Set<String> selectCapabilities(Connection connection, int contextId, int userId) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT cap FROM capability_user WHERE cid=? AND user=?");
            stmt.setInt(1, contextId);
            stmt.setInt(2, userId);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return Collections.emptySet();
            }
            Set<String> set = new HashSet<>();
            do {
                set.add(rs.getString(1));
            } while (rs.next());
            return set;
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

}
