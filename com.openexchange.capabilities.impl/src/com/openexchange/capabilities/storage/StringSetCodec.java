/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.capabilities.storage;

import com.openexchange.cache.v2.codec.json.AbstractJSONArrayCacheValueCodec;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;

/**
 * {@link StringSetCodec} - Cache codec for <code>Set&lt;String&gt</code>
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class StringSetCodec extends AbstractJSONArrayCacheValueCodec<Set<String>> {

    private static final UUID CODEC_ID = UUID.fromString("7871d12b-a82f-2f2d-00fc-c0e11e000001");

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONArray writeJson(Set<String> value) throws Exception {
        if (null == value || 0 == value.size()) {
            return JSONArray.EMPTY_ARRAY;
        }
        JSONArray jsonArray = new JSONArray(value.size());
        for (String element : value) {
            jsonArray.put(element);
        }
        return jsonArray;
    }

    @Override
    protected Set<String> parseJson(JSONArray jArray) throws Exception {
        if (null == jArray || jArray.isEmpty()) {
            return Collections.emptySet();
        }
        HashSet<String> set = HashSet.newHashSet(jArray.length());
        for (int i = 0; i < jArray.length(); i++) {
            set.add(jArray.getString(i));
        }
        return set;
    }

}
