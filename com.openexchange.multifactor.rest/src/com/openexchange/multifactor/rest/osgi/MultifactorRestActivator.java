/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.multifactor.rest.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.auth.Authenticator;
import com.openexchange.config.ConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.database.DatabaseService;
import com.openexchange.multifactor.MultifactorManagementService;
import com.openexchange.multifactor.rest.api.MultifactorManagementREST;
import com.openexchange.multifactor.rest.requestanalyzer.MultifactorRESTRequestAnalyzer;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.user.UserService;

/**
 * {@link MultifactorRestActivator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class MultifactorRestActivator extends HousekeepingActivator {

    private static final Logger logger = LoggerFactory.getLogger(MultifactorRestActivator.class);

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] { MultifactorManagementService.class, UserService.class, ContextService.class, Authenticator.class };
    }

    @Override
    protected void startBundle() throws Exception {
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());
        trackService(ConfigurationService.class);
        trackService(DatabaseService.class);
        openTrackers();
        registerService(MultifactorManagementREST.class, new MultifactorManagementREST(this));
        registerService(RequestAnalyzer.class, new MultifactorRESTRequestAnalyzer(this));
    }

    @Override
    protected void stopBundle() throws Exception {
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        super.stopBundle();
    }
}
