/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.multifactor.rest.requestanalyzer;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.segment.SegmentMarker;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * {@link MultifactorRESTRequestAnalyzer}- {@link RequestAnalyzer} for multifactor admin REST servlet
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public class MultifactorRESTRequestAnalyzer implements RequestAnalyzer {

    private static final Logger LOG = LoggerFactory.getLogger(MultifactorRESTRequestAnalyzer.class);
    private static final String PATH_PREFIX = "/admin/v1/contexts/";
    private static final String PATH_MULTIFACTOR_INFIX = "/multifactor/devices";

    private final ErrorAwareSupplier<DatabaseService> dbServiceSupplier;

    /**
     * Initializes a new {@link MultifactorRESTRequestAnalyzer}.
     * 
     * @param services A service lookup reference yielding the {@link DatabaseService}
     */
    public MultifactorRESTRequestAnalyzer(ServiceLookup services) {
        super();
        this.dbServiceSupplier = () -> services.getServiceSafe(DatabaseService.class);
    }

    @Override
    public Optional<AnalyzeResult> analyze(RequestData data) throws OXException {
        if (false == "GET".equals(data.getMethod()) && false == "DELETE".equals(data.getMethod())) {
            // not a request for the multifactor REST service
            return Optional.empty();
        }
        Optional<String> path = data.getParsedURL().getPath();
        if (path.isEmpty() || false == path.get().startsWith(PATH_PREFIX) || false == path.get().contains(PATH_MULTIFACTOR_INFIX)) {
            // not a request for the multifactor REST service
            return Optional.empty();
        }

        String sCtxId = path.get().substring(PATH_PREFIX.length(), path.get().indexOf('/', PATH_PREFIX.length()));
        int ctxId = Strings.parseUnsignedInt(sCtxId);
        if (-1 == ctxId) {
            LOG.debug("Unable to parse context id from analyzed request path {}", path.get());
            return Optional.empty();
        }
        String schema = dbServiceSupplier.get().getSchemaName(ctxId);
        return Optional.of(new AnalyzeResult(SegmentMarker.of(schema), null));
    }

}
