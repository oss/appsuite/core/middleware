/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.authorization.standard;

import static com.openexchange.java.Autoboxing.I;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Map;
import com.openexchange.authorization.AuthorizationExceptionCodes;
import com.openexchange.authorization.AuthorizationService;
import com.openexchange.context.ContextExceptionCodes;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.user.User;


/**
 * {@link DefaultAuthorizationImpl} - The standard authorization implementation that simply checks if either context or user is disabled.
 */
public final class DefaultAuthorizationImpl implements AuthorizationService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(DefaultAuthorizationImpl.class);

    /**
     * Initializes a new {@link DefaultAuthorizationImpl}.
     */
    public DefaultAuthorizationImpl() {
        super();
    }

    @Override
    public int getRanking() {
        // Specify a low ranking for the standard authorization service so that other services are favored
        return 0;
    }

    @Override
    public void authorizeUser(Context context, User user, Map<String, Object> properties) throws OXException {
        try {
            if (!context.isEnabled()) {
                LOG.debug("Context {} ({}) is disabled.", Integer.valueOf(context.getContextId()), context.getName());
                throw AuthorizationExceptionCodes.USER_DISABLED.create(ContextExceptionCodes.CONTEXT_DISABLED.create(I(context.getContextId()), context.getName()), I(user.getId()), I(context.getContextId()));
            }
        } catch (UndeclaredThrowableException e) {
            throw AuthorizationExceptionCodes.UNKNOWN.create(e);
        }
        if (!user.isMailEnabled()) {
            throw AuthorizationExceptionCodes.USER_DISABLED.create(I(user.getId()), I(context.getContextId()));
        }
        if (user.getShadowLastChange() == 0) {
            throw AuthorizationExceptionCodes.PASSWORD_EXPIRED.create();
        }
    }

}
