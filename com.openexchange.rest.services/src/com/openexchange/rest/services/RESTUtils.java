/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.rest.services;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import com.openexchange.ajax.writer.ResponseWriter;
import com.openexchange.exception.OXException;

/**
 * {@link RESTUtils} - Utility class for REST related topics.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class RESTUtils {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(RESTUtils.class);
    }

    /**
     * Convenience method for creating a 400 - bad request - plain response
     *
     * @param msg The plain message
     * @return The 400 response
     */
    public static Response respondBadRequest(String msg) {
        return Response.status(Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN_TYPE).entity(msg).build();
    }

    /**
     * Convenience method for creating a 400 - bad request - JSON response
     *
     * @param json The JSON message
     * @return The 400 response
     */
    public static Response respondBadRequest(JSONObject json) {
        return Response.status(Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON).entity(json).build();
    }

    /**
     * Convenience method for creating a 404 - not found - plain response
     *
     * @param msg The plain message
     * @return The 404 response
     */
    public static Response respondNotFound(String msg) {
        return Response.status(Status.NOT_FOUND).type(MediaType.TEXT_PLAIN_TYPE).entity(msg).build();
    }

    /**
     * Convenience method for creating a 404 - not found - JSON response
     *
     * @param json The JSON message
     * @return The 400 response
     */
    public static Response respondNotFound(JSONObject json) {
        return Response.status(Status.NOT_FOUND).type(MediaType.APPLICATION_JSON).entity(json).build();
    }

    /**
     * Convenience method for creating a 500 - internal server - plain response
     *
     * @param msg The plain message
     * @return The 500 response
     */
    public static Response respondServerError(String msg) {
        return Response.status(Status.INTERNAL_SERVER_ERROR).type(MediaType.TEXT_PLAIN_TYPE).entity(msg).build();
    }

    /**
     * Convenience method for creating a 500 - internal server - JSON response
     *
     * @param json The JSON message
     * @return The 500 response
     */
    public static Response respondServerError(JSONObject json) {
        return Response.status(Status.INTERNAL_SERVER_ERROR).type(MediaType.APPLICATION_JSON).entity(json).build();
    }

    /**
     * Generates an error JSON object
     *
     * @param ex The {@link OXException} to create the error object from
     * @return The JSON error object for the given {@link OXException}
     */
    public static JSONObject generateError(OXException ex) {
        JSONObject main = new JSONObject();
        try {
            ResponseWriter.addException(main, ex);
        } catch (JSONException e) {
            LoggerHolder.LOG.error("Error while generating error for client.", e);
        }
        return main;
    }
}
