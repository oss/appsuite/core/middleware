---
title: Mail Push
icon: fa-bell
tags: Mail, Configuration, Push, Installation
---

# Enable permanent mail push listeners

The existing mail push framework of the Open-Xchange Middleware has been extended by the capability to spawn "permanent" listeners for incoming new message deliveries. Up to that point the life cycle for a listener was bound to at least one active session, which is associated with a client that is allowed to receive mail push notifications.

With introduction of the previously mentioned capability, listeners can be started without the need for an existent session right on the start of an Open-Xchange Middleware node. In addition those permanent listeners are spread approximately even over capable cluster members as - dependent on the underlying implementation - a listener representation may open/hold resources (socket connections) in order to receive notifications about new message deliveries.

To prepare a certain Open-Xchange Middleware node to spawn permanent mail push listeners the following properties need to be configured in file ``/opt/open-xchange/etc/mail-push.properties``:

* ``com.openexchange.push.allowPermanentPush``
This is the general switch to enable/disable support for permanent listeners on a node. Thus needs to be set to "true"
* ``com.openexchange.push.allowedClient``
Specify the comma-separated list of clients which are allowed to receive notifications about new mails. Ensure that "open-xchange-mobile-api-facade*" is listed here in case new mail push is supposed to be setup for OX Mail v2 0.
* ``com.openexchange.push.credstorage.enabled``
As permanent listeners are required to run without an active session, the credential storage can be used to store user credentials in installations that do not support a master authentication to the mail storage. Hence, if the property ``com.openexchange.mail.passwordSource`` (``/opt/open-xchange/etc/mail.properties``) is not set to "global" this property is required to be set to "true"
* ``com.openexchange.push.credstorage.passcrypt``
This property is required if ``com.openexchange.push.credstorage.enabled`` is set to "true". It does specify the passphrase to use to symmetrically encrypt the stored credentials. The passphrase is required to be equal on each cluster member.
* ``com.openexchange.push.credstorage.rdb``
Once the credential storage is enabled, Open-Xchange offers two ways of storing the user-associated login/password combination. In cluster memory (default) or persisted to database. While the first way ensures that no user credentials are persisted anywhere in the Open-Xchange installation, it has the big disadvantage that stored credentials are gone once the last cluster member gets shut-down. Therefore there is also the possibility to store the credentials inside the database. Of course, no matter where the credentials are stored, they are encrypted using the value from ``com.openexchange.push.credstorage.passcrypt`` property

With setting the properties above the configuration on the Open-Xchange Middleware node is prepared to spawn permanent listeners for new mails.

Now an appropriate mail push bundle/package needs to be enabled that supports spawning permanent listeners. Currently Open-Xchange ships with three implementations:

* ``open-xchange-push-dovecot`` (also requires the optional open-xchange-rest package)
* ``open-xchange-push-imapidle`` (Not recommended, therefore disabled for permanent listeners by default: ``com.openexchange.push.imapidle.supportsPermanentListeners`` is set to "false" by default)
* ``open-xchange-push-mailnotify``

This article is focussed on ``open-xchange-push-dovecot``. Packages can be enabled in your chart's `values.yaml`:

```yaml
core-mw:
  packages:
    status:
      open-xchange-push-dovecot: enabled
```

Putting all together the following execution flow is taken to decide whether permanent listeners are spawned or not:

![Mail push decision flow](mail_push/mail_push_configuration.png "Mail push decision flow")

To check at any time what listeners are currently running, there is a new command-line tool ``/opt/open-xchange/sbin/pushusers`` that outputs the user-id/context-id pair along-side with the information if the listener is of permanent nature or bound to an active session:

```
usage: pushusers [-l | [ -r -c <contextId> -u <userId> -i <client>] ] -A <masterAdmin> -P <masterAdminPassword> [-p
                 <RMI-Port>] [-s <RMI-Server>] [--responsetimeout <responseTimeout>] | [-h]
 -A,--adminuser <adminUser>       Admin username
 -c,--context <contextId>         A valid context identifier
 -h,--help                        Prints this help text
 -i,--client <clientId>           The client identifier
 -l,--list-client-registrations   Flag to list client registrations
 -p,--port <rmiPort>              The optional RMI port (default:1099)
 -P,--adminpass <adminPassword>   Admin password
 -r,--unregister                  Flag to unregister a push user
    --responsetimeout <timeout>   The optional response timeout in seconds when reading data from server (default: 0s;
                                  infinite)
 -s,--server <rmiHost>            The optional RMI server (default: localhost)
 -u,--user <userId>               A valid user identifier

Command-line tool for unregistering and listing push users and client registrations
```

An exemplary out put might look like:

```
~# /opt/open-xchange/sbin/pushusers --list -A oxadmin -P secret
Context  User  Permanent
      1  249   true
      1  402   true
```

# Setup Dovecot Mail Push

Please see [here]({{ site.baseurl }}/middleware/mail/dovecot/dovecot_push.html)

# Setup Push Notification Service

Please see [here]({{ site.baseurl }}/middleware/push_notifications/push_notification_service.html).