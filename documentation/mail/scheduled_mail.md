---
title: Scheduled Mails
icon: fa-clock
tags: Mail, Configuration, Installation
---

# Motivation

Starting with v8.20, the Open-Xchange Server supports the transport of mails at a later time. The user is able to specify the desired time
when a certain mail is supposed to be sent.

# Prerequisites

In order to support sending a composed mail at a desired date, the package `open-xchange-mail-scheduled` needs to be installed.

Moreover, for safe connect against mail/transport system, master authentication is required being setup:

```
 com.openexchange.mail.passwordSource=global
 com.openexchange.mail.masterPassword=secret
```

In addition, the feature also needs to be enabled for App Suite UI through the following UI setting:

```
io.ox/core//features/scheduleSend = true
```

# Configuring scheduled mail feature

- `"com.openexchange.mail.scheduled.enabled"` controls whether the feature is available or not. This property is reloadable and config-cascade aware.
- `"com.openexchange.mail.scheduled.maxNumberOfScheduledMails"` specifies the max. allowed number of scheduled mails per user. This property is reloadable and config-cascade aware.
- `"com.openexchange.mail.scheduled.maxNumberOfScheduledMailsPerHour"` specifies the max. allowed number of scheduled mails being sent per hour for a user. This property is reloadable and config-cascade aware.
- `"com.openexchange.mail.scheduled.checkFrequencyMinutes"` specifies the frequency in minutes when to check for due scheduled mails. This property is reloadable, but not config-cascade aware.
- `"com.openexchange.mail.scheduled.lookAheadMinutes"` specifies the look-ahead in minutes that is the extra time added to <i>now</i> when a scheduled mail is considered as due. This property is reloadable, but not config-cascade aware.
- `"com.openexchange.mail.scheduled.lockExpiryMinutes"` specifies the time in minutes when the lock marking a scheduled mail as "in processing" is considered as expired and thus may be newly acquired by another process. This property is reloadable, but not config-cascade aware.
- `"com.openexchange.mail.scheduled.lockRefreshMinutes"` specifies the time in minutes when the lock marking a scheduled mail as "in processing" is refreshed by lock-holding process. This property is reloadable, but not config-cascade aware.
