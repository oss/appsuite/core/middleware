---
title: Mail Flagging
icon: fa-flag
tags: Mail, Configuration
---

# Introduction

Almost every mail client supports IMAP \Flagged to enable highlighting of certain mails and so does the OX Appsuite Middleware. Additionally it also supports 10 custom color flags which allows mails to be marked with a certain color. Both of these variants are offered in a configurable manner. 

Please note that the flags and colors are only capabilities the Middleware provides. In which way they are used is up to the client.

The following modes of operation are supported by the Middleware:

* **Color only:**
With this mode only the color flags are provided. The special \Flagged system flag is not available.

* **Flagged only:**
With this mode only the special \Flagged system flag is provided. No color flags are available.

* **Flagged and color:**
With this mode both the special \Flagged system flag and the color flags are provided. They are can be set independently, so that setting a color doesn´t set \Flagged and setting \Flagged will not be mapped to a color. 

* **Flagged implicit:**
With this mode only the color flags are provided but the special \Flagged system flag is used implicitly. That mean that mails that have been flagged by other clients via IMAP \Flagged are tagged with a configurable color and also a mail flagged with a color by an App Suite client appears highlighted in non-App Suite clients. So a certain color label is linked with the \Flagged system flag.

These capabilities are provided to the client via two jslob boolean entries:

* **io.ox/mail/features/flag/star** - defines \Flagged flag support
* **io.ox/mail/features/flag/color** - defines custom color flags support


# Configuration

If you want to configure the OX App Suite UI then please refer to its documentation for more information. 
For informations on how to configure the Middleware part keep reading.

Relevant are only two properties: 

* **com.openexchange.mail.flagging.mode**
* **com.openexchange.mail.flagging.color**

The flagging mode accepts the following modes which relate to the operation modes mentioned earlier:

* **colorOnly**
* **flaggedOnly**
* **flaggedAndColor**
* **flaggedImplicit**

The color property accepts values from 1-10. Whereby each color is represents a client specific color.
This property is only used in case the mode is set to `flaggedImplicit`.