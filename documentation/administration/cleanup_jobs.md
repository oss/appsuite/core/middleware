---
title: Database Cleanup Jobs
icon: fa-database
tags: Administration, Database, Configuration
---

App Suite middleware performs certain background routines, primarily to periodically clean up no longer needed records from the database. One common thing of those tasks is that they iterate over all groupware database schemas of the installation when they're running. Depending on the size, this iteration can become expensive, which is why they're executed within a controllable framework.

Besides some special jobs that have to operate permanently (e.g. scheduled email delivery), most of the housekeeping tasks can and should run in the background, i.e. during non-peak hours in the night or at the weekends. 

The middleware takes care that all cleanup jobs are only excecuted once within the cluster; mutual exclusion is done via Redis as well as information about last executions of each task within the groupware databases. 


# Configuration

General clean-up jobs are only executed in a configured "maintenance" window. This can be configured using the property [`com.openexchange.database.cleanup.schedule`](https://documentation.open-xchange.com/components/middleware/config{{site.baseurl}}/#mode=search&term=com.openexchange.database.cleanup.schedule). 

The timeframe is specified as a pattern. The default time zone of the Java virtual machine is assumed, which is typically the system's default time zone; unless the <code>user.timezone</code> property is set otherwise. It takes a semicolon-separated listing of tokens, while a token consists of a weekday and an optional time range: 
- A weekday is one of: `Mon`, `Tue`, `Wed`, `Thu`, `Fri`, `Sat` and `Sun`. It also accepts ranges e.g. `Mon-Fri`.
- A time range is a hour of day range of the 24h clock. E.g. `0-6` for 0 to 6 in the morning or `22:30-24` for 22:30h until 24h in the evening. Also accepts comma-separated pairs; e.g. `0-6,22:30-24`.
- A more complex example: `Mon 0:12-6:45; Tue-Thu 0-7:15; Fri 0-6,22:30-24; Sat,Sun 0-8` 

Besides the schedule itself, there are also some further options available to control certain aspects of cleanup execution. See the [property documentation](https://documentation.open-xchange.com/components/middleware/config{{site.baseurl}}/#mode=search&term=com.openexchange.database.cleanup) for further details.


# Cleanup Jobs

The following gives a brief overview about the jobs included in core middleware. Which of them are actually executed partly also depends on whether the underlying feature is activated via Helm charts.

## `com.openexchange.database.tombstone.cleanup.SchemaTombstoneCleaner`

Whenever a contact, appointment, task, folder or file (infostore item) is removed, we store a record in a special 'tombstone' database table to aid synchronization purposes ("get all deleted items since last sync"). However, an incremental synchronization is usually only needed for a certain timespan in the past, which varies from a couple of minutes (for the web client) up to longer timespan in the magnitude of months for client applications built upon CalDAV or CardDAV.

Therefore, tombstone entries are purged from those tables that are older than a configurable timespan. Per default, three months are ssumed; configurable via [`com.openexchange.database.tombstone.cleanup.timespan`](https://documentation.open-xchange.com/components/middleware/config{{site.baseurl}}/#mode=search&term=com.openexchange.database.tombstone.cleanup.timespan). Can also be disabled completely via [`com.openexchange.database.tombstone.cleanup.enabled`](https://documentation.open-xchange.com/components/middleware/config{{site.baseurl}}/#mode=search&term=com.openexchange.database.tombstone.cleanup.enabled). 


## `com.openexchange.drive.impl.checksum.PeriodicChecksumCleaner`

Cleans up no longer used pre-calculated checksums of files and directories that are used during OX Drive synchronization. Only relevant if feature `open-xchange-drive` is active. 


## `com.openexchange.gdpr.dataexport.impl.cleanup.DataExportCleanUpTask` 

Clean-up task for orphaned data export files, available with feature `open-xchange-gdpr-dataexport`. 


## `com.openexchange.mail.compose.impl.cleanup.CompositionSpaceCleanUpTask` 

A global task responsible for deleting expired composition spaces. 


## `com.openexchange.mail.json.compose.share.DefaultAttachmentStoragePeriodicCleaner` 

The periodic cleaner for shared email attachments that are expired.  


## `com.openexchange.objectusecount.cleanup.GenericUseCountCleanUpExecution` and `com.openexchange.objectusecount.cleanup.ObjectUseCountCleanUpExecution` 

Adjusts the internally stored individual usage frequency of contacts and other groupware items like groups or resources, to improve ordering of results during auto-completion in App Suite UI.


## `com.openexchange.share.impl.cleanup.GuestCleanupExecution`

Removes temporal shares and guest accounts that have been expired. 

