---
title: Servlets
icon: fa-puzzle-piece
tags: Administration, Security, Configuration, Servlet
---

This page contains an overview of Servlets registered by the Middleware. It also explains how to activate the Servlets and their respective purposes. Additionally, there is a recommendation regarding whom to expose the Servlets to.
Please note that the table below is not a complete overview of all registered Servlets. Additional components, such as the Document Converter, will register their own Servlets. Please refer to the corresponding documentation to find additional registered Servlets.

# Overview

| Servlet             | Path                        | Package                           | Expose to    | Property to configure                     |
|---------------------|:---------------------------:|:---------------------------------:|:------------:|------------------------------------------:|
| DAV                 | /servlet/dav                | open-xchange-dav                  | Clients      | [com.openexchange.dav.prefixPath](https://documentation.open-xchange.com/components/middleware/config{{ site.baseurl }}/index.html#com.openexchange.dav.prefixPath)   |
| Dispatcher          | /ajax                       | open-xchange-core                 | Clients      | [com.openexchange.dispatcher.prefixPath](https://documentation.open-xchange.com/components/middleware/config{{ site.baseurl }}/index.html#com.openexchange.dispatcher.prefix)   |
| EAS                 | /serlvet/mobileconfig       | open-xchange-eas-provisioning     | Clients      |                                           |
| Guard               | /oxguard                    | open-xchange-guard                | Clients      |                                           |
| Infostore           | /infostore, /files, /drive, /servlet/webdav.infostore, /servlet/webdav.drive"   | open-xchange-core     | Clients    |     |
| Jolokia             | /monitoring/jolokia         | open-xchange-core                 | Admins       |                                           |
| Proxy               | /servlet/proxy              | open-xchange-core                 | Clients      |                                           |
| REST                | /                           | open-xchange-core                 | [See REST section](#rest)    |                                           |
| SOAP CXF            | /webservices                | open-xchange-soap-cxf   | Admins     | [com.openexchange.soap.cxf.baseAddress](https://documentation.open-xchange.com/components/middleware/config{{ site.baseurl }}/index.html#com.openexchange.soap.cxf.baseAddress)   |

Please note that client facing Servlets must be made available to the used loadbalancer, like Istio.

## How to enable Servlets

Servlets will be started and exposed whenever the packages containing the Servlets are enabled. For most Servlets this will be the `open-xchange-core` package, which must be enabled anyways. If, for example, the additional *DAV* Servlets for e.g. CalDAV shall be reachable, the package `open-xchange-dav` must be enabled in the `core-mw` chart:

```yaml
core-mw:
  packages:
    status:
      open-xchange-dav: enabled
```

# Details

## DAV

Enables the DAV related functionality for the MW, in particular support for CalDAV and CardDAV. See [CalDAV and CardDAV](https://documentation.open-xchange.com{{ site.baseurl }}/middleware/miscellaneous/caldav_carddav.html)

## Dispatcher

The dispatcher framework handles most request meant for the Middleware. Per default the dispatcher will register itself under the path ``/ajax``. However, paths like ``/appsuite/api`` might be preferred and must be set accordingly in the configuration.

## EAS

The endpoint for EAS clients to request their configuration from.

## Guard

The Open Xchange Guard feature. As this feature provides additional API for clients, it registers new Servlets, too.
Note: The Servlets registered by Guard will begin with ``/oxguard``. However clients may query ``/appsuite/api/oxguard``. Therefore, likewise, for the dispatcher, the load balancer before the middleware should redirect calls.

## Infostore

The infostore aka drive module for the Middleware. Provides files for clients.

## Jolokia

Jolokia is an HTTP/JSON bridge for remote JMX access. Designed to be used by administrators. For additional information please visit https://jolokia.org/

## Proxy

A module for the Middleware in which it will act as a proxy.

## REST

REST API endpoints created by the Middleware have different paths. Therefore a general statement is not possible. The list below shows all registered REST endpoints and outline by whom the APIs are accessed.

| Feature                   | Path                                                                  | Package                          | Accessible by      |
|---------------------------|:---------------------------------------------------------------------:|:--------------------------------:|-------------------:|
| Session closer            | /admin/v1/close-sessions                                              | open-xchange-core                | Master Admins      |
| Password change history   | /admin/v1/contexts/{context-id}/users/{user-id}/passwd-changes        | open-xchange-admin               | [Individual](https://documentation.open-xchange.com{{ site.baseurl }}/middleware/security_and_encryption/password_change_history.html)   |
| Multifactor               | /admin/v1/contexts/{context-id}/users/{user-id}/multifactor/devices   | open-xchange-multifactor         | Basic Auth login   |
| Adverstiment              | /advertisement/v1                                                     | open-xchange-advertisment        | Basic Auth login   |
| Chronos Push Mail         | /chronos/v1/itip/pushmail                                             | open-xchange-core                | Basic Auth login   |
| MW Healtch checks         | /health                                                               | open-xchange-core                | [Health check login](http://documentation.open-xchange.com/components/middleware/config{{ site.baseurl }}/index.html#com.openexchange.health.username)   |
| Guard guest share link    | /preliminary/guard/guest/v1                                           | open-xchange-guard               | Basic Auth login   |
| Dovecot Push              | /preliminary/http-notify/v1/                                          | open-xchange-push-dovecot        | Basic Auth login   |
| Authentication service    | /preliminary/adminproc/v1                                             | open-xchange-rest                | Basic Auth login   |
| Capability service        | /preliminary/capabilities/v1/                                         | open-xchange-rest                | Basic Auth login   |
| ConfigView service        | /preliminary/configuration/v1/                                        | open-xchange-rest                | Basic Auth login   |
| HTML sanitizer service    | /preliminary/htmlproc/v1/                                             | open-xchange-rest                | Basic Auth login   |
| SessionD service          | /preliminary/session/v1/                                              | open-xchange-rest                | Basic Auth login   |
| Mail resolver             | /preliminary/utilities/mailResolver/v1                                | open-xchange-rest                | Basic Auth login   |
| User Feedback             | /userfeedback/v1                                                      | open-xchange-userfeedback        | Basic Auth login   |
| Request Analysis          | /request-analysis/v1                                                  | open-xchange-core                | Basic Auth login   |
| Context Pre-assembly      | /admin/v1/contexts/pre-assemble                                       | open-xchange-admin-autocontextid | Master Admins      |

Note: The "Basic Auth login" can be adjusted via [com.openexchange.rest.services.basic-auth.login](https://documentation.open-xchange.com/components/middleware/config{{ site.baseurl }}/index.html#com.openexchange.rest.services.basic-auth.login)

### Further reading

The REST API is described [here](https://documentation.open-xchange.com/components/middleware/rest{{ site.baseurl }}/index.html)

## SOAP CFX

The SOAP admin interfaces for provisioning calls.
