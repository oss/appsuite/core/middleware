---
title: Provision rate limiting
icon: fa-hand-paper-o
tags: Administration, Rate Limit, Configuration, Command Line Tools, SOAP, RMI
---

# Provision rate limiting

In case multiple admins perform parallel provisioning operations on the same machine it is advisable to restrict the provisioning rate for those accounts in case an account fires too many requests. To accomplish that a new rate limit was introduced which limits the amount of requests per admin in a one minute timeslot. 

## How it works

The requests are limited in constant one minute buckets. This means that it is theoretically possible for a single admin to perform up the the double amount of requests within a one minute timeframe if the requests are perfectly divided into two separate buckets.

The limit applies to all available apis (e.g. soap, rmi and clts) and is tracked cluster wide.


## Configuration

The rate limit is active by default. You can adjust the default limit by using the `com.openexchange.admin.rmi.rate.limit.default` property. 
E.g.:

```
com.openexchange.admin.rmi.rate.limit.default=200
```

Additionally you can also adjust the limit for an individual admin by using the `com.openexchange.admin.rmi.rate.limit.[admin]` property. E.g. for an admin with the name 'oxadmin':

```
com.openexchange.admin.rmi.rate.limit.oxadmin=150
```
