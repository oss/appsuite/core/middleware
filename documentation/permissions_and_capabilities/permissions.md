---
title: Permissions
icon: fa-lock
tags: Permissions
---

# Introduction

This page provides more detailed descriptions for some permissions.

## read_create_shared_folders 

If this permission is not granted, neither folders are allowed to be shared nor shared folders are allowed to be seen by the user.
Existing permissions are not removed if a user loses this right, but the display of shared folders is suppressed.

## edit_public_folders 

If this permission is not granted, it is prohibited for the user to create or edit public folders. Existing public folders are
visible in any case. If a user doesn't have this permission the folder permissions of public folders are reduced to read only.
This permission is also a requirement for some other permissions like groupware, teamview, freebusy, calendar conflict handling and more.

## public_folder_editable

Allows to add the admin flag to permissions of root public folders. This permission is only allowed for context admins.