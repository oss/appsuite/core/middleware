---
title: Deputy permissions
icon: fa-lock
tags: Permissions, Capabilities
---

# Introduction

Since version v7.10.6 the Open-Xchange Middleware offers the feature to grant deputy permissions to one ore more users from the same context that are supposed to act as representatives (e.g. vacation replacement).

A deputy has configurable access to granting user's primary mail account's Inbox and/or standard Calendar folder. Moreover, a deputy might get the "send on behalf of" permission granted, which allows him to send E-Mails on behalf of the granting user.

# Configuration

To enable the deputy permission feature, the `open-xchange-deputy` package needs to be enabled in your chart's `values.yaml`:

```yaml
core-mw:
  packages:
    status:
      open-xchange-deputy: enabled
```

# Enable deputy permissions

To enable deputy permission management the `com.openexchange.deputy.enabled` property needs to be set to `true`, which is `false` by default. That property is fully config-cascade-aware.
After the feature is enabled, the user will gain access to a new deputy permission management menu in the App Suite UI.

# Behavior

Deputy permissions do overwrite possibly existent permissions. Moreover, granted deputy permissions cannot be changed/deleted via regular permission dialogue, but only via deputy permission management by the granting user.

The setting `com.openexchange.deputy.simplePermissionMode` controls whether deputy permissions may only be assigned to users (not groups) and may only contain such permission rights for viewer, editor and author as
App Suite UI would grant those permission rights.

Currently, deputy permissions for the following modules can be granted:

 * Mail
 * Calendar

Furthermore for the mail module, the granting user of the deputy permission can decide whether the deputy can sent mails using the granting user's mail account. If granted, mails can be send by the deputy "on behalf of" the granting user. To indicate that a deputy and not the granting user responded, the `Sender` header filled with the deputy's mail address will be set within the mail. Whereby the `From` header will be set to granting user's default sender address. E.g.:

```
From: Granting user <grantinguser@example.org>
Sender: Deputy <deputy@example.org>
```

In case the deputy has been given write permissions to the granting user's calendar, outgoing notification and/or iTIP mails (see also the [iTIP article]({{ site.baseurl }}/middleware/calendar/iTip.html)) will automatically be decorated with the "on behalf" relationship. This applies to the outgoing mails as well as for the generated iCAL files sent along the mails. E.g.:

```
ORGANIZER;CN=Granting user;SENT-BY="mailto:deputy@example.org";EMAIL=grantinguser@example.org:mailto:grantinguser@example.org
```

# Requirements

To grant deputy permission to the primary mail account's Inbox folder, the mail account is required to be an IMAP account that supports the RFC 2086 `ACL` and RFC 5464 `METADATA` extensions, and supports the `/shared/vendor/vendor.open-xchange/deputy` METADATA key.

For further reading on METADATA and METADATA keys, read for example the [Dovecot IMAP METADATA](https://doc.dovecot.org/configuration_manual/imap_metadata/) documentation

# Provisioning

With v8.29 of the Open-Xchange Server, deputy permissions can also be managed through the "http://soap.admin.openexchange.com/OXDeputyPermissionsService" SOAP end-point.

However, using that SOAP API requires that primary mail server is a Dovecot IMAP server with [DoveAdm REST API](https://doc.dovecotpro.com/3.0/core/admin/doveadm.html#http-api) enabled.
Hence, the Middleware's connector for that DoveAdm REST API needs to be enabled.

Example `doveadm.properties` file

```
# Enable
com.openexchange.dovecot.doveadm.enabled=true

# Connection settings
com.openexchange.dovecot.doveadm.endpoints=https://127.0.0.1:8080/doveadm/v1
com.openexchange.dovecot.doveadm.endpoints.totalConnections=100
com.openexchange.dovecot.doveadm.endpoints.maxConnectionsPerRoute=0 (max. connections per route is then determined automatically by specified end-points)
com.openexchange.dovecot.doveadm.endpoints.readTimeout=20000
com.openexchange.dovecot.doveadm.endpoints.connectTimeout=5000

# Secret
com.openexchange.dovecot.doveadm.apiSecret=secret
```
