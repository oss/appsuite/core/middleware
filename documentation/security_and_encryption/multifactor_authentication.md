---
title: Multifactor Authentication
icon: fa-link
tags: Security, Configuration
---

Appsuite provides methods for users to require secondary, additional methods of authentication before creating a valid Appsuite session. These methods may include SMS messages, Time based authenticator methods, U2F compatible devices/keyfobs, and other custom methods.

**Please Note:** When a user enables multifactor authentication for their account, their logins will be limited to the Appsuite UI only, as client applications (Mail app, Drive app, Exchange Active Sync) do not currently have the interface to perform the additional multifactor requirements. As of 7.10.4, users can add Application Passwords to use in order to bypass multifactor authentication for their client applications. See [Application Passwords]({{site.baseurl}}/middleware/login_and_sessions/application_passwords.html) for details.

# Enabling Multifactor

The `open-xchange-multifactor` package is required for the core of multifactor authentication. The package can be enabled in your chart's `values.yaml`:

```yaml
core-mw:
  packages:
    status:
      open-xchange-multifactor: enabled
```

For some methods (such as SMS), additional packages will be required. With SMS, for example, you must also enable a provider, such as `open-xchange-sms-sipgate`.

Then, multifactor must be enabled as a capability. This can be done in the multifactor.properties file, or as a cascade value

```properties
com.openexchange.capability.multifactor=true
```

# Enabling SMS

First, the SMS provider must be installed and configured. Most will require a configured username and password, or AUTH_TOKEN. Install the needed package and configure.

At that point, you should enable SMS in the multifactor.properties file

```properties
 com.openexchange.multifactor.sms.enabled=true
```

The following properties are also available

```properties
com.openexchange.multifactor.sms.tokenLength (default is 8 characters)
com.openexchange.multifactor.sms.tokenLifetime (Number of minutes until challenge expires)
com.openexchange.multifactor.maxTokenAmount (Maximum number of challenges before locked out)
```

# Enabling TOTP

TOTP is Time-based One Time Password. This works with several apps available in mobile stores, such as Google Authenticator.

To enable, just set in the multifactor.properties file

```properties
com.openexchange.multifactor.totp.enabled=true
```

## Enabling Backup String

This is a method to allow a user to log into their account if they lose their primary multifactor authentication device (say losing their phone or U2F token). It is a long string that they can copy, download, or print to use to unlock the account in the event of loss

To enable, set in the multifactor.properties file

```properties
com.openexchange.multifactor.backupString.enabled=true
```

# Enabling U2F

**Note that U2F is a deprecated protocol that has been replaced by webauthn.  Browsers do not support U2F any longer.  For existing users, webauthn has backward compatibility with U2F.  If U2F is enabled, then existing devices will still be used for login.  No new devices will be regsitered using U2F**

In multifactor.properties, enable U2F

```properties
com.openexchange.multifactor.u2f.enabled=true
```

Then, the domain that the user will be using must be specified. This will be used with the requests to the U2F device, and must mach the website. This configuration is config-cascade aware

```properties
 com.openexchange.multifactor.u2f.appId=https://yourdomain
```

# Enabling WebAuthn

WebAuthn allows authentication with public-key cryptography. The WebAuthn provider is backwards compatible with U2F, which means that authentication with already registered U2F devices is possible (assuming u2f is enabled above).


In multifactor.properties, enable WebAuthn

```properties
com.openexchange.multifactor.webAuthn.enabled=true
```

Then, the name of the domain that the user will be using must be specified. This will be used with the requests to the WebAuthn device, and must mach the website. This configuration is config-cascade aware.

```properties
 com.openexchange.multifactor.webAuthn.rpId=yourdomain
```

Please see additional webauthn settings documentation for additional settings.

# Login Page

The user interfaces for multifactor authentication will be rendered into the App Suite login page and keep the styling of that page. See the _Login page_ section in the _Configuration_ chapter of the UI documentation for detailed information on customizing the login and multifactor pages. Additionally, a dedicated background can be set via `default.multifactorBackground` in `as-config.yml`.

For example:

```yaml
 default:
     host: all
     multifactorBackground: pages/secondFactor
```
