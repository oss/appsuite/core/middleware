---
title: Anti-Virus
icon: fa-bug
tags: Security, Installation, Configuration
---

# Introduction

Since 7.10.2, the middleware is equipped with the ability to perform anti-virus scanning on mail and PIM attachments as well as drive items upon download (only via the AppSuite UI so far; i.e. synced drive items will NOT be scanned). To enable this feature a few components should be added to your infrastructure.

In a nutshell the middleware is "outsourcing" the anti-virus scans to a remote anti-virus server via requests based on the [ICAP](https://tools.ietf.org/html/rfc3507) protocol. And those are the two components with which your infrastructure needs to be enhanced for the anti-virus feature to work properly: the ICAP server and an anti-virus server/daemon which is ICAP-capable.

In the following example we will use [ClamAV](https://www.clamav.net/) as the anti-virus service and [SquidClamav](https://squidclamav.darold.net/) as an ICAP service through the c-icap server.

# Prerequisites

- Helm v3.13.2
- Kubernetes v1.27
- Kubectl v1.27
- [Middleware Chart](https://gitlab.open-xchange.com/oss/appsuite/core/middleware)
- [Example Antivirus Chart](www.example.com)

# Example Installation

## Image Build

First of all you need to build, and afterwards push, the c-icap Docker image to your registry. You can find the respective `Dockerfile` at [examples/images/c-icap/Dockerfile](www.example.com):

```bash
export REGISTRY_URL=<YOUR_REGISTRY_URL>
docker build -f examples/images/c-icap/Dockerfile . -t ${REGISTRY_URL}/c-icap:latest
docker push ${REGISTRY_URL}/c-icap:latest
```

## Configuration

On the middleware side, only a few things need to be configured. Additionally, you have the option to configure other settings, such as the maximum file size allowed for scanning or the timeout of the ICAP client. For more information on configuration, please refer to the [documentation](https://documentation.open-xchange.com/components/middleware/config{{site.baseurl}}/#mode=tags&tag=Anti-Virus). 

**values.yaml**

```yaml
core-mw:
  packages:
    status:
      open-xchange-antivirus: enabled
  properties:
    com.openexchange.antivirus.enabled: "true"
    com.openexchange.antivirus.server: "${RELEASE}-c-icap-service"
    com.openexchange.antivirus.port: "1344"
    com.openexchange.antivirus.service: "squidclamav"
```

Once you have enabled and configured the anti-virus feature in the middleware, add a dependency for your example anti-virus chart in the `Chart.yaml`` file.

**Chart.yaml**

```yaml
dependencies:
  - name: anti-virus
    repository: file://../anti-virus # Path to the example anti-virus chart 
    version: "1.0.0"
```

Finally, set the reference to the c-icap image you have previously pushed.

**values.yaml**

```yaml
anti-virus:
  c-icap:
    image:
      repository: <YOUR_REGISTRY_URL>/c-icap:latest
      tag: "latest"
    # Optional
    imagePullSecrets: []
```

### Advanced Configuration

#### Maximum File Size

The default maximum allowed file size in mega-bytes that is acceptable for the middleware to scan is set to `100`. You can increase or decrease that value at your leisure by adjusting the value in the `com.openexchange.antivirus.maxFileSize` property. If the file size that is to be scanned exceeds the configured amount, then a warning message will be displayed to the user informing him about the fact and whether he still wants to download the content.

Note that at the moment the entire data stream is fetched twice from the underlying file storage; once to send it to the anti-virus service for scanning, and once to deliver it to the user. With that being said, be aware that enabling the anti-virus feature _will_ increase the I/O in your file storage servers.

#### ICAP Client Timeout

The connection time-out of the middleware's ICAP client can be configured via the `com.openexchange.icap.client.socketTimeout` property and defaults to 10.000 milliseconds. This property ensures that in case of a third party service disruptions (service down/unreachable/updating/what-have-you) the download of the file will not be block indefinitely and a warning message will be returned to the user instead, prompting him that the file was not scanned due to xyz and it is his decision if he wants to download the content unscanned.

### Helm

Build the chart and install it.

```bash
export RELEASE=example
helm repo add truecharts https://charts.truecharts.org/
helm dependency update helm/core-mw/
helm upgrade --install anti-virus-example helm/core-mw/ -namespace ${RELEASE} --create-namespace
```

# Testing

To test that your entire stack is working as expected, you can upload some harmless test viruses on your test user's account (send them either as a mail attachment, or upload them as PIM attachment or in his drive account) and then try downloading them via the OX App Suite UI. Harmless test viruses can be found [here](https://www.ikarussecurity.com/support/virus-info/test-viruses/).