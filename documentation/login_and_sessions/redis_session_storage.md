---
title: Redis Session Storage
icon: far fa-clock
tags: Session, Installation, Configuration
---

# Introduction

Client sessions are managed and stored using [Redis in-memory data structure store](https://redis.io/). The Redis store is used as central session storage replacing the formerly used in-memory per-node SessionD and (Hazelcast-backed) session storage.

Since it acts as a central session storage, many problems related to remote invalidation of sessions are thus solved while maintaining the benefits offered by Hazelcast-backed session storage: sessions survive server restarts and/or upgrades, and are accessible for each node, independently from where the session got spawned.

# Configuration

This feature is included in ``open-xchange-core`` package. Thus, no additional packages need to be enabled.

Of course, a running Redis installation is required and appropriate configuration settings for the Open-Xchange Redis Connector need to be defined.

```
# Properties for Redis connector

# Configure accessing Redis end-point
com.openexchange.redis.mode=standalone
com.openexchange.redis.hosts=localhost:6379
com.openexchange.redis.ssl=false
com.openexchange.redis.starttls=false

# If left empty means no user-name and/or password required
com.openexchange.redis.username=
com.openexchange.redis.password=
```

There are a bunch more properties that can be set.

# Configuration

As mentioned previously, access to Redis needs to be configured properly.

# Sizing

With session data being no longer stored de-centralized on middleware pods, but centrally within Redis, the demand for memory is shifted 
accordingly. Therefore, sufficient memory should be assigned to this Redis pods so that no session data gets lost unexpectedly. 

During tests with synthetic data, we observed a memory consumption of approximately **2GB for 1M stored sessions**. 

However, this might vary depending on the actual usage patterns, additionally stored session attributes etc. Therefore, it is important 
to monitor the respective metrics and adjust the memory settings as needed.

