---
title: Extended Logging
icon: fa-stream
---

For some use cases dedicated loggers / logging options are available to gather more information as normally exposed by the logging. This goes beyond existing debug logs and provides additional insights e. g. on which client is executing which call.

# Additional SOAP logging for context/user calls

To get more insights on SOAP API provisioning calls of contexts and users this additional logging was introduced. If enabled by configuration (see below) all interface methods of OXContextService and OXUserService that create, change or delete contexts/users will be written into the log in a parsable format.

## Configuration

To enable this logging either one of the following configuration properties need to be set. In order to get the information logged one of the properties need to match against the mentioned MDC entries, which in turn are based on HTTP header provided through the provisioning call.

- `com.openexchange.logging.extensions.authUsernames`: A comma separated list of auth user names. This includes authenticated logins which could be a brand, a sub-brand, a context admin or a useradmin and will be send with each OX Cloud API request to the OX middleware in a header named `X-OX-Auth-Username`. The log filter will check this entry against the special MDC entry `com.openexchange.soap.authUsername`.
- `com.openexchange.logging.extensions.authBrandnames`: A comma separated list of brand names. The information to verify might come with OX Cloud API request header `X-OX-Auth-Brandname`. The log filter will check this entry against the special MDC entry `com.openexchange.soap.authBrandname`.
- `com.openexchange.logging.extensions.hosts`: A comma separated list of hosts or IP addresses. The host or IP addresses of the hostname which was used to access the middleware API. The log filter will check this entry against the special MDC entries `com.openexchange.originalHostname` and `com.openexchange.grizzly.serverName`.
- `com.openexchange.logging.extensions.remoteAddresses`: A comma separated list of clients IP addresses. The IP address of the remote client which was used to access the middleware API. The log filter will check this entry against the special MDC entries `com.openexchange.grizzly.remoteAddress` and `com.openexchange.grizzly.originalRemoteAddress`.

## Parsable logs

To be able to parse and evaluate the logs the following notation is used:

	ExtensionLogs:<operation>:<objectType>:<result>:<operationExecutor>:<contextId>:<userId(s)>

While `ExtensionLogs` is set the values for `<operation>`, `objectType`, `<result>`, `<operationExecutor`, `<id>` for context and `<userIds>` for user might vary:

	* operation - defines which operation was executed. Possible values are `create`, `change`, `delete`, `activate`, `deactivate` and `downgrade`
	* objectType - shows tge type of object that was involved, e. g. `context`, `user`, `capability`, `quota`, ...
	* resultType - The result of the operation: `ok` or `ko`
	* operationExecutor - The login of the user executing the operation
	* contextId - The context id (if available) the operation relates to
	* userIds - The user id(s) the operation relates to. If more than one user id is available they will be separated by ','


Following this notation identifiers for such logs might be:

	ExtensionLogs:create:context:ok:oxadminmaster:53
	ExtensionLogs:change:context:ok:oxadminmaster:53
	ExtensionLogs:create:user:ok:oxadmin:53:4
	ExtensionLogs:delete:context:ok:oxadminmaster:53

Following a concrete example where the oxadminaster tried to create context 1 but it failed.

	20XX-MM-DDTHH:MM:SS,730+0200 TRACE [OXWorker-0000010] com.openexchange.admin.plugin.hosting.rmi.impl.OXContext.create(OXContext.java:651) 
		ExtensionLogs:create:context:ko:oxadminmaster:1

## Additional notes

Following some additional information that might be of interest when using this feature.

- when enabled the logger will always log on level TRACE regardless on what was configured globally.


