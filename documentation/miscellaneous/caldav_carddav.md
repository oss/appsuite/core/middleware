---
title: CalDAV and CardDAV
icon: fa-address-card
tags: DAV, CalDAV, CardDAV
---

# General

The Open-Xchange server can be accessed via it's CalDAV- and CardDAV-interfaces to allow the synchronization of Calendar- and Contact-data with external applications like the Mac OS Calendar and Address Book clients.

CalDAV and CardDAV are standard protocols for the exchange of calendar data and address data respectively. The CalDAV interface publishes all the user's calendar folders via CalDAV so the user can subscribe to them in a client application. Similarly, the CardDAV interface publishes the user's contact folders. Depending on the used client, the user can either subscribe one or more folders, or access all available data in an aggregated way.

# Istio Configuration

In order to redirect DAV requests to the appropriate servlets, the Istio's virtual service configuration may need to be adjusted using one of the following alternatives. Please note that most clients will refuse to establish connections to non-SSL servers, so a proper SSL configuration is required.

In contrast to other, browser-based clients, most CalDAV- and CardDAV-clients don't preserve cookies when making requests, so that there's no session-stickiness possible. Therefore, it's recommended to route this traffic to a single middleware node in the cluster statically, so that consecutive requests will not encounter stale data in caches that is not yet invalidated.

## Via dedicated DAV host

Please edit your <code>values.yaml</code>, so that the DAV host is added to a virtual service:

```
##
## App Suite Stack chart configuration
##
appsuite-pro:
  istio:
    enabled: true
    ingressGateway:
      name: "istio-system/gateway"
    virtualServices:
      appsuite:
        hosts:
          - "MYSERVER.TLD"
      dav:
        hosts:
          - "dav.MYSERVER.TLD"
```

To make sure that <code>dav.MYSERVER.TLD</code> is reachable, you will have to
 * adjust the SSL/TLS configuration as needed
 * add the <code>dav.MYSERVER.TLD</code> host to the ingress gateway
 * add an entry for this name to your DNS configuration.

### How it works

The following picture shows an abstract request and response flow with the above configuration. Please note that the *DAV servlets* are registered under `/servlet/dav` and that the proxy balancer adds this path before transmitting the request to the server.

![Execution flow](istio_dav_routing_virtual_service.png "Istio DAV routing flowchart with virtual service")

## Via prefix path configuration

Starting with v7.10.3, it is also possible to provide access to CalDAV- and CardDAV-servlets under a designated path. Client requests will then no longer be targeted at the server's root level, but will include the advertised prefix path instead. This prefix path needs to be configured in the middleware (core-mw) `values.yaml` like in the example below:

```
##
## Middleware chart configuration
##
core-mw:
  properties:
    com.openexchange.dav.prefixPath: "/dav"
    com.openexchange.dav.proxyPrefixPath: "/"
```

> Note: For further information about the properties, have a look at [com.openexchange.dav.prefixPath](https://documentation.open-xchange.com/components/middleware/config{{ site.baseurl }}/#com.openexchange.dav.prefixPath) and [com.openexchange.dav.proxyPrefixPath](https://documentation.open-xchange.com/components/middleware/config{{ site.baseurl }}/#com.openexchange.dav.proxyPrefixPath)

With this configuration, the server will register all *DAV servlets* with the prefix <code>/dav/*</code>, thus e.g. the CalDAV servlet will be reachable under <code>MYSERVER.TLD/dav/caldav</code>. To ensure requests will reach the server, the DAV virtual service needs to be configured, too:

```
##
## App Suite Stack chart configuration
##
appsuite-pro:
  istio:
    enabled: true
    ingressGateway:
      name: "istio-system/gateway"
    virtualServices:
      appsuite:
        hosts:
          - "MYSERVER.TLD"
      dav:
        hosts:
          - "MYSERVER.TLD"
        extraRoutes:
          - name: "dav-root-route"
            matchPrefix:
              - "/dav"
            destinationHost: "core-mw-sync"
```

It is highly recommended to use <code>/dav</code> as path prefix. Clients like the macOS Calendar will automatically search under this path if e.g. only <code>MYSERVER.TLD</code> is entered as server address.

### How it works

The following picture shows an abstract request and response flow with the above configuration. Please note that the *DAV servlets* are registered under `/dav` and that the middleware is fully responsible for handling the path.

![Execution flow](istio_dav_routing_prefix.png "Istio DAV routing flowchart with prefix path")

# Autodiscovery

By providing some DNS service name registrations for your domain, it's possible for some clients to automatically discover the account settings by just providing the user's e-mail address and password. This procedure is specified in [RFC 6764](http://tools.ietf.org/html/rfc6764).

The following example illustrates the DNS entries where MYSERVER.TLD would be the domain name of the ox-server, both for CalDAV and CardDAV via HTTP and HTTPS on the virtual host dav.MYSERVER.TLD:

```
 _caldavs._tcp.MYSERVER.TLD.      10800 IN SRV      10 1 443 dav.MYSERVER.TLD.
 _caldav._tcp.MYSERVER.TLD.       10800 IN SRV      10 1  80 dav.MYSERVER.TLD.
 _carddavs._tcp.MYSERVER.TLD.     10800 IN SRV      10 1 443 dav.MYSERVER.TLD.
 _carddav._tcp.MYSERVER.TLD.      10800 IN SRV      10 1  80 dav.MYSERVER.TLD.
```

The rewrite-rules for the well-known aliases are already added to the Istio DAV configuration by default.

In the case of not serving the DAV service on the vhost root additionally some DNS TXT records are recommended, pointing to the path where the *DAV servlets are registered:

```
 _caldavs._tcp.MYSERVER.TLD.      10800 IN TXT   path=/servlet/dav
 _caldav._tcp.MYSERVER.TLD.       10800 IN TXT   path=/servlet/dav
 _carddavs._tcp.MYSERVER.TLD.     10800 IN TXT   path=/servlet/dav
 _carddav._tcp.MYSERVER.TLD.      10800 IN TXT   path=/servlet/dav
```

# Middleware Configuration

> Note: For further information about the properties, have a look at [CalDAV Properties](https://documentation.open-xchange.com/components/middleware/config{{ site.baseurl }}/#mode=features&feature=CalDAV) and [CardDAV Properties](https://documentation.open-xchange.com/components/middleware/config{{ site.baseurl }}/#mode=features&feature=CardDAV)

## CalDAV Configuration

The following configuration options are available in the <code>values.yaml</code>:

### com.openexchange.caldav.enabled
The property '''com.openexchange.caldav.enabled''' governs whether a user has access to the CalDAV interface. This can be configured along the config cascade, in the default setting, everyone that has access to the infostore also has access to caldav. This is achieved in the following way:

```
##
## Middleware Stack chart configuration
##
core-mw:
  contextSets:
    premium:
      withTags: ucInfostore
      com.openexchange.caldav.enabled: "true"

  properties:
    com.openexchange.caldav.enabled: "false"
```

This means: In general CalDAV is turned off, but using the <code>contextSets</code> feature of the config cascade it is turned on for everyone that has infostore access.

### com.openexchange.caldav.tree
Configures the ID of the folder tree used by the CalDAV interface. Currently, this should be set to the default value of '0'.

### com.openexchange.caldav.interval.start
Defines the minimum end time of appointments to be synchronized via the CalDAV interface, relative to the current date. Possible values are "one_month" (default), "one_year" and "six_months".

### com.openexchange.caldav.interval.end
Defines the maximum start time of appointments to be synchronized via the CalDAV interface, relative to the current date. Possible values are "one_year" (default) and "two_years".

### com.openexchange.caldav.url
Tells users where to find a caldav folder. This can be displayed in frontends. You can use the variables [hostname] and [folderId]. If you chose to deploy caldav as a virtual host (say 'dav.open-xchange.com') use <code>https://dav.open-xchange.com/caldav/[folderId]</code> as the value. If you are using user-agent sniffing use <code>https://[hostname]/caldav/[folderId]</code>.

## CardDAV Configuration

The following configuration options are available in the <code>values.yaml</code>:

### com.openexchange.carddav.enabled
Similarly to CalDAV, the property <code>com.openexchange.carddav.enabled</code> governs whether CardDAV is available for a certain user. This is configured exactly like CalDAV with the config cascade only enabling this for users that have access to the infostore:

```
##
## Middleware Stack chart configuration
##
core-mw:
  contextSets:
    premium:
      withTags: ucInfostore
      com.openexchange.carddav.enabled: "true"

  properties:
    com.openexchange.carddav.enabled: "false"
```

### com.openexchange.carddav.ignoreFolders
A comma-separated list of folder IDs to exclude from the synchronization. Use this to disable syncing of very large folders (e.g. the global address list in large contexts, which always has ID 6). By default, no folders are excluded.

### com.openexchange.carddav.tree
Configures the ID of the folder tree used by the CardDAV interface. Currently, this should be set to the default value of '0'.

### com.openexchange.carddav.exposedCollections
Controls which collections are exposed via the CardDAV interface. Possible values are '0', '1' and '2'. A value of '1' makes each visible folder available as a resource collection, while '2' only exposes an aggregated collection containing  all contact resources from all visible folders. The default value '0' exposes either an aggregated collection or individual collections for each folder, depending on the client's user-agent that is matched against the pattern in 'userAgentForAggregatedCollection'.

### com.openexchange.carddav.userAgentForAggregatedCollection
Regular expression to match against the client's user-agent to decide whether the aggregated collection is exposed or not. The default pattern matches all known varieties of the macOS Address-book client, that doesn't support multiple collections. Only used if 'exposedCollections' is set to '0'. The pattern is used case-insensitive.

### com.openexchange.carddav.reducedAggregatedCollection
Specifies if all visible folders are used to create the aggregated collection, or if a reduced set of folders only containing the global addressbook and the personal contacts folders should be used. This setting only influences the aggregated collection that is used for clients that don't support multiple collections. Possible values are 'true' and 'false.

### com.openexchange.carddav.url
Tells users where to find a CardDAV folder. This can be displayed in frontends. You can use the variables <code>[hostname]</code> and <code>[folderId]</code>. If you chose to deploy CardDAV as a virtual host (say 'dav.example.com') use <code>https://dav.example.com/carddav/[folderId]</code> as the value. If you are using user-agent sniffing use <code>https://[hostname]/carddav/[folderId]</code>.
 