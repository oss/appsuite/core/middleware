---
title: Migration Guide - Transition from GCM to FCM
icon: fa-fire
tags: Push, Installation, Configuration
---

Google has officially [announced](https://firebase.google.com/support/faq?hl=de#fcm-23-deprecation) the deprecation of the existing HTTP API for sending push notifications via Firebase Cloud Messaging (FCM), effective from June 20, 2023, with plans to shut it down on June 21, 2024. In response to this, a new FCM API, known as "v1," has been introduced as a replacement.

Firebase Cloud Messaging (FCM) represents the modern successor to Google Cloud Messaging (GCM) and serves as the primary channel for delivering messages to both Android and iOS devices. This comprehensive migration guide is designed to assist you in seamlessly transitioning from GCM to FCM within your deployment infrastructure.

## Prerequisites

Before embarking on the migration process, it's essential to ensure that the following prerequisites are met:

**For On-Premises Deployments:**
- Creation of a Firebase project through the [Firebase Console](https://console.firebase.google.com/).
- Successful integration of your application with GCM and a prior migration to FCM. Detailed instructions for this step are available in Google's official [migration guide](https://firebase.google.com/docs/cloud-messaging/migrate-v1).

**For OX Cloud Deployments:**
- Installation or updating of the `drive-restricted` component to its latest version, inclusive of the FCM keys.
- Upgrade of the OX Drive mobile app (for testing purposes) to the latest version, which includes the essential FCM keys.

## Database Modifications

As part of the migration, several database changes will be implemented. Specifically, two database update tasks are in place to facilitate the transition. These tasks are responsible for renaming the service identifiers from GCM to FCM for both drive events and the generic PNS transport layer. The two tasks involved are as follows:
- `com.openexchange.drive.events.fcm.groupware.RenameGCM2FCMUpdateTask`
- `com.openexchange.pns.transport.fcm.groupware.RenameGCM2FCMUpdateTask`

## Configuration

In terms of configuration adjustments, there is minimal need for modification beyond the inclusion of the key `_type: fcm` within each client configuration in the `pushClientConfig/*.yml` files. These configurations should also be adjusted to point to the appropriate key using the `keyPath` property. For example:

```yaml
drive-mobile-android:
  _type: fcm
  keyPath: /var/lib/firebase/key-01.json
  
open-xchange-mobile-api-facade-android:
  _type: fcm
  keyPath: /var/lib/firebase/key-02.json
```

Furthermore, the properties `com.openexchange.drive.events.gcm.enabled` and `com.openexchange.pns.transport.gcm.enabled` were renamed to `com.openexchange.drive.events.fcm.enabled` and `com.openexchange.pns.transport.fcm.enabled` and should be set to true to enable PNS and drive-based push notifications.

For further fine-tuning of your FCM configuration, please refer to the [Push Notification Service]({{ site.baseurl }}/middleware/push_notifications/push_notification_service.html).

This migration guide should enable you to smoothly transition from GCM to FCM and ensure the continued effectiveness of your push notification infrastructure. 
