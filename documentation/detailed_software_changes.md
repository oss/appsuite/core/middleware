---
title: Detailed software changes
icon: fa fa-info-circle
classes: no-counting
---

This page contains detailed information about software changes.



[8.35]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1513
**Summary**: Update Apache Commons CSV from v1.6 to v1.13.0

* Update Apache Commons CSV from v1.6 to v1.13.0 in target platform (`com.openexchange.bundles`)
* Update Apache Commons IO from v1.16.1 to v1.18.0 in target platform (`com.openexchange.bundles`)

### SCR-1512
**Summary**: Updated Apache Commons CLI library from v1.6.0 to v1.9.0

Updated Apache Commons CLI library from v1.6.0 to v1.9.0 in target platform (`com.openexchange.bundles`)

### SCR-1511
**Summary**: Update Apache Commons Codec

Update Apache Commons Codec from v.1.17.0 to v1.17.2 in target platform (`com.openexchange.bundles`)

### SCR-1507
**Summary**: Added Apache Aries SPI Fly to target platform

Added Apache Aries SPI Fly to target platform. SPI Fly is the Reference Implementation of the OSGi ServiceLoader Mediator specification. This is needed to due to update of logging-related libraries, in which SLF4J changed from static initialization to the usage of Java's `java.util.ServiceLoader`.

Added bundles to target platform:

* asm-9.6.jar
* asm-analysis-9.6.jar
* asm-commons-9.6.jar
* asm-tree-9.6.jar
* asm-util-9.6.jar
* org.apache.aries.spifly.dynamic.bundle-1.3.7.jar

### SCR-1506
**Summary**: Updated logging-related libraries

Updated logging-related libraries in target platform - thereof SLF4J API and Logback as well as associated logging bridges

* Updated slf4j-api from v1.7.36 to v2.0.16
* Updated logback-core from v1.2.13 to v1.5.16
* Updated logback-classic from v1.2.13 to v1.5.16
* Updated jcl-over-slf4j from v1.7.36 to v2.0.16
* Updated jul-to-slf4jj from v1.7.36 to v2.0.16
* Updated log4j-over-slf4j from v1.7.36 to v2.0.16
* Updated osgi-over-slf4j from v1.7.36 to v2.0.16

API - HTTP-API
--------------

### SCR-1525
**Summary**: Added field "sharedreadonly" to "snippet" module

Added field "sharedreadonly" to "snippet" module. It provides the information whether a shared snippet may be seen, but must not be modified/deleted by other users.


```json
{
   "id":"3",
   "content":"...",
   "createdby":3,
   "displayname":"My signature",
   "misc":{
      "insertion":"below",
      "content-type":"text/html"
   },
   "module":"io.ox/mail",
   "type":"signature",
   "shared":false,
   "sharedreadonly":false
}
```


API - Java
----------

### SCR-1509
**Summary**: Update cxf-libraries to 3.5.10

Update cxf-libraries from v3.5.9 to v3.5.10:

* cxf-core
* cxf-rt-bindings-soap
* cxf-rt-bindings-xml
* cxf-rt-databinding-jaxb
* cxf-rt-features-logging
* cxf-rt-frontend-jaxws
* cxf-rt-frontend-simple
* cxf-rt-transports-http
* cxf-rt-ws-addr
* cxf-rt-wsdl
* cxf-rt-ws-policy

### SCR-1496
**Summary**: Enhanced OAuthAuthorizationService#validateAccessToken with Header collection parameter

The method `com.openexchange.oauth.provider.authorizationserver.spi.OAuthAuthorizationService#validateAccessToken` is enhanced with the additional parameter `com.openexchange.servlet.Headers` holding the headers of the underlying HTTP servlet request that is used to trigger the authentication request. 

A default implementation of the new method overload is added, which delegates to the existing method passing the extracted access tokens only.



Configuration
-------------

### SCR-1521
**Summary**: Added config switch to keep own address when replying to self-sent message

Added new lean boolean property to configure whether to keep own address when replying to self-sent message


 * `com.openexchange.mail.keepOwnAddressWhenReplyingToSelfSentMail`
  Define whether to keep own address when replying to self-sent message. Default value is `"false"`. Reloadable and config-cascade aware.

### SCR-1514
**Summary**: New property 'com.openexchange.cache.v2.redis.multiKeyLimit'

In order to limit the maximum number of addressed keys per `MGET`, `MSET`, `MHGET` or `MHSET` command, the lean configuration property

```text
com.openexchange.cache.v2.redis.multiKeyLimit
```


is introduced. It defaults to `100`, and is neither reloadable, nor config-cascade aware.

### SCR-1510
**Summary**: Changed defaults for Client-Onboarding YAML configuration file

Changed defaults in `client-onboarding-scenarios.yml` YAML configuration file:


* Removed sections
        ** Removed section for identifier `mailappinstall` referencing discontinued OX Mail App
* Changed attribute `enabled` to `true`
        ** Changed attribute `enabled` to `true` for section `driveappinstall` (OX Drive App)
        ** Changed attribute `enabled` to `true` for section `syncappinstall` (Sync App)
        ** Changed attribute `enabled` to `true` for section `davsync` (CalDAV & CardDAV Sync)
        ** Changed attribute `enabled` to `true` for section `davmanual` (CalDAV & CardDAV Sync)
        ** Changed attribute `enabled` to `true` for section `eassync` (Exchange ActiveSync)
        ** Changed attribute `enabled` to `true` for section `easmanual` (Exchange ActiveSync)
        ** Changed attribute `enabled` to `true` for section `mailsync` (IMAP/SMTP)
        ** Changed attribute `enabled` to `true` for section `mailmanual` (IMAP/SMTP)


### SCR-1499
**Summary**: Added new properties to configure the new async framework

The new async framework introduces a few new lean properties which control the behaviour of framework.


* `com.openexchange.admin.ctx.async.enabled`
  Enables asynchronous deletion of contexts. Default value is `false` and config-cascade aware.
* `com.openexchange.admin.async.schedule`
  The schedule to run async admin operations. No default value and not config-cascade aware.
* `com.openexchange.admin.async.pool.size`
  The size of the threadpool which is responsible to run async tasks. The greater the pool the more tasks can be executed in parallel. Default value is 10 and not config-cascade aware.
* `com.openexchange.admin.async.refresh.interval`
  The interval in minutes in which claims of running tasks are refreshed. This must always be lower than `com.openexchange.admin.async.retry.interval`. Default value is 5 and not config-cascade aware.
* `com.openexchange.admin.async.retry.limit`
  The amount of times a failed async task is tried to be repeated. Default value is 5 and not config-cascade aware.
* `com.openexchange.admin.async.retry.interval`
  Defines the interval in minutes after a task is considered stale.  Or in other words a task is considered stale if: now > last update + interval. Default value is 15 and not config-cascade aware.
* `com.openexchange.admin.async.refill.interval`
  Defines the interval in minutes in which new tasks are added to the queue of available tasks.  This happens only during the defined schedule (see `com.openexchange.admin.async.schedule`). Default value is 5 and not config-cascade aware.
* `com.openexchange.admin.async.cleanup.retentionDays`
  The maximum amount of days a task is stored. Default value is 730 and not config-cascade aware.
* `com.openexchange.admin.async.cleanup.interval`
  The interval between runs of the cleanup task.  A value of 0 disables the task. Default value is 7 and not config-cascade aware.

### SCR-1486
**Summary**: New property `com.openexchange.carddav.addressbookMultigetLimit`

The lean configuration property `com.openexchange.carddav.addressbookMultigetLimit` is introduced to configure the maximum number of elements included in `CARDDAV:addressbook-multiget` responses to the client. 

If data from more elements was requested, `HTTP/1.1 507 Insufficient Storage` responses will get inserted. It defaults to `1000`, a value of `-1` disabled the limit. Property is _reloadable_ and can be defined through the config-cascade.


Database
--------

### SCR-1524
**Summary**: Added column "shared_read_only" to "snippet" table


<warning>
**Update Task**
com.openexchange.snippet.mime.groupware.SnippetAddReadOnlyColumnUpdateTask
</warning>


Added column "shared_read_only" to "snippet" table. It stores the information whether a shared snippet may be seen, but must not be modified/deleted by other users.


```sql
  `shared_read_only` tinyint(3) unsigned NOT NULL DEFAULT 0
```


Full table layout is now:


```sql
CREATE TABLE `snippet` (
  `cid` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `id` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `accountId` int(10) unsigned DEFAULT NULL,
  `displayName` varchar(255) NOT NULL,
  `module` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `shared` tinyint(3) unsigned DEFAULT NULL,
  `refType` tinyint(3) unsigned NOT NULL,
  `refId` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lastModified` bigint(64) NOT NULL,
  `size` int(10) unsigned DEFAULT NULL,
  `shared_read_only` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`cid`,`user`,`id`),
  KEY `indexShared` (`cid`,`shared`),
  KEY `indexRefType` (`cid`,`user`,`id`,`refType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
```


### SCR-1497
**Summary**: Added a new table async_tasks to the configdb database


<warning>
**Update Task**
8:addAsyncTasksTable
</warning>


The new async framework introduces a new table _async_tasks in the configdb_ which is responsible to store asynchronous tasks.

That table is created by the liquibase task _8:addAsyncTasksTable._


```sql
CREATE TABLE `async_tasks` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `entityId` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `args` text DEFAULT NULL,
  `state` varchar(100) NOT NULL,
  `lastUpdate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `claim` BINARY(16) DEFAULT NULL,
  `retryCount` int unsigned NOT NULL DEFAULT 0,
  `error` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entity_type_unique` (`entityId`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
```




Packaging/Bundles
-----------------

### SCR-1522
**Summary**: Updated Snappy library from v1.1.10.5 to v1.1.10.7

Updated Snappy library from v1.1.10.5 to v1.1.10.7 in Target Platform (`com.openexchange.bundles`)

### SCR-1508
**Summary**: Added new SLF4J API fragment bundle

Added new SLF4J API fragment bundle `org.slf4j.fragment` to `open-xchange-core` package/bundle list.

Purpose of this bundle is to make package `ch.qos.logback.classic.spi` available to SLF4J API bundle in order to inject Logback's SLF4j service provider into initialization phase of the SLF4J logging runtime.

### SCR-1498
**Summary**: Added the new bundle com.openexchange.admin.async

For the new async framework the new bundle "com.openexchange.admin.async" is added to the open-xchange-admin package



[8.34]
======

API - HTTP-API
--------------

### SCR-1489
**Summary**: New additional folder field 'com.openexchange.carddav.url' (id 3221)

The "detailed folder data" model of the HTTP API is extended by the additional folder field `com.openexchange.carddav.url` with column id `3221`. The _read only_ field is available for address book folders that are synchronizable via CardDAV, and points to the actual collection's endpoint as used by CardDAV clients.

See the [documentation](https://documentation.open-xchange.com/components/middleware/http/main/index.html?version=main#detailed-folder-data) for further details.



API - Java
----------

### SCR-1496
**Summary**: Enhanced OAuthAuthorizationService#validateAccessToken with Header collection parameter

The method `com.openexchange.oauth.provider.authorizationserver.spi.OAuthAuthorizationService#validateAccessToken` is enhanced with the additional parameter `com.openexchange.servlet.Headers` holding the headers of the underlying HTTP servlet request that is used to trigger the authentication request. 

A default implementation of the new method overload is added, which delegates to the existing method passing the extracted access tokens only.



Behavioral Changes
------------------

### SCR-1490
**Summary**: Change of Key Format used for Redis Cache

For the Redis instance dedicated to caching purposes, operation mode _Cluster_ may be used to scale out the Redis service horizontally. Doing so, the keyspace of the stored data is distributed evenly among the available Redis nodes. Technically, every master node in the cluster is responsible for certain hash slots. These slots are the underlying unit of sharding and calculated dynamically from the targeted key(s). Based on that, a command is routed based on its key's hash slot to the corresponding Redis node. If more than one node is targeted by a single command (e.g. `DEL` or `MGET`), single commands are executed in a fork/join manner against the correct Redis nodes automatically.

In order to prevent commands targeting multiple keys being distributed among multiple nodes, most of the cache keys that are actually associated with a certain OX context are changed so that the context identifier becomes the designated part of the key where the hash slot is calculated for, by surrounding it with curly braces (`\{` and `\`}). For example, a typical key used to cache user data changes from `ox-cache:usr:v1:5:12` to `ox-cache:usr:v1:\{5\}:12`

Due to this change, existing cached values using the previous key format will no longer be used when a rolling upgrade is performed, and will evict eventually (after `com.openexchange.cache.v2.defaultExpirationSeconds`, one hour by default). During this period, an increased memory usage will be noticeable after the middleware containers were updated. Therefore it is important that Redis is configured with an appropriate _max-memory policy_, especially if there's not much headroom left regarding the available usage. Alternatively, one can also flush all cached data once after the upgrade (using the `FLUSHALL` or `FLUSHDB` command manually). 

### SCR-1487
**Summary**: Health Check extended by Redis Cache

The health check of core middleware is extended with an check named `redis.cache` for the Redis connector attached to the instance used for caching purposes. It automatically becomes enabled once a Redis cache instance is configured via `com.openexchange.redis.cache.enabled=true`, and performs an operational check via `PING` command when called.

Like other checks, it can explicitly be disabled by including `redis.cache` in the value of property `com.openexchange.health.skip`. Or, to enable it, but not account its status to the overall result, it can be added to `com.openexchange.health.ignore`. 

See [the documentation](https://documentation.open-xchange.com/main/middleware/monitoring/01_health_checks.html) for further details.




Changed defaults
----------------

### SCR-1495
**Summary**: Disable Global Folder Cache by Default

Whether the so called "global folder cache" is enabled or not can be controlled via property `com.openexchange.folderstorage.cache.enableGlobalFolderCache`. 

To avoid redundancies with the low-level database folder cache, its default value will change from `true` to `false`. 

The absence of the additional caching layer will lower the memory requirements, as well as reduce the number of issued commands towards the Redis cache pod(s). However, since raw folder data is now always post-processed dynamically, this may lead to a slightly increased load on the middleware pods depending on client access patterns. 



Configuration
-------------

### SCR-1488
**Summary**: New Property `com.openexchange.carddav.url`

In order to display the CardDAV endpoint of address book collections to users in App Suite, a new lean configuration property named `com.openexchange.carddav.url` is introduced. Through this property, a URL template can be specified using the variables `[hostname]` and `[folderId]`. 

The property can be defined through the config-cascade and is _reloadable_. It defaults to `https://[hostname]/carddav/[folderId]`.

See the [property documentation](https://documentation.open-xchange.com/components/middleware/config/8/#mode=search&term=com.openexchange.carddav.url) for further details.

### SCR-1486
**Summary**: New property 'com.openexchange.carddav.addressbookMultigetLimit'

The lean configuration property `com.openexchange.carddav.addressbookMultigetLimit` is introduced to configure the maximum number of elements included in `CARDDAV:addressbook-multiget` responses to the client. 

If data from more elements was requested, `HTTP/1.1 507 Insufficient Storage` responses will get inserted. It defaults to `1000`, a value of `-1` disabled the limit. Property is _reloadable_ and can be defined through the config-cascade.


Database
-------------

### SCR-1468
**Summary**: Add table for webauthn data

In order to implement webauthn for multifactor and authentication, a database table needs to be created to store the public key, signature count, and additional relevant data about the authenticator.

Create a table within the oxdatabase shards:

```sql
CREATE TABLE web_authn
    `cid` int(11) DEFAULT NULL,
    `id` int(11) DEFAULT NULL,
    `deviceId` varchar(100) NOT NULL,
    `keyId` varchar(500) NOT NULL,
    `userId` varchar(100) DEFAULT NULL,
    `name` varchar(100) DEFAULT NULL,
    `login` varchar(100) DEFAULT NULL,
    `publicKey` varchar(1000) DEFAULT NULL,
    `attestation` varchar(10000) DEFAULT NULL,
    `clientData` varchar(2000) DEFAULT NULL,
    `counter` int(11) DEFAULT NULL,
    `compromised` bit(1) DEFAULT NULL,
    `isMultifactor` bit(1) DEFAULT NULL,
    `discoverable` bit(1) DEFAULT NULL,
    `lastUsed` datetime DEFAULT NULL,
    `enabled` bit(1) DEFAULT NULL,
    PRIMARY KEY (cid,id,deviceId),
    KEY `web_authn_id_IDX` (`cid`) USING BTREE
    ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
```


Packaging/Bundles
-------------

### SCR-1491
**Summary**: Bundle to allow new MFA registrations using WebAuthn instead of U2F

Added new bundle `com.openexchange.webauthn` to allow new MFA registrations using WebAuthn instead of U2F. Also already providing code to implement webauthn as a "full" authentication service. The new bundle has been added to `open-xchange-multifactor` package.



[8.33]
======

General
-------

### SCR-1482
**Summary**: Added redis.tls chart value

Whether to use TLS to connect to the Redis endpoint can now be configured using `redis.tls.enabled` and `redis.cache.tls.enabled`.

### SCR-1480
**Summary**: Updated lettuce library from v6.5.0 to v6.5.1

Updated lettuce library from v6.5.0 to v6.5.1 in bundle `io.lettuce`


* lettuce-core-6.5.1.RELEASE.jar



3rd Party Libraries/License Change
----------------------------------

### SCR-1481
**Summary**: Updated Fabric8 libraries from v6.10.0 to v6.13.4

Updated Fabric8 libraries from v6.10.0 to v6.13.4 in "io.fabric8.kubernetes" bundle


* kubernetes-client-6.13.4.jar
* kubernetes-client-api-6.13.4.jar
* kubernetes-httpclient-jdk-6.13.4.jar
* kubernetes-model-admissionregistration-6.13.4.jar
* kubernetes-model-apiextensions-6.13.4.jar
* kubernetes-model-apps-6.13.4.jar
* kubernetes-model-autoscaling-6.13.4.jar
* kubernetes-model-batch-6.13.4.jar
* kubernetes-model-certificates-6.13.4.jar
* kubernetes-model-common-6.13.4.jar
* kubernetes-model-coordination-6.13.4.jar
* kubernetes-model-core-6.13.4.jar
* kubernetes-model-discovery-6.13.4.jar
* kubernetes-model-events-6.13.4.jar
* kubernetes-model-extensions-6.13.4jar
* kubernetes-model-flowcontrol-6.13.4.jar
* kubernetes-model-gatewayapi-6.13.4.jar
* kubernetes-model-metrics-6.13.4.jar
* kubernetes-model-networking-6.13.4.jar
* kubernetes-model-node-6.13.4.jar
* kubernetes-model-policy-6.13.4.jar
* kubernetes-model-rbac-6.13.4.jar
* kubernetes-model-resource-6.13.4.jar
* kubernetes-model-scheduling-6.13.4.jar
* kubernetes-model-storageclass-6.13.4.jar


### SCR-1479
**Summary**: Updated Netty libraries from v4.1.114 to v4.1.115

Updated Netty libraries from v4.1.114 to v4.1.115 in bundle `io.netty`


* netty-buffer-4.1.115.Final.jar
* netty-codec-4.1.115.Final.jar
* netty-codec-dns-4.1.115.Final.jar
* netty-codec-http2-4.1.115.Final.jar
* netty-codec-http-4.1.115.Final.jar
* netty-codec-socks-4.1.115.Final.jar
* netty-common-4.1.115.Final.jar
* netty-handler-4.1.115.Final.jar
* netty-handler-proxy-4.1.115.Final.jar
* netty-resolver-4.1.115.Final.jar
* netty-resolver-dns-4.1.115.Final.jar
* netty-transport-4.1.115.Final.jar
* netty-transport-native-unix-common-4.1.115.Final.jar



Configuration
-------------

### SCR-1485
**Summary**: New options for Redis Connector

Added new lean options for the Redis connector


* `com.openexchange.redis.connection.pool.newConnectionIfWaitExceeded`
  Specifies whether to establish a new connection if waiting for an available connection in pool is exceeded. Default value is `"true"`. Neither reloadable, nor config-cascade aware


* `com.openexchange.redis.cluster.periodicTopologyRefreshMillis`
  Defines the interval in milliseconds for periodic refreshing of the the cluster topology. Only effective if connecting against a Redis Cluster; e.g. `"com.openexchange.redis.mode"` is set to `"cluster"`. Default value is `"600000"`. Neither reloadable, nor config-cascade aware

### SCR-1477
**Summary**: New property to select default collection for new contacts via iOS / CardDAV

In the Contacts App on iOS, it is not possible to pick a certain folder while creating a new contact within the CardDAV account. Instead, the iOS client creates new contacts in a somehow randomly chosen folder of the account, which could also be the "Global Address Book" or the "Collected Contacts" folder, or even folders that are shared from others. Therefore, as the user cannot influence the target folder, an implicit workaround is in place so that new contacts are always created within the user's default contacts folder on App Suite. Within the iOS client, this is reflected after the next synchronization cycle as well.

To influence the applied fallback logic, the following lean configuration property is introduced:

```java
com.openexchange.carddav.iosFallbackToDefaultCollectionForNewResources 
```

Possibly settings are:

 * `always`: New contacts are always created within the user's default contacts folder on App Suite
 * `disabled`: Contacts are created within the folder targeted by the client, but rejected on insufficient permissions
 * `insufficientPermissions`: Contacts are created within the folder targeted by the client, falling back to the user's default folder on insufficient permissions

It defaults to `always` so that contacts are created in the user's default contacts folder independently of which folder is targeted by the client.

The new property is reloadable and config-cascade-aware.



[8.32]
======

General
-------

### SCR-1473
**Summary**: Updated lettuce library from v6.4.0 to v6.5.0

Updated lettuce library from v6.4.0 to v6.5.0in bundle `io.lettuce`


* lettuce-core-6.5.0.RELEASE.jar



3rd Party Libraries/License Change
----------------------------------

### SCR-1476
**Summary**: Updated Jackson libraries from v2.16.1 to v2.18.1 in target platfom

Updated several libraries to update Jackson libraries  from v2.16.1 to v2.18.1

*Target platform bundles (com.open-xchange.bundles)*

* stax2-api-4.2.1.jar replaced with stax2-api-4.2.2.jar
* jackson-annotations-2.16.1.jar replaced with jackson-annotations-2.18.1.jar
* jackson-core-2.16.1.jar replaced with jackson-core-2.18.1.jar
* jackson-databind-2.16.1.jar replaced with jackson-databind-2.18.1.jar
* jackson-dataformat-cbor-2.16.1.jar replaced with jackson-dataformat-cbor-2.18.1.jar
* jackson-dataformat-xml-2.16.1.jar replaced with jackson-dataformat-xml-2.18.1.jar
* jackson-datatype-jsr310-2.16.1.jar replaced with jackson-datatype-jsr310-2.18.1.jar
* jackson-datatype-jsr310-2.16.1.jar replaced with jackson-datatype-jsr310-2.18.1.jar
* jackson-datatype-jsr353-2.16.1.jar replaced with jackson-datatype-jsr353-2.18.1.jar
* jackson-jakarta-rs-base-2.16.1.jar replaced with jackson-jakarta-rs-base-2.18.1.jar
* jackson-jakarta-rs-json-provider-2.16.1.jar replaced with jackson-jakarta-rs-json-provider-2.18.1.jar
* jackson-jakarta-rs-xml-provider-2.16.1.jar replaced with jackson-jakarta-rs-xml-provider-2.18.1.jar
* jackson-module-jakarta-xmlbind-annotations-2.16.1.jar replaced with jackson-module-jakarta-xmlbind-annotations-2.18.1.jar
* jackson-module-jaxb-annotations-2.16.1.jar replaced with jackson-module-jaxb-annotations-2.18.1.jar

*Bundle com.ctc.wstx*

* woodstox-core-6.5.1.jar replaced with woodstox-core-7.0.0.jar

*Bundle org.yaml.snakeyaml*

* snakeyaml-2.2.jar replaced with snakeyaml-2.3.jar

### SCR-1472
**Summary**: Updated Netty libraries from v4.1.112 to v4.1.114

Updated Netty libraries from v4.1.112 to v4.1.114 in bundle `io.netty`



* netty-buffer-4.1.112.Final.jar
* netty-codec-4.1.112.Final.jar
* netty-codec-dns-4.1.112.Final.jar
* netty-codec-http2-4.1.112.Final.jar
* netty-codec-http-4.1.112.Final.jar
* netty-codec-socks-4.1.112.Final.jar
* netty-common-4.1.112.Final.jar
* netty-handler-4.1.112.Final.jar
* netty-handler-proxy-4.1.112.Final.jar
* netty-resolver-4.1.112.Final.jar
* netty-resolver-dns-4.1.112.Final.jar
* netty-transport-4.1.112.Final.jar
* netty-transport-native-unix-common-4.1.112.Final.jar




Configuration
-------------

### SCR-1475
**Summary**: New property to configure SSO Logout when OX Sessions are closed

In order to configure after which session removal events an OpenID Connect session is also closed on the provider side, the following lean configuration property is introduced:

```text
com.openexchange.oidc.opLogoutOnSessionRemoval
```


It specifies an optional comma-separated list of certain session removal events for which the logout endpoint of the OP should be invoked as well to terminate the OIDC session. This does not affect regular/explicit, client-initiated logout flows where the OP is always included as per `com.openexchange.oidc.ssoLogout`. Also, sessions spawned using the _Resource Owner Password Credentials Grant_ are not considered.

Configurable removal events include:

* `expired` - The session is removed after being idle/unused for a certain duration
* `user_closed` - Another session is removed explicitly by the user (usually via session management API)
* `admin_closed` - A session is removed explicitly via an administrative interface (e.g. _close sessions_ commandline utility or REST API)

The property is empty by default, reloadable, and not config-cascade-aware. See also the [documentation](https://documentation.open-xchange.com/8/middleware/login_and_sessions/openid_connect_1.0_sso.html#termination-of-op-sessions) for further details.

### SCR-1470
**Summary**: Added new lean property to control detection of inline images

Added new lean property `com.openexchange.mail.detectInlineImageByDispositionOnly` that controls whether to detect inline images solely by its value for `"Content-Disposition"` header (required to be `"inline"`) and to ignore any file name information (e.g through `"filename"` parameter).

### SCR-1462
**Summary**: Added new property to track Redis operation taking longer than a configured threshold

Added new lean property `com.openexchange.redis.operationExecutionTimeThreshold` to track Redis operation taking longer than a configured threshold. Default value is `0` (zero), therefore disabled by default. Not reloadable and not config-cascade aware.



[8.31]
======

Configuration
-------------

### SCR-1462
**Summary**: Added new property to track Redis operation taking longer than a configured threshold

Added new lean property `com.openexchange.redis.operationExecutionTimeThreshold` to track Redis operation taking longer than a configured threshold. Default value is `0` (zero), therefore disabled by default. Not reloadable and not config-cascade aware.



[8.31]
======

API - Java
----------

### SCR-1460
**Summary**: Constructor change in `com.openexchange.passwordchange.common.AbstractPasswordChangeService`

With dropping legacy caching bundles `com.openexchange.caching.*`, the constructor in `com.openexchange.passwordchange.common.AbstractPasswordChangeService` changed as `c.o.caching.CacheService` is no longer available



CLT
---

### SCR-1464
**Summary**: Dropped 'checkconfigconsistency' CLT

With removal of legacy `com.openexchange.caching` bundles, the `checkconfigconsistency` CLT is no longer needed



Configuration
-------------

### SCR-1466
**Summary**: Allow specifying the name of the HTTP header that forwards the originating remote port

Introduced new lean property `"com.openexchange.server.portHeader"` specifying the name of the HTTP header that forwards the originating remote port. Default value is `"X-Forwarded-Port"`. It is neither reloadable nor config-cascade aware.

### SCR-1465
**Summary**: New configuration property `com.openexchange.push.dovecot.unregisterAfterDelete`

Depending on the setup, it may not be suitable to attempt a de-registration of active push listeners on the mail server during user- or context deletion. Therefore, a new lean configuration property is introduced:

```java
com.openexchange.push.dovecot.unregisterAfterDelete
```

It defaults to `true` so that current semantics are not changed. The new configuration property is *reloadable*, yet not config-cascade aware.

### SCR-1462
**Summary**: Added new property to track Redis operation taking longer than a configured threshold

Added new lean property `com.openexchange.redis.operationExecutionTimeThreshold` to track Redis operation taking longer than a configured threshold. Default value is `0` (zero), therefore disabled by default. Not reloadable and not config-cascade aware.

### SCR-1461
**Summary**: Dropped legacy cache service properties

Dropped properties (referenced in `open-xchange-core/debian/postinst`):


* `com.openexchange.caching.jcs.remoteInvalidationForPersonalFolders`
* `com.openexchange.caching.jcs.enabled`
* `jcs.region.*`

Dropped from `system.properties`:

* `UserConfigurationStorage`
* `Cache`



Frontend
-----------------

### SCR-1469
**Summary**: New Setting for Preferred Calendar User Address

Introduced the JSLob entry `io.ox/calendar/preferredAddress` to let the end user control which email address is assigned to the corresponding attendee in calendar events, to e.g. avoid exposing of an unwanted email address. 
The value of the entry can always be set by the user, however is limited to the known aliases of the user.



Packaging/Bundles
-----------------

### SCR-1459
**Summary**: Dropped com.openexchange.caching.* bundles

As all caches are transformed to `com.openexchange.cache.v2` implementation, the legacy cache implementation in `com.openexchange.caching` and `com.openexchange.caching.events` bundles is dropped



[8.30]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1452
**Summary**: Moved Java Data Objects (JDO) API to target platform

Moved the Java Data Objects (JDO) API formerly held in bundle `com.openexchange.common` to target platform bundles managed in `com.openexchange.bundles`.

Therefore, the target platform (`com.openexchange.bundles`) has been extended by the libraries:


* `glassfish-corba-omgapi-4.2.4.jar`
* `jdo-api-3.2.1.jar`



API - HTTP-API
--------------

### SCR-1456
**Summary**: Removed "messaging"-related APIs

#### Removed HTTP-API paths


* `messaging/account`
* `messaging/message`
* `messaging/service`



API - Java
----------

### SCR-1458
**Summary**: Upgrade to Java 21

Middleware core will be upgraded to Java 21. This means that each bundle's required execution environment will now be JaveSE-21, and a compatible runtime JRE must be used.



Configuration
-------------

### SCR-1457
**Summary**: Removed "messaging"-related properties

Removed "messaging"-related properties


* "`com.openexchange.messaging.enabled`"

### SCR-1454
**Summary**: New configuration property "com.openexchange.cache.v2.redis.disableHashExpiration"

<warning>
**Breaking Change**
</warning>

A new lean configuration property `com.openexchange.cache.v2.redis.disableHashExpiration` is temporarily introduced for increased compatibility with older versions of Redis.

The property optionally disables field expiration of hash keys. If set to `true`, no `HEXPIRE` commands are invoked after setting values in hashes, resulting in _persistent_ values that will only be removed explicitly by the application, or due to a general `maxmemory` eviction policy of Redis like `allkeys-lru`.

It should therefore only be enabled if a dedicated Redis instance for cache-purposes is used (see `com.openexchange.redis.cache.enabled`)!

The property is neither config-cascade aware nor reloadable. It is only available for a temporary grace period and will be removed again in a future version - therefore it should be considered as _deprecated_ from the beginning.

### SCR-1442
**Summary**: Removed 'hazelcast-data-holding' and 'hazelcast-lite-member' roles

Since the core middleware no longer depends on Hazelcast, the roles `hazelcast-data-holding` and `hazelcast-lite-member` defined in the `core-mw` Helm chart are no longer needed. As a result, they will be removed in version 6.0.0.

However, OX Documents, which is still included in the middleware image, relies on Hazelcast and requires a headless service. Therefore, the headless service from the `hazelcast-data-holding` role has been moved to a new role named `documents`.

Please note that custom node definitions (`scaling.nodes`) must include this new role. Otherwise, the headless service will not be deployed, and OX Documents bundles will fail to start.

For more information, refer to the chart [documentation](https://gitlab.open-xchange.com/oss/appsuite/core/middleware/-/blob/main/helm/core-mw/README.md).



Packaging/Bundles
-----------------

### SCR-1455
**Summary**: Removed "messaging"-related functionality

Through removal of "messaging"-related functionality the following bundles and packages were dropped:

#### Bundles


* `com.openexchange.messaging`
* `com.openexchange.messaging.generic`
* `com.openexchange.messaging.json`
* `com.openexchange.messaging.rss`
* `com.openexchange.messaging.sms`

#### Packages


* `open-xchange-messaging`
* `open-xchange-messaging-sms`

### SCR-1427
**Summary**: Removed "MsService" and Parent Bundle "com.openexchange.ms"

The service `com.openexchange.ms.MsService` as well as its parent bundle `com.openexchange.ms` are no longer used and had been deprecated along with SCR-1342. Now they're removed from middleware core.



[8.29]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1451
**Summary**: Updated Google Guava from v33.2.1 to v33.3.0

Updated Google Guava from v33.2.1 to v33.3.0 in bundle `com.google.guava`

### SCR-1450
**Summary**: Removed org.eclipse.osgi.services helper bundle

Removed org.eclipse.osgi.services helper bundle in favor of individual org.osgi.service.X bundles

### SCR-1448
**Summary**: Upgraded dnsjava (an implementation of DNS in Java)

Upgraded dnsjava from v3.5.3 to v3.6.1 in target platform (`com.openexchange.bundles`)

### SCR-1440
**Summary**: Updated OSGi target platform bundles

Updated the following OSGi target platform bundles


* `org.apache.felix.gogo.runtime_1.1.4.v20210111-1007.jar` updated to `org.apache.felix.gogo.runtime_1.1.6.jar`
* `org.eclipse.osgi.util_3.7.200.v20230103-1101.jar` updated to `org.eclipse.osgi.util_3.7.300.v20231104-1118.jar`
* `org.eclipse.osgi_3.18.400.v20230509-2241.jar` updated to `org.eclipse.osgi_3.20.0.v20240509-1421.jar`

### SCR-1433
**Summary**: Updated lettuce library from v6.3.2 to v6.4.0

Updated lettuce library from v6.3.2 to v6.4.0in bundle `io.lettuce`


* lettuce-core-6.4.0.RELEASE.jar

### SCR-1432
**Summary**: Updated Netty libraries from v4.1.111 to v4.1.112

Updated Netty libraries from v4.1.111 to v4.1.112 in bundle `io.netty`


* netty-buffer-4.1.112.Final.jar
* netty-codec-4.1.112.Final.jar
* netty-codec-dns-4.1.112.Final.jar
* netty-codec-http2-4.1.112.Final.jar
* netty-codec-http-4.1.112.Final.jar
* netty-codec-socks-4.1.112.Final.jar
* netty-common-4.1.112.Final.jar
* netty-handler-4.1.112.Final.jar
* netty-handler-proxy-4.1.112.Final.jar
* netty-resolver-4.1.112.Final.jar
* netty-resolver-dns-4.1.112.Final.jar
* netty-transport-4.1.112.Final.jar
* netty-transport-native-unix-common-4.1.112.Final.jar



API - HTTP-API
--------------

### SCR-1406
**Summary**: Added an client defined expiration time to /token?action=acquireToken

The parameter `expiry` was added to the request `acquireToken`. Thus, a client is able to define an individual expiry for a login token, see also `/login?action=redeemToken`.
The expiry parameter will only be considered if the value is less the configured value through `com.openexchange.tokenlogin.maxIdleTime`

### SCR-1405
**Summary**: Renamed parameter in '/login?action=redeemToken'

The parameter `secret` has been renamed to `appId` since it is more fitting to the nature of the parameter. `secret` can still be used, but is deprecated from now on. It will be removed in upcoming releases.




API - Java
----------

### SCR-1428
**Summary**: Deprecation of JCS-based "CacheService"

After a new caching implementation has been introduced with `com.openexchange.cache.v2.CacheService`, the previously used, JCS-based `com.openexchange.caching.CacheService` is now deprecated and is scheduled for removal in a later release. Until then, the interfaces are available and the service is basically still usable, however, without _remote_ cache invalidation features.

### SCR-1192
**Summary**: Removed com.openexchange.cluster.timer.ClusterTimerService

Removed com.openexchange.cluster.timer.ClusterTimerService



API - RMI
---------

### SCR-1430
**Summary**: Added new RMI API for deputy permissions management

Added new RMI API "`com.openexchange.admin.rmi.OXDeputyPermissionsInterface`" for deputy permissions management offering methods:


* `grantDeputyPermission()`
  Grants a new deputy permission


* `updateDeputyPermission()`
  Updates an existent deputy permission


* `revokeDeputyPermission()`
  Revokes/deletes an existent deputy permission


* `getDeputyPermission()`
  Retrieves a certain deputy permission


* `listAll()`
  Lists all deputy permissions for a given context




API - SOAP
----------

### SCR-1431
**Summary**: Added new SOAP API for deputy permissions management

Added new SOAP API "`http://soap.admin.openexchange.com/OXDeputyPermissionsService`" for deputy permissions management offering methods:


* `grant()`
  Grants a new deputy permission


* `update()`
  Updates an existent deputy permission


* `revoke()`
  Revokes/deletes an existent deputy permission


* `get()`
  Retrieves a certain deputy permission


* `list()`
  Lists all deputy permissions for a given context




Behavioral Changes
------------------

### SCR-1425
**Summary**: Removed Replacement of "email 1" by "default sender address"

Previously, the middleware implicitly injected the configured "default sender address" into the "email 1" field of contacts representing internal users, in case `com.openexchange.notification.fromSource` was set to `defaultSenderAddress`. This handling goes back to a workaround that was introduced to make this mail address available in Outlook through the former _OXtender_ integration.

This caused different issues in the past, e.g. unexpected values in the global address book, incorrect search results, multiple users with apparently the same mail addresses etc.

As the client for which the workaround has been introduced has passed away a long time ago, this implicit mail address replacement is now removed, as it is obviously no longer necessary. Consequently, the provisioned "email 1" address is now always exposed in user contact objects through all APIs, regardless of aforementioned configuration setting `com.openexchange.notification.fromSource`. 

So for cases where a different default sender address has been set before in a user's mail settings, the behavioral change would be that this address will now no longer be displayed for the associated contact object, but the actually stored value for "email 1".



Configuration
-------------

### SCR-1449
**Summary**: Added DNS configuration options for MX records look-up on ISPDB auto-config detection

Added new lean DNS configuration options for MX records look-up on ISPDB auto-config detection


* `com.openexchange.mail.autoconfig.ispdb.dns.resolverHost`
  The optional host name for the DNS server on MX record look-up. If not specified system's default DNS service is used.


* `com.openexchange.mail.autoconfig.ispdb.dns.resolverPort`
  The optional port number for the DNS server on MX record look-up. If not specified default port (53 UDP) is used.

### SCR-1441
**Summary**: New Configuration Property "com.openexchange.oidc.staySignedIn"

If the default OIDC backend implementation is used, sessions created through OpenID Connect have the "stay signed in"  marker set to `false` by default. 

This can now be changed through configuration parameter [com.openexchange.oidc.staySignedIn](https://documentation.open-xchange.com/components/middleware/config/8/#com.openexchange.oidc.staySignedIn), and has an impact on the cookie- and OX session lifetime. If `true`, cookies will be decorated with the max. age as configured via [com.openexchange.cookie.ttl](https://documentation.open-xchange.com/components/middleware/config/8/#com.openexchange.cookie.ttl). Also, the maximum idle time of OX sessions will be aligned to [com.openexchange.sessiond.sessionLongLifeTime](https://documentation.open-xchange.com/components/middleware/config/8/#com.openexchange.sessiond.sessionLongLifeTime). If set to `false`, cookies will use _session_ lifetime, and the maximum OX session idle time follows [com.openexchange.sessiond.sessionDefaultLifeTime](https://documentation.open-xchange.com/components/middleware/config/8/#com.openexchange.sessiond.sessionDefaultLifeTime).

The lean property defaults to `false`, is reloadable, and not config-cascade aware.

### SCR-1438
**Summary**: Removed all xing properties

The following xing properties have been removed:

* com.openexchange.oauth.xing
* com.openexchange.oauth.xing.apiKey
* com.openexchange.oauth.xing.apiSecret
* com.openexchange.oauth.xing.consumerKey
* com.openexchange.oauth.xing.consumerSecret
* com.openexchange.subscribe.socialplugin.xing
* com.openexchange.subscribe.socialplugin.xing.autorunInterval

### SCR-1435
**Summary**: Removed option from Redis configuration

Removed option `"com.openexchange.redis.resilientDatabase"` from Redis configuration in favor of possibility to specify a dedicated Redis instance for volatile (cache) data; see SCR-1434.

Thus instead of specifying a dedicated database for resilient data (such as sessions), the administrator is supposed to deploy dedicated Redis instance for volatile (cache) data.

### SCR-1434
**Summary**: Added option to Redis configuration to specify a separate instance

Added new lean option `"com.openexchange.redis.cache.enabled"` to Redis configuration to specify a separate instance dedicated for volatile (cache) data. This option is neither reloadable nor config-cascade aware.

With that option set to `"true"`, the administrator may specify further Redis options for that special Redis instance through using the `"cache"` infix; e.g.


```
com.openexchange.redis.cache.enabled=true
com.openexchange.redis.cache.mode=standalone
com.openexchange.redis.cache.hosts=localhost:6379
 ...
```


### SCR-1407
**Summary**: Replaced 'tokenlogin-secrets' file with lean configuration

<warning>
**Breaking Change**
The file `tokelogin-secrets` has been *removed*.
</warning>

The file `tokenlogin-secrets` was used to define "secret" applications for which a token login was allowed. Such "secrets" could be enriched with parameters, controlling flows in the middleware. The file was not reloadable nor config cascade aware.
Therefore, replaced the file with config cascade aware and reloadable properties. The properties are defined as followed:

 * `com.openexchange.tokenlogin.applications` specifies the application identifiers to use, split by comma. These IDs were formerly knwon as "secrets". No default value is defined. The IDs are used to define application specific behaviour through the other properties
 * `com.openexchange.tokenlogin.[applicationId].accessPassword` specifies whether or not the user's password is part of the response when redeeming the token. Default is `false`.
 * `com.openexchange.tokenlogin.[applicationId].copyParameters` specifies whether or not to copy all session parameters into the cloned session, that is created during the token login action. Default is `false`
 * `com.openexchange.tokenlogin.[applicationId].announceId` specifies whether or not to announce the application identifier to a client within the JSLob. Default is `false`.
 * `com.openexchange.tokenlogin.[applicationId].parameters` specifies additional key-value pairs for the application, paired by equals, split by semicolon. Default is empty. Mainly kept for legacy reasons.


Database
--------

### SCR-1436
**Summary**: Introduced update task for removing xing accounts

Introduced the update task `com.openexchange.oauth.impl.internal.groupware.RemoveXingAccountsUpdateTask` for removing xing accounts.


Packaging/Bundles
-----------------

### SCR-1437
**Summary**: Removed xing bundles

Removed xing bundles:

 * com.openexchange.xing
 * com.openexchange.xing.access
 * com.openexchange.xing.json
 * com.openexchange.subscribe.xing
 * com.openexchange.oauth.xing
 * com.openexchange.halo.xing

Package definitions have been removed as well:

 * open-xchange-xing-json

### SCR-1429
**Summary**: Added new bundle for deputy permissions management via SOAP

Added new bundle "`com.openexchange.admin.soap.deputy`" for deputy permissions management via SOAP. That new bundle has been added to "`open-xchange-admin-soap`" package.

### SCR-1426
**Summary**: Removed "FilteringObjectStreamFactory" Service and parent Bundle "com.openexchange.serialization"

The service `com.openexchange.serialization.FilteringObjectStreamFactory` as well as its parent bundle `com.openexchange.serialization` are no longer used and had been deprecated along with SCR-1421. Now they're removed from middleware core.



[8.28]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1420
**Summary**: Removed `xmlbeans-2.6.0` library

The library `xmlbeans-2.6.0` has known vulnerabilities. Since it is no longer used in the Middleware, the library is removed from target platform (`com.openexchange.bundles`)

### SCR-1419
**Summary**: Upgraded ROME library for RSS and Atom feeds

Upgraded ROME library for RSS and Atom feeds from `v1.0` to `v1.19.0` in bundle `com.openexchange.messaging.rss`

### SCR-1415
**Summary**: Updated Google Guava from `v33.0.0` to `v33.2.1`

Updated Google Guava from `v33.0.0` to `v33.2.1` in bundle `com.google.guava`



API - Java
----------

### SCR-1421
**Summary**: Deprecation of "FilteringObjectStreamFactory" Service

The service `com.openexchange.serialization.FilteringObjectStreamFactory` has been introduced to secure serialization routines in the first version of the "Realtime" framework.

Therefore, it should now be considered as deprecated, and is scheduled to be removed along with its parent bundle `com.openexchange.serialization` in a future release.



Behavioral Changes
------------------

### SCR-1390
**Summary**: Introduced an admin based rate limit for provisioning calls

Up until now the provisioning apis (soap, rmi, clt) were not rate limited which could lead to downtimes in case a client provisioned too fast. This is especially painful in case multiple customers are on the same platform and could influence each other.

To prevent such scenarios in the future we introduced a new rate limit which is applied per admin. It effects all provisioning apis and is checked during the authentication process.

The limit is applied in constant 1 minute timeframes and can be configured for all admins or a single ones in case one would like to introduce different limits for different admins.

For this the following lean properties were introduced as well:

```properties
com.openexchange.rmi.rate.limit.default=-1
com.openexchange.rmi.rate.limit.[admin] 
```
See https://gitlab.open-xchange.com/app-suite-platform-1/provisioning/-/issues/1 for details



Configuration
-------------

### SCR-1422
**Summary**: Removed unused cache regions from cache.ccf file

Removed unused cache regions from `cache.ccf` file since according caches are now held in Redis storage or refactored to a local (Guava) cache.

Removed regions are:


* `OXFolderCache`
* `OXFolderQueryCache`
* `GlobalFolderCache`


### SCR-1416
**Summary**: Changed default value for property "com.openexchange.net.ssl.protocols"

Changed default value for lean property "`com.openexchange.net.ssl.protocols`" from "`TLSv1, TLSv1.1, TLSv1.2`" to "`TLSv1.2, TLSv1.3`" following the recommendation to always use TLS 1.2 or higher



[8.27]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1409
**Summary**: Apache Commons Lang 2.6 removed from target platform

The library Apache Commons Lang 2.6 has been removed from the target platform. The code should be migrated to Apache Commons Lang 3.x. 

All known dependencies have already been resolved in previous releases.

### SCR-1404
**Summary**: Updated JCTools in target platform

Updated JCTools (Java Concurrency Tools for the JVM) from v4.0.3 to v4.0.5  in target platform

### SCR-1394
**Summary**: Updated lettuce library from v6.3.1 to v6.3.2

Updated lettuce library from v6.3.1 to v6.3.2 in bundle `io.lettuce`

* lettuce-core-6.3.2.RELEASE.jar

### SCR-1393
**Summary**: Updated Netty libraries from v4.1.106 to v4.1.111

Updated Netty libraries from v4.1.106 to v4.1.111 in bundle `io.netty`

* netty-buffer-4.1.111.Final.jar
* netty-codec-4.1.111.Final.jar
* netty-codec-dns-4.1.111.Final.jar
* netty-codec-http2-4.1.111.Final.jar
* netty-codec-http-4.1.111.Final.jar
* netty-codec-socks-4.1.111.Final.jar
* netty-common-4.1.111.Final.jar
* netty-handler-4.1.111.Final.jar
* netty-handler-proxy-4.1.111.Final.jar
* netty-resolver-4.1.111.Final.jar
* netty-resolver-dns-4.1.111.Final.jar
* netty-transport-4.1.111.Final.jar
* netty-transport-native-unix-common-4.1.111.Final.jar


### SCR-1389
**Summary**: Removed jboss library

The library `jboss-jms-api.jar` is no longer needed. Therefore, it has been removed.

### SCR-1212
**Summary**: Update BouncyCastle Libraries to Latest

Bouncy Castle Libraries have been updated to include several bug fixes.  Want to update Bouncy Libraries to version 1.78.1

* bcmail-jdk18on-1.78.1.jar
* bcpg-jkd180n-1.78.1.jar
* pcpkix-jkd180n-1.78.1.jar
* bcprov-ext REMOVED, use bcprov.  ext was extended support of old methods and no longer required
* bcprov-jkd180n-1.78.1.jar
* bcutil-jkd180n-1.78.1.jar


API - HTTP-API
--------------

### SCR-1406
**Summary**: Added an client defined expiration time to /token?action=acquireToken

The parameter `expiry` was added to the request `acquireToken`. Thus, a client is able to define an individual expiry for a login token, see also `/login?action=redeemToken`.
The expiry parameter will only be considered if the value is less the configured value through `com.openexchange.tokenlogin.maxIdleTime`

### SCR-1405
**Summary**: Renamed parameter in '/login?action=redeemToken'

The parameter `secret` has been renamed to `appId` since it is more fitting to the nature of the parameter. `secret` can still be used, but is deprecated from now on. It will be removed in upcoming releases.


### SCR-1403
**Summary**: New field 'priority' in Event model of HTTP API

The `Event` model of the HTTP API is extended by a new field named `priority`. Its value defines the relative priority of the calendar event with the following semantics (see [RFC 5545, section 3.8.1.9](https://datatracker.ietf.org/doc/html/rfc5545#section-3.8.1.9) for further details):

> This priority is specified as an integer in the range 0 to 9.  A value of 0 specifies an undefined priority.  A value of 1 is the highest priority.  A value of 2 is the second highest priority.  Subsequent numbers specify a decreasing ordinal priority.  A value of 9 is the lowest priority.


### SCR-1401
**Summary**: Removal of transport "websocket" from "pns" API

The transport "websocket" in "pns" API was marked as deprecated with SCR-1297. It is now removed with version 8.27.

Behavioral Changes
------------------

### SCR-1412
**Summary**: Caches Transformed to Redis

In an iterative approach, more and more caches are being outsourced from middleware node-local caches (JCS), towards a centralized caching architecture using Redis.

Consequently, the memory requirements for the Redis pods are going to increase - with this, as well as in upcoming releases.

Therefore, it is recommended to increase the memory assigned to the Redis pods via Helm charts (`{}resources.limits{`} and/or `{}resources.requests{`}), and to monitor the memory consumption of the Redis pods closely, e.g. by utilizing the exposed metrics like `{}redis_memory_used_dataset_bytes{`}.


Configuration
-------------

### SCR-1408
**Summary**: Added new config option controlling whether to use HTML on reply/forward if preferred

Added new lean config option:
* `com.openexchange.mail.useHtmlOnReplyForwardIfPreferred`
  That boolean config option controls whether to use HTML on reply/forward to/of text-only E-Mails if HTML is chosen as preferred message format. Default value is `false`. That property is both - reloadable and config-cascade aware.

### SCR-1383
**Summary**: New Property `com.openexchange.redis.resilientDatabase`

In order to configure an alternative database number for Redis/KeyDB, the following lean configuration property is introduced:

```
com.openexchange.redis.resilientDatabase

```
This property allows to configure an alternative database number to use for unrecoverable data like sessions that might be replayed to remote sites in _sharded_ environments with multiple data centers ("Active/Active").

Especially, data stored there would be _resilient_ when flushing the default database after recovering from failover situations.

Databases are only available for Redis stand-alone and Redis Master/Slave.

A negative number means no specific database.

The property defaults to `-1`, which means that no separate database number is used. It is neither reloadable, nor config-cascade-aware.


Database
--------

### SCR-1411
**Summary**: Added an account index to various calendar tables for improved look-up

Added an index for columns `cid` and `account` to following calendar tables for improved look-up:

* `calendar_event`
* `calendar_event_tombstone`
* `calendar_attendee`
* `calendar_attendee_tombstone`
* `calendar_alarm`
* `calendar_alarm_trigger`
* `calendar_conference`

### SCR-1402
**Summary**: New Column 'priority' for Database Tables 'calendar_event' and 'calendar_event_tombstone'


<warning>
**Update Task**
com.openexchange.chronos.storage.rdb.groupware.CalendarEventAddPriorityColumnTask
</warning>


The database tables `calendar_event` and `calendar_event_tombstone` are extended with a new column as follows:

```
`priority` INT4 UNSIGNED DEFAULT NULL

```

This is done through the following update task:

```
com.openexchange.chronos.storage.rdb.groupware.CalendarEventAddPriorityColumnTask

```

Packaging/Bundles
-----------------

### SCR-1388
**Summary**: Removed obsolete bundle 'com.google.gdata'

The bundle `com.google.gdata` is no longer in use and therefore is removed, along with its reference from package/feature `{}open-xchange-oauth{`}.



[8.26]
======

API - HTTP-API
-------------

### SCR-1399
**Summary**: Deprecate `messaging`-related functionality and APIs

The middleware used to provide a generic messaging service for different use cases like SMS or data from RSS feeds. However, these are no longer in use by App Suite UI, or got replaced with an alternative solution in the meantime. 

Therefore, corresponding functionality as well as the following modules of the HTTP API should be considered as deprecated, and will be removed in a future version:

* `messaging/account`
* `messaging/message`
* `messaging/service`


Configuration
-------------

### SCR-1382
**Summary**: Added new lean property to specify SMTP chunk size

Added new lean property `com.openexchange.smtp.chunksize` (and `com.openexchange.smtp.primary.chunksize` respectively) to specify SMTP chunk size to use the SMTP extension for transmission of large messages; see [RFC 3030](https://datatracker.ietf.org/doc/html/rfc3030). Default is `131072` (128KB). Reloadbale and config-cascade aware.

### SCR-1386
**Summary**: Added new config options for webhook-based password change service

In order to configure the webhook-based password change service, the following properties have been introduced:

* com.openexchange.passwordchange.webhook.enabled

    Enables the webhook-based password change service (default: false)

* com.openexchange.passwordchange.webhook.endpoint

    The webhook endpoint

* com.openexchange.passwordchange.webhook.username

    The optional basic auth username for the webhook endpoint

* com.openexchange.passwordchange.webhook.password
  
    The optional basic auth password for the webhook endpoint

All properties are reloadable and config-cascade aware.


Database
--------

### SCR-1387
**Summary**: Introduced a new count table `users_per_filestore` for users using a certain file storage

Introduced `users_per_context` table in ConfigDB to have a direct access how many users use a certain file storage:


Table layout is:

```
CREATE TABLE `users_per_filestore` (
  `filestore_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`filestore_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
```

### SCR-1424
**Summary**: Changes for "infostore_document" and "infostore" that are required to support MySQL 8.4

Update task `com.openexchange.groupware.update.tasks.InfostoreDocumentDropForeignKey` drops the foreign key from "infostore_document" table due to missing unique key in the referenced table "infostore" and adds an appropriate index instead.



[8.25]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1373
**Summary**: Deprecation of Apache Commons Lang 2.6

The Apache Commons Lang in version 2.6 will be removed from the target platform with version 8.26! As version 3.x (currently 3.14.0) of Apache Commons Lang is already available within the target platform please make sure to migrate your code to this version until the release of App Suite 8.26!



API - HTTP-API
--------------

### SCR-1375
**Summary**: Added sort_first_name field to DistributionListMember

Each member of a distribution list now contains an additional field sort_first_name which provides a name which can be used for sorting by first name. This field works similar to the sort_first_name field of the contact itself.

Additionally the members are also sorted according to this field.

### SCR-1372
**Summary**: Added virtual contact field column 'sort_first_name' 

Added virtual contact field column `sort_first_name` with the column identifier 623.

In analogy to the `sort_name` column (607), which sorts by surname, this column id can be used to sort the contacts in a contact request by the first name, taking into account the YOMI names.



Configuration
-------------

### SCR-1380
**Summary**: Added new config option to control if user's local part should be assumed

Added new lean config option `com.openexchange.imap.assumeUserLocalPartForSharedFolderPath` to control if user's local part should be assumed when determining the path for a shared IMAP folder; e.g. assume "jane.doe" instead of "jane.doe@invalid.com". Default is `false`. Reloadable and config-cascade aware.

### SCR-1378
**Summary**: Removal of Property `com.openexchange.redis.enabled`

After Redis becoming mandatory for core middleware services (SCR-1310, SCR-1330, SCR-1342 and furthers), the previously available switch `com.openexchange.redis.enabled` is no longer needed and therefore removed.

That means that the middleware will no longer start or work properly without configured Redis instance. By default, a _standalone_ Redis instance on `localhost:6379`, is used. See [property documentation](https://documentation.open-xchange.com/components/middleware/config/8/#mode=features&feature=Redis) for further details.

Consequently, in the "core-mw" Helm chart, the previously available `enabled` switch in the `redis` section is removed as well. That means that, unless configured differently via `hosts`, a fallback standalone Redis service is deployed by default. 



Database
--------

### SCR-1319
**Summary**: Drop oauth-provider tables

Update tasks `com.openexchange.groupware.update.tasks.DropOAuthGrantTableTask` and `com.openexchange.groupware.update.tasks.DropAuthCodeTableTask` drop the tables `oauth_grant` and `authCode` from contextdb. The changesets `8.x:oauth_client:drop` and `8.x:oauth_client_uri:drop` drop the tables `oauth_client` and `oauth_client_uri` from globaldb.



[8.24]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1366
**Summary**: Updated Spring Framework

Updated Spring Framework from v5.3.21 to v6.1.4 in bundle `com.openexchange.xml`


* `spring-beans-6.1.4.jar`
* `spring-core-6.1.4.jar`
* `spring-jcl-6.1.4.jar`



API - Java
----------

### SCR-1367
**Summary**: Slightly incompatible update to BasicAuthenticatorPluginInterface

In order to gain more flexibility in case of multiple Plugins implementing `BasicAuthenticatorPluginInterface` and to introduce Context-Admin capability for regular users using the provisioning APIs, the return type of the three methods


* `isOwnerOfContext()`
* `isMasterOfContext()`
* `isMasterOfContext()`

will be changed from the primitive type boolean to `Optional<Boolean>` also supporting the third state _empty_ to ignore the result and skip to the next plugin.



API - REST
----------

### SCR-1368
**Summary**: New REST API endpoint /admin/v1/contexts/pre-assemble to pre-assemble contexts

The concept of pre-assembled contexts consists of the asynchronous pre-creation of deactivated contexts that will be reused (and adapted to the provided settings) during the usual `createcontext` call. Pre-assembled contexts are characterized by being deactivated with `reason_id` equals `666` and their name beginning with the `preassembled-` prefix followed by an UUID.

In order to pick up pre-assembled contexts during regular provisioning operations, context skeletons need to be inserted into the database first. This can be achieved in two ways, by calling a REST API or via a background job. 

The REST API is located at `http://oxhost:8009/admin/v1/contexts/pre-assemble`. Pre-assembled contexts can be generated by a POST request using master authentication with a body containing a JSON object describing the schema name and how many context skeletons should be created. Optionally you can add the id of the filestore that should be used for the pre-assembled context by defining `filestore_id` parameter. 

Example:


```
POST /admin/v1/contexts/pre-assemble HTTP/1.1
Content-Type: application/json
Authorization: Basic amFuOmphbg==
User-Agent: PostmanRuntime/7.36.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 3992936c-9d95-43c6-beb7-8b110e0088e2
Host: devenv.oxmw.io:8009
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 41

{
    "number":4,"schema": "oxuserdb_5"
}

```



```
HTTP/1.1 200 OK
Server: grizzly/2.4.4
X-Robots-Tag: none
Content-Type: application/json; charset=UTF-8
Content-Length: 52

{
    "contextIds": [
        11832,
        11833,
        11834,
        11835
    ],
    "errors": []
}

```


The response contains a JSON array of identifiers of the created pre-assembled contexts at field `contextIds`. Any errors that might have occurred when inserting the data are supplied in the `errors` array as well. In case of fatal errors, a response status different from  `HTTP 200` will be returned depending on the error that occurred. Please have a look at the full REST API description of the endpoint for more details.



Configuration
-------------

### SCR-1370
**Summary**: Added option to Redis configuration to specify a compression method

Added lean property to Redis configuration to specify a compression method


* `com.openexchange.redis.compressionType`
  The compression type to globally compress/decompress any data written to/read from Redis end-point according to specified type. Allowed type: `snappy`, `gzip`, `deflate` and `none`. Enabling or disabling compression is backward-compatible; meaning any previously written uncompressed data can still be decoded as well as any previously compressed data (provided that compression gets turned off later on). Default value is `none`. Neither config-cascade aware nor reloadable.


* `com.openexchange.redis.minimumCompressionSize`
  The minimum size for a data chunk in bytes for being considered for compression. Only effective if `"com.openexchange.redis.compressionType"` is set to not `"none"`.

### SCR-1360
**Summary**: New lean properties for background job to create pre-assembled contexts

New lean and non-reloadable properties to configure the background job that creates pre-assembled contexts:


* `com.openexchange.admin.context.preassembly.job.enabled`, defaults to `false`. Whether background job is active or not
* `com.openexchange.admin.context.preassembly.job.schedule`, defaults to `Mon-Sun 0-4`. The pattern specifying when to execute context the pre-assemble job. The default time zone of the Java virtual machine is assumed, which is typically the system's default time zone; unless the `user.timezone` property is set otherwise.        
* `com.openexchange.admin.context.preassembly.job.contextsPerSchema`, defaults to `100`. Configures the number of pre-assembled contexts that should exist per schema after the job was executed. This number will never be exceeded by using the background job. Of course it is possible to exceed this limit by using the REST API.
* `com.openexchange.admin.context.preassembly.job.contextLimitFactor`, defaults to `0.9`. Configures the maximum filling level of schemas when adding pre-assembled contexts via periodic background job, as a factor of CONTEXTS_PER_SCHEMA. I. e. pre-assembly is only performed until the total number of contexts exceeds `<factor> * <contexts_per_schema>`.

* `com.openexchange.admin.context.preassembly.job.frequency`, defaults to `3600000` (1 hour). The frequency in milliseconds when to check for new job executions within configured schedule.
* `com.openexchange.admin.context.preassembly.job.executionDelay`, defaults to `86400000` (1 day). The (minimum) delay between repeated executions of the pre-assembly job in milliseconds.

### SCR-1331
**Summary**: Added lean property 'com.openexchange.admin.context.preassembly.enabled'

Added lean property `com.openexchange.admin.context.preassembly.enabled` to enable using pre-assembled contexts instead of creating new contexts. Pre-assembled contexts must exist in database before enabling!
Defaults to `false`.

### SCR-1292
**Summary**: Dropped the 'com.openexchange.drive.events.gcm.key' property

Dropped the `com.openexchange.drive.events.gcm.key` property. Introduced the `com.openexchange.drive.events.fcm.keyPath` property as a replacement.

### SCR-1290
**Summary**: Dropped the 'key' attribute in the 'pushClientConfig'

Dropped the `key` attribute in the `pushClientConfig` config file for the `_type` `fcm`.
Introduced the `keyPath` attribute, which defines the full path of the FCM key file.

### SCR-1289
**Summary**: Renamed *.gcm.* properties to *.fcm.*

Affected properties are:

* `com.openexchange.drive.events.gcm.enabled`
* `com.openexchange.drive.events.gcm.clientId`
* `com.openexchange.pns.transport.gcm.enabled.*`

The new properties are now available under a new qualified name:

* `com.openexchange.drive.events.fcm.enabled`
* `com.openexchange.drive.events.fcm.clientId`
* `com.openexchange.pns.transport.fcm.enabled.*`



Database
--------

### SCR-1288
**Summary**: Rename 'serviceId' and 'transport' values from GCM to FCM


<warning>
**Update Task**
com.openexchange.drive.events.fcm.groupware.RenameGCM2FCMUpdateTask
com.openexchange.pns.transport.fcm.groupware.RenameGCM2FCMUpdateTask
</warning>


Due to deprecation and end-of-life of GCM, we need to switch to FCM, hence the rename of the serviceId and transport values in the database.



Packaging/Bundles
-----------------

### SCR-1369
**Summary**: Removal of Kerberos Authentication

The Kerberos authentication integration that was available via supplementary package `open-xchange-authentication-kerberos` was removed.

See SCR-1315 for the deprecation with version 8.19.

### SCR-1314
**Summary**: Introduced new FCM bundles

Added the following FCM-related bundles:

- `com.google.firebase`
- `com.openexchange.drive.events.fcm`
- `com.openexchange.pns.transport.fcm`

### SCR-1313
**Summary**: Removed GCM bundles

Removed the following GCM-related bundles:

- `com.google.android.gcm`
- `com.openexchange.drive.events.gcm`
- `com.openexchange.pns.transport.gcm`



[8.23]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1362
**Summary**: Updated metadata-extractor

Updated 3rd party library metadata-extractor from v2.18.0 to v2.19.0 in bundle `com.drew`

### SCR-1361
**Summary**: Updated Pushy library

Updated Pushy library from v0.15.2 to v0.15.4 in bundle `com.eatthepath.pushy`

### SCR-1359
**Summary**: Updated a bunch of bundles in target platform

### Updated the following bundles in target platform (`com.openexchange.bundles`):

#### Apache Mime4j


* apache-mime4j-core-0.8.7.jar -> apache-mime4j-core-0.8.10.jar
* apache-mime4j-dom-0.8.7.jar -> apache-mime4j-dom-0.8.10.jar
* apache-mime4j-storage-0.8.7.jar -> apache-mime4j-storage-0.8.10.jar

#### Apache Commons


* commons-net-3.9.0.jar -> commons-net-3.10.0.jar
* commons-pool2-2.11.1.jar -> commons-pool2-2.12.0.jar
* commons-text-1.10.0.jar -> commons-text-1.11.0.jar
* commons-validator-1.6.jar -> commons-validator-1.8.0.jar

#### Various/other

* dnsjava-3.5.2.jar -> dnsjava-3.5.3.jar
* expiringmap-0.5.11.jar 
* fontbox-2.0.24.jar -> fontbox-2.0.30.jar
* jctools-core-4.0.1.jar -> jctools-core-4.0.3.jar
* joda-time-2.10.5.jar -> joda-time-2.12.7.jar
* jsoup-1.16.1.jar -> jsoup-1.17.2.jar
* pdfbox-2.0.24.jar -> pdfbox-2.0.30.jar
* snappy-java-1.1.10.3.jar -> snappy-java-1.1.10.5.jar

### SCR-1358
**Summary**: Updated Apache Commons Lang3 library

Updated Apache Commons Lang3 library from v3.12.0 to v3.14.0 in target platform (`com.openexchange.bundles`)

### SCR-1357
**Summary**: Updated Apache Commons IO library

Updated Apache Commons IO library from v2.11.0 to v2.15.1 in target platform (`com.openexchange.bundles`)

### SCR-1356
**Summary**: Updated Apache Commons Exec library

Updated Apache Commons Exec library from v1.3 to v1.4.0 in target platform (`com.openexchange.bundles`)


### SCR-1355
**Summary**: Updated Apache Commons CLI library

Updated Apache Commons Codec library from v1.5.0 to v1.6.0 in target platform (`com.openexchange.bundles`)


### SCR-1354
**Summary**: Updated Apache Commons Codec library

Updated Apache Commons Codec library from v1.15 to v1.16.1 in target platform (`com.openexchange.bundles`)


### SCR-1353
**Summary**: Updated Apache Commons Compress library

Updated Apache Commons Compress library from v1.21 to v1.26.0 in target platform (`com.openexchange.bundles`)

### SCR-1349
**Summary**: Upgraded MaxMind GeoIP Libraries

The following 3rd party libraries in bundle `com.openexchange.geolocation.maxmind.binary` are upgraded:

* MaxMind GeoIP2 API from v2.12.0 to v2.17.0 (`geoip2-2.12.0.jar`)
* MaxMind DB Reader from v1.2.2 to v2.1.0 (`maxmind-db-2.1.0.jar`)

### SCR-1348
**Summary**: Updated Amazon Java SDK

Updated Amazon Java SDK from v1.12.487 to v1.12.661 in bundle `com.amazonaws`

### SCR-1345
**Summary**: Updated Google Guava from v32.1.3 to v33.0.0

Updated Google Guava from v32.1.3 to v33.0.0 in bundle `com.google.guava`



Configuration
-------------

### SCR-1365
**Summary**: Support new property for Sproxyd connector to specify connection lease timeout

Support new property for Sproxyd connector to specify connection lease timeout:


* `com.openexchange.filestore.sproxyd.connectionLeaseTimeout`
  The connection lease timeout in milliseconds when waiting for a free connection in connection pool to become available. Default is 5 seconds (5000). Reloadable, but not config-cascade aware.

### SCR-1351
**Summary**: New property `com.openexchange.health.noServicesMissing.enabled`

A new lean configuration property is introduced to activate an additional health check regarding internal service dependencies:

```
com.openexchange.health.noServicesMissing.enabled=false

```

It defaults to `false` for now hence needs to be enabled explicitly. Once enabled, the overall health check will only yield an `UP` result if all service/package dependencies are met. Therefore it is recommended to ensure that this is the case, i.e. the output of `getmissingservices` utility is empty.

The property is not config-cascade-aware, and not reloadable.

### SCR-1350
**Summary**: Added possibility to have ZIP archive compiled for a certain module during a data export being spooled to a local disk

Added possibility to have ZIP archive compiled for a certain module being spooled to a local disk. Therefore, the following new lean properties were added:


* `com.openexchange.gdpr.dataexport.spoolToFile`
  Whether to spool collected data to a ZIP archive held on local disk or to append directly to destination file storage. Default value: `false`. Neither reloadable nor config-cascade aware


* `com.openexchange.gdpr.dataexport.spoolDirectory`
  The spool directory on disk to use for spooling. Requires that `"com.openexchange.gdpr.dataexport.spoolToFile"` is set to "true". if not set or specifies a non-existent, non-writable directory path, the default upload directory is used instead. Neither reloadable nor config-cascade aware



Packaging/Bundles
-----------------

### SCR-1347
**Summary**: Added new bundles for Redis-backed cache

Added new bundles (interface/API & implementation) for Redis-backed cache to `open-xchange-core` package:


* `com.openexchange.cache.v2`
* `com.openexchange.cache.v2.redis`



[8.22]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1344
**Summary**: Updated lettuce library from v6.2.6 to v6.3.1

Updated lettuce library from v6.2.6 to v6.3.1 in bundle `io.lettuce`


* lettuce-core-6.3.1.RELEASE.jar

### SCR-1343
**Summary**: Updated Netty libraries from v4.1.97 to v4.1.106

Updated Netty libraries from v4.1.97 to v4.1.106 in bundle `io.netty`


* netty-buffer-4.1.106.Final.jar
* netty-codec-4.1.106.Final.jar
* netty-codec-dns-4.1.106.Final.jar
* netty-codec-http2-4.1.106.Final.jar
* netty-codec-http-4.1.106.Final.jar
* netty-codec-socks-4.1.106.Final.jar
* netty-common-4.1.106.Final.jar
* netty-handler-4.1.106.Final.jar
* netty-handler-proxy-4.1.106.Final.jar
* netty-resolver-4.1.106.Final.jar
* netty-resolver-dns-4.1.106.Final.jar
* netty-transport-4.1.106.Final.jar
* netty-transport-native-unix-common-4.1.106.Final.jar

### SCR-1340
**Summary**: Updated Jackson & Fabric8 libraries

### Updated Jackson libraries from v2.15.3 to v2.16.1 in target platfom


* jackson-annotations-2.16.1.jar
* jackson-core-2.16.1.jar
* jackson-databind-2.16.1.jar
* jackson-dataformat-cbor-2.16.1.jar
* jackson-dataformat-xml-2.16.1.jar
* jackson-dataformat-yaml-2.16.1.jar
* jackson-datatype-jsr310-2.16.1.jar
* jackson-datatype-jsr353-2.16.1.jar
* jackson-jakarta-rs-base-2.16.1.jar
* jackson-jakarta-rs-json-provider-2.16.1.jar
* jackson-jakarta-rs-xml-provider-2.16.1.jar
* jackson-module-jakarta-xmlbind-annotations-2.16.1.jar
* jackson-module-jaxb-annotations-2.16.1.jar

### Updated Fabric8 ibraries from v6.9.0 to v6.10.0 in "`io.fabric8.kubernetes`" bundle


* kubernetes-client-6.10.0.jar
* kubernetes-client-api-6.10.0.jar
* kubernetes-httpclient-jdk-6.10.0.jar
* kubernetes-model-admissionregistration-6.10.0.jar
* kubernetes-model-apiextensions-6.10.0.jar
* kubernetes-model-apps-6.10.0.jar
* kubernetes-model-autoscaling-6.10.0.jar
* kubernetes-model-batch-6.10.0.jar
* kubernetes-model-certificates-6.10.0.jar
* kubernetes-model-common-6.10.0.jar
* kubernetes-model-coordination-6.10.0.jar
* kubernetes-model-core-6.10.0.jar
* kubernetes-model-discovery-6.10.0.jar
* kubernetes-model-events-6.10.0.jar
* kubernetes-model-extensions-6.10.0.jar
* kubernetes-model-flowcontrol-6.10.0.jar
* kubernetes-model-gatewayapi-6.10.0.jar
* kubernetes-model-metrics-6.10.0.jar
* kubernetes-model-networking-6.10.0.jar
* kubernetes-model-node-6.10.0.jar
* kubernetes-model-policy-6.10.0.jar
* kubernetes-model-rbac-6.10.0.jar
* kubernetes-model-resource-6.10.0.jar
* kubernetes-model-scheduling-6.10.0.jar
* kubernetes-model-storageclass-6.10.0.jar


### SCR-1337
**Summary**: Updated bucket4j library

Updated bucket4j library (Java rate-limiting library based on token-bucket algorithm) from v7.0.0 to v8.7.0



API - HTTP-API
--------------

### SCR-1305
**Summary**: Completely removed the ramp-up action

Completely removed the ramp-up action handling



API - Java
----------

### SCR-1339
**Summary**: Added methods in 'com.openexchange.admin.storage.interfaces.OXUserStorageInterface' for using pre-assembled contexts

Added methods

 * `com.openexchange.admin.storage.interfaces.OXUserStorageInterface.changeModuleAccess(Context, int[], UserModuleAccess, Connection)` - use an already established connection to change module access (already implemented with MW-2229)
 * `com.openexchange.admin.storage.interfaces.OXUserStorageInterface.change(Context, User, Connection)` - use an already established conntection to change user data (already implemented with MW-2229)
 * `com.openexchange.admin.storage.interfaces.OXUserStorageInterface.updatePreassembledLogin2UserData(Context, User, Connection)` - update pre-assembled dummy data in `login2user` table (implemented with MWB-2470)

### SCR-1306
**Summary**: Completely removed the ramp-up APIs and services

Completely removed the ramp-up APIs and services



Behavioral Changes
------------------

### SCR-1342
**Summary**: Use Redis-based pub/sub functionality in favor over Hazelcast-based topics/queues

Using Redis-based pub/sub functionality in favor over Hazelcast-based topics/queues. API-wise the former `com.openexchange.ms.MsService` is marked as deprecated and developers should use new `com.openexchange.pubsub.PubSubService` instead.

### SCR-1330
**Summary**: Redis becoming mandatory for cluster-wide functions

In our step-wise approach of integrating Redis-based services into the middleware, we already introduced the Redis-backed session storage. With completion of the story "Redis by Default: Configuration, Documentation" (MW-2144), Redis will be enabled by default.

With "Switch Hazelcast Map Usages to New Service" (MW-2145) Redis will now be mandatory for many advanced features that make use of distributed states, when multiple middleware nodes are used in the cluster.



CLT
---

### SCR-1336
**Summary**: Dropped argument from oxinstaller command-line tool

Dropped argument `"--jkroute"` from oxinstaller command-line tool



Configuration
-------------

### SCR-1341
**Summary**: Added new lean property to possibly add Open-Xchange server information to HTTP responses

Added new lean property "`com.openexchange.http.grizzly.addServerVersion`" to possibly add Open-Xchange server information as "`X-Open-Xchange-Server`" HTTP header to responses. Default is "`false`". Neither reloadable nor config-cascade aware.

### SCR-1338
**Summary**: Added new configuration options for session look-ups at remote sites

Added new lean configuration options for session look-ups at remote sites


* `com.openexchange.sessiond.redis.remote.ratelimit.overallMaxAccesses`
  Specifies the max. number of overall remote site look-ups: not more than overallMaxAccesses per overallTimeWindowMillis. Default value is 60. Reloadable, but not config-cascade aware


* `com.openexchange.sessiond.redis.remote.ratelimit.overallTimeWindowMillis`
  Specifies the time window for overall remote site look-ups: not more than overallMaxAccesses per overallTimeWindowMillis. Default value is 60000. Reloadable, but not config-cascade aware


* `com.openexchange.sessiond.redis.remote.ratelimit.maxRatePerClient`
  Specifies the max. number of per-client remote site look-ups: not more than maxRatePerClient per timeWindowMillisPerClient. Default value is 10. Reloadable, but not config-cascade aware


* `com.openexchange.sessiond.redis.remote.ratelimit.timeWindowMillisPerClient`
  Specifies the time window for per-client remote site look-ups: not more than maxRatePerClient per timeWindowMillisPerClient. Default value is 60000. Reloadable, but not config-cascade aware

### SCR-1335
**Summary**: Dropped legacy property com.openexchange.server.backendRoute

Dropped legacy property `com.openexchange.server.backendRoute` from file `server.properties`.

This was no lean property, thus that property needs to be removed the old way.

### SCR-1331
**Summary**: Added lean property 'com.openexchange.admin.usePreAssembledContexts'

Added lean property `com.openexchange.admin.usePreAssembledContexts` to enable using pre-assembled contexts instead of creating new contexts. Pre-assembled contexts musts exist in database before enabling!
Defaults to `false`.



Database
--------

### SCR-1332
**Summary**: Added table 'context_lock' to configdb


<warning>
**Update Task**
com.openexchange.database.internal.change.custom.ServerCreateContextLockTable
</warning>


Added table `context_lock` to `configdb`, used for claiming/locking pre-assembled contexts.

```
CREATE TABLE `context_lock` (
    `cid` INT(10) UNSIGNED NOT NULL,
    `claim` BINARY(16) NOT NULL,
    `timestamp` BIGINT(20) UNSIGNED NOT NULL,
    PRIMARY KEY(`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

```



Packaging/Bundles
-----------------

### SCR-1327
**Summary**: New soap request analyzer bundle

Introduced a new `com.openexchange.admin.soap.request.analyzer` bundle



[8.21]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1279
**Summary**: Upgraded javacc to 7.10.12

Upgraded the javacc library to version 7.10.12



API - HTTP-API
--------------

### SCR-1333
**Summary**: Added action chronos/itip?action=decline_party_crasher

A new action, `chronos/itip?action=decline_party_crasher`, was added to the module `chronos`.
The action enables the end user to decline the participation of an unknown calendar user (aka. "party crasher"), that responded to a certain event. The action triggers a CANCEL mail to the unknown calendar user. The CANCEL mail can be used by the unknown calendar user to (automatically) remove the appointment from her calendar.
The action is defined as followed:


```
REQUEST:
PUT http://example.org/appsuite/api/chronos/itip?action=decline_party_crasher&session=1234xyz
{
  "com.openexchange.mail.conversion.fullname": "INBOX",
  "com.openexchange.mail.conversion.mailid": "1337",
  "com.openexchange.mail.conversion.sequenceid": "1.3"
}


RESPONSE:
{
    "data":
    {
        "recipient":
        {
            "uri": "mailto:partyCrasher@example.org",
            "cn": "Party Crasher",
            "email": "partyCrasher@example.org"
        },
        "status": "SENT"
    },
    "timestamp": 1704188470824
}

```

API - HTTP-REST
--------------

### SCR-1295
**Summary**: Introduced a new REST interface for managing logging configuration

New REST endpoints are introduced in order to manage logging configuration via HTTP:
The REST endpoints are registered under the /admin path and requires  admin BASIC AUTH.

The new API routes are.

```
/admin/v1/logconf/system/loggers
/admin/v1/logconf/system/logger/{{logger}}
/admin/v1/logconf/session/{{session}}/loggers/
/admin/v1/logconf/session/{{session}}/logger/{{logger}}
/admin/v1/logconf/context/{{context-id}}/loggers/
/admin/v1/logconf/context/{{context-id}}/logger/{{logger}}
/admin/v1/logconf/context/{{context-id}}/user/{{user-id}}/loggers/
/admin/v1/logconf/context/{{context-id}}/user/{{user-id}}/logger/{{logger}}
/admin/v1/logconf/suppressed/exception-categories
/admin/v1/logconf/context/{{context-id}}/user/{{user-id}}/stacktrace/include-on-error
```

Configuration
-------------

### SCR-1329
**Summary**: Added config option to avoid using IMAP entity's display name when listing shared folders

Added new lean config option `com.openexchange.imap.useIMAPEntityDisplayNameIfPossible` to control whether to use IMAP entity's display name when listing shared folders. Default is `true`

Packaging/Bundles
-------------

### SCR-1293
**Summary**: New bundle com.openexchange.logging.rest

Introduced a new bundle `com.openexchange.logging.rest`, as part of the open-xchange-core package, which provides a RESTful API for configuring logging behavior.


[8.20]
======

General
-------

### SCR-1328
**Summary**: Removed CPU Resource Limit

Removed CPU resource limit since it's not best practice to have it set, see https://home.robusta.dev/blog/stop-using-cpu-limits

### SCR-1227
**Summary**: Enhanced existent SOAP end-points by standard "Scheduled" folder

Enhanced existent SOAP end-points by standard "Scheduled" folder. The folder that holds such E-Mails that are scheduled for being sent at a later time.

To do so, the `"User"` data object contained in several SOAP end-points has been extended by `"mail_folder_scheduled_full_name"` element to output/specify the standard "Scheduled" folder.




3rd Party Libraries/License Change
----------------------------------

### SCR-1326
**Summary**: Updated Hazelcast from v3.5.1 to v3.5.6

Updated Hazelcast from v3.5.1 to v3.5.6 in bundle `com.hazelcast`

### SCR-1325
**Summary**: Updated Google Guava from v32.1.1 to v32.1.3

Updated Google Guava from v32.1.1 to v32.1.3 in bundle `com.google.guava`



API - HTTP-API
--------------

### SCR-1302
**Summary**: Added context_id field to TokenLogin json response

Added integer field `context_id` to tokenLogin JSON response, needed for successful request analyzing.

```
{
    "jsessionid": "<JSESSIONID>",
    "user":"<USER>",
    "user_id":10,
    "context_id":2,
    "url":"https://path/to/redirect"
} 
```



Behavioral Changes
------------------

### SCR-1310
**Summary**: Enabled Redis-based session storage by default

#### Changed default value for properties


* The already introduced Redis-based session storage is now enabled by default with this behavioral change. Precisely, the former added property `"com.openexchange.sessiond.redis.enabled"` is now assumed to be `"true"` if not specified otherwise.


* Furthermore, the property `"com.openexchange.sessionstorage.hazelcast.enabled"` is now assumed to be `"false"` if not specified otherwise.


* Please follow the instructions given at [this article](https://documentation.open-xchange.com/main/middleware/login_and_sessions/redis_session_storage.html) in order to set further config options for having the Middleware being orderly connected against running Redis backend.

#### Deprecation of former implementations

Moreover, the implementing classes for interface `com.openexchange.sessiond.SessiondService` and `com.openexchange.sessionstorage.SessionStorageService` are marked as deprecated. This applies to:


* The in-memory based `com.openexchange.sessiond.impl.SessiondServiceImpl` as well as
* The Hazelcast-backed `com.openexchange.sessionstorage.hazelcast.HazelcastSessionStorageService`




Configuration
-------------

### SCR-1317
**Summary**: Added configuration options to enable debugging/profiling SQL queries

Added new lean configuration options to trace queries and their execution/fetch times


* `com.openexchange.database.profileSQL`
  Enables to trace queries and their execution/fetch times. Default is `false`. Neither reloadable nor config-cascade aware.


* `com.openexchange.database.logger`
  The name of a class that implements 'com.mysql.cj.log.Log' that will be used to log messages to. Default is `'com.mysql.cj.log.Slf4JLogger'`. Neither reloadable nor config-cascade aware.

### SCR-1316
**Summary**: New Default Value for "com.openexchange.tools.images.transformations.maxSize"

To better support practical use cases, the default value for the configuration property `com.openexchange.tools.images.transformations.maxSize` is adjusted from `10485760` (10 MB) to `20971520` (20 MB).

### SCR-1309
**Summary**: Added lean property `com.openexchange.database.logWritesToNonLocalSegments`

Added lean property `com.openexchange.database.logWritesToNonLocalSegments` configuring whether to log writeable database accesses from non-local sites. Defaults to `false`

### SCR-1284
**Summary**: Add parameters to drive jump redirect for request analyzing

Added `context_id` and `user_id` parameters to drive jump redirect url `com.openexchange.drive.jumpLink`

New default value is `[protocol]://[hostname]/[uiwebpath]#[app]&[folder]&[id]&[context]&[user]`

### SCR-1211
**Summary**: Added several configuration options for scheduled mails

Added several lean configuration options for scheduled mails


* `com.openexchange.mail.scheduled.enabled`
  Switch to enable or disable the scheduled mail feature. Default is `true`. Both - reloadable and config-cascade aware.


* `com.openexchange.mail.scheduled.maxNumberOfScheduledMails`
  The max. allowed number of scheduled mails per user. Default is `1000`. Both - reloadable and config-cascade aware.


* `com.openexchange.mail.scheduled.maxNumberOfScheduledMailsPerHour`
  The max. allowed number of scheduled mails being sent per hour for a user. Default is `100`. Both - reloadable and config-cascade aware.


* `com.openexchange.mail.scheduled.checkFrequencyMinutes`
  The frequency in minutes when to check for due scheduled mails. Default is `30`. Reloadable, but not config-cascade aware.


* `com.openexchange.mail.scheduled.lookAheadMinutes`
  The look-ahead in minutes specifies the extra time added to current time when a scheduled mail is considered as due. Default is `35`. Reloadable, but not config-cascade aware.


* `com.openexchange.mail.scheduled.lockExpiryMinutes`
  The time in minutes when the lock marking a scheduled mail as "in processing" is considered as expired and thus may be newly acquired by another process. Default is `5`. Reloadable, but not config-cascade aware.


* `com.openexchange.mail.scheduled.lockRefreshMinutes`
  The time in minutes when the lock marking a scheduled mail as "in processing" is refreshed by lock-holding process. Default is `2`. Reloadable, but not config-cascade aware.



Database
--------

### SCR-1225
**Summary**: Added new tables for scheduled mail feature

Added new tables in user database for scheduled mail feature


```
CREATE TABLE scheduledMail(
 uuid BINARY(16) NOT NULL,
 cid INT4 unsigned NOT NULL,
 user INT4 unsigned NOT NULL,
 dateToSend BIGINT(64) unsigned NOT NULL,
 mailPath TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
 processing BIGINT(64) unsigned NOT NULL DEFAULT 0,
 meta TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
 PRIMARY KEY (uuid),
 KEY id (cid, user, uuid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

```


```
CREATE TABLE scheduledMailLock(
 cid INT4 unsigned NOT NULL DEFAULT 0,
 user INT4 unsigned NOT NULL DEFAULT 0,
 name VARCHAR(16) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
 stamp BIGINT(64) unsigned NOT NULL,
 PRIMARY KEY (cid, user, name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

```



Packaging/Bundles
-----------------

### SCR-1226
**Summary**: Added new bundles for scheduled mail feature




[8.19]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1308
**Summary**: Update vulnerable 3rd party libraries

*Target platform:*
New libraries:

 * jackson-core-2.15.3.jar
 * jackson-annotations-2.15.3.jar
 * jackson-dataformat-xml-2.15.3.jar
 * jackson-databind-2.15.3.jar
 * jackson-dataformat-cbor-2.15.3.jar
 * jackson-dataformat-yaml-2.15.3.jar
 * jackson-datatype-jsr310-2.15.3.jar
 * jackson-datatype-jsr353-2.15.3.jar
 * jackson-jakarta-rs-base-2.15.3.jar
 * jackson-jakarta-rs-json-provider-2.15.3.jar
 * jackson-jakarta-rs-xml-provider-2.15.3.jar
 * jackson-module-jakarta-xmlbind-annotations-2.15.3.jar
 * jackson-module-jaxb-annotations-2.15.3.jar
 * jakarta.activation-api-2.1.2.jar
 * jakarta.json-api-2.1.2.jar
 * jackrabbit-webdav-2.21.19-custom.jar
 * snappy-java-1.1.10.3.jar
 * commons-fileupload-1.5.jar

Removed libraries:

 * jackson-core-2.14.2.jar
 * jackson-annotations-2.14.2.jar
 * jackson-dataformat-xml-2.14.2.jar
 * jackson-databind-2.14.2.jar
 * jackson-dataformat-cbor-2.14.2.jar
 * jackson-dataformat-yaml-2.14.2.jar
 * jackson-datatype-jsr310-2.14.2.jar
 * jackson-datatype-jsr353-2.14.2.jar
 * jackson-jakarta-rs-base-2.14.2.jar
 * jackson-jakarta-rs-json-provider-2.14.2.jar
 * jackson-jakarta-rs-xml-provider-2.14.2.jar
 * jackson-module-jakarta-xmlbind-annotations-2.14.2.jar
 * jackson-module-jaxb-annotations-2.14.2.jar
 * jakarta.activation-api-2.1.0.jar
 * jakarta.activation-2.0.1.jar
 * jackrabbit-webdav-2.19.1.jar
 * sqlite-jdbc-3.19.3.jar
 * commons-fileupload-1.4.jar

----
*Bundle com.squareup.okhttp3:*
New libraries:

 * kotlin-stdlib-common-1.9.10.jar
 * kotlin-stdlib-1.9.10.jar
 * okio-jvm-3.5.0.jar
 * okio-3.5.0.jar
 * okhttp-4.11.0.jar
 * logging-interceptor-4.11.0.jar

Removed libraries:

 * kotlin-stdlib-common-1.7.22.jar
 * kotlin-stdlib-1.7.22.jar
 * okio-jvm-2.8.0.jar
 * okhttp-4.9.3.jar
 * logging-interceptor-4.9.3.jar

----
*Bundle com.ctc.wstx:*
New library:

 * woodstox-core-6.5.1.jar

Removed library:

 * woodstox-core-6.5.0.jar

----
*Bundle org.yaml.snakeyaml:*
New library:

 * snakeyaml-2.2.jar

Removed library:

 * snakeyaml-1.33.jar

----
*Bundle com.nimbus:*
New libraries:

 * json-smart-2.4.11.jar
 * accessors-smart-2.4.11.jar

Removed libraries:

 * json-smart-2.4.8.jar
 * accessors-smart-2.4.8.jar

### SCR-1266
**Summary**: Upgraded gson from 2.9.0 to 2.10.1

Upgraded the gson library from 2.9.0 to 2.10.1



API - HTTP-API
--------------

### SCR-1304
**Summary**: Dropped `shard` query parameter from SAML request

Dropped `shard` query paramter from SAML request


Configuration
-------------

### SCR-1307
**Summary**: New property to configure allowed URI schemes for external calendar attachments

In order to prevent inaccessible attachment references getting stored for appointments imported to App Suite, a new lean configuration property is introduced. 

Its value can be configured to a comma-separated list of URI schemes that are allowed be stored for externally linked attachments of appointments. Attachments with other URI schemes will be rejected/ignored during import:


```
com.openexchange.calendar.allowedAttachmentSchemes=http,https,ftp,ftps
```

The property is reloadable, and can be defined through the config cascade sown to level "context".

### SCR-1303
**Summary**: Dropped sharding related property

Dropped property `com.openexchange.server.shardName`

### SCR-1277
**Summary**: New properties for Segmenter Client Service

For accessing a segmenter service in a _sharded_ environment with multiple data centers ("Active/Active"), a new configuration property is introduced where the base URI to the service can be defined (empty by default):

```
com.openexchange.segmenter.baseUrl=
```

Also, a new configuration property is introduced through which the identifier of the 'local' site can be defined, defaulting to the value `default`.

```
com.openexchange.segmenter.localSiteId=default
```

Both properties are _reloadable_. By default, if no segmenter service URI is defined, a non-sharded environment is assumed where all segments are served by the local site itself.



Packaging/Bundles
-----------------

### SCR-1315
**Summary**: Deprecation of Kerberos Authentication

The Kerberos authentication integration that was available via supplementary package `open-xchange-authentication-kerberos` is now deprecated and subject for removal in a future release.

### SCR-1312
**Summary**: Removed obsolete bundle `com.openexchange.message.timeline`

As it is no longer used, bundle `com.openexchange.message.timeline` is removed, along with its reference in `open-xchange-core` package.

### SCR-1311
**Summary**: Removed obsolete Rhino Scripting 

As they're no longer used, the following bundles are removed, along with their references in `open-xchange-halo` package:

* `com.openexchange.scripting.rhino`
* `com.openexchange.scripting.rhino.apiBridge`

### SCR-1241
**Summary**: Added new bundles for the request analyzer feature

The following new bundles are added to `open-xchange-core` in order to support request routing in _sharded_ environments with multiple data centers ("Active/Active"):

* `com.openexchange.request.analyzer`
* `com.openexchange.request.analyzer.rest`
* `com.openexchange.segmenter.client`


[8.18]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1286
**Summary**: Updated lettuce library from v6.2.5 to v6.2.6

Updated lettuce library from v6.2.5 to v6.2.6 in bundle `io.lettuce`

### SCR-1285
**Summary**: Updated Netty NIO libraries from v4.1.94 to v4.1.97

Updated Netty NIO libraries from v4.1.94 to v4.1.97 in bundle `io.netty`



API - HTTP-API
--------------

### SCR-1300
**Summary**: Remove templating as valid format option

The publication and OXMF-based subscriptions features were removed with 7.10.2, see also MW-1089. Now, we remove a leftover within the API. The

`&format=template`

API parameter is no longer supported and will result in an error if used. 

### SCR-1297
**Summary**: Deprecate transport "websocket" in "pns" API

To get rid of the stateful socket between Frontend and App Suite MW, Switchboard will be the only service that maintains a socket connection to clients. Instead of pushing directly from MW to the Client, MW will just use a HTTP webhook of Switchboard to announce new events. Switchboard will then push to the client. 

Therefore the `websocket` transport identifier as used in actions `subscribe` and `unsubscribe` of the `pns` module in the HTTP API is now deprecated and will finally be removed in a future version.



Behavioral Changes
------------------

### SCR-1272
**Summary**: Convert mail user flags to UTF-8

Mail user flags are persisted in UTF-7 on the mail server. However, web clients like the App Suite UI do use UTF-8 as default encoding for strings in communication with the Middleware.

Instead of using user flags as-is, the Middleware now converts incoming or outgoing user flags as need, so web clients can use UTF-8 based strings for mail user flags as usual.



Configuration
-------------

### SCR-1301
**Summary**: Remove properties regarding user templating

With 7.10.2, we removed the publications and OXMF-based subscriptions features, see MW-1089. 

Now the last pieces of code belonging to those features were removed. Along the code, two properties that aren't needed anymore, have been removed:

```
com.openexchange.templating.trusted 
com.openexchange.templating.usertemplating
```

### SCR-1278
**Summary**: Added configuration option to enable/disable encoding of IMAP user flags

Added configuration option controlling whether IMAP user flags are supposed to be encoded using RFC2060's UTF-7 encoding. Thus allowing non-ascii strings being stored as user flags.

Added support for properties:


* `"com.openexchange.imap.useUTF7ForUserFlags"`
  Enables (or disables) whether IMAP user flags are supposed to be encoded/decoded using RFC2060's UTF-7 encoding. Default value is `"false"`. Config-cascade aware.


* `"com.openexchange.imap.primary.useUTF7ForUserFlags"`
  Enables (or disables) whether IMAP user flags are supposed to be encoded/decoded only for the primary IMAP account using RFC2060's UTF-7 encoding. Default value is `"false"`. Config-cascade aware. This property effectively overwrites `"com.openexchange.imap.encodeUserFlagsAsUTF7"` for primary IMAP accounts

### SCR-1229
**Summary**: Introduced new properties for Webhooks support

Introduced new lean properties for Webhooks support.

### Webhook properties


* `com.openexchange.webhooks.enabledIds`
  Specifies a comma-separated list of Webhook identifiers that are considered as enabled. Reloadable and config-cascade aware.

### Webhook PNS properties


* `com.openexchange.pns.transport.webhooks.enabled`
  Specifies whether the Webhook transport is enabled. Reloadable and config-cascade aware.


* `com.openexchange.pns.transport.webhooks.httpsOnly`
  Whether only HTTPS is accepted when communicating with a Webhook. Reloadable and config-cascade aware.


* `com.openexchange.pns.transport.webhooks.allowTrustAll`
  Whether SSL configuration for "trust all" is allowed. If set to "false" only valid certificates are accepted when communicating with a Webhook using a secure connection. Neither reloadable nor config-cascade aware.


* `com.openexchange.pns.transport.webhooks.allowLocalWebhooks`
  Whether Webhooks having end-point set to an internal address are allowed. Neither reloadable nor config-cascade aware.

### Webhook PNS HTTP properties


* `com.openexchange.pns.transport.webhooks.http.maxConnections`
  The number of total connections held in HTTP connection pool for communicating with a certain Webhook end-point. Reloadable and config-cascade aware.


* `com.openexchange.pns.transport.webhooks.http.maxConnectionsPerHost`
  The number of connections per route held in HTTP connection pool for communicating with a certain Webhook end-point. Reloadable and config-cascade aware.


* `com.openexchange.pns.transport.webhooks.http.connectionTimeout`
  Specifies the timeout in milliseconds until a connection is established to a certain Webhook end-point. Reloadable and config-cascade aware.


* `com.openexchange.pns.transport.webhooks.http.socketReadTimeout`
  Specifies the socket timeout in milliseconds, which is the timeout for waiting for data when communicating with a certain Webhook end-point.. Reloadable and config-cascade aware.

### Webhook configuration file

Added new configuration file `webhooks.yml` containing the static configurations for known Webhook end-points. That file is in YAML notation and expects the following structure


```
 <unique-identifier>:
     uri: <URI>
       String. The URI end-point of the Webhook. May be overridden during subscribe depending on "uriValidationMode".

     uriValidationMode: <uri-validation-mode>
       String. Specifies how the possible client-specified URI for a Webhook end-point is supposed to be validated against the URI
               from configured Webhook end-point. Possible values: `none`, `prefix`, and `exact`. For `none` no requirements given.
               Any client-specified URI is accepted. For `prefix` he client-specified and configured URI for a Webhook end-point are
               required to start with same prefix. For `exact` the client-specified and configured URI for a Webhook end-point are
               required to be exactly the same. `prefix` is default.

     webhookSecret: <webhook-secret>
       String. The value for the "Authorization" HTTP header to pass on calling Webhook's URI. May be overridden during subscribe.

     login: <login>
       String. The login part for HTTP Basic Authentication if no value for the "Authorization" HTTP header is specified. May be overridden during subscribe.

     password: <password>
       String. The password part for HTTP Basic Authentication if no value for the "Authorization" HTTP header is specified. May be overridden during subscribe.

     signatureSecret: <signature-secret>
       String. Specifies shared secret known by caller and Webhook host. Used for signing.

     version: <version>
       Integer. Specifies the version of the Webhook. Used for signing.

     signatureHeaderName: <signature-header-name>
       String. Specifies the name of the signature header that carries the signature.

     maxTimeToLiveMillis: <max-time-to-live>
       Number. The max. time to live in milliseconds for the Webhook before considered as expired. If absent Webhook "lives" forever.

     maxNumberOfSubscriptionsPerUser: <max-number-per-user>
       Number. The max. number of subscriptions for this Webhook allowed for a single user. Equal or less than 0 (zero) means infinite.

     allowSharedUri: <allow-shared-uri>
       Boolean. Whether the same URI can be used by multiple different users or not. Optional, defaults to `true`.


```

#### Example

`webhooks.yml`

```
mywebhook:
    uri: https://my.endpoint.com:8080/webhook/event
    webhookSecret: supersecret
    signatureSecret: da39a3ee5e6b4b
    version: 1
    signatureHeaderName: X-OX-Signature
    maxTimeToLiveMillis: 2678400000
    maxNumberOfSubscriptionsPerUser: 2
    uriValidationMode: prefix

```



Database
--------

### SCR-1296
**Summary**: Changed column 'propertyValue' of table 'subadmin_config_properties' to be of type TEXT

Modified Config-DB to have column 'propertyValue' of table 'subadmin_config_properties' to be of type TEXT

New table layout is therefore:


```
CREATE TABLE subadmin_config_properties (
	sid INT4 UNSIGNED NOT NULL,
	propertyKey VARCHAR(64) CHARACTER SET latin1 NOT NULL DEFAULT '',
	propertyValue TEXT CHARACTER SET latin1 NOT NULL DEFAULT '',
	PRIMARY KEY (sid, propertyKey)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_unicode_ci;

```

This database change contains no update task since this is a modification of the Config-DB, which is performed through liquibase framework on node start-up

### SCR-1258
**Summary**: Added column `meta` to table `pns_subscription`


<warning>
**Update Task**
com.openexchange.pns.subscription.storage.groupware.PnsSubscriptionsAddMetaColumTask
</warning>


Added `TEXT` column `meta` to table `pns_subscription`:


```
meta TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL

```



Packaging/Bundles
-----------------

### SCR-1294
**Summary**: Added new bundle for Config-Cascade implementation

Added new bundle `com.openexchange.config.cascade.impl` containing Config-Cascade implementation. This separates the API classes from actual implementation and allows less dependencies. That new bundle is added to `open-xchange-core` package

### SCR-1228
**Summary**: New bundles for Webhooks support

Introduced new bundles for Webhooks support


* `com.openexchange.webhooks`
* `com.openexchange.pns.transport.webhooks`




[8.17]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1275
**Summary**: Upgraded MySQL Connector for Java

Upgraded MySQL Connector for Java from v8.0.29 to v8.0.33 in OSGi  target platform

### SCR-1270
**Summary**: Updated Google Client API libraries

Updated Google Client API libraries

* `google-api-client-1.35.1.jar` to `google-api-client-2.2.0.jar`
* `google-api-client-appengine-1.35.1.jar` to `google-api-client-appengine-2.2.0.jar`
* `google-api-client-gson-1.35.1.jar` to `google-api-client-gson-2.2.0.jar`
* `google-api-client-jackson2-1.35.1.jar` to `google-api-client-jackson2-2.2.0.jar`
* `google-api-client-protobuf-1.35.1.jar` to `google-api-client-protobuf-2.2.0.jar`
* `google-api-client-servlet-1.35.1.jar` to `google-api-client-servlet-2.2.0.jar`
* `google-api-client-xml-1.35.1.jar` to `google-api-client-xml-2.2.0.jar`

* `google-api-services-calendar-v3-rev20220520-1.32.1.jar` to `google-api-services-calendar-v3-rev20230602-2.0.0.jar`
* `google-api-services-drive-v3-rev20220508-1.32.1.jar` to `google-api-services-drive-v3-rev20230610-2.0.0.jar`
* `google-api-services-gmail-v1-rev20220404-1.32.1.jar` to `google-api-services-gmail-v1-rev20230612-2.0.0.jar`
* `google-api-services-oauth2-v2-rev20200213-1.32.1.jar` to `google-api-services-oauth2-v2-rev20200213-2.0.0.jar`
* `google-api-services-people-v1-rev20220531-1.32.1.jar` to `google-api-services-people-v1-rev20230103-2.0.0.jar`



API - HTTP-API
--------------

### SCR-1232
**Summary**: Extended `updateAttendee` call with `tranps` parameter

To allow per-attendee transparency for a certain event, the HTTP API call `updateAttedee` was extended by the optional parameter `transp`.
Allowed values for the new parameter are:

```
TRANSPARENT
OPAQUE
```

If the transparency is set for a certain attendee, the event transparency for the corresponding event is adjusted implicitly, including all other chronos related calls.


Database
--------

### SCR-1264
**Summary**: Update task to insert missing references into 'filestore2user' table


<warning>
**Update Task**
com.openexchange.groupware.update.tasks.Filestore2UserUpdateReferencesTask
</warning>


To ensure the table `filestore2user` (in config-db) holds all references to users with individual filestores in a groupware schema , a new update task named `com.openexchange.groupware.update.tasks.Filestore2UserUpdateReferencesTask` is introduced.

API - SOAP
--------------

### SCR-1280
**Summary**: Added possibility to manage user sessions through SOAP interface

Added the possibility to manage user sessions through the new OXSessionService SOAP interface

Packaging/Bundles
--------------

### SCR-1281
**Summary**: Added new bundle/package to manage sessions via SOAP

Added new bundle `com.openexchange.sessiond.soap` to manage sessions via SOAP. That new bundle is contained in newly introduced package `open-xchange-sessiond-soap`

[8.16]
======

General
-------

### SCR-1252
**Summary**: Updated Netty NIO libraries

Updated Netty NIO libraries from v4.1.89 to v4.1.94 in bundle `io.netty`



3rd Party Libraries/License Change
----------------------------------

### SCR-1256
**Summary**: Upgraded Javassist Library

Library Javasisst is upgraded to v3.29.2-GA in target platform `com.openexchange.bundles` and bundle `com.openexchange.test`.

### SCR-1255
**Summary**: Updated Apache Tika library

Updated Apache Tika library from v2.6.0 to v2.8.0 in bundle `com.openexchange.tika.util`

### SCR-1253
**Summary**: Updated lettuce library

Updated lettuce library from v6.2.3 to v6.2.5 in bundle `io.lettuce`

### SCR-1247
**Summary**: Updated pushy library from v0.15.1 to v0.15.2

Updated pushy library from v0.15.1 to v0.15.2 in bundle `com.eatthepath.pushy`

### SCR-1245
**Summary**: Updated metadata-extractor from v2.17.0 to v2.18.0

Updated 3rd party library `metadata-extractor` from v2.17.0 to v2.18.0 in bundle `com.drew`

### SCR-1244
**Summary**: Updated htmlcleaner from v2.22 to v2.29

Updated 3rd party library `htmlcleaner` from v2.22 to v2.29 in target platform

### SCR-1243
**Summary**: Updated dnsjava from v3.5.1 to v3.5.2

Updated 3rd party library `dnsjava` from v3.5.1 to v3.5.2 in target platform

### SCR-1242
**Summary**: Updated Apache HttpCore and HttpClient libraries

Updated Apache HttpCore and HttpClient libraries

* Updated HttpCore from v4.4.15 to v4.4.16
* Updated HttpClient from v4.5.13 to v4.5.14

### SCR-1234
**Summary**: Updated Hazelcast Core Module

Updated Hazelcast Core Module from v5.2.1 to v5.3.1

### SCR-1231
**Summary**: Updated OSGi target platform bundles

### Updated OSGi target platform bundles

* `org.eclipse.osgi.services_3.10.200.v20210723-0643.jar` updated to `org.eclipse.osgi.services_3.11.100.v20221006-1531.jar`
* `org.eclipse.osgi.util_3.6.100.v20210723-1119.jar` updated to `org.eclipse.osgi.util_3.7.200.v20230103-1101.jar`
* `org.eclipse.osgi_3.18.0.v20220516-2155.jar` updated to `org.eclipse.osgi_3.18.400.v20230509-2241.jar`

### Added new OSGi bundles to target platform

Since content of shipped `org.eclipse.osgi.services` bundle has been changed. Missing classes/interfaces are now contained in separate OSGi bundles.

* Added `org.osgi.annotation.bundle_2.0.0.202202082230.jar`
* Added `org.osgi.annotation.versioning_1.1.2.202109301733.jar`
* Added `org.osgi.service.cm_1.6.1.202109301733.jar`
* Added `org.osgi.service.component_1.5.1.202212101352.jar`
* Added `org.osgi.service.component.annotations_1.5.1.202212101352.jar`
* Added `org.osgi.service.device_1.1.1.202109301733.jar`
* Added `org.osgi.service.event_1.4.1.202109301733.jar`
* Added `org.osgi.service.metatype_1.4.1.202109301733.jar`
* Added `org.osgi.service.metatype.annotations_1.4.1.202109301733.jar`
* Added `org.osgi.service.prefs_1.1.2.202109301733.jar`
* Added `org.osgi.service.provisioning_1.2.0.201505202024.jar`
* Added `org.osgi.service.repository_1.1.0.201505202024.jar`
* Added `org.osgi.service.upnp_1.2.1.202109301733.jar`
* Added `org.osgi.service.useradmin_1.1.1.202109301733.jar`
* Added `org.osgi.service.wireadmin_1.0.2.202109301733.jar`
* Added `org.osgi.util.function_1.2.0.202109301733.jar`
* Added `org.osgi.util.measurement_1.0.2.201802012109.jar`
* Added `org.osgi.util.position_1.0.1.201505202026.jar`
* Added `org.osgi.util.promise_1.3.0.202212101352.jar`
* Added `org.osgi.util.xml_1.0.2.202109301733.jar`




API - HTTP-API
--------------

### SCR-1235
**Summary**: Introduced a new action to the 'mail' module for exporting mails as PDFs

Introduced the action `export_PDF` to the `mail` module.

It is a `PUT` request and has the following URL parameters:

* `folder`: defines the mail folder which holds the mail that shall be exported
* `id`: defines the mail id

The request also accepts a mandatory JSON body with the following attributes:

* `folder_id`: Defines the drive folder in which the exported PDF/A document will be saved. This option is required.
* `pageFormat`: Defines the page format of the export document. It can either be `a4` (which is the default behaviour) or `letter`. This option is not required. If absent, the page format will be derived from the user's locale setting (for `us` or `ca` the page format will be `letter` and for anything else `a4`).
* `preferRichText`: If this option is enabled then, if an e-mail message contains both text and HTML versions of the body, then the latter is preferred and converted to a PDF/A document before it is appended to the exported PDF/A document. If only the text version is available, and the option is enabled, then the text version is converted to a PDF/A document and appended to the exported PDF/A document. This option is not required and by default is set to `true`.
* `includeExternalImages`: If this option is enabled then, and the e-mail contains any external inline images, then those images will be fetched from their respective sources and included to the exported PDF/A document at their supposed positions. This option is not required and is by default `false`.
* `appendAttachmentPreviews`: If this option is enabled, then any previewable attachment (i.e., documents and pictures) is converted from their original format, e.g., from docx or tiff, to a PDF/A document and is appended as one or more pages to the exported PDF/A document. This option is not required and is `false` by default.
* `embedAttachmentPreviews`: If this option is enabled, then any previewable attachment is converted from their original format to a PDF/A document and is embedded as an attachment to the exported PDF/A document. This option is not required and is `false` by default.
* `embedRawAttachments`: If this option is enabled, then all attachments are embedded without further processing to the exported PDF/A document as attachments. This option is not required and is `false` by default.
* `embedNonConvertibleAttachments`: If this option is enabled, then all attachments (previewable and non-previewable, i.e., zips, mp4s, etc.) are embedded without further processing to the exported PDF/A document as attachments. This option is not required and is `false` by default.



Configuration
-------------

### SCR-1240
**Summary**: Introduced a new capability to activate the PDF MailExportService

Introduced the capability `mail_export_pdf` to activate the PDF MailExportService.

### SCR-1239
**Summary**: Introduced new properties for the CollaboraPDFAConverter

Introduced the following properties to configure the \`CollaboraPDFAConverter`:

* `com.openexchange.mail.exportpdf.pdfa.collabora.enabled`: Defines whether the collabora online converter is enabled. Defaults to false
* `com.openexchange.mail.exportpdf.pdfa.collabora.url`: The Collabora URL to use: Allows to specify a dedicated Collabora service only for PDFA creation. By default is empty and uses the server configured via the property \`com.openexchange.mail.exportpdf.collabora.url`.

### SCR-1238
**Summary**: Introduced new properties for the GotenbergMailExportConverter

Introduced the following properties to configure the `GotenbergMailExportConverter`:

* `com.openexchange.mail.exportpdf.gotenberg.enabled`: Defines whether the gotenberg online converter is enabled. Defaults to false
* `com.openexchange.mail.exportpdf.gotenberg.url`: Defines the base URL of the Gotenberg Online server. Defaults to `http://localhost:3000`
* `com.openexchange.mail.exportpdf.gotenberg.fileExtensions`: Defines a comma separated list of file extensions that are handled by the gotenberg converter. Defaults to `htm, html`.
* `com.openexchange.mail.exportpdf.gotenberg.pdfFormat`: Specifies which PDF format to use. "PDF/A-1a", "PDF/A-2b" and "PDF/A-3b" are supported formats, or "PDF" for regular PDF. Defaults to "PDF"

### SCR-1237
**Summary**: Introduced new properties for the CollaboraMailExportConverter

Introduced the following properties to configure the `CollaboraMailExportConverter`:

* `com.openexchange.mail.exportpdf.collabora.enabled`: Defines whether the collabora online converter is enabled. Defaults to false
* `com.openexchange.mail.exportpdf.collabora.url`: Defines the base URL of the Collabora Online server. Defaults to `http://localhost:9980`
* `com.openexchange.mail.exportpdf.collabora.fileExtensions`: Defines a comma separated list of file extensions that are handled by the collabora converter. Defaults to `sxw, odt, fodt, sxc, ods, fods, sxi, odp, fodp, sxd, odg, fodg, odc, sxg, odm, stw, ott, otm, stc, ots, sti, otp std, otg, odb, oxt, doc, dot xls, ppt, docx, docm, dotx, dotm, xltx, xltm, xlsx, xlsb, xlsm, pptx, pptm, potx, potm, wpd, pdb, hwp, wps, wri, wk1, cgm, dxf, emf, wmf, cdr, vsd, pub, vss, lrf, gnumeric, mw, numbers, p65, pdf, jpg, jpeg, gif, png, dif, slk, csv, dbf, oth, rtf, txt, html, htm, xml`.
* `com.openexchange.mail.exportpdf.collabora.imageReplacementMode`: Defines the mode on how to handle/replace inline images. Defaults to `distributedFile`.

### SCR-1236
**Summary**: Introduced new properties for the MailExportService

Introduced the following properties to configure the `MailExportService`:

* `com.openexchange.mail.exportpdf.concurrentExports`: Defines the maximum concurrent mail exports that the server is allowed to process. If the limit is reached an error will be returned to the client, advising it to retry again in a while. Defaults to 10. 
* `com.openexchange.mail.exportpdf.pageMarginTop`:  Defines the top margin (in millimeters) of the exported pages. Defaults to 12.7 millimeters (0.5 inches).
* `com.openexchange.mail.exportpdf.pageMarginBottom`:  Defines the bottom margin (in millimeters) of the exported pages. Defaults to 12.7 millimeters (0.5 inches).
* `com.openexchange.mail.exportpdf.pageMarginLeft`:  Defines the left margin (in millimeters) of the exported pages. Defaults to 12.7 millimeters (0.5 inches).
* `com.openexchange.mail.exportpdf.pageMarginRight`:  Defines the right margin (in millimeters) of the exported pages. Defaults to 12.7 millimeters (0.5 inches).
* `com.openexchange.mail.exportpdf.headerFontSize`: Defines the font size of the exported mail's headers. Defaults to 12 points.
* `com.openexchange.mail.exportpdf.bodyFontSize`: Defines the font size of the exported mail's body. Defaults to 12 points.
* `com.openexchange.mail.exportpdf.autoPageOrientation`: Defines whether PDF pages will be auto-oriented in landscape mode whenever a full page appended image is in landscape mode. Defaults to false



Database
--------

### SCR-1233
**Summary**: Update encryption for passwords of anonymous guest users

<warning>
**Update Task**
com.openexchange.groupware.update.tasks.RecryptGuestUserPasswords</p>
</warning>

Update encryption for anonymous guest user passwords using newly introduced mechanisms with implicit salt

Table `user` altered, extend column `userPassword` from `VARCHAR(128)` to `VARCHAR(512)`


[8.15]
======

General
-------

### SCR-1227
**Summary**: Enhanced existent SOAP end-points by standard "Scheduled" folder

Enhanced existent SOAP end-points by standard "Scheduled" folder. The folder that holds such E-Mails that are scheduled for being sent at a later time.

To do so, the `"User"` data object contained in several SOAP end-points has been extended by `"mail_folder_scheduled_full_name"` element to output/specify the standard "Scheduled" folder.


### SCR-1201
**Summary**: Added separate bundle offering HTTP liveness end-point

Added separate bundle `com.openexchange.http.liveness` part of `open-xchange-core` package list that offers the HTTP liveness end-point at configured HTTP host name (default "127.0.0.1") and liveness port (default 8016).


Configuration
-------------

### SCR-1224
**Summary**: Add property `com.openexchange.log.extensionHttpHeaders`

`com.openexchange.log.extensionHttpHeaders` defines a comma separated list of HTTP headers that shall additionally be logged for incoming requests

Example
```
com.openexchange.log.extensionHttpHeaders=X-custom-Header,X-host
```

The property is neither reloadable nor ConfigCascade-aware.


Database
--------

### SCR-1225
**Summary**: Added new tables for scheduled mail feature

Added new tables in user database for scheduled mail feature


```

CREATE TABLE scheduledMail(
 uuid BINARY(16) NOT NULL,
 cid INT4 unsigned NOT NULL,
 user INT4 unsigned NOT NULL,
 dateToSend BIGINT(64) unsigned NOT NULL,
 mailPath TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
 processing BIGINT(64) unsigned NOT NULL DEFAULT 0,
 meta TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
 PRIMARY KEY (uuid),
 KEY id (cid, user, uuid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

```



```

CREATE TABLE scheduledMailLock(
 cid INT4 unsigned NOT NULL DEFAULT 0,
 user INT4 unsigned NOT NULL DEFAULT 0,
 name VARCHAR(16) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
 stamp BIGINT(64) unsigned NOT NULL,
 PRIMARY KEY (cid, user, name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

```


### SCR-1223
**Summary**: Update task to add the "claim" column to "calendar_alarm_trigger" table


<warning>
**Update Task**
com.openexchange.chronos.storage.rdb.groupware.CalendarAlarmTriggerAddClaimColumnTask</p>
</warning>


Adds the "claim" column to "calendar_alarm_trigger" table


[8.14]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1219
**Summary**: Upgraded JSoup library

Upgraded JSoup library in target platform (`con.openexchange.bundles`) from v1.15.3 to v1.16.1



API - HTTP-API
--------------

### SCR-1216
**Summary**: Accept parameter `harddelete` for composition space's delete end-point

Accept boolean parameter `"harddelete"` for composition space's delete end-point


```
DELETE /mailcompose/draft.xyz?harddelete=true
```


If set to `"true"` any associated draft message for the denoted composition space gets hard-deleted. That is no copy is created in standard trash folder.

### SCR-1213
**Summary**: New Flag `all_others_declined` for Events

The "flags" enumeration for events in `chronos` module of the HTTP API is extended by the value `all_others_declined`. If set, all other individual attendees in the event have a participation status of *declined*, which could be used by clients to show a hint that one might be alone in a meeting.

See [https://documentation.open-xchange.com/latest/middleware/calendar/implementation_details.html#event-flags] for further details.

### SCR-1198
**Summary**: New Settings for Free/Busy Visibility in JSlob

The `io.ox/calendar` JSlob entry is extended by the following item which indicates the free/busy visibility of the user:

```
{
  "id": "io.ox/calendar",
  "tree": {
    "chronos": {
      "freeBusyVisibility": "all",
    }
  },
  "meta": {
    "chronos": {
      "freeBusyVisibility": {
        "possibleValues": [
          "all",
          "internal-only",
          "none"
        ],
        "configurable": true
      }
    }
  }
}
```

Within the `meta` section, clients are able to derive the possible values - the enumeration will only yield the value `internal-only` if cross-context features are available. Also, the "configurable" flag will indicate whether the property is settable by the user or not.



Configuration
-------------

### SCR-1220
**Summary**: Introduce new properties for DAV client matching

Once in a while, vendors like Apple decide to change the User Agents of their products in a way, we don't recognize the matching clients anymore. Thus, the Open Xchange server isn't able to apply special handling for those clients. This leads to subsequent problems and errors.

Until 8.14, the user agent matching was a static, programmatically pre-defined process. For every user agent's change, there needed to be a patch applied. Now, the mechanism is replaced by a more dynamically approach:

Administrators can define regular expressions for the known \*DAV clients. In detail, the following properties are added for known \*DAV clients:



```
com.openexchange.dav.useragent.mac_calendar
com.openexchange.dav.useragent.mac_contacts
com.openexchange.dav.useragent.ios
com.openexchange.dav.useragent.ios_reminders
com.openexchange.dav.useragent.thunderbird_lightning
com.openexchange.dav.useragent.thunderbird_cardbook
com.openexchange.dav.useragent.em_client
com.openexchange.dav.useragent.ox_sync
com.openexchange.dav.useragent.caldav_sync
com.openexchange.dav.useragent.carddav_sync
com.openexchange.dav.useragent.smooth_sync
com.openexchange.dav.useragent.davdroid
com.openexchange.dav.useragent.davx5
com.openexchange.dav.useragent.outlook_caldav_synchronizer
com.openexchange.dav.useragent.windows_phone
com.openexchange.dav.useragent.windwos
```

All properties have pre-defined default values and are reloadable.

### SCR-1218
**Summary**: New config option for sanitizing CSV cell content on contact export

New lean config option `"com.openexchange.export.csv.sanitize"` for sanitizing CSV cell content on contact export. Default is `false`. Reloadable, but not config-cascade aware

### SCR-1217
**Summary**: New property to limit number of considered filestore candidates

New lean property `com.openexchange.admin.limitFilestoreCandidates` to limit number of considered filestore candidates to a reasonable amount when determining the filestore to use for a new context/user. Neither reloadable, nor config-cascade aware. Default is `100`.

### SCR-1215
**Summary**: Accept specifying a max. running time that must not be exceeded by execution of an individual health check

Accept new lean property for specifying a max. running time that must not be exceeded by execution of an individual health check

* `com.openexchange.health.maxRunningTimeSeconds`
  The max. allowed running time in seconds for an individual health check. It a check's execution is canceled if it exceeds that running time. A value of equal to or less than zero 0 (zero) ignores this setting. Default is 5. Reloadbale, but not config-cascade aware.

### SCR-1197
**Summary**: New Properties for Free/Busy Visibility

In order to define the default free/busy visibility setting of users, and to control whether it is changeable by end users, the following new lean configuration properties are introduced:

 * `com.openexchange.calendar.freeBusyVisibility.default=all`: Defines the default free/busy visibility setting to assume unless overridden by the user. Possible values are:
     * `none` to not expose a user's availability to others at all
     * `internal-only` to make the free/busy data available to other users within the same context
     * `all` to expose availability data also beyond context boundaries (i.e. for cross-context- or other external access if configured)
 * `com.openexchange.calendar.freeBusyVisibility.protected=false`: Configures if the default value that determines if public calendar folders from the default account are considered for synchronization may be overridden by the user or not.

More details are available at [https://documentation.open-xchange.com/components/middleware/config/latest/#mode=search&term=com.openexchange.calendar.freeBusyVisibility] .


[8.13]
======

General
-------

### SCR-1201
**Summary**: Added separate bundle offering HTTP liveness end-point

Added separate bundle `com.openexchange.http.liveness` part of `open-xchange-core` package list that offers the HTTP liveness end-point at configured HTTP host name (default "127.0.0.1") and liveness port (default 8016).


API - HTTP-API
--------------

### SCR-1183
**Summary**: Deprecate `delivery=view` and `content_disposition=inline` options in HTTP API

The parameter options `delivery=view` and `content_disposition=inline` and the possibility to let the client define the content type of documents and attachments, can be used to inject executable scripts into data that is rendered in browsers. This lead to several bugs in the past.
Therefore the usage of those options is deprecated and will be removed.


API - RMI
---------

### SCR-1207
**Summary**: Additional parameter 'auth' for data modification RMI services.

The following registered RMI services now require auth parameters for data modification interfaces. The parameter `com.openexchange.auth.Credentials auth` needs to be provided.

```
DBMigrationRMIService
OXContextGroup
RemoteAdvertisementService
ExternalAccountRMIService
RemoteCompositionSpaceService
SocketLoggerRMIService
LoginCounterRMIService
GABRestorerRMIService
SessiondRMIService
ChronosRMIService
ContactStorageRMIService
DataExportRMIService
ConsistencyRMIService
ContextRMIService
FileChecksumsRMIService
ResourceCacheRMIService
ShareRMIService
PushRMIService
UpdateTaskRMIService
LogbackConfigurationRMIService -> java.lang.String user, java.lang.String password
```

Behavioral Changes
------------------

### SCR-1208
**Summary**: Deprecation of Internal OAuth Authorization Server

Certain APIs of the App Suite middleware can be accessed via OAuth 2.0. In this scenario, the middleware typically acts as resource server only, and the whole client- / grant management is done by an external IDM acting as authorization server. See [the documentation|https://documentation.open-xchange.com/latest/middleware/login_and_sessions/oauth_2.0_provider/01_operator_guide.html] for further details.

Mainly as demo/showcase, it has also been possible to configure the middleware to act as OAuth authorization server itself, with integrated client- and grant management. Since this never was or meant to be used in production, this part of the OAuth provider is now deprecated, and will be removed in an upcoming version.

In practical terms, this means that the setting `auth_server` for [com.openexchange.oauth.provider.mode|https://documentation.open-xchange.com/components/middleware/config/latest/#mode=search&term=com.openexchange.oauth.provider.mode] will no longer be available, along with dependent features and functionality.


Configuration
-------------

### SCR-1203
**Summary**: New property `com.openexchange.share.guestEmailCheckRegex`

In order to prevent creation of guest users with certain email addresses, a new lean configuration property `com.openexchange.share.guestEmailCheckRegex` is introduced. The property is empty by default, reloadable and config-cascade aware.

It allows the definition of a regular expression pattern for email addresses of invited guest users. If defined, the email address of newly invited named guest users must additionally match the pattern (besides regular RFC 822 syntax checks, which are always performed), otherwise creation of the guest user is denied. The pattern is used in a case-insensitive manner.

This may be used to prevent specific email address domains for guests, e.g. by defining a pattern like

```
^((?!(?:@example\.com\s*$)|(?:@example\.org\s*$)).)*$
```

See https://documentation.open-xchange.com/components/middleware/config/latest/#mode=search&term=com.openexchange.share.guestEmailCheckRegex for further details.

### SCR-1191
**Summary**: New property to control format of internal scheduling mails

In order to control whether scheduling-related notification mails to other internal entities are sent as regular iMIP message (including iCalendar attachment) or not, a new lean configuration property named `com.openexchange.calendar.useIMipForInternalUsers` is introduced. It defaults to `false`, is reloadable, and can be set through the config-cascade down to "context" level.

Since automatic scheduling takes place within a context, attendee and organizer copies of appointments are in sync implicitly, and updates don't need to be distributed via iMIP. However, still enabling iMIP mails (in favor of notification messages only) also for internal users may be useful if external client applications are in use, or to ease forwarding invitations to others.

### SCR-1158
**Summary**: Disable mail push implementations by default, made existing properties reloadable

Changed default value for `enabled` properties for mail push features to `false`:
 * `com.openexchange.push.dovecot.enabled`
 * `com.openexchange.push.imapidle.enabled`
 * `com.openexchange.push.mail.notify.enabled`
 * `com.openexchange.push.malpoll.enabled`

Refactored mail push configuration, now all existing mail push related properties are lean and reloadable:
 * `com.openexchange.push.dovecot.*`
 * `com.openexchange.push.imapidle.*`
 * `com.openexchange.push.mail.notify.*`
 * `com.openexchange.push.malpoll.*`



Database
--------

### SCR-1186
**Summary**: New column `uuid` for table `server` in Config-DB

<warning>
**Update Task**
8.12:server:addUuidColumn / com.openexchange.database.internal.change.custom.ServerAddUuidColumnCustomTaskChange</p>
</warning>

The table `server` in the config database will get extended by a new column named `uuid` with the following column definition:

```
`uuid` BINARY(16) NOT NULL
```


This will happen through the Liquibase change set with id "8.12:server:addUuidColumn", using the custom change implemented in class `com.openexchange.database.internal.change.custom.ServerAddUuidColumnCustomTaskChange`. 


[8.12]
======

General
-------

### SCR-1195
**Summary**: New default value for `com.openexchange.sessiond.maxSession` property

With introduction of Redis-backed session storage the property `com.openexchange.sessiond.maxSession` specifying the max. allowed number of sessions becomes obsolete. That pretty old property's intention is to avoid memory problems on Middleware nodes hosting sessions node-local in memory. That is no more the case with Redis.

Hence, the old default value of "50000" for that property is changed to "0" (unlimited) in file `/opt/open-xchange/etc/sessiond.properties`.



API - HTTP-API
--------------

### SCR-1200
**Summary**: Extended the mailfilter?action=config response to include blocked action commands for the apply action

To allow a client to disable the apply button for filter rules with blocked action commands, the response of the action=config call has been extended so that the options object now contains a 'blockedApplyActions' field which contains a string array of all the blocked actions.



Configuration
-------------

### SCR-1199
**Summary**: Introduced the new lean property 'com.openexchange.mail.filter.options.apply.blockedActions' which allows to block certain mail filter actions from the apply action

Introduced the new lean property 'com.openexchange.mail.filter.options.apply.blockedActions' which defaults to "redirect". This property accepts a comma separated lists of mail filter actions which will be denied from the apply mail filter action. This helps, for example, to prevent that a message delivery system is overwhelmed by a lot of simultanous redirect actions.


### SCR-1120
**Summary**: Allow enforcing 'STARTTLS' for IMAP, POP3, SMTP, sieve

Added a few lean properties to enforce usage of `STARTTLS`.

IMAP related properties:
 `com.openexchange.imap.requireTls`
 `com.openexchange.imap.primary.requireTls`

POP3 related properties
 `com.openexchange.pop3.requireTls`

SMTP related properties:
 `com.openexchange.smtp.requireTls`
 `com.openexchange.smtp.primary.requireTls`

Sieve related properties:
 `com.openexchange.mail.filter.requireTls`

All properties are reloadable and config-cascade aware. All properties default to `true`



[8.11]
======

API - Java
----------

### SCR-1145
**Summary**: Refactored CardDAV to use IDBasedContactsAccess

Interfaces changed due to refactoring CardDAV to use `IDBasedContactsAccess`

Added methods in `com.openexchange.contact.provider.composition.IDBasedContactsAccess`:
`Map<String, UpdatesResult<Contact>> getUpdatedContacts(List<String>, Date)` - Gets lists of new and updated as well as deleted contacts since a specific timestamp in certain folders
`Map<String, SequenceResult> getSequenceNumbers(List<String>)` - Gets the sequence numbers of certain contacts folders, which is the highest timestamp of all contained items
`String getCTag(String)` - Retrieves the CTag (Collection Entity Tag) for a folder

Added methods in `com.openexchange.contact.provider.folder.FolderSyncAware`:
`Map<String, UpdatesResult<Contact>> getUpdatedContacts(List<String>, Date)` - Gets lists of new and updated as well as deleted contacts since a specific timestamp in certain folders
`Map<String, SequenceResult> getSequenceNumbers(List<String>)` - Gets the sequence numbers of certain contacts folders, which is the highest timestamp of all contained items



Behavioral Changes
------------------

### SCR-1146
**Summary**: External contacts providers are now synced via CardDAV

External contacts providers are now synced via CardDAV after refactoring to use IDBasedContactsAccess



Configuration
-------------

### SCR-1193
**Summary**: New Property "com.openexchange.admin.autoDeleteGuestsUsingFilestore"

In case a per-user filestore is associated to a guest user, and the "parent" user owning this filestore is deleted, the guest account is purged implicitly as well by default. In order to prevent that, a new lean, reloadable and config-cascade-aware property is introduced: `com.openexchange.admin.autoDeleteGuestsUsingFilestore`.

See https://documentation.open-xchange.com/components/middleware/config/latest/#mode=search&term=com.openexchange.admin.autoDeleteGuestsUsingFilestore for further details.

### SCR-1190
**Summary**: Specify a timeout when reading responses from IMAP server after a command has been issued

Added new lean property `"com.openexchange.imap.readResponsesTimeout"` accepting to define a timeout in milliseconds when reading responses from IMAP server after a command has been issued. That timeout does only apply to subscribed (not provisioned) IMAP accounts; neither primary nor secondary ones.

Default value is 60000 (one minute). A value equal to zero is infinite timeout. Reloadable and config-cascade aware.

### SCR-1189
**Summary**: Option to enable/disable usage of XCLIENT sieve extension

Added new lean configuration option `"com.openexchange.mail.filter.allowXCLIENT"` to explicitly enable (or disable) usage of the XCLIENT sieve extension.
When a sieve server announces support for the XCLIENT command, a sieve client may send information that overrides one or more client-related session attributes.

Default is false (not enabled). Reloadable and config-cascade aware.

### SCR-1188
**Summary**: Introduced a new lean property which allows to omit certain labels

Introduced the new lean property: com.openexchange.http.metrics.label.filter which allows to omit certain labels from http api metrics.



Packaging/Bundles
-----------------

### SCR-1184
**Summary**: Removed com.openexchange.hazelcast.upgrade* bundles

The following upgrade bundles are no longer needed in cloud environments after we introduced the new pre-upgrade framework (MW-1785):
 * com.openexchange.hazelcast.upgrade324
 * com.openexchange.hazelcast.upgrade312
 * com.openexchange.hazelcast.upgrade355
 * com.openexchange.hazelcast.upgrade371
 * com.openexchange.hazelcast.upgrade311
 * com.openexchange.hazelcast.upgrade381
 * com.openexchange.hazelcast.upgrade411
 * com.openexchange.hazelcast.upgrade3100

Corresponding package definitions have been removed as well:
 * open-xchange-cluster-upgrade-from-76x
 * open-xchange-cluster-upgrade-from-780-782
 * open-xchange-cluster-upgrade-from-783
 * open-xchange-cluster-upgrade-from-784
 * open-xchange-cluster-upgrade-from-7100-7101
 * open-xchange-cluster-upgrade-from-7102
 * open-xchange-cluster-upgrade-from-7103-7104
 * open-xchange-cluster-upgrade-from-7105



[8.10]
======

3rd Party Libraries/License Change
----------------------------------

### SCR-1139
**Summary**: Upgraded Socket.IO server components

Upgraded Socket.IO server components in bundle "`com.openexchange.socketio`" to support Engine.IO v4 and Socket.IO v3

* engine.io-server-1.3.5.jar  -->  engine.io-server-6.1.0.jar
* socket.io-server-1.0.3.jar  -->  socket.io-server-4.0.1.jar



API - HTTP-API
--------------

### SCR-1180
**Summary**: Allow adding attachments from other mails during mail composition

The `addAttachment` action from the module `mailcompose` of the HTTP API is extended with an additional "origin" within the existing JSON form field of the multipart/form-data payload.

By specifying "mail" as "origin" the client is allowed to add a file attachment from an existing mail message to the composition space

Example:


```
POST /mailcompose?action=addAttachment
Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryyhuRsdTCa7hO6MJ4

------WebKitFormBoundaryyhuRsdTCa7hO6MJ4
Content-Disposition: form-data; name="contentDisposition"

ATTACHMENT
------WebKitFormBoundaryyhuRsdTCa7hO6MJ4
Content-Disposition: form-data; name="JSON"

{"origin":"mail", "id":"40", "folderId":"default0/INBOX", "attachmentId":"2"}
------WebKitFormBoundaryyhuRsdTCa7hO6MJ4--
```


### SCR-1166
**Summary**: New action "getRecurrence" in Module "chronos"

In order to supply clients with information whether a change exception is considered as rescheduled or overridden, the new action `getRecurrence` is introduced in the `chronos` module of the HTTP API. Prior chosing whether the whole series or just the actual recurrence should be changed by an update operation, this action can be performed to get all necessary information.

Further details are available at https://documentation.open-xchange.com/components/middleware/http/latest/index.html#!Chronos/getRecurrence . 

### SCR-1162
**Summary**: New Parameter "includeDelegates" for Action "needsAction" in Module "chronos"

The `needsAction` action in the module `chronos` of the HTTP API is extended by the new URL parameter `indludeDelegates`. 

If set to `true`, an enhanced response is returned which includes the events needing action of the session user itself, along with the data for other attendees the user has delegate scheduling permissions for. This includes both resource attendees where the user may act as booking delegate for, as well as other attendees where the user has a shared calendar with write access. If the parameter is set to `false`, only events for the current user are included in the response.

Whenever the parameter is set, an enhanced response in form of an array will be returned, where each element lists the attendee, together with the corresponding events needing action for that attendee. As before, for series events, overridden instances that are not considered as re-scheduled are hidden implicitly in the results. For backwards compatibility reasons, if the parameter `includeDelegates` is not set in the request, the previous, 'flat' response is returned to clients for the time being.

Further details are available at https://documentation.open-xchange.com/components/middleware/http/latest/index.html#!Chronos/getEventsNeedingAction .

### SCR-1154
**Summary**: Extended resource model with scheduling privileges

In order to store scheduling privileges for resources of users and groups, the `resource` object of the HTTP API is extended with an array holding scheduling privileges per user names `permissions`. Each array element holds a scheduling privilege object which has the following properties:
* `entity`, `integer`: Internal identifier of the user or group to which this permission applies.
* `group`, `boolean`: Set `true` if entity refers to a group, `false` if it refers to a user
* `privilege`, `string`: One of 
** `none` - No privileges to book the resource 
** `ask_to_book` - May submit a request to book the resource if it is available 
** `book_directly` - May book the resource directly if it is available 
** `delegate` - Act as delegate of the resource and manage bookings

Additionally, the read-only field `own_privilege` is introduced for resource objects, which indicates which effective privileges apply for the requesting user.

More details are available at https://documentation.open-xchange.com/components/middleware/http/latest/index.html#!Resources



API - Java
----------

### SCR-1170
**Summary**: Removed publication of TextXtractService and changed interface IMailMessageStorage

The use of Apache Tika within the Open-Xchange server was reduced to a possible minimum. 

As a result there is no need to keep the publication of '`TextXtractService`'. All implementations and the interface will be removed. There was no need to adapt the usage as it just was used in obsolete code.

The last java related change was the removal of the following method from `IMailMessageStorage`: 

`'public String[] getPrimaryContents(String folder, String[] mailIds) throws OXException;'`

### SCR-1167
**Summary**: New method "getRecurrenceInfo" within Chronos Stack

In order to drive the new action `getRecurrence` of the HTTP API, the Chronos stack is extended with a corresponding method with the following signature:

```
RecurrenceInfo getRecurrenceInfo(EventID eventID) throws OXException;
```

Implementations are available for the default internal, as well as the cross-context provider.

### SCR-1163
**Summary**: Adjusted Signature of "getEventsNeedingAction" Method throughout Chronos Stack

The method `#getEventsNeedingAction` is adjusted throughout the calendar stack, which includes the compositing layer, as well as the interfaces of the implementing services. A new `boolean` method parameter named `includeDelegates` is introduced, and the method response type is now a `Map` associating `Attendee` s  to their `EventsResult` s.



API - SOAP
----------

### SCR-1161
**Summary**: Extended SOAP provisioning interface for managed resources

The resource object for SOAP webservices `OXResourceServicePortType` and `OXResellerResourceServicePortType` have been extended for provisioning managed resources. The resource object has now additional `permissions` parameter:


```
<xsd:permissions>
   <xsd:entity>2</xsd:entity>
   <xsd:group>0</xsd:group>
   <xsd:privilege>book_directly</xsd:privilege>
</xsd:permissions>
```


-Also SOAP webservices `OXResourceServicePortType` and `OXResellerResourceServicePortType` got new operation `removePermissions`-
Permissions are removed by not mentioning them in resource object



Behavioral Changes
------------------

### SCR-1160
**Summary**: Removed direct link from notification mails

Within the internal notification mails for calendar events, there were direct links pointing to the appointment and (if those existed) for their attachments, for a quicker access.

Those direct links however are static and might be, shortly after the generation, out of date. For example, a user only had to move the appointment to a different calendar and the static link in the notification mail doesn't lead anywhere.

Further, the UI requests, renders and links the current event data on notification mails dynamically, efficiently solving the problem the direct links were created for much better. Thus, there is no need for the direct links anymore.



Configuration
-------------

### SCR-1181
**Summary**: New Properties to Control 'used-for-sync" Behavior of Calendar Folders

In order to control whether _public_ or _shared_ calendar folders are considered for synchronization via CalDAV by default or not, the following new lean configuration properties are introduced with the indicated defaults:

```
# Configures if shared calendar folders from the default account are considered for 
# synchronization by default or not. May still be set individually by the end user 
# unless also marked as protected.
com.openexchange.calendar.usedForSync.shared.default=true
```


```
# Configures if the default value that determines if shared calendar folders from the 
# default account are considered for synchronization may be overridden by the user or not.
com.openexchange.calendar.usedForSync.shared.protected=false
```


```
# Configures if public calendar folders from the default account are considered for 
# synchronization by default or not. May still be set individually by the end user 
# unless also marked as protected.
com.openexchange.calendar.usedForSync.public.default=true
```


```
# Configures if the default value that determines if public calendar folders from the 
# default account are considered for synchronization may be overridden by the user or not. 
com.openexchange.calendar.usedForSync.public.protected=false
```

All properties are reloadable and can be configured through the config cascade. With the implicit defaults, no existing semantics are changed, i.e. all shared/public folders of the default account continue to be used for sync by default, overridable by end users.

More details are available at [https://documentation.open-xchange.com/components/middleware/config/latest/#mode=search&term=com.openexchange.calendar.usedForSync] .

### SCR-1148
**Summary**: Allow using multiple services for password-change functionality

Since we now allow different `PasswordChangeServices` to be used in parallel, we must have some configuration that enables or disables certain services for certain context/users. Therefore, the following properties were introduced:



```
com.openexchange.passwordchange.script.enabled=false
com.openexchange.passwordchange.db.enabled=false
```


The database based password change is disabled by default, reflecting the status before the code changes. In older versions you had to actively install the packages.

### SCR-1142
**Summary**: Helm: Configuration of sensitive mandatory properties

With MW-1814 we removed the default values for some sensitive properties. 
 As some of those properties are still mandatory, we have updated the ox-common chart to generate secure random values, if no values have been specified (MW-1830). Those values are stored in a k8s secret called `<RELEASE>-common-env` and will be used by multiple charts/services (e.g. core-mw, core-imageconverter, ...).

The following properties are affected:

```
com.openexchange.cookie.hash.salt
com.openexchange.share.cryptKey
com.openexchange.sessiond.encryptionKey
```

From now on, administrators should set those properties in the global section of the deployment's `values.yaml` file.

Example:

```
global:
  core:
    cookieHashSalt: "KtLUTLKZrbXvCAOn"
    shareCryptKey: "lJZEFPzUYfapWbXL"
    sessiondEncryptionKey: "auw948cz,spdfgibcsp9e8ri+<#qawcghgifzign7c6gnrns9oysoeivn"
```

This will create the following k8s secret:

```
apiVersion: v1
kind: Secret
metadata:
  name: <RELEASE>-common-env
  namespace: <RELEASE>
  annotations:
    helm.sh/resource-policy: "keep"
  labels:
    helm.sh/chart: ox-common-1.0.22
data:
  COOKIE_HASH_SALT: cHlDN3p5RU1kZ0FmT3Znag==
  SHARE_CRYPT_KEY: Ujk5RFFVUGd4TWox
  SESSIOND_ENCRYPTION_KEY: eTY2cGk4azdXdFNpZ1BzTkJhVVIwWm9rN1lHM0M1YTZGVGZLenJkRWd5eVlwMGRuVjVtWjloSDFJUw==
```

Those environment variables will then be injected into the service containers and written into the relevant `.properties` files by the individual charts.



Packaging/Bundles
-----------------

### SCR-1182
**Summary**: Upgraded logback-extensions to 2.1.4

The logback-extensions library was upgraded to version 2.1.4 which includes some previously missing fields in the json logger.

### SCR-1171
**Summary**: Removed bundles com.openexchange.textxtraction and org.apache.tika

The use of Apache Tika within the Open-Xchange server was reduced to a possible minimum. As a result the bundles `org.apache.tika` and `com.openexchange.textxtraction` will be removed.

### SCR-1147
**Summary**: Allow multiple services for password-change functionality

With the new version 8.x of the Open Xchange App Suite we moved from package based installations to Docker/Kubernetes. For this, we need to be able to install all packages in parallel within the images we deliver. The different password change implementations however were conflicting. Therefore, we removed those packages and restructured the code.

Removed packages:

```
open-xchange-passwordchange-database
open-xchange-passwordchange-script
```


Removed bundles: 

```
com.openexchange.passwordchange.database
com.openexchange.passwordchange.script
```


Added bundles:


```
com.openexchange.passwordchange
com.openexchange.passwordchange.common
com.openexchange.passwordchange.impl
```


The added bundles are now delivered within the `open-xchange-core` package

The property files `change_pwd_script.properties` and `passwordchange.properties` were moved to the bundle `com.openexchange.passwordchange.impl` alongside the restructuring.


[8.10]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.9.0...8.10.0
[8.11]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.10.0...8.11.0
[8.12]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.11.0...8.12.0
[8.13]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.12.0...8.13.0
[8.14]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.13.0...8.14.0
[8.15]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.14.0...8.15.0
[8.16]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.15.0...8.16.0
[8.17]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.16.0...8.17.0
[8.18]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.17.0...8.18.0
[8.19]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.18.0...8.19.0
[8.20]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.19.0...8.20.0
[8.21]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.20.0...8.21.0
[8.22]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.21.0...8.22.0
[8.23]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.22.0...8.23.0
[8.24]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.23.0...8.24.0
[8.25]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.24.0...8.25.0
[8.26]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.25.0...8.26.0
[8.27]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.26.0...8.27.0
[8.28]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.27.0...8.28.0
[8.29]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.28.0...8.29.0
[8.30]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.29.0...8.30.0
[8.31]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.30.0...8.31.0
[8.32]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.31.0...8.32.0
[8.33]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.32.0...8.33.0
[8.34]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.33.0...8.34.0
[8.35]: https://gitlab.open-xchange.com/appsuite/platform/core/-/compare/8.34.0...8.35.0
