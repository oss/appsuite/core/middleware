/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.logging.rest;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.LogLevel;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.logging.LogConfigurationService;
import com.openexchange.logging.LogConfigurationService.LoggerCollection;
import com.openexchange.logging.LogExceptionCodes;
import com.openexchange.logging.rest.logconf.LogRestExceptionCodes;
import com.openexchange.rest.services.RESTUtils;
import com.openexchange.server.ServiceLookup;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

/**
 * {@link AbstractLogConfAPI}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public abstract class AbstractLogConfAPI {

    static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(AbstractLogConfAPI.class);

    private static final List<Level> VALID_LEVELS = Arrays.asList(Level.ALL, Level.TRACE, Level.DEBUG, Level.INFO, Level.WARN, Level.ERROR, Level.OFF);

    protected static String MSG_INVALID_VALUE = "Invalid value.";

    protected final ServiceLookup services;

    protected AbstractLogConfAPI(ServiceLookup services) {
        super();
        this.services = services;
    }

    /**
     * Gets the {@link LogConfigurationService}
     *
     * @return The {@link LogConfigurationService} to use
     * @throws OXException if the service is not available
     */
    protected final LogConfigurationService getLogService() throws OXException {
        return this.services.getServiceSafe(LogConfigurationService.class);
    }

    /**
     * Gets the {@link Level} for a given string
     *
     * @param loglevel The level to get
     * @return The {@link Level} for the given string
     * @throws IllegalArgumentException if the given loglevel is invalid
     */
    private Level getLogLevel(String loglevel) throws IllegalArgumentException {
        Optional<Level> level = VALID_LEVELS.stream().filter(lvl -> lvl.levelStr.equals(loglevel.toUpperCase())).findFirst();
        return level.orElseThrow(() -> new IllegalArgumentException("Invalid log level: \"" + loglevel + "\""));
    }

    /**
     * Asserts that the given map of loggers and levels, contains an entry related to a specific logger
     *
     * @param map The map to check
     * @param name The name to check
     * @throws OXException if the logger with the given name was not part of the given map
     */
    protected void assertLoggerExists(Map<String, Level> map, String name) throws OXException {
        if (map.containsKey(name) == false) {
            throw LogRestExceptionCodes.LOGGER_NOT_FOUND.create();
        }
    }

    /**
     * Parses the given string into a {@link Level}
     *
     * @param loglevel The string to parse
     * @return The parsed {@link LogLevel} if the given string is a valid log level, or <code>null</code> if the given string represents JSON </code>null</code>
     * @throws IllegalArgumentException if the given string does not represent a valid log level
     */
    protected Level parseLogLevel(String loglevel) throws IllegalArgumentException {
        //Parse given log level
        //The log level can be "null" or empty in order to set it to "null"
        if (JSONObject.NULL.toString().equals(loglevel) == false && Strings.isNotEmpty(loglevel)) {

            if (loglevel.startsWith("\"") == false || loglevel.endsWith("\"") == false) {
                //Not a valid JSON String!
                throw new IllegalArgumentException("Invalid log level " + loglevel);
            }
            return getLogLevel(loglevel.substring(1, loglevel.length() - 1).toUpperCase());
        }
        return null;
    }

    /**
     * Parsed an JSON array of loggers to a Map as used by the underlying service
     *
     * @param loggers The array to parse
     * @return The map parsed
     * @throws JSONException if an error occurred during parsing the {@link JSONArray}
     * @throws IllegalArgumentException if one of the loggers' log level is not valid
     */
    protected Map<String, Level> parseLoggers(JSONArray loggers) throws JSONException, IllegalArgumentException {
        HashMap<String, Level> map = HashMap.newHashMap(loggers.length());
        for (int i = 0; i < loggers.length(); i++) {
            JSONObject logger = loggers.getJSONObject(i);
            String loggerName = logger.getString("name");
            String level = logger.getString("level");
            map.put(loggerName, getLogLevel(level));
        }
        return map;
    }

    /**
     * Converts the given list of loggers into a JSON Array
     *
     * @param loggers The list of loggers to convert
     * @return The list as JSON array
     * @throws JSONException
     */
    protected JSONArray toJSON(Collection<Logger> loggers) throws JSONException {
        JSONArray array = new JSONArray(loggers.size());
        for (Logger logger : loggers) {
            array.put(toJSON(logger));
        }
        return array;
    }

    /**
     * Converts the given collections into a JSON Array
     *
     * @param loggerCollections The collections to convert
     * @param idDescriptor Defines how to name the ID in the created JSON
     * @return The JSON array
     * @throws JSONException
     */
    protected JSONArray toJSON(Collection<LoggerCollection> loggerCollections, String idDescriptor) throws JSONException {
        JSONArray array = new JSONArray();
        for (LoggerCollection collection : loggerCollections) {
            array.put(toJSON(collection, idDescriptor));
        }
        return array;
    }

    /**
     * Converts the given collection into a JSON Object
     *
     * @param loggerCollection The collection to convert
     * @param idDescriptor Defines how to name the ID in the created JSON
     * @return The JSON Object
     * @throws JSONException
     */
    protected JSONObject toJSON(LoggerCollection loggerCollection, String idDescriptor) throws JSONException {
        JSONObject context = new JSONObject();
        context.put(idDescriptor, loggerCollection.id());
        context.put("loggers", toJSON(loggerCollection.loggers()));
        return context;
    }

    /**
     * Converts the given map into an {@link JSONArray}
     *
     * @param logger The logger to convert
     * @return The logger as JSON
     * @throws JSONException
     */
    protected JSONArray toJSON(Map<String, Level> loggers) throws JSONException {
        JSONArray json = new JSONArray(loggers.size());
        int pos = 0;
        for (Map.Entry<String, Level> entry : loggers.entrySet()) {
            String loggerName = entry.getKey();
            Level level = entry.getValue();
            json.add(pos++, toJSON(loggerName, level != null ? level.levelStr : null));
        }
        return json;
    }

    /**
     * Converts the log level string of the given {@link Level} to a JSON string
     *
     * @param level The level
     * @return The level as JSON String
     */
    protected String toJSONString(Level level) {
        if (level != null) {
            return JSONObject.quote(level.levelStr);
        }
        return JSONObject.NULL.toString();
    }

    /**
     * Converts the given {@link Logger} into JSON
     *
     * @param logger The logger to convert
     * @return The logger as JSON
     * @throws JSONException
     */
    protected JSONObject toJSON(Logger logger) throws JSONException {
        return toJSON(logger.getName(), logger.getLevel() != null ? logger.getLevel().levelStr : null);
    }

    /**
     * Converts the given logger information into a {@link JSONObject}
     *
     * @param loggerName The name of the logger
     * @param loglevel The log level
     * @return The {@link JSONObject} representing the logger with the given information
     * @throws JSONException
     */
    protected JSONObject toJSON(String loggerName, String loglevel) throws JSONException {
        JSONObject json = new JSONObject(2);
        json.put("name", loggerName);
        json.put("level", loglevel == null ? JSONObject.NULL : loglevel);
        return json;
    }

    /**
     * {@link LoggingAction} represents a generic action in the logging API
     */
    @FunctionalInterface
    protected interface LoggingAction {
            Response doAction() throws Exception;
    }

    /**
     * Performs a generic {@link LoggingAction} with basic error handling
     *
     * @param action The action to perform
     * @return The response of the action
     */
    protected Response doAction(LoggingAction action) {
        try {
            return action.doAction();
        } catch (OXException e) {
            LOG.error("Error while performing logging action: ", e);
            JSONObject jsonError = RESTUtils.generateError(e);
            if (e.getExceptionCode() instanceof LogExceptionCodes && e.similarTo(LogExceptionCodes.CONTEXT_NOT_FOUND)) {
                return RESTUtils.respondNotFound(jsonError);
            } else if (e.getExceptionCode() instanceof LogExceptionCodes && e.similarTo(LogExceptionCodes.USER_NOT_FOUND)) {
                return RESTUtils.respondNotFound(jsonError);
            } else if (e.getExceptionCode() instanceof LogExceptionCodes && e.similarTo(LogExceptionCodes.SESSION_NOT_FOUND)) {
                return RESTUtils.respondNotFound(jsonError);
            } else if (e.getExceptionCode() instanceof LogRestExceptionCodes && e.similarTo(LogRestExceptionCodes.LOGGER_NOT_FOUND)) {
                return RESTUtils.respondNotFound(jsonError);
            }
            return RESTUtils.respondServerError(jsonError);
        } catch (Exception e) {
            LOG.error("Error while performing logging action: ", e);
            return RESTUtils.respondServerError(e.getMessage());
        }
    }
}
