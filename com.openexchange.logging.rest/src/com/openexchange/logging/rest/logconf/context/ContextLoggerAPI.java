/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.logging.rest.logconf.context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import com.openexchange.logging.rest.AbstractLogConfAPI;
import com.openexchange.rest.services.RESTUtils;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.server.ServiceLookup;
import ch.qos.logback.classic.Level;

/**
 * {@link ContextLoggerAPI}- provides RESTful API routes for configuring context logging filters
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
@Path("admin/v1/logconf/context")
@RoleAllowed(Role.MASTER_ADMIN_AUTHENTICATED)
public class ContextLoggerAPI extends AbstractLogConfAPI {

    static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextLoggerAPI.class);

    /**
     * Initializes a new {@link ContextLoggerAPI}.
     *
     * @param services The {@link ServiceLookup}
     */
    public ContextLoggerAPI(ServiceLookup services) {
        super(services);
    }

    /**
     * Gets all context loggers / filters for a given context
     *
     * @param contextId The contextId to set the loggers / filters for
     * @return The {@link Response} containing a map of known loggers for the given context
     */
    @GET
    @Path("/{contextId}/loggers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLoggersForContext(@PathParam("contextId") int contextId) {
        return doAction(() -> {
            Map<String, Level> lvls = getLogService().getContextLogLevels(contextId);
            return Response.ok().entity(toJSON(lvls)).build();
        });

    }

    /**
     * Gets a specific context logger / filters
     *
     * @param contextId The contextId to get the loggers / filters for
     * @param logger The name of the logger to get
     * @return The {@link Response} containing the specified log level for the context
     */
    @GET
    @Path("/{contextId}/logger/{logger}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLoggerForContext(@PathParam("contextId") int contextId, @PathParam("logger") String logger) {
        return doAction(() -> {
            //Looking up the requests logger/filter
            Map<String, Level> lvls = getLogService().getContextLogLevels(contextId);
            assertLoggerExists(lvls, logger);

            //Returning the string -log level as root element of the JSON
            Level level = lvls.get(logger);
            return Response.ok().entity(toJSONString(level)).build();
        });
    }

    /**
     * Replaces all existing loggers for a context, if any, with the given loggers
     *
     * @param contextId The contextId to set the loggers / filters for
     * @param loggers The JSON body array containing the loggers
     * @return The {@link Response} containing an array of known loggers for the given context
     */
    @PUT
    @Path("/{contextId}/loggers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response setLoggersForContext(@PathParam("contextId") int contextId, JSONArray loggers) {
        return doAction(() -> {
            //PUT implies a replace, so we need to remove the context based filters first
            var logService = getLogService();
            Map<String, Level> contextLogLevels = logService.getContextLogLevels(contextId);
            logService.removeContextFilter(contextId, new ArrayList<>(contextLogLevels.keySet()));

            //Set the new loggers/filters
            Map<String, Level> parsedLoggers = null;
            try {
                parsedLoggers = parseLoggers(loggers);
            } catch (IllegalArgumentException e) {
                LOG.error("Error while setting context loggers: ", e);
                return RESTUtils.respondBadRequest(e.getMessage());
            }
            logService.createContextFilter(contextId, parsedLoggers);

            //Return what was set
            Map<String, Level> lvls = logService.getContextLogLevels(contextId);
            return Response.ok().entity(toJSON(lvls)).build();
        });
    }

    /**
     * Sets the log level of a given context logger
     *
     * @param contextId The context ID to set the loggers / filters for
     * @param logger The name of the logger
     * @param jsonLoglevel The log level to set
     * @return The {@link Response} containing the new log level set
     */
    @PUT
    @Path("/{contextId}/logger/{logger}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setLoggerForContext(@PathParam("contextId") int contextId, @PathParam("logger") String logger, String jsonLoglevel) {
        return doAction(() -> {
            //Parse given log level
            Level logLevel = null;
            try {
                logLevel = parseLogLevel(jsonLoglevel);
            } catch (IllegalArgumentException e) {
                return RESTUtils.respondBadRequest(e.getMessage());
            }

            //Changing the log level
            var logService = getLogService();
            var modifications = new HashMap<String, Level>();
            modifications.put(logger, logLevel);
            logService.createContextFilter(contextId, modifications);

            //Returning what was set
            Map<String, Level> lvls = getLogService().getContextLogLevels(contextId);
            assertLoggerExists(lvls, logger);

            Level level = lvls.get(logger);
            return Response.ok().entity(toJSONString(level)).build();
        });
    }

    /**
     * Deletes all existing loggers for a context
     *
     * @param contextId The context id
     * @return An empty {@link Response}
     */
    @DELETE
    @Path("/{contextId}/loggers")
    public Response deleteLoggersForContext(@PathParam("contextId") int contextId) {
        return doAction(() -> {
            //Delete
            var logService = getLogService();
            Map<String, Level> contextLoggers = logService.getContextLogLevels(contextId);
            logService.removeContextFilter(contextId, new ArrayList<>(contextLoggers.keySet()));

            return Response.ok().build();
        });
    }

    /**
     * Deletes a specific logger/filter for a context
     *
     * @param contextId The context id
     * @param logger The logger to delete
     * @return An empty {@link Response}
     */
    @DELETE
    @Path("/{contextId}/logger/{logger}")
    public Response deleteLoggerForContext(@PathParam("contextId") int contextId, @PathParam("logger") String logger) {
        return doAction(() -> {
            //Looking up the requests logger/filter
            Map<String, Level> lvls = getLogService().getContextLogLevels(contextId);
            assertLoggerExists(lvls, logger);

            //Delete
            var logService = getLogService();
            logService.removeContextFilter(contextId, Collections.singletonList(logger));
            return Response.ok().build();
        });
    }
}
