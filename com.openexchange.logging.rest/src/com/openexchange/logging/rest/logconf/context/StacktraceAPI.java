/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.logging.rest.logconf.context;

import static com.openexchange.java.Autoboxing.b;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;
import com.openexchange.logging.rest.AbstractLogConfAPI;
import com.openexchange.logging.rest.logconf.suppressed.exceptioncategories.ExceptionCategoriesAPI;
import com.openexchange.rest.services.RESTUtils;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.server.ServiceLookup;

/**
 * {@link StacktraceAPI} - provides RESTfull API routes for configuring stacktrace behavior
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
@Path("admin/v1/logconf/context")
@RoleAllowed(Role.MASTER_ADMIN_AUTHENTICATED)
public class StacktraceAPI extends AbstractLogConfAPI {

    static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(StacktraceAPI.class);

    /**
     * Initializes a new {@link ExceptionCategoriesAPI}.
     *
     * @param services The {@link ServiceLookup}
     */
    public StacktraceAPI(ServiceLookup services) {
        super(services);
    }

    /**
     * Gets the "includeStackTraceOnError" setting for a specific user
     *
     * @param contextId The context ID
     * @param userId The user ID
     * @return JSON "true", if the includeStackTraceOnError setting is enabled for the given user, or JSON "false" otherwise
     */
    @GET
    @Path("/{contextId}/user/{userId}/stacktrace/include-on-error")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIncludeStacktraceOnError(@PathParam("contextId") int contextId, @PathParam("userId") int userId) {
        return doAction(() -> {
            boolean includeStackTraceOnError = getLogService().getIncludeStackTraceForUser(contextId, userId);
            return Response.ok().entity(Boolean.toString(includeStackTraceOnError)).type(MediaType.APPLICATION_JSON).build();
        });
    }

    /**
     * Sets the "includeStackTraceOnError" option for a given user
     *
     * @param contextId The context ID
     * @param userId The user ID
     * @param json JSON "true" in order to set enable the "includeStackTraceOnError" option for the user, or JSON "false" to disable it
     * @return JSON "true", if the includeStackTraceOnError setting was enabled for the given user, or JSON "false" if it was disabled
     */
    @PUT
    @Path("/{contextId}/user/{userId}/stacktrace/include-on-error")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setIncludeStacktraceOnError(@PathParam("contextId") int contextId, @PathParam("userId") int userId, String json) {
        return doAction(() -> {
            Boolean includeStacktraceOnError = JSONObject.booleanFor(json);
            if (includeStacktraceOnError == null) {
                return RESTUtils.respondBadRequest(MSG_INVALID_VALUE);
            }

            getLogService().includeStackTraceForUser(contextId, userId, b(includeStacktraceOnError));
            return Response.ok().type(MediaType.APPLICATION_JSON).build();
        });
    }
}
