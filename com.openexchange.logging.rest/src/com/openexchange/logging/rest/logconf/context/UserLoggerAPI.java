/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.logging.rest.logconf.context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import com.openexchange.logging.LogConfigurationService.LoggerCollection;
import com.openexchange.logging.rest.AbstractLogConfAPI;
import com.openexchange.rest.services.RESTUtils;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.server.ServiceLookup;
import ch.qos.logback.classic.Level;

/**
 * {@link UserLoggerAPI} - provides RESTful API routes for configuring user logging filters
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
@Path("admin/v1/logconf/context")
@RoleAllowed(Role.MASTER_ADMIN_AUTHENTICATED)
public class UserLoggerAPI extends AbstractLogConfAPI {

    static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(UserLoggerAPI.class);

    /**
     * Initializes a new {@link UserLoggerAPI}.
     *
     * @param services The {@link ServiceLookup}
     */
    public UserLoggerAPI(ServiceLookup services) {
        super(services);
    }

    @GET
    @Path("/{contextId}/users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUsersLoggers(@PathParam("contextId") int contextId) {
        return doAction(() -> {
            List<LoggerCollection> allUsersLoggers = getLogService().getUserLogLevels(contextId);
            return Response.ok().entity(toJSON(allUsersLoggers, "userId")).build();
        });
    }

    /**
     * Gets all user loggers / filters for a given user within a context
     *
     * @param contextId The contextId related to the user
     * @param userId The user id
     * @return The {@link Response} containing a map of known loggers for the given user
     */
    @GET
    @Path("/{contextId}/user/{userId}/loggers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLoggersForUser(@PathParam("contextId") int contextId, @PathParam("userId") int userId) {
        return doAction(() -> {
            Map<String, Level> lvls = getLogService().getUserLogLevels(contextId, userId);
            return Response.ok().entity(toJSON(lvls)).build();
        });
    }

    /**
     * Gets a specific user logger / filters
     *
     * @param contextId The contextId to get the loggers / filters for
     * @param userId The userId to get the loggers / filters for
     * @param logger The name of the logger to get the log level for
     * @return The {@link Response} containing the specified log level for the user
     */
    @GET
    @Path("/{contextId}/user/{userId}/logger/{logger}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLoggerForUser(@PathParam("contextId") int contextId, @PathParam("userId") int userId, @PathParam("logger") String logger) {
        return doAction(() -> {
            //Looking up the requests logger/filter
            Map<String, Level> lvls = getLogService().getUserLogLevels(contextId, userId);
            assertLoggerExists(lvls, logger);

            //Returning the string -log level as root element of the JSON
            Level level = lvls.get(logger);
            return Response.ok().entity(toJSONString(level)).build();
        });
    }

    /**
     * Sets the log level of a given user logger
     *
     * @param contextId The context ID to set the loggers / filters for
     * @param userId The user ID to set the loggers / filters for
     * @param logger The name of the logger to set the log level for
     * @param jsonLogLevel The loglevel to set
     * @return The {@link Response} The new loglevel set
     */
    @PUT
    @Path("/{contextId}/user/{userId}/logger/{logger}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setLoggerForUser(@PathParam("contextId") int contextId, @PathParam("userId") int userId, @PathParam("logger") String logger, String jsonLoglevel) {
        return doAction(() -> {
            //Parse given log level
            Level logLevel = null;
            try {
                logLevel = parseLogLevel(jsonLoglevel);
            } catch (IllegalArgumentException e) {
                return RESTUtils.respondBadRequest(e.getMessage());
            }

            //Changing the log level
            var logService = getLogService();
            var modifications = new HashMap<String, Level>();
            modifications.put(logger, logLevel);
            logService.createUserFilter(userId, contextId, modifications);

            //Returning what was set
            Map<String, Level> lvls = getLogService().getUserLogLevels(contextId, userId);
            assertLoggerExists(lvls, logger);

            Level level = lvls.get(logger);
            return Response.ok().entity(toJSONString(level)).build();
        });
    }

    /**
     * Replaces all existing loggers for a user, if any, with the given loggers
     *
     * @param contextId The contextId to set the loggers / filters for
     * @param userId The userId to set the loggers / filters for
     * @param loggers The JSON body array containing the loggers
     * @return The {@link Response} containing an array of known loggers for the given context
     */
    @PUT
    @Path("/{contextId}/user/{userId}/loggers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response setLoggersForUser(@PathParam("contextId") int contextId, @PathParam("userId") int userId, JSONArray loggers) {
        return doAction(() -> {
            //PUT implies a replace, so we need to remove the user based filters first
            var logService = getLogService();
            Map<String, Level> userLogLevels = logService.getUserLogLevels(contextId, userId);
            logService.removeUserFilter(contextId, userId, new ArrayList<>(userLogLevels.keySet()));

            //Set the new loggers/filters
            Map<String, Level> parsedLoggers = null;
            try {
                parsedLoggers = parseLoggers(loggers);
            } catch (IllegalArgumentException e) {
                LOG.error("Error while setting user loggers: ", e);
                return RESTUtils.respondBadRequest(e.getMessage());
            }
            logService.createUserFilter(userId, contextId, parsedLoggers);

            //Return what was set
            Map<String, Level> lvls = logService.getUserLogLevels(contextId, userId);
            return Response.ok().entity(toJSON(lvls)).build();
        });
    }

    /**
     * Deletes all existing loggers for a user
     *
     * @param contextId The context id
     * @param userId The userId
     * @return An empty {@link Response}
     */
    @DELETE
    @Path("/{contextId}/user/{userId}/loggers")
    public Response deleteLoggersForUser(@PathParam("contextId") int contextId, @PathParam("userId") int userId) {
        return doAction(() -> {
            //Delete
            var logService = getLogService();
            Map<String, Level> userLoggers = logService.getUserLogLevels(contextId, userId);
            logService.removeUserFilter(contextId, userId, new ArrayList<>(userLoggers.keySet()));
            return Response.ok().build();
        });
    }

    /**
     * Deletes a specific logger/filter for a user
     *
     * @param contextId The context id
     * @param userId The user id
     * @param logger The logger to delete
     * @return An empty {@link Response}
     */
    @DELETE
    @Path("/{contextId}/user/{userId}/logger/{logger}")
    public Response deleteLoggerForUser(@PathParam("contextId") int contextId, @PathParam("userId") int userId, @PathParam("logger") String logger) {
        return doAction(() -> {
            //Looking up the requests logger/filter
            Map<String, Level> lvls = getLogService().getUserLogLevels(contextId, userId);
            assertLoggerExists(lvls, logger);

            //Delete
            var logService = getLogService();
            logService.removeUserFilter(contextId, userId, Collections.singletonList(logger));
            return Response.ok().build();
        });
    }
}
