/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.logging.rest.logconf.system;

import java.util.HashMap;
import java.util.Optional;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.openexchange.exception.OXException;
import com.openexchange.logging.rest.AbstractLogConfAPI;
import com.openexchange.logging.rest.logconf.LogRestExceptionCodes;
import com.openexchange.rest.services.RESTUtils;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.server.ServiceLookup;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

/**
 * {@link SystemLoggerAPI} - provides RESTful API routes for configuring system loggers
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
@Path("admin/v1/logconf/system")
@RoleAllowed(Role.MASTER_ADMIN_AUTHENTICATED)
public class SystemLoggerAPI extends AbstractLogConfAPI {

    static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(SystemLoggerAPI.class);

    /**
     * Initializes a new {@link SystemLoggerAPI}.
     *
     * @param services The {@link ServiceLookup} to use
     */
    public SystemLoggerAPI(ServiceLookup services) {
        super(services);
    }

    /**
     * Gets the logger with the given name
     *
     * @param loggerName The name of the logger to get
     * @return The logger
     * @throws OXException if the logger was not found
     */
    private Logger getLoggerInternal(String loggerName) throws OXException {
        Set<Logger> loggers = getLogService().getLoggers();
        Optional<Logger> logger = loggers.stream().filter(l -> loggerName.equals(l.getName())).findFirst();
        return logger.orElseThrow(LogRestExceptionCodes.LOGGER_NOT_FOUND::create);
    }

    /**
     * Gets a list off all known system loggers.
     *
     * @return The {@link Response} containing an array of loggers
     */
    @GET
    @Path("/loggers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLoggers() {
        return doAction(() -> {
            Set<Logger> systemLoggers = getLogService().getLoggers();
            return Response.ok().entity(toJSON(systemLoggers)).build();
        });
    }

    /**
     * Gets the log level of a specific system logger
     *
     * @param loggerName The name of the logger to get the level for
     * @return The {@link Response} containing the log level of the given logger
     */
    @GET
    @Path("/logger/{logger}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLogger(@PathParam("logger") String loggerName) {
        return doAction(() -> {
            //Getting a single logger from the underlying LogContext would implicitly create a new logger
            //if it does not exist. Therefore getting all loggers and filter for the requested one.
            Logger logger = getLoggerInternal(loggerName);
            //Returning the string -log level as root element of the JSON
            return Response.ok().entity(toJSONString(logger.getLevel())).build();
        });
    }

    /**
     * Sets the log level for a specific system logger
     *
     * @param loggerName The name of the logger to set the log level for
     * @param jsonLoglevel The log level to set
     * @return The {@link Response} containing the new log level set
     */
    @PUT
    @Path("/logger/{logger}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setLogger(@PathParam("logger") String loggerName, String jsonLoglevel) {
        return doAction(() -> {
            //Parse given log level
            Level logLevel = null;
            try {
                logLevel = parseLogLevel(jsonLoglevel);
            } catch (IllegalArgumentException e) {
                return RESTUtils.respondBadRequest(e.getMessage());
            }

            //Check if the logger exists by getting it
            getLoggerInternal(loggerName);

            //Changing the log level
            var modifications = new HashMap<String, Level>();
            modifications.put(loggerName, logLevel);
            getLogService().modifyLogLevels(modifications);

            //Return what was set
            Logger changedLogger = getLoggerInternal(loggerName);
            //Returning the string -log level as root element of the JSON
            return Response.ok().entity(toJSONString(changedLogger.getLevel())).build();
        });
    }
}
