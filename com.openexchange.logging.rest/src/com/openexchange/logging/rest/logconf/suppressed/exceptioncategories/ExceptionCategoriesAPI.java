/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.logging.rest.logconf.suppressed.exceptioncategories;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import com.openexchange.logging.rest.AbstractLogConfAPI;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.server.ServiceLookup;

/**
 * {@link ExceptionCategoriesAPI} - provides RESTfull API routes for configuring suppressed exception categories
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
@Path("admin/v1/logconf/suppressed/exception-categories")
@RoleAllowed(Role.MASTER_ADMIN_AUTHENTICATED)
public class ExceptionCategoriesAPI extends AbstractLogConfAPI {

    static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ExceptionCategoriesAPI.class);

    /**
     * Initializes a new {@link ExceptionCategoriesAPI}.
     *
     * @param services The {@link ServiceLookup}
     */
    public ExceptionCategoriesAPI(ServiceLookup services) {
        super(services);
    }

    /**
     * Internal method to convert the given set of exception categories to a JSONArray
     *
     * @param categories The set of categories to convert
     * @return The categories as JSONArray
     */
    protected JSONArray toJSON(Set<String> categories) {
        return new JSONArray(categories);
    }

    /**
     * Parses the given JSONArray of exception categories into a set
     *
     * @param categories The array to parse
     * @return The set of categories parsed
     */
    protected Set<String> parseCategories(JSONArray categories) {
        return new HashSet<>(categories.asList().stream().map(String.class::cast).toList());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getExceptionCategories() {
        return doAction(() -> {
            Set<String> exceptionCategories = getLogService().listExceptionCategories();
            return Response.ok().entity(toJSON(exceptionCategories)).build();
        });
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response setExceptionCategories(JSONArray categories) {
        return doAction(() -> {
            //Set/overwrite
            var logService = getLogService();
            Set<String> parsedCategories = parseCategories(categories);
            logService.overrideExceptionCategories(parsedCategories);

            //Return what was set
            Set<String> exceptionCategories = logService.listExceptionCategories();
            return Response.ok().entity(toJSON(exceptionCategories)).build();
        });
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteExceptionCategories() {
        return doAction(() -> {
            //Override/delete
            var logService = getLogService();
            logService.overrideExceptionCategories(Collections.emptySet());

            //Return what was set/deleted
            Set<String> exceptionCategories = logService.listExceptionCategories();
            return Response.ok().entity(toJSON(exceptionCategories)).build();
        });
    }
}
