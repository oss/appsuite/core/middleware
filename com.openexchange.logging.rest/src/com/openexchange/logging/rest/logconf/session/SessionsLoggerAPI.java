/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.logging.rest.logconf.session;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.server.ServiceLookup;
import com.openexchange.logging.LogConfigurationService.LoggerCollection;
import com.openexchange.logging.rest.AbstractLogConfAPI;
import com.openexchange.rest.services.annotation.Role;

/**
 * {@link SessionsLoggerAPI} - provides RESTful API routes for getting all session logging filters
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
@Path("admin/v1/logconf/sessions")
@RoleAllowed(Role.MASTER_ADMIN_AUTHENTICATED)
public class SessionsLoggerAPI extends AbstractLogConfAPI {

    /**
     * Initializes a new {@link ContextsLoggerAPI}.
     *
     * @param services The {@link ServiceLookup}
     */
    public SessionsLoggerAPI(ServiceLookup services) {
        super(services);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllContextLoggers() {
        return doAction(() -> {
            List<LoggerCollection> allContextLoggers = getLogService().getSessionsLogLevels();
            return Response.ok().entity(toJSON(allContextLoggers, "sessionId")).build();
        });
    }
}
