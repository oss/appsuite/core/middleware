/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.logging.rest.logconf.session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.logging.rest.AbstractLogConfAPI;
import com.openexchange.rest.services.RESTUtils;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.server.ServiceLookup;
import ch.qos.logback.classic.Level;

/**
 * {@link SessionLoggerAPI}- provides RESTful API routes for configuring session logging filters
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
@Path("admin/v1/logconf/session")
@RoleAllowed(Role.MASTER_ADMIN_AUTHENTICATED)
public class SessionLoggerAPI extends AbstractLogConfAPI {

    static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(SessionLoggerAPI.class);

    private static final String MSG_INVALID_SESSION_ID = "Invalid session id.";

    /**
     * Initializes a new {@link SessionLoggerAPI}.
     *
     * @param services The {@link ServiceLookup}
     */
    public SessionLoggerAPI(ServiceLookup services) {
        super(services);
    }

    /**
     * Gets all session loggers / filters for a given session
     *
     * @param sessionId The session ID to set the loggers / filters for
     * @return The {@link Response} containing a map of known loggers for the given sessions
     */
    @GET
    @Path("/{sessionid}/loggers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLoggersForSession(@PathParam("sessionid") String sessionId) {
        return doAction(() -> {
            //Check if a session was given at all
            if (Strings.isEmpty(sessionId)) {
                return RESTUtils.respondBadRequest(MSG_INVALID_SESSION_ID);
            }

            Map<String, Level> lvls = getLogService().getSessionLogLevels(sessionId);
            return Response.ok().entity(toJSON(lvls)).build();
        });
    }

    /**
     * Gets the log level for a specific session based logger
     *
     * @param sessionId The session ID to get the log level for
     * @param logger The name of the logger to get the log level for
     * @return The {@link Response} containing the specified log level for the session
     */
    @GET
    @Path("/{sessionid}/logger/{logger}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLoggerForSession(@PathParam("sessionid") String sessionId, @PathParam("logger") String logger) {
        return doAction(() -> {
            //Check if a session was given at all
            if (Strings.isEmpty(sessionId)) {
                return RESTUtils.respondBadRequest(MSG_INVALID_SESSION_ID);
            }

            //Looking up the requests logger/filter
            Map<String, Level> lvls = getLogService().getSessionLogLevels(sessionId);
            assertLoggerExists(lvls, logger);

            //Returning the string -log level as root element of the JSON
            Level level = lvls.get(logger);
            return Response.ok().entity(toJSONString(level)).build();
        });
    }

    /**
     * Replaces all existing loggers for a session, if any, with the given loggers
     *
     * @param sessionId The session ID to set the loggers / filters for
     * @param loggers The JSON body array containing the loggers
     * @return The {@link Response} containing an array of known loggers for the given session
     */
    @PUT
    @Path("/{sessionid}/loggers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response setLoggersForSession(@PathParam("sessionid") String sessionId, JSONArray loggers) {
        return doAction(() -> {
            //Check if a session was given at all
            if (Strings.isEmpty(sessionId)) {
                return RESTUtils.respondBadRequest(MSG_INVALID_SESSION_ID);
            }

            //PUT implies a replace, so we need to remove the session based filters first
            var logService = getLogService();
            Map<String, Level> sessionLogLevels = logService.getSessionLogLevels(sessionId);
            logService.removeSessionFilter(sessionId, new ArrayList<String>(sessionLogLevels.keySet()));

            //Set the new loggers/filters
            Map<String, Level> parsedLoggers = null;
            try {
                parsedLoggers = parseLoggers(loggers);
            } catch (IllegalArgumentException e) {
                LOG.error("Error while setting session loggers: ", e);
                return RESTUtils.respondBadRequest(e.getMessage());
            }
            logService.createSessionFilter(sessionId, parsedLoggers);

            //Return what was set
            Map<String, Level> lvls = getLogService().getSessionLogLevels(sessionId);
            return Response.ok().entity(toJSON(lvls)).build();
        });
    }

    /**
     * Sets the log level of a given session logger
     *
     * @param sessionId The session ID to set the loggers / filters for
     * @param logger The name of the logger
     * @param jsonLogLevel The log level to set
     * @return The {@link Response} containing the new log level set
     */
    @PUT
    @Path("/{sessionid}/logger/{logger}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setLoggerForSession(@PathParam("sessionid") String sessionId, @PathParam("logger") String logger, String jsonLoglevel) {
        return doAction(() -> {
            //Parse given log level
            Level logLevel = null;
            try {
                logLevel = parseLogLevel(jsonLoglevel);
            } catch (IllegalArgumentException e) {
                return RESTUtils.respondBadRequest(e.getMessage());
            }

            //Check if a session was given at all
            if (Strings.isEmpty(sessionId)) {
                return RESTUtils.respondBadRequest(MSG_INVALID_SESSION_ID);
            }

            //Looking up the requests logger/filter
            //Changing the log level
            var logService = getLogService();
            var modifications = new HashMap<String, Level>();
            modifications.put(logger, logLevel);
            logService.createSessionFilter(sessionId, modifications);

            //Returning what was set
            Map<String, Level> lvls = getLogService().getSessionLogLevels(sessionId);
            assertLoggerExists(lvls, logger);
            Level level = lvls.get(logger);
            return Response.ok().entity(toJSONString(level)).build();
        });
    }

    /**
     * Deletes all existing loggers for a session
     *
     * @param sessionId The session id
     * @return An empty {@link Response}
     */
    @DELETE
    @Path("/{sessionid}/loggers")
    public Response deleteLoggersForSession(@PathParam("sessionid") String sessionId) {
        return doAction(() -> {
            //Check if a session was given at all
            if (Strings.isEmpty(sessionId)) {
                return RESTUtils.respondBadRequest(MSG_INVALID_SESSION_ID);
            }

            //Delete
            var logService = getLogService();
            Map<String, Level> sessionLogLevels = logService.getSessionLogLevels(sessionId);
            logService.removeSessionFilter(sessionId, new ArrayList<String>(sessionLogLevels.keySet()));

            return Response.ok().build();
        });
    }

    /**
     * Deletes a specific logger/filter for a session
     *
     * @param sessionId The session id
     * @param logger The logger to delete
     * @return An empty {@link Response}
     * @throws OXException If the {@link LogService} is not available
     */
    @DELETE
    @Path("/{sessionid}/logger/{logger}")
    public Response deleteLoggerForSession(@PathParam("sessionid") String sessionId, @PathParam("logger") String logger) {
        return doAction(() -> {
            //Check if a session was given at all
            if (Strings.isEmpty(sessionId)) {
                return RESTUtils.respondBadRequest(MSG_INVALID_SESSION_ID);
            }

            //Looking up the requests logger/filter
            Map<String, Level> lvls = getLogService().getSessionLogLevels(sessionId);
            assertLoggerExists(lvls, logger);

            //Delete
            var logService = getLogService();
            logService.removeSessionFilter(sessionId, Collections.singletonList(logger));
            return Response.ok().build();

        });
    }
}
