/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.contact.provider.utils;

import static com.openexchange.java.Autoboxing.B;
import static com.openexchange.java.Autoboxing.b;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import org.json.JSONObject;
import com.openexchange.contact.common.ContactsFolder;
import com.openexchange.contact.common.ContactsFolderProperty;
import com.openexchange.contact.common.DefaultContactsFolder;
import com.openexchange.contact.common.ExtendedProperties;
import com.openexchange.contact.common.ExtendedProperty;
import com.openexchange.contact.common.UsedForSync;
import com.openexchange.contact.provider.ContactsProviderExceptionCodes;
import com.openexchange.exception.OXException;

/**
 * {@link SettingsHelper}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class SettingsHelper {

    /** The key under which the properties of folders are stored in configuration */
    private static final String KEY_FOLDERS = "folders";

    /** The key under which the "shown in tree" setting is stored in a folder configuration */
    public static final String KEY_SHOWN_IN_TREE = "shownInTree";

    /** The key under which the "shown in tree" setting is stored in a folder configuration */
    public static final String KEY_USED_IN_PICKER = "usedInPicker";

    /** The key under which the "used for sync" setting is stored in a folder configuration */
    public static final String KEY_USED_FOR_SYNC = "usedForSync";

    /**
     * Prevent initialization.
     */
    private SettingsHelper() {
        super();
    }

    /**
     * Optionally gets the value of (the first) extended property with a specific name.
     * 
     * @param <T> The value's type
     * @param extendedProperties The extended properties to get the matching one from, or <code>null</code> if not set
     * @param name The name of the extended property to get
     * @param clazz The class to cast the value to
     * @return The value
     */
    public static <T> T optExtendedPropertyValue(ExtendedProperties extendedProperties, String name, Class<T> clazz) {
        ExtendedProperty extendedProperty = optExtendedProperty(extendedProperties, name);
        if (null != extendedProperty) {
            return clazz.cast(extendedProperty.getValue());
        }
        return null;
    }

    private static ExtendedProperty optExtendedProperty(ExtendedProperties extendedProperties, String name) {
        return null != extendedProperties ? extendedProperties.get(name) : null;
    }

    /**
     * Sets the value of a certain folder property in the supplied configuration object.
     * 
     * @param <T> The property value's type
     * @param internalConfig The internal configuration object of the account to set the value in
     * @param folderId The identifier of the folder to set the value for
     * @param propertyConfig The property config to consider
     * @param newValue The (new) value to set
     * @return <code>true</code> if the configuration was changed, <code>false</code>, otherwise
     * @throws OXException If the property value could be set
     */
    public static <T> boolean setFolderProperty(JSONObject internalConfig, String folderId, String propertyName, ProtectableValue<?> propertyConfig, T newValue) throws OXException {
        Object oldValue = putFolderProperty(internalConfig, folderId, propertyName, newValue);
        if (false == Objects.equals(oldValue, newValue)) {
            if (propertyConfig.isProtected() && false == Objects.equals(propertyConfig.getDefaultValue(), newValue)) {
                throw ContactsProviderExceptionCodes.CANT_CHANGE_PROTECTED_FOLDER_PROPERTY.create(folderId, propertyName);
            }
            return true;
        }
        return false;
    }

    /**
     * Applies the <i>subscribed</i> / <i>shown in tree</i> flag for the given folder, based on the property defaults and data stored for
     * the folder in the account settings.
     * 
     * @param internalConfig The internal configuration object of the account to get the stored value from, or <code>null</code> if not defined
     * @param folder The folder to apply the config for, with its <code>id</code> already set
     * @param propertyConfig The property config to consider
     */
    public static void applySubscribed(JSONObject internalConfig, DefaultContactsFolder folder, ProtectableValue<Boolean> propertyConfig) {
        folder.setSubscribed(getFolderProperty(internalConfig, folder.getId(), propertyConfig, KEY_SHOWN_IN_TREE, Boolean.class));
    }

    /**
     * Applies the <i>used for sync</i> flag for the given folder, based on the property defaults and data stored for the folder in the account settings.
     * 
     * @param internalConfig The internal configuration object of the account to get the stored value from, or <code>null</code> if not defined
     * @param folder The folder to apply the config for, with its <code>id</code> already set
     * @param propertyConfig The property config to consider
     */
    public static void applyUsedForSync(JSONObject internalConfig, DefaultContactsFolder folder, ProtectableValue<Boolean> propertyConfig) {
        folder.setUsedForSync(new UsedForSync(b(getFolderProperty(internalConfig, folder.getId(), propertyConfig, KEY_USED_FOR_SYNC, Boolean.class)), propertyConfig.isProtected()));
    }

    /**
     * Applies the <i>used in picker</i> flag for the given folder, based on the property defaults and data stored for the folder in the account settings.
     * 
     * @param internalConfig The internal configuration object of the account to get the stored value from, or <code>null</code> if not defined
     * @param folder The folder to apply the config for, with its <code>id</code> already set
     * @param propertyConfig The property config to consider
     */
    public static void applyUsedInPicker(JSONObject internalConfig, DefaultContactsFolder folder, ProtectableValue<Boolean> propertyConfig) {
        ExtendedProperties extendedProperties = folder.getExtendedProperties();
        if (null == extendedProperties) {
            extendedProperties = new ExtendedProperties();
        }
        String value = getFolderProperty(internalConfig, folder.getId(), propertyConfig, KEY_USED_IN_PICKER, Boolean.class).toString();
        extendedProperties.add(ContactsFolderProperty.USED_IN_PICKER(value, propertyConfig.isProtected()));
        folder.setExtendedProperties(extendedProperties);
    }

    /**
     * Sets the <i>subscribed</i> / <i>shown in tree</i> flag in the passed internal configuration object for the given folder.
     * 
     * @param internalConfig The internal configuration object of the account to set the value in
     * @param folder The folder to take over the value from, with its <code>id</code> set
     * @param propertyConfig The property config to consider
     * @return <code>true</code> if the configuration was changed, <code>false</code>, otherwise
     * @throws OXException If the property value could be set
     */
    public static boolean setSubscribed(JSONObject internalConfig, ContactsFolder folder, ProtectableValue<Boolean> propertyConfig) throws OXException {
        return setFolderProperty(internalConfig, folder, KEY_SHOWN_IN_TREE, propertyConfig, f -> Optional.ofNullable(f.isSubscribed()));
    }

    /**
     * Sets the <i>used for sync</i> flag in the passed internal configuration object for the given folder.
     * 
     * @param internalConfig The internal configuration object of the account to set the value in
     * @param folder The folder to take over the value from, with its <code>id</code> set
     * @param propertyConfig The property config to consider
     * @return <code>true</code> if the configuration was changed, <code>false</code>, otherwise
     * @throws OXException If the property value could be set
     */
    public static boolean setUsedForSync(JSONObject internalConfig, ContactsFolder folder, ProtectableValue<Boolean> propertyConfig) throws OXException {
        return setFolderProperty(internalConfig, folder, KEY_USED_FOR_SYNC, propertyConfig, f -> Optional.ofNullable(null == f.getUsedForSync() ? null : B(f.getUsedForSync().isUsedForSync())));
    }

    /**
     * Sets the <i>used in picker</i> flag in the passed internal configuration object for the given folder.
     * 
     * @param internalConfig The internal configuration object of the account to set the value in
     * @param folder The folder to take over the value from, with its <code>id</code> set
     * @param propertyConfig The property config to consider
     * @return <code>true</code> if the configuration was changed, <code>false</code>, otherwise
     * @throws OXException If the property value could be set
     */
    public static boolean setUsedInPicker(JSONObject internalConfig, ContactsFolder folder, ProtectableValue<Boolean> propertyConfig) throws OXException {
        return setFolderProperty(internalConfig, folder, KEY_USED_IN_PICKER, propertyConfig, f -> Optional.ofNullable(optExtendedPropertyValue(f.getExtendedProperties(), "usedInPicker", Boolean.class)));
    }

    /**
     * Sets the value of a certain folder property in the supplied configuration object.
     * 
     * @param <T> The property value's type
     * @param internalConfig The internal configuration object of the account to set the value in
     * @param folder The folder to set the value for
     * @param propertyConfig The property config to consider
     * @param valueFunction A function to extract the optional new value from the folder
     * @return <code>true</code> if the configuration was changed, <code>false</code>, otherwise
     * @throws OXException If the property value could be set
     */
    public static <T> boolean setFolderProperty(JSONObject internalConfig, ContactsFolder folder, String propertyName, ProtectableValue<T> propertyConfig, Function<ContactsFolder, Optional<T>> valueFunction) throws OXException {
        Optional<T> value = valueFunction.apply(folder);
        if (value.isEmpty()) {
            return false;
        }
        return setFolderProperty(internalConfig, folder.getId(), propertyName, propertyConfig, value.get());
    }

    /**
     * Gets the value of a certain folder property.
     * 
     * @param <T> The property value's type
     * @param internalConfig The internal configuration object of the account to get the value from
     * @param folderId The identifier of the folder to get the value for
     * @param propertyName The name of the property to get
     * @param clazz The class to cast the value to
     * @return The configured value, or the property's default value if not overridden
     */
    public static <T> T getFolderProperty(JSONObject internalConfig, String folderId, ProtectableValue<T> propertyConfig, String propertyName, Class<T> clazz) {
        if (propertyConfig.isProtected() || null == internalConfig) {
            return propertyConfig.getDefaultValue();
        }
        JSONObject foldersObject = internalConfig.optJSONObject(KEY_FOLDERS);
        if (null == foldersObject) {
            return propertyConfig.getDefaultValue();
        }
        JSONObject folderObject = foldersObject.optJSONObject(folderId);
        if (null == folderObject) {
            return propertyConfig.getDefaultValue();
        }
        Object value = folderObject.optIfNotNull(propertyName);
        if (null == value) {
            return propertyConfig.getDefaultValue();
        }
        return clazz.cast(value);
    }

    private static Object putFolderProperty(JSONObject internalConfig, String folderId, String propertyName, Object value) {
        JSONObject foldersObject = internalConfig.optJSONObject(KEY_FOLDERS);
        if (null == foldersObject) {
            foldersObject = new JSONObject();
            internalConfig.putSafe(KEY_FOLDERS, foldersObject);
        }
        JSONObject folderObject = foldersObject.optJSONObject(folderId);
        if (null == folderObject) {
            folderObject = new JSONObject();
            foldersObject.putSafe(folderId, folderObject);
        }
        Object oldValue = folderObject.optIfNotNull(propertyName);
        folderObject.putSafe(propertyName, value);
        return oldValue;
    }

}
