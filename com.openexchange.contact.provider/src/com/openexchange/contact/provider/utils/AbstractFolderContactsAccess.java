/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.contact.provider.utils;

import static com.openexchange.contact.common.ContactsFolderProperty.USED_IN_PICKER_LITERAL;
import static com.openexchange.contact.common.ContactsParameters.PARAMETER_COLLATION;
import static com.openexchange.contact.common.ContactsParameters.PARAMETER_CONNECTION;
import static com.openexchange.contact.common.ContactsParameters.PARAMETER_IGNORE_DISTRIBUTION_LISTS;
import static com.openexchange.contact.common.ContactsParameters.PARAMETER_INCLUDE_UNSUBSCRIBED_FOLDERS;
import static com.openexchange.contact.common.ContactsParameters.PARAMETER_LEFT_HAND_LIMIT;
import static com.openexchange.contact.common.ContactsParameters.PARAMETER_ORDER;
import static com.openexchange.contact.common.ContactsParameters.PARAMETER_ORDER_BY;
import static com.openexchange.contact.common.ContactsParameters.PARAMETER_PICKER_FOLDERS_ONLY;
import static com.openexchange.contact.common.ContactsParameters.PARAMETER_REQUIRE_EMAIL;
import static com.openexchange.contact.common.ContactsParameters.PARAMETER_RIGHT_HAND_LIMIT;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.b;
import static com.openexchange.java.Autoboxing.i;
import java.sql.Connection;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.contact.AutocompleteParameters;
import com.openexchange.contact.ContactID;
import com.openexchange.contact.SortOptions;
import com.openexchange.contact.SortOrder;
import com.openexchange.contact.common.ContactsAccount;
import com.openexchange.contact.common.ContactsParameters;
import com.openexchange.contact.common.ExtendedProperties;
import com.openexchange.contact.common.ExtendedProperty;
import com.openexchange.contact.common.GroupwareContactsFolder;
import com.openexchange.contact.common.GroupwareFolderType;
import com.openexchange.contact.common.UsedForSync;
import com.openexchange.contact.provider.ContactsProviderExceptionCodes;
import com.openexchange.contact.provider.extensions.SubscribeAware;
import com.openexchange.contact.provider.extensions.WarningsAware;
import com.openexchange.contact.provider.folder.FolderSearchAware;
import com.openexchange.contact.provider.folder.UpdateableFolderContactsAccess;
import com.openexchange.contact.provider.groupware.GroupwareContactsAccess;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contact.helpers.ContactField;
import com.openexchange.groupware.search.Order;
import com.openexchange.java.Collators;
import com.openexchange.java.Strings;
import com.openexchange.java.Suppliers;
import com.openexchange.session.Session;

/**
 * {@link AbstractFolderContactsAccess}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public abstract class AbstractFolderContactsAccess implements UpdateableFolderContactsAccess, GroupwareContactsAccess, FolderSearchAware, WarningsAware, SubscribeAware {

    /** Named logger instance */
    protected static final Logger LOG = LoggerFactory.getLogger(AbstractFolderContactsAccess.class);

    /** All contact fields as array */
    protected static final ContactField[] ALL_CONTACT_FIELDS = ContactField.values();

    protected final ContactsAccount account;
    protected final Session session;
    protected final ContactsParameters parameters;
    protected final List<OXException> warnings;

    /**
     * Initializes a new {@link AbstractFolderContactsAccess}.
     *
     * @param session The current user's session
     * @param account The underlying contacts account
     * @param parameters The contacts parameters
     */
    protected AbstractFolderContactsAccess(Session session, ContactsAccount account, ContactsParameters parameters) {
        super();
        this.account = account;
        this.session = session;
        this.parameters = parameters;
        this.warnings = new ArrayList<OXException>();
    }
    
    @Override
    public void close() {
        // nothing to close by default
    }

    @Override
    public List<OXException> getWarnings() {
        return warnings;
    }

    /**
     * Gets the identifiers of the folders that should be considered for a search request, depending on
     * {@link ContactsParameters#PARAMETER_PICKER_FOLDERS_ONLY} and {@link ContactsParameters#PARAMETER_INCLUDE_UNSUBSCRIBED_FOLDERS}.
     *
     * @param requestedFolderIds The explicitly requested folder ids from the client
     * @return The identifiers of the folders to perform the search in, or an empty list if none are applicable, or <code>null</code> if
     *         there are no restrictions, meaning to fallback to any subscribed folder
     */
    protected List<String> getSearchFolderIds(List<String> requestedFolderIds) throws OXException {
        return getSearchFolderIds(requestedFolderIds, () -> null);
    }

    /**
     * Gets the identifiers of the folders that should be considered for a search request, depending on
     * {@link ContactsParameters#PARAMETER_PICKER_FOLDERS_ONLY} and {@link ContactsParameters#PARAMETER_INCLUDE_UNSUBSCRIBED_FOLDERS}.
     *
     * @param requestedFolderIds The explicitly requested folder ids from the client
     * @return The identifiers of the folders to perform the search in, or the given fallback if there are no client-side restrictions
     */
    protected List<String> getSearchFolderIds(List<String> requestedFolderIds, Suppliers.OXSupplier<List<String>, OXException> fallbackSupplier) throws OXException {
        if (null != requestedFolderIds) {
            return requestedFolderIds; // as requested
        }
        boolean pickerFoldersOnly = b(parameters.get(PARAMETER_PICKER_FOLDERS_ONLY, Boolean.class, Boolean.FALSE));
        boolean includeUnsubscribedFolders = b(parameters.get(PARAMETER_INCLUDE_UNSUBSCRIBED_FOLDERS, Boolean.class, Boolean.FALSE));
        if (false == pickerFoldersOnly && false == includeUnsubscribedFolders) {
            return fallbackSupplier.get(); // any subscribed folder by default
        }
        List<String> searchFolderIds = new LinkedList<String>();
        for (GroupwareContactsFolder folder : getVisibleFolders()) {
            if (pickerFoldersOnly && false == isUsedInPicker(folder)) {
                continue; // skip non-picker folders
            }
            if (false == includeUnsubscribedFolders && false == isShownInTree(folder)) {
                continue; // skip unsubscribed folders
            }
            searchFolderIds.add(folder.getId());
        }
        return searchFolderIds;
    }

    /**
     * Gets all visible folders that are actually <i>subscribed</i>, i.e. shown in the folder tree.
     * 
     * @return The list of subscribed folders, or an empty list if none are subscribed
     */
    protected List<String> getSubscribedFolders() throws OXException {
        return getVisibleFolders().stream().filter(f -> isShownInTree(f)).map(GroupwareContactsFolder::getId).toList();
    }

    /**
     * Gets a value indicating whether a specific folder is configured to be used in the address book picker dialog or not.
     *
     * @param folder The folder to check
     * @return <code>true</code> if the folder is used in the picker, <code>false</code>, otherwise
     * @see #getDefaultUsedInPicker(GroupwareFolderType)
     */
    protected boolean isUsedInPicker(GroupwareContactsFolder folder) {
        ProtectableValue<Boolean> propertyConfig = getDefaultUsedInPicker(folder.getType());
        if (propertyConfig.isProtected()) {
            return b(propertyConfig.getDefaultValue());
        }
        ExtendedProperties extendedProperties = folder.getExtendedProperties();
        if (null != extendedProperties) {
            ExtendedProperty usedInPickerProperty = extendedProperties.get(USED_IN_PICKER_LITERAL);
            if (null != usedInPickerProperty && Boolean.parseBoolean((String) usedInPickerProperty.getValue())) {
                return true;
            }
        }
        return b(propertyConfig.getDefaultValue());
    }

    /**
     * Gets a value indicating whether a specific folder is configured to be used in the folder tree or not, i.e.
     * its <i>subscribed</i> flag.
     *
     * @param folder The folder to check
     * @return <code>true</code> if the folder is shown in tree, <code>false</code>, otherwise
     * @see #getDefaultShownInTree(GroupwareFolderType)
     */
    protected boolean isShownInTree(GroupwareContactsFolder folder) {
        ProtectableValue<Boolean> propertyConfig = getDefaultShownInTree(folder.getType());
        if (propertyConfig.isProtected()) {
            return b(propertyConfig.getDefaultValue());
        }
        Boolean subscribed = folder.isSubscribed();
        return b(null == subscribed ? propertyConfig.getDefaultValue() : subscribed);
    }

    /**
     * Gets a value indicating whether a specific folder is configured to be used for sync or not.
     *
     * @param folder The folder to check
     * @return The {@link UsedForSync} configuration of the folder
     * @see #getDefaultUsedForSync(GroupwareFolderType)
     */
    protected UsedForSync getUsedForSync(GroupwareContactsFolder folder) {
        ProtectableValue<Boolean> propertyConfig = getDefaultUsedForSync(folder.getType());
        if (propertyConfig.isProtected()) {
            return new UsedForSync(b(propertyConfig.getDefaultValue()), true);
        }
        UsedForSync usedForSync = folder.getUsedForSync();
        return null == usedForSync ? new UsedForSync(b(propertyConfig.getDefaultValue()), false) : usedForSync;
    }

    /**
     * Gets an extended property representing the configured default for the <i>usedInPicker</i> setting for a specific folder type.
     *
     * @param type The type of folder to get the configured default for the <i>usedInPicker</i> setting for
     * @return The configured default for the <i>usedInPicker</i> setting for this folder type
     */
    protected ProtectableValue<Boolean> getDefaultUsedInPicker(GroupwareFolderType type) {
        return new ProtectableValue<Boolean>(Boolean.TRUE, false);
    }

    /**
     * Gets an extended property representing the configured default for the <i>shownInTree</i> setting for a specific folder type, i.e.
     * its <i>subscribed</i> flag.
     *
     * @param type The type of folder to get the configured default for the <i>shownInTree</i> setting for
     * @return The configured default for the <i>shownInTree</i> setting for this folder type
     */
    protected ProtectableValue<Boolean> getDefaultShownInTree(GroupwareFolderType type) {
        return new ProtectableValue<Boolean>(Boolean.TRUE, false);
    }

    /**
     * Gets an extended property representing the configured default for the <i>usedForSync</i> setting for a specific folder type.
     *
     * @param type The type of folder to get the configured default for the <i>usedForSync</i> setting for
     * @return The configured default for the <i>usedForSync</i> setting for this folder type
     */
    protected ProtectableValue<Boolean> getDefaultUsedForSync(GroupwareFolderType type) {
        return new ProtectableValue<Boolean>(Boolean.FALSE, false);
    }

    /**
     * Initializes a new {@link AutocompleteParameters} based on {@link ContactsParameters#PARAMETER_REQUIRE_EMAIL} and
     * {@link ContactsParameters#PARAMETER_IGNORE_DISTRIBUTION_LISTS}. The current session user is inserted for
     * {@link AutocompleteParameters#USER_ID} as well.
     * 
     * @return The initialized autocomplete parameters
     */
    protected AutocompleteParameters getAutocompleteParameters() {
        AutocompleteParameters autocompleteParameters = getAutocompleteParameters(parameters);
        autocompleteParameters.put(AutocompleteParameters.USER_ID, I(session.getUserId()));
        return autocompleteParameters;
    }

    /**
     * Initializes a new {@link AutocompleteParameters} based on {@link ContactsParameters#PARAMETER_REQUIRE_EMAIL} and
     * {@link ContactsParameters#PARAMETER_IGNORE_DISTRIBUTION_LISTS}.
     *
     * @param parameters The contacts parameters to query
     * @return The initialized autocomplete parameters
     */
    protected static AutocompleteParameters getAutocompleteParameters(ContactsParameters parameters) {
        AutocompleteParameters autocompleteParameters = AutocompleteParameters.newInstance();
        autocompleteParameters.put(AutocompleteParameters.REQUIRE_EMAIL, parameters.get(PARAMETER_REQUIRE_EMAIL, Boolean.class));
        autocompleteParameters.put(AutocompleteParameters.IGNORE_DISTRIBUTION_LISTS, parameters.get(PARAMETER_IGNORE_DISTRIBUTION_LISTS, Boolean.class));
        return autocompleteParameters;
    }

    /**
     * Initializes the {@link SortOptions} based on several contacts parameters.
     * 
     * @return The sort options
     * @throws OXException If invalid sort options are requested by the client
     */
    protected SortOptions getSortOptions() throws OXException {
        return getSortOptions(parameters);
    }

    /**
     * Initializes the {@link SortOptions} based on several contacts parameters.
     * 
     * @param parameters The contacts parameters to query
     * @return The sort options
     * @throws OXException If invalid sort options are requested by the client
     */
    protected static SortOptions getSortOptions(ContactsParameters parameters) throws OXException {
        ContactField orderBy = parameters.get(PARAMETER_ORDER_BY, ContactField.class);
        if (null == orderBy) {
            return SortOptions.EMPTY;
        }
        SortOptions options;
        if (ContactField.USE_COUNT.equals(orderBy)) {
            // special handling for use count: default to DESC order, add folder id as tie-breaker 
            Order order = parameters.get(PARAMETER_ORDER, Order.class, Order.DESCENDING);
            options = new SortOptions(new SortOrder(orderBy, order), new SortOrder(ContactField.FOLDER_ID, Order.ASCENDING));
        } else {
            Order order = parameters.get(PARAMETER_ORDER, Order.class, Order.ASCENDING);
            options = new SortOptions(SortOptions.sortOrderFor(orderBy, order));
        }
        String collation = parameters.get(PARAMETER_COLLATION, String.class);
        if (Strings.isNotEmpty(collation)) {
            options.setCollation(collation);
        }
        int leftHandLimit = i(parameters.get(PARAMETER_LEFT_HAND_LIMIT, Integer.class, I(0)));
        if (0 < leftHandLimit) {
            options.setRangeStart(leftHandLimit);
        }
        int rightHandLimit = i(parameters.get(PARAMETER_RIGHT_HAND_LIMIT, Integer.class, I(-1)));
        if (-1 != rightHandLimit) {
            if (rightHandLimit < leftHandLimit) {
                throw ContactsProviderExceptionCodes.INVALID_RANGE_LIMITS.create();
            }
            options.setLimit(rightHandLimit - leftHandLimit);
        }
        return options;
    }

    /**
     * Gets the {@link ContactsParameters#PARAMETER_FIELDS} parameter. If absent falls back to the {@link ALL_CONTACT_FIELDS}.
     *
     * @return The {@link ContactField}s array
     */
    protected ContactField[] getFields() {
        ContactField[] fields = parameters.get(ContactsParameters.PARAMETER_FIELDS, ContactField[].class);
        if (fields == null) {
            fields = ALL_CONTACT_FIELDS;
        }
        return fields;
    }

    /**
     * Optionally gets the {@link Connection} that was passed as {@link ContactsParameters#PARAMETER_CONNECTION()}.
     *
     * @return The {@link Connection} or <code>null</code>
     */
    protected Connection optConnection() {
        return parameters.get(PARAMETER_CONNECTION(), Connection.class);
    }

    protected static Map<String, List<String>> getObjectIdsPerFolderId(List<ContactID> contactIDs) {
        if (null == contactIDs || contactIDs.isEmpty()) {
            return Collections.emptyMap();
        }
        if (1 == contactIDs.size()) {
            ContactID contactID = contactIDs.get(0);
            return Collections.singletonMap(contactID.getFolderID(), Collections.singletonList(contactID.getObjectID()));
        }
        Map<String, List<String>> objectsIdsPerFolderId = new HashMap<String, List<String>>();
        for (ContactID contactID : contactIDs) {
            com.openexchange.tools.arrays.Collections.put(objectsIdsPerFolderId, contactID.getFolderID(), contactID.getObjectID());
        }
        return objectsIdsPerFolderId;
    }

    /**
     * Sorts the specified list of contacts folders by name. The default folders will end up in the first places.
     *
     * @param contactsFolders The contacts folders to sort
     * @param locale The locale
     * @return The sorted contacts folders
     */
    protected static List<GroupwareContactsFolder> sort(List<GroupwareContactsFolder> contactsFolders, Locale locale) {
        if (null == contactsFolders || 2 > contactsFolders.size()) {
            return contactsFolders;
        }
        Collator collator = Collators.getSecondaryInstance(locale);
        contactsFolders.sort((folder1, folder2) -> {
            if (folder1.isDefaultFolder() != folder2.isDefaultFolder()) {
                // Default folders first
                return folder1.isDefaultFolder() ? -1 : 1;
            }
            // Otherwise, compare folder names
            return collator.compare(folder1.getName(), folder2.getName());
        });
        return contactsFolders;
    }

    @Override
    public String toString() {
        return "AbstractFolderContactsAccess [account=" + account + "]";
    }

}
