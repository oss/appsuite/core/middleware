/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cluster.serialization.session;

import static com.openexchange.cluster.serialization.session.utils.JSONParameterName.*;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.session.SessionVersionService.DEFAULT_VERSION;
import static com.openexchange.session.SessionVersionService.JSON_V1_VERSION;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PushbackInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.lang.reflect.Array;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.GZIPOutputStream;
import org.json.JSONCoercion;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import org.json.SimpleJSONSerializer;
import org.slf4j.Logger;
import com.openexchange.cluster.serialization.session.utils.BoolParameterHandler;
import com.openexchange.cluster.serialization.session.utils.LongParameterHandler;
import com.openexchange.cluster.serialization.session.utils.ParameterHandler;
import com.openexchange.cluster.serialization.session.utils.StringParameterHandler;
import com.openexchange.cluster.serialization.session.utils.UserAgentParameterHandler;
import com.openexchange.java.Streams;
import com.openexchange.java.Strings;
import com.openexchange.log.LogUtility;
import com.openexchange.session.ObfuscatorService;
import com.openexchange.session.Origin;
import com.openexchange.session.Session;
import com.openexchange.session.SessionVersionService;
import com.openexchange.session.VersionMismatchException;

/**
 * {@link SessionCodec} - The codec for session serialization in cluster.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.0.0
 */
public final class SessionCodec {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(SessionCodec.class);
    }

    private static final AtomicReference<ObfuscatorService> OBFUSCATOR_REFERENCE = new AtomicReference<>();

    /**
     * Sets the obfuscator service.
     *
     * @param obfuscatorService The obfuscator service
     */
    public static void setObfuscatorService(ObfuscatorService obfuscatorService) {
        OBFUSCATOR_REFERENCE.set(obfuscatorService);
    }

    private static final AtomicReference<SessionVersionService> SESSION_VERSION_REFERENCE = new AtomicReference<>();

    /**
     * Sets the session version service.
     *
     * @param obfuscatorService The session version service
     */
    public static void setSessionVersionService(SessionVersionService sessionVersionService) {
        SESSION_VERSION_REFERENCE.set(sessionVersionService);
    }

    /**
     * Initializes a new {@link SessionCodec}.
     */
    private SessionCodec() {
        super();
    }

    private static JSONObject stream2json(InputStream jsonData) throws JSONException {
        try {
            return JSONServices.parseObject(jsonData);
        } finally {
            Streams.close(jsonData);
        }
    }

    /**
     * Gets the session from given byte stream.
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * <b>Note</b>: Session's password is unobfuscated from JSON representation.
     * </div>
     *
     * @param stream The stream to parse
     * @return The session
     * @throws IOException If an I/O error occurs
     * @throws JSONException If conversion fails
     * @throws VersionMismatchException If version of stored session data does not match the expected version of the application
     * @throws IllegalStateException If tracked obfuscator is absent
     */
    public static ClusterSession stream2Session(InputStream stream) throws IOException, JSONException, VersionMismatchException {
        if (null == stream) {
            return null;
        }

        ObfuscatorService obfuscatorService = OBFUSCATOR_REFERENCE.get();
        if (obfuscatorService == null) {
            throw new IllegalStateException("Obfuscator is absent");
        }

        SessionVersionService versionService = SESSION_VERSION_REFERENCE.get();
        if (versionService == null) {
            throw new IllegalStateException("Session version service is absent");
        }

        return stream2Session(stream, obfuscatorService, versionService);
    }

    /**
     * Gets the session from given byte stream.
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * <b>Note</b>: Session's password is unobfuscated from JSON representation.
     * </div>
     *
     * @param stream The stream to parse
     * @param optObfuscatorService The obfuscator to use or <code>null</code> to leave password obfuscated
     * @param versionService The version service to obtain expected application version from
     * @return The session
     * @throws IOException If an I/O error occurs
     * @throws JSONException If conversion fails
     * @throws VersionMismatchException If version of stored session data does not match the expected version of the application
     * @throws IllegalArgumentException If passed obfuscator is <code>null</code>
     */
    public static ClusterSession stream2Session(InputStream stream, ObfuscatorService optObfuscatorService, SessionVersionService versionService) throws IOException, JSONException, VersionMismatchException {
        if (null == stream) {
            return null;
        }

        PushbackInputStream pis = null;
        try {
            // Read first byte
            int first = stream.read();

            // Check for possible end of stream
            if (first < 0) {
                // End of the stream reached
                throw new JSONException("No starting '{' character");
            }

            // Check for starting JSON object
            if (first == '{') {
                // Apparently a stream with starting JSON object content.
                // Push back first non-whitespace byte and parse it.
                pis = stream instanceof PushbackInputStream ? ((PushbackInputStream) stream) : new PushbackInputStream(stream);
                pis.unread(first);
                return json2Session(stream2json(pis), optObfuscatorService, versionService);
            }

            // Assume encoded version number in first two bytes. Thus, parse first two bytes to version number and check it.
            int version = bytesToInt((byte) first, (byte) stream.read());
            int expectedVersion = versionService == null ? DEFAULT_VERSION : versionService.getVersion();
            if (expectedVersion > DEFAULT_VERSION) {
                // A certain version is expected
                if (version > DEFAULT_VERSION && version != expectedVersion) { // NOSONARLINT
                    // Stored session data does not match expected version
                    throw new VersionMismatchException(version, expectedVersion);
                }
            }

            if (DEFAULT_VERSION == version || JSON_V1_VERSION == version) {
                // Assume data is encoded according to JSON v1. No version check needed then...
                return json2Session(stream2json(stream), optObfuscatorService, null);
            }

            /*-
             * Other versions here
             *
             * if (JSON_V2_VERSION == version) {
             *     ...
             * }
             */

            // Version unknown to this application
            throw new VersionMismatchException(version, expectedVersion);
        } finally {
            Streams.close(pis, stream);
        }
    }

    /**
     * Gets the session from given JSON representation.
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * <b>Note</b>: Session's password is unobfuscated from JSON representation.
     * </div>
     *
     * @param jSession The JSON representation to convert
     * @return The session
     * @throws JSONException If conversion fails
     * @throws VersionMismatchException If version of stored session data does not match the expected version of the application
     * @throws IllegalStateException If tracked obfuscator is absent
     */
    public static ClusterSession json2Session(JSONObject jSession) throws JSONException, VersionMismatchException {
        if (null == jSession) {
            return null;
        }

        ObfuscatorService obfuscatorService = OBFUSCATOR_REFERENCE.get();
        if (obfuscatorService == null) {
            throw new IllegalStateException("Obfuscator is absent");
        }

        SessionVersionService versionService = SESSION_VERSION_REFERENCE.get();
        if (versionService == null) {
            throw new IllegalStateException("Session version service is absent");
        }

        return json2Session(jSession, obfuscatorService, versionService);
    }

    /**
     * Gets the session from given JSON representation.
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * <b>Note</b>: Session's password is unobfuscated from JSON representation.
     * </div>
     *
     * @param jSession The JSON representation to convert
     * @param optObfuscatorService The obfuscator to use or <code>null</code> to leave password obfuscated
     * @param optVersionService The optional version service to obtain expected application version from or <code>null</code> to bypass version check
     * @return The session
     * @throws JSONException If conversion fails
     * @throws VersionMismatchException If version of stored session data does not match the expected version of the application
     * @throws IllegalArgumentException If passed obfuscator is <code>null</code>
     */
    public static ClusterSession json2Session(JSONObject jSession, ObfuscatorService optObfuscatorService, SessionVersionService optVersionService) throws JSONException, VersionMismatchException {
        if (jSession == null) {
            return null;
        }

        // Check version (if required)
        if (optVersionService != null) {
            int expectedVersion = optVersionService.getVersion();
            if (expectedVersion > DEFAULT_VERSION) {
                int version = jSession.optInt(VERSION.getShortName(), DEFAULT_VERSION);
                if (version == DEFAULT_VERSION) {
                    version = VERSION.getLongName().map(longName -> I(jSession.optInt(longName, DEFAULT_VERSION))).orElse(I(DEFAULT_VERSION)).intValue();
                }
                if (version > DEFAULT_VERSION && version != optVersionService.getVersion()) {
                    // Stored session data does not match expected version
                    throw new VersionMismatchException(version, optVersionService.getVersion());
                }
            }
        }

        String sessionId = SESSION_ID.getStringFrom(jSession);
        int contextId = CONTEXT_ID.getIntFrom(jSession);
        int userId = USER_ID.getIntFrom(jSession);
        String loginName = LOGIN_NAME.getStringFrom(jSession);
        String obfuscatedPassword = PASSWORD.getStringFrom(jSession);
        String secret = SECRET.getStringFrom(jSession);
        String login = LOGIN.getStringFrom(jSession);
        String randomToken = RANDOM_TOKEN.getStringFrom(jSession);
        String localIp = LOCAL_IP.getStringFrom(jSession);
        String authId = AUTH_ID.getStringFrom(jSession);
        String hash = HASH.getStringFrom(jSession);
        String client = CLIENT.getStringFrom(jSession);
        String userLogin = USER_LOGIN.getStringFrom(jSession);
        Origin origin = Origin.originFor(ORIGIN.getStringFrom(jSession));
        boolean staySignedIn = STAY_SIGNED_IN.getBoolFrom(jSession);

        Map<String, Object> parameters = null;
        for (ParameterHandler<?> parameterHandler : PARAMETER_HANDLERS) {
            parameters = parameterHandler.deserializeParameter(jSession, parameters);
        }

        JSONObject jParameters = PARAMETERS.getJSONObjectFrom(jSession);
        if (jParameters != null) {
            if (parameters == null) {
                parameters = LinkedHashMap.newLinkedHashMap(jParameters.length());
            }
            for (Map.Entry<String, Object> entry : jParameters.entrySet()) {
                parameters.put(entry.getKey(), JSONCoercion.coerceToNative(entry.getValue()));
            }
        }

        String passwordToSet = obfuscatedPassword == null ? null : (optObfuscatorService == null ? obfuscatedPassword : optObfuscatorService.unobfuscate(obfuscatedPassword));
        return new ClusterSession(sessionId, loginName, passwordToSet, contextId, userId, secret, login, randomToken, localIp, authId, hash, client, userLogin, staySignedIn, origin, parameters);
    }

    /**
     * Gets the JSON content stream for given session.
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * <b>Note</b>: Session's password is obfuscated in JSON representation.
     * </div>
     *
     * @param session The session to convert
     * @param gzip Whether to compress bytes using GZIP or not
     * @return The session's JSON content stream
     * @throws JSONException If conversion fails
     * @throws IllegalStateException If tracked obfuscator or session version service is absent
     */
    public static InputStream session2JsonStream(Session session, boolean gzip) throws JSONException {
        if (null == session) {
            return null;
        }

        ObfuscatorService obfuscatorService = OBFUSCATOR_REFERENCE.get();
        if (obfuscatorService == null) {
            throw new IllegalStateException("Obfuscator is absent");
        }

        SessionVersionService versionService = SESSION_VERSION_REFERENCE.get();
        if (versionService == null) {
            throw new IllegalStateException("Session version service is absent");
        }

        return session2JsonStream(session, gzip, obfuscatorService, versionService);
    }

    /**
     * Gets the JSON content stream for given session.
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * <b>Note</b>: Session's password is obfuscated in JSON representation.
     * </div>
     *
     * @param session The session to convert
     * @param gzip Whether to compress bytes using GZIP or not
     * @param optObfuscatorService The obfuscator to use or <code>null</code> to leave password as-is
     * @param versionService The version service to obtain expected application version from
     * @return The session's JSON content stream
     * @throws JSONException If conversion fails
     * @throws IllegalArgumentException If passed obfuscator or session version service is <code>null</code>
     */
    public static InputStream session2JsonStream(Session session, boolean gzip, ObfuscatorService optObfuscatorService, SessionVersionService versionService) throws JSONException {
        return Streams.asInputStream(session2JsonBaos(session, gzip, optObfuscatorService, versionService));
    }

    /**
     * Gets the JSON content bytes for given session.
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * <b>Note</b>: Session's password is obfuscated in JSON representation.
     * </div>
     *
     * @param session The session to convert
     * @param gzip Whether to compress bytes using GZIP or not
     * @param optObfuscatorService The obfuscator to use or <code>null</code> to leave password as-is
     * @param versionService The version service to obtain expected application version from
     * @return The session's JSON content bytes
     * @throws JSONException If conversion fails
     * @throws IllegalArgumentException If passed obfuscator or session version service is <code>null</code>
     */
    public static byte[] session2JsonBytes(Session session, boolean gzip, ObfuscatorService optObfuscatorService, SessionVersionService versionService) throws JSONException {
        return session2JsonBaos(session, gzip, optObfuscatorService, versionService).toByteArray();
    }

    /**
     * Gets the JSON content stream for given session.
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * <b>Note</b>: Session's password is obfuscated in JSON representation.
     * </div>
     *
     * @param session The session to convert
     * @param gzip Whether to compress bytes using GZIP or not
     * @param optObfuscatorService The obfuscator to use or <code>null</code> to leave password as-is
     * @param versionService The version service to obtain expected application version from
     * @return The session's JSON content stream
     * @throws JSONException If conversion fails
     * @throws IllegalArgumentException If passed obfuscator or session version service is <code>null</code>
     */
    private static ByteArrayOutputStream session2JsonBaos(Session session, boolean gzip, ObfuscatorService optObfuscatorService, SessionVersionService versionService) throws JSONException {
        if (session == null) {
            return null;
        }
        if (versionService == null) {
            throw new IllegalArgumentException("Session version service is absent");
        }

        ByteArrayOutputStream baos = Streams.newByteArrayOutputStream();
        try {
            // Encode version
            int applicationVersion = versionService.getVersion();
            baos.write((byte) ((applicationVersion >>> 8) & 0xFF));
            baos.write((byte) (applicationVersion & 0xFF));

            GZIPOutputStream gzipOutputStream = null;

            OutputStreamWriter writer;
            if (gzip) {
                gzipOutputStream = new GZIPOutputStream(baos);
                writer = new OutputStreamWriter(gzipOutputStream, StandardCharsets.UTF_8);
            } else {
                writer = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
            }

            writer.append('{');

            SimpleJSONSerializer.writeJsonValue(SESSION_ID.getShortName(), writer);
            writer.append(':');
            SimpleJSONSerializer.writeJsonValue(session.getSessionID(), writer);

            writer.append(',');
            SimpleJSONSerializer.writeJsonValue(CONTEXT_ID.getShortName(), writer);
            writer.append(':');
            SimpleJSONSerializer.writeJsonValue(session.getContextId(), writer);

            writer.append(',');
            SimpleJSONSerializer.writeJsonValue(USER_ID.getShortName(), writer);
            writer.append(':');
            SimpleJSONSerializer.writeJsonValue(session.getUserId(), writer);

            writer.append(',');
            SimpleJSONSerializer.writeJsonValue(VERSION.getShortName(), writer);
            writer.append(':');
            SimpleJSONSerializer.writeJsonValue(versionService.getVersion(), writer);

            if (session.getLoginName() != null) {
                writer.append(',');
                SimpleJSONSerializer.writeJsonValue(LOGIN_NAME.getShortName(), writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(session.getLoginName(), writer);
            }

            if (session.getPassword() != null) {
                writer.append(',');
                SimpleJSONSerializer.writeJsonValue(PASSWORD.getShortName(), writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(optObfuscatorService == null ? session.getPassword() : optObfuscatorService.obfuscate(session.getPassword()), writer);
            }

            if (session.getSecret() != null) {
                writer.append(',');
                SimpleJSONSerializer.writeJsonValue(SECRET.getShortName(), writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(session.getSecret(), writer);
            }

            if (session.getLogin() != null) {
                writer.append(',');
                SimpleJSONSerializer.writeJsonValue(LOGIN.getShortName(), writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(session.getLogin(), writer);
            }

            if (session.getRandomToken() != null) {
                writer.append(',');
                SimpleJSONSerializer.writeJsonValue(RANDOM_TOKEN.getShortName(), writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(session.getRandomToken(), writer);
            }

            if (session.getLocalIp() != null) {
                writer.append(',');
                SimpleJSONSerializer.writeJsonValue(LOCAL_IP.getShortName(), writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(session.getLocalIp(), writer);
            }

            if (session.getAuthId() != null) {
                writer.append(',');
                SimpleJSONSerializer.writeJsonValue(AUTH_ID.getShortName(), writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(session.getAuthId(), writer);
            }

            if (session.getHash() != null) {
                writer.append(',');
                SimpleJSONSerializer.writeJsonValue(HASH.getShortName(), writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(session.getHash(), writer);
            }

            if (session.getClient() != null) {
                writer.append(',');
                SimpleJSONSerializer.writeJsonValue(CLIENT.getShortName(), writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(session.getClient(), writer);
            }

            if (session.getUserlogin() != null) {
                writer.append(',');
                SimpleJSONSerializer.writeJsonValue(USER_LOGIN.getShortName(), writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(session.getUserlogin(), writer);
            }

            if (session.getOrigin() != null) {
                writer.append(',');
                SimpleJSONSerializer.writeJsonValue(ORIGIN.getShortName(), writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(session.getOrigin() == null ? null : session.getOrigin().name(), writer);
            }

            writer.append(',');
            SimpleJSONSerializer.writeJsonValue(STAY_SIGNED_IN.getShortName(), writer);
            writer.append(':');
            SimpleJSONSerializer.writeJsonValue(session.isStaySignedIn(), writer);

            for (ParameterHandler<?> parameterHandler : PARAMETER_HANDLERS) {
                Object optParameter = session.getParameter(parameterHandler.getParameterName());
                if (optParameter != null) {
                    writer.append(',');
                    SimpleJSONSerializer.writeJsonValue(parameterHandler.getJsonParameterName().getShortName(), writer);
                    writer.append(':');
                    SimpleJSONSerializer.writeJsonValue(parameterHandler.coerceTo(optParameter), writer);
                }
            }

            boolean parameterWritten = false;
            Object optParameter;
            for (String parameterName : session.getParameterNames()) {
                if (!IGNOREES.contains(parameterName)) {
                    optParameter = session.getParameter(parameterName);
                    if (isSerializablePojo(optParameter)) {
                        if (parameterWritten) {
                            // Append parameter
                            writer.append(',');
                            SimpleJSONSerializer.writeJsonValue(parameterName, writer);
                            writer.append(':');
                            SimpleJSONSerializer.writeJsonValue(optParameter, writer);
                        } else {
                            // Start new JSON object for parameter
                            writer.append(',');
                            SimpleJSONSerializer.writeJsonValue(PARAMETERS.getShortName(), writer);
                            writer.append(':');
                            writer.append('{');

                            SimpleJSONSerializer.writeJsonValue(parameterName, writer);
                            writer.append(':');
                            SimpleJSONSerializer.writeJsonValue(optParameter, writer);

                            parameterWritten = true;
                        }
                    }
                }
            }
            if (parameterWritten) {
                writer.append('}');
            }

            writer.append('}');

            writer.flush();
            writer.close();

            if (gzipOutputStream != null) {
                gzipOutputStream.flush();
                gzipOutputStream.close();
            }

            return baos;
        } catch (IOException e) {
            // Should not occur for ByteArrayOutputStream
            throw new JSONException("I/O error occurred", e);
        }
    }

    /**
     * Gets the JSON representation for given session.
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * <b>Note</b>: Session's password is obfuscated in JSON representation.
     * </div>
     *
     * @param session The session to convert
     * @return The session's JSON representation
     * @throws JSONException If conversion fails
     * @throws IllegalStateException If tracked obfuscator or session version service is absent
     */
    public static JSONObject session2Json(Session session) throws JSONException {
        if (null == session) {
            return null;
        }

        ObfuscatorService obfuscatorService = OBFUSCATOR_REFERENCE.get();
        if (obfuscatorService == null) {
            throw new IllegalStateException("Obfuscator is absent");
        }

        SessionVersionService versionService = SESSION_VERSION_REFERENCE.get();
        if (versionService == null) {
            throw new IllegalStateException("Session version service is absent");
        }

        return session2Json(session, obfuscatorService, versionService);
    }

    /**
     * Gets the JSON representation for given session.
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * <b>Note</b>: Session's password is obfuscated in JSON representation.
     * </div>
     *
     * @param session The session to convert
     * @param obfuscatorService The obfuscator to use
     * @param versionService The version service to obtain expected application version from
     * @return The session's JSON representation
     * @throws JSONException If conversion fails
     * @throws IllegalArgumentException If passed obfuscator or session version service is <code>null</code>
     */
    public static JSONObject session2Json(Session session, ObfuscatorService obfuscatorService, SessionVersionService versionService) throws JSONException {
        if (session == null) {
            return null;
        }
        if (obfuscatorService == null) {
            throw new IllegalArgumentException("Obfuscator is absent");
        }
        if (versionService == null) {
            throw new IllegalArgumentException("Session version service is absent");
        }

        JSONObject jSession = new JSONObject(18);

        jSession.putOpt(SESSION_ID.getShortName(), session.getSessionID());
        jSession.put(CONTEXT_ID.getShortName(), session.getContextId());
        jSession.put(USER_ID.getShortName(), session.getUserId());
        jSession.put(VERSION.getShortName(), versionService.getVersion());
        jSession.putOpt(LOGIN_NAME.getShortName(), session.getLoginName());
        jSession.putOpt(PASSWORD.getShortName(), session.getPassword() == null ? null : obfuscatorService.obfuscate(session.getPassword()));
        jSession.putOpt(SECRET.getShortName(), session.getSecret());
        jSession.putOpt(LOGIN.getShortName(), session.getLogin());
        jSession.putOpt(RANDOM_TOKEN.getShortName(), session.getRandomToken());
        jSession.putOpt(LOCAL_IP.getShortName(), session.getLocalIp());
        jSession.putOpt(AUTH_ID.getShortName(), session.getAuthId());
        jSession.putOpt(HASH.getShortName(), session.getHash());
        jSession.putOpt(CLIENT.getShortName(), session.getClient());
        jSession.putOpt(USER_LOGIN.getShortName(), session.getUserlogin());
        jSession.putOpt(ORIGIN.getShortName(), session.getOrigin() == null ? null : session.getOrigin().name());
        jSession.put(STAY_SIGNED_IN.getShortName(), session.isStaySignedIn());

        for (ParameterHandler<?> parameterHandler : PARAMETER_HANDLERS) {
            parameterHandler.serializeParameter(session, jSession);
        }

        JSONObject jRemoteParameters = null;
        Object optParameter;
        for (String parameterName : session.getParameterNames()) {
            if (!IGNOREES.contains(parameterName)) {
                optParameter = session.getParameter(parameterName);
                if (isSerializablePojo(optParameter)) {
                    if (jRemoteParameters == null) {
                        jRemoteParameters = new JSONObject();
                    }
                    try {
                        jRemoteParameters.putSafe(parameterName, JSONCoercion.coerceToJSON(optParameter));
                    } catch (java.lang.IllegalStateException e) {
                        // Max. size for JSON object exceeded
                        LoggerHolder.LOG.error("JSON representation for session {} could not be created. Too many (remote) parameters:{}{}",
                            session.getSessionID(), Strings.getLineSeparator(), LogUtility.toStringObjectFor(jRemoteParameters.keySet(), null, 25), e);
                        throw new JSONException("JSON representation for session " + session.getSessionID() + " could not be created", e);
                    }
                }
            }
        }
        jSession.putOpt(PARAMETERS.getShortName(), jRemoteParameters);

        return jSession;
    }

    private static final String POJO_PACKAGE = "java.lang.";

    private static boolean isSerializablePojo(Object obj) {
        if (null == obj) {
            // No null values
            return false;
        }
        if (obj instanceof String) {
            return true;
        }
        if (obj instanceof Boolean) {
            return true;
        }
        if ((obj instanceof Number) && obj.getClass().getName().startsWith(POJO_PACKAGE)) {
            return true;
        }
        if (obj instanceof List) {
            // Allow list of coercible objects
            for (Object entry : (List<?>) obj) {
                if (!isSerializablePojo(entry)) {
                    return false;
                }
            }
            return true;
        }
        if (obj instanceof Map) {
            // Allow map of string to coercible object associations
            for (Map.Entry<?, ?> entry : (Set<Map.Entry<?, ?>>) ((Map) obj).entrySet()) {
                if (!(entry.getKey() instanceof String)) {
                    return false;
                }
                if (!isSerializablePojo(entry.getValue())) {
                    return false;
                }
            }
            return true;
        }
        if (JSONCoercion.isArray(obj)) {
            // Allow array of coercible objects
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                final Object object = Array.get(obj, i);
                if (!isSerializablePojo(object)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    // ----------------------------------------------------- Parameter handlers ------------------------------------------------------------

    /** The names of such parameters that shall be ignored when serializing a session to its JSON representation */
    private static final Set<String> IGNOREES = Set.copyOf(yieldParametersToIgnoreOnSerialization());

    private static Set<String> yieldParametersToIgnoreOnSerialization() {
        List<ParameterHandler<?>> parameterHandlers = yieldListOfParameterHandlers();
        Set<String> ignorees = HashSet.newHashSet(parameterHandlers.size() + 2);
        for (ParameterHandler<?> parameterHandler : parameterHandlers) {
            ignorees.add(parameterHandler.getParameterName());
        }
        ignorees.add(Session.PARAM_COUNTER);
        ignorees.add(Session.PARAM_LOCK);
        return ignorees;
    }

    /** The static list of parameter handlers */
    private static final List<ParameterHandler<?>> PARAMETER_HANDLERS = List.copyOf(yieldListOfParameterHandlers());

    private static List<ParameterHandler<?>> yieldListOfParameterHandlers() {
        List<ParameterHandler<?>> parameterHandlers = new ArrayList<>(16);

        // Session.PARAM_ALTERNATIVE_ID
        parameterHandlers.add(new StringParameterHandler(Session.PARAM_ALTERNATIVE_ID, ALTERNATIVE_ID));

        // Session.PARAM_GUEST
        parameterHandlers.add(new BoolParameterHandler(Session.PARAM_GUEST, GUEST_FLAG));

        // Session.PARAM_DEVICE_USER_AGENT
        parameterHandlers.add(new UserAgentParameterHandler(Session.PARAM_DEVICE_USER_AGENT, DEVICE_USER_AGENT));

        // Session.PARAM_USER_AGENT
        parameterHandlers.add(new UserAgentParameterHandler(Session.PARAM_USER_AGENT, USER_AGENT));

        // Session.PARAM_HOST_NAME
        parameterHandlers.add(new StringParameterHandler(Session.PARAM_HOST_NAME, HOST_NAME));

        // Session.PARAM_LOGIN_TIME
        parameterHandlers.add(new LongParameterHandler(Session.PARAM_LOGIN_TIME, LOGIN_TIME));

        // Session.PARAM_COOKIE_REFRESH_TIMESTAMP
        parameterHandlers.add(new LongParameterHandler(Session.PARAM_COOKIE_REFRESH_TIMESTAMP, COOKIE_REFRESH_TIMESTAMP));

        // Session.PARAM_LOCAL_LAST_ACTIVE
        parameterHandlers.add(new LongParameterHandler(Session.PARAM_LOCAL_LAST_ACTIVE, LOCAL_LAST_ACTIVE));

        // Session.PARAM_OAUTH_ACCESS_TOKEN
        parameterHandlers.add(new StringParameterHandler(Session.PARAM_OAUTH_ACCESS_TOKEN, OAUTH_ACCESS_TOKEN));

        // Session.PARAM_OAUTH_REFRESH_TOKEN
        parameterHandlers.add(new StringParameterHandler(Session.PARAM_OAUTH_REFRESH_TOKEN, OAUTH_REFRESH_TOKEN));

        // Session.PARAM_OAUTH_ACCESS_TOKEN_EXPIRY_DATE
        parameterHandlers.add(new StringParameterHandler(Session.PARAM_OAUTH_ACCESS_TOKEN_EXPIRY_DATE, OAUTH_ACCESS_TOKEN_EXPIRY_DATE));

        // Session.PARAM_OAUTH_ID_TOKEN
        parameterHandlers.add(new StringParameterHandler(Session.PARAM_OAUTH_ID_TOKEN, OAUTH_ID_TOKEN));

        // Session.PARAM_BRAND
        parameterHandlers.add(new StringParameterHandler(Session.PARAM_BRAND, BRAND));

        // Session.PARAM_USER_SCHEMA
        parameterHandlers.add(new StringParameterHandler(Session.PARAM_USER_SCHEMA, USER_SCHEMA));

        // Session.PARAM_MASTER_AUTHENTICATED
        parameterHandlers.add(new BoolParameterHandler(Session.PARAM_MASTER_AUTHENTICATED, MASTER_AUTHENTICATED));

        return parameterHandlers;
    }

    private static int bytesToInt(byte first, byte second) {
        return ((first & 0xff) << 8) | (second & 0xff);
    }

    private static byte[] intToBytes(int i) {
        byte[] data = new byte[2];
        data[1] = (byte) i;
        data[0] = (byte) (i >>> 8);
        return data;
    }

}
