/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cluster.serialization.session.utils;

import org.json.JSONObject;

/**
 * {@link LongParameterHandler} - The long parameter handler.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class LongParameterHandler extends ParameterHandler<Long> {

    /**
     * Initializes a new {@link LongParameterHandler}.
     *
     * @param parameterName The name of the parameter in session
     * @param jsonParameterName The name of the parameter in session's JSON representation
     */
    public LongParameterHandler(String parameterName, JSONParameterName jsonParameterName) {
        super(parameterName, jsonParameterName);
    }

    @Override
    public Long coerceTo(Object nonNullValue) {
        return nonNullValue instanceof Long ? (Long) nonNullValue : Long.valueOf(nonNullValue.toString().trim());
    }

    @Override
    protected Object getParameterFrom(JSONObject jSession) {
        long optLong = jsonParameterName.getLongFrom(jSession);
        return optLong == 0 ? null : Long.valueOf(optLong);
    }
}