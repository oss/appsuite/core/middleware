/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cluster.serialization.session.utils;

import java.util.List;
import org.json.JSONObject;

/**
 * {@link UserAgentParameterHandler} - The User-Agent string parameter handler.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class UserAgentParameterHandler extends ParameterHandler<String> {

    /**
     * Initializes a new {@link UserAgentParameterHandler}.
     *
     * @param parameterName The name of the parameter in session
     * @param jsonParameterName The name of the parameter in session's JSON representation
     */
    public UserAgentParameterHandler(String parameterName, JSONParameterName jsonParameterName) {
        super(parameterName, jsonParameterName);
    }

    @Override
    public String coerceTo(Object nonNullValue) {
        return compressUserAgentString(nonNullValue.toString());
    }

    @Override
    protected Object getParameterFrom(JSONObject jSession) {
        return decompressUserAgentString(jsonParameterName.getStringFrom(jSession));
    }

    // ------------------------------------------------ User-Agent helpers -----------------------------------------------------------------

    /**
     * A list of replacements
     * <p>
     * Tried to identify most common words in User-Agent strings according to: <a href="https://www.useragents.me/">https://www.useragents.me/</a>
     */
    private static final List<String[]> UA_REPLACEMENTS = List.of(
        new String[] {"like", "^l"},
        new String[] {"OS", "^o"},
        new String[] {"Mozilla", "^M"},
        new String[] {"Windows", "^W"},
        new String[] {"AppleWebKit", "^A"},
        new String[] {"Gecko", "^G"},
        new String[] {"Chrome", "^C"},
        new String[] {"Macintosh", "^T"},
        new String[] {"Win64", "^I"},
        new String[] {"Safari", "^S"},
        new String[] {"KHTML", "^H"},
        new String[] {"Firefox", "^F"},
        new String[] {"Android", "^D"},
        new String[] {"iPhone", "^P"},
        new String[] {"Linux", "^U"},
        new String[] {"Mobile", "^O"},
        new String[] {"Version", "^V"},
        new String[] {"Intel", "^N"},
        new String[] {"Mac", "^J"},
        new String[] {"SAMSUNG", "^Q"}
    );

    private static String compressUserAgentString(String userAgent) {
        if (userAgent == null) {
            return null;
        }
        String retval = userAgent;
        for (String[] replacement : UA_REPLACEMENTS) {
            retval = retval.replace(replacement[0], replacement[1]);
        }
        return retval;
    }

    private static String decompressUserAgentString(String compressedUserAgent) {
        if (compressedUserAgent == null) {
            return null;
        }
        String retval = compressedUserAgent;
        for (String[] replacement : UA_REPLACEMENTS) {
            retval = retval.replace(replacement[1], replacement[0]);
        }
        return retval;
    }

//    public static void main(String[] args) throws Exception {
//        String usa = "["
//            + "{\"ua\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.3\", \"pct\": 30.88}, {\"ua\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.6 Mobile/15E148 Safari/604.\", \"pct\": 15.96}"
//            + ", {\"ua\": \"Mozilla/5.0 (Linux; Android 10; K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Mobile Safari/537.3\", \"pct\": 52.82}, {\"ua\": \"Mozilla/5.0 (iPhone; CPU iPhone OS 17_2_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.2 Mobile/15E148 Safari/604.\", \"pct\": 17.61}"
//        + "]";
//        JSONArray ja = new JSONArray(usa);
//
//        HashMap<String, Integer> map = new HashMap<>();
//        for (Object object : ja) {
//            JSONObject jo = (JSONObject) object;
//            for (String word : jo.getString("ua").split("\\W")) {
//                if (word.isEmpty()) {
//                    continue;
//                }
//                Integer number;
//                try {
//                    number = Integer.valueOf(word);
//                } catch (NumberFormatException e) {
//                    number = null;
//                }
//                if (number != null) {
//                    continue;
//                }
//                if (map.containsKey(word)) {
//                    map.put(word, map.get(word) + 1);
//                } else {
//                    map.put(word, 1);
//                }
//            }
//        }
//        ja = null; // No more needed
//
//        TreeMap<Integer, String> tm = new TreeMap<>(new Comparator<Integer>() {
//
//            @Override
//            public int compare(Integer o1, Integer o2) {
//                return Integer.compareUnsigned(o2.intValue(), o1.intValue());
//            }
//        });
//        for (Entry<String, Integer> e : map.entrySet()) {
//            tm.put(e.getValue(), e.getKey());
//        }
//        map = null; // No more needed
//        for (Entry<Integer, String> e : tm.entrySet()) {
//            System.out.println(e.getValue() + " -> " + e.getKey());
//        }
//    }

}