/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cluster.serialization.session.utils;

import java.util.Optional;
import org.json.JSONObject;

/**
 * The enumeration for used parameter names in session's JSON representation.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public enum JSONParameterName {

    /** The version parameter */
    VERSION("v", "version"),
    /** The session identifier parameter */
    SESSION_ID("s", "sessionId"),
    /** The context identifier parameter */
    CONTEXT_ID("c", "contextId"),
    /** The user identifier parameter */
    USER_ID("u", "userId"),
    /** The login parameter */
    LOGIN("l", "login"),
    /** The password parameter */
    PASSWORD("p", "password"),
    /** The login name parameter */
    LOGIN_NAME("ln", "loginName"),
    /** The secret parameter */
    SECRET("st", "secret"),
    /** The random token parameter */
    RANDOM_TOKEN("rt", "randomToken"),
    /** The authentication identifier parameter */
    AUTH_ID("a", "authId"),
    /** The local IP address parameter */
    LOCAL_IP("ip", "localIp"),
    /** The hash parameter */
    HASH("h", "hash"),
    /** The client identifier parameter */
    CLIENT("cl", "client"),
    /** The user login parameter */
    USER_LOGIN("ul", "userLogin"),
    /** The origin parameter */
    ORIGIN("o", "origin"),
    /** The stay-signed-in flag parameter */
    STAY_SIGNED_IN("si", "staySignedIn"),
    /** The name for session's further parameters */
    PARAMETERS("ps", "parameters"),
    /** The alternative identifier parameter */
    ALTERNATIVE_ID("ai", "alternativeId"),
    /** The guest flag parameter */
    GUEST_FLAG("g", "guest"),
    /** The device user agent parameter */
    DEVICE_USER_AGENT("da", "deviceUserAgent"),
    /** The user agent parameter */
    USER_AGENT("ua", "userAgent"),
    /** The host name parameter */
    HOST_NAME("n", null),
    /** The login time parameter */
    LOGIN_TIME("lt", "loginTime"),
    /** The cookie refresh times tamp parameter */
    COOKIE_REFRESH_TIMESTAMP("cr", "cookieRefreshTimestamp"),
    /** The local last-active time stamp parameter */
    LOCAL_LAST_ACTIVE("la", "localLastActive"),
    /** The OAuth access token parameter */
    OAUTH_ACCESS_TOKEN("oa", "oauthAccessToken"),
    /** The OAuth refresh token parameter */
    OAUTH_REFRESH_TOKEN("or", "oauthRefreshToken"),
    /** The expiration date for OAuth access token parameter */
    OAUTH_ACCESS_TOKEN_EXPIRY_DATE("oe", "oauthAccessTokenExpiryDate"),
    /** The OAuth identifier parameter */
    OAUTH_ID_TOKEN("oi", "oauthIdToken"),
    /** The brand identifier parameter */
    BRAND("b", "brand"),
    /** The user's database schema */
    USER_SCHEMA("us", "userSchema"),
    /** The flag to indicate session established on behalf of the user */
    MASTER_AUTHENTICATED("ma", "masterAuthenticated"),
    ;

    /** The parameter's short name */
    private final String shortName;

    /** The parameter's long name */
    private final String optLongName;

    /**
     * Initializes a new {@link JSONParameterName}.
     *
     * @param shortName The parameter's short name
     * @param longName The parameter's long name or <code>null</code>
     */
    private JSONParameterName(String shortName, String optLongName) {
        this.shortName = shortName;
        this.optLongName = optLongName;
    }

    /**
     * Gets the short name.
     *
     * @return The short name
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Gets the optional long name.
     *
     * @return The long name or empty
     */
    public Optional<String> getLongName() {
        return Optional.ofNullable(optLongName);
    }

    /**
     * Gets the JSON object value for this parameter from given session's JSON representation.
     *
     * @param jSession The session's JSON representation to get from
     * @return The JSON object value or <code>null</code> if absent
     */
    public JSONObject getJSONObjectFrom(JSONObject jSession) {
        JSONObject jo = jSession.optJSONObject(shortName);
        return jo == null ? (optLongName == null ? jo : jSession.optJSONObject(optLongName)) : jo;
    }

    /**
     * Gets the string value for this parameter from given session's JSON representation.
     *
     * @param jSession The session's JSON representation to get from
     * @return The string value or <code>null</code> if absent
     */
    public String getStringFrom(JSONObject jSession) {
        String s = jSession.optString(shortName, null);
        return s == null ? (optLongName == null ? s : jSession.optString(optLongName, null)) : s;
    }

    /**
     * Gets the boolean value for this parameter from given session's JSON representation.
     *
     * @param jSession The session's JSON representation to get from
     * @return The boolean value or <code>false</code> if absent
     */
    public boolean getBoolFrom(JSONObject jSession) {
        boolean b = jSession.optBoolean(shortName, false);
        return b == false ? (optLongName == null ? b : jSession.optBoolean(optLongName, false)) : b;
    }

    /**
     * Gets the integer value for this parameter from given session's JSON representation.
     *
     * @param jSession The session's JSON representation to get from
     * @return The integer value or <code>0</code> if absent
     */
    public int getIntFrom(JSONObject jSession) {
        int i = jSession.optInt(shortName, 0);
        return i == 0 ? (optLongName == null ? i : jSession.optInt(optLongName, 0)) : i;
    }

    /**
     * Gets the long value for this parameter from given session's JSON representation.
     *
     * @param jSession The session's JSON representation to get from
     * @return The long value or <code>0</code> if absent
     */
    public long getLongFrom(JSONObject jSession) {
        long l = jSession.optLong(shortName, 0);
        return l == 0 ? (optLongName == null ? l : jSession.optLong(optLongName, 0)) : l;
    }
}
