/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cluster.serialization.session.utils;

import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.session.Session;

/**
 * A handler for a certain session parameter during serialization/deserialization of a session
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 * @param <T> The expected type of parameter's value in session
 */
public abstract class ParameterHandler<T> {

    /** The name of the parameter in session */
    protected final String parameterName;

    /** The name of the parameter in session's JSON representation */
    protected final JSONParameterName jsonParameterName;

    /**
     * Initializes a new {@link ParameterHandler}.
     *
     * @param parameterName The name of the parameter in session
     * @param jsonParameterName The name of the parameter in session's JSON representation
     */
    protected ParameterHandler(String parameterName, JSONParameterName jsonParameterName) {
        super();
        this.parameterName = parameterName;
        this.jsonParameterName = jsonParameterName;
    }

    /**
     * Gets the name of the parameter handled by this instance.
     *
     * @return The parameter name
     */
    public String getParameterName() {
        return parameterName;
    }

    /**
     * Gets the parameter name in session's JSON representation.
     *
     * @return The JSON parameter name
     */
    public JSONParameterName getJsonParameterName() {
        return jsonParameterName;
    }

    /**
     * Serializes the parameter into given session's JSON representation.
     *
     * @param session The session to grab parameter from
     * @param jSession The session's JSON representation to write to
     * @throws JSONException If writing to JSON fails
     */
    public void serializeParameter(Session session, JSONObject jSession) throws JSONException {
        Object optParameter = session.getParameter(getParameterName());
        if (optParameter != null) {
            jSession.putOpt(jsonParameterName.getShortName(), coerceTo(optParameter));
        }
    }

    /**
     * Deserializes the parameter from given JSON representation.
     *
     * @param jSession The session's JSON representation to read from
     * @param parameters The parameters to put into; May be <code>null</code>
     * @return The resulting parameters
     */
    public Map<String, Object> deserializeParameter(JSONObject jSession, Map<String, Object> parameters) {
        Object value = getParameterFrom(jSession);
        if (value == null) {
            return parameters;
        }

        Map<String, Object> params = parameters == null ? new LinkedHashMap<>() : parameters;
        params.put(getParameterName(), value);
        return params;
    }

    /**
     * Coerces specified parameter value to appropriate type.
     *
     * @param nonNullValue The parameter value to coerce (that is ensured to be not <code>null</code>)
     * @return The coerced parameter value
     */
    public abstract T coerceTo(Object nonNullValue);

    /**
     * Gets the parameter value from JSON representation
     *
     * @param jSession The session's JSON representation to read from
     * @return The parameter value or <code>null</code>
     */
    protected abstract Object getParameterFrom(JSONObject jSession);

}
