/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.provider.ical.utils;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import com.openexchange.chronos.provider.ical.exception.ICalProviderExceptionCodes;
import com.openexchange.chronos.provider.ical.httpclient.properties.ICalCalendarProviderProperties;
import com.openexchange.chronos.provider.ical.osgi.Services;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.configuration.ServerProperty;
import com.openexchange.exception.OXException;
import com.openexchange.java.InetAddresses;
import com.openexchange.java.Strings;

/**
 * {@link ICalProviderUtils}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v7.10.0
 */
public final class ICalProviderUtils {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ICalProviderUtils.class); // NOSONARLINT
    }

    /**
     * Initializes a new {@link ICalProviderUtils}.
     */
    private ICalProviderUtils() {
        super();
    }

    /**
     * Verifies specified feed URI for validity.
     * <ul>
     * <li>Follows the grammar in RFC 2396
     * <li>Contains scheme/protocol component
     * <li>Contains host component
     * <li>Contains ASCII-only characters (no surrogate pairs)
     * <li>Not blocked by configuration
     * <li>No local address
     * </ul>
     *
     * @param feedUrl The feed URI to check
     * @throws OXException If feed URI violates any of the above rules
     */
    public static void verifyURI(String feedUrl) throws OXException {
        if (Strings.isEmpty(feedUrl)) {
            throw ICalProviderExceptionCodes.MISSING_FEED_URI.create();
        }

        try {
            URI uri = new URI(feedUrl);
            check(uri);
            boolean denied = ICalCalendarProviderProperties.isDenied(uri);

            if (denied || !isValid(uri)) {
                throw ICalProviderExceptionCodes.FEED_URI_NOT_ALLOWED.create(feedUrl);
            }
        } catch (URISyntaxException e) {
            throw ICalProviderExceptionCodes.BAD_FEED_URI.create(e, feedUrl);
        }
    }

    private static void check(URI uri) throws OXException {
        if (Strings.isEmpty(uri.getScheme()) || Strings.isEmpty(uri.getHost()) || Strings.containsSurrogatePairs(uri.toString())) {
            throw ICalProviderExceptionCodes.BAD_FEED_URI.create(uri.toString());
        }
    }

    private static boolean isValid(URI uri) {
        boolean production = Services.getService(LeanConfigurationService.class).getBooleanProperty(ServerProperty.production);
        if (!production) {
            // Accept any URI
            return true;
        }

        try {
            InetAddress inetAddress = InetAddresses.forString(uri.getHost());
            if (inetAddress.isAnyLocalAddress() || inetAddress.isSiteLocalAddress() || inetAddress.isLoopbackAddress() || inetAddress.isLinkLocalAddress()) {
                LoggerHolder.LOG.debug("Given feed URL \"{}\" with destination IP {} appears not to be valid.", uri, inetAddress.getHostAddress());
                return false;
            }
        } catch (UnknownHostException e) {
            LoggerHolder.LOG.debug("Given feed URL \"{}\" appears not to be valid.", uri, e);
            return false;
        }
        return true;
    }
}
