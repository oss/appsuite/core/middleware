/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.provider.ical.httpclient.properties;

import static com.openexchange.chronos.provider.ical.httpclient.properties.ICalCalendarProviderProperties.CONNECTION_TIMEOUT;
import static com.openexchange.chronos.provider.ical.httpclient.properties.ICalCalendarProviderProperties.HARD_CONNECT_TIMEOUT;
import static com.openexchange.chronos.provider.ical.httpclient.properties.ICalCalendarProviderProperties.HARD_READ_TIMEOUT;
import static com.openexchange.chronos.provider.ical.httpclient.properties.ICalCalendarProviderProperties.MAX_CONNECTIONS;
import static com.openexchange.chronos.provider.ical.httpclient.properties.ICalCalendarProviderProperties.MAX_CONNECTIONS_PER_ROUTE;
import static com.openexchange.chronos.provider.ical.httpclient.properties.ICalCalendarProviderProperties.SOCKET_READ_TIMEOUT;
import static com.openexchange.java.Autoboxing.i;
import java.util.Optional;
import org.apache.http.ProtocolException;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.LoggerFactory;
import com.openexchange.config.DefaultInterests;
import com.openexchange.config.Interests;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.configuration.ServerProperty;
import com.openexchange.exception.OXException;
import com.openexchange.rest.client.httpclient.DefaultHttpClientConfigProvider;
import com.openexchange.rest.client.httpclient.HttpBasicConfig;
import com.openexchange.rest.client.httpclient.util.HostCheckingRoutePlanner;
import com.openexchange.server.ServiceLookup;
import com.openexchange.version.VersionService;

/**
 * {@link ICalCalendarHttpProperties}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @since v7.10.4
 */
public class ICalCalendarHttpProperties extends DefaultHttpClientConfigProvider {

    private final ServiceLookup serviceLookup;

    /**
     * Initializes a new {@link ICalCalendarHttpProperties}.
     *
     * @param serviceLookup The {@link ServiceLookup}
     */
    public ICalCalendarHttpProperties(ServiceLookup serviceLookup) {
        super("icalfeed", VersionService.NAME + "/", Optional.ofNullable(serviceLookup.getService(VersionService.class)));
        this.serviceLookup = serviceLookup;
    }

    @Override
    public Interests getAdditionalInterests() {
        return DefaultInterests.builder().propertiesOfInterest(ICalCalendarProviderProperties.PREFIX + "*").build();
    }

    @Override
    public HttpBasicConfig configureHttpBasicConfig(HttpBasicConfig config) {
        try {
            return getFromConfiguration(config);
        } catch (OXException e) {
            LoggerFactory.getLogger(ICalCalendarHttpProperties.class).warn("Unable to apply configuration for ICalFeed HTTP client", e);
        }
        config.setMaxTotalConnections(i(MAX_CONNECTIONS.getDefaultValue(Integer.class)));
        config.setMaxConnectionsPerRoute(i(MAX_CONNECTIONS_PER_ROUTE.getDefaultValue(Integer.class)));
        config.setConnectTimeout(i(CONNECTION_TIMEOUT.getDefaultValue(Integer.class)));
        config.setSocketReadTimeout(i(SOCKET_READ_TIMEOUT.getDefaultValue(Integer.class)));
        config.setHardConnectTimeout(i(HARD_CONNECT_TIMEOUT.getDefaultValue(Integer.class)));
        config.setHardReadTimeout(i(HARD_READ_TIMEOUT.getDefaultValue(Integer.class)));
        return config;
    }

    @Override
    public void modify(HttpClientBuilder builder) {
        super.modify(builder);
        boolean production = serviceLookup.getService(LeanConfigurationService.class).getBooleanProperty(ServerProperty.production);
        if (!production) {
            return;
        }
        builder.setRedirectStrategy(BlacklistAwareRedirectStrategy.getInstance());
        HostCheckingRoutePlanner.injectHostCheckingRoutePlanner((host, hostAddress) ->  {
            if (ICalCalendarProviderProperties.isBlacklisted(hostAddress)) {
                throw new ProtocolException("Invalid redirect URI: " + host.getHostName() + ". The URI is not allowed due to configuration.");
            }
        }, builder);
    }

    /**
     * Configures the {@link HttpBasicConfig} with values from configuration
     *
     * @param config The {@link HttpBasicConfig} to configure
     * @return the configured {@link HttpBasicConfig}
     * @throws OXException in case the {@link LeanConfigurationService} is missing
     */
    private HttpBasicConfig getFromConfiguration(HttpBasicConfig config) throws OXException {
        LeanConfigurationService configurationService = serviceLookup.getServiceSafe(LeanConfigurationService.class);

        config.setMaxTotalConnections(configurationService.getIntProperty(MAX_CONNECTIONS));
        config.setMaxConnectionsPerRoute(configurationService.getIntProperty(MAX_CONNECTIONS_PER_ROUTE));
        config.setConnectTimeout(configurationService.getIntProperty(CONNECTION_TIMEOUT));
        config.setSocketReadTimeout(configurationService.getIntProperty(SOCKET_READ_TIMEOUT));
        config.setHardConnectTimeout(configurationService.getIntProperty(HARD_CONNECT_TIMEOUT));
        config.setHardReadTimeout(configurationService.getIntProperty(HARD_READ_TIMEOUT));
        return config;
    }
}
