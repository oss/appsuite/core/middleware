/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.provider.ical.httpclient.properties;

import com.openexchange.config.ConfigurationService;
import com.openexchange.config.DefaultInterests;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadable;


/**
 * {@link ICalCalendarProviderReloadable}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v7.10.0
 */
public class ICalCalendarProviderReloadable implements Reloadable {

    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        ICalCalendarProviderProperties.reset();
    }

    private static final String[] PROPERTIES = new String[] {
        ICalCalendarProviderProperties.REFRESH_INTERVAL.getFQPropertyName(), // reloadable via LeanConfigurationService
        ICalCalendarProviderProperties.RETRY_AFTER_ERROR_INTERVAL.getFQPropertyName(), // reloadable via LeanConfigurationService
        ICalCalendarProviderProperties.MAX_FILE_SIZE.getFQPropertyName(), // reloadable via LeanConfigurationService

        ICalCalendarProviderProperties.CONNECTION_TIMEOUT.getFQPropertyName(),
        ICalCalendarProviderProperties.MAX_CONNECTIONS.getFQPropertyName(),
        ICalCalendarProviderProperties.MAX_CONNECTIONS_PER_ROUTE.getFQPropertyName(),
        ICalCalendarProviderProperties.SOCKET_READ_TIMEOUT.getFQPropertyName(),
        
        ICalCalendarProviderProperties.BLACKLISTED_HOSTS.getFQPropertyName(),
        ICalCalendarProviderProperties.SCHEMES.getFQPropertyName(),
    };

    @Override
    public Interests getInterests() {
        return DefaultInterests.builder().propertiesOfInterest(PROPERTIES).build();
    }
}
