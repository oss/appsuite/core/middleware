/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.impl.osgi;

import static com.openexchange.java.Autoboxing.I;
import java.util.Dictionary;
import java.util.Hashtable;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import com.openexchange.capabilities.CapabilityChecker;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.capabilities.FailureAwareCapabilityChecker;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadable;
import com.openexchange.config.Reloadables;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.mail.api.MailConfig.PasswordSource;
import com.openexchange.mail.compose.CompositionSpaceServiceFactoryRegistry;
import com.openexchange.mail.config.MailProperties;
import com.openexchange.mail.scheduled.ScheduledMailProperty;
import com.openexchange.mail.scheduled.ScheduledMailService;
import com.openexchange.mail.scheduled.ScheduledMailStorageService;
import com.openexchange.mail.scheduled.impl.DueScheduledMailsChecker;
import com.openexchange.mail.scheduled.impl.ScheduledMailServiceImpl;
import com.openexchange.mail.service.MailService;
import com.openexchange.mailaccount.MailAccountStorageService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.startup.SignalStartedService;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.timer.TimerService;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link ScheduledMailServiceActivator} - Activator for <i>com.openexchange.mail.scheduled.impl</i> bundle.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ScheduledMailServiceActivator extends HousekeepingActivator implements Reloadable {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ScheduledMailServiceActivator.class);

    private ScheduledMailServiceImpl service;

    /**
     * Initializes a new {@link ScheduledMailServiceActivator}.
     */
    public ScheduledMailServiceActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { ScheduledMailStorageService.class, MailAccountStorageService.class, UserService.class, MailService.class,
            LeanConfigurationService.class, CompositionSpaceServiceFactoryRegistry.class, CapabilityService.class, TimerService.class,
            ThreadPoolService.class };
    }

    @Override
    protected synchronized void startBundle() throws Exception {
        ScheduledMailServiceImpl service = new ScheduledMailServiceImpl(this);
        this.service = service;

        boolean error = true;
        try {
            // Announce scheduled mail available
            {
                final ServiceLookup services = this;
                final String sCapability = ScheduledMailService.CAPABILITY_SCHEDULED_MAIL;
                Dictionary<String, Object> properties = new Hashtable<>(1);
                properties.put(CapabilityChecker.PROPERTY_CAPABILITIES, sCapability);
                registerService(CapabilityChecker.class, new FailureAwareCapabilityChecker() {

                    @Override
                    public FailureAwareCapabilityChecker.Result checkEnabled(String capability, Session session) {
                        if (sCapability.equals(capability)) {
                            if (session == null || session.getContextId() <= 0 || session.getUserId() <= 0) {
                                return FailureAwareCapabilityChecker.Result.DISABLED;
                            }

                            try {
                                if (PasswordSource.GLOBAL != MailProperties.getInstance().getPasswordSource(session.getUserId(), session.getContextId())) {
                                    // Master authentication is not enabled for session-associated user
                                    return FailureAwareCapabilityChecker.Result.DISABLED;
                                }

                                boolean enabled = service.isAvailable(session);
                                if (!enabled) {
                                    return FailureAwareCapabilityChecker.Result.DISABLED;
                                }

                                User user = getUserFor(session, services);
                                if (user.isAnonymousGuest() || user.isGuest()) {
                                    return FailureAwareCapabilityChecker.Result.DISABLED;
                                }
                            } catch (Exception e) {
                                LOG.warn("Failed to check if scheduled mail is enabled for user {} in context {}", I(session.getUserId()), I(session.getContextId()), e);
                                return FailureAwareCapabilityChecker.Result.FAILURE;
                            }
                        }

                        return FailureAwareCapabilityChecker.Result.ENABLED;
                    }

                    private User getUserFor(Session session, ServiceLookup services) throws OXException {
                        if (session instanceof ServerSession) {
                            return ((ServerSession) session).getUser();
                        }
                        return services.getServiceSafe(UserService.class).getUser(session.getUserId(), session.getContextId());
                    }

                }, properties);
                getService(CapabilityService.class).declareCapability(sCapability);
            }

            // Start timer task when start-up is through
            ServiceLookup services = this;
            BundleContext context = this.context;
            ServiceTrackerCustomizer<SignalStartedService, SignalStartedService> startUpTracker = new ServiceTrackerCustomizer<SignalStartedService, SignalStartedService>() {

                @Override
                public SignalStartedService addingService(ServiceReference<SignalStartedService> reference) {
                    SignalStartedService signalStartedService = context.getService(reference);
                    try {
                        DueScheduledMailsChecker.reinitTimerTask(service, services);
                        SignalStartedService retval = signalStartedService;
                        signalStartedService = null;
                        return retval;
                    } catch (Exception e) {
                        LOG.error("Failed to start checker for due scheduled mails", e);
                    } finally {
                        if (signalStartedService != null) {
                            context.ungetService(reference);
                        }
                    }
                    return null;
                }

                @Override
                public void modifiedService(ServiceReference<SignalStartedService> reference, SignalStartedService service) {
                    // Nothing
                }

                @Override
                public void removedService(ServiceReference<SignalStartedService> reference, SignalStartedService service) {
                    DueScheduledMailsChecker.dropTimerTask(services);
                    context.ungetService(reference);
                }
            };
            track(SignalStartedService.class, startUpTracker);
            openTrackers();

            // Register scheduled mail service
            registerService(ScheduledMailService.class, service);

            // Register reloadable
            registerService(Reloadable.class, this);

            // All fine
            error = false;
        } finally {
            if (error) {
                service.shutDown();
                this.service = null;
            }
        }

    }

    @Override
    protected synchronized void stopBundle() throws Exception {
        ScheduledMailServiceImpl service = this.service;
        if (service != null) {
            this.service = null;
            service.shutDown();
        }
        super.stopBundle();
    }

    // -------------------------------------------------------- Reloadable stuff -----------------------------------------------------------

    @Override
    public Interests getInterests() {
        return Reloadables.interestsForProperties(
            ScheduledMailProperty.CHECK_FREQUENCY_MINUTES.getFQPropertyName(),
            ScheduledMailProperty.LOOK_AHEAD_MINUTES.getFQPropertyName()
        );
    }

    @Override
    public synchronized void reloadConfiguration(ConfigurationService configService) {
        // Re-start timer task
        ScheduledMailServiceImpl service = this.service;
        if (service != null) {
            try {
                DueScheduledMailsChecker.reinitTimerTask(service, this);
            } catch (Exception e) {
                LOG.warn("Failed to re-start timer task that checks fur due scheduled mails & triggers their transport");
            }
        }
    }

}
