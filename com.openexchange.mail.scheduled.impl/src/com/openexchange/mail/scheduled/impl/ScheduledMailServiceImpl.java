/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.impl;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.log.LogUtility.toStringObjectFor;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.LockSupport;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Functions;
import com.openexchange.java.ISO8601Utils;
import com.openexchange.java.Predicates;
import com.openexchange.java.Strings;
import com.openexchange.java.util.UUIDs;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailPath;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.mail.api.MailConfig;
import com.openexchange.mail.api.MailConfig.PasswordSource;
import com.openexchange.mail.compose.ClientToken;
import com.openexchange.mail.compose.CompositionSpaceService;
import com.openexchange.mail.compose.CompositionSpaceServiceFactory;
import com.openexchange.mail.compose.CompositionSpaceServiceFactoryRegistry;
import com.openexchange.mail.compose.CompositionSpaces;
import com.openexchange.mail.compose.DeleteAfterTransportOptions;
import com.openexchange.mail.compose.HeaderUtility;
import com.openexchange.mail.config.MailConfigException;
import com.openexchange.mail.config.MailProperties;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.scheduled.KnownMetaName;
import com.openexchange.mail.scheduled.LockedScheduledMail;
import com.openexchange.mail.scheduled.LockedScheduledMails;
import com.openexchange.mail.scheduled.Meta;
import com.openexchange.mail.scheduled.ScheduledMail;
import com.openexchange.mail.scheduled.ScheduledMailDescription;
import com.openexchange.mail.scheduled.ScheduledMailExceptionCodes;
import com.openexchange.mail.scheduled.ScheduledMailProperty;
import com.openexchange.mail.scheduled.ScheduledMailService;
import com.openexchange.mail.scheduled.ScheduledMailStorageService;
import com.openexchange.mail.scheduled.Schema;
import com.openexchange.mail.service.MailService;
import com.openexchange.mailaccount.Account;
import com.openexchange.mailaccount.MailAccount;
import com.openexchange.mailaccount.MailAccountStorageService;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.GeneratedSession;
import com.openexchange.session.IUserAndContext;
import com.openexchange.session.Session;
import com.openexchange.session.UserAndContext;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.threadpool.ThreadPools;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tools.session.ServerSessionAdapter;
import com.openexchange.user.UserService;

/**
 * {@link ScheduledMailServiceImpl} - The implementation of the scheduled mail service.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ScheduledMailServiceImpl implements ScheduledMailService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ScheduledMailServiceImpl.class);

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final ServiceLookup services;
    private final Map<ScheduledTimerTaskKey, TimerTaskAndLockedMail> scheduledTasks;

    /**
     * Initializes a new {@link ScheduledMailServiceImpl}.
     *
     * @param services The service look-up
     */
    public ScheduledMailServiceImpl(ServiceLookup services) {
        super();
        this.services = services;
        this.scheduledTasks = new ConcurrentHashMap<>();
    }

    /**
     * Shuts-down this instance.
     */
    public void shutDown() {
        for (Iterator<Entry<ScheduledTimerTaskKey, TimerTaskAndLockedMail>> it = scheduledTasks.entrySet().iterator(); it.hasNext();) {
            Map.Entry<ScheduledTimerTaskKey, TimerTaskAndLockedMail> timerTaskAndLockedMailEntry = it.next();
            try {
                TimerTaskAndLockedMail timerTaskAndLockedMail = timerTaskAndLockedMailEntry.getValue();
                timerTaskAndLockedMail.timerTask.cancel();
                LockedScheduledMails.unlock(timerTaskAndLockedMail.lockedScheduledMail);
            } finally {
                it.remove();
            }
        }
    }

    @Override
    public boolean isAvailable(Session session) throws OXException {
        if (!services.getServiceSafe(LeanConfigurationService.class).getBooleanProperty(session.getUserId(), session.getContextId(), ScheduledMailProperty.ENABLED)) {
            LOG.debug("Scheduled mail functionality not enabled per configuration");
            return false;
        }
        return true;
    }

    @Override
    public ScheduledMail scheduleTransportFor(UUID optionalId, ScheduledMailDescription scheduledMailDesc, Session session) throws OXException {
        // Acquire needed services
        ScheduledMailStorageService storageService = services.getServiceSafe(ScheduledMailStorageService.class);
        LeanConfigurationService leanConfigService = services.getServiceSafe(LeanConfigurationService.class);

        // Perform create
        int maxNumberOfScheduledMails = leanConfigService.getIntProperty(session.getUserId(), session.getContextId(), ScheduledMailProperty.MAX_NUMBER_SCHEDULED_MAILS);
        int maxNumberOfScheduledMailsPerHour = leanConfigService.getIntProperty(session.getUserId(), session.getContextId(), ScheduledMailProperty.MAX_NUMBER_SCHEDULED_MAILS_PER_HOUR);
        ScheduledMail createdScheduledMail = storageService.createScheduledMail(optionalId, scheduledMailDesc, maxNumberOfScheduledMails, maxNumberOfScheduledMailsPerHour, session);

        long frequencyMillis = Duration.ofMinutes(leanConfigService.getIntProperty(ScheduledMailProperty.CHECK_FREQUENCY_MINUTES)).toMillis();
        if (System.currentTimeMillis() + frequencyMillis <= createdScheduledMail.getDateToSend()) {
            // Let actual transport happen through DueScheduledMailsChecker
            return createdScheduledMail;
        }

        // Might be delayed too much. Schedule now...
        Optional<LockedScheduledMail> optLockedScheduledMail = storageService.acquireProcessingLock(createdScheduledMail.getId(), session);
        if (optLockedScheduledMail.isEmpty()) {
            // Already in processing
            return createdScheduledMail;
        }

        LockedScheduledMail lockedScheduledMail = optLockedScheduledMail.get();
        try {
            NeededServices neededServices = new NeededServices(storageService, services);

            String masterPassword = requireMasterPasswordIfNeeded(lockedScheduledMail);
            schedule(lockedScheduledMail, masterPassword, neededServices);

            lockedScheduledMail = null; // Avoid premature unlocking
            return createdScheduledMail;
        } finally {
            if (lockedScheduledMail != null) {
                // Something went wrong
                LockedScheduledMails.unlock(lockedScheduledMail);
            }
        }
    }

    @Override
    public ScheduledMail updateScheduledMail(UUID id, ScheduledMailDescription scheduledMailDesc, Session session) throws OXException {
        // Acquire needed services
        ScheduledMailStorageService storageService = services.getServiceSafe(ScheduledMailStorageService.class);
        LeanConfigurationService leanConfigService = services.getServiceSafe(LeanConfigurationService.class);

        // Perform update
        int maxNumberOfScheduledMailsPerHour = leanConfigService.getIntProperty(session.getUserId(), session.getContextId(), ScheduledMailProperty.MAX_NUMBER_SCHEDULED_MAILS_PER_HOUR);
        return storageService.updateScheduledMail(id, scheduledMailDesc, maxNumberOfScheduledMailsPerHour, session);
    }

    @Override
    public Optional<ScheduledMail> deleteScheduledMail(UUID id, boolean deleteAssociatedMail, Session session) throws OXException {
        Optional<ScheduledMail> optDeletedScheduledMail = services.getServiceSafe(ScheduledMailStorageService.class).deleteScheduledMail(id, session);
        if (optDeletedScheduledMail.isPresent()) {
            ScheduledMail deletedScheduledMail = optDeletedScheduledMail.get();
            TimerTaskAndLockedMail timerTaskAndLockedMail = scheduledTasks.remove(new ScheduledTimerTaskKey(deletedScheduledMail));
            if (timerTaskAndLockedMail != null) {
                timerTaskAndLockedMail.timerTask.cancel();
                LockedScheduledMails.unlock(timerTaskAndLockedMail.lockedScheduledMail);
            }
            if (deleteAssociatedMail) {
                deleteAssociatedMailSafe(session, deletedScheduledMail);
            }
        }
        return optDeletedScheduledMail;
    }

    private void deleteAssociatedMailSafe(Session session, ScheduledMail scheduledMail) {
        try {
            MailPath mailPath = MailPath.getMailPathFor(scheduledMail.getMailPath());
            if (mailPath == null) {
                return;
            }

            MailService mailService = services.getOptionalService(MailService.class);
            MailAccess<? extends IMailFolderStorage,? extends IMailMessageStorage> mailAccess = null;
            try {
                mailAccess = mailService.getMailAccess(session, mailPath.getAccountId());
                mailAccess.connect(false);

                mailAccess.getMessageStorage().deleteMessages(mailPath.getFolder(), new String[] { mailPath.getMailID() }, true);
            } finally {
                if (mailAccess != null) {
                    mailAccess.close(true);
                }
            }
        } catch (Exception e) {
            LOG.warn("Failed to delete the mail {} associated with scheduled mail {}", scheduledMail.getMailPath(), UUIDs.getUnformattedString(scheduledMail.getId()), e);
        }
    }

    @Override
    public ScheduledMail[] deleteScheduledMails(Collection<UUID> ids, boolean deleteAssociatedMails, Session session) throws OXException {
        if (ids == null || ids.isEmpty()) {
            return new ScheduledMail[0];
        }

        ScheduledMail[] deletedScheduledMails = services.getServiceSafe(ScheduledMailStorageService.class).deleteScheduledMails(ids, session);
        if (deletedScheduledMails != null && deletedScheduledMails.length > 0) {
            for (ScheduledMail deletedScheduledMail : deletedScheduledMails) {
                if (deletedScheduledMail != null) {
                    TimerTaskAndLockedMail timerTaskAndLockedMail = scheduledTasks.remove(new ScheduledTimerTaskKey(deletedScheduledMail));
                    if (timerTaskAndLockedMail != null) {
                        timerTaskAndLockedMail.timerTask.cancel();
                        LockedScheduledMails.unlock(timerTaskAndLockedMail.lockedScheduledMail);
                    }
                }
            }
            if (deleteAssociatedMails) {
                deleteAssociatedMailsSafe(session, deletedScheduledMails);
            }
        }
        return deletedScheduledMails;
    }

    private void deleteAssociatedMailsSafe(Session session, ScheduledMail[] deletedScheduledMails) {
        try {
            MailService mailService = services.getOptionalService(MailService.class);

            List<MailPath> mailPaths = Arrays.stream(deletedScheduledMails)
                .filter(Predicates.isNotNullPredicate())
                .map(sm -> {
                    try {
                        return MailPath.getMailPathFor(sm.getMailPath());
                    } catch (Exception e) {
                        throw new IllegalStateException("Invalid mail path", e);
                    }
                })
                .collect(CollectorUtils.toList(deletedScheduledMails.length)); // NOSONARLINT

            // Group by account
            Map<Integer, List<MailPath>> byAccount = new LinkedHashMap<>();
            for (MailPath mp : mailPaths) {
                byAccount.computeIfAbsent(I(mp.getAccountId()), Functions.getNewLinkedListFuntion()).add(mp);
            }

            // Iterate account-wise
            for (Map.Entry<Integer, List<MailPath>> e : byAccount.entrySet()) {
                // Account identifier
                int accountId = e.getKey().intValue();

                // Group by folder
                Map<String, List<String>> byFolder = new LinkedHashMap<>();
                for (MailPath mp : e.getValue()) {
                    byFolder.computeIfAbsent(mp.getFolder(), Functions.getNewLinkedListFuntion()).add(mp.getMailID());
                }

                // Iterate folder-wise
                for (Map.Entry<String, List<String>> fe : byFolder.entrySet()) {
                    String folder = fe.getKey();
                    List<String> mailIds = fe.getValue();

                    MailAccess<? extends IMailFolderStorage,? extends IMailMessageStorage> mailAccess = null;
                    try {
                        mailAccess = mailService.getMailAccess(session, accountId);
                        mailAccess.connect(false);

                        mailAccess.getMessageStorage().deleteMessages(folder, mailIds.toArray(new String[mailIds.size()]), true);
                    } finally {
                        if (mailAccess != null) {
                            mailAccess.close(true);
                        }
                    }
                }
            }
        } catch (Exception e) {
            // Fall-back handling
            for (ScheduledMail scheduledMail : deletedScheduledMails) {
                if (scheduledMail != null) {
                    deleteAssociatedMailSafe(session, scheduledMail);
                }
            }
        }
    }

    @Override
    public boolean existsScheduledMail(UUID id, Session session) throws OXException {
        return services.getServiceSafe(ScheduledMailStorageService.class).existsScheduledMail(id, session);
    }

    @Override
    public Optional<ScheduledMail> optScheduledMail(UUID id, Session session) throws OXException {
        return services.getServiceSafe(ScheduledMailStorageService.class).optScheduledMail(id, session);
    }

    @Override
    public ScheduledMail getScheduledMail(UUID id, Session session) throws OXException {
        return services.getServiceSafe(ScheduledMailStorageService.class).getScheduledMail(id, session);
    }

    @Override
    public List<ScheduledMail> getScheduledMails(Session session) throws OXException {
        return services.getServiceSafe(ScheduledMailStorageService.class).getScheduledMails(session);
    }

    /**
     * Schedules the transport for such scheduled mails that are due for being sent according to given due date.
     *
     * @param dueDate The due time stamp to consider; the number of milliseconds since January 1, 1970, 00:00:00 GMT representing the due date
     * @throws OXException If operation fails
     */
    public void scheduleTransportForDueScheduledMails(long dueDate) throws OXException {
        // Obtain necessary service for look-up
        ScheduledMailStorageService scheduledMailStorageService = services.getServiceSafe(ScheduledMailStorageService.class);

        // Determine due scheduled mails for which transport should be scheduled
        Map<Schema, Map<UserAndContext, List<LockedScheduledMail>>> dueScheduledMailsBySchema = scheduledMailStorageService.getDueScheduledMails(dueDate);
        if (dueScheduledMailsBySchema.isEmpty()) {
            LOG.info("Detected no scheduled mails that are due for being sent. Aborting...");
            return;
        }

        // Schedule transport for due scheduled mails
        try {
            LOG.info("Detected scheduled mails that are due for being sent. Processing...");
            NeededServices neededServices = new NeededServices(scheduledMailStorageService, services);
            for (Map<UserAndContext, List<LockedScheduledMail>> lockedMailsOfSchema : dueScheduledMailsBySchema.values()) {
                for (List<LockedScheduledMail> lockedMailsOfUser : lockedMailsOfSchema.values()) {
                    String masterPassword = requireMasterPasswordIfNeeded(lockedMailsOfUser.get(0));
                    for (Iterator<LockedScheduledMail> it = lockedMailsOfUser.iterator(); it.hasNext();) {
                        LockedScheduledMail lockedScheduledMail = it.next();
                        // Check if interrupted
                        if (Thread.interrupted()) {
                            return;
                        }

                        // Schedule transport
                        schedule(lockedScheduledMail, masterPassword, neededServices);

                        // Remove processed locked scheduled mail
                        it.remove();
                    }
                }
            }
            dueScheduledMailsBySchema = null;
        } finally {
            if (dueScheduledMailsBySchema != null) {
                // Something went wrong
                dueScheduledMailsBySchema.values().forEach(dueMailsOfSchema -> dueMailsOfSchema.values().forEach(LockedScheduledMails::unlock));
            }
        }
    }

    private static final long NANOS_1SECOND = 1_000_000_000L;

    private void schedule(LockedScheduledMail lockedScheduledMail, String masterPassword, NeededServices neededServices) {
        long delayMillis = lockedScheduledMail.getDateToSend() - System.currentTimeMillis();
        boolean scheduleAsTimerTask = delayMillis > 0;

        Runnable deliveryTask = new DeliveryRunnable(lockedScheduledMail, masterPassword, scheduleAsTimerTask ? scheduledTasks : null, neededServices);

        if (scheduleAsTimerTask) {
            // Schedule transport for a later time through a timer task
            ScheduledTimerTask timerTask = neededServices.timerService.schedule(deliveryTask, delayMillis);
            scheduledTasks.put(new ScheduledTimerTaskKey(lockedScheduledMail), new TimerTaskAndLockedMail(timerTask, lockedScheduledMail));
            LOG.info("Scheduled transport of scheduled mail {} ({}) for user {} in context {} at {}", CompositionSpaces.getUUIDForLogging(lockedScheduledMail.getId()), lockedScheduledMail.getMailPath(), I(lockedScheduledMail.getUserId()), I(lockedScheduledMail.getContextId()), toStringObjectFor(() -> ISO8601Utils.format(new Date(lockedScheduledMail.getDateToSend()))));
        } else {
            // Immediate transport
            LOG.info("Initiate immediate transport of scheduled mail {} ({}) for user {} in context {}", CompositionSpaces.getUUIDForLogging(lockedScheduledMail.getId()), lockedScheduledMail.getMailPath(), I(lockedScheduledMail.getUserId()), I(lockedScheduledMail.getContextId()));
            neededServices.threadPool.submit(ThreadPools.task(deliveryTask));
        }
    }

    private static String requireMasterPasswordIfNeeded(IUserAndContext userAndContext) throws OXException {
        if (MailProperties.getInstance().getPasswordSource(userAndContext.getUserId(), userAndContext.getContextId()) != PasswordSource.GLOBAL) {
            return null;
        }

        String masterPassword = MailProperties.getInstance().getMasterPassword(userAndContext.getUserId(), userAndContext.getContextId());
        if (masterPassword == null) {
            throw MailConfigException.create("Property \"com.openexchange.mail.masterPassword\" not set");
        }
        return masterPassword;
    }



    // -------------------------------------------------------------------------------------------------------------------------------------

    private static class DeliveryRunnable implements Runnable {

        private final Map<ScheduledTimerTaskKey, TimerTaskAndLockedMail> optScheduledTasks;
        private final String masterPassword;
        private final NeededServices neededServices;
        private final LockedScheduledMail lockedScheduledMail;

        /**
         * Initializes a new {@link DeliveryRunnable}.
         *
         * @param lockedScheduledMail The locked scheduled mail to deliver
         * @param masterPassword The master password or <code>null</code>
         * @param optScheduledTasks The scheduled task collection or <code>null</code>
         * @param neededServices The wrapper for needed services
         */
        DeliveryRunnable(LockedScheduledMail lockedScheduledMail, String masterPassword, Map<ScheduledTimerTaskKey, TimerTaskAndLockedMail> optScheduledTasks, NeededServices neededServices) {
            super();
            this.lockedScheduledMail = lockedScheduledMail;
            this.masterPassword = masterPassword;
            this.optScheduledTasks = optScheduledTasks; // NOSONARLINT
            this.neededServices = neededServices;
        }

        @Override
        public void run() {
            if (optScheduledTasks == null) {
                // No need to update `scheduledTasks`
                transportAndUnlock(lockedScheduledMail);
                return;
            }

            ScheduledTimerTaskKey key = new ScheduledTimerTaskKey(lockedScheduledMail);
            try {
                while (!optScheduledTasks.containsKey(key)) {
                    // Await appearance
                    LockSupport.parkNanos(NANOS_1SECOND);
                }

                transportAndUnlock(lockedScheduledMail);
            } finally {
                optScheduledTasks.remove(key);
            }
        }

        private void transportAndUnlock(LockedScheduledMail lockedScheduledMail) {
            try {
                transport(lockedScheduledMail);
            } catch (Exception e) {
                LOG.error("Failed to transport scheduled mail {} ({}) for user {} in context {}", CompositionSpaces.getUUIDForLogging(lockedScheduledMail.getId()), lockedScheduledMail.getMailPath(), I(lockedScheduledMail.getUserId()), I(lockedScheduledMail.getContextId()), e);
            } finally {
                LockedScheduledMails.unlock(lockedScheduledMail);
            }
        }

        private void transport(LockedScheduledMail lockedScheduledMail) throws OXException {
            // Initiate transport
            List<LockedScheduledMail> dueMailsOfUser = Collections.singletonList(lockedScheduledMail);
            transportDueMailOfUser(dueMailsOfUser, lockedScheduledMail.getUserId(), lockedScheduledMail.getContextId(), masterPassword, neededServices);
        }

        private static void transportDueMailOfUser(List<LockedScheduledMail> dueMails, int userId, int contextId, String masterPassword, NeededServices neededServices) throws OXException {
            List<MailPathKnowingLockedScheduledMail> lockedScheduledMails = new ArrayList<>(dueMails.size());
            for (LockedScheduledMail lockedScheduledMail : dueMails) {
                MailPath mailPath = new MailPath(lockedScheduledMail.getMailPath());
                if (mailPath.getAccountId() != Account.DEFAULT_ID) {
                    throw ScheduledMailExceptionCodes.ONLY_PRIMARY_ACCOUNT.create();
                }
                lockedScheduledMails.add(new MailPathKnowingLockedScheduledMail(lockedScheduledMail, mailPath));
            }

            MailAccount account = neededServices.mailAccountStorageService.getDefaultMailAccount(userId, contextId);
            String login = MailConfig.getLogin(account, neededServices.userService.getUser(userId, contextId).getLoginInfo(), userId, contextId, true);

            Set<UUID> idsToDelete = HashSet.newHashSet(dueMails.size());
            try {
                for (MailPathKnowingLockedScheduledMail lockedScheduledMail : lockedScheduledMails) {
                    // Generate session
                    GeneratedSession session = generateSessionFor(userId, contextId, login, masterPassword, lockedScheduledMail);

                    // Transport scheduled mail
                    transportLockedScheduledMail(lockedScheduledMail, session, idsToDelete, neededServices.csFactoryRegistry, neededServices.mailService);
                }
            } finally {
                if (!idsToDelete.isEmpty()) {
                    try {
                        neededServices.scheduledMailStorageService.deleteScheduledMails(idsToDelete, contextId);
                    } catch (Exception e) {
                        LOG.warn("Failed to delete scheduled mails {}", idsToDelete, e);
                    }
                }
            }
        }

        private static final String CS_SERICE_ID = "draft";

        private static void transportLockedScheduledMail(MailPathKnowingLockedScheduledMail lockedScheduledMail, GeneratedSession session, Set<UUID> idsToDelete, CompositionSpaceServiceFactoryRegistry csFactoryRegistry, MailService mailService) throws OXException {
            LOG.info("Handling due scheduled mail {} ({}) for being sent for user {} in context {}", CompositionSpaces.getUUIDForLogging(lockedScheduledMail.getId()), lockedScheduledMail.getMailPath(), I(session.getUserId()), I(session.getContextId()));

            Map<Integer, MailAccess<? extends IMailFolderStorage,? extends IMailMessageStorage>> mailAccesses = HashMap.newHashMap(2);
            try {
                MailPath mailPath = lockedScheduledMail.getKnownMailPath();

                MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> defaultMailAccess = mailService.getMailAccess(session, mailPath.getAccountId());
                mailAccesses.put(I(Account.DEFAULT_ID), defaultMailAccess);
                defaultMailAccess.connect();

                MailMessage mailMessage = defaultMailAccess.getMessageStorage().getMessage(mailPath.getFolder(), mailPath.getMailID(), false);
                if (mailMessage == null) {
                    // No such mail in mail storage
                    LOG.warn("No such mail in mail storage for user {} in context {}: {}", I(session.getUserId()), I(session.getContextId()), mailPath.getStr());
                    idsToDelete.add(lockedScheduledMail.getId());
                    return;
                }

                // Appropriate MIME message found in mail storage
                MailPath toDelete = new MailPath(Account.DEFAULT_ID, mailMessage.getFolder(), mailMessage.getMailId());
                try {
                    String sCompositionSpaceId = mailMessage.getFirstHeader(HeaderUtility.HEADER_X_OX_COMPOSITION_SPACE_ID);
                    UUID compositionSpaceId = CompositionSpaces.parseCompositionSpaceIdIfValid(sCompositionSpaceId);
                    if (compositionSpaceId == null) {
                        LOG.warn("Missing or invalid \"{}\" header in mail {} for user {} in context {}: {}", HeaderUtility.HEADER_X_OX_COMPOSITION_SPACE_ID, mailPath.getStr(), I(session.getUserId()), I(session.getContextId()), sCompositionSpaceId == null ? "null" : sCompositionSpaceId);
                        idsToDelete.add(lockedScheduledMail.getId());
                        return;
                    }

                    // Determine adequate composition space service
                    CompositionSpaceServiceFactory factoryToUse = null;
                    try {
                        factoryToUse = csFactoryRegistry.getFactoryFor(CS_SERICE_ID, session);
                    } catch (OXException e) {
                        LOG.warn("Failed look-up of composition space service by identifier \"{}\" for user {} in context {}.", CS_SERICE_ID, I(session.getUserId()), I(session.getContextId()), e);
                    }
                    if (factoryToUse == null) {
                        LOG.warn("No composition space service available that supports scheduled mails for user {} in context {}. Aborting transport for: {}", I(session.getUserId()), I(session.getContextId()), mailPath.getStr());
                        idsToDelete.add(lockedScheduledMail.getId());
                        return;
                    }

                    // Appropriate factory found
                    try {
                        // Move to standard drafts folder for being transported
                        String draftsFolder = defaultMailAccess.getFolderStorage().getDraftsFolder();
                        String[] movedMessages = defaultMailAccess.getMessageStorage().moveMessages(mailPath.getFolder(), draftsFolder, new String[] { mailPath.getMailID() }, true);
                        if (movedMessages == null || movedMessages.length <= 0) {
                            toDelete = null;
                        } else {
                            toDelete = new MailPath(Account.DEFAULT_ID, draftsFolder, movedMessages[0]);
                        }

                        // Transport using composition space service
                        ServerSession serverSession = ServerSessionAdapter.valueOf(session);
                        CompositionSpaceService compositionSpaceService = factoryToUse.createServiceFor(serverSession);
                        ClientToken clientToken = ClientToken.NONE;
                        DeleteAfterTransportOptions options = DeleteAfterTransportOptions.builder().withDeleteAfterTransport(true).withDeleteDraftAfterTransport(true).build();
                        List<OXException> warnings = new ArrayList<OXException>(4);
                        AJAXRequestData request = new AJAXRequestData();
                        request.setSession(serverSession);
                        Optional<Meta> optMeta = lockedScheduledMail.getMeta();
                        if (optMeta.isPresent()) {
                            Meta meta = optMeta.get();
                            Object oParams = meta.get(KnownMetaName.PARAMETERS.getName());
                            if (oParams instanceof Meta mParams) {
                                for (Map.Entry<String, Object> e : mParams.entrySet()) {
                                    request.putParameter(e.getKey(), e.getValue().toString());
                                }
                            }
                            String remoteAddress = meta.getString(KnownMetaName.REMOTE_ADDRESS.getName(), null);
                            if (remoteAddress != null) {
                                request.setRemoteAddress(remoteAddress);
                            }
                        }
                        MailPath newMailPath = compositionSpaceService.transportCompositionSpace(compositionSpaceId, Optional.empty(), true, serverSession.getUserSettingMail(), request, warnings, options, clientToken);
                        toDelete = null;
                        if (newMailPath == null) {
                            LOG.info("Scheduled mail {} ({}) successfully transported for user {} in context {}", CompositionSpaces.getUUIDForLogging(lockedScheduledMail.getId()), mailPath, I(session.getUserId()), I(session.getContextId()));
                        } else {
                            LOG.info("Scheduled mail {} ({}) successfully transported for user {} in context {}. Sent mail: {}", CompositionSpaces.getUUIDForLogging(lockedScheduledMail.getId()), mailPath, I(session.getUserId()), I(session.getContextId()), newMailPath);
                        }
                        idsToDelete.add(lockedScheduledMail.getId());
                    } catch (Exception e) {
                        LOG.warn("Failed to transport scheduled mail {} ({}) for user {} in context {}", CompositionSpaces.getUUIDForLogging(lockedScheduledMail.getId()), mailPath, I(session.getUserId()), I(session.getContextId()), e);
                    }
                } finally {
                    if (toDelete != null) {
                        try {
                            defaultMailAccess.getMessageStorage().deleteMessages(toDelete.getFolder(), new String[] { toDelete.getMailID() }, true);
                        } catch (Exception e) {
                            LOG.warn("Failed to delete mail representation from mail storage for scheduled mail {} ({})", CompositionSpaces.getUUIDForLogging(lockedScheduledMail.getId()), mailPath, e);
                        }
                    }
                }
            } finally {
                for (MailAccess<? extends IMailFolderStorage,? extends IMailMessageStorage> mailAccess : mailAccesses.values()) {
                    mailAccess.close(true);
                }
            }
        }

        private static GeneratedSession generateSessionFor(int userId, int contextId, String login, String masterPassword, MailPathKnowingLockedScheduledMail lockedScheduledMail) throws OXException {
            // Generate session instance
            GeneratedSession session = new GeneratedSession(userId, contextId);

            // Login
            session.setLoginName(login);

            // Password
            if (masterPassword != null) {
                session.setPassword(masterPassword);
            } else {
                // Get credentials
                Optional<Meta> optMeta = lockedScheduledMail.getMeta();
                if (optMeta.isPresent()) {
                    Meta meta = optMeta.get();
                    String mailLogin = meta.getString(KnownMetaName.LOGIN.getName(), null);
                    String mailPassword = meta.getString(KnownMetaName.PASSWORD.getName(), null);
                    String oauthAccessToken = meta.getString(KnownMetaName.OAUTH_ACCESS_TOKEN.getName(), null);
                    String oauthRefreshToken = meta.getString(KnownMetaName.OAUTH_REFRESH_TOKEN.getName(), null);
                    String oauthAccessTokenExpiryDate = meta.getString(KnownMetaName.OAUTH_ACCESS_TOKEN_EXPIRY_DATE.getName(), null);
                    if (Strings.isNotEmpty(mailLogin)) {
                        session.setLoginName(mailLogin);
                    }
                    if (Strings.isNotEmpty(mailPassword)) {
                        session.setPassword(mailPassword);
                    }
                    if (Strings.isNotEmpty(oauthAccessToken)) {
                        session.setParameter(Session.PARAM_OAUTH_ACCESS_TOKEN, oauthAccessToken);
                    }
                    if (Strings.isNotEmpty(oauthRefreshToken)) {
                        session.setParameter(Session.PARAM_OAUTH_REFRESH_TOKEN, oauthRefreshToken);
                    }
                    if (Strings.isNotEmpty(oauthAccessTokenExpiryDate)) {
                        session.setParameter(Session.PARAM_OAUTH_ACCESS_TOKEN_EXPIRY_DATE, oauthAccessTokenExpiryDate);
                    }
                }

                if (null == session.getPassword() && null == session.getParameter(Session.PARAM_OAUTH_ACCESS_TOKEN)) {
                    OXException e = MailExceptionCode.MISSING_CONNECT_PARAM.create("Neither session password nor OAuth access token set. Either an invalid session or master authentication is not enabled (property \"com.openexchange.mail.passwordSource\" is not set to \"global\")");
                    e.setArgument(MailConfig.MISSING_SESSION_PASSWORD, Boolean.TRUE);
                    throw e;
                }
            }

            // Login
            {
                String proxyDelimiter = MailProperties.getInstance().getAuthProxyDelimiter();
                if (null != proxyDelimiter && null == session.getLoginName()) {
                    // Login cannot be determined
                    throw MailExceptionCode.MISSING_CONNECT_PARAM.create("Session login not set. Either an invalid session or master authentication is not enabled (property \"com.openexchange.mail.passwordSource\" is not set to \"global\")");
                }
            }

            // Client IP
            {
                Optional<Meta> optMeta = lockedScheduledMail.getMeta();
                if (optMeta.isPresent()) {
                    Meta meta = optMeta.get();
                    String clientIp = meta.getString(KnownMetaName.CLIENT_IP.getName(), null);
                    if (Strings.isNotEmpty(clientIp)) {
                        session.setLocalIp(clientIp);
                    }
                }
            }

            return session;
        }
    } // End of class DeliveryRunnable

    private static final class ScheduledTimerTaskKey implements Comparable<ScheduledTimerTaskKey> {

        private final UUID scheduledMailId;
        private final int userId;
        private final int contextId;
        private final int hash;

        ScheduledTimerTaskKey(ScheduledMail scheduledMail) {
            this(scheduledMail.getId(), scheduledMail.getUserId(), scheduledMail.getContextId());
        }

        ScheduledTimerTaskKey(UUID scheduledMailId, int userId, int contextId) {
            super();
            this.scheduledMailId = scheduledMailId;
            this.userId = userId;
            this.contextId = contextId;

            int prime = 31;
            int result = prime * 1 + contextId;
            result = prime * result + userId;
            result = prime * result + ((scheduledMailId == null) ? 0 : scheduledMailId.hashCode());
            hash = result;
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            ScheduledTimerTaskKey other = (ScheduledTimerTaskKey) obj;
            if (contextId != other.contextId) {
                return false;
            }
            if (userId != other.userId) {
                return false;
            }
            if (scheduledMailId == null) {
                if (other.scheduledMailId != null) {
                    return false;
                }
            } else if (!scheduledMailId.equals(other.scheduledMailId)) {
                return false;
            }
            return true;
        }

        @Override
        public int compareTo(ScheduledTimerTaskKey o) {
            int c = Integer.compare(contextId, o.contextId);
            if (c != 0) {
                return c;
            }
            c = Integer.compare(userId, o.userId);
            if (c != 0) {
                return c;
            }
            return scheduledMailId.compareTo(o.scheduledMailId);
        }
    }

    /** A tuple for scheduled timer task and associated locked scheduled mail */
    private static class TimerTaskAndLockedMail {

        final ScheduledTimerTask timerTask;
        final LockedScheduledMail lockedScheduledMail;

        TimerTaskAndLockedMail(ScheduledTimerTask timerTask, LockedScheduledMail lockedScheduledMail) {
            super();
            this.timerTask = timerTask;
            this.lockedScheduledMail = lockedScheduledMail;
        }
    }

    /** Simple helper class acting as parameter object for needed services */
    private static final class NeededServices {

        /** The scheduled mail storage service */
        final ScheduledMailStorageService scheduledMailStorageService;

        /** The timer service */
        final TimerService timerService;

        /** The thread pool service */
        final ThreadPoolService threadPool;

        /** The mail account storage service */
        final MailAccountStorageService mailAccountStorageService;

        /** The user service */
        final UserService userService;

        /** The registry for known composition space factories */
        final CompositionSpaceServiceFactoryRegistry csFactoryRegistry;

        /** The mail service */
        final MailService mailService;

        /**
         * Initializes a new {@link NeededServices} from given service look-up.
         *
         * @param scheduledMailStorageService The scheduled mail storage service
         * @param services The service look-up to acquire needed services from
         * @throws OXException If a certain service is absent
         */
        NeededServices(ScheduledMailStorageService scheduledMailStorageService, ServiceLookup services) throws OXException {
            super();
            this.scheduledMailStorageService = scheduledMailStorageService != null ? scheduledMailStorageService : services.getServiceSafe(ScheduledMailStorageService.class);
            this.timerService = services.getServiceSafe(TimerService.class);
            this.threadPool = services.getServiceSafe(ThreadPoolService.class);
            this.mailAccountStorageService = services.getServiceSafe(MailAccountStorageService.class);
            this.userService = services.getServiceSafe(UserService.class);
            this.csFactoryRegistry = services.getServiceSafe(CompositionSpaceServiceFactoryRegistry.class);
            this.mailService = services.getServiceSafe(MailService.class);
        }
    }

}
