/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.mail.scheduled.impl;

import java.time.Duration;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.mail.scheduled.ScheduledMailProperty;
import com.openexchange.server.ServiceLookup;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

/**
 * {@link DueScheduledMailsChecker} - Checks fur due scheduled mails and triggers their transport.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class DueScheduledMailsChecker implements Runnable {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(DueScheduledMailsChecker.class);

    private static ScheduledTimerTask timerTask;

    /**
     * (Re-)Initializes the timer task that checks for due scheduled mails & triggers their transport.
     *
     * @param service The scheduled mail service
     * @param services The service look-up
     * @throws OXException If timer task cannot be initiated
     */
    public static synchronized void reinitTimerTask(ScheduledMailServiceImpl service, ServiceLookup services) throws OXException {
        TimerService timerService = services.getServiceSafe(TimerService.class);

        ScheduledTimerTask t = timerTask;
        if (t != null) {
            timerTask = null;
            t.cancel(true);
            timerService.purge();
        }

        LeanConfigurationService configurationService = services.getServiceSafe(LeanConfigurationService.class);
        long frequencyMillis = Duration.ofMinutes(configurationService.getIntProperty(ScheduledMailProperty.CHECK_FREQUENCY_MINUTES)).toMillis();
        long lookAheadMillis = Duration.ofMinutes(configurationService.getIntProperty(ScheduledMailProperty.LOOK_AHEAD_MINUTES)).toMillis();
        long initialDelayMillis = 15_000 + ((long) (Math.random() * 1000));
        timerTask = timerService.scheduleWithFixedDelay(new DueScheduledMailsChecker(lookAheadMillis, service), initialDelayMillis, frequencyMillis);
    }

    /**
     * Drops the timer task that checks fur due scheduled mails & triggers their transport.
     *
     * @param services The service look-up
     */
    public static synchronized void dropTimerTask(ServiceLookup services) {
        ScheduledTimerTask t = timerTask;
        if (t != null) {
            timerTask = null;
            t.cancel(true);
            TimerService timerService = services.getOptionalService(TimerService.class);
            if (timerService != null) {
                timerService.purge();
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final long lookAheadMillis;
    private final ScheduledMailServiceImpl service;

    /**
     * Initializes a new {@link DueScheduledMailsChecker}.
     *
     * @param lookAheadMillis The look-ahead in milliseconds
     * @param service The service look-up
     */
    private DueScheduledMailsChecker(long lookAheadMillis, ScheduledMailServiceImpl service) {
        super();
        this.lookAheadMillis = lookAheadMillis;
        this.service = service;
    }

    @Override
    public void run() {
        try {
            service.scheduleTransportForDueScheduledMails(System.currentTimeMillis() + lookAheadMillis);
        } catch (Exception e) {
            LOG.warn("Failed to transport due scheduled mails", e);
        }
    }

}
