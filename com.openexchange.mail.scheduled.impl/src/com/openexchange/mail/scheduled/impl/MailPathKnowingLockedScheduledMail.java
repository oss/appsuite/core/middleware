/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.impl;

import java.util.Optional;
import java.util.UUID;
import com.openexchange.mail.MailPath;
import com.openexchange.mail.scheduled.LockedScheduledMail;
import com.openexchange.mail.scheduled.Meta;

/**
 * {@link MailPathKnowingLockedScheduledMail} - A locked scheduled mail knowing its mail path.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class MailPathKnowingLockedScheduledMail implements LockedScheduledMail {

    private final LockedScheduledMail lockedScheduledMail;
    private final MailPath knownMailPath;

    /**
     * Initializes a new {@link MailPathKnowingLockedScheduledMail}.
     *
     * @param lockedScheduledMail The locked scheduled mail to delegate to
     * @param knownMailPath The known mail path
     */
    public MailPathKnowingLockedScheduledMail(LockedScheduledMail lockedScheduledMail, MailPath knownMailPath) {
        super();
        this.lockedScheduledMail = lockedScheduledMail;
        this.knownMailPath = knownMailPath;
    }

    @Override
    public void unlock() {
        lockedScheduledMail.unlock();
    }

    @Override
    public UUID getId() {
        return lockedScheduledMail.getId();
    }

    @Override
    public int getContextId() {
        return lockedScheduledMail.getContextId();
    }

    @Override
    public int getUserId() {
        return lockedScheduledMail.getUserId();
    }

    @Override
    public String getMailPath() {
        return lockedScheduledMail.getMailPath();
    }

    @Override
    public long getDateToSend() {
        return lockedScheduledMail.getDateToSend();
    }

    @Override
    public Optional<Meta> getMeta() {
        return lockedScheduledMail.getMeta();
    }

    /**
     * Gets the known mail path.
     *
     * @return The known mail path
     */
    public MailPath getKnownMailPath() {
        return knownMailPath;
    }

    @Override
    public String toString() {
        return lockedScheduledMail.toString();
    }

}
