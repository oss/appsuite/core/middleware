/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.jslob.storage.db.cache;

import java.util.Collection;
import org.osgi.framework.BundleContext;
import com.openexchange.exception.OXException;
import com.openexchange.pubsub.AbstractTrackingChannelListener;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.Message;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.segmenter.sitechange.SiteChangedListener;

/**
 * {@link CachingJSlobInvalidator}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class CachingJSlobInvalidator extends AbstractTrackingChannelListener<Collection<StampedJSlobId>> implements SiteChangedListener {

    /**
     * Initializes a new {@link CachingJSlobInvalidator}.
     *
     * @param context The bundle context
     */
    public CachingJSlobInvalidator(BundleContext context) {
        super(context);
    }

    @Override
    public void onMessage(Message<Collection<StampedJSlobId>> message) {
        if (message.isRemote()) {
            CachingJSlobStorage storage = CachingJSlobStorage.getInstance();
            if (storage == null) {
                return;
            }

            if (message.getData().stream().anyMatch(stamped -> stamped.getJSlobId().equals(CachingJSlobStorage.ALL))) {
                // Special CachingJSlobStorage.ALL identifier given
                storage.invalidateAll(false);
                return;
            }

            for (StampedJSlobId stampedJSlobId : message.getData()) {
                storage.invalidateIfNotStoredAfter(stampedJSlobId.getJSlobId(), stampedJSlobId.getStoreTime());
            }
        }
    }

    @Override
    protected Channel<Collection<StampedJSlobId>> getChannel(PubSubService service) {
        return CachingJSlobStorage.getChannel(service);
    }

    @Override
    public void onSiteChange() throws OXException {
        CachingJSlobStorage storage = CachingJSlobStorage.getInstance();
        if (storage != null) {
            storage.invalidateAll(false);
        }
    }

}
