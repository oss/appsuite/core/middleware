/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.jslob.storage.db.cache;

import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.sql.SQLException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONArray;
import org.json.JSONServices;
import org.json.SimpleJSONSerializer;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.UnsynchronizedStringWriter;
import com.openexchange.jslob.ImmutableJSlob;
import com.openexchange.jslob.JSlob;
import com.openexchange.jslob.JSlobExceptionCodes;
import com.openexchange.jslob.JSlobId;
import com.openexchange.jslob.storage.JSlobStorage;
import com.openexchange.jslob.storage.db.DBJSlobStorage;
import com.openexchange.jslob.storage.db.Services;
import com.openexchange.jslob.storage.db.util.DelayedStoreOp;
import com.openexchange.jslob.storage.db.util.DelayedStoreOpDelayQueue;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.ChannelKey;
import com.openexchange.pubsub.ChannelMessageCodec;
import com.openexchange.pubsub.DefaultChannelKey;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.pubsub.core.CoreChannelName;

/**
 * {@link CachingJSlobStorage}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class CachingJSlobStorage implements JSlobStorage {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CachingJSlobStorage.class);

    private static final AtomicReference<CachingJSlobStorage> INSTANCE_REF = new AtomicReference<>();

    /**
     * Initializes the caching JSlob storage.
     *
     * @param delegate The database-backed storage to delegate to
     */
    public static CachingJSlobStorage initialize(DBJSlobStorage delegate) {
        CachingJSlobStorage tmp = INSTANCE_REF.get();
        if (null == tmp) {
            CachingJSlobStorage newInstance = new CachingJSlobStorage(delegate);
            tmp = INSTANCE_REF.compareAndExchange(null, newInstance);
            if (tmp == null) {
                // New instance successfully set
                newInstance.start();
                tmp = newInstance;
            }
        }
        return tmp;
    }

    /**
     * Gets the instance
     *
     * @return The instance
     */
    public static CachingJSlobStorage getInstance() {
        return INSTANCE_REF.get();
    }

    /**
     * Shuts-down the caching JSlob storage.
     */
    public static void shutdown() {
        CachingJSlobStorage tmp = INSTANCE_REF.getAndSet(null);
        if (null != tmp) {
            tmp.release();
        }
    }

    /** The special identifier to reference all cached entries */
    public static final JSlobId ALL = new JSlobId("all", "all", 0, 0);

    // ------------------------------------------------------------------------------------------------------------------------------------

    /** The poison element */
    private static final DelayedStoreOp POISON = DelayedStoreOp.POISON;

    /** The database-backed storage to delegate to. */
    private final DBJSlobStorage delegate;

    /** The queue for delayed store operations */
    private final DelayedStoreOpDelayQueue delayedStoreOps;

    /** The keep-going flag */
    private final AtomicBoolean keepgoing;

    /** The (local) cache for JSlobs; invalidation happens via {@link #getChannel(PubSubService)} and {@link CachingJSlobInvalidator} */
    private final Cache<JSlobId, JSlobReference> cache;

    /** The thread checking for delayed JSlobs in queue that need to be written back to database */
    private final AtomicReference<Thread> delayQueueWatcherRef;

    /**
     * Initializes a new {@link CachingJSlobStorage}.
     */
    private CachingJSlobStorage(DBJSlobStorage delegate) {
        super();
        this.delegate = delegate;
        delayedStoreOps = new DelayedStoreOpDelayQueue();
        keepgoing = new AtomicBoolean(true);
        cache = CacheBuilder.newBuilder().expireAfterAccess(Duration.ofSeconds(360)).build();
        delayQueueWatcherRef = new AtomicReference<>();
    }

    /**
     * Starts this instance's delay queue watcher.
     */
    private void start() {
        Thread delayQueueWatcher = new Thread(this::checkForDelayedJSlobs, "JSlobDelayQueueWatcher");
        delayQueueWatcher.start();
        this.delayQueueWatcherRef.set(delayQueueWatcher);
    }

    /**
     * Releases this instance through halting watcher thread and writing back remaining JSlobs to database.
     */
    private void release() {
        LOG.info("Awaiting JSlob watcher thread to finish...");
        keepgoing.set(false);
        delayedStoreOps.offer(POISON); // NOSONARLINT
        try {
            delayQueueWatcherRef.getAndSet(null).join(0);
        } catch (InterruptedException e) {
            // Keep thread's interrupted status
            Thread.currentThread().interrupt();
        }
        flushDelayedOps2Storage();
        cache.invalidateAll();
        LOG.info("JSlob watcher thread finished and all delayed JSlobs written back to database");
    }

    /**
     * Checks for JSlobs that were put into cache and need to be written back to database.
     */
    void checkForDelayedJSlobs() {
        List<DelayedStoreOp> objects = new ArrayList<DelayedStoreOp>(16);
        while (keepgoing.get()) {
            try {
                // Blocking wait for at least one DelayedPushMsObject to expire.
                DelayedStoreOp object = delayedStoreOps.take();
                if (POISON == object) {
                    // Poison element: Leave method
                    return;
                }
                objects.clear();
                objects.add(object);

                // Drain more if available
                delayedStoreOps.drainTo(objects);
                try {
                    if (writeMultiple2DB(objects)) {
                        // Encountered poison element
                        return;
                    }
                } catch (OXException e) {
                    // Multiple store failed
                    if (!JSlobExceptionCodes.UNEXPECTED_ERROR.equals(e) || !(e.getCause() instanceof SQLException)) {
                        throw e;
                    }
                    boolean leave = false;
                    for (DelayedStoreOp delayedStoreOp : objects) {
                        if (POISON == delayedStoreOp) {
                            // Encountered poison element
                            leave = true;
                        } else if (delayedStoreOp != null) {
                            try {
                                write2DB(delayedStoreOp);
                            } catch (Exception x) {
                                LOG.error("JSlob could not be flushed to database", x);
                            }
                        }
                    }
                    if (leave) {
                        return;
                    }
                }
            } catch (InterruptedException e) {
                LOG.debug("Interrupted while checking for delayed JSlobs.", e);
                // Keep interrupted state
                Thread.currentThread().interrupt();
            } catch (Exception e) {
                LOG.error("Checking for delayed JSlobs failed", e);
            }
        }
    }

    private void write2DB(DelayedStoreOp delayedStoreOp) throws OXException {
        JSlobReference jslobReference = cache.getIfPresent(delayedStoreOp.jSlobId);
        if (jslobReference != null) {
            ImmutableJSlob t = jslobReference.jslob;
            if (null != t) {
                // Write to store
                delegate.store(delayedStoreOp.jSlobId, t);

                // Propagate among remote caches
                fireInvalidateEvent(new StampedJSlobId(delayedStoreOp.jSlobId, jslobReference.storeTime));
            }
        }
    }

    /**
     * Writes specified elapsed delayed store operations back to database.
     *
     * @param delayedStoreOps The elapsed delayed store operations
     * @return <code>true</code> if the special poison element has been encountered; otherwise <code>false</code>
     * @throws OXException If processing elapsed delayed store operations fails
     */
    private boolean writeMultiple2DB(List<DelayedStoreOp> delayedStoreOps) throws OXException {
        boolean leave = false;

        // Collect valid delayed store operations
        int size = delayedStoreOps.size();
        Map<JSlobId, JSlob> jslobs = HashMap.newHashMap(size);
        Map<StampedJSlobId, StampedJSlobId> stampedJSlobIds = HashMap.newHashMap(size);
        for (int i = 0; i < size; i++) {
            DelayedStoreOp delayedStoreOp = delayedStoreOps.get(i);
            if (POISON == delayedStoreOp) {
                leave = true;
            } else if (delayedStoreOp != null) {
                JSlobReference jSlobReference = cache.getIfPresent(delayedStoreOp.jSlobId);
                if (jSlobReference != null) {
                    ImmutableJSlob jslob = jSlobReference.jslob;
                    if (null != jslob) {
                        jslobs.put(delayedStoreOp.jSlobId, jslob);
                        StampedJSlobId stampedJSlobId = new StampedJSlobId(delayedStoreOp.jSlobId, jSlobReference.storeTime);
                        StampedJSlobId replaced = stampedJSlobIds.put(stampedJSlobId, stampedJSlobId);
                        // Check if replaced element's store time was greater than current ones
                        if (replaced != null && (replaced.getStoreTime() > 0 && replaced.getStoreTime() > stampedJSlobId.getStoreTime())) {
                            // Restore replaced element
                            stampedJSlobIds.put(replaced, replaced);
                        }
                    }
                }
            }
        }

        // Store them
        delegate.storeMultiple(jslobs);

        // Invalidate caches
        fireInvalidateEvent(stampedJSlobIds.values());

        return leave;
    }

    /**
     * Invalidates all JSlob entries associated with specified user from cache.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param invalidateDueToRemoval <code>true</code> to signal that caches entries are supposed to be invalidated since entries were removed (and remote propagation is necessary); otherwise <code>false</code>
     */
    public void dropAllUserJSlobs(int userId, int contextId, boolean invalidateDueToRemoval) {
        flushDelayedOpsForUser(userId, contextId);

        List<JSlobId> ids = null;
        for (JSlobId jSlobId : cache.asMap().keySet()) {
            if (jSlobId.getUserId() == userId && jSlobId.getContextId() == contextId) {
                if (ids == null) {
                    ids = new ArrayList<>();
                }
                ids.add(jSlobId);
            }
        }
        if (ids != null) {
            cache.invalidateAll(ids);
            if (invalidateDueToRemoval) {
                fireInvalidateEvent(ids.stream().map(id -> new StampedJSlobId(id)).collect(CollectorUtils.toList(ids.size()))); // NOSONARLINT
            }
        }
    }

    /** The channel key for JSlob messages */
    private static final ChannelKey JSLOB_CHANNEL_KEY = DefaultChannelKey.builder().withChannelName(CoreChannelName.JSLOB_EVENTS).build();

    /** The codec for LIST/LSUB messages */
    private static final ChannelMessageCodec<Collection<StampedJSlobId>> JSLOB_CHANNEL_CODEC = new ChannelMessageCodec<Collection<StampedJSlobId>>() {

        @Override
        public String serialize(Collection<StampedJSlobId> jSlobIds) throws Exception {
            Writer writer = new UnsynchronizedStringWriter(128);
            writer.append('[');

            Iterator<StampedJSlobId> it = jSlobIds.iterator();
            if (it.hasNext()) {
                writeJSlobId(it.next(), false, writer);
                while (it.hasNext()) {
                    writeJSlobId(it.next(), true, writer);
                }
            }

            writer.append(']');
            return writer.toString();
        }

        private void writeJSlobId(StampedJSlobId stampedJSlobId, boolean addComma, Writer writer) throws IOException {
            if (addComma) {
                writer.append(',');
            }
            JSlobId jSlobId = stampedJSlobId.getJSlobId();

            writer.append('[');

            SimpleJSONSerializer.writeJsonValue(jSlobId.getServiceId(), writer);

            writer.append(',');
            SimpleJSONSerializer.writeJsonValue(jSlobId.getId(), writer);

            writer.append(',');
            writer.append(Integer.toString(jSlobId.getUserId()));

            writer.append(',');
            writer.append(Integer.toString(jSlobId.getContextId()));

            writer.append(',');
            writer.append(Long.toString(stampedJSlobId.getStoreTime()));

            writer.append(']');
        }

        @Override
        public Collection<StampedJSlobId> deserialize(String data) throws Exception {
            JSONArray jList = JSONServices.parseArray(data);
            List<StampedJSlobId> jSlobIds = new ArrayList<>(jList.length());
            for (Object o : jList) {
                JSONArray jParts = (JSONArray) o;
                long storeTime = jParts.length() > 4 ? jParts.getLong(4) : -1L;
                jSlobIds.add(new StampedJSlobId(new JSlobId(jParts.getString(0), jParts.getString(1), jParts.getInt(2), jParts.getInt(3)), storeTime));
            }
            return jSlobIds;
        }
    };

    /**
     * Gets the channel to use for JSlob invalidation messages.
     *
     * @param service The Pub/Sub service
     * @return The channel
     */
    public static Channel<Collection<StampedJSlobId>> getChannel(PubSubService service) {
        return service.getChannel(JSLOB_CHANNEL_KEY, JSLOB_CHANNEL_CODEC);
    }

    private static void fireInvalidateEvent(Collection<StampedJSlobId> ids) {
        PubSubService service = Services.getOptionalService(PubSubService.class);
        if (service == null) {
            return;
        }

        try {
            getChannel(service).publish(ids);
        } catch (Exception e) {
            LOG.warn("Failed publishing invalidation message for JSlobs {}", ids, e);
        }
    }

    private static void fireInvalidateEvent(StampedJSlobId id) {
        fireInvalidateEvent(Collections.singletonList(id));
    }

    private void flushDelayedOps2Storage() {
        for (DelayedStoreOp delayedStoreOp : delayedStoreOps) {
            if (delayedStoreOp != null && POISON != delayedStoreOp) {
                try {
                    write2DB(delayedStoreOp);
                } catch (Exception e) {
                    LOG.error("JSlobs could not be flushed to database", e);
                }
            }
        }
    }

    /**
     * Flushes the delayed operations associated with given user to storage
     *
     * @param userId The suer identifier
     * @param contextId The context identifier
     */
    public void flushDelayedOpsForUser(int userId, int contextId) {
        List<DelayedStoreOp> ops = new LinkedList<DelayedStoreOp>();
        int n = delayedStoreOps.drainForUser(userId, contextId, ops);

        if (n > 0) {
            for (DelayedStoreOp delayedStoreOp : ops) {
                if (delayedStoreOp != null && POISON != delayedStoreOp) {
                    try {
                        write2DB(delayedStoreOp);
                    } catch (Exception e) {
                        LOG.error("JSlobs could not be flushed to database", e);
                    }
                }
            }
        }
    }

    @Override
    public String getIdentifier() {
        return delegate.getIdentifier();
    }

    @Override
    public boolean store(JSlobId id, JSlob t) throws OXException {
        // Delay store operation
        delayedStoreOps.offerIfAbsent(new DelayedStoreOp(id));

        // Added to OR already contained in delay queue -- put current to cache
        cache.put(id, new JSlobReference(t.setId(id), System.currentTimeMillis()));
        return true;
    }

    /**
     * Invalidates denoted element if local element has not been stored after given remote store time.
     *
     * @param id The identifier
     * @param storeTime The store time of the remote JSlob entry or <code>-1</code> if unknown
     */
    public void invalidateIfNotStoredAfter(JSlobId id, long storeTime) {
        if (storeTime < 0) {
            cache.invalidate(id);
        } else {
            JSlobReference reference = cache.getIfPresent(id);
            if (reference != null && (reference.storeTime < 0 || reference.storeTime < storeTime)) {
                cache.invalidate(id);
            }
        }
    }

    @Override
    public JSlob load(JSlobId id) throws OXException {
        JSlobReference object = cache.getIfPresent(id);
        if (object != null) {
            ImmutableJSlob jslob = object.jslob;
            if (null == jslob) {
                throw JSlobExceptionCodes.NOT_FOUND_EXT.create(id.getServiceId(), Integer.valueOf(id.getUserId()), Integer.valueOf(id.getContextId()));
            }
            return jslob.clone();
        }
        JSlob loaded = delegate.load(id);
        cache.put(id, new JSlobReference(loaded, -1L));
        return loaded.clone();
    }

    @Override
    public JSlob opt(JSlobId id) throws OXException {
        JSlobReference fromCache = cache.getIfPresent(id);
        if (fromCache != null) {
            ImmutableJSlob jslob = fromCache.jslob;
            return null == jslob ? null : jslob.clone();
        }

        // Optional retrieval from DB storage
        JSlob opt = delegate.opt(id);
        if (null == opt) {
            // Null
            cache.put(id, new JSlobReference(null, -1L));
            return null;
        }
        cache.put(id, new JSlobReference(opt, -1L));
        return opt.clone();
    }

    @Override
    public List<JSlob> list(List<JSlobId> ids) throws OXException {
        int size = ids.size();
        Map<String, JSlob> map = HashMap.newHashMap(size);
        List<JSlobId> toLoad = new ArrayList<JSlobId>(size);
        for (int i = 0; i < size; i++) {
            JSlobId id = ids.get(i);
            JSlobReference object = cache.getIfPresent(id);
            if (object != null) {
                ImmutableJSlob jslob = object.jslob;
                if (null == jslob) {
                    cache.invalidate(id);
                    toLoad.add(id);
                    continue;
                }
                map.put(id.getId(), jslob.clone());
            } else {
                toLoad.add(id);
            }
        }

        if (!toLoad.isEmpty()) {
            List<JSlob> loaded = delegate.list(toLoad);
            for (JSlob jSlob : loaded) {
                if (null != jSlob) {
                    JSlobId id = jSlob.getId();
                    cache.put(id, new JSlobReference(jSlob, -1L));
                    map.put(id.getId(), jSlob.clone());
                }
            }
        }

        List<JSlob> ret = new ArrayList<JSlob>(size);
        for (JSlobId id : ids) {
            ret.add(null == id ? null : map.get(id.getId()));
        }
        return ret;
    }

    @Override
    public Collection<JSlob> list(JSlobId id) throws OXException {
        Collection<String> ids = delegate.getIDs(id);
        List<JSlob> ret = new ArrayList<JSlob>(ids.size());
        String serviceId = id.getServiceId();
        int user = id.getUserId();
        int context = id.getContextId();
        for (String sId : ids) {
            ret.add(load(new JSlobId(serviceId, sId, user, context)));
        }
        return ret;
    }

    @Override
    public JSlob remove(JSlobId id) throws OXException {
        JSlob removed = delegate.remove(id);
        // Successfully removed
        cache.invalidate(id);
        fireInvalidateEvent(new StampedJSlobId(removed.getId()));
        return removed;
    }

    /**
     * Invalidates all entries in the cache.
     *
     * @param notify Whether to notify among remote nodes or not
     */
    public void invalidateAll(boolean notify) {
        flushDelayedOps2Storage();
        cache.invalidateAll();
        if (notify) {
            fireInvalidateEvent(new StampedJSlobId(ALL));
        }
        LOG.info("Cache flushed & all delayed JSlobs written back to database");
    }

    // --------------------------------------------------------------------------------------------

    /**
     * {@link ImmutableReference} - A simple immutable reference class.
     *
     * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
     * @since v7.8.4
     */
    private static final class JSlobReference implements Serializable {

        private static final long serialVersionUID = 1129602965001367804L;

        /** The immutable JSlob */
        final transient ImmutableJSlob jslob;

        /** The time (the number of milliseconds since January 1, 1970, 00:00:00 GMT) when associated JSlob has been stored or <code>-1</code> if not set */
        final long storeTime;

        /**
         * Initializes a new instance of {@link JSlobReference}.
         *
         * @param jslob The JSlob
         * @param storeTime The time (the number of milliseconds since January 1, 1970, 00:00:00 GMT) when associated JSlob has been stored or <code>-1</code> if unknown
         */
        JSlobReference(JSlob jslob, long storeTime) {
            super();
            this.jslob = ImmutableJSlob.valueOf(jslob);
            this.storeTime = storeTime;
        }
    }

}
