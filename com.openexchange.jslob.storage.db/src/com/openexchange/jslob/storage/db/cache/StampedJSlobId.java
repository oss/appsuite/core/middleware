/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.jslob.storage.db.cache;

import com.openexchange.jslob.JSlobId;

/**
 * {@link StampedJSlobId} - A tuple of a JSlob identifier and a time stamp.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class StampedJSlobId implements Comparable<StampedJSlobId> {

    private final JSlobId jSlobId;
    private final long storeTime;
    private final int hash;

    /**
     * Initializes a new instance of {@link StampedJSlobId}.
     *
     * @param jSlobId The JSlob identifier
     */
    public StampedJSlobId(JSlobId jSlobId) {
        this(jSlobId, -1L);
    }

    /**
     * Initializes a new instance of {@link StampedJSlobId}.
     *
     * @param jSlobId The JSlob identifier
     * @param storeTime The time (the number of milliseconds since January 1, 1970, 00:00:00 GMT) when associated JSlob has been stored
     *                  or <code>-1</code> if unknown
     */
    public StampedJSlobId(JSlobId jSlobId, long storeTime) {
        super();
        this.jSlobId = jSlobId;
        this.storeTime = storeTime;
        hash = 31 * 1 + ((jSlobId == null) ? 0 : jSlobId.hashCode());
    }

    /**
     * Gets the JSlob identifier.
     *
     * @return The JSlob identifier
     */
    public JSlobId getJSlobId() {
        return jSlobId;
    }

    /**
     * Gets the time when associated JSlob has been stored.
     *
     * @return The store time or <code>-1</code>
     */
    public long getStoreTime() {
        return storeTime;
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        StampedJSlobId other = (StampedJSlobId) obj;
        if (jSlobId == null) {
            if (other.jSlobId != null) {
                return false;
            }
        } else if (!jSlobId.equals(other.jSlobId)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(StampedJSlobId o) {
        return jSlobId.compareTo(o.jSlobId);
    }

}
