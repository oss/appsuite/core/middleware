/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.jslob.storage.db.osgi;

import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.config.ConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.database.CreateTableService;
import com.openexchange.database.DatabaseService;
import com.openexchange.groupware.delete.DeleteListener;
import com.openexchange.groupware.update.DefaultUpdateTaskProviderService;
import com.openexchange.groupware.update.UpdateTaskProviderService;
import com.openexchange.jslob.storage.JSlobStorage;
import com.openexchange.jslob.storage.db.DBJSlobStorage;
import com.openexchange.jslob.storage.db.Services;
import com.openexchange.jslob.storage.db.cache.CachingJSlobInvalidator;
import com.openexchange.jslob.storage.db.cache.CachingJSlobStorage;
import com.openexchange.jslob.storage.db.groupware.DBJSlobCreateTableService;
import com.openexchange.jslob.storage.db.groupware.DBJSlobCreateTableTask;
import com.openexchange.jslob.storage.db.groupware.DBJSlobIncreaseBlobSizeTask;
import com.openexchange.jslob.storage.db.groupware.JSlobDBDeleteListener;
import com.openexchange.jslob.storage.db.groupware.JsonStorageTableUtf8Mb4UpdateTask;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.segmenter.sitechange.SiteChangedListener;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessiondEventConstants;
import com.openexchange.threadpool.AbstractTask;
import com.openexchange.threadpool.ThreadPoolService;

/**
 * {@link DBJSlobStorageActivcator}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class DBJSlobStorageActivcator extends HousekeepingActivator {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(DBJSlobStorageActivcator.class);

    /**
     * Initializes a new {@link DBJSlobStorageActivcator}.
     */
    public DBJSlobStorageActivcator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { DatabaseService.class, ConfigurationService.class, ThreadPoolService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        LOG.info("Starting bundle: com.openexchange.jslob.storage.db");
        try {
            Services.setServices(this);
            DBJSlobStorage dbJSlobStorage = new DBJSlobStorage(this);
            CachingJSlobStorage cachingJSlobStorage = CachingJSlobStorage.initialize(dbJSlobStorage);
            CachingJSlobInvalidator invalidator = new CachingJSlobInvalidator(context);
            registerService(JSlobStorage.class, cachingJSlobStorage);
            registerService(SiteChangedListener.class, invalidator);
            /*
             * Register services for table creation
             */
            registerService(CreateTableService.class, new DBJSlobCreateTableService());
            registerService(UpdateTaskProviderService.class, new DefaultUpdateTaskProviderService(new DBJSlobCreateTableTask(), new DBJSlobIncreaseBlobSizeTask(), new JsonStorageTableUtf8Mb4UpdateTask()));
            /*
             * Register delete listener
             */
            registerService(DeleteListener.class, new JSlobDBDeleteListener(dbJSlobStorage));
            /*
             * Add tracker for cache service
             */
            trackService(ContextService.class);
            trackService(CacheService.class);
            trackService(PubSubService.class);
            track(PubSubService.class, invalidator);
            openTrackers();

            {
                EventHandler eventHandler = new EventHandler() {

                    @Override
                    public void handleEvent(final Event event) {
                        String topic = event.getTopic();
                        if (SessiondEventConstants.TOPIC_REMOVE_CONTAINER.equals(topic)) {
                            Map<String, Session> container = (Map<String, Session>) event.getProperty(SessiondEventConstants.PROP_CONTAINER);
                            ThreadPoolService threadPool = DBJSlobStorageActivcator.this.getService(ThreadPoolService.class);
                            if (null == threadPool) {
                                doHandleSessionContainer(container);
                            } else {
                                threadPool.submit(new AbstractTask<Void>() {

                                    @Override
                                    public Void call() throws Exception {
                                        doHandleSessionContainer(container);
                                        return null;
                                    }
                                });
                            }
                        } else if (SessiondEventConstants.TOPIC_REMOVE_SESSION.equals(topic)) {
                            Session session = (Session) event.getProperty(SessiondEventConstants.PROP_SESSION);
                            cachingJSlobStorage.dropAllUserJSlobs(session.getUserId(), session.getContextId(), false);
                        }
                    }

                    void doHandleSessionContainer(Map<String, Session> container) {
                        Set<UsID> set = HashSet.newHashSet(container.size());
                        for (Session session : container.values()) {
                            if (!session.isTransient()) {
                                int contextId = session.getContextId();
                                int userId = session.getUserId();
                                if (set.add(new UsID(userId, contextId))) {
                                    cachingJSlobStorage.dropAllUserJSlobs(userId, contextId, false);
                                }
                            }
                        }
                    }
                };

                Dictionary<String, Object> props = new Hashtable<String, Object>(2);
                props.put(EventConstants.EVENT_TOPIC, SessiondEventConstants.getAllTopics());
                registerService(EventHandler.class, eventHandler, props);
            }
        } catch (Exception e) {
            LOG.error("Starting bundle \"com.openexchange.jslob.storage.db\" failed", e);
            throw e;
        }
    }

    @Override
    public <S> boolean addService(final Class<S> clazz, final S service) {
        return super.addService(clazz, service);
    }

    @Override
    public <S> boolean removeService(final Class<? extends S> clazz) {
        return super.removeService(clazz);
    }

    @Override
    protected void stopBundle() throws Exception {
        LOG.info("Stopping bundle: com.openexchange.jslob.storage.db");
        try {
            CachingJSlobStorage.shutdown();
            super.stopBundle();
        } catch (Exception e) {
            LOG.error("Stopping bundle \"com.openexchange.jslob.storage.db\" failed", e);
            throw e;
        } finally {
            Services.setServices(null);
        }
    }

    // ------------------------------------------------------------------------------------------------------------------

    static final class UsID {

        final int userId;
        final int contextId;
        private final int hash;

        UsID(int userId, int contextId) {
            super();
            this.userId = userId;
            this.contextId = contextId;

            int prime = 31;
            int result = 1;
            result = prime * result + contextId;
            result = prime * result + userId;
            this.hash = result;
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof UsID)) {
                return false;
            }
            UsID other = (UsID) obj;
            if (contextId != other.contextId) {
                return false;
            }
            if (userId != other.userId) {
                return false;
            }
            return true;
        }
    }

}
