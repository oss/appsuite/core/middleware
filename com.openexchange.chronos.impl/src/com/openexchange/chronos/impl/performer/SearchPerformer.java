/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.impl.performer;

import static com.openexchange.chronos.common.SearchUtils.buildSearchTerm;
import static com.openexchange.chronos.common.SearchUtils.getSearchTerm;
import static com.openexchange.chronos.impl.Utils.getVisibleFolders;
import static com.openexchange.folderstorage.Permission.NO_PERMISSIONS;
import static com.openexchange.folderstorage.Permission.READ_FOLDER;
import static com.openexchange.folderstorage.Permission.READ_OWN_OBJECTS;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.openexchange.chronos.Event;
import com.openexchange.chronos.EventField;
import com.openexchange.chronos.common.DefaultEventsResult;
import com.openexchange.chronos.impl.CalendarFolder;
import com.openexchange.chronos.impl.Check;
import com.openexchange.chronos.service.CalendarSession;
import com.openexchange.chronos.service.EventsResult;
import com.openexchange.chronos.storage.CalendarStorage;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.Type;
import com.openexchange.folderstorage.type.PublicType;
import com.openexchange.java.Lists;
import com.openexchange.search.CompositeSearchTerm;
import com.openexchange.search.CompositeSearchTerm.CompositeOperation;
import com.openexchange.search.SearchTerm;
import com.openexchange.search.SingleSearchTerm.SingleOperation;

/**
 * {@link SearchPerformer}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @since v7.10.0
 */
public class SearchPerformer extends AbstractSearchPerformer {

    private final int minimumSearchPatternLength;

    /**
     * Initializes a new {@link SearchPerformer}.
     *
     * @param session The calendar session
     * @param storage The underlying calendar storage
     */
    public SearchPerformer(CalendarSession session, CalendarStorage storage) throws OXException {
        super(session, storage);
        this.minimumSearchPatternLength = session.getConfig().getMinimumSearchPatternLength();
    }

    /**
     * Performs the operation
     *
     * @param <O> The search term type
     * @param folderIds The identifiers of the folders to perform the search in, or <code>null</code> to search all visible folders
     * @param term The search term
     * @return The found events
     */
    public <O> Map<String, EventsResult> perform(List<String> folderIds, SearchTerm<O> term) throws OXException {
        return search(folderIds, term, null);
    }

    /**
     * Performs the operation.
     *
     * @param folderIDs The identifiers of the folders to perform the search in, or <code>null</code> to search all visible folders
     * @param pattern The pattern to search for
     * @return The found events
     */
    public List<Event> perform(String[] folderIDs, String pattern) throws OXException {
        List<Event> events = new ArrayList<Event>();
        List<String> folderIds = null == folderIDs ? null : Arrays.asList(folderIDs);
        Map<String, EventsResult> resultsPerFolder = perform(folderIds, Collections.singletonList(pattern));
        for (Entry<String, EventsResult> entry : resultsPerFolder.entrySet()) {
            List<Event> eventsPerFolder = entry.getValue().getEvents();
            if (null != eventsPerFolder) {
                events.addAll(eventsPerFolder);
            }
        }
        return events;
    }

    /**
     * Performs the operation.
     *
     * @param folderIds The identifiers of the folders to perform the search in, or <code>null</code> to search all visible folders
     * @param filters A list of additional filters to be applied on the search, or <code>null</code> if not specified
     * @param queries The queries to search for
     * @return The found events
     */
    public Map<String, EventsResult> perform(List<String> folderIds, List<String> queries) throws OXException {
        return search(folderIds, null, queries);
    }

    /**
     * Performs the search operation.
     *
     * @param folderIds The identifiers of the folders to perform the search in, or <code>null</code> to search all visible folders
     * @param term A generic term for the search, or <code>null</code> if not specified
     * @param filters A list of additional filters to be applied on the search, or <code>null</code> if not specified
     * @param queries The queries to search for
     * @return The found events
     */
    private Map<String, EventsResult> search(List<String> folderIds, SearchTerm<?> term, List<String> queries) throws OXException {
        /*
         * check search input
         */
        Check.minimumSearchPatternLength(queries, minimumSearchPatternLength);
        Check.minimumSearchPatternLength(term, minimumSearchPatternLength);
        SearchTerm<?> searchTerm = combine(term, buildSearchTerm(queries), CompositeOperation.AND);
        if (null != folderIds) {
            /*
             * search in specific folder(s), combine queries and term, then pass to common retrieval function
             */
            return getEvents(folderIds, searchTerm, true);
        }
        /*
         * no folders specified, search in all visible folders, along with a special search to pick up orphaned events (w/o visible parent folder)
         */
        List<CalendarFolder> visibleFolders = getVisibleFolders(session, false, READ_FOLDER, READ_OWN_OBJECTS, NO_PERMISSIONS, NO_PERMISSIONS);
        Map<String, EventsResult> resultsPerFolderId = getEvents(visibleFolders.stream().map(f -> f.getId()).toList(), searchTerm, true);
        SearchTerm<?> orphanedEventsTerm = combine(getNotInVisiblePublicFolderTerm(visibleFolders), searchTerm, CompositeOperation.AND);
        List<Event> orphanedEventsOfUser = getEventsOfUser(null, null, new Type[] { PublicType.getInstance() }, orphanedEventsTerm, true);
        return addEvents(resultsPerFolderId, orphanedEventsOfUser);
    }

    /**
     * Injects additional events into existing events results, mapped to their corresponding parent folder identifier.
     * 
     * @param resultsPerFolderId The results per folder id to add the events to
     * @param events The events to add
     * @return The passed results per folder id map, extended with the additional events
     */
    private static Map<String, EventsResult> addEvents(Map<String, EventsResult> resultsPerFolderId, List<Event> events) {
        Map<String, List<Event>> eventsPerFolderId = new HashMap<String, List<Event>>();
        for (Event event : events) {
            com.openexchange.tools.arrays.Collections.put(eventsPerFolderId, event.getFolderId(), event);
        }
        for (Entry<String, List<Event>> entry : eventsPerFolderId.entrySet()) {
            EventsResult eventsResult = resultsPerFolderId.get(entry.getKey());
            if (null == eventsResult) {
                resultsPerFolderId.put(entry.getKey(), new DefaultEventsResult(entry.getValue()));
            } else {
                resultsPerFolderId.put(entry.getKey(), new DefaultEventsResult(Lists.combine(eventsResult.getEvents(), entry.getValue())));
            }
        }
        return resultsPerFolderId;
    }

    /**
     * Constructs a search term to match all events that are located in a <i>public</i> folder that is <b>not</b> contained in the 
     * supplied list of visible folders.
     * 
     * @param visibleFolders The visible folders to consider
     * @return The search term
     */
    private static SearchTerm<?> getNotInVisiblePublicFolderTerm(List<CalendarFolder> visibleFolders) {
        /*
         * begin with term to match "public" folders only
         */
        CompositeSearchTerm folderNotNullTerm = new CompositeSearchTerm(CompositeOperation.NOT).addSearchTerm(getSearchTerm(EventField.FOLDER_ID, SingleOperation.ISNULL));
        /*
         * optionally combine with term to exclude "public" folders that are visible anyways (already covered by parent search)
         */
        SearchTerm<?> visiblePublicFolderIdsTerm = getVisiblePublicFolderIdsTerm(visibleFolders);
        if (null == visiblePublicFolderIdsTerm) {
            return folderNotNullTerm;
        }
        return combine(folderNotNullTerm, new CompositeSearchTerm(CompositeOperation.NOT).addSearchTerm(visiblePublicFolderIdsTerm), CompositeOperation.AND);
    }

    /**
     * Constructs a search term to match all events located in a <i>public</i> folder of the supplied list of visible folders.
     * 
     * @param visibleFolders The visible folders to consider
     * @return The search term, or <code>null</code> if no such folders are accessible at all
     */
    private static SearchTerm<?> getVisiblePublicFolderIdsTerm(List<CalendarFolder> visibleFolders) {
        List<String> visiblePublicFolderIds = visibleFolders.stream().filter(f -> PublicType.getInstance().equals(f.getType())).map(f -> f.getId()).toList();
        if (visiblePublicFolderIds.isEmpty()) {
            return null;
        }
        if (1 == visibleFolders.size()) {
            return getSearchTerm(EventField.FOLDER_ID, SingleOperation.EQUALS, visibleFolders.get(0).getId());
        }
        CompositeSearchTerm visiblePublicFolderIdsTerm = new CompositeSearchTerm(CompositeOperation.OR);
        visiblePublicFolderIds.forEach(id -> visiblePublicFolderIdsTerm.addSearchTerm(getSearchTerm(EventField.FOLDER_ID, SingleOperation.EQUALS, id)));
        return visiblePublicFolderIdsTerm;
    }
}
