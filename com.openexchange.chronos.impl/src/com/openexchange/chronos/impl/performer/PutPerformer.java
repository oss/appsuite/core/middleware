/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.impl.performer;

import static com.openexchange.chronos.common.CalendarUtils.find;
import static com.openexchange.chronos.common.CalendarUtils.getFolderView;
import static com.openexchange.chronos.common.CalendarUtils.hasExternalOrganizer;
import static com.openexchange.chronos.common.CalendarUtils.isInternal;
import static com.openexchange.chronos.common.CalendarUtils.isSeriesException;
import static com.openexchange.chronos.common.CalendarUtils.isSeriesMaster;
import static com.openexchange.chronos.common.CalendarUtils.matches;
import static com.openexchange.chronos.common.CalendarUtils.sortSeriesMasterFirst;
import static com.openexchange.chronos.impl.Utils.findByRecurrenceId;
import static com.openexchange.chronos.impl.Utils.getResolvableEntities;
import static com.openexchange.chronos.impl.Utils.prepareOrganizer;
import static com.openexchange.chronos.impl.Utils.updateEventsWithResult;
import static com.openexchange.tools.arrays.Collections.isNullOrEmpty;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import com.openexchange.chronos.Attendee;
import com.openexchange.chronos.CalendarObjectResource;
import com.openexchange.chronos.DelegatingEvent;
import com.openexchange.chronos.Event;
import com.openexchange.chronos.EventField;
import com.openexchange.chronos.Organizer;
import com.openexchange.chronos.common.CalendarUtils;
import com.openexchange.chronos.common.DefaultCalendarObjectResource;
import com.openexchange.chronos.common.DefaultEventUpdates;
import com.openexchange.chronos.exception.CalendarExceptionCodes;
import com.openexchange.chronos.impl.CalendarFolder;
import com.openexchange.chronos.impl.Check;
import com.openexchange.chronos.impl.InternalCalendarResult;
import com.openexchange.chronos.impl.InternalEventUpdate;
import com.openexchange.chronos.service.CalendarSession;
import com.openexchange.chronos.service.EventUpdate;
import com.openexchange.chronos.service.EventUpdates;
import com.openexchange.chronos.storage.CalendarStorage;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;

/**
 * {@link PutPerformer}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @since v7.10.6
 */
public class PutPerformer extends AbstractUpdatePerformer {

    /**
     * Initializes a new {@link PutPerformer}.
     *
     * @param storage The underlying calendar storage
     * @param session The calendar session
     * @param folder The calendar folder representing the current view on the events
     */
    public PutPerformer(CalendarStorage storage, CalendarSession session, CalendarFolder folder) throws OXException {
        super(storage, session, folder);
    }

    /**
     * Initializes a new {@link PutPerformer}.
     *
     * @param performer Another performer
     */
    public PutPerformer(AbstractUpdatePerformer performer) {
        super(performer);
    }

    /**
     * Puts a new or updated event from a calendar object resource to the calendar. In case the calendar resource already exists, the event
     * is updated or added to the resource implicitly if possible.
     *
     * @param eventData The event to store for the calendar object resource
     * @return The result
     */
    public InternalCalendarResult perform(Event eventData) throws OXException {
        return perform(Collections.singletonList(eventData), false);
    }

    /**
     * Puts a new or updated calendar object resource, i.e. an event and/or its change exceptions, to the calendar. In case the calendar
     * resource already exists under the perspective of the parent folder, it is updated implicitly: No longer indicated events are
     * removed, new ones are added, and existing ones are updated.
     *
     * @param resource The calendar object resource to store
     * @param replace <code>true</code> to automatically remove stored events that are no longer present in the supplied resource, <code>false</code>, otherwise
     * @return The result
     */
    public InternalCalendarResult perform(CalendarObjectResource resource, boolean replace) throws OXException {
        return perform(resource, replace, (EventField[]) null);
    }

    /**
     * Puts a new or updated calendar object resource, i.e. an event and/or its change exceptions, to the calendar. In case the calendar
     * resource already exists under the perspective of the parent folder, it is updated implicitly: No longer indicated events are
     * removed, new ones are added, and existing ones are updated.
     *
     * @param resource The calendar object resource to store
     * @param replace <code>true</code> to automatically remove stored events that are no longer present in the supplied resource, <code>false</code>, otherwise
     * @param ignoredFields Additional fields to ignore during update operations; {@link #SKIPPED_FIELDS} are always skipped
     * @return The result
     */
    public InternalCalendarResult perform(CalendarObjectResource resource, boolean replace, EventField... ignoredFields) throws OXException {
        return perform(resource.getEvents(), replace, ignoredFields);
    }

    /**
     * Puts new or updated events from a calendar object resource, i.e. an event and/or its change exceptions, to the calendar. In case
     * the calendar resource already exists under the perspective of the parent folder, it is updated (replaced) implicitly.
     *
     * @param eventData The list of events to store for the calendar object resource
     * @param replace <code>true</code> to remove events from the calendar object resource that are no longer present in the supplied collection, <code>false</code>, otherwise
     * @return The result
     */
    private InternalCalendarResult perform(List<Event> eventData, boolean replace) throws OXException {
        return perform(eventData, replace, (EventField[]) null);
    }

    /**
     * Puts new or updated events from a calendar object resource, i.e. an event and/or its change exceptions, to the calendar. In case
     * the calendar resource already exists under the perspective of the parent folder, it is updated (replaced) implicitly.
     *
     * @param eventData The list of events to store for the calendar object resource
     * @param replace <code>true</code> to remove events from the calendar object resource that are no longer present in the supplied collection, <code>false</code>, otherwise
     * @param ignoredFields Additional fields to ignore during update operations; {@link #SKIPPED_FIELDS} are always skipped
     * @return The result
     */
    private InternalCalendarResult perform(List<Event> eventData, boolean replace, EventField... ignoredFields) throws OXException {
        if (isNullOrEmpty(eventData)) {
            return resultTracker.getResult();
        }
        /*
         * extract & check designating properties from supplied events
         */
        List<Event> suppliedEvents = new ArrayList<Event>(eventData);
        Check.organizerMatches(null, suppliedEvents);
        String uid = Check.uidMatches(suppliedEvents);
        String filename = Check.filenameMatches(suppliedEvents);
        /*
         * lookup any existing events for this calendar object resource (with the same uid) & prepare to keep track of changes
         */
        List<Event> storedEvents = new ArrayList<Event>(sortSeriesMasterFirst(new ResolvePerformer(session, storage).lookupByUid(uid, calendarUserId, (EventField[]) null)));
        CalendarObjectResource originalResource = storedEvents.isEmpty() ? null : new DefaultCalendarObjectResource(storedEvents);
        List<EventUpdates> eventUpdates = new ArrayList<EventUpdates>();
        if (null != originalResource) {
            /*
             * ensure integrity of existing calendar object resource (same folder and optional filename), otherwise treat as conflict
             */
            for (Event storedEvent : storedEvents) {
                if (false == Objects.equals(getFolderView(storedEvent, calendarUserId), folder.getId()) ||
                    (Strings.isNotEmpty(filename) || storedEvent.containsFilename()) && false == Objects.equals(storedEvent.getFilename(), filename)) {
                    throw CalendarExceptionCodes.UID_CONFLICT.create(uid, getFolderView(storedEvent, calendarUserId), storedEvent.getId());
                }
            }
            if (false == hasExternalOrganizer(originalResource)) {
                /*
                 * for internally organized events, check & hide incoming sequence numbers beforehand to avoid conflicts with
                 * intermediate updates of series master event
                 */
                for (ListIterator<Event> iterator = suppliedEvents.listIterator(); iterator.hasNext();) {
                    Event suppliedEvent = iterator.next();
                    Event originalEvent = findByRecurrenceId(session, calendarUserId, storedEvents, suppliedEvent);
                    if (null != originalEvent) {
                        iterator.set(maskSequence(Check.requireInSequence(originalEvent, suppliedEvent)));
                    }
                }
            }
            if (replace) {
                /*
                 * delete previously existing, but no longer indicated events
                 */
                eventUpdates.add(deleteMissing(suppliedEvents, storedEvents, originalResource));
            }
        }
        /*
         * create or update events from calendar object resource one after the other
         */
        eventUpdates.add(createOrUpdate(suppliedEvents, storedEvents, ignoredFields));
        /*
         * handle scheduling based on aggregated results & return tracked result
         */
        handleScheduling(calendarUser, originalResource, new DefaultCalendarObjectResource(storedEvents), combine(eventUpdates));
        return resultTracker.getResult();
    }

    /**
     * Creates or updates events in the calendar object resource as indicated by the client. Performed changes are tracked, and the
     * supplied list of stored events is kept up to date implicitly.
     *
     * @param indicatedEvents The events as supplied by the client
     * @param storedEvents The originally stored events
     * @param ignoredFields Additional fields to ignore during update operations; {@link #SKIPPED_FIELDS} are always skipped
     * @return The updates performed for the underlying calendar object resource
     */
    private EventUpdates createOrUpdate(List<Event> indicatedEvents, List<Event> storedEvents, EventField... ignoredFields) throws OXException {
        List<EventUpdate> updatedEvents = new ArrayList<EventUpdate>();
        List<Event> deletedEvents = new ArrayList<Event>();
        List<Event> createdEvents = new ArrayList<Event>();
        for (Event suppliedEvent : sortSeriesMasterFirst(indicatedEvents)) {
            Event originalEvent = findByRecurrenceId(session, calendarUserId, storedEvents, suppliedEvent);
            if (null != originalEvent) {
                /*
                 * update for existing event
                 */
                LOG.debug("Updating {} using incoming {} for calendar object resource [{}] in {}.", originalEvent, suppliedEvent, originalEvent.getUid(), folder);
                Check.eventIsInFolder(originalEvent, folder, true);
                InternalEventUpdate eventUpdate = new UpdatePerformer(this).updateEvent(originalEvent, suppliedEvent, ignoredFields).getEventUpdate();
                updatedEvents.add(eventUpdate);
                deletedEvents.addAll(eventUpdate.getDeletedExceptions());
            } else if (null != suppliedEvent.getRecurrenceId()) {
                /*
                 * new change exception event; try and create recurrence based on existing series master event
                 */
                if (false == isNullOrEmpty(storedEvents) && isSeriesMaster(storedEvents.get(0))) {
                    Event originalSeriesMaster = storedEvents.get(0);
                    LOG.debug("Adding new exception with series master {} using incoming {} for calendar object resource [{}] in {}.",
                        originalSeriesMaster, suppliedEvent, originalSeriesMaster.getUid(), folder);
                    Check.eventIsInFolder(originalSeriesMaster, folder, true);
                    InternalEventUpdate eventUpdate = new UpdatePerformer(this).updateRecurrence(originalSeriesMaster, suppliedEvent.getRecurrenceId(), suppliedEvent, ignoredFields).getEventUpdate();
                    updatedEvents.add(eventUpdate);
                    deletedEvents.addAll(eventUpdate.getDeletedExceptions());
                } else {
                    /*
                     * new detached occurrence w/o known series master event; insert new orphaned series exception event, supplying those stored events with equal organizer
                     */
                    LOG.debug("Adding new orphaned exception w/o series master event using incoming {} for calendar object resource [{}] in {}.", suppliedEvent, suppliedEvent.getUid(), folder);
                    Organizer preparedOrganizer = prepareOrganizer(session, folder, suppliedEvent.getOrganizer(), getResolvableEntities(session, folder, suppliedEvent));
                    createdEvents.add(new CreatePerformer(this).createEvent(suppliedEvent, getEventsWithEqualOrganizer(storedEvents, preparedOrganizer)));
                }
            } else {
                /*
                 * new event (series master or non-recurring) for this calendar object resource, otherwise
                 */
                LOG.debug("Adding new event using incoming {} for calendar object resource [{}] in {}.", suppliedEvent, suppliedEvent.getUid(), folder);
                createdEvents.add(new CreatePerformer(this).createEvent(suppliedEvent, storedEvents));
            }
            /*
             * update list of stored events with the so-far changes
             */
            updateEventsWithResult(storedEvents, resultTracker.getResult().getPlainResult());
        }
        return new DefaultEventUpdates(createdEvents, deletedEvents, updatedEvents);
    }

    /**
     * Deletes all previously stored events that are no longer indicated by the client. Performed changes are tracked, and the supplied
     * list of stored events is kept up to date implicitly.
     *
     * @param indicatedEvents The events as supplied by the client
     * @param storedEvents The originally stored events
     * @param originalResource The original calendar object resource
     * @return The updates performed for the underlying calendar object resource
     */
    private EventUpdates deleteMissing(List<Event> indicatedEvents, List<Event> storedEvents, CalendarObjectResource originalResource) throws OXException {
        List<EventUpdate> updatedEvents = new ArrayList<EventUpdate>();
        List<Event> deletedEvents = new ArrayList<Event>();
        for (Event originalEvent : originalResource.getEvents()) {
            if (null == findByRecurrenceId(session, calendarUserId, indicatedEvents, originalEvent)) {
                /*
                 * event from original calendar object resource no longer indicated by client; proceed with deletion if still in intermediate resource
                 */
                Event storedEvent = find(storedEvents, originalEvent.getId());
                if (null == storedEvent) {
                    continue; // already processed
                }
                Event originalSeriesMaster = isSeriesException(storedEvent) ? originalResource.getSeriesMaster() : null;
                if (deleteRemovesEvent(storedEvent, storedEvent.getRecurrenceId())) {
                    /*
                     * deletion of not group-scheduled event / by organizer / last user attendee
                     */
                    LOG.debug("Deleting no longer indicated {} for calendar object resource [{}] in {}.", storedEvent, storedEvent.getUid(), folder);
                    deletedEvents.addAll(new DeletePerformer(this).deleteEvent(storedEvent, originalSeriesMaster));
                } else {
                    Attendee userAttendee = find(storedEvent.getAttendees(), calendarUserId);
                    if (null == userAttendee) {
                        continue; // not applicable
                    }
                    /*
                     * deletion as one of the attendees
                     */
                    LOG.debug("Deleting {} in no longer indicated {} for calendar object resource [{}] in {}.", userAttendee, storedEvent, storedEvent.getUid(), folder);
                    updatedEvents.addAll(new DeletePerformer(this).deleteEvent(storedEvent, originalSeriesMaster, userAttendee));
                }
                updateEventsWithResult(storedEvents, resultTracker.getResult().getPlainResult());
            }
        }
        return new DefaultEventUpdates(Collections.emptyList(), deletedEvents, updatedEvents);
    }

    /**
     * Combines a collection of multiple event updates into a single instance by combining each of the contained added/updated/removed item collections.
     * 
     * @param eventUpdates The event updates to combine
     * @return The combined event update
     */
    private static EventUpdates combine(List<EventUpdates> eventUpdates) {
        if (null != eventUpdates && 1 == eventUpdates.size()) {
            return eventUpdates.get(0);
        }
        List<Event> addedItems = new ArrayList<Event>();
        List<Event> removedItems = new ArrayList<Event>();
        List<EventUpdate> updatedItems = new ArrayList<EventUpdate>();
        if (null != eventUpdates) {
            for (EventUpdates updates : eventUpdates) {
                addedItems.addAll(updates.getAddedItems());
                removedItems.addAll(updates.getRemovedItems());
                updatedItems.addAll(updates.getUpdatedItems());
            }
        }
        return new DefaultEventUpdates(addedItems, removedItems, updatedItems);
    }

    private static Event maskSequence(Event delegate) {
        return new DelegatingEvent(delegate) {

            @Override
            public boolean containsSequence() {
                return false;
            }
        };
    }

    /**
     * Gets those events from the supplied event collection where the organizer is <i>equal</i> to the supplied organizer, in terms of
     * matching URI <b>and</b> entity identifier.
     * 
     * @param events The events to filter
     * @param organizer The organizer to match against
     * @return The events where the organizer is equal to the supplied one, or an empty list if there are none
     * @see CalendarUtils#matches(com.openexchange.chronos.CalendarUser, com.openexchange.chronos.CalendarUser)
     * @see CalendarUtils#isInternal(Organizer)
     */
    private static List<Event> getEventsWithEqualOrganizer(List<Event> events, Organizer organizer) {
        if (null == events || events.isEmpty()) {
            return Collections.emptyList();
        }
        List<Event> eventsWithSameOrganizer = new ArrayList<>(events.size());
        for (Event event : events) {
            if (isInternal(event.getOrganizer()) == isInternal(organizer) && matches(event.getOrganizer(), organizer)) {
                eventsWithSameOrganizer.add(event);
            }
        }
        return eventsWithSameOrganizer;
    }

}
