/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.impl.performer;

import static com.openexchange.chronos.common.CalendarUtils.isClassifiedFor;
import static com.openexchange.chronos.common.CalendarUtils.isSeriesEvent;
import static com.openexchange.chronos.common.CalendarUtils.isSeriesMaster;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.chronos.Attendee;
import com.openexchange.chronos.CalendarObjectResource;
import com.openexchange.chronos.CalendarUser;
import com.openexchange.chronos.CalendarUserType;
import com.openexchange.chronos.Classification;
import com.openexchange.chronos.DelegatingEvent;
import com.openexchange.chronos.Event;
import com.openexchange.chronos.common.CalendarUtils;
import com.openexchange.chronos.common.DefaultCalendarObjectResource;
import com.openexchange.chronos.exception.CalendarExceptionCodes;
import com.openexchange.chronos.impl.scheduling.AttachmentDataProviderImpl;
import com.openexchange.chronos.impl.scheduling.DefaultRecipientSettings;
import com.openexchange.chronos.scheduling.ScheduleStatus;
import com.openexchange.chronos.scheduling.SchedulingBroker;
import com.openexchange.chronos.scheduling.SchedulingMessage;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.chronos.scheduling.changes.ScheduleChange;
import com.openexchange.chronos.scheduling.changes.SchedulingChangeService;
import com.openexchange.chronos.scheduling.common.MessageBuilder;
import com.openexchange.chronos.service.CalendarParameters;
import com.openexchange.chronos.service.CalendarSession;
import com.openexchange.chronos.service.EventID;
import com.openexchange.chronos.storage.CalendarStorage;
import com.openexchange.exception.OXException;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.arrays.Collections;

/**
 * 
 * {@link ForwardPerformer} - Prepares the forwarding of event(s) to a given recipients
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class ForwardPerformer extends AbstractQueryPerformer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ForwardPerformer.class);

    private final ServiceLookup services;

    /**
     * Initializes a new {@link ForwardPerformer}.
     *
     * @param services The service lookup
     * @param session The calendar session
     * @param storage The underlying calendar storage
     */
    public ForwardPerformer(ServiceLookup services, CalendarSession session, CalendarStorage storage) {
        super(session, storage);
        this.services = services;
    }

    /**
     * Prepares the forwarding of the given event.
     * <p>
     * This method mainly loads the needed event data from the DB and checks if sending is allowed.
     * <p>
     * Please note that <b>NO</b> messages to the recipients are sent, yet, to avoid long running DB queries. This method
     * only prepares the messages used by the broker. The {@link SchedulingBroker} must be used to sent messages.
     *
     * @param eventID The identifier of the event to forward
     * @param recipients The recipients to sent the event to
     * @return A callback to obtain the sent status for each recipient
     * @throws OXException In case forwarding fails, i.e. the acting user hasn't enough permissions
     */
    public Map<CalendarUser, Function<SchedulingBroker, ScheduleStatus>> prepareForward(EventID eventID, List<CalendarUser> recipients) throws OXException {
        if (Collections.isNullOrEmpty(recipients)) {
            return Map.of();
        }
        /*
         * Load an check event data
         */
        Event event = new GetPerformer(session, storage).perform(eventID.getFolderID(), eventID.getObjectID(), eventID.getRecurrenceID());
        if (false == CalendarUtils.isGroupScheduled(event) || CalendarUtils.isPseudoGroupScheduled(event)) {
            throw CalendarExceptionCodes.EVENT_FORWARD_NOT_POSSIBLE.create(eventID.getObjectID(), eventID.getFolderID(), eventID.getRecurrenceID(), "Can't forward non-group-scheduled event(s).");
        }
        if (isClassifiedFor(event, session.getUserId()) && Classification.CONFIDENTIAL.matches(event.getClassification())) {
            throw CalendarExceptionCodes.EVENT_FORWARD_NOT_POSSIBLE.create(eventID.getObjectID(), eventID.getFolderID(), eventID.getRecurrenceID(), "Forwarding of confidential event(s) not allowed.");
        }
        /*
         * Prepare forward messages to be sent
         */
        Optional<Event> master = isSeriesEvent(event) || null != event.getRecurrenceId() ? optSeriesMaster(eventID.getFolderID(), event.getSeriesId()) : Optional.empty();
        CalendarObjectResource resource = prepareCalendarResource(event, eventID.getFolderID());
        return prepareForward(recipients, resource, getOriginator(), master);
    }

    /*
     * ============================== HELPERS ==============================
     */

    /**
     * Get the master event if accessible
     *
     * @param folderId The identifier of the folder representing the view on the event to load
     * @param seriesId The identifier of the event series to load the master event for
     * @return The optional master event
     * @throws OXException In case of error
     */
    private Optional<Event> optSeriesMaster(String folderId, String seriesId) throws OXException {
        try {
            return Optional.of(new GetPerformer(session, storage).perform(folderId, seriesId, null));
        } catch (OXException e) {
            if (CalendarExceptionCodes.NO_READ_PERMISSION.equals(e) || CalendarExceptionCodes.EVENT_NOT_FOUND_IN_FOLDER.equals(e)) {
                LOGGER.debug("Uanble to access master event", e);
                return Optional.empty();
            }
            throw e;
        }
    }

    /**
     * Prepares a {@link CalendarObjectResource} based on the given event.
     * <p>
     * Might load additional data in case of a series
     *
     * @param event The event
     * @param folderId The folder identifier
     * @return The {@link CalendarObjectResource}
     * @throws OXException If loading fails
     */
    private CalendarObjectResource prepareCalendarResource(Event event, String folderId) throws OXException {
        Event usedEvent = event;
        List<Event> exceptions = null;
        if (isSeriesMaster(event)) {
            /*
             * Load series data
             */
            if (Collections.isNotEmpty(event.getChangeExceptionDates())) {
                exceptions = new ChangeExceptionsPerformer(session, storage).perform(folderId, event.getSeriesId());
            }
        } else if (isSeriesEvent(event) && null != event.getRecurrenceId()) {
            /*
             * Instance of recurring event series, ensure to hide RRULE to avoid ambiguities
             */
            usedEvent = new DelegatingEvent(event) {

                @Override
                public boolean containsRecurrenceRule() {
                    return false;
                }

                @Override
                public String getRecurrenceRule() {
                    return null;
                }
            };
        }
        return CalendarUtils.injectComment(new DefaultCalendarObjectResource(usedEvent, exceptions), getComment());
    }

    private String getComment() {
        return session.get(CalendarParameters.PARAMETER_COMMENT, String.class);
    }

    /**
     * Get the originator for the forward messages.
     * <p>
     * The originator is always the current session user.
     * 
     * @return The originator of the forward messages
     * @throws OXException In case the originator can't be loaded
     */
    private CalendarUser getOriginator() throws OXException {
        return session.getEntityResolver().applyEntityData(new CalendarUser(), session.getUserId());
    }

    /**
     * Prepares messages to forward with the given resource to the recipients
     *
     * @param recipients The recipients
     * @param resource The resource to send
     * @param originator The originator of the forward messages
     * @param master The optional master event, in case an instance of an event series is forwarded, or empty if not applicable
     * @param folderId The folder identifier
     * @return A callback to obtain the sent status for each recipient
     * @throws OXException In case of error
     */
    private Map<CalendarUser, Function<SchedulingBroker, ScheduleStatus>> prepareForward(List<CalendarUser> recipients, CalendarObjectResource resource, CalendarUser originator, Optional<Event> master) throws OXException {
        Map<CalendarUser, Function<SchedulingBroker, ScheduleStatus>> recipientToStatus = LinkedHashMap.newLinkedHashMap(recipients.size());
        for (CalendarUser recipient : recipients) {
            recipientToStatus.put(recipient, prepareForwardCallback(resource, originator, recipient, master));
        }
        return recipientToStatus;
    }

    /**
     * Prepares the message to be sent and returns them as function for later invocation with the scheduling broker.
     * 
     * @param resource The resource to prepare the message for
     * @param originator The originator of the message
     * @param recipient The recipient of the message
     * @param master The optional master event, in case an instance of an event series is forwarded, or empty if not applicable
     * @param folderId The folder identifier
     * @return A callback for message sending
     * @throws OXException In case of error
     */
    private Function<SchedulingBroker, ScheduleStatus> prepareForwardCallback(CalendarObjectResource resource, CalendarUser originator, CalendarUser recipient, Optional<Event> master) throws OXException {
        /*
         * Resolve and implicitly check recipient
         */
        CalendarUser resolvedRecipient;
        try {
            if (recipient.getEntity() > 0) {
                resolvedRecipient = session.getEntityResolver().prepareUserAttendee(recipient.getEntity());
            } else {
                resolvedRecipient = session.getEntityResolver().prepare(recipient, CalendarUserType.INDIVIDUAL);
            }
        } catch (OXException e) {
            if (false == CalendarExceptionCodes.INVALID_CALENDAR_USER.equals(e)) {
                throw e;
            }
            return s -> ScheduleStatus.UNKOWN_CALENDAR_USER;
        }
        /*
         * Check that recipient isn't already attending
         */
        if (null != CalendarUtils.find(resource, resolvedRecipient)) {
            return s -> ScheduleStatus.REJECTED;
        }
        /*
         * Prepare message and create the callback
         */
        SchedulingMessage message = createForward(resource, recipient, originator, master);
        return s -> s.handleScheduling(session.getSession(), List.of(message)).get(0);
    }

    /*
     * ============================== scheduling message ==============================
     */

    /**
     * Creates a {@link SchedulingMessage} for a forwarded calendar object resource
     *
     * @param resource The resource to send
     * @param recipient The recipient to sent the forward message to
     * @param originator The originator of the message
     * @param master The optional master event, in case an instance of an event series is forwarded, or empty if not applicable
     * @return The {@link SchedulingMessage}
     * @throws OXException In case of error
     */
    public SchedulingMessage createForward(CalendarObjectResource resource, CalendarUser recipient, CalendarUser originator, Optional<Event> master) throws OXException {
        if (recipient instanceof Attendee attendee) {
            return createForward(resource, originator, attendee, attendee.getCuType(), master);
        }
        return createForward(resource, originator, recipient, CalendarUserType.INDIVIDUAL, master);
    }

    private SchedulingMessage createForward(CalendarObjectResource resource, CalendarUser originator, CalendarUser recipient, CalendarUserType recipientType, Optional<Event> master) throws OXException {
        return new MessageBuilder()
                                   .setMethod(SchedulingMethod.REQUEST)
                                   .setOriginator(originator)
                                   .setRecipient(recipient)
                                   .setResource(resource)
                                   .setScheduleChange(describeForward(resource, originator, master))
                                   .setAttachmentDataProvider(new AttachmentDataProviderImpl(services, session.getContextId()))
                                   .setRecipientSettings(new DefaultRecipientSettings(services, session, originator, recipient, recipientType, resource))
                                   .setAdditionals(Map.of(SchedulingMessage.KEY_FORWARD, Boolean.TRUE)) // Set flag to identify forward messages
                                   .build();
    }

    private ScheduleChange describeForward(CalendarObjectResource resource, CalendarUser originator, Optional<Event> master) throws OXException {
        if (null != resource.getSeriesMaster() || false == CalendarUtils.isSeriesEvent(resource.getFirstEvent()) || master.isEmpty()) {
            return services.getServiceSafe(SchedulingChangeService.class).describeForwardRequest(originator, getComment(), resource);
        }
        return services.getServiceSafe(SchedulingChangeService.class).describeForwardInstance(originator, getComment(), resource, master.get());
    }

}
