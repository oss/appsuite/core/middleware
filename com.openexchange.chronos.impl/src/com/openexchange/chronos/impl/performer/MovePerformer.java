/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.impl.performer;

import static com.openexchange.chronos.common.CalendarUtils.contains;
import static com.openexchange.chronos.common.CalendarUtils.filter;
import static com.openexchange.chronos.common.CalendarUtils.find;
import static com.openexchange.chronos.common.CalendarUtils.hasExternalOrganizer;
import static com.openexchange.chronos.common.CalendarUtils.isGroupScheduled;
import static com.openexchange.chronos.common.CalendarUtils.isPseudoGroupScheduled;
import static com.openexchange.chronos.common.CalendarUtils.isSeriesException;
import static com.openexchange.chronos.common.CalendarUtils.isSeriesMaster;
import static com.openexchange.chronos.common.CalendarUtils.matches;
import static com.openexchange.chronos.impl.Check.classificationIsValidOnMove;
import static com.openexchange.chronos.impl.Check.requireCalendarPermission;
import static com.openexchange.chronos.impl.Check.requireUpToDateTimestamp;
import static com.openexchange.chronos.impl.Utils.getCalendarUser;
import static com.openexchange.chronos.impl.Utils.prepareOrganizer;
import static com.openexchange.folderstorage.Permission.CREATE_OBJECTS_IN_FOLDER;
import static com.openexchange.folderstorage.Permission.DELETE_ALL_OBJECTS;
import static com.openexchange.folderstorage.Permission.DELETE_OWN_OBJECTS;
import static com.openexchange.folderstorage.Permission.NO_PERMISSIONS;
import static com.openexchange.folderstorage.Permission.READ_FOLDER;
import static com.openexchange.folderstorage.Permission.WRITE_OWN_OBJECTS;
import static com.openexchange.java.Autoboxing.I;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import com.openexchange.chronos.Alarm;
import com.openexchange.chronos.Attendee;
import com.openexchange.chronos.AttendeeField;
import com.openexchange.chronos.CalendarUser;
import com.openexchange.chronos.CalendarUserType;
import com.openexchange.chronos.DefaultAttendeePrivileges;
import com.openexchange.chronos.DelegatingEvent;
import com.openexchange.chronos.Event;
import com.openexchange.chronos.Organizer;
import com.openexchange.chronos.common.CalendarUtils;
import com.openexchange.chronos.common.mapping.AttendeeMapper;
import com.openexchange.chronos.exception.CalendarExceptionCodes;
import com.openexchange.chronos.impl.CalendarFolder;
import com.openexchange.chronos.impl.Consistency;
import com.openexchange.chronos.impl.InternalAttendeeUpdates;
import com.openexchange.chronos.impl.InternalCalendarResult;
import com.openexchange.chronos.impl.Utils;
import com.openexchange.chronos.service.CalendarSession;
import com.openexchange.chronos.storage.CalendarStorage;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.type.PublicType;

/**
 * {@link MovePerformer}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @since v7.10.0
 */
public class MovePerformer extends AbstractUpdatePerformer {

    /**
     * Initializes a new {@link MovePerformer}.
     *
     * @param storage The underlying calendar storage
     * @param session The calendar session
     * @param folder The calendar folder representing the current view on the events
     */
    public MovePerformer(CalendarStorage storage, CalendarSession session, CalendarFolder folder) throws OXException {
        super(storage, session, folder);
    }

    /**
     * Performs the move of an event to another folder. No further updates are processed.
     *
     * @param objectId The identifier of the event to move
     * @param targetFolder The target folder to move the event to
     * @param clientTimestamp The client timestamp to catch concurrent modifications
     * @return The result
     */
    public InternalCalendarResult perform(String objectId, CalendarFolder targetFolder, long clientTimestamp) throws OXException {
        /*
         * load original event & check current session user's permissions
         */
        List<Event> originalEvents = loadAndCheckEvents(objectId, targetFolder, clientTimestamp);
        /*
         * perform move operation based on parent folder types
         */
        if (PublicType.getInstance().equals(folder.getType()) && PublicType.getInstance().equals(targetFolder.getType())) {
            moveBetweenPublicFolders(originalEvents, targetFolder);
        } else if (false == PublicType.getInstance().equals(folder.getType()) && false == PublicType.getInstance().equals(targetFolder.getType())) {
            if (matches(calendarUser, getCalendarUser(session, targetFolder))) {
                moveBetweenPersonalFoldersOfSameUser(originalEvents, targetFolder);
            } else {
                moveBetweenPersonalFoldersOfDifferentUsers(originalEvents, targetFolder);
            }
        } else if (PublicType.getInstance().equals(folder.getType()) && false == PublicType.getInstance().equals(targetFolder.getType())) {
            moveFromPublicToPersonalFolder(originalEvents, targetFolder);
        } else if (false == PublicType.getInstance().equals(folder.getType()) && PublicType.getInstance().equals(targetFolder.getType())) {
            moveFromPersonalToPublicFolder(originalEvents, targetFolder);
        } else {
            throw new UnsupportedOperationException("Move not implemented from " + folder.getType() + " to " + targetFolder.getType());
        }
        renewAlarmTriggers(originalEvents);
        return resultTracker.getResult();
    }

    /**
     * Loads all events targeted by the move operations and checks different preconditions for moving them to the designated target folder.
     *
     * @param objectId The identifier of the event or event series to move
     * @param targetFolder The folder to move to
     * @param clientTimestamp The client timestamp to catch concurrent modifications
     * @return A list of events to move
     * @throws OXException In case preconditions are not met
     */
    private List<Event> loadAndCheckEvents(String objectId, CalendarFolder targetFolder, long clientTimestamp) throws OXException {
        /*
         * load targeted event
         */
        Event originalEvent = loadEventData(objectId);
        if (isSeriesException(originalEvent)) {
            throw CalendarExceptionCodes.MOVE_OCCURRENCE_NOT_SUPPORTED.create(originalEvent.getId(), folder.getId(), originalEvent.getId());
        }
        /*
         * load and check single event or whole series (including "hidden" events)
         */
        List<Event> loadedEvents = (isSeriesMaster(originalEvent) ? Stream.concat(Stream.of(originalEvent), loadExceptionData(originalEvent).stream()) : Stream.of(originalEvent))
            .filter(e -> Utils.isInFolder(e, folder, true)).toList();
        if (loadedEvents.isEmpty()) {
            throw CalendarExceptionCodes.EVENT_NOT_FOUND_IN_FOLDER.create(folder.getId(), objectId);
        }
        /*
         * perform further checks prior returning final list of events
         */
        requireCalendarPermission(targetFolder, CREATE_OBJECTS_IN_FOLDER, NO_PERMISSIONS, WRITE_OWN_OBJECTS, NO_PERMISSIONS);
        for (Event event : loadedEvents) {
            requireUpToDateTimestamp(event, clientTimestamp);
            if (matches(event.getCreatedBy(), session.getUserId())) {
                requireCalendarPermission(folder, READ_FOLDER, NO_PERMISSIONS, NO_PERMISSIONS, DELETE_OWN_OBJECTS);
            } else {
                requireCalendarPermission(folder, READ_FOLDER, NO_PERMISSIONS, NO_PERMISSIONS, DELETE_ALL_OBJECTS);
            }
            /*
             * check if move is supported
             */
            classificationIsValidOnMove(event.getClassification(), folder, targetFolder);
        }
        return loadedEvents;
    }

    private void moveFromPersonalToPublicFolder(List<Event> originalEvents, CalendarFolder targetFolder) throws OXException {
        /*
         * move from personal to public folder, take over common public folder identifier for user attendees & update any existing alarms
         */
        requireWritePermissions(originalEvents.getFirst());
        for (Event e : originalEvents) {
            /*
             * Remove per attendee folder ids and update alarms
             */
            Map<Integer, List<Alarm>> originalAlarms = storage.getAlarmStorage().loadAlarms(e);
            for (Attendee originalAttendee : filter(e.getAttendees(), Boolean.TRUE, CalendarUserType.INDIVIDUAL)) {
                updateAttendeeFolderId(e, originalAttendee, null);
                updateAttendeeAlarms(e, targetFolder.getId(), originalAttendee.getEntity(), originalAlarms);
            }
            /*
             * take over new common folder id, touch event & reset calendar user
             */
            updateCommonFolderId(e, targetFolder.getId());
            updateCalendarUser(e, null);
            /*
             * Remove attendee privileges
             */
            removeAttendeePrivileges(e);
        }
    }

    private void moveFromPublicToPersonalFolder(List<Event> originalEvents, CalendarFolder targetFolder) throws OXException {
        /*
         * move from public to personal folder, require same organizer for group-scheduled events
         */
        Event originalEvent = originalEvents.getFirst();
        requireWritePermissions(originalEvent);
        CalendarUser targetCalendarUser = getCalendarUser(session, targetFolder);
        if (isGroupScheduled(originalEvent) && false == matches(targetCalendarUser, originalEvent.getOrganizer())) {
            throw CalendarExceptionCodes.NOT_ORGANIZER.create(targetFolder.getId(), originalEvent.getId(), targetCalendarUser.getUri(), targetCalendarUser.getSentBy());
        }
        for (Event e : originalEvents) {
            /*
             * take over default personal folders for user attendees & update any existing alarms
             */
            Map<Integer, List<Alarm>> originalAlarms = storage.getAlarmStorage().loadAlarms(e);
            for (Attendee originalAttendee : filter(e.getAttendees(), Boolean.TRUE, CalendarUserType.INDIVIDUAL)) {
                String folderId = matches(targetCalendarUser, originalAttendee) ? targetFolder.getId() : getDefaultCalendarId(originalAttendee.getEntity());
                updateAttendeeFolderId(e, originalAttendee, folderId);
                updateAttendeeAlarms(e, folderId, originalAttendee.getEntity(), originalAlarms);
            }
            /*
             * ensure to add default calendar user if not already present
             */
            ensureDefaultAttendee(targetFolder, targetCalendarUser, e);
            /*
             * remove previous common folder id from event, touch event & assign calendar user
             */
            updateCommonFolderId(e, null);
            updateCalendarUser(e, targetCalendarUser);
        }
    }

    private void moveBetweenPublicFolders(List<Event> originalEvents, CalendarFolder targetFolder) throws OXException {
        /*
         * move from one public folder to another, update event's common folder & update any existing alarms
         */
        requireWritePermissions(originalEvents.getFirst());
        for (Event e : originalEvents) {
            updateAttendeeAlarms(e, targetFolder.getId(), -1, storage.getAlarmStorage().loadAlarms(e));
            updateCommonFolderId(e, targetFolder.getId());
        }
    }

    private void moveBetweenPersonalFoldersOfSameUser(List<Event> originalEvents, CalendarFolder targetFolder) throws OXException {
        for (Event e : originalEvents) {
            /*
             * move from one personal folder to another of the same user
             */
            if (isGroupScheduled(e)) {
                /*
                 * update attendee's folder in a group-scheduled event
                 */
                Attendee originalAttendee = find(e.getAttendees(), calendarUserId);
                if (null == originalAttendee) {
                    throw CalendarExceptionCodes.ATTENDEE_NOT_FOUND.create(I(calendarUserId), e.getId());
                }
                updateAttendeeFolderId(e, originalAttendee, targetFolder.getId());
                touch(e);
            } else {
                /*
                 * update event's common folder id, otherwise
                 */
                updateCommonFolderId(e, targetFolder.getId());
            }
            updateAttendeeAlarms(e, targetFolder.getId(), calendarUserId, storage.getAlarmStorage().loadAlarms(e));
        }
    }

    private void moveBetweenPersonalFoldersOfDifferentUsers(List<Event> originalEvents, CalendarFolder targetFolder) throws OXException {
        for (Event e : originalEvents) {
            /*
             * move between personal calendar folders
             */
            if (false == isGroupScheduled(e)) {
                /*
                 * update event's common folder id, assign new calendar user & remove alarms of previous calendar user
                 */
                updateCommonFolderId(e, targetFolder.getId());
                updateCalendarUser(e, getCalendarUser(session, targetFolder));
                storage.getAlarmStorage().deleteAlarms(e.getId(), calendarUserId);
            } else if (isPseudoGroupScheduled(e)) {
                updatePseudoGroupScheduledEvent(e, targetFolder);
            } else {
                /*
                 * not allowed/supported, otherwise
                 */
                throw CalendarExceptionCodes.UNSUPPORTED_FOLDER.create(targetFolder.getId(), targetFolder.getType());
            }
        }
    }

    private void updatePseudoGroupScheduledEvent(Event originalEvent, CalendarFolder targetFolder) throws OXException {
        /*
         * exchange default attendee & organizer, remove alarms of previous calendar user
         */
        Attendee originalAttendee = find(originalEvent.getAttendees(), calendarUserId);
        if (null == originalAttendee) {
            throw CalendarExceptionCodes.ATTENDEE_NOT_FOUND.create(I(calendarUserId), originalEvent.getId());
        }
        storage.getAttendeeStorage().insertAttendeeTombstone(originalEvent.getId(), storage.getUtilities().getTombstone(originalAttendee));
        storage.getEventStorage().insertEventTombstone(storage.getUtilities().getTombstone(originalEvent, timestamp, calendarUser));
        storage.getAttendeeStorage().deleteAttendees(originalEvent.getId(), Collections.singletonList(originalAttendee));
        storage.getAlarmStorage().deleteAlarms(originalEvent.getId(), originalAttendee.getEntity());
        ensureDefaultAttendee(targetFolder, null, originalEvent);
        updateOrganizer(originalEvent, targetFolder);
    }

    private void updateAttendeeFolderId(Event originalEvent, Attendee originalAttendee, String folderId) throws OXException {
        if ((null == folderId && null != originalAttendee.getFolderId()) || (null != folderId && false == folderId.equals(originalAttendee.getFolderId()))) {
            storage.getEventStorage().insertEventTombstone(storage.getUtilities().getTombstone(originalEvent, timestamp, calendarUser));
            Attendee attendeeUpdate = AttendeeMapper.getInstance().copy(originalAttendee, null, AttendeeField.ENTITY, AttendeeField.MEMBER, AttendeeField.CU_TYPE, AttendeeField.URI);
            attendeeUpdate.setFolderId(folderId);
            storage.getAttendeeStorage().insertAttendeeTombstone(originalEvent.getId(), storage.getUtilities().getTombstone(originalAttendee));
            storage.getAttendeeStorage().updateAttendee(originalEvent.getId(), attendeeUpdate);
        }
    }

    private void updateCommonFolderId(Event originalEvent, String folderId) throws OXException {
        if ((null == folderId && null != originalEvent.getFolderId()) || (null != folderId && false == folderId.equals(originalEvent.getFolderId()))) {
            Event eventUpdate = new Event();
            eventUpdate.setId(originalEvent.getId());
            eventUpdate.setFolderId(folderId);
            Consistency.setModified(session, timestamp, eventUpdate, session.getUserId(), hasExternalOrganizer(originalEvent));
            storage.getEventStorage().insertEventTombstone(storage.getUtilities().getTombstone(originalEvent, timestamp, calendarUser));
            storage.getEventStorage().updateEvent(eventUpdate);
        }
    }

    private void updateCalendarUser(Event originalEvent, CalendarUser newCalendarUser) throws OXException {
        if (false == matches(newCalendarUser, originalEvent.getCalendarUser())) {
            Event eventUpdate = new Event();
            eventUpdate.setId(originalEvent.getId());
            eventUpdate.setCalendarUser(newCalendarUser);
            Consistency.setModified(session, timestamp, eventUpdate, session.getUserId(), hasExternalOrganizer(originalEvent));
            storage.getEventStorage().updateEvent(eventUpdate);
        }
    }

    private void updateOrganizer(Event originalEvent, CalendarFolder targetFolder) throws OXException {
        if (false == matches(getCalendarUser(session, targetFolder), originalEvent.getOrganizer())) {
            Event eventUpdate = new Event();
            eventUpdate.setId(originalEvent.getId());
            Organizer organizer = prepareOrganizer(session, targetFolder, null, null);
            eventUpdate.setOrganizer(organizer);
            Consistency.setModified(session, timestamp, eventUpdate, session.getUserId(), hasExternalOrganizer(originalEvent));
            storage.getEventStorage().updateEvent(eventUpdate);
        }
    }

    /**
     * Updates the timestamp in all alarms for the given event
     *
     * @param originalEvent The event to update alarms for
     * @param folderId The folder identifier the event is moved to
     * @param userId The optional user identifier to update the alarms for, <code>0</code> or less indicates all users
     * @param originalAlarms The alarms to update, grouped by user ids
     * @throws OXException In case of error
     */
    private void updateAttendeeAlarms(Event originalEvent, String folderId, int userId, Map<Integer, List<Alarm>> originalAlarms) throws OXException {
        if (userId > 0) {
            updateAttendeeAlarms(originalEvent, originalAlarms.get(I(userId)), userId, folderId);
            return;
        }
        for (Map.Entry<Integer, List<Alarm>> entry : originalAlarms.entrySet()) {
            updateAttendeeAlarms(originalEvent, entry.getValue(), entry.getKey().intValue(), folderId);
        }
    }

    private void updateAttendeeAlarms(Event originalEvent, List<Alarm> originalAlarms, int userId, final String folderId) throws OXException {
        if (null != originalAlarms && 0 < originalAlarms.size()) {
            Event userizedEvent = new DelegatingEvent(originalEvent) {

                @Override
                public String getFolderId() {
                    return folderId;
                }

                @Override
                public boolean containsFolderId() {
                    return true;
                }
            };
            originalAlarms.stream().forEach(a -> a.setTimestamp(System.currentTimeMillis()));
            storage.getAlarmStorage().updateAlarms(userizedEvent, userId, originalAlarms);
        }
    }

    private void removeAttendeePrivileges(Event event) throws OXException {
        if (null != event.getAttendeePrivileges() && false == CalendarUtils.hasAttendeePrivileges(event, DefaultAttendeePrivileges.DEFAULT)) {
            Event eventUpdate = new Event();
            eventUpdate.setId(event.getId());
            Consistency.setModified(session, timestamp, eventUpdate, session.getUserId(), hasExternalOrganizer(event));
            eventUpdate.setAttendeePrivileges(DefaultAttendeePrivileges.DEFAULT);
            storage.getEventStorage().updateEvent(eventUpdate);
        }
    }

    /**
     * Gets the identifier of a specific user's default personal calendar folder.
     *
     * @param userId The identifier of the user to retrieve the default calendar identifier for
     * @return The default calendar folder identifier
     */
    private String getDefaultCalendarId(int userId) throws OXException {
        return session.getConfig().getDefaultFolderId(userId);
    }

    /**
     * Ensures that the calendar user is an attendee of the event
     *
     * @param targetFolder The folder
     * @param targetCalendarUser The user to ensure
     * @param event The event to ensure participation of the user in
     * @throws OXException
     */
    private void ensureDefaultAttendee(CalendarFolder targetFolder, CalendarUser targetCalendarUser, Event event) throws OXException {
        if (null == targetCalendarUser || false == contains(event.getAttendees(), targetCalendarUser)) {
            Attendee defaultAttendee = InternalAttendeeUpdates.getDefaultAttendee(session, targetFolder, null, timestamp);
            storage.getAttendeeStorage().insertAttendees(event.getId(), Collections.singletonList(defaultAttendee));
        }
    }

    private void renewAlarmTriggers(List<Event> originalEvents) throws OXException {
        for (Event event : originalEvents) {
            /*
             * rewrite any alarm triggers, track & return result
             */
            storage.getAlarmTriggerStorage().deleteTriggers(event.getId());
            Event updatedEvent = loadEventData(event.getId());
            storage.getAlarmTriggerStorage().insertTriggers(updatedEvent, storage.getAlarmStorage().loadAlarms(updatedEvent));
            resultTracker.trackUpdate(event, updatedEvent);
        }
    }
}
