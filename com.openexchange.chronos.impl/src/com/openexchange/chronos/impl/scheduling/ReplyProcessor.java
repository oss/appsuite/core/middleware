/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.impl.scheduling;

import static com.openexchange.chronos.common.CalendarUtils.find;
import static com.openexchange.chronos.common.CalendarUtils.hasExternalOrganizer;
import static com.openexchange.chronos.common.CalendarUtils.isInternal;
import static com.openexchange.chronos.common.CalendarUtils.isOrganizer;
import static com.openexchange.chronos.common.CalendarUtils.isSeriesMaster;
import static com.openexchange.chronos.common.CalendarUtils.matches;
import static com.openexchange.chronos.common.CalendarUtils.sortSeriesMasterFirst;
import static com.openexchange.chronos.impl.Utils.findByRecurrenceId;
import static com.openexchange.chronos.impl.Utils.updateEventsWithResult;
import static com.openexchange.java.Autoboxing.I;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import com.openexchange.chronos.Attendee;
import com.openexchange.chronos.AttendeeField;
import com.openexchange.chronos.CalendarUser;
import com.openexchange.chronos.CalendarUserType;
import com.openexchange.chronos.DelegatingEvent;
import com.openexchange.chronos.Event;
import com.openexchange.chronos.EventField;
import com.openexchange.chronos.Organizer;
import com.openexchange.chronos.common.CalendarUtils;
import com.openexchange.chronos.common.DefaultCalendarObjectResource;
import com.openexchange.chronos.common.DefaultEventUpdates;
import com.openexchange.chronos.common.EventOccurrence;
import com.openexchange.chronos.common.mapping.AttendeeMapper;
import com.openexchange.chronos.exception.CalendarExceptionCodes;
import com.openexchange.chronos.impl.CalendarFolder;
import com.openexchange.chronos.impl.Check;
import com.openexchange.chronos.impl.InternalCalendarResult;
import com.openexchange.chronos.impl.InternalEventUpdate;
import com.openexchange.chronos.impl.performer.AbstractUpdatePerformer;
import com.openexchange.chronos.impl.performer.ResolvePerformer;
import com.openexchange.chronos.impl.performer.UpdatePerformer;
import com.openexchange.chronos.scheduling.ITipSequence;
import com.openexchange.chronos.scheduling.IncomingSchedulingMessage;
import com.openexchange.chronos.scheduling.SchedulingSource;
import com.openexchange.chronos.service.CalendarSession;
import com.openexchange.chronos.service.EventUpdate;
import com.openexchange.chronos.storage.CalendarStorage;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.tools.mappings.common.DefaultItemUpdate;
import com.openexchange.java.Lists;

/**
 * {@link ReplyProcessor} - Handles incoming <code>REPLY</code> message by external calendar users and tries to apply
 * the changes transmitted in the message.
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @since v7.10.4
 * @see <a href="https://tools.ietf.org/html/rfc5546#section-3.2.3">RFC5546 Section 3.2.3</a>
 */
public class ReplyProcessor extends AbstractUpdatePerformer {

    private static final AttendeeField[] UPDATE_FIELDS = { AttendeeField.COMMENT, AttendeeField.EXTENDED_PARAMETERS, AttendeeField.PARTSTAT, AttendeeField.SENT_BY };

    private static final AttendeeField[] ADDITIONAL_FIELDS = { AttendeeField.CN, AttendeeField.EMAIL, AttendeeField.URI, AttendeeField.CU_TYPE };

    private final SchedulingSource source;

    /**
     * Initializes a new {@link ReplyProcessor}.
     *
     * @param storage The underlying calendar storage
     * @param session The calendar session
     * @param folder The calendar folder representing the current view on the events
     * @param source The source from which the scheduling has been triggered
     * @throws OXException If initialization fails
     */
    public ReplyProcessor(CalendarStorage storage, CalendarSession session, CalendarFolder folder, SchedulingSource source) throws OXException {
        super(storage, session, folder);
        this.source = source;
    }

    /**
     * Performs an update on an event by either
     * <li> taking over the attendees new participant status</li>
     * or
     * <li>adding an unknown calendar user to the event. This will trigger new messages to inform all other attendees.</li>
     *
     * @param message The {@link IncomingSchedulingMessage}
     * @return An {@link InternalCalendarResult} containing the changes that has been performed
     * @throws OXException In case data is invalid, outdated or permissions are missing
     */
    public InternalCalendarResult process(IncomingSchedulingMessage message) throws OXException {
        /*
         * extract & check designating properties from supplied events
         */
        List<Event> suppliedEvents = sortSeriesMasterFirst(new ArrayList<Event>(message.getResource().getEvents()));
        Check.organizerMatches(null, suppliedEvents);
        String uid = Check.uidMatches(suppliedEvents);
        CalendarUser originator = message.getSchedulingObject().getOriginator();
        /*
         * lookup any existing events for this calendar object resource (with the same uid)
         */
        List<Event> storedEvents = new ArrayList<Event>(sortSeriesMasterFirst(new ResolvePerformer(session, storage).lookupByUid(uid, calendarUserId, (EventField[]) null)));
        if (storedEvents.isEmpty()) {
            throw CalendarExceptionCodes.EVENT_NOT_FOUND_IN_FOLDER.create(folder.getId(), uid);
        }
        DefaultCalendarObjectResource originalResource = new DefaultCalendarObjectResource(storedEvents);
        /*
         * take over replies from originator of each event from scheduling message & collect updates
         */
        List<EventUpdate> eventUpdates = new LinkedList<EventUpdate>();
        for (Event suppliedEvent : suppliedEvents) {
            /*
             * mask timestamp and sequence of supplied event, after first REPLY to series master event has been applied 
             */
            if (false == suppliedEvent.equals(suppliedEvents.get(0)) && null == suppliedEvents.get(0).getRecurrenceId()) {
                suppliedEvent = hideSequenceAndDTStamp(suppliedEvent);
            }
            /*
             * apply the reply and propagate to intermediate results
             */
            Optional<InternalEventUpdate> optEventUpdate = applyReply(originator, suppliedEvent, storedEvents);
            if (optEventUpdate.isPresent()) {
                touchSeriesMasterIfNeeded(storedEvents);
                updateEventsWithResult(storedEvents, resultTracker.getResult().getPlainResult());
                eventUpdates.add(optEventUpdate.get());
            }
        }
        /*
         * handle scheduling based on aggregated results & return tracked result
         */
        handleScheduling(originator, originalResource, new DefaultCalendarObjectResource(storedEvents), new DefaultEventUpdates(null, null, eventUpdates));
        return resultTracker.getResult();
    }

    /**
     * Applies one or more <code>REPLY</code> messages from an incoming scheduling object resource.
     * 
     * @param originator The originator of the scheduling message
     * @param suppliedEvent The events of the incoming scheduling message
     * @param storedEvents The currently stored events of the targeted calendar object resource
     * @return The performed event update, or {@link Optional#empty()} if nothing was changed
     * @throws OXException If operation fails
     */
    private Optional<InternalEventUpdate> applyReply(CalendarUser originator, Event suppliedEvent, List<Event> storedEvents) throws OXException {
        /*
         * lookup targeted original event & apply the reply there
         */
        Event originalEvent = findByRecurrenceId(session, calendarUserId, storedEvents, suppliedEvent);
        if (null != originalEvent) {
            /*
             * reply for existing event, prepare event with attendee changes & perform update
             */
            Optional<Event> eventToUpdate = prepareEventToUpdate(originalEvent, suppliedEvent, getReplyingAttendee(suppliedEvent, originator));
            if (eventToUpdate.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new UpdatePerformer(this).updateEvent(originalEvent, eventToUpdate.get(), isOrganizer(originalEvent, calendarUserId)).getEventUpdate());
        }
        if (null != suppliedEvent.getRecurrenceId() && null == suppliedEvent.getRecurrenceId().getRange() && isSeriesMaster(storedEvents.get(0))) {
            /*
             * reply for non-overridden instance of series, prepare event with attendee changes & perform recurrence update
             */
            Event originalSeriesMaster = storedEvents.get(0);
            Optional<Event> recurrenceToUpdate = prepareEventToUpdate(new EventOccurrence(originalSeriesMaster, suppliedEvent.getRecurrenceId()), suppliedEvent, getReplyingAttendee(suppliedEvent, originator));
            if (recurrenceToUpdate.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(new UpdatePerformer(this).updateRecurrence(
                originalSeriesMaster, suppliedEvent.getRecurrenceId(), recurrenceToUpdate.get(), isOrganizer(originalSeriesMaster, calendarUserId)).getEventUpdate());
        }
        /*
         * unknown event, otherwise
         */
        throw CalendarExceptionCodes.EVENT_NOT_FOUND_IN_FOLDER.create(folder, suppliedEvent.getUid());
    }

    /**
     * Prepares an event object containing the updated attendee representing the reply.
     * <p/>
     * This is either an update of the participation status for an already attending participant, or a newly added attendee in case of
     * handling an unsolicited <code>REPLY</code> from a previously uninvited attendee. The latter is only done if scheduling has been
     * triggered explicitly by a user ({@link SchedulingSource#API}, otherwise it is treated as no-op.
     * 
     * @param originalEvent The original event being updated
     * @param indicatedEvent The event as indicated by the <code>REPLY</code> message
     * @param reyplingAttendee The replying attendee
     * @return The prepared event, or {@link Optional#empty()} if nothing needs to be updated
     * @throws OXException If preparation fails
     */
    private Optional<Event> prepareEventToUpdate(Event originalEvent, Event indicatedEvent, Attendee reyplingAttendee) throws OXException {
        /*
         * prepare update based on replying attendee
         */
        Optional<DefaultItemUpdate<Attendee, AttendeeField>> optAttendeeUpdate = prepareAttendeeUpdate(originalEvent, indicatedEvent, reyplingAttendee);
        if (optAttendeeUpdate.isEmpty()) {
            /*
             * REPLY from uninvited attendee (party crasher), only proceed if interactively triggered by a user
             */
            if (SchedulingSource.API.equals(source)) {
                LOG.debug("Adding \"party-crasher\" to event {}", originalEvent.getId());
                Attendee partyCrasher = preparePartyCrasher(originalEvent, reyplingAttendee);
                if (removeAttendeeCopy(originalEvent, partyCrasher)) {
                    LOG.info("Purged matching attendee copy prior adding party crasher {}", partyCrasher);
                }
                return Optional.of(initEvent(Lists.combine(originalEvent.getAttendees(), partyCrasher)));
            }
            LOG.info("Found a \"party-crasher\". Stop auto-processing.");
            return Optional.empty();
        }
        /*
         * REPLY from existing attendee, take over update as needed
         */
        DefaultItemUpdate<Attendee, AttendeeField> attendeeUpdate = optAttendeeUpdate.get();
        if (attendeeUpdate.isEmpty()) {
            LOG.trace("No data to update for event {}.", originalEvent);
            return Optional.empty(); // no data changed, so ignore
        }
        List<Attendee> attendeesToUpdate = new ArrayList<Attendee>(originalEvent.getAttendees().size());
        for (Attendee originalAttendee : originalEvent.getAttendees()) {
            attendeesToUpdate.add(matches(originalAttendee, attendeeUpdate.getOriginal()) ? attendeeUpdate.getUpdate() : originalAttendee);
        }
        return Optional.of(initEvent(attendeesToUpdate));
    }

    /**
     * Looks up and removes a matching copy of the event in the party crasher's calendar, so that the attendee can be added to the
     * organizer copy afterwards w/o conflicts.
     * 
     * @param organizerCopy The organizer copy where the party crasher is about to be added
     * @param partyCrasher The party crasher as indicated in the unsolicited <code>REPLY</code> message
     * @return <code>true</code> if an attendee copy was removed, <code>false</code>, otherwise
     * @throws OXException In case of an unexpected error, or if a newer attendee copy is found
     */
    private boolean removeAttendeeCopy(Event organizerCopy, Attendee partyCrasher) throws OXException {
        /*
         * resolve entity of internal party crasher & lookup matching attendee copy in his calendar
         */
        Attendee resolvedAttendee = session.getEntityResolver().prepare(AttendeeMapper.getInstance().copy(partyCrasher, null, (AttendeeField[]) null), false);
        if (false == isInternal(resolvedAttendee)) {
            return false;
        }
        Event attendeeCopy = getMatchingAttendeeCopy(organizerCopy, resolvedAttendee.getEntity());
        if (null == attendeeCopy) {
            return false;
        }
        /*
         * cross-check sequences and participation status (still) matches
         */
        Attendee attendeeInCopy = find(attendeeCopy.getAttendees(), resolvedAttendee.getEntity());
        if (null != attendeeInCopy && (null != attendeeInCopy.getPartStat() && false == attendeeInCopy.getPartStat().matches(partyCrasher.getPartStat()) ||
            attendeeInCopy.getSequence() > partyCrasher.getSequence())) {
            throw CalendarExceptionCodes.OUT_OF_SEQUENCE.create(attendeeCopy.getId(), ITipSequence.of(attendeeInCopy), ITipSequence.of(partyCrasher));
        }
        /*
         * proceed with deletion
         */
        LOG.debug("Removing attendee copy {} prior applying party crasher to organizer copy {}.", organizerCopy, attendeeCopy);
        delete(attendeeCopy);
        return true;
    }
    
    /**
     * Looks up a possibly conflicting event copy in the calendar of a certain calendar user that could be replaced by the organizer copy,
     * in case this calendar user would be added to the list of attendees.
     * <p/>
     * Lookup is performed by <code>UID</code> and <code>RECURRENCE-ID</code>, assuming that the attendee copy is <i>external</i> from the
     * calendar user's point of view, i.e. the organizer in the attendee copy is not resolved to its internal entity.
     * 
     * @param organizerCopy The organizer copy to lookup a matching attendee copy for
     * @param entity The calendar user to check the calendar for
     * @return A matching attendee copy, or <code>null</code> none could be found
     */
    private Event getMatchingAttendeeCopy(Event organizerCopy, int entity) throws OXException {
        /*
         * resolve events by UID under perspective of calendar user, then match by recurrence id
         */
        List<Event> attendeeCopies = new ResolvePerformer(session, storage).lookupByField(
            EventField.UID, organizerCopy.getUid(), entity, new EventField[] { EventField.ID, EventField.ORGANIZER, EventField.RECURRENCE_ID, EventField.SEQUENCE }, false);
        Event attendeeCopy = findByRecurrenceId(session, entity, attendeeCopies, organizerCopy);
        if (null == attendeeCopy) {
            return null;
        }
        /*
         * compare internal object/series identifiers as well to prevent false-positive matches with the organizer copy itself
         */
        if (Objects.equals(organizerCopy.getId(), attendeeCopy.getId()) || null != organizerCopy.getSeriesId() && Objects.equals(organizerCopy.getSeriesId(), attendeeCopy.getSeriesId())) {
            return null;
        }
        /*
         * cross-check organizer in attendee copy is 'external'
         */
        if (false == hasExternalOrganizer(attendeeCopy) || hasExternalOrganizer(organizerCopy)) {
            return null;
        }
        Organizer resolvedOrganizer = session.getEntityResolver().prepare(new Organizer(attendeeCopy.getOrganizer()), CalendarUserType.INDIVIDUAL);
        if (false == matches(organizerCopy.getOrganizer(), resolvedOrganizer)) {
            return null;
        }
        return attendeeCopy;
    }    

    private static Event initEvent(List<Attendee> attendees) {
        Event event = new Event();
        event.setAttendees(attendees);
        return event;
    }

    /**
     * <i>Touches</i> the series master event of the supplied stored events of a calendar object resource if needed, i.e. the first event
     * of the collection is the series master event, and it has not already been updated before.
     * 
     * @param storedEvents The currently stored events of the targeted calendar object resource
     * @return <code>true</code> if the series master event was updated, <code>false</code>, otherwise
     * @throws OXException
     */
    private boolean touchSeriesMasterIfNeeded(List<Event> storedEvents) throws OXException {
        if (null != storedEvents && 0 < storedEvents.size()) {
            Event firstEvent = storedEvents.get(0);
            if (isSeriesMaster(firstEvent) && firstEvent.getTimestamp() < timestamp.getTime()) {
                touch(firstEvent);
                resultTracker.trackUpdate(firstEvent, loadEventData(firstEvent.getId()));
                return true;
            }
        }
        return false;
    }

    /**
     * Prepares an attendee update by applying new data for an existing attendee by updating fields {@link #UPDATE_FIELDS}
     *
     * @param message The incoming message
     * @param originalEvent The original event to get the attendee data from
     * @param updatedEventData The updated event data as transmitted by the client
     * @param replyingAttendee The attendee replying to copy the new data from
     * @return An {@link Optional} containing the attendee update with new data, or {@link Optional#empty()} if reypling attendee was not found in original event
     */
    private Optional<DefaultItemUpdate<Attendee, AttendeeField>> prepareAttendeeUpdate(Event originalEvent, Event updatedEventData, Attendee replyingAttendee) throws OXException {
        Attendee originalAttendee = CalendarUtils.find(originalEvent.getAttendees(), replyingAttendee);
        if (null == originalAttendee) {
            LOG.trace("Can't find attendee {} to update in event {}.", replyingAttendee, originalEvent);
            return Optional.empty();
        }
        /*
         * Prepare copy, set timestamp and sequence based on incoming event data
         */
        Attendee update = prepareAttendee(updatedEventData, replyingAttendee);
        update = AttendeeMapper.getInstance().copy(replyingAttendee, update, UPDATE_FIELDS);
        update = AttendeeMapper.getInstance().copy(originalAttendee, update, AttendeeField.ENTITY, AttendeeField.MEMBER, AttendeeField.CU_TYPE, AttendeeField.URI);
        if (update.getTimestamp() < 0) {
            update.setTimestamp(replyingAttendee.getTimestamp());
        }
        if (update.getSequence() < 0) {
            update.setSequence(replyingAttendee.getSequence());
        }
        Check.requireUpToDateTimestamp(originalAttendee, update);

        return Optional.of(new DefaultItemUpdate<Attendee, AttendeeField>(AttendeeMapper.getInstance(), originalAttendee, update));
    }

    /**
     * Prepares an attendee by applying data from the given event to the attendee
     *
     * @param updatedEventData The updated event data as transmitted by the client
     * @param replyingAttendee The attendee replying to copy the new data from
     * @return The updated attendee
     * @throws OXException In case copy fails
     */
    private Attendee prepareAttendee(Event updatedEventData, Attendee replyingAttendee) throws OXException {
        if (null == updatedEventData) {
            return replyingAttendee;
        }
        /*
         * Prepare copy, set timestamp and sequence based on incoming event data
         */
        Attendee update = AttendeeMapper.getInstance().copy(replyingAttendee, null, (AttendeeField[]) null);
        update.setTimestamp(updatedEventData.getDtStamp());
        update.setSequence(updatedEventData.getSequence());
        return update;
    }

    /**
     * Generates a party crasher attendee
     * 
     * @param original The original event to add the party crasher to
     * @param partCrasher The party crasher
     * @return Copy of the party crasher with only relevant fields
     * @throws OXException In case mail of the party crasher is invalid
     */
    private Attendee preparePartyCrasher(Event original, Attendee attendee) throws OXException {
        Check.requireValidEMail(attendee);
        Attendee partyCrasher = prepareAttendee(original, attendee);
        AttendeeMapper.getInstance().copy(attendee, partyCrasher, UPDATE_FIELDS);
        AttendeeMapper.getInstance().copy(attendee, partyCrasher, ADDITIONAL_FIELDS);
        return partyCrasher;
    }

    /**
     * Get the attendee that replied
     *
     * @param updatedEventData The event containing updated data
     * @param originator The originator of the message
     * @return The attendee that replied
     * @throws OXException If the attendee can not be found
     * @see <a href="https://tools.ietf.org/html/rfc5546#section-3.2.3">RFC5546 Section 3.2.3</a>
     * @see <a href="https://tools.ietf.org/html/rfc6047#section-3">RFC6047 Section 3</a>
     */
    private Attendee getReplyingAttendee(Event updatedEventData, CalendarUser originator) throws OXException {
        Attendee replyingAttendee = CalendarUtils.find(updatedEventData.getAttendees(), originator);
        if (null == replyingAttendee) {
            LOG.debug("Didn't find originator {} in the attendee list.", originator);
            /*
             * iTIP only allows one attendee in a reply, search for it
             */
            if (SchedulingSource.API.equals(source)) {
                /*
                 * Scheduled or rather allowed via API, use attendee transmitted in the iCal, regardless security considerations
                 */
                if (1 != updatedEventData.getAttendees().size()) {
                    throw CalendarExceptionCodes.ATTENDEE_NOT_FOUND.create(I(originator.getEntity()), updatedEventData.getId());
                }
            } else {
                /*
                 * Look up if originator is set in SENT-BY
                 */
                LOG.debug("Didn't find attendee. Searching in SENT-BY field.");
                if (1 != updatedEventData.getAttendees().size() || false == updatedEventData.getAttendees().get(0).containsSentBy()) {
                    throw CalendarExceptionCodes.ATTENDEE_NOT_FOUND.create(I(originator.getEntity()), updatedEventData.getId());
                }
                CalendarUser sentBy = updatedEventData.getAttendees().get(0).getSentBy();
                if (false == CalendarUtils.matches(originator, sentBy)) {
                    throw CalendarExceptionCodes.ATTENDEE_NOT_FOUND.create(I(originator.getEntity()), updatedEventData.getId());
                }
            }
            replyingAttendee = updatedEventData.getAttendees().get(0);
        }
        replyingAttendee = AttendeeMapper.getInstance().copy(replyingAttendee, null, (AttendeeField[]) null);
        replyingAttendee.setCuType(CalendarUserType.INDIVIDUAL);
        return replyingAttendee;
    }

    /**
     * Returns a {@link DelegatingEvent} where {@link EventField#SEQUENCE} and {@link EventField#DTSTAMP} are not 'set'.
     * 
     * @param event The event to hide the sequence in
     * @return The event, behind a delegating event that forcibly hides the sequence- and dtstamp properties
     */
    private static Event hideSequenceAndDTStamp(Event event) {
        return new DelegatingEvent(event) {

            @Override
            public int getSequence() {
                return 0;
            }

            @Override
            public boolean containsSequence() {
                return false;
            }

            @Override
            public long getDtStamp() {
                return 0L;
            }

            @Override
            public boolean containsDtStamp() {
                return false;
            }
        };
    }

}
