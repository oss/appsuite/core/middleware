/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tokenlogin.impl.osgi;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.jslob.JSlobEntry;
import com.openexchange.jslob.JSlobKey;
import com.openexchange.session.Session;
import com.openexchange.tokenlogin.impl.TokenLoginProperties;

/**
 * 
 * {@link TokenApplicationsJSlobEntry}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class TokenApplicationsJSlobEntry implements JSlobEntry {

    private final LeanConfigurationService configurationService;

    /**
     * Initializes a new {@link TokenApplicationsJSlobEntry}.
     *
     * @param configurationService To geth configuration from
     */
    public TokenApplicationsJSlobEntry(LeanConfigurationService configurationService) {
        super();
        this.configurationService = configurationService;
    }

    @Override
    public JSlobKey getKey() {
        return JSlobKey.CORE;
    }

    @Override
    public String getPath() {
        return "tokens/applications";
    }

    @Override
    public boolean isWritable(Session session) throws OXException {
        return false;
    }

    @Override
    public Object getValue(Session sessiond) throws OXException {
        ArrayList<String> appIds = Strings.splitBy(configurationService.getProperty(sessiond.getUserId(), sessiond.getContextId(), TokenLoginProperties.APPLICATIONS), ',', true, new ArrayList<String>());
        for (Iterator<String> iterator = appIds.iterator(); iterator.hasNext();) {
            String appId = iterator.next();
            if (false == configurationService.getBooleanProperty(sessiond.getUserId(), sessiond.getContextId(), TokenLoginProperties.ANNOUNCE_ID, TokenLoginProperties.getReplacement(appId))) {
                iterator.remove();
            }
        }
        return new JSONArray(appIds);
    }

    @Override
    public void setValue(Object value, Session sessiond) throws OXException {
        throw new OXException();
    }

    @Override
    public Map<String, Object> metadata(Session session) throws OXException {
        return Map.of();
    }

}
