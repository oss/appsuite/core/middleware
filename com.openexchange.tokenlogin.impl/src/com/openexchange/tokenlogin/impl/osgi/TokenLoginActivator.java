/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tokenlogin.impl.osgi;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.exception.OXException;
import com.openexchange.jslob.JSlobEntry;
import com.openexchange.jslob.JSlobService;
import com.openexchange.lock.LockService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.session.ObfuscatorService;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessiondEventConstants;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.tokenlogin.TokenLoginService;
import com.openexchange.tokenlogin.impl.RedeemTokenRequestAnalyzer;
import com.openexchange.tokenlogin.impl.Services;
import com.openexchange.tokenlogin.impl.TokenLoginProperties;
import com.openexchange.tokenlogin.impl.TokenLoginServiceImpl;

/**
 * {@link TokenLoginActivator} - Activator for token-login implementation bundle.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class TokenLoginActivator extends HousekeepingActivator {

    /** The logger */
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(TokenLoginActivator.class);

    /**
     * Initializes a new {@link TokenLoginActivator}.
     */
    public TokenLoginActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { LeanConfigurationService.class, SessiondService.class, ContextService.class, LockService.class, ObfuscatorService.class, JSlobService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        Services.setServiceLookup(this);
        final BundleContext context = this.context;

        // Get configuration service
        LeanConfigurationService configService = getServiceSafe(LeanConfigurationService.class);

        // Check if disabled
        if (!configService.getBooleanProperty(TokenLoginProperties.ENABLED)) {
            LOG.info("Bundle \"com.openexchange.tokenlogin\" per configuration.");
            return;
        }

        // Create service instance
        final TokenLoginServiceImpl serviceImpl = new TokenLoginServiceImpl(configService.getIntProperty(TokenLoginProperties.MAX_IDLE_TIME));

        // Check ClusterMap stuff
        {
            // Track ClusterMapService service
            final ServiceTrackerCustomizer<ClusterMapService, ClusterMapService> customizer = new ServiceTrackerCustomizer<ClusterMapService, ClusterMapService>() {

                @Override
                public void removedService(final ServiceReference<ClusterMapService> reference, final ClusterMapService service) {
                    removeService(ClusterMapService.class);
                    serviceImpl.changeBackingMapToLocalMap();
                    context.ungetService(reference);
                }

                @Override
                public void modifiedService(final ServiceReference<ClusterMapService> reference, final ClusterMapService service) {
                    // Ignore
                }

                @Override
                public ClusterMapService addingService(final ServiceReference<ClusterMapService> reference) {
                    final ClusterMapService clusterMapService = context.getService(reference);
                    try {
                        addService(ClusterMapService.class, clusterMapService);
                        serviceImpl.changeBackingMapToClusterMap();
                        return clusterMapService;
                    } catch (OXException e) {
                        LOG.warn("Couldn't initialize distributed token-login map.", e);
                    } catch (RuntimeException e) {
                        LOG.warn("Couldn't initialize distributed token-login map.", e);
                    }
                    context.ungetService(reference);
                    return null;
                }
            };
            track(ClusterMapService.class, customizer);
        }

        // Event handler to detect sessions that have been removed/trimmed in the meantime.
        {
            final String propSession = SessiondEventConstants.PROP_SESSION;
            final String propContainer = SessiondEventConstants.PROP_CONTAINER;
            final EventHandler eventHandler = new EventHandler() {

                @SuppressWarnings("unchecked")
                @Override
                public void handleEvent(final Event event) {
                    final String topic = event.getTopic();
                    if (SessiondEventConstants.TOPIC_REMOVE_SESSION.equals(topic)) {
                        final Session session = (Session) event.getProperty(propSession);
                        handleSession(session);
                    } else if (SessiondEventConstants.TOPIC_REMOVE_CONTAINER.equals(topic)) {
                        for (final Session session : ((Map<String, Session>) event.getProperty(propContainer)).values()) {
                            handleSession(session);
                        }
                    }
                }

                private void handleSession(final Session session) {
                    try {
                        serviceImpl.removeTokenFor(session);
                    } catch (Exception e) {
                        LOG.warn("Failed to remove token for session {}", session.getSessionID(), e);
                    }
                }
            };
            final Dictionary<String, Object> serviceProperties = new Hashtable<String, Object>(1);
            serviceProperties.put(EventConstants.EVENT_TOPIC, SessiondEventConstants.getAllTopics());
            registerService(EventHandler.class, eventHandler, serviceProperties);
        }

        // Open trackers
        openTrackers();

        // Register service instances
        registerService(TokenLoginService.class, serviceImpl);
        registerService(RequestAnalyzer.class, new RedeemTokenRequestAnalyzer(this));
        registerService(JSlobEntry.class, new TokenApplicationsJSlobEntry(configService));
    }

    @Override
    protected void stopBundle() throws Exception {
        super.stopBundle();
        Services.setServiceLookup(null);
    }

    @Override
    public <S> boolean addService(Class<S> clazz, S service) {
        return super.addService(clazz, service);
    }

    @Override
    public <S> boolean removeService(Class<? extends S> clazz) {
        return super.removeService(clazz);
    }

}
