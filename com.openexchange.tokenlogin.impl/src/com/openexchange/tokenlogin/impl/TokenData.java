/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tokenlogin.impl;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.session.ObfuscatorService;
import com.openexchange.tokenlogin.TokenLoginExceptionCodes;

/**
 * {@link TokenData} - Data encoded into tokens.
 *
 * @param token The random token itself
 * @param sessionId The identifier of the session where the token was generated
 * @param expiry The time when the token is considered as expired, or <code>null</code> if not defined
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public record TokenData(String token, String sessionId, Date expiry) {

    /**
     * Encodes the token data to its string representation, as sent to the client.
     *
     * @param obfuscatorService A reference to the obfuscator service to use
     * @return The encoded token
     */
    public String encode(ObfuscatorService obfuscatorService) {
        String obfuscatedPart = null != expiry ? sessionId + ':' + expiry.getTime() : sessionId;
        byte[] bSessionId = obfuscatorService.obfuscate(obfuscatedPart).getBytes(StandardCharsets.UTF_8);
        return new StringBuilder(Strings.asHex(bSessionId)).append('-').append(token).toString();
    }

    /**
     * Decodes the given encoded token string.
     * 
     * @param obfuscatorService A reference to the obfuscator service to use
     * @param encodedToken The token string to decode, as received back by the client
     * @return The decoded token data
     * @throws OXException {@link TokenLoginExceptionCodes#NO_SUCH_TOKEN} if decoding fails
     */
    public static TokenData decode(ObfuscatorService obfuscatorService, String encodedToken) throws OXException {
        String[] splitToken = Strings.splitBy(encodedToken, '-', true);
        if (splitToken.length != 2) {
            // Token is of invalid format; expected: <obfuscated-data> + "-" + <UUID> 
            throw TokenLoginExceptionCodes.NO_SUCH_TOKEN.create(encodedToken);
        }
        String sessionAndExpiry = obfuscatorService.unobfuscate(new String(Strings.unhex(splitToken[0]), StandardCharsets.UTF_8));
        String token = splitToken[1];
        int colonIndex = sessionAndExpiry.indexOf(':');
        if (-1 == colonIndex) {
            return new TokenData(token, sessionAndExpiry, null);
        }
        try {
            return new TokenData(token, sessionAndExpiry.substring(0, colonIndex), new Date(Long.parseLong(sessionAndExpiry.substring(colonIndex + 1))));
        } catch (Exception e) {
            throw TokenLoginExceptionCodes.NO_SUCH_TOKEN.create(encodedToken, e);
        }
    }

}
