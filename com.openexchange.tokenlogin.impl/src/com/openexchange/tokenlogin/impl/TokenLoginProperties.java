/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tokenlogin.impl;

import static com.openexchange.java.Autoboxing.I;
import java.util.Map;
import org.apache.commons.text.CaseUtils;
import com.openexchange.config.lean.Property;

/**
 * 
 * {@link TokenLoginProperties}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public enum TokenLoginProperties implements Property {

    /** Whether or not token login is enabled */
    ENABLED(Boolean.TRUE),

    /** Specify the maximum time (in milliseconds) a token may be idle before it is evicted */
    MAX_IDLE_TIME(I(300000)),

    /** The application identifiers to use, split by comma */
    APPLICATIONS(""),

    /** Whether or not the user's password is part of the response */
    ACCESS_PASSWORD(Boolean.FALSE),

    /** Whether or not to copy all session parameters into a created session */
    COPY_PARAMETERS(Boolean.FALSE),

    /** Whether or not to announce the application identifier to a client */
    ANNOUNCE_ID(Boolean.FALSE),

    /** Additional parameters, key values pairs paired by equals, split by semicolon */
    PARAMETERS(""),
    ;

    private static final String APPLICATION_ID = "applicationId";

    private static final String PREFIX = "com.openexchange.tokenlogin";

    private final String propName;
    private final Object defaultValue;

    private TokenLoginProperties(Object defaultValue) {
        this.defaultValue = defaultValue;
        this.propName = CaseUtils.toCamelCase(name().toLowerCase(), false, '_');
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

    @Override
    public String getFQPropertyName() {
        switch (this) {
            case ENABLED, MAX_IDLE_TIME, APPLICATIONS:
                return PREFIX + '.' + propName;
            default:
                return PREFIX + ".[" + APPLICATION_ID + "]." + propName;
        }
    }

    /**
     * Get the replacement for a specific application ID
     *
     * @param appId The application ID
     * @return The map to use when getting properties for a specific application
     */
    public static Map<String, String> getReplacement(String appId) {
        return Map.of(APPLICATION_ID, appId);
    }

}
