/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tokenlogin.impl;

import java.util.Date;
import com.google.common.base.Objects;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.java.util.UUIDs;
import com.openexchange.session.ObfuscatorService;
import com.openexchange.tokenlogin.TokenLoginExceptionCodes;

/**
 * {@link TokenLoginUtility}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public final class TokenLoginUtility {

    private TokenLoginUtility() {}

    /**
     * Gets a value indicating whether the given session ID matches the supplied token or not
     * 
     * @param sessionId The session ID to match
     * @param tokenData The decoded token data to check
     * @return <code>true</code> if the token belongs to the supplied session ID
     * @throws OXException - {@link TokenLoginExceptionCodes#NO_SUCH_TOKEN} if the session couldn't be extracted
     */
    public static boolean isTokenForSession(String sessionId, TokenData tokenData) {
        String extractedSessionId = tokenData.sessionId();
        return Strings.isNotEmpty(extractedSessionId) && Objects.equal(sessionId, extractedSessionId);
    }

    /**
     * Checks whether the token has expired or not
     * 
     * @param tokenData The decoded token data to extract the expiration date from
     * @return <code>true</code> if the token expired, <code>false</code> if the token is still valid
     */
    public static boolean isExpired(TokenData tokenData) {
        return null != tokenData.expiry() && new Date().after(tokenData.expiry());
    }

    /**
     * Creates a new encoded token.
     *
     * @param sessionId The session identifier
     * @param obfuscatorService The obfuscator service to use
     * @return The new token in its encoded format
     */
    public static String createNewToken(ObfuscatorService obfuscatorService, String sessionId) {
        return createNewTokenWithExpiry(obfuscatorService, sessionId, -1L);
    }

    /**
     * Creates a new encoded token.
     *
     * @param sessionId The session identifier
     * @param obfuscatorService The obfuscator service to use
     * @param expiry The time in milliseconds when the token shall expire
     * @return The new token in its encoded format
     */
    public static String createNewTokenWithExpiry(ObfuscatorService obfuscatorService, String sessionId, long expiry) {
        return new TokenData(UUIDs.getUnformattedStringFromRandom(), sessionId, 0 < expiry ? new Date(System.currentTimeMillis() + expiry) : null).encode(obfuscatorService);
    }

}
