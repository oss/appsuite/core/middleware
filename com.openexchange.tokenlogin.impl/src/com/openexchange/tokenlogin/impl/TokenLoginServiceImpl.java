/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tokenlogin.impl;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.tokenlogin.impl.TokenLoginUtility.createNewToken;
import static com.openexchange.tokenlogin.impl.TokenLoginUtility.createNewTokenWithExpiry;
import static com.openexchange.tokenlogin.impl.TokenLoginUtility.isExpired;
import static com.openexchange.tokenlogin.impl.TokenLoginUtility.isTokenForSession;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.ClusterMap;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.cluster.map.codec.MapCodecs;
import com.openexchange.concurrent.Blocker;
import com.openexchange.concurrent.ConcurrentBlocker;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.exception.LogLevel;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.lock.LockService;
import com.openexchange.log.LogUtility;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.session.ObfuscatorService;
import com.openexchange.session.Session;
import com.openexchange.sessiond.DefaultAddSessionParameter;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.tokenlogin.DefaultTokenLoginApplication;
import com.openexchange.tokenlogin.TokenLoginApplication;
import com.openexchange.tokenlogin.TokenLoginExceptionCodes;
import com.openexchange.tokenlogin.TokenLoginService;

/**
 * {@link TokenLoginServiceImpl} - Implementation of {@code TokenLoginService}.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class TokenLoginServiceImpl extends BasicCoreClusterMapProvider<String> implements TokenLoginService {

    private static final Logger LOG = LoggerFactory.getLogger(TokenLoginServiceImpl.class);

    private final Blocker blocker = new ConcurrentBlocker();
    private final Cache<String, String> sessionId2token;

    private volatile boolean useClusterMap = false;

    /**
     * Initializes a new {@link TokenLoginServiceImpl}.
     *
     * @param maxTokenLifetime The maximum time (in milliseconds) a previously acquired token is remembered
     */
    public TokenLoginServiceImpl(long maxTokenLifetime) {
        super(CoreMap.SESSION_SESSIONID_TO_TOKEN, MapCodecs.getStringCodec(), maxTokenLifetime, () -> Services.getService(ClusterMapService.class));
        CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder().expireAfterWrite(maxTokenLifetime, TimeUnit.MILLISECONDS);
        this.sessionId2token = cacheBuilder.build();
    }

    @Override
    public TokenLoginApplication getTokenLoginApplication(int contextId, int userId, String appId) throws OXException {
        if (Strings.isEmpty(appId)) {
            return null;
        }
        LeanConfigurationService configurationService = Services.getServiceLookup().getServiceSafe(LeanConfigurationService.class);
        String applications = configurationService.getProperty(userId, contextId, TokenLoginProperties.APPLICATIONS);
        String[] ids;
        if (Strings.isEmpty(applications) || null == (ids = Strings.splitByComma(applications)) || ids.length <= 0) {
            return null; // No applications configured
        }
        Optional<String> optAppId = Arrays.asList(ids).stream().filter(appId::equals).findAny();
        if (optAppId.isEmpty()) {
            return null; // No matching application found
        }
        // Application is enabled, fill in parameter
        Map<String, String> rep = TokenLoginProperties.getReplacement(appId);
        return new DefaultTokenLoginApplication(appId,
                                           configurationService.getBooleanProperty(userId, contextId, TokenLoginProperties.ACCESS_PASSWORD, rep),
                                           configurationService.getBooleanProperty(userId, contextId, TokenLoginProperties.COPY_PARAMETERS, rep),
                                           configurationService.getBooleanProperty(userId, contextId, TokenLoginProperties.ANNOUNCE_ID, rep),
                                           configurationService.getProperty(userId, contextId, TokenLoginProperties.PARAMETERS, rep));

    }

    // -------------------------------------------------------------------------------------------------------- //

    /**
     * Removes the key/value pair from the cluster map with the given identifier
     *
     * @param key The key to remove
     * @return The removed value or <code>null</code>s
     * @throws OXException If remove from map fails
     */
    private String removeFromClusterMap(String key) throws OXException {
        ClusterMap<String> clusterMap = getMap();
        String retval = null;
        if (null == clusterMap) {
            LOG.trace("Cluster map for remote token logins is not available.");
        } else {
            // This MUST be synchronous!
            retval = clusterMap.remove(key);
        }
        return retval;
    }

    /**
     * Puts the given key/value pair into the cluster map with given identifier if absent
     *
     * @param key The key to add
     * @param value The value to add
     * @return The already existing value or <code>null</code>
     * @throws OXException If cluster map is not available
     */
    private String putToClusterMapIfAbsent(String key, String value) throws OXException {
        ClusterMap<String> clusterMap = getMap();
        if (null == clusterMap) {
            throw OXException.general("Cluster map for remote token logins is not available.");
        }
        // This MUST be synchronous!
        return clusterMap.putIfAbsent(key, value);
    }

    /**
     * Puts the given key/value pair into the cluster map
     *
     * @param key The key to add
     * @param value The value to add
     * @throws OXException If cluster map is not available
     */
    private void putToClusterMap(String key, String value) throws OXException {
        ClusterMap<String> clusterMap = getMap();
        if (null == clusterMap) {
            throw OXException.general("Cluster map for remote token logins is not available.");
        }
        // This MUST be synchronous!
        clusterMap.put(key, value);
    }

    /**
     * Gets a value from a given cluster map
     *
     * @param key The key to get
     * @return The value for the given key or <code>null</code>
     * @throws OXException If get from map fails
     */
    private String getFromClusterMap(String key) throws OXException {
        ClusterMap<String> clusterMap = getMap();
        String retval = null;
        if (null == clusterMap) {
            LOG.trace("Cluster map for remote token logins is not available.");
        } else {
            retval = clusterMap.get(key);
        }
        return retval;
    }

    @Override
    public String acquireToken(Session session, long expiry) throws OXException {
        if (null == session) {
            throw TokenLoginExceptionCodes.UNEXPECTED_ERROR.create("No session");
        }
        ObfuscatorService obfuscatorService = Services.getService(ObfuscatorService.class);
        if (null == obfuscatorService) {
            throw ServiceExceptionCode.SERVICE_UNAVAILABLE.create(ObfuscatorService.class.getName());
        }

        // Only one token per session
        String sessionId = session.getSessionID();
        String encodedToken = getToken(sessionId);
        if (null != encodedToken) {
            // There is already a token associated with given session. Check that token is (still) valid
            TokenData tokenData = TokenData.decode(obfuscatorService, encodedToken);
            if (isTokenForSession(sessionId, tokenData) && false == isExpired(tokenData)) {
                // Everything fine, generate new token to reset expiry if needed
                if (0 >= expiry || expiry > expireMillis) {
                    return encodedToken;
                }
            }
            // Token is of invalid format or has expired, or expiry needs to be reset
            remove(sessionId);
        }

        // No such token or token is of invalid format
        String newToken;
        if (expiry > 0 && expiry < expireMillis) {
            newToken = createNewTokenWithExpiry(obfuscatorService, sessionId, expiry);
        } else {
            newToken = createNewToken(obfuscatorService, sessionId);
        }
        putIfAbsent(sessionId, newToken);
        return newToken;
    }

    @Override
    public Session redeemToken(String token, String appId, String optClientIdentifier, String optAuthId, String optHash, String optClientIp, String optUserAgent, boolean staySignedIn) throws OXException {
        // Check available services needed for session creation
        SessiondService sessiondService = Services.getServiceLookup().getService(SessiondService.class);
        ContextService contextService = Services.getServiceLookup().getService(ContextService.class);
        ObfuscatorService obfuscatorService = Services.getServiceLookup().getService(ObfuscatorService.class);
        // Decode token and check if expired
        TokenData tokenData = TokenData.decode(obfuscatorService, token);
        if (isExpired(tokenData)) {
            throw TokenLoginExceptionCodes.TOKEN_REDEEM_DENIED.create();
        }
        // Look-up session identifier
        String sessionId = lookUpSessionIdByToken(token, tokenData.sessionId());
        // Get session to duplicate
        Session session = sessiondService.peekSession(sessionId);
        if (null == session) {
            throw TokenLoginExceptionCodes.NO_SUCH_SESSION_FOR_TOKEN.create(token);
        }
        // Check token
        TokenLoginApplication tokenLoginApplication = getTokenLoginApplication(session.getContextId(), session.getUserId(), appId);
        if (null == tokenLoginApplication) {
            OXException e = TokenLoginExceptionCodes.TOKEN_REDEEM_DENIED.create();
            LogUtility.logOncePerDay(LOG, LogLevel.WARNING, LogLevel.DEBUG, "Denying attempt to redeem token for unknown application id \"{}\" by user {} in context {}.", appId, I(session.getContextId()), I(session.getUserId()), e);
            throw e;
        }

        // Create parameter object
        final DefaultAddSessionParameter parameter = new DefaultAddSessionParameter().setUserId(session.getUserId());
        parameter.setClientIP(Strings.isEmpty(optClientIp) ? session.getLocalIp() : optClientIp);
        parameter.setFullLogin(session.getLogin()).setPassword(session.getPassword());
        parameter.setContext(contextService.getContext(session.getContextId()));
        parameter.setUserLoginInfo(session.getLoginName());
        // Client identifier
        parameter.setClient(Strings.isEmpty(optClientIdentifier) ? session.getClient() : optClientIdentifier);
        // Authentication identifier
        parameter.setAuthId(Strings.isEmpty(optAuthId) ? session.getAuthId() : optAuthId);
        // Hash value
        parameter.setHash(Strings.isEmpty(optHash) ? session.getHash() : optHash);
        parameter.setUserAgent(Strings.isEmpty(optUserAgent) ? (String) session.getParameter(Session.PARAM_USER_AGENT) : optUserAgent);
        if (staySignedIn) {
            parameter.setTransient(false).setStaySignedIn(true);
        } else {
            // short-living session without fail-over
            parameter.setTransient(true).setStaySignedIn(false);
        }
        if (tokenLoginApplication.shallCopyParameters()) {
            parameter.addEnhancement(new SessionParameterEnhancement(session));
        }
        // Add & return session
        return sessiondService.addSession(parameter);
    }

    private String lookUpSessionIdByToken(String encodedToken, String extractedSessionId) throws OXException {
        Lock lock = getLockFor(encodedToken);
        lock.lock();
        try {
            // Get session identifier
            String sessionId = extractedSessionId;

            // Fetch token for session identifier & validate it
            String storedToken = remove(sessionId);
            if (storedToken == null) {
                // No such token at all
                throw TokenLoginExceptionCodes.NO_SUCH_TOKEN.create(encodedToken);
            }

            // Check equality
            if (storedToken.equals(encodedToken)) {
                // All fine...
                return sessionId;
            }

            // Client-given and stored token are different. Restore stored token & signal error.
            put(sessionId, storedToken);
            throw TokenLoginExceptionCodes.NO_SUCH_TOKEN.create(encodedToken);
        } finally {
            lock.unlock();
        }
    }

    private static Lock getLockFor(String token) throws OXException {
        LockService lockService = Services.getService(LockService.class);
        return null == lockService ? Session.EMPTY_LOCK : lockService.getLockFor(token);
    }

    /**
     * Puts the given token into the map if not already present.
     *
     * @param sessionId The session identifier
     * @param newToken The new token
     * @return The existing value or <code>null</code>
     * @throws OXException If put operation fails
     */
    private String putIfAbsent(String sessionId, String newToken) throws OXException {
        blocker.acquire();
        try {
            if (useClusterMap) {
                return putToClusterMapIfAbsent(sessionId, newToken);
            }
            return sessionId2token.asMap().putIfAbsent(sessionId, newToken);
        } finally {
            blocker.release();
        }
    }

    /**
     * Puts the given token into the map.
     *
     * @param sessionId The session identifier
     * @param newToken The new token
     * @throws OXException If put operation fails
     */
    private void put(String sessionId, String newToken) throws OXException {
        blocker.acquire();
        try {
            if (useClusterMap) {
                putToClusterMap(sessionId, newToken);
            } else {
                sessionId2token.asMap().put(sessionId, newToken);
            }
        } finally {
            blocker.release();
        }
    }

    /**
     * Gets the token associated with the given session identifier.
     *
     * @param sessionId The session identifier
     * @return The token or null
     * @throws OXException
     */
    private String getToken(String sessionId) throws OXException {
        blocker.acquire();
        try {
            if (useClusterMap) {
                return getFromClusterMap(sessionId);
            }
            return sessionId2token.getIfPresent(sessionId);
        } finally {
            blocker.release();
        }
    }

    /**
     * Removes the token for the given session identifier.
     *
     * @param sessionId The session identifier
     * @return The associated token or null
     * @throws OXException If remove from map fails
     */
    private String remove(String sessionId) throws OXException {
        blocker.acquire();
        try {
            if (useClusterMap) {
                return removeFromClusterMap(sessionId);
            }
            return sessionId2token.asMap().remove(sessionId);
        } finally {
            blocker.release();
        }
    }

    /**
     * Removes the token for specified session.
     *
     * @param session The session
     * @throws OXException If remove from map fails
     */
    public void removeTokenFor(final Session session) throws OXException {
        Objects.requireNonNull(session);
        remove(session.getSessionID());
    }

    /**
     * Changes the backing map to a local map
     */
    public void changeBackingMapToLocalMap() {
        blocker.block();
        try {
            // This happens if ClusterMapService is removed in the meantime. We cannot copy any information back to the local map.
            useClusterMap = false;
            LOG.info("Token-login backing map changed to local");
        } finally {
            blocker.unblock();
        }
    }

    /**
     * Changes the backing map to a cluster one
     * 
     * @throws OXException If cluster map is unavailable
     */
    public void changeBackingMapToClusterMap() throws OXException {
        blocker.block();
        try {
            if (useClusterMap) {
                return;
            }
            if (0 < sessionId2token.size()) {
                ClusterMap<String> clusterMap = getMap();
                if (null == clusterMap) {
                    throw OXException.general("Cluster map for remote token logins is not available.");
                }

                for (Map.Entry<String, String> e : sessionId2token.asMap().entrySet()) {
                    clusterMap.put(e.getKey(), e.getValue());
                }
                sessionId2token.invalidateAll();
            }
            useClusterMap = true;
            LOG.info("Token-login backing map changed to cluster");
        } finally {
            blocker.unblock();
        }
    }
}
