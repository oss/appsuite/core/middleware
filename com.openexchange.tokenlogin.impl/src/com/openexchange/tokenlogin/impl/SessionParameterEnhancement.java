/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tokenlogin.impl;

import com.openexchange.authentication.SessionEnhancement;
import com.openexchange.session.Session;

/**
 * {@link SessionParameterEnhancement} - Copies parameters from one session into another
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class SessionParameterEnhancement implements SessionEnhancement {

    private final Session existingSession;

    /**
     * Initializes a new {@link SessionParameterEnhancement}.
     *
     * @param existingSession The existing session to copy parameter from
     */
    public SessionParameterEnhancement(Session existingSession) {
        super();
        this.existingSession = existingSession;
    }

    @Override
    public void enhanceSession(Session session) {
        for (String name : existingSession.getParameterNames()) {
            session.setParameter(name, existingSession.getParameter(name));
        }
    }
}
