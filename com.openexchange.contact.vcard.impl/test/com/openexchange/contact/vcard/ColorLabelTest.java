/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.contact.vcard;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.openexchange.groupware.container.Contact;
import ezvcard.VCard;

/**
 * {@link ColorLabelTest}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class ColorLabelTest extends VCardTest {

    /**
     * Initializes a new {@link ColorLabelTest}.
     */
    public ColorLabelTest() {
        super();
    }

    @Test
    public void testExportColorLabel() {
        // Create test contact
        Contact contact = new Contact();
        contact.setDisplayName("test");
        contact.setLabel(Contact.LABEL_4);

        // Export to new vCard
        VCard vCard = getMapper().exportContact(contact, null, null, null);

        // Verify vCard
        assertNotNull(vCard, "No vCard exported");
        assertNotNull(vCard.getExtendedProperty("X-OX-COLOR-LABEL"), "No color label exported");
        Assertions.assertEquals(String.valueOf(Contact.LABEL_4), vCard.getExtendedProperty("X-OX-COLOR-LABEL").getValue(), "Wrong value for color label");
    }

    @Test
    public void testImportColorLabel() {
        // Create test vCard
        VCard vCard = new VCard();
        vCard.setFormattedName("test");
        vCard.setExtendedProperty("X-OX-COLOR-LABEL", String.valueOf(Contact.LABEL_7));

        // Parse vCard & verify color label
        Contact contact = getMapper().importVCard(vCard, null, null, null);
        assertNotNull(contact, "No contact imported");
        Assertions.assertEquals(Contact.LABEL_7, contact.getLabel(), "Wrong value for color label");
    }
}
