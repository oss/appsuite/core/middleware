/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.deputy.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.openexchange.deputy.ActiveDeputyPermission;
import com.openexchange.deputy.AdministrativeDeputyService;
import com.openexchange.deputy.DeputyInfo;
import com.openexchange.deputy.DeputyPermission;
import com.openexchange.deputy.DeputyService;
import com.openexchange.exception.OXException;
import com.openexchange.session.GeneratedSession;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSessionAdapter;

/**
 * {@link DelegatingAdministrativeDeputyService} - Delegating to {@link DeputyService} with special administrative user session.
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class DelegatingAdministrativeDeputyService implements AdministrativeDeputyService {

    private final DeputyServiceImpl deputyService;

    /**
     * Initializes a new {@link DelegatingAdministrativeDeputyService}.
     *
     * @param deputyService The delegating deputy service
     */
    public DelegatingAdministrativeDeputyService(DeputyServiceImpl deputyService) {
        super();
        this.deputyService = deputyService;
    }

    /**
     * Creates a new artificial session for administrative calls to deputy management.
     *
     * @param userId The suer identifier
     * @param contextId The context identifier
     * @return The newly created session
     * @throws OXException If creating the session fails
     */
    private static Session getAdminsitrativeSession(int userId, int contextId) throws OXException {
        GeneratedSession session = new GeneratedSession(userId, contextId);
        session.setParameter(SESSION_PARAM_ADMINISTRATIVE_FLAG, Boolean.TRUE);
        return ServerSessionAdapter.valueOf(session);
    }

    @Override
    public boolean isAvailable(int userId, int contextId) throws OXException {
        return deputyService.isAvailable(getAdminsitrativeSession(userId, contextId));
    }

    @Override
    public boolean isEnabled(int userId, int contextId) throws OXException {
        return deputyService.isEnabled(getAdminsitrativeSession(userId, contextId));
    }

    @Override
    public List<String> getAvailableModules(int userId, int contextId) throws OXException {
        return deputyService.getAvailableModules(getAdminsitrativeSession(userId, contextId));
    }

    @Override
    public String grantDeputyPermission(DeputyPermission deputyPermission, int userId, int contextId) throws OXException {
        return deputyService.grantDeputyPermission(deputyPermission, getAdminsitrativeSession(userId, contextId));
    }

    @Override
    public void updateDeputyPermission(String deputyId, DeputyPermission deputyPermission, int userId, int contextId) throws OXException {
        deputyService.updateDeputyPermission(deputyId, deputyPermission, getAdminsitrativeSession(userId, contextId));
    }

    @Override
    public void revokeDeputyPermission(String deputyId, int userId, int contextId) throws OXException {
        deputyService.revokeDeputyPermission(deputyId, getAdminsitrativeSession(userId, contextId));
    }

    @Override
    public boolean existsDeputyPermission(String deputyId, int contextId) throws OXException {
        return deputyService.existsDeputyPermission(deputyId, contextId);
    }

    @Override
    public ActiveDeputyPermission getDeputyPermission(String deputyId, int userId, int contextId) throws OXException {
        return deputyService.getDeputyPermission(deputyId, getAdminsitrativeSession(userId, contextId));
    }

    @Override
    public List<String> listIdsOfDeputyPermissions(int userId, int contextId) throws OXException {
        return deputyService.listIdsOfDeputyPermissions(getAdminsitrativeSession(userId, contextId));
    }

    @Override
    public List<ActiveDeputyPermission> listDeputyPermissions(int userId, int contextId) throws OXException {
        return deputyService.listDeputyPermissions(getAdminsitrativeSession(userId, contextId));
    }

    @Override
    public List<ActiveDeputyPermission> listDeputyPermissions(int contextId) throws OXException {
        List<DeputyInfo> deputyInfos = deputyService.getDeputyStorage().list(contextId);
        if (deputyInfos.isEmpty()) {
            return Collections.emptyList();
        }

        List<ActiveDeputyPermission> deputyPermissions = new ArrayList<ActiveDeputyPermission>(deputyInfos.size());
        for (DeputyInfo deputyInfo : deputyInfos) {
            Session session = getAdminsitrativeSession(deputyInfo.getUserId(), contextId);
            ActiveDeputyPermission deputyPermission = deputyService.loadDeputyPermission(deputyInfo, session);
            if (deputyPermission != null) {
                deputyPermissions.add(deputyPermission);
            } else {
                deputyService.deleteFromStorageSafe(deputyInfo.getDeputyId(), session);
            }
        }
        return deputyPermissions;
    }

}
