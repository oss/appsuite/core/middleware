/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.deputy.impl;

import static com.openexchange.deputy.PermissionSet.getMatchingPermissionSet;
import com.openexchange.deputy.ModulePermission;
import com.openexchange.deputy.Permission;

/**
 * {@link DeputyTools} - Utility class for deputy management.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class DeputyTools {

    /**
     * Initializes a new instance of {@link DeputyTools}.
     */
    private DeputyTools() {
        super();
    }

    /**
     * Validates the module permission.
     *
     * @param modulePermission The module permission to check
     * @return <code>true</code> if module permissions is valid; otherwise <code>false</code>
     */
    public static boolean validatePermissions(ModulePermission modulePermission) {
        if (modulePermission == null) {
            return true;
        }

        Permission permission = modulePermission.getPermission();
        return permission != null && !permission.isGroup() && getMatchingPermissionSet(permission).isPresent();
    }

}
