/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
* 
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.chronos.provider.internal.config;

import java.util.HashSet;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tools.session.ServerSessionAdapter;
import com.openexchange.user.User;

/**
 * {@link PreferredCUAddressEntry}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class PreferredCUAddressEntry extends ChronosJSlobEntry {

    private static final String PREFERRED_CALENDAR_USER_ADDRESS = "preferredAddress";

    /**
     * Initializes a new {@link PreferredCUAddressEntry}.
     *
     * @param services The service lookup
     */
    public PreferredCUAddressEntry(ServiceLookup services) {
        super(services);
    }

    @Override
    public String getPath() {
        return "chronos/" + PREFERRED_CALENDAR_USER_ADDRESS;
    }

    @Override
    public boolean isWritable(Session session) throws OXException {
        return true;
    }

    @Override
    protected Object getValue(ServerSession session, JSONObject userConfig) throws OXException {
        String preferredAddress = userConfig.optString(PREFERRED_CALENDAR_USER_ADDRESS);
        if (Strings.isNotEmpty(preferredAddress)) {
            return preferredAddress;
        }
        return session.getUser().getMail();
    }

    @Override
    protected void setValue(ServerSession session, JSONObject userConfig, Object value) throws OXException {
        if (value instanceof String preferredAddress) {
            UserConfigHelper userConfigHelper = new UserConfigHelper(services);
            userConfigHelper.setPeferredCalendarUserAddress(session.getContextId(), session.getUserId(), userConfig, preferredAddress);
            userConfigHelper.checkUserConfig(userConfig);
        }
    }

    @Override
    public Map<String, Object> metadata(Session session) throws OXException {
        User user = ServerSessionAdapter.valueOf(session).getUser();
        String[] aliases = user.getAliases();
        HashSet<String> mails = HashSet.newHashSet(null == aliases ? 1 : (aliases.length + 1));
        Strings.ifNotEmpty(mails::add, user.getMail());
        Strings.ifNotEmpty(mails::add, aliases);
        return Map.of("possibleValues", new JSONArray(mails));
    }

}
