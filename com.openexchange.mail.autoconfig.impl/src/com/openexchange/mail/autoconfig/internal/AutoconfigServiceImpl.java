/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.autoconfig.internal;

import java.util.List;
import javax.mail.internet.AddressException;
import com.google.common.collect.ImmutableList;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.mail.MailProperty;
import com.openexchange.mail.autoconfig.Autoconfig;
import com.openexchange.mail.autoconfig.AutoconfigException;
import com.openexchange.mail.autoconfig.AutoconfigService;
import com.openexchange.mail.autoconfig.DefaultAutoconfig;
import com.openexchange.mail.autoconfig.sources.ConfigServer;
import com.openexchange.mail.autoconfig.sources.ConfigSource;
import com.openexchange.mail.autoconfig.sources.ConfigurationFile;
import com.openexchange.mail.autoconfig.sources.Guess;
import com.openexchange.mail.autoconfig.sources.ISPDB;
import com.openexchange.mail.autoconfig.sources.staticsource.KnownStaticConfigSource;
import com.openexchange.mail.mime.QuotedInternetAddress;
import com.openexchange.server.ServiceLookup;

/**
 * {@link AutoconfigServiceImpl}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 */
public class AutoconfigServiceImpl implements AutoconfigService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(AutoconfigServiceImpl.class);

    private final List<ConfigSource> sources;
    private final ServiceLookup services;

    /**
     * Initializes a new {@link AutoconfigServiceImpl}.
     *
     * @param services THe service look-up
     */
    public AutoconfigServiceImpl(final ServiceLookup services) {
        super();
        this.services = services;
        KnownStaticConfigSource[] staticConfigSources = KnownStaticConfigSource.values();

        ImmutableList.Builder<ConfigSource> sources = ImmutableList.builderWithExpectedSize(staticConfigSources.length + 4);
        sources.add(new ConfigurationFile(services));
        sources.add(new ConfigServer(services));
        sources.add(new ISPDB(services));
        for (KnownStaticConfigSource staticConfigSource : staticConfigSources) {
            sources.add(staticConfigSource);
        }
        sources.add(new Guess(services));

        this.sources = sources.build();
    }

    @Override
    public Autoconfig getConfig(final String email, final String password, final int userId, final int contextId) throws OXException {
        return getConfig(email, password, userId, contextId, true);
    }

    @Override
    public Autoconfig getConfig(final String email, final String password, final int userId, final int contextId, boolean forceSecure) throws OXException {
        QuotedInternetAddress internetAddress;
        try {
            internetAddress = new QuotedInternetAddress(email);
        } catch (AddressException e) {
            throw AutoconfigException.invalidMail(e, email);
        }

        LocalAndDomainPart localAndDomainPart;
        try {
            localAndDomainPart = getLocalAndDomainPart(internetAddress);
        } catch (Exception e) {
            LOG.warn("Failed to determine local and domain part from given E-Mail address {}", internetAddress, e);
            return null;
        }

        String localPart = localAndDomainPart.getLocalPart();
        String domainPart = localAndDomainPart.getDomainPart();
        for (ConfigSource source : sources) {
            DefaultAutoconfig config = source.getAutoconfig(localPart, domainPart, password, userId, contextId, forceSecure);
            if (config != null) {
                config.setSource(source.getClass().getSimpleName());
                filterTransportDetails(config, userId, contextId);
                return config;
            }
        }
        return null;
    }

    /**
     * Filters the transport details if necessary, i.e. if {@link MailProperty.SMTP_ALLOW_EXTERNAL} is enabled
     *
     * @param config The config with the mail details
     * @throws OXException if the lean config service is absent
     */
    private void filterTransportDetails(DefaultAutoconfig config, int userId, int contextId) throws OXException {
        LeanConfigurationService configService = services.getServiceSafe(LeanConfigurationService.class);
        if (configService.getBooleanProperty(userId, contextId, MailProperty.SMTP_ALLOW_EXTERNAL)) {
            // External SMTP allowed. Return as-is.
            return;
        }

        // Invalidate SMTP settings in auto-config instance
        config.setTransportPort(-1);
        config.setTransportServer(null);
        config.setTransportProtocol(null);
        config.setTransportStartTls(false);
        config.setTransportSecure(false);
        config.setTransportOAuthId(-1);
    }

    /**
     * Gets the local and domain part from given E-Mail address.
     *
     * @param internetAddress The E-Mail address
     * @return The local and domain part
     */
    protected static LocalAndDomainPart getLocalAndDomainPart(final QuotedInternetAddress internetAddress) {
        String address = internetAddress.getAddress();
        int pos = address.indexOf('@');
        if (pos <= 0) {
            return LocalAndDomainPart.instanceFor(address, address);
        }
        String domain = address.substring(pos + 1);
        String localPart = address.substring(0, pos);
        return LocalAndDomainPart.instanceFor(localPart, domain);
    }

}
