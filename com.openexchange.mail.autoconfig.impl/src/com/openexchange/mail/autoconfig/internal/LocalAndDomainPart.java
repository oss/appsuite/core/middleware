/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.autoconfig.internal;

/**
 * {@link LocalAndDomainPart} - The local and domain part of an E-Mail address.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class LocalAndDomainPart {

    /**
     * Creates a new instance for given local and doamin part.
     *
     * @param localPart The local part of an E-Mail address; e.g. <code>"jane.doe"</code> from <code>"jane.doe@example.org"</code>
     * @param domainPart The domain part of an E-Mail address; e.g. <code>"example.org"</code> from <code>"jane.doe@example.org"</code>
     * @return The newly created instance
     */
    public static LocalAndDomainPart instanceFor(String localPart, String domainPart) {
        return new LocalAndDomainPart(localPart, domainPart);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** The local part of an E-Mail address */
    private final String localPart;

    /** The domain part of an E-Mail address */
    private final String domainPart;

    /**
     * Initializes a new instance of {@link LocalAndDomainPart}.
     *
     * @param localPart The local part of an E-Mail address; e.g. <code>"jane.doe"</code> from <code>"jane.doe@example.org"</code>
     * @param domainPart The domain part of an E-Mail address; e.g. <code>"example.org"</code> from <code>"jane.doe@example.org"</code>
     */
    private LocalAndDomainPart(String localPart, String domainPart) {
        super();
        this.localPart = localPart;
        this.domainPart = domainPart;
    }

    /**
     * Gets the local part of an E-Mail address; e.g. <code>"jane.doe"</code> from <code>"jane.doe@example.org"</code>
     *
     * @return The local part of an E-Mail address
     */
    public String getLocalPart() {
        return localPart;
    }

    /**
     * Gets the domain part of an E-Mail address; e.g. <code>"example.org"</code> from <code>"jane.doe@example.org"</code>
     *
     * @return The domain part of an E-Mail address
     */
    public String getDomainPart() {
        return domainPart;
    }

}
