/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.autoconfig.sources;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.mail.autoconfig.tools.Utils.getHttpProxyIfEnabled;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.MXRecord;
import org.xbill.DNS.Record;
import org.xbill.DNS.SimpleResolver;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.net.InternetDomainName;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.exception.OXException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Strings;
import com.openexchange.mail.autoconfig.Autoconfig;
import com.openexchange.mail.autoconfig.DefaultAutoconfig;
import com.openexchange.mail.autoconfig.IndividualAutoconfig;
import com.openexchange.mail.autoconfig.tools.ProxyInfo;
import com.openexchange.mail.autoconfig.xmlparser.AutoconfigParser;
import com.openexchange.mail.autoconfig.xmlparser.ClientConfig;
import com.openexchange.rest.client.httpclient.HttpClientService;
import com.openexchange.rest.client.httpclient.HttpClients;
import com.openexchange.server.ServiceLookup;

/**
 * Connects to the Mozilla ISPDB. For more information see <a
 * href="https://developer.mozilla.org/en/Thunderbird/Autoconfiguration">https://developer.mozilla.org/en/Thunderbird/Autoconfiguration</a>
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a> Added google-common cache
 */
public class ISPDB extends AbstractProxyAwareConfigSource {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ISPDB.class);

    private static final String PROPERTY_ISPDB_URL = "com.openexchange.mail.autoconfig.ispdb";
    private static final String PROPERTY_ISPDB_DNS_RESOLVER_HOST = "com.openexchange.mail.autoconfig.ispdb.dns.resolverHost";
    private static final String PROPERTY_ISPDB_DNS_RESOLVER_PORT = "com.openexchange.mail.autoconfig.ispdb.dns.resolverPort";

    // -------------------------------------------------------------------------------------------------- //

    private final Cache<String, DefaultAutoconfig> autoConfigCache;

    /**
     * Initializes a new {@link ISPDB}.
     *
     * @param services The service look-up
     */
    public ISPDB(ServiceLookup services) {
        super(services);
        autoConfigCache = CacheBuilder.newBuilder().initialCapacity(16).expireAfterAccess(1, TimeUnit.DAYS).maximumSize(500).build();
    }

    @Override
    protected String getAccountId() {
        return "ispdb";
    }

    @Override
    public Autoconfig getAutoconfig(String emailLocalPart, String emailDomain, String password, int userId, int contextId) throws OXException {
        return getAutoconfig(emailLocalPart, emailDomain, password, userId, contextId, true);
    }

    @Override
    public DefaultAutoconfig getAutoconfig(String emailLocalPart, String emailDomain, String password, int userId, int contextId, boolean forceSecure) throws OXException {
        ConfigViewFactory configViewFactory = services.getServiceSafe(ConfigViewFactory.class);
        ConfigView view = configViewFactory.getView(userId, contextId);

        String baseUrl = view.get(PROPERTY_ISPDB_URL, String.class);
        if (baseUrl == null) {
            return null;
        }

        String sUrl = baseUrl;
        if (!sUrl.endsWith("/")) {
            sUrl += "/";
        }
        sUrl += emailDomain;

        // Check cache
        {
            DefaultAutoconfig autoconfig = autoConfigCache.getIfPresent(sUrl);
            if (null != autoconfig) {
                if (skipDueToForcedSecure(forceSecure, autoconfig)) {
                    // Either mail or transport do not support a secure connection (or neither of them)
                    return null;
                }

                return generateIndividualAutoconfig(emailLocalPart, emailDomain, autoconfig, forceSecure);
            }
        }

        ProxyInfo proxy = getHttpProxyIfEnabled(view);
        HttpClient httpclient = services.getServiceSafe(HttpClientService.class).getHttpClient("autoconfig-ispdb");

        try {
            Optional<DefaultAutoconfig> optAutoconfig = queryDefaultAutoconfigFor(emailDomain, baseUrl, userId, contextId, forceSecure, proxy, httpclient);
            if (optAutoconfig.isPresent()) {
                DefaultAutoconfig autoconfig = optAutoconfig.get();
                autoConfigCache.put(sUrl, autoconfig);

                // If 'forceSecure' is true, ensure that both - mail and transport settings - either support SSL or STARTTLS
                if (skipDueToForcedSecure(forceSecure, autoconfig)) {
                    // Either mail or transport do not support a secure connection (or neither of them)
                    return null;
                }

                return generateIndividualAutoconfig(emailLocalPart, emailDomain, autoconfig, forceSecure);
            }

            List<String> mxHostNames = lookupMxRecords(emailDomain, view);
            if (mxHostNames == null || mxHostNames.isEmpty()) {
                LOG.info("Could not retrieve config XML for {}", sUrl);
                return null;
            }

            for (String mxHostName : mxHostNames) {
                String altEmailDomain = InternetDomainName.from(mxHostName).topPrivateDomain().toString();
                optAutoconfig = queryDefaultAutoconfigFor(altEmailDomain, baseUrl, userId, contextId, forceSecure, proxy, httpclient);
                if (optAutoconfig.isPresent()) {
                    DefaultAutoconfig autoconfig = optAutoconfig.get();
                    autoConfigCache.put(sUrl, autoconfig);

                    // If 'forceSecure' is true, ensure that both - mail and transport settings - either support SSL or STARTTLS
                    if (skipDueToForcedSecure(forceSecure, autoconfig)) {
                        // Either mail or transport do not support a secure connection (or neither of them)
                        return null;
                    }

                    return generateIndividualAutoconfig(emailLocalPart, emailDomain, autoconfig, forceSecure);
                }
            }

            LOG.info("No such config XML for {}", sUrl);
            return null;
        } catch (ISPDBLookUpFailedException e) {
            LOG.info("Could not retrieve config XML for {}", sUrl, e);
            return null;
        }
    }

    private Optional<DefaultAutoconfig> queryDefaultAutoconfigFor(String emailDomain, String baseUrl, int userId, int contextId, boolean forceSecure, ProxyInfo proxy, HttpClient httpclient) throws ISPDBLookUpFailedException {
        String sUrl = baseUrl;
        if (!sUrl.endsWith("/")) {
            sUrl += "/";
        }
        sUrl += emailDomain;

        URI url;
        try {
            url = new URI(sUrl);
        } catch (URISyntaxException e) {
            throw new ISPDBLookUpFailedException("Unable to parse URL: " + sUrl, e);
        }

        HttpGet req = null;
        HttpResponse rsp = null;
        try {
            int port = getPort(forceSecure, url);
            HttpHost target = new HttpHost(url.getHost(), port, url.getScheme());
            req = new HttpGet(url.getPath());

            LOG.info("Executing request to retrieve config XML via {} using {}", target, null == proxy ? "no proxy" : proxy);
            rsp = httpclient.execute(target, req, httpContextFor(contextId, userId));

            int httpCode = rsp.getStatusLine().getStatusCode();
            if (httpCode != 200) {
                if (HttpStatus.SC_NOT_FOUND == httpCode) {
                    LOG.info("Could not retrieve config XML for {}. Return code was: {}", sUrl, rsp.getStatusLine());
                    return Optional.empty();
                }
                throw new ISPDBLookUpFailedException("Could not retrieve config XML for " + sUrl + ". Return code was: " + rsp.getStatusLine());
            }

            // Read & parse response
            ClientConfig clientConfig = AutoconfigParser.getConfig(rsp.getEntity().getContent());
            return getBestConfiguration(clientConfig, emailDomain);
        } catch (ISPDBLookUpFailedException e) {
            throw e;
        } catch (Exception e) {
            throw new ISPDBLookUpFailedException("Could not retrieve config XML for " + sUrl, e);
        } finally {
            HttpClients.close(req, rsp);
        }
    }

    private static DefaultAutoconfig generateIndividualAutoconfig(String emailLocalPart, String emailDomain, DefaultAutoconfig autoconfig, boolean forceSecure) {
        IndividualAutoconfig retval = new IndividualAutoconfig(autoconfig);
        retval.setUsername(autoconfig.getUsername());
        replaceUsername(retval, emailLocalPart, emailDomain);
        retval.setMailStartTls(forceSecure);
        retval.setTransportStartTls(forceSecure);
        return retval;
    }

    private static List<String> lookupMxRecords(String domainPart, ConfigView view) {
        try {
            String resolverHost = view.get(PROPERTY_ISPDB_DNS_RESOLVER_HOST, String.class);
            Integer resolverPort = view.get(PROPERTY_ISPDB_DNS_RESOLVER_PORT, Integer.class);

            SimpleResolver resolver = Strings.isEmpty(resolverHost) ? new SimpleResolver() : new SimpleResolver(resolverHost);
            if (resolverPort != null && resolverPort.intValue() > 0) {
                resolver.setPort(resolverPort.intValue());
            }

            Lookup lookup = new Lookup(domainPart, org.xbill.DNS.Type.MX);
            lookup.setResolver(resolver);
            lookup.setCache(null);

            Record[] records = lookup.run();
            if (records == null || records.length <= 0) {
                return Collections.emptyList();
            }

            List<MXRecord> mxRecords = Arrays.stream(records).map(r -> (MXRecord) r).collect(CollectorUtils.toList(records.length));
            Collections.sort(mxRecords, (mx1, mx2) -> Integer.compare(mx1.getPriority(), mx2.getPriority()));
            return mxRecords.stream().map(mx -> mx.getTarget().toString()).collect(CollectorUtils.toList(mxRecords.size()));
        } catch (Exception e) {
            LOG.warn("Failed to look-up MX records for {}", domainPart, e);
            return Collections.emptyList();
        }
    }

    private static int getPort(boolean forceSecure, URI url) {
        if (forceSecure) {
            LOG.debug("Using port 443 because the \"forceSecure\" flag is set");
            return 443;
        }

        int port = url.getPort();
        if (port < 0) {
            /*
             * No port set, e.g. "https://autoconfig.thunderbird.net/v1.1/"
             */
            if ("https".equalsIgnoreCase(url.getScheme())) {
                port = 443;
            } else {
                port = 80;
                LOG.warn("Using port 80 for communication as per configured URL {}. Please note that this is deprecated and might not work in the future. For details see \"https://developer.mozilla.org/en-US/docs/Mozilla/Thunderbird/Autoconfiguration#ssl\"", url);
            }
        } else {
            /* Port set per configuration, e.g. http://autoconfig.thunderbird.net:80/v1.1/ */
            LOG.debug("Using port {} as per configured URL {}", I(port), url);
        }
        return port;
    }


    // -------------------------------------------------------------------------------------------------------------------------------------

    private static class ISPDBLookUpFailedException extends Exception {

        /** The serial version UID */
        private static final long serialVersionUID = -7377533894176993096L;

        ISPDBLookUpFailedException(String message) {
            super(message);
        }

        ISPDBLookUpFailedException(String message, Throwable cause) {
            super(message, cause);
        }
    }

}
