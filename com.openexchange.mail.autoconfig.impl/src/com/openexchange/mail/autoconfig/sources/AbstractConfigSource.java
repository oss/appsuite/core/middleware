/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.autoconfig.sources;

import static com.openexchange.mail.autoconfig.xmlparser.Server.EMAILADDRESS;
import static com.openexchange.mail.autoconfig.xmlparser.Server.MAILLOCALPART;
import java.util.Optional;
import com.openexchange.mail.autoconfig.DefaultAutoconfig;
import com.openexchange.mail.autoconfig.xmlparser.ClientConfig;
import com.openexchange.mail.autoconfig.xmlparser.EmailProvider;
import com.openexchange.mail.autoconfig.xmlparser.IncomingServer;
import com.openexchange.mail.autoconfig.xmlparser.IncomingServer.IncomingType;
import com.openexchange.mail.autoconfig.xmlparser.OutgoingServer;
import com.openexchange.mail.autoconfig.xmlparser.OutgoingServer.OutgoingType;
import com.openexchange.mail.autoconfig.xmlparser.Server.SocketType;

/**
 * {@link AbstractConfigSource}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 */
public abstract class AbstractConfigSource implements ConfigSource {

    /**
     * Initializes a new {@link AbstractConfigSource}.
     */
    protected AbstractConfigSource() {
        super();
    }

    /**
     * Gets the best-fitting mail auto-config from specified client config source.
     *
     * @param clientConfig The client config to get from
     * @param domain The applicable domain
     * @return The best-fitting mail auto-config or empty
     */
    protected static Optional<DefaultAutoconfig> getBestConfiguration(ClientConfig clientConfig, String domain) {
        DefaultAutoconfig autoconfig = new DefaultAutoconfig();
        for (EmailProvider emailProvider : clientConfig.getEmailProvider()) {
            if (emailProvider.getDomains().contains(domain)) {
                // Apply data from best fitting incoming server
                if (!applyDataFromBestFittingIncomingServer(emailProvider, autoconfig)) {
                    // No such incoming server
                    return Optional.empty();
                }

                // Apply data from best fitting outgoing server
                if (!applyDataFromBestFittingOutgoingServer(emailProvider, autoconfig)) {
                    // No such outgoing server
                    return Optional.empty();
                }

                return Optional.of(autoconfig);
            }
        }

        // None found
        return Optional.empty();
    }

    /**
     * Applies the data from best-fitting incoming server to given auto-config instance.
     *
     * @param emailProvider The provider to grab incoming servers from
     * @param autoconfig The auto-config instance to apply to
     * @return <code>true</code> if there was a suitable incoming server; otherwise <code>false</code> if there was no such incoming server
     */
    private static boolean applyDataFromBestFittingIncomingServer(EmailProvider emailProvider, DefaultAutoconfig autoconfig) {
        IncomingServer currentIncomingServer = null;
        for (IncomingServer incomingServer : emailProvider.getIncomingServer()) {
            if ((currentIncomingServer == null) || isPreferableIncomingServer(incomingServer, currentIncomingServer)) {
                currentIncomingServer = incomingServer;
            }
        }
        if (null == currentIncomingServer) {
            // No incoming server at all
            return false;
        }

        autoconfig.setMailPort(currentIncomingServer.getPort());
        autoconfig.setMailProtocol(currentIncomingServer.getType().getKeyword());
        SocketType incomingSocket = currentIncomingServer.getSocketType();
        switch (incomingSocket) {
            case PLAIN:
                autoconfig.setMailSecure(false);
                autoconfig.setMailStartTls(false);
                break;
            case SSL:
                autoconfig.setMailSecure(true);
                autoconfig.setMailStartTls(false);
                break;
            case STARTTLS:
                autoconfig.setMailSecure(false);
                autoconfig.setMailStartTls(true);
                break;
            default:
                break;
        }
        autoconfig.setMailServer(currentIncomingServer.getHostname());
        autoconfig.setUsername(currentIncomingServer.getUsername());
        return true;
    }

    /**
     * Checks if given incoming server is preferable over current incoming server.
     *
     * @param incomingServer The new incoming server to check
     * @param currentIncomingServer The current incoming server to check against
     * @return <code>true</code> to prefer new incoming server; otherwise <code>false</code>
     */
    private static boolean isPreferableIncomingServer(IncomingServer incomingServer, IncomingServer currentIncomingServer) {
        // Preferable ServerType (e.g. IMAP > POP3)  OR  preferable SocketType (e.g. SSL > STARTTLS)
        IncomingType incomingType = incomingServer.getType();
        return (incomingType.compareTo(currentIncomingServer.getType()) > 0) ||
            (incomingType.compareTo(currentIncomingServer.getType()) == 0 && incomingServer.getSocketType().compareTo(currentIncomingServer.getSocketType()) > 0);
    }

    /**
     * Applies the data from best-fitting outgoing server to given auto-config instance.
     *
     * @param emailProvider The provider to grab outgoing servers from
     * @param autoconfig The auto-config instance to apply to
     * @return <code>true</code> if there was a suitable outgoing server; otherwise <code>false</code> if there was no such outgoing server
     */
    private static boolean applyDataFromBestFittingOutgoingServer(EmailProvider emailProvider, DefaultAutoconfig autoconfig) {
        OutgoingServer currentOutgoingServer = null;
        for (OutgoingServer outgoingServer : emailProvider.getOutgoingServer()) {
            if (currentOutgoingServer == null || isPreferableOutgoingServer(outgoingServer, currentOutgoingServer)) {
                currentOutgoingServer = outgoingServer;
            }
        }
        if (null == currentOutgoingServer) {
            // No outgoing server at all
            return false;
        }

        autoconfig.setTransportPort(currentOutgoingServer.getPort());
        autoconfig.setTransportProtocol(currentOutgoingServer.getType().getKeyword());
        SocketType outgoingSocket = currentOutgoingServer.getSocketType();
        switch (outgoingSocket) {
            case PLAIN:
                autoconfig.setTransportSecure(false);
                autoconfig.setTransportStartTls(false);
                break;
            case SSL:
                autoconfig.setTransportSecure(true);
                autoconfig.setTransportStartTls(false);
                break;
            case STARTTLS:
                autoconfig.setTransportSecure(false);
                autoconfig.setTransportStartTls(true);
                break;
            default:
                break;
        }

        autoconfig.setTransportServer(currentOutgoingServer.getHostname());
        return true;
    }

    /**
     * Checks if given outgoing server is preferable over current outgoing server.
     *
     * @param outgoingServer The new outgoing server to check
     * @param currentOutgoingServer The current outgoing server to check against
     * @return <code>true</code> to prefer new outgoing server; otherwise <code>false</code>
     */
    private static boolean isPreferableOutgoingServer(OutgoingServer outgoingServer, OutgoingServer currentOutgoingServer) {
        // Preferable ServerType (e.g. SMTP > ???) OR  preferable SocketType (e.g. SSL > STARTTLS)
        OutgoingType outgoingType = outgoingServer.getType();
        return (outgoingType.compareTo(currentOutgoingServer.getType()) > 0) ||
            (outgoingType.compareTo(currentOutgoingServer.getType()) == 0 && outgoingServer.getSocketType().compareTo(currentOutgoingServer.getSocketType()) > 0);
    }

    /**
     * Replaces the place-holder user name (either <code>"%EMAILADDRESS%"</code> or <code>"%EMAILLOCALPART%"</code>) in given auto-config instance.
     *
     * @param autoconfig The auto-config instance
     * @param emailLocalPart The local part of the E-Mail address; e.g. <code>"jane.doe"</code> from <code>"jane.doe@example.org"</code>
     * @param emailDomain The domain part of the E-Mail address; e.g. <code>"example.org"</code> from <code>"jane.doe@example.org"</code>
     */
    protected static void replaceUsername(DefaultAutoconfig autoconfig, String emailLocalPart, String emailDomain) {
        if (null != autoconfig) {
            String username = autoconfig.getUsername();
            if (null != username) {
                if (username.equalsIgnoreCase(EMAILADDRESS)) {
                    autoconfig.setUsername(new StringBuilder(24).append(emailLocalPart).append('@').append(emailDomain).toString());
                } else if (username.equalsIgnoreCase(MAILLOCALPART)) {
                    autoconfig.setUsername(emailLocalPart);
                }
            }
        }
    }

    /**
     * Checks if a secure connection is enforced, but not available in given auto-config instance.
     *
     * @param forceSecure <code>true</code> if a secure connection is enforced; otherwise <code>false</code>
     * @param autoconfig The auto-config instance
     * @return <code>true</code> to skip given auto-config instance since no secure connection available (but is required); otherwise <code>false</code> to keep it
     */
    protected static boolean skipDueToForcedSecure(boolean forceSecure, DefaultAutoconfig autoconfig) {
        return forceSecure && ((!autoconfig.isMailSecure().booleanValue() && !autoconfig.isMailStartTls()) || (!autoconfig.isTransportSecure().booleanValue() && !autoconfig.isTransportStartTls()));
    }

}
