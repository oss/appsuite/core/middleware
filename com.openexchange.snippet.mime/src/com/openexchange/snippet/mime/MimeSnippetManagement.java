/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.snippet.mime;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import static com.openexchange.mail.mime.MimeDefaultSession.getDefaultSession;
import static com.openexchange.snippet.mime.Services.getService;
import static com.openexchange.snippet.utils.SnippetUtils.sanitizeContent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DataTruncation;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;
import com.openexchange.ajax.container.ThresholdFileHolder;
import com.openexchange.ajax.container.ThresholdFileHolder.ThresholdFileHolderInputStream;
import com.openexchange.config.ConfigTools;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.lean.DefaultProperty;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.filestore.FileStorage;
import com.openexchange.filestore.FileStorageCodes;
import com.openexchange.filestore.FileStorages;
import com.openexchange.filestore.Info;
import com.openexchange.id.IDGeneratorService;
import com.openexchange.image.ImageLocation;
import com.openexchange.image.ImageUtility;
import com.openexchange.java.Streams;
import com.openexchange.java.Strings;
import com.openexchange.java.util.UUIDs;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.mime.ContentDisposition;
import com.openexchange.mail.mime.ContentType;
import com.openexchange.mail.mime.MessageHeaders;
import com.openexchange.mail.mime.MimeDefaultSession;
import com.openexchange.mail.mime.MimeMailException;
import com.openexchange.mail.mime.converters.FileBackedMimeMessage;
import com.openexchange.mail.mime.datasource.FileDataSource;
import com.openexchange.mail.mime.datasource.MessageDataSource;
import com.openexchange.mail.mime.utils.MimeMessageUtility;
import com.openexchange.mail.utils.MaxBytesExceededMessagingException;
import com.openexchange.mail.utils.MessageUtility;
import com.openexchange.quota.AccountQuota;
import com.openexchange.quota.Quota;
import com.openexchange.quota.QuotaExceptionCodes;
import com.openexchange.quota.QuotaProvider;
import com.openexchange.quota.QuotaType;
import com.openexchange.session.Session;
import com.openexchange.snippet.Attachment;
import com.openexchange.snippet.DefaultAttachment;
import com.openexchange.snippet.DefaultAttachment.InputStreamProvider;
import com.openexchange.snippet.DefaultSnippet;
import com.openexchange.snippet.Property;
import com.openexchange.snippet.ReferenceType;
import com.openexchange.snippet.Snippet;
import com.openexchange.snippet.SnippetExceptionCodes;
import com.openexchange.snippet.SnippetManagement;
import com.openexchange.snippet.mime.groupware.QuotaMode;
import com.openexchange.snippet.utils.SnippetUtils;

/**
 * {@link MimeSnippetManagement}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class MimeSnippetManagement implements SnippetManagement {

    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(MimeSnippetManagement.class);

    private static final String PROP_QUOTA_MODE = "com.openexchange.snippet.filestore.quota.mode";

    private static final AtomicReference<QuotaMode> MODE = new AtomicReference<>();

    /**
     * Retrieves the configured {@link QuotaMode}.
     *
     * @return The {@link QuotaMode}
     */
    public static QuotaMode getMode() {
        QuotaMode tmp = MODE.get();
        if (null == tmp) {
            synchronized (MimeSnippetManagement.class) {
                tmp = MODE.get();
                if (null == tmp) {
                    ConfigurationService configurationService = Services.getService(ConfigurationService.class);
                    QuotaMode def = QuotaMode.CONTEXT;
                    if (null == configurationService) {
                        return def;
                    }

                    tmp = QuotaMode.getModeByName(configurationService.getProperty(PROP_QUOTA_MODE, def.getName()), def);
                    MODE.set(tmp);
                    LOGGER.info("Using '{}' as the filestore quota mode for snippets.", tmp.getName());
                }
            }
        }
        return tmp;
    }

    /**
     * The file storage reference type identifier: <b><code>1</code></b>.
     */
    private static final int FS_TYPE = ReferenceType.FILE_STORAGE.getType();

    /**
     * Gets the file storage reference type identifier: <b><code>1</code></b>.
     *
     * @return The file storage reference type identifier: <b><code>1</code></b>.
     */
    public static int getFsType() {
        return FS_TYPE;
    }

    private static final class InputStreamProviderImpl implements InputStreamProvider {

        private final MimePart part;

        protected InputStreamProviderImpl(final MimePart part) {
            this.part = part;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            try {
                return part.getInputStream();
            } catch (MessagingException e) {
                throw new IOException(e.getMessage(), e);
            }
        }
    }

    private static DatabaseService getDatabaseService() {
        return getService(DatabaseService.class);
    }

    private static IDGeneratorService getIdGeneratorService() {
        return getService(IDGeneratorService.class);
    }

    private static FileStorage getFileStorage(int contextId) throws OXException {
        if (QuotaMode.CONTEXT == getMode()) {
            return FileStorages.getQuotaFileStorageService().getQuotaFileStorage(contextId, Info.general());
        }

        URI uri = FileStorages.getQuotaFileStorageService().getFileStorageUriFor(-1, contextId);
        return FileStorages.getFileStorageService().getFileStorage(uri);
    }

    // ----------------------------------------------------------------------------------------------------------------------------

    private final int contextId;
    private final int userId;
    private final Session session;
    private final QuotaProvider quotaProvider;
    private final LeanConfigurationService leanConfigurationService;

    /**
     * Initializes a new instance of {@link MimeSnippetManagement}.
     *
     * @param session The session providing user information
     * @param quotaProvider The quota provider to use
     * @param leanConfigurationService The configuration service to use
     */
    public MimeSnippetManagement(Session session, QuotaProvider quotaProvider, LeanConfigurationService leanConfigurationService) {
        super();
        this.session = session;
        this.userId = session.getUserId();
        this.contextId = session.getContextId();
        this.quotaProvider = quotaProvider;
        this.leanConfigurationService = leanConfigurationService;
    }

    private AccountQuota getQuota() throws OXException {
        return null == quotaProvider ? null : quotaProvider.getFor(session, "0");
    }

    @Override
    public List<Snippet> getSnippets(String... types) throws OXException {
        List<SnippetInfo> ids;
        {
            DatabaseService databaseService = getDatabaseService();
            Connection con = databaseService.getReadOnly(contextId);
            try {
                ids = loadSnippets(types, con);
            } finally {
                databaseService.backReadOnly(contextId, con);
            }
        }

        List<Snippet> list = new ArrayList<Snippet>(ids.size());
        for (SnippetInfo snippetInfo : ids) {
            try {
                list.add(loadSnippet(snippetInfo));
            } catch (OXException e) {
                if (!FileStorageCodes.FILE_NOT_FOUND.equals(e)) {
                    throw e;
                }
            }
        }
        return list;
    }

    private List<SnippetInfo> loadSnippets(String[] types, Connection con) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder("SELECT id, refId, user, displayName, module, type, shared, shared_read_only FROM snippet WHERE cid=? AND (user=? OR shared>0) AND refType=").append(FS_TYPE);
            boolean hasTypes = (null != types) && (types.length > 0);
            if (hasTypes) {
                sql.append(" AND (");
                sql.append("type=?");
                for (int i = 1; i < types.length; i++) { // Guarded by 'hasTypes'
                    sql.append(" OR type=?");
                }
                sql.append(')');
            }
            stmt = con.prepareStatement(sql.toString());
            sql = null;

            int pos = 0;
            stmt.setInt(++pos, contextId);
            stmt.setInt(++pos, userId);
            if (hasTypes) {
                for (final String type : types) { // Guarded by 'hasTypes'
                    stmt.setString(++pos, type);
                }
            }
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return Collections.emptyList();
            }

            List<SnippetInfo> ids = new LinkedList<>();
            do {
                ids.add(new SnippetInfo(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7) > 0, rs.getInt(8) > 0));
            } while (rs.next());
            return ids;
        } catch (SQLException e) {
            throw SnippetExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw SnippetExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    @Override
    public int getOwnSnippetsCount() throws OXException {
        AccountQuota quota = getQuota();
        return null == quota ? 0 : (int) quota.getQuota(QuotaType.AMOUNT).getUsage();
    }

    @Override
    public List<Snippet> getOwnSnippets() throws OXException {
        List<SnippetInfo> ids;
        {
            DatabaseService databaseService = getDatabaseService();
            Connection con = databaseService.getReadOnly(contextId);
            try {
                ids = loadOwnSnippets(con);
            } finally {
                databaseService.backReadOnly(contextId, con);
            }
        }

        List<Snippet> list = new ArrayList<Snippet>(ids.size());
        for (SnippetInfo snippetInfo : ids) {
            try {
                list.add(loadSnippet(snippetInfo));
            } catch (OXException e) {
                if (!FileStorageCodes.FILE_NOT_FOUND.equals(e)) {
                    throw e;
                }
            }
        }
        return list;
    }

    private List<SnippetInfo> loadOwnSnippets(Connection con) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            final StringBuilder sql = new StringBuilder("SELECT id, refId, user, displayName, module, type, shared, shared_read_only FROM snippet WHERE cid=? AND user=? AND refType=").append(FS_TYPE);
            stmt = con.prepareStatement(sql.toString());
            int pos = 0;
            stmt.setInt(++pos, contextId);
            stmt.setInt(++pos, userId);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return Collections.emptyList();
            }

            List<SnippetInfo> ids = new LinkedList<>();
            do {
                ids.add(new SnippetInfo(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7) > 0, rs.getInt(8) > 0));
            } while (rs.next());
            return ids;
        } catch (SQLException e) {
            throw SnippetExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    @Override
    public Snippet getSnippet(final String identifier) throws OXException {
        if (null == identifier) {
            return null;
        }

        SnippetInfo snippetInfo;
        {
            DatabaseService databaseService = getDatabaseService();
            Connection con = databaseService.getReadOnly(contextId);
            try {
                snippetInfo = loadSnippetInfo(identifier, con);
                if (snippetInfo == null) {
                    throw SnippetExceptionCodes.SNIPPET_NOT_FOUND.create(identifier);
                }
            } finally {
                databaseService.backReadOnly(contextId, con);
            }
        }

        return loadSnippet(snippetInfo);
    }

    private SnippetInfo loadSnippetInfo(String identifier, Connection con) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT refId, user, displayName, module, type, shared, shared_read_only FROM snippet WHERE cid=? AND id=? AND refType=" + FS_TYPE);
            int pos = 0;
            stmt.setInt(++pos, contextId);
            stmt.setString(++pos, identifier);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return null;
            }
            return new SnippetInfo(identifier, rs.getString(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6) > 0, rs.getInt(7) > 0);
        } catch (SQLException e) {
            throw SnippetExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw SnippetExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    private Snippet loadSnippet(SnippetInfo snippetInfo) throws OXException {
        try {
            MimeMessage mimeMessage = createMimeMessage(snippetInfo.identifier, snippetInfo.file);
            com.openexchange.mail.mime.converters.MimeMessageConverter.saveChanges(mimeMessage);
            return createSnippet(snippetInfo.identifier, snippetInfo.creator, snippetInfo.displayName, snippetInfo.module, snippetInfo.type, snippetInfo.shared, mimeMessage);
        } catch (MessagingException | IOException e) {
            throw SnippetExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw SnippetExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    private DefaultSnippet createSnippet(String identifier, final int creator, final String displayName, final String module, final String type, final boolean shared, MimeMessage mimeMessage) throws MessagingException, IOException, OXException {
        final DefaultSnippet snippet = new DefaultSnippet().setId(identifier).setCreatedBy(creator);
        final String lcct;
        {
            final String tmp = mimeMessage.getHeader(MessageHeaders.HDR_CONTENT_TYPE, null);
            if (!isEmpty(tmp)) {
                lcct = tmp.trim().toLowerCase(Locale.US);
            } else {
                lcct = "text/plain; charset=us-ascii";
            }
        }
        if (lcct.startsWith("multipart/", 0)) {
            final Multipart multipart = MimeMessageUtility.getMultipartContentFrom(mimeMessage);
            if (multipart != null) {
                parseSnippet(mimeMessage, (MimePart) multipart.getBodyPart(0), snippet);
                final int count = multipart.getCount();
                if (count > 1) {
                    for (int i = 1; i < count; i++) {
                        parsePart((MimePart) multipart.getBodyPart(i), snippet);
                    }
                }
            }
        } else {
            parseSnippet(mimeMessage, mimeMessage, snippet);
        }
        snippet.setDisplayName(displayName).setModule(module).setType(type).setShared(shared).setCreatedBy(creator);
        return snippet;
    }

    private MimeMessage createMimeMessage(String identifier, String file) throws OXException {
        InputStream in = null;
        try {
            FileStorage fileStorage = getFileStorage(session.getContextId());
            in = fileStorage.getFile(file);
            return newMimeMessage(in);
        } catch (OXException e) {
            if (!FileStorageCodes.FILE_NOT_FOUND.equals(e)) {
                throw e;
            }
            throw SnippetExceptionCodes.SNIPPET_NOT_FOUND.create(e, identifier);
        } finally {
            Streams.close(in);
        }
    }

    private static MimeMessage newMimeMessage(InputStream is) throws OXException {
        InputStream msgSrc = is;
        ThresholdFileHolder sink = new ThresholdFileHolder();
        try {
            sink.write(msgSrc);
            msgSrc = null;

            File tempFile = sink.getTempFile();
            MimeMessage tmp;
            if (null == tempFile) {
                tmp = new MimeMessage(MimeDefaultSession.getDefaultSession(), sink.getStream());
            } else {
                tmp = new FileBackedMimeMessage(MimeDefaultSession.getDefaultSession(), tempFile, null);
            }
            sink = null; // Avoid premature closing
            return tmp;
        } catch (MessagingException e) {
            throw MimeMailException.handleMessagingException(e);
        } catch (IOException e) {
            throw MailExceptionCode.IO_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (sink != null) {
                sink.close();
            }
        }
    }

    private static void parsePart(MimePart part, DefaultSnippet snippet) throws OXException, MessagingException {
        String header = part.getHeader(MessageHeaders.HDR_CONTENT_TYPE, null);
        final ContentType contentType = isEmpty(header) ? ContentType.DEFAULT_CONTENT_TYPE : new ContentType(header);
        if (contentType.startsWith("text/javascript")) {
            snippet.setMisc(MessageUtility.readMimePart(part, contentType));
        } else {
            header = part.getHeader(MessageHeaders.HDR_CONTENT_DISPOSITION, null);
            final ContentDisposition contentDisposition = isEmpty(header) ? null : new ContentDisposition(header);
            final DefaultAttachment attachment = new DefaultAttachment();
            attachment.setContentDisposition(contentDisposition == null ? null : contentDisposition.toString());
            attachment.setContentType(contentType.toString());

            header = part.getHeader(MessageHeaders.HDR_CONTENT_ID, null);
            if (null != header) {
                if (header.startsWith("<")) {
                    header = header.substring(1, header.length() - 1);
                }
                attachment.setContentId(header);
            }

            attachment.setSize(part.getSize());
            header = part.getHeader("attachmentid", null);
            if (null != header) {
                attachment.setId(header);
            }
            attachment.setStreamProvider(new InputStreamProviderImpl(part));
            snippet.addAttachment(attachment);
        }
    }

    private static final com.openexchange.config.lean.Property MAX_SNIPPET_SIZE = DefaultProperty.valueOf("com.openexchange.snippet.mime.maxSnippetSize", "10MB");

    /**
     * Returns the maximum allowed snippet size
     *
     * @return the maximum allowed snippet size
     */
    private long getMaxSnippetSize() {
        return ConfigTools.parseBytes(leanConfigurationService.getProperty(session.getUserId(), session.getContextId(), MAX_SNIPPET_SIZE));
    }

    protected void parseSnippet(MimeMessage mimeMessage, MimePart part, DefaultSnippet snippet) throws OXException, MessagingException {
        // Read content from part
        final String header = part.getHeader(MessageHeaders.HDR_CONTENT_TYPE, null);
        final ContentType contentType = isEmpty(header) ? ContentType.DEFAULT_CONTENT_TYPE : new ContentType(header);
        try {
            snippet.setContent(MessageUtility.readMimePart(part, contentType, getMaxSnippetSize()));
        } catch (MaxBytesExceededMessagingException e) {
            LOGGER.debug("Max. number of bytes exceeded for snippet. Max. size: {}, actual size: {}", L(e.getMaxSize()), L(e.getSize()), e);
            snippet.setError(e.getMessage());
        } catch (MessagingException e) {
            throw e;
        }

        // Read message's headers
        Enumeration<Header> others = mimeMessage.getAllHeaders();
        while (others.hasMoreElements()) {
            final Header hdr = others.nextElement();
            snippet.put(hdr.getName(), MimeMessageUtility.decodeMultiEncodedHeader(hdr.getValue()));
        }
    }

    private static final Set<String> IGNORABLES = Set.of(Snippet.PROP_MISC);

    private static String encode(String value) {
        try {
            return MimeUtility.encodeText(value, StandardCharsets.UTF_8, "Q");
        } catch (UnsupportedEncodingException e) {
            LOGGER.debug("Unsupported encoding: UTF-8. Using original value.", e);
            return value;
        }
    }

    @Override
    public String createSnippet(Snippet snippet) throws OXException {
        long maxSnippetSize = getMaxSnippetSize();
        if (snippet.getContent().length() > maxSnippetSize) {
            throw SnippetExceptionCodes.MAXIMUM_SNIPPET_SIZE.create(L(maxSnippetSize), L(snippet.getContent().length()));
        }
        AccountQuota quota = getQuota();
        if (null != quota) {
            Quota amountQuota = quota.getQuota(QuotaType.AMOUNT);
            if (null != amountQuota && (amountQuota.isExceeded() || amountQuota.willExceed(1))) {
                throw QuotaExceptionCodes.QUOTA_EXCEEDED_SNIPPETS.create(L(amountQuota.getUsage()), L(amountQuota.getLimit()));
            }
            // Check if size is already exceeded
            Quota sizeQuota = quota.getQuota(QuotaType.SIZE);
            if (null != sizeQuota && sizeQuota.isExceeded()) {
                throw QuotaExceptionCodes.QUOTA_EXCEEDED_SIGNATURES.create(bytesToReadableString(sizeQuota.getLimit()));
            }
        }
        final DatabaseService databaseService = getDatabaseService();
        final int contextId = this.contextId;
        Connection con = null;
        PreparedStatement stmt = null;
        boolean backAfterRead=false;
        try {
            final MimeMessage mimeMessage = new MimeMessage(getDefaultSession());
            mimeMessage.setHeader(Property.CREATED_BY.getPropName(), Integer.toString(userId));
            // Set headers
            for (final Map.Entry<String, Object> entry : snippet.getProperties().entrySet()) {
                final String name = entry.getKey();
                if (!IGNORABLES.contains(name)) {
                    mimeMessage.setHeader(name, encode(entry.getValue().toString()));
                }
            }

            // Sanitize content to be an empty string if null is passed
            String content = snippet.getContent();
            if (content == null) {
                // Set to empty string
                content = "";
            }

            // Some variables
            Object misc = snippet.getMisc();
            String contentSubType = determineContentSubtype(misc);

            // Set other stuff
            final List<Attachment> attachments = snippet.getAttachments();
            if (notEmpty(attachments) || (null != misc)) {
                final MimeMultipart multipart = new MimeMultipart("mixed");
                // Content part
                {
                    final MimeBodyPart textPart = new MimeBodyPart();
                    MessageUtility.setText(sanitizeContent(content), "UTF-8", null == misc ? "plain" : contentSubType, textPart);
                    // textPart.setText(sanitizeContent(snippet.getContent()), "UTF-8", "plain");
                    multipart.addBodyPart(textPart);
                }
                // Misc
                if (null != misc) {
                    final MimeBodyPart miscPart = new MimeBodyPart();
                    miscPart.setDataHandler(new DataHandler(new MessageDataSource(misc.toString(), "text/javascript; charset=UTF-8")));
                    miscPart.setHeader(MessageHeaders.HDR_MIME_VERSION, "1.0");
                    miscPart.setHeader(MessageHeaders.HDR_CONTENT_TYPE, MimeMessageUtility.foldContentType("text/javascript; charset=UTF-8"));
                    multipart.addBodyPart(miscPart);
                }
                // Attachments
                if (notEmpty(attachments)) {
                    for (final Attachment attachment : attachments) {
                        if (null == attachment.getId()) {
                            if (!(attachment instanceof DefaultAttachment)) {
                                throw SnippetExceptionCodes.ILLEGAL_STATE.create("Missing attachment identifier");
                            }
                            ((DefaultAttachment) attachment).setId(UUID.randomUUID().toString());
                        }
                        multipart.addBodyPart(attachment2MimePart(attachment));
                    }
                }
                // Apply multipart
                MessageUtility.setContent(multipart, mimeMessage);
            } else {
                // The variable "misc" can only be null at this location
                MessageUtility.setText(sanitizeContent(content), "UTF-8", "plain", mimeMessage);
            }
            // Save
            mimeMessage.saveChanges();
            mimeMessage.removeHeader("Message-ID");
            mimeMessage.removeHeader("MIME-Version");
            // Save MIME content to file storage
            FileStorage fileStorage = getFileStorage(session.getContextId());
            String file;
            long size = -1L;
            {
                ThresholdFileHolder fileHolder = new ThresholdFileHolder();
                try {
                    mimeMessage.writeTo(fileHolder.asOutputStream());
                    size = fileHolder.getLength();
                    if (null != quota) {
                        Quota sizeQuota = quota.getQuota(QuotaType.SIZE);
                        if (null != sizeQuota && (sizeQuota.isExceeded() || sizeQuota.willExceed(size))) {
                            backAfterRead = true;
                            throw QuotaExceptionCodes.QUOTA_EXCEEDED_SIGNATURES.create(bytesToReadableString(sizeQuota.getLimit()));
                        }
                    }
                    file = fileStorage.saveNewFile(fileHolder.getClosingStream());
                } finally {
                    fileHolder.close();
                }
            }
            // Store in DB, too
            con = databaseService.getWritable(contextId);
            String newId = null;
            boolean error = true;
            try {
                newId = Integer.toString(getIdGeneratorService().getId("com.openexchange.snippet.mime", contextId));
                stmt = con.prepareStatement("INSERT INTO snippet (cid, user, id, accountId, displayName, module, type, shared, shared_read_only, lastModified, refId, refType, size) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + FS_TYPE + ", ?)");
                stmt.setInt(1, contextId);
                stmt.setInt(2, userId);
                stmt.setString(3, newId);
                {
                    final int accountId = snippet.getAccountId();
                    if (accountId >= 0) {
                        stmt.setInt(4, accountId);
                    } else {
                        stmt.setNull(4, Types.INTEGER);
                    }
                }
                stmt.setString(5, snippet.getDisplayName());
                stmt.setString(6, snippet.getModule());
                stmt.setString(7, snippet.getType());
                stmt.setInt(8, snippet.isShared() ? 1 : 0);
                stmt.setInt(9, snippet.isSharedReadOnly() ? 1 : 0);
                stmt.setLong(10, System.currentTimeMillis());
                stmt.setString(11, file);
                // refType is set hard-coded
                if (size > 0) {
                    stmt.setLong(12, size);
                } else {
                    stmt.setNull(12, Types.INTEGER);
                }
                stmt.executeUpdate();
                error = false;
            } finally {
                if (error) {
                    // Delete file on error
                    fileStorage.deleteFile(file);
                    newId = null;
                }
            }
            // Return identifier
            return newId;
        } catch (MessagingException e) {
            throw SnippetExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (IOException e) {
            throw SnippetExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (DataTruncation e) {
            throw SnippetExceptionCodes.DATA_TRUNCATION_ERROR.create(e);
        } catch (SQLException e) {
            throw SnippetExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw SnippetExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(stmt);
            if (con != null) {
                if (backAfterRead){
                    databaseService.backWritableAfterReading(contextId, con);
                }else {
                    databaseService.backWritable(contextId, con);
                }
            }
        }
    }

    private static final List<String> UNITS = Arrays.asList("B", "KB", "MB", "GB", "TB");

    private static String bytesToReadableString(long bytes) {
        int x = 0;
        double tmp = bytes;
        while (tmp > 1024) {
            x++;
            tmp = tmp / 1024;
        }

        return new StringBuilder(String.valueOf(Integer.valueOf((int) Math.floor(tmp)))).append(' ').append(UNITS.get(x)).toString();
    }

    @Override
    public String updateSnippet(String identifier, Snippet snippet, Set<Property> properties, Collection<Attachment> addAttachments, Collection<Attachment> removeAttachments) throws OXException {
        if (null == identifier) {
            return null;
        }
        AccountQuota quota = getQuota();
        final DatabaseService databaseService = getDatabaseService();
        final int contextId = this.contextId;
        final FileStorage fileStorage = getFileStorage(session.getContextId());
        boolean error = true;
        String oldFile = null;
        String newFile = null;
        try {
            // Obtain file identifier
            final String displayName;
            final String module;
            final String type;
            final boolean shared;
            final long oldSize;
            final boolean sharedReadOnly;
            {
                final Connection con = databaseService.getReadOnly(contextId);
                PreparedStatement stmt = null;
                ResultSet rs = null;
                try {
                    stmt = con.prepareStatement("SELECT refId, displayName, module, type, shared, size, shared_read_only FROM snippet WHERE cid=? AND id=? AND refType=" + FS_TYPE);
                    int pos = 0;
                    stmt.setInt(++pos, contextId);
                    stmt.setString(++pos, identifier);
                    rs = stmt.executeQuery();
                    if (!rs.next()) {
                        throw SnippetExceptionCodes.SNIPPET_NOT_FOUND.create(identifier);
                    }
                    oldFile = rs.getString(1);
                    if (null == oldFile) {
                        throw SnippetExceptionCodes.SNIPPET_NOT_FOUND.create(identifier);
                    }
                    displayName = rs.getString(2);
                    module = rs.getString(3);
                    type = rs.getString(4);
                    shared = rs.getInt(5) > 0;
                    oldSize = rs.getLong(6);
                    sharedReadOnly = rs.getInt(7) > 0;
                } finally {
                    Databases.closeSQLStuff(rs, stmt);
                    databaseService.backReadOnly(contextId, con);
                }
            }
            // Create MIME message from existing file
            MimeMessage storageMessage;
            {
                InputStream in = null;
                try {
                    in = fileStorage.getFile(oldFile);
                    storageMessage = new MimeMessage(getDefaultSession(), in);
                } catch (OXException e) {
                    if (!FileStorageCodes.FILE_NOT_FOUND.equals(e)) {
                        throw e;
                    }
                    throw SnippetExceptionCodes.SNIPPET_NOT_FOUND.create(e, identifier);
                } finally {
                    Streams.close(in);
                }
            }
            final ContentType storageContentType;
            {
                final String header = storageMessage.getHeader(MessageHeaders.HDR_CONTENT_TYPE, null);
                storageContentType = isEmpty(header) ? ContentType.DEFAULT_CONTENT_TYPE : new ContentType(header);
            }

            // New MIME message for changes
            MimeMessage updateMessage = new MimeMessage(getDefaultSession());

            // Update properties
            {
                List<String> propNames = new ArrayList<>(properties.size());
                for (Property property : properties) {
                    switch (property) {
                        case ACCOUNT_ID:
                            updateMessage.setHeader(Property.ACCOUNT_ID.getPropName(), Integer.toString(snippet.getAccountId()));
                            propNames.add(Snippet.PROP_ACCOUNT_ID);
                            break;
                        case CREATED_BY:
                            updateMessage.setHeader(Property.CREATED_BY.getPropName(), Integer.toString(snippet.getCreatedBy()));
                            propNames.add(Snippet.PROP_CREATED_BY);
                            break;
                        case DISPLAY_NAME:
                            updateMessage.setHeader(Property.DISPLAY_NAME.getPropName(), encode(snippet.getDisplayName()));
                            propNames.add(Snippet.PROP_DISPLAY_NAME);
                            break;
                        case MODULE:
                            updateMessage.setHeader(Property.MODULE.getPropName(), snippet.getModule());
                            propNames.add(Snippet.PROP_MODULE);
                            break;
                        case SHARED:
                            updateMessage.setHeader(Property.SHARED.getPropName(), Boolean.toString(snippet.isShared()));
                            propNames.add(Snippet.PROP_SHARED);
                            break;
                        case SHARED_READ_ONLY:
                            updateMessage.setHeader(Property.SHARED_READ_ONLY.getPropName(), Boolean.toString(snippet.isSharedReadOnly()));
                            propNames.add(Snippet.PROP_SHARED_READ_ONLY);
                            break;
                        case TYPE:
                            updateMessage.setHeader(Property.TYPE.getPropName(), snippet.getType());
                            propNames.add(Snippet.PROP_TYPE);
                            break;
                        default:
                            break;
                    }
                }

                // Copy remaining to updateMessage; this action includes unnamed properties
                Enumeration<Header> nonMatchingHeaders = storageMessage.getNonMatchingHeaders(propNames.toArray(Strings.getEmptyStrings()));
                final Set<String> propertyNames = Property.getPropertyNames();
                while (nonMatchingHeaders.hasMoreElements()) {
                    final Header hdr = nonMatchingHeaders.nextElement();
                    if (propertyNames.contains(hdr.getName())) {
                        updateMessage.setHeader(hdr.getName(), encode(hdr.getValue()));
                    }
                }
            }

            // Check for content
            String content;
            if (properties.contains(Property.CONTENT)) {
                content = snippet.getContent();
            } else {
                final MimePart textPart;
                final ContentType ct;
                if (storageContentType.startsWith("multipart/")) {
                    textPart = (MimePart) ((Multipart) storageMessage.getContent()).getBodyPart(0);
                    final String header = textPart.getHeader(MessageHeaders.HDR_CONTENT_TYPE, null);
                    ct = isEmpty(header) ? ContentType.DEFAULT_CONTENT_TYPE : new ContentType(header);
                } else {
                    textPart = storageMessage;
                    ct = storageContentType;
                }
                content = MessageUtility.readMimePart(textPart, ct);
            }

            // Extract image identifiers
            Set<String> contentIds = new HashSet<String>(MimeMessageUtility.getContentIDs(content));
            contentIds.addAll(extractContentIDs(content));

            // Check for misc
            MimePart miscPart;
            if (properties.contains(Property.MISC)) {
                final Object misc = snippet.getMisc();

                if (null == misc) {
                    miscPart = null;
                } else {
                    miscPart = new MimeBodyPart();
                    miscPart.setDataHandler(new DataHandler(new MessageDataSource(misc.toString(), "text/javascript; charset=UTF-8")));
                    miscPart.setHeader(MessageHeaders.HDR_MIME_VERSION, "1.0");
                    miscPart.setHeader(MessageHeaders.HDR_CONTENT_TYPE, MimeMessageUtility.foldContentType("text/javascript; charset=UTF-8"));
                }
            } else {
                if (storageContentType.startsWith("multipart/")) {
                    Multipart multipart = (Multipart) storageMessage.getContent();
                    int length = multipart.getCount();
                    MimePart mp = null;
                    for (int i = 1; null == mp && i < length; i++) { // skip first
                        BodyPart bodyPart = multipart.getBodyPart(i);
                        String header = Strings.asciiLowerCase(MimeMessageUtility.getHeader(MessageHeaders.HDR_CONTENT_TYPE, null, bodyPart));
                        if (null != header && header.startsWith("text/javascript")) {
                            mp = (MimePart) bodyPart;
                        }
                    }
                    miscPart = mp;
                } else {
                    miscPart = null;
                }
            }

            // Check for attachments
            List<MimeBodyPart> attachmentParts = new ArrayList<MimeBodyPart>();

            // Add existing
            if (storageContentType.startsWith("multipart/")) {
                Multipart storageMultipart = (Multipart) storageMessage.getContent();
                int length = storageMultipart.getCount();
                for (int i = 1; i < length; i++) { // skip first
                    MimeBodyPart bodyPart = (MimeBodyPart) storageMultipart.getBodyPart(i);
                    String header = Strings.asciiLowerCase(MimeMessageUtility.getHeader(MessageHeaders.HDR_CONTENT_TYPE, null, bodyPart));
                    if (null == header) {
                        attachmentParts.add(bodyPart);
                    } else if (!header.startsWith("text/javascript")) {
                        // Check for inline image attachment
                        if (header.startsWith("image/")) {
                            String optContentId = MimeMessageUtility.getHeader(MessageHeaders.HDR_CONTENT_ID, null, bodyPart);
                            if (null == content) {
                                attachmentParts.add(bodyPart);
                            } else {
                                String disp = MimeMessageUtility.getHeader(MessageHeaders.HDR_CONTENT_DISPOSITION, null, bodyPart);
                                if (null != disp && Strings.asciiLowerCase(disp).trim().startsWith("inline")) {
                                    // Still referenced in HTML content
                                    if (contentIds.contains(MimeMessageUtility.trimContentId(optContentId))) {
                                        attachmentParts.add(bodyPart);
                                    }
                                } else {
                                    attachmentParts.add(bodyPart);
                                }
                            }
                        } else {
                            attachmentParts.add(bodyPart);
                        }
                    }
                }
            }

            // Removed
            if (notEmpty(removeAttachments)) {
                for (Attachment attachment : removeAttachments) {
                    for (Iterator<MimeBodyPart> iterator = attachmentParts.iterator(); iterator.hasNext();) {
                        final String header = iterator.next().getHeader("attachmentid", null);
                        if (null != header && header.equals(attachment.getId())) {
                            iterator.remove();
                        }
                    }
                }
            }

            // New ones
            if (notEmpty(addAttachments)) {
                for (Attachment attachment : addAttachments) {
                    attachmentParts.add(attachment2MimePart(attachment));
                }
            }
            // Check gathered parts
            if (null != miscPart || notEmpty(attachmentParts)) {
                // Create a multipart message
                Multipart primaryMultipart = new MimeMultipart();

                // Add text part
                MimeBodyPart textPart = new MimeBodyPart();
                String subType = determineContentSubtype(snippet.getMisc());
                // MessageUtility.setText(sanitizeContent(snippet.getContent()), "UTF-8", null == miscPart ? "plain" :
                // determineContentSubtype(MessageUtility.readMimePart(miscPart, "UTF-8")), textPart);
                textPart.setText(sanitizeContent(content), "UTF-8", subType);
                textPart.setHeader(MessageHeaders.HDR_MIME_VERSION, "1.0");
                primaryMultipart.addBodyPart(textPart);

                // Add attachment parts
                if (notEmpty(attachmentParts)) {
                    for (final MimeBodyPart mimePart : attachmentParts) {
                        primaryMultipart.addBodyPart(mimePart);
                    }
                }

                // Add misc part
                if (null != miscPart) {
                    primaryMultipart.addBodyPart((BodyPart) miscPart);
                }

                // Apply to message
                updateMessage.setContent(primaryMultipart);

                // MessageUtility.setContent(primaryMultipart, updateMessage);
                // updateMessage.setContent(primaryMultipart);
            } else {
                MessageUtility.setText(sanitizeContent(content), "UTF-8", "plain", updateMessage);
                // updateMessage.setText(sanitizeContent(content), "UTF-8", "plain");
                updateMessage.setHeader(MessageHeaders.HDR_MIME_VERSION, "1.0");
            }

            // Save to MIME structure...
            updateMessage.saveChanges();
            updateMessage.removeHeader("Message-ID");
            updateMessage.removeHeader("MIME-Version");

            ThresholdFileHolder fileHolder = new ThresholdFileHolder();
            long size = 0;
            try {
                updateMessage.writeTo(fileHolder.asOutputStream());
                size = fileHolder.getLength();
                long difference = size - oldSize;
                if (difference > 0 && null != quota) {
                    Quota sizeQuota = quota.getQuota(QuotaType.SIZE);
                    if (null != sizeQuota && (sizeQuota.isExceeded() || sizeQuota.willExceed(difference))) {
                        throw QuotaExceptionCodes.QUOTA_EXCEEDED_SIGNATURES.create(bytesToReadableString(sizeQuota.getLimit()));
                    }
                }

                // Create file carrying new MIME data
                newFile = fileStorage.saveNewFile(fileHolder.getClosingStream());
            } finally {
                fileHolder.close();
            }

            {
                Connection con = databaseService.getWritable(contextId);
                PreparedStatement stmt = null;
                int rollback = 0;
                try {
                    /*-
                     * Update DB, too
                     *
                     * 1. Create dummy entry to check DB schema consistency
                     * 2. Delete existing
                     * 3. Make dummy entry the real entry
                     */
                    final String dummyId = "--" + identifier;
                    con.setAutoCommit(false); // BEGIN
                    rollback = 1;
                    stmt = con.prepareStatement("INSERT INTO snippet (cid, user, id, accountId, displayName, module, type, shared, shared_read_only, lastModified, refId, refType, size) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + FS_TYPE + ", ?, ?)");
                    int pos = 0;
                    stmt.setInt(++pos, contextId);
                    stmt.setInt(++pos, userId);
                    stmt.setString(++pos, dummyId);
                    {
                        final String sAccountId = updateMessage.getHeader(Property.ACCOUNT_ID.getPropName(), null);
                        if (sAccountId != null) {
                            stmt.setInt(++pos, Integer.parseInt(sAccountId));
                        } else {
                            stmt.setNull(++pos, Types.INTEGER);
                        }
                    }
                    stmt.setString(++pos, properties.contains(Property.DISPLAY_NAME) ? getObject(snippet.getDisplayName(), displayName) : displayName);
                    stmt.setString(++pos, properties.contains(Property.MODULE) ? getObject(snippet.getModule(), module) : module);
                    stmt.setString(++pos, properties.contains(Property.TYPE) ? getObject(snippet.getType(), type) : type);
                    stmt.setInt(++pos, properties.contains(Property.SHARED) ? (snippet.isShared() ? 1 : 0) : (shared ? 1 : 0));
                    stmt.setInt(++pos, properties.contains(Property.SHARED_READ_ONLY) ? (snippet.isSharedReadOnly() ? 1 : 0) : (sharedReadOnly ? 1 : 0));
                    stmt.setLong(++pos, System.currentTimeMillis());
                    stmt.setString(++pos, newFile);
                    if (size > 0) {
                        stmt.setLong(++pos, size);
                    } else {
                        stmt.setNull(++pos, Types.INTEGER);
                    }
                    stmt.executeUpdate();
                    Databases.closeSQLStuff(stmt);
                    stmt = con.prepareStatement("DELETE FROM snippet WHERE cid=? AND user=? AND id=? AND refType=" + FS_TYPE);
                    pos = 0;
                    stmt.setLong(++pos, contextId);
                    stmt.setLong(++pos, userId);
                    stmt.setString(++pos, identifier);
                    stmt.executeUpdate();
                    Databases.closeSQLStuff(stmt);
                    stmt = con.prepareStatement("UPDATE snippet SET id=? WHERE cid=? AND user=? AND id=? AND refType=" + FS_TYPE);
                    pos = 0;
                    stmt.setString(++pos, identifier);
                    stmt.setLong(++pos, contextId);
                    stmt.setLong(++pos, userId);
                    stmt.setString(++pos, dummyId);
                    stmt.executeUpdate();
                    con.commit(); // COMMIT
                    rollback = 2;
                } finally {
                    Databases.closeSQLStuff(stmt);
                    if (rollback > 0) {
                        if (rollback == 1) {
                            Databases.rollback(con);
                        }
                        Databases.autocommit(con);
                    }
                    databaseService.backWritable(contextId, con);
                }
            }

            // Mark as successfully processed
            error = false;
            return identifier;
        } catch (MessagingException e) {
            throw SnippetExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (IOException e) {
            throw SnippetExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (DataTruncation e) {
            throw SnippetExceptionCodes.DATA_TRUNCATION_ERROR.create(e);
        } catch (SQLException e) {
            throw SnippetExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw SnippetExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (error) {
                // Delete newly created file
                deleteSafe(newFile, fileStorage);
            } else {
                // Delete obsolete file
                deleteSafe(oldFile, fileStorage);
            }
        }
    }

    /**
     * Extracts image identifiers from given HTML content
     *
     * @param htmlContent The HTML content
     * @return The extracted image identifiers
     */
    protected static Set<String> extractContentIDs(String htmlContent) {
        // Fast check
        if (htmlContent.indexOf("<img") < 0) {
            return Collections.emptySet();
        }

        Matcher matcher = MimeMessageUtility.PATTERN_SRC.matcher(htmlContent);
        if (!matcher.find()) {
            return Collections.emptySet();
        }

        Set<String> result = HashSet.newHashSet(2);
        do {
            String imageUri = matcher.group(1);
            if (!imageUri.startsWith("cid:")) {
                try {
                    ImageLocation imageLocation = ImageUtility.parseImageLocationFrom(imageUri);
                    if (null != imageLocation) {
                        String imageId = imageLocation.getImageId();
                        if (null != imageId) {
                            result.add(imageId);
                        }
                    }
                } catch (IllegalArgumentException e) {
                    LOGGER.debug("Unable to find image location for uri: {}", imageUri);
                    // Probably an external image
                }
            }
        } while (matcher.find());
        return result;
    }

    private static void deleteSafe(String file, FileStorage fileStorage) {
        if (null == file) {
            return;
        }
        try {
            fileStorage.deleteFile(file);
        } catch (Exception e) {
            // Ignore any regular exception
        }
    }

    private static <V> V getObject(V o1, V o2) {
        return o1 == null ? (o2 == null ? o1 : o2) : o1;
    }

    @Override
    public void deleteSnippet(final String id) throws OXException {
        DatabaseService databaseService = getDatabaseService();

        int contextId = this.contextId;
        Connection con = databaseService.getWritable(contextId);
        try {
            deleteSnippet(id, userId, contextId, con);
        } finally {
            databaseService.backWritable(contextId, con);
        }
    }

    /**
     * Safely deletes specified snippet
     *
     * @param identifier The snippet identifier
     * @param userId The user identifier
     * @param contextId The context identifier
     * @throws OXException If delete attempt fails
     */
    public static void deleteSnippetSafe(String identifier, int userId, int contextId) {
        DatabaseService databaseService = getDatabaseService();

        Connection con = null;
        try {
            con = databaseService.getWritable(contextId);
            deleteSnippetSafe(identifier, userId, contextId, con);
        } catch (Exception e) {
            // Ignore
        } finally {
            if (null != con) {
                databaseService.backWritable(contextId, con);
            }
        }
    }

    /**
     * Safely deletes specified snippet
     *
     * @param identifier The snippet identifier
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param con The connection to use
     * @throws OXException If delete attempt fails
     */
    public static void deleteSnippetSafe(String identifier, int userId, int contextId, Connection con) {
        if (null == con) {
            deleteSnippetSafe(identifier, userId, contextId);
            return;
        }
        try {
            deleteSnippet(identifier, userId, contextId, con);
        } catch (Exception e) {
            LOGGER.warn("Failed to delete snippet {} for user {} in context {}", identifier, I(userId), I(contextId), e);
        }
    }

    /**
     * Deletes specified snippet
     *
     * @param identifier The snippet identifier
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param con The connection to use
     * @throws OXException If delete attempt fails
     */
    public static void deleteSnippet(String identifier, int userId, int contextId, Connection con) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            final String file;
            {
                stmt = con.prepareStatement("SELECT refId FROM snippet WHERE cid=? AND id=? AND refType=" + FS_TYPE);
                int pos = 0;
                stmt.setInt(++pos, contextId);
                stmt.setString(++pos, identifier);
                rs = stmt.executeQuery();
                if (!rs.next()) {
                    throw SnippetExceptionCodes.SNIPPET_NOT_FOUND.create(identifier);
                }
                file = rs.getString(1);
                Databases.closeSQLStuff(rs, stmt);
                rs = null;
                stmt = null;
            }

            final FileStorage fileStorage = getFileStorage(contextId);
            deleteSafe(file, fileStorage);
            stmt = con.prepareStatement("DELETE FROM snippet WHERE cid=? AND user=? AND id=? AND refType=" + FS_TYPE);
            int pos = 0;
            stmt.setLong(++pos, contextId);
            stmt.setLong(++pos, userId);
            stmt.setString(++pos, identifier);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw SnippetExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw SnippetExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    private static MimeBodyPart attachment2MimePart(Attachment attachment) throws MessagingException, IOException, OXException {
        /*
         * Content-Type
         */
        String header = attachment.getContentType();
        final String contentType = isEmpty(header) ? "text/plain; charset=UTF-8" : header;
        final MimeBodyPart bodyPart = new MimeBodyPart();
        /*
         * Content
         */
        {
            ThresholdFileHolder fileHolder;
            InputStream inputStream = attachment.getInputStream();
            try {
                if (inputStream instanceof ThresholdFileHolderInputStream) {
                    ThresholdFileHolderInputStream fileHolderInputStream = (ThresholdFileHolderInputStream) inputStream;
                    fileHolder = fileHolderInputStream.getFileHolder();
                } else {
                    fileHolder = new ThresholdFileHolder();
                    fileHolder.write(inputStream);
                }
            } finally {
                Streams.close(inputStream);
            }

            File tempFile = fileHolder.getTempFile();
            if (null == tempFile) {
                bodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(fileHolder.getBuffer().toByteArray(), contentType)));
            } else {
                bodyPart.setDataHandler(new DataHandler(new FileDataSource(tempFile, contentType)));
            }
        }
        /*
         * Rest...
         */
        bodyPart.setHeader(MessageHeaders.HDR_MIME_VERSION, "1.0");
        bodyPart.setHeader(MessageHeaders.HDR_CONTENT_TYPE, MimeMessageUtility.foldContentType(contentType));
        String attachmentId = attachment.getId();
        if (null == attachmentId) {
            attachmentId = UUID.randomUUID().toString();
        }
        bodyPart.setHeader("attachmentid", attachmentId);
        header = attachment.getContentId();
        if (null == header) {
            header = "<" + UUIDs.getUnformattedString(UUID.randomUUID()) + ">";
        }
        bodyPart.setHeader(MessageHeaders.HDR_CONTENT_ID, header.startsWith("<") ? header : ("<" + header + ">"));
        /*
         * Force base64 encoding
         */
        bodyPart.setHeader(MessageHeaders.HDR_CONTENT_TRANSFER_ENC, "base64");
        /*
         * Content-Disposition
         */
        header = attachment.getContentDisposition();
        if (null != header) {
            bodyPart.setHeader(MessageHeaders.HDR_CONTENT_DISPOSITION, MimeMessageUtility.foldContentDisposition(header));
        }
        return bodyPart;
    }

    private static <E> boolean notEmpty(final Collection<E> col) {
        return null != col && !col.isEmpty();
    }

    private static boolean isEmpty(final String string) {
        return com.openexchange.java.Strings.isEmpty(string);
    }

    private static String determineContentSubtype(final Object misc) throws OXException {
        if (misc == null) {
            return "plain";
        }
        final String ct = SnippetUtils.parseContentTypeFromMisc(misc);
        return Strings.asciiLowerCase(new ContentType(ct).getSubType());
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    /** Helper class */
    private static class SnippetInfo {

        final String identifier;
        final String file;
        final int creator;
        final String displayName;
        final String module;
        final String type;
        final boolean shared;
        final boolean sharedReadOnly;

        /**
         * Initializes a new {@link SnippetInfo}.
         */
        SnippetInfo(String identifier, String file, int creator, String displayName, String module, String type, boolean shared, boolean sharedReadOnly) {
            super();
            this.identifier = identifier;
            this.file = file;
            this.creator = creator;
            this.displayName = displayName;
            this.module = module;
            this.type = type;
            this.shared = shared;
            this.sharedReadOnly = sharedReadOnly;
        }

    }

}
