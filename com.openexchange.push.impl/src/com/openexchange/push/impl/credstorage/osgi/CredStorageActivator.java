/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.push.impl.credstorage.osgi;

import static com.openexchange.osgi.Tools.withRanking;
import java.util.Dictionary;
import java.util.Hashtable;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.config.ConfigurationService;
import com.openexchange.crypto.CryptoService;
import com.openexchange.database.CreateTableService;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.delete.DeleteListener;
import com.openexchange.groupware.update.DefaultUpdateTaskProviderService;
import com.openexchange.groupware.update.UpdateTaskProviderService;
import com.openexchange.java.Strings;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.push.credstorage.CredentialStorage;
import com.openexchange.push.credstorage.CredentialStorageProvider;
import com.openexchange.push.impl.credstorage.OSGiCredentialStorageProvider;
import com.openexchange.push.impl.credstorage.Obfuscator;
import com.openexchange.push.impl.credstorage.inmemory.ClusterMapCredentialStorage;
import com.openexchange.push.impl.credstorage.rdb.RdbCredentialStorage;
import com.openexchange.push.impl.credstorage.rdb.groupware.CreateCredStorageTable;
import com.openexchange.push.impl.credstorage.rdb.groupware.CredConvertUtf8ToUtf8mb4Task;
import com.openexchange.push.impl.credstorage.rdb.groupware.CredStorageCreateTableTask;
import com.openexchange.push.impl.credstorage.rdb.groupware.CredStorageDeleteListener;
import com.openexchange.push.impl.credstorage.rdb.groupware.AdjustCredentialsTableTask;


/**
 * {@link CredStorageActivator}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.0
 */
public class CredStorageActivator extends HousekeepingActivator {

    /**
     * Initializes a new {@link CredStorageActivator}.
     */
    public CredStorageActivator() {
        super();
    }

    @Override
    public <S> boolean removeService(Class<? extends S> clazz) {
        return super.removeService(clazz);
    }

    @Override
    public <S> boolean addService(Class<S> clazz, S service) {
        return super.addService(clazz, service);
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { ConfigurationService.class, CryptoService.class, DatabaseService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        final Logger log = org.slf4j.LoggerFactory.getLogger(CredStorageActivator.class);
        CredStorageServices.setServiceLookup(this);
        final BundleContext context = this.context;

        ConfigurationService configService = getService(ConfigurationService.class);
        boolean credStoreEnabled = configService.getBoolProperty("com.openexchange.push.credstorage.enabled", false);

        final ClusterMapCredentialStorage cmCredStorage;
        final RdbCredentialStorage rdbCredStorage;
        if (credStoreEnabled) {
            String key = configService.getProperty("com.openexchange.push.credstorage.passcrypt");
            if (Strings.isEmpty(key)) {
                throw new BundleException("Property \"com.openexchange.push.credstorage.enabled\" set to \"true\", but missing value for \"com.openexchange.push.credstorage.passcrypt\" property.");
            }

            Obfuscator obfuscator = new Obfuscator(key.trim());

            cmCredStorage = new ClusterMapCredentialStorage(obfuscator, this);
            rdbCredStorage = configService.getBoolProperty("com.openexchange.push.credstorage.rdb", false) ? new RdbCredentialStorage(obfuscator) : null;
            // Check cluster map stuff
            {
                // Track cluster map service
                ServiceTrackerCustomizer<ClusterMapService, ClusterMapService> customizer = new ServiceTrackerCustomizer<ClusterMapService, ClusterMapService>() {

                    @Override
                    public ClusterMapService addingService(ServiceReference<ClusterMapService> reference) {
                        ClusterMapService clusterMapService = context.getService(reference);
                        try {
                            addService(ClusterMapService.class, clusterMapService);
                            cmCredStorage.changeBackingMapToClusterMap();
                            return clusterMapService;
                        } catch (OXException e) {
                            log.warn("Couldn't initialize remote credentials map.", e);
                        } catch (RuntimeException e) {
                            log.warn("Couldn't initialize remote credentials map.", e);
                        }
                        context.ungetService(reference);
                        return null;
                    }

                    @Override
                    public void modifiedService(ServiceReference<ClusterMapService> reference, ClusterMapService service) {
                        // Ignore
                    }

                    @Override
                    public void removedService(ServiceReference<ClusterMapService> reference, ClusterMapService service) {
                        removeService(ClusterMapService.class);
                        cmCredStorage.changeBackingMapToLocalMap();
                        context.ungetService(reference);
                    }
                };
                track(ClusterMapService.class, customizer);
            }
        } else {
            cmCredStorage = null;
            rdbCredStorage = null;
        }

        OSGiCredentialStorageProvider storageProvider = new OSGiCredentialStorageProvider(context);
        rememberTracker(storageProvider);

        openTrackers();

        {
            CredStoragePasswordChangeHandler handler = new CredStoragePasswordChangeHandler();
            Dictionary<String, Object> props = new Hashtable<String, Object>(2);
            props.put(EventConstants.EVENT_TOPIC, handler.getTopic());
            registerService(EventHandler.class, handler, props);
        }

        registerService(CreateTableService.class, new CreateCredStorageTable());
        registerService(DeleteListener.class, new CredStorageDeleteListener());
        registerService(UpdateTaskProviderService.class, new DefaultUpdateTaskProviderService(new CredStorageCreateTableTask(), new CredConvertUtf8ToUtf8mb4Task(),
                                                                                              new AdjustCredentialsTableTask(configService, getService(CryptoService.class))));

        registerService(CredentialStorageProvider.class, storageProvider);
        addService(CredentialStorageProvider.class, storageProvider);
        if (null != cmCredStorage) {
            registerService(CredentialStorage.class, cmCredStorage, withRanking(0));
        }
        if (null != rdbCredStorage) {
            // Higher ranked
            registerService(CredentialStorage.class, rdbCredStorage, withRanking(10));
        }
    }

    @Override
    protected void stopBundle() throws Exception {
        CredStorageServices.setServiceLookup(null);
        super.stopBundle();
    }

}
