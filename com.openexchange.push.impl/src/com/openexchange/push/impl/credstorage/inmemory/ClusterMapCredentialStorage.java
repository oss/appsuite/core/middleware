/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.push.impl.credstorage.inmemory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.json.JSONObject;
import org.slf4j.Logger;
import com.openexchange.annotation.NonNull;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.ClusterMap;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.cluster.map.RemoteSiteOptions;
import com.openexchange.cluster.map.codec.AbstractJSONMapCodec;
import com.openexchange.cluster.map.codec.MapCodec;
import com.openexchange.concurrent.Blocker;
import com.openexchange.concurrent.ConcurrentBlocker;
import com.openexchange.exception.OXException;
import com.openexchange.push.PushUser;
import com.openexchange.push.credstorage.CredentialStorage;
import com.openexchange.push.credstorage.Credentials;
import com.openexchange.push.credstorage.DefaultCredentials;
import com.openexchange.push.impl.credstorage.Obfuscator;
import com.openexchange.server.ServiceLookup;


/**
 * {@link ClusterMapCredentialStorage} - The credential storage using a cluster map.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.6.2
 */
public class ClusterMapCredentialStorage extends BasicCoreClusterMapProvider<Credentials> implements CredentialStorage {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ClusterMapCredentialStorage.class);

    private static MapCodec<Credentials> createCodec() {
        return new AbstractJSONMapCodec<Credentials>() {

            @Override
            protected @NonNull JSONObject writeJson(Credentials value) throws Exception {
                JSONObject jCreds = new JSONObject(4);
                jCreds.put("userId", value.getUserId());
                jCreds.put("contextId", value.getContextId());
                jCreds.putOpt("login", value.getLogin());
                jCreds.putOpt("password", value.getPassword());
                return jCreds;
            }

            @Override
            protected @NonNull Credentials parseJson(JSONObject jCreds) throws Exception {
                int userId = jCreds.optInt("userId", 0);
                int contextId = jCreds.optInt("contextId", 0);
                String login = jCreds.optString("login", null);
                String password = jCreds.optString("password", null);
                return DefaultCredentials.builder().withContextId(contextId).withUserId(userId).withLogin(login).withPassword(password).build();
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    private final Blocker blocker = new ConcurrentBlocker();
    private final ConcurrentMap<PushUser, Credentials> sources;
    private final Obfuscator obfuscator;

    private volatile boolean useClusterMap = false;

    /**
     * Initializes a new {@link ClusterMapCredentialStorage}.
     */
    public ClusterMapCredentialStorage(Obfuscator obfuscator, ServiceLookup services) {
        super(CoreMap.PUSH_CREDENTIALS, createCodec(), 0L, RemoteSiteOptions.ASYNC, () -> services.getServiceSafe(ClusterMapService.class));
        this.obfuscator = obfuscator;
        sources = new ConcurrentHashMap<>(256);
    }

    private String keyFor(PushUser user) {
        return new StringBuilder().append(user.getUserId()).append('@').append(user.getContextId()).toString();
    }

    // ---------------------------------------------------------------------------------------------------

    private void putCredentials(PushUser user, Credentials newObfuscatedCredentials) throws OXException {
        blocker.acquire();
        try {
            if (useClusterMap) {
                putCredentialsToClusterMap(user, newObfuscatedCredentials);
            } else {
                sources.put(user, newObfuscatedCredentials);
            }
        } finally {
            blocker.release();
        }
    }

    private void putCredentialsToClusterMap(PushUser user, Credentials newObfuscatedCredentials) throws OXException {
        ClusterMap<Credentials> clusterMap = getMap();
        clusterMap.put(keyFor(user), newObfuscatedCredentials);
    }

    private Credentials peekCredentials(PushUser user) throws OXException {
        blocker.acquire();
        try {
            return useClusterMap ? peekCredentialsFromClusterMap(user) : sources.get(user);
        } finally {
            blocker.release();
        }
    }

    private Credentials peekCredentialsFromClusterMap(PushUser user) throws OXException {
        ClusterMap<Credentials> clusterMap = getMap();
        return clusterMap.get(keyFor(user));
    }

    private boolean checkCredentials(PushUser user) throws OXException {
        blocker.acquire();
        try {
            return useClusterMap ? checkCredentialsFromClusterMap(user) : sources.containsKey(user);
        } finally {
            blocker.release();
        }
    }

    private boolean checkCredentialsFromClusterMap(PushUser user) throws OXException {
        ClusterMap<Credentials> clusterMap = getMap();
        return clusterMap.containsKey(keyFor(user));
    }

    private Credentials pollCredentials(PushUser user) throws OXException {
        blocker.acquire();
        try {
            return useClusterMap ? pollCredentialsFromClusterMap(user) : sources.remove(user);
        } finally {
            blocker.release();
        }
    }

    private Credentials pollCredentialsFromClusterMap(PushUser user) throws OXException {
        ClusterMap<Credentials> clusterMap = getMap();
        return clusterMap.remove(keyFor(user));
    }

    // ---------------------------------------------------------------------------------------------------

    @Override
    public boolean containsCredentials(int userId, int contextId) throws OXException {
        return checkCredentials(new PushUser(userId, contextId));
    }

    // ---------------------------------------------------------------------------------------------------

    @Override
    public Credentials getCredentials(int userId, int contextId) throws OXException {
        return obfuscator.unobfuscateCredentials(peekCredentials(new PushUser(userId, contextId)));
    }

    @Override
    public void storeCredentials(Credentials credentials) throws OXException {
        PushUser pushUser = new PushUser(credentials.getUserId(), credentials.getContextId());
        Credentials obfuscatedCredentials = obfuscator.obfuscateCredentials(credentials);

        Credentials curObfuscatedCredentials = peekCredentials(pushUser);
        if (null == curObfuscatedCredentials) {
            putCredentials(pushUser, obfuscatedCredentials);
        } else {
            if (curObfuscatedCredentials.getLogin().equals(obfuscatedCredentials.getLogin()) && curObfuscatedCredentials.getPassword().equals(obfuscatedCredentials.getPassword())) {
                return;
            }

            putCredentials(pushUser, obfuscatedCredentials);
        }
    }

    @Override
    public Credentials deleteCredentials(int userId, int contextId) throws OXException {
        return obfuscator.unobfuscateCredentials(pollCredentials(new PushUser(userId, contextId)));
    }

    // ---------------------------------------------------------------------------------------------------

   /**
    *
    */
    public void changeBackingMapToLocalMap() {
        blocker.block();
        try {
            // This happens if cluster map service is removed in the meantime. We cannot copy any information back to the local map.
            useClusterMap = false;
            LOG.info("Binary sources backing map changed to local");
        } finally {
            blocker.unblock();
        }
    }

   /**
    * @throws OXException
    */
    public void changeBackingMapToClusterMap() throws OXException {
        blocker.block();
        try {
            if (useClusterMap) {
                return;
            }

            ClusterMap<Credentials> clusterMap = getMap();
            // This MUST be synchronous!
            for (Map.Entry<PushUser, Credentials> entry : sources.entrySet()) {
                clusterMap.put(keyFor(entry.getKey()), entry.getValue());
            }
            sources.clear();
            useClusterMap = true;
            LOG.info("Remote credentials backing map changed to hazelcast");
        } finally {
            blocker.unblock();
        }
    }

    // ---------------------------------------------------------------------------------------------------------

}
