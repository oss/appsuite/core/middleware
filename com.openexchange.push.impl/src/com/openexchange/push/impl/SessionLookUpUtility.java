/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.push.impl;

import com.openexchange.push.PushUser;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessionMatcher;
import com.openexchange.sessiond.SessiondService;

/**
 * {@link SessionLookUpUtility} - Utility class to look-up a session.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.0
 */
public class SessionLookUpUtility {

    private final ServiceLookup services;
    private final PushManagerRegistry registry;

    /**
     * Initializes a new {@link SessionLookUpUtility}.
     */
    public SessionLookUpUtility(PushManagerRegistry registry, ServiceLookup services) {
        super();
        this.registry = registry;
        this.services = services;
    }

    /**
     * Attempts to look-up a session for specified user
     *
     * @param pushUser The user to look-up for
     * @param considerGenerated Whether to consider a generated session
     * @return The looked-up session or <code>null</code>
     */
    public Session lookUpSessionFor(PushUser pushUser, boolean considerGenerated) {
        int contextId = pushUser.getContextId();
        int userId = pushUser.getUserId();

        SessiondService sessiondService = services.getService(SessiondService.class);
        if (sessiondService == null) {
            throw new IllegalStateException("SessiondService is absent");
        }

        Session session = sessiondService.findFirstMatchingSessionForUser(userId, contextId, SessionMatcher.ALL_MATCHER);
        if (null == session && considerGenerated) {
            try {
                session = registry.generateSessionFor(pushUser);
            } catch (Exception e) {
                // Failed...
            }
        }

        return session;
    }

}
