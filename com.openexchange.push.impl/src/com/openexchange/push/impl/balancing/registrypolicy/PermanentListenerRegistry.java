/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.push.impl.balancing.registrypolicy;

import org.json.JSONObject;
import com.openexchange.annotation.NonNull;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.ClusterMap;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.cluster.map.codec.AbstractJSONMapCodec;
import com.openexchange.cluster.map.codec.MapCodec;
import com.openexchange.exception.OXException;
import com.openexchange.push.PushUser;


/**
 * {@link PermanentListenerRegistry} - A registry for permanent listeners.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.0
 */
public class PermanentListenerRegistry extends BasicCoreClusterMapProvider<Owner> {

    private static MapCodec<Owner> createCodec() {
        return new AbstractJSONMapCodec<Owner>() {

            @Override
            protected @NonNull JSONObject writeJson(Owner owner) throws Exception {
                JSONObject jOwner = new JSONObject(2);
                jOwner.putOpt("member", owner.getMember());
                jOwner.put("reason", owner.getReason().ordinal());
                return jOwner;
            }

            @Override
            protected @NonNull Owner parseJson(JSONObject jOwner) throws Exception {
                String member = jOwner.optString("member", null);
                int reason = jOwner.optInt("reason", 0);
                return new Owner(member, Reason.byOrdinal(reason));
            }
        };
    }

    /**
     * Initializes a new {@link PermanentListenerRegistry}.
     *
     * @param clusterMapService The cluster map service to use
     */
    public PermanentListenerRegistry(ClusterMapService clusterMapService) {
        super(CoreMap.PUSH_PERMANENT_LISTENER_REGISTRY, createCodec(), 0L, () -> clusterMapService);
    }

    private String keyFor(PushUser user) {
        return new StringBuilder().append(user.getUserId()).append('@').append(user.getContextId()).toString();
    }

    // ----------------------------------------------------------------------------------------------------------------------------

    /**
     * Puts the specified owner into this registry associated with given push user.
     * <p>
     * Existing entries are replaced.
     *
     * @param user The push user
     * @param newOwner The owner
     * @return The previous owner or <code>null</code>
     * @throws OXException
     */
    public Owner putOwner(PushUser user, Owner newOwner) throws OXException {
        ClusterMap<Owner> clusterMap = getMap();
        return clusterMap.put(keyFor(user), newOwner);
    }

    /**
     * Gets the owner from this registry associated with given push user.
     *
     * @param user The associated push user
     * @return The owner or <code>null</code>
     * @throws OXException
     */
    public Owner getOwner(PushUser user) throws OXException {
        ClusterMap<Owner> clusterMap = getMap();
        return clusterMap.get(keyFor(user));
    }

    /**
     * Removes the owner from this registry associated with given push user.
     *
     * @param user The associated push user
     * @return The removed owner or <code>null</code>
     * @throws OXException
     */
    public Owner removeOwner(PushUser user) throws OXException {
        ClusterMap<Owner> clusterMap = getMap();
        return clusterMap.remove(keyFor(user));
    }

}
