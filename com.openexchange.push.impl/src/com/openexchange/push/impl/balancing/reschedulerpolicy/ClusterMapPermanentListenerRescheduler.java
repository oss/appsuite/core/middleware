/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.push.impl.balancing.reschedulerpolicy;

import static com.openexchange.cluster.map.CoreMap.PUSH_RESCHEDULER;
import static com.openexchange.java.Autoboxing.I;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;
import org.json.SimpleJSONSerializer;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import com.openexchange.annotation.NonNull;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.ClusterMap;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.codec.AbstractJSONMapCodec;
import com.openexchange.cluster.map.codec.MapCodec;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.update.UpdaterEventConstants;
import com.openexchange.java.AsciiWriter;
import com.openexchange.java.Streams;
import com.openexchange.java.util.UUIDs;
import com.openexchange.push.PushUser;
import com.openexchange.push.impl.PushManagerRegistry;
import com.openexchange.push.impl.jobqueue.PermanentListenerJob;
import com.openexchange.push.impl.osgi.Services;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;


/**
 * {@link ClusterMapPermanentListenerRescheduler} - Reschedules permanent listeners.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.0.0
 */
public class ClusterMapPermanentListenerRescheduler extends BasicCoreClusterMapProvider<InstanceIdAndStamp> implements ServiceTrackerCustomizer<ClusterMapService, ClusterMapService>, EventHandler {

    /** The logger constant */
    static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ClusterMapPermanentListenerRescheduler.class);

    /**
     * Initializes a new {@link ClusterMapPermanentListenerRescheduler}.
     *
     * @param pushManagerRegistry The associated push manager registry
     * @param context The bundle context
     */
    public static ClusterMapPermanentListenerRescheduler newInstance(PushManagerRegistry pushManagerRegistry, BundleContext context) {
        AtomicReference<ClusterMapService> clusterMapServiceRef = new AtomicReference<>(null);
        return new ClusterMapPermanentListenerRescheduler(pushManagerRegistry, context, clusterMapServiceRef);
    }

    private static MapCodec<InstanceIdAndStamp> createCodec() {
        return new AbstractJSONMapCodec<InstanceIdAndStamp>() {

            @Override
            public InputStream serializeValue(InstanceIdAndStamp value) throws Exception {
                try {
                    ByteArrayOutputStream out = Streams.newByteArrayOutputStream(76);
                    AsciiWriter writer = new AsciiWriter(out);

                    writer.write('{');

                    SimpleJSONSerializer.writeJsonValue("instanceId", writer);
                    writer.write(':');
                    SimpleJSONSerializer.writeJsonValue(UUIDs.getUnformattedString(value.getInstanceId()), writer);

                    writer.write(',');
                    SimpleJSONSerializer.writeJsonValue("stamp", writer);
                    writer.write(':');
                    SimpleJSONSerializer.writeJsonValue(value.getStamp(), writer);

                    writer.write('}');

                    return Streams.asInputStream(out);
                } catch (Exception e) {
                    throw OXException.general("Serialisation failed", e);
                }
            }

            @Override
            protected @NonNull JSONObject writeJson(InstanceIdAndStamp value) throws Exception {
                JSONObject j = new JSONObject(2);
                j.put("instanceId", UUIDs.getUnformattedString(value.getInstanceId()));
                j.put("stamp", value.getStamp());
                return j;
            }

            @Override
            protected @NonNull InstanceIdAndStamp parseJson(JSONObject jObject) throws Exception {
                String instanceId = jObject.optString("instanceId", null);
                long stamp = jObject.optLong("stamp", 0);
                return new InstanceIdAndStamp(UUIDs.fromUnformattedString(instanceId), stamp);
            }
        };
    }

    // -------------------------------------------------------------------------------------------------------------------------------

    private final PushManagerRegistry pushManagerRegistry;
    private final BundleContext context;
    private final AtomicReference<ClusterMapService> clusterMapServiceRef;
    private final UUID instanceId;

    private boolean masterFlag; // Accessed synchronized
    private ScheduledTimerTask lockRefreshTask; // Accessed synchronized
    private ScheduledTimerTask lockCheckTask; // Accessed synchronized
    private boolean stopped; // Accessed synchronized

    /**
     * Initializes a new {@link ClusterMapPermanentListenerRescheduler}.
     *
     * @param pushManagerRegistry The associated push manager registry
     * @param context The bundle context
     * @param clusterMapServiceRef The reference for cluster map service
     */
    private ClusterMapPermanentListenerRescheduler(PushManagerRegistry pushManagerRegistry, BundleContext context, AtomicReference<ClusterMapService> clusterMapServiceRef) {
        super(PUSH_RESCHEDULER, createCodec(), LOCK_EXPIRE_MILLIS, clusterMapServiceRef::get);
        this.instanceId = UUID.randomUUID();
        this.clusterMapServiceRef = clusterMapServiceRef;
        this.masterFlag = false;
        this.pushManagerRegistry = pushManagerRegistry;
        this.context = context;
    }

    /**
     * Stops this rescheduler.
     */
    public void stop() {
        synchronized (this) {
            cancelTimerTasks();
            stopped = true;
        }
    }

    /**
     * Cancels the currently running timer tasks (if any).
     */
    private void cancelTimerTasks() {
        ScheduledTimerTask lockRefreshTask = this.lockRefreshTask;
        if (null != lockRefreshTask) {
            this.lockRefreshTask = null;
            lockRefreshTask.cancel();
        }

        ScheduledTimerTask lockCheckTask = this.lockCheckTask;
        if (null != lockCheckTask) {
            this.lockCheckTask = null;
            lockCheckTask.cancel();
        }
    }

    /**
     * Gets the cluster map.
     *
     * @return The cluster map or <code>null</code>
     */
    ClusterMap<InstanceIdAndStamp> optClusterMap() {
        try {
            return getMap();
        } catch (Exception e) {
            return null;
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------

    @Override
    public void handleEvent(Event event) {
        if (UpdaterEventConstants.TOPIC.equals(event.getTopic())) {
            try {
                planReschedule("Update tasks executed.", false);
            } catch (Exception e) {
                LOG.error("Failed to plan rescheduling", e);
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------

    private static final long LOCK_EXPIRE_MILLIS = 60000L;
    private static final long LOCK_CHECK_FREQUENCY_MILLIS = 20000L;
    private static final long LOCK_REFRESH_FREQUENCY_MILLIS = 20000L;

    /**
     * Plan to reschedule.
     *
     * @throws OXException If timer service is absent
     */
    public void planReschedule(String reason, boolean force) throws OXException {
        synchronized (this) {
            // Stopped
            if (stopped) {
                return;
            }

            if (!force && masterFlag) {
                // Already master
                return;
            }

            // Acquire required service
            TimerService timerService = Services.requireService(TimerService.class);

            // Cancel any timer tasks
            cancelTimerTasks();

            // Check if this node becomes master
            boolean master = false;
            {
                ClusterMap<InstanceIdAndStamp> clusterMap = optClusterMap();
                if (clusterMap == null) {
                    // No cluster map
                    LOG.warn("Missing cluster map service");
                    return;
                }
                InstanceIdAndStamp lockEntry = clusterMap.get("LOCK");
                if (lockEntry == null) {
                    // Try to acquire lock
                    if (clusterMap.putIfAbsent("LOCK", new InstanceIdAndStamp(instanceId, System.currentTimeMillis())) == null) {
                        master = true;
                    }
                } else {
                    InstanceIdAndStamp ias = lockEntry;

                    long now = System.currentTimeMillis();
                    boolean expired = now - ias.getStamp() > LOCK_EXPIRE_MILLIS; // not refreshed for a minute

                    if (instanceId.equals(ias.getInstanceId()) && !expired) {
                        // This node still holding the lock
                        master = true;
                    } else {
                        if (expired) {
                            // Try to acquire lock
                            if (clusterMap.replace("LOCK", lockEntry, new InstanceIdAndStamp(instanceId, now))) {
                                master = true;
                            }
                        }
                    }

                }

                // Remember flag
                masterFlag = master;
            }

            if (master) {
                // This is (new) master node
                String hostAddress;
                try {
                    hostAddress = InetAddress.getLocalHost().getHostAddress();
                } catch (Exception x) {
                    hostAddress = "127.0.0.1";
                }

                // Determine push users to start
                List<PushUser> allPushUsers;
                try {
                    allPushUsers = pushManagerRegistry.getUsersWithPermanentListeners();
                } catch (Exception e) {
                    LOG.warn("Failed to start permanent listeners on node {}", hostAddress, e);
                    return;
                }

                if (allPushUsers.isEmpty() == false) {
                    LOG.info("Going to apply all push users to node {}: ", hostAddress, reason);
                    List<PermanentListenerJob> startedOnes = pushManagerRegistry.applyInitialListeners(allPushUsers, true, 0L);
                    LOG.info("Node {} now runs permanent listeners for {} users", hostAddress, I(startedOnes.size()));
                }

                // Steadily refresh the lock as long as this node is master
                lockRefreshTask = timerService.scheduleWithFixedDelay(() -> {
                    try {
                        ClusterMap<InstanceIdAndStamp> clusterMap = optClusterMap();
                        if (clusterMap != null) {
                            clusterMap.put("LOCK", new InstanceIdAndStamp(instanceId, System.currentTimeMillis()));
                        } else {
                            LOG.error("Failed to update lock. Cluster map not available");
                        }
                    } catch (Exception e) {
                        LOG.error("Failed to update lock in cluster map", e);
                    }
                }, LOCK_REFRESH_FREQUENCY_MILLIS, LOCK_REFRESH_FREQUENCY_MILLIS); // Every 20 seconds
            } else {
                Runnable task = () -> {
                    try {
                        // Check for expired lock
                        ClusterMap<InstanceIdAndStamp> clusterMap = optClusterMap();
                        if (clusterMap != null) {
                            InstanceIdAndStamp lockEntry = clusterMap.get("LOCK");
                            if (lockEntry == null) {
                                cancelTimerTasks();
                                planReschedule("No lock entry in cluster map", true);
                                return;
                            }

                            InstanceIdAndStamp ias = lockEntry;
                            if (System.currentTimeMillis() - ias.getStamp() > LOCK_EXPIRE_MILLIS) { // not refreshed for a minute
                                cancelTimerTasks();
                                planReschedule("Expired lock entry in cluster map", true);
                            }
                        }
                    } catch (Exception e) {
                        LOG.error("Failed to check lock in cluster map", e);
                    }
                };
                lockCheckTask  = timerService.scheduleWithFixedDelay(task, LOCK_CHECK_FREQUENCY_MILLIS, LOCK_CHECK_FREQUENCY_MILLIS); // Every 20 seconds
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------

    @Override
    public ClusterMapService addingService(ServiceReference<ClusterMapService> reference) {
        ClusterMapService service = context.getService(reference);
        try {
            clusterMapServiceRef.set(service);
            planReschedule("Cluster map service appeared", false);
            return service;
        } catch (Exception e) {
            LOG.warn("Failed to distribute permanent listeners among cluster nodes", e);
        }
        context.ungetService(reference);
        return null;
    }

    @Override
    public void modifiedService(ServiceReference<ClusterMapService> reference, ClusterMapService service) {
        // Nothing
    }

    @Override
    public void removedService(ServiceReference<ClusterMapService> reference, ClusterMapService service) {
        clusterMapServiceRef.set(null);
        context.ungetService(reference);
    }

}
