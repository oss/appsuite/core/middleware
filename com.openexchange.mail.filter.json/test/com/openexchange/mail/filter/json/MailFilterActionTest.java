
package com.openexchange.mail.filter.json;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.jsieve.export.SieveHandlerFactory;
import com.openexchange.mailfilter.Credentials;
import com.openexchange.mailfilter.exceptions.MailFilterExceptionCode;
import com.openexchange.mailfilter.json.ajax.actions.MailFilterAction;
import com.openexchange.mailfilter.json.osgi.Services;
import com.openexchange.mailfilter.properties.MailFilterProperty;
import com.openexchange.mailfilter.properties.PasswordSource;

public class MailFilterActionTest extends MailFilterAction {

    public MailFilterActionTest() {
        super();
    }

    @BeforeEach
    public void setUpBeforeClass() {
        Common.prepare(null, null);
    }

    @Test
    public void testGetRightPasswordNothing() {
        LeanConfigurationService config = Services.getService(LeanConfigurationService.class);
        Credentials creds = mock(Credentials.class, inv -> { throw new UnsupportedOperationException(inv.toString()); });
        doReturn("pw2").when(creds).getPassword();
        doReturn(3).when(creds).getUserId();
        doReturn(123).when(creds).getContextId();

        try {
            SieveHandlerFactory.getRightPassword(config, creds);
            Assertions.fail("No exception thrown");
        } catch (OXException e) {
            Assertions.assertTrue(MailFilterExceptionCode.NO_VALID_PASSWORDSOURCE.equals(e));
        }
    }

    @Test
    public void testGetRightPasswordSession() throws OXException {
        Common.simMailFilterConfigurationService.delegateConfigurationService.stringProperties.put(MailFilterProperty.passwordSource.getFQPropertyName(), PasswordSource.SESSION.name);
        LeanConfigurationService config = Services.getService(LeanConfigurationService.class);
        String credsPW = "pw2";
        Credentials creds = mock(Credentials.class, inv -> { throw new UnsupportedOperationException(inv.toString()); });
        doReturn(credsPW).when(creds).getPassword();
        doReturn(3).when(creds).getUserId();
        doReturn(123).when(creds).getContextId();

        String rightPassword = SieveHandlerFactory.getRightPassword(config, creds);
        Assertions.assertEquals(credsPW, rightPassword, "Password should be equal to \"" + credsPW + "\"");
    }

    @Test
    public void testGetRightPasswordGlobalNoMasterPW() {
        Common.simMailFilterConfigurationService.delegateConfigurationService.stringProperties.put(MailFilterProperty.passwordSource.getFQPropertyName(), PasswordSource.GLOBAL.name);
        LeanConfigurationService config = Services.getService(LeanConfigurationService.class);
        String credsPW = "pw2";
        Credentials creds = mock(Credentials.class, inv -> { throw new UnsupportedOperationException(inv.toString()); });
        doReturn(credsPW).when(creds).getPassword();
        doReturn(3).when(creds).getUserId();
        doReturn(123).when(creds).getContextId();
        try {
            SieveHandlerFactory.getRightPassword(config, creds);
            Assertions.fail("No exception thrown");
        } catch (OXException e) {
            Assertions.assertTrue(MailFilterExceptionCode.NO_MASTERPASSWORD_SET.equals(e));
        }
    }

    @Test
    public void testGetRightPasswordGlobal() throws OXException {
        String masterPW = "masterPW";
        Common.simMailFilterConfigurationService.delegateConfigurationService.stringProperties.put(MailFilterProperty.passwordSource.getFQPropertyName(), PasswordSource.GLOBAL.name);
        Common.simMailFilterConfigurationService.delegateConfigurationService.stringProperties.put(MailFilterProperty.masterPassword.getFQPropertyName(), masterPW);
        LeanConfigurationService config = Services.getService(LeanConfigurationService.class);
        String credsPW = "pw2";
        Credentials creds = mock(Credentials.class, inv -> { throw new UnsupportedOperationException(inv.toString()); });
        doReturn(credsPW).when(creds).getPassword();
        doReturn(3).when(creds).getUserId();
        doReturn(123).when(creds).getContextId();
        String rightPassword = SieveHandlerFactory.getRightPassword(config, creds);
        Assertions.assertEquals(masterPW, rightPassword, "Password should be equal to \"" + masterPW + "\"");
    }

}
