/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cluster.map.redis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import com.openexchange.cluster.map.ClusterMap;
import com.openexchange.cluster.map.RemoteSiteOptions;
import com.openexchange.cluster.map.codec.MapCodec;
import com.openexchange.exception.OXException;
import com.openexchange.java.Streams;
import com.openexchange.redis.RedisConnector;
import com.openexchange.redis.RedisConnectorProvider;
import com.openexchange.threadpool.AbstractTask;
import com.openexchange.threadpool.ThreadPools;
import io.lettuce.core.SetArgs;

/**
 * {@link RemoteAwareRedisClusterMap} - The Redis-backed map.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 *
 * @param <V> The type for map values
 */
public class RemoteAwareRedisClusterMap<V> implements ClusterMap<V> {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(RemoteAwareRedisClusterMap.class);
    }

    private final RedisClusterMap<V> clusterMap;
    private final List<RedisConnectorProvider> remoteConnectorProviders;
    private final RemoteSiteOptions remoteSiteOptions;

    /**
     * Initializes a new {@link RemoteAwareRedisClusterMap}.
     *
     * @param clusterMap The backing cluster map
     * @param remoteSiteOptions The remote site options
     * @param remoteConnectorProviders The remote Redis connector providers to consider for modifying operations
     */
    public RemoteAwareRedisClusterMap(RedisClusterMap<V> clusterMap, RemoteSiteOptions remoteSiteOptions, List<RedisConnectorProvider> remoteConnectorProviders) {
        super();
        this.clusterMap = clusterMap;
        this.remoteSiteOptions = remoteSiteOptions;
        this.remoteConnectorProviders = remoteConnectorProviders;
    }

    private List<RedisConnector> getRemoteConnectors() {
        if (remoteConnectorProviders == null || remoteConnectorProviders.isEmpty()) {
            return Collections.emptyList();
        }

        List<RedisConnector> connectors = new ArrayList<>(remoteConnectorProviders.size());
        for (RedisConnectorProvider connectorProvider : remoteConnectorProviders) {
            connectors.add(connectorProvider.getConnector());
        }
        return connectors;
    }

    @Override
    public boolean containsKey(String key) throws OXException {
        return clusterMap.containsKey(key);
    }

    @Override
    public Set<String> keySet() throws OXException {
        return clusterMap.keySet();
    }

    @Override
    public V get(String key) throws OXException {
        return clusterMap.get(key);
    }

    @Override
    public V put(String key, V value, long expireTimeMillis) throws OXException {
        // Put into local
        V retval = clusterMap.put(key, value, expireTimeMillis);

        // Put into remote sites
        putIntoRemoteSites(key, value, expireTimeMillis, getRemoteConnectors());

        return retval;
    }

    @Override
    public V remove(String key) throws OXException {
        // Remove from local
        V retval = clusterMap.remove(key);

        // Remove from remote sites
        removeFromRemoteSites(key, getRemoteConnectors());

        return retval;
    }

    @Override
    public boolean replace(String key, V oldValue, V newValue, long expireTimeMillis) throws OXException {
        // Replace at local site
        boolean retval = clusterMap.replace(key, oldValue, newValue, expireTimeMillis);

        // Put into remote sites (if replaced)
        if (retval) {
            putIntoRemoteSites(key, newValue, expireTimeMillis, getRemoteConnectors());
        }

        return retval;
    }

    @Override
    public V putIfAbsent(String key, V value, long expireTimeMillis) throws OXException {
        // Put into local site (if absent)
        V retval = clusterMap.putIfAbsent(key, value, expireTimeMillis);

        // Put into remote sites (if it was absent)
        if (retval == null) {
            putIntoRemoteSites(key, value, expireTimeMillis, getRemoteConnectors());
        }

        return retval;
    }

    private void putIntoRemoteSites(String key, V value, long expireTimeMillis, List<RedisConnector> remoteConnectors) {
        if (!remoteConnectors.isEmpty()) {
            if (remoteSiteOptions.isAsyncRemoteSiteCalling()) {
                ThreadPools.submitElseExecute(new PutIntoRemoteSitesTask<V>(key, value, expireTimeMillis, remoteConnectors, clusterMap));
            } else {
                try {
                    ThreadPools.execute(new PutIntoRemoteSitesTask<V>(key, value, expireTimeMillis, remoteConnectors, clusterMap));
                } catch (Exception e) {
                    // Cannot occur
                    LoggerHolder.LOG.error("Failed remote call for {}", clusterMap.getFullKey(key), e);
                }
            }
        }
    }

    private void removeFromRemoteSites(String key, List<RedisConnector> remoteConnectors) {
        if (!remoteConnectors.isEmpty()) {
            if (remoteSiteOptions.isAsyncRemoteSiteCalling()) {
                ThreadPools.submitElseExecute(new RemoveFromRemoteSitesTask<V>(key, remoteConnectors, clusterMap));
            } else {
                try {
                    ThreadPools.execute(new RemoveFromRemoteSitesTask<V>(key, remoteConnectors, clusterMap));
                } catch (Exception e) {
                    // Cannot occur
                    LoggerHolder.LOG.error("Failed remote call for {}", clusterMap.getFullKey(key), e);
                }
            }
        }
    }

    // --------------------------------------------------------- Helper classes ------------------------------------------------------------

    /**
     * The task for removing a key from remote sites.
     */
    private static class RemoveFromRemoteSitesTask<V> extends AbstractTask<Void> {

        private final String key;
        private final List<RedisConnector> remoteConnectors;
        private final RedisClusterMap<V> clusterMap;

        /**
         * Initializes a new {@link RemoveFromRemoteSitesTask}.
         *
         * @param key The key
         * @param remoteConnectors The remote connectors to connect to
         * @param clusterMap The parental map
         */
        RemoveFromRemoteSitesTask(String key, List<RedisConnector> remoteConnectors, RedisClusterMap<V> clusterMap) {
            super();
            this.remoteConnectors = remoteConnectors;
            this.key = key;
            this.clusterMap = clusterMap;
        }

        @Override
        public Void call() throws Exception {
            String fullKey = clusterMap.getFullKey(key);
            for (RedisConnector connector : remoteConnectors) {
                try {
                    connector.executeVoidOperation(commandsProvider -> {
                        commandsProvider.getKeyCommands().del(fullKey);
                    });
                } catch (Exception e) {
                    LoggerHolder.LOG.error("Failed to remove key {} from site {}", fullKey, connector, e);
                }
            }
            return null;
        }
    }

    /**
     * The task for putting a key-value pair to remote sites.
     */
    private static class PutIntoRemoteSitesTask<V> extends AbstractTask<Void> {

        private final String key;
        private final V value;
        private final long expireTimeMillis;
        private final List<RedisConnector> remoteConnectors;
        private final RedisClusterMap<V> clusterMap;

        /**
         * Initializes a new {@link PutIntoRemoteSitesTask}.
         *
         * @param key The key
         * @param value The value
         * @param expireTimeMillis The expiration time in milliseconds
         * @param remoteConnectors The remote connectors to connect to
         * @param clusterMap The parental map
         */
        PutIntoRemoteSitesTask(String key, V value, long expireTimeMillis, List<RedisConnector> remoteConnectors, RedisClusterMap<V> clusterMap) {
            super();
            this.value = value;
            this.remoteConnectors = remoteConnectors;
            this.expireTimeMillis = expireTimeMillis;
            this.key = key;
            this.clusterMap = clusterMap;
        }

        @Override
        public Void call() throws Exception {
            byte[] valueBytes;
            try {
                MapCodec<V> codec = clusterMap.getCodec();
                valueBytes = Streams.stream2bytes(codec.serializeValue(value));
            } catch (Exception e) {
                LoggerHolder.LOG.error("Failed to serialize value for key {} for publishing to remote sites", clusterMap.getFullKey(key), e);
                return null;
            }

            String fullKey = clusterMap.getFullKey(key);
            for (RedisConnector connector : remoteConnectors) {
                try {
                    connector.executeVoidOperation(commandsProvider -> {
                        try {
                            if (expireTimeMillis > 0) {
                                commandsProvider.getRawStringCommands().set(fullKey, Streams.newByteArrayInputStream(valueBytes), new SetArgs().px(expireTimeMillis));
                            } else {
                                commandsProvider.getRawStringCommands().set(fullKey, Streams.newByteArrayInputStream(valueBytes));
                            }
                        } catch (RuntimeException e) {
                            throw RedisClusterMap.assumeRedisError(e);
                        } catch (Exception e) {
                            throw RedisClusterMap.assumeSerializationError(e);
                        }
                    });
                } catch (Exception e) {
                    LoggerHolder.LOG.error("Failed to put key {} to site {}", fullKey, connector, e);
                }
            }
            return null;
        }
    }

}
