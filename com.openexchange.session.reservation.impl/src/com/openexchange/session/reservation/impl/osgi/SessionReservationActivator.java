/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.session.reservation.impl.osgi;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.config.ConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.session.reservation.SessionReservationService;
import com.openexchange.session.reservation.impl.Services;
import com.openexchange.session.reservation.impl.SessionReservationServiceImpl;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.user.UserService;


/**
 * {@link SessionReservationActivator}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.0
 */
public class SessionReservationActivator extends HousekeepingActivator {

    static final Logger LOG = org.slf4j.LoggerFactory.getLogger(SessionReservationActivator.class);

    /**
     * Initializes a new {@link SessionReservationActivator}.
     */
    public SessionReservationActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { SessiondService.class, ContextService.class, UserService.class, ConfigurationService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        Services.setServiceLookup(this);
        final BundleContext context = this.context;

        // Create service instance
        final SessionReservationServiceImpl serviceImpl = new SessionReservationServiceImpl();

        // Check ClusterMap stuff
        final ServiceTrackerCustomizer<ClusterMapService, ClusterMapService> customizer = new ServiceTrackerCustomizer<ClusterMapService, ClusterMapService>() {

            @Override
            public void removedService(final ServiceReference<ClusterMapService> reference, final ClusterMapService service) {
                removeService(ClusterMapService.class);
                serviceImpl.changeBackingMapToLocalMap();
                context.ungetService(reference);
            }

            @Override
            public void modifiedService(final ServiceReference<ClusterMapService> reference, final ClusterMapService service) {
                // Ignore
            }

            @Override
            public ClusterMapService addingService(final ServiceReference<ClusterMapService> reference) {
                final ClusterMapService clusterMapService = context.getService(reference);
                try {
                    addService(ClusterMapService.class, clusterMapService);
                    serviceImpl.changeBackingMapToClusterMap();
                    return clusterMapService;
                } catch (OXException e) {
                    LOG.warn("Couldn't initialize distributed reservation map.", e);
                } catch (RuntimeException e) {
                    LOG.warn("Couldn't initialize distributed reservation map.", e);
                }
                context.ungetService(reference);
                return null;
            }
        };
        track(ClusterMapService.class, customizer);

        // Open trackers
        openTrackers();

        // Register service instance
        registerService(SessionReservationService.class, serviceImpl);
    }

    @Override
    protected void stopBundle() throws Exception {
        super.stopBundle();
        Services.setServiceLookup(null);
    }

    @Override
    public <S> boolean removeService(java.lang.Class<? extends S> clazz) {
        return super.removeService(clazz);
    }

    @Override
    public <S> boolean addService(java.lang.Class<S> clazz, S service) {
        return super.addService(clazz, service);
    }

}
