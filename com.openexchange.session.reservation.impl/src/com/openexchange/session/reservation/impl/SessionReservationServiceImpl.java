/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.session.reservation.impl;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;
import org.json.SimpleJSONSerializer;
import org.slf4j.Logger;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.annotation.NonNull;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.ClusterMap;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.cluster.map.RemoteSiteOptions;
import com.openexchange.cluster.map.codec.AbstractJSONMapCodec;
import com.openexchange.cluster.map.codec.MapCodec;
import com.openexchange.concurrent.Blocker;
import com.openexchange.concurrent.ConcurrentBlocker;
import com.openexchange.exception.OXException;
import com.openexchange.java.Streams;
import com.openexchange.java.util.UUIDs;
import com.openexchange.session.reservation.Reservation;
import com.openexchange.session.reservation.SessionReservationService;

/**
 * {@link SessionReservationServiceImpl}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.6.1
 */
public class SessionReservationServiceImpl extends BasicCoreClusterMapProvider<Reservation> implements SessionReservationService {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(SessionReservationServiceImpl.class);

    private static MapCodec<Reservation> createReservationCodec() {
        return new AbstractJSONMapCodec<Reservation>() {

            @Override
            public InputStream serializeValue(Reservation value) throws Exception {
                ByteArrayOutputStream baos = Streams.newByteArrayOutputStream();
                OutputStreamWriter out = new OutputStreamWriter(baos, StandardCharsets.UTF_8);

                out.append('{');

                SimpleJSONSerializer.writeJsonValue("token", out);
                out.append(':');
                SimpleJSONSerializer.writeJsonValue(value.getToken(), out);

                out.append(',');
                SimpleJSONSerializer.writeJsonValue("timeoutMillis", out);
                out.append(':');
                SimpleJSONSerializer.writeJsonValue(value.getTimeoutMillis(), out);

                out.append(',');
                SimpleJSONSerializer.writeJsonValue("creationStamp", out);
                out.append(':');
                SimpleJSONSerializer.writeJsonValue(value.getCreationStamp(), out);

                out.append(',');
                SimpleJSONSerializer.writeJsonValue("contextId", out);
                out.append(':');
                SimpleJSONSerializer.writeJsonValue(value.getContextId(), out);

                out.append(',');
                SimpleJSONSerializer.writeJsonValue("userId", out);
                out.append(':');
                SimpleJSONSerializer.writeJsonValue(value.getUserId(), out);

                Map<String, String> state = value.getState();
                if (state != null) {
                    out.append(',');
                    SimpleJSONSerializer.writeJsonValue("state", out);
                    out.append(':');
                    SimpleJSONSerializer.writeJsonObject(state, out);
                }

                out.append('}');

                out.flush();
                out.close();
                return Streams.asInputStream(baos);
            }

            @Override
            protected @NonNull JSONObject writeJson(Reservation value) throws Exception {
                JSONObject j = new JSONObject(6);
                j.putOpt("token", value.getToken());
                j.put("timeoutMillis", value.getTimeoutMillis());
                j.put("creationStamp", value.getCreationStamp());
                j.put("contextId", value.getContextId());
                j.put("userId", value.getUserId());
                Map<String, String> state = value.getState();
                if (state != null) {
                    JSONObject jState = new JSONObject(state.size());
                    for (Map.Entry<String, String> e : state.entrySet()) {
                        jState.put(e.getKey(), e.getValue());
                    }
                    j.put("state", jState);
                }
                return j;
            }

            @Override
            protected @NonNull Reservation parseJson(JSONObject j) throws Exception {
                ReservationImpl reservation = new ReservationImpl();
                reservation.setContextId(j.optInt("contextId"));
                reservation.setUserId(j.optInt("userId"));
                reservation.setCreationStamp(j.optLong("creationStamp"));
                reservation.setTimeoutMillis(j.optLong("timeoutMillis"));
                reservation.setToken(j.optString("token"));
                JSONObject jState = j.optJSONObject("state");
                if (jState != null) {
                    Map<String, String> state = LinkedHashMap.newLinkedHashMap(jState.length());
                    for (Map.Entry<String, Object> e : jState.entrySet()) {
                        state.put(e.getKey(), e.getValue().toString());
                    }
                    reservation.setState(state);
                }
                return reservation;
            }
        };
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    private final Blocker blocker = new ConcurrentBlocker();
    private final Cache<String, Reservation> reservations;
    private volatile boolean useClusterMap = false;

    /**
     * Initializes a new {@link SessionReservationServiceImpl}.
     */
    public SessionReservationServiceImpl() {
        super(CoreMap.SESSION_RESERVATIONS, createReservationCodec(), Duration.ofMinutes(5).toMillis(), RemoteSiteOptions.SYNC, () -> Services.getService(ClusterMapService.class));
        CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder()
            .concurrencyLevel(1)
            .maximumSize(Integer.MAX_VALUE)
            .initialCapacity(1024)
            .expireAfterAccess(5, TimeUnit.MINUTES);
        reservations = cacheBuilder.build();
    }

    private void putReservation(Reservation reservation) throws OXException {
        blocker.acquire();
        try {
            if (useClusterMap) {
                putToClusterMap(reservation);
            } else {
                reservations.put(reservation.getToken(), reservation);
            }
        } finally {
            blocker.release();
        }
    }

    private void putToClusterMap(Reservation reservation) throws OXException {
        ClusterMap<Reservation> clusterMap = getMap();
        clusterMap.put(reservation.getToken(), reservation);
    }

    private Reservation pollReservation(String token) throws OXException {
        blocker.acquire();
        try {
            return useClusterMap ? pollFromClusterMap(token) : reservations.asMap().remove(token);
        } finally {
            blocker.release();
        }
    }

    /**
     * Gets the reservation for the given token without removing it
     *
     * @param token The reservation token
     * @return The {@link Reservation} or <code>null</code>
     * @throws OXException If operation fails
     */
    private Reservation peekReservation(String token) throws OXException {
        blocker.acquire();
        try {
            return useClusterMap ? peekFromClusterMap(token) : reservations.getIfPresent(token);
        } finally {
            blocker.release();
        }
    }

    private Reservation pollFromClusterMap(String token) throws OXException {
        ClusterMap<Reservation> clusterMap = getMap();
        return clusterMap.remove(token);
    }

    /**
     * Gets the reservation from the hazelcast map without removing it
     *
     * @param token The reservation token
     * @return The {@link Reservation} or <code>null</code>
     * @throws OXException If cluster map cannot be obtained
     */
    private Reservation peekFromClusterMap(String token) throws OXException {
        ClusterMap<Reservation> clusterMap = getMap();
        return clusterMap.get(token);
    }

    /**
     * Checks if the reservation is elapsed
     *
     * @param reservation The reservation to check
     * @return <code>true</code> if the reservation is elapsed, <code>false</code> otherwise
     */
    private boolean isElapsed(Reservation reservation) {
        return (System.currentTimeMillis() - reservation.getCreationStamp()) > reservation.getTimeoutMillis();
    }

    // ---------------------------------------------------------------------------------------------------

    @Override
    public String reserveSessionFor(int userId, int contextId, long timeout, TimeUnit unit, Map<String, String> optState) throws OXException {
        ReservationImpl reservation = new ReservationImpl();
        reservation.setContextId(contextId);
        reservation.setUserId(userId);
        reservation.setTimeoutMillis(unit.toMillis(timeout));
        reservation.setCreationStamp(System.currentTimeMillis());
        reservation.setToken(UUIDs.getUnformattedString(UUID.randomUUID()) + "-" + UUIDs.getUnformattedString(UUID.randomUUID()));
        reservation.setState(optState);

        putReservation(reservation);

        return reservation.getToken();
    }

    @Override
    public Reservation removeReservation(String token) throws OXException {
        Reservation reservation = pollReservation(token);
        if (null == reservation) {
            return null;
        }
        if (isElapsed(reservation)) {
            return null;
        }

        return reservation;
    }

    @Override
    public Reservation getReservation(String token) throws OXException {
        Reservation reservation = peekReservation(token);
        if (null == reservation) {
            return null;
        }
        if (isElapsed(reservation)) {
            pollReservation(token);
            return null;
        }

        return reservation;
    }

    // ---------------------------------------------------------------------------------------------------

   /**
    * Changes the backing from cluster map to in-memory map.
    */
    public void changeBackingMapToLocalMap() {
        blocker.block();
        try {
            // This happens if Hazelcast is removed in the meantime. We cannot copy any information back to the local map.
            useClusterMap = false;
            LOG.info("Reservations backing map changed to local");
        } finally {
            blocker.unblock();
        }
    }

   /**
    * Changes the backing from in-memory map to cluster map.
    *
    * @throws OXException
    */
    public void changeBackingMapToClusterMap() throws OXException {
        blocker.block();
        try {
            if (useClusterMap) {
                return;
            }

            ClusterMap<Reservation> clusterMap = getMap();
            {
                // This MUST be synchronous!
                for (Map.Entry<String, Reservation> entry : reservations.asMap().entrySet()) {
                    clusterMap.put(entry.getKey(), entry.getValue());
                }
                reservations.invalidateAll();
            }
            useClusterMap = true;
            LOG.info("Reservations backing map changed to redis cluster map");
        } finally {
            blocker.unblock();
        }
    }

}
