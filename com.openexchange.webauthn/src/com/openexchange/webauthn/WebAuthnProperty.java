/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.webauthn;

import static com.openexchange.java.Autoboxing.I;
import com.openexchange.config.lean.Property;

/**
 * {@link WebAuthnProperty} Properties for webauthn
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public enum WebAuthnProperty implements Property {

    /**
     * Allow untrusted attestation certificates. If set to false, all
     * assertions must have valid chain. Must be sure all types of webauthn
     * keys are trusted
     */
    ALLOW_UNTRUSTED_ATTESTATION("allowUntrustedAttesation", Boolean.TRUE),

    /**
     * Allow subdomains to work with the relying party ID
     */
    ALLOW_SUBDOMAIN("allowSubdomain", Boolean.TRUE),

    /**
     * Attestation of the webauthn key certificate. Acceptable
     * 'direct', 'indirect', 'enterprise', 'none'
     */
    ATTESTATION_PREFERENCE("attestationPreference", "direct"),

    /**
     * Time allowed for authenticator response. When an authentication
     * challenge is sent, the authenticator/user must respond within this
     * time frame. Time in minutes
     */
    AUTENTICATOR_ACTION_TIME_MINUTES("authenticatorActionTimeMinutes", I(5)),

    /**
     * If webauthn service is enabled
     */
    ENABLED("enabled", Boolean.FALSE),

    /**
     * Replying Party. If defined, will override multifactor.webauth rpId.
     * domain only
     */
    RELYING_PARTY("relyingParty", ""),

    /**
     * User verification requirement. Acceptable values
     * 'preferred', 'required', 'discouraged'
     */
    USER_VERIFICATION_REQUIRED("userVerificationRequired", "required"),

    /**
     * Validate the signature counter. Helps prevent replay attacks
     */
    VALIDATE_SIGNATURE_COUNTER("validateSignatureCounter", Boolean.TRUE),
    ;

    public static final String PREFIX = "com.openexchange.webauthn.";
    private final Object defaultValue;
    private final String fqdn;

    WebAuthnProperty(String appendix, Object defaultValue) {
        this.defaultValue = defaultValue;
        this.fqdn = PREFIX + appendix;
    }

    @Override
    public String getFQPropertyName() {
        return fqdn;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
