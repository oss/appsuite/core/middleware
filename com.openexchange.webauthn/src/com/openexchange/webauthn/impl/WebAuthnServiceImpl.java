/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.webauthn.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import org.slf4j.Logger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.multifactor.Challenge;
import com.openexchange.multifactor.ChallengeAnswer;
import com.openexchange.multifactor.RegistrationChallenge;
import com.openexchange.multifactor.exceptions.MultifactorExceptionCodes;
import com.openexchange.server.ServiceLookup;
import com.openexchange.webauthn.WebAuthnDevice;
import com.openexchange.webauthn.WebAuthnProperty;
import com.openexchange.webauthn.WebAuthnService;
import com.openexchange.webauthn.cache.WebAuthnCache;
import com.openexchange.webauthn.cache.WebAuthnMemoryCache;
import com.openexchange.webauthn.cache.WebAuthnRegistrationItem;
import com.openexchange.webauthn.storage.WebAuthnStorageService;
import com.yubico.webauthn.AssertionRequest;
import com.yubico.webauthn.AssertionResult;
import com.yubico.webauthn.CredentialRepository;
import com.yubico.webauthn.FinishAssertionOptions;
import com.yubico.webauthn.FinishRegistrationOptions;
import com.yubico.webauthn.RegistrationResult;
import com.yubico.webauthn.RelyingParty;
import com.yubico.webauthn.StartAssertionOptions;
import com.yubico.webauthn.StartRegistrationOptions;
import com.yubico.webauthn.data.AttestationConveyancePreference;
import com.yubico.webauthn.data.AuthenticatorAssertionResponse;
import com.yubico.webauthn.data.AuthenticatorAttestationResponse;
import com.yubico.webauthn.data.AuthenticatorSelectionCriteria;
import com.yubico.webauthn.data.ByteArray;
import com.yubico.webauthn.data.ClientAssertionExtensionOutputs;
import com.yubico.webauthn.data.ClientRegistrationExtensionOutputs;
import com.yubico.webauthn.data.PublicKeyCredential;
import com.yubico.webauthn.data.PublicKeyCredentialCreationOptions;
import com.yubico.webauthn.data.RelyingPartyIdentity;
import com.yubico.webauthn.data.ResidentKeyRequirement;
import com.yubico.webauthn.data.UserIdentity;
import com.yubico.webauthn.data.UserVerificationRequirement;
import com.yubico.webauthn.data.exception.Base64UrlException;
import com.yubico.webauthn.exception.AssertionFailedException;
import com.yubico.webauthn.exception.RegistrationFailedException;

/**
 *
 * {@link WebAuthnServiceImpl} implements WebAuthn authentication.
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public class WebAuthnServiceImpl implements WebAuthnService {

    private static final String PARAMETER_RESPONSE = "response";
    private static final String PARAMETER_CLIENT_DATA_JSON = "clientDataJSON";
    private static final String PARAMETER_CREDENTIALS_GET_JSON_PARAMETER = "credentialsGetJson";
    private static final String PARAMETER_CHALLENGE = "challenge";

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(WebAuthnServiceImpl.class);
    }
    ServiceLookup services;  //  Service lookup
    private final Random random;   //  Random generator
    private WebAuthnCache cache;   // Cache to be used to hold the challenge/registration before authenticated by device

    /**
     * Initializes a new {@link WebAuthnServiceImpl}.
     *
     * @param services ServiceLookup
     */
    public WebAuthnServiceImpl(ServiceLookup services) {
        this.services = services;
        this.random = new SecureRandom();
        this.cache = new WebAuthnMemoryCache();
    }

    @Override
    public void setCache(WebAuthnCache cache) {
        this.cache = cache;
    }

    /**
     * Internal method to obtain the {@link LeanConfigurationService}
     *
     * @return The configuration service
     * @throws OXException
     */
    private LeanConfigurationService getConfigurationService() throws OXException {
        return services.getServiceSafe(LeanConfigurationService.class);
    }

    /**
     * Returns the registered WebAuthnStorage Service
     *
     * @return the webauthn storage service
     * @throws OXException
     */
    private WebAuthnStorageService getWebauthnStorage() throws OXException {
        return services.getServiceSafe(WebAuthnStorageService.class);
    }

    /**
     *
     * Returns configured time limit for cache
     *
     * @param contextId ContextId of the user
     * @param userId Id of the user
     * @return Duration for cache
     * @throws OXException
     */
    private Duration getTimeLimit(int contextId, int userId) throws OXException {
        return Duration.ofMinutes(getConfigurationService().getIntProperty(userId, contextId, WebAuthnProperty.AUTENTICATOR_ACTION_TIME_MINUTES));
    }

    /**
     * Gets the WebAuthn Identity
     *
     * @param multifactorRequest The request to create the identity for
     * @return The WebAuthn identity set in the configuration, or the host name of the request as fallback
     */
    private RelyingPartyIdentity getRelyingPartyIdentity(String id) {
        return RelyingPartyIdentity.builder().id(id).name(id).build();
    }

    /**
     * Return configured AttestationConveyancePreference
     * Determines if the device should return the certificate attestation chain.
     * 
     * @param contextId Context of the user
     * @param userId Id of the user
     * @param config Leanconfiguration service
     * @return Configured AttestationConveyancePreference
     */
    private AttestationConveyancePreference getAttPref(int contextId, int userId, LeanConfigurationService config) {
        AttestationConveyancePreference att = AttestationConveyancePreference.DIRECT;
        switch (config.getProperty(userId, contextId, WebAuthnProperty.ATTESTATION_PREFERENCE)) {
            case "direct":
                att = AttestationConveyancePreference.DIRECT;
                break;
            case "indirect":
                att = AttestationConveyancePreference.INDIRECT;
                break;
            case "enterprise":
                att = AttestationConveyancePreference.ENTERPRISE;
                break;
            case "none":
                att = AttestationConveyancePreference.NONE;
        }
        return att;
    }

    /**
     * Creates the {@link RelyingParty} instance to use
     *
     * @param cid ContextId of the user
     * @param userId Id of the user
     * @param rpId The configured relay party ID
     * @param credentialRepository The credential repository to use
     * @return The {@link RelyingParty} instance for the given request
     * @throws OXException
     */
    private RelyingParty createRelyingParty(int cid, int userId, String rpId, CredentialRepository credentialRepository) throws OXException {
        RelyingParty relyingParty;
        LeanConfigurationService config = getConfigurationService();
        try {
            //
            String waRpId = config.getProperty(userId, cid, WebAuthnProperty.RELYING_PARTY);
            // If configured, override host/multifactor property
            if (waRpId != null && !waRpId.isEmpty()) {
                rpId = waRpId;
            }

            String origin = new URI("https", rpId, "", null).toURL().toString();
            relyingParty = RelyingParty.builder()
                .identity(getRelyingPartyIdentity(rpId))
                .credentialRepository(credentialRepository)
                .origins(Collections.singleton(origin))
                .attestationConveyancePreference(getAttPref(cid, userId, config))
                .allowOriginSubdomain(config.getBooleanProperty(userId, cid, WebAuthnProperty.ALLOW_SUBDOMAIN))
                .allowUntrustedAttestation(config.getBooleanProperty(userId, cid, WebAuthnProperty.ALLOW_UNTRUSTED_ATTESTATION))
                .validateSignatureCounter(config.getBooleanProperty(userId, cid, WebAuthnProperty.VALIDATE_SIGNATURE_COUNTER))
                .build();
            return relyingParty;
        } catch (MalformedURLException | URISyntaxException e) {
            throw OXException.general("Error creating relyingParty " + rpId, e);
        }
    }

    /**
     * Create a UserIdentity from the users login
     *
     * @param login The login string
     * @param id The ByteArray id of the identity. If null will create new based on the login string
     * @return A UserIdentity from the login string
     */
    private UserIdentity buildIdentity(String login, ByteArray id) {
        return UserIdentity.builder()
            .name(login)
            .displayName(login)
            .id(id == null ? new ByteArray(createHandleFromLogin(login)) : id)
            .build();
    }

    /**
     * Find existing user with the context Id, user Id and username
     *
     * @param cid The users contextId
     * @param userId The users Id
     * @param username Login username
     * @return UserIdentity if found, otherwise empty
     * @throws OXException
     */
    Optional<UserIdentity> findExistingUser(int cid, int userId, String username) throws OXException {
        try {
            Optional<WebAuthnDevice> dev = getWebauthnStorage().getDevices(cid, userId).stream().filter((d) -> d.getLogin().equals(username)).findFirst();
            if (dev.isPresent()) {
                WebAuthnDevice device = dev.get();
                return Optional.ofNullable(buildIdentity(device.getLogin(), device.getUserId()));
            }
        } catch (OXException | Base64UrlException e) {
            LoggerHolder.LOG.error("Error finding existing users", e);
            throw MultifactorExceptionCodes.ERROR_CREATING_FACTOR.create();
        }
        return Optional.empty();
    }

    /**
     * Internal method to obtain the configured user verification requirement
     *
     * @param contextId The contextId of the user
     * @param userId The users Id
     * @return configured UserVerificationRequirement
     * @throws OXException
     */
    private UserVerificationRequirement getUserVerificationRequirement(int contextId, int userId) throws OXException {
        switch (getConfigurationService().getProperty(userId, contextId, WebAuthnProperty.USER_VERIFICATION_REQUIRED)) {
            case "preferred":
                return UserVerificationRequirement.PREFERRED;
            case "required":
                return UserVerificationRequirement.REQUIRED;
            case "discouraged":
                return UserVerificationRequirement.DISCOURAGED;
        }
        return UserVerificationRequirement.PREFERRED;
    }

    /**
     * Creates a byte array of length 64 containing the users login followed by random
     * characters. Login/random separated by !!
     * Login will be required for implementation of passkeys
     *
     * @param login
     * @return Byte array for userHandle
     */
    private byte[] createHandleFromLogin(String login) {
        byte[] loginData = (login + "!!").getBytes();
        byte[] userHandleFill = new byte[64 - loginData.length];
        random.nextBytes(userHandleFill);
        byte[] userHandle = new byte[64];
        ByteBuffer buff = ByteBuffer.wrap(userHandle);
        buff.put(loginData);
        buff.put(userHandleFill);
        return buff.array();
    }

    @Override
    public RegistrationChallenge startRegistration(int contextId, int userId, String login, String deviceName, String rpId, boolean secondFactor) throws OXException {
        CredentialRepository repo = new WebAuthnCredentialRepository(contextId, userId, getWebauthnStorage());
        RelyingParty rp = createRelyingParty(contextId, userId, rpId, repo);
        PublicKeyCredentialCreationOptions request = rp.startRegistration(
            StartRegistrationOptions.builder()
                .user(
                    findExistingUser(contextId, userId, login)
                        .orElseGet(() -> {
                            return buildIdentity(login, null);
                        }))
                .authenticatorSelection(AuthenticatorSelectionCriteria.builder()
                    .userVerification(getUserVerificationRequirement(contextId, userId))
                    // If second factor, we don't need resident key.  If used as primary, then we'll require it.
                    .residentKey(secondFactor ? ResidentKeyRequirement.DISCOURAGED : ResidentKeyRequirement.REQUIRED)
                    .build())
                .build());

        try {
            JSONObject credentialCreateJson = (JSONObject) JSONObject.parse(request.toJson());
            String id = UUID.randomUUID().toString();
            cache.storePublicKeyCredOptions(id, new WebAuthnRegistrationItem(request, deviceName), getTimeLimit(contextId, userId));
            return new WebAuthRegistrationChallenge(id, credentialCreateJson);
        } catch (JsonProcessingException | JSONException e) {
            throw OXException.general("Failed to generate registation json: ", e);
        }
    }

    /**
     * Internal method to check extension results string for rk parameter
     * Determines if device may be used for passkey login
     *
     * @param extResultsString
     * @return True if has residentKey
     */
    private boolean hasResidentKey(Object extResultsString) {
        if (extResultsString == null) {
            return false;
        }
        try {
            JSONObject extResults = (JSONObject) JSONObject.parse(extResultsString.toString().replaceAll("=", ":"));
            if (extResults.has("credProps")) {
                JSONObject credProp = extResults.getJSONObject("credProps");
                if (credProp.has("rk")) {
                    return credProp.getBoolean("rk");
                }
            }
        } catch (JSONException e) {

        }
        return false;
    }

    /**
     * Create PulicKeyCredential from Challenge answer
     *
     * @param answer The ChallengeAnswer
     * @return PublicKeyCredential
     * @throws OXException
     */
    private PublicKeyCredential<AuthenticatorAttestationResponse, ClientRegistrationExtensionOutputs> getPkc(ChallengeAnswer answer) throws OXException {
        try {
            JSONObject resp = new JSONObject();
            resp.put("id", answer.requireField(WebAuthnAnswerField.ID).toString());
            JSONObject response = new JSONObject();
            response.put("attestationObject", answer.requireField(WebAuthnAnswerField.ATTESTATION_OBJECT));
            response.put("clientDataJSON", answer.requireField(WebAuthnAnswerField.CLIENT_DATA_JSON).toString());
            resp.put("response", response);
            resp.put("clientExtensionResults", new JSONObject());
            resp.put("type", "public-key");
            return PublicKeyCredential.parseRegistrationResponseJson(resp.toString());
        } catch (JSONException | IOException e) {
            LoggerHolder.LOG.error("Error creating registration", e);
            throw MultifactorExceptionCodes.ERROR_CREATING_FACTOR.create(e.getMessage());
        }
    }

    /**
     * Creates an new WebAuthn device from the registration data
     *
     * @param contextId Users contextId
     * @param userId Users Id
     * @param deviceId The device Id
     * @param secondFactor If the device is used as a second factor
     * @param hasResidentKey If the device has a resident Key
     * @param registration The registration data returned
     * @param item The WebAuthRegistrationitem
     * @param pkc The PublicKeyCredential
     * @return new new WebAuthnDevice
     * @throws OXException
     */
    private WebAuthnDevice createDevice(int contextId, int userId, String deviceId, boolean secondFactor, boolean hasResidentKey, RegistrationResult registration, Optional<WebAuthnRegistrationItem> item, PublicKeyCredential<AuthenticatorAttestationResponse, ClientRegistrationExtensionOutputs> pkc) throws OXException {
        PublicKeyCredentialCreationOptions pkco = item.get().getPkc();
        WebAuthnDevice dev = new WebAuthnDevice.Builder()
            .withPublicKey(Base64.getEncoder().encodeToString(registration.getPublicKeyCose().getBytes()))
            .withAttestationCertificate(pkc.getResponse().getAttestationObject().getBase64())
            .withClientDataJson(pkc.getResponse().getClientDataJSON().getBase64())
            .isMultifactor(secondFactor)
            .isDiscoverable(hasResidentKey)
            .withId(userId)
            .withContextId(contextId)
            .isEnabled(true)
            .withDeviceId(deviceId)
            .withUserId(pkco.getUser().getId())
            .withKeyId(Base64.getEncoder().encodeToString(registration.getKeyId().getId().getBytes()))
            .withName(item.get().getName())
            .withLogin(pkco.getUser().getName())
            .withCounter(registration.getSignatureCount())
            .build();
        getWebauthnStorage().registerDevice(contextId, userId, dev);
        return dev;
    }

    @Override
    public WebAuthnDevice finishRegistration(int contextId, int userId, String deviceId, ChallengeAnswer answer, boolean secondFactor) throws OXException {
        Optional<WebAuthnRegistrationItem> opt = cache.getPublicKeyCredOptions(deviceId);
        if (opt.isEmpty()) {
            throw MultifactorExceptionCodes.REGISTRATION_FAILED.create();
        }
        // Check passkey requirement
        boolean hasResidentKey = hasResidentKey(answer.getOptionalField(WebAuthnAnswerField.CLIENT_EXTENSION_RESULTS));
        if (!hasResidentKey && !secondFactor) {
            throw MultifactorExceptionCodes.REGISTRATION_FAILED.create("Requires resident key");
        }
        PublicKeyCredential<AuthenticatorAttestationResponse, ClientRegistrationExtensionOutputs> pkc = getPkc(answer);
        PublicKeyCredentialCreationOptions pkco = opt.get().getPkc();
        RelyingParty rp = createRelyingParty(contextId, userId, pkco.getRp().getId(), new WebAuthnCredentialRepository(contextId, userId, getWebauthnStorage()));
        try {
            RegistrationResult registration = rp.finishRegistration(
                FinishRegistrationOptions.builder()
                    .request(pkco)
                    .response(pkc)
                    .build());

            // Create new webauthn device
            return createDevice(contextId, userId, deviceId, secondFactor, hasResidentKey, registration, opt, pkc);
        } catch (RegistrationFailedException e) {
            LoggerHolder.LOG.error("Error creating registration", e);
            throw MultifactorExceptionCodes.ERROR_CREATING_FACTOR.create(e.getMessage());
        }
    }

    @Override
    public Challenge beginAuthentication(int contextId, int userId, String login, String rpId) throws OXException {
        // TODO For passkey login, handle userId and context unknown.
        RelyingParty relyingParty = createRelyingParty(contextId, userId, rpId, new WebAuthnCredentialRepository(contextId, userId, getWebauthnStorage()));

        StartAssertionOptions.StartAssertionOptionsBuilder saob = StartAssertionOptions.builder()
            .userVerification(getUserVerificationRequirement(contextId, userId));
        if (login != null) { //  If we have login username then set so lookup for available public keys done
            saob.username(login);
        }
        AssertionRequest assertionRequest = relyingParty.startAssertion(saob.build());

        //Store the assertionRequest; we use the challenge as key
        String challenge = assertionRequest.getPublicKeyCredentialRequestOptions().getChallenge().getBase64Url();
        cache.storeAttestationObject(challenge, assertionRequest, getTimeLimit(contextId, userId));
        return new Challenge() {

            @Override
            public Map<String, Object> getChallenge() throws OXException {
                HashMap<String, Object> result = new HashMap<>(1);
                try {
                    final JSONObject json = JSONServices.parseObject(assertionRequest.toCredentialsGetJson());
                    result.put(PARAMETER_CREDENTIALS_GET_JSON_PARAMETER, json);
                } catch (JsonProcessingException | JSONException e) {
                    throw MultifactorExceptionCodes.JSON_ERROR.create(e.getMessage());
                }
                return result;
            }

        };
    }

    /**
     * Internal method to return the challenge from the given {@link ChallengeAnswer} instance
     *
     * @param answer The answer to get the challenge from
     * @return The challenge, or null if no challenge was found in the given answer
     * @throws OXException
     * @throws JSONException
     */
    private static String getChallenge(ChallengeAnswer answer) throws OXException, JSONException {
        JSONObject jsonAnswer = new JSONObject(answer.asMap());
        if (jsonAnswer.hasAndNotNull(PARAMETER_RESPONSE)) {
            JSONObject response = jsonAnswer.getJSONObject(PARAMETER_RESPONSE);
            if (response.hasAndNotNull(PARAMETER_CLIENT_DATA_JSON)) {
                JSONObject clientDataJSON = JSONServices.parseObject(Base64.getUrlDecoder().decode(response.getString(PARAMETER_CLIENT_DATA_JSON)));
                if (clientDataJSON.hasAndNotNull(PARAMETER_CHALLENGE)) {
                    return clientDataJSON.getString(PARAMETER_CHALLENGE);
                }
                throw MultifactorExceptionCodes.MISSING_PARAMETER.create(PARAMETER_CHALLENGE);
            }
            throw MultifactorExceptionCodes.MISSING_PARAMETER.create(PARAMETER_CLIENT_DATA_JSON);
        }
        throw MultifactorExceptionCodes.MISSING_PARAMETER.create(PARAMETER_CLIENT_DATA_JSON);
    }

    /**
     * Parse the public key credentials from answer
     *
     * @param answer to parse
     * @return PublicKeyCredentials returned
     * @throws IOException
     */
    private PublicKeyCredential<AuthenticatorAssertionResponse, ClientAssertionExtensionOutputs> parsePublicKeyCredentials(ChallengeAnswer answer) throws IOException {
        JSONObject answerJSON = new JSONObject(answer.asMap());
        PublicKeyCredential<AuthenticatorAssertionResponse, ClientAssertionExtensionOutputs> pkc = PublicKeyCredential.parseAssertionResponseJson(answerJSON.toString());
        return pkc;
    }

    @Override
    public void doAuthentication(int contextId, int userId, String rpId, ChallengeAnswer answer) throws OXException {
        try {
            //Get the temporary token from the storage by using the provided challenge
            String challenge = getChallenge(answer);
            Optional<AssertionRequest> assertionRequest = cache.getAttestationobject(challenge);
            if (!assertionRequest.isPresent()) {
                throw MultifactorExceptionCodes.AUTHENTICATION_FAILED.create();
            }
            PublicKeyCredential<AuthenticatorAssertionResponse, ClientAssertionExtensionOutputs> pkc = parsePublicKeyCredentials(answer);

            // TODO   Development for passkey login without username
            /// If login without userid/cid, see if we can get it from login info
            if (contextId == 0) {
                Optional<ByteArray> userHandle = pkc.getResponse().getUserHandle();
                if (userHandle.isPresent()) {
                    String userHandleStr = new String(userHandle.get().getBytes());
                    if (userHandleStr.contains("!!")) {
                        String login = userHandleStr.substring(0, userHandleStr.indexOf("!!"));
                        //  Get the userid/context for the user here.
                    }
                }
                // Throw error if unable to get id/cid
            }

            RelyingParty relyingParty = createRelyingParty(contextId, userId, rpId, new WebAuthnCredentialRepository(contextId, userId, getWebauthnStorage()));
            AssertionResult result = relyingParty.finishAssertion(FinishAssertionOptions.builder()
                .request(assertionRequest.get())
                .response(pkc)
                .build());

            //Increment the signature counter after use
            getWebauthnStorage().incrementCounterUpdateLastUsed(contextId, userId, result.getCredential().getCredentialId().getBase64(), result.getSignatureCount());

        } catch (AssertionFailedException e) {
            LoggerHolder.LOG.info(e.getMessage());
            throw MultifactorExceptionCodes.AUTHENTICATION_FAILED.create();
        } catch (@SuppressWarnings("unused") IOException e) {
            throw MultifactorExceptionCodes.UNKNOWN_ERROR.create("Failed to verify WebAuthn signature.");
        } catch (@SuppressWarnings("unused") RuntimeException e) {
            //Seems like the library throws a RuntimeException in some cases
            throw MultifactorExceptionCodes.UNKNOWN_ERROR.create("Failed to verify WebAuthn signature.");
        } catch (JSONException e) {
            throw MultifactorExceptionCodes.JSON_ERROR.create(e.getMessage());
        }
    }

    @Override
    public void renameDevice(int contextId, int userId, String deviceId, String newName) throws OXException {
        getWebauthnStorage().renameDevice(contextId, userId, deviceId, newName);
    }
}
