
package com.openexchange.webauthn.impl;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import com.openexchange.exception.OXException;
import com.openexchange.webauthn.WebAuthnDevice;
import com.openexchange.webauthn.storage.WebAuthnStorageService;
import com.yubico.webauthn.CredentialRepository;
import com.yubico.webauthn.RegisteredCredential;
import com.yubico.webauthn.data.ByteArray;
import com.yubico.webauthn.data.PublicKeyCredentialDescriptor;
import com.yubico.webauthn.data.PublicKeyCredentialType;
import com.yubico.webauthn.data.exception.Base64UrlException;

/**
 * {@link WebAuthnCredentialRepository} Implements Yubico CredentialRepository
 * for device lookup
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public class WebAuthnCredentialRepository implements CredentialRepository {

    private static class LoggerHolder {

        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(WebAuthnCredentialRepository.class);
    }

    private final int userId;
    private final int contextId;
    private final WebAuthnStorageService storage;

    /**
     * 
     * Initializes a new {@link WebAuthnCredentialRepository}.
     *
     * @param userId
     * @param contextId
     * @param storage WebAuthn Storage
     */
    public WebAuthnCredentialRepository(int contextId, int userId, WebAuthnStorageService storage) {
        super();
        this.userId = userId;
        this.contextId = contextId;
        this.storage = storage;
    }


    /**
     * Get all devices from storage for a user
     *
     * @return Collection of WebAuthnDevices for the user
     */
    private Collection<WebAuthnDevice> getWebAuthnDevices() {
        try {
            return storage.getDevices(contextId, userId);
        } catch (OXException e) {
            LoggerHolder.LOG.error("Problem getting devices", e);
            return Collections.emptyList();
        }

    }

    /**
     * Get PublicKeyCredentialDescriptors for devices by username
     *
     * @return Collection of PublicKeyCredentialDescriptors for the user
     */
    private Collection<PublicKeyCredentialDescriptor> getWebAuthnCredentials() {
        return getWebAuthnDevices().stream().map((Function<WebAuthnDevice, PublicKeyCredentialDescriptor>) device -> {
                return PublicKeyCredentialDescriptor.builder()
                .id(new ByteArray(Base64.getDecoder().decode(device.getKeyId())))
                .type(PublicKeyCredentialType.PUBLIC_KEY)
                .build();
        }).collect(Collectors.toList());
    }

    @Override
    public Set<PublicKeyCredentialDescriptor> getCredentialIdsForUsername(String username) {
        // Username not needed in search, as this will be done for the user specified by the userId and context when initialized
        return new HashSet<>(getWebAuthnCredentials());
    }

    /**
     * Compare two strings that may contain '@' If '@' present, then only
     * compares prefix. This is useful as login may be email@domain or email@context
     *
     * @param one First to compare
     * @param two Second to compare
     * @return True if same or prefix before '@' is same
     */
    private boolean compare(String one, String two) {
        if (one == null || two == null)
            return false;
        String toCompare = one.contains("@") ? one.substring(0, one.indexOf("@")) : one;
        String toCompare2 = two.contains("@") ? two.substring(0, two.indexOf("@")) : two;
        return toCompare.equals(toCompare2);
    }

    @Override
    public Optional<ByteArray> getUserHandleForUsername(String username) {
        if (username.isEmpty()) {
            return Optional.of(new ByteArray("".getBytes()));
        }
        Optional<WebAuthnDevice> dev = getWebAuthnDevices()
            .stream()
            .filter(d -> compare(d.getLogin(), username))
            .findFirst();
        if (!dev.isEmpty()) {
            try {
                return Optional.of(dev.get().getUserId());
            } catch (Base64UrlException e) {
                LoggerHolder.LOG.error("Error getting userhandle", e);
            }
        }
        return Optional.empty();
    }

    @Override
    public Optional<String> getUsernameForUserHandle(ByteArray userHandle) {
        Optional<WebAuthnDevice> webAuthnDevice = getWebAuthnDevices()
            .stream()
            .filter(d -> d.getUserIdString().equals(userHandle.getBase64Url()))
            .findFirst();
        if (webAuthnDevice.isPresent()) {
            return Optional.of(webAuthnDevice.get().getLogin());
        }
        return Optional.empty();
    }

    @Override
    public Optional<RegisteredCredential> lookup(ByteArray credentialId, ByteArray userHandle) {
        Optional<WebAuthnDevice> webauthnDevice = getWebAuthnDevices()
            .stream()
            .filter(d -> d.getKeyId().equals(credentialId.getBase64()))
            .findFirst();
        if (webauthnDevice.isPresent()) {
            RegisteredCredential registeredCredentials;
                registeredCredentials = RegisteredCredential.builder()
                    .credentialId(credentialId)
                    .userHandle(userHandle)
                    .publicKeyCose(ByteArray.fromBase64(webauthnDevice.get().getPublicKeyCose()))
                    .signatureCount(webauthnDevice.get().getSignatureCount())
                    .build();
                return Optional.of(registeredCredentials);


        }
        return Optional.empty();
    }

    @Override
    public Set<RegisteredCredential> lookupAll(ByteArray credentialId) {
        ArrayList<RegisteredCredential> ret = new ArrayList<RegisteredCredential>();
        getWebAuthnDevices()
            .stream()
            .filter(d -> d.getKeyId().equals(credentialId.getBase64()))
            .forEach(dev -> {
                RegisteredCredential registeredCredentials;
                try {
                    registeredCredentials = RegisteredCredential.builder()
                        .credentialId(credentialId)
                        .userHandle(dev.getUserId())
                        .publicKeyCose(ByteArray.fromBase64(dev.getPublicKeyCose()))
                        .signatureCount(dev.getSignatureCount())
                        .build();
                    ret.add(registeredCredentials);
                } catch (Base64UrlException e) {
                    LoggerHolder.LOG.error("Error getting lookup by credentialID ", e);
                }

            });
        return new HashSet<RegisteredCredential>(ret);
    }
}
