/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.webauthn.impl;

import com.openexchange.multifactor.AnswerField;

/**
 * {@link WebAuthnAnswerField}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public enum WebAuthnAnswerField implements AnswerField {

    /**
     * ID
     */
    ID("id"),
    /**
     * The attestation object
     */
    ATTESTATION_OBJECT("attestationObject"),
    /**
     * The client data json
     */
    CLIENT_DATA_JSON("clientDataJSON"),

    /**
     * The client extension results
     */
    CLIENT_EXTENSION_RESULTS("clientExtensionResults"),

    ;

    private final String key;

    /**
     * Initializes a new {@link WebAuthnAnswerField}.
     */
    private WebAuthnAnswerField(String key) {
        this.key = key;
    }

    @Override
    public String getKey() {
        return key;
    }

}
