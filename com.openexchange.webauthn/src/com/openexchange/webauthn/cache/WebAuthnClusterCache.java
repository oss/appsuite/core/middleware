/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.webauthn.cache;

import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.exception.OXException;
import com.yubico.webauthn.AssertionRequest;

/**
 * {@link WebAuthnClusterCache} Implements cache for Cluster based cache
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
@SuppressWarnings("rawtypes")
public class WebAuthnClusterCache implements WebAuthnCache {

    private final BasicCoreClusterMapProvider<WebAuthnCacheItem> clusterMapProvider;

    /**
     * Initializes a new {@link WebAuthnClusterCache}.
     *
     * @param clusterMapService ClusterMapService to use
     * @param mapName The CoreMap to be used
     */
    public WebAuthnClusterCache(ClusterMapService clusterMapService, CoreMap mapName) {
        this.clusterMapProvider = BasicCoreClusterMapProvider.<WebAuthnCacheItem> builder() //@formatter:off
            .withCoreMap(Objects.requireNonNull(mapName, "mapName must not be null"))
            .withCodec(new WebAuthnItemCodec())
            .withServiceSupplier(BasicCoreClusterMapProvider.singletonClusterMapServiceSupplier(Objects.requireNonNull(clusterMapService, "ClusterMapService must not be null")))
            .build();
    }

    /**
     * Simple internal method to return Duration in milliseconds
     * getMs
     *
     * @param d  Duration
     * @return  Duration in milliseconds
     */
    private long getMs(Duration d) {
        if (d == null) {
            return 0L;
        }
        return d.toMillis();
    }


    @SuppressWarnings("unchecked")
    @Override
    public void storeAttestationObject(String id, AssertionRequest object, Duration d) throws OXException {
        WebAuthnCacheItem<AssertionRequest> item = new WebAuthnCacheItem(object);
        clusterMapProvider.getMap().put(id, item, getMs(d));
    }

    @SuppressWarnings({ "unchecked" })
    @Override
    public Optional<AssertionRequest> getAttestationobject(String id) throws OXException {
        WebAuthnCacheItem<AssertionRequest> item = clusterMapProvider.getMap().remove(id);
        if (item == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(item.getItem());
    }

    @Override
    public void storePublicKeyCredOptions(String challenge, WebAuthnRegistrationItem pkc, Duration d) throws OXException {
        WebAuthnCacheItem<WebAuthnRegistrationItem> item = new WebAuthnCacheItem<WebAuthnRegistrationItem>(pkc);
        clusterMapProvider.getMap().put(challenge, item, getMs(d));
    }

    @SuppressWarnings("unchecked")
    @Override
    public Optional<WebAuthnRegistrationItem> getPublicKeyCredOptions(String challenge) throws OXException {
        WebAuthnCacheItem<WebAuthnRegistrationItem> item = clusterMapProvider.getMap().remove(challenge);
        if (item == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(item.getItem());
    }

}
