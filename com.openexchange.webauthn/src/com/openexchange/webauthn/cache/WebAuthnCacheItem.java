/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.webauthn.cache;

import com.openexchange.exception.OXException;
import com.yubico.webauthn.AssertionRequest;

/**
 *
 * {@link WebAuthnCacheItem} Item for storage in cache
 * Should be object of AssertionRequest or WebAuthnRegistrationItem
 * Other types will be rejected
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @param <T>
 */
public class WebAuthnCacheItem<T> {

    private final T item;       // Item to store
    private final Object_Type type;  // Type of the item

    protected enum Object_Type {
        REGISTRATION,
        AUTHENTICATION
    }

    /**
     *
     * Initializes a new {@link WebAuthnCacheItem}.
     *
     * @param item Item to store
     * @throws OXException
     */
    public WebAuthnCacheItem(T item) throws OXException {
        this.type = getType(item);
        this.item = item;
    }

    /**
     * Gets the type of item. Throws error if its not known.
     *
     * @param item Item to be stored
     * @return Enum of item type
     * @throws OXException
     */
    private Object_Type getType(Object item) throws OXException {
        if (item instanceof AssertionRequest) {
            return Object_Type.AUTHENTICATION;
        }
        if (item instanceof WebAuthnRegistrationItem) {
            return Object_Type.REGISTRATION;
        }
        throw OXException.notFound("Invalid cache type " + item.getClass().getName());
    }

    /**
     * Simple getter of item
     *
     * @return The item
     */
    public T getItem() {
        return item;
    }

    /**
     * Simple getter of object type
     *
     * @return The object type
     */
    protected Object_Type getType() {
        return type;
    }

}
