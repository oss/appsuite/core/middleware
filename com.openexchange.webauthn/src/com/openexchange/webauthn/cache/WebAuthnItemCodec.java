/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.webauthn.cache;

import org.json.JSONObject;
import com.openexchange.annotation.NonNull;
import com.openexchange.cluster.map.codec.AbstractJSONMapCodec;
import com.openexchange.exception.OXException;
import com.yubico.webauthn.AssertionRequest;
import com.yubico.webauthn.data.PublicKeyCredentialCreationOptions;

/**
 * Codec for WebAuthn Cache. Converts items to/from json for cache
 */
@SuppressWarnings("rawtypes")
public class WebAuthnItemCodec extends AbstractJSONMapCodec<WebAuthnCacheItem> {

    @Override
    protected @NonNull JSONObject writeJson(WebAuthnCacheItem value) throws Exception {
        JSONObject json = new JSONObject();
        if (value.getType().equals(WebAuthnCacheItem.Object_Type.AUTHENTICATION)) {
            json.put("t", "a");
            AssertionRequest request = (AssertionRequest) value.getItem();
            json.put("a", JSONObject.parse(request.toJson()));
            return json;
        }
        if (value.getType().equals(WebAuthnCacheItem.Object_Type.REGISTRATION)) {
            json.put("t", "r");
            WebAuthnRegistrationItem pkc = (WebAuthnRegistrationItem) value.getItem();
            json.put("r", JSONObject.parse(pkc.getPkc().toJson()));
            json.put("n", pkc.getName());
            return json;
        }
        throw OXException.general("Serialization of WebAuthn cache item failed");
    }

    @Override
    protected @NonNull WebAuthnCacheItem parseJson(JSONObject jObject) throws Exception {
        if (jObject.has("t")) {
            switch (jObject.getString("t")) {
                case "a":
                    AssertionRequest req = AssertionRequest.fromJson(jObject.getString("a"));
                    return new WebAuthnCacheItem<>(req);
                case "r":
                    PublicKeyCredentialCreationOptions pkc = PublicKeyCredentialCreationOptions.fromJson(jObject.getString("r"));
                    String name = jObject.getString("n");
                    return new WebAuthnCacheItem<>(new WebAuthnRegistrationItem(pkc, name));
            }
        }
        // Unsupported type/unkown
        throw OXException.general("Deserialization of WebAuthn Cache item failed");
    }

}
