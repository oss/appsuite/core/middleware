/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.webauthn.cache;

import java.time.Duration;
import java.util.Optional;
import com.openexchange.exception.OXException;
import com.yubico.webauthn.AssertionRequest;

/**
 * 
 * {@link WebAuthnCache} To store challenges temporarily for authenticator use/registration
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public interface WebAuthnCache {

    /**
     * Stores an attestation object after sent to the device for authentication
     *
     * @param id Device id for storage
     * @param object Attestation object
     * @param d Duration before item times out
     * @throws OXException
     */
    public void storeAttestationObject(String id, AssertionRequest object, Duration d) throws OXException;

    /**
     * Gets the cached attestation object from storage
     *
     * @param id Device id for the lookup
     * @return The stored AssertionRequest
     * @throws OXException
     */
    public Optional<AssertionRequest> getAttestationobject(String id) throws OXException;

    /**
     * Stores the WebAuthnRegistrationItem sent to the device for signing.
     *
     * @param challenge Challenge for storage
     * @param reg The registration item
     * @param d Duration before item times out
     * @throws OXException
     */
    public void storePublicKeyCredOptions(String challenge, WebAuthnRegistrationItem reg, Duration d) throws OXException;

    /**
     * Retrieves the WebAuthnRegistrationItem from cache
     *
     * @param challenge Challenge for lookup
     * @return Stored WebAuthnRegistrationItem
     * @throws OXException
     */
    public Optional<WebAuthnRegistrationItem> getPublicKeyCredOptions(String challenge) throws OXException;

}
