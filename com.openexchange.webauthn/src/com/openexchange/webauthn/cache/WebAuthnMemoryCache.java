/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.webauthn.cache;

import java.time.Duration;
import java.util.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.yubico.webauthn.AssertionRequest;

/**
 * Simple memory only cache
 * {@link WebAuthnMemoryCache}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public class WebAuthnMemoryCache implements WebAuthnCache {

    
    private Cache<String, AssertionRequest> attCache;
    private Cache<String, WebAuthnRegistrationItem> pkcCache;
    
    
    public WebAuthnMemoryCache() {
        super();
        attCache = CacheBuilder.newBuilder()
            .maximumSize(2000)
            .expireAfterWrite(Duration.ofMinutes(5))
            .build();
        pkcCache = CacheBuilder.newBuilder()
            .maximumSize(2000)
            .expireAfterWrite(Duration.ofMinutes(5))
            .build();
    }

    @Override
    public void storeAttestationObject(String id, AssertionRequest object, Duration d) {
        attCache.put(id, object);
        
    }

    @Override
    public Optional<AssertionRequest> getAttestationobject(String id) {
        return Optional.ofNullable(attCache.getIfPresent(id));
    }

    @Override
    public void storePublicKeyCredOptions(String challenge, WebAuthnRegistrationItem pkc, Duration d) {
        pkcCache.put(challenge, pkc);
        
    }
    @Override
    public Optional<WebAuthnRegistrationItem> getPublicKeyCredOptions(String challenge) {
        return Optional.ofNullable(pkcCache.getIfPresent(challenge));
    }

}
