package com.openexchange.webauthn.osgi;

import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.database.CreateTableService;
import com.openexchange.groupware.update.DefaultUpdateTaskProviderService;
import com.openexchange.groupware.update.UpdateTaskProviderService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.webauthn.WebAuthnService;
import com.openexchange.webauthn.cache.WebAuthnClusterCache;
import com.openexchange.webauthn.cache.WebAuthnMemoryCache;
import com.openexchange.webauthn.impl.WebAuthnServiceImpl;
import com.openexchange.webauthn.storage.WebAuthnStorageService;
import com.openexchange.webauthn.storage.rdb.CreateWebAuthnTable;
import com.openexchange.webauthn.storage.rdb.CreateWebAuthnTableTask;
import com.openexchange.webauthn.storage.rdb.WebAuthnDbStorage;

public class WebauthnActivator extends HousekeepingActivator {

    static final Logger LOG = org.slf4j.LoggerFactory.getLogger(WebauthnActivator.class);

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] {};
    }

    @Override
    protected void startBundle() throws Exception {
        LOG.info("Starting WebAuthn bundle");
        registerService(WebAuthnStorageService.class, new WebAuthnDbStorage(this));
        registerService(UpdateTaskProviderService.class, new DefaultUpdateTaskProviderService(new CreateWebAuthnTableTask()));
        registerService(CreateTableService.class, new CreateWebAuthnTable());
        final WebAuthnService webAuthnService = new WebAuthnServiceImpl(this);
        webAuthnService.setCache(new WebAuthnMemoryCache());  // Use memory cache unless redis/cluster avail
        registerService(WebAuthnService.class, webAuthnService);
        //Switch to ClusterMapService token storage, if ClusterMapService is available
        final ServiceTrackerCustomizer<ClusterMapService, ClusterMapService> clusterMapServiceTracker = new ServiceTrackerCustomizer<ClusterMapService, ClusterMapService>() {

            @Override
            public ClusterMapService addingService(ServiceReference<ClusterMapService> reference) {
                final ClusterMapService clusterMapService = context.getService(reference);

                final WebAuthnClusterCache cachedTokenStorage = new WebAuthnClusterCache(
                    clusterMapService,
                    CoreMap.WEB_AUTHN);
                webAuthnService.setCache(cachedTokenStorage);
                return clusterMapService;
            }

            @Override
            public void modifiedService(ServiceReference<ClusterMapService> reference, ClusterMapService service) {
                //no-op
            }

            @Override
            public void removedService(ServiceReference<ClusterMapService> reference, ClusterMapService service) {
                //Switch back to the default storage
                webAuthnService.setCache(new WebAuthnMemoryCache());
            }
        };
        track(ClusterMapService.class, clusterMapServiceTracker);
        openTrackers();

    }

    @Override
    protected void stopBundle() throws Exception {
        LOG.info("Stopping webauthn bundle");
        super.stopBundle();
        unregisterServices();

    }

}
