/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.webauthn;

import java.util.Date;
import com.openexchange.exception.OXException;
import com.yubico.webauthn.data.ByteArray;
import com.yubico.webauthn.data.exception.Base64UrlException;

/**
 * 
 * {@link WebAuthnDevice} Stores details of webauthn device
 * used for authentication
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public class WebAuthnDevice {

    /**
     * WebAuthn device builder
     *
     * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
     */
    public static class Builder {

        private int id;                                 // User OX Id
        private int contextId;                          // User Context Id
        private String deviceId;                        // An id of the device
        private String keyId;                           // The id of the public key
        private String userId;                          // Users webauth id
        private String login;                           // Users OX login
        private String name;                            // Name of the device
        private String publicKey;                       // The public key
        private String attestationCertificate = null;   // Attestation certificate
        private String clientDataJson;                  // Client data json returned at registration
        private boolean isMultifactor;                  // If device is used as multifactor device
        private boolean isDiscoverable;                 // If the device is passkey discoverable
        private long counter;                           // Signature counter
        private boolean compromised;                    // If compromised (for future)
        private boolean enabled = true;                 // If device enabled
        private Date lastUsed;                          // Last time device was used

        /**
         * Sets the users OX Id
         *
         * @param id OX User Id
         * @return
         */
        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        /**
         * Sets the user contextId
         *
         * @param cid
         * @return Builder
         */
        public Builder withContextId(int cid) {
            this.contextId = cid;
            return this;
        }

        /**
         * Sets the ID of the device
         *
         * @param id
         * @return Builder
         */
        public Builder withDeviceId(String id) {
            this.deviceId = id;
            return this;
        }

        /**
         * Sets the public key ID
         *
         * @param keyId
         * @return Builder
         */
        public Builder withKeyId(String keyId) {
            this.keyId = keyId;
            return this;
        }

        /**
         * Sets the name for the device. User assigned or default
         *
         * @param name
         * @return Builder
         */
        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the user login string
         *
         * @param login
         * @return Builder
         */
        public Builder withLogin(String login) {
            this.login = login;
            return this;
        }

        /**
         * Sets the user Id
         *
         * @param userId in ByteArray
         * @return Builder
         */
        public Builder withUserId(ByteArray userId) {
            this.userId = userId.getBase64Url();
            return this;
        }

        /**
         * Sets the user Id as string
         *
         * @param userId
         * @return Builder
         */
        public Builder withUserIdString(String userId) {
            this.userId = userId;
            return this;
        }

        /**
         * Sets the public key
         *
         * @param key
         * @return Builder
         */
        public Builder withPublicKey(String key) {
            this.publicKey = key;
            return this;
        }

        /**
         * Sets the attestation chain
         *
         * @param att
         * @return Builder
         */
        public Builder withAttestationCertificate(String att) {
            this.attestationCertificate = att;
            return this;
        }

        /**
         * Sets the clientDataJson
         *
         * @param json
         * @return Builder
         */
        public Builder withClientDataJson(String json) {
            this.clientDataJson = json;
            return this;
        }

        /**
         * Sets if multifactor
         *
         * @param isMulti
         * @return Builder
         */
        public Builder isMultifactor(boolean isMulti) {
            this.isMultifactor = isMulti;
            return this;
        }

        /**
         * Sets if discoverable
         *
         * @param isDiscoverable
         * @return Builder
         */
        public Builder isDiscoverable(boolean isDiscoverable) {
            this.isDiscoverable = isDiscoverable;
            return this;
        }

        /**
         * Sets if compromised
         *
         * @param isComp
         * @return Builder
         */
        public Builder isCompromized(boolean isComp) {
            this.compromised = isComp;
            return this;
        }

        /**
         * Sets existing signature count
         *
         * @param counter
         * @return Builder
         */
        public Builder withCounter(long counter) {
            this.counter = counter;
            return this;
        }

        /**
         * Sets if device is enabled
         *
         * @param enabled
         * @return Builder
         */
        public Builder isEnabled(boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets last used date
         *
         * @param lastUsed
         * @return Builder
         */
        public Builder lastUsed(Date lastUsed) {
            this.lastUsed = lastUsed;
            return this;
        }

        /**
         * Build the device
         *
         * @return A WebAuthnDevice
         * @throws OXException
         */
        public WebAuthnDevice build() throws OXException {
            if (deviceId == null || name == null || publicKey == null) {
                throw OXException.mandatoryField("Missing required parameters for WebAuthnDevice");
            }
            return new WebAuthnDevice(id, contextId, deviceId, name, login, userId, keyId, publicKey, attestationCertificate, clientDataJson, isMultifactor, isDiscoverable, counter, compromised, enabled, lastUsed);
        }
        
    }

    private int id;                                 // User OX Id
    private int contextId;                          // User Context Id
    private String deviceId;                        // An id of the device
    private String keyId;                           // The id of the public key
    private String userId;                          // Users webauthn id
    private String login;                           // Users login string
    private String name;                            // Name of the device
    private String publicKey;                       // The public key
    private String attestationCertificate;          // Attestation certificate
    private String clientDataJson;                  // Client data json returned at registration
    private boolean isMultifactor;                  // If device is used as multifactor device
    private boolean isDiscoverable;                 // If the device is passkey discoverable
    private long counter;                           // Signature counter
    private boolean compromised;                    // If compromised (for future)
    private boolean enabled;                        // If device enabled
    private Date lastUsed;                          // Last time device was used

    /**
     * A WebAuthn Device for storage
     *
     * @param id
     * @param contextId
     * @param deviceId
     * @param name
     * @param login
     * @param userId
     * @param keyId
     * @param publicKey
     * @param attestationCertificate
     * @param isMultifactor
     * @param counter
     * @param compromised
     * @param enabled
     * @param lastUsed
     */
    public WebAuthnDevice(int id,
                          int contextId,
                          String deviceId,
                          String name,
                          String login,
                          String userId,
                          String keyId,
                          String publicKey,
                          String attestationCertificate,
                          String clientDataJson,
                          boolean isMultifactor,
                          boolean isDiscoverable,
                          long counter,
                          boolean compromised,
                          boolean enabled,
                          Date lastUsed) {
        super();
        this.id = id;
        this.contextId = contextId;
        this.deviceId = deviceId;
        this.name = name;
        this.userId = userId;
        this.login = login;
        this.publicKey = publicKey;
        this.attestationCertificate = attestationCertificate;
        this.clientDataJson = clientDataJson;
        this.isMultifactor = isMultifactor;
        this.counter = counter;
        this.compromised = compromised;
        this.keyId = keyId;
        this.enabled = enabled;
        this.lastUsed = lastUsed;
    }

    /**
     * Gets the id
     *
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the contextId
     *
     * @return The contextId
     */
    public int getContextId() {
        return contextId;
    }

    /**
     * Gets the counter
     *
     * @return The counter
     */
    public long getCounter() {
        return counter;
    }

    /**
     * Gets the enabled
     *
     * @return The enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Gets the lastUsed
     *
     * @return The lastUsed
     */
    public Date getLastUsed() {
        return lastUsed;
    }

    /**
     * Get the deviceId
     *
     * @return String deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * 
     * Gets the name of the device
     *
     * @return String of the name
     */
    public String getName() {
        return name;
    }

    /**
     * Get the key ID
     * 
     * @return The Key Id
     */
    public String getKeyId() {
        return keyId;
    }

    /**
     * Get the public key cose
     *
     * @return The Public Key Cose endocded
     */
    public String getPublicKeyCose() {
        return publicKey;
    }

    /**
     * Get the signature count
     *
     * @return Current signature count
     */
    public long getSignatureCount() {
        return counter;
    }

    /**
     * Returns the users login string
     *
     * @return User login string
     */
    public String getLogin() {
        return login;
    }

    /**
     * Get ByteArray of the userId
     *
     * @return A Base64URL Encoded string of the userId
     * @throws Base64UrlException
     */
    public ByteArray getUserId() throws Base64UrlException {
        return ByteArray.fromBase64Url(userId);
    }

    /**
     * Returns the base64 encoded UserId
     *
     * @return the base64 encoded UserId
     */
    public String getUserIdString() {
        return userId;
    }

    /**
     * Sets the new signature count
     *
     * @param signature count
     */
    public void setSignatureCount(long count) {
        this.counter = count;
    }

    /**
     * Gets the publicKey
     *
     * @return The publicKey
     */
    public String getPublicKey() {
        return publicKey;
    }

    /**
     * Sets the publicKey
     *
     * @param publicKey The publicKey to set
     */
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    /**
     * Gets the attestationCertificate
     *
     * @return The attestationCertificate
     */
    public String getAttestationCertificate() {
        return attestationCertificate;
    }

    /**
     * Gets the clientDataJson
     *
     * @return The clientDataJson
     */
    public String getClientDataJson() {
        return clientDataJson;
    }

    /**
     * Sets the attestationCertificate
     *
     * @param attestationCertificate The attestationCertificate to set
     */
    public void setAttestationCertificate(String attestationCertificate) {
        this.attestationCertificate = attestationCertificate;
    }

    /**
     * Gets the isMultifactor
     *
     * @return The isMultifactor
     */
    public boolean isMultifactor() {
        return isMultifactor;
    }

    /**
     * Gets the isDiscoverable
     *
     * @return The isDiscoverable
     */
    public boolean isDiscoverable() {
        return isDiscoverable;
    }

    /**
     * Sets the isMultifactor
     *
     * @param isMultifactor The isMultifactor to set
     */
    public void setMultifactor(boolean isMultifactor) {
        this.isMultifactor = isMultifactor;
    }

    /**
     * Gets the compromised
     *
     * @return if auth compromised
     */
    public boolean isCompromised() {
        return compromised;
    }

    /**
     * Sets the compromised
     *
     * @param compromised The compromised to set
     */
    public void setCompromised(boolean compromised) {
        this.compromised = compromised;
    }

    /**
     * Sets the deviceId
     *
     * @param deviceId The deviceId to set
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * Sets the name
     *
     * @param name The name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the userId
     *
     * @param userId The userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Sets the keyId
     *
     * @param keyId The keyId to set
     */
    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

}
