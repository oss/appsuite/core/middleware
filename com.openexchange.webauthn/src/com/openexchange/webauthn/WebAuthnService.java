/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.webauthn;

import com.openexchange.exception.OXException;
import com.openexchange.multifactor.Challenge;
import com.openexchange.multifactor.ChallengeAnswer;
import com.openexchange.multifactor.RegistrationChallenge;
import com.openexchange.webauthn.cache.WebAuthnCache;

/**
 * {@link WebAuthnService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public interface WebAuthnService {

    /**
     * startRegistration begins the registration process. Creates a challenge to return
     * to the device for signing and enrollment.
     *
     * @param contextId OX Context Id
     * @param userId OX User Id
     * @param login OX Login
     * @param deviceName Name for the device
     * @param rpId ReplyingParty address such as open-xchange.com
     * @param secondFactor If the device is second factor auth (i.e. multifactor)
     * @return A registration challenge
     * @throws OXException
     */
    RegistrationChallenge startRegistration(int contextId, int userId, String login, String deviceName, String rpId, boolean secondFactor) throws OXException;

    /**
     * finishRegistration completes the registration for a device.
     *
     * @param contextId OX Context Id
     * @param userId OX User Id
     * @param deviceId ID of the device
     * @param answer The Challenge Answer
     * @param secondFactor If the device is second factor auth (i.e. multifactor)
     * @return The newly created webAuthnDevice
     * @throws OXException
     */
    WebAuthnDevice finishRegistration(int contextId, int userId, String deviceId, ChallengeAnswer answer, boolean secondFactor) throws OXException;

    /**
     * beginAuthentication begins the authentication process. Creates a challenge for
     * the device to sign and return
     *
     * @param contextId OX Context Id
     * @param userId OX User Id
     * @param rpId RelyingParty ID
     * @return A challenge
     * @throws OXException
     */
    Challenge beginAuthentication(int contextId, int userId, String login, String rpId) throws OXException;

    /**
     * Do the authentication based on the challenge answer
     *
     * @param contextId OX Context Id
     * @param userId OX User Id
     * @param rpId The configured RelyingParty ID
     * @param answer The challenge answer
     * @throws OXException OX Exception if authentication fails
     */
    void doAuthentication(int contextid, int userId, String rpId, ChallengeAnswer answer) throws OXException;

    /**
     * setCache Sets the cache to use for storing challenges during registration
     * and authentication.
     *
     * @param cache
     */
    void setCache(WebAuthnCache cache);

    /**
     * Rename a device
     *
     * @param contextId OX Context Id
     * @param userId OX User Id
     * @param deviceId The device ID to rename
     * @param newName The new name for the device
     * @throws OXException
     */
    void renameDevice(int contextId, int userId, String deviceId, String newName) throws OXException;

}
