/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.webauthn.storage.rdb;

import com.openexchange.database.AbstractCreateTableImpl;

/**
 * {@link CreateWebAuthnTable} Creates the webauthn table which contains
 * Webauthn device information
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public class CreateWebAuthnTable extends AbstractCreateTableImpl {

    private static final String TABLE_WEBAUTHN = "web_authn";

    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_WEBAUTHN + " (\n"
        + "  `cid` int(11) NOT NULL,\n"
        + "  `id` int(11) NOT NULL,\n"
        + "  `deviceId` varchar(100) NOT NULL,\n"
        + "  `keyId` varchar(500) NOT NULL,\n"
        + "  `userId` varchar(100) DEFAULT NULL,\n"
        + "  `name` varchar(100) DEFAULT NULL,\n"
        + "  `login` varchar(100) DEFAULT NULL,\n"
        + "  `publicKey` varchar(1000) DEFAULT NULL,\n"
        + "  `attestation` varchar(10000) DEFAULT NULL,\n"
        + "  `clientData` varchar(2000) DEFAULT NULL,\n"
        + "  `counter` int(11) DEFAULT NULL,\n"
        + "  `compromised` bit(1) DEFAULT NULL,\n"
        + "  `isMultifactor` bit(1) DEFAULT NULL,\n"
        + "  `discoverable` bit(1) DEFAULT NULL,\n"
        + "  `lastUsed` datetime DEFAULT NULL,\n"
        + "  `enabled` bit(1) DEFAULT NULL,\n"
        + "  PRIMARY KEY (cid,id,deviceId),\n"
        + "  KEY `web_authn_id_IDX` (`cid`) USING BTREE\n"
        + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;";

    @Override
    public String[] requiredTables() {
        return NO_TABLES;
    }

    @Override
    public String[] tablesToCreate() {
        return new String[] { TABLE_WEBAUTHN };
    }

    @Override
    protected String[] getCreateStatements() {
        return new String[] { CREATE_TABLE };
    }
}

