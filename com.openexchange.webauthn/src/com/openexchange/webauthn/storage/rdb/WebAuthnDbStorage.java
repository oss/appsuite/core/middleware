/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.webauthn.storage.rdb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.multifactor.exceptions.MultifactorExceptionCodes;
import com.openexchange.server.ServiceLookup;
import com.openexchange.webauthn.WebAuthnDevice;
import com.openexchange.webauthn.storage.WebAuthnStorageService;

/**
 * {@link WebAuthnDbStorage} Database storage implementation for WebAuthn
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public class WebAuthnDbStorage implements WebAuthnStorageService {

    private final ServiceLookup serviceLookup;

    /**
     * 
     * Initializes a new {@link WebAuthnDbStorage}.
     *
     * @param serviceLookup
     */
    public WebAuthnDbStorage(ServiceLookup serviceLookup) {
        this.serviceLookup = serviceLookup;
    }

    /**
     * Simple internal method to get DatabaseService
     *
     * @return The databaseService
     * @throws OXException
     */
    private DatabaseService getDatabaseService() throws OXException {
        return serviceLookup.getServiceSafe(DatabaseService.class);
    }

    /**
     * Internal method to create device from ResultSet
     *
     * @param rs The resultSet
     * @return A new WebAuthnDevice from the resultSet
     * @throws OXException
     * @throws SQLException
     */
    private WebAuthnDevice getDevice(ResultSet rs) throws OXException, SQLException {
        return new WebAuthnDevice.Builder()
            .withId(rs.getInt("id"))
            .withContextId(rs.getInt("cid"))
            .withDeviceId(rs.getString("deviceId"))
            .withKeyId(rs.getString("keyId"))
            .withUserIdString(rs.getString("userId"))
            .withName(rs.getString("name"))
            .withLogin(rs.getString("login"))
            .withPublicKey(rs.getString("publicKey"))
            .withAttestationCertificate(rs.getString("attestation"))
            .withClientDataJson(rs.getString("clientData"))
            .withCounter(rs.getLong("counter"))
            .isCompromized(rs.getBoolean("compromised"))
            .isMultifactor(rs.getBoolean("isMultifactor"))
            .lastUsed(rs.getTimestamp("lastUsed"))
            .isEnabled(rs.getBoolean("enabled"))
            .build();
    }

    @Override
    public void registerDevice(int contextId, int userId, WebAuthnDevice device) throws OXException {
        PreparedStatement statement = null;
        final DatabaseService dbs = getDatabaseService();
        final Connection connection = dbs.getWritable(contextId);
        try {
            statement = connection.prepareStatement(WebAuthnSql.INSERT_DEVICE);
            int index = 1;
            statement.setInt(index++, device.getId());
            statement.setInt(index++, device.getContextId());
            statement.setString(index++, device.getDeviceId());
            statement.setString(index++, device.getKeyId());
            statement.setString(index++, device.getUserIdString());
            statement.setString(index++, device.getName());
            statement.setString(index++, device.getLogin());
            statement.setString(index++, device.getPublicKey());
            statement.setString(index++, device.getAttestationCertificate());
            statement.setString(index++, device.getClientDataJson());
            statement.setLong(index++, device.getCounter());
            statement.setBoolean(index++, device.isCompromised());
            statement.setBoolean(index++, device.isMultifactor());
            statement.setBoolean(index++, device.isDiscoverable());
            statement.setTimestamp(index++, new java.sql.Timestamp(System.currentTimeMillis()));
            statement.setBoolean(index++, device.isEnabled());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw MultifactorExceptionCodes.SQL_EXCEPTION.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(statement);
            if (connection != null) {
                dbs.backWritable(connection);
            }
        }

    }

    @Override
    public boolean unregisterDevice(int contextId, int userId, String deviceId) throws OXException {
        PreparedStatement statement = null;
        final DatabaseService dbs = getDatabaseService();
        final Connection connection = dbs.getWritable(contextId);
        try {
            statement = connection.prepareStatement(WebAuthnSql.REMOVE_DEVICE);
            int index = 1;
            statement.setInt(index++, contextId);
            statement.setInt(index++, userId);
            statement.setString(index++, deviceId);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw MultifactorExceptionCodes.SQL_EXCEPTION.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(statement);
            if (connection != null) {
                dbs.backWritable(connection);
            }
        }
    }

    @Override
    public Collection<WebAuthnDevice> getDevices(int contextId, int userId) throws OXException {
        ArrayList<WebAuthnDevice> devices = new ArrayList<WebAuthnDevice>();
        PreparedStatement statement = null;
        ResultSet rs = null;
        final DatabaseService dbs = getDatabaseService();
        final Connection connection = dbs.getReadOnly(contextId);
        try {
            statement = connection.prepareStatement(WebAuthnSql.GET_DEVICES_FOR_USER);
            int index = 1;
            statement.setInt(index++, contextId);
            statement.setInt(index++, userId);
            rs = statement.executeQuery();
            while (rs.next()) {
                devices.add(getDevice(rs));
            }
        } catch (SQLException e) {
            throw MultifactorExceptionCodes.SQL_EXCEPTION.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(statement, rs);
            if (connection != null) {
                dbs.backReadOnly(connection);
            }
        }
        return devices;

    }

    @Override
    public Optional<WebAuthnDevice> getDevice(int contextId, int userId, String deviceId) throws OXException {
        PreparedStatement statement = null;
        ResultSet rs = null;
        final DatabaseService dbs = getDatabaseService();
        final Connection connection = dbs.getReadOnly(contextId);
        try {
            statement = connection.prepareStatement(WebAuthnSql.GET_DEVICE_FOR_USER);
            int index = 1;
            statement.setInt(index++, contextId);
            statement.setInt(index++, userId);
            statement.setString(index++, deviceId);
            rs = statement.executeQuery();
            if (rs.next()) {
                return Optional.of(getDevice(rs));
            }
            return Optional.empty();
        } catch (SQLException e) {
            throw MultifactorExceptionCodes.SQL_EXCEPTION.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(statement, rs);
            if (connection != null) {
                dbs.backReadOnly(connection);
            }
        }
    }

    @Override
    public boolean renameDevice(int contextId, int userId, String deviceId, String name) throws OXException {
        PreparedStatement statement = null;
        final DatabaseService dbs = getDatabaseService();
        final Connection connection = dbs.getWritable(contextId);
        try {
            statement = connection.prepareStatement(WebAuthnSql.UPDATE_NAME);
            int index = 1;
            statement.setString(index++, name);
            statement.setInt(index++, contextId);
            statement.setInt(index++, userId);
            statement.setString(index++, deviceId);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw MultifactorExceptionCodes.SQL_EXCEPTION.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(statement);
            if (connection != null) {
                dbs.backWritable(connection);
            }
        }
    }

    @Override
    public boolean incrementCounterUpdateLastUsed(int contextId, int userId, String keyId, long current) throws OXException {
        PreparedStatement statement = null;
        final DatabaseService dbs = getDatabaseService();
        final Connection connection = dbs.getWritable(contextId);
        try {
            statement = connection.prepareStatement(WebAuthnSql.UPDATE_COUNTER_AND_LAST_USED);
            int index = 1;
            statement.setLong(index++, current);
            statement.setTimestamp(index++, new Timestamp(System.currentTimeMillis()));
            statement.setInt(index++, contextId);
            statement.setInt(index++, userId);
            statement.setString(index++, keyId);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw MultifactorExceptionCodes.SQL_EXCEPTION.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(statement);
            if (connection != null) {
                dbs.backWritable(connection);
            }
        }
    }

    @Override
    public void updateLastUsed(int contextId, int userId, String deviceId) throws OXException {
        PreparedStatement statement = null;
        final DatabaseService dbs = getDatabaseService();
        final Connection connection = dbs.getWritable(contextId);
        try {
            statement = connection.prepareStatement(WebAuthnSql.UPDATE_LAST_USED);
            int index = 1;
            statement.setTimestamp(index++, new Timestamp(System.currentTimeMillis()));
            statement.setInt(index++, contextId);
            statement.setInt(index++, userId);
            statement.setString(index++, deviceId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw MultifactorExceptionCodes.SQL_EXCEPTION.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(statement);
            if (connection != null) {
                dbs.backWritable(connection);
            }
        }

    }

    @Override
    public int getCount(int contextId, int userId) throws OXException {
        return getDevices(contextId, userId).size();
    }

    @Override
    public boolean deleteAllForUser(int contextId, int userId) throws OXException {
        PreparedStatement statement = null;
        final DatabaseService dbs = getDatabaseService();
        final Connection connection = dbs.getWritable(contextId);
        try {
            statement = connection.prepareStatement(WebAuthnSql.DELETE_ALL_FOR_USER);
            int index = 1;
            statement.setInt(index++, contextId);
            statement.setInt(index++, userId);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw MultifactorExceptionCodes.SQL_EXCEPTION.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(statement);
            if (connection != null) {
                dbs.backWritable(connection);
            }
        }
    }

    @Override
    public boolean deleteAllForContext(int contextId) throws OXException {
        PreparedStatement statement = null;
        final DatabaseService dbs = getDatabaseService();
        final Connection connection = dbs.getWritable(contextId);
        try {
            statement = connection.prepareStatement(WebAuthnSql.DELETE_ALL_BY_CONTEXT);
            int index = 1;
            statement.setInt(index++, contextId);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw MultifactorExceptionCodes.SQL_EXCEPTION.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(statement);
            if (connection != null) {
                dbs.backWritable(connection);
            }
        }
    }

}
