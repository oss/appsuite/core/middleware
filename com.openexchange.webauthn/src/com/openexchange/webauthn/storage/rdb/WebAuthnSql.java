/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.webauthn.storage.rdb;


/**
 * {@link WebAuthnSql}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public class WebAuthnSql {

    // Create new device entry
    public static String INSERT_DEVICE = "INSERT INTO web_authn (`id`, `cid`, `deviceId`, `keyId`, `userId`, `name`, `login`, `publicKey`, `attestation`, `clientData`, `counter`, `compromised`, `isMultifactor`, `discoverable`, `lastUsed`, `enabled`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    // Get all devices for user
    public static String GET_DEVICES_FOR_USER = "SELECT * FROM web_authn WHERE `cid` = ? AND `id` = ?";
    // Get device for user with specified deviceId
    public static String GET_DEVICE_FOR_USER = "SELECT * FROM web_authn WHERE `cid` = ? AND `id` = ? and `deviceId` = ?";
    // Remove specific device for user
    public static String REMOVE_DEVICE = "DELETE FROM web_authn WHERE `cid` = ? AND `id` = ? and `deviceId` = ?";
    // Update the signature counter and last used timestamp
    public static String UPDATE_COUNTER_AND_LAST_USED = "UPDATE web_authn SET `counter` = ?, `lastUsed` = ? WHERE `cid` = ? AND `id` = ? and `keyId` = ?";
    // Update the last used timestamp
    public static String UPDATE_LAST_USED = "UPDATE web_authn SET `lastUsed` = ? WHERE `cid` = ? AND `id` = ? and `deviceId` = ?";
    // Update the name of a device
    public static String UPDATE_NAME = "UPDATE web_authn SET `name` = ? WHERE `cid` = ? AND `id` = ? and `deviceId` = ?";
    // Delete all devices for user
    public static String DELETE_ALL_FOR_USER = "DELETE FROM web_authn WHERE `cid` = ? AND `id` = ?";
    // Delete all devices for context
    public static String DELETE_ALL_BY_CONTEXT = "DELETE FROM web_authn WHERE `cid` = ?";

}
