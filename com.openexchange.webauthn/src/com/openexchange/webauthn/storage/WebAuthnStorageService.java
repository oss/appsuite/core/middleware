/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.webauthn.storage;

import java.util.Collection;
import java.util.Optional;
import com.openexchange.exception.OXException;
import com.openexchange.multifactor.MultifactorDevice;
import com.openexchange.webauthn.WebAuthnDevice;

/**
 * Storage service for WebAuthnDevices
 */
public interface WebAuthnStorageService {

    /**
     * Stores a new {@link MultifactorDevice} for the given session
     *
     * @param contextId The context ID to register the device for
     * @param userId The user ID to register the device for
     * @param device The device to store
     * @throws OXException
     */
    public void registerDevice(int contextId, int userId, WebAuthnDevice device) throws OXException;

    /**
     * Removes a device from the storage
     *
     * @param contextId The context ID to remove the device for
     * @param userId The user ID to remove the device for
     * @param deviceId The ID of the device to remove
     * @return <code>true</code>, if the device was removed, <code>false</code> if there was no device with the given ID
     * @throws OXException
     */
    public boolean unregisterDevice(int contextId, int userId, String deviceId) throws OXException;

    /**
     * Gets a collection of devices
     *
     * @param contextId The context ID to get the devices for
     * @param userId The user ID to get the devices for
     * @return A collection of devices for the given session, or an empty Collection
     * @throws OXException
     */
    public Collection<WebAuthnDevice> getDevices(int contextId, int userId) throws OXException;

    /**
     * Gets a specific device
     *
     * @param contextId The context ID to get the device for
     * @param userId The user ID to get the device for
     * @param deviceId The ID of the device to retrieve
     * @return The device with the given ID or an empty Optional
     * @throws OXException
     */
    public Optional<WebAuthnDevice> getDevice(int contextId, int userId, String deviceId) throws OXException;

    /**
     * Store a new name for a device
     *
     * @param contextId The context ID to rename the device for
     * @param userId The user ID to rename the device for
     * @param deviceId The device to be renamed
     * @param name New name for the device
     * @return <code>true</code>, if the device was renamed, <code>false</code> otherwise (not found)
     * @throws OXException
     */
    public boolean renameDevice(int contextId, int userId, String deviceId, String name) throws OXException;

    /**
     * Increment authentication counter and update last used
     *
     * @param contextId The context ID to increment the counter for
     * @param userId The user ID to increment the counter for
     * @param keyId The id of the public key used
     * @param current the current counter value
     * @return <code>true</code> if the counter was successfully incremented, <code>false</code> otherwise
     * @throws OXException
     */
    boolean incrementCounterUpdateLastUsed(int contextId, int userId, String keyId, long current) throws OXException;

    /**
     * Updates last used to current date/time
     *
     * @param contextId The context ID to increment the counter for
     * @param userId The user ID to increment the counter for
     * @param deviceId The id of the device
     * @throws OXException
     */
    void updateLastUsed(int contextId, int userId, String deviceId) throws OXException;

    /**
     * Get count of WebAuthnDevices devices registered to the user
     *
     * @param contextId The context ID to get the count for
     * @param userId The user ID to get the count for
     * @return The count of WebAuthnDevices
     * @throws OXException
     */
    int getCount(int contextId, int userId) throws OXException;

    /**
     * Deletes all WebAuthn registrations for a user
     *
     * @param contextId The id of the context
     * @param userId The id of the user
     * @return <code>true</code> if devices have been deleted, <code>false</code> otherwise
     * @throws OXException
     */
    boolean deleteAllForUser(int contextId, int userId) throws OXException;

    /**
     * Deletes all WebAuthn registrations for a context
     *
     * @param contextId The id of the context
     * @return <code>true</code> if devices have been deleted, <code>false</code> otherwise
     * @throws OXException
     */
    boolean deleteAllForContext(int contextId) throws OXException;

}
