/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.multifactor.provider.u2f.osgi;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.CoreMap;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.java.Streams;
import com.openexchange.multifactor.MultifactorProvider;
import com.openexchange.multifactor.provider.u2f.impl.MultifactorU2FProvider;
import com.openexchange.multifactor.provider.u2f.impl.SignToken;
import com.openexchange.multifactor.provider.u2f.storage.U2FMultifactorDeviceStorage;
import com.openexchange.multifactor.storage.clusterMap.CachedMultifactorToken;
import com.openexchange.multifactor.storage.clusterMap.ClusterMapMultifactorTokenStorage;
import com.openexchange.multifactor.storage.clusterMap.ClusterMapMultifactorTokenStorage.TokenFactory;
import com.openexchange.multifactor.storage.impl.MemoryMultifactorDeviceStorage;
import com.openexchange.multifactor.storage.impl.MemoryMultifactorTokenStorage;
import com.openexchange.osgi.HousekeepingActivator;
import com.yubico.u2f.data.messages.SignRequestData;

/**
 * {@link U2FProviderActivator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class U2FProviderActivator extends HousekeepingActivator {

    static final Logger logger = org.slf4j.LoggerFactory.getLogger(U2FProviderActivator.class);

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] { LeanConfigurationService.class, U2FMultifactorDeviceStorage.class };
    }

    @Override
    protected void startBundle() throws Exception {
        final BundleContext context = super.context;
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());
        final MemoryMultifactorTokenStorage<SignToken> memoryTokenStorage = new MemoryMultifactorTokenStorage<>();
        final MultifactorU2FProvider u2fProvider = new MultifactorU2FProvider(
            getServiceSafe(LeanConfigurationService.class),
            getServiceSafe(U2FMultifactorDeviceStorage.class),
            new MemoryMultifactorDeviceStorage<>(),
            memoryTokenStorage);
        registerService(MultifactorProvider.class, u2fProvider);

        //Switch to ClusterMapService token storage, if ClusterMapService is available
        final ServiceTrackerCustomizer<ClusterMapService, ClusterMapService> clusterMapServiceTracker =
            new ServiceTrackerCustomizer<ClusterMapService, ClusterMapService>() {

                @Override
                public ClusterMapService addingService(ServiceReference<ClusterMapService> reference) {
                    final ClusterMapService clusterMapService = context.getService(reference);
                    TokenFactory<SignToken> tokenFactory = new TokenFactory<SignToken>() {

                        @Override
                        public SignToken create(CachedMultifactorToken cachedToken) {
                            byte[] tokenValueData = cachedToken.getTokenValueData();
                            if (tokenValueData == null) {
                                return new SignToken(null, cachedToken.getLifeTime().orElse(null));
                            }

                            ObjectInputStream ois = null;
                            try {
                                ois = new ObjectInputStream(Streams.newByteArrayInputStream(tokenValueData));
                                SignRequestData requestData = (SignRequestData) ois.readObject();
                                return new SignToken(requestData, cachedToken.getLifeTime().orElse(null));
                            } catch (ClassNotFoundException | IOException e) {
                                logger.error(e.getMessage(), e);
                                return null;
                            } finally {
                                Streams.close(ois);
                            }
                        }

                        @Override
                        public byte[] generateBytesFor(SignToken token) throws OXException {
                            SignRequestData requestData = token.getValue();
                            if (requestData == null) {
                                return null; // NOSONARLINT
                            }

                            ByteArrayOutputStream output = null;
                            ObjectOutputStream objectOutputStream = null;
                            try {
                                output = Streams.newByteArrayOutputStream();
                                objectOutputStream = new ObjectOutputStream(output);
                                objectOutputStream.writeObject(requestData);
                                objectOutputStream.flush();
                                return output.toByteArray();
                            } catch (IOException e) {
                                throw OXException.general("Serialization failed for token: " + token.getClass().getName(), e);
                            } finally {
                                Streams.close(objectOutputStream, output);
                            }
                        }
                    };
                    final ClusterMapMultifactorTokenStorage<SignToken> hzTokenStorage = new ClusterMapMultifactorTokenStorage<>(
                        clusterMapService,
                        CoreMap.MULTIFACTOR_U2F_TOKENS,
                        tokenFactory);
                    u2fProvider.setTokenStorage(hzTokenStorage);
                    return clusterMapService;
                }

                @Override
                public void modifiedService(ServiceReference<ClusterMapService> reference, ClusterMapService service) {
                    //no-op
                }

                @Override
                public void removedService(ServiceReference<ClusterMapService> reference, ClusterMapService service) {
                    //Switch back to the default storage
                    u2fProvider.setTokenStorage(memoryTokenStorage);
                }
        };
        track(ClusterMapService.class, clusterMapServiceTracker);
        openTrackers();
    }

    @Override
    protected void stopBundle() throws Exception {
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        super.stopBundle();
    }
}
