/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.drive.events.internal;

import com.openexchange.config.lean.Property;

/**
 * {@link DriveEventProperty} - Properties for drive events.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public enum DriveEventProperty implements Property {

    CONSOLIDATION_TIME("consolidationTime", Integer.valueOf(1000)),

    MAX_DELAY_TIME("maxDelayTime", Integer.valueOf(10000)),

    DEFAULT_DELAY_TIME("defaultDelayTime", Integer.valueOf(2500)),

    PUBLISHER_DELAY("publisherDelay", Integer.valueOf(2500)),

    ;

    private final String fqn;
    private final Object defaultValue;

    private DriveEventProperty(String appendix, Object defaultValue) {
        this.fqn = "com.openexchange.drive.events." + appendix;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
