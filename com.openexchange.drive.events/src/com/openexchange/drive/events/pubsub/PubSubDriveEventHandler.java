/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.drive.events.pubsub;

import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.drive.events.DriveEvent;
import com.openexchange.drive.events.DriveEventPublisher;
import com.openexchange.drive.events.internal.DriveEventServiceImpl;
import com.openexchange.exception.OXException;
import com.openexchange.java.util.UUIDs;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.ChannelListener;
import com.openexchange.pubsub.Message;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.pubsub.core.CoreChannelApplicationName;
import com.openexchange.pubsub.core.CoreChannelName;
import com.openexchange.server.ServiceExceptionCode;

/**
 * {@link PubSubDriveEventHandler}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public final class PubSubDriveEventHandler implements DriveEventPublisher, ChannelListener<DriveEvent> {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(PubSubDriveEventHandler.class);

    private static final AtomicReference<PubSubService> PUBSUB_REFERENCE = new AtomicReference<>();

    private final DriveEventServiceImpl driveEventService;
    private final String senderId;

    /**
     * Sets the specified {@link PubSubService}.
     *
     * @param service The {@link PubSubService}
     */
    public static void setPubSubService(final PubSubService service) {
        PUBSUB_REFERENCE.set(service);
    }

    /**
     * Initializes a new {@link PubSubDriveEventHandler}.
     *
     * @param driveEventService
     * @throws OXException
     */
    public PubSubDriveEventHandler(DriveEventServiceImpl driveEventService) throws OXException {
        super();
        this.driveEventService = driveEventService;
        driveEventService.registerPublisher(this);
        Channel<DriveEvent> channel = getChannel();
        this.senderId = UUIDs.getUnformattedString(channel.getInstanceId());
        channel.subscribe(this);
    }

    public void stop() {
        driveEventService.unregisterPublisher(this);
        try {
            getChannel().unsubscribe(this);
        } catch (OXException e) {
            LOG.warn("Error removing message listener", e);
        }
    }

    @Override
    public void publish(DriveEvent event) {
        if (null != event && false == event.isRemote()) {
            LOG.debug("publishing drive event: {} [{}]", event, senderId);
            try {
                getChannel().publish(event);
            } catch (OXException e) {
                LOG.warn("Error publishing drive event", e);
            }
        }
    }

    @Override
    public void onMessage(Message<DriveEvent> message) {
        if (null != message && message.isRemote()) {
            DriveEvent driveEvent = message.getData();
            if (driveEvent != null) {
                LOG.debug("onMessage: {} [{}]", driveEvent, message.getSenderId());
                driveEventService.notifyPublishers(driveEvent);
            }
        }
    }

    @Override
    public boolean isLocalOnly() {
        return true;
    }

    private Channel<DriveEvent> getChannel() throws OXException {
        PubSubService pubSubService = PUBSUB_REFERENCE.get();
        if (null == pubSubService) {
            throw ServiceExceptionCode.absentService(PubSubService.class);
        }
        return pubSubService.getChannel(pubSubService.newKey(CoreChannelApplicationName.CORE, CoreChannelName.DRIVE_EVENTS), new DriveEventCodec(pubSubService.getInstanceId()));
    }

}
