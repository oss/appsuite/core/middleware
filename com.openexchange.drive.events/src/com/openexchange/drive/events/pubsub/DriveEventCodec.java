/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.drive.events.pubsub;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONServices;
import com.openexchange.drive.events.DriveContentChange;
import com.openexchange.drive.events.DriveEvent;
import com.openexchange.drive.events.DriveEventImpl;
import com.openexchange.drive.events.internal.DriveContentChangeImpl;
import com.openexchange.file.storage.IdAndName;
import com.openexchange.java.util.UUIDs;
import com.openexchange.pubsub.ChannelMessageCodec;

/**
 * {@link DriveEventCodec} - The codec for channel messages describing a Drive event.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class DriveEventCodec implements ChannelMessageCodec<DriveEvent> {

    private final String localInstanceId;

    /**
     * Initializes a new {@link DriveEventCodec}.
     *
     * @param localInstanceId The local sender identifier
     */
    public DriveEventCodec(UUID localInstanceId) {
        super();
        this.localInstanceId = UUIDs.getUnformattedString(localInstanceId);
    }

    @Override
    public String serialize(DriveEvent driveEvent) throws Exception {
        JSONObject jMessage = new JSONObject(8);
        jMessage.put("instanceId", localInstanceId);
        jMessage.put("contextId", driveEvent.getContextID());
        jMessage.putOpt("pushToken", driveEvent.getPushTokenReference());
        {
            Map<Integer, Set<String>> affectedFoldersPerUser = driveEvent.getAffectedFoldersPerUser();
            if (affectedFoldersPerUser != null) {
                JSONObject jAffectedFoldersPerUser = new JSONObject(affectedFoldersPerUser.size());
                for (Map.Entry<Integer, Set<String>> entry : affectedFoldersPerUser.entrySet()) {
                    Integer userId = entry.getKey();
                    Set<String> folderIds = entry.getValue();
                    JSONArray jFolderIDs = new JSONArray(folderIds.size());
                    for (String key : folderIds) {
                        jFolderIDs.put(key);
                    }
                    jAffectedFoldersPerUser.put(userId.toString(), jFolderIDs);
                }
                jMessage.put("affectedFoldersPerUser", jAffectedFoldersPerUser);
            }
        }
        {
            List<DriveContentChange> contentChanges = driveEvent.getContentChanges();
            if (contentChanges != null) {
                JSONArray jContentChanges = new JSONArray(contentChanges.size());
                for (DriveContentChange contentChange : contentChanges) {
                    jContentChanges.put(jContentChange(contentChange));
                }
                jMessage.put("contentChanges", jContentChanges);
            }
        }
        jMessage.put("contentChangesOnly", driveEvent.isContentChangesOnly());
        return jMessage.toString();
    }

    private static JSONObject jContentChange(DriveContentChange contentChange) throws Exception {
        JSONObject jContentChange = new JSONObject(8);
        jContentChange.putOpt("folderId", contentChange.getFolderId());
        jContentChange.putOpt("pathToRoot", jPathToRoot(contentChange.getPathToRoot()));
        jContentChange.put("myFilesUserId", contentChange.getMyFilesUserId());
        return jContentChange;
    }

    private static JSONArray jPathToRoot(List<IdAndName> pathToRoot) throws Exception {
        JSONArray jPathToRoot = new JSONArray(pathToRoot.size());
        for (IdAndName idAndName : pathToRoot) {
            jPathToRoot.put(new JSONObject(2).putOpt("id", idAndName.getId()).putOpt("name", idAndName.getName()));
        }
        return jPathToRoot;
    }

    @Override
    public DriveEvent deserialize(String data) throws Exception {
        JSONObject jMessage = JSONServices.parseObject(data);
        int contextId = jMessage.optInt("contextId", 0);
        String pushToken = jMessage.optString("pushToken", null);
        Map<Integer, Set<String>> affectedFoldersPerUser = null;
        {
            JSONObject jAffectedFoldersPerUser = jMessage.optJSONObject("affectedFoldersPerUser");
            if (jAffectedFoldersPerUser != null) {
                affectedFoldersPerUser = LinkedHashMap.newLinkedHashMap(jAffectedFoldersPerUser.length());
                for (Map.Entry<String, Object> jEntry : jAffectedFoldersPerUser.entrySet()) {
                    Integer userId = Integer.valueOf(jEntry.getKey());
                    JSONArray jFolderIDs = (JSONArray) jEntry.getValue();
                    Set<String> folderIDs = new LinkedHashSet<String>(jFolderIDs.length());
                    for (Object jFolderId : jFolderIDs) {
                        folderIDs.add(jFolderId.toString());
                    }
                    affectedFoldersPerUser.put(userId, folderIDs);
                }
            }
        }
        List<DriveContentChange> contentChanges = null;
        {
            JSONArray jContentChanges = jMessage.optJSONArray("contentChanges");
            if (jContentChanges != null) {
                contentChanges = new ArrayList<DriveContentChange>(jContentChanges.length());
                for (Object obj : jContentChanges) {
                    JSONObject jContentChange = (JSONObject) obj;
                    contentChanges.add(new DriveContentChangeImpl(jContentChange.optString("folderId", null), pathToRoot(jContentChange.optJSONArray("pathToRoot")), jContentChange.optInt("myFilesUserId", -1)));
                }
            }
        }
        boolean contentChangesOnly = jMessage.optBoolean("contentChangesOnly", false);
        String instanceId = jMessage.getString("instanceId");
        return new DriveEventImpl(contextId, affectedFoldersPerUser, contentChanges, contentChangesOnly, instanceId.equals(localInstanceId) == false, pushToken);
    }

    private static List<IdAndName> pathToRoot(JSONArray jPathToRoot) {
        if (jPathToRoot == null) {
            return null;
        }

        List<IdAndName> pathToRoot = new ArrayList<IdAndName>(jPathToRoot.length());
        for (Object obj : jPathToRoot) {
            JSONObject jPathElem = (JSONObject) obj;
            pathToRoot.add(new IdAndName(jPathElem.optString("id", null), jPathElem.optString("name", null)));
        }
        return pathToRoot;
    }

}
