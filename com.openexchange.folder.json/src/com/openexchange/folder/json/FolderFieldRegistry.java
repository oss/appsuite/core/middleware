/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.folder.json;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReference;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import com.openexchange.folderstorage.FolderField;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.ConcurrentTIntObjectHashMap;


/**
 * {@link FolderFieldRegistry} - A simple registry for folder fields.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class FolderFieldRegistry {

    /**
     * The dummy object.
     */
    private static final Object PRESENT = new Object();

    private static final AtomicReference<FolderFieldRegistry> INSTANCE_REF = new AtomicReference<FolderFieldRegistry>(null);

    /**
     * Gets the registry instance.
     *
     * @return The registry instance
     */
    public static FolderFieldRegistry getInstance() {
        FolderFieldRegistry tmp = INSTANCE_REF.get();
        if (tmp == null) {
            FolderFieldRegistry newInst = new FolderFieldRegistry();
            tmp = INSTANCE_REF.compareAndExchange(null, newInst);
            if (tmp == null) {
                tmp = newInst;
            }
        }
        return tmp;
    }

    /**
     * Releases the registry instance.
     */
    public static void releaseInstance() {
        INSTANCE_REF.set(null);
    }

    /*-
     *
     * ----------------------- MEMBER STUFF --------------------------
     *
     */

    private final ConcurrentMap<FolderField, Object> map;

    private final ConcurrentTIntObjectHashMap<FolderField> numSet;

    private final AtomicReference<ServiceTracker<FolderField, FolderField>> serviceTrackerRef;

    /**
     * Initializes a new {@link FolderFieldRegistry}.
     */
    private FolderFieldRegistry() {
        super();
        map = new ConcurrentHashMap<FolderField, Object>(8, 0.9F, 1);
        numSet = new ConcurrentTIntObjectHashMap<FolderField>(8);
        serviceTrackerRef = new AtomicReference<>();
    }

    /**
     * Starts up this registry.
     *
     * @param context The bundle context
     */
    public void startUp(final BundleContext context) {
        ServiceTracker<FolderField, FolderField> tmp = serviceTrackerRef.get();
        if (null == tmp) {
            ConcurrentMap<FolderField, Object> m = this.map;
            ConcurrentTIntObjectHashMap<FolderField> ns = this.numSet;
            ServiceTracker<FolderField, FolderField> newTracker = new ServiceTracker<FolderField, FolderField>(context, FolderField.class, new ServiceTrackerCustomizer<FolderField, FolderField>() {

                @Override
                public FolderField addingService(final ServiceReference<FolderField> reference) {
                    final FolderField pair = context.getService(reference);
                    if (null == m.putIfAbsent(pair, PRESENT)) {
                        ns.put(pair.getField(), pair);
                        return pair;
                    }
                    context.ungetService(reference);
                    return null;
                }

                @Override
                public void modifiedService(final ServiceReference<FolderField> reference, final FolderField pair) {
                    // Nope
                }

                @Override
                public void removedService(final ServiceReference<FolderField> reference, final FolderField pair) {
                    m.remove(pair);
                    ns.remove(pair.getField());
                    context.ungetService(reference);
                }
            });
            tmp = serviceTrackerRef.compareAndExchange(null, newTracker);
            if (tmp == null) {
                newTracker.open();
            }
        }
    }

    /**
     * Shuts down this registry.
     */
    public void shutDown() {
        ServiceTracker<FolderField, FolderField> tmp = serviceTrackerRef.getAndSet(null);
        if (null != tmp) {
            tmp.close();
        }
    }

    /**
     * Gets the registered pairs.
     *
     * @return The registered pairs
     */
    public Set<FolderField> getPairs() {
        return map.keySet();
    }

    /**
     * Gets the registered fields.
     *
     * @return The registered fields
     */
    public TIntObjectMap<FolderField> getFields() {
        return numSet;
    }

}
