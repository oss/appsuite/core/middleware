/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.folder.json.actions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestDataTools;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.EnqueuableAJAXActionService;
import com.openexchange.ajax.requesthandler.UndoableAJAXActionService;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.ajax.requesthandler.jobqueue.JobKey;
import com.openexchange.exception.Category;
import com.openexchange.exception.OXException;
import com.openexchange.folder.json.Constants;
import com.openexchange.folder.json.FolderField;
import com.openexchange.folder.json.services.ServiceRegistry;
import com.openexchange.folderstorage.FolderExceptionErrorMessage;
import com.openexchange.folderstorage.FolderResponse;
import com.openexchange.folderstorage.FolderService;
import com.openexchange.folderstorage.FolderServiceDecorator;
import com.openexchange.folderstorage.TrashAwareFolderService;
import com.openexchange.folderstorage.TrashResult;
import com.openexchange.folderstorage.UserizedFolder;
import com.openexchange.session.restricted.RestrictedAccessCheck;
import com.openexchange.session.restricted.Scope;
import com.openexchange.tools.RestrictedActionUtil;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.undo.DefaultUndoOperation;
import com.openexchange.undo.UndoOperation;
import com.openexchange.undo.UndoService;
import com.openexchange.undo.UndoToken;

/**
 * {@link DeleteAction} - Maps the action to a DELETE action.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@RestrictedAction(type = RestrictedAction.Type.WRITE, hasCustomRestrictedAccessCheck = true)
public final class DeleteAction extends AbstractFolderAction implements EnqueuableAJAXActionService, UndoableAJAXActionService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(DeleteAction.class);

    private static final String PARAM_HARD_DELETE = "hardDelete";
    private static final String PARAM_FAIL_ON_ERROR = "failOnError";
    public static final String ACTION = AJAXServlet.ACTION_DELETE;
    private static final String NEW_PATH = "new_path";
    private static final String PATH = "path";
    private static final String HAS_FAILED = "hasFailed";
    private static final String IS_TRASHED = "isTrashed";
    private static final String SUPPORTED = "isSuppoprted";
    private static final String EXTENDED_RESPONSE = "extendedResponse";

    /**
     * Initializes a new {@link DeleteAction}.
     */
    public DeleteAction() {
        super();
    }

    @Override
    protected AJAXRequestResult doPerform(final AJAXRequestData request, final ServerSession session) throws OXException, JSONException {
        /*
         * Parse parameters
         */
        String treeId = getEffectiveTreeIdentifier(request);
        final Date timestamp;
        {
            final String timestampStr = request.getParameter("timestamp");
            if (null == timestampStr) {
                timestamp = null;
            } else {
                try {
                    timestamp = new Date(Long.parseLong(timestampStr));
                } catch (NumberFormatException e) {
                    throw AjaxExceptionCodes.INVALID_PARAMETER_VALUE.create("timestamp", timestampStr);
                }
            }
        }
        boolean extendedResponse;
        {
            Boolean bExtendedResponse = request.getParameter(EXTENDED_RESPONSE, boolean.class, true);
            extendedResponse = bExtendedResponse != null ? bExtendedResponse.booleanValue() : false;
        }
        /*
         * Compose JSON array with id
         */
        JSONArray jFolderIds = (JSONArray) request.requireData();
        int len = jFolderIds.length();
        /*
         * Delete
         */
        boolean failOnError = AJAXRequestDataTools.parseBoolParameter(PARAM_FAIL_ON_ERROR, request, false);
        long undoTimeToLive = UndoableAJAXActionService.getUndoTimeToLiveFrom(request);
        boolean prepareUndo = undoTimeToLive > 0;
        final FolderService folderService = ServiceRegistry.getInstance().getService(FolderService.class, true);
        FolderServiceDecorator decorator = getDecorator(request).put(PARAM_HARD_DELETE, request.getParameter(PARAM_HARD_DELETE));
        List<UndoOperation> undoOperations = null;
        final AJAXRequestResult result;
        if (failOnError) {
            Map<String, OXException> foldersWithError = HashMap.newHashMap(len);
            List<TrashResult> trashResults = new ArrayList<>(len);
            for (int i = 0; i < len; i++) {
                String folderId = jFolderIds.getString(i);
                try {
                    FolderResponse<?> response = null;
                    if ((extendedResponse || prepareUndo) && (folderService instanceof TrashAwareFolderService)) {
                        try {
                            // Retrieve current parent in order to undo the operation
                            String parentId = prepareUndo ? folderService.getFolder(treeId, folderId, session, decorator).getParentID() : null;
                            response = ((TrashAwareFolderService) folderService).trashFolder(treeId, folderId, timestamp, session, decorator);
                            if (prepareUndo) {
                                TrashResult trashResult = (TrashResult) response.getResponse();
                                if (trashResult.isTrashed()) {
                                    DefaultUndoOperation.Builder undoOperation = DefaultUndoOperation.builder();
                                    undoOperation.withModule(Constants.getModule());
                                    undoOperation.withAction("update");
                                    Map<String, String> parameters = HashMap.newHashMap(2);
                                    parameters.put(PARAM_TREE, treeId);
                                    parameters.put("id", trashResult.getNewPath());
                                    undoOperation.withParameters(parameters);
                                    JSONObject jFolder = new JSONObject(2);
                                    jFolder.putSafe(FolderField.ID.getName(), trashResult.getNewPath());
                                    jFolder.putSafe(FolderField.FOLDER_ID.getName(), parentId);
                                    undoOperation.withData(jFolder);
                                    if (undoOperations == null) {
                                        undoOperations = new ArrayList<>(len);
                                    }
                                    undoOperations.add(undoOperation.build());
                                }
                            }
                        } catch (OXException e) {
                            if (!e.equalsCode(1041, "FLD")) {
                                throw e;
                            }
                            // else continue with normal operation
                        }
                    }

                    if (response == null) {
                        response = folderService.deleteFolder(treeId, folderId, timestamp, session, decorator);
                        if (extendedResponse) {
                            trashResults.add(TrashResult.createUnsupportedTrashResult());
                        }
                    } else {
                        if (extendedResponse) {
                            trashResults.add((TrashResult) response.getResponse());
                        }
                    }
                    final Collection<OXException> warnings = response.getWarnings();
                    if (null != warnings && !warnings.isEmpty()) {
                        throw warnings.iterator().next();
                    }
                } catch (OXException e) {
                    e.setCategory(Category.CATEGORY_ERROR);
                    LOG.error("Failed to delete folder {} in tree {}.", folderId, treeId, e);
                    foldersWithError.put(folderId, e);
                }
            }
            final int size = foldersWithError.size();
            if (size > 0) {
                if (1 == size) {
                    throw foldersWithError.values().iterator().next();
                }
                final StringBuilder sb = new StringBuilder(64);
                Iterator<String> iterator = foldersWithError.keySet().iterator();
                sb.append(getFolderNameSafe(folderService, iterator.next(), treeId, session));
                while (iterator.hasNext()) {
                    sb.append(", ").append(getFolderNameSafe(folderService, iterator.next(), treeId, session));
                }
                throw FolderExceptionErrorMessage.FOLDER_DELETION_FAILED.create(sb.toString());
            }
            if (extendedResponse) {
                result = createExtendedResponse(trashResults, true);
            } else {
                result = new AJAXRequestResult(JSONArray.EMPTY_ARRAY);
            }
        } else {
            final JSONArray responseArray = new JSONArray();
            final List<OXException> warnings = new LinkedList<>();
            List<TrashResult> trashResults = new ArrayList<>(len);
            for (int i = 0; i < len; i++) {
                final String folderId = jFolderIds.getString(i);
                try {
                    FolderResponse<?> response = null;
                    if ((extendedResponse || prepareUndo) && (folderService instanceof TrashAwareFolderService)) {
                        try {
                            // Retrieve current parent in order to undo the operation
                            String parentId = prepareUndo ? folderService.getFolder(treeId, folderId, session, decorator).getParentID() : null;
                            response = ((TrashAwareFolderService) folderService).trashFolder(treeId, folderId, timestamp, session, decorator);
                            if (prepareUndo) {
                                TrashResult trashResult = (TrashResult) response.getResponse();
                                if (trashResult.isTrashed()) {
                                    DefaultUndoOperation.Builder undoOperation = DefaultUndoOperation.builder();
                                    undoOperation.withModule(Constants.getModule());
                                    undoOperation.withAction("update");
                                    JSONObject jFolder = new JSONObject(2);
                                    jFolder.putSafe(FolderField.ID.getName(), trashResult.getNewPath());
                                    jFolder.putSafe(FolderField.FOLDER_ID.getName(), parentId);
                                    undoOperation.withData(jFolder);
                                    if (undoOperations == null) {
                                        undoOperations = new ArrayList<>(len);
                                    }
                                    undoOperations.add(undoOperation.build());
                                }
                            }
                        } catch (OXException e) {
                            if (!e.equalsCode(1041, "FLD")) {
                                throw e;
                            }
                            // else continue with normal operation
                        }
                    }
                    if (response == null) {
                        response = folderService.deleteFolder(treeId, folderId, timestamp, session, decorator);
                        if (extendedResponse) {
                            trashResults.add(TrashResult.createUnsupportedTrashResult());
                        }
                    } else {
                        if (extendedResponse) {
                            trashResults.add((TrashResult) response.getResponse());
                        }
                    }
                } catch (OXException e) {
                    final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(DeleteAction.class);
                    log.error("Failed to delete folder {} in tree {}.", folderId, treeId, e);
                    e.setCategory(Category.CATEGORY_WARNING);
                    warnings.add(e);
                    responseArray.put(folderId);
                    if (extendedResponse) {
                        trashResults.add(new TrashResult(folderId, true));
                    }
                }
            }
            if (extendedResponse) {
                result = createExtendedResponse(trashResults, false).addWarnings(warnings);
            } else {
                result = new AJAXRequestResult(responseArray).addWarnings(warnings);
            }
        }
        /*
         * Register undo operation(s)
         */
        if (undoOperations != null) {
            UndoService undoService = ServiceRegistry.getInstance().getService(UndoService.class);
            if (undoService != null) {
                UndoToken undoToken = undoService.registerUndoOperations(undoOperations, undoTimeToLive, session);
                result.setUndoTokenIfNotEmpty(undoToken);
            }
        }
        /*
         * Return appropriate result
         */
        return result;
    }

    private static AJAXRequestResult createExtendedResponse(List<TrashResult> results, boolean failOnError) throws JSONException {
        JSONArray resultArray = new JSONArray(results.size());
        if (failOnError) {

            for (TrashResult trashResult : results) {
                if (trashResult.isSupported()) {
                    JSONObject obj = new JSONObject(3);
                    obj.put(SUPPORTED, true);
                    if (trashResult.isTrashed()) {
                        obj.put(IS_TRASHED, true);
                        obj.put(NEW_PATH, trashResult.getNewPath());
                        obj.put(PATH, trashResult.getOldPath());
                    } else {
                        obj.put(IS_TRASHED, false);
                        obj.put(PATH, trashResult.getOldPath());
                    }
                    resultArray.put(obj);
                } else {
                    JSONObject obj = new JSONObject(1);
                    obj.put(SUPPORTED, false);
                    resultArray.put(obj);
                }
            }
        } else {
            for (TrashResult trashResult : results) {
                if (trashResult.isSupported()) {
                    JSONObject obj = new JSONObject(3);
                    obj.put(SUPPORTED, true);
                    if (trashResult.hasFailed()) {
                        obj.put(HAS_FAILED, true);
                        obj.put(PATH, trashResult.getOldPath());
                    } else {
                        if (trashResult.isTrashed()) {
                            obj.put(IS_TRASHED, true);
                            obj.put(PATH, trashResult.getOldPath());
                            obj.put(NEW_PATH, trashResult.getNewPath());
                        } else {
                            obj.put(IS_TRASHED, false);
                            obj.put(PATH, trashResult.getOldPath());
                        }
                    }
                    resultArray.put(obj);
                } else {
                    JSONObject obj = new JSONObject(1);
                    obj.put(SUPPORTED, false);
                    resultArray.put(obj);
                }

            }
        }
        return new AJAXRequestResult(resultArray);
    }

    /**
     * Tries to get the name of a folder, not throwing an exception in case retrieval fails, but falling back to the folder identifier.
     *
     * @param folderService The folder service
     * @param folderId The ID of the folder to get the name for
     * @param treeId The folder tree
     * @param session the session
     * @return The folder name, or the passed folder ID as fallback
     */
    private static String getFolderNameSafe(FolderService folderService, String folderId, String treeId, ServerSession session) {
        try {
            UserizedFolder folder = folderService.getFolder(treeId, folderId, session, null);
            if (null != folder) {
                return folder.getName(session.getUser().getLocale());
            }
        } catch (OXException e) {
            org.slf4j.LoggerFactory.getLogger(DeleteAction.class).debug("Error getting folder name for {}", folderId, e);
        }
        return folderId;
    }

    @RestrictedAccessCheck
    public boolean accessAllowed(final AJAXRequestData request, final ServerSession session, final Scope scope) throws OXException {
        final JSONArray jsonArray = (JSONArray) request.requireData();
        final int len = jsonArray.length();
        String treeId = getEffectiveTreeIdentifier(request);

        final FolderService folderService = ServiceRegistry.getInstance().getService(FolderService.class, true);
        try {
            for (int i = 0; i < len; i++) {
                final String folderId = jsonArray.getString(i);
                UserizedFolder folder = folderService.getFolder(treeId, folderId, session, new FolderServiceDecorator());
                if (!RestrictedActionUtil.mayWriteWithScope(folder.getContentType(), scope)) {
                    return false;
                }
            }

            return true;
        } catch (JSONException e) {
            throw AjaxExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public EnqueuableAJAXActionService.Result isEnqueueable(AJAXRequestData request, ServerSession session) throws OXException {
        String treeId = getEffectiveTreeIdentifier(request);

        Boolean bExtendedResponse = request.getParameter(EXTENDED_RESPONSE, boolean.class, true);
        boolean extendedResponse = bExtendedResponse != null ? bExtendedResponse.booleanValue() : false;

        boolean failOnError = AJAXRequestDataTools.parseBoolParameter(PARAM_FAIL_ON_ERROR, request, false);
        String hardDelete = request.getParameter(PARAM_HARD_DELETE);

        final JSONArray jsonArray = (JSONArray) request.requireData();
        int hash = jsonArray.toString().hashCode();

        try {
            JSONObject jKeyDesc = new JSONObject(8);
            jKeyDesc.put("module", "folder");
            jKeyDesc.put("action", ACTION);
            jKeyDesc.put(PARAM_TREE, treeId);
            jKeyDesc.put(PARAM_FAIL_ON_ERROR, failOnError);
            jKeyDesc.put(EXTENDED_RESPONSE, extendedResponse);
            jKeyDesc.put(PARAM_HARD_DELETE, hardDelete);
            jKeyDesc.put("body", hash);

            return EnqueuableAJAXActionService.resultFor(true, new JobKey(session.getUserId(), session.getContextId(), jKeyDesc.toString()), this);
        } catch (JSONException e) {
            throw AjaxExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public UndoableAJAXActionService.Result isUndoable(AJAXRequestData request, ServerSession session) throws OXException {
        UndoService undoService = ServiceRegistry.getInstance().getService(UndoService.class);
        if (undoService == null) {
            return UndoableAJAXActionService.resultFor(false, this);
        }

        FolderService folderService = ServiceRegistry.getInstance().getService(FolderService.class, true);
        return UndoableAJAXActionService.resultFor((folderService instanceof TrashAwareFolderService), this);
    }

}
