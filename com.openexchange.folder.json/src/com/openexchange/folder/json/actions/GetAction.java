/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.folder.json.actions;

import org.json.JSONObject;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.exception.OXException;
import com.openexchange.folder.json.Constants;
import com.openexchange.folder.json.services.ServiceRegistry;
import com.openexchange.folder.json.writer.FolderWriter;
import com.openexchange.folderstorage.FolderService;
import com.openexchange.folderstorage.UserizedFolder;
import com.openexchange.session.restricted.RestrictedAccessCheck;
import com.openexchange.session.restricted.Scope;
import com.openexchange.tools.RestrictedActionUtil;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link GetAction} - Maps the action to a GET action.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@RestrictedAction(type = RestrictedAction.Type.READ, hasCustomRestrictedAccessCheck = true)
public final class GetAction extends AbstractFolderAction {

    public static final String ACTION = AJAXServlet.ACTION_GET;

    /**
     * Initializes a new {@link GetAction}.
     */
    public GetAction() {
        super();
    }

    @Override
    protected AJAXRequestResult doPerform(final AJAXRequestData request, final ServerSession session) throws OXException {
        /*
         * Parse parameters
         */
        String treeId = getEffectiveTreeIdentifier(request);
        final String folderId = request.getParameter("id");
        if (null == folderId) {
            throw AjaxExceptionCodes.MISSING_PARAMETER.create("id");
        }
        /*
         * Request subfolders from folder service
         */
        final FolderService folderService = ServiceRegistry.getInstance().getService(FolderService.class, true);
        // System.out.println("TOPMOST: " + folderId);

        // @formatter:off
        final UserizedFolder folder =
            folderService.getFolder(
                treeId,
                folderId,
                session,
                getDecorator(request));
        // @formatter:on

        /*
         * Write subfolders as JSON arrays to JSON array
         */
        final JSONObject jsonObject = FolderWriter.writeSingle2Object(request, null, folder, Constants.ADDITIONAL_FOLDER_FIELD_LIST);
        /*
         * Return appropriate result
         */
        return new AJAXRequestResult(jsonObject, folder.getLastModifiedUTC());
    }

    @RestrictedAccessCheck
    public boolean accessAllowed(AJAXRequestData request, ServerSession session, Scope scope) throws OXException {
        String treeId = getEffectiveTreeIdentifier(request);
        String folderId = request.getParameter("id");
        if (null == folderId) {
            throw AjaxExceptionCodes.MISSING_PARAMETER.create("id");
        }
        FolderService folderService = ServiceRegistry.getInstance().getService(FolderService.class, true);
        UserizedFolder folder = folderService.getFolder(treeId, folderId, session, getDecorator(request));
        return RestrictedActionUtil.mayReadWithScope(folder.getContentType(), scope);
    }
}
