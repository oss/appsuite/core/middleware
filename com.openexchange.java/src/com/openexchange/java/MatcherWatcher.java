/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.java;

import java.time.Duration;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import com.openexchange.java.InterruptibleCharSequence.InterruptedRuntimeException;

/**
 * {@link MatcherWatcher} - The watcher for matching a regular expression.
 * <p>
 * This watcher is meant to prevent from possible
 * <a href=""https://owasp.org/www-community/attacks/Regular_expression_Denial_of_Service_-_ReDoS>Regular expression Denial of Service</a>
 * (ReDoS) attacks.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class MatcherWatcher extends AbstractOperationsWatcher<MatchInfo> { // NOSONARLINT

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(MatcherWatcher.class);

    /**
     * The default timeout when applying a regular expression.
     */
    public static final int DEFAULT_REGEX_TIMEOUT = 3_000;

    private static final MatcherWatcher INSTANCE = new MatcherWatcher();

    /**
     * Gets the instance
     *
     * @return The instance
     */
    public static MatcherWatcher getInstance() {
        return INSTANCE;
    }

    /**
     * Checks if given pattern can be found in specified input.
     * <p>
     * In case a timeout occurs, <code>false</code> is returned, but no <code>MatcherException</code> is thrown.
     * Otherwise, see method {@link #find(Pattern, String, int)}.
     *
     * @param pattern The pattern to find
     * @param input The input to look in
     * @param timeoutMillis The timeout in milliseconds when examination is aborted and <code>false</code> is returned
     * @return <code>true</code> if found and no timeout occurred; otherwise <code>false</code> (either not found or a timeout occurred)
     */
    public static boolean findSafe(Pattern pattern, String input, int timeoutMillis) {
        InterruptibleCharSequence interruptible = InterruptibleCharSequence.valueOf(input);
        MatchInfo info = new MatchInfo(Thread.currentThread(), interruptible, timeoutMillis);
        if (!MatcherWatcher.getInstance().add(info)) {
            // Examine w/o watcher
            return pattern.matcher(input).find();
        }

        // Added to watcher
        try {
            return pattern.matcher(interruptible).find();
        } catch (InterruptedRuntimeException e) {
            // Interrupted while searching for match
            LOG.error("Finding pattern occurrence took too long", new MatcherException(pattern, input, e));
        } finally {
            MatcherWatcher.getInstance().remove(info);
        }
        return false;
    }

    /**
     * Checks if given pattern can be found in specified input.
     *
     * @param pattern The pattern to find
     * @param input The input to look in
     * @param timeoutMillis The timeout in milliseconds when examination is aborted and a <code>MatcherException</code> is raised
     * @return <code>true</code> if found; otherwise <code>false</code>
     * @throws MatcherException If pattern couldn't be applied to given input since examination took too long
     */
    public static boolean find(Pattern pattern, String input, int timeoutMillis) throws MatcherException {
        InterruptibleCharSequence interruptible = InterruptibleCharSequence.valueOf(input);
        MatchInfo info = new MatchInfo(Thread.currentThread(), interruptible, timeoutMillis);
        if (!MatcherWatcher.getInstance().add(info)) {
            // Examine w/o watcher
            return pattern.matcher(input).find();
        }

        // Added to watcher
        try {
            return pattern.matcher(interruptible).find();
        } catch (InterruptedRuntimeException e) {
            // Interrupted while searching for match
            throw new MatcherException(pattern, input, e);
        } finally {
            MatcherWatcher.getInstance().remove(info);
        }
    }

    /**
     * Checks if given pattern matches specified input.
     * <p>
     * In case a timeout occurs, <code>false</code> is returned, but no <code>MatcherException</code> is thrown.
     * Otherwise, see method {@link #matches(Pattern, String, int)}.
     *
     * @param pattern The pattern that might match
     * @param input The input to look in
     * @param timeoutMillis The timeout in milliseconds when examination is aborted and <code>false</code> is returned
     * @return <code>true</code> if it matches and no timeout occurred; otherwise <code>false</code> (either not found or a timeout occurred)
     */
    public static boolean matchesSafe(Pattern pattern, String input, int timeoutMillis) {
        InterruptibleCharSequence interruptible = InterruptibleCharSequence.valueOf(input);
        MatchInfo info = new MatchInfo(Thread.currentThread(), interruptible, timeoutMillis);
        if (!MatcherWatcher.getInstance().add(info)) {
            // Examine w/o watcher
            return pattern.matcher(input).matches();
        }

        // Added to watcher
        try {
            return pattern.matcher(interruptible).matches();
        } catch (InterruptedRuntimeException e) {
            // Interrupted while searching for match
            LOG.error("Finding pattern occurrence took too long", new MatcherException(pattern, input, e));
        } finally {
            MatcherWatcher.getInstance().remove(info);
        }
        return false;
    }

    /**
     * Checks if given pattern matches specified input.
     *
     * @param pattern The pattern that might match
     * @param input The input to look in
     * @param timeoutMillis The timeout in milliseconds when examination is aborted and a <code>MatcherException</code> is raised
     * @return <code>true</code> if it matches; otherwise <code>false</code>
     * @throws MatcherException If pattern couldn't be applied to given input since examination took too long
     */
    public static boolean matches(Pattern pattern, String input, int timeoutMillis) throws MatcherException {
        InterruptibleCharSequence interruptible = InterruptibleCharSequence.valueOf(input);
        MatchInfo info = new MatchInfo(Thread.currentThread(), interruptible, timeoutMillis);
        if (!MatcherWatcher.getInstance().add(info)) {
            // Examine w/o watcher
            return pattern.matcher(input).matches();
        }

        // Added to watcher
        try {
            return pattern.matcher(interruptible).matches();
        } catch (InterruptedRuntimeException e) {
            // Interrupted while searching for match
            throw new MatcherException(pattern, input, e);
        } finally {
            MatcherWatcher.getInstance().remove(info);
        }
    }

    // ------------------------------------------------------------------------------------------------------------------------

    /**
     * Initializes a new {@link MatcherWatcher}.
     */
    private MatcherWatcher() {
        super("MatcherWatcher", Duration.ofMinutes(1));
    }

    @Override
    protected MatchInfo getPoisonElement() {
        return MatchInfo.POISON;
    }

    @Override
    protected Logger getLogger() {
        return LOG;
    }

    @Override
    protected void handleExpiredOperation(MatchInfo info) throws Exception {
        info.getProcessingThread().interrupt();
        LOG.info("Interrupted long running regex matcher for {}", info.getInput());
    }

}
