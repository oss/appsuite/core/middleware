/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.java;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.json.JSONArray;
import org.json.JSONCoercion;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * {@link CollectorUtils} - Utility class for {@link Collector}s.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class CollectorUtils {

    /** The default capacity of <code>10</code> */
    private static final int DEFAULT_CAPACITY = 10;

    /**
     * Initializes a new instance of {@link CollectorUtils}.
     */
    private CollectorUtils() {
        super();
    }

    private static final Set<Collector.Characteristics> CH_ID = Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.IDENTITY_FINISH));

    // ---------------------------------------- ArrayList collector ------------------------------------------------------------------------

    /**
     * Returns a {@code Collector} that accumulates the input elements into a new {@code ArrayList} taking over given collection's size as
     * initial capacity of the list.
     * <p>
     * <div style="background-color:#FFDDDD; padding:6px; margin:0px;">
     * <b>Note</b>:<br>
     * This collector is only useful when applied to {@link Collection#stream() a sequential stream} for which the capacity is known.<br>
     * Use common {@link Collectors#toList()} for {@link Collection#parallelStream() a possibly parallel stream}.
     * </div>
     *
     * @param <E> The type of the input elements
     * @param col The collection of which size is taken over as initial capacity of the list
     * @return The {@code Collector} which collects all the input elements into a {@code ArrayList}, in encounter order
     */
    public static <E, X> Collector<E, ?, List<E>> toList(Collection<X> col) {
        return toList(col == null ? DEFAULT_CAPACITY : col.size());
    }

    /**
     * Returns a {@code Collector} that accumulates the input elements into a new {@code ArrayList} accepting an initial capacity of the list.
     * <p>
     * <div style="background-color:#FFDDDD; padding:6px; margin:0px;">
     * <b>Note</b>:<br>
     * This collector is only useful when applied to {@link Collection#stream() a sequential stream} for which the capacity is known.<br>
     * Use common {@link Collectors#toList()} for {@link Collection#parallelStream() a possibly parallel stream}.
     * </div>
     *
     * @param <E> The type of the input elements
     * @param initialCapacity The initial capacity of the list
     * @return The {@code Collector} which collects all the input elements into a {@code ArrayList}, in encounter order
     */
    public static <E> Collector<E, ?, List<E>> toList(int initialCapacity) {
        return new ArrayListCollector<>(initialCapacity);
    }

    private static class ArrayListCollector<E> implements Collector<E, ArrayList<E>, List<E>> {

        private final int initialCapacity;

        /**
         * Initializes a new instance of {@link ArrayListCollector}.
         *
         * @param initialCapacity The initial capacity of the {@code ArrayList}
         */
        ArrayListCollector(int initialCapacity) {
            super();
            this.initialCapacity = initialCapacity;
        }

        @Override
        public Supplier<ArrayList<E>> supplier() {
            return () -> new ArrayList<>(initialCapacity);
        }

        @Override
        public BiConsumer<ArrayList<E>, E> accumulator() {
            return List::add;
        }

        @Override
        public BinaryOperator<ArrayList<E>> combiner() {
            return (left, right) -> {
                left.addAll(right);
                return left;
            };
        }

        @Override
        public Function<ArrayList<E>, List<E>> finisher() {
            return al -> al;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return CH_ID;
        }
    }

    // ---------------------------------------- HashSet collector ------------------------------------------------------------------------

    /**
     * Returns a {@code Collector} that accumulates the input elements into a new {@code HashSet} accepting an initial capacity of the set.
     * <p>
     * <div style="background-color:#FFDDDD; padding:6px; margin:0px;">
     * <b>Note</b>:<br>
     * This collector is only useful when applied to {@link Collection#stream() a sequential stream} for which the capacity is known.<br>
     * Use common {@link Collectors#toSet()} for {@link Collection#parallelStream() a possibly parallel stream}.
     * </div>
     *
     * @param <E> The type of the input elements
     * @param initialCapacity The initial capacity of the set
     * @return The {@code Collector} which collects all the input elements into a {@code HashSet}, in encounter order
     */
    public static <E> Collector<E, ?, Set<E>> toSet(int initialCapacity) {
        return new HashSetCollector<>(initialCapacity);
    }

    private static class HashSetCollector<E> implements Collector<E, HashSet<E>, Set<E>> {

        private final int initialCapacity;

        /**
         * Initializes a new instance of {@link HashSetCollector}.
         *
         * @param initialCapacity The initial capacity of the {@code HashSet}
         */
        HashSetCollector(int initialCapacity) {
            super();
            this.initialCapacity = initialCapacity;
        }

        @Override
        public Supplier<HashSet<E>> supplier() {
            return () -> HashSet.newHashSet(initialCapacity);
        }

        @Override
        public BiConsumer<HashSet<E>, E> accumulator() {
            return Set::add;
        }

        @Override
        public BinaryOperator<HashSet<E>> combiner() {
            return (left, right) -> {
                left.addAll(right);
                return left;
            };
        }

        @Override
        public Function<HashSet<E>, Set<E>> finisher() {
            return al -> al;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return CH_ID;
        }
    }

    // ---------------------------------------- HashMap collector -------------------------------------------------------------------------

    /**
     * Returns a {@code Collector} that accumulates the input elements into a new {@code HashMap} accepting an initial capacity of the map.
     * <p>
     * <div style="background-color:#FFDDDD; padding:6px; margin:0px;">
     * <b>Note</b>:<br>
     * This collector is only useful when applied to {@link Collection#stream() a sequential stream} for which the capacity is known.<br>
     * Use common {@link Collectors#toMap(Function, Function)} for {@link Collection#parallelStream() a possibly parallel stream}.
     * </div>
     *
     * @param <E> The type of the input elements
     * @param <K> The output type of the key mapping function
     * @param <V> The output type of the value mapping function
     * @param initialCapacity The initial capacity of the map
     * @return The collector which collects all the input elements into a {@code HashMap}, in encounter order
     */
    public static <E, K, V> Collector<E, ?, Map<K, V>> toMap(Function<? super E, K> keyMapper, Function<? super E, ? extends V> valueMapper, int initialCapacity) {
        return toMap(keyMapper, valueMapper, initialCapacity, false);
    }

    /**
     * Returns a {@code Collector} that accumulates the input elements into a new {@code HashMap} accepting an initial capacity of the map.
     * <p>
     * <div style="background-color:#FFDDDD; padding:6px; margin:0px;">
     * <b>Note</b>:<br>
     * This collector is only useful when applied to {@link Collection#stream() a sequential stream} for which the capacity is known.<br>
     * Use common {@link Collectors#toMap(Function, Function)} for {@link Collection#parallelStream() a possibly parallel stream}.
     * </div>
     *
     * @param <E> The type of the input elements
     * @param <K> The output type of the key mapping function
     * @param <V> The output type of the value mapping function
     * @param initialCapacity The initial capacity of the map
     * @param useLinkedHashMap <code>true</code> to prefer using a {@link LinkedHashMap}; otherwise <code>false</code> for unordered {@link HashMap}
     * @return The collector which collects all the input elements into a {@code HashMap}, in encounter order
     */
    public static <E, K, V> Collector<E, ?, Map<K, V>> toMap(Function<? super E, K> keyMapper, Function<? super E, ? extends V> valueMapper, int initialCapacity, boolean useLinkedHashMap) {
        return new HashMapCollector<>(keyMapper, valueMapper, initialCapacity, useLinkedHashMap);
    }

    private static class HashMapCollector<E, K, V> implements Collector<E, HashMap<K, V>, Map<K, V>> {

        private final Supplier<HashMap<K, V>> supplier;
        private final Function<? super E, K> keyMapper;
        private final Function<? super E, ? extends V> valueMapper;

        /**
         * Initializes a new instance of {@link JSONObjectCollector}.
         *
         * @param keyMapper The key mapping function
         * @param valueMapper The value mapping function
         * @param initialCapacity The initial capacity of the {@code HashMap}
         * @param useLinkedHashMap <code>true</code> to prefer using a {@link LinkedHashMap}; otherwise <code>false</code> for unordered {@link HashMap}
         */
        HashMapCollector(Function<? super E, K> keyMapper, Function<? super E, ? extends V> valueMapper, int initialCapacity, boolean useLinkedHashMap) {
            super();
            this.supplier = useLinkedHashMap ? () -> LinkedHashMap.newLinkedHashMap(initialCapacity) : () -> HashMap.newHashMap(initialCapacity);
            this.keyMapper = keyMapper;
            this.valueMapper = valueMapper;
        }

        @Override
        public Supplier<HashMap<K, V>> supplier() {
            return supplier;
        }

        @Override
        public BiConsumer<HashMap<K, V>, E> accumulator() {
            return (map, v) -> {
                K key = keyMapper.apply(v);
                V value = valueMapper.apply(v);
                map.put(key, value);
            };
        }

        @Override
        public BinaryOperator<HashMap<K, V>> combiner() {
            return (left, right) -> {
                left.putAll(right);
                return left;
            };
        }

        @Override
        public Function<HashMap<K, V>, Map<K, V>> finisher() {
            return j -> j;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return CH_ID;
        }
    }

    // ---------------------------------------- JSONArray collector -------------------------------------------------------------------------

    /**
     * Returns a {@code Collector} that accumulates the input elements into a new {@code JSONArray}.
     *
     * @param <E> The type of the input elements
     * @return The collector which collects all the input elements into a {@code JSONArray}, in encounter order
     */
    public static <E> Collector<E, JSONArray, JSONArray> toJsonArray() {
        return toJsonArray(false);
    }

    /**
     * Returns a {@code Collector} that accumulates the input elements into a new {@code JSONArray}.
     * <p>
     * <div style="background-color:#FFDDDD; padding:6px; margin:0px;">
     * <b>Note</b>:<br>
     * This collector is only useful when applied to {@link Collection#stream() a sequential stream} for which the capacity is known.<br>
     * Use common {@link CollectorUtils#toJsonArray()} for {@link Collection#parallelStream() a possibly parallel stream}.
     * </div>
     *
     * @param <E> The type of the input elements
     * @param initialCapacity The initial capacity of the JSON array
     * @return The collector which collects all the input elements into a {@code JSONArray}, in encounter order
     */
    public static <E> Collector<E, JSONArray, JSONArray> toJsonArray(int initialCapacity) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Initial capacity must not be less than 0 (zero)");
        }
        return new JSONArrayCollector<>(initialCapacity, false, null);
    }

    /**
     * Returns a {@code Collector} that accumulates the input elements into a new {@code JSONArray}.
     *
     * @param <E> The type of the input elements
     * @param swallowNullElements <code>true</code> to swallow <code>null</code> elements; otherwise put {@link JSONObject#NULL} for them
     * @return The collector which collects all the input elements into a {@code JSONArray}, in encounter order
     */
    @SuppressWarnings("unchecked")
    public static <E> Collector<E, JSONArray, JSONArray> toJsonArray(boolean swallowNullElements) {
        return swallowNullElements ? COLLECTOR_TO_JSON_ARRAY_NO_NULL : COLLECTOR_TO_JSON_ARRAY;
    }

    /**
     * Returns a {@code Collector} that accumulates the input elements into a new {@code JSONArray}.
     *
     * @param <E> The type of the input elements
     * @param swallowNullElements <code>true</code> to swallow <code>null</code> elements; otherwise put {@link JSONObject#NULL} for them
     * @param mapper The mapping function to use to yield the appropriate JSON object for an input element; or <code>null</code> for common JSON coercion
     * @return The collector which collects all the input elements into a {@code JSONArray}, in encounter order
     */
    @SuppressWarnings("unchecked")
    public static <E> Collector<E, JSONArray, JSONArray> toJsonArray(boolean swallowNullElements, Function<E, Object> mapper) {
        if (mapper == null) {
            return swallowNullElements ? COLLECTOR_TO_JSON_ARRAY_NO_NULL : COLLECTOR_TO_JSON_ARRAY;
        }
        return new JSONArrayCollector<>(-1, swallowNullElements, mapper);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static final Collector COLLECTOR_TO_JSON_ARRAY = new JSONArrayCollector(-1, false, null);

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static final Collector COLLECTOR_TO_JSON_ARRAY_NO_NULL = new JSONArrayCollector(-1, true, null);

    private static class JSONArrayCollector<E> implements Collector<E, JSONArray, JSONArray> {

        private static final Supplier<JSONArray> SUPPLIER = JSONArray::new;

        private final Supplier<JSONArray> supplier;
        private final boolean swallowNullElements;
        private final Function<E, Object> optMapper;

        /**
         * Initializes a new instance of {@link CollectorUtils.JSONArrayCollector}.
         *
         * @param capacity The capacity to use when creating a new JSON array or <code>-1</code> to use no explicit capacity
         * @param swallowNullElements <code>true</code> to swallow <code>null</code> elements; otherwise put {@link JSONObject#NULL} for them
         * @param optMapper The mapping function to use to yield the appropriate JSON object for an input element; or <code>null</code> for common JSON coercion
         */
        JSONArrayCollector(int capacity, boolean swallowNullElements, Function<E, Object> optMapper) {
            super();
            this.supplier = capacity < 0 ? SUPPLIER : (() -> new JSONArray(capacity));
            this.swallowNullElements = swallowNullElements;
            this.optMapper = optMapper;
        }

        @Override
        public Supplier<JSONArray> supplier() {
            return supplier;
        }

        @Override
        public BiConsumer<JSONArray, E> accumulator() {
            return (ja, v) -> {
                if (!swallowNullElements || v != null) {
                    if (optMapper == null) {
                        try {
                            ja.put(JSONCoercion.coerceToJSON(v));
                        } catch (JSONException e) {
                            throw new IllegalArgumentException("Value cannot be coerced to JSON: " + v.getClass().getName(), e);
                        }
                    } else {
                        ja.put(optMapper.apply(v));
                    }
                }
            };
        }

        @Override
        public BinaryOperator<JSONArray> combiner() {
            return (left, right) -> {
                left.putAll(right);
                return left;
            };
        }

        @Override
        public Function<JSONArray, JSONArray> finisher() {
            return j -> j;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return CH_ID;
        }
    }

    // ---------------------------------------- JSONObject collector -------------------------------------------------------------------------

    /**
     * Returns a {@code Collector} that accumulates the input elements into a new {@code JSONObject}.
     *
     * @param <E> The type of the input elements
     * @param <V> The output type of the value mapping function
     * @return The collector which collects all the input elements into a {@code JSONObject}, in encounter order
     */
    public static <E, V> Collector<E, JSONObject, JSONObject> toJsonObject(Function<? super E, String> keyMapper, Function<? super E, ? extends V> valueMapper) {
        return new JSONObjectCollector<>(keyMapper, valueMapper);
    }

    private static class JSONObjectCollector<E, V> implements Collector<E, JSONObject, JSONObject> {

        private static final Supplier<JSONObject> SUPPLIER = JSONObject::new;

        private final Function<? super E, String> keyMapper;
        private final Function<? super E, ? extends V> valueMapper;

        /**
         * Initializes a new instance of {@link JSONObjectCollector}.
         */
        JSONObjectCollector(Function<? super E, String> keyMapper, Function<? super E, ? extends V> valueMapper) {
            super();
            this.keyMapper = keyMapper;
            this.valueMapper = valueMapper;
        }

        @Override
        public Supplier<JSONObject> supplier() {
            return SUPPLIER;
        }

        @Override
        public BiConsumer<JSONObject, E> accumulator() {
            return (jo, v) -> {
                try {
                    String key = keyMapper.apply(v);
                    V value = valueMapper.apply(v);
                    jo.put(key, JSONCoercion.coerceToJSON(value));
                } catch (JSONException e) {
                    throw new IllegalArgumentException("Value cannot be coerced to JSON: " + v.getClass().getName(), e);
                }
            };
        }

        @Override
        public BinaryOperator<JSONObject> combiner() {
            return (left, right) -> {
                left.putAll(right);
                return left;
            };
        }

        @Override
        public Function<JSONObject, JSONObject> finisher() {
            return j -> j;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return CH_ID;
        }
    }

}
