/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.java;

import java.util.regex.Pattern;

/**
 * {@link MatcherException} - A matcher cannot examine a certain input in time.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class MatcherException extends Exception {

    private static final long serialVersionUID = -3379579246788699685L;

    private final Pattern pattern;
    private final String input;

    /**
     * Initializes a new {@link MatcherException}.
     *
     * @param pattern The pattern from which the matcher was spawned
     * @param input The input that could not be examined
     */
    public MatcherException(Pattern pattern, String input) {
        super("Pattern \"" + pattern + "\" could not be applied to input");
        this.pattern = pattern;
        this.input = input;
    }

    /**
     * Initializes a new {@link MatcherException}.
     *
     * @param pattern The pattern from which the matcher was spawned
     * @param input The input that could not be examined
     * @apram The cause
     */
    public MatcherException(Pattern pattern, String input, Throwable cause) {
        super("Pattern \"" + pattern + "\" could not be applied to input", cause);
        this.pattern = pattern;
        this.input = input;
    }

    /**
     * Initializes a new {@link MatcherException}.
     *
     * @param pattern The pattern from which the matcher was spawned
     * @param input The input that could not be examined
     * @param message The message
     * @apram The cause
     */
    public MatcherException(Pattern pattern, String input, String message, Throwable cause) {
        super(message, cause);
        this.pattern = pattern;
        this.input = input;
    }

    /**
     * Gets the pattern from which the matcher was spawned.
     *
     * @return The pattern
     */
    public Pattern getPattern() {
        return pattern;
    }

    /**
     * Gets the input that could not be examined.
     *
     * @return The input
     */
    public String getInput() {
        return input;
    }

}
