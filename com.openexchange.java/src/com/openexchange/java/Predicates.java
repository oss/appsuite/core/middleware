/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.java;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * {@link Predicates} - Utility class for predicates.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class Predicates {

    /**
     * Initializes a new {@link Predicates}.
     */
    private Predicates() {
        super();
    }

    /**
     * Gets the predicate checking if input is <code>null</code>.
     *
     * @param <T> The type of the input to the predicate
     * @return The predicate checking for <code>null</code>
     */
    @SuppressWarnings("unchecked")
    public static <T> Predicate<T> isNullPredicate() {
        return PREDICATE_IS_NULL;
    }

    @SuppressWarnings("rawtypes")
    private static final Predicate PREDICATE_IS_NULL = new IsNullPredicate<>();

    private static class IsNullPredicate<T> implements Predicate<T> {

        @Override
        public boolean test(T t) {
            return t == null;
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Gets the predicate checking if input is <b>not</b> <code>null</code>.
     *
     * @param <T> The type of the input to the predicate
     * @return The predicate checking for not-<code>null</code>
     */
    @SuppressWarnings("unchecked")
    public static <T> Predicate<T> isNotNullPredicate() {
        return PREDICATE_IS_NOT_NULL;
    }

    @SuppressWarnings("rawtypes")
    private static final Predicate PREDICATE_IS_NOT_NULL = new IsNotNullPredicate<>();

    private static class IsNotNullPredicate<T> implements Predicate<T> {

        @Override
        public boolean test(T t) {
            return t != null;
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Gets the special predicate accepting all values.
     *
     * @param <T> The type of the input to the predicate
     * @return The all-accepting predicate
     */
    @SuppressWarnings("unchecked")
    public static <T> Predicate<T> acceptAllPredicate() {
        return PREDICATE_ACCEPT_ALL;
    }

    @SuppressWarnings("rawtypes")
    private static final Predicate PREDICATE_ACCEPT_ALL = new AcceptAllPredicate<>();

    private static class AcceptAllPredicate<T> implements Predicate<T> {

        @Override
        public boolean test(T t) {
            return true;
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Yields a predicate to get distinct values from a stream using a custom function.
     *
     * @param <T> The type from which to extract the key
     * @param <K> The type of the key
     * @param keyExtractor The function to extract the key
     * @return The predicate to get distinct values
     * @throws IllegalArgumentException If specified function to extract the key is <code>null</code>
     */
    public static <T, K> Predicate<T> distinctByKey(Function<? super T, K> keyExtractor) {
        if (keyExtractor == null) {
            throw new IllegalArgumentException("The function to extract the key must not be null");
        }
        return new DistinctKeyPredicate<>(keyExtractor);
    }

    private static final class DistinctKeyPredicate<T, K> implements Predicate<T> {

        private final Map<K, Boolean> map;
        private final Function<? super T, K> keyExtractor;

        DistinctKeyPredicate(Function<? super T, K> keyExtractor) {
            super();
            this.map = new ConcurrentHashMap<>();
            this.keyExtractor = keyExtractor;
        }

        @Override
        public boolean test(T t) {
            return map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
        }
    }

}
