/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.java;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * {@link Consumers} - Utility class for {@link Consumer}.
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
public class Consumers {

    /**
     * Initializes a new {@link Consumers}.
     *
     */
    private Consumers() {
        super();
    }

    /**
     * {@link OXConsumer} Represents an exception aware consumer that applies one argument.
     * @param <T> The type to be consumed
     * @param <E> The type of the exception
     */
    @FunctionalInterface
    public interface OXConsumer<T, E extends Exception> {

        /**
         * Performs this operation on the given argument.
         *
         * @param t the input argument
         * @throws E In case of error
         */
        void accept(T t) throws E;

        /**
         * Returns a composed {@code Consumer} that performs, in sequence, this
         * operation followed by the {@code after} operation. If performing either
         * operation throws an exception, it is relayed to the caller of the
         * composed operation. If performing this operation throws an exception,
         * the {@code after} operation will not be performed.
         *
         * @param after the operation to perform after this operation
         * @return a composed {@code Consumer} that performs in sequence this
         *         operation followed by the {@code after} operation
         * @throws NullPointerException if {@code after} is null
         */
        default OXConsumer<T, E> andThen(OXConsumer<? super T, E> after) {
            Objects.requireNonNull(after);
            return (T t) -> {
                accept(t);
                after.accept(t);
            };
        }
    }

}
