/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.datatypes.genericonf.storage;

import static com.openexchange.java.Autoboxing.I;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * @author <a href="mailto:martin.herfurth@open-xchange.org">Martin Herfurth</a>
 */
public class SimConfigurationStorageService implements GenericConfigurationStorageService {

    private static final AtomicInteger CURRENT_ID = new AtomicInteger(0);

    private final TIntObjectMap<Map<String, Object>> entries;

    /**
     * Initializes a new instance of {@link SimConfigurationStorageService}.
     */
    public SimConfigurationStorageService() {
        super();
        entries = new TIntObjectHashMap<Map<String, Object>>();
    }

    @Override
    public void delete(Context ctx, int id) throws OXException {
        entries.remove(id);
    }

    @Override
    public void delete(Connection con, Context ctx, int id) throws OXException {
        entries.remove(id);
    }

    @Override
    public void delete(Connection writeConnection, Context ctx) throws OXException {
        entries.clear();
    }

    @Override
    public void fill(Context ctx, int id, Map<String, Object> content) throws OXException {
        if (!entries.containsKey(id)) {
            return;
        }
        content.putAll(entries.get(id));
    }

    @Override
    public void fill(Connection con, Context ctx, int id, Map<String, Object> content) throws OXException {
        if (!entries.containsKey(id)) {
            return;
        }
        content.putAll(entries.get(id));
    }

    @Override
    public int save(Context ctx, Map<String, Object> content) throws OXException {
        int id = CURRENT_ID.incrementAndGet();
        entries.put(id, content);
        return id;
    }

    @Override
    public int save(Connection con, Context ctx, Map<String, Object> content) throws OXException {
        int id = CURRENT_ID.incrementAndGet();
        entries.put(id, content);
        return id;
    }

    @Override
    public void update(Context ctx, int id, Map<String, Object> content) throws OXException {
        entries.put(id, content);
    }

    @Override
    public void update(Connection con, Context ctx, int id, Map<String, Object> content) throws OXException {
        entries.put(id, content);
    }

    @Override
    public List<Integer> search(Context ctx, Map<String, Object> query) throws OXException {
        return search(query);
    }

    @Override
    public List<Integer> search(Connection con, Context ctx, Map<String, Object> query) throws OXException {
        return search(query);
    }

    private List<Integer> search(Map <String, Object> query) {
        List<Integer> retval = new ArrayList<Integer>();

        entries.forEachEntry((currentId, currentMap) -> {
            if (currentMap.size() != query.size()) {
                return true;
            }

            boolean equal = true;
            for (Iterator<Entry<String, Object>> queryIterator = query.entrySet().iterator(); equal && queryIterator.hasNext();) {
                Map.Entry<String, Object> queryEntry = queryIterator.next();
                Object currentMapObject = currentMap.get(queryEntry.getKey());
                if (currentMapObject == null || !currentMapObject.equals(queryEntry.getValue())) {
                    equal = false;
                }
            }
            if (equal) {
                retval.add(I(currentId));
            }
            return true;
        });

        return retval;
    }

}
