# How-To Use Gradle

**Gradle version: 7.6.4**

To execute a Gradle task, ensure you've configured `gitlabUsername` and `gitlabPassword` in the `gradle.properties` file to be able to fetch the dependencies required for the build from GitLab.
The username should always be set to `Private-Token`, and the password must be a [personal access token](https://gitlab.open-xchange.com/-/user_settings/personal_access_tokens) with the scope `read_api`.

To minimize the risk of accidentally committing the token, it is recommended to set: 
```
git update-index --skip-worktree gradle.properties
```

If there are upstream changes, you need to unset the `skip-worktree` bit for the relevant paths before pulling:

```
git update-index --no-skip-worktree gradle.properties
git stash
git pull
git stash pop //Resolve conflicts, if any
git update-index --skip-worktree gradle.properties
```

After configuration, simply execute `gradle` or the wrapper that corresponds to your operating system. Note that you should run this with a Java 17 JDK. Either set up a `java_home` environment variable or use the `-Dorg.gradle.java.home=` parameter.

E.g.:

    gradlew.bat :http-api:clean :http-api:build -Dorg.gradle.java.home="C:/Program Files/Java/jdk17.0.10"
