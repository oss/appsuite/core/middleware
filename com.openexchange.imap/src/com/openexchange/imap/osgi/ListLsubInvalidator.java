/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imap.osgi;

import java.sql.Connection;
import java.util.Map;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import com.openexchange.exception.OXException;
import com.openexchange.imap.cache.ListLsubCache;
import com.openexchange.mailaccount.MailAccountListener;
import com.openexchange.pubsub.AbstractTrackingChannelListener;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.Message;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.segmenter.sitechange.SiteChangedListener;
import com.openexchange.session.UserAndContext;


/**
 * {@link ListLsubInvalidator}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since 7.6.0
 */
public final class ListLsubInvalidator extends AbstractTrackingChannelListener<UserAndContext> implements MailAccountListener, SiteChangedListener {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(ListLsubInvalidator.class);

    /**
     * Initializes a new {@link ListLsubInvalidator}.
     *
     * @param context The bundle context
     */
    public ListLsubInvalidator(BundleContext context) {
        super(context);
    }

    @Override
    public void onBeforeMailAccountDeletion(int id, Map<String, Object> eventProps, int userId, int contextId, Connection con) throws OXException {
        // Don't care
    }

    @Override
    public void onAfterMailAccountDeletion(int id, Map<String, Object> eventProps, int userId, int contextId, Connection con) throws OXException {
        invalidateFor(userId, contextId);
    }

    @Override
    public void onMailAccountCreated(int id, Map<String, Object> eventProps, int userId, int contextId, Connection con) {
        invalidateFor(userId, contextId);
    }

    @Override
    public void onMailAccountModified(int id, Map<String, Object> eventProps, int userId, int contextId, Connection con) {
        invalidateFor(userId, contextId);
    }

    private static void invalidateFor(int userId, int contextId) {
        ListLsubCache.dropFor(userId, contextId, true, true);
    }

    @Override
    public void onMessage(Message<UserAndContext> message) {
        if (message.isRemote()) {
            // Remotely received
            LOGGER.debug("Handling incoming remote message: {}", message);

            UserAndContext userAndContext = message.getData();
            int userId = userAndContext.getUserId();
            if (userId <= 0) {
                ListLsubCache.dropFor(userAndContext.getContextId(), false, true);
            } else {
                ListLsubCache.dropFor(userId, userAndContext.getContextId(), false, true);
            }
        }
    }

    @Override
    protected Channel<UserAndContext> getChannel(PubSubService service) {
        return ListLsubCache.getChannel(service);
    }

    @Override
    public void onSiteChange() throws OXException {
        ListLsubCache.dropFor(false, true);
    }

}
