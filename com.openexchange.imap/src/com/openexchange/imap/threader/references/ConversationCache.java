/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imap.threader.references;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.java.Charsets;
import com.openexchange.mail.MailFields;
import com.openexchange.mail.MailSortField;
import com.openexchange.mail.OrderDirection;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.session.IUserAndContext;
import com.openexchange.session.Session;
import com.openexchange.session.UserAndContext;

/**
 * {@link ConversationCache}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.0
 */
public final class ConversationCache {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ConversationCache.class);

    private static final ConversationCache INSTANCE = new ConversationCache();

    /**
     * Gets the instance
     *
     * @return The instance
     */
    public static ConversationCache getInstance() {
        return INSTANCE;
    }

    /**
     * Clears the instance.
     */
    public static void clearInstance() {
        INSTANCE.releaseCache();
    }

    /**
     * Gets the calculated hash string for specified arguments.
     *
     * @return The calculated hash string
     */
    public static String getArgsHash(MailSortField sortField, OrderDirection order, int lookAhead, boolean mergeWithSent, MailFields usedFields, String[] headerNames, int total, long uidnext, long highestModSeq, int sentTotal, long sentUidNext, long sentHighestModSeq, boolean preferFolderAssociatedMail) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(sortField.getKey().getBytes(Charsets.UTF_8));
            md.update(order.name().getBytes(Charsets.UTF_8));
            md.update(intToByteArray(lookAhead));
            md.update(mergeWithSent ? (byte) 1 : 0);
            md.update(usedFields.toByteArray());
            if (null != headerNames && headerNames.length > 0) {
                for (String headerName : headerNames) {
                    md.update(Charsets.getBytes(headerName, Charsets.UTF_8));
                }
            }
            md.update(intToByteArray(total));
            md.update(longToByteArray(uidnext));
            md.update(longToByteArray(highestModSeq));
            if (mergeWithSent) {
                md.update(intToByteArray(sentTotal));
                md.update(longToByteArray(sentUidNext));
                md.update(longToByteArray(sentHighestModSeq));
            }
            md.update(preferFolderAssociatedMail ? (byte) 1 : 0);
            return asHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            LOG.error("Failed to generate MD5 for conversation arguments", e);
            int prime = 31;
            int result = 1;
            result = prime * result + ((sortField == null) ? 0 : sortField.getKey().hashCode());
            result = prime * result + ((order == null) ? 0 : order.name().hashCode());
            result = prime * result + lookAhead;
            result = prime * result + (mergeWithSent ? 1231 : 1237);
            result = prime * result + ((usedFields == null) ? 0 : usedFields.hashCode());
            result = prime * result + Arrays.hashCode(headerNames);
            result = prime * result + total;
            result = prime * result + (int) (uidnext ^ (uidnext >>> 32));
            result = prime * result + (int) (highestModSeq ^ (highestModSeq >>> 32));
            if (mergeWithSent) {
                result = prime * result + sentTotal;
                result = prime * result + (int) (sentUidNext ^ (sentUidNext >>> 32));
                result = prime * result + (int) (sentHighestModSeq ^ (sentHighestModSeq >>> 32));
            }
            result = prime * result + (preferFolderAssociatedMail ? 1231 : 1237);
            return Integer.toString(result);
        }
    }


    /**
     * Gets the calculated hash string for specified arguments.
     *
     * @param args The arguments to include in the hash
     * @return The calculated hash string
     */
    public static String getArgsHash(String... args) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            if (null != args) {
                for (String value : args) {
                    md.update(value.getBytes(Charsets.UTF_8));
                }
            }
            return asHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            LOG.error("", e);
            throw new IllegalStateException(e);
        }
    }

    private static final char[] HEX_CHARS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

    /**
     * Turns array of bytes into string representing each byte as unsigned hex number.
     *
     * @param hash Array of bytes to convert to hex-string
     * @return Generated hex string
     */
    private static String asHex(byte[] hash) {
        int length = hash.length;
        char[] buf = new char[length << 1];
        for (int i = 0, x = 0; i < length; i++) {
            buf[x++] = HEX_CHARS[(hash[i] >>> 4) & 0xf];
            buf[x++] = HEX_CHARS[hash[i] & 0xf];
        }
        return new String(buf);
    }

    private static byte[] intToByteArray(int value) {
        return new byte[] { (byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value };
    }

    private static byte[] longToByteArray(long value) {
        return new byte[] { (byte) (value >> 56), (byte) (value >> 48), (byte) (value >> 40), (byte) (value >> 32), (byte) (value >> 24), (byte) (value >> 16), (byte) (value >> 8), (byte) value };
    }

    // ----------------------------------------------------------------------------------------------------------------------------

    private final Cache<UserAndContext, Cache<Integer, Cache<String, CacheEntry>>> cache;

    /**
     * Initializes a new {@link ConversationCache}.
     */
    private ConversationCache() {
        super();
        cache = CacheBuilder.newBuilder().maximumSize(1000000).expireAfterAccess(Duration.ofSeconds(360)).build();
    }

    /**
     * Releases cache reference.
     */
    public void releaseCache() {
        cache.invalidateAll();
    }

    /**
     * Creates a new cache key for given user.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The cache key for specified user
     */
    private static UserAndContext getMapKey(int userId, int contextId) {
        return UserAndContext.newInstance(userId, contextId);
    }

    /**
     * Checks if a cached conversation is contained for given arguments
     *
     * @param fullName The full name
     * @param accountId The account identifier
     * @param session The associated session
     * @return <code>true</code> if contained; otherwise <code>false</code>
     */
    public boolean containsCachedConversations(String fullName, int accountId, IUserAndContext session) {
        UserAndContext mapKey = getMapKey(session.getUserId(), session.getContextId());
        Cache<Integer, Cache<String, CacheEntry>> accounts = cache.getIfPresent(mapKey);
        if (accounts == null) {
            return false;
        }

        Cache<String, CacheEntry> folders = accounts.getIfPresent(Integer.valueOf(accountId));
        if (null == folders) {
            return false;
        }

        return folders.getIfPresent(fullName) != null;
    }

    /**
     * Removes the cached conversations for a user.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     */
    public void removeUserConversations(int userId, int contextId) {
        UserAndContext mapKey = getMapKey(userId, contextId);
        Cache<Integer, Cache<String, CacheEntry>> accounts = cache.getIfPresent(mapKey);
        if (accounts != null) {
            accounts.invalidateAll();
        }
    }

    /**
     * Removes the cached conversations for an account.
     *
     * @param session The associated session
     */
    public void removeAccountConversations(int accountId, IUserAndContext session) {
        UserAndContext mapKey = getMapKey(session.getUserId(), session.getContextId());
        Cache<Integer, Cache<String, CacheEntry>> accounts = cache.getIfPresent(mapKey);
        if (accounts != null) {
            accounts.invalidate(Integer.valueOf(accountId));
        }
    }

    /**
     * Gets the cached conversations
     *
     * @param fullName The full name
     * @param accountId The account identifier
     * @param argsHash The arguments hash
     * @param session The associated session
     * @return The cached conversations or <code>null</code>
     */
    public Optional<List<List<MailMessage>>> getCachedConversations(String fullName, int accountId, String argsHash, IUserAndContext session) {
        UserAndContext mapKey = getMapKey(session.getUserId(), session.getContextId());
        Cache<Integer, Cache<String, CacheEntry>> accounts = cache.getIfPresent(mapKey);
        if (accounts == null) {
            return Optional.empty();
        }

        Cache<String, CacheEntry> cacheEntries = accounts.getIfPresent(Integer.valueOf(accountId));
        if (null == cacheEntries) {
            return Optional.empty();
        }

        CacheEntry cacheEntry = cacheEntries.getIfPresent(fullName);
        if (null == cacheEntry) {
            return Optional.empty();
        }

        if (argsHash.equals(cacheEntry.argsHash)) {
            return Optional.of(cacheEntry.conversations);
        }

        // Arguments' hash does not match. Discard cache entry.
        cacheEntries.asMap().remove(fullName, cacheEntry);
        return Optional.empty();
    }

    /**
     * Puts the conversations into cache.
     *
     * @param conversations The conversations to put
     * @param fullName The full name
     * @param accountId The account identifier
     * @param argsHash The arguments hash
     * @param session The associated session
     */
    public void putCachedConversations(List<List<MailMessage>> conversations, String fullName, int accountId, String argsHash, Session session) {
        UserAndContext mapKey = getMapKey(session.getUserId(), session.getContextId());

        Cache<Integer, Cache<String, CacheEntry>> accounts = cache.getIfPresent(mapKey);
        if (accounts == null) {
            try {
                accounts = cache.get(mapKey, () -> CacheBuilder.newBuilder().initialCapacity(8).maximumSize(50).expireAfterAccess(Duration.ofSeconds(360)).build());
            } catch (ExecutionException e) {
                throw handleExecutionException(e);
            }
        }

        Cache<String, CacheEntry> cacheEntries = accounts.getIfPresent(Integer.valueOf(accountId));
        if (null == cacheEntries) {
            try {
                cacheEntries = accounts.get(Integer.valueOf(accountId), () -> CacheBuilder.newBuilder().initialCapacity(16).maximumSize(100).expireAfterAccess(Duration.ofSeconds(360)).build());
            } catch (ExecutionException e) {
                throw handleExecutionException(e);
            }
        }

        // Put the given one
        cacheEntries.put(fullName, new CacheEntry(conversations, argsHash));
    }

    private static RuntimeException handleExecutionException(ExecutionException e) {
        Throwable cause = e.getCause();
        if (cause == null) {
            return new IllegalStateException(e);
        }
        return cause instanceof RuntimeException ? ((RuntimeException) cause) : new IllegalStateException(cause);
    }

    // ----------------------------------------------------------------------------------------------------------------------------

    private static class CacheEntry {

        final List<List<MailMessage>> conversations;
        final String argsHash;

        CacheEntry(List<List<MailMessage>> conversations, String argsHash) {
            super();
            this.conversations = conversations;
            this.argsHash = argsHash;
        }
    }

}
