/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imap.config;

import static com.openexchange.java.Autoboxing.I;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.jctools.maps.NonBlockingHashMap;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadable;
import com.openexchange.config.Reloadables;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.config.cascade.ConfigViews;
import com.openexchange.exception.OXException;
import com.openexchange.imap.HostExtractingGreetingListener;
import com.openexchange.imap.IMAPProtocol;
import com.openexchange.imap.commandexecutor.AbstractFailsafeCircuitBreakerCommandExecutor;
import com.openexchange.imap.commandexecutor.FailsafeCircuitBreakerCommandExecutor;
import com.openexchange.imap.commandexecutor.GenericFailsafeCircuitBreakerCommandExecutor;
import com.openexchange.imap.commandexecutor.MonitoringCommandExecutor;
import com.openexchange.imap.commandexecutor.PrimaryFailsafeCircuitBreakerCommandExecutor;
import com.openexchange.imap.entity2acl.Entity2ACL;
import com.openexchange.imap.services.Services;
import com.openexchange.java.Autoboxing;
import com.openexchange.java.CharsetDetector;
import com.openexchange.java.Strings;
import com.openexchange.log.LogMessageBuilder;
import com.openexchange.mail.PreviewMode;
import com.openexchange.mail.api.AbstractProtocolProperties;
import com.openexchange.mail.api.IMailProperties;
import com.openexchange.mail.api.MailConfig.BoolCapVal;
import com.openexchange.mail.config.MailConfigException;
import com.openexchange.mail.config.MailProperties;
import com.openexchange.net.HostList;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.session.UserAndContext;
import com.openexchange.spamhandler.SpamHandler;
import com.sun.mail.imap.CommandExecutor;
import com.sun.mail.imap.IMAPStore;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import net.jodah.failsafe.util.Ratio;

/**
 * {@link IMAPProperties}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class IMAPProperties extends AbstractProtocolProperties implements IIMAPProperties {

    private static final long DEFAULT_FOLDER_CACHE_TIMEOUT = com.openexchange.imap.cache.ListLsubCache.DEFAULT_TIMEOUT;

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(IMAPProperties.class);

    private static final IMAPProperties instance = new IMAPProperties();

    /**
     * Gets the singleton instance of {@link IMAPProperties}
     *
     * @return The singleton instance of {@link IMAPProperties}
     */
    public static IMAPProperties getInstance() {
        return instance;
    }

    private static final class PrimaryIMAPProperties {

        private static class Params {

            HostExtractingGreetingListener hostExtractingGreetingListener;
            Boolean rootSubfoldersAllowed;
            boolean namespacePerUser;
            int umlautFilterThreshold;
            int maxMailboxNameLength;
            TIntSet invalidChars;
            boolean allowESORT;
            boolean allowSORTDISPLAY;
            boolean fallbackOnFailedSORT;
            boolean useMultipleAddresses;
            boolean useMultipleAddressesUserHash;
            int useMultipleAddressesMaxRetryAttempts;
            boolean ignoreDeletedMails;
            boolean includeSharedInboxExplicitly;
            boolean assumeUserLocalPartForSharedFolderPath;
            boolean useIMAPEntityDisplayNameIfPossible;

            Params() {
                super();
                useMultipleAddressesMaxRetryAttempts = -1;
                useIMAPEntityDisplayNameIfPossible = true;
                assumeUserLocalPartForSharedFolderPath = false;
            }
        }

        // -------------------------------------------------------------------------------------------------------

        final HostExtractingGreetingListener hostExtractingGreetingListener;
        final Boolean rootSubfoldersAllowed;
        final boolean namespacePerUser;
        final int umlautFilterThreshold;
        final int maxMailboxNameLength;
        final TIntSet invalidChars;
        final boolean allowESORT;
        final boolean allowSORTDISPLAY;
        final boolean fallbackOnFailedSORT;
        final boolean useMultipleAddresses;
        final boolean useMultipleAddressesUserHash;
        final int useMultipleAddressesMaxRetryAttempts;
        final boolean ignoreDeletedMails;
        final boolean includeSharedInboxExplicitly;
        final boolean assumeUserLocalPartForSharedFolderPath;
        final boolean useIMAPEntityDisplayNameIfPossible;

        PrimaryIMAPProperties(Params params) {
            super();
            this.hostExtractingGreetingListener = params.hostExtractingGreetingListener;
            this.rootSubfoldersAllowed = params.rootSubfoldersAllowed;
            this.namespacePerUser = params.namespacePerUser;
            this.umlautFilterThreshold = params.umlautFilterThreshold;
            this.maxMailboxNameLength = params.maxMailboxNameLength;
            this.invalidChars = params.invalidChars;
            this.allowESORT = params.allowESORT;
            this.allowSORTDISPLAY = params.allowSORTDISPLAY;
            this.fallbackOnFailedSORT = params.fallbackOnFailedSORT;
            this.useMultipleAddresses = params.useMultipleAddresses;
            this.useMultipleAddressesUserHash = params.useMultipleAddressesUserHash;
            this.useMultipleAddressesMaxRetryAttempts = params.useMultipleAddressesMaxRetryAttempts;
            this.ignoreDeletedMails = params.ignoreDeletedMails;
            this.includeSharedInboxExplicitly = params.includeSharedInboxExplicitly;
            this.assumeUserLocalPartForSharedFolderPath = params.assumeUserLocalPartForSharedFolderPath;
            this.useIMAPEntityDisplayNameIfPossible = params.useIMAPEntityDisplayNameIfPossible;
        }
    }

    /**
     * Clears the cache.
     */
    public static void invalidateCache() {
        instance.cacheForPrimaryProps.invalidateAll();
    }

    static PrimaryIMAPProperties doGetPrimaryIMAPProps(int userId, int contextId) throws OXException {
        ConfigViewFactory viewFactory = Services.getService(ConfigViewFactory.class);
        if (null == viewFactory) {
            throw ServiceExceptionCode.absentService(ConfigViewFactory.class);
        }

        ConfigView view = viewFactory.getView(userId, contextId);

        PrimaryIMAPProperties.Params params = new PrimaryIMAPProperties.Params();

        LogMessageBuilder logMessageBuilder = LOG.isDebugEnabled() ? LogMessageBuilder.createLogMessageBuilder(1024, 16) : LogMessageBuilder.emptyLogMessageBuilder();

        logMessageBuilder.appendln("Primary IMAP properties successfully loaded for user {} in context {}", Integer.valueOf(userId), Integer.valueOf(contextId));

        { // NOSONARLINT
            String tmp = ConfigViews.getNonEmptyPropertyFrom("com.openexchange.imap.greeting.host.regex", view);
            tmp = Strings.isEmpty(tmp) ? null : tmp;
            if (null != tmp) {
                try {
                    Pattern pattern = Pattern.compile(tmp);
                    params.hostExtractingGreetingListener = new HostExtractingGreetingListener(pattern);

                    logMessageBuilder.appendln("  Host name regular expression: {}", tmp);
                } catch (PatternSyntaxException e) {
                    LOG.warn("Invalid expression for host name", e);
                    logMessageBuilder.appendln("  Host name regular expression: {}", "<none>");
                }
            }
        }

        { // NOSONARLINT
            String tmp = ConfigViews.getNonEmptyPropertyFrom("com.openexchange.imap.rootSubfoldersAllowed", view);
            if (null == tmp) {
                params.rootSubfoldersAllowed = null;
            } else {
                params.rootSubfoldersAllowed = Boolean.valueOf(tmp);

                logMessageBuilder.appendln("  Root sub-folders allowed: {}", params.rootSubfoldersAllowed);
            }
        }

        { // NOSONARLINT
            params.namespacePerUser = ConfigViews.getDefinedBoolPropertyFrom("com.openexchange.imap.namespacePerUser", false, view);

            logMessageBuilder.appendln("  Namespace per User: {}", Autoboxing.valueOf(params.namespacePerUser));
        }

        { // NOSONARLINT
            params.umlautFilterThreshold = ConfigViews.getDefinedIntPropertyFrom("com.openexchange.imap.umlautFilterThreshold", 50, view);

            logMessageBuilder.appendln("  Umlaut filter threshold: {}", Autoboxing.valueOf(params.umlautFilterThreshold));
        }

        { // NOSONARLINT
            params.maxMailboxNameLength = ConfigViews.getDefinedIntPropertyFrom("com.openexchange.imap.maxMailboxNameLength", 60, view);

            logMessageBuilder.appendln("  Max. Mailbox Name Length: {}", Autoboxing.valueOf(params.maxMailboxNameLength));
        }

        { // NOSONARLINT
            String invalids = ConfigViews.getNonEmptyPropertyFrom("com.openexchange.imap.invalidMailboxNameCharacters", view);
            if (Strings.isEmpty(invalids)) {
                params.invalidChars = new TIntHashSet(0);
            } else {
                final String[] sa = Strings.splitByWhitespaces(Strings.unquote(invalids));
                final int length = sa.length;
                TIntSet invalidChars = new TIntHashSet(length);
                for (int i = 0; i < length; i++) {
                    invalidChars.add(sa[i].charAt(0));
                }

                params.invalidChars = invalidChars;

                logMessageBuilder.appendln("  Invalid Mailbox Characters: {}", invalids);
            }

        }

        { // NOSONARLINT
            params.allowESORT = ConfigViews.getDefinedBoolPropertyFrom("com.openexchange.imap.allowESORT", true, view);

            logMessageBuilder.appendln("  Allow ESORT: {}", Autoboxing.valueOf(params.allowESORT));
        }

        { // NOSONARLINT
            params.allowSORTDISPLAY = ConfigViews.getDefinedBoolPropertyFrom("com.openexchange.imap.allowSORTDISPLAY", false, view);

            logMessageBuilder.appendln("  Allow SORT-DSIPLAY: {}", Autoboxing.valueOf(params.allowSORTDISPLAY));
        }

        { // NOSONARLINT
            params.fallbackOnFailedSORT = ConfigViews.getDefinedBoolPropertyFrom("com.openexchange.imap.fallbackOnFailedSORT", false, view);

            logMessageBuilder.appendln("  Fallback On Failed SORT: {}", Autoboxing.valueOf(params.fallbackOnFailedSORT));
        }

        { // NOSONARLINT
            params.useMultipleAddresses = ConfigViews.getDefinedBoolPropertyFrom("com.openexchange.imap.useMultipleAddresses", false, view);

            logMessageBuilder.appendln("  Use Multiple IP addresses: {}", Autoboxing.valueOf(params.useMultipleAddresses));
        }

        if (params.useMultipleAddresses) {
            params.useMultipleAddressesUserHash = ConfigViews.getDefinedBoolPropertyFrom("com.openexchange.imap.useMultipleAddressesUserHash", false, view);

            logMessageBuilder.appendln("  Use User Hash for Multiple IP addresses: {}", Autoboxing.valueOf(params.useMultipleAddressesUserHash));
        }

        if (params.useMultipleAddresses) {
            params.useMultipleAddressesMaxRetryAttempts = ConfigViews.getDefinedIntPropertyFrom("com.openexchange.imap.useMultipleAddressesMaxRetries", 3, view);

            logMessageBuilder.appendln("  Use max. retry attempts for Multiple IP addresses: {}", Autoboxing.valueOf(params.useMultipleAddressesMaxRetryAttempts));
        }

        { // NOSONARLINT
            params.ignoreDeletedMails = ConfigViews.getDefinedBoolPropertyFrom("com.openexchange.imap.ignoreDeleted", false, view);

            logMessageBuilder.appendln("  Ignore deleted mails: {}", Autoboxing.valueOf(params.ignoreDeletedMails));
        }

        { // NOSONARLINT
            params.includeSharedInboxExplicitly = ConfigViews.getDefinedBoolPropertyFrom("com.openexchange.imap.includeSharedInboxExplicitly", false, view);

            logMessageBuilder.appendln("  Include shared inbox explicitly: {}", Autoboxing.valueOf(params.includeSharedInboxExplicitly));
        }

        { // NOSONARLINT
            params.assumeUserLocalPartForSharedFolderPath = ConfigViews.getDefinedBoolPropertyFrom("com.openexchange.imap.assumeUserLocalPartForSharedFolderPath", false, view);

            logMessageBuilder.appendln("  Assume user's local part for a shared folder path: {}", Autoboxing.valueOf(params.assumeUserLocalPartForSharedFolderPath));
        }

        { // NOSONARLINT
            params.useIMAPEntityDisplayNameIfPossible = ConfigViews.getDefinedBoolPropertyFrom("com.openexchange.imap.useIMAPEntityDisplayNameIfPossible", true, view);

            logMessageBuilder.appendln("  Use IMAP entity's display name (if possible): {}", Autoboxing.valueOf(params.useIMAPEntityDisplayNameIfPossible));
        }

        PrimaryIMAPProperties primaryIMAPProps = new PrimaryIMAPProperties(params);
        LOG.debug(logMessageBuilder.getMessage(), logMessageBuilder.getArgumentsAsArray());
        return primaryIMAPProps;
    }

    // --------------------------------------------------------------------------------------------------------------

    /*-
     * Fields for global properties
     */

    private final IMailProperties mailProperties;

    private boolean imapSort;

    private boolean imapSearch;

    private boolean forceImapSearch;

    private boolean fastFetch;

    private BoolCapVal supportsACLs;

    private int imapReadTimeout;

    private int imapConnectTimeout;

    private int imapFilterReadTimeout;

    private int readResponsesTimeout;

    private int imapTemporaryDown;

    private int imapFailedAuthTimeout;

    private String imapAuthEnc;

    private String entity2AclImpl;

    private int blockSize;

    private int maxNumConnection;

    private final Map<String, Boolean> newACLExtMap;

    private String spamHandlerName;

    private boolean propagateClientIPAddress;

    private boolean enableTls;

    private boolean requireTls;

    private boolean auditLogEnabled;

    private boolean debugLogEnabled;

    private String debugServerPattern;

    private boolean overwritePreLoginCapabilities;

    private boolean retryLoginOnUnavailableResponseCode;

    private Set<String> propagateHostNames;

    private boolean allowFolderCaches;

    private long folderCacheTimeoutMillis;

    private boolean allowFetchSingleHeaders;

    private String sContainerType;

    private String sslProtocols;

    private String cipherSuites;

    private HostExtractingGreetingListener hostExtractingGreetingListener;

    private boolean enableAttachmentSearch;

    private List<CommandExecutor> commandExecutors;

    private PreviewMode preferredPreviewMode;

    private boolean useUTF7ForUserFlags;

    private final Cache<UserAndContext, PrimaryIMAPProperties> cacheForPrimaryProps;

    /**
     * Initializes a new {@link IMAPProperties}
     */
    private IMAPProperties() {
        super();
        sContainerType = "boundary-aware";
        enableTls = true;
        requireTls = true;
        auditLogEnabled = false;
        debugLogEnabled = false;
        debugServerPattern = null;
        overwritePreLoginCapabilities = false;
        retryLoginOnUnavailableResponseCode = false;
        maxNumConnection = -1;
        newACLExtMap = new NonBlockingHashMap<String, Boolean>();
        mailProperties = MailProperties.getInstance();
        propagateHostNames = Collections.emptySet();
        allowFetchSingleHeaders = true;
        allowFolderCaches = true;
        folderCacheTimeoutMillis = DEFAULT_FOLDER_CACHE_TIMEOUT;
        hostExtractingGreetingListener = null;
        enableAttachmentSearch = false;
        commandExecutors = null;
        preferredPreviewMode = null;
        useUTF7ForUserFlags = false;
        cacheForPrimaryProps = CacheBuilder.newBuilder().maximumSize(65536).expireAfterAccess(30, TimeUnit.MINUTES).build();
    }

    /**
     * Gets the properties for given user's primary IMAP account.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The properties for primary IMAP account
     * @throws OXException If properties for primary IMAP account cannot be determined
     */
    private PrimaryIMAPProperties getPrimaryIMAPProps(int userId, int contextId) throws OXException {
        UserAndContext key = UserAndContext.newInstance(userId, contextId);
        PrimaryIMAPProperties primaryMailProps = cacheForPrimaryProps.getIfPresent(key);
        if (null != primaryMailProps) {
            return primaryMailProps;
        }

        try {
            return cacheForPrimaryProps.get(key, () -> doGetPrimaryIMAPProps(userId, contextId));
        } catch (ExecutionException e) { // NOSONARLINT
            Throwable cause = e.getCause();
            throw cause instanceof OXException ? (OXException) cause : new OXException(cause);
        }
    }

    @Override
    protected void loadProperties0() throws OXException {
        LogMessageBuilder logBuilder = LOG.isInfoEnabled() ? LogMessageBuilder.createLogMessageBuilder(1024, 32) : LogMessageBuilder.emptyLogMessageBuilder();

        logBuilder.lfappendln("Loading global IMAP properties...");

        final ConfigurationService configuration = Services.getService(ConfigurationService.class);
        { // NOSONARLINT
            final String allowFolderCachesStr = configuration.getProperty("com.openexchange.imap.allowFolderCaches", "true").trim();
            allowFolderCaches = "true".equalsIgnoreCase(allowFolderCachesStr);
            logBuilder.appendln("    IMAP allow folder caches: {}", Boolean.valueOf(allowFolderCaches));
        }

        { // NOSONARLINT
            String str = configuration.getProperty("com.openexchange.imap.folderCacheTimeoutMillis", Long.toString(DEFAULT_FOLDER_CACHE_TIMEOUT)).trim();
            folderCacheTimeoutMillis = parsePositiveInt(str);
            if (folderCacheTimeoutMillis < 0) {
                folderCacheTimeoutMillis = DEFAULT_FOLDER_CACHE_TIMEOUT;
                logBuilder.appendln("    Folder Cache Timeout: Invalid value \"{}\". Setting to fallback: {}", str, Long.valueOf(folderCacheTimeoutMillis));
            } else {
                logBuilder.appendln("    Folder Cache Timeout: {}", Long.valueOf(folderCacheTimeoutMillis));
            }
        }

        { // NOSONARLINT
            final String str = configuration.getProperty("com.openexchange.imap.allowFetchSingleHeaders", "true").trim();
            allowFetchSingleHeaders = "true".equalsIgnoreCase(str);
            logBuilder.appendln("    IMAP allow FETCH single headers: {}", Boolean.valueOf(allowFetchSingleHeaders));
        }

        { // NOSONARLINT
            final String imapSortStr = configuration.getProperty("com.openexchange.imap.imapSort", "application").trim();
            imapSort = "imap".equalsIgnoreCase(imapSortStr);
            logBuilder.appendln("    IMAP-Sort: {}", Boolean.valueOf(imapSort));
        }

        { // NOSONARLINT
            final String imapSearchStr = configuration.getProperty("com.openexchange.imap.imapSearch", "force-imap").trim();
            forceImapSearch = "force-imap".equalsIgnoreCase(imapSearchStr);
            imapSearch = forceImapSearch || "imap".equalsIgnoreCase(imapSearchStr);
            logBuilder.appendln("    IMAP-Search: {}{}", Boolean.valueOf(imapSearch), forceImapSearch ? " (forced)" : "");

        }

        { // NOSONARLINT
            final String fastFetchStr = configuration.getProperty("com.openexchange.imap.imapFastFetch", STR_TRUE).trim();
            fastFetch = Boolean.parseBoolean(fastFetchStr);
            logBuilder.appendln("    Fast Fetch Enabled: {}", Boolean.valueOf(fastFetch));
        }

        { // NOSONARLINT
            final String tmp = configuration.getProperty("com.openexchange.imap.propagateClientIPAddress", STR_FALSE).trim();
            propagateClientIPAddress = Boolean.parseBoolean(tmp);
            logBuilder.appendln("    Propagate Client IP Address: {}", Boolean.valueOf(propagateClientIPAddress));
        }

        { // NOSONARLINT
            final String tmp = configuration.getProperty("com.openexchange.imap.enableTls", STR_TRUE).trim();
            enableTls = Boolean.parseBoolean(tmp);
            logBuilder.appendln("    Enable TLS: {}", Boolean.valueOf(enableTls));
        }

        { // NOSONARLINT
            final String tmp = configuration.getProperty("com.openexchange.imap.requireTls", STR_TRUE).trim();
            requireTls = Boolean.parseBoolean(tmp);
            logBuilder.appendln("    Require TLS: {}", Boolean.valueOf(requireTls));
        }

        { // NOSONARLINT
            String tmp = configuration.getProperty("com.openexchange.imap.auditLog.enabled", STR_FALSE).trim();
            auditLogEnabled = Boolean.parseBoolean(tmp);
            logBuilder.appendln("    Audit Log Enabled: {}", Boolean.valueOf(auditLogEnabled));
        }

        { // NOSONARLINT
            String tmp = configuration.getProperty("com.openexchange.imap.debugLog.enabled", STR_FALSE).trim();
            debugLogEnabled = Boolean.parseBoolean(tmp);
            logBuilder.appendln("    Debug Log Enabled: {}", Boolean.valueOf(debugLogEnabled));
        }

        { // NOSONARLINT
            String tmp = configuration.getProperty("com.openexchange.imap.debugLog.serverPattern", "").trim();
            if (Strings.isNotEmpty(tmp)) {
                debugServerPattern = tmp;
                logBuilder.appendln("    Debug Log Server Pattern: {}", debugServerPattern);
            }
        }

        { // NOSONARLINT
            String tmp = configuration.getProperty("com.openexchange.imap.overwritePreLoginCapabilities", STR_FALSE).trim();
            overwritePreLoginCapabilities = Boolean.parseBoolean(tmp);
            logBuilder.appendln("    Overwrite Pre-Login Capabilities: {}", Boolean.valueOf(overwritePreLoginCapabilities));
        }

        { // NOSONARLINT
            String tmp = configuration.getProperty("com.openexchange.imap.retryLoginOnUnavailableResponseCode", STR_FALSE).trim();
            retryLoginOnUnavailableResponseCode = Boolean.parseBoolean(tmp);
            logBuilder.appendln("    Retry Login On UNAVAILABLE Response Code: {}", Boolean.valueOf(retryLoginOnUnavailableResponseCode));
        }

        { // NOSONARLINT
            final String tmp = configuration.getProperty("com.openexchange.imap.propagateHostNames", "").trim();
            if (!tmp.isEmpty()) {
                propagateHostNames = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(Strings.splitByComma(tmp))));
            } else {
                propagateHostNames = Collections.emptySet();
            }
            logBuilder.appendln("    Propagate Host Names: {}", propagateHostNames.isEmpty() ? "<none>" : propagateHostNames.toString());
        }

        { // NOSONARLINT
            final String supportsACLsStr = configuration.getProperty("com.openexchange.imap.imapSupportsACL", "auto").trim();
            supportsACLs = BoolCapVal.parseBoolCapVal(supportsACLsStr);
            logBuilder.appendln("    Support ACLs: {}", supportsACLs);
        }

        { // NOSONARLINT
            final String imapTimeoutStr = configuration.getProperty("com.openexchange.imap.imapTimeout", "50000").trim();
            imapReadTimeout = parsePositiveInt(imapTimeoutStr);
            if (imapReadTimeout < 0) {
                imapReadTimeout = 50000;
                logBuilder.appendln("    IMAP Read Timeout: Invalid value \"{}\". Setting to fallback: {}", imapTimeoutStr, Integer.valueOf(imapReadTimeout));
            } else {
                logBuilder.appendln("    IMAP Read Timeout: {}", Integer.valueOf(imapReadTimeout));
            }
        }

        { // NOSONARLINT
            final String imapConTimeoutStr = configuration.getProperty("com.openexchange.imap.imapConnectionTimeout", "20000").trim();
            imapConnectTimeout = parsePositiveInt(imapConTimeoutStr);
            if (imapConnectTimeout < 0) {
                imapConnectTimeout = 20000;
                logBuilder.appendln("    IMAP Connect Timeout: Invalid value \"{}\". Setting to fallback: {}", imapConTimeoutStr, Integer.valueOf(imapConnectTimeout));
            } else {
                logBuilder.appendln("    IMAP Connect Timeout: {}", Integer.valueOf(imapConnectTimeout));
            }
        }

        { // NOSONARLINT
            String imapFilterReadTimeoutStr = configuration.getProperty("com.openexchange.imap.filterCommandTimeout", "-1").trim();
            try {
                int tmp = Integer.parseInt(imapFilterReadTimeoutStr);
                if (tmp < 0) {
                    tmp = -1;
                }
                imapFilterReadTimeout = tmp;
                logBuilder.appendln("    IMAP Filter Read Timeout: {}", Integer.valueOf(imapFilterReadTimeout));
            } catch (NumberFormatException x) {
                imapFilterReadTimeout = imapReadTimeout;
                logBuilder.appendln("    IMAP Filter Read Timeout: Invalid value \"{}\". Setting to IMAP Read Timeout: {}", imapFilterReadTimeoutStr, Integer.valueOf(imapFilterReadTimeout));
            }
        }

        { // NOSONARLINT
            String readResponsesTimeoutStr = configuration.getProperty("com.openexchange.imap.readResponsesTimeout", "60000").trim();
            readResponsesTimeout = parsePositiveInt(readResponsesTimeoutStr);
            if (readResponsesTimeout < 0) {
                readResponsesTimeout = 60_000;
                logBuilder.appendln("    IMAP Read Responses Timeout: Invalid value \"{}\". Setting to fallback: {}", readResponsesTimeoutStr, Integer.valueOf(readResponsesTimeout));
            } else {
                logBuilder.appendln("    IMAP Read Responses Timeout: {}", Integer.valueOf(readResponsesTimeout));
                // Assume "-1" if set to "0" (zero)
                readResponsesTimeout = readResponsesTimeout == 0 ? -1 : readResponsesTimeout;
            }
        }

        { // NOSONARLINT
            final String imapTempDownStr = configuration.getProperty("com.openexchange.imap.imapTemporaryDown", "10000").trim();
            imapTemporaryDown = parsePositiveInt(imapTempDownStr);
            if (imapTemporaryDown < 0) {
                imapTemporaryDown = 10000;
                logBuilder.appendln("    IMAP Temporary Down: Invalid value \"{}\". Setting to fallback: {}", imapTempDownStr, Integer.valueOf(imapTemporaryDown));
            } else {
                logBuilder.appendln("    IMAP Temporary Down: {}", Integer.valueOf(imapTemporaryDown));
            }
        }

        { // NOSONARLINT
            final String imapFailedAuthTimeoutStr = configuration.getProperty("com.openexchange.imap.failedAuthTimeout", "10000").trim();
            imapFailedAuthTimeout = parsePositiveInt(imapFailedAuthTimeoutStr);
            if (imapFailedAuthTimeout < 0) {
                imapFailedAuthTimeout = 10000;
                logBuilder.appendln("    IMAP Failed Auth Timeout: Invalid value \"{}\". Setting to fallback: {}", imapFailedAuthTimeoutStr, Integer.valueOf(imapFailedAuthTimeout));
            } else {
                logBuilder.appendln("    IMAP Failed Auth Timeout: {}", Integer.valueOf(imapFailedAuthTimeout));
            }
        }

        { // NOSONARLINT
            final String imapAuthEncStr = configuration.getProperty("com.openexchange.imap.imapAuthEnc", "UTF-8").trim();
            if (CharsetDetector.isValid(imapAuthEncStr)) {
                imapAuthEnc = imapAuthEncStr;
                logBuilder.appendln("    Authentication Encoding: {}", imapAuthEnc);
            } else {
                imapAuthEnc = "UTF-8";
                logBuilder.appendln("    Authentication Encoding: Unsupported charset \"{}\". Setting to fallback: {}", imapAuthEncStr, imapAuthEnc);
            }
        }

        { // NOSONARLINT
            entity2AclImpl = configuration.getProperty("com.openexchange.imap.User2ACLImpl");
            if (null == entity2AclImpl) {
                throw MailConfigException.create("Missing IMAP property \"com.openexchange.imap.User2ACLImpl\"");
            }
            entity2AclImpl = entity2AclImpl.trim();
        }

        { // NOSONARLINT
            final String blockSizeStr = configuration.getProperty("com.openexchange.imap.blockSize", "1000").trim();
            blockSize = parsePositiveInt(blockSizeStr);
            if (blockSize < 0) {
                blockSize = 1000;
                logBuilder.appendln("    Block Size: Invalid value \"{}\". Setting to fallback: {}", blockSizeStr, Integer.valueOf(blockSize));
            } else {
                logBuilder.appendln("    Block Size: {}", Integer.valueOf(blockSize));
            }
        }

        { // NOSONARLINT
            String tmp = configuration.getProperty("com.openexchange.imap.maxNumExternalConnections");
            if (null != tmp) {
                tmp = tmp.trim();
                if (0 == tmp.length()) {
                    IMAPProtocol.getInstance().setOverallExternalMaxCount(-1);
                    logBuilder.appendln("    Max. Number of External Connections: No restrictions");
                } else if (tmp.indexOf(':') > 0) {
                    // Expect a comma-separated list
                    final String[] sa = Strings.splitByComma(tmp);
                    if (sa.length > 0) {
                        try {
                            final IMAPProtocol imapProtocol = IMAPProtocol.getInstance();
                            imapProtocol.initExtMaxCountMap();
                            logBuilder.append("    Max. Number of External Connections: ");
                            boolean first = true;
                            for (String desc : sa) {
                                final int pos = desc.indexOf(':');
                                if (pos > 0) {
                                    try {
                                        imapProtocol.putIfAbsent(desc.substring(0, pos), Integer.parseInt(desc.substring(pos + 1).trim()));
                                        if (first) {
                                            first = false;
                                        } else {
                                            logBuilder.append(", ");
                                        }
                                        logBuilder.append("{}", desc);
                                    } catch (RuntimeException e) {
                                        LOG.warn("Max. Number of External Connections: Invalid entry: {}", desc, e);
                                    }
                                }
                            }
                            logBuilder.appendln("");
                        } catch (@SuppressWarnings("unused") final Exception e) {
                            IMAPProtocol.getInstance().setOverallExternalMaxCount(-1);
                            logBuilder.appendln("    Max. Number of External Connections: Invalid value \"{}\". Setting to fallback: No restrictions", tmp);
                        }
                    }
                } else {
                    // Expect a single integer value
                    int maxCount = parsePositiveInt(tmp);
                    if (maxCount < 0) {
                        IMAPProtocol.getInstance().setOverallExternalMaxCount(-1);
                        logBuilder.appendln("    Max. Number of External Connections: Invalid value \"{}\". Setting to fallback: No restrictions", tmp);
                    } else {
                        IMAPProtocol.getInstance().setOverallExternalMaxCount(maxCount);
                        logBuilder.appendln("    Max. Number of External Connections: {} (applied to all external IMAP accounts)", tmp);
                    }
                }
            }
        }

        { // NOSONARLINT
            final String tmp = configuration.getProperty("com.openexchange.imap.maxNumConnections", "-1").trim();
            maxNumConnection = parsePositiveInt(tmp);
            if (maxNumConnection < 0) {
                maxNumConnection = -1;
                logBuilder.appendln("    Max. Number of connections: Invalid value \"{}\". Setting to fallback: {}", tmp, Integer.valueOf(maxNumConnection));
            } else {
                logBuilder.appendln("    Max. Number of connections: {}", Integer.valueOf(maxNumConnection));
                if (maxNumConnection > 0) {
                    IMAPProtocol.getInstance().setMaxCount(maxNumConnection);
                }
            }
        }

        { // NOSONARLINT
            final String tmp = configuration.getProperty("com.openexchange.imap.storeContainerType", "boundary-aware").trim();
            sContainerType = tmp;
            logBuilder.appendln("    Store container type: {}", sContainerType);
        }

        spamHandlerName = configuration.getProperty("com.openexchange.imap.spamHandler", SpamHandler.SPAM_HANDLER_FALLBACK).trim();
        logBuilder.appendln("    Spam Handler: {}", spamHandlerName);

        { // NOSONARLINT
            final String tmp = configuration.getProperty("com.openexchange.imap.ssl.protocols", "").trim();
            this.sslProtocols = Strings.isEmpty(tmp) ? null : tmp;
            logBuilder.appendln("    Supported SSL protocols: {}", null == this.sslProtocols ? "<default>" : sslProtocols);
        }

        { // NOSONARLINT
            final String tmp = configuration.getProperty("com.openexchange.imap.ssl.ciphersuites", "").trim();
            this.cipherSuites = Strings.isEmpty(tmp) ? null : tmp;
            logBuilder.appendln("    Supported SSL cipher suites: {}", null == this.cipherSuites ? "<default>" : cipherSuites);
        }

        { // NOSONARLINT
            String tmp = configuration.getProperty("com.openexchange.imap.greeting.host.regex", "").trim();
            tmp = Strings.isEmpty(tmp) ? null : tmp;
            if (null != tmp) {
                try {
                    Pattern pattern = Pattern.compile(tmp);
                    hostExtractingGreetingListener = new HostExtractingGreetingListener(pattern);
                    logBuilder.appendln("    Host name regular expression: {}", tmp);
                } catch (@SuppressWarnings("unused") PatternSyntaxException e) {
                    logBuilder.appendln("    Host name regular expression: Invalid value \"{}\". Using no host name extraction", tmp);
                }
            }
        }

        { // NOSONARLINT
            String tmp = configuration.getProperty("com.openexchange.imap.attachmentMarker.enabled", STR_FALSE).trim();
            enableAttachmentSearch = Boolean.parseBoolean(tmp);
            logBuilder.appendln("    Enable attachment search: {}", Boolean.valueOf(enableAttachmentSearch));
        }

        { // NOSONARLINT

            MonitoringCommandExecutor monitoringCommandExecutor = initMonitoringCommandExecutor(configuration);
            List<CommandExecutor> commandExecutorList = initCircuitBreakers(configuration, logBuilder, monitoringCommandExecutor);
            commandExecutorList.add(monitoringCommandExecutor);
            commandExecutors = commandExecutorList;
            for (CommandExecutor commandExecutor : commandExecutorList) {
                IMAPStore.addCommandExecutor(commandExecutor);
            }

            IMAPReloadable.getInstance().addReloadable(new Reloadable() {

                @Override
                public Interests getInterests() {
                    return Reloadables.interestsForProperties("com.openexchange.imap.breaker.*", "com.openexchange.imap.metrics.*");
                }

                @Override
                public void reloadConfiguration(ConfigurationService configService) {
                    List<CommandExecutor> commandExecutorList = commandExecutors;
                    if (null != commandExecutorList) {
                        for (CommandExecutor commandExecutor : commandExecutorList) {
                            IMAPStore.removeCommandExecutor(commandExecutor);
                        }
                    }
                    commandExecutors = null;

                    MonitoringCommandExecutor monitoringCommandExecutor = initMonitoringCommandExecutor(configuration);
                    commandExecutorList = initCircuitBreakers(configuration, logBuilder, monitoringCommandExecutor);
                    commandExecutorList.add(monitoringCommandExecutor);
                    commandExecutors = commandExecutorList;
                    for (CommandExecutor commandExecutor : commandExecutorList) {
                        IMAPStore.addCommandExecutor(commandExecutor);
                    }
                }

            });
        }

        { // NOSONARLINT
            String tmp = configuration.getProperty("com.openexchange.imap.preferredPreviewMode", "").trim();
            if (Strings.isNotEmpty(tmp)) {
                PreviewMode mode = PreviewMode.previewModeFor(tmp);
                if (mode != null && PreviewMode.NONE != mode) {
                    preferredPreviewMode = mode;
                }
            }
            logBuilder.appendln("    Preferred preview mode: {}", Boolean.valueOf(preferredPreviewMode == null ? "<none>" : preferredPreviewMode.getCapabilityName()));
        }

        { // NOSONARLINT
            final String tmp = configuration.getProperty("com.openexchange.imap.useUTF7ForUserFlags", STR_FALSE).trim();
            useUTF7ForUserFlags = Boolean.parseBoolean(tmp);
            logBuilder.appendln("    Encode User Flags as UTF-7: {}", Boolean.valueOf(useUTF7ForUserFlags));
        }

        logBuilder.append("Global IMAP properties successfully loaded!");

        LOG.info(logBuilder.getMessage(), logBuilder.getArgumentsAsArray());
    }

    /**
     * Initializes and configures the command executor that wraps all performed IMAP commands with proper monitoring.
     *
     * @param configuration Current {@link ConfigurationService} instance
     * @return The command executor
     */
    private static MonitoringCommandExecutor initMonitoringCommandExecutor(ConfigurationService configuration) {
        MonitoringCommandExecutor.Config monitoringConfig = new MonitoringCommandExecutor.Config();
        monitoringConfig.setEnabled(configuration.getBoolProperty("com.openexchange.imap.metrics.enabled", true));
        monitoringConfig.setGroupByPrimaryHosts(configuration.getBoolProperty("com.openexchange.imap.metrics.groupByPrimaryHosts", false));
        monitoringConfig.setGroupByPrimaryEndpoints(configuration.getBoolProperty("com.openexchange.imap.metrics.groupByPrimaryEndpoints", false));
        monitoringConfig.setMeasureExternalAccounts(configuration.getBoolProperty("com.openexchange.imap.metrics.measureExternalAccounts", true));
        monitoringConfig.setGroupByExternalHosts(configuration.getBoolProperty("com.openexchange.imap.metrics.groupByExternalHosts", false));
        monitoringConfig.setGroupByCommands(configuration.getBoolProperty("com.openexchange.imap.metrics.groupByCommands", false));
        monitoringConfig.setCommandWhitelist(configuration.getProperty("com.openexchange.imap.metrics.commandWhitelist", MonitoringCommandExecutor.Config.DEFAULT_COMMAND_WHITELIST_STRING, ","));
        return new MonitoringCommandExecutor(monitoringConfig);
    }

    /**
     * Initializes IMAP circuit breakers and returns their respective command executors.
     *
     * @param configuration Current {@link ConfigurationService} instance
     * @param logBuilder Log output
     * @param monitoringCommandExecutor The monitoring command executor as delegate for the actual IMAP commands
     * @return List of command executors in expected evaluation order
     */
    private static List<CommandExecutor> initCircuitBreakers(ConfigurationService configuration, LogMessageBuilder logBuilder, MonitoringCommandExecutor monitoringCommandExecutor) {
        // Load generic breaker
        GenericFailsafeCircuitBreakerCommandExecutor genericBreaker = null;
        Optional<AbstractFailsafeCircuitBreakerCommandExecutor> optionalGenericBreaker = initGenericCircuitBreaker(configuration, monitoringCommandExecutor);
        if (optionalGenericBreaker.isPresent()) {
            genericBreaker = (GenericFailsafeCircuitBreakerCommandExecutor) optionalGenericBreaker.get();
            logBuilder.appendln("    Added generic circuit breaker");
        }

        // Collect names of other breaker-associated properties
        Set<String> names = null;
        String prefix = "com.openexchange.imap.breaker.";
        for (Iterator<String> it = configuration.propertyNames(); it.hasNext();) {
            String propName = it.next();
            if (propName.startsWith(prefix, 0)) {
                int pos = propName.indexOf('.', prefix.length());
                if (pos > 0) {
                    if (names == null) {
                        names = new HashSet<>();
                    }
                    names.add(propName.substring(prefix.length(), pos));
                }
            }
        }

        // Any collected?
        if (names == null) {
            return genericBreaker == null ? new ArrayList<>(0) : new ArrayList<>(Collections.singletonList(genericBreaker));
        }

        // Iterate them
        List<CommandExecutor> breakerList = new ArrayList<>(names.size() + 1);
        if (genericBreaker != null) {
            breakerList.add(genericBreaker);
        }
        for (String name : names) {
            Optional<AbstractFailsafeCircuitBreakerCommandExecutor> optBreaker = initCircuitBreakerForName(Optional.of(name), configuration, genericBreaker, monitoringCommandExecutor);
            if (optBreaker.isPresent()) {
                AbstractFailsafeCircuitBreakerCommandExecutor circuitBreaker = optBreaker.get();
                breakerList.add(circuitBreaker);
                if ("primary".equals(name)) {
                    logBuilder.appendln("    Added circuit breaker for primary account");
                } else {
                    Optional<HostList> hostList = circuitBreaker.getHostList();
                    logBuilder.appendln("    Added \"{}\"  circuit breaker for hosts: {}", name, hostList.isEmpty() ? "<none>" : hostList.get().getHostList());
                }
            }
        }

        return breakerList;
    }

    private static Optional<AbstractFailsafeCircuitBreakerCommandExecutor> initGenericCircuitBreaker(ConfigurationService configuration,
            MonitoringCommandExecutor monitoringCommandExecutor) {
        return initCircuitBreakerForName(Optional.empty(), configuration, null, monitoringCommandExecutor);
    }

    /**
     * Initializes the circuit breaker with the optional infix.
     *
     * @param optionalInfix The optional circuit breaker name
     * @param configuration The configuration service to use
     * @param genericBreaker The already initialized generic circuit breaker or <code>null</code>
     * @param monitoringCommandExecutor The delegate executor
     * @return An optional {@link AbstractFailsafeCircuitBreakerCommandExecutor}
     */
    private static Optional<AbstractFailsafeCircuitBreakerCommandExecutor> initCircuitBreakerForName(Optional<String> optionalInfix, ConfigurationService configuration,
            GenericFailsafeCircuitBreakerCommandExecutor genericBreaker, MonitoringCommandExecutor monitoringCommandExecutor) {
        if (!optionalInfix.isPresent()) {
            return initGenericCircuitBreakerInternal(configuration, monitoringCommandExecutor);
        } // End of generic

        String infix = optionalInfix.get();

        if ("generic".equals(infix)) {
            return initGenericCircuitBreakerInternal(configuration, monitoringCommandExecutor);
        } // End of generic

        if ("primary".equals(infix)) {
            return initPrimaryCircuitBreaker(configuration, genericBreaker, monitoringCommandExecutor);
        } // End of primary

        return initSpecificCircuitBreaker(configuration, genericBreaker, infix, monitoringCommandExecutor);
    }

    /**
     * Initializes the generic circuit breaker.
     *
     * @param configuration The configuration service to use
     * @param monitoringCommandExecutor
     * @return An optional generic circuit breaker
     */
    private static Optional<AbstractFailsafeCircuitBreakerCommandExecutor> initGenericCircuitBreakerInternal(ConfigurationService configuration,
            MonitoringCommandExecutor monitoringCommandExecutor) {
        // The generic IMAP circuit breaker
        String propertyName = "com.openexchange.imap.breaker.enabled";
        boolean enabled = configuration.getBoolProperty(propertyName, false);
        if (!enabled) {
            return Optional.empty();
        }

        int failures;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker.failureThreshold";
            String sFailures = configuration.getProperty(propertyName, "5").trim();
            if (Strings.isEmpty(sFailures)) {
                LOG.warn("Missing value for property {}. Skipping generic breaker configuration", propertyName);
                return Optional.empty();
            }
            try {
                failures = Integer.parseInt(sFailures.trim());
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.warn("Invalid value for property {}. Not a number. Skipping generic breaker configuration", propertyName);
                return Optional.empty();
            }
        }
        int failureExecutions = failures;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker.failureExecutions";
            String sFailures = configuration.getProperty(propertyName, "").trim();
            if (Strings.isNotEmpty(sFailures)) {
                try {
                    failureExecutions = Integer.parseInt(sFailures.trim());
                    if (failureExecutions == 0) {
                        LOG.warn("Invalid value for property {}, value must not be '0' to prevent division by zero", propertyName);
                        return Optional.empty();
                    }
                } catch (@SuppressWarnings("unused") NumberFormatException e) {
                    LOG.warn("Invalid value for property {}. Not a number. Skipping generic breaker configuration", propertyName);
                    return Optional.empty();
                }
            }
        }
        if (failureExecutions < failures) {
            failureExecutions = failures;
        }

        int success;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker.successThreshold";
            String sSuccess = configuration.getProperty(propertyName, "2").trim();
            if (Strings.isEmpty(sSuccess)) {
                LOG.warn("Missing value for property {}. Skipping generic breaker configuration", propertyName);
                return Optional.empty();
            }
            try {
                success = Integer.parseInt(sSuccess.trim());
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.warn("Invalid value for property {}. Not a number. Skipping generic breaker configuration", propertyName);
                return Optional.empty();
            }
        }
        int successExecutions = success;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker.successExecutions";
            String sSuccess = configuration.getProperty(propertyName, "").trim();
            if (Strings.isNotEmpty(sSuccess)) {
                try {
                    successExecutions = Integer.parseInt(sSuccess.trim());
                    if (successExecutions == 0) {
                        LOG.warn("Invalid value for property {}, value must not be '0' to prevent division by zero", propertyName);
                        return Optional.empty();
                    }
                } catch (@SuppressWarnings("unused") NumberFormatException e) {
                    LOG.warn("Invalid value for property {}. Not a number. Skipping generic breaker configuration", propertyName);
                    return Optional.empty();
                }
            }
        }
        if (successExecutions < success) {
            successExecutions = success;
        }

        long delayMillis;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker.delayMillis";
            String sDelayMillis = configuration.getProperty(propertyName, "60000").trim();
            if (Strings.isEmpty(sDelayMillis)) {
                LOG.warn("Missing value for property {}. Skipping generic breaker configuration", propertyName);
                return Optional.empty();
            }
            try {
                delayMillis = Long.parseLong(sDelayMillis.trim());
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.warn("Invalid value for property {}. Not a number. Skipping generic breaker configuration", propertyName);
                return Optional.empty();
            }
        }

        return Optional.of(new GenericFailsafeCircuitBreakerCommandExecutor(ratioOf(failures, failureExecutions), ratioOf(success, successExecutions), delayMillis, monitoringCommandExecutor));
    }

    /**
     * Initializes a specific circuit breaker.
     *
     * @param configuration The configuration service to use
     * @param genericBreaker The already initialized generic circuit breaker or <code>null</code>
     * @param infix The name of the circuit breaker
     * @param monitoringCommandExecutor
     * @return An optional specific circuit breaker
     */
    private static Optional<AbstractFailsafeCircuitBreakerCommandExecutor> initSpecificCircuitBreaker(ConfigurationService configuration,
            GenericFailsafeCircuitBreakerCommandExecutor genericBreaker, String infix, MonitoringCommandExecutor monitoringCommandExecutor) {
        // Specific
        String propertyName = "com.openexchange.imap.breaker." + infix + ".hosts";
        String hosts = configuration.getProperty(propertyName, "");
        if (Strings.isEmpty(hosts)) {
            LOG.warn("Missing value for property {}. Skipping breaker configuration for {}", propertyName, infix);
            return Optional.empty();
        }

        propertyName = "com.openexchange.imap.breaker." + infix + ".enabled";
        boolean enabled = configuration.getBoolProperty(propertyName, false);
        if (!enabled) {
            if (genericBreaker != null) {
                genericBreaker.addHostsToExclude(hosts);
            }
            return Optional.empty();
        }

        propertyName = "com.openexchange.imap.breaker." + infix + ".ports";
        String ports = configuration.getProperty(propertyName, "");
        if (Strings.isEmpty(ports)) {
            ports = null;
        }

        int failures;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker." + infix + ".failureThreshold";
            String sFailures = configuration.getProperty(propertyName, "5").trim();
            if (Strings.isEmpty(sFailures)) {
                LOG.warn("Missing value for property {}. Skipping breaker configuration for {}", propertyName, infix);
                return Optional.empty();
            }
            try {
                failures = Integer.parseInt(sFailures.trim());
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.warn("Invalid value for property {}. Not a number. Skipping breaker configuration for {}", propertyName, infix);
                return Optional.empty();
            }
        }
        int failureExecutions = failures;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker." + infix + ".failureExecutions";
            String sFailures = configuration.getProperty(propertyName, "").trim();
            if (Strings.isNotEmpty(sFailures)) {
                try {
                    failureExecutions = Integer.parseInt(sFailures.trim());
                    if (failureExecutions == 0) {
                        LOG.warn("Invalid value for property {}, value must not be '0' to prevent division by zero", propertyName);
                        return Optional.empty();
                    }
                } catch (@SuppressWarnings("unused") NumberFormatException e) {
                    LOG.warn("Invalid value for property {}. Not a number. Skipping breaker configuration for {}", propertyName, infix);
                    return Optional.empty();
                }
            }
        }
        if (failureExecutions < failures) {
            failureExecutions = failures;
        }

        int success;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker." + infix + ".successThreshold";
            String sSuccess = configuration.getProperty(propertyName, "2").trim();
            if (Strings.isEmpty(sSuccess)) {
                LOG.warn("Missing value for property {}. Skipping breaker configuration for {}", propertyName, infix);
                return Optional.empty();
            }
            try {
                success = Integer.parseInt(sSuccess.trim());
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.warn("Invalid value for property {}. Not a number. Skipping breaker configuration for {}", propertyName, infix);
                return Optional.empty();
            }
        }
        int successExecutions = success;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker." + infix + ".successExecutions";
            String sSuccess = configuration.getProperty(propertyName, "").trim();
            if (Strings.isNotEmpty(sSuccess)) {
                try {
                    successExecutions = Integer.parseInt(sSuccess.trim());
                    if (successExecutions == 0) {
                        LOG.warn("Invalid value for property {}, value must not be '0' to prevent division by zero", propertyName);
                        return Optional.empty();
                    }
                } catch (@SuppressWarnings("unused") NumberFormatException e) {
                    LOG.warn("Invalid value for property {}. Not a number. Skipping breaker configuration for {}", propertyName, infix);
                    return Optional.empty();
                }
            }
        }
        if (successExecutions < success) {
            successExecutions = success;
        }

        long delayMillis;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker." + infix + ".delayMillis";
            String sDelayMillis = configuration.getProperty(propertyName, "60000").trim();
            if (Strings.isEmpty(sDelayMillis)) {
                LOG.warn("Missing value for property {}. Skipping breaker configuration for {}", propertyName, infix);
                return Optional.empty();
            }
            try {
                delayMillis = Long.parseLong(sDelayMillis.trim());
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.warn("Invalid value for property {}. Not a number. Skipping breaker configuration for {}", propertyName, infix);
                return Optional.empty();
            }
        }

        HostList hostList = HostList.valueOf(hosts); // NOSONARLINT
        Set<Integer> portSet;
        if (null == ports) {
            portSet = null;
        } else {
            portSet = HashSet.newHashSet(6);
            for (String sPort : Strings.splitByComma(ports)) {
                try {
                    portSet.add(Integer.valueOf(sPort.trim()));
                } catch (@SuppressWarnings("unused") NumberFormatException e) {
                    LOG.warn("Invalid value for port. Not a number. Skipping breaker configuration for {}", infix);
                    return Optional.empty();
                }
            }
        }

        return Optional.of(new FailsafeCircuitBreakerCommandExecutor(infix, hostList, portSet, ratioOf(failures, failureExecutions), ratioOf(success, successExecutions), delayMillis, 100, monitoringCommandExecutor));
    }

    /**
     * Initializes the primary account circuit breaker.
     *
     * @param configuration The configuration service to use
     * @param genericBreaker The already initialized generic circuit breaker or <code>null</code>
     * @param monitoringCommandExecutor
     * @return An optional primary account circuit breaker
     */
    private static Optional<AbstractFailsafeCircuitBreakerCommandExecutor> initPrimaryCircuitBreaker(ConfigurationService configuration,
            GenericFailsafeCircuitBreakerCommandExecutor genericBreaker, MonitoringCommandExecutor monitoringCommandExecutor) {
        // The IMAP circuit breaker form primary account
        String propertyName = "com.openexchange.imap.breaker.primary.enabled";
        boolean enabled = configuration.getBoolProperty(propertyName, false);
        if (!enabled) {
            if (genericBreaker != null) {
                genericBreaker.excludePrimaryAccount();
            }
            return Optional.empty();
        }

        boolean applyPerEndpoint;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker.primary.applyPerEndpoint";
            applyPerEndpoint = configuration.getBoolProperty(propertyName, true);
        }

        int failures;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker.primary.failureThreshold";
            String sFailures = configuration.getProperty(propertyName, "5").trim();
            if (Strings.isEmpty(sFailures)) {
                LOG.warn("Missing value for property {}. Skipping breaker configuration for primary account", propertyName);
                return Optional.empty();
            }
            try {
                failures = Integer.parseInt(sFailures.trim());
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.warn("Invalid value for property {}. Not a number. Skipping breaker configuration for primary account", propertyName);
                return Optional.empty();
            }
        }
        int failureExecutions = failures;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker.primary.failureExecutions";
            String sFailures = configuration.getProperty(propertyName, "").trim();
            if (Strings.isNotEmpty(sFailures)) {
                try {
                    failureExecutions = Integer.parseInt(sFailures.trim());
                    if (failureExecutions == 0) {
                        LOG.warn("Invalid value for property {}, value must not be '0' to prevent division by zero", propertyName);
                        return Optional.empty();
                    }
                } catch (@SuppressWarnings("unused") NumberFormatException e) {
                    LOG.warn("Invalid value for property {}. Not a number. Skipping breaker configuration for primary account", propertyName);
                    return Optional.empty();
                }
            }
        }
        if (failureExecutions < failures) {
            failureExecutions = failures;
        }

        int success;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker.primary.successThreshold";
            String sSuccess = configuration.getProperty(propertyName, "2").trim();
            if (Strings.isEmpty(sSuccess)) {
                LOG.warn("Missing value for property {}. Skipping breaker configuration for primary account", propertyName);
                return Optional.empty();
            }
            try {
                success = Integer.parseInt(sSuccess.trim());
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.warn("Invalid value for property {}. Not a number. Skipping breaker configuration for primary account", propertyName);
                return Optional.empty();
            }
        }
        int successExecutions = success;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker.primary.successExecutions";
            String sSuccess = configuration.getProperty(propertyName, "").trim();
            if (Strings.isNotEmpty(sSuccess)) {
                try {
                    successExecutions = Integer.parseInt(sSuccess.trim());
                    if (successExecutions == 0) {
                        LOG.warn("Invalid value for property {}, value must not be '0' to prevent division by zero", propertyName);
                        return Optional.empty();
                    }
                } catch (@SuppressWarnings("unused") NumberFormatException e) {
                    LOG.warn("Invalid value for property {}. Not a number. Skipping breaker configuration for primary account", propertyName);
                    return Optional.empty();
                }
            }
        }
        if (successExecutions < success) {
            successExecutions = success;
        }

        long delayMillis;
        { // NOSONARLINT
            propertyName = "com.openexchange.imap.breaker.primary.delayMillis";
            String sDelayMillis = configuration.getProperty(propertyName, "60000").trim();
            if (Strings.isEmpty(sDelayMillis)) {
                LOG.warn("Missing value for property {}. Skipping breaker configuration for primary account", propertyName);
                return Optional.empty();
            }
            try {
                delayMillis = Long.parseLong(sDelayMillis.trim());
            } catch (@SuppressWarnings("unused") NumberFormatException e) {
                LOG.warn("Invalid value for property {}. Not a number. Skipping breaker configuration for primary account", propertyName);
                return Optional.empty();
            }
        }

        return Optional.of(new PrimaryFailsafeCircuitBreakerCommandExecutor(ratioOf(failures, failureExecutions), ratioOf(success, successExecutions), delayMillis, applyPerEndpoint, monitoringCommandExecutor));
    }

    /**
     * Creates a new {@link Ratio} object avoiding division by zero. If the
     * denominator is zero then a new {@link Ratio} object will be created
     * with numerator equals to 0 and denominator equals to 1.
     *
     * @param numerator The numerator
     * @param denominator The denominator
     * @return The new {@link Ratio} object
     */
    private static Ratio ratioOf(int numerator, int denominator) {
        return denominator == 0 ? new Ratio(0, 1) : new Ratio(numerator, denominator);
    }

    @Override
    protected void resetFields() {
        cacheForPrimaryProps.invalidateAll();
        imapSort = false;
        imapSearch = false;
        forceImapSearch = false;
        fastFetch = true;
        propagateClientIPAddress = false;
        enableTls = true;
        requireTls = true;
        auditLogEnabled = false;
        debugLogEnabled = false;
        overwritePreLoginCapabilities = false;
        propagateHostNames = Collections.emptySet();
        supportsACLs = null;
        imapReadTimeout = 50000;
        imapFilterReadTimeout = -1;
        readResponsesTimeout = 60_000;
        imapConnectTimeout = 20000;
        imapTemporaryDown = 10000;
        imapFailedAuthTimeout = 10000;
        imapAuthEnc = null;
        entity2AclImpl = null;
        blockSize = 0;
        maxNumConnection = -1;
        sContainerType = "boundary-aware";
        spamHandlerName = null;
        sslProtocols = null;
        cipherSuites = null;
        hostExtractingGreetingListener = null;
        enableAttachmentSearch = false;
        preferredPreviewMode = null;
        List<CommandExecutor> commandExecutorsList = commandExecutors;
        commandExecutors = null;
        if (null != commandExecutorsList) {
            for (CommandExecutor commandExecutor : commandExecutorsList) {
                IMAPStore.removeCommandExecutor(commandExecutor);
            }
        }
    }

    /**
     * Gets the container type.
     *
     * @return The container type
     */
    public String getsContainerType() {
        return sContainerType;
    }

    /**
     * Gets the {@link Entity2ACL}.
     *
     * @return The {@link Entity2ACL}
     */
    public String getEntity2AclImpl() {
        return entity2AclImpl;
    }

    /**
     * Gets the spam handler name.
     *
     * @return The spam handler name
     */
    public String getSpamHandlerName() {
        return spamHandlerName;
    }

    /**
     * Gets the command executors
     *
     * @return The command executors
     */
    public List<CommandExecutor> getCommandExecutors() {
        return Collections.unmodifiableList(commandExecutors);
    }

    /**
     * Gets the greeting listener to parse the host name information from <b><i>primary</i></b> IMAP server's greeting string.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The host name extractor or <code>null</code>
     */
    public HostExtractingGreetingListener getHostNameRegex(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.hostExtractingGreetingListener;
        } catch (Exception e) {
            LOG.error("Failed to get host name expression for user {} in context {}. Using default {} instead.", I(userId), I(contextId), hostExtractingGreetingListener, e);
            return hostExtractingGreetingListener;
        }
    }

    /**
     * Checks whether possible multiple IP addresses for a host name are supposed to be considered.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> to use multiple IP addresses; otherwise <code>false</code>
     */
    public boolean isUseMultipleAddresses(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.useMultipleAddresses;
        } catch (Exception e) {
            LOG.error("Failed to check for usage of multiple addresses for user {} in context {}. Using default {} instead.", I(userId), I(contextId), Boolean.FALSE, e);
            return false;
        }
    }

    /**
     * Checks whether a user hash should be used for selecting one of possible multiple IP addresses for a host name.
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * Only effective if {@link #isUseMultipleAddresses(int, int)} returns <code>true</code>!
     * </div>
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> to use multiple IP addresses; otherwise <code>false</code>
     */
    public boolean isUseMultipleAddressesUserHash(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.useMultipleAddressesUserHash;
        } catch (Exception e) {
            LOG.error("Failed to get hash for multiple addresses for user {} in context {}. Using default {} instead.", I(userId), I(contextId), Boolean.FALSE, e);
            return false;
        }
    }

    /**
     * Gets the max. number of retry attempts when failing over to another IP address-
     * <p>
     * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
     * Only effective if {@link #isUseMultipleAddresses(int, int)} returns <code>true</code>!
     * </div>
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The max. wait timeout or <code>-1</code>
     */
    public int getMultipleAddressesMaxRetryAttempts(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.useMultipleAddressesMaxRetryAttempts;
        } catch (Exception e) {
            LOG.error("Failed to get max. retry attempts for multiple addresses for user {} in context {}. Using default {} instead.", I(userId), I(contextId), Integer.valueOf(-1), e);
            return -1;
        }
    }

    /**
     * Checks whether deleted mails should be ignored.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> to ignore deleted mails; otherwise <code>false</code>
     */
    public boolean isIgnoreDeletedMails(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.ignoreDeletedMails;
        } catch (Exception e) {
            LOG.error("Failed to get ignoreDeleted for user {} in context {}. Using default {} instead.", I(userId), I(contextId), Boolean.FALSE, e);
            return false;
        }
    }

    /**
     * Checks whether shared INBOX should be visible as "shared/user" or "shared/user/INBOX"
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> to explicitly include "INBOX"; otherwise <code>false</code>
     */
    public boolean includeSharedInboxExplicitly(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.includeSharedInboxExplicitly;
        } catch (Exception e) {
            LOG.error("Failed to get includeSharedInboxExplicitly for user {} in context {}. Using default {} instead.", I(userId), I(contextId), Boolean.FALSE, e);
            return false;
        }
    }

    /**
     * Checks if user's local part should be assumed when determining a shared folder path;<br>
     * e.g. assume <code>"jane.doe"</code> instead of <code>"jane.doe@invalid.com"</code>.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> to assume user's local part; otherwise <code>false</code>
     */
    public boolean assumeUserLocalPartForSharedFolderPath(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.assumeUserLocalPartForSharedFolderPath;
        } catch (Exception e) {
            LOG.error("Failed to get assumeUserLocalPartForSharedFolderPath for user {} in context {}. Using default {} instead.", I(userId), I(contextId), Boolean.FALSE, e);
            return false;
        }
    }

    /**
     * Checks whether shared folders should be displayed using sharing entity's display name or not;<br>
     * e.g. <code>"Shared folders/Jane Doe"</code> vs. <code>"Shared folders/jane.doe@somewhere.com"</code>.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> to use display name; otherwise <code>false</code>
     */
    public boolean useIMAPEntityDisplayNameIfPossible(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.useIMAPEntityDisplayNameIfPossible;
        } catch (Exception e) {
            LOG.error("Failed to get useIMAPEntityDisplayNameIfPossible for user {} in context {}. Using default {} instead.", I(userId), I(contextId), Boolean.TRUE, e);
            return true;
        }
    }

    /**
     * Checks whether root sub-folders are allowed for primary IMAP server.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>Boolean.TRUE</code> if allowed; otherwise <code>Boolean.FALSE</code>. Or even <code>null</code> if behavior cannot be determined
     */
    public Boolean areRootSubfoldersAllowed(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.rootSubfoldersAllowed;
        } catch (Exception e) {
            LOG.error("Failed to get rootSubfoldersAllowed for user {} in context {}. Using default instead.", I(userId), I(contextId), e);
            return null;
        }
    }

    /**
     * Checks whether to assume a namespace per user for primary IMAP server.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> to assume a namespace per user; otherwise <code>false</code>
     */
    public boolean isNamespacePerUser(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.namespacePerUser;
        } catch (Exception e) {
            LOG.error("Failed to get namespacePerUser for user {} in context {}. Using default instead.", I(userId), I(contextId), e);
            return true;
        }
    }

    /**
     * Gets the threshold when to manually search with respect to umlauts.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The threshold
     */
    public int getUmlautFilterThreshold(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.umlautFilterThreshold;
        } catch (Exception e) {
            LOG.error("Failed to get umlautFilterThreshold for user {} in context {}. Using default instead.", I(userId), I(contextId), e);
            return 50;
        }
    }

    /**
     * Gets max. length for a mailbox name.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The length
     */
    public int getMaxMailboxNameLength(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.maxMailboxNameLength;
        } catch (Exception e) {
            LOG.error("Failed to get maxMailboxNameLength for user {} in context {}. Using default instead.", I(userId), I(contextId), e);
            return 60;
        }
    }

    /**
     * Gets the threshold when to manually search with respect to umlauts.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The threshold
     */
    public TIntSet getInvalidChars(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.invalidChars;
        } catch (Exception e) {
            LOG.error("Failed to get invalidChars for user {} in context {}. Using default instead.", I(userId), I(contextId), e);
            return new TIntHashSet(0);
        }
    }

    /**
     * Whether ESORT is allowed to be utilized
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> if allowed; otherwise <code>false</code>
     */
    public boolean allowESORT(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.allowESORT;
        } catch (Exception e) {
            LOG.error("Failed to get allowESORT for user {} in context {}. Using default instead.", I(userId), I(contextId), e);
            return true;
        }
    }

    /**
     * Whether ESORT is allowed to be utilized
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> if allowed; otherwise <code>false</code>
     */
    public boolean allowSORTDISPLAY(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.allowSORTDISPLAY;
        } catch (Exception e) {
            LOG.error("Failed to get allowSORTDISPLAY for user {} in context {}. Using default instead.", I(userId), I(contextId), e);
            return false;
        }
    }

    /**
     * Whether in-app sort is supposed to be utilized if IMAP-side SORT fails with a "NO" response
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> if allowed; otherwise <code>false</code>
     */
    public boolean fallbackOnFailedSORT(int userId, int contextId) {
        try {
            PrimaryIMAPProperties primaryIMAPProps = getPrimaryIMAPProps(userId, contextId);
            return primaryIMAPProps.fallbackOnFailedSORT;
        } catch (Exception e) {
            LOG.error("Failed to get fallbackOnFailedSORT for user {} in context {}. Using default instead.", I(userId), I(contextId), e);
            return false;
        }
    }

    // -----------------------------------------------------------------------------------------------------------

    @Override
    public boolean isFastFetch() {
        return fastFetch;
    }

    @Override
    public boolean isPropagateClientIPAddress() {
        return propagateClientIPAddress;
    }

    @Override
    public boolean isEnableTls() {
        return enableTls;
    }

    @Override
    public boolean isRequireTls() {
        return requireTls;
    }

    @Override
    public boolean isAuditLogEnabled() {
        return auditLogEnabled;
    }

    @Override
    public boolean isDebugLogEnabled() {
        return debugLogEnabled;
    }

    @Override
    public String getDebugServerPattern() {
        return debugServerPattern;
    }

    @Override
    public boolean isOverwritePreLoginCapabilities() {
        return overwritePreLoginCapabilities;
    }

    @Override
    public boolean isRetryLoginOnUnavailableResponseCode() {
        return retryLoginOnUnavailableResponseCode;
    }

    @Override
    public Set<String> getPropagateHostNames() {
        return propagateHostNames;
    }

    @Override
    public String getImapAuthEnc() {
        return imapAuthEnc;
    }

    @Override
    public int getConnectTimeout() {
        return imapConnectTimeout;
    }

    @Override
    public int getImapTemporaryDown() {
        return imapTemporaryDown;
    }

    @Override
    public int getImapFailedAuthTimeout() {
        return imapFailedAuthTimeout;
    }

    @Override
    public boolean isImapSearch() {
        return imapSearch;
    }

    @Override
    public boolean forceImapSearch() {
        return forceImapSearch;
    }

    @Override
    public boolean isImapSort() {
        return imapSort;
    }

    @Override
    public boolean isUseUTF7ForUserFlags() {
        return useUTF7ForUserFlags;
    }

    @Override
    public int getReadTimeout() {
        return imapReadTimeout;
    }

    @Override
    public int getFilterReadTimeout() {
        return imapFilterReadTimeout;
    }

    @Override
    public int getReadResponsesTimeout() {
        return readResponsesTimeout;
    }

    @Override
    public BoolCapVal getSupportsACLs() {
        return supportsACLs;
    }

    @Override
    public int getBlockSize() {
        return blockSize;
    }

    @Override
    public int getMaxNumConnection() {
        return maxNumConnection;
    }

    @Override
    public Map<String, Boolean> getNewACLExtMap() {
        return Collections.unmodifiableMap(newACLExtMap);
    }

    @Override
    public int getMailFetchLimit() {
        return mailProperties.getMailFetchLimit();
    }

    @Override
    public boolean hideInlineImages() {
        return mailProperties.hideInlineImages();
    }

    @Override
    public boolean detectInlineImageByDispositionOnly() {
        return mailProperties.detectInlineImageByDispositionOnly();
    }

    @Override
    public boolean isAllowNestedDefaultFolderOnAltNamespace() {
        return mailProperties.isAllowNestedDefaultFolderOnAltNamespace();
    }

    @Override
    public boolean isIgnoreSubscription() {
        return mailProperties.isIgnoreSubscription();
    }

    @Override
    public boolean isSupportSubscription() {
        return mailProperties.isSupportSubscription();
    }

    @Override
    public boolean isUserFlagsEnabled() {
        return mailProperties.isUserFlagsEnabled();
    }

    @Override
    public boolean allowFolderCaches() {
        return allowFolderCaches;
    }

    @Override
    public long getFolderCacheTimeoutMillis() {
        return folderCacheTimeoutMillis;
    }

    @Override
    public boolean allowFetchSingleHeaders() {
        return allowFetchSingleHeaders;
    }

    @Override
    public String getSSLProtocols() {
        return sslProtocols;
    }

    @Override
    public String getSSLCipherSuites() {
        return cipherSuites;
    }

    @Override
    public boolean isAttachmentMarkerEnabled() {
        return isUserFlagsEnabled() && enableAttachmentSearch;
    }

    @Override
    public PreviewMode getPreferredPreviewMode() {
        return preferredPreviewMode;
    }

    /**
     * Gets the suitable preview mode for given IMAP configuration.
     *
     * @param imapConfig The IMAP configuration
     * @return The preview mode to use
     */
    public static PreviewMode previewModeFor(IMAPConfig imapConfig) {
        // Get supported capabilities
        Map<String, String> capabilities = imapConfig.asMap();

        // Check if the preferred preview mode is set & supported
        PreviewMode preferredPreviewMode = imapConfig.getIMAPProperties().getPreferredPreviewMode();
        if (preferredPreviewMode != null) {
            String capabilityName = preferredPreviewMode.getCapabilityName();
            if (capabilityName != null && capabilities.containsKey(capabilityName)) {
                // The preferred preview mode is supported
                return preferredPreviewMode;
            }
        }

        // Default behavior for detecting proper preview mode
        PreviewMode previewMode = PreviewMode.NONE;
        for (PreviewMode pm : PreviewMode.values()) { // Already iterates is precedence order
            String capabilityName = pm.getCapabilityName();
            if (capabilityName != null && capabilities.containsKey(capabilityName)) {
                previewMode = pm;
            }
        }
        return previewMode;
    }

    private static int parsePositiveInt(String s) {
        try {
            int i = Integer.parseInt(s);
            return i < 0 ? -1 : i;
        } catch (NumberFormatException e) {
            LOG.trace("", e);
            return -1;
        }
    }

}
