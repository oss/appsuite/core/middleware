/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imap.config;

import org.slf4j.Logger;
import com.openexchange.config.AbstractCompositeReloadable;
import com.openexchange.config.ConfigurationService;
import com.openexchange.java.Strings;

/**
 * {@link IMAPReloadable} - Collects reloadables for IMAP bundle.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since 7.6.0
 */
public final class IMAPReloadable extends AbstractCompositeReloadable {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(IMAPReloadable.class);

    private static final IMAPReloadable INSTANCE = new IMAPReloadable();

    /**
     * Gets the instance.
     *
     * @return The instance
     */
    public static IMAPReloadable getInstance() {
        return INSTANCE;
    }

    // --------------------------------------------------------------------------------------------------- //

    /**
     * Initializes a new {@link IMAPReloadable}.
     */
    private IMAPReloadable() {
        super();
    }

    @Override
    protected void preReloadConfiguration(ConfigurationService configService) {
        try {
            final IMAPProperties imapProperties = IMAPProperties.getInstance();
            if (null != imapProperties) {
                imapProperties.resetProperties();
                imapProperties.loadProperties();
            }
        } catch (Exception e) {
            LOGGER.warn("Failed to reload IMAP properties", e);
        }
    }

    @Override
    protected void postReloadConfiguration(ConfigurationService configService) {
        // Nothing
    }

    @Override
    protected String[] getPropertiesOfInterest() {
        return new String[] { "com.openexchange.imap.*" };
    }

    @Override
    protected String[] getFileNamesOfInterest() {
        return Strings.getEmptyStrings();
    }

}
