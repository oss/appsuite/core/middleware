/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imap.command;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.UIDFolder;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MailDateFormat;
import com.google.common.collect.ImmutableMap;
import com.openexchange.exception.OXException;
import com.openexchange.imap.IMAPCapabilities;
import com.openexchange.imap.IMAPServerInfo;
import com.openexchange.java.Strings;
import com.openexchange.log.LogProperties;
import com.openexchange.mail.FullnameArgument;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.PreviewMode;
import com.openexchange.mail.compose.HeaderUtility;
import com.openexchange.mail.dataobjects.IDMailMessage;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.mime.ContentDisposition;
import com.openexchange.mail.mime.ContentType;
import com.openexchange.mail.mime.MessageHeaders;
import com.openexchange.mail.mime.MimeMailException;
import com.openexchange.mail.mime.PlainTextAddress;
import com.openexchange.mail.mime.QuotedInternetAddress;
import com.openexchange.mail.mime.converters.MimeMessageConverter;
import com.openexchange.mail.mime.utils.MimeMessageUtility;
import com.openexchange.mail.mime.utils.MimeStorageUtility;
import com.openexchange.session.IUserAndContext;
import com.sun.mail.iap.Response;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.protocol.BODY;
import com.sun.mail.imap.protocol.BODYSTRUCTURE;
import com.sun.mail.imap.protocol.ENVELOPE;
import com.sun.mail.imap.protocol.FLAGS;
import com.sun.mail.imap.protocol.FetchResponse;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.INTERNALDATE;
import com.sun.mail.imap.protocol.Item;
import com.sun.mail.imap.protocol.PREVIEW;
import com.sun.mail.imap.protocol.RFC822DATA;
import com.sun.mail.imap.protocol.RFC822SIZE;
import com.sun.mail.imap.protocol.SNIPPET;
import com.sun.mail.imap.protocol.UID;
import com.sun.mail.imap.protocol.X_GUID;
import com.sun.mail.imap.protocol.X_MAILBOX;
import com.sun.mail.imap.protocol.X_REAL_UID;
import gnu.trove.impl.Constants;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TLongIntHashMap;
import gnu.trove.procedure.TIntIntProcedure;
import gnu.trove.procedure.TLongIntProcedure;

/**
 * {@link MailMessageFetchIMAPCommand} - performs a prefetch of messages in given folder with only those fields set that need to be present for
 * display and sorting. A corresponding instance of <code>javax.mail.FetchProfile</code> is going to be generated from given fields.
 * <p>
 * This method avoids calling JavaMail's fetch() methods which implicitly requests whole message envelope (FETCH 1:* (ENVELOPE INTERNALDATE
 * RFC822.SIZE)) when later working on returned <code>javax.mail.Message</code> objects.
 * </p>
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class MailMessageFetchIMAPCommand extends AbstractIMAPCommand<MailMessage[]> {

    /** The logger constant */
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(MailMessageFetchIMAPCommand.class);

    /**
     * Interceptor for fetched instances of {@link MailMessage}.
     */
    public static interface MailMessageFetchInterceptor {

        /**
         * Intercepts specified instance of {@link MailMessage}.
         *
         * @param mail The mail message to intercept
         * @throws MessagingException If interception fails
         */
        void intercept(MailMessage mail) throws MessagingException;

        /**
         * Gets the return value from this interceptor.
         *
         * @return The return value
         */
        MailMessage[] getMails();

    }

    private static final int LENGTH = 9; // "FETCH <nums> (<command>)"
    private static final int LENGTH_WITH_UID = 13; // "UID FETCH <nums> (<command>)"

    private final int accountId;
    private String[] args;
    private final String command;
    private boolean uid;
    private final int length;
    private int index;
    private final MailMessage[] retval;
    private final MailMessageFetchInterceptor interceptor;
    private boolean determineAttachmentByHeader;
    private boolean checkICal;
    private boolean checkVCard;
    private boolean treatEmbeddedAsAttachment;
    private final boolean examineHasAttachmentUserFlags;
    private final IUserAndContext optUserAndContext;
    private final String fullname;
    private final Set<FetchItemHandler> lastHandlers;
    private final TLongIntHashMap uid2index;
    private final TIntIntHashMap seqNum2index;
    private final List<Runnable> pendingActions;

    /**
     * Initializes a new {@link MailMessageFetchIMAPCommand}.
     *
     * @param imapFolder The IMAP folder providing connected protocol
     * @param isRev1 Whether IMAP server has <i>IMAP4rev1</i> capability or not
     * @param seqNums The sequence numbers to fetch
     * @param fp The fetch profile to use
     * @param serverInfo The IMAP server information deduced from configuration
     * @param examineHasAttachmentUserFlags Whether has-attachment user flags should be considered
     * @param optUserAndContext The user and context tuple or <code>null</code>
     * @param previewMode Whether target IMAP server supports any preview capability
     * @param optionalInterceptor The optional interceptor
     * @throws MessagingException If initialization fails
     */
    public MailMessageFetchIMAPCommand(IMAPFolder imapFolder, boolean isRev1, int[] seqNums, FetchProfile fp, IMAPServerInfo serverInfo, boolean examineHasAttachmentUserFlags, IUserAndContext optUserAndContext, PreviewMode previewMode, Optional<? extends MailMessageFetchInterceptor> optionalInterceptor) throws MessagingException {
        super(imapFolder);
        pendingActions = new ArrayList<>(2);
        determineAttachmentByHeader = false;
        this.examineHasAttachmentUserFlags = examineHasAttachmentUserFlags;
        this.optUserAndContext = optUserAndContext;
        int messageCount = imapFolder.getMessageCount();
        if (messageCount <= 0) {
            returnDefaultValue = true;
        }
        accountId = null == serverInfo ? 0 : serverInfo.getAccountId();
        lastHandlers = new HashSet<FetchItemHandler>();
        command = getFetchCommand(isRev1, fp, false, serverInfo, previewMode);
        uid = false;
        length = seqNums.length;
        uid2index = null;
        if (optionalInterceptor.isPresent()) {
            seqNum2index = null;
        } else {
            seqNum2index = new TIntIntHashMap(length, Constants.DEFAULT_LOAD_FACTOR, 0, -1);
            for (int i = 0; i < length; i++) {
                seqNum2index.put(seqNums[i], i);
            }
        }
        args = length == messageCount ? (1 == length ? ARGS_FIRST : ARGS_ALL) : IMAPNumArgSplitter.splitSeqNumArg(seqNums, false, LENGTH + command.length());
        if (0 == length) {
            returnDefaultValue = true;
        }
        fullname = imapFolder.getFullName();
        if (optionalInterceptor.isPresent()) {
            retval = null;
            interceptor = optionalInterceptor.get();
        } else {
            retval = new MailMessage[length];
            interceptor = null;
        }
    }

    /**
     * Initializes a new {@link MailMessageFetchIMAPCommand} to fetch all messages.
     *
     * @param imapFolder The IMAP folder providing connected protocol
     * @param isRev1 Whether IMAP server has <i>IMAP4rev1</i> capability or not
     * @param messageCount The IMAP folder's message count
     * @param fp The fetch profile to use
     * @param serverInfo The IMAP server information deduced from configuration
     * @param examineHasAttachmentUserFlags Whether has-attachment user flags should be considered
     * @param optUserAndContext The user and context tuple or <code>null</code>
     * @param previewMode Whether target IMAP server supports any preview capability
     * @param optionalInterceptor The optional interceptor
     */
    public MailMessageFetchIMAPCommand(IMAPFolder imapFolder, boolean isRev1, int messageCount, FetchProfile fp, IMAPServerInfo serverInfo, boolean examineHasAttachmentUserFlags, IUserAndContext optUserAndContext, PreviewMode previewMode, Optional<? extends MailMessageFetchInterceptor> optionalInterceptor) {
        super(imapFolder);
        pendingActions = new ArrayList<>(2);
        determineAttachmentByHeader = false;
        this.examineHasAttachmentUserFlags = examineHasAttachmentUserFlags;
        this.optUserAndContext = optUserAndContext;
        if (messageCount <= 0) {
            returnDefaultValue = true;
        }
        accountId = null == serverInfo ? 0 : serverInfo.getAccountId();
        lastHandlers = new HashSet<FetchItemHandler>();
        command = getFetchCommand(isRev1, fp, false, serverInfo, previewMode);
        uid = false;
        length = messageCount;
        uid2index = null;
        seqNum2index = null;
        args = (1 == length ? ARGS_FIRST : ARGS_ALL);
        if (0 == length) {
            returnDefaultValue = true;
        }
        fullname = imapFolder.getFullName();
        if (optionalInterceptor.isPresent()) {
            retval = null;
            interceptor = optionalInterceptor.get();
        } else {
            retval = new MailMessage[length];
            interceptor = null;
        }
    }

    /**
     * Initializes a new {@link MailMessageFetchIMAPCommand}.
     *
     * @param imapFolder The IMAP folder providing connected protocol
     * @param isRev1 Whether IMAP server has <i>IMAP4rev1</i> capability or not
     * @param uids The UIDs to fetch
     * @param fp The fetch profile to use
     * @param serverInfo The IMAP server information deduced from configuration
     * @param examineHasAttachmentUserFlags Whether has-attachment user flags should be considered
     * @param optUserAndContext The user and context tuple or <code>null</code>
     * @param previewMode Whether target IMAP server supports any preview capability
     * @param optionalInterceptor The optional interceptor
     * @throws MessagingException If initialization fails
     */
    public MailMessageFetchIMAPCommand(IMAPFolder imapFolder, boolean isRev1, long[] uids, FetchProfile fp, IMAPServerInfo serverInfo, boolean examineHasAttachmentUserFlags, IUserAndContext optUserAndContext, PreviewMode previewMode, Optional<? extends MailMessageFetchInterceptor> optionalInterceptor) throws MessagingException {
        super(imapFolder);
        pendingActions = new ArrayList<>(2);
        determineAttachmentByHeader = false;
        this.examineHasAttachmentUserFlags = examineHasAttachmentUserFlags;
        this.optUserAndContext = optUserAndContext;
        int messageCount = imapFolder.getMessageCount();
        if (messageCount <= 0) {
            returnDefaultValue = true;
        }
        accountId = null == serverInfo ? 0 : serverInfo.getAccountId();
        lastHandlers = new HashSet<FetchItemHandler>();
        length = uids.length;
        seqNum2index = null;
        if (optionalInterceptor.isPresent()) {
            uid2index = null;
        } else {
            uid2index = new TLongIntHashMap(length, Constants.DEFAULT_LOAD_FACTOR, 0, -1);
            for (int i = 0; i < length; i++) {
                uid2index.put(uids[i], i);
            }
        }
        if (length == messageCount) {
            fp.add(UIDFolder.FetchProfileItem.UID);
            command = getFetchCommand(isRev1, fp, false, serverInfo, previewMode);
            args = (1 == length ? ARGS_FIRST : ARGS_ALL);
            uid = false;
        } else {
            command = getFetchCommand(isRev1, fp, false, serverInfo, previewMode);
            args = IMAPNumArgSplitter.splitUIDArg(uids, false, LENGTH_WITH_UID + command.length());
            uid = true;
        }
        if (0 == length) {
            returnDefaultValue = true;
        }
        fullname = imapFolder.getFullName();
        if (optionalInterceptor.isPresent()) {
            retval = null;
            interceptor = optionalInterceptor.get();
        } else {
            retval = new MailMessage[length];
            interceptor = null;
        }
    }

    /**
     * Sets whether detection if message contains attachment is performed by "Content-Type" header only.
     * <p>
     * If <code>true</code> a message is considered to contain attachments if its "Content-Type" header equals "multipart/mixed".
     *
     * @param determineAttachmentByHeader <code>true</code> to detect if message contains attachment is performed by "Content-Type" header
     *            only; otherwise <code>false</code>
     * @return This FETCH IMAP command with value applied
     */
    public MailMessageFetchIMAPCommand setDetermineAttachmentByHeader(boolean determineAttachmentByHeader) {
        this.determineAttachmentByHeader = determineAttachmentByHeader;
        return this;
    }

    /**
     * Sets the checkICal
     *
     * @param checkICal The checkICal to set
     * @return This FETCH IMAP command with value applied
     */
    public MailMessageFetchIMAPCommand setCheckICal(boolean checkICal) {
        this.checkICal = checkICal;
        return this;
    }

    /**
     * Sets the checkVCard
     *
     * @param checkVCard The checkVCard to set
     * @return This FETCH IMAP command with value applied
     */
    public MailMessageFetchIMAPCommand setCheckVCard(boolean checkVCard) {
        this.checkVCard = checkVCard;
        return this;
    }

    /**
     * Sets the treatEmbeddedAsAttachment
     *
     * @param treatEmbeddedAsAttachment The treatEmbeddedAsAttachment to set
     * @return This FETCH IMAP command with value applied
     */
    public MailMessageFetchIMAPCommand setTreatEmbeddedAsAttachment(boolean treatEmbeddedAsAttachment) {
        this.treatEmbeddedAsAttachment = treatEmbeddedAsAttachment;
        return this;
    }

    @Override
    protected String getDebugInfo(int argsIndex) {
        StringBuilder sb = new StringBuilder(command.length() + 64);
        if (uid) {
            sb.append("UID ");
        }
        sb.append("FETCH ");
        String arg = args[argsIndex];
        if (arg.length() > 32) {
            int pos = arg.indexOf(',');
            if (pos == -1) {
                sb.append("...");
            } else {
                sb.append(arg.substring(0, pos)).append(",...,").append(arg.substring(arg.lastIndexOf(',') + 1));
            }
        } else {
            sb.append(arg);
        }
        sb.append(" (").append(command).append(')');
        return sb.toString();
    }

    @Override
    protected boolean addLoopCondition() {
        return (index < length);
    }

    @Override
    protected String[] getArgs() {
        return args;
    }

    @Override
    protected CommandAndArgument getCommand(int argsIndex, IMAPProtocol protocol) {
        StringBuilder sb = new StringBuilder(args[argsIndex].length() + 64);
        if (uid) {
            sb.append("UID ");
        }
        sb.append("FETCH ");
        sb.append(args[argsIndex]);
        sb.append(" (").append(command).append(')');
        return CommandAndArgument.instanceFor(sb.toString());
    }

    @Override
    protected boolean isFetchCommand() {
        return true;
    }

    private static final MailMessage[] EMPTY_ARR = new MailMessage[0];

    @Override
    protected MailMessage[] getDefaultValue() {
        return EMPTY_ARR;
    }

    @Override
    protected MailMessage[] getReturnVal() throws MessagingException {
        for (Runnable pendingAction : pendingActions) {
            pendingAction.run();
        }
        return getFetchedMessages();
    }

    private MailMessage[] getFetchedMessages() {
        if (interceptor != null) {
            return interceptor.getMails();
        }

        final MailMessage[] retval = this.retval;
        if (null != seqNum2index) {
            seqNum2index.forEachEntry(new TIntIntProcedure() {

                @Override
                public boolean execute(int seqNum, int pos) {
                    if (pos > 0) {
                        retval[pos] = handleMessage(seqNum);
                    }
                    return true;
                }
            });
        } else if (null != uid2index) {
            uid2index.forEachEntry(new TLongIntProcedure() {

                @Override
                public boolean execute(long uid, int pos) {
                    if (pos > 0) {
                        retval[pos] = handleMessage(uid);
                    }
                    return true;
                }
            });
        } else if (index < length) {
            String server = imapFolder.getStore().toString();
            int pos = server.indexOf('@');
            if (pos >= 0 && ++pos < server.length()) {
                server = server.substring(pos);
            }
            MessagingException e = new MessagingException(new StringBuilder(32).append("Expected ").append(length).append(" FETCH responses but got ").append(index).append(" from IMAP folder \"").append(imapFolder.getFullName()).append("\" on server \"").append(server).append("\".").toString());
            LOG.warn("", e);
        }
        return retval;
    }

    IDMailMessage handleMessage(int seqNum) {
        if (seqNum < 0) {
            return null;
        }
        try {
            return handleMessage(imapFolder.getMessage(seqNum));
        } catch (Exception e) {
            LOG.warn("Message #{} discarded", I(seqNum), e);
            return null;
        }
    }

    IDMailMessage handleMessage(long uid) {
        if (uid < 0) {
            return null;
        }
        try {
            return handleMessage(imapFolder.getMessageByUID(uid));
        } catch (Exception e) {
            LOG.warn("Message uid={} discarded", L(uid), e);
            return null;
        }
    }

    private IDMailMessage handleMessage(Message message) {
        if (null == message) {
            return null;
        }
        try {
            IDMailMessage mail = new IDMailMessage(null, fullname);
            for (FetchItemHandler fetchItemHandler : lastHandlers.isEmpty() ? FETCH_ITEM_HANDLER_BY_CLASS.values() : lastHandlers) {
                fetchItemHandler.handleMessage(message, mail, LOG);
            }
            return mail;
        } catch (Exception e) {
            LOG.warn("Message #{} discarded", I(message.getMessageNumber()), e);
            return null;
        }
    }

    @Override
    protected boolean handleResponse(Response currentReponse) throws MessagingException {
        /*
         * Response is null or not a FetchResponse
         */
        if (!(currentReponse instanceof FetchResponse fetchResponse)) {
            return false;
        }
        int seqNum = fetchResponse.getNumber();
        int pos;
        if (null != seqNum2index) {
            pos = seqNum2index.remove(seqNum);
            if (pos < 0) {
                pos = index;
            }
        } else if (null != uid2index) {
            UID uidItem = getItemOf(UID.class, fetchResponse);
            if (null != uidItem) {
                pos = uid2index.remove(uidItem.uid);
                if (pos < 0) {
                    pos = index;
                }
            } else {
                pos = index;
            }
        } else {
            pos = index;
        }
        index++;
        boolean error = true;
        MailMessage mail;
        try {
            mail = handleFetchRespone(fetchResponse, fullname, accountId, lastHandlers, determineAttachmentByHeader, checkICal, checkVCard, treatEmbeddedAsAttachment, examineHasAttachmentUserFlags, optUserAndContext, imapFolder, pendingActions);
            error = false;
        } catch (MessagingException e) {
            /*
             * Discard corrupt message
             */
            LOG.warn("Message #{} discarded", I(seqNum), MimeMailException.handleMessagingException(e));
            mail = null;
        } catch (OXException e) {
            /*
             * Discard corrupt message
             */
            LOG.warn("Message #{} discarded", I(seqNum), e);
            mail = null;
        }
        if (!error) {
            if (interceptor == null) {
                retval[pos] = mail;
            } else {
                interceptor.intercept(mail);
            }
        }
        return true;
    }

    /**
     * Converts given FETCH response to an appropriate {@link MailMessage} instance.
     *
     * @param fetchResponse The FETCH response to handle
     * @param fullName The full name of associated folder
     * @param accountId The account identifier
     * @param examineHasAttachmentUserFlags Whether has-attachment user flags should be considered
     * @param optUserAndContext The user and context tuple or <code>null</code>
     * @return The resulting mail message
     * @throws MessagingException If a messaging error occurs
     * @throws OXException If an Open-Xchange error occurs
     */
    public static MailMessage handleFetchRespone(FetchResponse fetchResponse, String fullName, int accountId, boolean examineHasAttachmentUserFlags, IUserAndContext optUserAndContext) throws MessagingException, OXException {
        IDMailMessage mail = new IDMailMessage(null, fullName);
        mail.setAccountId(accountId);
        return handleFetchRespone(mail, fetchResponse, fullName, null, false, false, false, false, examineHasAttachmentUserFlags, optUserAndContext, null, null);
    }

    /**
     * Applies given FETCH response to an given {@link MailMessage} instance.
     *
     * @param mail The message to apply to
     * @param fetchResponse The FETCH response to handle
     * @param fullName The full name of associated folder
     * @param examineHasAttachmentUserFlags Whether has-attachment user flags should be considered
     * @param optUserAndContext The user and context tuple or <code>null</code>
     * @return The resulting mail message
     * @throws MessagingException If a messaging error occurs
     * @throws OXException If an Open-Xchange error occurs
     */
    public static MailMessage handleFetchRespone(IDMailMessage mail, FetchResponse fetchResponse, String fullName, boolean examineHasAttachmentUserFlags, IUserAndContext optUserAndContext) throws MessagingException, OXException {
        return handleFetchRespone(mail, fetchResponse, fullName, null, false, false, false, false, examineHasAttachmentUserFlags, optUserAndContext, null, null);
    }

    private static MailMessage handleFetchRespone(FetchResponse fetchResponse, String fullName, int accountId, Set<FetchItemHandler> lastHandlers, boolean determineAttachmentByHeader, boolean checkICal, boolean checkVCard, boolean treatEmbeddedAsAttachment, boolean examineHasAttachmentUserFlags, IUserAndContext optUserAndContext, IMAPFolder optImapFolder, List<Runnable> pendingActions) throws MessagingException, OXException {
        IDMailMessage mail = new IDMailMessage(null, fullName);
        mail.setAccountId(accountId);
        return handleFetchRespone(mail, fetchResponse, fullName, lastHandlers, determineAttachmentByHeader, checkICal, checkVCard, treatEmbeddedAsAttachment, examineHasAttachmentUserFlags, optUserAndContext, optImapFolder, pendingActions);
    }

    private static MailMessage handleFetchRespone(IDMailMessage mail, FetchResponse fetchResponse, String fullName, Set<FetchItemHandler> lastHandlers, boolean determineAttachmentByHeader, boolean checkICal, boolean checkVCard, boolean treatEmbeddedAsAttachment, boolean examineHasAttachmentUserFlags, IUserAndContext optUserAndContext, IMAPFolder optImapFolder, List<Runnable> pendingActions) throws MessagingException, OXException {
        try {
            return doHandleFetchRespone(mail, fetchResponse, fullName, lastHandlers, determineAttachmentByHeader, checkICal, checkVCard, treatEmbeddedAsAttachment, examineHasAttachmentUserFlags, optUserAndContext, optImapFolder, pendingActions);
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    private static MailMessage doHandleFetchRespone(IDMailMessage mail, FetchResponse fetchResponse, String fullName, Set<FetchItemHandler> lastHandlers, boolean determineAttachmentByHeader, boolean checkICal, boolean checkVCard, boolean treatEmbeddedAsAttachment, boolean examineHasAttachmentUserFlags, IUserAndContext optUserAndContext, IMAPFolder optImapFolder, List<Runnable> pendingActions) throws MessagingException, OXException {
        IDMailMessage m;
        if (null == mail) {
            m = new IDMailMessage(null, fullName);
        } else {
            m = mail;
            m.setFolder(fullName);
        }
        int seqnum = fetchResponse.getNumber();
        m.setSeqnum(seqnum);

        // Handle items contained in FETCH response
        Item delayed = null;
        MessageProvider messageProvider = optImapFolder == null ? null : new SeqNumMessageProvider(seqnum, optImapFolder);
        for (Iterator<Item> it = fetchResponse.itemsIterator(); it.hasNext();) {
            Item item = it.next();
            if (examineHasAttachmentUserFlags && (item instanceof BODYSTRUCTURE)) {
                // Delay BODYSTRUCTURE item...
                delayed = item;
            } else {
                FetchItemHandler itemHandler = FETCH_ITEM_HANDLER_BY_CLASS.get(item.getClass());
                if (null == itemHandler) {
                    itemHandler = getItemHandlerByItem(item, checkICal, checkVCard, treatEmbeddedAsAttachment, examineHasAttachmentUserFlags, optUserAndContext);
                    if (null == itemHandler) {
                        LOG.warn("Unknown FETCH item: {}", item.getClass().getName());
                    } else {
                        if (null != lastHandlers) {
                            lastHandlers.add(itemHandler);
                        }
                        itemHandler.handleItem(item, m, messageProvider, LOG, pendingActions);
                    }
                } else {
                    if (null != lastHandlers) {
                        lastHandlers.add(itemHandler);
                    }
                    itemHandler.handleItem(item, m, messageProvider, LOG, pendingActions);
                }
            }
        }

        if (null != delayed) {
            FetchItemHandler itemHandler = getItemHandlerByItem(delayed, checkICal, checkVCard, treatEmbeddedAsAttachment, examineHasAttachmentUserFlags, optUserAndContext);
            if (null == itemHandler) {
                LOG.warn("Unknown FETCH item: {}", delayed.getClass().getName());
            } else {
                if (null != lastHandlers) {
                    lastHandlers.add(itemHandler);
                }
                itemHandler.handleItem(delayed, m, messageProvider, LOG, pendingActions);
            }
        }

        if (determineAttachmentByHeader) {
            String cts = m.getHeader(MessageHeaders.HDR_CONTENT_TYPE, null);
            if (null != cts) {
                m.setAlternativeHasAttachment(new ContentType(cts).startsWith("multipart/mixed"));
            }
        }

        // Drop possibly set "com.openexchange.mail.mailId" log property
        LogProperties.remove(LogProperties.Name.MAIL_MAIL_ID);

        return m;
    }

    private static interface MessageProvider {

        /**
         * Gets the message from IMAP server.
         *
         * @return The message
         * @throws MessagingException If message cannot be provided
         */
        Message getMessage() throws MessagingException;
    }

    private static final class SeqNumMessageProvider implements MessageProvider {

        private final int seqnum;
        private final IMAPFolder imapFolder;

        /**
         * Initializes a new {@link MessageProviderImplementation}.
         *
         * @param seqnum The sequence number
         * @param imapFolder The IMAP folder providing access
         */
        SeqNumMessageProvider(int seqnum, IMAPFolder imapFolder) {
            this.seqnum = seqnum;
            this.imapFolder = imapFolder;
        }

        @Override
        public Message getMessage() throws MessagingException {
            return imapFolder.getMessage(seqnum);
        }
    }

    private static FetchItemHandler getItemHandlerByItem(Item item, boolean checkICal, boolean checkVCard, boolean treatEmbeddedAsAttachment, boolean examineHasAttachmentUserFlags, IUserAndContext optUserAndContext) {
        if (item instanceof BODYSTRUCTURE) {
            return BODYSTRUCTUREFetchItemHandler.getInstance(checkICal, checkVCard, treatEmbeddedAsAttachment);
        }
        if ((item instanceof RFC822DATA) || (item instanceof BODY)) {
            return HEADER_ITEM_HANDLER;
        }
        if (item instanceof UID) {
            return UID_ITEM_HANDLER;
        }
        if (item instanceof INTERNALDATE) {
            return INTERNALDATE_ITEM_HANDLER;
        }
        if (item instanceof Flags) {
            return FLAGSFetchItemHandler.getInstance(examineHasAttachmentUserFlags, optUserAndContext);
        }
        if (item instanceof ENVELOPE) {
            return ENVELOPE_ITEM_HANDLER;
        }
        if (item instanceof RFC822SIZE) {
            return SIZE_ITEM_HANDLER;
        }
        if (item instanceof X_REAL_UID) {
            return X_REAL_UID_ITEM_HANDLER;
        }
        if (item instanceof X_MAILBOX) {
            return X_MAILBOX_ITEM_HANDLER;
        }
        if (item instanceof SNIPPET) {
            return SNIPPET_ITEM_HANDLER;
        }
        if (item instanceof PREVIEW) {
            return PREVIEW_ITEM_HANDLER;
        }
        return null;
    }

    private static interface FetchItemHandler {

        /**
         * Handles given <code>com.sun.mail.imap.protocol.Item</code> instance and applies it to given message.
         *
         * @param item The item to handle
         * @param msg The message to apply to
         * @param messageProvider The message provider to get message from IMAP server
         * @param logger The logger
         * @param pendingActions A list to add a pending action to
         * @throws MessagingException If a messaging error occurs
         * @throws OXException If a mail error occurs
         */
        void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) throws MessagingException, OXException;

        void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws MessagingException, OXException;
    }

    /*-
     * ++++++++++++++ Item handlers ++++++++++++++
     */

    private interface HeaderHandler {

        void handle(Header hdr, IDMailMessage mailMessage) throws OXException;
    }

    private static final FetchItemHandler HEADER_ITEM_HANDLER = new FetchItemHandler() {

        private final Map<String, HeaderHandler> hh = ImmutableMap.<String, HeaderHandler> builder()
            .put(MessageHeaders.HDR_FROM, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    mailMessage.addFrom(MimeMessageUtility.getAddressHeader(hdr.getValue()));
                }
            })
            .put(MessageHeaders.HDR_TO, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    mailMessage.addTo(MimeMessageUtility.getAddressHeader(hdr.getValue()));
                }
            })
            .put(MessageHeaders.HDR_CC, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    mailMessage.addCc(MimeMessageUtility.getAddressHeader(hdr.getValue()));
                }
            })
            .put(MessageHeaders.HDR_BCC, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    mailMessage.addBcc(MimeMessageUtility.getAddressHeader(hdr.getValue()));
                }
            })
            .put(MessageHeaders.HDR_SENDER, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    mailMessage.addSender(MimeMessageUtility.getAddressHeader(hdr.getValue()));
                }
            })
            .put(MessageHeaders.HDR_REPLY_TO, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    mailMessage.addReplyTo(MimeMessageUtility.getAddressHeader(hdr.getValue()));
                }
            })
            .put(MessageHeaders.HDR_DISP_NOT_TO, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    InternetAddress[] dispNotTo = MimeMessageUtility.getAddressHeader(hdr.getValue());
                    if (dispNotTo.length > 0) {
                        mailMessage.setDispositionNotification(dispNotTo[0]);
                    }
                }
            })
            .put(MessageHeaders.HDR_SUBJECT, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    mailMessage.setSubject(MimeMessageUtility.decodeMultiEncodedHeader(MimeMessageUtility.checkNonAscii(hdr.getValue())), true);
                }
            })
            .put(MessageHeaders.HDR_DATE, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    try {
                        mailMessage.setSentDate(MailDateFormat.parseDate(hdr.getValue()));
                    } catch (ParseException e) {
                        LOG.error("Failed to parse \"Date\" header: {}", hdr.getValue(), e);
                    }
                }
            })
            .put(MessageHeaders.HDR_IMPORTANCE, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    String value = hdr.getValue();
                    if (null != value) {
                        mailMessage.setPriority(MimeMessageUtility.parseImportance(value));
                    }
                }
            })
            .put(MessageHeaders.HDR_X_PRIORITY, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    if (!mailMessage.containsPriority()) {
                        mailMessage.setPriority(MimeMessageUtility.parsePriority(hdr.getValue()));
                    }
                }
            })
            .put(MessageHeaders.HDR_X_OX_DATE_TO_SEND, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    if (!mailMessage.containsDateToSend()) {
                        mailMessage.setDateToSend(HeaderUtility.headerValue2DateToSend(hdr.getValue()));
                    }
                }
            })
            .put(MessageHeaders.HDR_REFERENCES, new HeaderHandler() {

                @Override
                public void handle(Header hdr, IDMailMessage mailMessage) throws OXException {
                    mailMessage.setReferences(hdr.getValue());
                }
            })
            .build();

        private final Set<String> headerFields = new HashSet<String>(Arrays.asList("content-type", "from", "to", "cc", "bcc", "disposition-notification-to", "subject"));

        @Override
        public void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) throws MessagingException, OXException {
            List<Header> headers;
            {
                InputStream headerStream = item instanceof BODY ? ((BODY) item).getByteArrayInputStream() : ((RFC822DATA) item).getByteArrayInputStream();
                if (null == headerStream) {
                    logger.debug("Cannot retrieve headers from message #{} in folder {}", I(msg.getSeqnum()), msg.getFolder());
                    headers = Collections.emptyList();
                } else {
                    headers = InternetHeaders.parse(headerStream);
                }
            }

            Set<String> headerFields = new HashSet<String>(this.headerFields);
            for (Header hdr : headers) {
                String name = hdr.getName();
                headerFields.remove(Strings.toLowerCase(name));
                {
                    HeaderHandler headerHandler = hh.get(name);
                    if (null != headerHandler) {
                        headerHandler.handle(hdr, msg);
                    }
                }
                try {
                    msg.addHeader(name, hdr.getValue());
                } catch (IllegalArgumentException illegalArgumentExc) {
                    logger.debug("Ignoring invalid header.", illegalArgumentExc);
                }
                /*-
                 *
                HeaderHandler hdrHandler = hdrHandlers.get(hdr.getName());
                if (hdrHandler == null) {
                    msg.setHeader(hdr.getName(), hdr.getValue());
                } else {
                    hdrHandler.handleHeader(hdr.getValue(), msg);
                }
                 */
            }
            if (headerFields.contains("disposition-notification-to")) {
                msg.setDispositionNotification(null);
            }
        }

        @Override
        public void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws MessagingException, OXException {
            for (Enumeration<Header> e = message.getAllHeaders(); e.hasMoreElements();) {
                Header hdr = e.nextElement();
                String name = hdr.getName();
                {
                    HeaderHandler headerHandler = hh.get(name);
                    if (null != headerHandler) {
                        headerHandler.handle(hdr, msg);
                    }
                }
                try {
                    msg.addHeader(name, hdr.getValue());
                } catch (IllegalArgumentException illegalArgumentExc) {
                    logger.debug("Ignoring invalid header.", illegalArgumentExc);
                }
                /*-
                 *
                HeaderHandler hdrHandler = hdrHandlers.get(hdr.getName());
                if (hdrHandler == null) {
                    msg.setHeader(hdr.getName(), hdr.getValue());
                } else {
                    hdrHandler.handleHeader(hdr.getValue(), msg);
                }
                 */
            }
        }
    };

    private static final class FLAGSFetchItemHandler implements FetchItemHandler {

        static FLAGSFetchItemHandler getInstance(boolean examineHasAttachmentUserFlags, IUserAndContext optUserAndContext) {
            return new FLAGSFetchItemHandler(examineHasAttachmentUserFlags, optUserAndContext);
        }

        // ---------------------------------------------------------------------------------------------------------------------------------

        private final boolean examineHasAttachmentUserFlags;
        private final IUserAndContext optUserAndContext;

        private FLAGSFetchItemHandler(boolean examineHasAttachmentUserFlags, IUserAndContext optUserAndContext) {
            super();
            this.examineHasAttachmentUserFlags = examineHasAttachmentUserFlags;
            this.optUserAndContext = optUserAndContext;
        }

        @Override
        public void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) throws MessagingException {
            FLAGS flags = (FLAGS) item;
            if (optUserAndContext == null) {
                MimeMessageConverter.parseFlags(flags, examineHasAttachmentUserFlags, msg);
            } else {
                MimeMessageConverter.parseFlags(flags, examineHasAttachmentUserFlags, optUserAndContext.getUserId(), optUserAndContext.getContextId(), msg);
            }
        }

        @Override
        public void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws MessagingException {
            Flags flags = message.getFlags();
            if (optUserAndContext == null) {
                MimeMessageConverter.parseFlags(flags, examineHasAttachmentUserFlags, msg);
            } else {
                MimeMessageConverter.parseFlags(flags, examineHasAttachmentUserFlags, optUserAndContext.getUserId(), optUserAndContext.getContextId(), msg);
            }
        }
    }

    private static final FetchItemHandler ENVELOPE_ITEM_HANDLER = new FetchItemHandler() {

        @Override
        public void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) throws MessagingException {
            ENVELOPE env = (ENVELOPE) item;
            msg.addFrom(wrap(messageProvider, MessageHeaders.HDR_FROM, logger, pendingActions, msg, env.from));
            msg.addTo(wrap(messageProvider, MessageHeaders.HDR_TO, logger, pendingActions, msg, env.to));
            msg.addCc(wrap(messageProvider, MessageHeaders.HDR_CC, logger, pendingActions, msg, env.cc));
            msg.addBcc(wrap(messageProvider, MessageHeaders.HDR_BCC, logger, pendingActions, msg, env.bcc));
            msg.addReplyTo(wrap(messageProvider, MessageHeaders.HDR_REPLY_TO, logger, pendingActions, msg, env.replyTo));
            msg.addSender(wrap(messageProvider, MessageHeaders.HDR_SENDER, logger, pendingActions, msg, env.sender));
            msg.setHeader("In-Reply-To", env.inReplyTo);
            msg.setHeader("Message-Id", env.messageId);
            msg.setSubject(decodeSubject(env.subject), true);
            msg.setSentDate(env.date);
        }

        private String decodeSubject(String envelopeSubject) {
            try {
                return MimeMessageUtility.decodeEnvelopeSubject(envelopeSubject);
            } catch (Exception e) {
                LOG.warn("Failed to decode subject from ENVELOPE fetch item: '{}'. Using raw one instead.", envelopeSubject, e);
                return envelopeSubject;
            }
        }

        private static final Map<String, BiConsumer<InternetAddress[], IDMailMessage>> ADDRESS_APPLIERS = Map.ofEntries(
            Map.entry(MessageHeaders.HDR_FROM, (internetAddresses, msg) -> {
                msg.removeFrom();
                msg.addFrom(internetAddresses);
            }),
            Map.entry(MessageHeaders.HDR_TO, (internetAddresses, msg) -> {
                msg.removeTo();
                msg.addTo(internetAddresses);
            }),
            Map.entry(MessageHeaders.HDR_CC, (internetAddresses, msg) -> {
                msg.removeCc();
                msg.addCc(internetAddresses);
            }),
            Map.entry(MessageHeaders.HDR_BCC, (internetAddresses, msg) -> {
                msg.removeBcc();
                msg.addBcc(internetAddresses);
            }),
            Map.entry(MessageHeaders.HDR_REPLY_TO, (internetAddresses, msg) -> {
                msg.removeReplyTo();
                msg.addReplyTo(internetAddresses);
            }),
            Map.entry(MessageHeaders.HDR_SENDER, (internetAddresses, msg) -> {
                msg.removeSender();
                msg.addSender(internetAddresses);
            })
        );

        private InternetAddress[] wrap(MessageProvider messageProvider, String headerName, org.slf4j.Logger logger, List<Runnable> pendingActions, IDMailMessage msg, InternetAddress... addresses) {
            if (null == addresses || 0 == addresses.length) {
                return null;
            }

            int numberOfAddresses = addresses.length;
            InternetAddress[] ret = new InternetAddress[numberOfAddresses];
            boolean actionAdded = false;
            for (int i = numberOfAddresses; i-- > 0;) {
                if (!actionAdded && messageProvider != null && MimeMessageUtility.isDummyDomain(addresses[i].getAddress())) {
                    // Address appears to be corruptly parsed by IMAP server; e.g "... (("Jane Doe" NIL "MISSING_MAILBOX" "SYNTAX_ERROR")) ..."
                    Runnable r = () -> {
                        try {
                            InternetAddress[] internetAddresses = MimeMessageUtility.getAddressHeader(headerName, messageProvider.getMessage());
                            BiConsumer<InternetAddress[], IDMailMessage> addrApplier = ADDRESS_APPLIERS.get(headerName);
                            if (addrApplier != null) {
                                addrApplier.accept(internetAddresses, msg);
                            }
                        } catch (Exception e) {
                            logger.warn("Failed to apply {} header", headerName, e);
                        }
                    };
                    pendingActions.add(r);
                    actionAdded = true;
                }

                // Common handling
                String sAddress = addresses[i].toString();
                try {
                    ret[i] = new QuotedInternetAddress(sAddress, false);
                } catch (AddressException e) {
                    // Use as-is
                    LOG.debug("Failed to parse E-Mail address string: {}", sAddress, e);
                    String parsed = e.getRef();
                    ret[i] = new PlainTextAddress(null == parsed ? sAddress : parsed);
                }
            }
            return ret;
        }

        @Override
        public void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws MessagingException {
            msg.addFrom((InternetAddress[]) message.getFrom());
            msg.addTo((InternetAddress[]) message.getRecipients(RecipientType.TO));
            msg.addCc((InternetAddress[]) message.getRecipients(RecipientType.CC));
            msg.addBcc((InternetAddress[]) message.getRecipients(RecipientType.BCC));
            msg.addReplyTo((InternetAddress[]) message.getReplyTo());
            String[] header = message.getHeader("In-Reply-To");
            if (null != header && header.length > 0) {
                msg.addHeader("In-Reply-To", header[0]);
            }
            header = message.getHeader("Message-Id");
            if (null != header && header.length > 0) {
                msg.addHeader("Message-Id", header[0]);
            }
            header = message.getHeader("Subject");
            if (null != header && header.length > 0) {
                msg.setSubject(MimeMessageUtility.decodeMultiEncodedHeader(header[0]), true);
            }
            header = message.getHeader("Sender");
            if (null != header && header.length > 0) {
                msg.addSender(MimeMessageUtility.getAddressHeader(header[0]));
            }
            msg.setSentDate(message.getSentDate());
        }
    };

    private static final FetchItemHandler INTERNALDATE_ITEM_HANDLER = new FetchItemHandler() {

        @Override
        public void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) {
            msg.setReceivedDate(((INTERNALDATE) item).getDate());
        }

        @Override
        public void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws MessagingException {
            msg.setReceivedDate(message.getReceivedDate());
        }
    };

    private static final FetchItemHandler SIZE_ITEM_HANDLER = new FetchItemHandler() {

        @Override
        public void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) {
            msg.setSize(((RFC822SIZE) item).size);
        }

        @Override
        public void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws MessagingException {
            msg.setSize(message.getSize());
        }
    };

    // ------------------------------------------------------------------------------------------------------------------------------------------------

    private static final class BODYSTRUCTUREFetchItemHandler implements FetchItemHandler {

        private static final BODYSTRUCTUREFetchItemHandler DEFAULT_INSTANCE = new BODYSTRUCTUREFetchItemHandler(false, false, false);

        static BODYSTRUCTUREFetchItemHandler getInstance(boolean checkICal, boolean checkVCard, boolean treatEmbeddedAsAttachment) {
            if (checkICal || checkVCard || treatEmbeddedAsAttachment) {
                return new BODYSTRUCTUREFetchItemHandler(checkICal, checkVCard, treatEmbeddedAsAttachment);
            }
            return DEFAULT_INSTANCE;
        }

        // ---------------------------------------------------------------------------------------------------------------------------------

        final boolean checkICal;
        final boolean checkVCard;
        final boolean treatEmbeddedAsAttachment;

        private BODYSTRUCTUREFetchItemHandler(boolean checkICal, boolean checkVCard, boolean treatEmbeddedAsAttachment) {
            super();
            this.checkICal = checkICal;
            this.checkVCard = checkVCard;
            this.treatEmbeddedAsAttachment = treatEmbeddedAsAttachment;
        }

        @Override
        public void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) throws OXException {
            BODYSTRUCTURE bs = (BODYSTRUCTURE) item;

            ContentType contentType = MimeMessageUtility.parseContentType(bs, msg);
            msg.setContentType(contentType);
            msg.addHeader("Content-Type", contentType.toString(true));

            ContentDisposition contentDisposition = MimeMessageUtility.parseContentDisposition(bs, msg);
            msg.setContentDisposition(contentDisposition);
            msg.addHeader("Content-Disposition", contentDisposition.toString(true));

            msg.setMailStructure(MimeMessageUtility.parseMailStructure(bs, contentType, contentDisposition, msg));

            if (msg.containsHasAttachment()) {
                boolean hasAttachment = msg.isHasAttachment();
                if (hasAttachment) {
                    if (checkICal && hasICal(bs)) {
                        msg.addHeader("X-ICAL", "true");
                    }

                    if (checkVCard && hasVCard(bs)) {
                        msg.addHeader("X-VCARD", "true");
                    }
                }
            } else {
                boolean hasAttachment = MimeMessageUtility.hasAttachments(bs);
                if (hasAttachment) {
                    if (checkICal && hasICal(bs)) {
                        msg.addHeader("X-ICAL", "true");
                    }

                    if (checkVCard && hasVCard(bs)) {
                        msg.addHeader("X-VCARD", "true");
                    }
                } else if (treatEmbeddedAsAttachment) {
                    hasAttachment = hasEmbedded(bs);
                }
                msg.setAlternativeHasAttachment(hasAttachment);
            }
        }

        boolean hasICal(BODYSTRUCTURE bs) {
            String baseContentType = bs.type + "/" + bs.subtype;
            if (baseContentType.indexOf("calendar") >= 0 || baseContentType.indexOf("ics") >= 0) {
                return true;
            }

            BODYSTRUCTURE[] subs = bs.bodies;
            if (null != subs) {
                for (BODYSTRUCTURE sub : subs) {
                    if (hasICal(sub)) {
                        return true;
                    }
                }
            }

            return false;
        }

        boolean hasVCard(BODYSTRUCTURE bs) {
            String baseContentType = bs.type + "/" + bs.subtype;
            if (baseContentType.indexOf("card") >= 0 || baseContentType.indexOf("vcf") >= 0) {
                return true;
            }

            BODYSTRUCTURE[] subs = bs.bodies;
            if (null != subs) {
                for (BODYSTRUCTURE sub : subs) {
                    if (hasVCard(sub)) {
                        return true;
                    }
                }
            }

            return false;
        }

        boolean hasEmbedded(BODYSTRUCTURE bs) {
            if ("image".equals(bs.type) && Strings.isNotEmpty(bs.id)) {
                return true;
            }

            BODYSTRUCTURE[] subs = bs.bodies;
            if (null != subs) {
                for (BODYSTRUCTURE sub : subs) {
                    if (hasEmbedded(sub)) {
                        return true;
                    }
                }
            }

            return false;
        }

        @Override
        public void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws OXException {
            String contentType = null;
            try {
                try {
                    contentType = message.getContentType();
                } catch (@SuppressWarnings("unused") MessagingException e) {
                    String[] header = message.getHeader("Content-Type");
                    if (null != header && header.length > 0) {
                        contentType = header[0];
                    }
                }
                msg.setAlternativeHasAttachment(null == contentType ? false : MimeMessageUtility.hasAttachments(message, contentType));
            } catch (MessagingException e) {
                if (null == contentType) {
                    throw MimeMailException.handleMessagingException(e);
                }
                // Don't know better...
                msg.setAlternativeHasAttachment(Strings.asciiLowerCase(contentType).startsWith("multipart/mixed"));
            } catch (IOException e) {
                if ((e.getCause() instanceof com.sun.mail.util.MessageRemovedIOException) || (e.getCause() instanceof MessageRemovedException)) {
                    throw MailExceptionCode.MAIL_NOT_FOUND_SIMPLE.create(e);
                }
                throw MailExceptionCode.IO_ERROR.create(e, e.getMessage());
            }
        }
    }

    // ------------------------------------------------------------------------------------------------------------------------------------------------

    private static final FetchItemHandler UID_ITEM_HANDLER = new FetchItemHandler() {

        @Override
        public void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) {
            long id = ((UID) item).uid;
            String sUid = Long.toString(id);
            msg.setMailId(sUid);
            msg.setUid(id);
            LogProperties.put(LogProperties.Name.MAIL_MAIL_ID, sUid);
        }

        @Override
        public void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws MessagingException {
            long id = ((IMAPFolder) message.getFolder()).getUID(message);
            msg.setMailId(Long.toString(id));
            msg.setUid(id);
        }
    };

    private static final FetchItemHandler X_REAL_UID_ITEM_HANDLER = new FetchItemHandler() {

        @Override
        public void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) {
            long originalUid = ((X_REAL_UID) item).uid;
            msg.setOriginalUid(originalUid);
        }

        @Override
        public void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws MessagingException {
            // Nothing
        }
    };

    private static final FetchItemHandler X_MAILBOX_ITEM_HANDLER = new FetchItemHandler() {

        @Override
        public void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) {
            String mailbox = ((com.sun.mail.imap.protocol.X_MAILBOX) item).mailbox;
            msg.setOriginalFolder(new FullnameArgument(msg.getAccountId(), mailbox));
        }

        @Override
        public void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws MessagingException {
            // Nothing
        }
    };

    private static final FetchItemHandler X_GUID_ITEM_HANDLER = new FetchItemHandler() {

        @Override
        public void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) {
            msg.setGuid(((X_GUID) item).guid);
        }

        @Override
        public void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws MessagingException {
            // Nothing
        }
    };

    private static final FetchItemHandler SNIPPET_ITEM_HANDLER = new FetchItemHandler() {

        @Override
        public void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) {
            String textPreview = ((SNIPPET) item).getText();
            msg.setTextPreview(textPreview);
        }

        @Override
        public void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws MessagingException {
            // Nothing
        }
    };

    private static final FetchItemHandler PREVIEW_ITEM_HANDLER = new FetchItemHandler() {

        @Override
        public void handleItem(Item item, IDMailMessage msg, MessageProvider messageProvider, org.slf4j.Logger logger, List<Runnable> pendingActions) {
            String textPreview = ((PREVIEW) item).getText();
            msg.setTextPreview(textPreview);
        }

        @Override
        public void handleMessage(Message message, IDMailMessage msg, org.slf4j.Logger logger) throws MessagingException {
            // Nothing
        }
    };

    /** The mapping of static fetch item handler to associated {@link Item item} class */
    private static final Map<Class<? extends Item>, FetchItemHandler> FETCH_ITEM_HANDLER_BY_CLASS = Map.of(
        UID.class, UID_ITEM_HANDLER,
        X_REAL_UID.class, X_REAL_UID_ITEM_HANDLER,
        X_MAILBOX.class, X_MAILBOX_ITEM_HANDLER,
        X_GUID.class, X_GUID_ITEM_HANDLER,
        SNIPPET.class, SNIPPET_ITEM_HANDLER,
        PREVIEW.class, PREVIEW_ITEM_HANDLER,
        INTERNALDATE.class, INTERNALDATE_ITEM_HANDLER,
        ENVELOPE.class, ENVELOPE_ITEM_HANDLER,
        RFC822SIZE.class, SIZE_ITEM_HANDLER
    );

    /*-
     * ++++++++++++++ End of item handlers ++++++++++++++
     */

    /**
     * This is the Envelope item. Despite of JavaMail's ENVELOPE item, this item does not include INTERNALDATE nor RFC822.SIZE; it solely
     * consists of the ENVELOPE.
     * <p>
     * The Envelope is an aggregation of the common attributes of a Message. Implementations should include the following attributes: From,
     * To, Cc, Bcc, ReplyTo, Subject and Date. More items may be included as well.
     */
    public static final FetchProfile.Item ENVELOPE_ONLY = MimeStorageUtility.ENVELOPE_ONLY;

    /**
     * This is the INTERNALDATE item.
     */
    public static final FetchProfile.Item INTERNALDATE = MimeStorageUtility.INTERNALDATE;

    /**
     * This is the X-MAILBOX item.
     */
    public static final FetchProfile.Item ORIGINAL_MAILBOX = MimeStorageUtility.ORIGINAL_MAILBOX;

    /**
     * This is the X-REAL-UID item.
     */
    public static final FetchProfile.Item ORIGINAL_UID = MimeStorageUtility.ORIGINAL_UID;

    /**
     * This is the X-GUID item.
     */
    public static final FetchProfile.Item GUID = MimeStorageUtility.GUID;

    /**
     * Turns given fetch profile into FETCH items to craft a FETCH command.
     *
     * @param isRev1 Whether IMAP protocol is revision 1 or not
     * @param fp The fetch profile to convert
     * @param loadBody <code>true</code> if message body should be loaded; otherwise <code>false</code>
     * @param serverInfo The IMAP server information
     * @param previewMode Whether target IMAP server supports any preview capability
     * @return The FETCH items to craft a FETCH command
     */
    public static String getFetchCommand(boolean isRev1, FetchProfile fp, boolean loadBody, IMAPServerInfo serverInfo, PreviewMode previewMode) {
        StringBuilder command = new StringBuilder(128);
        boolean sizeIncluded;
        if (fp.contains(FetchProfile.Item.ENVELOPE)) {
            if (loadBody) {
                command.append("INTERNALDATE");
                sizeIncluded = false;
            } else {
                command.append("ENVELOPE INTERNALDATE RFC822.SIZE");
                sizeIncluded = true;
            }
        } else if (fp.contains(ENVELOPE_ONLY)) {
            if (loadBody) {
                command.append("INTERNALDATE");
            } else {
                command.append("ENVELOPE INTERNALDATE");
            }
            sizeIncluded = false;
        } else if (fp.contains(INTERNALDATE)) {
            command.append("INTERNALDATE");
            sizeIncluded = false;
        } else {
            command.append("INTERNALDATE");
            sizeIncluded = false;
        }
        if (fp.contains(FetchProfile.Item.FLAGS)) {
            command.append(" FLAGS");
        }
        if (fp.contains(FetchProfile.Item.CONTENT_INFO)) {
            command.append(" BODYSTRUCTURE");
        }
        boolean uidIncluded = fp.contains(UIDFolder.FetchProfileItem.UID);
        if (uidIncluded) {
            command.append(" UID");
        }

        Map<String, String> capabilities = null == serverInfo ? null : serverInfo.getCapabilities();

        // Decide per IMAP server
        if (fp.contains(ORIGINAL_MAILBOX)) {
            if (null != capabilities && capabilities.containsKey(IMAPCapabilities.CAP_XDOVECOT)) {
                command.append(" X-MAILBOX");
            } else if (!uidIncluded) {
                command.append(" UID");
            }
        }

        // Decide per IMAP server
        if (fp.contains(ORIGINAL_UID)) {
            if (null != capabilities && capabilities.containsKey(IMAPCapabilities.CAP_XDOVECOT)) {
                command.append(" X-REAL-UID");
            }
        }

        // Decide per IMAP server
        if (fp.contains(GUID)) {
            if (null != capabilities && capabilities.containsKey(IMAPCapabilities.CAP_XDOVECOT)) {
                command.append(" X-GUID");
            }
        }

        switch (previewMode) {
            case NONE:
                break;
            case PREVIEW_FUZZY:
                if (fp.contains(IMAPFolder.PreviewFetchProfileItem.PREVIEW_LAZY)) {
                    if (null != capabilities && capabilities.containsKey(previewMode.getCapabilityName())) {
                        command.append(" PREVIEW (LAZY=FUZZY)");
                    }
                } else if (fp.contains(IMAPFolder.PreviewFetchProfileItem.PREVIEW)) {
                    if (null != capabilities && capabilities.containsKey(previewMode.getCapabilityName())) {
                        command.append(" PREVIEW (FUZZY)");
                    }
                }
                break;
            case PREVIEW_RFC8970:
                if (fp.contains(IMAPFolder.Rfc8970PreviewFetchProfileItem.PREVIEW_LAZY)) {
                    if (null != capabilities && capabilities.containsKey(previewMode.getCapabilityName())) {
                        command.append(" PREVIEW (LAZY)");
                    }
                } else if (fp.contains(IMAPFolder.Rfc8970PreviewFetchProfileItem.PREVIEW)) {
                    if (null != capabilities && capabilities.containsKey(previewMode.getCapabilityName())) {
                        command.append(" PREVIEW");
                    }
                }
                break;
            case SNIPPET_FUZZY:
                if (fp.contains(IMAPFolder.SnippetFetchProfileItem.SNIPPETS_LAZY)) {
                    if (null != capabilities && capabilities.containsKey(previewMode.getCapabilityName())) {
                        command.append(" SNIPPET (LAZY=FUZZY)");
                    }
                } else if (fp.contains(IMAPFolder.SnippetFetchProfileItem.SNIPPETS)) {
                    if (null != capabilities && capabilities.containsKey(previewMode.getCapabilityName())) {
                        command.append(" SNIPPET (FUZZY)");
                    }
                }
                break;
            default:
                break;
        }

        boolean allHeaders = (fp.contains(IMAPFolder.FetchProfileItem.HEADERS) && !loadBody);
        if (allHeaders) {
            if (isRev1) {
                command.append(" BODY.PEEK[HEADER]");
            } else {
                command.append(" RFC822.HEADER");
            }
        }
        if (!sizeIncluded && fp.contains(FetchProfile.Item.SIZE)) {
            command.append(" RFC822.SIZE");
        }
        /*
         * If we're not fetching all headers, fetch individual headers
         */
        if (!allHeaders && !loadBody) {
            String[] hdrs = fp.getHeaderNames();
            if (hdrs.length > 0) {
                command.append(' ');
                if (isRev1) {
                    command.append("BODY.PEEK[HEADER.FIELDS (");
                } else {
                    command.append("RFC822.HEADER.LINES (");
                }
                command.append(hdrs[0]);
                for (int i = 1; i < hdrs.length; i++) {
                    command.append(' ');
                    command.append(hdrs[i]);
                }
                if (isRev1) {
                    command.append(")]");
                } else {
                    command.append(')');
                }
            }
        }
        if (loadBody) {
            /*
             * Load full message
             */
            if (isRev1) {
                command.append(" BODY.PEEK[]");
            } else {
                command.append(" RFC822");
            }
        }
        return command.toString();
    }

    /**
     * Strips BODYSTRUCTURE item from given fetch profile.
     *
     * @param fetchProfile The fetch profile
     * @return The fetch profile with BODYSTRUCTURE item stripped
     */
    public static FetchProfile getSafeFetchProfile(FetchProfile fetchProfile) {
        if (fetchProfile.contains(FetchProfile.Item.CONTENT_INFO)) {
            FetchProfile newFetchProfile = new FetchProfile();
            newFetchProfile.add("Content-Type");
            if (!fetchProfile.contains(UIDFolder.FetchProfileItem.UID)) {
                newFetchProfile.add(UIDFolder.FetchProfileItem.UID);
            }
            FetchProfile.Item[] items = fetchProfile.getItems();
            for (FetchProfile.Item item : items) {
                if (!FetchProfile.Item.CONTENT_INFO.equals(item)) {
                    newFetchProfile.add(item);
                }
            }
            String[] names = fetchProfile.getHeaderNames();
            for (String name : names) {
                newFetchProfile.add(name);
            }
            return newFetchProfile;
        }
        return fetchProfile;
    }

    /**
     * Gets the item associated with given class in specified <i>FETCH</i> response.
     *
     * @param <I> The returned item's class
     * @param clazz The item class to look for
     * @param fetchResponse The <i>FETCH</i> response
     * @return The item associated with given class in specified <i>FETCH</i> response or <code>null</code>.
     */
    protected static <I extends Item> I getItemOf(Class<? extends I> clazz, FetchResponse fetchResponse) {
        for (Iterator<Item> it = fetchResponse.itemsIterator(); it.hasNext();) {
            Item item = it.next();
            if (clazz.isInstance(item)) {
                return clazz.cast(item);
            }
        }
        return null;
    }

}
