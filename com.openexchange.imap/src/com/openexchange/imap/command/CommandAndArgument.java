/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imap.command;

import com.sun.mail.iap.Argument;

/**
 * {@link CommandAndArgument} . A simple pair for command string and optional IMAP argument(s).
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class CommandAndArgument {

    /**
     * Creates a new instance only containing given command string.
     *
     * @param command The command string
     * @return The newly created instance
     */
    public static CommandAndArgument instanceFor(String command) {
        return new CommandAndArgument(command, null);
    }

    private final String command;
    private final Argument args;

    /**
     * Initializes a new {@link CommandAndArgument}.
     *
     * @param command The command string
     * @param args The optional argument(s)
     */
    public CommandAndArgument(String command, Argument args) {
        super();
        this.command = command;
        this.args = args;
    }

    /**
     * Gets the command
     *
     * @return The command
     */
    public String getCommand() {
        return command;
    }

    /**
     * Gets the optional argument(s)
     *
     * @return The argument(s) or <code>null</code>
     */
    public Argument getArgs() {
        return args;
    }

}
