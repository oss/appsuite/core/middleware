/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imap.util;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.imap.util.ImapUtility.prepareImapCommandForLogging;
import static com.openexchange.java.Strings.isEmpty;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import javax.mail.MessagingException;
import org.json.JSONObject;
import org.slf4j.Logger;
import com.google.common.collect.ImmutableMap;
import com.openexchange.ajax.customizer.AdditionalFieldsUtils;
import com.openexchange.ajax.customizer.folder.AdditionalFolderField;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.cache.v2.filter.VersionName;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.Folder;
import com.openexchange.imap.IMAPCapabilities;
import com.openexchange.imap.IMAPCommandsCollection;
import com.openexchange.imap.IMAPFolderStorage;
import com.openexchange.imap.services.Services;
import com.openexchange.java.Strings;
import com.openexchange.log.LogProperties;
import com.openexchange.mail.FullnameArgument;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.mail.mime.MimeMailException;
import com.openexchange.mail.utils.MailFolderUtility;
import com.openexchange.mailaccount.Account;
import com.openexchange.tools.session.ServerSession;
import com.sun.mail.iap.BadCommandException;
import com.sun.mail.iap.ParsingException;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;
import com.sun.mail.imap.protocol.BASE64MailboxDecoder;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.IMAPResponse;


/**
 * {@link ExtAccountFolderField}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ExtAccountFolderField implements AdditionalFolderField {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ExtAccountFolderField.class);

    /**
     * The cache version number
     */
    static final VersionName VERSION = CoreModuleName.IMAP_EXTERNAL_ACCOUNT_FOLDERS.getVersionName();

    private final CacheOptions<Map<String, FolderInfo>> cacheOptions;

    /**
     * Initializes a new {@link ExtAccountFolderField}.
     */
    public ExtAccountFolderField() {
        super();
        cacheOptions = CacheOptions.<Map<String,FolderInfo>> builder()
            .withCoreModuleName(CoreModuleName.IMAP_EXTERNAL_ACCOUNT_FOLDERS)
            .withCodecAndVersion(new ExtAccountFolderCodec())
            .build();
    }

    private Map<String, FolderInfo> getExternalAccountFolders(ServerSession session) throws OXException {
        CacheService cacheService = Services.getService(CacheService.class);
        if (cacheService == null) {
            // No cache service available
            return determineExternalAccountFolders(session);
        }

        // Get from cache
        Cache<Map<String, FolderInfo>> cache = cacheService.getCache(cacheOptions);
        CacheKey key = cache.newKey(asHashPart(session.getContextId()), Integer.toString(session.getUserId()));
        return cache.get(key, k -> determineExternalAccountFolders(session));
    }

    private static Map<String, FolderInfo> determineExternalAccountFolders(ServerSession session) throws OXException {
        IMAPFolderStorage folderStorage = null;
        MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> mailAccess = null;
        try {
            mailAccess = MailAccess.getInstance(session, Account.DEFAULT_ID);
            mailAccess.connect();
            folderStorage = com.openexchange.imap.IMAPAccess.getIMAPFolderStorageFrom(mailAccess);
            IMAPStore imapStore = folderStorage.getImapStore();
            return getExternalAccountFolders((IMAPFolder) imapStore.getDefaultFolder());
        } catch (MessagingException e) {
            throw folderStorage.handleMessagingException(e);
        } finally {
            if (mailAccess != null) {
                mailAccess.close(true);
            }
        }
    }

    @Override
    public Object renderJSON(AJAXRequestData requestData, Object value) {
        return value == null ? JSONObject.NULL : value;
    }

    @Override
    public Object getValue(Folder folder, ServerSession session) {
        String fullName = folder.getID();
        if (Strings.isEmpty(fullName)) {
            return JSONObject.NULL;
        }

        Optional<FullnameArgument> optional = MailFolderUtility.optPrepareMailFolderParam(fullName);
        if (!optional.isPresent()) {
            return JSONObject.NULL;
        }

        FullnameArgument fa = optional.get();
        if (fa.getAccountId() != Account.DEFAULT_ID) {
            return JSONObject.NULL;
        }

        try {
            FolderInfo folderInfo = getExternalAccountFolders(session).get(fa.getFullName());
            if (null == folderInfo) {
                return JSONObject.NULL;
            }

            JSONObject jResult = new JSONObject(2);
            if (null != folderInfo.alias) {
                jResult.put("alias", folderInfo.alias);
            }
            if (null != folderInfo.externalAccount) {
                jResult.put("externalAccount", folderInfo.externalAccount);
            }
            return jResult;
        } catch (@SuppressWarnings("unused") Exception e) {
            return null;
        }
    }

    @Override
    public String getColumnName() {
        return "com.openexchange.imap.extAccount";
    }

    @Override
    public int getColumnID() {
        return 3050;
    }

    @Override
    public List<Object> getValues(List<Folder> folders, ServerSession session) {
        return AdditionalFieldsUtils.bulk(this, folders, session);
    }

    // -------------------------------------------------------------------------------------------------------------

    private static class ExtAccountFolderCodec extends AbstractJSONObjectCacheValueCodec<Map<String, FolderInfo>> {

        private final UUID codecId;

        /**
         * Initializes a new {@link ExtAccountFolderCodec}.
         */
        ExtAccountFolderCodec() {
            super();
            this.codecId = UUID.randomUUID();
        }

        @Override
        public UUID getCodecId() {
            return codecId;
        }

        @Override
        public int getVersionNumber() {
            return VERSION.getVersionNumber();
        }

        @Override
        protected JSONObject writeJson(Map<String, FolderInfo> map) throws Exception {
            int size = map.size();
            if (size <= 0) {
                return JSONObject.EMPTY_OBJECT;
            }
            JSONObject jMap = new JSONObject(size);
            for (Map.Entry<String, FolderInfo> mapEntry : map.entrySet()) {
                FolderInfo folderInfo = mapEntry.getValue();
                jMap.put(mapEntry.getKey(), new JSONObject(2).putOpt("e", folderInfo.externalAccount).putOpt("a", folderInfo.alias));
            }
            return jMap;
        }

        @Override
        protected Map<String, FolderInfo> parseJson(JSONObject jMap) throws Exception {
            int length = jMap.length();
            if (length <= 0) {
                return Collections.emptyMap();
            }
            ImmutableMap.Builder<String, FolderInfo> map = ImmutableMap.builderWithExpectedSize(length);
            for (Map.Entry<String, Object> jMapEntry : jMap.entrySet()) {
                JSONObject jFolderInfo = (JSONObject) jMapEntry.getValue();
                map.put(jMapEntry.getKey(), new FolderInfo(jFolderInfo.optString("e", null), jFolderInfo.optString("a", null)));
            }
            return map.build();
        }
    }

    private static final class FolderInfo {

        final String externalAccount;
        final String alias;

        FolderInfo(String externalAccount, String alias) {
            super();
            this.externalAccount = externalAccount;
            this.alias = alias;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder(48);
            builder.append('{');
            builder.append("externalAccount=").append(externalAccount).append(", ");
            builder.append("alias=").append(alias);
            builder.append('}');
            return builder.toString();
        }
    }

    /**
     * Get the external account folders
     *
     * @param imapFolder
     * @return
     * @throws OXException
     */
    private static Map<String, FolderInfo> getExternalAccountFolders(IMAPFolder imapFolder) throws OXException {
        try {
            final Map<String, FolderInfo> results = HashMap.newHashMap(8);
            imapFolder.doCommand(new IMAPFolder.ProtocolCommand() {

                @Override
                public Object doCommand(IMAPProtocol protocol) throws ProtocolException {
                    if (!protocol.hasCapability(IMAPCapabilities.CAP_METADATA)) {
                        // No support for METADATA extension
                        LOG.debug("METADATA not supported by IMAP server {}", protocol.getHost());
                        return null;
                    }

                    // Perform command
                    String command = "GETMETADATA * (/shared/vendor/vendor.dovecot/ext-account /shared/vendor/vendor.dovecot/alias)";
                    com.sun.mail.iap.Response[] r = IMAPCommandsCollection.performCommand(protocol, command);
                    int mlen = r.length - 1;
                    com.sun.mail.iap.Response response = r[mlen];

                    // Also consider NO responses here
                    if (response.isOK() || response.isNO()) {
                        String cMetadata = "METADATA";
                        for (int i = 0; i < mlen; i++) {
                            if (!(r[i] instanceof IMAPResponse)) {
                                continue;
                            }

                            IMAPResponse ir = (IMAPResponse) r[i];
                            if (ir.keyEquals(cMetadata)) {
                                // E.g.
                                // * METADATA INBOX/alias@orange.fr (/shared/vendor/vendor.dovecot/ext-account NIL /shared/vendor/vendor.dovecot/alias {15} alias@orange.fr)
                                // * METADATA INBOX/teppo.testaaja.in@gmail.com (/shared/vendor/vendor.dovecot/ext-account {27} teppo.testaaja.in@gmail.com /shared/vendor/vendor.dovecot/alias NIL)
                                // * METADATA INBOX/QUARANTAINE (/shared/vendor/vendor.dovecot/ext-account NIL /shared/vendor/vendor.dovecot/alias NIL)
                                String fullName = ir.readAtomString();
                                if (!ir.supportsUtf8()) {
                                    fullName = BASE64MailboxDecoder.decode(fullName);
                                }
                                fullName = javax.mail.util.Interners.internFullName(fullName);
                                String[] metadatas = ir.readAtomStringList();
                                int length = metadatas == null ? -1 : metadatas.length;
                                int index = 0;

                                String extAccount = null;
                                String alias = null;

                                while (index < length) {
                                    @SuppressWarnings("null") String name = metadatas[index++]; // Cannot be null
                                    if ("/shared/vendor/vendor.dovecot/ext-account".equals(name)) {
                                        String value = metadatas[index++];
                                        if (!"NIL".equalsIgnoreCase(value)) {
                                            if (value.startsWith("{")) {
                                                extAccount = metadatas[index++];
                                            } else {
                                                extAccount = value;
                                            }
                                        }
                                    } else if ("/shared/vendor/vendor.dovecot/alias".equals(name)) {
                                        String value = metadatas[index++];
                                        if (!"NIL".equalsIgnoreCase(value)) {
                                            if (value.startsWith("{")) {
                                                alias = metadatas[index++];
                                            } else {
                                                alias = value;
                                            }
                                        }
                                    }
                                }

                                if (null != extAccount || null != alias) {
                                    results.put(fullName, new FolderInfo(extAccount, alias));
                                }
                                r[i] = null;
                            }
                        }

                        // Dispatch remaining untagged responses
                        protocol.notifyResponseHandlers(r);

                        return null;
                    } else if (response.isBAD()) {
                        LogProperties.putProperty(LogProperties.Name.MAIL_COMMAND, prepareImapCommandForLogging(command));
                        LogProperties.putProperty(LogProperties.Name.MAIL_FULL_NAME, imapFolder.getFullName());
                        throw new BadCommandException(response);
                    } else {
                        LogProperties.putProperty(LogProperties.Name.MAIL_COMMAND, prepareImapCommandForLogging(command));
                        LogProperties.putProperty(LogProperties.Name.MAIL_FULL_NAME, imapFolder.getFullName());
                        protocol.handleResult(response);
                    }
                    return null;
                }
            });
            return results;
        } catch (MessagingException e) {
            throw MimeMailException.handleMessagingException(e);
        }
    }

    /**
     * Parse the metadata response from the IMAP server
     *
     * @param metadataResponse
     * @param results
     * @throws ParsingException
     */
    void parseMetadataResponse(com.sun.mail.iap.Response metadataResponse, Map<String, String> results) throws ParsingException {
        if (null == metadataResponse) {
            throw new ParsingException("Parse error in METADATA response: No opening parenthesized list found.");
        }
        int cnt = 0;
        {
            final String resp = metadataResponse.toString();
            if (isEmpty(resp)) {
                throw new ParsingException("Parse error in METADATA response: No opening parenthesized list found.");
            }
            int pos = -1;
            while ((pos = resp.indexOf('(', pos + 1)) > 0) {
                cnt++;
            }
        }
        if (cnt <= 0) {
            throw new ParsingException("Parse error in STATUS response: No opening parenthesized list found.");
        }
        // Read full name; decode the name (using RFC2060's modified UTF7)
        metadataResponse.skipSpaces();
        String fullName = metadataResponse.readAtomString();
        if (!metadataResponse.supportsUtf8()) {
            fullName = BASE64MailboxDecoder.decode(fullName);
        }
        fullName = javax.mail.util.Interners.internFullName(fullName);

        // Read until opening parenthesis or EOF
        byte b = 0;
        do {
            b = metadataResponse.readByte();
            if (b == '(' && --cnt > 0) {
                b = metadataResponse.readByte();
            }
        } while (b != 0 && b != '(');
        if (0 == b || cnt > 0) {
            // EOF
            throw new ParsingException("Parse error in STATUS response: No opening parenthesized list found.");
        }

        // Parse parenthesized list
        // * METADATA INBOX.jane@barfoo.org (/shared/vendor/vendor.dovecot/ext-account {15} jane@barfoo.org)
        {
            String attr = metadataResponse.readAtom();
            if ("/shared/vendor/vendor.dovecot/ext-account".equals(attr)) {
                String literal = metadataResponse.readString();
                if (null != literal) {
                    String addr = metadataResponse.readAtomString();
                    if (null != addr) {
                        results.put(fullName, addr);
                        return;
                    }
                }
            }
        }
    }

}
