/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imap.util;

import static com.openexchange.java.MessageFormatter.format;

/**
 * {@link ReceivedDateInconsistentException} - Thrown when an inconsistent received date has been detected.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class ReceivedDateInconsistentException extends Exception {

    private static final long serialVersionUID = 7641838189067060574L;

    private final long previousReceivedDate;
    private final long currentReceivedDate;
    private final String mailId;
    private final String folderId;

    /**
     * Initializes a new instance of {@link ReceivedDateInconsistentException}.
     *
     * @param previousReceivedDate The previous received date (the number of milliseconds since January 1, 1970, 00:00:00 GMT)
     * @param currentReceivedDate The current received date (the number of milliseconds since January 1, 1970, 00:00:00 GMT)
     * @param mailId The mail identifier
     * @param folderId The folder identifier
     */
    public ReceivedDateInconsistentException(long previousReceivedDate, long currentReceivedDate, String mailId, String folderId) {
        super(format("Fetched received date is inconsistent for mail {} in folder {}. Previous: '{}', but now: '{}'", mailId, folderId, Long.valueOf(previousReceivedDate), Long.valueOf(currentReceivedDate)));
        this.previousReceivedDate = previousReceivedDate;
        this.currentReceivedDate = currentReceivedDate;
        this.mailId = mailId;
        this.folderId = folderId;
    }

    /**
     * Gets the previous received date (the number of milliseconds since January 1, 1970, 00:00:00 GMT).
     *
     * @return The previous received date
     */
    public long getPreviousReceivedDate() {
        return previousReceivedDate;
    }

    /**
     * Gets the current received date (the number of milliseconds since January 1, 1970, 00:00:00 GMT).
     *
     * @return The current received date
     */
    public long getCurrentReceivedDate() {
        return currentReceivedDate;
    }

    /**
     * Gets the mail identifier.
     *
     * @return The mail identifier
     */
    public String getMailId() {
        return mailId;
    }

    /**
     * Gets the folder identifier.
     *
     * @return The folder identifier
     */
    public String getFolderId() {
        return folderId;
    }

}
