/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.pns.mobile.api.facade;

import static com.openexchange.java.Autoboxing.I;
import java.util.Map;
import java.util.Set;
import com.eatthepath.pushy.apns.util.ApnsPayloadBuilder;
import com.eatthepath.pushy.apns.util.SimpleApnsPayloadBuilder;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.pns.ApnsConstants;
import com.openexchange.pns.KnownTransport;
import com.openexchange.pns.Message;
import com.openexchange.pns.PushExceptionCodes;
import com.openexchange.pns.PushMessageGenerator;
import com.openexchange.pns.PushNotification;

/**
 * {@link MobileApiFacadeMessageGenerator} - The message generator for Mobile API Facade.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @since v7.8.f
 */
public class MobileApiFacadeMessageGenerator implements PushMessageGenerator {

    private static final String TRANSPORT_ID_FCM = KnownTransport.FCM.getTransportId();
    private static final String TRANSPORT_ID_GCM = KnownTransport.GCM.getTransportId();
    private static final String TRANSPORT_ID_APNS = KnownTransport.APNS.getTransportId();
    private static final Set<String> SUPPORTED_TRANSPORTS = Set.of(TRANSPORT_ID_GCM, TRANSPORT_ID_FCM, TRANSPORT_ID_APNS);
    private static final String DEFAULT_NAMESPACE = "/";

    // ------------------------------------------------------------------------------------------------------------------------

    private final ClientConfig clientConfig;
    private final ConfigViewFactory viewFactory;

    /**
     * Initializes a new {@link MobileApiFacadeMessageGenerator}.
     *
     * @param viewFactory The service to use
     */
    public MobileApiFacadeMessageGenerator(ClientConfig clientConfig, ConfigViewFactory viewFactory) {
        super();
        this.clientConfig = clientConfig;
        this.viewFactory = viewFactory;
    }

    @Override
    public String getClient() {
        return clientConfig.getClientId();
    }

    @Override
    public boolean servesClient(String client, String transportId) {
        return getClient().equals(client) && SUPPORTED_TRANSPORTS.contains(transportId);
    }

    @Override
    public Message<?> generateMessageFor(String transportId, PushNotification notification) throws OXException {
        if (TRANSPORT_ID_GCM.equals(transportId) || TRANSPORT_ID_FCM.equals(transportId)) {
            return generateForAndroid(notification);
        } else if (TRANSPORT_ID_APNS.equals(transportId)) {
            return generateForApple(notification);
        }
        throw PushExceptionCodes.UNSUPPORTED_TRANSPORT.create(null == transportId ? "null" : transportId);
    }

    /**
     * Generates the message for Android devices
     *
     * @param notification The notification
     * @return The notification message
     */
    private Message<?> generateForAndroid(PushNotification notification) {
        return notification::getMessageData;
    }

    /**
     * Generates the message for Apple devices
     *
     * @param notification The notification
     * @return The notification message
     * @throws OXException if an error is occurred
     */
    private Message<?> generateForApple(PushNotification notification) throws OXException {
        // Build APNS payload as expected by client
        ApnsPayloadBuilder builder = new SimpleApnsPayloadBuilder();
        Map<String, Object> messageData = notification.getMessageData();
        try {
            String subject = MessageDataUtil.getSubject(messageData);
            String displayName = MessageDataUtil.getDisplayName(messageData);
            String senderAddress = MessageDataUtil.getSender(messageData);
            String sender = displayName.isEmpty() ? senderAddress : displayName;
            String folder = MessageDataUtil.getFolder(messageData);
            String id = MessageDataUtil.getId(messageData);
            String path = MessageDataUtil.getPath(messageData);
            int unread = MessageDataUtil.getUnread(messageData);

            if (Strings.isNotEmpty(subject) && Strings.isNotEmpty(sender)) {
                // Non-silent push
                StringBuilder sb = new StringBuilder(sender);
                sb.append("\n");
                sb.append(subject);
                String alertMessage = sb.toString();
                alertMessage = alertMessage.length() > ApnsConstants.APNS_MAX_ALERT_LENGTH ? alertMessage.substring(0, ApnsConstants.APNS_MAX_ALERT_LENGTH) : alertMessage;
                builder.setAlertTitle(subject);
                builder.setAlertBody(alertMessage);

                MobileApiFacadePushConfiguration config = MobileApiFacadePushConfiguration.getConfigFor(notification.getUserId(), notification.getContextId(), viewFactory);

                if (config.isApnBadgeEnabled() && unread >= 0) {
                    builder.setBadgeNumber(I(unread));
                }

                if (config.isApnSoundEnabled()) {
                    builder.setSound(config.getApnSoundFile());
                }

                builder.setCategoryName("new-message-category");
                builder.setContentAvailable(false);
            } else {
                // Silent push
                builder.setContentAvailable(true);
            }
            if (Strings.isEmpty(path) && Strings.isNotEmpty(folder) && Strings.isNotEmpty(id)) {
                path = folder + "/" + id;
            }
            if (!path.isEmpty()) {
                builder.addCustomProperty("cid", path);
            } else if (!folder.isEmpty()) {
                builder.addCustomProperty("folder", folder);
            }
            builder.addCustomProperty("namespace", DEFAULT_NAMESPACE);
            return builder::build;
        } catch (Exception e) {
            throw PushExceptionCodes.MESSAGE_GENERATION_FAILED.create(e, e.getMessage());
        }
    }
}
