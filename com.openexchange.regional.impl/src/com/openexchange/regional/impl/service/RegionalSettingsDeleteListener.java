/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.regional.impl.service;

import java.sql.Connection;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.delete.DeleteEvent;
import com.openexchange.groupware.delete.DeleteListener;
import com.openexchange.regional.impl.storage.CachingRegionalSettingStorage;

/**
 * {@link RegionalSettingsDeleteListener}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v7.10.3
 */
public class RegionalSettingsDeleteListener implements DeleteListener {

    private final CachingRegionalSettingStorage storage;

    /**
     * Initializes a new {@link RegionalSettingsDeleteListener}.
     * 
     * @param storage The underlying regional settings storage
     */
    public RegionalSettingsDeleteListener(CachingRegionalSettingStorage storage) {
        super();
        this.storage = storage;
    }

    @Override
    public void deletePerformed(DeleteEvent event, Connection readCon, Connection writeCon) throws OXException {
        if (DeleteEvent.TYPE_USER == event.getType()) {
            storage.delete(event.getContext().getContextId(), event.getId(), writeCon);
        } else if (DeleteEvent.TYPE_CONTEXT == event.getType()) {
            storage.handleDeletedContext(event.getContext().getContextId(), event.getUserIds(), writeCon);
        }
    }
}
