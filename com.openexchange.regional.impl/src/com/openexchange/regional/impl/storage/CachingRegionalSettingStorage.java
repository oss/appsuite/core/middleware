/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.regional.impl.storage;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.database.DatabaseConnectionListeners.addAfterCommitCallbackElseExecute;
import static com.openexchange.java.Autoboxing.i;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;
import com.openexchange.exception.OXException;
import com.openexchange.regional.RegionalSettings;
import com.openexchange.regional.impl.service.RegionalSettingsImpl;
import com.openexchange.server.ServiceLookup;
import com.openexchange.user.UserService;

/**
 * {@link CachingRegionalSettingStorage}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v7.10.3
 */
public class CachingRegionalSettingStorage implements RegionalSettingStorage {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(CachingRegionalSettingStorage.class);
    }

    /** Options for the regional settings cache */
    private static final CacheOptions<RegionalSettings> CACHE_OPTIONS = CacheOptions.<RegionalSettings> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.REGIONAL_SETTINGS)
        .withCodecAndVersion(new RegionalSettingsCodec())
    .build(); // formatter:on

    /** Special marker object for cached <i>empty</i> regional settings */
    private static final RegionalSettings EMPTY_SETTINGS = RegionalSettingsImpl.newBuilder().build();

    private final SQLRegionalSettingStorage storage;
    private final ServiceLookup services;

    /**
     * Initializes a new {@link CachingRegionalSettingStorage}.
     *
     * @param services A service lookup reference
     * @param storage The underlying regional settings storage
     */
    public CachingRegionalSettingStorage(ServiceLookup services, SQLRegionalSettingStorage storage) {
        super();
        this.services = services;
        this.storage = storage;
    }

    @Override
    public RegionalSettings get(int contextId, int userId) throws OXException {
        Cache<RegionalSettings> cache = optCache();
        if (null == cache) {
            return storage.get(contextId, userId);
        }
        CacheKey key = getKey(cache, contextId, userId);
        RegionalSettings value = cache.get(key, k -> Optional.ofNullable(storage.get(contextId, userId)).orElse(EMPTY_SETTINGS));
        return EMPTY_SETTINGS.equals(value) ? null : value;
    }

    @Override
    public void upsert(int contextId, int userId, RegionalSettings settings) throws OXException {
        storage.upsert(contextId, userId, settings);
        Cache<RegionalSettings> cache = optCache();
        if (null == cache) {
            return;
        }
        cache.put(getKey(cache, contextId, userId), settings);
    }

    @Override
    public void delete(int contextId, int userId) throws OXException {
        delete(contextId, userId, null);
    }

    @Override
    public void delete(int contextId, int userId, Connection writeCon) throws OXException {
        storage.delete(contextId, userId, writeCon);
        Cache<RegionalSettings> cache = optCache();
        if (null == cache) {
            return;
        }
        addAfterCommitCallbackElseExecute(writeCon, (c) -> cache.invalidate(getKey(cache, contextId, userId)));
    }

    @Override
    public void delete(int contextId) throws OXException {
        delete(contextId, null);
    }

    @Override
    public void delete(int contextId, Connection writeCon) throws OXException {
        storage.delete(contextId, writeCon);
        InvalidationCacheService invalidationService = services.getOptionalService(InvalidationCacheService.class);
        if (null == invalidationService) {
            return;
        }
        addAfterCommitCallbackElseExecute(writeCon, c -> invalidationService.invalidate(buildCacheFilter(contextId, services), false));
    }

    /**
     * Purges all regional settings from the persistent storage and invalidates caches upon deletion of a context.
     *
     * @param contextId The identifier of the deleted context
     * @param userIds The identifiers of all users in the deleted context, or <code>null</code> if not available
     * @param connection The database connection to use
     */
    public void handleDeletedContext(int contextId, List<Integer> userIds, Connection connection) throws OXException {
        /*
         * delete in storage
         */
        storage.delete(contextId, connection);
        /*
         * cleanup any leftovers in cache, using individual, full-qualified keys if possible
         */
        if (null != userIds) {
            Cache<RegionalSettings> cache = optCache();
            if (null == cache) {
                return;
            }
            List<CacheKey> keys = userIds.stream().map(userId -> getKey(cache, contextId, i(userId))).toList();
            addAfterCommitCallbackElseExecute(connection, c -> cache.invalidate(keys));
        } else {
            InvalidationCacheService invalidationService = services.getOptionalService(InvalidationCacheService.class);
            if (null == invalidationService) {
                return;
            }
            addAfterCommitCallbackElseExecute(connection, c -> invalidationService.invalidate(buildCacheFilter(contextId, services), false));
        }
    }

    ////////////////////////////////////////// HELPERS //////////////////////////////////////////////

    private Cache<RegionalSettings> optCache() {
        CacheService cacheService = services.getOptionalService(CacheService.class);
        return null != cacheService ? cacheService.getCache(CACHE_OPTIONS) : null;
    }

    private static Collection<CacheFilter> buildCacheFilter(int contextId, ServiceLookup services) {
        UserService userService = services.getOptionalService(UserService.class);
        if (userService == null) {
            return Collections.singletonList(buildCacheFilter(contextId));
        }

        try {
            int[] userIds = userService.getUserIds(contextId);
            List<CacheFilter> filters = new ArrayList<>(userIds.length);
            for (int userId : userIds) {
                filters.add(CacheFilter.builder() // @ formatter:off
                    .withModuleName(CACHE_OPTIONS.getModuleName())
                    .withVersionName(CACHE_OPTIONS.getVersionName())
                    .addSuffix(asHashPart(contextId))
                    .addSuffix(userId)
                .build()); // @ formatter:on
            }
            return filters;
        } catch (Exception e) {
            LoggerHolder.LOG.warn("Failed to acquire user identifiers for context {}. Using wild-card filter for cache invalidation instead.", Integer.valueOf(contextId), e);
        }
        return Collections.singletonList(buildCacheFilter(contextId));
    }

    private static CacheFilter buildCacheFilter(int contextId) {
        return CacheFilter.builder() // @ formatter:off
            .withModuleName(CACHE_OPTIONS.getModuleName())
            .withVersionName(CACHE_OPTIONS.getVersionName())
            .addSuffixAsWildcard(asHashPart(contextId))
        .build(); // @ formatter:on
    }

    private static CacheKey getKey(Cache<?> cache, int contextId, int userId) {
        return cache.newKey(asHashPart(contextId), Integer.toString(userId));
    }

}
