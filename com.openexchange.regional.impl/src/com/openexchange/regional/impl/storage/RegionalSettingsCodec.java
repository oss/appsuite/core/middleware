/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.regional.impl.storage;

import java.util.UUID;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;
import com.openexchange.regional.RegionalSettings;
import com.openexchange.regional.impl.service.RegionalSettingsImpl;
import com.openexchange.regional.impl.service.RegionalSettingsImpl.Builder;

/**
 * {@link RegionalSettingsCodec} - Cache codec for regional settings.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class RegionalSettingsCodec extends AbstractJSONObjectCacheValueCodec<RegionalSettings> {

    private static final UUID CODEC_ID = UUID.fromString("7e1441ad-5b99-4a95-00fc-c0e11e000001");

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONObject writeJson(RegionalSettings value) throws Exception {
        JSONObject jsonObject = new JSONObject(10);
        jsonObject.putOpt("df", value.getDateFormat());
        jsonObject.putOpt("dff", value.getDateFormatFull());
        jsonObject.putOpt("dfl", value.getDateFormatLong());
        jsonObject.putOpt("dfm", value.getDateFormatMedium());
        jsonObject.putOpt("dfs", value.getDateFormatShort());
        jsonObject.putOpt("fdw", value.getFirstDayOfWeek());
        jsonObject.putOpt("fdy", value.getFirstDayOfYear());
        jsonObject.putOpt("nf", value.getNumberFormat());
        jsonObject.putOpt("tf", value.getTimeFormat());
        jsonObject.putOpt("tfl", value.getTimeFormatLong());
        return jsonObject;
    }

    @Override
    protected RegionalSettings parseJson(JSONObject jObject) throws Exception {
        Builder builder = RegionalSettingsImpl.newBuilder();
        builder.withDateFormat(jObject.optString("df", null));
        builder.withDateFormatFull(jObject.optString("dff", null));
        builder.withDateFormatLong(jObject.optString("dfl", null));
        builder.withDateFormatMedium(jObject.optString("dfm", null));
        builder.withDateFormatShort(jObject.optString("dfs", null));
        Number firstDayOfWeek = jObject.optNumber("fdw");
        if (null != firstDayOfWeek) {
            builder.withFirstDayOfWeek(firstDayOfWeek.intValue());
        }
        Number firstDayOfYear = jObject.optNumber("fdy");
        if (null != firstDayOfYear) {
            builder.withFirstDayOfYear(firstDayOfYear.intValue());
        }
        builder.withNumberFormat(jObject.optString("nf", null));
        builder.withTimeFormat(jObject.optString("tf", null));
        builder.withTimeFormatLong(jObject.optString("tfl", null));
        return builder.build();
    }

}
