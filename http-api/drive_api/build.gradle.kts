import org.openapitools.generator.gradle.plugin.tasks.GenerateTask

repositories {
    mavenCentral()
}

plugins {
    java
    id("org.openapi.generator") version "7.0.1"
    id("com.openexchange.gradle.plugin.openapi-resolver") version "2.0.0"
    id("com.openexchange.gradle.plugin.openapi-processor") version "1.1.3"
    id("com.openexchange.gradle.plugin.openapi-replacer") version "1.0.5"
}

java {
    withSourcesJar()
}

val documentationTools: Configuration by configurations.creating
val openAPIJSONFile = buildDir.resolve("openAPI.json")

openApiValidate {
    inputSpec.set(openAPIJSONFile.absolutePath)
}

openApiGenerate {
    generatorName.set("java")
    generateAliasAsModel.set(true)
    generateApiTests.set(false)
    generateApiDocumentation.set(false)
    generateModelTests.set(false)
    generateModelDocumentation.set(false)
    configFile.set(projectDir.parentFile.resolve("client-gen/config/http_api.json").absolutePath)
    templateDir.set(projectDir.parentFile.resolve("client-gen/templates/").absolutePath)
    inputSpec.set(openAPIJSONFile.absolutePath)
    outputDir.set(buildDir.resolve("openapi-java-client").toString())
}

resolve {
    rootFile.set(project.file("index.yaml"))
    outputFile.set(file(buildDir.resolve("openAPI.json")))
}

process{
    inputFolder.set(file("../" + project.projectDir.name + "/build"))
    outputFolder.set(file("../../documentation-generic/${project.projectDir.name}"))
    fileName.set("openAPI.json")
}

replace {
    workingDir.set(file("../../documentation-generic/" + project.projectDir.name))
    outputDir.set(file("../../documentation-generic/" + project.projectDir.name))
}

dependencies {
    implementation("io.swagger:swagger-annotations:1.6.8")
    implementation("com.google.code.findbugs:jsr305:3.0.2")
    implementation("com.squareup.okhttp3:okhttp:4.11.0")
    implementation("com.squareup.okhttp3:logging-interceptor:4.11.0")
    implementation("com.google.code.gson:gson:2.9.1")
    implementation("io.gsonfire:gson-fire:1.8.5")
    implementation("javax.ws.rs:jsr311-api:1.1.1")
    implementation("javax.ws.rs:javax.ws.rs-api:2.1.1")
    implementation("org.openapitools:jackson-databind-nullable:0.2.6")
    implementation("org.apache.oltu.oauth2:org.apache.oltu.oauth2.client:1.0.2")
    implementation("org.apache.commons:commons-lang3:3.12.0")
    implementation("jakarta.annotation:jakarta.annotation-api:1.3.5")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.1")
    testImplementation("org.mockito:mockito-core:3.12.4")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.1")
}

val resolve = tasks.named("resolve", com.openexchange.gradle.plugin.resolver.ResolveTask::class.java){}

val openApiGenerate = tasks.named("openApiGenerate", GenerateTask::class.java) {
    dependsOn(resolve)
}

val process = tasks.named("process", com.openexchange.gradle.plugin.processor.ProcessTask::class.java){
    dependsOn(resolve)
}

val replace = tasks.named("replace", com.openexchange.gradle.plugin.replacer.ReplaceTask::class.java){
    dependsOn(process)
}

val generateHttpApiDoc = tasks.register("generateHttpApiDoc") {
    group = "documentation"
    description = "Build http api documentation"

    dependsOn(replace)
}

sourceSets {
    main {
        java {
            srcDir(file(openApiGenerate.get().outputDir.get()).resolve("src/main/java"))
        }
    }
    test {
        java {
            srcDir(file(openApiGenerate.get().outputDir.get()).resolve("src/test/java"))
        }
    }
}

tasks.named("compileJava", JavaCompile::class.java) {
    dependsOn(openApiGenerate)
}

val collectDependencies = tasks.register("collectDependencies", Copy::class.java) {
    from(configurations["runtimeClasspath"].files)
    into(buildDir.resolve("dependencies"))
}

tasks.named("build") {
    dependsOn(collectDependencies)
}

tasks.named("sourcesJar") {
    dependsOn(openApiGenerate)
}