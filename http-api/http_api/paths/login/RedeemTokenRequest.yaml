/login?action=redeemToken:
  post:
    operationId: redeemToken
    tags:
      - Login
    summary: Redeem Token Login.
    description: >
      With a valid session it is possible to acquire a secret (see
      `token?action=acquireToken`). Using this secret another system is able

      to generate a valid session. This session may also contain the users
      password (configurable).

      The system in question needs to be registered at the server and has to
      identify itself with a key

      configured at the open-xchange server. This is only for internal
      communication and by default no keys

      are available.
    parameters:
      - in: query
        name: authId
        description: >
          Identifier for tracing every single login request passed between
          different systems in a cluster.

          The value should be some token that is unique for every login request.
          This parameter must be

          given as URL parameter and not inside the body of the POST request.
        required: true
        schema:
          type: string
      - in: query
        name: staySignedIn
        description: Cookies will be persisted if user chooses to stay signed in. Default is `false`.
        required: false
        schema:
          type: boolean
    responses:
      '200':
        description: >
          A JSON object containing the session ID used for all subsequent
          requests. Additionally a random token is contained to be used for the Easy Login method. If configured
          by `com.openexchange.tokenlogin.[applicationId].accessPassword` even the user password will be returned. 
          In case of errors the responsible fields in the response are filled (see [Error handling](#error-handling)).
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LoginResponse'
    requestBody:
      content:
        application/x-www-form-urlencoded:
          schema:
            type: object
            title: redeemTokenBody
            properties:
              token:
                description: The token created with `token?action=acquireToken`.
                type: string
              client:
                description: >-
                  Identifier of the client using the HTTP/JSON interface. The
                  client identifier must be the same for each request after
                  creating the login session.
                type: string
              appId:
                description: >-
                  The application identifier of the token login service.
                  Applications can be defined through ` com.openexchange.tokenlogin.applications` and will be
                  announced to the client via JSLob if `com.openexchange.tokenlogin.[applicationId].announceId` is
                  set to `true`
                type: string
            required:
              - token
              - client
              - appId
