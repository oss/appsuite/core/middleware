/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2;


/**
 * {@link SuffixBuilder} - The builder for the suffix of a cache key.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public interface SuffixBuilder {

    /**
     * Checks if this suffix builder is empty.
     *
     * @return <code>true</code> if empty; otherwise <code>false</code>
     */
    boolean isEmpty();

    /**
     * Resets this suffix builder for being re-used.
     *
     * @return This builder
     */
    SuffixBuilder reset();

    /**
     * Appends given suffix parts from which the {@link Object#toString() toString()} representation is used.
     *
     * @param suffix The suffix parts
     * @return This builder
     */
    SuffixBuilder append(Object... suffix);

    /**
     * Appends given suffix part from which the {@link Integer#toString() toString()} representation is used.
     *
     * @param suffix The suffix part
     * @return This builder
     */
    SuffixBuilder append(int suffix);

    /**
     * Appends given suffix parts from which the {@link Integer#toString() toString()} representation is used.
     *
     * @param suffix The suffix parts
     * @return This builder
     */
    SuffixBuilder append(int... suffix);

    /**
     * Appends given hash parts from which the {@link Object#toString() toString()} representation is used.
     *
     * @param hashPart The hash parts
     * @return This builder
     * @throws IllegalArgumentException If this suffix builder already contains a hash part
     */
    SuffixBuilder appendHashPart(Object... hashPart);

    /**
     * Appends given hash part from which the {@link Integer#toString() toString()} representation is used.
     *
     * @param hashPart The hash part
     * @return This builder
     * @throws IllegalArgumentException If this suffix builder already contains a hash part
     */
    SuffixBuilder appendHashPart(int hashPart);

    /**
     * Appends given hash parts from which the {@link Integer#toString() toString()} representation is used.
     *
     * @param hashPart The hash parts
     * @return This builder
     * @throws IllegalArgumentException If this suffix builder already contains a hash part
     */
    SuffixBuilder appendHashPart(int... hashPart);

}
