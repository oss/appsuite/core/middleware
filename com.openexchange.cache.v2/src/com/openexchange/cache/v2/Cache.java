/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import com.openexchange.exception.OXException;

/**
 * {@link Cache} - A typed cache.
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @param <V> The type of the cache values
 */
public interface Cache<V> extends CacheKeyFactory {

    /**
     * Set multiple keys to multiple values. If there is currently another object associated with a specified key,
     * the old object will be replaced.
     * <p>
     * The specified arguments from cache options are used for time to live seconds and "fire cache events" flag.
     * <p>
     * See either {@link #newKey(Object...)} or {@link #newGroupMemberKey(String, Object...)} to build a cache key.
     *
     * @param map The key-to-value map
     * @throws OXException If operation fails
     * @throws IllegalArgumentException If given map is <code>null</code>
     * @see #newKey(Object...)
     * @see #newGroupMemberKey(String, Object...)
     */
    void mput(Map<CacheKey, V> map) throws OXException;

    /**
     * Puts a new object in the cache associated with the specified key. If there is currently
     * another object associated with the specified key, the old object will be replaced
     * with the current one.
     * <p>
     * The specified arguments from cache options are used for time to live seconds and "fire cache events" flag.
     * <p>
     * See either {@link #newKey(Object...)} or {@link #newGroupMemberKey(String, Object...)} to build a cache key.
     *
     * @param key The key
     * @param object The object
     * @return <code>true</code> if an existent value has been replaced by put; otherwise <code>false</code>
     * @throws OXException If the put operation fails
     * @throws IllegalArgumentException If given key is <code>null</code>
     * @see #newKey(Object...)
     * @see #newGroupMemberKey(String, Object...)
     */
    boolean put(CacheKey key, V object) throws OXException;

    /**
     * Puts a new object in the cache associated with the specified key only if no such key exists.
     * <p>
     * The specified arguments from cache options are used for time to live seconds and "fire cache events" flag
     * in case given object is put into cache.
     * <p>
     * See either {@link #newKey(Object...)} or {@link #newGroupMemberKey(String, Object...)} to build a cache key.
     *
     * @param key The key
     * @param object The object
     * @return <code>true</code> if object has been put; otherwise <code>false</code> if such a key already exists
     * @throws OXException If the put operation fails
     * @throws IllegalArgumentException If given key is <code>null</code>
     * @see #newKey(Object...)
     * @see #newGroupMemberKey(String, Object...)
     */
    boolean putIfAbsent(CacheKey key, V object) throws OXException;

    /**
     * Checks existence of an object in the cache that is associated with the specified key.
     * <p>
     * Specified key can be anything from
     * <ul>
     * <li>{@link #newKey(Object...)} (check existence for regular fully-qualifying cache key),
     * <li>{@link #newGroupMemberKey(String, Object...)} (check existence for fully-qualifying key contained in a group) or
     * <li>{@link #newGroupKey(String)} (check existence for fully-qualifying group key).
     * </ul>
     *
     * @param key The key
     * @return <code>true</code> if such an object exists; otherwise <code>false</code>
     * @throws OXException If the exists operation fails
     * @throws IllegalArgumentException If given key is <code>null</code>
     * @see #newKey(Object...)
     * @see #newGroupMemberKey(String, Object...)
     * @see #newGroupKey(String)
     */
    boolean exists(CacheKey key) throws OXException;

    /**
     * Retrieves an object from the cache that is associated with the specified key.
     * <p>
     * See either {@link #newKey(Object...)} or {@link #newGroupMemberKey(String, Object...)} to build a cache key.
     *
     * @param key The key
     * @return The cache object or <code>null</code> if no object is bound to the specified key
     * @throws OXException If the get operation fails
     * @throws IllegalArgumentException If given key is <code>null</code>
     * @see #newKey(Object...)
     * @see #newGroupMemberKey(String, Object...)
     */
    V get(CacheKey key) throws OXException;

    /**
     * Retrieves the objects from the cache that are associated with the specified keys.
     * <p>
     * See either {@link #newKey(Object...)} or {@link #newGroupMemberKey(String, Object...)} to build a cache key.
     *
     * @param keys The keys
     * @return The cache objects; individual values are empty if there is no object bound to the appropriate key
     * @throws OXException If the get operation fails
     * @throws IllegalArgumentException If given key is <code>null</code>
     * @see #newKey(Object...)
     * @see #newGroupMemberKey(String, Object...)
     */
    default List<CacheKeyValue<V>> mget(Collection<? extends CacheKey> keys) throws OXException {
        if (keys == null || keys.isEmpty()) {
            return Collections.emptyList();
        }
        return mget(keys.toArray(new CacheKey[keys.size()]));
    }

    /**
     * Retrieves the objects from the cache that are associated with the specified keys.
     * <p>
     * See either {@link #newKey(Object...)} or {@link #newGroupMemberKey(String, Object...)} to build a cache key.
     *
     * @param keys The keys
     * @return The cache objects; individual values are empty if there is no object bound to the appropriate key
     * @throws OXException If the get operation fails
     * @throws IllegalArgumentException If given key is <code>null</code>
     * @see #newKey(Object...)
     * @see #newGroupMemberKey(String, Object...)
     */
    List<CacheKeyValue<V>> mget(CacheKey... keys) throws OXException;

    /**
     * Invalidates the value that is associated with the specified key.
     * <p>
     * See either {@link #newKey(Object...)} or {@link #newGroupMemberKey(String, Object...)} to build a cache key.
     *
     * @param key The key
     * @return <code>true</code> if the value associated with given key has been invalidated; otherwise <code>false</code> if there was no such key
     * @throws OXException If invalidate operation on cache fails
     * @throws IllegalArgumentException If given key is <code>null</code>
     * @see #newKey(Object...)
     * @see #newGroupMemberKey(String, Object...)
     */
    boolean invalidate(CacheKey key) throws OXException;

    /**
     * Invalidates multiple objects from the cache which are bound to the specified keys.
     * <p>
     * See either {@link #newKey(Object...)} or {@link #newGroupMemberKey(String, Object...)} to build a cache key.
     *
     * @param keys The keys
     * @throws OXException If invalidate operation on cache fails
     * @see #newKey(Object...)
     * @see #newGroupMemberKey(String, Object...)
     */
    void invalidate(List<CacheKey> keys) throws OXException;

    /**
     * Invalidates given key's group.
     * <p>
     * See {@link #newGroupKey(String)} to build a cache key.
     *
     * @param groupKey The group key
     * @return The keys that were invalidated through dropping specified group
     * @throws OXException If invalidate operation on cache fails
     * @throws IllegalArgumentException If given group key is <code>null</code>
     * @see #newGroupKey(String)
     */
    List<? extends CacheKey> invalidateGroup(CacheKey groupKey) throws OXException;

    // ------------------------------------------------ Extended GETs ----------------------------------------------------------------------

    /**
     * Retrieves an object from the cache that is associated with the specified key, obtaining that value from <code>loader</code> if
     * necessary. If loader yields <code>null</code>, then <code>null</code> is returned as well and nothing is put into cache.
     * <p>
     * The method improves upon the conventional "if cached, return; otherwise create, cache and return" pattern.
     * <p>
     * The specified arguments from cache options are used for time to live seconds and "fire cache events" flag
     * in case loaded object is put into cache.
     * <p>
     * See either {@link #newKey(Object...)} or {@link #newGroupMemberKey(String, Object...)} to build a cache key.
     *
     * @param key The key
     * @param loader The loader to yield the appropriate value, which is then put into cache for future access
     * @return The cached object or the one returned by given loader
     * @throws OXException If the operation fails
     * @throws IllegalArgumentException If given key is <code>null</code>
     * @see #newKey(Object...)
     * @see #newGroupMemberKey(String, Object...)
     * @see SignalingCacheLoader
     * @deprecated Please use {@link #get(CacheKey, CacheLoader)}
     */
    @Deprecated
    default V get(CacheKey key, Callable<V> loader) throws OXException {
        if (loader == null) {
            throw new IllegalArgumentException("Loader must not be null");
        }
        return get(key, new WrappingCacheLoader<V>(loader));
    }

    /**
     * Retrieves an object from the cache that is associated with the specified key, obtaining that value from <code>loader</code> if
     * necessary. If loader yields <code>null</code>, then <code>null</code> is returned as well and nothing is put into cache.
     * <p>
     * The method improves upon the conventional "if cached, return; otherwise create, cache and return" pattern.
     * <p>
     * <div style="background-color:#FFDDDD; padding:6px; margin:0px;">
     * Please pass an instance of {@link SignalingCacheLoader} in case caller needs to know whether computed value has been put into cache
     * since simple invocation of the loader is not sufficient to determine that yielded value has actually been used to populate
     * the cache.
     * </div>
     * <p>
     * The specified arguments from cache options are used for time to live seconds and "fire cache events" flag
     * in case loaded object is put into cache.
     * <p>
     * See either {@link #newKey(Object...)} or {@link #newGroupMemberKey(String, Object...)} to build a cache key.
     *
     * @param key The key
     * @param loader The loader to yield the appropriate value, which is then put into cache for future access
     * @return The cached object or the one returned by given loader
     * @throws OXException If the operation fails
     * @throws IllegalArgumentException If given key is <code>null</code>
     * @see #newKey(Object...)
     * @see #newGroupMemberKey(String, Object...)
     * @see SignalingCacheLoader
     */
    V get(CacheKey key, CacheLoader<V> loader) throws OXException;

}
