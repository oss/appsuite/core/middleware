/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2;

import java.util.concurrent.Callable;

/**
 * {@link WrappingCacheLoader} - A cache loader wrapping an instance of {@link Callable} that computes or retrieves the cache value.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class WrappingCacheLoader<V> implements CacheLoader<V> {

    private final Callable<V> loader;

    /**
     * Initializes a new instance of {@link WrappingCacheLoader}.
     *
     * @param loader The loader callable to wrap
     */
    public WrappingCacheLoader(Callable<V> loader) {
        super();
        this.loader = loader;
    }

    @Override
    public V load(CacheKey key) throws Exception {
        return loader.call();
    }

}
