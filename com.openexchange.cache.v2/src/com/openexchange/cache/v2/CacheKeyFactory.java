/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2;

/**
 * {@link CacheKeyFactory} - Factory for {@link CacheKey}s.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public interface CacheKeyFactory {

    /**
     * Creates a new fully-qualifying cache key for given suffix; e.g. <code>"ox-cache:context:v1:1337"</code>.
     *
     * @param suffix The suffix parts from which the {@link Object#toString() toString()} representation is used
     * @return The new fully-qualifying cache key
     * @throws IllegalArgumentException If a suffix part is <code>null</code>
     * @see CacheKeyType#KEY
     */
    CacheKey newKey(Object... suffix);

    /**
     * Creates a new fully-qualifying cache key associated with given group; e.g. <code>"ox-cache:user:v1:1337[3]"</code>.
     *
     * @param group The group the key belongs to
     * @param suffix The suffix parts from which the {@link Object#toString() toString()} representation is used
     * @return The new fully-qualifying cache key with a group association
     * @throws IllegalArgumentException If a suffix part is <code>null</code>
     * @see CacheKeyType#MEMBER
     */
    CacheKey newGroupMemberKey(String group, Object... suffix);

    /**
     * Creates a new fully-qualifying cache key identifying given group; e.g. <code>"ox-cache:user:v1:1337"</code>.
     *
     * @param group The group the key belongs to
     * @return The new fully-qualifying group cache key
     * @see CacheKeyType#GROUP
     */
    CacheKey newGroupKey(String group);

    // ---------------------------------------------- SUFFIX BUILDER ----------------------------------------------------------------------

    /**
     * Creates a new fully-qualifying cache key for given suffix; e.g. <code>"ox-cache:context:v1:1337"</code>.
     *
     * @param suffix The suffix builder providing the suffix to use
     * @return The new fully-qualifying cache key
     * @throws IllegalArgumentException If a suffix part is <code>null</code>
     * @see CacheKeyType#KEY
     */
    CacheKey newKey(SuffixBuilder suffix);

    /**
     * Creates a new fully-qualifying cache key associated with given group; e.g. <code>"ox-cache:user:v1:1337[3]"</code>.
     *
     * @param group The group the key belongs to
     * @param suffix The suffix builder providing the suffix to use
     * @return The new fully-qualifying cache key with a group association
     * @throws IllegalArgumentException If a suffix part is <code>null</code>
     * @see CacheKeyType#MEMBER
     */
    CacheKey newGroupMemberKey(String group, SuffixBuilder suffix);

    /**
     * Creates a new suffix builder with a default capacity of 16 characters that may be passed to {@link #newKey(Object...)} or
     * {@link #newGroupMemberKey(String, Object...)}.
     *
     * @return The newly created suffix builder
     */
    default SuffixBuilder suffixBuilder() {
        return suffixBuilder(16);
    }

    /**
     * Creates a new suffix builder that may be passed to {@link #newKey(Object...)} or {@link #newGroupMemberKey(String, Object...)}.
     *
     * @param capacity The initial capacity
     * @return The newly created suffix builder
     * @throws NegativeArraySizeException If the {@code capacity} argument is less than {@code 0}
     */
    SuffixBuilder suffixBuilder(int capacity);

}
