/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.codec;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONServices;
import com.openexchange.cache.v2.codec.json.AbstractSimpleJSONValueCacheValueCodec;

/**
 * {@link GenericJSONObjectCacheValueCodec} - The generic cache codec for primitive <code>int[][]</code> arrays.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class IntIntArrayCacheValueCodec extends AbstractSimpleJSONValueCacheValueCodec<int[][], JSONArray> {

    private static final UUID CODEC_ID = UUID.fromString("97ebf109-6dc5-4e5c-b22a-592433f1dcdf");

    private static final IntIntArrayCacheValueCodec INSTANCE = new IntIntArrayCacheValueCodec();

    /**
     * Gets the instance.
     *
     * @return The instance
     */
    public static IntIntArrayCacheValueCodec getInstance() {
        return INSTANCE;
    }

    // ---------------------------------------------------------------------------------------------------

    /**
     * Initializes a new {@link IntIntArrayCacheValueCodec}.
     */
    private IntIntArrayCacheValueCodec() {
        super();
    }

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected boolean isAscii() {
        return true;
    }

    @Override
    protected JSONArray newJsonValueFrom(InputStream data) throws JSONException {
        return JSONServices.parseArray(data);
    }

    @Override
    protected void writeJson(int[][] value, Writer writer) throws Exception {
        writer.append('[');

        boolean first = true;
        for (int[] is : value) {
            if (first) {
                first = false;
            } else {
                writer.append(',');
            }
            writeAsJson(is, writer);
        }

        writer.append(']');
    }

    private static void writeAsJson(int[] value, Writer writer) throws IOException {
        writer.append('[');

        int len = value.length;
        if (len > 0) {
            writer.append(Integer.toString(value[0]));
            for (int i = 1; i < len; i++) {
                writer.append(',');
                writer.append(Integer.toString(value[i]));
            }
        }

        writer.append(']');
    }

    @Override
    protected int[][] parseJson(JSONArray jArray) throws Exception {
        int length = jArray.length();
        if (length <= 0) {
            return new int[0][];
        }

        int[][] retval = new int[length][];
        int i = 0;
        for (Object object : jArray) {
            retval[i++] = Codecs.jsonArrayToIntArray((JSONArray) object);
        }
        return retval;
    }

}
