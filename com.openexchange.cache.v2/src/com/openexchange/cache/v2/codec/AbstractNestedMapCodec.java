/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.cache.v2.codec;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;

/**
 * {@link AbstractNestedMapCodec} - Generic codec for a mapping of string keys to a map.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public abstract class AbstractNestedMapCodec<V> extends AbstractJSONObjectCacheValueCodec<Map<String, Map<String, V>>> {

    /**
     * Initializes a new instance of {@link AbstractNestedMapCodec}.
     */
    protected AbstractNestedMapCodec() {
        super();
    }

    @Override
    protected JSONObject writeJson(Map<String, Map<String, V>> value) throws Exception {
        if (null == value || value.isEmpty()) {
            return JSONObject.EMPTY_OBJECT;
        }
        JSONObject jsonObject = new JSONObject(value.size());
        for (Map.Entry<String, Map<String, V>> entry : value.entrySet()) {
            Map<String, V> keyValues = entry.getValue();
            JSONObject innerObject = new JSONObject(keyValues.size());
            for (Map.Entry<String, V> e : keyValues.entrySet()) {
                innerObject.put(e.getKey(), serializeValue(e.getValue()));
            }
            jsonObject.put(entry.getKey(), innerObject);
        }
        return jsonObject;
    }

    @Override
    protected Map<String, Map<String, V>> parseJson(JSONObject jsonObject) throws Exception {
        if (null == jsonObject || jsonObject.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<String, Map<String, V>> nestedMap = HashMap.newHashMap(jsonObject.length());
        for (Map.Entry<String, Object> map : jsonObject.entrySet()) {
            JSONObject innerObject = (JSONObject) map.getValue();
            Map<String, V> keyValues = HashMap.newHashMap(innerObject.length());
            for (Map.Entry<String, Object> e : innerObject.entrySet()) {
                keyValues.put(e.getKey(), deserializeValue((JSONObject) e.getValue()));
            }
            nestedMap.put(map.getKey(), keyValues);
        }
        return nestedMap;
    }

    /**
     * Serializes a value to its JSON representation.
     *
     * @param value The value to serialize
     * @return The JSON representation
     * @throws Exception If writing JSON fails
     */
    protected abstract JSONObject serializeValue(V value) throws Exception;

    /**
     * De-serializes a value from given JSON representation.
     *
     * @param jsonObject The JSON representation to parse
     * @return The parsed value
     * @throws Exception If parsing fails
     */
    protected abstract V deserializeValue(JSONObject jsonObject) throws Exception;

}
