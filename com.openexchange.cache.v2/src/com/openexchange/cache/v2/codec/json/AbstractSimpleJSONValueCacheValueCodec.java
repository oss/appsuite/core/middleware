/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.codec.json;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import org.json.EmptyJSONInputException;
import org.json.JSONValue;
import org.slf4j.Logger;
import com.openexchange.exception.OXException;
import com.openexchange.java.AsciiWriter;
import com.openexchange.java.Streams;

/**
 * {@link AbstractSimpleJSONValueCacheValueCodec} - The abstract cache value codec converting to/from JSON value.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public abstract class AbstractSimpleJSONValueCacheValueCodec<V, J extends JSONValue> extends AbstractJSONCacheValueCodec<V, J> {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(AbstractSimpleJSONValueCacheValueCodec.class); // NOSONARLINT
    }

    /**
     * Initializes a new {@link AbstractSimpleJSONValueCacheValueCodec}.
     */
    protected AbstractSimpleJSONValueCacheValueCodec() {
        super();
    }

    @Override
    public InputStream serialize(V value) throws Exception {
        try {
            ByteArrayOutputStream baos = Streams.newByteArrayOutputStream();
            Writer writer = isAscii() ? new AsciiWriter(baos) : new OutputStreamWriter(baos, StandardCharsets.UTF_8);
            writeJson(value, writer);
            writer.flush();
            writer.close();
            return Streams.asInputStream(baos);
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw OXException.general("Serialisation failed", e);
        }
    }

    @Override
    public V deserialize(InputStream data) throws Exception {
        try {
            V value = parseJson(newJsonValueFrom(data));
            if (value == null) {
                throw OXException.general("Deserialisation failed. Cache value codec '" + this.getClass().getName() + "' returned a null result from call to parseJson().");
            }
            return value;
        } catch (OXException e) {
            throw e;
        } catch (EmptyJSONInputException e) {
            LoggerHolder.LOG.debug("Encountered an empty input for expected JSON content. Returning null instead", e);
            return null;
        } catch (Exception e) {
            throw OXException.general("Deserialisation failed", e);
        } finally {
            Streams.close(data);
        }
    }

    /**
     * Writes given value to specified writer.
     *
     * @param value The value to generate the JSON value from
     * @param writer The sink to write to
     * @throws Exception If JSON value cannot be written
     */
    protected abstract void writeJson(V value, Writer writer) throws Exception;

}
