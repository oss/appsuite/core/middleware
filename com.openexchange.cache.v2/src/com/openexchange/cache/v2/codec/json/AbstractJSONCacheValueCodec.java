/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2.codec.json;

import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONValue;
import com.openexchange.cache.v2.codec.CacheValueCodec;

/**
 * {@link AbstractJSONCacheValueCodec} - Abstract codec for reading/writing JSON data.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public abstract class AbstractJSONCacheValueCodec<V, J extends JSONValue> implements CacheValueCodec<V> {

    /**
     * Initializes a new {@link AbstractJSONCacheValueCodec}.
     */
    protected AbstractJSONCacheValueCodec() {
        super();
    }

    /**
     * Checks if the JSON content is known to only consist of 7-bit ASCII characters.
     *
     * @return <code>true</code> if only of ASCII characters; otherwise <code>false</code>
     */
    protected boolean isAscii() {
        return false;
    }

    /**
     * Creates a new JSON value carrying the content from given reader.
     *
     * @param data The stream providing the content
     * @return The new JSON value
     * @throws JSONException If new JSON value cannot be created
     */
    protected abstract J newJsonValueFrom(InputStream data) throws JSONException;

    /**
     * Parses given JSON value to the appropriate type.
     *
     * @param jObject The JSON value to parse
     * @return The parsed type
     * @throws Exception If parsing fails
     */
    protected abstract V parseJson(J jObject) throws Exception;

}
