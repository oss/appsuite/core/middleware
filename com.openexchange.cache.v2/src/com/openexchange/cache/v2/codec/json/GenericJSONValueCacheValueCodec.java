/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.codec.json;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PushbackReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import org.json.JSONServices;
import org.json.JSONValue;
import com.openexchange.cache.v2.codec.CacheValueCodec;
import com.openexchange.java.Streams;
import com.openexchange.java.Strings;

/**
 * {@link GenericJSONObjectCacheValueCodec} - The generic cache codec to read JSON from arbitrary cache entries.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class GenericJSONValueCacheValueCodec implements CacheValueCodec<JSONValue> {

    private static final GenericJSONValueCacheValueCodec INSTANCE = new GenericJSONValueCacheValueCodec();

    /**
     * Gets the instance.
     *
     * @return The instance
     */
    public static GenericJSONValueCacheValueCodec getInstance() {
        return INSTANCE;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static final UUID ID = UUID.fromString("bdfba751-a183-4ad7-aa1f-575119990125");

    /**
     * Initializes a new {@link GenericJSONObjectCacheValueCodec}.
     */
    private GenericJSONValueCacheValueCodec() {
        super();
    }

    @Override
    public UUID getCodecId() {
        return ID;
    }

    @Override
    public InputStream serialize(JSONValue json) throws Exception {
        return json.getStream(false);
    }

    @Override
    public JSONValue deserialize(InputStream data) throws Exception {
        Reader reader = null;
        PushbackReader pushbackReader = null;
        try {
            reader = new InputStreamReader(data, StandardCharsets.UTF_8);
            pushbackReader = new PushbackReader(reader);
            int ch;
            while ((ch = pushbackReader.read()) > 0 && Strings.isWhitespace((char) ch)) {
                // Consume
            }
            if (ch <= 0) {
                // Empty input
                return null;
            }
            pushbackReader.unread(ch);
            return switch (ch) {
                case '{' -> JSONServices.parseObject(pushbackReader);
                case '[' -> JSONServices.parseArray(pushbackReader);
                default -> throw new IllegalStateException("Data holds illegal JSON value: " + Streams.reader2string(pushbackReader));
            };
        } finally {
            Streams.close(pushbackReader, reader);
        }
    }

}
