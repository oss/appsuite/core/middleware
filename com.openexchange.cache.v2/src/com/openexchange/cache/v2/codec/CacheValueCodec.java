/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.codec;

import java.io.InputStream;
import java.util.UUID;
import com.openexchange.cache.v2.filter.VersionName;

/**
 * {@link CacheValueCodec} - The cache value codec accepting raw bytes of any Java object's string representation.
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @param <V> The type of the codec from/to which is de-/serialized
 */
public interface CacheValueCodec<V> {

    /**
     * Gets this codec's unique identifier.
     *
     * @return The codec identifier
     */
    UUID getCodecId();

    /**
     * Gets the version number of this codes that is used in referencing cache key.
     *
     * @return The version number
     */
    default int getVersionNumber() {
        return VersionName.DEFAULT_VERSION_NAME.getVersionNumber();
    }

    /**
     * Serializes the specified object to data bytes.
     *
     * @param value The value to encode
     * @return The data bytes
     * @throws Exception If serialization fails
     */
    InputStream serialize(V value) throws Exception;

    /**
     * Deserializes the given data to the a value.
     *
     * @param data The raw bytes of any Java object's string representation
     * @return The value or <code>null</code> if data cannot be deserialized into codec's type
     * @throws Exception If deserialization fails
     */
    V deserialize(InputStream data) throws Exception;

    /**
     * Gets the compression type to use for this codec.
     *
     * @return The compression type
     */
    default CompressionType getCompressionType() {
        return CompressionType.NONE;
    }

}
