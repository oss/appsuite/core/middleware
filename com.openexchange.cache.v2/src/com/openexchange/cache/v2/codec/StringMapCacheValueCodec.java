/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.codec;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;

/**
 * {@link StringMapCacheValueCodec} - The generic cache codec for simple <code>Map&lt;String, String&gt;</code> with string keys and values.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public final class StringMapCacheValueCodec extends AbstractJSONObjectCacheValueCodec<Map<String, String>> {

    private static final UUID CODEC_ID = UUID.fromString("e7aef0d7-dda6-486e-9278-1bb212d7d559");

    private static final StringMapCacheValueCodec ORDERED_INSTANCE = new StringMapCacheValueCodec(true);
    private static final StringMapCacheValueCodec UNORDERED_INSTANCE = new StringMapCacheValueCodec(false);

    /**
     * Gets the instance ignoring order of keys on serialization/de-serialization.
     *
     * @return The instance
     * @see #getInstance(boolean)
     */
    public static StringMapCacheValueCodec getInstance() {
        return getInstance(false);
    }

    /**
     * Gets the instance.
     *
     * @param keepOrder <code>true</code> to keep the order of keys in the map on serialization/de-serialization; otherwise <code>false</code>
     * @return The instance
     */
    public static StringMapCacheValueCodec getInstance(boolean keepOrder) {
        return keepOrder ? ORDERED_INSTANCE : UNORDERED_INSTANCE;
    }

    // ---------------------------------------------------------------------------------------------------

    private final boolean keepOrder;

    /**
     * Initializes a new {@link StringMapCacheValueCodec}.
     *
     * @param keepOrder <code>true</code> to keep the order of keys in the map on serialization/de-serialization; otherwise <code>false</code>
     */
    private StringMapCacheValueCodec(boolean keepOrder) {
        super();
        this.keepOrder = keepOrder;
    }

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONObject writeJson(Map<String, String> value) throws Exception {
        return new JSONObject(value);
    }

    @Override
    protected Map<String, String> parseJson(JSONObject jObject) throws Exception {
        if (null == jObject) {
            return null; // NOSONARLINT
        }

        Map<String, String> map = keepOrder ? LinkedHashMap.newLinkedHashMap(jObject.length()) : HashMap.newHashMap(jObject.length());
        for (Map.Entry<String, Object> entry : jObject.entrySet()) {
            map.put(entry.getKey(), entry.getValue().toString());
        }
        return map;
    }

}
