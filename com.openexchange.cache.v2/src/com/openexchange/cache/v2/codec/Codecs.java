/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.codec;

import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * {@link Codecs} - Utilities for cache/channel codecs.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class Codecs {

    /**
     * Initializes a new {@link Codecs}.
     */
    private Codecs() {
        super();
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Converts given integer array to a JSON array.
     *
     * @param arr The integer array
     * @return The JSON array
     */
    public static JSONArray intArrayToJsonArray(int[] arr) {
        if (arr == null) {
            return null;
        }

        int length = arr.length;
        JSONArray jArr = new JSONArray(length);
        for (int i = 0; i < length; i++) {
            jArr.put(arr[i]);
        }
        return jArr;
    }

    /**
     * Converts given JSON array to an integer array.
     *
     * @param jArr The JSON array
     * @return The integer array
     * @throws JSONException If conversion fails
     */
    public static int[] jsonArrayToIntArray(JSONArray jArr) throws JSONException {
        if (jArr == null) {
            return null; // NOSONARLINT
        }

        int length = jArr.length();
        int[] arr = new int[length];
        for (int i = length; i-- > 0;) {
            arr[i] = jArr.getInt(i);
        }
        return arr;
    }

    /**
     * Converts given String array to a JSON array.
     *
     * @param arr The String array
     * @return The JSON array
     */
    public static JSONArray stringArrayToJsonArray(String[] arr) {
        if (arr == null) {
            return null;
        }

        int length = arr.length;
        JSONArray jArr = new JSONArray(length);
        for (int i = 0; i < length; i++) {
            jArr.put(arr[i]);
        }
        return jArr;
    }

    /**
     * Converts given JSON array to a String array.
     *
     * @param jArr The JSON array
     * @return The String array
     * @throws JSONException If conversion fails
     */
    public static String[] jsonArrayToStringArray(JSONArray jArr) throws JSONException {
        if (jArr == null) {
            return null; // NOSONARLINT
        }

        int length = jArr.length();
        String[] arr = new String[length];
        for (int i = length; i-- > 0;) {
            arr[i] = jArr.getString(i);
        }
        return arr;
    }

    /**
     * Converts given JSON object to a String map.
     *
     * @param jObj The JSON object
     * @return The String map
     * @throws JSONException If conversion fails
     */
    public static Map<String, String> jsonObjectToStringMap(JSONObject jObj) {
        if (jObj == null) {
            return null; // NOSONARLINT
        }

        int length = jObj.length();
        Map<String, String> map = LinkedHashMap.newLinkedHashMap(length);
        for (Map.Entry<String, Object> entry : jObj.entrySet()) {
            map.put(entry.getKey(), entry.getValue().toString());
        }
        return map;
    }

    /**
     * Converts given String map to a JSON object.
     *
     * @param map The map
     * @return The JSON object
     * @throws JSONException If conversion fails
     */
    public static JSONObject stringMapToJsonObject(Map<String, String> map) {
        return map == null ? null : JSONObject.jsonObjectFor(map);
    }

    /**
     * Replaces the whole string or substring of given <code>toReplace</code> with specified replacements using
     * {@link String#replace(CharSequence, CharSequence)}.
     * <p>
     * Replacements are a listing of string and associated replacement; e.g.
     * <pre>
     *  List.of(
     *    new String[] {"longlongword1", "^1"},
     *    new String[] {"longlongword2", "^2"},
     *    ...
     *    new String[] {"longlongwordN", "^N"}
     *  );
     * </pre>
     * Example
     * <pre>
     *  Codecs.replace("longlongword1 longlongword2 ... longlongwordN", replacements)
     *
     * yields
     *
     *  "^1 ^2 ... ^N"
     * </pre>
     *
     * @param toReplace The string to replace or replace in
     * @param replacements The replacements to use
     * @return The string with replacements or <code>null</code> if input string was <code>null</code>, too
     */
    public static String replace(String toReplace, Iterable<String[]> replacements) {
        if (toReplace == null) {
            return null;
        }
        String retval = toReplace;
        for (String[] replacement : replacements) {
            retval = retval.replace(replacement[0], replacement[1]);
        }
        return retval;
    }

    /**
     * Un-Replaces the whole string or substring of given <code>toReplace</code> with specified replacements using
     * {@link String#replace(CharSequence, CharSequence)}.<br>
     * In other words, this method reverts the replacements previously performed through {@link #replace(String, Iterable)}.
     * <p>
     * Replacements are a listing of string and associated replacement; e.g.
     * <pre>
     *  List.of(
     *    new String[] {"longlongword1", "^1"},
     *    new String[] {"longlongword2", "^2"},
     *    ...
     *    new String[] {"longlongwordN", "^N"}
     *  );
     * </pre>
     * Example
     * <pre>
     *  Codecs.unreplace("^1 ^2 ... ^N", replacements)
     *
     * yields
     *
     *  "longlongword1 longlongword2 ... longlongwordN"
     * </pre>
     *
     * @param replaced The string to unreplace or unreplace in
     * @param replacements The replacements to revert
     * @return The string with replacements
     */
    public static String unreplace(String replaced, Iterable<String[]> replacements) {
        if (replaced == null) {
            return null;
        }
        String retval = replaced;
        for (String[] replacement : replacements) {
            retval = retval.replace(replacement[1], replacement[0]);
        }
        return retval;
    }

}
