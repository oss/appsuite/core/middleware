/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.cache.v2.codec;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;

/**
 * {@link AbstractMultiMapCodec} - Generic codec for a mapping of string keys to multiple values.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public abstract class AbstractMultiMapCodec<V> extends AbstractJSONObjectCacheValueCodec<Map<String, List<V>>> {

    /**
     * Initializes a new instance of {@link AbstractMultiMapCodec}.
     */
    protected AbstractMultiMapCodec() {
        super();
    }

    @Override
    protected JSONObject writeJson(Map<String, List<V>> value) throws Exception {
        if (null == value || value.isEmpty()) {
            return JSONObject.EMPTY_OBJECT;
        }
        JSONObject jsonObject = new JSONObject(value.size());
        for (Map.Entry<String, List<V>> entry : value.entrySet()) {
            List<V> values = entry.getValue();
            JSONArray jsonArray = new JSONArray(values.size());
            for (V v : values) {
                jsonArray.put(serializeValue(v));
            }
            jsonObject.put(entry.getKey(), jsonArray);
        }
        return jsonObject;
    }

    @Override
    protected Map<String, List<V>> parseJson(JSONObject jsonObject) throws Exception {
        if (null == jsonObject || jsonObject.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<String, List<V>> multiMap = HashMap.newHashMap(jsonObject.length());
        for (String key : jsonObject.keySet()) {
            JSONArray jsonArray = jsonObject.getJSONArray(key);
            List<V> values = new ArrayList<>(jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                values.add(deserializeValue(jsonArray.optJSONObject(i)));
            }
            multiMap.put(key, values);
        }
        return multiMap;
    }

    /**
     * Serializes a value to its JSON representation.
     *
     * @param value The value to serialize
     * @return The JSON representation
     * @throws Exception If writing JSON fails
     */
    protected abstract JSONObject serializeValue(V value) throws Exception;

    /**
     * De-serializes a value from given JSON representation.
     *
     * @param jsonObject The JSON representation to parse
     * @return The parsed value
     * @throws Exception If parsing fails
     */
    protected abstract V deserializeValue(JSONObject jsonObject) throws Exception;

}
