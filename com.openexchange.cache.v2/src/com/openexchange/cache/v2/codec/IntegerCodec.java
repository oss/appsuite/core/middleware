/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.cache.v2.codec;

import static java.nio.charset.StandardCharsets.US_ASCII;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import com.openexchange.java.Streams;

/**
 * {@link IntegerCodec} - The generic cache codec for {@link Integer}s.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public final class IntegerCodec implements CacheValueCodec<Integer> {

    private static final UUID CODEC_ID = UUID.fromString("6b441142-9bbd-4147-8277-39b2dbcfc2c3");

    private static final IntegerCodec INSTANCE = new IntegerCodec();

    /**
     * Gets the instance.
     *
     * @return The instance
     */
    public static IntegerCodec getInstance() {
        return INSTANCE;
    }

    // ---------------------------------------------------------------------------------------------------

    /**
     * Initializes a new {@link IntegerCodec}.
     */
    private IntegerCodec() {
        super();
    }

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    public InputStream serialize(Integer value) throws Exception {
        if (null == value) {
            return null;
        }
        return Streams.newByteArrayInputStream(value.toString().getBytes(US_ASCII));
    }

    @Override
    public Integer deserialize(InputStream data) throws Exception {
        return Integer.valueOf(Streams.stream2string(data, StandardCharsets.US_ASCII));
    }

}
