/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import com.openexchange.cache.v2.codec.CacheValueCodec;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.cache.v2.filter.ApplicationName;
import com.openexchange.cache.v2.filter.DefaultVersionName;
import com.openexchange.cache.v2.filter.ModuleName;
import com.openexchange.cache.v2.filter.VersionName;
import com.openexchange.cache.v2.properties.CacheProperty;

/**
 * {@link CacheOptions} - The options specifying the cache view.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @param <V> The type of the cache values
 */
public class CacheOptions<V> {

    /**
     * The configurable default expiration seconds for cache entries (default is 1 hour).
     */
    private static final AtomicLong DEFAULT_EXPIRATION_SECONDS = new AtomicLong(((Long) CacheProperty.DEFAULT_EXPIRATION_SECONDS.getDefaultValue()).longValue());

    /**
     * Sets the default expiration seconds to assume for values added to cache.
     *
     * @param defaultExpirationSeconds The default expiration seconds
     */
    public static void setDefaultExpirationSeconds(long defaultExpirationSeconds) {
        if (defaultExpirationSeconds <= 0) {
            throw new IllegalArgumentException("Default expiration seconds are required to be greater than 0 (zero)");
        }
        DEFAULT_EXPIRATION_SECONDS.set(defaultExpirationSeconds);
    }

    /**
     * Gets the configurable default expiration seconds to assume for values added to cache.
     *
     * @return The default expiration seconds
     */
    public static long getDefaultExpirationSeconds() {
        return DEFAULT_EXPIRATION_SECONDS.get();
    }

    /**
     * Gets the effective expiration seconds for given parameter.
     * <p>
     * {@link CacheOptions#getDefaultExpirationSeconds()} is returned if passed parameter is less than <code>0</code> (zero).
     *
     * @param expirationSeconds The expiration seconds to get the effective value for
     * @return The effective expiration seconds
     */
    public static long getEffectiveExpirationSecondsFor(long expirationSeconds) {
        return expirationSeconds < 0 ? getDefaultExpirationSeconds() : expirationSeconds;
    }

    /**
     * Creates a new builder instance with time-to-live seconds set to <code>0</code> (zero), version set to
     * {@link VersionName#DEFAULT_VERSION_NAME default name} and disabled flag to fire cache events.
     *
     * @return The new builder instance
     */
    public static <V> Builder<V> builder() {
        return new Builder<>();
    }

    /** A builder for an instance of <code>CacheOptions</code> */
    public static final class Builder<V> {

        private CacheValueCodec<V> codec;
        private ModuleName moduleName;
        private VersionName versionName;
        private long expirationSeconds;
        private boolean fireCacheEvents;

        /**
         * Initializes a new {@link Builder}.
         */
        Builder() {
            super();
            expirationSeconds = 0L;
            versionName = VersionName.DEFAULT_VERSION_NAME;
            fireCacheEvents = false;
        }

        /**
         * Sets the flag whether to fire cache events; e.g. invalidation events on replacements/puts into cache.
         *
         * @param fireCacheEvents The flag to set
         * @return This builder
         */
        public Builder<V> withFireCacheEvents(boolean fireCacheEvents) {
            this.fireCacheEvents = fireCacheEvents;
            return this;
        }

        /**
         * Sets the expiration in seconds.
         * <p>
         * These expiration seconds are used as default when calling {@link Cache#put(CacheKey, Object)} or
         * {@link Cache#putIfAbsent(CacheKey, Object)}.
         * <p>
         * Pass a negative value to signal that {@link CacheOptions#getDefaultExpirationSeconds()} should be used.
         *
         * @param expirationSeconds The expiration in seconds
         * @return This builder
         */
        public Builder<V> withExpirationSeconds(long expirationSeconds) {
            this.expirationSeconds = expirationSeconds < 0 ? -1L : expirationSeconds;
            return this;
        }

        /**
         * Sets the codec and its associated version.
         *
         * @param codec The codec to set
         * @return This builder
         * @see #withVersionName(VersionName)
         */
        public Builder<V> withCodecAndVersion(CacheValueCodec<V> codec) {
            this.codec = codec;
            this.versionName = DefaultVersionName.versionNameFor(codec.getVersionNumber());
            return this;
        }

        /**
         * Sets the module name.
         *
         * @param moduleName The module name to set
         * @return This builder
         * @see #withCoreModuleName(CoreModuleName)
         */
        public Builder<V> withModuleName(ModuleName moduleName) {
            this.moduleName = moduleName;
            return this;
        }

        /**
         * Sets given version name.
         *
         * @param versionName The version name to set
         * @return This builder
         * @see #withCodecAndVersion(CacheValueCodec)
         */
        public Builder<V> withVersionName(VersionName versionName) {
            this.versionName = versionName;
            return this;
        }

        /**
         * Sets the module name and expiration in seconds.
         * <p>
         * Pass a negative value for expiration seconds to signal that {@link CacheOptions#getDefaultExpirationSeconds()} should be used.
         *
         * @param moduleName The module name to set
         * @param expirationSeconds The expiration in seconds
         * @return This builder
         */
        public Builder<V> withModuleNameAndExpiration(ModuleName moduleName, long expirationSeconds) {
            this.moduleName = moduleName;
            this.expirationSeconds = expirationSeconds < 0 ? -1L : expirationSeconds;
            return this;
        }

        /**
         * Sets the core module name and its expiration seconds.
         *
         * @param moduleName The module name to set
         * @return This builder
         */
        public Builder<V> withCoreModuleName(CoreModuleName moduleName) {
            this.moduleName = moduleName;
            this.expirationSeconds = moduleName.getExpirationSeconds() < 0 ? -1L : moduleName.getExpirationSeconds();
            return this;
        }

        /**
         * Creates the resulting instance of <code>CacheOptions</code> from this builder's arguments.
         *
         * @return The resulting instance of <code>CacheOptions</code>
         * @throws NullPointerException If any required argument is <code>null</code>
         */
        public CacheOptions<V> build() {
            Objects.requireNonNull(codec, "The codec must not be null");
            Objects.requireNonNull(moduleName, "The module name must not be null");
            Objects.requireNonNull(versionName, "The version name must not be null");
            return new CacheOptions<>(codec, moduleName, versionName, expirationSeconds, fireCacheEvents);
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final CacheValueCodec<V> codec;
    private final ModuleName moduleName;
    private final VersionName versionName;
    private final long expirationSeconds;
    private final boolean fireCacheEvents;
    private int hash;

    /**
     * Initializes a new {@link CacheOptions}.
     *
     * @param codec The codec
     * @param moduleName The module name
     * @param versionName The version name
     * @param expirationSeconds The expire time in seconds
     * @param fireCacheEvents The flag whether to fire cache events
     */
    CacheOptions(CacheValueCodec<V> codec, ModuleName moduleName, VersionName versionName, long expirationSeconds, boolean fireCacheEvents) {
        super();
        this.codec = codec;
        this.moduleName = moduleName;
        this.versionName = versionName;
        this.expirationSeconds = expirationSeconds < 0 ? -1L : expirationSeconds;
        this.fireCacheEvents = fireCacheEvents;
        hash = 0;
    }

    /**
     * Gets the flag whether to fire cache events.
     *
     * @return <code>true</code> to fire cache events; otherwise <code>false</code>
     */
    public boolean isFireCacheEvents() {
        return fireCacheEvents;
    }

    /**
     * Gets the expiration in seconds.
     * <p>
     * These expiration seconds are used as default when calling {@link Cache#put(CacheKey, Object)} or
     * {@link Cache#putIfAbsent(CacheKey, Object)}.
     * <p>
     * Return a negative value for expiration seconds to signal that {@link CacheOptions#getDefaultExpirationSeconds()} should be used.
     *
     * @return The expiration in seconds
     */
    public long getExpirationSeconds() {
        return expirationSeconds;
    }

    /**
     * Gets the codec.
     *
     * @return The codec
     */
    public CacheValueCodec<V> getCodec() {
        return codec;
    }

    /**
     * Gets the application name.
     *
     * @return The application name
     */
    public ApplicationName getApplicationName() {
        return ApplicationName.getInstance();
    }

    /**
     * Gets the module name.
     *
     * @return The module name
     */
    public ModuleName getModuleName() {
        return moduleName;
    }

    /**
     * Gets the version name.
     *
     * @return The version name
     */
    public VersionName getVersionName() {
        return versionName;
    }

    /**
     * Gets the composite name of the cache; e.g. <code>"ox-cache:contacts:v1"</code>.
     *
     * @return The composite name of the cache
     */
    @Override
    public String toString() {
        return new StringBuilder(ApplicationName.getInstance().getName()).append(':').append(moduleName.getName()).append(':').append(versionName.getName()).toString();
    }

    @Override
    public int hashCode() {
        int h = hash;
        if (h == 0) {
            int prime = 31;
            int result = 1;
            result = prime * result + (fireCacheEvents ? 1231 : 1237);
            result = prime * result + (int) (expirationSeconds ^ (expirationSeconds >>> 32));
            result = prime * result + ((moduleName == null) ? 0 : moduleName.getName().hashCode());
            result = prime * result + ((versionName == null) ? 0 : versionName.getName().hashCode());
            result = prime * result + ((codec == null) ? 0 : codec.getCodecId().hashCode());
            h = result;
            hash = h;
        }
        return h;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CacheOptions<?> other = (CacheOptions<?>) obj;
        if (fireCacheEvents != other.fireCacheEvents) {
            return false;
        }
        if (expirationSeconds != other.expirationSeconds) {
            return false;
        }
        if (moduleName == null) {
            if (other.moduleName != null) {
                return false;
            }
        } else if (other.moduleName == null || !moduleName.getName().equals(other.moduleName.getName())) {
            return false;
        }
        if (versionName == null) {
            if (other.versionName != null) {
                return false;
            }
        } else if (other.versionName == null || !versionName.getName().equals(other.versionName.getName())) {
            return false;
        }
        if (codec == null) {
            if (other.codec != null) {
                return false;
            }
        } else if (other.codec == null || !codec.getCodecId().equals(other.codec.getCodecId())) {
            return false;
        }
        return true;
    }

}
