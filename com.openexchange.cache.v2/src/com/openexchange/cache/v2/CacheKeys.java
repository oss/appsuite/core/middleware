/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2;

import com.openexchange.java.Strings;

/**
 * {@link CacheKeys} - Utility class for {@link CacheKey}s.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public final class CacheKeys {

    /** The <code>'{'</code> character marking the start of the hash part */
    private static final char HASH_MARKER_START = '{';

    /** The <code>'{'</code> character marking the end of the hash part */
    private static final char HASH_MARKER_END = '}';

    /**
     * Initializes a new {@link CacheKeys}.
     */
    private CacheKeys() {
        super();
    }

    /**
     * Surrounds the supplied cache key suffix or group name with curly braces to mark it as <i>hashtag</i>.
     * <p>
     * Example:
     * <pre>
     *  ox-cache:context:v1:{1234}
     * </pre>
     *
     * @param keyPart The cache key suffix or group name to surround
     * @return The supplied key part marked as <i>hashtag</i>
     */
    public static String asHashPart(int keyPart) {
        return new StringBuilder(10).append(HASH_MARKER_START).append(keyPart).append(HASH_MARKER_END).toString();
    }

    /**
     * Surrounds the supplied cache key suffix or group name with curly braces to mark it as <i>hashtag</i>.
     * <p>
     * Example:
     * <pre>
     *  ox-cache:context:v1:{1234}
     * </pre>
     *
     * @param keyPart The cache key suffix or group name to surround
     * @return The supplied key part marked as <i>hashtag</i>
     * @throws IllegalArgumentException If given key part is <code>null</code> or its string representation is empty
     */
    public static String asHashPart(Object keyPart) {
        String s = keyPart == null ? null : keyPart.toString();
        if (Strings.isEmpty(s)) {
            throw new IllegalArgumentException("The key part must not be null or empty");
        }

        return new StringBuilder(s.length() + 2).append(HASH_MARKER_START).append(s).append(HASH_MARKER_END).toString(); // NOSONARLINT
    }

    /**
     * Removes surrounding curly braces from the given key suffix or group name that was used as <i>hashtag</i>.
     *
     * @param hashPart The cache key suffix or group name marked as <i>hashtag</i>
     * @return The supplied cache key suffix or group name, stripped by its surrounding markers
     */
    public static String stripHashMarkers(String hashPart) {
        if (null != hashPart) {
            int length = hashPart.length();
            if (length > 2 && HASH_MARKER_START == hashPart.charAt(0) && HASH_MARKER_END == hashPart.charAt(length - 1)) {
                return hashPart.substring(1, length - 1);
            }
        }
        return hashPart;
    }

}
