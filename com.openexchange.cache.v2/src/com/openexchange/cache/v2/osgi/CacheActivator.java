/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2.osgi;

import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.properties.CacheProperty;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link CacheActivator} - The activator for <code>"com.openexchange.cache.v2"</code> bundle.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class CacheActivator extends HousekeepingActivator {

    /**
     * Initializes a new {@link CacheActivator}.
     */
    public CacheActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { LeanConfigurationService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        LeanConfigurationService configService = getService(LeanConfigurationService.class);
        long defaultExpirationSeconds = configService.getLongProperty(CacheProperty.DEFAULT_EXPIRATION_SECONDS);
        CacheOptions.setDefaultExpirationSeconds(defaultExpirationSeconds);
    }

}
