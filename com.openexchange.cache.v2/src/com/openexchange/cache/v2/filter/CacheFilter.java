/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.cache.v2.filter;

import static com.openexchange.cache.v2.filter.CacheFilters.moduleNameFor;
import static com.openexchange.cache.v2.filter.CacheFilters.versionNameFor;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.java.Strings;

/**
 * {@link CacheFilter} - A cache filter to match by wild-card or by fully-qualifying key.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class CacheFilter implements Comparable<CacheFilter> {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CacheFilter.class);

    /** The delimiter character for parts of a filter expression */
    public static final char DELIMITER = ':';

    /** The wild-card character (<code>'*'</code>) to signal any character sequence is accepted */
    public static final char WILDCARD = '*';

    /**
     * The wild-card ending for a cache filter expression (<code>":*"</code>).<br>
     * <pre>Example: "ox-cache:users:v1:1337:*"</pre>
     */
    public static final String WILDCARD_ENDING = new StringBuilder(2).append(DELIMITER).append(WILDCARD).toString();

    private static final CacheFilter ALL = new CacheFilter(null, null, null);

    /**
     * Gets the special "match all" cache filter: every cache key is accepted.
     *
     * @return The "match all" cache filter
     */
    public static CacheFilter matchAll() {
        return ALL;
    }

    /** The expected start of a cache filter expression: <code>"ox-cache:"</code> */
    private static final String CORE_START = ApplicationName.getInstance().getName() + DELIMITER;

    /** The match-all filter expression: <code>"ox-cache:*"</code> */
    private static final String FILTER_EXPRESSION_MATCH_ALL = CacheFilter.matchAll().getFilterExpression();

    /**
     * Parses specified filter expression to a cache filter instance.
     *
     * @param filterExpression The filter expression; e.g. <code>"ox-cache:users:v1:1337:*"</code>
     * @return The cache filter
     */
    public static CacheFilter parseFromExpression(String filterExpression) { // NOSONARLINT
        if (Strings.isEmpty(filterExpression)) {
            throw new IllegalArgumentException("Filter expression must not be null or empty");
        }

        if (filterExpression.indexOf('[') >= 0 || filterExpression.indexOf(']') >= 0) {
            throw new IllegalArgumentException("Filter expression must not be a group key");
        }

        String filter = filterExpression.trim();
        if (FILTER_EXPRESSION_MATCH_ALL.equals(filter)) {
            return matchAll();
        }
        if (!filter.startsWith(CORE_START)) {
            throw new IllegalArgumentException("Invalid filter expression: " + filterExpression);
        }

        boolean isWildcard = false;
        if (filter.endsWith(WILDCARD_ENDING)) {
            isWildcard = true;
            filter = filter.substring(0, filter.length() - 2);
            if (filter.isEmpty()) {
                throw new IllegalArgumentException("Invalid filter expression: " + filterExpression);
            }
        }

        // Split to parts and check each part to be not empty
        String[] parts = Strings.splitBy(filter, DELIMITER, true);
        for (String part : parts) {
            if (Strings.isEmpty(part)) {
                throw new IllegalArgumentException("Invalid filter expression (infix part must not be empty): " + filterExpression);
            }
        }

        // Turn parts to appropriate cache filter
        switch (parts.length) {
            case 0, 1:
                // "ox-cache"
                return CacheFilter.builder().build();
            case 2:
                // "ox-cache:context"
                return builderWith(parts[1]).build();
            case 3:
                // "ox-cache:context:v1"
                return builderWith(parts[1], parts[2]).build();
            default:
                // "ox-cache:context:v1:1337"
                CacheFilter.Builder builder = builderWith(parts[1], parts[2]);
                int mlen = parts.length - 1;
                for (int i = 3; i < mlen; i++) {
                    builder.addSuffix(parts[i]);
                }
                // Add last suffix
                if (isWildcard) {
                    builder.addSuffixAsWildcard(parts[mlen]);
                } else {
                    builder.addSuffix(parts[mlen]);
                }
                return builder.build();
        }
    }

    /**
     * Creates a filter builder with given module name.
     *
     * @param moduleName The module name
     * @return The newly created filter builder
     */
    private static Builder builderWith(String moduleName) {
        return CacheFilter.builder().withModuleName(moduleNameFor(moduleName));
    }

    /**
     * Creates a filter builder with given module and version name.
     *
     * @param moduleName The module name
     * @param versionName The version name
     * @return The newly created filter builder
     */
    private static Builder builderWith(String moduleName, String versionName) {
        return CacheFilter.builder()
            .withModuleName(moduleNameFor(moduleName))
            .withVersionName(versionNameFor(versionName));
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final ModuleName moduleName;
    private final VersionName versionName;
    private final String suffix;
    private final String filterExpression;
    private final String prefix;
    private int hashCode = 0; // NOSONARLINT

    /**
     * Initializes a new {@link DefaultCacheKey}.
     *
     * @param moduleName The module name
     * @param versionName The version name
     * @param suffix The suffix
     */
    CacheFilter(ModuleName moduleName, VersionName versionName, String suffix) {
        super();
        this.moduleName = moduleName;
        this.versionName = versionName;
        this.suffix = suffix;
        this.filterExpression = createFilter(moduleName, versionName, suffix);
        int length = filterExpression.length();
        prefix = length == 0 ? filterExpression : (filterExpression.charAt(length - 1) == WILDCARD ? filterExpression.substring(0, length - 1) : filterExpression);
    }

    private static final boolean LOG_WILDCARD_USAGES = false;

    private static String createFilter(ModuleName moduleName, VersionName versionName, String suffix) {
        StringBuilder sb = new StringBuilder(64);
        sb.append(ApplicationName.getInstance().getName()).append(DELIMITER);
        if (moduleName == null) {
            // Only application name: implicit wild-card expression --> "ox-cache:*"
            sb.append(WILDCARD);
            if (LOG_WILDCARD_USAGES) {
                logWildcardUsage(sb);
            }
            return sb.toString();
        }
        sb.append(moduleName.getName()).append(DELIMITER);
        if (versionName == null) {
            // Only application name & module name: implicit wild-card expression --> "ox-cache:users:*"
            sb.append(WILDCARD);
            if (LOG_WILDCARD_USAGES) {
                logWildcardUsage(sb);
            }
            return sb.toString();
        }
        sb.append(versionName.getName()).append(DELIMITER);
        if (suffix == null) {
            // Only application name, module name & version name: implicit wild-card expression --> "ox-cache:users:v1:*"
            sb.append(WILDCARD);
            if (LOG_WILDCARD_USAGES) {
                logWildcardUsage(sb);
            }
            return sb.toString();
        }
        // Either fully-qualifying name or wild-card expression --> "ox-cache:users:v1:1337:3" or "ox-cache:users:v1:1337:*"
        sb.append(suffix);
        return sb.toString();
    }

    private static void logWildcardUsage(StringBuilder sb) {
        LOG.debug("Detected usage of wild-card cache filter: {}", sb, new Throwable("Wild-card cache filter detection"));
    }

    @Override
    public int compareTo(CacheFilter o) {
        return this.filterExpression.compareTo(o.filterExpression);
    }

    /**
     * Gets the filter expression, which is either:
     * <ul>
     * <li>The general wild-card expression (<code>"ox-cache:*"</code>) to signal {@link #matchAll() interest in all keys}
     * <li>A wild-card expression; e.g. <code>"ox-cache:users:v1:1337:*"</code>
     * <li>Or a fully-qualifying key; e.g. <code>"ox-cache:users:v1:1337:3"</code>
     * </ul>
     *
     * @return The filter expression or an empty string
     */
    public String getFilterExpression() {
        return filterExpression;
    }

    /**
     * Gets the application name; e.g. <code>"ox-cache"</code>.
     *
     * @return The application name
     */
    public ApplicationName getApp() {
        return ApplicationName.getInstance();
    }

    /**
     * Gets the module name.
     *
     * @return The module name or <code>null</code>
     */
    public ModuleName getModule() {
        return moduleName;
    }

    /**
     * Gets the version name.
     *
     * @return The version name or <code>null</code>
     */
    public VersionName getVersion() {
        return versionName;
    }

    /**
     * Gets the suffix.
     *
     * @return The suffix or <code>null</code>
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * Whether the key matches this filter.
     *
     * @param key The key to check
     * @return <code>true</code> if key matches; otherwise <code>false</code>
     */
    public boolean matches(CacheKey key) {
        return key.getFQN().startsWith(prefix);
    }

    @Override
    public int hashCode() {
        int retval = hashCode;
        if (retval == 0) {
            retval = 31 * 1 + ((filterExpression == null) ? 0 : filterExpression.hashCode());
            hashCode = retval;
        }
        return retval;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return true;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        CacheFilter other = (CacheFilter) obj;
        if (filterExpression == null) {
            if (other.filterExpression != null) {
                return false;
            }
        } else if (!filterExpression.equals(other.filterExpression)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return filterExpression;
    }

    // --------------------------------------------------------- Builder -------------------------------------------------------------------

    /**
     * Creates a new builder.
     *
     * @return The new builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * The builder for an instance of <code>CacheFilter</code>.
     */
    public static final class Builder {

        private ModuleName moduleName;
        private VersionName versionName;
        private final StringBuilder suffixBuilder;

        /**
         * Initializes a new {@link Builder}.
         */
        private Builder() {
            super();
            suffixBuilder = new StringBuilder(16);
        }

        /**
         * Sets given module name.
         *
         * @param moduleName The module name to set
         * @return This builder
         */
        public Builder withModuleName(ModuleName moduleName) {
            if (moduleName != null) {
                this.moduleName = moduleName;
            }
            return this;
        }

        /**
         * Sets given core module name.
         *
         * @param coreModuleName The core module name to set
         * @return This builder
         */
        public Builder withCoreModuleName(CoreModuleName coreModuleName) {
            if (coreModuleName != null) {
                this.moduleName = coreModuleName;
                this.versionName = coreModuleName.getVersionName();
            }
            return this;
        }

        /**
         * Sets given version name.
         *
         * @param versionName The version name to set
         * @return This builder
         */
        public Builder withVersionName(VersionName versionName) {
            if (versionName != null) {
                this.versionName = versionName;
            }
            return this;
        }

        /**
         * Sets given version name according to given version number.
         *
         * @param versionName The version name to set
         * @return This builder
         */
        public Builder withVersionName(int versionNumber) {
            versionName = DefaultVersionName.versionNameFor(versionNumber);
            return this;
        }

        /**
         * Adds given <code>int</code> suffix portion.
         *
         * @param suffix The <code>int</code> suffix to add
         * @return This builder
         */
        public Builder addSuffix(int suffix) {
            return addSuffix(suffix, false);
        }

        /**
         * Adds each given <code>int</code> as separate suffix.
         *
         * @param suffixes The <code>int</code> suffixes to add
         * @return This builder
         */
        public Builder addSuffixes(int... suffixes) {
            if (null != suffixes) {
                for (int suffix : suffixes) {
                    addSuffix(suffix, false);
                }
            }
            return this;
        }

        /**
         * Adds given <code>int</code> suffix portion as terminating wild-card; e.g. <code>"ox-cache:users:v1:1337:*"</code>.
         * <p>
         * Appending further suffixes to this builder will yield an <code>IllegalArgumentException</code>.
         * <p>
         * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
         * <b>Note</b>: Using wild-card invalidation may be a performance bottleneck.
         * </div>
         * <p>
         *
         * @param suffix The <code>int</code> suffix to add as terminating wild-card
         * @return This builder
         */
        public Builder addSuffixAsWildcard(int suffix) {
            return addSuffix(suffix, true);
        }

        /**
         * Adds specified suffix portion to suffix builder.
         *
         * @param suffix The suffix portion to add
         * @param asWildcard <code>true</code> to append wild-card character; otherwise <code>false</code>
         * @return This builder
         */
        private Builder addSuffix(int suffix, boolean asWildcard) {
            checkAndPrepareSuffixBuilder();
            suffixBuilder.append(suffix);
            optAppendWildcard(asWildcard);
            return this;
        }

        /**
         * Adds given <code>long</code> suffix portion.
         *
         * @param suffix The <code>long</code> suffix to add
         * @return This builder
         */
        public Builder addSuffix(long suffix) {
            return addSuffix(suffix, false);
        }

        /**
         * Adds given <code>long</code> suffix portion as terminating wild-card; e.g. <code>"ox-cache:users:v1:1337:*"</code>.
         * <p>
         * Appending further suffixes to this builder will yield an <code>IllegalArgumentException</code>.
         * <p>
         * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
         * <b>Note</b>: Using wild-card invalidation may be a performance bottleneck.
         * </div>
         * <p>
         *
         * @param suffix The <code>long</code> suffix to add as terminating wild-card
         * @return This builder
         */
        public Builder addSuffixAsWildcard(long suffix) {
            return addSuffix(suffix, true);
        }

        /**
         * Adds specified suffix portion to suffix builder.
         *
         * @param suffix The suffix portion to add
         * @param asWildcard <code>true</code> to append wild-card character; otherwise <code>false</code>
         * @return This builder
         */
        private Builder addSuffix(long suffix, boolean asWildcard) {
            checkAndPrepareSuffixBuilder();
            suffixBuilder.append(suffix);
            optAppendWildcard(asWildcard);
            return this;
        }

        /**
         * Adds given <code>char</code> suffix portion.
         *
         * @param suffix The <code>char</code> suffix to add
         * @return This builder
         */
        public Builder addSuffix(char suffix) {
            return addSuffix(suffix, false);
        }

        /**
         * Adds given <code>char</code> suffix portion as terminating wild-card; e.g. <code>"ox-cache:mycache:v1:c:*"</code>.
         * <p>
         * Appending further suffixes to this builder will yield an <code>IllegalArgumentException</code>.
         * <p>
         * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
         * <b>Note</b>: Using wild-card invalidation may be a performance bottleneck.
         * </div>
         * <p>
         *
         * @param suffix The <code>char</code> suffix to add as terminating wild-card
         * @return This builder
         */
        public Builder addSuffixAsWildcard(char suffix) {
            return addSuffix(suffix, true);
        }

        /**
         * Adds specified suffix portion to suffix builder.
         *
         * @param suffix The suffix portion to add
         * @param asWildcard <code>true</code> to append wild-card character; otherwise <code>false</code>
         * @return This builder
         */
        private Builder addSuffix(char suffix, boolean asWildcard) {
            checkAndPrepareSuffixBuilder();
            suffixBuilder.append(suffix);
            optAppendWildcard(asWildcard);
            return this;
        }

        /**
         * Adds given suffix portion.
         *
         * @param suffix The suffix to add
         * @return This builder
         */
        public Builder addSuffix(String suffix) {
            return addSuffix(suffix, false);
        }

        /**
         * Adds each given <code>String</code> as separate suffix.
         *
         * @param suffixes The <code>String</code> suffixes to add
         * @return This builder
         */
        public Builder addSuffixes(String... suffixes) {
            if (null != suffixes) {
                for (String suffix : suffixes) {
                    addSuffix(suffix, false);
                }
            }
            return this;
        }

        /**
         * Adds given suffix portion as terminating wild-card; e.g. <code>"ox-cache:users:v1:1337:*"</code>.
         * <p>
         * Appending further suffixes to this builder will yield an <code>IllegalArgumentException</code>.
         * <p>
         * <div style="margin-left: 0.1in; margin-right: 0.5in; margin-bottom: 0.1in; background-color:#FFDDDD;">
         * <b>Note</b>: Using wild-card invalidation may be a performance bottleneck.
         * </div>
         * <p>
         *
         * @param suffix The suffix to add as terminating wild-card
         * @return This builder
         */
        public Builder addSuffixAsWildcard(String suffix) {
            return addSuffix(suffix, true);
        }

        /**
         * Adds specified suffix portion to suffix builder.
         *
         * @param suffix The suffix portion to add
         * @param asWildcard <code>true</code> to append wild-card character; otherwise <code>false</code>
         * @return This builder
         */
        private Builder addSuffix(String suffix, boolean asWildcard) {
            if (Strings.isEmpty(suffix)) {
                return this;
            }
            if (suffix.indexOf(DELIMITER) >= 0 || suffix.indexOf(WILDCARD) >= 0 || suffix.indexOf('[') >= 0 || suffix.indexOf(']') >= 0) {
                throw new IllegalArgumentException("Invalid suffix: " + suffix);
            }
            checkAndPrepareSuffixBuilder();
            suffixBuilder.append(suffix.trim());
            optAppendWildcard(asWildcard);
            return this;
        }

        /**
         * Checks suffix builder if already ends with wild-card character.
         * <p>
         * Appends delimiter character as preparation for next suffix portion
         * @throws IllegalArgumentException If suffix builder has already been terminated as a wild-card expression
         */
        private void checkAndPrepareSuffixBuilder() {
            if (suffixBuilder.length() > 0) {
                if (suffixBuilder.charAt(suffixBuilder.length() - 1) == '*') {
                    // Already ends with wild-card
                    throw new IllegalArgumentException("Cannot append suffix to wild-card expression");
                }
                suffixBuilder.append(DELIMITER);
            }
        }

        /**
         * Appends the wild-card character to suffix builder if flag advertises <code>true</code>.
         *
         * @param asWildcard <code>true</code> to append wild-card character; otherwise <code>false</code>
         */
        private void optAppendWildcard(boolean asWildcard) {
            if (asWildcard) {
                suffixBuilder.append(WILDCARD_ENDING);
            }
        }

        /**
         * Builds the resulting instance of <code>CacheFilter</code> from this builder.
         *
         * @return The resulting instance of <code>CacheFilter</code>
         * @throws IllegalArgumentException If this builder's arguments are inconsistent
         */
        public CacheFilter build() {
            checkValidity();
            boolean suffixIsEmpty = suffixBuilder.length() <= 0;
            if (moduleName == null && versionName == null && suffixIsEmpty) {
                return ALL;
            }
            return new CacheFilter(moduleName, versionName, suffixIsEmpty ? null : suffixBuilder.toString());
        }

        private void checkValidity() {
            if (moduleName == null) {
                if (versionName != null || suffixBuilder.length() > 0) { // NOSONARLINT
                    throw new IllegalArgumentException("Neither version nor suffix may be specified if no module name is set");
                }
            }
            if (versionName == null) {
                if (suffixBuilder.length() > 0) { // NOSONARLINT
                    throw new IllegalArgumentException("No suffix may be specified if no version name is set");
                }
            }
        }
    }

}
