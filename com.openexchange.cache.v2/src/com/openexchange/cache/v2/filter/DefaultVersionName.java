/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.filter;

/**
 * {@link DefaultVersionName} - The default version name implementation.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class DefaultVersionName implements VersionName {

    /**
     * Gets the version name for given version number.
     *
     * @param versionNumber The version number
     * @return The version name
     * @throws IllegalArgumentException If given version number is equal to or less than <code>0</code> (zero)
     */
    public static VersionName versionNameFor(int versionNumber) {
        return versionNumber == DEFAULT_VERSION_NAME.getVersionNumber() ? DEFAULT_VERSION_NAME : new DefaultVersionName(versionNumber);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final int versionNumber;

    /**
     * Initializes a new {@link DefaultVersionName}.
     *
     * @param versionNumber The version number
     */
    private DefaultVersionName(int versionNumber) {
        super();
        if (versionNumber <= 0) {
            throw new IllegalArgumentException("Version number must be greater than 0 (zero)");
        }
        this.versionNumber = versionNumber;
    }

    @Override
    public int getVersionNumber() {
        return versionNumber;
    }

}
