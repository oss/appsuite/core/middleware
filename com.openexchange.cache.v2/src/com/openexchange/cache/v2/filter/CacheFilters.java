/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2.filter;

import com.openexchange.cache.v2.CacheKeys;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.java.Strings;

/**
 * {@link CacheFilters} - Utility class for cache filter.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class CacheFilters {

    /**
     * Initializes a new {@link CacheFilters}.
     */
    private CacheFilters() {
        super();
    }

    /**
     * Checks if given cache filter references a context entry in cache.
     *
     * @param cacheKey The cache filter to examine
     * @return <code>true</code> if cache filter references a context entry; otherwise <code>false</code>
     */
    public static boolean isContextFilter(CacheFilter cacheFilter) {
        return isFilterOfModule(cacheFilter, CoreModuleName.CONTEXT);
    }

    /**
     * Checks if given cache filter references an entry in cache associated with specified module.
     *
     * @param cacheFilter The cache filter to examine
     * @param moduleName The module name to check against
     * @return <code>true</code> if cache filter references a module's cache entry; otherwise <code>false</code>
     */
    public static boolean isFilterOfModule(CacheFilter cacheFilter, ModuleName moduleName) {
        if (cacheFilter == null || moduleName == null) {
            return false;
        }

        ModuleName m0duleName = cacheFilter.getModule();
        return (m0duleName != null && moduleName.getName().equals(m0duleName.getName()));
    }

    /**
     * Gets the {@link ModuleName} for the specified name
     *
     * @param name The name
     * @return The {@link ModuleName}
     * @throws IllegalArgumentException If module name is <code>null</code> or empty
     */
    public static ModuleName moduleNameFor(String name) {
        if (Strings.isEmpty(name)) {
            throw new IllegalArgumentException("Module name must not null or empty");
        }
        CoreModuleName coreModuleName = CoreModuleName.moduleNameFor(Strings.asciiLowerCase(name));
        return coreModuleName == null ? new StringModuleName(name) : coreModuleName;
    }
    
    /**
     * Gets the {@link VersionName} for the specified version number
     *
     * @param version The version number
     * @return The {@link VersionName}
     * @throws IllegalArgumentException If version number is equal to or less than <code>0</code> (zero)
     */
    public static VersionName versionNameFor(int version) {
        if (version <= 0) {
            throw new IllegalArgumentException("Version number must not be equal to or less than 0 (zero)");
        }
        return VersionName.DEFAULT_VERSION_NAME.getVersionNumber() == version ? VersionName.DEFAULT_VERSION_NAME : new IntVersionName(version);
    }

    /**
     * Gets the {@link VersionName} for the specified name
     *
     * @param name The name; e.g. <code>"v1"</code> or <code>"1"</code>
     * @return The {@link VersionName}
     * @throws IllegalArgumentException If version name is <code>null</code> or empty
     * @throws NumberFormatException If specified version name cannot be parsed to a number
     */
    public static VersionName versionNameFor(String name) {
        if (Strings.isEmpty(name)) {
            throw new IllegalArgumentException("Version name must not null or empty");
        }
        String n = Strings.asciiLowerCase(name);
        return VersionName.DEFAULT_VERSION_NAME.getName().equals(n) ? VersionName.DEFAULT_VERSION_NAME : new StringVersionName(n);
    }

    /**
     * Surrounds the supplied cache filter suffix or group name with curly braces to mark it as <i>hashtag</i>.
     * 
     * @param filterPart The cache filter suffix or group name to enhance
     * @return The supplied filter part marked as <i>hashtag</i>
     */
    public static String asHashPart(Object filterPart) {
        return CacheKeys.asHashPart(filterPart);
    }

    /**
     * Surrounds the supplied cache filter suffix or group name with curly braces to mark it as <i>hashtag</i>.
     * 
     * @param filterPart The cache filter suffix or group name to enhance
     * @return The supplied filter part marked as <i>hashtag</i>
     */
    public static String asHashPart(int filterPart) {
        return CacheKeys.asHashPart(filterPart);
    }

    private static class StringModuleName implements ModuleName {

        private final String name;

        StringModuleName(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }
    }

    private static class StringVersionName implements VersionName {

        private final String name;
        private final int version;

        StringVersionName(String name) {
            this.name = name;
            this.version = Integer.parseInt((name.startsWith("v") ? name.substring(1) : name).trim());
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public int getVersionNumber() {
            return version;
        }
    }
    
    private static class IntVersionName implements VersionName {

        private final String name;
        private final int version;

        IntVersionName(int version) {
            this.version = version;
            this.name = new StringBuilder(4).append('v').append(version).toString();
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public int getVersionNumber() {
            return version;
        }
    }

}
