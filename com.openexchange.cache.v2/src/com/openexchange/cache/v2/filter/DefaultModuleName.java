/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.filter;

/**
 * {@link DefaultModuleName} - The default module name implementation accepting expire time in seconds.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class DefaultModuleName implements ModuleName {

    /**
     * Creates an instance of <code>DefaultModuleName</code> for given module name.
     *
     * @param moduleName The module name
     * @return The appropriate instance of <code>DefaultModuleName</code>
     */
    public static DefaultModuleName of(String moduleName) {
        return moduleName == null ? null : new DefaultModuleName(moduleName);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final String name;

    /**
     * Initializes a new {@link DefaultModuleName}.
     *
     * @param name The module name
     * @param timeoutSeconds The expire time in seconds
     */
    public DefaultModuleName(String name) {
        super();
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

}
