/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.core;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.filter.DefaultVersionName;
import com.openexchange.cache.v2.filter.ModuleAndVersionName;
import com.openexchange.cache.v2.filter.ModuleName;
import com.openexchange.cache.v2.filter.VersionName;

/**
 * {@link CoreModuleName} - An enumeration for known core module names.
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.0.0
 */
public enum CoreModuleName implements ModuleName, ModuleAndVersionName {

    ADVERTISEMENT_CONFIG("advConf"),
    ALIAS("alias"),
    CACHE_EVENT_LISTENERS("cacheventlisteners", 600, VersionName.DEFAULT_VERSION_NAME.getVersionNumber()),
    CALENDAR_ACCOUNT("calAct"),
    CAPABILITIES("caps"),
    CONTACTS_ACCOUNT("conAct"),
    CONTEXT("ctx"),
    CONTEXT_ID("ctxid"),
    DATABASE_FOLDER("dbFlds"),
    DB_ASSIGNMENTS("dbAs"),
    DB_CLEANUP("dbCleanUp"),
    FILE_STORAGE_ACCOUNTS("fsAct"),
    FILESTORE("fs"),
    FOLDER_USER_PROPERTY("fldUsrPrp"),
    GLOBAL_FOLDER("glbFlds"),
    GROUP("group"),
    IMAP_EXTERNAL_ACCOUNT_FOLDERS("iapExtAccFlds"),
    LDAP_HOST_NAME("ldapHost"),
    MAIL_ACCOUNT("malAcc"),
    REGIONAL_SETTINGS("regSetts"),
    RESELLER_ADMIN("resAdm"),
    RESELLER_CONTEXT("resCtx"),
    RESOLVED_MAIL_ACCOUNT_IDS("rslvMalAccIds"),
    RESOURCE("resource"),
    SCHEMA_UPDATE_STATE("sUpdSt"),
    USER("usr"),
    USER_IMAP_LOGINS("usrIapLgi"),
    USER_LOGINS("usrLgi"),
    USER_MAIL_ACCOUNTS("usrMalAcc"),
    USER_PERMISSION_BITS("permBits"),
    USER_SETTING_MAIL("usm"),
    ;

    private final String name;
    private final VersionName versionName;
    private final long expirationSeconds;

    /**
     * Initializes a new {@link CoreModuleName}.
     *
     * @param name The name
     */
    private CoreModuleName(String name) {
        this(name, -1L, VersionName.DEFAULT_VERSION_NAME.getVersionNumber());
    }

    /**
     * Initializes a new {@link CoreModuleName}.
     *
     * @param name The name
     * @param versionNumber The cache version number
     */
    private CoreModuleName(String name, int versionNumber) {
        this(name, -1L, versionNumber);
    }

    /**
     * Initializes a new {@link CoreModuleName}.
     *
     * @param name The name
     * @param expirationSeconds The expiration seconds
     * @param versionNumber The cache version number
     */
    private CoreModuleName(String name, long expirationSeconds, int versionNumber) {
        this.name = name;
        this.expirationSeconds = expirationSeconds;
        this.versionName = DefaultVersionName.versionNameFor(versionNumber);
    }

    /**
     * Gets the expiration seconds or <code>-1</code> signaling to use {@link CacheOptions#getDefaultExpirationSeconds()}.
     *
     * @return The expiration seconds or <code>-1</code>
     */
    public long getExpirationSeconds() {
        return expirationSeconds < 0 ? -1L : expirationSeconds;
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * Gets the version name associated with this core module.
     *
     * @return The version name
     */
    @Override
    public VersionName getVersionName() {
        return versionName;
    }

    @Override
    public ModuleName getModuleName() {
        return this;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static final Map<String, CoreModuleName> VALUES = Arrays.stream(CoreModuleName.values()).collect(Collectors.toMap(CoreModuleName::getName, e -> e));

    /**
     * Returns the {@link CoreModuleName} for the specified name
     *
     * @param name the name to resolve
     * @return The {@link CoreModuleName} or <code>null</code> if none exists
     */
    public static CoreModuleName moduleNameFor(String name) {
        return VALUES.get(name);
    }
}
