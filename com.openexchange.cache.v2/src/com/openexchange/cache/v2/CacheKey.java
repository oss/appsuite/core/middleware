/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2;

import java.io.Serializable;

/**
 * {@link CacheKey} - Represents a cache key.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public interface CacheKey extends Serializable, Comparable<CacheKey> {

    /**
     * Gets the <b>f</b>ully-<b>q</b>ualified <b>n</b>ame (FQN) of the key.
     * <p>
     * Example
     * <ul>
     * <li><code>"ox-cache:context:v1:1"</code> (regular FQN)
     * <li><code>"ox-cache:user:v1:1337[1]"</code> (FQN for a group member)
     * <li><code>"ox-cache:user:v1:1337[]"</code> (FQN for a group)
     * </ul>
     *
     * @return The fully-qualified name of the key
     */
    String getFQN();

    /**
     * Gets the optional group identifier; e.g. <code>"1337"</code> (possible context identifier).
     *
     * @return The group identifier or <code>null</code>
     */
    String getGroup();

    /**
     * Gets the type of this cache key.
     *
     * @return The type
     */
    CacheKeyType getType();

    @Override
    default int compareTo(CacheKey o) {
        return getFQN().compareTo(o.getFQN());
    }

}
