/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.cache.v2.invalidation;

import java.util.Collection;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.cache.v2.filter.CacheFilters;
import com.openexchange.cache.v2.filter.ModuleAndVersionName;
import com.openexchange.cache.v2.filter.ModuleName;
import com.openexchange.cache.v2.filter.VersionName;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.annotation.SingletonService;

/**
 * {@link InvalidationCacheService} - Extends existent cache service by the possibility to invalidate cache entries by a filter.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@SingletonService
public interface InvalidationCacheService extends CacheService {

    /**
     * Invalidates all keys which apply to the given filters.
     *
     * @param filters The filters to apply
     * @param fireCacheEvents <code>true</code> to fire cache events for invalidated cache entries; otherwise <code>false</code>
     * @throws OXException If invalidation fails
     */
    default void invalidate(Collection<CacheFilter> filters, boolean fireCacheEvents) throws OXException {
        if (filters != null) {
            for (CacheFilter filter : filters) {
                if (filter != null) {
                    invalidate(filter, fireCacheEvents);
                }
            }
        }
    }

    /**
     * Invalidates all keys which apply to the given filter.
     *
     * @param filter The filter to apply
     * @param fireCacheEvents <code>true</code> to fire cache events for invalidated cache entries; otherwise <code>false</code>
     * @throws OXException If invalidation fails
     * @throws IllegalArgumentException If specified filter is <code>null</code>
     */
    void invalidate(CacheFilter filter, boolean fireCacheEvents) throws OXException;

    /**
     * Invalidates specified group.
     *
     * @param moduleAndVersion The module and version of the cache group
     * @param group The group identifier
     * @param fireCacheEvents <code>true</code> to fire cache events for invalidated cache entries; otherwise <code>false</code>
     * @throws OXException If invalidation fails
     */
    default void invalidateGroup(ModuleAndVersionName moduleAndVersion, String group, boolean fireCacheEvents) throws OXException {
        invalidateGroup(moduleAndVersion.getModuleName(), moduleAndVersion.getVersionName(), group, fireCacheEvents);
    }

    /**
     * Invalidates specified group.
     *
     * @param module The module of the cache group; see {@link CacheFilters#moduleNameFor(String)}
     * @param version The version of the cache group; see {@link CacheFilters#versionNameFor(int)}
     * @param group The group identifier
     * @param fireCacheEvents <code>true</code> to fire cache events for invalidated cache entries; otherwise <code>false</code>
     * @throws OXException If invalidation fails
     */
    void invalidateGroup(ModuleName module, VersionName version, String group, boolean fireCacheEvents) throws OXException;

    /**
     * Invalidates specified group member.
     *
     * @param moduleAndVersion The module and version of the cache group
     * @param group The group identifier
     * @param fireCacheEvents <code>true</code> to fire cache events for invalidated cache entries; otherwise <code>false</code>
     * @param suffix The suffix parts from which the {@link Object#toString() toString()} representation is used
     * @throws OXException If invalidation fails
     */
    default void invalidateGroupMember(ModuleAndVersionName moduleAndVersion, String group, boolean fireCacheEvents, Object... suffix) throws OXException {
        invalidateGroupMember(moduleAndVersion.getModuleName(), moduleAndVersion.getVersionName(), group, fireCacheEvents, suffix);
    }

    /**
     * Invalidates specified group member.
     *
     * @param module The module of the cache group; see {@link CacheFilters#moduleNameFor(String)}
     * @param version The version of the cache group; see {@link CacheFilters#versionNameFor(int)}
     * @param group The group identifier
     * @param fireCacheEvents <code>true</code> to fire cache events for invalidated cache entries; otherwise <code>false</code>
     * @param suffix The suffix parts from which the {@link Object#toString() toString()} representation is used
     * @throws OXException If invalidation fails
     */
    void invalidateGroupMember(ModuleName module, VersionName version, String group, boolean fireCacheEvents, Object... suffix) throws OXException;

}
