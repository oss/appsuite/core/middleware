/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2;

import com.openexchange.exception.OXException;

/**
 * {@link Caches} - Utility class for cache module.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class Caches {

    /**
     * Initializes a new instance of {@link Caches}.
     */
    private Caches() {
        super();
    }

    /**
     * Checks if specified exception signals a caching error.
     *
     * @param e The exception to examine
     * @return <code>true</code> if specified exception signals a caching error; otherwise <code>false</code>
     */
    public static boolean isCacheException(OXException e) {
        if (e == null) {
            return false;
        }

        String prefix = e.getPrefix();
        return "REDIS".equals(prefix) || "CACHE".equals(prefix);
    }

    /**
     * Checks if specified exception signals a timeout.
     *
     * @param e The exception to examine
     * @return <code>true</code> if specified exception signals a timeout; otherwise <code>false</code>
     */
    public static boolean isTimeoutException(OXException e) {
        // com.openexchange.exception.OXException: REDIS-0007 Categories=ERROR Message='Redis command exceeded timeout 5,000 (5s).'
        return e != null && e.equalsCode(7, "REDIS");
    }

}
