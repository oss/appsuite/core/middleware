/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2;

import java.util.Map;
import com.openexchange.cache.v2.codec.CacheValueCodec;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.annotation.SingletonService;

/**
 * {@link CacheService} - The cache service.
 * <p>
 * Check for <code>com.openexchange.cache.v2.invalidation.InvalidationCacheService</code> to have an extended
 * possibility to invalidate cache entries by a filter.
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@SingletonService
public interface CacheService {

    /**
     * Gets the typed view of the cache using given options.
     *
     * @param <V> The value type
     * @param options The options to use for building the cache view
     * @return The cache
     * @throws IllegalArgumentException If given options are <code>null</code>
     */
    <V> Cache<V> getCache(CacheOptions<V> options);

    // ----------------------------------------------------- MGET stuff --------------------------------------------------------------------

    /**
     * Retrieves multiple objects from different cache views that are associated with the specified keys, using the corresponding codecs
     * for de-serialization of the cached data.
     *
     * @param k1 The cache key
     * @param c1 The cache value codec
     * @return The cache objects; individual values are empty if there is no object bound to the appropriate key
     * @throws OXException If the MGET operation fails
     */
    default Map<CacheKey, CacheKeyValue<Object>> mget(CacheKey k1, CacheValueCodec<?> c1) throws OXException {
        return mget(Map.of(k1, c1));
    }

    /**
     * Retrieves multiple objects from different cache views that are associated with the specified keys, using the corresponding codecs
     * for de-serialization of the cached data.
     *
     * @param k1 The first cache key
     * @param c1 The first cache value codec
     * @param k2 The second cache key
     * @param c2 The second cache value codec
     * @return The cache objects; individual values are empty if there is no object bound to the appropriate key
     * @throws OXException If the MGET operation fails
     */
    default Map<CacheKey, CacheKeyValue<Object>> mget(CacheKey k1, CacheValueCodec<?> c1, CacheKey k2, CacheValueCodec<?> c2) throws OXException {
        return mget(Map.of(k1, c1, k2, c2));
    }

    /**
     * Retrieves multiple objects from different cache views that are associated with the specified keys, using the corresponding codecs
     * for de-serialization of the cached data.
     *
     * @param k1 The first cache key
     * @param c1 The first cache value codec
     * @param k2 The second cache key
     * @param c2 The second cache value codec
     * @param k3 The third cache key
     * @param c3 The third cache value codec
     * @return The cache objects; individual values are empty if there is no object bound to the appropriate key
     * @throws OXException If the MGET operation fails
     */
    default Map<CacheKey, CacheKeyValue<Object>> mget(CacheKey k1, CacheValueCodec<?> c1, CacheKey k2, CacheValueCodec<?> c2, CacheKey k3, CacheValueCodec<?> c3) throws OXException {
        return mget(Map.of(k1, c1, k2, c2, k3, c3));
    }

    /**
     * Retrieves multiple objects from different cache views that are associated with the specified keys, using the corresponding codecs
     * for de-serialization of the cached data.
     *
     * @param k1 The first cache key
     * @param c1 The first cache value codec
     * @param k2 The second cache key
     * @param c2 The second cache value codec
     * @param k3 The third cache key
     * @param c3 The third cache value codec
     * @param k4 The fourth cache key
     * @param c4 The fourth cache value codec
     * @return The cache objects; individual values are empty if there is no object bound to the appropriate key
     * @throws OXException If the MGET operation fails
     */
    default Map<CacheKey, CacheKeyValue<Object>> mget(CacheKey k1, CacheValueCodec<?> c1, CacheKey k2, CacheValueCodec<?> c2, CacheKey k3, CacheValueCodec<?> c3, CacheKey k4, CacheValueCodec<?> c4) throws OXException {
        return mget(Map.of(k1, c1, k2, c2, k3, c3, k4, c4));
    }

    /**
     * Retrieves multiple objects from different cache views that are associated with the specified keys, using the corresponding codecs
     * for de-serialization of the cached data.
     *
     * @param keysAndCodecs The keys associated with their corresponding cache codecs to get
     * @return The cache objects; individual values are empty if there is no object bound to the appropriate key
     * @throws OXException If the MGET operation fails
     */
    default Map<CacheKey, CacheKeyValue<Object>> mget(Map.Entry<CacheKey, CacheValueCodec<?>>... keysAndCodecs) throws OXException {
        return keysAndCodecs == null || keysAndCodecs.length <= 0 ? Map.of() : mget(Map.ofEntries(keysAndCodecs));
    }

    /**
     * Retrieves multiple objects from different cache views that are associated with the specified keys, using the corresponding codecs
     * for de-serialization of the cached data.
     *
     * @param keysAndCodecs The keys associated with their corresponding cache codecs to get
     * @return The cache objects; individual values are empty if there is no object bound to the appropriate key
     * @throws OXException If the MGET operation fails
     */
    Map<CacheKey, CacheKeyValue<Object>> mget(Map<CacheKey, CacheValueCodec<?>> keysAndCodecs) throws OXException;

}
