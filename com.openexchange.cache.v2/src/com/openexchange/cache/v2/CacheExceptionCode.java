/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2;

import static com.openexchange.exception.OXExceptionStrings.MESSAGE;
import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;

/**
 * {@link CacheExceptionCode}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public enum CacheExceptionCode implements DisplayableOXExceptionCode {

    /**
     * Unexpected error: %1$s
     */
    UNEXPECTED_ERROR("Unexpected error: %1$s", MESSAGE, CATEGORY_ERROR, 1),
    /**
     * An I/O error occurred: %1$s
     */
    IO_ERROR("An I/O error occurred: %1$s", MESSAGE, CATEGORY_ERROR, 2),
    /**
     * A JSON error occurred: %1$s
     */
    JSON_ERROR("A JSON error occurred: %1$s", MESSAGE, CATEGORY_ERROR, 3),
    /**
     * 'Get' from cache failed for key %1$s.
     */
    FAILED_GET("'Get' from cache failed for key %1$s.", MESSAGE, CATEGORY_ERROR, 4),
    /**
     * 'Put' into cache failed for key %1$s.
     */
    FAILED_PUT("'Put' into cache failed for key %1$s", MESSAGE, Category.CATEGORY_ERROR, 5),
    /**
     * 'Put' into cache failed for key %1$s.
     */
    FAILED_PUT_WITH_TTL("'Put with TTL' into cache failed for key %1$s", MESSAGE, Category.CATEGORY_ERROR, 6),
    /**
     * Remove on cache failed for key %1$s
     */
    FAILED_REMOVE("Remove on cache failed for key %1$s", MESSAGE, Category.CATEGORY_ERROR, 7),
    /**
     * No such message collection: %1$s
     */
    NO_SUCH_MESSAGE_COLLECTION("No such message collection: %1$s", MESSAGE, Category.CATEGORY_ERROR, 8),
    /**
     * Operation timed out
     */
    TIMEOUT("Operation timed out", MESSAGE, Category.CATEGORY_TRY_AGAIN, 9),
    /**
     * 'MGet' from cache failed.
     */
    FAILED_MGET("'MGet' from cache failed.", MESSAGE, CATEGORY_ERROR, 10),
    /**
     * 'MPut' into cache failed.
     */
    FAILED_MPUT("'MPut' into cache failed.", MESSAGE, CATEGORY_ERROR, 11),
    ;

    /** The cache exception prefix */
    private static final String PREFIX = "CACHE";

    /**
     * Gets the prefix for cache exception.
     *
     * @return The <code>"CACHE"</code> prefix
     */
    public static String prefix() {
        return PREFIX;
    }

    private final String message;
    private final String displayMessage;
    private final int detailNumber;
    private final Category category;

    private CacheExceptionCode(final String message, final String displayMessage, final Category category, final int detailNumber) {
        this.message = message;
        this.displayMessage = displayMessage;
        this.detailNumber = detailNumber;
        this.category = category;
    }

    @Override
    public String getPrefix() {
        return PREFIX;
    }

    @Override
    public int getNumber() {
        return detailNumber;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDisplayMessage() {
        return displayMessage;
    }

    @Override
    public boolean equals(final OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this, new Object[0]);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(final Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(final Throwable cause, final Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }

}
