/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2.properties;

import com.openexchange.config.lean.Property;

/**
 * {@link CacheProperty} - Properties enumeration for cache v2.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public enum CacheProperty implements Property {

    /**
     * Defines the default expiration seconds for cache entries.
     */
    DEFAULT_EXPIRATION_SECONDS("defaultExpirationSeconds", Long.valueOf(3600)),
    ;

    private final Object defaultValue;
    private final String fqn;

    /**
     * Initializes a new {@link RedisCacheProperty}.
     *
     * @param appendix The appendix for full-qualified name
     * @param defaultValue The default value
     */
    private CacheProperty(String appendix, Object defaultValue) {
        this.fqn = "com.openexchange.cache.v2." + appendix;
        this.defaultValue = defaultValue;
    }

    /**
     * Returns the fully qualified name for the property
     *
     * @return the fully qualified name for the property
     */
    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    /**
     * Returns the default value of this property
     *
     * @return the default value of this property
     */
    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }
}
