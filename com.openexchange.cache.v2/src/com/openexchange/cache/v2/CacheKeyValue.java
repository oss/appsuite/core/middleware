/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2;

import java.util.NoSuchElementException;

/**
 * {@link CacheKeyValue} - A pair of a cache key and its value.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 * @param <V> The type of the value
 */
public interface CacheKeyValue<V> {

    /**
     * Gets the cache key.
     *
     * @return The cache key
     */
    CacheKey getKey();

    /**
     * Gets the value, if present, otherwise throws {@code NoSuchElementException}.
     *
     * @return The non-<code>null</code> value held by this instance
     * @throws NoSuchElementException If there is no value present
     */
    default V getValue() {
        if (!hasValue()) {
            throw new NoSuchElementException();
        }
        return getValueOrElse(null);
    }

    /**
     * Checks if there is a value present.
     *
     * @return <code>true</code> if there is a value present, otherwise <code>false</code>
     */
    default boolean hasValue() {
        return getValueOrElse(null) != null;
    }

    /**
     * Checks if there is no value present.
     *
     * @return <code>true</code> if there is no value present, otherwise <code>false</code>
     */
    default boolean isEmpty() {
        return getValueOrElse(null) == null;
    }

    /**
     * Gets the value if present, otherwise return <code>other</code>.
     *
     * @param other The value to be returned if there is no value present, may be <code>null</code>
     * @return The value, if present, otherwise <code>other</code>
     */
    V getValueOrElse(V other);

}
