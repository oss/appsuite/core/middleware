/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * {@link SignalingCacheLoader} - Extends cache loader by {@link #isValuePutToCache()} signaling whether the computed value yielded by this
 * loader was not only loaded, but also actually been put into cache.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public abstract class SignalingCacheLoader<V> implements CacheLoader<V> { // NOSONARLINT

    private final AtomicBoolean valuePutToCache;

    /**
     * Initializes a new instance of {@link SignalingCacheLoader}.
     */
    protected SignalingCacheLoader() {
        super();
        valuePutToCache = new AtomicBoolean(false);
    }

    /**
     * Sets whether the computed value yielded by this loader has actually been put into cache.
     * <p>
     * <div style="background-color:#FFDDDD; padding:6px; margin:0px;">
     * <b>NOTE</b>: This method is intended being invoked by the cache!
     * </div>
     *
     * @param valuePutToCache <code>true</code> if this loader's value has been put into cache; otherwise <code>false</code>
     */
    public void setValuePutToCache(boolean valuePutToCache) {
        this.valuePutToCache.set(valuePutToCache);
    }

    /**
     * Checks if the computed value yielded by this loader was not only loaded, but also actually been put into cache.
     *
     * @return <code>true</code> if this loader's value has been put into cache; otherwise <code>false</code>
     */
    public boolean isValuePutToCache() {
        return valuePutToCache.get();
    }

}
