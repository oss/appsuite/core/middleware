/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.events;

import com.openexchange.cache.v2.filter.CacheFilter;

/**
 * {@link CacheEventInterest} - Checks the interest for a certain cache event.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public interface CacheEventInterest {

    /**
     * Gets the filter specifying the keys this listener is interested in.
     *
     * @return The filter
     */
    CacheFilter getFilter();

    /**
     * Checks if listener is only interested in remote cache events.
     *
     * @return <code>true</code> if listener is only interested in remote cache events; otherwise <code>false</code>
     */
    boolean onlyRemote();

}
