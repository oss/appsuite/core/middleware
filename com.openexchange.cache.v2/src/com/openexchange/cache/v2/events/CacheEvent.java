/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.events;

import java.util.List;
import com.openexchange.cache.v2.CacheKey;

/**
 * {@link CacheEvent} - A cache event signaling that certain cache entries became invalid (since invalidated or replaced).
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class CacheEvent {

    /**
     * Creates a cache event for invalidating a cache entry associated with given key.
     *
     * @param key The cache key
     * @return The cache event
     */
    public static CacheEvent create(CacheKey key) {
        if (key == null) {
            throw new IllegalArgumentException("Key must not be null");
        }
        return new CacheEvent(key);
    }

    /**
     * Creates a cache event for invalidating cache entries associated with given keys.
     *
     * @param keys The cache keys
     * @return The cache event
     */
    public static CacheEvent create(List<? extends CacheKey> keys) {
        if (keys == null) {
            throw new IllegalArgumentException("Keys must not be null");
        }
        if (keys.isEmpty()) {
            throw new IllegalArgumentException("Keys must not be empty");
        }
        for (CacheKey key : keys) {
            if (key == null) {
                throw new IllegalArgumentException("Key must not be null");
            }
        }
        return new CacheEvent(keys);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final List<CacheKey> keys;

    /**
     * Initializes a new {@link CacheEvent}.
     *
     * @param key The cache key
     */
    private CacheEvent(CacheKey key) {
        super();
        this.keys = List.of(key);
    }

    /**
     * Initializes a new {@link CacheEvent}.
     *
     * @param keys The cache keys
     */
    private CacheEvent(List<? extends CacheKey> keys) {
        super();
        this.keys = List.copyOf(keys);
    }

    /**
     * Gets the (immutable) cache keys.
     *
     * @return The cache keys
     */
    public List<CacheKey> getKeys() {
        return keys;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(128);
        builder.append('{');
        if (keys != null) {
            builder.append("keys=").append(keys).append(", ");
        }
        builder.append('}');
        return builder.toString();
    }

}
