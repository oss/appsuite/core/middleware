/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.events;

import static com.openexchange.cache.v2.CacheKeys.stripHashMarkers;
import static com.openexchange.java.Autoboxing.I;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.IntFunction;
import java.util.stream.Stream;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.cache.v2.filter.ApplicationName;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.java.Strings;

/**
 * {@link CacheEvents} - Utilities for cache events.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public final class CacheEvents {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CacheEvents.class);

    /**
     * Initializes a new instance of {@link CacheEvents}.
     */
    private CacheEvents() {
        super();
    }

    /**
     * Extracts a specific number of <i>suffixes</i> of a cache key as <code>int</code> primitives, if the key prefixes match the given
     * core module name. Suffixes used as <i>hashtags</i> are parsed implicitly as well.
     *
     * @param cacheKey The cache key to extract the suffixes from
     * @param expectedModuleName The core module name to match
     * @param expectedCount The number of expected numerical suffixes
     * @return The extracted suffixes, or {@link Optional#empty()} if the cache key does not match the expectations
     */
    public static Optional<int[]> extractIntSuffixes(CacheKey cacheKey, CoreModuleName expectedModuleName, int expectedCount) {
        return parseSuffixes(extractSuffixes(cacheKey, expectedModuleName).orElse(null), expectedCount);
    }

    /**
     * Extracts a specific number of <i>suffixes</i> of a cache key as <code>int</code> primitives, if the key prefixes match the given
     * core module name and group identifier.
     *
     * @param cacheKey The group cache key to extract the suffixes from
     * @param expectedModuleName The core module name to match
     * @param expectedGroup The expected group infix
     * @param expectedCount The number of expected numerical suffixes
     * @return The extracted suffixes, or {@link Optional#empty()} if the cache key does not match the expectations
     */
    public static Optional<int[]> extractIntSuffixes(CacheKey cacheKey, CoreModuleName expectedModuleName, String expectedGroup, int expectedCount) {
        if (expectedGroup.equals(cacheKey.getGroup())) {
            return parseSuffixes(extractSuffixes(cacheKey, expectedModuleName).orElse(null), expectedCount);
        }
        return Optional.empty();
    }

    /**
     * Extracts the <i>suffixes</i> of a cache key, if the key prefixes match the given core module name.
     *
     * @param cacheKey The cache key to extract the suffixes from
     * @param expectedModuleName The core module name to match
     * @return The extracted suffixes, or {@link Optional#empty()} if the cache key does not match the core module
     */
    public static Optional<String[]> extractSuffixes(CacheKey cacheKey, CoreModuleName expectedModuleName) {
        if (null == cacheKey) {
            return Optional.empty();
        }
        String[] parts = Strings.splitBy(cacheKey.getFQN(), CacheFilter.DELIMITER, true);
        if (isInvalidPrefix(expectedModuleName, cacheKey.getGroup(), parts)) {
            LOG.debug("Cache key \"{}\" does not match module \"{}\", won't extract suffixes.", cacheKey.getFQN(), expectedModuleName);
            return Optional.empty();
        }
        if (Strings.isNotEmpty(cacheKey.getGroup())) {
            // A group key
            return Optional.of(getSuffixesForGroupKey(cacheKey, parts));
        }
        return Optional.of(Arrays.copyOfRange(parts, 3, parts.length));
    }

    private static final IntFunction<String[]> FUNCTION_STRING_ARRAY = size -> new String[size];

    /**
     * Get the suffixes for the group key
     *
     * @param cacheKey The cache key
     * @param parts The key parts
     * @return The suffixes for the group key
     */
    private static String[] getSuffixesForGroupKey(CacheKey cacheKey, String[] parts) {
        Optional<String[]> optMemberSuffixes = extractGroupSuffixes(cacheKey.getGroup(), parts[parts.length - 1]);
        String[] setSuffixes = Arrays.copyOfRange(parts, 3, parts.length - 1);
        if (optMemberSuffixes.isEmpty()) {
            return setSuffixes;
        }
        return Stream.concat(Arrays.stream(optMemberSuffixes.get()),
                             Arrays.stream(setSuffixes))
                     .toArray(FUNCTION_STRING_ARRAY);
    }

    /**
     * Extracts the suffixes from the group part
     *
     * @param group The group name
     * @param part The group part
     * @return The optional suffixes for the group
     */
    private static Optional<String[]> extractGroupSuffixes(String group, String part) {
        if (part.startsWith(group)) {
            String suffixes = part.substring(group.length() + 1, part.length() - 1);
            return Optional.of(Strings.splitBy(suffixes, CacheFilter.DELIMITER, true));
        }
        return Optional.empty();
    }

    /**
     * Checks if the prefix is invalid
     *
     * @param expectedModuleName The expected module name
     * @param group The optional group part or null
     * @param parts The parts of the key
     * @return <code>true</code> if the prefix is invalid, <code>false</code> otherwise
     */
    private static boolean isInvalidPrefix(CoreModuleName expectedModuleName, String group, String[] parts) {
        return !isValidPrefix(expectedModuleName, group, parts);
    }

    /**
     * Checks if the prefix is valid
     *
     * @param expectedModuleName The expected module name
     * @param group The optional group part or null
     * @param parts The parts of the key
     * @return <code>true</code> if the prefix is valid, <code>false</code> otherwise
     */
    private static boolean isValidPrefix(CoreModuleName expectedModuleName, String group, String[] parts) {
        if (parts == null) {
            return false;
        }
        int minElements = group != null ? 4 : 3;
        if (minElements > parts.length) {
            return false;
        }
        return (ApplicationName.getInstance().getName().equals(parts[0]) &&
            expectedModuleName.getName().equals(parts[1]) &&
            expectedModuleName.getVersionName().getName().equals(parts[2]) &&
            (group == null || parts[3].startsWith(group)));
    }

    /**
     * Parses integer suffixes from the given optional list of suffixes. <i>Hashtag</i> markers around a suffix are ignored implicitly.
     *
     * @param optSuffixes The optional suffixes
     * @param expectedCount The expected suffix count
     * @return The parsed suffixes
     */
    private static Optional<int[]> parseSuffixes(String[] optSuffixes, int expectedCount) {
        if (optSuffixes == null) {
            return Optional.empty();
        }
        if (expectedCount != optSuffixes.length) {
            LOG.info("Cache key suffixes in \"{}\" don't match expected count \"{}\", won't parse suffixes.", Arrays.toString(optSuffixes), I(expectedCount));
            return Optional.empty();
        }
        int[] parsedSuffixes = new int[expectedCount];
        for (int i = 0; i < expectedCount; i++) {
            String suffix = stripHashMarkers(optSuffixes[i]);
            try {
                parsedSuffixes[i] = Strings.parseInt(suffix);
            } catch (NumberFormatException e) {
                LOG.info("Unexpected error parsing \"{}\" to integers", Arrays.toString(optSuffixes), e);
                return Optional.empty();
            }
        }
        return Optional.of(parsedSuffixes);
    }

}
