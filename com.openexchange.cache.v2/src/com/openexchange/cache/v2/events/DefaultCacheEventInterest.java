/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.events;

import com.openexchange.cache.v2.filter.CacheFilter;

/**
 * {@link DefaultCacheEventInterest} - The default implementation for cache event interests.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class DefaultCacheEventInterest implements CacheEventInterest {

    /**
     * Creates a new builder instance.
     *
     * @return The new builder instance
     */
    public static Builder builder() {
        return new Builder();
    }

    /** The builder for an instance of <code>DefaultCacheEventInterest</code> */
    public static final class Builder {

        private CacheFilter filter;
        private boolean onlyRemote;

        /**
         * Initializes a new {@link Builder}.
         */
        Builder() {
            super();
        }

        /**
         * Sets if only interested in remote cache events.
         *
         * @param onlyRemote <code>true</code> for only remote cache events; otherwise <code>false</code> to consider local ones as well
         * @return This builder
         */
        public Builder withOnlyRemote(boolean onlyRemote) {
            this.onlyRemote = onlyRemote;
            return this;
        }

        /**
         * Sets the filter.
         *
         * @param filter The filter to set
         * @return This builder
         */
        public Builder withFilter(CacheFilter filter) {
            this.filter = filter;
            return this;
        }

        /**
         * Creates the resulting instance of <code>DefaultCacheEventInterest</code> from this builder's arguments.
         *
         * @return The instance of <code>DefaultCacheEventInterest</code>
         */
        public DefaultCacheEventInterest build() {
            return new DefaultCacheEventInterest(onlyRemote, filter);
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final boolean onlyRemote;
    private final CacheFilter filter;

    /**
     * Initializes a new {@link DefaultCacheEventInterest}.
     *
     * @param onlyRemote <code>true</code> for only remote cache events; otherwise <code>false</code> to consider local ones as well
     * @param filter The filter
     */
    DefaultCacheEventInterest(boolean onlyRemote, CacheFilter filter) {
        super();
        this.onlyRemote = onlyRemote;
        this.filter = filter;
    }

    @Override
    public boolean onlyRemote() {
        return onlyRemote;
    }

    @Override
    public CacheFilter getFilter() {
        return filter;
    }

}
