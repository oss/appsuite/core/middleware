/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cache.v2.events;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 * {@link AbstractTrackingCacheEventListener} - The abstract service-tracking cache event listener.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public abstract class AbstractTrackingCacheEventListener implements ServiceTrackerCustomizer<CacheEventService, CacheEventService>, CacheEventListener {

    private final BundleContext context;
    private CacheEventListener listener;

    /**
     * Initializes a new {@link AbstractTrackingCacheEventListener}.
     *
     * @param context The bundle context
     */
    protected AbstractTrackingCacheEventListener(BundleContext context) {
        super();
        this.context = context;
    }

    @Override
    public synchronized CacheEventService addingService(ServiceReference<CacheEventService> reference) {
        CacheEventService service = context.getService(reference);
        try {
            CacheEventListener listener = this;
            service.subscribe(listener);
            this.listener = listener;
            return service;
        } catch (Exception e) {
            // Failed
            context.ungetService(reference);
            return null;
        }
    }

    @Override
    public void modifiedService(ServiceReference<CacheEventService> reference, CacheEventService service) {
        // Ignore
    }

    @Override
    public synchronized void removedService(ServiceReference<CacheEventService> reference, CacheEventService service) {
        CacheEventListener listener = this.listener;
        if (listener != null) {
            this.listener = null;
            try {
                service.unsubscribe(listener);
            } catch (Exception e) {
                // Failed
            }
        }
    }

}
