/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.events;

import java.util.Objects;
import java.util.UUID;

/**
 * {@link CacheEventCollectionId} - The identifier for a cache event collection.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class CacheEventCollectionId {

    private final UUID uuid;

    /**
     * Initializes a new {@link CacheEventCollectionId}.
     *
     * @param uuid The UUID of the message collection
     */
    public CacheEventCollectionId(UUID uuid) {
        super();
        this.uuid = uuid;
    }

    /**
     * Gets the UUID of the message collection.
     *
     * @return The message collection UUID
     */
    public UUID getId() {
        return uuid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CacheEventCollectionId other = (CacheEventCollectionId) obj;
        return Objects.equals(uuid, other.uuid);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(40);
        builder.append('[');
        if (uuid != null) {
            builder.append("uuid=").append(uuid);
        }
        builder.append(']');
        return builder.toString();
    }

}
