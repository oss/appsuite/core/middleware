/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cache.v2.events;

import java.util.Collection;
import java.util.Collections;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.annotation.SingletonService;

/**
 * {@link CacheEventService} - The service for cache events.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@SingletonService
public interface CacheEventService {

    /**
     * Publishes given cache event.
     *
     * @param key The key
     * @param cacheEvent The cache event
     * @throws OXException If publishing cache event fails
     * @throws IllegalArgumentException If cache event is <code>null</code>
     */
    default void publish(CacheEvent cacheEvent) throws OXException {
        if (cacheEvent != null) {
            publish(Collections.singleton(cacheEvent));
        }
    }

    /**
     * Publishes given cache events.
     *
     * @param key The key
     * @param cacheEvents The cache events
     * @throws OXException If publishing cache events fails
     * @throws IllegalArgumentException If either of cache events is <code>null</code>
     */
    void publish(Collection<CacheEvent> cacheEvents) throws OXException;

    /**
     * Subscribes given cache event listener to this channel.
     *
     * @param listener The cache event listener to subscribe
     * @throws OXException If subscription fails
     */
    void subscribe(CacheEventListener listener) throws OXException;

    /**
     * Un-Subscribes given cache event listener to this channel.
     *
     * @param listener The cache event listener to un-subscribe
     * @throws OXException If un-subscription fails
     */
    void unsubscribe(CacheEventListener listener) throws OXException;

    // ------------------------------------------------- Event collections -----------------------------------------------------------------

    /**
     * Starts a new cache event collection having given optional cache event as initial element.
     * <p>
     * A cache event collection is meant to {@link #addToCacheEventollection(CacheEvent, CacheEventCollectionId) collect cache events}
     * associated with a certain identifier and to {@link #releaseCacheEventCollection(CacheEventCollectionId) "release"} those cache events
     * later on. Releasing a cache event collection means to publish its contained cache events as a batch operation.
     *
     * @param optionalInitialCacheEvent The optional cache event that is initially contained
     * @return The identifier of the newly started cache event collection
     * @throws OXException If starting a cache event collection fails
     */
    CacheEventCollectionId startCacheEventCollection(CacheEvent optionalInitialCacheEvent) throws OXException;

    /**
     * Adds given cache event to denoted cache event collection.
     *
     * @param cacheEvent The cache event to add
     * @param collectionId The identifier of the cache event collection
     * @throws OXException If no such collection exists
     */
    void addToCacheEventollection(CacheEvent cacheEvent, CacheEventCollectionId collectionId) throws OXException;

    /**
     * Releases denoted cache event collection.
     * <p>
     * All contained cache events are published to channel.
     *
     * @param collectionId The identifier of the cache event collection
     * @throws OXException If no such collection exists
     */
    void releaseCacheEventCollection(CacheEventCollectionId collectionId) throws OXException;

    /**
     * Drops denoted cache event collection.
     * <p>
     * All contained cache events are discarded.
     *
     * @param collectionId The identifier of the cache event collection
     * @throws OXException If dropping the cache event collection fails
     */
    void dropCacheEventCollection(CacheEventCollectionId collectionId) throws OXException;

}
