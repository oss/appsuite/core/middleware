/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.oauth.impl.proxy;

import static com.openexchange.java.Autoboxing.I;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.builder.api.DefaultApi10a;
import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.github.scribejava.core.oauth.OAuthService;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.oauth.API;
import com.openexchange.oauth.KnownApi;
import com.openexchange.oauth.OAuthAccount;
import com.openexchange.oauth.OAuthExceptionCodes;
import com.openexchange.oauth.OAuthServiceMetaData;
import com.openexchange.oauth.http.OAuthProxyHttpHelper;
import com.openexchange.oauth.http.ProxyRequest;
import com.openexchange.session.Session;

/**
 * {@link OAuthProxyHttpHelperImpl}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v7.10.6
 */
public class OAuthProxyHttpHelperImpl implements OAuthProxyHttpHelper {

    private static final Logger LOG = LoggerFactory.getLogger(OAuthProxyHttpHelperImpl.class);

    @Override
    public String execute(Session session, ProxyRequest req) throws OXException, IllegalArgumentException {

        final OAuthRequest httpRequest;
        switch (req.getMethod()) {
            case GET:
                httpRequest = buildGet(req);
                break;
            case DELETE:
                httpRequest = buildDelete(req);
                break;
            case PUT:
                httpRequest = buildPut(req);
                break;
            case POST:
                httpRequest = buildPost(req);
                break;
            default:
                throw new IllegalArgumentException("HTTP method not supported");
        }
        OAuthAccount account = req.getAccount();
        OAuthService service = getOAuthService(account.getMetaData(), session);
        if (service instanceof OAuth10aService o) {
            o.signRequest(getOAuth1Token(account), httpRequest);
        } else if (service instanceof OAuth20Service o) {
            o.signRequest(getOAuth2Token(account).getRawResponse(), httpRequest);
        }
        try {
            Response resp = service.execute(httpRequest);
            return resp.getBody();
        } catch (InterruptedException | ExecutionException e) {
            throw OAuthExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } catch (IOException e) {
            throw OAuthExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Gets the Scribe provider for given API.
     *
     * @param api The API
     * @return The associated Scribe provider
     * @throws IllegalStateException If given API cannot be mapped to a Scribe provider
     */
    protected static Class<?> getProvider(final API api) {
        KnownApi stdApi = KnownApi.getApiByServiceId(api.getServiceId());
        if (stdApi == null) {
            throw new IllegalStateException("Unsupported API type: " + api);
        }
        return stdApi.getApiClass();
    }

    /**
     * Gets the Scribe provider for given API.
     *
     * @param api The API
     * @return The associated Scribe provider
     * @throws OXException
     * @throws IllegalStateException If given API cannot be mapped to a Scribe provider
     */
    protected static com.github.scribejava.core.oauth.OAuthService getOAuthService(OAuthServiceMetaData metaData, Session session) throws OXException {
        ServiceBuilder serviceBuilder = new ServiceBuilder(metaData.getAPIKey(session)).apiSecret(metaData.getAPISecret(session));
        if (metaData instanceof com.openexchange.oauth.impl.ScribeAware e) {
            return serviceBuilder.build(e.getScribeService());
        }
        if (metaData instanceof com.openexchange.oauth.impl.ScribeAware20 e) {
            return serviceBuilder.build(e.getScribeService());
        }
        String serviceId = Strings.asciiLowerCase(metaData.getId());
        KnownApi knownApi = KnownApi.getApiByServiceId(serviceId);
        if (knownApi == null) {
            throw OAuthExceptionCodes.UNSUPPORTED_SERVICE.create(serviceId);
        }
        try {
            if (knownApi.getApiClass().isAssignableFrom(DefaultApi10a.class)) {
                return serviceBuilder.build((DefaultApi10a) knownApi.getApiClass().getConstructor().newInstance());
            }
            if (knownApi.getApiClass().isAssignableFrom(DefaultApi20.class)) {
                return serviceBuilder.build((DefaultApi20) knownApi.getApiClass().getConstructor().newInstance());
            }
        } catch (Exception e1) {
            throw OAuthExceptionCodes.UNSUPPORTED_SERVICE.create(serviceId);
        }
        throw OAuthExceptionCodes.UNSUPPORTED_SERVICE.create(serviceId);
    }

    /**
     * Gets the Scribe token for associated OAuth account.
     *
     * @return The Scribe token
     * @throws OXException If operation fails due to an invalid account
     */
    private static OAuth1AccessToken getOAuth1Token(OAuthAccount account) throws OXException {
        try {
            return new OAuth1AccessToken(account.getToken(), account.getSecret());
        } catch (IllegalArgumentException e) {
            LOG.warn("Associated OAuth '{} ({})' account misses token information.", account.getDisplayName(), I(account.getId()));
            throw OAuthExceptionCodes.INVALID_ACCOUNT_EXTENDED.create(e, account.getDisplayName(), I(account.getId()));
        }
    }

    /**
     * Gets the Scribe token for associated OAuth account.
     *
     * @return The Scribe token
     * @throws OXException If operation fails due to an invalid account
     */
    private static OAuth2AccessToken getOAuth2Token(OAuthAccount account) throws OXException {
        try {
            return new OAuth2AccessToken(account.getToken(), account.getSecret());
        } catch (IllegalArgumentException e) {
            LOG.warn("Associated OAuth '{} ({})' account misses token information.", account.getDisplayName(), I(account.getId()));
            throw OAuthExceptionCodes.INVALID_ACCOUNT_EXTENDED.create(e, account.getDisplayName(), I(account.getId()));
        }
    }

    private static OAuthRequest buildPost(ProxyRequest proxyRequest) {
        OAuthRequest result = new OAuthRequest(Verb.POST, proxyRequest.getUrl());
        proxyRequest.getHeaders().forEach((k, v) -> result.addHeader(k, v));
        proxyRequest.getParameters().forEach((k, v) -> result.addBodyParameter(k, v));
        return result;
    }

    private static OAuthRequest buildPut(ProxyRequest proxyRequest) {
        OAuthRequest result = buildCommon(Verb.GET, proxyRequest);
        result.addBodyParameter("body", proxyRequest.getBody()); // FIXME: find the correct body parameter name
        return result;
    }

    private static OAuthRequest buildGet(ProxyRequest proxyRequest) {
        return buildCommon(Verb.GET, proxyRequest);
    }

    private static OAuthRequest buildDelete(ProxyRequest proxyRequest) {
        return buildCommon(Verb.DELETE, proxyRequest);
    }

    private static OAuthRequest buildCommon(Verb verb, ProxyRequest proxyRequest) {
        OAuthRequest result = new OAuthRequest(verb, proxyRequest.getUrl());
        proxyRequest.getHeaders().forEach((k, v) -> result.addHeader(k, v));
        proxyRequest.getParameters().forEach((k, v) -> result.addQuerystringParameter(k, v));
        return result;
    }

}
