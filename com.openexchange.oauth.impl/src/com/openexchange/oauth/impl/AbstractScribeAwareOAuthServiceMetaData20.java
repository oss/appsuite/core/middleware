/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.oauth.impl;

import static com.openexchange.java.Autoboxing.I;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.SSLHandshakeException;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.net.ssl.exception.SSLExceptionCode;
import com.openexchange.oauth.API;
import com.openexchange.oauth.DefaultAPI;
import com.openexchange.oauth.DefaultOAuthToken;
import com.openexchange.oauth.OAuthConstants;
import com.openexchange.oauth.OAuthExceptionCodes;
import com.openexchange.oauth.OAuthToken;
import com.openexchange.oauth.OAuthUtil;
import com.openexchange.oauth.scope.OAuthScope;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;

/**
 * {@link AbstractScribeAwareOAuthServiceMetaData20}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public abstract class AbstractScribeAwareOAuthServiceMetaData20 extends AbstractExtendedScribeAwareOAuthServiceMetaData implements ScribeAware20 {

    /**
     * Initializes a new {@link AbstractExtendedScribeAwareOAuthServiceMetaData}.
     *
     * @param services The {@link ServiceLookup}
     * @param api The {@link DefaultAPI}
     * @param scopes The {@link OAuthScope}s
     */
    protected AbstractScribeAwareOAuthServiceMetaData20(ServiceLookup services, API api, OAuthScope... scopes) {
        this(services, api, false, true, scopes);
    }

    /**
     * Initializes a new {@link AbstractExtendedScribeAwareOAuthServiceMetaData}.
     *
     * @param services The {@link ServiceLookup} instance
     * @param api The {@link DefaultAPI}
     * @param needsRequestToken Whether it needs a request token
     * @param registerTokenBasedDeferrer Whether to register the token based deferrer
     */
    protected AbstractScribeAwareOAuthServiceMetaData20(ServiceLookup services, API api, boolean needsRequestToken, boolean registerTokenBasedDeferrer, OAuthScope... scopes) {
        super(services, api, needsRequestToken, registerTokenBasedDeferrer, scopes);
    }

    @Override
    public OAuthToken getOAuthToken(Map<String, Object> arguments, Set<OAuthScope> scopes) throws OXException {
        Session session = (Session) arguments.get(OAuthConstants.ARGUMENT_SESSION);
        ServiceBuilder serviceBuilder = new ServiceBuilder(getAPIKey(session)).apiSecret(getAPISecret(session));

        String callbackUrl = (String) arguments.get(OAuthConstants.ARGUMENT_CALLBACK);
        if (null != callbackUrl) {
            serviceBuilder.callback(callbackUrl);
        } else {
            String authUrl = (String) arguments.get(OAuthConstants.ARGUMENT_AUTH_URL);
            String pRedirectUri = "&redirect_uri=";
            int pos = authUrl.indexOf(pRedirectUri);
            int nextPos = authUrl.indexOf('&', pos + 1);
            String callback = nextPos < 0 ? authUrl.substring(pos + pRedirectUri.length()) : authUrl.substring(pos + pRedirectUri.length(), nextPos);
            callback = URLDecoder.decode(callback, StandardCharsets.UTF_8);
            serviceBuilder.callback(callback);
        }

        // Add requested scopes
        String mappings = OAuthUtil.providerScopesToString(scopes);
        if (Strings.isNotEmpty(mappings)) {
            serviceBuilder.withScope(mappings);
        }

        try (OAuth20Service scribeOAuthService = serviceBuilder.build(getScribeService())) {
            OAuth2AccessToken accessToken = scribeOAuthService.getAccessToken((String) arguments.get(com.github.scribejava.core.model.OAuthConstants.CODE));
            return new DefaultOAuthToken(accessToken.getAccessToken(), Strings.isEmpty(accessToken.getRefreshToken()) ? "" : accessToken.getRefreshToken(), accessToken.getExpiresIn() == null ? 0L : System.currentTimeMillis() + accessToken.getExpiresIn() * 1000);
        } catch (IOException e) {
            throw OAuthExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (ExecutionException | InterruptedException e) {
            throw OAuthExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public String getUserIdentity(Session session, int accountId, String accessToken, String accessSecret) throws OXException {
        // Contact the OAuth provider and fetch the identity of the currently logged-in user
        try (OAuth20Service scribeService = new ServiceBuilder(getAPIKey(session)).apiSecret(getAPISecret(session)).build(getScribeService())) {
            OAuthRequest request = new OAuthRequest(getIdentityHTTPMethod(), getIdentityURL(accessToken));
            request.addHeader("Content-Type", getContentType());
            scribeService.signRequest(accessToken, request);
            Response response = execute(scribeService, request);

            int responseCode = response.getCode();
            String body = response.getBody();
            if (responseCode == 403) {
                throw OAuthExceptionCodes.OAUTH_ACCESS_TOKEN_INVALID.create(getId(), I(accountId), I(session.getUserId()), I(session.getContextId()));
            }
            if (responseCode >= 400 && responseCode <= 499) {
                throw OAuthExceptionCodes.DENIED_BY_PROVIDER.create(body);
            }

            final Matcher matcher = compileIdentityPattern(getIdentityFieldName()).matcher(body);
            if (matcher.find()) {
                return matcher.group(1);
            }
            throw OAuthExceptionCodes.CANNOT_GET_USER_IDENTITY.create(getId());
        } catch (IOException e) {
            throw OAuthExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Compiles the pattern that will be used to extract the identity of the user
     * from the identity response.
     *
     * @param fieldName The field name that contains the identity of the user in the identity response
     * @return The compiled {@link Pattern}
     */
    private static Pattern compileIdentityPattern(String fieldName) {
        return Pattern.compile("\"" + fieldName + "\":\\s*\"(\\S*?)\"");
    }

    /**
     * Executes specified request and returns its response.
     *
     * @param request The request
     * @return The response
     * @throws OXException If executing request fails
     */
    private Response execute(OAuth20Service service, OAuthRequest request) throws OXException {
        try {
            // FIXME: no request tuner in the new scribe framework
            //        how to set connection timeout, read timeout and ssl connection factory to request?
            return service.execute(request);
        } catch (com.github.scribejava.core.exceptions.OAuthException e) {
            // Handle Scribe's com.github.scribejava.core.exceptions.OAuthException (inherits from RuntimeException)
            if (ExceptionUtils.isEitherOf(e, SSLHandshakeException.class)) {
                List<Object> displayArgs = new ArrayList<>(2);
                displayArgs.add(SSLExceptionCode.extractArgument(e, "fingerprint"));
                displayArgs.add(getIdentityURL("** obfuscated **"));
                throw SSLExceptionCode.UNTRUSTED_CERTIFICATE.create(e, displayArgs.toArray(new Object[] {}));
            }

            Throwable cause = e.getCause();
            if (cause instanceof java.net.SocketTimeoutException) {
                // A socket timeout
                throw OAuthExceptionCodes.CONNECT_ERROR.create(cause);
            }

            throw OAuthExceptionCodes.OAUTH_ERROR.create(e, e.getMessage());
        } catch (IOException e) {
            throw OAuthExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (ExecutionException | InterruptedException e) {
            throw OAuthExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }
}
