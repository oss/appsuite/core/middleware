/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.oauth.impl.internal;

import static com.openexchange.cluster.map.CoreMap.OAUTH_CALLBACK_REGISTRY;
import static com.openexchange.oauth.OAuthConstants.OAUTH_PROBLEM_PERMISSION_DENIED;
import java.time.Duration;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import com.openexchange.cluster.map.BasicCoreClusterMapProvider;
import com.openexchange.cluster.map.ClusterMapService;
import com.openexchange.cluster.map.RemoteSiteOptions;
import com.openexchange.cluster.map.codec.MapCodecs;
import com.openexchange.http.deferrer.CustomRedirectURLDetermination;
import com.openexchange.oauth.CallbackRegistry;
import com.openexchange.oauth.OAuthConstants;

/**
 * {@link CallbackRegistryImpl}
 *
 * @author <a href="mailto:francisco.laguna@open-xchange.com">Francisco Laguna</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class CallbackRegistryImpl extends BasicCoreClusterMapProvider<String> implements CustomRedirectURLDetermination, CallbackRegistry {

    private static final long MILLIS_10_MINUTES = Duration.ofMinutes(10).toMillis();

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(CallbackRegistryImpl.class);
    }

    // ----------------------------------------------------------------------------------- //

    /**
     * Initializes a new {@link CallbackRegistryImpl}.
     *
     * @param service The service to use
     */
    public CallbackRegistryImpl(ClusterMapService service) {
        super(OAUTH_CALLBACK_REGISTRY, MapCodecs.getStringCodec(), MILLIS_10_MINUTES, RemoteSiteOptions.SYNC, singletonClusterMapServiceSupplier(service));
        Objects.requireNonNull(service, "ClusterMapService must not be null");
    }

    @Override
    public void add(final String token, final String callbackUrl) {
        if (null != token && null != callbackUrl) {
            try {
                getMap().put(token, callbackUrl, MILLIS_10_MINUTES);
            } catch (Exception e) {
                LoggerHolder.LOG.error("Failed to put call-back into cluster map", e);
            }
        }
    }

    @Override
    public String getURL(final HttpServletRequest req) {
        String token = req.getParameter("oauth_token");
        if (null != token) {
            String url = getByToken(token);
            if (null != url) {
                return url;
            }
        }

        token = req.getParameter("state");
        if (null != token && token.startsWith("__ox")) {
            String url = getByToken(token);
            if (null != url) {
                return url;
            }
        }

        token = req.getParameter("denied");
        if (null != token) {
            // Denied...
            String callbackUrl = getByToken(token);
            if (null == callbackUrl) {
                return null;
            }

            StringBuilder callback = new StringBuilder(callbackUrl);
            callback.append(callbackUrl.indexOf('?') > 0 ? '&' : '?');
            callback.append(OAuthConstants.URLPARAM_OAUTH_PROBLEM).append('=').append(OAUTH_PROBLEM_PERMISSION_DENIED);
            return callback.toString();
        }

        return null;
    }

    private String getByToken(String token) {
        try {
            return getMap().remove(token);
        } catch (Exception e) {
            LoggerHolder.LOG.error("Failed to remove call-back from cluster map", e);
            return null;
        }
    }

}
