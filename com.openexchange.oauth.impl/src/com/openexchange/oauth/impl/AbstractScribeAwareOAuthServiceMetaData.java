/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.oauth.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.xml.ws.handler.MessageContext.Scope;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadable;
import com.openexchange.config.Reloadables;
import com.openexchange.java.Strings;
import com.openexchange.oauth.API;
import com.openexchange.oauth.KnownApi;
import com.openexchange.oauth.OAuthConfigurationProperty;
import com.openexchange.oauth.scope.OAuthScope;
import com.openexchange.server.ServiceLookup;

/**
 * {@link AbstractScribeAwareOAuthServiceMetaData}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public abstract class AbstractScribeAwareOAuthServiceMetaData extends AbstractOAuthServiceMetaData implements OAuthIdentityAware, Reloadable {

    private static final String DEFAULT_CONTENT_TYPE = "application/json";
    protected static final String EMPTY_CONTENT_TYPE = " ";

    protected final ServiceLookup services;
    private final List<OAuthPropertyID> propertyNames;
    private static final String PROP_PREFIX = "com.openexchange.oauth";
    private final API api;

    /**
     * Initialises a new {@link AbstractScribeAwareOAuthServiceMetaData}.
     *
     * @param services the service lookup instance
     * @param api The {@link KnownApi}
     * @param scopes The {@link Scope}s
     */
    protected AbstractScribeAwareOAuthServiceMetaData(final ServiceLookup services, API api, OAuthScope... scopes) {
        super(scopes);
        this.services = services;
        this.api = api;

        setId(api.getServiceId());
        setDisplayName(api.getDisplayName());

        // Common properties for all OAuthServiceMetaData implementations.
        propertyNames = new ArrayList<>();
        propertyNames.add(OAuthPropertyID.apiKey);
        propertyNames.add(OAuthPropertyID.apiSecret);

        // Add the extra properties (if any)
        propertyNames.addAll(getExtraPropertyNames());

        // Load configuration
        loadConfiguration();
    }

    /**
     * Load the configuration.
     */
    protected void loadConfiguration() {
        ConfigurationService configService = services.getService(ConfigurationService.class);
        if (null == configService) {
            throw new IllegalStateException("Missing configuration service");
        }
        reloadConfiguration(configService);
    }

    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        StringBuilder builder = new StringBuilder();
        for (OAuthPropertyID prop : propertyNames) {
            String propName = builder.append(PROP_PREFIX).append('.').append(getPropertyId()).append('.').append(prop).toString();
            String propValue = configService.getProperty(propName);
            if (Strings.isEmpty(propValue)) {
                throw new IllegalStateException("Missing following property in configuration: " + propName);
            }
            addOAuthProperty(prop, new OAuthConfigurationProperty(propName, propValue));
            builder.setLength(0);
        }

        // Basic URL encoding
        OAuthConfigurationProperty redirectUrl = getOAuthProperty(OAuthPropertyID.redirectUrl);
        if (redirectUrl != null) {
            String r = urlEncode(redirectUrl.getValue());
            addOAuthProperty(OAuthPropertyID.redirectUrl, new OAuthConfigurationProperty(redirectUrl.getName(), r));
        }
    }

    @Override
    public Interests getInterests() {
        return Reloadables.interestsForProperties(getConfigurationPropertyNames());
    }

    @Override
    public API getAPI() {
        return api;
    }

    @Override
    public String getContentType() {
        return DEFAULT_CONTENT_TYPE;
    }

    /**
     * Get the property identifier
     *
     * @return the property identifier
     */
    protected abstract String getPropertyId();

    /**
     * Get the extra property names
     *
     * @return A collection with extra property names
     */
    protected abstract Collection<OAuthPropertyID> getExtraPropertyNames();
}
