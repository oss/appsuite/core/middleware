/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.threadpool.internal;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Thread factory creating the single master thread that is then used in a single thread executor service to create new threads for the
 * thread pool.
 *
 * This is necessary the ensure that threads for the thread pool are always created with the correct OSGi thread context class loader. See
 * bug 26072 for threads in the thread pool created with the wrong thread context class loader.
 *
 * @author <a href="mailto:marcus.klein@open-xchange.com">Marcus Klein</a>
 */
final class MasterThreadFactory implements ThreadFactory {

    private final String namePrefix;
    private final AtomicReference<CustomThreadPoolExecutor> threadPoolReference;

    /**
     * Initializes a new instance of {@link MasterThreadFactory}.
     *
     * @param namePrefix The name prefix to use
     * @param threadPoolReference The reference for parental thread pool
     */
    MasterThreadFactory(String namePrefix, AtomicReference<CustomThreadPoolExecutor> threadPoolReference) {
        super();
        this.namePrefix = namePrefix;
        this.threadPoolReference = threadPoolReference;
    }

    @Override
    public Thread newThread(Runnable r) {
        return newCustomThread(r, namePrefix + "ThreadCreator", threadPoolReference);
    }

    /**
     * Creates a new custom thread.
     *
     * @param runnable The object whose run method is called
     * @param threadName The name of the new thread which is also used as original name
     * @param threadPoolReference The reference for parental thread pool
     * @return The newly created thread
     */
    public static CustomThread newCustomThread(Runnable runnable, String threadName, AtomicReference<CustomThreadPoolExecutor> threadPoolReference) {
        CustomThread retval = new CustomThread(runnable, threadName, threadPoolReference);
        retval.setUncaughtExceptionHandler(CustomUncaughtExceptionhandler.getInstance());
        return retval;
    }
}
