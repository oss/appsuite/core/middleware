/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.timer;

import org.slf4j.Logger;

/**
 * {@link ScheduledTimerTasks} - Utility class for timer tasks.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class ScheduledTimerTasks {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {
        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ScheduledTimerTasks.class); // NOSONARLINT
    }

    /**
     * Initializes a new instance of {@link ScheduledTimerTasks}.
     */
    private ScheduledTimerTasks() {
        super();
    }

    /**
     * Attempts to cancel execution of specified timer task and tries to remove from the work queue all tasks that have been canceled.
     * <p>
     * A convenience method that invokes {@link #cancelAndPurgeSafely(ScheduledTimerTask, boolean, TimerService)} with
     * <code>mayInterruptIfRunning</code> argument set to <code>false</code>.
     *
     * @param timerTask The timer task to cancel
     * @param timerService The timer service to purge
     */
    public static void cancelAndPurgeSafely(ScheduledTimerTask timerTask, TimerService timerService) {
        cancelAndPurgeSafely(timerTask, false, timerService);
    }

    /**
     * Attempts to cancel execution of specified timer task and tries to remove from the work queue all tasks that have been canceled.
     *
     * @param timerTask The timer task to cancel
     * @param mayInterruptIfRunning <code>true</code> if the thread executing this task should be interrupted; otherwise, in-progress tasks
     *            are allowed to complete
     * @param timerService The timer service to purge
     */
    public static void cancelAndPurgeSafely(ScheduledTimerTask timerTask, boolean mayInterruptIfRunning, TimerService timerService) {
        cancelSafely(timerTask, mayInterruptIfRunning);
        purgeSafely(timerService);
    }

    /**
     * Tries to (safely) remove from the work queue all tasks that have been canceled.
     *
     * @param timerService The timer service to purge
     */
    public static void purgeSafely(TimerService timerService) {
        if (timerService == null) {
            return;
        }

        try {
            timerService.purge();
        } catch (Exception e) {
            LoggerHolder.LOG.error("Failed to purge timer service", e);
        }
    }

    /**
     * Attempts to cancel execution of specified timer task; see {@link ScheduledTimerTask#cancel()}.
     * <p>
     * A convenience method that invokes {@link #cancelSafely(ScheduledTimerTask, boolean)} with <code>mayInterruptIfRunning</code> argument
     * set to <code>false</code>.
     *
     * @param timerTask The timer task to cancel
     */
    public static void cancelSafely(ScheduledTimerTask timerTask) {
        cancelSafely(timerTask, false);
    }

    /**
     * Attempts to cancel execution of specified timer task; see {@link ScheduledTimerTask#cancel(boolean)}.
     *
     * @param timerTask The timer task to cancel
     * @param mayInterruptIfRunning <code>true</code> if the thread executing this task should be interrupted; otherwise, in-progress tasks
     *            are allowed to complete
     */
    public static void cancelSafely(ScheduledTimerTask timerTask, boolean mayInterruptIfRunning) {
        if (timerTask == null) {
            return;
        }

        try {
            timerTask.cancel(mayInterruptIfRunning);
        } catch (Exception e) {
            LoggerHolder.LOG.error("Failed to cancel timer task", e);
        }
    }

}
