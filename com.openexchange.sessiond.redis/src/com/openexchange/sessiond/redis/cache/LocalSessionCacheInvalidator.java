/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.sessiond.redis.cache;

import org.osgi.framework.BundleContext;
import com.openexchange.exception.OXException;
import com.openexchange.pubsub.AbstractTrackingChannelListener;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.Message;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.segmenter.sitechange.SiteChangedListener;
import com.openexchange.session.UserAndContext;
import com.openexchange.sessiond.redis.RedisSessiondService;

/**
 * {@link LocalSessionCacheInvalidator}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class LocalSessionCacheInvalidator extends AbstractTrackingChannelListener<UserAndContext> implements SiteChangedListener {

    private final RedisSessiondService sessiondService;

    /**
     * Initializes a new {@link LocalSessionCacheInvalidator}.
     *
     * @param sessiondService The Redis-backed session service
     * @param context The bundle context
     */
    public LocalSessionCacheInvalidator(RedisSessiondService sessiondService, BundleContext context) {
        super(context);
        this.sessiondService = sessiondService;
    }

    @Override
    public void onMessage(Message<UserAndContext> message) {
        if (message.isRemote()) {
            // Acquire local session cache
            LocalSessionCache localSessionCache = sessiondService.getState().getLocalSessionCache();

            // Invalidate
            int contextId = message.getData().getContextId();
            if (contextId <= 0) {
                localSessionCache.invalidateAll(false);
                return;
            }
            int userId = message.getData().getUserId();
            if (userId <= 0) {
                localSessionCache.removeSessionsByContext(contextId);
                return;
            }
            localSessionCache.removeAndGetSessionsByUser(userId, contextId);
        }
    }

    @Override
    protected Channel<UserAndContext> getChannel(PubSubService service) {
        return LocalSessionCache.getChannel(service);
    }

    @Override
    public void onSiteChange() throws OXException {
        sessiondService.getState().getLocalSessionCache().invalidateAll(false);
    }

}
