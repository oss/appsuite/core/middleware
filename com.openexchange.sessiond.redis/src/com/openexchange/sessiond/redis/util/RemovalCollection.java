/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.sessiond.redis.util;

import static com.openexchange.redis.RedisUtils.deleteChunksAsync;
import static com.openexchange.redis.RedisUtils.disableAutoFlushCommands;
import static com.openexchange.sessiond.redis.util.BrandNames.getBrandIdentifierFrom;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import com.google.common.collect.Lists;
import com.openexchange.exception.OXException;
import com.openexchange.java.Functions;
import com.openexchange.redis.RedisCommandsProvider;
import com.openexchange.session.Session;
import com.openexchange.sessiond.redis.RedisSessiondService;
import io.lettuce.core.RedisException;
import io.lettuce.core.api.sync.RedisKeyCommands;
import io.lettuce.core.api.sync.RedisSetCommands;
import io.lettuce.core.api.sync.RedisSortedSetCommands;

/**
 * {@link RemovalCollection} - Helper class to collect keys/members to drop when removing sessions from Redis storage.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RemovalCollection {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(RemovalCollection.class);

    private List<String> keysToRemove;
    private Map<String, List<String>> sessionsToRemoveFromSets;
    private Map<String, List<String>> sessionsToRemoveFromSortedSets;

    /**
     * Initializes a new {@link RemovalCollection}.
     */
    public RemovalCollection() {
        super();
    }

    /**
     * Resets this collection for being re-used.
     */
    public void reset() {
        keysToRemove = null;
        sessionsToRemoveFromSets = null;
        sessionsToRemoveFromSortedSets = null;
    }

    /**
     * Adds the session to this remove collection.
     * <p>
     * <ul>
     * <li>Session's key; e.g. <code>"ox-session:abcde123de"</code></li>
     * <li>Alternative identifier mapping</li>
     * <li>Auth identifier mapping</li>
     * <li>Brand identifier mapping</li>
     * <li>Session sets</li>
     * <li>Sorted sets</li>
     * </ul>
     *
     * @param session The session to add
     * @param withUserSet Whether to consider session set per user; e.g. <code>"ox-sessionids:1337:3"</code>
     * @param withSortedSet Whether to consider main sorted set; <code>"ox-sessionids-longlife"</code> or <code>"ox-sessionids-shortlife"</code>
     * @return This collection
     */
    public RemovalCollection addSession(Session session, boolean withUserSet, boolean withSortedSet) {
        addKey(RedisSessiondService.getSessionKey(session.getSessionID()));
        addSessionAssociates(session, withUserSet, withSortedSet);
        return this;
    }

    /**
     * Adds the session associates to this remove collection, but not the session's key itself.
     * <p>
     * <ul>
     * <li>Alternative identifier mapping</li>
     * <li>Auth identifier mapping</li>
     * <li>Brand identifier mapping</li>
     * <li>Session sets</li>
     * <li>Sorted sets</li>
     * </ul>
     *
     * @param session The session to add
     * @param withUserSet Whether to consider session set per user; e.g. <code>"ox-sessionids:1337:3"</code>
     * @param withSortedSet Whether to consider main sorted set; <code>"ox-sessionids-longlife"</code> or <code>"ox-sessionids-shortlife"</code>
     * @return This collection
     */
    public RemovalCollection addSessionAssociates(Session session, boolean withUserSet, boolean withSortedSet) {
        Object alternativeId = session.getParameter(Session.PARAM_ALTERNATIVE_ID);
        if (alternativeId != null) {
            addKey(RedisSessiondService.getSessionAlternativeKey(alternativeId.toString()));
        }
        String authId = session.getAuthId();
        if (authId != null) {
            addKey(RedisSessiondService.getSessionAuthIdKey(authId));
        }
        String brandId = getBrandIdentifierFrom(session);
        if (brandId != null) {
            addSortedSetMember(RedisSessiondService.getSetKeyForBrand(brandId), session.getSessionID());
        }
        if (withUserSet) {
            addSetMember(RedisSessiondService.getSetKeyForUser(session.getUserId(), session.getContextId()), session.getSessionID());
        }
        if (withSortedSet) {
            addSortedSetMember(RedisSessiondService.getSortSetKeyForSession(session), session.getSessionID());
        }
        return this;
    }

    /**
     * Adds the given key to remove.
     *
     * @param key The key to remove
     * @return This collection
     */
    public RemovalCollection addKey(String key) {
        if (key == null) {
            return this;
        }
        if (keysToRemove == null) {
            keysToRemove = new ArrayList<>();
        }
        keysToRemove.add(key);
        return this;
    }

    /**
     * Adds the given keys to remove.
     *
     * @param keys The keys to remove
     * @return This collection
     */
    public RemovalCollection addKeys(Collection<String> keys) {
        if (keys == null || keys.isEmpty()) {
            return this;
        }
        if (keysToRemove == null) {
            keysToRemove = new ArrayList<>();
        }
        keysToRemove.addAll(keys.stream().filter(Objects::nonNull).toList());
        return this;
    }

    /**
     * Adds the given member to remove from specified set.
     *
     * @param setKey The key of the set to remove member from
     * @param member The member to remove
     * @return This collection
     */
    public RemovalCollection addSetMember(String setKey, String member) {
        if (setKey == null || member == null) {
            return this;
        }
        if (sessionsToRemoveFromSets == null) {
            sessionsToRemoveFromSets = new HashMap<>();
        }
        sessionsToRemoveFromSets.computeIfAbsent(setKey, Functions.getNewLinkedListFuntion()).add(member);
        return this;
    }

    /**
     * Adds the given member to remove from specified set.
     *
     * @param setKey The key of the sorted set to remove member from
     * @param member The member to remove
     * @return This collection
     */
    public RemovalCollection addSortedSetMember(String setKey, String member) {
        if (setKey == null || member == null) {
            return this;
        }
        if (sessionsToRemoveFromSortedSets == null) {
            sessionsToRemoveFromSortedSets = new HashMap<>();
        }
        sessionsToRemoveFromSortedSets.computeIfAbsent(setKey, Functions.getNewLinkedListFuntion()).add(member);
        return this;
    }

    /**
     * Removes the collected keys/members.
     *
     * @param sessiondService The Redis sessiond service instance to acquire Redis connector & commands
     * @throws OXException If removal fails
     */
    public void removeCollected(RedisSessiondService sessiondService) throws OXException {
        if (isNotEmpty()) {
            sessiondService.getConnector().executeVoidOperation(this::removeCollected);
        }
    }

    private static final int LIMIT = 1000;

    /**
     * Removes the collected keys/members.
     *
     * @param commandsProvider Provides access to different command sets to communicate with Redis end-point
     * @throws RedisException If removal fails
     */
    public void removeCollected(RedisCommandsProvider commandsProvider) {
        if (keysToRemove != null) {
            RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
            if (keysToRemove.size() <= LIMIT) {
                keyCommands.del(keysToRemove.toArray(new String[keysToRemove.size()]));
            } else {
                List<List<String>> partitions = Lists.partition(keysToRemove, LIMIT);
                if (disableAutoFlushCommands(commandsProvider.getConnection())) {
                    try {
                        deleteChunksAsync(partitions, commandsProvider, e -> LOG.error("Failed to delete chunk of keys from Redis storage", e));
                    } catch (Exception e) {
                        LOG.error("Failed to delete keys from Redis storage", e);
                    }
                } else {
                    for (List<String> partition : partitions) {
                        keyCommands.del(partition.toArray(new String[partition.size()]));
                    }
                }
            }
            keysToRemove = null;
        }
        if (sessionsToRemoveFromSets != null) {
            RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();
            for (Map.Entry<String, List<String>> e : sessionsToRemoveFromSets.entrySet()) {
                List<String> sessionIds2Drop = e.getValue();
                if (sessionIds2Drop.size() <= LIMIT) {
                    setCommands.srem(e.getKey(), sessionIds2Drop.toArray(new String[sessionIds2Drop.size()]));
                } else {
                    for (List<String> partition : Lists.partition(sessionIds2Drop, LIMIT)) {
                        setCommands.srem(e.getKey(), partition.toArray(new String[partition.size()]));
                    }
                }
            }
            sessionsToRemoveFromSets = null;
        }
        if (sessionsToRemoveFromSortedSets != null) {
            RedisSortedSetCommands<String, String> sortedSetCommands = commandsProvider.getSortedSetCommands();
            for (Map.Entry<String, List<String>> e : sessionsToRemoveFromSortedSets.entrySet()) {
                List<String> sessionIdsToDrop = e.getValue();
                if (sessionIdsToDrop.size() <= LIMIT) {
                    sortedSetCommands.zrem(e.getKey(), sessionIdsToDrop.toArray(new String[sessionIdsToDrop.size()]));
                } else {
                    for (List<String> partition : Lists.partition(sessionIdsToDrop, LIMIT)) {
                        sortedSetCommands.zrem(e.getKey(), partition.toArray(new String[partition.size()]));
                    }
                }
            }
            sessionsToRemoveFromSortedSets = null;
        }
    }

    /**
     * Checks if this collection is <b>not</b> empty.
     *
     * @return <code>true</code> if <b>not</b> empty; otherwise <code>false</code>
     */
    public boolean isNotEmpty() {
        return (keysToRemove != null) || (sessionsToRemoveFromSets != null) || (sessionsToRemoveFromSortedSets != null);
    }

    /**
     * Checks if this collection is empty.
     *
     * @return <code>true</code> if empty; otherwise <code>false</code>
     */
    public boolean isEmpty() {
        return (keysToRemove == null) && (sessionsToRemoveFromSets == null) && (sessionsToRemoveFromSortedSets == null);
    }

}
