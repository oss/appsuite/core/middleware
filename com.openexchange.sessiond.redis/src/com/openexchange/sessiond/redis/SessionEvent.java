/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.sessiond.redis;

import java.util.Collection;
import com.openexchange.session.UserAndContext;

/**
 * {@link SessionEvent} - A session event.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 *
 */
public final class SessionEvent {

    /**
     * New invalidate event for given session identifiers.
     *
     * @param sessionIds The session identifiers
     * @return The newly created event
     */
    public static SessionEvent newInvalidateSessionEvent(Collection<String> sessionIds) {
        return new SessionEvent(SessionOperation.INVALIDATE, sessionIds, null, null);
    }

    /**
     * New last-gone event for given context identifiers.
     *
     * @param contextIds The context identifiers
     * @return The newly created event
     */
    public static SessionEvent newLastGoneContextSessionEvent(Collection<Integer> contextIds) {
        return new SessionEvent(SessionOperation.LAST_GONE_CONTEXT, null, contextIds, null);
    }

    /**
     * New last-gone event for given user identifiers.
     *
     * @param userIds The user identifiers
     * @return The newly created event
     */
    public static SessionEvent newLastGoneUserSessionEvent(Collection<UserAndContext> userIds) {
        return new SessionEvent(SessionOperation.LAST_GONE_USER, null, null, userIds);
    }

    // ------------------------------------------------------------------------------------------------------------------

    private final SessionOperation operation;
    private final Collection<String> sessionIds;
    private final Collection<Integer> contextIds;
    private final Collection<UserAndContext> userIds;

    /**
     * Initializes a new {@link SessionEvent}.
     *
     * @param operation The operation
     * @param sessionIds The session identifiers
     * @param contextIds The context identifiers
     * @param userIds The user identifiers
     */
    private SessionEvent(SessionOperation operation, Collection<String> sessionIds, Collection<Integer> contextIds, Collection<UserAndContext> userIds) {
        super();
        this.operation = operation;
        this.sessionIds = sessionIds;
        this.contextIds = contextIds;
        this.userIds = userIds;
    }

    /**
     * Gets the operation.
     *
     * @return The operation
     */
    public SessionOperation getOperation() {
        return operation;
    }

    /**
     * Gets the session identifiers.
     * <p>
     * <b>Note</b>: Only set of {@link #getOperation()} returns {@link SessionOperation#INVALIDATE INVALIDATE}
     *
     * @return The session identifiers or <code>null</code>
     */
    public Collection<String> getSessionIds() {
        return sessionIds;
    }

    /**
     * Gets the context identifiers.
     * <p>
     * <b>Note</b>: Only set of {@link #getOperation()} returns {@link SessionOperation#LAST_GONE_CONTEXT LAST_GONE_CONTEXT}
     *
     * @return The context identifiers or <code>null</code>
     */
    public Collection<Integer> getContextIds() {
        return contextIds;
    }

    /**
     * Gets the user identifiers.
     * <p>
     * <b>Note</b>: Only set of {@link #getOperation()} returns {@link SessionOperation#LAST_GONE_USER LAST_GONE_USER}
     *
     * @return The user identifiers or <code>null</code>
     */
    public Collection<UserAndContext> getUserIds() {
        return userIds;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(128);
        builder.append('{');
        if (operation != null) {
            builder.append("operation=").append(operation).append(", ");
        }
        if (sessionIds != null) {
            builder.append("sessionIds=").append(sessionIds);
        }
        if (contextIds != null) {
            builder.append("contextIds=").append(contextIds);
        }
        if (userIds != null) {
            builder.append("userIds=").append(userIds.stream().map(u -> new StringBuilder().append(u.getUserId()).append('@').append(u.getContextId()).toString()).toList());
        }
        builder.append('}');
        return builder.toString();
    }

}
