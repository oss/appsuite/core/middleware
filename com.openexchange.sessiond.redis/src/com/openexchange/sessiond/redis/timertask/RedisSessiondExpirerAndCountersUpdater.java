/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.sessiond.redis.timertask;

import static com.openexchange.log.Constants.DROP_MDC_MARKER;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.log.HumanTimeOutputter;
import com.openexchange.redis.RedisCommandsProvider;
import com.openexchange.session.RemovalReason;
import com.openexchange.session.Session;
import com.openexchange.session.UserAndContext;
import com.openexchange.sessiond.redis.RedisLock;
import com.openexchange.sessiond.redis.RedisSessionConstants;
import com.openexchange.sessiond.redis.RedisSessiondService;
import com.openexchange.sessiond.redis.RedisSessiondState;
import com.openexchange.sessiond.redis.config.RedisSessiondConfigProperty;
import com.openexchange.sessiond.redis.osgi.Services;
import com.openexchange.sessiond.redis.util.Counter;
import com.openexchange.threadpool.ThreadPools;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import io.lettuce.core.KeyScanCursor;
import io.lettuce.core.Limit;
import io.lettuce.core.Range;
import io.lettuce.core.ScanArgs;
import io.lettuce.core.api.sync.RedisHashCommands;
import io.lettuce.core.api.sync.RedisKeyCommands;
import io.lettuce.core.api.sync.RedisSortedSetCommands;

/**
 * {@link RedisSessiondExpirerAndCountersUpdater} - Performs session expiration & the counters update for data structures held in Redis for session data.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RedisSessiondExpirerAndCountersUpdater extends AbstractRedisTimerTask {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(RedisSessiondExpirerAndCountersUpdater.class);

    /**
     * Initializes a new {@link RedisSessiondExpirerAndCountersUpdater}.
     *
     * @param sessiondService The Redis SessiondD service
     */
    public RedisSessiondExpirerAndCountersUpdater(RedisSessiondService sessiondService) {
        super(sessiondService);
    }

    /**
     * Expires sessions & updates the counters for session-related Redis keys and collections.
     *
     * @param lockExpirationMillis The time in milliseconds after which the expiration/counters lock is considered as expired
     * @param lockUpdateFrequencyMillis The frequency in milliseconds for updating the named lock
     */
    public void expireSessionsAndUpdateCounters(long lockExpirationMillis, long lockUpdateFrequencyMillis) {
        if (lockExpirationMillis <= 0) {
            throw new IllegalArgumentException("Lock expiration milliseconds must not be less than or equal to 0 (zero)");
        }

        try {
            // Check whether "last gone" events are supposed to be fired at all
            LeanConfigurationService configService = Services.optService(LeanConfigurationService.class);
            boolean issueLastGoneEvents = configService != null && configService.getBooleanProperty(RedisSessiondConfigProperty.ISSUE_LAST_GONE_EVENTS);

            // Perform session expiration & update counts
            List<Session> expiredSessions = redisSessiondService.getConnector().executeNoTimeoutOperation(commandsProvider -> doExpireSessionsAndUpdateCounters(lockExpirationMillis, lockUpdateFrequencyMillis, issueLastGoneEvents, commandsProvider));

            // Issue possible "last gone" events
            if (issueLastGoneEvents && expiredSessions != null && !expiredSessions.isEmpty()) {
                ThreadPools.submitElseExecute(ThreadPools.task(() -> issueLastGoneEvents(expiredSessions)));
            }
        } catch (Exception e) {
            LOG.warn(DROP_MDC_MARKER, "Failed session expiration & counters update for Redis session storage", e);
        }
    }

    /**
     * Checks for expired sessions in long-lifetime and short-lifetime Redis sorted set and updates the counters (total number of sessions, number of short-term sessions, ...)
     *
     * @param lockExpirationMillis The expiration time for this task's lock in milliseconds
     * @param lockUpdateFrequencyMillis The lock update (aka touch) frequency in milliseconds
     * @param returnExpiredSessions <code>true</code> to return the expired sessions; otherwise <code>false</code> to return an empty list
     * @param commandsProvider The commands provider to access Redis storage
     * @return The expired sessions (or an empty list if <code>returnExpiredSessions</code> is <code>false</code>)
     */
    private List<Session> doExpireSessionsAndUpdateCounters(long lockExpirationMillis, long lockUpdateFrequencyMillis, boolean returnExpiredSessions, RedisCommandsProvider commandsProvider) {
        // Try to acquire lock
        boolean release = true;
        Optional<RedisLock> optLock = RedisLock.lockFor(RedisSessionConstants.REDIS_SESSION_EXPIRE_AND_COUNTERS_LOCK, lockExpirationMillis, lockUpdateFrequencyMillis, commandsProvider, redisSessiondService);
        if (optLock.isPresent()) {
            try {
                // Lock acquired
                LOG.info(DROP_MDC_MARKER, "Starting session expiration & counters update for Redis session storage...");
                long start = System.nanoTime();

                // Remove expired sessions
                RedisSessiondState state = redisSessiondService.getState();
                long now = System.currentTimeMillis();
                List<Session> expiredSessions;
                if (returnExpiredSessions) {
                    expiredSessions = expireSessions(commandsProvider, RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_LONG_LIFETIME, now - state.getSessionLongLifeTime(), true);
                    expiredSessions.addAll(expireSessions(commandsProvider, RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_SHORT_LIFETIME, now - state.getSessionDefaultLifeTime(), true));
                } else {
                    expireSessions(commandsProvider, RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_LONG_LIFETIME, now - state.getSessionLongLifeTime(), false);
                    expireSessions(commandsProvider, RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_SHORT_LIFETIME, now - state.getSessionDefaultLifeTime(), false);
                    expiredSessions = Collections.emptyList();
                }

                // Update counts
                updateCounts(commandsProvider);

                long dur = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start);
                LOG.info(DROP_MDC_MARKER, "Finished session expiration & counters update for Redis session storage after {}", new HumanTimeOutputter(dur, true));

                // Let lock expire for next run since this run was successful
                release = false;
                return expiredSessions;
            } finally {
                optLock.get().unlock(release, commandsProvider);
            }
        }

        LOG.info(DROP_MDC_MARKER, "Aborted session expiration & counters update for Redis session storage. Another node is currently processing or operation already performed recently");
        return Collections.emptyList();
    }

    /**
     * Issues last-gone events for contexts/users in case the last context-/user-associated session has been dropped from Redis storage.
     *
     * @param expiredSessions The sessions that exceeded expiration
     */
    private void issueLastGoneEvents(List<Session> expiredSessions) {
        try {
            // Collect contexts and users for which a session has been dropped from Redis storage
            TIntSet affectedContextIds = new TIntHashSet();
            Set<UserAndContext> affectedUserIds = new HashSet<>();
            for (Session expiredSession : expiredSessions) {
                affectedContextIds.add(expiredSession.getContextId());
                affectedUserIds.add(UserAndContext.newInstance(expiredSession));
            }

            // Check for each context if there are no more sessions in Redis storage associated with that context
            Set<Integer> nullableLastContexts = redisSessiondService.getConnector().executeOperation(commandsProvider -> {
                RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();

                Set<Integer> lastGone = null;
                for (TIntIterator it = affectedContextIds.iterator(); it.hasNext();) {
                    int contextId = it.next();
                    ScanArgs scanArgs = ScanArgs.Builder.matches(RedisSessiondService.getSetKeyPattern(contextId)).limit(RedisSessionConstants.LIMIT_1000);
                    KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
                    boolean found = false;
                    while (!found && cursor != null) {
                        // Check current keys
                        if (!cursor.getKeys().isEmpty()) {
                            found = true;
                        } else {
                            // Move cursor forward
                            cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
                        }
                    }
                    if (!found) {
                        if (lastGone == null) {
                            lastGone = new HashSet<>();
                        }
                        lastGone.add(Integer.valueOf(contextId));
                    }
                }
                return lastGone;
            });

            // Check for each user if there are no more sessions in Redis storage associated with that user
            Set<UserAndContext> nullableLastUsers = redisSessiondService.getConnector().executeOperation(commandsProvider -> {
                RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();

                Set<UserAndContext> lastGone = null;
                for (UserAndContext userId : affectedUserIds) {
                    if (nullableLastContexts != null && nullableLastContexts.contains(Integer.valueOf(userId.getContextId()))) {
                        if (lastGone == null) {
                            lastGone = new HashSet<>();
                        }
                        lastGone.add(userId);
                    } else {
                        ScanArgs scanArgs = ScanArgs.Builder.matches(RedisSessiondService.getSetKeyForUser(userId.getUserId(), userId.getContextId())).limit(RedisSessionConstants.LIMIT_1000);
                        KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
                        boolean found = false;
                        while (!found && cursor != null) {
                            // Check current keys
                            if (!cursor.getKeys().isEmpty()) {
                                found = true;
                            } else {
                                // Move cursor forward
                                cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
                            }
                        }
                        if (!found) {
                            if (lastGone == null) {
                                lastGone = new HashSet<>();
                            }
                            lastGone.add(userId);
                        }
                    }
                }
                return lastGone;
            });

            if (nullableLastContexts != null) {
                redisSessiondService.postCentralLastSessionGoneForContexts(nullableLastContexts, true);
            }
            if (nullableLastUsers != null) {
                redisSessiondService.postCentralLastSessionGoneForUsers(nullableLastUsers, true);
            }
        } catch (Exception e) {
            LOG.error(DROP_MDC_MARKER, "Failed to check for last session gone", e);
        }
    }

    private static final int LIMIT = 1000;

    /**
     * Checks for expired session in specified Redis sorted set based on given threshold time stamp.
     * <p>
     * Any session with an idle time before that threshold are considered as expired
     *
     * @param commandsProvider The commands provider to access Redis storage
     * @param setKey The identifier of the Redis sorted set to check for expired sessions
     * @param thresholdTimeStamp The threshold time stamp when sessions are considered as expired
     * @param returnExpiredSessions <code>true</code> to return the expired sessions; otherwise <code>false</code> to return an empty list
     * @return The expired sessions (or an empty list if <code>returnExpiredSessions</code> is <code>false</code>)
     */
    private List<Session> expireSessions(RedisCommandsProvider commandsProvider, String setKey, long thresholdTimeStamp, boolean returnExpiredSessions) {
        // Determine sessions with expiration time stamp before now
        RedisSortedSetCommands<String, String> sortedSetCommands = commandsProvider.getSortedSetCommands();

        final Range<Number> range = Range.create(Long.valueOf(0), Long.valueOf(thresholdTimeStamp));
        final Limit limit = Limit.create(0, LIMIT);

        List<Session> removedSessions = null;

        Map<String, Counter> sessionsByBrand = new HashMap<>();
        List<String> expiredSessionIds = sortedSetCommands.zrangebyscore(setKey, range, limit);
        while (expiredSessionIds != null && !expiredSessionIds.isEmpty()) {
            // Remove expired sessions
            List<Session> removedOnes = redisSessiondService.removeSessions(expiredSessionIds, false, false, RemovalReason.EXPIRED);
            LOG.debug(DROP_MDC_MARKER, "Expired session(s) {} from Redis storage", expiredSessionIds);

            // Output number of deleted sessions by brand
            processDeletedSessions(removedOnes, sessionsByBrand);

            // Remember removed sessions
            if (returnExpiredSessions) {
                if (removedSessions == null) {
                    removedSessions = new ArrayList<>(removedOnes);
                } else {
                    removedSessions.addAll(removedOnes);
                }
            }

            // Delete current chunk from Redis sorted set and query again
            int numberOfExpiredSessions = expiredSessionIds.size();
            sortedSetCommands.zrem(setKey, expiredSessionIds.toArray(new String[numberOfExpiredSessions]));

            // Get next chunk (if any)
            expiredSessionIds = numberOfExpiredSessions >= LIMIT ? sortedSetCommands.zrangebyscore(setKey, range, limit) : null;
        }
        logDeletedSessionsByBrand(sessionsByBrand);
        sessionsByBrand = null; // NOSONARLINT: No more needed; help GC

        return removedSessions == null ? Collections.emptyList() : removedSessions;
    }

    /** The fall-back brand name if session holds no brand parameter value */
    private static final String BRAND_NONE = "none";

    /**
     * Processes deleted sessions.
     *
     * @param removedOnes The sessions previously dropped from Redis storage
     * @param sessionsByBrand The helper map to group sessions by brand
     */
    private static void processDeletedSessions(List<Session> removedOnes, Map<String, Counter> sessionsByBrand) {
        // Log & count by brand
        for (Session removedSession : removedOnes) {
            LOG.info(DROP_MDC_MARKER, "Session timed out. ID: {}", removedSession.getSessionID());
            sessionsByBrand.computeIfAbsent(removedSession.getParameterOrElse(Session.PARAM_BRAND, BRAND_NONE), RedisSessionConstants.getNewCounterFunction()).increment();
        }
    }

    /**
     * Logs number of deleted sessions per brand association.
     *
     * @param sessionsByBrand The grouped sessions by brand
     */
    private static void logDeletedSessionsByBrand(Map<String, Counter> sessionsByBrand) {
        // Output by brand association
        Counter brandlessCounter = sessionsByBrand.remove(BRAND_NONE);
        if (brandlessCounter != null) {
            long numberOfDeletedSessions = brandlessCounter.getCount();
            if (numberOfDeletedSessions == 1) {
                LOG.info(DROP_MDC_MARKER, "Expired 1 session with no brand association");
            } else {
                LOG.info(DROP_MDC_MARKER, "Expired {} sessions with no brand association", Long.valueOf(numberOfDeletedSessions));
            }
        }
        for (Map.Entry<String, Counter> e : sessionsByBrand.entrySet()) {
            long numberOfDeletedSessions = e.getValue().getCount();
            if (numberOfDeletedSessions == 1) {
                LOG.info(DROP_MDC_MARKER, "Expired 1 session for brand {}", e.getKey());
            } else {
                LOG.info(DROP_MDC_MARKER, "Expired {} sessions for brand {}", Long.valueOf(numberOfDeletedSessions), e.getKey());
            }
        }
    }

    private static final Set<String> HASH_FIELDS_TO_IGNORE = Set.of(RedisSessionConstants.COUNTER_SESSION_TOTAL, RedisSessionConstants.COUNTER_SESSION_ACTIVE, RedisSessionConstants.COUNTER_SESSION_LONG, RedisSessionConstants.COUNTER_SESSION_SHORT);

    private static final long MILLIS_12_MINUTES = 720_000L;

    private void updateCounts(RedisCommandsProvider commandsProvider) {
        RedisSortedSetCommands<String, String> sortedSetCommands = commandsProvider.getSortedSetCommands();
        RedisHashCommands<String, String> hashCommands = commandsProvider.getHashCommands();

        /*-
         * According to previous implementation considering first two short-term session containers;
         * see com.openexchange.sessiond.impl.SessiondConfigImpl.SHORT_CONTAINER_LIFE_TIME (6 minutes)
         */
        long ageMillis = MILLIS_12_MINUTES;
        long now = System.currentTimeMillis();
        long threshold = now - ageMillis;
        Range<Number> activeRange = Range.create(Long.valueOf(threshold), Long.valueOf(now + RedisSessionConstants.MILLIS_DAY));

        long countLong;
        {
            String setKey = RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_LONG_LIFETIME;
            countLong = sortedSetCommands.zcard(setKey).longValue();
            hashCommands.hset(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, RedisSessionConstants.COUNTER_SESSION_LONG, Long.toString(countLong));
        }

        long countShort;
        {
            String setKey = RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_SHORT_LIFETIME;
            countShort = sortedSetCommands.zcard(setKey).longValue();
            hashCommands.hset(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, RedisSessionConstants.COUNTER_SESSION_SHORT, Long.toString(countShort));
        }

        {
            hashCommands.hset(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, RedisSessionConstants.COUNTER_SESSION_TOTAL, Long.toString(countLong + countShort));

            long activeLong = countLong == 0 ? 0 : determineCounterForNumberOfActiveSessions(RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_LONG_LIFETIME, activeRange, sortedSetCommands);
            long activeShort = countShort == 0 ? 0 : determineCounterForNumberOfActiveSessions(RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_SHORT_LIFETIME, activeRange, sortedSetCommands);
            hashCommands.hset(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, RedisSessionConstants.COUNTER_SESSION_ACTIVE, Long.toString(activeLong + activeShort));
        }

        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
        Set<String> handledBrandIds = new HashSet<>();
        ScanArgs scanArgs = ScanArgs.Builder.matches(RedisSessiondService.getAllBrandSetKeyPattern()).limit(1000);
        KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
        while (cursor != null) {
            // Iterate current keys...
            for (String setKey : cursor.getKeys()) {
                String brandId = setKey.substring(setKey.lastIndexOf(':') + 1);
                long count = sortedSetCommands.zcard(setKey).longValue();
                hashCommands.hset(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, brandId + RedisSessionConstants.COUNTER_SESSION_TOTAL_APPENDIX, Long.toString(count));

                long active = count == 0 ? 0 : determineCounterForNumberOfActiveSessions(setKey, activeRange, sortedSetCommands);
                hashCommands.hset(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, brandId + RedisSessionConstants.COUNTER_SESSION_ACTIVE_APPENDIX, Long.toString(active));
                handledBrandIds.add(brandId);
            }

            // Move cursor forward
            cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
        }

        for (String field : hashCommands.hkeys(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER)) {
            if (!HASH_FIELDS_TO_IGNORE.contains(field)) {
                String brandId = field.substring(0, field.lastIndexOf('.'));
                if (handledBrandIds.add(brandId)) {
                    // Not yet handled
                    String setKey = RedisSessiondService.getSetKeyForBrand(brandId);
                    long count = sortedSetCommands.zcard(setKey).longValue();
                    hashCommands.hset(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, brandId + RedisSessionConstants.COUNTER_SESSION_TOTAL_APPENDIX, Long.toString(count));

                    long active = count == 0 ? 0 : determineCounterForNumberOfActiveSessions(setKey, activeRange, sortedSetCommands);
                    hashCommands.hset(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, brandId + RedisSessionConstants.COUNTER_SESSION_ACTIVE_APPENDIX, Long.toString(active));
                    handledBrandIds.add(brandId);
                }
            }
        }
    }

    protected long determineCounterForNumberOfActiveSessions(String setKey, Range<Number> range, RedisSortedSetCommands<String, String> sortedSetCommands) {
        long offset = 0;
        Limit limit = Limit.create(offset, RedisSessionConstants.LIMIT_1000);

        long count = 0;
        List<String> active = sortedSetCommands.zrangebyscore(setKey, range, limit);
        for (int size; active != null && (size = active.size()) > 0;) {
            count += size;

            // Get next chunk (if any)
            if (size >= RedisSessionConstants.LIMIT_1000) {
                offset += RedisSessionConstants.LIMIT_1000;
                limit = Limit.create(offset, RedisSessionConstants.LIMIT_1000);
                active = sortedSetCommands.zrangebyscore(setKey, range, limit);
            } else {
                active = null;
            }
        }
        return count;
    }

}
