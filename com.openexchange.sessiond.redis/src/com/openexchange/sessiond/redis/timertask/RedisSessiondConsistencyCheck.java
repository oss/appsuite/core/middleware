/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.sessiond.redis.timertask;

import static com.openexchange.log.Constants.DROP_MDC_MARKER;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import com.google.common.collect.Lists;
import com.openexchange.java.Functions;
import com.openexchange.java.Sets;
import com.openexchange.log.DurationOutputter;
import com.openexchange.redis.RedisCommandsProvider;
import com.openexchange.session.Session;
import com.openexchange.sessiond.redis.RedisLock;
import com.openexchange.sessiond.redis.RedisSessionConstants;
import com.openexchange.sessiond.redis.RedisSessiondService;
import com.openexchange.sessiond.redis.util.BrandNames;
import io.lettuce.core.KeyScanCursor;
import io.lettuce.core.KeyValue;
import io.lettuce.core.ScanArgs;
import io.lettuce.core.ScoredValue;
import io.lettuce.core.ScoredValueScanCursor;
import io.lettuce.core.api.sync.RedisKeyCommands;
import io.lettuce.core.api.sync.RedisSetCommands;
import io.lettuce.core.api.sync.RedisSortedSetCommands;
import io.lettuce.core.api.sync.RedisStringCommands;

/**
 * {@link RedisSessiondConsistencyCheck} - Performs the consistency check for data structures held in Redis for session data.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 *
 */
public class RedisSessiondConsistencyCheck extends AbstractRedisTimerTask {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(RedisSessiondConsistencyCheck.class);

    /** The limit for MGET command */
    private static final int LIMIT_MGET = 100;

    /** The limit for SCAN command */
    private static final int LIMIT_SCAN = 1000;

    /**
     * Initializes a new {@link RedisSessiondConsistencyCheck}.
     *
     * @param sessiondService The Redis SessiondD service
     */
    public RedisSessiondConsistencyCheck(RedisSessiondService sessiondService) {
        super(sessiondService);
    }

    /**
     * Checks consistency of session-related Redis keys and collections.
     * <ul>
     * <li><code>"ox-session"</code>; e.g. <code>"ox-session:abcde123de"</code></li>
     * <li><code>"ox-session-altid"</code>; e.g. <code>"ox-session-altid:abcde123de"</code></li>
     * <li><code>"ox-session-authid"</code>; e.g. <code>"ox-session-authid:abcde123de"</code></li>
     * <li><code>"ox-sessionids"</code>; e.g. <code>"ox-sessionids:1337:3"</code></li>
     * </ul>
     *
     * @param withSessionConsistency <code>true</code> to check that each session has its corresponding entry in Redis set and Redis mapping keys; otherwise <code>false</code>
     * @param lockExpirationMillis The time in milliseconds after which the consistency lock is considered as expired
     * @param lockUpdateFrequencyMillis The frequency in milliseconds for updating the named lock
     */
    public void consistencyCheck(boolean withSessionConsistency, long lockExpirationMillis, long lockUpdateFrequencyMillis) {
        if (lockExpirationMillis <= 0) {
            throw new IllegalArgumentException("Lock expiration milliseconds must not be less than or equal to 0 (zero)");
        }
        try {
            redisSessiondService.getConnector().executeNoTimeoutOperation(commandsProvider -> doConsistencyCheck(withSessionConsistency, lockExpirationMillis, lockUpdateFrequencyMillis, commandsProvider));
        } catch (Exception e) {
            LOG.warn(DROP_MDC_MARKER, "Failed consistency check for Redis session storage", e);
        }
    }

    private Void doConsistencyCheck(boolean withSessionConsistency, long lockExpirationMillis, long lockUpdateFrequencyMillis, RedisCommandsProvider commandsProvider) {
        LOG.info(DROP_MDC_MARKER, "Starting consistency check for Redis session storage...");

        // Try to acquire lock
        boolean release = true;
        Optional<RedisLock> optLock = RedisLock.lockFor(RedisSessionConstants.REDIS_SESSION_CONSISTENCY_LOCK, lockExpirationMillis, lockUpdateFrequencyMillis, commandsProvider, redisSessiondService);
        if (optLock.isPresent()) {
            try {
                // Lock acquired
                long overallStart = System.nanoTime();

                long start = overallStart;
                checkSortedSetSessionIdsConsistency(commandsProvider, RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_LONG_LIFETIME);
                LOG.debug(DROP_MDC_MARKER, "Finished consistency check for long lifetime sorted-set after appr. {}", DurationOutputter.startNanos(start));

                start = System.nanoTime();
                checkSortedSetSessionIdsConsistency(commandsProvider, RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_SHORT_LIFETIME);
                LOG.debug(DROP_MDC_MARKER, "Finished consistency check for short lifetime sorted-set after appr. {}", DurationOutputter.startNanos(start));

                start = System.nanoTime();
                checkSessionIdsBrandSetConsistency(commandsProvider);
                LOG.debug(DROP_MDC_MARKER, "Finished consistency check for session-to-brand set after appr. {}", DurationOutputter.startNanos(start));

                start = System.nanoTime();
                checkSessionIdsSetConsistency(commandsProvider);
                LOG.debug(DROP_MDC_MARKER, "Finished consistency check for context/user-to-session set after appr. {}", DurationOutputter.startNanos(start));

                start = System.nanoTime();
                checkAlternativeIdsConsistency(commandsProvider);
                LOG.debug(DROP_MDC_MARKER, "Finished consistency check for session-to-alternativeId set after appr. {}", DurationOutputter.startNanos(start));

                start = System.nanoTime();
                checkAuthIdsConsistency(commandsProvider);
                LOG.debug(DROP_MDC_MARKER, "Finished consistency check for session-to-authId set after appr. {}", DurationOutputter.startNanos(start));

                if (withSessionConsistency) {
                    checkSessionConsistency(commandsProvider);
                    LOG.debug(DROP_MDC_MARKER, "Finished consistency check for main session collection after appr. {}", DurationOutputter.startNanos(start));
                }

                LOG.info(DROP_MDC_MARKER, "Finished (presumably long-running) background consistency check for Redis session storage after appr. {}", DurationOutputter.startNanos(overallStart));

                // Let lock expire for next run since this run was successful
                release = false;
                return null;
            } finally {
                optLock.get().unlock(release, commandsProvider);
            }
        }

        LOG.info(DROP_MDC_MARKER, "Aborted consistency check for Redis session storage. Another node is currently processing or consistency check already performed recently");
        return null;
    }

    private void checkSessionConsistency(RedisCommandsProvider commandsProvider) {
        // Ensure each session has its entry in session-ids-set and alternativeId-to-sessionId association
        RedisStringCommands<String, Session> sessionCommands = redisSessiondService.getSessionCommandsWithoutObfuscator(commandsProvider);
        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
        RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();
        RedisSortedSetCommands<String, String> sortedSetCommands = commandsProvider.getSortedSetCommands();

        String pattern = RedisSessiondService.getAllSessionsPattern();
        ScanArgs scanArgs = ScanArgs.Builder.matches(pattern).limit(LIMIT_SCAN);

        KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
        while (cursor != null) {
            // Obtain current keys...
            List<String> keys = cursor.getKeys();
            if (keys.isEmpty() == false) {
                for (String sessionKey : keys) {
                    // Get session for Redis key
                    Session session = sessionCommands.get(sessionKey);
                    if (session != null) {
                        long now = System.currentTimeMillis();
                        String brandId = BrandNames.getBrandIdentifierFrom(session);
                        if (brandId != null) {
                            String setKey = RedisSessiondService.getSetKeyForBrand(brandId);
                            if (sortedSetCommands.zrank(setKey, session.getSessionID()) == null) {
                                sortedSetCommands.zadd(setKey, now, session.getSessionID());
                            }
                        }

                        String setKey = RedisSessiondService.getSetKeyForUser(session.getUserId(), session.getContextId());
                        if (setCommands.sismember(setKey, session.getSessionID()).booleanValue() == false) {
                            setCommands.sadd(setKey, session.getSessionID());
                        }

                        Object alternativeId = session.getParameter(Session.PARAM_ALTERNATIVE_ID);
                        if (alternativeId != null) {
                            String sessionAlternativeKey = RedisSessiondService.getSessionAlternativeKey(alternativeId.toString());
                            if (keyCommands.exists(sessionAlternativeKey).longValue() <= 0) {
                                commandsProvider.getStringCommands().set(sessionAlternativeKey, session.getSessionID());
                            }
                        }

                        String authId = session.getAuthId();
                        if (authId != null) {
                            String authIdKey = RedisSessiondService.getSessionAuthIdKey(authId);
                            if (keyCommands.exists(authIdKey).longValue() <= 0) {
                                commandsProvider.getStringCommands().set(authIdKey, session.getSessionID());
                            }
                        }

                        String sortedSetKey = RedisSessiondService.getSortSetKeyForSession(session);
                        if (sortedSetCommands.zrank(sortedSetKey, session.getSessionID()) == null) {
                            sortedSetCommands.zadd(sortedSetKey, now, session.getSessionID());
                        }
                    }
                }
            }

            // Move cursor forward
            cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
        }
    }

    private static void checkAuthIdsConsistency(RedisCommandsProvider commandsProvider) {
        // Query all authentication identifiers
        String pattern = new StringBuilder(RedisSessionConstants.REDIS_SESSION_AUTH_ID).append(RedisSessionConstants.DELIMITER).append('*').toString();
        checkIdToSessionIdMappingConsistency(pattern, commandsProvider);
    }

    private static void checkAlternativeIdsConsistency(RedisCommandsProvider commandsProvider) {
        // Query all alternative identifiers
        String pattern = new StringBuilder(RedisSessionConstants.REDIS_SESSION_ALTERNATIVE_ID).append(RedisSessionConstants.DELIMITER).append('*').toString();
        checkIdToSessionIdMappingConsistency(pattern, commandsProvider);
    }

    private static void checkIdToSessionIdMappingConsistency(String pattern, RedisCommandsProvider commandsProvider) {
        // Query all identifiers by pattern
        ScanArgs scanArgs = ScanArgs.Builder.matches(pattern).limit(LIMIT_SCAN);
        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
        RedisStringCommands<String, InputStream> rawStringCommands = commandsProvider.getRawStringCommands();
        RedisStringCommands<String, String> stringCommands = commandsProvider.getStringCommands();

        List<String> keysToDelete = null;
        KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
        while (cursor != null) {
            // Obtain current keys...
            List<String> keys = cursor.getKeys();

            // ... and check them
            int numOfKeys = keys.size();
            if (numOfKeys > 0) {
                Map<String, String> sessionKey2key = HashMap.newHashMap(numOfKeys);
                for (List<String> partitionKeys : Lists.partition(keys, LIMIT_MGET)) {
                    // Get values (identifiers) for current keys
                    for (KeyValue<String, String> idKeyValue : stringCommands.mget(partitionKeys.toArray(new String[partitionKeys.size()]))) {
                        // Check key's session identifier value
                        String sessionId = idKeyValue.getValueOrElse(null);
                        if (sessionId == null) {
                            // No such session identifier value. Remember to remove from mapping
                            if (keysToDelete == null) {
                                keysToDelete = new ArrayList<>();
                            }
                            keysToDelete.add(idKeyValue.getKey());
                        } else {
                            // Put into session key to key map
                            sessionKey2key.put(RedisSessiondService.getSessionKey(sessionId), idKeyValue.getKey());
                        }
                    }
                }

                // Use MGET to check session existence for multiple session keys
                if (!sessionKey2key.isEmpty()) {
                    for (Set<String> sessionKeysPartition : Sets.partition(sessionKey2key.keySet(), LIMIT_MGET)) {
                        for (KeyValue<String, InputStream> sessionKeyValue : rawStringCommands.mget(sessionKeysPartition.toArray(Functions.getNewStringArrayIntFunction()))) {
                            if (sessionKeyValue.isEmpty()) {
                                // Remember to remove from mapping
                                String key = sessionKey2key.get(sessionKeyValue.getKey());
                                if (key != null) {
                                    if (keysToDelete == null) {
                                        keysToDelete = new ArrayList<>();
                                    }
                                    keysToDelete.add(key);
                                }
                            }
                        }
                    }
                }
            }

            // Move cursor forward
            cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
        }

        if (keysToDelete != null) {
            keyCommands.del(keysToDelete.toArray(new String[keysToDelete.size()]));
        }
    }

    private static void checkSessionIdsSetConsistency(RedisCommandsProvider commandsProvider) {
        // Query all sets.
        ScanArgs scanArgs = ScanArgs.Builder.matches(RedisSessiondService.getAllSetKeyPattern()).limit(LIMIT_SCAN);
        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
        RedisStringCommands<String, InputStream> rawStringCommands = commandsProvider.getRawStringCommands();
        RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();

        Map<String, List<String>> sessionIdsToDelete = null;
        KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
        while (cursor != null) {
            // Obtain current keys...
            List<String> setKeys = cursor.getKeys();

            // ... and add them
            if (!setKeys.isEmpty()) {
                // Check validity for each key
                for (String setKey : setKeys) {
                    Set<String> sessionIds = setCommands.smembers(setKey);
                    if (sessionIds != null && !sessionIds.isEmpty()) {
                        // Use MGET to check existence for multiple keys
                        for (KeyValue<String, InputStream> sessionKeyValue : rawStringCommands.mget(RedisSessiondService.getSessionKeysFor(sessionIds))) {
                            if (sessionKeyValue.isEmpty()) {
                                // No such session. Remember to remove from set since non-existent
                                if (sessionIdsToDelete == null) {
                                    sessionIdsToDelete = new HashMap<>();
                                }
                                String sessionId = RedisSessiondService.getSessionIdFrom(sessionKeyValue.getKey());
                                sessionIdsToDelete.computeIfAbsent(setKey, Functions.getNewLinkedListFuntion()).add(sessionId);
                            }
                        }
                    }
                }
            }

            // Move cursor forward
            cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
        }

        if (sessionIdsToDelete != null) {
            for (Map.Entry<String, List<String>> entry : sessionIdsToDelete.entrySet()) {
                List<String> sremList = entry.getValue();
                setCommands.srem(entry.getKey(), sremList.toArray(new String[sremList.size()]));
            }
        }
    }

    private static void checkSessionIdsBrandSetConsistency(RedisCommandsProvider commandsProvider) {
        // Query all sets.
        ScanArgs scanArgs = ScanArgs.Builder.matches(RedisSessiondService.getAllBrandSetKeyPattern()).limit(LIMIT_SCAN);
        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();

        List<String> setIds = null;

        KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
        while (cursor != null) {
            // Obtain current keys...
            List<String> keys = cursor.getKeys();
            if (!keys.isEmpty()) {
                if (setIds == null) {
                    setIds = new ArrayList<>(keys);
                } else {
                    setIds.addAll(keys);
                }
            }

            // Move cursor forward
            cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
        }

        // Check validity for each brand's sorted set
        if (setIds != null) {
            for (String setId : setIds) {
                checkSortedSetSessionIdsConsistency(commandsProvider, setId);
            }
        }
    }

    private static void checkSortedSetSessionIdsConsistency(RedisCommandsProvider commandsProvider, String setKey) {
        ScanArgs scanArgs = ScanArgs.Builder.limit(LIMIT_SCAN);
        RedisStringCommands<String, InputStream> rawStringCommands = commandsProvider.getRawStringCommands();
        RedisSortedSetCommands<String, String> sortedSetCommands = commandsProvider.getSortedSetCommands();

        List<String> sessionIdsToDelete = null;
        boolean emptyStringAdded = false;
        ScoredValueScanCursor<String> cursor = sortedSetCommands.zscan(setKey, scanArgs);
        while (cursor != null) {
            List<ScoredValue<String>> sessionIdValues = cursor.getValues();

            List<String> sessionKeysToCheck = new ArrayList<>(sessionIdValues.size());
            for (ScoredValue<String> sessionIdValue : sessionIdValues) {
                // Check session existence
                String sessionId = sessionIdValue.getValueOrElse(null);
                if (sessionId != null) {
                    sessionKeysToCheck.add(RedisSessiondService.getSessionKey(sessionId));
                } else {
                    // An empty session identifier is propagated as null, too.
                    // Thus add the empty string to sessionIdsToDelete collection
                    if (!emptyStringAdded) {
                        if (sessionIdsToDelete == null) {
                            sessionIdsToDelete = new ArrayList<>();
                        }
                        sessionIdsToDelete.add("");
                        emptyStringAdded = true;
                    }
                }
            }

            // Use MGET to check existence for multiple session keys
            if (!sessionKeysToCheck.isEmpty()) {
                for (List<String> sessionKeysPartition : Lists.partition(sessionKeysToCheck, LIMIT_MGET)) {
                    for (KeyValue<String, InputStream> sessionKeyValue : rawStringCommands.mget(sessionKeysPartition.toArray(Functions.getNewStringArrayIntFunction()))) {
                        if (sessionKeyValue.isEmpty()) {
                            // Remember to remove from sorted set
                            if (sessionIdsToDelete == null) {
                                sessionIdsToDelete = new ArrayList<>();
                            }
                            sessionIdsToDelete.add(RedisSessiondService.getSessionIdFrom(sessionKeyValue.getKey()));
                        }
                    }
                }
            }

            // Move cursor forward
            cursor = cursor.isFinished() ? null : sortedSetCommands.zscan(setKey, cursor, scanArgs);
        }

        if (sessionIdsToDelete != null) {
            sortedSetCommands.zrem(setKey, sessionIdsToDelete.toArray(new String[sessionIdsToDelete.size()]));
        }
    }

}
