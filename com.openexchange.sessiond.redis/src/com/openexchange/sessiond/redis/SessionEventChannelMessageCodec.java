/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.sessiond.redis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONServices;
import com.openexchange.pubsub.ChannelMessageCodec;
import com.openexchange.session.UserAndContext;

/**
 * {@link SessionEventChannelMessageCodec} - The channel message codec for session events.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 *
 */
public class SessionEventChannelMessageCodec implements ChannelMessageCodec<SessionEvent> {

    private static final String OPERATION = "o";
    private static final String SESSION_IDS = "s";
    private static final String CONTEXT_IDS = "c";
    private static final String USER_IDS = "u";

    /**
     * Initializes a new {@link SessionEventChannelMessageCodec}.
     */
    public SessionEventChannelMessageCodec() {
        super();
    }

    @Override
    public String serialize(SessionEvent message) throws Exception {
        // Put operation to JSON representation
        JSONObject jMessage = new JSONObject(2);
        jMessage.put(OPERATION, message.getOperation().getId());

        // Put session identifiers to JSON representation
        Collection<String> sessionIds = message.getSessionIds();
        if (sessionIds != null && !sessionIds.isEmpty()) {
            JSONArray jKeys = new JSONArray(sessionIds.size());
            for (String sessionId : sessionIds) {
                jKeys.put(sessionId);
            }
            jMessage.put(SESSION_IDS, jKeys);
        }

        Collection<Integer> contextIds = message.getContextIds();
        if (contextIds != null && !contextIds.isEmpty()) {
            JSONArray jKeys = new JSONArray(contextIds.size());
            for (Integer contextId : contextIds) {
                jKeys.put(contextId.intValue());
            }
            jMessage.put(CONTEXT_IDS, jKeys);
        }

        Collection<UserAndContext> userIds = message.getUserIds();
        if (userIds != null && !userIds.isEmpty()) {
            JSONArray jKeys = new JSONArray(userIds.size());
            StringBuilder sb = new StringBuilder();
            for (UserAndContext userId : userIds) {
                sb.setLength(0);
                jKeys.put(sb.append(userId.getUserId()).append('@').append(userId.getContextId()).toString());
            }
            jMessage.put(USER_IDS, jKeys);
        }

        return jMessage.toString();
    }

    @Override
    public SessionEvent deserialize(String data) throws Exception {
        JSONObject jMessage = JSONServices.parseObject(data);

        // Get operation from JSON representation
        SessionOperation operation = SessionOperation.operationFor(jMessage.getString(OPERATION));

        switch (operation) {
            case INVALIDATE:
                // Get session identifiers from JSON representation
                JSONArray jSessionIds = jMessage.getJSONArray(SESSION_IDS);
                int l1 = jSessionIds.length();
                List<String> sessionIds = new ArrayList<>(l1);
                for (int i = 0; i < l1; i++) {
                    sessionIds.add(jSessionIds.getString(i));
                }
                return SessionEvent.newInvalidateSessionEvent(sessionIds);
            case LAST_GONE_CONTEXT:
                // Get context identifiers from JSON representation
                JSONArray jContextIds = jMessage.getJSONArray(CONTEXT_IDS);
                int l2 = jContextIds.length();
                List<Integer> contextIds = new ArrayList<>(l2);
                for (int i = 0; i < l2; i++) {
                    contextIds.add(Integer.valueOf(jContextIds.getInt(i)));
                }
                return SessionEvent.newLastGoneContextSessionEvent(contextIds);
            case LAST_GONE_USER:
                // Get user identifiers from JSON representation
                JSONArray jUserIds = jMessage.getJSONArray(USER_IDS);
                int l3 = jUserIds.length();
                List<UserAndContext> userIds = new ArrayList<>(l3);
                for (int i = 0; i < l3; i++) {
                    userIds.add(parseUserAndContextFrom(jUserIds.getString(i)));
                }
                return SessionEvent.newLastGoneUserSessionEvent(userIds);
        }

        // huh...?
        return SessionEvent.newInvalidateSessionEvent(new ArrayList<>(0));
    }

    private static UserAndContext parseUserAndContextFrom(String s) {
        int pos = s.indexOf('@');
        return UserAndContext.newInstance(Integer.parseInt(s.substring(0, pos)), Integer.parseInt(s.substring(pos + 1)));
    }

}
