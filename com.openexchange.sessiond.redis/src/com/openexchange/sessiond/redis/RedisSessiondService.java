/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.sessiond.redis;

import static com.openexchange.java.Autoboxing.B;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import static com.openexchange.sessiond.redis.SessionId.newAlternativeSessionId;
import static com.openexchange.sessiond.redis.SessionId.newSessionId;
import static com.openexchange.sessiond.redis.cache.InstanceLoader.loaderFor;
import static com.openexchange.sessiond.redis.commands.SessionRedisStringCommands.DO_NOTHING_VERSION_MISMATCH_HANDLER;
import static com.openexchange.sessiond.redis.util.BrandNames.getBrandIdentifierFor;
import static com.openexchange.sessiond.redis.util.BrandNames.getBrandIdentifierFrom;
import java.io.InputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.LockSupport;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.json.JSONException;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import com.google.common.collect.Maps;
import com.openexchange.annotation.Nullable;
import com.openexchange.authentication.SessionEnhancement;
import com.openexchange.cluster.serialization.session.ClusterSession;
import com.openexchange.cluster.serialization.session.SessionCodec;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXRuntimeException;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.ExceptionCatchingRunnable;
import com.openexchange.java.Functions;
import com.openexchange.java.ImmutableReference;
import com.openexchange.java.Predicates;
import com.openexchange.java.Reference;
import com.openexchange.java.Streams;
import com.openexchange.java.Strings;
import com.openexchange.java.util.UUIDs;
import com.openexchange.lock.AccessControl;
import com.openexchange.lock.AccessControls;
import com.openexchange.lock.LockService;
import com.openexchange.log.HumanTimeOutputter;
import com.openexchange.log.LogUtility;
import com.openexchange.osgi.ServiceListing;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.ChannelKey;
import com.openexchange.pubsub.ChannelListener;
import com.openexchange.pubsub.DefaultChannelKey;
import com.openexchange.pubsub.Message;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.pubsub.core.CoreChannelApplicationName;
import com.openexchange.pubsub.core.CoreChannelName;
import com.openexchange.redis.DefaultRedisOperationKey;
import com.openexchange.redis.RedisCommand;
import com.openexchange.redis.RedisCommandsProvider;
import com.openexchange.redis.RedisConnector;
import com.openexchange.redis.RedisConnectorProvider;
import com.openexchange.redis.RedisConnectorService;
import com.openexchange.redis.RedisExceptionCode;
import com.openexchange.redis.RedisOperation;
import com.openexchange.redis.RedisOperationKey;
import com.openexchange.redis.RedisVoidOperation;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Origin;
import com.openexchange.session.RemovalReason;
import com.openexchange.session.Session;
import com.openexchange.session.SessionAttributes;
import com.openexchange.session.SessionDescription;
import com.openexchange.session.SessionSerializationInterceptor;
import com.openexchange.session.UserAndContext;
import com.openexchange.sessiond.AddSessionParameter;
import com.openexchange.sessiond.SessionExceptionCodes;
import com.openexchange.sessiond.SessionFilter;
import com.openexchange.sessiond.SessionMatcher;
import com.openexchange.sessiond.SessiondEventConstants;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.sessiond.redis.cache.InstanceLoader;
import com.openexchange.sessiond.redis.cache.Loader;
import com.openexchange.sessiond.redis.cache.LocalSessionCache;
import com.openexchange.sessiond.redis.commands.SessionRedisStringCommands;
import com.openexchange.sessiond.redis.commands.SessionRedisStringCommands.VersionMismatchHandler;
import com.openexchange.sessiond.redis.config.RedisSessiondConfigProperty;
import com.openexchange.sessiond.redis.metrics.SessionMetricHandler;
import com.openexchange.sessiond.redis.osgi.Services;
import com.openexchange.sessiond.redis.ratelimit.BucketRegistry;
import com.openexchange.sessiond.redis.timertask.RedisSessiondConsistencyCheck;
import com.openexchange.sessiond.redis.timertask.RedisSessiondExpirerAndCountersUpdater;
import com.openexchange.sessiond.redis.token.TokenSessionContainer;
import com.openexchange.sessiond.redis.token.TokenSessionControl;
import com.openexchange.sessiond.redis.usertype.UserTypeSessiondConfigInterface;
import com.openexchange.sessiond.redis.util.Counter;
import com.openexchange.sessiond.redis.util.RemovalCollection;
import com.openexchange.threadpool.ThreadPools;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;
import com.openexchange.user.UserService;
import io.github.bucket4j.Bucket;
import io.lettuce.core.KeyScanCursor;
import io.lettuce.core.KeyValue;
import io.lettuce.core.Limit;
import io.lettuce.core.Range;
import io.lettuce.core.RedisException;
import io.lettuce.core.ScanArgs;
import io.lettuce.core.SetArgs;
import io.lettuce.core.api.sync.RedisHashCommands;
import io.lettuce.core.api.sync.RedisKeyCommands;
import io.lettuce.core.api.sync.RedisSetCommands;
import io.lettuce.core.api.sync.RedisSortedSetCommands;
import io.lettuce.core.api.sync.RedisStringCommands;

/**
 * {@link RedisSessiondService} - The abstract Redis sessiond service service.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RedisSessiondService implements SessiondService, ChannelListener<SessionEvent> {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(RedisSessiondService.class);

    private static final List<String> SORTED_SETS = List.of(RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_LONG_LIFETIME, RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_SHORT_LIFETIME);

    /** The channel key for session events */
    private static final ChannelKey SESSION_EVENT_CHANNEL_KEY = DefaultChannelKey.builder().withChannelApplicationName(CoreChannelApplicationName.CORE).withChannelName(CoreChannelName.SESSION_EVENTS).build();

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static final long LOCK_UPDATE_FREQUENCY_MILLIS = 30_000;

    /** The function to yield the appropriate Redis key for a given session identifier */
    private static final Function<String, String> FUNCTION_SESSION_KEY = RedisSessiondService::getSessionKey;

    protected final ServiceLookup services;
    protected final RedisConnector connector;

    private final VersionMismatchHandler versionMismatchHandler;
    private final AtomicReference<RedisSessiondState> stateReference;
    private final ServiceListing<SessionSerializationInterceptor> serializationInterceptors;
    private final AtomicReference<ScheduledTimerTask> consistencyCheckTimerTaskReference;
    private final AtomicReference<ScheduledTimerTask> expirerAndCounterUpdateTimerTaskReference;
    private final Channel<SessionEvent> channel;

    /**
     * Initializes a new {@link RedisSessiondService}.
     *
     * @param initialState The initial state to take over
     * @param serializationInterceptors The serialization interceptors
     * @param services The service look-up
     * @throws OXException If initialization fails
     */
    public RedisSessiondService(RedisSessiondState initialState, ServiceListing<SessionSerializationInterceptor> serializationInterceptors, ServiceLookup services) throws OXException {
        super();
        this.connector = services.getServiceSafe(RedisConnectorService.class).getConnectorProvider().getConnector();
        this.serializationInterceptors = serializationInterceptors;
        this.services = services;
        this.stateReference = new AtomicReference<>(initialState);

        TimerService timerService = services.getServiceSafe(TimerService.class);
        {
            boolean lockExists = connector.executeOperation(commandsProvider -> B(commandsProvider.getKeyCommands().exists(RedisSessionConstants.REDIS_SESSION_CONSISTENCY_LOCK).longValue() > 0)).booleanValue();
            long intervalMillis = Duration.ofMinutes(initialState.getConsistencyCheckIntervalMinutes()).toMillis();
            long initialDelayMillis = ((long) (Math.random() * (lockExists ? intervalMillis : 10000)));
            consistencyCheckTimerTaskReference = new AtomicReference<>(timerService.scheduleWithFixedDelay(() -> new RedisSessiondConsistencyCheck(this).consistencyCheck(true, intervalMillis, LOCK_UPDATE_FREQUENCY_MILLIS), initialDelayMillis, intervalMillis, TimeUnit.MILLISECONDS));
        }
        {
            boolean lockExists = connector.executeOperation(commandsProvider -> B(commandsProvider.getKeyCommands().exists(RedisSessionConstants.REDIS_SESSION_EXPIRE_AND_COUNTERS_LOCK).longValue() > 0)).booleanValue();
            long intervalMillis = Duration.ofMinutes(initialState.getExpirerAndCountersUpdateIntervalMinutes()).toMillis();
            long initialDelayMillis =  ((long) (Math.random() * (lockExists ? intervalMillis : 10000)));
            expirerAndCounterUpdateTimerTaskReference = new AtomicReference<>(timerService.scheduleWithFixedDelay(() -> new RedisSessiondExpirerAndCountersUpdater(this).expireSessionsAndUpdateCounters(intervalMillis, LOCK_UPDATE_FREQUENCY_MILLIS), initialDelayMillis, intervalMillis, TimeUnit.MILLISECONDS));
        }

        PubSubService pubSubService = services.getServiceSafe(PubSubService.class);
        channel = pubSubService.getChannel(SESSION_EVENT_CHANNEL_KEY, new SessionEventChannelMessageCodec());
        channel.subscribe(this);

        if (checkVersion()) {
            for (String brandId : getBrandIdsForCounters()) {
                SessionMetricHandler.registerBrandMetricIfAbsent(brandId);
            }
        }

        versionMismatchHandler = (key, e, command) -> {
            String sessionId = getSessionIdFrom(key);
            if (LOG.isDebugEnabled()) {
                LOG.info("Encountered version mismatch for session {}", sessionId, e);
            } else {
                LOG.info("Encountered version mismatch for session {}. Expected version {}, but got version {} in stored session data", sessionId, Integer.valueOf(e.getExpectedVersion()), Integer.valueOf(e.getVersion()));
            }

            if (RedisCommand.GETSET == command) {
                // Session data has been replaced anyway
                return true;
            }

            try {
                removeSession(sessionId, RemovalReason.INVALIDATED);
            } catch (Exception fail) {
                LOG.error("Failed to handle version mismatch: {}", e.getMessage(), fail);
            }
            return true;
        };
    }

    /**
     * Replaces the state used by this service.
     *
     * @param state The state to set
     */
    public void setState(RedisSessiondState state) {
        if (state != null) {
            // Drop existent timer task (if any)
            ScheduledTimerTask tmrTask = consistencyCheckTimerTaskReference.getAndSet(null);
            if (tmrTask != null) {
                tmrTask.cancel();
            }
            tmrTask = expirerAndCounterUpdateTimerTaskReference.getAndSet(null);
            if (tmrTask != null) {
                tmrTask.cancel();
            }

            // Re-schedule timer task
            TimerService timerService = services.getOptionalService(TimerService.class);
            if (timerService != null) {
                {
                    int intervalMinutes = state.getConsistencyCheckIntervalMinutes();
                    long initialDelayMinutes = 1 + ((long) (Math.random() * intervalMinutes));
                    consistencyCheckTimerTaskReference.set(timerService.scheduleWithFixedDelay(() -> new RedisSessiondConsistencyCheck(this).consistencyCheck(true, Duration.ofMinutes(intervalMinutes).toMillis(), LOCK_UPDATE_FREQUENCY_MILLIS), initialDelayMinutes, intervalMinutes, TimeUnit.MINUTES));
                }
                {
                    int intervalMinutes = state.getExpirerAndCountersUpdateIntervalMinutes();
                    long initialDelayMinutes = 1 + ((long) (Math.random() * intervalMinutes));
                    expirerAndCounterUpdateTimerTaskReference.set(timerService.scheduleWithFixedDelay(() -> new RedisSessiondExpirerAndCountersUpdater(this).expireSessionsAndUpdateCounters(Duration.ofMinutes(intervalMinutes).toMillis(), LOCK_UPDATE_FREQUENCY_MILLIS), initialDelayMinutes, intervalMinutes, TimeUnit.MINUTES));
                }
            }

            // Replace state
            replaceState(state);
        }
    }

    private void replaceState(RedisSessiondState state) {
        RedisSessiondState oldState = this.stateReference.getAndSet(state);
        if (oldState != null) {
            oldState.destroy();
        }
    }

    /**
     * Shuts-down this sessiond service.
     */
    public void shutDown() {
        // Drop existent timer task (if any)
        ScheduledTimerTask timerTask = this.consistencyCheckTimerTaskReference.getAndSet(null);
        if (timerTask != null) {
            timerTask.cancel();
        }

        // Replace state
        replaceState(null);

        // Unsubscribe from Pub/Sub channel
        try {
            channel.unsubscribe(this);
        } catch (Exception e) {
            LOG.warn("Failed to unsibscribe channel listener", e);
        }
    }

    /**
     * Gets the current state.
     *
     * @return The state
     */
    public RedisSessiondState getState() {
        RedisSessiondState state = this.stateReference.get();
        if (state == null) {
            throw new IllegalStateException("Redis session storage shutting down...");
        }
        return state;
    }

    /**
     * Gets the Redis connector.
     *
     * @return The Redis connector
     */
    public RedisConnector getConnector() {
        return connector;
    }

    /**
     * Gets the service look-up.
     *
     * @return The service look-up
     */
    public ServiceLookup getServices() {
        return services;
    }

    // ------------------------------------------------------------ Utils ------------------------------------------------------------------

    private static DefaultRedisOperationKey newExistsOperationKey(String sessionId) {
        return newExistsOperationKey(sessionId, false);
    }

    private static DefaultRedisOperationKey newExistsOperationKey(String sessionId, boolean custom) {
        return DefaultRedisOperationKey.builder().withCommand(RedisCommand.EXISTS).withContextId(custom ? 1 : 0).withUserId(1).withHash(sessionId).build();
    }

    /**
     * Extracts the session identifier portion from specified session key.
     * <pre>
     *   "ox-session:abcde123de" --&gt; "abcde123de"
     * </pre>
     *
     * @param sessionKey The session key; e.g. <code>"ox-session:abcde123de"</code>
     * @return The session identifier portion extracted from session key
     */
    public static String getSessionIdFrom(String sessionKey) {
        if (sessionKey == null) {
            throw new IllegalArgumentException("Session key must not be null");
        }
        int delimPos = sessionKey.lastIndexOf(':');
        if ((delimPos <= 0) || (delimPos >= sessionKey.length() - 1)) {
            throw new IllegalArgumentException("Invalid session key: " + sessionKey);
        }
        return sessionKey.substring(delimPos + 1);
    }

    /**
     * Gets the session keys for given collection of session identifiers.
     * <pre>
     *   "abcde123de" --&gt; "ox-session:abcde123de"
     * </pre>
     *
     * @param sessionIds The session identifiers to generate session keys for
     * @return The session keys
     */
    public static String[] getSessionKeysFor(Collection<String> sessionIds) {
        if (sessionIds == null) {
            throw new IllegalArgumentException("Session identifier collection must not be null");
        }
        int numSessionIds = sessionIds.size();
        if (numSessionIds <= 0) {
            return Strings.getEmptyStrings();
        }
        return sessionIds.stream().map(RedisSessiondService::getSessionKey).toArray(Functions.getNewStringArrayIntFunction());
    }

    /**
     * Gets the key for a session; e.g. <code>"ox-session:abcde123de"</code>
     *
     * @param sessionId The session identifier
     * @return The key
     */
    public static String getSessionKey(String sessionId) {
        if (sessionId == null) {
            throw new IllegalArgumentException("Session identifier must not be null");
        }
        return new StringBuilder(RedisSessionConstants.REDIS_SESSION).append(RedisSessionConstants.DELIMITER).append(sessionId).toString();
    }

    /**
     * Gets the key pattern to query all sessions; e.g. <code>"ox-session:*"</code>.
     *
     * @return The key pattern for all sessions
     */
    public static String getAllSessionsPattern() {
        return new StringBuilder(RedisSessionConstants.REDIS_SESSION).append(RedisSessionConstants.DELIMITER).append('*').toString();
    }

    /**
     * Gets the key for an alternative session identifier; e.g. <code>"ox-session-altid:abcde123de"</code>
     *
     * @param alternativeId The alternative session identifier
     * @return The key for an alternative session identifier
     */
    public static String getSessionAlternativeKey(String alternativeId) {
        return new StringBuilder(RedisSessionConstants.REDIS_SESSION_ALTERNATIVE_ID).append(RedisSessionConstants.DELIMITER).append(alternativeId).toString();
    }

    /**
     * Gets the key for a non-existing session identifier; e.g. <code>"ox-session-nonexistent:abcde123de"</code>
     *
     * @param nonExistentKey The non-existent session identifier
     * @return The key for an alternative session identifier
     */
    public static String getSessionNonExistentKey(String nonExistentKey) {
        return new StringBuilder(RedisSessionConstants.REDIS_SESSION_NONEXISTENT).append(RedisSessionConstants.DELIMITER).append(nonExistentKey).toString();
    }

    /**
     * Gets the key for an authentication identifier; e.g. <code>"ox-session-authid:3e5de123de"</code>
     *
     * @param authId The authentication identifier
     * @return The key for an authentication identifier
     */
    public static String getSessionAuthIdKey(String authId) {
        return new StringBuilder(RedisSessionConstants.REDIS_SESSION_AUTH_ID).append(RedisSessionConstants.DELIMITER).append(authId).toString();
    }

    /**
     * Gets the set key for given user; e.g. <code>"ox-sessionids:1337:3"</code>
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The set key
     */
    public static String getSetKeyForUser(int userId, int contextId) {
        return new StringBuilder(RedisSessionConstants.REDIS_SET_SESSIONIDS).append(RedisSessionConstants.DELIMITER).append(contextId).append(RedisSessionConstants.DELIMITER).append(userId).toString();
    }

    /**
     * Gets the pattern to look-up context-associated set keys; e.g. <code>"ox-sessionids:1337:*"</code>
     *
     * @return The pattern
     */
    public static String getSetKeyPattern(int contextId) {
        return new StringBuilder(RedisSessionConstants.REDIS_SET_SESSIONIDS).append(RedisSessionConstants.DELIMITER).append(contextId).append(RedisSessionConstants.DELIMITER).append('*').toString();
    }

    /**
     * Gets the pattern to look-up all set keys: <code>"ox-sessionids:*"</code>
     *
     * @return The pattern
     */
    public static String getAllSetKeyPattern() {
        return new StringBuilder(RedisSessionConstants.REDIS_SET_SESSIONIDS).append(RedisSessionConstants.DELIMITER).append('*').toString();
    }

    /**
     * Gets the set key for given brand; e.g. <code>"ox-sessionids-brand:aol"</code>
     *
     * @param brandId The identifier of the brand; e.g. <code>"aol"</code>
     * @return The set key
     */
    public static String getSetKeyForBrand(String brandId) {
        return new StringBuilder(RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_BRAND).append(RedisSessionConstants.DELIMITER).append(brandId).toString();
    }

    /**
     * Gets the pattern to look-up all brand set keys: <code>"ox-sessionids-brand:*"</code>
     *
     * @return The pattern
     */
    public static String getAllBrandSetKeyPattern() {
        return new StringBuilder(RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_BRAND).append(RedisSessionConstants.DELIMITER).append('*').toString();
    }

    /**
     * Gets the key for the sorted set applicable to given session.
     *
     * @param session The session
     * @return The sort set key
     */
    public static String getSortSetKeyForSession(Session session) {
        return session.isStaySignedIn() ? RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_LONG_LIFETIME : RedisSessionConstants.REDIS_SORTEDSET_SESSIONIDS_SHORT_LIFETIME;
    }

    // --------------------------------------------------------- Commands ------------------------------------------------------------------

    /**
     * Gets the session commands using given Redis commands provider using default version mismatch handler, but w/o
     * obfuscation/un-obfuscation of sessions' passwords.
     *
     * @param commandsProvider The commands provider
     * @param versionMismatchHandler The version mismatch handler to use to check deserialzed session data for consistency
     * @return The session commands
     */
    public RedisStringCommands<String, Session> getSessionCommandsWithoutObfuscator(RedisCommandsProvider commandsProvider) {
        return getSessionCommandsWithoutObfuscator(commandsProvider, versionMismatchHandler);
    }

    /**
     * Gets the session commands using given Redis commands provider, but w/o obfuscation/un-obfuscation of sessions' passwords.
     *
     * @param commandsProvider The commands provider
     * @param versionMismatchHandler The version mismatch handler to use to check deserialzed session data for consistency
     * @return The session commands
     */
    public static RedisStringCommands<String, Session> getSessionCommandsWithoutObfuscator(RedisCommandsProvider commandsProvider, VersionMismatchHandler versionMismatchHandler) {
        return new SessionRedisStringCommands(commandsProvider.getRawStringCommands(), null, versionMismatchHandler);
    }

    /**
     * Gets the session commands using given Redis commands provider using state's obfuscator and default version mismatch handler.
     *
     * @param commandsProvider The commands provider
     * @return The session commands
     * @see #getSessionCommands(RedisCommandsProvider, Obfuscator, VersionMismatchHandler)
     */
    public RedisStringCommands<String, Session> getSessionCommandsWithObfuscator(RedisCommandsProvider commandsProvider) {
        return getSessionCommandsWithObfuscator(commandsProvider, getState().getObfuscator(), versionMismatchHandler);
    }

    /**
     * Gets the session commands using given Redis commands provider performing obfuscation/un-obfuscation of sessions' passwords.
     *
     * @param commandsProvider The commands provider
     * @param optObfuscator The obfuscator to use or <code>null</code> to serialize/deserialize sessions w/o obfuscation/un-obfuscation
     * @param versionMismatchHandler The version mismatch handler to use to check deserialzed session data for consistency
     * @return The session commands
     */
    public static RedisStringCommands<String, Session> getSessionCommandsWithObfuscator(RedisCommandsProvider commandsProvider, Obfuscator optObfuscator, VersionMismatchHandler versionMismatchHandler) {
        return new SessionRedisStringCommands(commandsProvider.getRawStringCommands(), optObfuscator, versionMismatchHandler);
    }

    // ------------------------------------------------------- Locking/version stuff -------------------------------------------------------

    /**
     * Acquires the named lock.
     *
     * @param lockKey The key of the lock
     * @param timeoutMillis The lock's time to live in milliseconds
     * @param updateFrequencyMillis The frequency in milliseconds to reset lock's time to live
     * @param commandsProvider The commands provider to use
     * @return The lock instance if acquired; otherwise empty
     * @throws RedisException If lock acquisition fails
     */
    public Optional<RedisLock> acquireLock(String lockKey, long timeoutMillis, long updateFrequencyMillis, RedisCommandsProvider commandsProvider) {
        return RedisLock.lockFor(lockKey, timeoutMillis, updateFrequencyMillis, commandsProvider, this);
    }

    private static final long VERSION_LOCK_TIMEOUT_MILLIS = 30_000L;
    private static final long VERSION_LOCK_UPDATE_MILLIS = 10_000L;

    /**
     * Check if version of session-related stuff held in Redis is equal to expected version.
     * <p>
     * Otherwise version is incremented and all sesssion-related stuff is dropped.
     *
     * @return <code>true</code> if version is all fine; otherwise <code>false</code> to signal version has been updated
     * @throws OXException If version check fails
     */
    private boolean checkVersion() throws OXException {
        Boolean result = connector.<Boolean> executeOperation(commandsProvider -> {
            String sVersion = commandsProvider.getStringCommands().get(RedisSessionConstants.REDIS_SESSION_VERSION);
            if (sVersion != null && RedisSessionConstants.STRUCTURE_VERSION <= Integer.parseInt(sVersion)) {
                return Boolean.TRUE;
            }

            Optional<RedisLock> optionalLock = acquireLock(RedisSessionConstants.REDIS_SESSION_VERSION_LOCK, VERSION_LOCK_TIMEOUT_MILLIS, VERSION_LOCK_UPDATE_MILLIS, commandsProvider);
            if (optionalLock.isEmpty()) {
                // Lock NOT acquired
                return null;
            }

            // Lock acquired
            return checkVersionElseDrop(commandsProvider, optionalLock.get());
        });

        if (result != null) {
            // There is a result already
            return result.booleanValue();
        }

        // Lock could not be acquired. Await lock availability & proceed
        int retryCount = 0;
        while (result == null) {
            long millisToWait = (++retryCount * VERSION_LOCK_UPDATE_MILLIS) + ((long) (Math.random() * VERSION_LOCK_UPDATE_MILLIS));
            LOG.info("Failed to acquire lock \"{}\". Waiting for {} and then retrying...", RedisSessionConstants.REDIS_SESSION_VERSION_LOCK, new HumanTimeOutputter(millisToWait, true));
            long nanosToWait = TimeUnit.NANOSECONDS.convert(millisToWait, TimeUnit.MILLISECONDS);
            LockSupport.parkNanos(nanosToWait);
            result = connector.<Boolean> executeOperation(commandsProvider -> {
                Optional<RedisLock> optionalLock = acquireLock(RedisSessionConstants.REDIS_SESSION_VERSION_LOCK, VERSION_LOCK_TIMEOUT_MILLIS, VERSION_LOCK_UPDATE_MILLIS, commandsProvider);
                if (optionalLock.isEmpty()) {
                    // Lock NOT acquired
                    return null;
                }

                // Lock acquired
                return checkVersionElseDrop(commandsProvider, optionalLock.get());
            });
        }
        return result.booleanValue();
    }

    private static Boolean checkVersionElseDrop(RedisCommandsProvider commandsProvider, RedisLock lock) {
        try {
            // Check version again
            String sVersion = commandsProvider.getStringCommands().get(RedisSessionConstants.REDIS_SESSION_VERSION);
            if (sVersion != null && RedisSessionConstants.STRUCTURE_VERSION <= Integer.parseInt(sVersion)) {
                // Version equal to expected version in the meantime
                return Boolean.TRUE;
            }

            // Collect keys to drop
            List<String> keysToDrop = collectAllSessionReleatedKeys(commandsProvider);

            // Delete all keys related to session
            int numOfKeys = keysToDrop.size();
            if (numOfKeys > 0) {
                commandsProvider.getKeyCommands().del(keysToDrop.toArray(new String[numOfKeys]));
            }

            // Finally, update version
            commandsProvider.getStringCommands().set(RedisSessionConstants.REDIS_SESSION_VERSION, Integer.toString(RedisSessionConstants.STRUCTURE_VERSION));
        } finally {
            lock.unlock(commandsProvider);
        }
        return Boolean.FALSE;
    }

    private static final Set<String> VERSION_KEYS = Set.of(RedisSessionConstants.REDIS_SESSION_VERSION, RedisSessionConstants.REDIS_SESSION_VERSION_LOCK);

    private static List<String> collectAllSessionReleatedKeys(RedisCommandsProvider commandsProvider) {
        List<String> keysToDrop = null;
        ScanArgs allSessionKeysArgs = ScanArgs.Builder.matches(RedisSessionConstants.REDIS_SESSION + "*").limit(RedisSessionConstants.LIMIT_1000);
        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
        KeyScanCursor<String> cursor = keyCommands.scan(allSessionKeysArgs);
        while (cursor != null) {
            // Obtain current keys...
            List<String> keys = cursor.getKeys();
            if (!keys.isEmpty()) {
                keys = keys.stream().filter(k -> !VERSION_KEYS.contains(k)).collect(CollectorUtils.toList(keys));
                keysToDrop = addAllWithCreateIfNull(keys, keysToDrop);
            }

            // Move cursor forward
            cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, allSessionKeysArgs);
        }
        return keysToDrop == null ? Collections.emptyList() : keysToDrop;
    }

    // ------------------------------------------------------- Counter stuff ---------------------------------------------------------------

    private void incrementCountersFor(Session session) {
        try {
            String brandName = (String) session.getParameter(Session.PARAM_BRAND);
            String brandId = getBrandIdentifierFor(brandName);

            // Register metric
            if (brandId != null) {
                SessionMetricHandler.registerBrandMetricIfAbsent(brandId);
            }

            // Increment counter in Redis
            connector.executeVoidOperation(commandsProvider -> {
                RedisHashCommands<String, String> hashCommands = commandsProvider.getHashCommands();

                hashCommands.hincrby(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, RedisSessionConstants.COUNTER_SESSION_TOTAL, 1);
                hashCommands.hincrby(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, RedisSessionConstants.COUNTER_SESSION_ACTIVE, 1);
                hashCommands.hincrby(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, session.isStaySignedIn() ? RedisSessionConstants.COUNTER_SESSION_LONG : RedisSessionConstants.COUNTER_SESSION_SHORT, 1);

                if (brandId != null) {
                    hashCommands.hincrby(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, brandId + RedisSessionConstants.COUNTER_SESSION_TOTAL_APPENDIX, 1);
                    hashCommands.hincrby(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, brandId + RedisSessionConstants.COUNTER_SESSION_ACTIVE_APPENDIX, 1);
                }
            });
        } catch (Exception e) {
            LOG.warn("Failed to increment counters for session {}", session.getSessionID(), e);
        }
    }

    private void decrementCountersFor(Session session) {
        try {
            String brandId = getBrandIdentifierFrom(session);
            connector.executeVoidOperation(commandsProvider -> {
                RedisHashCommands<String, String> hashCommands = commandsProvider.getHashCommands();
                hashCommands.hincrby(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, RedisSessionConstants.COUNTER_SESSION_TOTAL, -1);
                hashCommands.hincrby(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, session.isStaySignedIn() ? RedisSessionConstants.COUNTER_SESSION_LONG : RedisSessionConstants.COUNTER_SESSION_SHORT, -1);
                if (brandId != null) {
                    hashCommands.hincrby(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, brandId + RedisSessionConstants.COUNTER_SESSION_TOTAL_APPENDIX, -1);
                }
            });
        } catch (Exception e) {
            LOG.warn("Failed to decrement counters for session {}", session.getSessionID(), e);
        }
    }

    private <S extends Session> void decrementCountersFor(Collection<S> sessions) {
        if (sessions == null) {
            return;
        }

        if (sessions.size() == 1) {
            decrementCountersFor(sessions.iterator().next());
            return;
        }

        try {
            // Collect counts to decrement
            Map<String, Counter> countByField = new HashMap<>();
            for (Session session : sessions) {
                String brandId = getBrandIdentifierFrom(session);
                if (brandId != null) {
                    countByField.computeIfAbsent(brandId + RedisSessionConstants.COUNTER_SESSION_TOTAL_APPENDIX, RedisSessionConstants.getNewCounterFunction()).increment();
                }
                if (session.isStaySignedIn()) {
                    countByField.computeIfAbsent(RedisSessionConstants.COUNTER_SESSION_LONG, RedisSessionConstants.getNewCounterFunction()).increment();
                } else {
                    countByField.computeIfAbsent(RedisSessionConstants.COUNTER_SESSION_SHORT, RedisSessionConstants.getNewCounterFunction()).increment();
                }
            }

            // Apply collected counts
            connector.executeVoidOperation(commandsProvider -> {
                RedisHashCommands<String, String> hashCommands = commandsProvider.getHashCommands();
                hashCommands.hincrby(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, RedisSessionConstants.COUNTER_SESSION_TOTAL, Math.negateExact(sessions.size()));
                for (Map.Entry<String, Counter> countEntry : countByField.entrySet()) {
                    hashCommands.hincrby(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, countEntry.getKey(), Math.negateExact(countEntry.getValue().getCount()));
                }
            });
        } catch (Exception e) {
            LOG.warn("Failed to decrement counters for sessions {}", sessions, e);
        }
    }

    /**
     * Gets the configured value for max. number of sessions.
     *
     * @return The max. number of sessions
     */
    public int getMaxNumberOfSessions() {
        return getState().getMaxSessions();
    }

    /**
     * Queries the counter for the number of active sessions.
     *
     * @param brandId The brand identifier
     * @return The counter's value for number of active sessions
     * @throws OXException If number of active sessions be returned
     */
    public long queryCounterForNumberOfActiveSessionsForBrand(String brandId) throws OXException {
        return connector.<Long> executeOperation(commandsProvider -> {
            long total = doQueryNumberOfSessionsForBrand(brandId, commandsProvider);
            if (total <= 0) {
                return Long.valueOf(0);
            }

            String sCount = commandsProvider.getHashCommands().hget(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, brandId + RedisSessionConstants.COUNTER_SESSION_ACTIVE_APPENDIX);
            long active = sCount == null ? 0L : Long.parseLong(sCount);
            return Long.valueOf(active > total ? total : active);
        }).longValue();
    }

    /**
     * Queries the number of sessions for given brand name.
     *
     * @param brandId The identifier of the brand
     * @return The number of sessions for given brand name
     * @throws OXException If number of sessions for given brand name cannot be returned
     */
    public long queryNumberOfSessionsForBrand(String brandId) throws OXException {
        return connector.<Long> executeOperation(commandsProvider -> Long.valueOf(doQueryNumberOfSessionsForBrand(brandId, commandsProvider))).longValue();
    }

    private static long doQueryNumberOfSessionsForBrand(String brandId, RedisCommandsProvider commandsProvider) {
        String sCount = commandsProvider.getHashCommands().hget(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, brandId + RedisSessionConstants.COUNTER_SESSION_TOTAL_APPENDIX);
        return (sCount == null ? 0L : Long.parseLong(sCount));
    }

    /**
     * Queries the counter for the number of active sessions.
     *
     * @return The counter's value for number of active sessions
     * @throws OXException If number of active sessions be returned
     */
    public long queryCounterForNumberOfActiveSessions() throws OXException {
        return connector.<Long> executeOperation(commandsProvider -> {
            long total = doQueryCounterForNumberOfSessions(commandsProvider);
            if (total <= 0) {
                return Long.valueOf(0);
            }

            String sCount = commandsProvider.getHashCommands().hget(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, RedisSessionConstants.COUNTER_SESSION_ACTIVE);
            long active = sCount == null ? 0L : Long.parseLong(sCount);
            return Long.valueOf(active > total ? total : active);
        }).longValue();
    }

    /**
     * Queries the counter for the total number of sessions.
     *
     * @return The counter's value for total number of sessions
     * @throws OXException If total number of sessions cannot be returned
     */
    public long queryCounterForNumberOfSessions() throws OXException {
        return connector.<Long> executeOperation(commandsProvider -> Long.valueOf(doQueryCounterForNumberOfSessions(commandsProvider))).longValue();
    }

    /**
     * Queries the counter for the total number of sessions.
     *
     * @return The counter's value for total number of sessions
     */
    protected static long doQueryCounterForNumberOfSessions(RedisCommandsProvider commandsProvider) {
        String sCount = commandsProvider.getHashCommands().hget(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, RedisSessionConstants.COUNTER_SESSION_TOTAL);
        return (sCount == null ? 0 : Long.parseLong(sCount));
    }

    /**
     * Queries the counter for the number of sessions having a short life time.
     *
     * @return The counter's value for number of sessions having a short life time
     * @throws OXException If number of sessions having a short life time cannot be returned
     */
    public long queryCounterForNumberOfShortSessions() throws OXException {
        return connector.<Long> executeOperation(commandsProvider -> {
            String sCount = commandsProvider.getHashCommands().hget(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, RedisSessionConstants.COUNTER_SESSION_SHORT);
            return Long.valueOf(sCount == null ? 0L : Long.parseLong(sCount));
        }).longValue();
    }

    /**
     * Queries the counter for the number of sessions having a long life time.
     *
     * @return The counter's value for number of sessions having a long life time
     * @throws OXException If number of sessions having a long life time cannot be returned
     */
    public long queryCounterForNumberOfLongSessions() throws OXException {
        return connector.<Long> executeOperation(commandsProvider -> {
            String sCount = commandsProvider.getHashCommands().hget(RedisSessionConstants.REDIS_HASH_SESSION_COUNTER, RedisSessionConstants.COUNTER_SESSION_LONG);
            return Long.valueOf(sCount == null ? 0L : Long.parseLong(sCount));
        }).longValue();
    }

    private List<String> getBrandIdsForCounters() {
        ScanArgs scanArgs = ScanArgs.Builder.matches(getAllBrandSetKeyPattern()).limit(RedisSessionConstants.LIMIT_1000);

        try {
            return connector.executeOperation(commandsProvider -> {
                RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
                List<String> brandIds = null;

                KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
                while (cursor != null) {
                    // Obtain current keys...
                    List<String> brandSetKeys = cursor.getKeys();
                    for (String brandSetKey : brandSetKeys) {
                        brandIds = addWithCreateIfNull(brandSetKey.substring(brandSetKey.lastIndexOf(':') + 1), brandIds, brandSetKeys.size());
                    }

                    // Move cursor forward
                    cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
                }

                return brandIds == null ? Collections.emptyList() : brandIds;
            });
        } catch (Exception e) {
            LOG.error("Failed to determine brand identifiers from Redis storage", e);
            return Collections.emptyList();
        }
    }

    // ------------------------------------------------------- Session stuff ---------------------------------------------------------------

    @Override
    public boolean isCentral() {
        return true;
    }

    @Override
    public Session addSession(AddSessionParameter parameter) throws OXException {
        int userId = parameter.getUserId();
        int contextId = parameter.getContext().getContextId();
        LOG.debug("Adding session with client {} for user {} in context {} ({})", parameter.getClient(), I(userId), I(contextId), parameter.getFullLogin());

        // Obtain current state
        RedisSessiondState state = getState();

        // Various checks
        {
            int maxSessions = state.getMaxSessions();
            UserTypeSessiondConfigInterface userTypeConfig = state.getUserConfigRegistry().getConfigFor(userId, contextId);
            int maxSessPerUser = userTypeConfig.getMaxSessionsPerUserType();
            int maxSessPerClient = services.getServiceSafe(LeanConfigurationService.class).getIntProperty(userId, contextId, RedisSessiondConfigProperty.MAX_SESSIONS_PER_CLIENT);
            LOG.debug("Performing checks for max. number of sessions with client {} for user {} in context {} ({})", parameter.getClient(), I(userId), I(contextId), parameter.getFullLogin());
            connector.executeVoidOperation(commandsProvider -> {
                checkMaxSessions(maxSessions, commandsProvider);
                checkMaxSessPerUser(maxSessPerUser, userId, contextId, commandsProvider);
                checkMaxSessPerClient(maxSessPerClient, parameter.getClient(), userId, contextId, commandsProvider);
                checkAuthId(parameter.getFullLogin(), parameter.getAuthId(), commandsProvider);
            });
        }

        // Create and optionally enhance new session instance
        SessionImpl newSession;
        List<SessionEnhancement> enhancements = parameter.getEnhancements();
        if (null == enhancements || enhancements.isEmpty()) {
            newSession = createNewSession(userId, parameter.getUserLoginInfo(), parameter.getPassword(), contextId, parameter.getClientIP(), parameter.getFullLogin(), parameter.getAuthId(), parameter.getHash(), parameter.getClient(), parameter.isStaySignedIn(), parameter.getOrigin());
        } else {
            // Create intermediate SessionDescription instance to offer more flexibility to possible SessionEnhancement implementations
            SessionDescription sessionDescription = createSessionDescription(userId, parameter.getUserLoginInfo(), parameter.getPassword(), contextId, parameter.getClientIP(), parameter.getFullLogin(), parameter.getAuthId(), parameter.getHash(), parameter.getClient(), parameter.isStaySignedIn(), parameter.getOrigin());
            for (SessionEnhancement enhancement: enhancements) {
                enhancement.enhanceSession(sessionDescription);
            }
            newSession = new SessionImpl(sessionDescription);
        }

        if (Strings.isNotEmpty(parameter.getUserAgent())) {
            newSession.setParameter(Session.PARAM_USER_AGENT, parameter.getUserAgent());
        }

        // Set time stamp
        newSession.setParameter(Session.PARAM_LOGIN_TIME, Long.valueOf(System.currentTimeMillis()));
        LOG.debug("Created new session instance {} with client {} for user {} in context {} ({})", newSession.getSessionID(), parameter.getClient(), I(userId), I(contextId), parameter.getFullLogin());

        // Either add session or yield short-time token for it
        if (null != parameter.getClientToken()) {
            String serverToken = UUIDs.getUnformattedString(UUID.randomUUID());
            newSession.setParameter("serverToken", serverToken);
            TokenSessionContainer.getInstance().addSession(newSession, parameter.getClientToken(), serverToken);
            LOG.debug("Created server token for session {} of user {} in context {}", newSession.getSessionID(), I(userId), I(contextId));
            return newSession;
        }

        // Add session & increment counters
        putSessionIntoRedisAndLocal(newSession, state);
        incrementCountersFor(newSession);

        // Post event for created session
        postSessionCreation(newSession);
        postSessionStored(newSession, false);
        LOG.debug("Completed adding session {} of user {} in context {}", newSession.getSessionID(), I(userId), I(contextId));
        return newSession;
    }

    /**
     * Puts specified session instance into Redis storage and local cache
     *
     * @param sessionToAdd The session to add
     * @param addIfAbsent <code>true</code> to only add session if not already contained; otherwise <code>false</code>
     * @return <code>true</code> if session has been put; otherwise <code>false</code>
     * @throws OXException If operation fails
     */
    public boolean putSessionIntoRedisAndLocal(SessionImpl sessionToAdd, boolean addIfAbsent) throws OXException {
        if (addIfAbsent && connector.executeOperation(commandsProvider -> commandsProvider.getKeyCommands().exists(getSessionKey(sessionToAdd.getSessionID()))).longValue() > 0) {
            // Already contained
            return false;
        }

        // Do add session
        putSessionIntoRedisAndLocal(sessionToAdd, getState());
        return true;
    }

    /**
     * Puts specified session instance into Redis storage and local cache
     *
     * @param sessionToAdd The session to add
     * @param state The current state
     * @throws OXException If adding session fails
     */
    private void putSessionIntoRedisAndLocal(SessionImpl sessionToAdd, RedisSessiondState state) throws OXException {
        // Add to Redis
        putSessionIntoRedis(sessionToAdd, state, true);

        // Add to local cache
        try {
            // Pre-set last-checked time stamp if threshold is enabled to avoid superfluous EXISTS command for newly added session
            if (state.getCheckExistenceThreshold() > 0) {
                sessionToAdd.setLastChecked(System.currentTimeMillis());
            }
            state.getLocalSessionCache().put(sessionToAdd, true);
            LOG.debug("Put session {} into local cache for user {} in context {}", sessionToAdd.getSessionID(), I(sessionToAdd.getUserId()), I(sessionToAdd.getContextId()));
        } catch (Exception e) {
            LOG.warn("Failed to put session {} into local cache for user {} in context {}", sessionToAdd.getSessionID(), I(sessionToAdd.getUserId()), I(sessionToAdd.getContextId()), e);
        }
    }

    private void putSessionIntoRedis(SessionImpl sessionToAdd, RedisSessiondState state, boolean addToRemoteSites) throws OXException {
        // Invoke interceptors...
        SessionImpl modifiedSession = null;
        for (SessionSerializationInterceptor interceptor : serializationInterceptors) {
            if (modifiedSession == null) {
                // Only copy if needed
                modifiedSession = new SessionImpl(sessionToAdd);
            }
            interceptor.serialize(modifiedSession);
        }

        SessionImpl newSession = modifiedSession != null ? modifiedSession : sessionToAdd;
        byte[] sessionBytes = generateBytesFromSession(newSession, state);

        connector.executeVoidOperation(commandsProvider -> addToRedisCollections(newSession, sessionBytes, false, state, commandsProvider));

        if (addToRemoteSites) {
            List<RedisConnectorProvider> remoteConnectorProviders = getRemoteConnectorProviders();
            if (!remoteConnectorProviders.isEmpty()) {
                RedisVoidOperation addToRedisCollectionsOperation = commandsProvider -> addToRedisCollections(newSession, sessionBytes, true, state, commandsProvider);
                for (RedisConnectorProvider connectorProvider : remoteConnectorProviders) {
                    RedisConnector remoteConnector = connectorProvider.getConnector();
                    try {
                        remoteConnector.executeVoidOperation(addToRedisCollectionsOperation);
                    } catch (Exception e) {
                        LOG.error("Failed to add session {} to remote Redis storage {}", newSession.getSessionID(), remoteConnector, e);
                    }
                }
            }
        }
    }

    private static byte[] generateBytesFromSession(Session session, RedisSessiondState optState) throws OXException {
        try {
            return SessionCodec.session2JsonBytes(session, RedisSessionConstants.GZIP, optState == null ? null : optState.getObfuscator(), RedisSessionVersionService.getInstance());
        } catch (JSONException e) {
            throw RedisExceptionCode.JSON_ERROR.create(e, e.getMessage());
        }
    }

    private void addToRedisCollections(SessionImpl newSession, byte[] sessionBytes, boolean remote, RedisSessiondState state, RedisCommandsProvider commandsProvider) {
        RedisSortedSetCommands<String, String> sortedSetCommands = commandsProvider.getSortedSetCommands();
        RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();
        RedisStringCommands<String, String> stringCommands = commandsProvider.getStringCommands();
        RedisStringCommands<String, InputStream> rawStringCommands = commandsProvider.getRawStringCommands();

        // Add to main Redis collection
        rawStringCommands.set(getSessionKey(newSession.getSessionID()), Streams.newByteArrayInputStream(sessionBytes));

        // Add to Redis set and mapping tables
        long now = System.currentTimeMillis();
        sortedSetCommands.zadd(getSortSetKeyForSession(newSession), now, newSession.getSessionID()); // The Redis sorted set for session timeouts and detection of "active" sessions
        setCommands.sadd(getSetKeyForUser(newSession.getUserId(), newSession.getContextId()), newSession.getSessionID()); // The Redis set for user-context-tuple to session identifier
        if (newSession.getAlternativeId() != null) {
            stringCommands.set(getSessionAlternativeKey(newSession.getAlternativeId()), newSession.getSessionID()); // The Redis entry for alternative to regular session identifier mapping
        }
        if (newSession.getAuthId() != null) {
            stringCommands.set(getSessionAuthIdKey(newSession.getAuthId()), newSession.getSessionID()); // The Redis entry for authentication identifier to session identifier mapping
        }
        String brandId = getBrandIdentifierFrom(newSession);
        if (brandId != null) {
            sortedSetCommands.zadd(getSetKeyForBrand(brandId), now, newSession.getSessionID()); // The Redis sorted set for brand-specific(!) session timeouts and detection of "active" sessions
        }
        LOG.debug("Put session {} into {}Redis storage for user {} in context {}:{}{}", newSession.getSessionID(), remote ? "remote " : "", I(newSession.getUserId()), I(newSession.getContextId()), Strings.getLineSeparator(), prettyPrinterFor(newSession, state));
    }

    /**
     * Creates a new instance of {@code SessionImpl} from specified arguments
     *
     * @param userId The user identifier
     * @param loginName The login name
     * @param password The password
     * @param contextId The context identifier
     * @param clientHost The client host name or IP address
     * @param login The login; e.g. <code>"someone@invalid.com"</code>
     * @param authId The authentication identifier
     * @param hash The hash string
     * @param client The client identifier
     * @param staySignedIn Whether session is supposed to be annotated with "stay signed in"; otherwise <code>false</code>
     * @return The newly created {@code SessionImpl} instance
     */
    private static SessionImpl createNewSession(int userId, String loginName, String password, int contextId, String clientHost, String login, String authId, String hash, String client, boolean staySignedIn, Origin origin) {
        // Generate identifier, secret, and random
        String sessionId = UUIDs.getUnformattedString(UUID.randomUUID());
        String secret = UUIDs.getUnformattedString(UUID.randomUUID());
        String randomToken = UUIDs.getUnformattedString(UUID.randomUUID());

        // Create & return the instance
        return new SessionImpl(userId, loginName, password, contextId, sessionId, secret, randomToken, clientHost, login, authId, hash, client, staySignedIn, origin);
    }

    /**
     * Creates a new instance of {@code SessionDescription} from specified arguments
     *
     * @param userId The user identifier
     * @param loginName The login name
     * @param password The password
     * @param contextId The context identifier
     * @param clientHost The client host name or IP address
     * @param login The login; e.g. <code>"someone@invalid.com"</code>
     * @param authId The authentication identifier
     * @param hash The hash string
     * @param client The client identifier
     * @param staySignedIn Whether session is supposed to be annotated with "stay signed in"; otherwise <code>false</code>
     * @return The newly created {@code SessionDescription} instance
     */
    private static SessionDescription createSessionDescription(int userId, String loginName, String password, int contextId, String clientHost, String login, String authId, String hash, String client, boolean staySignedIn, Origin origin) {
        // Generate identifier, secret, and random
        String sessionId = UUIDs.getUnformattedString(UUID.randomUUID());
        String secret = UUIDs.getUnformattedString(UUID.randomUUID());
        String randomToken = UUIDs.getUnformattedString(UUID.randomUUID());

        // Create instance
        SessionDescription newSession = new SessionDescription(userId, contextId, login, password, sessionId, secret, UUIDs.getUnformattedString(UUID.randomUUID()), origin);
        newSession.setLoginName(loginName);
        newSession.setLocalIp(clientHost);
        newSession.setAuthId(authId);
        newSession.setStaySignedIn(staySignedIn);
        newSession.setClient(client);
        newSession.setRandomToken(randomToken);
        newSession.setHash(hash);
        return newSession;
    }

    private static void checkMaxSessions(int maxSessions, RedisCommandsProvider commandsProvider) throws OXException {
        if (maxSessions <= 0) {
            return;
        }

        long numSessions = doQueryCounterForNumberOfSessions(commandsProvider);
        if (numSessions + 1 > maxSessions) {
            LOG.debug("Max. number of sessions ({}) exceeded. Denying adding session...", I(maxSessions));
            throw SessionExceptionCodes.MAX_SESSION_EXCEPTION.create();
        }
    }

    private static void checkMaxSessPerUser(int maxSessPerUser, int userId, int contextId, RedisCommandsProvider commandsProvider) throws OXException {
        if (maxSessPerUser <= 0) {
            return;
        }

        Long numberOfSessions = commandsProvider.getSetCommands().scard(getSetKeyForUser(userId, contextId));
        if (numberOfSessions != null && numberOfSessions.longValue() >= maxSessPerUser) {
            LOG.debug("Max. number of sessions ({}) exceeded user {} in context {}. Denying adding session...", I(maxSessPerUser), I(userId), I(contextId));
            throw SessionExceptionCodes.MAX_SESSION_PER_USER_EXCEPTION.create(I(userId), I(contextId));
        }
    }

    private void checkMaxSessPerClient(int maxSessPerClient, String client, int userId, int contextId, RedisCommandsProvider commandsProvider) throws OXException {
        if (null == client || maxSessPerClient <= 0) {
            return;
        }

        RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();
        Set<String> sessionIds = setCommands.smembers(getSetKeyForUser(userId, contextId));
        if (sessionIds == null || sessionIds.isEmpty()) {
            return;
        }

        RedisStringCommands<String, Session> sessionCommands = getSessionCommandsWithoutObfuscator(commandsProvider);
        int count = 0;
        List<String> sessionKeys = sessionIds.stream().map(FUNCTION_SESSION_KEY).collect(CollectorUtils.toList(sessionIds));
        for (KeyValue<String, Session> keyValue : sessionCommands.mget(sessionKeys.toArray(new String[sessionKeys.size()]))) {
            String sessionId = getSessionIdFrom(keyValue.getKey());
            Session obfuscatedSession = keyValue.hasValue() ? keyValue.getValue() : null;
            if (obfuscatedSession == null) {
                setCommands.srem(getSetKeyForUser(userId, contextId), sessionId);
            } else if (client.equals(obfuscatedSession.getClient()) && ++count > maxSessPerClient) {
                LOG.debug("Max. number of sessions ({}) exceeded for client {} of user {} in context {}. Denying adding session...", I(maxSessPerClient), client, I(userId), I(contextId));
                throw SessionExceptionCodes.MAX_SESSION_PER_CLIENT_EXCEPTION.create(client, I(userId), I(contextId));
            }
        }
    }

    private void checkAuthId(String login, String authId, RedisCommandsProvider commandsProvider) throws OXException {
        String sessionId = commandsProvider.getStringCommands().get(getSessionAuthIdKey(authId));
        if (sessionId != null) {
            // There is a session identifier associated with given authentication identifier
            Session obfuscatedSession = getSessionCommandsWithoutObfuscator(commandsProvider).get(getSessionKey(sessionId));
            if (obfuscatedSession != null) {
                throw SessionExceptionCodes.DUPLICATE_AUTHID.create(obfuscatedSession.getLogin(), login);
            }
            // No such session
            commandsProvider.getKeyCommands().del(getSessionAuthIdKey(authId));
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    @Override
    public boolean storeSession(String sessionId) throws OXException {
        return storeSession(sessionId, false);
    }

    @Override
    public boolean storeSession(String sessionId, boolean addIfAbsent) throws OXException {
        LOG.debug("Storing session by {}", sessionId);
        RedisSessiondState state = getState();

        // Only "store" a session when there is a local session instance, which might hold changes that need to be added to Redis storage
        // Check local cache
        LOG.debug("Look-up in local cache for {}", sessionId);
        SessionImpl session = state.getLocalSessionCache().getSessionByIdIfPresent(sessionId);
        if (session == null) {
            // As already available in Redis storage the session can be considered as "stored" as long as session exists
            return connector.executeOperation(newExistsOperationKey(sessionId), commandsProvider -> commandsProvider.getKeyCommands().exists(getSessionKey(sessionId))).longValue() > 0;
        }

        // Ensure existence in Redis storage
        LOG.debug("Obtained session {} from local cache", sessionId);
        session = ensureExistenceElseNull(session, true, state);
        if (session == null) {
            return false;
        }

        // Replace session in Redis storage
        replaceSession(session, state);
        return true;
    }

    @Override
    public boolean storeSession(Session session, boolean addIfAbsent) throws OXException {
        if (session == null) {
            return false;
        }

        LOG.debug("Storing session instance for {}", session.getSessionID());
        if (connector.<Long> executeOperation(newExistsOperationKey(session.getSessionID()), commandsProvider -> commandsProvider.getKeyCommands().exists(getSessionKey(session.getSessionID()))).longValue() <= 0) {
            LOG.debug("Denied storing session for {}: No such session in Redis storage.", session.getSessionID());
        }

        // Replace session in Redis storage
        replaceSession(newSessionImplFor(session), getState());
        return true;
    }

    private void replaceSession(SessionImpl sessionToStore, RedisSessiondState state) throws OXException {
        // Do replace session
        putSessionIntoRedisAndLocal(sessionToStore, state);

        // Post event for "stored" session
        postSessionStored(sessionToStore, true);
        LOG.debug("Completed storing session {} of user {} in context {}", sessionToStore.getSessionID(), I(sessionToStore.getUserId()), I(sessionToStore.getContextId()));
    }

    @Override
    public void changeSessionPassword(String sessionId, String newPassword) throws OXException {
        // Change password in local session
        RedisSessiondState state = getState();
        SessionImpl session = state.getLocalSessionCache().getSessionByIdIfPresent(sessionId);
        if (session != null) {
            session.setPassword(newPassword);
        }

        // Check for session Redis storage
        ClusterSession obfuscatedSession = connector.executeOperation(commandsProvider -> {
            Session ses = getSessionCommandsWithoutObfuscator(commandsProvider).get(getSessionKey(sessionId));
            return (ses instanceof ClusterSession cs) ? cs : null;
        });

        List<String> sessionIdsToRemove;
        if (obfuscatedSession != null) {
            // Change password in session instance & serialize to bytes
            ClusterSession newSession = obfuscatedSession;
            newSession.setPassword(newPassword);
            byte[] sessionBytes = generateBytesFromSession(newSession, state);

            // Change password in Redis storage
            sessionIdsToRemove = connector.executeOperation(commandsProvider -> {
                // Put into Redis storage
                commandsProvider.getRawStringCommands().set(getSessionKey(sessionId), Streams.newByteArrayInputStream(sessionBytes));

                // Refresh session's expiration
                RedisSortedSetCommands<String, String> sortedSetCommands = commandsProvider.getSortedSetCommands();
                long now = System.currentTimeMillis();
                sortedSetCommands.zadd(getSortSetKeyForSession(newSession), now, newSession.getSessionID());
                String brandId = getBrandIdentifierFrom(newSession);
                if (brandId != null) {
                    sortedSetCommands.zadd(getSetKeyForBrand(brandId), now, newSession.getSessionID());
                }

                // Invalidate all other user sessions
                Set<String> sessionIds = commandsProvider.getSetCommands().smembers(getSetKeyForUser(newSession.getUserId(), newSession.getContextId()));
                List<String> toRemove = new ArrayList<>(sessionIds.size());
                for (String sessId : sessionIds) {
                    if (isNotEqual(sessionId, sessId)) {
                        toRemove.add(sessId);
                    }
                }
                return toRemove;
            });
        } else {
            // No such session in Redis storage
            sessionIdsToRemove = Collections.emptyList();
        }

        // Publish INVALIDATE event for changed session manually
        publishInvalidateEventQuietly(Collections.singletonList(sessionId));

        removeSessions(sessionIdsToRemove, RemovalReason.INVALIDATED);
    }

    private static boolean isNotEqual(String sessionId, String otherSessionId) {
        return !isEqual(sessionId, otherSessionId);
    }

    private static boolean isEqual(String sessionId, String otherSessionId) {
        if (sessionId == null) {
            if (otherSessionId != null) {
                return false;
            }
        } else if (!sessionId.equals(otherSessionId)) {
            return false;
        }
        return true;
    }

    @Override
    public void setSessionAttributes(String sessionId, SessionAttributes attrs) throws OXException {
        // Change attributes in local session
        RedisSessiondState state = getState();
        SessionImpl session = state.getLocalSessionCache().getSessionByIdIfPresent(sessionId);
        if (session != null) {
            if (attrs.getLocalIp().isSet()) {
                session.setLocalIp(attrs.getLocalIp().get(), false);
            }
            if (attrs.getClient().isSet()) {
                session.setClient(attrs.getClient().get(), false);
            }
            if (attrs.getHash().isSet()) {
                session.setHash(attrs.getHash().get(), false);
            }
            if (attrs.getUserAgent().isSet()) {
                session.setParameter(Session.PARAM_USER_AGENT, attrs.getUserAgent().get());
            }
        }

        // Check for session Redis storage
        ClusterSession obfuscatedSession = connector.executeOperation(commandsProvider -> {
            Session ses = getSessionCommandsWithoutObfuscator(commandsProvider).get(getSessionKey(sessionId));
            return (ses instanceof ClusterSession cs) ? cs : null;
        });

        if (obfuscatedSession != null) {
            // Change attributes in session instance & serialize to bytes
            ClusterSession newSession = obfuscatedSession;
            if (attrs.getLocalIp().isSet()) {
                newSession.setLocalIp(attrs.getLocalIp().get());
            }
            if (attrs.getClient().isSet()) {
                newSession.setClient(attrs.getClient().get());
            }
            if (attrs.getHash().isSet()) {
                newSession.setHash(attrs.getHash().get());
            }
            if (attrs.getUserAgent().isSet()) {
                newSession.setParameter(Session.PARAM_USER_AGENT, attrs.getUserAgent().get());
            }
            byte[] sessionBytes = generateBytesFromSession(newSession, null);

            // Change session attributes in Redis storage
            connector.executeVoidOperation(commandsProvider -> {
                // Put into Redis storage
                commandsProvider.getRawStringCommands().set(getSessionKey(sessionId), Streams.newByteArrayInputStream(sessionBytes));

                // Refresh session's expiration
                RedisSortedSetCommands<String, String> sortedSetCommands = commandsProvider.getSortedSetCommands();
                long now = System.currentTimeMillis();
                sortedSetCommands.zadd(getSortSetKeyForSession(newSession), now, newSession.getSessionID());
                String brandId = getBrandIdentifierFrom(newSession);
                if (brandId != null) {
                    sortedSetCommands.zadd(getSetKeyForBrand(brandId), now, newSession.getSessionID());
                }
            });
        }

        // Publish INVALIDATE event for changed session manually
        publishInvalidateEventQuietly(Collections.singletonList(sessionId));
    }

    @Override
    public boolean removeSession(String sessionId, RemovalReason removalReason) {
        return !removeSessions(Collections.singletonList(sessionId), removalReason).isEmpty();
    }

    /**
     * Removes the sessions associated with given identifiers.
     *
     * @param sessionIds The session identifiers
     * @param removalReason The reason why the sessions are removed
     * @return The number of actually removed sessions
     */
    public List<Session> removeSessions(Collection<String> sessionIds, RemovalReason removalReason) {
        return removeSessions(sessionIds, true, true, removalReason);
    }

    /**
     * Removes the sessions associated with given identifiers.
     *
     * @param sessionIdentifiers The identifiers of the sessions to remove
     * @param removeFromSortedSet Whether to remove from sorted set as well
     * @param replayToRemote Whether to replay Redis operation to remote sites
     * @param removalReason The reason why the sessions are removed
     * @return The number of actually removed sessions
     */
    public List<Session> removeSessions(Collection<String> sessionIdentifiers, boolean removeFromSortedSet, boolean replayToRemote, RemovalReason removalReason) {
        if (sessionIdentifiers == null || sessionIdentifiers.isEmpty()) {
            return Collections.emptyList();
        }

        Collection<String> sessionIds = filterNullElementsIfAny(sessionIdentifiers);
        if (sessionIds.isEmpty()) {
            // Apparently, collection of session identifiers only consists of null elements. Nothing to do...
            return Collections.emptyList();
        }

        LOG.debug("Removing session(s) {}", sessionIds);

        Map<String, Session> locallyRemovedSessions;
        { // NOSONARLINT
            List<SessionImpl> removedSessions = getState().getLocalSessionCache().removeAndGetSessionsByIds(sessionIds);
            postSessionOrContainerRemoval(removedSessions, removalReason, false);
            int numLocallyRemovedSessions = removedSessions.size();
            locallyRemovedSessions = numLocallyRemovedSessions > 0 ? removedSessions.stream().collect(CollectorUtils.toMap(Session::getSessionID, s -> s, numLocallyRemovedSessions)) : Collections.emptyMap();
        }

        try {
            List<Session> removedFromRedis = connector.<List<Session>> executeOperation(commandsProvider -> {
                RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator = getSessionCommandsWithoutObfuscator(commandsProvider, versionMismatchHandler);
                RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();

                List<Session> removedSessions = null;
                for (String sessionId : sessionIds) {
                    Session removedSession = locallyRemovedSessions.get(sessionId);
                    if (removedSession == null) {
                        // No such session removed from local cache. GETDEL from Redis storage then...
                        removedSession = sessionCommandsWithoutObfuscator.getdel(getSessionKey(sessionId));
                        if (removedSession == null) {
                            LOG.debug("No such session {} in Redis storage. Hence, nothing to remove.", sessionId); // NOSONARLINT
                        } else {
                            removedSessions = addWithCreateIfNull(removedSession, removedSessions, sessionIds.size());
                        }
                    } else {
                        if (keyCommands.del(getSessionKey(sessionId)).longValue() <= 0) {
                            LOG.debug("No such session {} in Redis storage. Hence, nothing to remove.", sessionId);
                        } else {
                            removedSessions = addWithCreateIfNull(removedSession, removedSessions, sessionIds.size());
                        }
                    }
                }

                if (removedSessions == null) {
                    return Collections.emptyList();
                }

                RemovalCollection removalCollection = new RemovalCollection();
                for (Session removedSession : removedSessions) {
                    removalCollection.addSession(removedSession, true, removeFromSortedSet);
                }
                removalCollection.removeCollected(commandsProvider);

                LOG.debug("Removed session(s) {} from Redis storage", sessionIds);

                return removedSessions;
            });

            int numRemoved = removedFromRedis.size();
            if (numRemoved > 0) {
                for (Session session : removedFromRedis) {
                    if (session instanceof ClusterSession cs) {
                        unobfuscatePasswordOf(cs, getState().getObfuscator());
                    }
                }
                if (numRemoved == 1) {
                    decrementCountersFor(removedFromRedis.get(0));
                } else {
                    decrementCountersFor(removedFromRedis);
                }
            }

            if (replayToRemote) {
                List<RedisConnectorProvider> remoteConnectorProviders = getRemoteConnectorProviders();
                if (!remoteConnectorProviders.isEmpty()) {
                    ExceptionCatchingRunnable task = () -> {
                        VersionMismatchHandler versionMismatchHandler = DO_NOTHING_VERSION_MISMATCH_HANDLER;
                        RedisVoidOperation removeSessionsOperation = commandsProvider -> {
                            RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator = getSessionCommandsWithoutObfuscator(commandsProvider, versionMismatchHandler);
                            RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();

                            List<Session> removedSessions = null;
                            for (String sessionId : sessionIds) {
                                Session removedSession = locallyRemovedSessions.get(sessionId);
                                if (removedSession == null) {
                                    // No such session removed from local cache. GETDEL from Redis storage then...
                                    removedSession = sessionCommandsWithoutObfuscator.getdel(getSessionKey(sessionId));
                                    if (removedSession == null) {
                                        LOG.debug("No such session {} in remote Redis storage. Hence, nothing to remove.", sessionId); // NOSONARLINT
                                    } else {
                                        removedSessions = addWithCreateIfNull(removedSession, removedSessions, sessionIds.size());
                                    }
                                } else {
                                    if (keyCommands.del(getSessionKey(sessionId)).longValue() <= 0) {
                                        LOG.debug("No such session {} in remote Redis storage. Hence, nothing to remove.", sessionId);
                                    } else {
                                        removedSessions = addWithCreateIfNull(removedSession, removedSessions, sessionIds.size());
                                    }
                                }
                            }

                            if (removedSessions == null) {
                                return;
                            }

                            RemovalCollection removalCollection = new RemovalCollection();
                            for (Session removedSession : removedSessions) {
                                removalCollection.addSession(removedSession, true, removeFromSortedSet);
                            }
                            removalCollection.removeCollected(commandsProvider);

                            LOG.debug("Removed session(s) {} from remote Redis storage", sessionIds);
                        };
                        for (RedisConnectorProvider connectorProvider : remoteConnectorProviders) {
                            RedisConnector remoteConnector = connectorProvider.getConnector();
                            try {
                                remoteConnector.executeVoidOperation(removeSessionsOperation);
                            } catch (Exception e) {
                                LOG.error("Failed to remove sessions from remote Redis storage {}", remoteConnector, e); // NOSONARLINT
                            }
                        }
                    };
                    ThreadPools.submitElseExecute(ThreadPools.task(task));
                }
            }

            // Issue event
            publishInvalidateEventQuietly(Stream.concat(locallyRemovedSessions.keySet().stream(), removedFromRedis.stream().map(Session::getSessionID)).collect(Collectors.toSet()));

            return removedFromRedis;
        } catch (Exception e) {
            LOG.warn("Failed to remove sessions from Redis session storage", e);
            return Collections.emptyList();
        }
    }

    @Override
    public int removeUserSessions(int userId, int contextId, RemovalReason removalReason) {
        try {
            return removeAndReturnUserSessions(userId, contextId, removalReason, true).size();
        } catch (OXException e) {
            LOG.warn("Failed to remove sessions for user {} in context {} from Redis session storage", I(userId), I(contextId), e);
            return 0;
        }
    }

    /**
     * Removes the sessions associated with given user from local cache storage.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param removalReason The reason why the sessions are removed
     */
    public void removeLocalUserSessions(int userId, int contextId, RemovalReason removalReason) {
        List<SessionImpl> removedSessions = getState().getLocalSessionCache().removeAndGetSessionsByUser(userId, contextId);
        postSessionOrContainerRemoval(removedSessions, removalReason, false);
        LOG.debug("Removed sessions from local cache for user {} in context {}", I(userId), I(contextId));
    }

    /**
     * Removes the sessions associated with given user from Redis session storage.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param removalReason The reason why the sessions are removed
     * @param replayToRemote Whether to replay Redis operations to remote ones
     * @return The removed sessions
     * @throws OXException If a Redis error occurs
     */
    public List<Session> removeAndReturnUserSessions(int userId, int contextId, RemovalReason removalReason, boolean replayToRemote) throws OXException {
        LOG.debug("Removing sessions for user {} in context {}", I(userId), I(contextId));

        Map<String, Session> locallyRemovedSessions;
        { // NOSONARLINT
            List<SessionImpl> removedSessions = getState().getLocalSessionCache().removeAndGetSessionsByUser(userId, contextId);
            postSessionOrContainerRemoval(removedSessions, removalReason, false);
            LOG.debug("Removed sessions from local cache for user {} in context {}", I(userId), I(contextId));
            int numLocallyRemovedSessions = removedSessions.size();
            locallyRemovedSessions = numLocallyRemovedSessions > 0 ? removedSessions.stream().collect(CollectorUtils.toMap(Session::getSessionID, s -> s, numLocallyRemovedSessions)) : Collections.emptyMap();
        }

        List<Session> removedFromRedis = connector.executeOperation(commandsProvider -> {
            Set<String> sessionIds = commandsProvider.getSetCommands().smembers(getSetKeyForUser(userId, contextId));
            if (sessionIds == null || sessionIds.isEmpty()) {
                LOG.debug("No such sessions in Redis storage for user {} in context {}", I(userId), I(contextId));
                return Collections.emptyList();
            }

            RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator = getSessionCommandsWithoutObfuscator(commandsProvider);
            RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();

            List<Session> removedSessions = null;
            RemovalCollection removalCollection = new RemovalCollection();
            for (String sessionId : sessionIds) {
                Session removedSession = locallyRemovedSessions.get(sessionId);
                if (removedSession == null) {
                    // No such session removed from local cache. GETDEL from Redis storage then...
                    removedSession = sessionCommandsWithoutObfuscator.getdel(getSessionKey(sessionId));
                    if (removedSession == null) {
                        LOG.debug("No such session {} in Redis storage. Hence, nothing to remove.", sessionId);
                    } else {
                        LOG.debug("Removed session {} from Redis storage for user {} in context {}", sessionId, I(removedSession.getUserId()), I(removedSession.getContextId())); // NOSONARLINT
                        removalCollection.addSessionAssociates(removedSession, false, true);
                        removedSessions = addWithCreateIfNull(removedSession, removedSessions, sessionIds.size());
                    }
                } else {
                    if (keyCommands.del(getSessionKey(sessionId)).longValue() <= 0) {
                        LOG.debug("No such session {} in Redis storage. Hence, nothing to remove.", sessionId);
                    } else {
                        LOG.debug("Removed session {} from Redis storage for user {} in context {}", sessionId, I(removedSession.getUserId()), I(removedSession.getContextId()));
                        removalCollection.addSessionAssociates(removedSession, false, true);
                        removedSessions = addWithCreateIfNull(removedSession, removedSessions, sessionIds.size());
                    }
                }
            }
            removalCollection.addKey(getSetKeyForUser(userId, contextId));
            removalCollection.removeCollected(commandsProvider);
            LOG.debug("Removed sessions from Redis storage for user {} in context {}", I(userId), I(contextId));

            if (removedSessions == null) {
                return Collections.emptyList();
            }

            decrementCountersFor(removedSessions);
            return removedSessions;
        });

        for (Session session : removedFromRedis) {
            if (session instanceof ClusterSession cs) {
                unobfuscatePasswordOf(cs, getState().getObfuscator());
            }
        }

        if (replayToRemote) {
            List<RedisConnectorProvider> remoteConnectorProviders = getRemoteConnectorProviders();
            if (!remoteConnectorProviders.isEmpty()) {
                ExceptionCatchingRunnable task = () -> {
                    VersionMismatchHandler versionMismatchHandler = DO_NOTHING_VERSION_MISMATCH_HANDLER;
                    RedisVoidOperation removeSessionsOperation = commandsProvider -> {
                        RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();

                        Set<String> sessionIds = setCommands.smembers(getSetKeyForUser(userId, contextId));
                        if (sessionIds == null || sessionIds.isEmpty()) {
                            LOG.debug("No such sessions in remote Redis storage for user {} in context {}", I(userId), I(contextId));
                            return;
                        }

                        RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator = getSessionCommandsWithoutObfuscator(commandsProvider, versionMismatchHandler);
                        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
                        RemovalCollection removalCollection = new RemovalCollection();
                        for (String sessionId : sessionIds) {
                            Session removedSession = locallyRemovedSessions.get(sessionId);
                            if (removedSession == null) {
                                // No such session removed from local cache. GETDEL from Redis storage then...
                                removedSession = sessionCommandsWithoutObfuscator.getdel(getSessionKey(sessionId));
                                if (removedSession == null) {
                                    LOG.debug("No such session {} in remote Redis storage. Hence, nothing to remove.", sessionId);
                                } else {
                                    LOG.debug("Removed session {} from remote Redis storage for user {} in context {}", sessionId, I(removedSession.getUserId()), I(removedSession.getContextId()));
                                    removalCollection.addSessionAssociates(removedSession, false, true);
                                }
                            } else {
                                if (keyCommands.del(getSessionKey(sessionId)).longValue() <= 0) {
                                    LOG.debug("No such session {} in remote Redis storage. Hence, nothing to remove.", sessionId);
                                } else {
                                    LOG.debug("Removed session {} from remote Redis storage for user {} in context {}", sessionId, I(removedSession.getUserId()), I(removedSession.getContextId()));
                                    removalCollection.addSessionAssociates(removedSession, false, true);
                                }
                            }
                        }
                        removalCollection.addKey(getSetKeyForUser(userId, contextId));
                        removalCollection.removeCollected(commandsProvider);
                        LOG.debug("Removed sessions from remote Redis storage for user {} in context {}", I(userId), I(contextId));
                    };
                    for (RedisConnectorProvider connectorProvider : remoteConnectorProviders) {
                        RedisConnector remoteConnector = connectorProvider.getConnector();
                        try {
                            remoteConnector.executeVoidOperation(removeSessionsOperation);
                        } catch (Exception e) {
                            LOG.error("Failed to remove sessions from remote Redis storage {} for user {} in context {}", remoteConnector, I(userId), I(contextId), e);
                        }
                    }
                };

                ThreadPools.submitElseExecute(ThreadPools.task(task));
            }
        }

        // Issue event
        publishInvalidateEventQuietly(Stream.concat(locallyRemovedSessions.keySet().stream(), removedFromRedis.stream().map(Session::getSessionID)).collect(Collectors.toSet()));

        return removedFromRedis;
    }

    @Override
    public boolean hasForContext(int contextId) {
        if (getState().getLocalSessionCache().hasForContext(contextId)) {
            return true;
        }

        ScanArgs scanArgs = ScanArgs.Builder.matches(getSetKeyPattern(contextId)).limit(RedisSessionConstants.LIMIT_1000);
        try {
            return connector.executeOperation(commandsProvider -> {
                KeyScanCursor<String> cursor = commandsProvider.getKeyCommands().scan(scanArgs);
                while (cursor != null) {
                    // Check current keys
                    if (!cursor.getKeys().isEmpty()) {
                        return Boolean.TRUE;
                    }

                    // Move cursor forward
                    cursor = cursor.isFinished() ? null : commandsProvider.getKeyCommands().scan(cursor, scanArgs);
                }
                return Boolean.FALSE;
            }).booleanValue();
        } catch (Exception e) {
            LOG.warn("Failed to check sessions for context {} from Redis session storage", I(contextId), e);
            return false;
        }
    }

    @Override
    public void removeContextSessions(Set<Integer> contextIds, RemovalReason removalReason) throws OXException {
        removeContextSessionsGlobalAndReturnIds(contextIds, removalReason);
    }

    /**
     * Removes all sessions belonging to specified context identifiers from Redis storage.
     *
     * @param contextIdentifiers The context identifiers
     * @param removalReason The reason why the sessions are removed
     * @return The identifiers of the removed sessions
     * @throws OXException If removal fails
     */
    public Collection<String> removeContextSessionsGlobalAndReturnIds(Set<Integer> contextIdentifiers, RemovalReason removalReason) throws OXException {
        if (contextIdentifiers == null || contextIdentifiers.isEmpty()) {
            return Collections.emptyList();
        }

        Collection<Integer> contextIds = filterNullElementsIfAny(contextIdentifiers);
        if (contextIds.isEmpty()) {
            return Collections.emptyList();
        }

        LOG.debug("Removing sessions for contexts {}", contextIds);

        // Drop local sessions for given context identifiers
        Map<String, Session> locallyRemovedSessions;
        { // NOSONARLINT
            List<SessionImpl> removedSessions = getState().getLocalSessionCache().removeAndGetSessionsByContexts(contextIds);
            postSessionOrContainerRemoval(removedSessions, removalReason, false);
            LOG.debug("Removed sessions from local cache for contexts {}", contextIds);
            int numLocallyRemovedSessions = removedSessions.size();
            locallyRemovedSessions = numLocallyRemovedSessions > 0 ? removedSessions.stream().collect(CollectorUtils.toMap(Session::getSessionID, s -> s, numLocallyRemovedSessions)) : Collections.emptyMap();
        }

        // Drop sessions from Redis
        UserService userService = services.getServiceSafe(UserService.class);
        Map<String, Session> removedFromRedisMap = null;
        for (Integer contextId : contextIds) {
            try {
                int[] userIds = userService.getUserIds(contextId.intValue());
                for (int userId : userIds) {
                    List<Session> removedFromRedis = removeAndReturnUserSessions(userId, contextId.intValue(), removalReason, true);
                    if (removedFromRedisMap == null) {
                        removedFromRedisMap = HashMap.newHashMap(removedFromRedis.size());
                    }
                    for (Session s : removedFromRedis) {
                        removedFromRedisMap.put(s.getSessionID(), s);
                    }
                }
            } catch (Exception e) {
                LOG.warn("Failed to remove sessions for context {} from Redis session storage", contextId, e);
            }
        }

        // Issue event
        Set<String> idsOfRemovedSessions = Stream.concat(locallyRemovedSessions.keySet().stream(), removedFromRedisMap == null ? Stream.empty() : removedFromRedisMap.keySet().stream()).collect(Collectors.toSet());
        publishInvalidateEventQuietly(idsOfRemovedSessions);
        return idsOfRemovedSessions;
    }

    @Override
    public void removeContextSessions(final int contextId, RemovalReason removalReason) {
        LOG.debug("Removing sessions for context {}", I(contextId));

        // Drop local sessions for given context identifier
        Map<String, Session> locallyRemovedSessions;
        { // NOSONARLINT
            List<SessionImpl> removedSessions = getState().getLocalSessionCache().removeAndGetSessionsByContext(contextId);
            postSessionOrContainerRemoval(removedSessions, removalReason, false);
            LOG.debug("Removed sessions from local cache for context {}", I(contextId));
            int numLocallyRemovedSessions = removedSessions.size();
            locallyRemovedSessions = numLocallyRemovedSessions > 0 ? removedSessions.stream().collect(CollectorUtils.toMap(Session::getSessionID, s -> s, numLocallyRemovedSessions)) : Collections.emptyMap();
        }

        // Drop sessions from Redis
        Map<String, Session> removedFromRedisMap = null;
        try {
            UserService userService = services.getServiceSafe(UserService.class);
            int[] userIds = userService.getUserIds(contextId);
            for (int userId : userIds) {
                List<Session> removedFromRedis = removeAndReturnUserSessions(userId, contextId, removalReason, true);
                if (removedFromRedisMap == null) {
                    removedFromRedisMap = HashMap.newHashMap(removedFromRedis.size());
                }
                for (Session s : removedFromRedis) {
                    removedFromRedisMap.put(s.getSessionID(), s);
                }
            }
            LOG.debug("Removed sessions from Redis storage for context {}", I(contextId));
        } catch (Exception e) {
            LOG.warn("Failed to remove sessions for context {} from Redis session storage", I(contextId), e);
        }

        // Issue event
        publishInvalidateEventQuietly(Stream.concat(locallyRemovedSessions.keySet().stream(), removedFromRedisMap == null ? Stream.empty() : removedFromRedisMap.keySet().stream()).collect(Collectors.toSet()));
    }

    @Override
    public void removeAllSessions(RemovalReason removalReason) {
        LOG.debug("Removing all sessions");

        // Drop all local sessions
        Map<String, Session> locallyRemovedSessions;
        { // NOSONARLINT
            List<SessionImpl> removedSessions = getState().getLocalSessionCache().invalidateAndGetAll(false);
            if (removedSessions.size() == 1) {
                postSessionRemoval(removedSessions.get(0), removalReason, false);
            } else {
                postContainerRemoval(removedSessions, removalReason, false);
            }
            int numLocallyRemovedSessions = removedSessions.size();
            locallyRemovedSessions = numLocallyRemovedSessions > 0 ? removedSessions.stream().collect(CollectorUtils.toMap(Session::getSessionID, s -> s, numLocallyRemovedSessions)) : Collections.emptyMap();
        }

        // Drop sessions from Redis
        List<Session> removedFromRedis = null;
        try {
            removedFromRedis = removeSessionsBy(locallyRemovedSessions, ScanArgs.Builder.matches(new StringBuilder(RedisSessionConstants.REDIS_SET_SESSIONIDS).append(RedisSessionConstants.DELIMITER).append('*').toString()).limit(RedisSessionConstants.LIMIT_1000), ScanArgs.Builder.matches(getAllSessionsPattern()).limit(RedisSessionConstants.LIMIT_1000), true);
            LOG.debug("Removed all sessions from Redis storage");
        } catch (Exception e) {
            LOG.warn("Failed to remove all sessions from Redis session storage", e);
        }

        // Issue event
        publishInvalidateEventQuietly(Stream.concat(locallyRemovedSessions.keySet().stream(), removedFromRedis == null ? Stream.empty() : removedFromRedis.stream().map(Session::getSessionID)).collect(Collectors.toSet()));
    }

    private List<Session> removeSessionsBy(Map<String, Session> optLocallyRemovedSessions, ScanArgs sessionSetArgs, ScanArgs optSessionArgs, boolean replayToRemote) throws OXException {
        List<Session> removedFromRedis = connector.executeOperation(commandsProvider -> {
            RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator = getSessionCommandsWithoutObfuscator(commandsProvider);

            List<Session> removedSessions = removeSessionsFromRedis(optLocallyRemovedSessions, sessionSetArgs, optSessionArgs, sessionCommandsWithoutObfuscator, commandsProvider);
            if (removedSessions != null) {
                decrementCountersFor(removedSessions);
            }
            return removedSessions;
        });

        if (removedFromRedis != null) {
            for (Session session : removedFromRedis) {
                if (session instanceof ClusterSession cs) {
                    unobfuscatePasswordOf(cs, getState().getObfuscator());
                }
            }
        }

        if (replayToRemote) {
            List<RedisConnectorProvider> remoteConnectorProviders = getRemoteConnectorProviders();
            if (!remoteConnectorProviders.isEmpty()) {
                ExceptionCatchingRunnable task = () -> {
                    VersionMismatchHandler versionMismatchHandler = DO_NOTHING_VERSION_MISMATCH_HANDLER;
                    RedisVoidOperation removeOperation = commandsProvider -> {
                        RedisStringCommands<String, Session> sessionCommands = getSessionCommandsWithoutObfuscator(commandsProvider, versionMismatchHandler);
                        removeSessionsFromRedis(optLocallyRemovedSessions, sessionSetArgs, optSessionArgs, sessionCommands, commandsProvider);
                    };
                    for (RedisConnectorProvider connectorProvider : remoteConnectorProviders) {
                        RedisConnector remoteConnector = connectorProvider.getConnector();
                        try {
                            remoteConnector.executeVoidOperation(removeOperation);
                        } catch (Exception e) {
                            LOG.error("Failed to remove sessions from remote Redis storage {}", remoteConnector, e);
                        }
                    }
                };

                ThreadPools.submitElseExecute(ThreadPools.task(task));
            }
        }

        return removedFromRedis;
    }

    private List<Session> removeSessionsFromRedis(Map<String, Session> optLocallyRemovedSessions, ScanArgs sessionSetArgs, ScanArgs optSessionArgs, RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator, RedisCommandsProvider commandsProvider) {
        RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();
        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();

        List<Session> removedSessions = null;
        RemovalCollection removalCollection = new RemovalCollection();
        KeyScanCursor<String> cursor = keyCommands.scan(sessionSetArgs);
        while (cursor != null) {
            // Obtain current keys...
            List<String> keys = cursor.getKeys();
            int numberOfKeys = keys.size();
            if (numberOfKeys > 0) {
                removalCollection.addKeys(keys);
                // Iterate set identifier
                for (String setKey : keys) {
                    // Get set's members (list of session identifiers)
                    Set<String> sessionIds = setCommands.smembers(setKey);
                    if (sessionIds != null && !sessionIds.isEmpty()) {
                        for (String sessionId : sessionIds) {
                            Session removedSession = optLocallyRemovedSessions == null ? null : optLocallyRemovedSessions.get(sessionId);
                            if (removedSession == null) {
                                // No such session removed from local cache. GETDEL from Redis storage then...
                                removedSession = sessionCommandsWithoutObfuscator.getdel(getSessionKey(sessionId));
                                if (removedSession != null) {
                                    LOG.debug("Removed session {} from Redis storage for user {} in context {}", sessionId, I(removedSession.getUserId()), I(removedSession.getContextId()));
                                    removalCollection.addSessionAssociates(removedSession, false, true);
                                    removedSessions = addWithCreateIfNull(removedSession, removedSessions, 0);
                                }
                            } else {
                                if (keyCommands.del(getSessionKey(sessionId)).longValue() > 0) {
                                    LOG.debug("Removed session {} from Redis storage for user {} in context {}", sessionId, I(removedSession.getUserId()), I(removedSession.getContextId()));
                                    removalCollection.addSessionAssociates(removedSession, false, true);
                                    removedSessions = addWithCreateIfNull(removedSession, removedSessions, 0);
                                }
                            }
                        }
                    }
                }
                keyCommands.del(keys.toArray(new String[numberOfKeys]));
            }

            // Move cursor forward
            cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, sessionSetArgs);
        }
        removalCollection.removeCollected(commandsProvider);

        // Check for fall-back SCAN argument for session keys
        if (optSessionArgs != null) {
            ScanArgs sessionArgs = optSessionArgs;
            List<String> sessionKeys = null;
            cursor = keyCommands.scan(sessionArgs);
            while (cursor != null) {
                // Obtain current keys...
                List<String> keys = cursor.getKeys();
                if (!keys.isEmpty()) {
                    // Add session identifiers
                    sessionKeys = addAllWithCreateIfNull(keys, sessionKeys);
                }

                // Move cursor forward
                cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, sessionArgs);
            }

            if (sessionKeys != null) {
                removalCollection.reset();
                for (String sessionKey : sessionKeys) {
                    Session removedSession = optLocallyRemovedSessions == null ? null : optLocallyRemovedSessions.get(getSessionIdFrom(sessionKey));
                    if (removedSession == null) {
                        // No such session removed from local cache. GETDEL from Redis storage then...
                        removedSession = sessionCommandsWithoutObfuscator.getdel(sessionKey);
                        if (removedSession != null) {
                            LOG.debug("Removed session {} from Redis storage for user {} in context {}", removedSession.getSessionID(), I(removedSession.getUserId()), I(removedSession.getContextId()));
                            removalCollection.addSession(removedSession, true, true);
                            removedSessions = addWithCreateIfNull(removedSession, removedSessions, 0);
                        }
                    } else {
                        if (keyCommands.del(sessionKey).longValue() > 0) {
                            LOG.debug("Removed session {} from Redis storage for user {} in context {}", removedSession.getSessionID(), I(removedSession.getUserId()), I(removedSession.getContextId()));
                            removalCollection.addSession(removedSession, true, true);
                            removedSessions = addWithCreateIfNull(removedSession, removedSessions, 0);
                        }
                    }
                }
                removalCollection.removeCollected(commandsProvider);
            }
        }
        return removedSessions;
    }

    @Override
    public long getLastActive(String sessionId) throws OXException {
        RedisSessiondState state = getState();
        LocalSessionCache localSessionCache = state.getLocalSessionCache();

        // First, check local cache
        SessionImpl localSession = localSessionCache.getSessionByIdIfPresent(sessionId);
        if (localSession != null) {
            Object oNumber = localSession.getParameter(Session.PARAM_LOCAL_LAST_ACTIVE);
            if (null != oNumber) {
                if (oNumber instanceof Number) {
                    return ((Number) oNumber).longValue();
                }

                try {
                    return Long.parseLong(oNumber.toString());
                } catch (@SuppressWarnings("unused") NumberFormatException e) {
                    // Cannot be parsed to a long value
                    return 0L;
                }
            }
        }

        // Then, check Redis storage
        Long localLastActive = connector.executeOperation(commandsProvider -> {
            Session fetchedFromRedis = getSessionCommandsWithoutObfuscator(commandsProvider).get(getSessionKey(sessionId));
            if (fetchedFromRedis == null) {
                // No such session in Redis storage
                return null;
            }

            RedisSortedSetCommands<String, String> sortedSetCommands = commandsProvider.getSortedSetCommands();
            List<Double> scores = sortedSetCommands.zmscore(getSortSetKeyForSession(fetchedFromRedis), fetchedFromRedis.getSessionID());
            if (!scores.isEmpty()) {
                return Long.valueOf(scores.get(0).longValue());
            }

            // Add to sorted set since obviously absent
            long currentTime = System.currentTimeMillis();
            String brandId = getBrandIdentifierFrom(fetchedFromRedis);
            resetSessionsExpireTime(fetchedFromRedis, brandId, currentTime, sortedSetCommands);
            return Long.valueOf(currentTime);
        });
        if (localLastActive == null) {
            // No such session
            throw SessionExceptionCodes.SESSION_EXPIRED.create(sessionId);
        }
        return localLastActive.longValue();
    }

    @Override
    public Session getSession(String sessionId) {
        return getSession(sessionId, true);
    }

    @Override
    public Session getSession(String sessionId, boolean considerLocalStorage) {
        if (isInvalidIdentifier(sessionId)) {
            // There can be no session with such an identifier
            LOG.debug("Invalid session identifier: {}", sessionId);
            return null;
        }
        return doGetSession(newSessionId(sessionId), false, considerLocalStorage);
    }

    @Override
    public Session peekSession(String sessionId) {
        if (isInvalidIdentifier(sessionId)) {
            // There can be no session with such an identifier
            LOG.debug("Invalid session identifier: {}", sessionId);
            return null;
        }
        return doGetSession(newSessionId(sessionId), true, true);
    }

    @Override
    public Session peekSessionByAlternativeId(String altId) {
        if (isInvalidIdentifier(altId)) {
            // There can be no session with such an identifier
            LOG.debug("Invalid alternative session identifier: {}", altId);
            return null;
        }
        return doGetSession(newAlternativeSessionId(altId), true, true);
    }

    @Override
    public Session getSessionByAlternativeId(String altId) {
        if (isInvalidIdentifier(altId)) {
            // There can be no session with such an identifier
            LOG.debug("Invalid alternative session identifier: {}", altId);
            return null;
        }
        return doGetSession(newAlternativeSessionId(altId), false, true);
    }

    private static boolean isInvalidIdentifier(String identifier) {
        // It is known that session identifiers are spawned from c.o.java.util.UUIDs.getUnformattedString(UUID)
        // Therefore it is required to have lower-case hex digits only no matter if regular or alternative session identifier
        return Strings.isEmpty(identifier) || !Strings.isHex(identifier.charAt(0), false);
    }

    private Session doGetSession(SessionId sessionId, boolean peek, boolean considerLocalStorage) {
        LOG.debug("Getting session by {}", sessionId);
        RedisSessiondState state = getState();
        LocalSessionCache localSessionCache = state.getLocalSessionCache();

        // Check local cache first
        SessionImpl session;
        if (considerLocalStorage) {
            LOG.debug("Look-up in local cache for {}", sessionId);
            session = localSessionCache.getSessionByIdIfPresent(sessionId);
            if (session != null) {
                // Ensure existence if locally fetched
                LOG.debug("Obtained session {} from local cache for {}", session.getSessionID(), sessionId);
                return ensureExistenceElseNull(session, peek, state);
            }
        }

        // Need to look-up in Redis session storage while optionally holding appropriate lock to avoid concurrent session look-up
        AccessControl accessControl = null;
        boolean acquired = false;
        try {
            boolean lookUpCacheAgain = false;
            if (state.isTryLockBeforeRedisLookUp()) {
                LOG.debug("Try lock before Redis look-up is enabled. Thus, try lock before Redis look-up for {}", sessionId);
                LockService optLockService = Services.optService(LockService.class);
                if (optLockService != null) {
                    accessControl = optLockService.getAccessControlFor(sessionId.getIdentifier(), 1, 1, 1);
                    if (accessControl.tryAcquireGrant()) {
                        LOG.debug("Immediately acquired lock for {}", sessionId);
                        acquired = true;
                    } else {
                        accessControl.acquireGrant();
                        acquired = true;
                        lookUpCacheAgain = true;
                        LOG.debug("Waited for acquiring lock for {}", sessionId);
                    }
                } else {
                    LOG.debug("No lock service available. Hence, no try lock before Redis look-up for {}", sessionId);
                }
            } else {
                LOG.debug("Try lock before Redis look-up is disabled. Hence, no try lock before Redis look-up for {}", sessionId);
            }

            // Check for previous "failed" attempt
            if (lookUpCacheAgain && accessControl != null && accessControl.valueSupported()) {
                ImmutableReference<Session> prev = ((ImmutableReference<Session>) accessControl.getValue());
                if (prev != null && prev.getValue() == null) {
                    // Previous attempt already found no such session
                    LOG.debug("Previous concurrent look-up attempt already found no such session for {}", sessionId);
                    return null;
                }
            }

            // Check local cache again
            if (considerLocalStorage && lookUpCacheAgain) {
                LOG.debug("Second look-up in local cache for {} since waited for lock acquisition", sessionId);
                session = localSessionCache.getSessionByIdIfPresent(sessionId);
                if (session != null) {
                    // Ensure existence if locally fetched
                    LOG.debug("Obtained session {} from local cache for {}", session.getSessionID(), sessionId);
                    return ensureExistenceElseNull(session, peek, state);
                }
            }

            // Fetch from Redis storage
            Session sessionFetchedFromRedis = getSessionFromRedisAndStoreLocally(sessionId, peek, state);
            if (accessControl != null && accessControl.valueSupported()) {
                accessControl.setValue(new ImmutableReference<Session>(sessionFetchedFromRedis));
            }
            return sessionFetchedFromRedis;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOG.warn("Interrupted while getting session by {} from Redis session storage", sessionId, e);
            return null;
        } catch (OXRuntimeException e) {
            throw e;
        } catch (OXException e) { // NOSONARLINT
            LOG.warn("Failed to get session by {} from Redis session storage", sessionId, e);
            throw new OXRuntimeException(e);
        } catch (Exception e) { // NOSONARLINT
            LOG.warn("Failed to get session by {} from Redis session storage", sessionId, e);
            throw new OXRuntimeException(OXException.general(new StringBuilder("Failed to get session by ").append(sessionId).append(" from Redis session storage").toString(), e));
        } finally {
            AccessControls.release(accessControl, acquired);
        }
    }

    private SessionImpl getSessionFromRedisAndStoreLocally(SessionId sessionId, boolean peek, RedisSessiondState state) throws OXException {
        // Fetch from Redis storage
        SessionImpl restoredSession = newSessionImplFor(getSessionFromRedis(sessionId, peek, state));
        if (restoredSession == null) {
            // No such session available in Redis session storage
            return null;
        }

        if (peek) {
            // Only peek session. Invoke interceptors
            for (SessionSerializationInterceptor interceptor : serializationInterceptors) {
                interceptor.deserialize(restoredSession);
            }
        } else {
            // Ensure session is loaded into  local cache (if not present)
            Loader loader = loaderFor(restoredSession);
            state.getLocalSessionCache().getSessionById(restoredSession.getSessionID(), loader);

            if (loader.isLoaded()) {
                LOG.debug("Put session {} into local cache", restoredSession.getSessionID());

                // Invoke interceptors
                for (SessionSerializationInterceptor interceptor : serializationInterceptors) {
                    interceptor.deserialize(restoredSession);
                }

                // Post event for restored session
                postSessionRestauration(restoredSession);
            }
        }

        LOG.debug("Returning session {} from Redis storage", restoredSession.getSessionID());
        return restoredSession;
    }

    /**
     * Gets referenced session from Redis storage.
     *
     * @param sessionId The session identifier
     * @param peek Whether session is peeked or not; if simply peeked its expiration time stamp is not reseted
     * @return The session fetched from Redis or <code>null</code>
     * @throws OXException If fetching session from Redis storage fails
     */
    public @Nullable Session getSessionFromRedis(SessionId sessionId, boolean peek) throws OXException {
        return getSessionFromRedis(sessionId, peek, getState());
    }

    /**
     * Gets referenced session from Redis storage.
     *
     * @param sessionId The session identifier
     * @param peek Whether session is peeked or not; if simply peeked its expiration time stamp is not reseted
     * @param state The current state
     * @return The session fetched from Redis or <code>null</code>
     * @throws OXException If fetching session from Redis storage fails
     */
    private Session getSessionFromRedis(SessionId sessionId, boolean peek, RedisSessiondState state) throws OXException {
        LOG.debug("Performing look-up for {} in Redis storage", sessionId);

        // Container variable for reset time stamp and brand identifier
        StampAndBrandIdReference optRefs = peek ? null : new StampAndBrandIdReference();

        // Look-up session in local Redis storage
        Session sessionFromRedis = connector.executeOperation(commandsProvider -> fetchObfuscatedAndResetSessionUsing(sessionId, !peek, optRefs, commandsProvider));

        // Session obtained from local Redis?
        if (sessionFromRedis == null) {
            LOG.debug("No such session for {} in local Redis storage", sessionId);

            // Probe remote sites for such a session
            Reference<Session> fromRemoteSite = getObfuscatedSessionFromRemoteSites(sessionId, optRefs);
            if (fromRemoteSite == null) {
                // No remote site look-up performed at all
                LOG.debug("No remote Redis storage look-up performed at all for {} since either not allowed or no remote Redis storage(s) available", sessionId);
                return null;
            }

            // Check remotely looked-up session
            sessionFromRedis = fromRemoteSite.getValue();
            if (sessionFromRedis == null) {
                LOG.debug("No such session for {}; neither in local nor in remote Redis storages", sessionId);
                return null;
            }

            // Un-Obfuscate password
            unobfuscatePasswordOf(sessionFromRedis, state.getObfuscator());

            // Add to local Redis storage
            LOG.debug("Obtained session {} from a remote Redis storage for {}{}{}", sessionFromRedis.getSessionID(), sessionId, Strings.getLineSeparator(), prettyPrinterFor(sessionFromRedis, state));
            putSessionIntoRedis(newSessionImplFor(sessionFromRedis), state, false);
        } else {
            // Un-Obfuscate password
            unobfuscatePasswordOf(sessionFromRedis, state.getObfuscator());
            LOG.debug("Obtained session {} from local Redis storage for {}{}{}", sessionFromRedis.getSessionID(), sessionId, Strings.getLineSeparator(), prettyPrinterFor(sessionFromRedis, state));
        }

        // Also consider remote ones to reset session's expiration time
        if (optRefs != null) {
            List<RedisConnectorProvider> remoteConnectorProviders = getRemoteConnectorProviders();
            if (!remoteConnectorProviders.isEmpty()) {
                long currentTime = optRefs.getStamp();
                String brandId = optRefs.getBrandId();
                Session toReset = sessionFromRedis;
                ExceptionCatchingRunnable task = () -> {
                    RedisVoidOperation resetOperation = commandsProvider -> resetSessionsExpireTime(toReset, brandId, currentTime, commandsProvider.getSortedSetCommands());
                    for (RedisConnectorProvider connectorProvider : remoteConnectorProviders) {
                        RedisConnector remoteConnector = connectorProvider.getConnector();
                        try {
                            remoteConnector.executeVoidOperation(resetOperation);
                        } catch (Exception e) {
                            LOG.error("Failed to reset expiration time for session {} on remote Redis storage {}", toReset.getSessionID(), remoteConnector, e);
                        }
                    }
                };

                ThreadPools.submitElseExecute(ThreadPools.task(task));
            }
        }
        return sessionFromRedis;
    }

    private Reference<Session> getObfuscatedSessionFromRemoteSites(SessionId sessionId, StampAndBrandIdReference optRefs) throws OXException {
        if (false == mayRemotelyLookUp(sessionId)) {
            return null;
        }

        List<RedisConnectorProvider> remoteConnectorProviders = getRemoteConnectorProviders();
        if (remoteConnectorProviders.isEmpty()) {
            return null;
        }

        RedisOperation<Session> fetchSessionOperation = commandsProvider -> fetchObfuscatedAndResetSessionUsing(sessionId, false, optRefs, commandsProvider);
        Session sessionFromRemoteSite = null;
        for (Iterator<RedisConnectorProvider> it = remoteConnectorProviders.iterator(); sessionFromRemoteSite == null && it.hasNext();) {
            RedisConnector remoteConnector = it.next().getConnector();
            try {
                sessionFromRemoteSite = remoteConnector.executeOperation(fetchSessionOperation);
            } catch (Exception e) {
                LOG.error("Failed to fetch session for {} from remote Redis storage {}", sessionId, remoteConnector, e);
            }
        }

        // Remember non-existent session identifier for 5 minutes
        if (sessionFromRemoteSite == null) {
            connector.executeVoidOperation(commandsProvider -> commandsProvider.getStringCommands().set(getSessionNonExistentKey(sessionId.toString()), "true", new SetArgs().px(300_000L)));
        }

        return new Reference<>(sessionFromRemoteSite);
    }

    /**
     * Checks if specified session identifier may be remotely looked-up.
     * <p>
     * <ul>
     * <li>Check if session identifier is already remembered as non-existent</li>
     * <li>Check against overall rate limit for remote site look-ups</li>
     * <li>Check against client-specific rate limit for remote site look-ups</li>
     * </ul>
     *
     * @param sessionId The session identifier
     * @return <code>true</code> if specified session identifier may be remotely looked-up; otherwise <code>false</code>
     * @throws OXException If check fails
     */
    private boolean mayRemotelyLookUp(SessionId sessionId) throws OXException {
        // Remembered as non-existent?
        if (connector.executeOperation(commandsProvider -> commandsProvider.getKeyCommands().exists(getSessionNonExistentKey(sessionId.toString()))).longValue() > 0) {
            // Non-existent
            LOG.debug("Remote look-up denied for {} since remembered as non-existent before", sessionId);
            return false;
        }

        // Check against overall rate limit for remote site look-ups
        BucketRegistry bucketRegistry = BucketRegistry.getInstance();
        Optional<Bucket> optOverallBucket = bucketRegistry.getOverallBucket();
        if (optOverallBucket.isPresent() && !optOverallBucket.get().tryConsume(1)) {
            // Overall bucket denies remote site look-up
            LOG.debug("Remote look-up denied for {} since too many overall remote session look-ups", sessionId);
            return false;
        }

        // Check against client-specific rate limit for remote site look-ups
        Optional<Bucket> optBucket = bucketRegistry.get();
        if (!optBucket.isEmpty() && !optBucket.get().tryConsume(1)) {
            LOG.debug("Remote look-up denied for {} since too many client-specific remote session look-ups", sessionId);
            return false;
        }
        return true;
    }

    /**
     * Fetches the <b>obfuscated</b> session from Redis storage for given identifier using specified commands provider.
     * <p>
     * Set <code>reset</code> boolean parameter to <code>true</code> in order to reset session's expiration (if exists).
     *
     * @param sessionId The session identifier to look-up
     * @param reset Whether to reset session's expiration time stamp or not
     * @param optRefs The references to reset time stamp and brand identifier or <code>null</code>
     * @param commandsProvider The commands provider to use
     * @return The <b>obfuscated</b> session or <code>null</code>
     */
    private Session fetchObfuscatedAndResetSessionUsing(SessionId sessionId, boolean reset, StampAndBrandIdReference optRefs, RedisCommandsProvider commandsProvider) {
        // Determine session identifier
        String sesId;
        if (sessionId.isAlternativeId()) {
            // Look-up of alternative identifier to session identifier association
            sesId = commandsProvider.getStringCommands().get(getSessionAlternativeKey(sessionId.getAlternativeIdentifier()));
            if (sesId == null) {
                // No such alternative identifier to session identifier association
                return null;
            }
        } else {
            sesId = sessionId.getIdentifier();
        }

        // Look-up of session in Redis storage by session identifier
        Session obfuscatedFetchedFromRedis = getSessionCommandsWithoutObfuscator(commandsProvider, versionMismatchHandler).get(getSessionKey(sesId));
        if (obfuscatedFetchedFromRedis != null) {
            // Session found
            if (reset) {
                // Reset session's expire time
                long currentTime = System.currentTimeMillis();
                String brandId = getBrandIdentifierFrom(obfuscatedFetchedFromRedis);
                resetSessionsExpireTime(obfuscatedFetchedFromRedis, brandId, currentTime, commandsProvider.getSortedSetCommands());
                if (optRefs != null) {
                    // Remember values for resetting session's expiration time
                    optRefs.setStamp(currentTime);
                    optRefs.setBrandId(brandId);
                }
            } else {
                // Grab values for resetting session's expiration time
                if (optRefs != null) {
                    optRefs.setStamp(System.currentTimeMillis());
                    optRefs.setBrandId(getBrandIdentifierFrom(obfuscatedFetchedFromRedis));
                }
            }

            // Return found session
            return obfuscatedFetchedFromRedis;
        }

        // No such session in Redis storage
        if (sessionId.isAlternativeId()) {
            // Delete alternative identifier to session identifier association
            commandsProvider.getKeyCommands().del(getSessionAlternativeKey(sessionId.getAlternativeIdentifier()));
        }
        return null;
    }

    /**
     * Resets specified session's expiry through re-adding to appropriate Redis sorted set:
     * <ul>
     * <li><code>"ox-sessionids-longlife"</code> for stay-signed-in sessions</li>
     * <li><code>"ox-sessionids-shortlife"</code> for volatile sessions</li>
     * </ul>
     *
     * @param session The session for which to reset expiry
     * @param brandId The optional brand identifier associated with given session; may be <code>null</code>
     * @param currentTime The current time to assume
     * @param sortedSetCommands The Redis sorted set commands to issue commands
     * @see RedisSessionConstants#REDIS_SORTEDSET_SESSIONIDS_LONG_LIFETIME
     * @see RedisSessionConstants#REDIS_SORTEDSET_SESSIONIDS_SHORT_LIFETIME
     */
    private static void resetSessionsExpireTime(Session session, String brandId, long currentTime, RedisSortedSetCommands<String, String> sortedSetCommands) {
        sortedSetCommands.zadd(getSortSetKeyForSession(session), currentTime, session.getSessionID());
        if (brandId != null) {
            sortedSetCommands.zadd(getSetKeyForBrand(brandId), currentTime, session.getSessionID());
        }
    }

    /**
     * Checks if given session does exist in Redis session storage; returns the session if it does otherwise <code>null</code>
     *
     * @param session The session to check existence for; must not be <code>null</code>
     * @param peek Whether to peek session (don't update its expiration) or not
     * @param state The current state
     * @return The session if existent; otherwise <code>null</code>
     * @throws OXRuntimeException If a connectivity issue occurs while trying to communicate with Redis end-point
     */
    private SessionImpl ensureExistenceElseNull(SessionImpl session, boolean peek, RedisSessiondState state) throws OXRuntimeException {
        if (!state.isEnsureExistenceOnLocalFetch()) {
            // No existence check enabled... Rely on session events only
            LOG.debug("No existence check enabled. Returning locally fetched session {} unchecked", session.getSessionID());
            return session;
        }

        try {
            // Check session existence in Redis session storage
            int checkExistenceThreshold = state.getCheckExistenceThreshold();
            if (checkExistenceThreshold <= 0) {
                // No last-checked test. Directly query Redis for existence.
                return checkExistenceFor(session, 0L, peek && state.isAllowExpireAfterAccess(), state);
            }

            // Do last-checked test
            LOG.debug("Check existence threshold enabled: {}", I(checkExistenceThreshold));
            long now = System.currentTimeMillis();
            long lastChecked = session.getLastChecked();
            if (lastChecked > 0) {
                long lastCheckedDuration = now - lastChecked;
                if (lastCheckedDuration < checkExistenceThreshold) {
                    // Assume still existent
                    LOG.debug("Last existence check {}ms ago, re-check threshold not exceeded. Returning locally fetched session {} unchecked", L(lastCheckedDuration), session.getSessionID());
                    return session;
                }
            }

            // Query Redis for existence
            return checkExistenceFor(session, now, peek && state.isAllowExpireAfterAccess(), state);
        } catch (OXException e) {
            throw new OXRuntimeException(e);
        }
    }

    /**
     * Checks existence of specified session in Redis storage;<br>
     * optionally resetting its expiration if <code>resetExpiryIfExists</code> flag says so.
     *
     * @param session The session to check existence for
     * @param lastCheckedToSet The optional last-checked time stamp to set (<code>0</code> means don't set)
     * @param resetExpiryIfExists <code>true</code> to reset session's expiration in Redis storage: otherwise <code>false</code>
     * @param state The current state
     * @return The session if existent or <code>null</code> if no such session exists in Redis storage
     * @throws OXException If checking existence fails
     */
    private SessionImpl checkExistenceFor(SessionImpl session, long lastCheckedToSet, boolean resetExpiryIfExists, RedisSessiondState state) throws OXException {
        try {
            LOG.debug("Checking existence for locally fetched session {} in Redis storage", session.getSessionID());

            RedisOperationKey<DefaultRedisOperationKey> key;
            RedisOperation<Boolean> existsOperation;
            if (lastCheckedToSet > 0) {
                // Set last-checked time stamp (if any) to session as soon as possible. Therefore include it in passed RedisOperation instance.
                key = newExistsOperationKey(session.getSessionID(), true);
                existsOperation = commandsProvider -> {
                    if (commandsProvider.getKeyCommands().exists(getSessionKey(session.getSessionID())).longValue() <= 0) {
                        // Session does NOT exist in Redis storage
                        return Boolean.FALSE;
                    }
                    // Session does exist in Redis storage. Thus, update/set last-checked time stamp
                    session.setLastChecked(lastCheckedToSet);
                    return Boolean.TRUE;
                };
            } else {
                key = newExistsOperationKey(session.getSessionID());
                existsOperation = commandsProvider -> B(commandsProvider.getKeyCommands().exists(getSessionKey(session.getSessionID())).longValue() > 0);
            }
            if (connector.executeOperation(key, existsOperation).booleanValue()) {
                // Exists...
                if (resetExpiryIfExists) {
                    LOG.debug("Locally fetched session {} does exist in Redis storage. Hence, update expiration & return that session", session.getSessionID());
                    long currentTime = System.currentTimeMillis();
                    String brandId = getBrandIdentifierFrom(session);
                    // Reset expiry for local Redis storage
                    connector.executeVoidOperation(commandsProvider -> resetSessionsExpireTime(session, brandId, currentTime, commandsProvider.getSortedSetCommands()));
                    // Reset expiry for remote Redis storages
                    List<RedisConnectorProvider> remoteConnectorProviders = getRemoteConnectorProviders();
                    if (!remoteConnectorProviders.isEmpty()) {
                        ExceptionCatchingRunnable task = () -> {
                            RedisVoidOperation resetOperation = commandsProvider -> resetSessionsExpireTime(session, brandId, currentTime, commandsProvider.getSortedSetCommands());
                            for (RedisConnectorProvider connectorProvider : remoteConnectorProviders) {
                                RedisConnector remoteConnector = connectorProvider.getConnector();
                                try {
                                    remoteConnector.executeVoidOperation(resetOperation);
                                } catch (Exception e) {
                                    LOG.error("Failed to reset expiration time for session {} on remote Redis storage {}", session.getSessionID(), remoteConnector, e);
                                }
                            }
                        };

                        ThreadPools.submitElseExecute(ThreadPools.task(task));
                    }
                } else {
                    LOG.debug("Locally fetched session {} does exist in Redis storage. Hence, return that session", session.getSessionID());
                }
                return session;
            }

            // Non-existent.
            LOG.debug("Locally fetched session {} does NOT exist in Redis storage. Dropping from local cache as well as Redis collections and returning negative result (null)", session.getSessionID());
            state.getLocalSessionCache().removeSessionById(session.getSessionID());

            // Drop from other Redis collections
            new RemovalCollection().addSession(session, true, true).removeCollected(this);
            return null;
        } catch (OXException e) { // NOSONARLINT
            LOG.warn("Failed to check existence for locally fetched session {} in Redis session storage", session.getSessionID(), e);
            throw e;
        } catch (Exception e) { // NOSONARLINT
            LOG.warn("Failed to check existence for locally fetched session {} in Redis session storage", session.getSessionID(), e);
            throw OXException.general(new StringBuilder("Failed to check existence for locally fetched session ").append(session.getSessionID()).append(" in Redis session storage").toString(), e);
        }
    }

    /**
     * Gets a new <code>SessionImpl</code> instance for given session.
     *
     * @param ses The session to yield for
     * @return The new <code>SessionImpl</code> instance or <code>null</code>
     */
    protected static SessionImpl newSessionImplFor(Session ses) {
        if (ses instanceof SessionImpl sessionImpl) {
            return sessionImpl;
        }

        if (ses == null) {
            return null;
        }

        SessionImpl sessionImpl = new SessionImpl(ses.getUserId(), ses.getLoginName(), ses.getPassword(), ses.getContextId(), ses.getSessionID(), ses.getSecret(), ses.getRandomToken(), ses.getLocalIp(), ses.getLogin(), ses.getAuthId(), ses.getHash(), ses.getClient(), ses.isStaySignedIn(), ses.getOrigin());
        for (String name : ses.getParameterNames()) {
            Object value = ses.getParameter(name);
            sessionImpl.setParameter(name, value);
        }
        return sessionImpl;
    }

    @Override
    public int getUserSessions(int userId, int contextId) {
        try {
            return connector.executeOperation(commandsProvider -> commandsProvider.getSetCommands().scard(getSetKeyForUser(userId, contextId))).intValue();
        } catch (Exception e) {
            LOG.warn("Failed to look-up sessions for user {} in context {} from Redis session storage", I(userId), I(contextId), e);
            return 0;
        }
    }

    @Override
    public Collection<Session> getSessions(int userId, int contextId, boolean considerSessionStorage) {
        if (!considerSessionStorage) {
            // Only look-up local cache
            RedisSessiondState state = getState();
            LocalSessionCache localSessionCache = state.getLocalSessionCache();
            List<SessionImpl> sessionsByUser = localSessionCache.getSessionsByUser(userId, contextId);
            Collection<Session> retval = new ArrayList<>(sessionsByUser.size());
            for (SessionImpl sessionImpl : sessionsByUser) {
                retval.add(sessionImpl);
            }
            return retval;
        }

        // Look-up in Redis storage
        try {
            Set<String> sessionIds = connector.executeOperation(commandsProvider -> commandsProvider.getSetCommands().smembers(getSetKeyForUser(userId, contextId)));
            if (sessionIds == null || sessionIds.isEmpty()) {
                return Collections.emptyList();
            }

            List<Session> sessions = new ArrayList<>(sessionIds.size());
            List<String> nonExistent = null;
            for (String sessionId : sessionIds) {
                Session session = doGetSession(newSessionId(sessionId), true, true);
                if (session != null) {
                    sessions.add(session);
                } else {
                    nonExistent = addWithCreateIfNull(sessionId, nonExistent, 2);
                }
            }

            if (nonExistent != null) {
                String[] members = nonExistent.toArray(new String[nonExistent.size()]);
                ThreadPools.submitElseExecute(ThreadPools.task(() -> {
                    try {
                        connector.executeVoidOperation(commandsProvider -> commandsProvider.getSetCommands().srem(getSetKeyForUser(userId, contextId), members));
                    } catch (Exception e) {
                        LOG.warn("Failed to remove non-existent sessions from Redis set {}", getSetKeyForUser(userId, contextId), e);
                    }
                }));
            }

            return sessions;
        } catch (Exception e) {
            LOG.warn("Failed to look-up sessions for user {} in context {} from Redis session storage", I(userId), I(contextId), e);
            return Collections.emptyList();
        }
    }

    @Override
    public boolean isActive(final String sessionId) {
        if (null == sessionId) {
            return false;
        }

        if (getState().getLocalSessionCache().getSessionByIdIfPresent(sessionId) != null) {
            return true;
        }

        try {
            return connector.executeOperation(newExistsOperationKey(sessionId), commandsProvider -> commandsProvider.getKeyCommands().exists(getSessionKey(sessionId))).longValue() > 0;
        } catch (Exception e) {
            LOG.warn("Failed to check existence of session {} in Redis session storage", sessionId, e);
            return false;
        }
    }

    @Override
    public List<String> getActiveSessionIDs() {
        long now = System.currentTimeMillis();
        long threshold = now - getState().getSessionDefaultLifeTime();
        final Range<Number> range = Range.create(Long.valueOf(threshold), Long.valueOf(now + RedisSessionConstants.MILLIS_DAY));

        try {
            DefaultRedisOperationKey key = DefaultRedisOperationKey.builder().withCommand(RedisCommand.ZRANGEBYSCORE).withContextId(1).withUserId(1).withHash("sessions.activeids").build();
            return connector.<List<String>> executeOperation(key, commandsProvider -> {
                RedisSortedSetCommands<String, String> sortedSetCommands = commandsProvider.getSortedSetCommands();

                List<String> sessionIds = null;
                for (String sortedSetKey : SORTED_SETS) {
                    long offset = 0;
                    Limit limit = Limit.create(offset, RedisSessionConstants.LIMIT_1000);

                    List<String> active = sortedSetCommands.zrangebyscore(sortedSetKey, range, limit);
                    for (int size; active != null && (size = active.size()) > 0;) {
                        sessionIds = addAllWithCreateIfNull(active, sessionIds);

                        // Get next chunk (if any)
                        if (size >= RedisSessionConstants.LIMIT_1000) {
                            offset += RedisSessionConstants.LIMIT_1000;
                            limit = Limit.create(offset, RedisSessionConstants.LIMIT_1000);
                            active = sortedSetCommands.zrangebyscore(sortedSetKey, range, limit);
                        } else {
                            active = null;
                        }
                    }
                }

                return sessionIds == null ? Collections.emptyList() : sessionIds;
            });
        } catch (Exception e) {
            LOG.warn("Failed to look-up active sessions in Redis session storage", e);
            return Collections.emptyList();
        }
    }

    @Override
    public Session getSessionByRandomToken(final String randomToken, final String localIp) {
        throw new UnsupportedOperationException("RedisSessiondService.getSessionByRandomToken()");
    }

    @Override
    public Session getSessionByRandomToken(final String randomToken) {
        throw new UnsupportedOperationException("RedisSessiondService.getSessionByRandomToken()");
    }

    @Override
    public Session getSessionWithTokens(final String clientToken, final String serverToken) throws OXException {
        // find session matching to token
        LOG.debug("Redeeming server token {}", serverToken);
        TokenSessionControl tokenControl = TokenSessionContainer.getInstance().getSession(clientToken, serverToken);
        SessionImpl activatedSession = tokenControl.getSession();
        LOG.debug("Redeemed server token {} for session {} of user {} in context {}", serverToken, activatedSession.getSessionID(), I(activatedSession.getUserId()), I(activatedSession.getContextId()));
        putSessionIntoRedisAndLocal(activatedSession, getState());
        LOG.debug("Completed adding session {} of user {} in context {}", activatedSession.getSessionID(), I(activatedSession.getUserId()), I(activatedSession.getContextId()));
        return activatedSession;
    }

    @Override
    public int getNumberOfActiveSessions() {
        try {
            return connector.executeOperation(commandsProvider -> L(doGetNumberSessionsInRedis(commandsProvider))).intValue();
        } catch (Exception e) {
            LOG.warn("Failed to count sessions in Redis session storage", e);
            return 0;
        }
    }

    /**
     * Gets the number of sessions in Redis session storage through issuing a <code>SCAN</code> command.
     *
     * @param connection The connection to use
     * @return The number of sessions
     */
    public static long doGetNumberSessionsInRedis(RedisCommandsProvider commandsProvider) {
        RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
        RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();

        ScanArgs scanArgs = ScanArgs.Builder.matches(getAllSetKeyPattern()).limit(RedisSessionConstants.LIMIT_1000);

        long count = 0;
        KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
        while (cursor != null) {
            // Iterate current keys...
            for (String setKey : cursor.getKeys()) {
                count += setCommands.scard(setKey).longValue();
            }

            // Move cursor forward
            cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
        }
        return count;
    }

    @Override
    public Session getAnyActiveSessionForUser(final int userId, final int contextId) {
        try {
            Session sessionFromRedis = connector.executeOperation(commandsProvider -> {
                RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();

                Session session;
                do {
                    String sessionId = setCommands.srandmember(getSetKeyForUser(userId, contextId));
                    if (sessionId == null) {
                        return null;
                    }
                    session = getSessionCommandsWithoutObfuscator(commandsProvider).get(getSessionKey(sessionId));
                    if (session == null) {
                        setCommands.srem(getSetKeyForUser(userId, contextId), sessionId);
                    }
                } while (session == null);
                return session;
            });

            unobfuscatePasswordOf(sessionFromRedis, getState().getObfuscator());

            SessionImpl newSessionImpl = newSessionImplFor(sessionFromRedis);
            if (newSessionImpl != null) {
                InstanceLoader loader = loaderFor(newSessionImpl);
                getState().getLocalSessionCache().getSessionById(newSessionImpl.getSessionID(), loader);

                if (loader.isLoaded()) {
                    LOG.debug("Put session {} into local cache after getAnyActiveSessionForUser()", newSessionImpl.getSessionID());

                    // Invoke interceptors
                    for (SessionSerializationInterceptor interceptor : serializationInterceptors) {
                        interceptor.deserialize(newSessionImpl);
                    }

                    // Post event for restored session (if newly put into cache)
                    postSessionRestauration(newSessionImpl);
                }
            }
            return newSessionImpl;
        } catch (Exception e) {
            LOG.warn("Failed to get session from Redis session storage", e);
            return null;
        }
    }

    @Override
    public Session findFirstMatchingSessionForUser(final int userId, final int contextId, final SessionMatcher matcher) {
        if (null == matcher) {
            return null;
        }

        RedisSessiondState state = getState();
        {
            SessionImpl session = state.getLocalSessionCache().getFirstMatchingSessionForUser(userId, contextId, matcher);
            if (session != null && ensureExistenceElseNull(session, true, state) != null) { // Ensure existence if locally fetched
                return session;
            }
        }

        try {
            Session sessionFromRedis = connector.executeOperation(commandsProvider -> {
                RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();
                Set<String> sessionIds = setCommands.smembers(getSetKeyForUser(userId, contextId));
                if (sessionIds == null || sessionIds.isEmpty()) {
                    return null;
                }

                RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator = getSessionCommandsWithoutObfuscator(commandsProvider);
                UnaryOperator<String> unobfuscationFunction = pw -> getState().getObfuscator().unobfuscate(pw);
                List<String> sessionKeys = sessionIds.stream().map(FUNCTION_SESSION_KEY).collect(CollectorUtils.toList(sessionIds));
                for (KeyValue<String, Session> keyValue : sessionCommandsWithoutObfuscator.mget(sessionKeys.toArray(new String[sessionKeys.size()]))) {
                    String sessionId = getSessionIdFrom(keyValue.getKey());
                    Session session = keyValue.getValueOrElse(null);
                    if (session == null) {
                        setCommands.srem(getSetKeyForUser(userId, contextId), sessionId);
                    } else {
                        // Un-obfuscate session's password when needed since matcher might use session's password for its acceptance criteria
                        ((ClusterSession) session).enableUnobfuscationOnPasswordAccess(unobfuscationFunction);
                        if (matcher.accepts(session)) {
                            return session;
                        }
                    }
                }
                return null;
            });

            SessionImpl newSessionImpl = newSessionImplFor(sessionFromRedis);
            if (newSessionImpl != null) {
                InstanceLoader loader = loaderFor(newSessionImpl);
                state.getLocalSessionCache().getSessionById(newSessionImpl.getSessionID(), loader);

                if (loader.isLoaded()) {
                    LOG.debug("Put session {} into local cache after findFirstMatchingSessionForUser()", newSessionImpl.getSessionID());

                    // Invoke interceptors
                    for (SessionSerializationInterceptor interceptor : serializationInterceptors) {
                        interceptor.deserialize(newSessionImpl);
                    }

                    // Post event for restored session (if newly put into cache)
                    postSessionRestauration(newSessionImpl);
                }
            }
            return newSessionImpl;
        } catch (Exception e) {
            LOG.warn("Failed to get session from Redis session storage", e);
            return null;
        }
    }

    @Override
    public Collection<String> removeSessions(SessionFilter filter, RemovalReason removalReason) throws OXException {
        LOG.debug("Removing sessions by session filter");

        // Look-up & remove matching sessions
        List<String> sessionIdentifiers = null;

        // First, check for user and context association
        UserAndContext userAndContext = getUserAssociationElseNull(filter);
        if (userAndContext != null) {
            int contextId = userAndContext.getContextId();
            int userId = userAndContext.getUserId();
            sessionIdentifiers = connector.executeOperation(commandsProvider -> {
                RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();

                Set<String> sessionIds = setCommands.smembers(getSetKeyForUser(userId, contextId));
                if (sessionIds == null || sessionIds.isEmpty()) {
                    return null;
                }

                RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator = getSessionCommandsWithoutObfuscator(commandsProvider);
                UnaryOperator<String> unobfuscationFunction = pw -> getState().getObfuscator().unobfuscate(pw);
                List<String> sessionKeys = sessionIds.stream().map(FUNCTION_SESSION_KEY).collect(CollectorUtils.toList(sessionIds));
                List<String> matchingSessionIds = null;
                RemovalCollection removalCollection = new RemovalCollection();

                for (KeyValue<String, Session> keyValue : sessionCommandsWithoutObfuscator.mget(sessionKeys.toArray(new String[sessionKeys.size()]))) {
                    Session session = keyValue.hasValue() ? keyValue.getValue() : null;
                    if (session == null) {
                        String sessionId = getSessionIdFrom(keyValue.getKey());
                        setCommands.srem(getSetKeyForUser(userId, contextId), sessionId);
                    } else {
                        // Un-obfuscate session's password when needed since filter might use session's password for its acceptance criteria
                        ((ClusterSession) session).enableUnobfuscationOnPasswordAccess(unobfuscationFunction);
                        if (filter.apply(session)) {
                            matchingSessionIds = addWithCreateIfNull(session.getSessionID(), matchingSessionIds, sessionKeys.size());
                            removalCollection.addSession(session, true, true);
                            LOG.debug("Removed session {} by session filter from Redis storage for user {} in context {}", session.getSessionID(), I(session.getUserId()), I(session.getContextId())); // NOSONARLINT
                        }
                    }
                }
                removalCollection.removeCollected(commandsProvider);
                return matchingSessionIds == null ? Collections.emptyList() : matchingSessionIds;
            });
        }

        if (sessionIdentifiers == null) {
            // Then, check for at least context association
            int contextId = getContextAssociationElseZero(filter);
            if (contextId > 0) {
                UserService userService = services.getServiceSafe(UserService.class);
                int[] userIds = userService.getUserIds(contextId);
                for (int userId : userIds) {
                    List<String> removedSessionsForUser = connector.executeOperation(commandsProvider -> {
                        RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();

                        Set<String> sessionIds = setCommands.smembers(getSetKeyForUser(userId, contextId));
                        if (sessionIds == null || sessionIds.isEmpty()) {
                            return null;
                        }

                        RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator = getSessionCommandsWithoutObfuscator(commandsProvider);
                        UnaryOperator<String> unobfuscationFunction = pw -> getState().getObfuscator().unobfuscate(pw);
                        List<String> sessionKeys = sessionIds.stream().map(FUNCTION_SESSION_KEY).collect(CollectorUtils.toList(sessionIds));
                        List<String> matchingSessionIds = null;
                        RemovalCollection removalCollection = new RemovalCollection();

                        for (KeyValue<String, Session> keyValue : sessionCommandsWithoutObfuscator.mget(sessionKeys.toArray(new String[sessionKeys.size()]))) {
                            Session session = keyValue.hasValue() ? keyValue.getValue() : null;
                            if (session == null) {
                                String sessionId = getSessionIdFrom(keyValue.getKey());
                                setCommands.srem(getSetKeyForUser(userId, contextId), sessionId);
                            } else {
                                // Un-obfuscate session's password when needed since filter might use session's password for its acceptance criteria
                                ((ClusterSession) session).enableUnobfuscationOnPasswordAccess(unobfuscationFunction);
                                if (filter.apply(session)) {
                                    matchingSessionIds = addWithCreateIfNull(session.getSessionID(), matchingSessionIds, sessionKeys.size());
                                    removalCollection.addSession(session, true, true);
                                    LOG.debug("Removed session {} by session filter from Redis storage for user {} in context {}", session.getSessionID(), I(session.getUserId()), I(session.getContextId()));
                                }
                            }
                        }
                        removalCollection.removeCollected(commandsProvider);
                        return matchingSessionIds == null ? Collections.emptyList() : matchingSessionIds;
                    });
                    if (sessionIdentifiers == null) {
                        sessionIdentifiers = new ArrayList<>(removedSessionsForUser);
                    } else {
                        sessionIdentifiers.addAll(removedSessionsForUser);
                    }
                }
            }
        }

        if (sessionIdentifiers == null) {
            // Well, the hard way... Check each session since no user nor context association is given
            ScanArgs scanArgs = ScanArgs.Builder.matches(getAllSetKeyPattern()).limit(RedisSessionConstants.LIMIT_1000);
            sessionIdentifiers = connector.executeOperation(commandsProvider -> {
                RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator = getSessionCommandsWithoutObfuscator(commandsProvider);
                UnaryOperator<String> unobfuscationFunction = pw -> getState().getObfuscator().unobfuscate(pw);
                RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();
                RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();

                List<String> retval = null;
                RemovalCollection removalCollection = new RemovalCollection();

                KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
                while (cursor != null) {
                    // Iterate current keys...
                    for (String setKey : cursor.getKeys()) {
                        // Get set's members (list of session identifiers)
                        Set<String> sessionIds = setCommands.smembers(setKey);
                        if (sessionIds != null && !sessionIds.isEmpty()) {
                            List<String> sessionKeys = sessionIds.stream().map(FUNCTION_SESSION_KEY).collect(CollectorUtils.toList(sessionIds));
                            for (KeyValue<String, Session> keyValue : sessionCommandsWithoutObfuscator.mget(sessionKeys.toArray(new String[sessionKeys.size()]))) {
                                String sessionId = getSessionIdFrom(keyValue.getKey());
                                Session session = keyValue.hasValue() ? keyValue.getValue() : null;
                                if (session == null) {
                                    // Remove from set since non-existent
                                    setCommands.srem(setKey, sessionId);
                                } else {
                                    // Un-obfuscate session's password when needed since filter might use session's password for its acceptance criteria
                                    ((ClusterSession) session).enableUnobfuscationOnPasswordAccess(unobfuscationFunction);

                                    // Check if filter applies. Remove session if it does.
                                    if (filter.apply(session) && (session = sessionCommandsWithoutObfuscator.getdel(getSessionKey(sessionId))) != null) {
                                        removalCollection.addSession(session, true, true);
                                        LOG.debug("Removed session {} by session filter from Redis storage for user {} in context {}", sessionId, I(session.getUserId()), I(session.getContextId()));
                                        retval = addWithCreateIfNull(sessionId, retval, 0);
                                    }
                                }
                            }
                        }
                    }

                    // Move cursor forward
                    cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
                }

                removalCollection.removeCollected(commandsProvider);

                return retval == null ? Collections.emptyList() : retval;
            });
        }

        // Ensure not null
        if (sessionIdentifiers == null) {
            sessionIdentifiers = Collections.emptyList();
        }

        // Any found?
        int numberOfRemovedSessions = sessionIdentifiers.size();
        if (numberOfRemovedSessions > 0) {
            // Drop matching session on remote sites, too
            List<RedisConnectorProvider> remoteConnectorProviders = getRemoteConnectorProviders();
            if (!remoteConnectorProviders.isEmpty()) {
                List<String> sessionIdentifiersToRemove = sessionIdentifiers;

                ExceptionCatchingRunnable task = () -> {
                    String[] sessionKeys = sessionIdentifiersToRemove.stream().map(FUNCTION_SESSION_KEY).collect(CollectorUtils.toList(sessionIdentifiersToRemove)).toArray(new String[numberOfRemovedSessions]);
                    RedisVoidOperation deleteOperation = commandsProvider -> commandsProvider.getKeyCommands().del(sessionKeys);
                    for (RedisConnectorProvider connectorProvider : remoteConnectorProviders) {
                        RedisConnector remoteConnector = connectorProvider.getConnector();
                        try {
                            remoteConnector.executeVoidOperation(deleteOperation);
                        } catch (Exception e) {
                            LOG.error("Failed to remove sessions from remote Redis storage {}", remoteConnector, e);
                        }
                    }
                };
                ThreadPools.submitElseExecute(ThreadPools.task(task));
            }

            // Drop them from local cache
            List<String> sessionIdentifiersToRemove = sessionIdentifiers;
            ExceptionCatchingRunnable task = () -> {
                List<SessionImpl> locallyRemovedSessions = getState().getLocalSessionCache().removeAndGetSessionsByIds(sessionIdentifiersToRemove);
                postSessionOrContainerRemoval(locallyRemovedSessions, removalReason, false);
                LOG.debug("Removed {} filtered sessions from local cache", I(locallyRemovedSessions.size()));
            };
            ThreadPools.submitElseExecute(ThreadPools.task(task));

            // Issue event
            publishInvalidateEventQuietly(sessionIdentifiers);
        }

        LOG.debug("Removed {} filtered sessions from Redis storage", I(numberOfRemovedSessions));
        return sessionIdentifiers;
    }

    @Override
    public Collection<String> findSessions(SessionFilter filter) throws OXException {
        UserAndContext userAndContext = getUserAssociationElseNull(filter);
        if (userAndContext != null) {
            int contextId = userAndContext.getContextId();
            int userId = userAndContext.getUserId();
            return connector.executeOperation(commandsProvider -> {
                RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();
                Set<String> sessionIds = setCommands.smembers(getSetKeyForUser(userId, contextId));
                if (sessionIds == null || sessionIds.isEmpty()) {
                    return Collections.emptyList();
                }

                RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator = getSessionCommandsWithoutObfuscator(commandsProvider);
                UnaryOperator<String> unobfuscationFunction = pw -> getState().getObfuscator().unobfuscate(pw);
                List<String> sessionKeys = sessionIds.stream().map(FUNCTION_SESSION_KEY).collect(CollectorUtils.toList(sessionIds));
                List<String> matchingSessionIds = null;
                for (KeyValue<String, Session> keyValue : sessionCommandsWithoutObfuscator.mget(sessionKeys.toArray(new String[sessionKeys.size()]))) {
                    String sessionId = getSessionIdFrom(keyValue.getKey());
                    Session session = keyValue.hasValue() ? keyValue.getValue() : null;
                    if (session == null) {
                        setCommands.srem(getSetKeyForUser(userId, contextId), sessionId);
                    } else {
                        // Un-obfuscate session's password when needed since filter might use session's password for its acceptance criteria
                        ((ClusterSession) session).enableUnobfuscationOnPasswordAccess(unobfuscationFunction);
                        if (filter.apply(session)) {
                            matchingSessionIds = addWithCreateIfNull(sessionId, matchingSessionIds, sessionKeys.size());
                        }
                    }
                }
                return matchingSessionIds == null ? Collections.emptyList() : matchingSessionIds;
            });
        }

        int contextId = getContextAssociationElseZero(filter);
        if (contextId > 0) {
            ScanArgs scanArgs = ScanArgs.Builder.matches(getSetKeyPattern(contextId)).limit(RedisSessionConstants.LIMIT_1000);
            return connector.executeOperation(commandsProvider -> {
                RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();
                RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator = getSessionCommandsWithoutObfuscator(commandsProvider);
                UnaryOperator<String> unobfuscationFunction = pw -> getState().getObfuscator().unobfuscate(pw);
                RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();

                List<String> matchingSessionIds = null;
                KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
                while (cursor != null) {
                    // Obtain current keys...
                    List<String> keys = cursor.getKeys();
                    int numberOfKeys = keys.size();
                    if (numberOfKeys > 0) {
                        // Iterate set identifier
                        for (String setKey : keys) {
                            // Get set's members (list of session identifiers)
                            Set<String> sessionIds = setCommands.smembers(setKey);
                            if (sessionIds != null && !sessionIds.isEmpty()) {
                                List<String> sessionKeys = sessionIds.stream().map(FUNCTION_SESSION_KEY).collect(CollectorUtils.toList(sessionIds));
                                for (KeyValue<String, Session> keyValue : sessionCommandsWithoutObfuscator.mget(sessionKeys.toArray(new String[sessionKeys.size()]))) {
                                    String sessionId = getSessionIdFrom(keyValue.getKey());
                                    Session session = keyValue.hasValue() ? keyValue.getValue() : null;
                                    if (session == null) {
                                        // Remove from set since non-existent
                                        setCommands.srem(setKey, sessionId);
                                    } else {
                                        // Un-obfuscate session's password when needed since filter might use session's password for its acceptance criteria
                                        ((ClusterSession) session).enableUnobfuscationOnPasswordAccess(unobfuscationFunction);
                                        if (filter.apply(session)) {
                                            matchingSessionIds = addWithCreateIfNull(sessionId, matchingSessionIds, sessionKeys.size());
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Move cursor forward
                    cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
                }
                return matchingSessionIds == null ? Collections.emptyList() : matchingSessionIds;
            });
        }

        // The hard way...
        ScanArgs scanArgs = ScanArgs.Builder.matches(getAllSetKeyPattern()).limit(RedisSessionConstants.LIMIT_1000);

        return connector.executeOperation(commandsProvider -> {
            RedisStringCommands<String, Session> sessionCommandsWithoutObfuscator = getSessionCommandsWithoutObfuscator(commandsProvider);
            UnaryOperator<String> unobfuscationFunction = pw -> getState().getObfuscator().unobfuscate(pw);
            RedisSetCommands<String, String> setCommands = commandsProvider.getSetCommands();
            RedisKeyCommands<String, InputStream> keyCommands = commandsProvider.getKeyCommands();
            List<String> retval = new ArrayList<>();

            KeyScanCursor<String> cursor = keyCommands.scan(scanArgs);
            while (cursor != null) {
                // Iterate current keys...
                for (String setKey : cursor.getKeys()) {
                    // Get set's members (list of session identifiers)
                    Set<String> sessionIds = setCommands.smembers(setKey);
                    if (sessionIds != null && !sessionIds.isEmpty()) {
                        List<String> sessionKeys = sessionIds.stream().map(FUNCTION_SESSION_KEY).collect(CollectorUtils.toList(sessionIds));
                        for (KeyValue<String, Session> keyValue : sessionCommandsWithoutObfuscator.mget(sessionKeys.toArray(new String[sessionKeys.size()]))) {
                            String sessionId = getSessionIdFrom(keyValue.getKey());
                            Session session = keyValue.hasValue() ? keyValue.getValue() : null;
                            if (session == null) {
                                // Remove from set since non-existent
                                setCommands.srem(setKey, sessionId);
                            } else {
                                // Un-obfuscate session's password when needed since filter might use session's password for its acceptance criteria
                                ((ClusterSession) session).enableUnobfuscationOnPasswordAccess(unobfuscationFunction);
                                if (filter.apply(session)) {
                                    retval.add(sessionId);
                                }
                            }
                        }
                    }
                }

                // Move cursor forward
                cursor = cursor.isFinished() ? null : keyCommands.scan(cursor, scanArgs);
            }
            return retval;
        });
    }

    private static final Pattern FILTER_USER = Pattern.compile("\\("+SessionFilter.CONTEXT_ID+"=([0-9]+)\\)\\("+SessionFilter.USER_ID+"=([0-9]+)\\)");

    /**
     * Extracts the user association from given filter if such an association is present; otherwise returns <code>null</code>.
     *
     * @param filter The filter to examine
     * @return The user association or <code>null</code>
     */
    private static UserAndContext getUserAssociationElseNull(SessionFilter filter) {
        Optional<UserAndContext> optUserFilter = filter.getUserAssociation();
        if (optUserFilter.isPresent()) {
            return optUserFilter.get();
        }

        if (filter.toString().indexOf(SessionFilter.CONTEXT_ID) < 0) {
            return null;
        }
        if (filter.toString().indexOf(SessionFilter.USER_ID) < 0) {
            return null;
        }

        Matcher m = FILTER_USER.matcher(filter.toString());
        if (!m.find()) {
            return null;
        }

        try {
            return UserAndContext.newInstance(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(1)));
        } catch (NumberFormatException e) {
            LOG.debug("Could not parse either context or user identifier from session filter expression: {}", filter, e);
            return null;
        }
    }

    private static final Pattern FILTER_CONTEXT = Pattern.compile("\\("+SessionFilter.CONTEXT_ID+"=([0-9]+)\\)");

    /**
     * Extracts the context association from given filter if such an association is present; otherwise returns <code>0</code> (zero).
     *
     * @param filter The filter to examine
     * @return The context association or <code>0</code> (zero)
     */
    private static int getContextAssociationElseZero(SessionFilter filter) {
        Optional<Integer> optContextFilter = filter.getContextAssociation();
        if (optContextFilter.isPresent()) {
            return optContextFilter.get().intValue();
        }

        if (filter.toString().indexOf(SessionFilter.CONTEXT_ID) < 0) {
            return 0;
        }

        Matcher m = FILTER_CONTEXT.matcher(filter.toString());
        if (!m.find()) {
            return 0;
        }

        try {
            return Integer.parseInt(m.group(1));
        } catch (NumberFormatException e) {
            LOG.debug("Could not parse context identifier from session filter expression: {}", filter, e);
            return 0;
        }
    }

    // -------------------------------------------------------- Channel listener------------------------------------------------------------

    @Override
    public void onMessage(Message<SessionEvent> message) {
        if (!message.isRemote()) {
            // Ignore local event
            return;
        }

        SessionEvent sessionEvent = message.getData();
        switch (sessionEvent.getOperation()) {
            case INVALIDATE:
                getState().getLocalSessionCache().removeSessionsByIds(sessionEvent.getSessionIds());
                break;
            case LAST_GONE_CONTEXT:
                postCentralLastSessionGoneForContexts(sessionEvent.getContextIds(), false);
                break;
            case LAST_GONE_USER:
                postCentralLastSessionGoneForUsers(sessionEvent.getUserIds(), false);
                break;
            default:
                LOG.debug("Recieved session event of no interest: {}", sessionEvent);
        }
    }

    // -------------------------------------------------------- Events ---------------------------------------------------------------------

    /**
     * Post the event that a single session has been put into Redis storage.
     *
     * @param session The stored session
     * @param publishInvalidateEvent Whether to publish invalidate event in cluster
     */
    private void postSessionStored(Session session, boolean publishInvalidateEvent) {
        EventAdmin eventAdmin = services.getOptionalService(EventAdmin.class);
        if (eventAdmin != null) {
            Map<String, Object> dic = HashMap.newHashMap(2);
            dic.put(SessiondEventConstants.PROP_SESSION, session);
            eventAdmin.postEvent(new Event(SessiondEventConstants.TOPIC_STORED_SESSION, dic));
            LOG.debug("Posted event for stored session");
        }

        if (publishInvalidateEvent) {
            publishInvalidateEventQuietly(Collections.singletonList(session.getSessionID()));
        }
    }

    /**
     * Posts the event that a single session has been locally created.
     *
     * @param incrementCounters Whether to increment counters for that session
     */
    private void postSessionCreation(Session session) {
        EventAdmin eventAdmin = services.getOptionalService(EventAdmin.class);
        if (eventAdmin != null) {
            Map<String, Object> dic = HashMap.newHashMap(2);
            dic.put(SessiondEventConstants.PROP_SESSION, session);
            eventAdmin.postEvent(new Event(SessiondEventConstants.TOPIC_ADD_SESSION, dic));
            LOG.debug("Posted event for added session");
        }
    }

    /**
     * Posts the event for a locally restored session.
     * <p>
     * That is session representation has been fetched from Redis storage and has been added to local cache.
     *
     * @param session The restored session
     */
    private void postSessionRestauration(Session session) {
        EventAdmin eventAdmin = services.getOptionalService(EventAdmin.class);
        if (eventAdmin != null) {
            Map<String, Object> dic = HashMap.newHashMap(2);
            dic.put(SessiondEventConstants.PROP_SESSION, session);
            eventAdmin.postEvent(new Event(SessiondEventConstants.TOPIC_RESTORED_SESSION, dic));
            LOG.debug("Posted event for restored session");
        }
    }

    /**
     * Posts the event for removal of locally held session(s), either using {@link SessiondEventConstants#TOPIC_REMOVE_SESSION} if a
     * single session has been removed, or {@link SessiondEventConstants#TOPIC_REMOVE_CONTAINER} for the removal of multiple sessions.
     *
     * @param <S> The session type
     * @param removedSessions The removed sessions
     * @param removalReason The reason why the sessions are removed
     * @param fireRemoteEvent Whether to publish that event remotely
     */
    private <S extends Session> void postSessionOrContainerRemoval(List<S> removedSessions, RemovalReason removalReason, boolean fireRemoteEvent) {
        if (null == removedSessions) {
            return;
        }
        int numRemovedSessions = removedSessions.size();
        if (numRemovedSessions > 0) {
            if (numRemovedSessions == 1) {
                postSessionRemoval(removedSessions.get(0), removalReason, fireRemoteEvent);
            } else {
                postContainerRemoval(removedSessions, removalReason, fireRemoteEvent);
            }
        }
    }

    /**
     * Posts the event for removal of locally held sessions.
     *
     * @param <S> The session type
     * @param sessions The removed sessions
     * @param removalReason The reason why the sessions are removed
     * @param fireRemoteEvent Whether to publish that event remotely
     */
    private <S extends Session> void postContainerRemoval(List<S> sessions, RemovalReason removalReason, boolean fireRemoteEvent) {
        EventAdmin eventAdmin = services.getOptionalService(EventAdmin.class);
        if (eventAdmin != null) {
            Map<String, Session> eventMap = Maps.newHashMapWithExpectedSize(sessions.size());
            for (Session session : sessions) {
                eventMap.put(session.getSessionID(), session);
            }
            Map<String, Object> dic = HashMap.newHashMap(2);
            dic.put(SessiondEventConstants.PROP_CONTAINER, eventMap);
            dic.put(SessiondEventConstants.PROP_REMOVAL_REASON, removalReason);
            eventAdmin.postEvent(new Event(SessiondEventConstants.TOPIC_REMOVE_CONTAINER, dic));
        }

        if (fireRemoteEvent) {
            publishInvalidateEventQuietly(sessions.stream().map(Session::getSessionID).collect(CollectorUtils.toList(sessions)));
        }
    }

    /**
     * Posts the event for removal of a locally held session.
     *
     * @param session The removed session
     * @param fireRemoteEvent Whether to publish that event remotely
     */
    private void postSessionRe8moval(Session session, boolean fireRemoteEvent) {
        postSessionRemoval(session, RemovalReason.UNKNOWN, fireRemoteEvent);
    }

    /**
     * Posts the event for removal of a locally held session.
     *
     * @param session The removed session
     * @param removalReason The reason why the sessions are removed
     * @param fireRemoteEvent Whether to publish that event remotely
     */
    private void postSessionRemoval(Session session, RemovalReason removalReason, boolean fireRemoteEvent) {
        EventAdmin eventAdmin = services.getOptionalService(EventAdmin.class);
        if (eventAdmin != null) {
            Map<String, Object> dic = HashMap.newHashMap(2);
            dic.put(SessiondEventConstants.PROP_SESSION, session);
            dic.put(SessiondEventConstants.PROP_REMOVAL_REASON, removalReason);
            eventAdmin.postEvent(new Event(SessiondEventConstants.TOPIC_REMOVE_SESSION, dic));
            LOG.debug("Posted event for removed session {}", session.getSessionID());
        }

        if (fireRemoteEvent) {
            publishInvalidateEventQuietly(Collections.singletonList(session.getSessionID()));
        }
    }

    private void publishInvalidateEventQuietly(Collection<String> sessionIds) {
        if (null == sessionIds || sessionIds.isEmpty()) {
            return;
        }
        try {
            channel.publish(SessionEvent.newInvalidateSessionEvent(sessionIds));
        } catch (Exception e) {
            LOG.warn("Failed to publish session event for invalidated session identifiers: {}", sessionIds, e);
        }
    }

    /**
     * Posts the event for last session gone for a certain context in central storage.
     *
     * @param contextIds The identifiers of affected contexts
     * @param fireRemoteEvent Whether to publish that event remotely
     */
    public void postCentralLastSessionGoneForContexts(Collection<Integer> contextIds, boolean fireRemoteEvent) {
        EventAdmin eventAdmin = services.getOptionalService(EventAdmin.class);
        if (eventAdmin != null) {
            for (Integer contextId : contextIds) {
                Dictionary<String, Object> dic = new Hashtable<String, Object>(2);
                dic.put(SessiondEventConstants.PROP_CONTEXT_ID, contextId);
                eventAdmin.postEvent(new Event(SessiondEventConstants.TOPIC_CENTRAL_LAST_SESSION_CONTEXT, dic));
                LOG.debug("Posted event for central last removed session for context {}", contextId);
            }
        }

        if (fireRemoteEvent) {
            publishLastContextGoneEventQuietly(contextIds);
        }
    }

    private void publishLastContextGoneEventQuietly(Collection<Integer> contextIds) {
        if (null == contextIds || contextIds.isEmpty()) {
            return;
        }
        try {
            channel.publish(SessionEvent.newLastGoneContextSessionEvent(contextIds));
        } catch (Exception e) {
            LOG.warn("Failed to publish session event for last-gone context identifiers: {}", contextIds, e);
        }
    }

    /**
     * Posts the event for last session gone for a certain user in central storage.
     *
     * @param userIds The identifiers of affected users
     * @param fireRemoteEvent Whether to publish that event remotely
     */
    public void postCentralLastSessionGoneForUsers(Collection<UserAndContext> userIds, boolean fireRemoteEvent) {
        EventAdmin eventAdmin = services.getOptionalService(EventAdmin.class);
        if (eventAdmin != null) {
            for (UserAndContext userId : userIds) {
                Dictionary<String, Object> dic = new Hashtable<>(2);
                dic.put(SessiondEventConstants.PROP_USER_ID, I(userId.getUserId()));
                dic.put(SessiondEventConstants.PROP_CONTEXT_ID, I(userId.getContextId()));
                eventAdmin.postEvent(new Event(SessiondEventConstants.TOPIC_CENTRAL_LAST_SESSION, dic));
                LOG.debug("Posted event for central last removed session for user {} in context {}", I(userId.getUserId()), I(userId.getContextId()));
            }
        }

        if (fireRemoteEvent) {
            publishLastUserGoneEventQuietly(userIds);
        }
    }

    private void publishLastUserGoneEventQuietly(Collection<UserAndContext> userIds) {
        if (null == userIds || userIds.isEmpty()) {
            return;
        }
        try {
            channel.publish(SessionEvent.newLastGoneUserSessionEvent(userIds));
        } catch (Exception e) {
            Object stringObject = LogUtility.toStringObjectFor(() -> userIds.stream().map(u -> new StringBuilder().append(u.getUserId()).append('@').append(u.getContextId()).toString()).toList());
            LOG.warn("Failed to publish session event for last-gone user identifiers: {}", stringObject, e);
        }
    }

    /**
     * Gets a list of Redis connector providers to <i>remote</i> sites, if configured.
     *
     * @return The remote connector providers, or an empty list if there are none
     * @throws OXException If remote connector providers cannot be returned
     */
    private List<RedisConnectorProvider> getRemoteConnectorProviders() throws OXException {
        RedisConnectorService connectorService = services.getOptionalService(RedisConnectorService.class);
        if (null != connectorService) {
            List<RedisConnectorProvider> remoteConnectorProviders = connectorService.getRemoteConnectorProviders();
            if (null != remoteConnectorProviders) {
                return remoteConnectorProviders;
            }
        }
        return Collections.emptyList();
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Un-obfuscates the password of given session that has been fetched/deserialized from Redis storage.
     *
     * @param sessionFromRedis The session previously fetched/deserialized from Redis storage
     * @param obfuscator The obfuscator to use; see {@link #getState()} and {@link RedisSessiondState#getObfuscator()}
     * @return The session with un-obfuscated password
     * @throws IllegalArgumentException If given obfuscator is <code>null</code>
     */
    private static Session unobfuscatePasswordOf(Session sessionFromRedis, Obfuscator obfuscator) {
        if (sessionFromRedis != null) {
            String password = sessionFromRedis.getPassword();
            if (password != null) {
                if (obfuscator == null) {
                    throw new IllegalArgumentException("Obfuscator must not be null");
                }
                ((ClusterSession) sessionFromRedis).setPassword(obfuscator.unobfuscate(password));
            }
        }
        return sessionFromRedis;
    }

    /**
     * Un-obfuscates the password of given session that has been fetched/deserialized from Redis storage.
     *
     * @param sessionFromRedis The session previously fetched/deserialized from Redis storage
     * @param obfuscator The obfuscator to use; see {@link #getState()} and {@link RedisSessiondState#getObfuscator()}
     * @return The session with un-obfuscated password
     * @throws IllegalArgumentException If given obfuscator is <code>null</code>
     */
    private static Session unobfuscatePasswordOf(ClusterSession sessionFromRedis, Obfuscator obfuscator) {
        if (sessionFromRedis != null) {
            String password = sessionFromRedis.getPassword();
            if (password != null) {
                if (obfuscator == null) {
                    throw new IllegalArgumentException("Obfuscator must not be null");
                }
                sessionFromRedis.setPassword(obfuscator.unobfuscate(password));
            }
        }
        return sessionFromRedis;
    }

    /** Utility class to remember reset time stamp and brand identifier for resetting a session's expiration */
    private static class StampAndBrandIdReference {

        /** Container variable for reset time stamp */
        private final AtomicLong stampRef;

        /** Container variable for brand identifier */
        private final AtomicReference<String> brandIdRef;

        /**
         * Initializes a new {@link StampAndBrandIdReference}.
         */
        StampAndBrandIdReference() {
            super();
            // Container variables for reset time stamp and brand identifier
            stampRef = new AtomicLong();
            brandIdRef = new AtomicReference<>();
        }

        /**
         * Gets the reset time stamp.
         *
         * @return The reset time stamp
         */
        public long getStamp() {
            return stampRef.get();
        }

        /**
         * Sets the reset time stamp.
         *
         * @param stamp The reset time stamp to set
         */
        public void setStamp(long stamp) {
            stampRef.set(stamp);
        }

        /**
         * Gets the brand identifier.
         *
         * @return The brand identifier
         */
        public String getBrandId() {
            return brandIdRef.get();
        }

        /**
         * Sets the brand identifier.
         *
         * @param brandId The brand identifier to set
         */
        public void setBrandId(String brandId) {
            brandIdRef.set(brandId);
        }
    }

    /**
     * Adds specified element to given list, creating a new list if <code>null</code>, and returns the list to which the element has been
     * added that is guaranteed to be not <code>null</code> then.
     *
     * @param <E> The type of the element to add
     * @param elementToAdd The element to add
     * @param nullableList The list to add the element to; may be <code>null</code>
     * @param optInitialCapacity The optional initial capacity of the list in case it needs to be newly created
     * @return The list to which the element has been added
     */
    private static <E> List<E> addWithCreateIfNull(E elementToAdd, List<E> nullableList, int optInitialCapacity) {
        List<E> l = nullableList;
        if (l == null) {
            l = optInitialCapacity > 0 ? new ArrayList<>(optInitialCapacity) : new ArrayList<>();
        }
        l.add(elementToAdd);
        return l;
    }

    /**
     * Adds specified elements to given list, creating a new list if <code>null</code>, and returns the list to which the elements have been
     * added that is guaranteed to be not <code>null</code> then.
     *
     * @param <E> The type of the elements to add
     * @param elementsToAdd The elements to add
     * @param nullableList The list to add the elements to; may be <code>null</code>
     * @return The list to which the elements have been added
     */
    private static <E> List<E> addAllWithCreateIfNull(Collection<E> elementsToAdd, List<E> nullableList) {
        List<E> l = nullableList;
        if (l == null) {
            l = new ArrayList<>(elementsToAdd);
        } else {
            l.addAll(elementsToAdd);
        }
        return l;
    }

    /**
     * Filters specified collection by <code>null</code> elements if it contains any; otherwise the collection is returned as-is.
     *
     * @param <E> The type of the collection's elements
     * @param collection The collection to check for possible <code>null</code> elements
     * @return The collection without <code>null</code> elements
     */
    private static <E> Collection<E> filterNullElementsIfAny(Collection<E> collection) {
        boolean hasNull = false;
        for (Iterator<E> it = collection.iterator(); !hasNull && it.hasNext();) {
            if (it.next() == null) {
                hasNull = true;
            }
        }
        return hasNull ? collection.stream().filter(Predicates.isNotNullPredicate()).collect(CollectorUtils.toList(collection.size())) : collection;
    }

    private static Object prettyPrinterFor(Session session, RedisSessiondState state) {
        return new SessionPrettyPrinter(session, state);
    }

    private static final class SessionPrettyPrinter {

        private final Session session;
        private final RedisSessiondState state;

        /**
         * Initializes a new {@link ObjectExtension}.
         *
         * @param session The session to pretty-print
         * @param state The current state
         */
        SessionPrettyPrinter(Session session, RedisSessiondState state) {
            super();
            this.session = session;
            this.state = state;
        }

        @Override
        public String toString() {
            try {
                return SessionCodec.session2Json(session, state.getObfuscator(), RedisSessionVersionService.getInstance()).toString(0);
            } catch (Exception e) {
                LOG.warn("Failed to pretty-print session {}", session.getSessionID(), e);
                return session.toString();
            }
        }
    }

}
