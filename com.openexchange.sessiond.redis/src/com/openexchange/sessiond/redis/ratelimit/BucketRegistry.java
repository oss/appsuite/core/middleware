/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.sessiond.redis.ratelimit;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.java.Strings;
import com.openexchange.log.LogProperties;
import com.openexchange.sessiond.redis.config.RedisSessiondConfigProperty;
import com.openexchange.sessiond.redis.osgi.Services;
import io.github.bucket4j.Bucket;

/**
 * {@link BucketRegistry} - The registry for call-specific buckets.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public final class BucketRegistry {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(BucketRegistry.class);

    private static final AtomicReference<BucketRegistry> INSTANCE_REFERENCE = new AtomicReference<>();

    /**
     * Gets the registry instance.
     *
     * @return The registry instance
     */
    public static BucketRegistry getInstance() {
        BucketRegistry instance = INSTANCE_REFERENCE.get();
        if (instance == null) {
            synchronized (INSTANCE_REFERENCE) {
                instance = INSTANCE_REFERENCE.get();
                if (instance == null) {
                    LeanConfigurationService configService = Services.optService(LeanConfigurationService.class);
                    instance = new BucketRegistry(configService);
                    INSTANCE_REFERENCE.set(instance);
                }
            }
        }
        return instance;
    }

    /**
     * (Re-) Initializes the registry instance.
     *
     * @param configService The configuration service needed for initialization
     */
    public static void reinitInstance(LeanConfigurationService configService) {
        BucketRegistry prevInstance = INSTANCE_REFERENCE.getAndSet(new BucketRegistry(configService));
        if (prevInstance != null) {
            prevInstance.bucketCache.invalidateAll();
        }
    }

    /**
     * Drops the current registry instance.
     */
    public static void dropInstance() {
        BucketRegistry instance = INSTANCE_REFERENCE.getAndSet(null);
        if (instance != null) {
            instance.bucketCache.invalidateAll();
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final LoadingCache<Key, Bucket> bucketCache;
    private final Bucket overallBucket;

    /**
     * Initializes a new {@link BucketRegistry}.
     *
     * @param configService The configuration service needed for initialization
     */
    private BucketRegistry(LeanConfigurationService configService) {
        super();

        // Read configuration values
        int maxAccessesPerClient;
        long timeWindowMillisPerClient;
        int overallMaxAccesses;
        long overallTimeWindowMillis;
        if (configService != null) {
            maxAccessesPerClient = configService.getIntProperty(RedisSessiondConfigProperty.REMOTE_SITE_RATELIMIT_MAX_ACCESSES_PER_CLIENT);
            timeWindowMillisPerClient = configService.getLongProperty(RedisSessiondConfigProperty.REMOTE_SITE_RATELIMIT_TIME_WINDOW_MILLIS_PER_CLIENT);
            overallMaxAccesses = configService.getIntProperty(RedisSessiondConfigProperty.REMOTE_SITE_RATELIMIT_OVERALL_MAX_ACCESSES);
            overallTimeWindowMillis = configService.getLongProperty(RedisSessiondConfigProperty.REMOTE_SITE_RATELIMIT_OVERALL_TIME_WINDOW_MILLIS);
        } else {
            maxAccessesPerClient = ((Integer) RedisSessiondConfigProperty.REMOTE_SITE_RATELIMIT_MAX_ACCESSES_PER_CLIENT.getDefaultValue()).intValue();
            timeWindowMillisPerClient = ((Long) RedisSessiondConfigProperty.REMOTE_SITE_RATELIMIT_TIME_WINDOW_MILLIS_PER_CLIENT.getDefaultValue()).longValue();
            overallMaxAccesses = ((Integer) RedisSessiondConfigProperty.REMOTE_SITE_RATELIMIT_OVERALL_MAX_ACCESSES.getDefaultValue()).intValue();
            overallTimeWindowMillis = ((Long) RedisSessiondConfigProperty.REMOTE_SITE_RATELIMIT_OVERALL_TIME_WINDOW_MILLIS.getDefaultValue()).longValue();
        }

        // Initialize per client rate limit
        if (maxAccessesPerClient <= 0 || timeWindowMillisPerClient <= 0) {
            bucketCache = null;
            LOG.info("No rate limit per client for remote site look-ups of sessions");
        } else {
            bucketCache = CacheBuilder.newBuilder().expireAfterAccess((long) (1.1 * timeWindowMillisPerClient), TimeUnit.MILLISECONDS).build(new CacheLoader<Key, Bucket>() {

                @Override
                public Bucket load(Key key) throws Exception {
                    return Bucket.builder().addLimit(limit -> limit.capacity(maxAccessesPerClient).refillGreedy(maxAccessesPerClient, Duration.ofMillis(timeWindowMillisPerClient))).build();
                }

            });
            LOG.info("Initialized rate limit per client for remote site look-ups of sessions: {} accesses per {}msec", Integer.valueOf(maxAccessesPerClient), Long.valueOf(timeWindowMillisPerClient));
        }

        // Initialize overall rate limit
        if (overallMaxAccesses <= 0 || overallTimeWindowMillis <= 0) {
            overallBucket = null;
            LOG.info("No overall rate limit for remote site look-ups of sessions");
        } else {
            overallBucket = Bucket.builder().addLimit(limit -> limit.capacity(overallMaxAccesses).refillGreedy(overallMaxAccesses, Duration.ofMillis(overallTimeWindowMillis))).build();
            LOG.info("Initialized overall rate limit for remote site look-ups of sessions: {} accesses per {}msec", Integer.valueOf(overallMaxAccesses), Long.valueOf(overallTimeWindowMillis));
        }
    }

    /**
     * Gets the overall bucket.
     *
     * @return The overall bucket
     */
    public Optional<Bucket> getOverallBucket() {
        return Optional.ofNullable(overallBucket);
    }

    /**
     * Gets the bucket for current calling thread.
     *
     * @return The bucket or empty
     */
    public Optional<Bucket> get() {
        if (bucketCache == null) {
            // Not enabled
            return Optional.empty();
        }

        // Gather variables for key
        int remotePort = parseIntElseZero(LogProperties.get(LogProperties.Name.GRIZZLY_REMOTE_PORT));
        if (remotePort == 0) {
            return Optional.empty();
        }
        String remoteAddr = LogProperties.get(LogProperties.Name.GRIZZLY_REMOTE_ADDRESS);
        if (Strings.isEmpty(remoteAddr)) {
            return Optional.empty();
        }
        String userAgent = LogProperties.get(LogProperties.Name.GRIZZLY_USER_AGENT);
        if (Strings.isEmpty(userAgent)) {
            return Optional.empty();
        }

        // Acquire bucket
        return Optional.of(bucketCache.getUnchecked(new Key(remotePort, remoteAddr, userAgent)));
    }

    private static int parseIntElseZero(String s) {
        if (Strings.isEmpty(s)) {
            return 0;
        }
        int i = Strings.parsePositiveInt(s.trim());
        return i > 0 ? i : 0;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** The key for a caller-specific bucket kept in registry */
    private static class Key {

        private final int remotePort;
        private final String remoteAddr;
        private final String userAgent;
        private final int hash;

        /**
         * Initializes a new {@link Key}.
         *
         * @param remotePort The remote port
         * @param remoteAddr The remote address
         * @param userAgent The User-Agent string
         */
        Key(int remotePort, String remoteAddr, String userAgent) {
            super();
            this.remotePort = remotePort;
            this.remoteAddr = remoteAddr;
            this.userAgent = userAgent;

            int prime = 31;
            int result = 1;
            result = prime * result + ((remoteAddr == null) ? 0 : remoteAddr.hashCode());
            result = prime * result + remotePort;
            result = prime * result + ((userAgent == null) ? 0 : userAgent.hashCode());
            this.hash = result;
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || this.getClass() != obj.getClass()) {
                return false;
            }
            final Key other = (Key) obj;
            if (remotePort != other.remotePort) {
                return false;
            }
            if (remoteAddr == null) {
                if (other.remoteAddr != null) {
                    return false;
                }
            } else if (!remoteAddr.equals(other.remoteAddr)) {
                return false;
            }
            if (userAgent == null) {
                if (other.userAgent != null) {
                    return false;
                }
            } else if (!userAgent.equals(other.userAgent)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder(256);
            builder.append("Key [");
            if (remotePort > 0) {
                builder.append("remotePort=").append(remotePort).append(", ");
            }
            if (remoteAddr != null) {
                builder.append("remoteAddr=").append(remoteAddr).append(", ");
            }
            if (userAgent != null) {
                builder.append("userAgent=").append(userAgent).append(", ");
            }
            builder.append(']');
            return builder.toString();
        }
    } // ENd of class Key

}
