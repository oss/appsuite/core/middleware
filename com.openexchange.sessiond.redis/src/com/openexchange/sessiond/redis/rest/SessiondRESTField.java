/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.sessiond.redis.rest;

/**
 * {@link SessiondRESTField} - Enumeration of known JSON field names.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public enum SessiondRESTField {

    /**
     * The <code>"sessionIds"</code> field; value is expected to be a JSON array.
     */
    SESSION_IDS("sessionIds"),
    /**
     * The <code>"contextIds"</code> field; value is expected to be a JSON array.
     */
    CONTEXT_IDS("contextIds"),
    /**
     * The <code>"contextId"</code> field; value is expected to be a JSON array.
     */
    CONTEXT_ID("contextId"),
    /**
     * The <code>"userId"</code> field; value is expected to be a JSON array.
     */
    USER_ID("userId"),
    /**
     * The <code>"users"</code> field; value is expected to be a JSON array.
     */
    USERS("users"),
    ;

    private final String fieldName;

    /**
     * Initializes a new {@link SessiondRESTField}.
     *
     * @param fieldName The field's name
     */
    private SessiondRESTField(String fieldName) {
        this.fieldName = fieldName;

    }

    /**
     * Gets the field name.
     *
     * @return The field name
     */
    public String getFieldName() {
        return fieldName;
    }
}
