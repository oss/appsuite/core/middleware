package com.openexchange.rest.services.capabilities.request.analyzer;

import java.util.List;
import java.util.Optional;
import javax.ws.rs.Path;
import com.openexchange.database.ConfigDatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.request.analyzer.utils.RequestAnalyzerUtils;
import com.openexchange.rest.services.capabilities.CapabilitiesRESTService;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * {@link CapabilitiesRESTRequestAnalyzer} is a {@link RequestAnalyzer} which uses the "{context}" path segment of the analyzed call to
 * the configuration servlet to determine the marker for the request.
 *
 * @author <a href="mailto:marcel.broecher@open-xchange.com">Marcel Broecher</a>
 */
public class CapabilitiesRESTRequestAnalyzer implements RequestAnalyzer {

    private final ErrorAwareSupplier<? extends ConfigDatabaseService> dbServiceSupplier;
    private final String servletPath;

    /**
     * Initializes a new {@link CapabilitiesRESTRequestAnalyzer}.
     *
     * @param dbServiceSupplier A supplier for the database service for resolving the database schema name during analysis
     */
    public CapabilitiesRESTRequestAnalyzer(ErrorAwareSupplier<? extends ConfigDatabaseService> dbServiceSupplier) {
        super();
        this.dbServiceSupplier = dbServiceSupplier;
        this.servletPath = CapabilitiesRESTService.class.getAnnotation(Path.class).value();
    }

    @Override
    public Optional<AnalyzeResult> analyze(RequestData data) throws OXException {
        if (false == "GET".equals(data.getMethod())) {
            // no preliminary capabilities request
            return Optional.empty();
        }
        Optional<String> path = data.getParsedURL().getPath();
        if (path.isEmpty() || false == path.get().startsWith(servletPath)) {
            // no preliminary capabilities request
            return Optional.empty();
        }
        List<String> pathSegments = data.getParsedURL().getPathSegments();
        if (null != pathSegments && 4 < pathSegments.size() && "all".equals(pathSegments.get(3))) {
            // /preliminary/capabilities/v1/all/{context}/{user}
            return Optional.of(RequestAnalyzerUtils.createAnalyzeResultFromCid(pathSegments.get(4), dbServiceSupplier));
        }
        // unknown, otherwise
        return Optional.of(AnalyzeResult.UNKNOWN);
    }
}
