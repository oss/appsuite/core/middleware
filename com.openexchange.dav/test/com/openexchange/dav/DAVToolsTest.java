/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.dav;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.exception.OXException;

/**
 * {@link DAVToolsTest}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @since v7.10.3
 */
public class DAVToolsTest {

    public static final String PREFIX_PATH_NAME = "com.openexchange.dav.prefixPath";
    public static final String PROXY_PREFIX_PATH_NAME = "com.openexchange.dav.proxyPrefixPath";

    @Mock
    private ConfigViewFactory factory;

    @Mock
    private ConfigView view;

    @InjectMocks
    private DAVTools davTools; // Assuming you have a class named DAVTools

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @ParameterizedTest
    @MethodSource("provideTestData")
    public void testCorrectPath(String prefixPath, String proxyPrefixPath, Map<String, String> rawToExpected) throws OXException {
        // Mock the behavior of ConfigView
        Mockito.when(factory.getView()).thenReturn(view);
        Mockito.when(view.get(PREFIX_PATH_NAME, String.class)).thenReturn(prefixPath);
        Mockito.when(view.get(PROXY_PREFIX_PATH_NAME, String.class)).thenReturn(proxyPrefixPath);

        for (Entry<String, String> entry : rawToExpected.entrySet()) {
            String path = DAVTools.getExternalPath(factory, entry.getKey());
            assertEquals(entry.getValue(), path, "Not the correct path");
        }
    }

    private static Stream<Arguments> provideTestData() {
        return Stream.of(
            Arguments.of(
                "/servlet/dav/",
                "/servlet/dav/",
                Map.ofEntries(
                    Map.entry("caldav", "/caldav"),
                    Map.entry("/caldav", "/caldav"),
                    Map.entry("/caldav/", "/caldav/"),
                    Map.entry("/photos/contactXY/image1.jpg", "/photos/contactXY/image1.jpg")
                )
            ),
            Arguments.of(
                "servlet/dav",
                "servlet/dav/",
                Map.ofEntries(
                    Map.entry("caldav", "/caldav"),
                    Map.entry("/caldav", "/caldav"),
                    Map.entry("/caldav/", "/caldav/"),
                    Map.entry("/photos/contactXY/image1.jpg", "/photos/contactXY/image1.jpg")
                )
            ),
            Arguments.of(
                "/servlet/dav/hidden/",
                "/servlet/dav/",
                Map.ofEntries(
                    Map.entry("caldav/", "/hidden/caldav/"),
                    Map.entry("/caldav", "/hidden/caldav"),
                    Map.entry("/caldav/", "/hidden/caldav/"),
                    Map.entry("/caldav/principals/foo/bar", "/hidden/caldav/principals/foo/bar"),
                    Map.entry("/caldav/principals/foo/bar/", "/hidden/caldav/principals/foo/bar/"),
                    Map.entry("/photos/contactXY/image1.jpg", "/hidden/photos/contactXY/image1.jpg")
                )
            ),
            Arguments.of(
                "/dav/",
                "/",
                Map.ofEntries(
                    Map.entry("caldav", "/dav/caldav"),
                    Map.entry("/caldav", "/dav/caldav"),
                    Map.entry("/caldav/", "/dav/caldav/"),
                    Map.entry("/photos/contactXY/image1.jpg", "/dav/photos/contactXY/image1.jpg")
                )
            ),
            Arguments.of(
                "/dav/",
                "",
                Map.ofEntries(
                    Map.entry("caldav", "/dav/caldav"),
                    Map.entry("/caldav", "/dav/caldav"),
                    Map.entry("/caldav/", "/dav/caldav/"),
                    Map.entry("/photos/contactXY/image1.jpg", "/dav/photos/contactXY/image1.jpg")
                )
            )
        );
    }
}
