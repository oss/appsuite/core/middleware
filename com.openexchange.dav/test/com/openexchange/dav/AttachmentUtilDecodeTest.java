/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.dav;

import static com.openexchange.dav.AttachmentUtils.decodeURI;
import static com.openexchange.java.Autoboxing.I;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.net.URI;
import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.groupware.attach.AttachmentMetadata;

/**
 * {@link AttachmentUtilDecodeTest}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @since v7.10.3
 */
public class AttachmentUtilDecodeTest {

    /**
     * Copied from {@link DAVTools}
     */
    public static final String PREFIX_PATH_NAME = "com.openexchange.dav.prefixPath";

    /**
     * Copied from {@link DAVTools}
     */
    public static final String PROXY_PREFIX_PATH_NAME = "com.openexchange.dav.proxyPrefixPath";

    public static Stream<Arguments> testData() {
        return Stream.of(Arguments.of("/servlet/dav/", "/servlet/dav/"),
                         Arguments.of("/servlet/dav/hidden/", "/servlet/dav/"),
                         Arguments.of("/dav/", "/"));
    }

    private String prefixPath;
    private String proxyprefixPath;

    @Mock
    private ConfigViewFactory factory;

    @Mock
    private ConfigView view;

    @BeforeEach
    public void setUp() throws Exception {
        /*
         * Initialize mocks
         */
        MockitoAnnotations.openMocks(this);

        // Mock used service classes
        Mockito.when(factory.getView()).thenReturn(view);
        Mockito.when(view.get(PREFIX_PATH_NAME, String.class)).thenReturn(prefixPath);
        Mockito.when(view.get(PROXY_PREFIX_PATH_NAME, String.class)).thenReturn(proxyprefixPath);
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void testDecode_noUri_fail(String proxyPrefixPath, String prefixPath) throws Exception {
        testSetup(proxyPrefixPath, prefixPath);
        assertThrows(IllegalArgumentException.class,() -> decodeURI(null, factory));
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void testDecode_emptyUri_fail(String proxyPrefixPath, String prefixPath) throws Exception {
        testSetup(proxyPrefixPath, prefixPath);
        URI uri = new URI("");
        assertThrows(IllegalArgumentException.class,() -> decodeURI(uri, factory));
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void testDecode_noAttachmentUri_fail(String proxyPrefixPath, String prefixPath) throws Exception {
        testSetup(proxyPrefixPath, prefixPath);
        URI uri = new URI(prefixPath + "caldav/foo");
        assertThrows(IllegalArgumentException.class,() -> decodeURI(uri, factory));
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void testDecode_noMetadata_fail(String proxyPrefixPath, String prefixPath) throws Exception {
        testSetup(proxyPrefixPath, prefixPath);
        URI uri = new URI(prefixPath + "attachments/");
        assertThrows(IllegalArgumentException.class,() -> decodeURI(uri, factory));
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void testDecode_wrongMetadata_fail(String proxyPrefixPath, String prefixPath) throws Exception {
        testSetup(proxyPrefixPath, prefixPath);
        URI uri = new URI(prefixPath + "attachments/foo");
        assertThrows(IllegalArgumentException.class,() -> decodeURI(uri, factory));
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void testDecode_wrongMetadataType_fail(String proxyPrefixPath, String prefixPath) throws Exception {
        testSetup(proxyPrefixPath, prefixPath);
        /*
         * Metadata format 1-1-1-a
         */
        URI uri = new URI(prefixPath + "attachments/MV8xXzFfYQ");
        assertThrows(IllegalArgumentException.class,() -> decodeURI(uri, factory));
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void testDecode_wrongMetadataSyntax_fail(String proxyPrefixPath, String prefixPath) throws Exception {
        testSetup(proxyPrefixPath, prefixPath);
        /*
         * Metadata format 1-1-1-a
         */
        URI uri = new URI(prefixPath + "attachments/MV8xXzFfYQ/foo");
        assertThrows(IllegalArgumentException.class,() -> decodeURI(uri, factory));
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void testDecode_validUri_MetadataParsed(String proxyPrefixPath, String prefixPath) throws Exception {
        testSetup(proxyPrefixPath, prefixPath);
        URI uri = new URI(prefixPath + "attachments/MS0xLTEtMQ");
        AttachmentMetadata metadata = decodeURI(uri, factory);

        Assertions.assertEquals(1, I(metadata.getId()));
        Assertions.assertEquals(1, I(metadata.getAttachedId()));
        Assertions.assertEquals(1, I(metadata.getFolderId()));
        Assertions.assertEquals(1, I(metadata.getModuleId()));
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void testDecode_validUriWithSlashSuffix_MetadataParsed(String proxyPrefixPath, String prefixPath) throws Exception {
        testSetup(proxyPrefixPath, prefixPath);
        URI uri = new URI(prefixPath + "attachments/MS0xLTEtMQ/");
        AttachmentMetadata metadata = decodeURI(uri, factory);

        Assertions.assertEquals(1, I(metadata.getId()));
        Assertions.assertEquals(1, I(metadata.getAttachedId()));
        Assertions.assertEquals(1, I(metadata.getFolderId()));
        Assertions.assertEquals(1, I(metadata.getModuleId()));
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void testDecode_completeValidUri_MetadataParsed(String proxyPrefixPath, String prefixPath) throws Exception {
        testSetup(proxyPrefixPath, prefixPath);
        URI uri = new URI(prefixPath + "attachments/MS0xLTEtMQ/jochen%20bahncard%202018.jpg");
        AttachmentMetadata metadata = decodeURI(uri, factory);

        Assertions.assertEquals(1, I(metadata.getId()));
        Assertions.assertEquals(1, I(metadata.getAttachedId()));
        Assertions.assertEquals(1, I(metadata.getFolderId()));
        Assertions.assertEquals(1, I(metadata.getModuleId()));
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void testDecode_validUriWithInvalidSuffix_MetadataParsed(String proxyPrefixPath, String prefixPath) throws Exception {
        testSetup(proxyPrefixPath, prefixPath);
        URI uri = new URI(prefixPath + "attachments/MS0xLTEtMQ/isNotRelevantForTheEndocding");
        AttachmentMetadata metadata = decodeURI(uri, factory);

        Assertions.assertEquals(1, I(metadata.getId()));
        Assertions.assertEquals(1, I(metadata.getAttachedId()));
        Assertions.assertEquals(1, I(metadata.getFolderId()));
        Assertions.assertEquals(1, I(metadata.getModuleId()));
    }


    private void testSetup(String proxyPrefixPath, String prefixPath) {
        this.prefixPath = prefixPath;
        this.proxyprefixPath  = proxyPrefixPath;
    }

}
