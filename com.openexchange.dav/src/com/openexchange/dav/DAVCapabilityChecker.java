/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.dav;

import com.openexchange.capabilities.CapabilityChecker;
import com.openexchange.capabilities.CapabilitySet;
import com.openexchange.config.cascade.ComposedConfigProperty;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.userconfiguration.Permission;
import com.openexchange.oauth.provider.resourceserver.scope.AbstractScopeProvider;
import com.openexchange.oauth.provider.resourceserver.scope.OAuthScopeProvider;
import com.openexchange.session.Session;
import com.openexchange.tools.functions.ErrorAwareSupplier;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tools.session.ServerSessionAdapter;

/**
 * {@link DAVCapabilityChecker} - Checker for the <code>caldav</code> or <code>carddav</code> capability
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class DAVCapabilityChecker implements CapabilityChecker {

    private final Permission permission;
    private final String propertyName;
    private final ErrorAwareSupplier<ConfigViewFactory> configViewFactorySupplier;

    /**
     * Initializes a new {@link DAVCapabilityChecker}.
     * 
     * @param permission The permission whose derived capability is checked
     * @param propertyName The name of the 'enabled' property to check view config cascade
     * @param configViewFactorySupplier A supplier for the config view factory
     */
    public DAVCapabilityChecker(Permission permission, String propertyName, ErrorAwareSupplier<ConfigViewFactory> configViewFactorySupplier) {
        super();
        this.permission = permission;
        this.propertyName = propertyName;
        this.configViewFactorySupplier = configViewFactorySupplier;
    }

    /**
     * Gets the name of the checked capability.
     * 
     * @return The capability name
     */
    public String getCapbilityName() {
        return permission.getCapabilityName();
    }

    /**
     * Initializes a new {@link OAuthScopeProvider} based on the checked capability.
     *
     * @param scope The OAuth scope to use
     */
    public OAuthScopeProvider createOAuthScopeProvider(DAVOAuthScope scope) {
        String capabilityName = getCapbilityName();
        return new AbstractScopeProvider(scope.getScope(), scope.getDescription()) {
            
            @Override
            public boolean canBeGranted(CapabilitySet capabilities) {
                return capabilities.contains(capabilityName);
            }
        };
    }

    @Override
    public boolean isEnabled(String capability, Session session) throws OXException {
        if (false == permission.getCapabilityName().equals(capability) || null == session || 1 > session.getUserId() || 1 > session.getContextId()) {
            return false;
        }
        /*
         * check via config cascade
         */
        ConfigViewFactory configViewFactory = configViewFactorySupplier.get();
        if (null != configViewFactory) {
            ConfigView configView = configViewFactory.getView(session.getUserId(), session.getContextId());
            ComposedConfigProperty<String> configProperty = configView.property(propertyName, String.class);
            if (null != configProperty) {
                return Boolean.parseBoolean(configProperty.get());
            }
        }
        /*
         * fall back to user permission (same semantics as previous "property handler" in c.o.capabilities.internal.AbstractCapabilityService)
         */
        ServerSession serverSession = ServerSessionAdapter.valueOf(session);
        return serverSession.getUserPermissionBits().hasPermission(permission);
    }

}
