/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.dav;


/**
 * {@link DAVOAuthScope} - The scope tokens known by DAV module.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.0
 */
public enum DAVOAuthScope {

    /**
     * The OAuth scope token for CalDAV: <code>caldav</code>
     */
    CALDAV("caldav", OAuthStrings.SYNC_CALENDAR),
    /**
     * The OAuth scope token for CardDAV: <code>carddav</code>
     */
    CARDDAV("carddav", OAuthStrings.SYNC_CONTACTS),
    ;

    private final String scope;
    private final String description;

    /**
     * Initializes a new {@link DAVOAuthScope}.
     * 
     * @param scope The scope token
     * @param description The tokens description as localizable string
     */
    private DAVOAuthScope(String scope, String description) {
        this.scope = scope;
        this.description = description;
    }

    /**
     * Gets the scope token.
     *
     * @return The scope token
     */
    public String getScope() {
        return scope;
    }

    /**
     * Gets the tokens description as localizable string
     * 
     * @return The tokens description as localizable string
     */
    public String getDescription() {
        return description;
    }

}
