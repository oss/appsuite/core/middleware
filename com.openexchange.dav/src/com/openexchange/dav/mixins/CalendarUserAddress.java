/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.dav.mixins;

/**
 * {@link CalendarUserAddress} - Calendar user address URI along with 'preferred' marker.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class CalendarUserAddress {

    private final String uri;
    private final boolean preferred;

    /**
     * Initializes a new {@link CalendarUserAddress}.
     * 
     * @param uri The calendar user address URI
     */
    CalendarUserAddress(String uri) {
        this(uri, false);
    }

    /**
     * Initializes a new {@link CalendarUserAddress}.
     * 
     * @param uri The calendar user address URI
     * @param preferred <code>true</code> if it is the preferred address, <code>false</code>, otherwise
     */
    CalendarUserAddress(String uri, boolean preferred) {
        super();
        this.uri = uri;
        this.preferred = preferred;
    }

    /**
     * Gets the calendar user address URI.
     * 
     * @return The URI
     */
    public String getUri() {
        return uri;
    }

    /**
     * Gets a value indicating whether this calendar user address is the <i>preferred</i> one.
     * 
     * @return <code>true</code> if preferred, <code>false</code>, otherwise
     */
    public boolean isPreferred() {
        return preferred;
    }

    @Override
    public String toString() {
        return "CalendarUserAddress [uri=" + uri + ", preferred=" + preferred + "]";
    }

}
