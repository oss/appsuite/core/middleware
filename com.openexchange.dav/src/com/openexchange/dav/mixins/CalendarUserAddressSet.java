/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.dav.mixins;

import static com.openexchange.mail.alias.UserAliasUtility.isAlias;
import static com.openexchange.tools.arrays.Arrays.forEach;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.LoggerFactory;
import com.openexchange.chronos.ResourceId;
import com.openexchange.chronos.common.CalendarUtils;
import com.openexchange.chronos.service.CalendarUtilities;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.dav.DAVFactory;
import com.openexchange.dav.DAVProtocol;
import com.openexchange.exception.OXException;
import com.openexchange.group.Group;
import com.openexchange.java.Strings;
import com.openexchange.resource.Resource;
import com.openexchange.user.User;
import com.openexchange.webdav.protocol.helpers.SingleXMLPropertyMixin;

/**
 * {@link CalendarUserAddressSet}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class CalendarUserAddressSet extends SingleXMLPropertyMixin {

    /** Comparator for calendar user addresses */
    private static final Comparator<CalendarUserAddress> USER_ADDRESS_COMPARATOR = (address1, address2) -> {
        if (null == address2) {
            return null == address1 ? 0 : 1;
        } else if (null == address1) {
            return -1;
        }
        if (address1.isPreferred() && false == address2.isPreferred()) {
            return -1;
        } else if (false == address1.isPreferred() && address2.isPreferred()) {
            return 1;
        }
        if (address1.getUri().startsWith("mailto:") && false == address2.getUri().startsWith("mailto:")) {
            return -1;
        } else if (false == address1.getUri().startsWith("mailto:") && address2.getUri().startsWith("mailto:")) {
            return 1;
        }
        return address1.getUri().compareTo(address2.getUri());
    };

    private final List<CalendarUserAddress> addresses;

    /**
     * Initializes a new {@link CalendarUserAddressSet}.
     *
     * @param factory The underlying DAV factory
     * @param user The user
     * @param configViewFactory The configuration view
     * @param reverseOrder <code>true</code> to list the addresses in reverse order, <code>false</code> to use the default order
     */
    public CalendarUserAddressSet(DAVFactory factory, User user, ConfigViewFactory configViewFactory, boolean reverseOrder) {
        this(getAddresses(factory, user, configViewFactory, reverseOrder));
    }

    /**
     * Initializes a new {@link CalendarUserAddressSet}.
     *
     * @param contextID The context identifier
     * @param group The group
     * @param configViewFactory The configuration view
     */
    public CalendarUserAddressSet(int contextID, Group group, ConfigViewFactory configViewFactory) {
        this(Arrays.asList(new CalendarUserAddress(PrincipalURL.forGroup(group.getIdentifier(), configViewFactory)), 
            new CalendarUserAddress(ResourceId.forGroup(contextID, group.getIdentifier()))));
    }

    /**
     * Initializes a new {@link CalendarUserAddressSet}.
     *
     * @param contextID The context identifier
     * @param resource The resource
     * @param configViewFactory The configuration view
     */
    public CalendarUserAddressSet(int contextID, Resource resource, ConfigViewFactory configViewFactory) {
        this(getAddresses(contextID, resource, configViewFactory));
    }

    private static List<CalendarUserAddress> getAddresses(int contextID, Resource resource, ConfigViewFactory configViewFactory) {
        List<CalendarUserAddress> addresses = new ArrayList<CalendarUserAddress>(3);
        if (Strings.isNotEmpty(resource.getMail())) {
            addresses.add(new CalendarUserAddress(CalendarUtils.getURI(resource.getMail()), true));
        }
        addresses.add(new CalendarUserAddress(PrincipalURL.forResource(resource.getIdentifier(), configViewFactory)));
        addresses.add(new CalendarUserAddress(ResourceId.forResource(contextID, resource.getIdentifier())));
        Collections.sort(addresses, USER_ADDRESS_COMPARATOR);
        return addresses;
    }

    private static List<CalendarUserAddress> getAddresses(DAVFactory factory, User user, ConfigViewFactory configViewFactory, boolean reverseOrder) {
        Set<String> addedUris = new HashSet<>();
        List<CalendarUserAddress> addresses = new ArrayList<>();
        CalendarUtilities calendarUtils = factory.getOptionalService(CalendarUtilities.class);
        if (null != calendarUtils) {
            String preferredAddress;
            try {
                preferredAddress = calendarUtils.getCalendarConfig(factory.getSession().getContextId()).getPreferredCalendarUserAddress(user.getId());
            } catch (OXException e) {
                LoggerFactory.getLogger(CalendarUserAddressSet.class).debug("Unable to get preferred calendar user address: {}", e.getMessage(), e);
                preferredAddress = null;
            }
            if (Strings.isNotEmpty(preferredAddress) && isAlias(preferredAddress, user.getMail(), user.getAliases())) {
                addIfAbsent(addedUris, addresses, preferredAddress, true);
            }
        }
        addIfAbsent(addedUris, addresses, user.getMail(), addresses.isEmpty());
        forEach(user.getAliases(), alias -> addIfAbsent(addedUris, addresses, alias, false));
        addresses.add(new CalendarUserAddress(PrincipalURL.forUser(user.getId(), configViewFactory)));
        addresses.add(new CalendarUserAddress(ResourceId.forUser(factory.getContext().getContextId(), user.getId())));
        Collections.sort(addresses, reverseOrder ? Collections.reverseOrder(USER_ADDRESS_COMPARATOR) : USER_ADDRESS_COMPARATOR);
        return addresses;
    }

    private static void addIfAbsent(Set<String> addedUris, List<CalendarUserAddress> addresses, String address, boolean prefer) {
        if (Strings.isNotEmpty(address)) {
            String uri = CalendarUtils.getURI(address);
            if (addedUris.add(uri)) {
                addresses.add(new CalendarUserAddress(uri, prefer));
            }
        }
    }

    /**
     * Initializes a new {@link CalendarUserAddressSet}.
     *
     * @param addresses The possible calendar user addresses
     */
    private CalendarUserAddressSet(List<CalendarUserAddress> addresses) {
        super(DAVProtocol.CAL_NS.getURI(), "calendar-user-address-set");
        this.addresses = addresses;
    }

    @Override
    protected String getValue() {
        StringBuilder stringBuilder = new StringBuilder();
        for (CalendarUserAddress address : addresses) {
            stringBuilder.append(address.isPreferred() ? "<D:href preferred=\"1\">" : "<D:href>").append(address.getUri()).append("</D:href>");
        }
        return stringBuilder.toString();
    }

}
