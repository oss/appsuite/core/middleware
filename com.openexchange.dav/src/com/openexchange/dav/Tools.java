/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.dav;

import static com.openexchange.exception.StackTraceBlacklist.PROPERTY_EXCLUDE_STACKTRACE_FOR;
import static com.openexchange.java.Autoboxing.b;
import static com.openexchange.threadpool.ThreadPools.futureForResult;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.slf4j.LoggerFactory.getLogger;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import javax.servlet.http.HttpServletRequest;
import org.jdom2.Element;
import com.openexchange.ajax.response.IncludeStackTraceService;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.ConsumingReloadable;
import com.openexchange.config.Reloadable;
import com.openexchange.config.lean.DefaultProperty;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.config.lean.Property;
import com.openexchange.exception.OXException;
import com.openexchange.exception.StackTraceBlacklist;
import com.openexchange.folderstorage.UserizedFolder;
import com.openexchange.groupware.container.CommonObject;
import com.openexchange.java.Streams;
import com.openexchange.webdav.protocol.WebdavPath;

/**
 * {@link Tools}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class Tools {

    /** Lean configuration property whether a stacktrace should be included in errors or not */
    private static final Property PROPERTY_INCLUDE_STACKTRACE = DefaultProperty.valueOf("com.openexchange.ajax.response.includeStackTraceOnError", Boolean.FALSE);

    /** The custom header signaling that the client explicitly requests to include a stacktrace in error responses */
    private static final String HEADER_INCLUDE_STACKTRACE = "X-OX-INCLUDE-STACKTRACE-ON-ERROR";

    /** Holds the configured stacktrace blacklist */
    private static final AtomicReference<Future<StackTraceBlacklist>> STACK_TRACE_BLACKLIST = new AtomicReference<>();

    // prevent instantiation
    private Tools() {}

    public static Date getLatestModified(Date lastModified1, Date lastModified2) {
        return lastModified1.after(lastModified2) ? lastModified1 : lastModified2;
    }

    public static Date getLatestModified(Date lastModified, CommonObject object) {
        return getLatestModified(lastModified, object.getLastModified());
    }

    public static Date getLatestModified(Date lastModified, UserizedFolder folder) {
        return getLatestModified(lastModified, folder.getLastModifiedUTC());
    }

    /**
     * Parses a numerical identifier from a string, wrapping a possible
     * NumberFormatException into an OXException.
     *
     * @param id the id string
     * @return the parsed identifier
     * @throws OXException
     */
    public static int parse(String id) throws OXException {
        try {
            return Integer.parseInt(id);
        } catch (NumberFormatException e) {
            throw new OXException(e);
        }
    }

    /**
     * Gets the resource name from an url, i.e. the path's name without the
     * filename extension.
     *
     * @param url the webdav path
     * @param fileExtension the extension
     * @return the resource name
     */
    public static String extractResourceName(WebdavPath url, String fileExtension) {
        return null != url ? extractResourceName(url.name(), fileExtension) : null;
    }

    /**
     * Gets the resource name from a filename, i.e. the resource name without the
     * filename extension.
     *
     * @param filename the filename
     * @param fileExtension the extension
     * @return the resource name
     */
    public static String extractResourceName(String filename, String fileExtension) {
        String name = filename;
        if (null != fileExtension) {
            String extension = fileExtension.toLowerCase();
            if (false == extension.startsWith(".")) {
                extension = "." + extension;
            }
            if (null != name && extension.length() < name.length() && name.toLowerCase().endsWith(extension)) {
                return name.substring(0, name.length() - extension.length());
            }
        }
        return name;
    }


    /**
     * Gets a value indicating whether an exception stack trace for the given {@link OXException} is allowed to be included in the
     * response, based on an optionally configured deny-list.
     *
     * @param e The exception to check
     * @param factory The DAV factory providing the context of the handled request
     * @return <code>true</code> if a stack trace is for the given exception is allowed to be included in the response, <code>false</code>, otherwise
     */
    static boolean isIncludeStackTraceAllowed(OXException e, DAVFactory factory) {
        return getStackTraceBlacklist(factory.getOptionalService(ConfigurationService.class)).isIncludeAllowed(e);
    }

    private static StackTraceBlacklist getStackTraceBlacklist(ConfigurationService configurationService) {
        Future<StackTraceBlacklist> f = STACK_TRACE_BLACKLIST.get();
        if (null == f) {
            // Not yet initialized
            if (null == configurationService) {
                // Missing required service. Return default instance...
                getLogger(Tools.class).debug("Unable to get configured stack trace exclusions, falling back to defaults.");
                return new StackTraceBlacklist(null);
            }
            // Initialize from service and remember result
            FutureTask<StackTraceBlacklist> ft = new FutureTask<>(() -> new StackTraceBlacklist(configurationService.getProperty(PROPERTY_EXCLUDE_STACKTRACE_FOR)));
            f = STACK_TRACE_BLACKLIST.compareAndExchange(null, ft);
            if (f == null) {
                f = ft;
                ft.run();
            }
        }
        return getFrom(f);
    }

    /**
     * Gets the computation result from specified future; waiting if necessary for the computation to complete.
     *
     * @param <V> The type of the result value
     * @param f The future to get the computation result from
     * @return The computation result
     * @throws RuntimeException If computation raises an error
     */
    private static <V> V getFrom(Future<V> f) {
        if (f == null) {
            return null;
        }

        try {
            return f.get();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new IllegalStateException(e);
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof RuntimeException rte) {
                throw rte;
            }
            throw new IllegalStateException(cause == null ? e : cause);
        }
    }

    /**
     * Initializes a reloadable implementation for the <code>Reloadable</code> instance for the {@link Tools}
     *
     * @return The <code>Reloadable</code> to handle reloads
     */
    public static Reloadable initReloadable() {
        Consumer<ConfigurationService> reloader = configService -> {
            String blacklist = configService.getProperty(PROPERTY_EXCLUDE_STACKTRACE_FOR);
            STACK_TRACE_BLACKLIST.set(futureForResult(new StackTraceBlacklist(blacklist)));
        };
        return new ConsumingReloadable(reloader, PROPERTY_EXCLUDE_STACKTRACE_FOR);
    }

    /**
     * Gets a value indicating whether an exception stack trace is requested to be included in the response - either explicitly by the
     * client via {@link #HEADER_INCLUDE_STACKTRACE}, or via configuration.
     *
     * @param request The currently server HTTP request
     * @param factory The DAV factory providing the context of the handled request
     * @return <code>true</code> if an exception stack trace is requested to be included in the response, <code>false</code>, otherwise
     */
    static boolean isIncludeStackTraceRequested(HttpServletRequest request, DAVFactory factory) {
        /*
         * check if requested via header
         */
        if (null != request && Boolean.parseBoolean(request.getHeader(HEADER_INCLUDE_STACKTRACE))) {
            return true;
        }
        /*
         * check if enabled for user via dedicated service
         */
        IncludeStackTraceService traceService = factory.getOptionalService(IncludeStackTraceService.class);
        if (null != traceService && traceService.isEnabled()) {
            try {
                if (traceService.includeStackTraceOnError(factory.getSession().getUserId(), factory.getSession().getContextId())) {
                    return true;
                }
            } catch (OXException x) {
                getLogger(Tools.class).warn("Unexepcted error checking whether stack trace should be included, falling back to configured value.", x);
            }
        }
        /*
         * check if enabled for user via config property
         */
        LeanConfigurationService configService = factory.getOptionalService(LeanConfigurationService.class);
        if (null != configService) {
            return configService.getBooleanProperty(factory.getSession().getUserId(), factory.getSession().getContextId(), PROPERTY_INCLUDE_STACKTRACE);
        }
        return b(PROPERTY_INCLUDE_STACKTRACE.getDefaultValue(Boolean.class));
    }

    /**
     * Gets a <code>DAV:error</code> element including additional information from the supplied {@link OXException}, aligned to the JSON
     * representation in the HTTP API (<code>com.openexchange.ajax.writer.ResponseWriter#writeException</code>).
     *
     * @param e The OX exception to include the details from
     * @param includeStackTrace <code>true</code> to include the exception stack trace, <code>false</code>, otherwise
     * @return The error element
     */
    static Element getErrorElement(OXException e, boolean includeStackTrace) {
        Element errorElement = new Element("error", DAVProtocol.DAV_NS);
        errorElement.addNamespaceDeclaration(DAVProtocol.OX_NS);
        errorElement // @formatter:off
            .addContent(new Element("error", DAVProtocol.OX_NS).setText(e.getDisplayMessage(Locale.US)))
            .addContent(new Element("category", DAVProtocol.OX_NS).setText(String.valueOf(e.getCategory())))
            .addContent(new Element("code", DAVProtocol.OX_NS).setText(e.getErrorCode()))
            .addContent(new Element("error_id", DAVProtocol.OX_NS).setText(e.getExceptionId()))
            .addContent(new Element("error_desc", DAVProtocol.OX_NS).setText(e.getSoleMessage()))
        ; // @formatter:on
        if (includeStackTrace) {
            try (ByteArrayOutputStream outputStream = Streams.newByteArrayOutputStream();
                PrintStream printStream = new PrintStream(outputStream, true, UTF_8)) {
                e.printStackTrace(printStream);
                errorElement.addContent(new Element("error_stack", DAVProtocol.OX_NS).setText(new String(outputStream.toByteArray(), UTF_8)));
            } catch (IOException x) {
                getLogger(Tools.class).warn("Unexepcted I/O exception while generating stacktrace", x);
            }
        }
        return errorElement;
    }

}
