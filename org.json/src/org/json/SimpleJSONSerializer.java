/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package org.json;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import org.json.helpers.UnsynchronizedByteArrayOutputStream;

/**
 * <p>A JSON serializer is responsible for producing complete and legal JSON texts (objects or arrays). It can serialize
 * a {@link Map} as a JSON object or any {@link Collection} or array as a JSON list. The keys of a {@code Map} are
 * always serialized as strings. Values within a {@code Map}, {@code Collection}, or array are converted to JSON values
 * as follows:</p>
 *
 * <table>
 *  <caption>Mapping of Java types to JSON types</caption>
 *  <thead>
 *      <tr>
 *          <th>Object type</th>
 *          <th>JSON representation</th>
 *      </tr>
 *  </thead>
 *  <tbody>
 *      <tr>
 *          <td>{@code null}</td>
 *          <td>{@code null}</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@link Map}</td>
 *          <td>{@code object}</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@link Collection}</td>
 *          <td>{@code list}</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@code byte[]}</td>
 *          <td>{@code list}</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@code short[]}</td>
 *          <td>{@code list}</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@code int[]}</td>
 *          <td>{@code list}</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@code long[]}</td>
 *          <td>{@code list}</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@code float[]}</td>
 *          <td>{@code list}</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@code double[]}</td>
 *          <td>{@code list}</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@code boolean[]}</td>
 *          <td>{@code list}</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@code char[]}</td>
 *          <td>{@code list}</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@link Object}{@code []}</td>
 *          <td>{@code list}</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@link Number}</td>
 *          <td>{@code number} (or {@code null})</td>
 *      </tr>
 *
 *      <tr>
 *          <td>{@link Boolean}</td>
 *          <td>{@code true} or {@code false} (or {@code null})</td>
 *      </tr>
 *
 *      <tr>
 *          <td>all others</td>
 *          <td>the result of {@code obj.toString()}</td>
 *      </tr>
 *  </tbody>
 * </table>
 *
 * @author <a href="https://github.com/jchambers">Jon Chambers</a>
 *
 * @see <a href="http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf">ECMA-404 The JSON Data Interchange Standard</a>
 */
public final class SimpleJSONSerializer {

    /**
     * Don't allow direct instantiation.
     *
     * @throws InstantiationException in all cases
     */
    private SimpleJSONSerializer() throws InstantiationException {
        throw new InstantiationException("SimpleJSONSerializer must not be instantiated directly.");
    }

    /**
     * Writes the given {@link Map} as a JSON object to the given {@link Writer}.
     * <p>
     * Note that the keys of the given map will be represented as {@code Strings} (via their {@link Object#toString() toString} method)
     * regardless of their actual type.
     *
     * @param map the map to write as a JSON text
     * @param out the {@code Writer} to which to write the JSON text
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonObject(final Map<?, ?> map, final Writer out) throws IOException {
        if (map == null) {
            out.append("null");
        } else if (map.isEmpty()) {
            out.append("{}");
        } else {
            out.append('{');

            boolean valuesWritten = false;

            for (final Map.Entry<?, ?> entry : map.entrySet()) {
                if (entry.getKey() == null) {
                    throw new NullPointerException("JSON object keys must not be null.");
                }

                if (valuesWritten) {
                    out.append(',');
                }

                writeJsonValue(entry.getKey().toString(), out);
                out.append(':');
                writeJsonValue(entry.getValue(), out);

                valuesWritten = true;
            }

            out.append('}');
        }
    }

    /**
     * Returns the given {@link Map} as a JSON content stream.
     * <p>
     * Note that the keys of the given map will be represented as {@code Strings} (via their {@link Object#toString() toString} method)
     * regardless of their actual type.
     *
     * @param map the map to return as a JSON content stream
     * @return a JSON content stream representing the given map
     */
    public static InputStream writeJsonTextAsStream(final Map<?, ?> map) {
        UnsynchronizedByteArrayOutputStream baos = new UnsynchronizedByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        try {
            writeJsonObject(map, out);
            out.flush();
            out.close();
            return baos.toByteArrayInputStream();
        } catch (final IOException e) {
            // This should never happen for a ByteArrayOutputStream
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes the given {@link Collection} as a JSON array to the given {@link Writer}.
     *
     * @param collection the collection to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON text
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonArray(final Collection<?> collection, final Writer out) throws IOException {
        if (collection == null) {
            out.append("null");
        } else if (collection.isEmpty()) {
            out.append("[]");
        } else {
            out.append('[');

            boolean valuesWritten = false;

            for (final Object o : collection) {
                if (valuesWritten) {
                    out.append(',');
                }

                writeJsonValue(o, out);

                valuesWritten = true;
            }

            out.append(']');
        }
    }

    /**
     * Returns the given {@link Collection} as a JSON content stream.
     *
     * @param collection the collection to return as a JSON content stream
     * @return a JSON content stream representing the given collection
     */
    public static InputStream writeJsonTextAsString(final Collection<?> collection) {
        UnsynchronizedByteArrayOutputStream baos = new UnsynchronizedByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        try {
            writeJsonArray(collection, out);
            out.flush();
            out.close();
             return baos.toByteArrayInputStream();
        } catch (final IOException e) {
            // This should never happen for a ByteArrayOutputStream
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes the given array of {@code bytes} as a JSON array to the given {@link Writer}.
     *
     * @param array the array to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON text
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonArray(final byte[] array, final Writer out) throws IOException {
        if (array == null) {
            out.append("null");
        } else if (array.length == 0) {
            out.append("[]");
        } else {
            out.append('[');
            out.append(String.valueOf(array[0]));

            for (int i = 1; i < array.length; i++) {
                out.append(',');
                out.append(String.valueOf(array[i]));
            }

            out.append(']');
        }
    }

    /**
     * Returns the given array of {@code bytes} as a JSON content stream.
     *
     * @param array the array to return as a JSON content stream
     * @return a JSON content stream representing the given array
     */
    public static InputStream writeJsonTextAsString(final byte[] array) {
        UnsynchronizedByteArrayOutputStream baos = new UnsynchronizedByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        try {
            writeJsonArray(array, out);
            out.flush();
            out.close();
             return baos.toByteArrayInputStream();
        } catch (final IOException e) {
            // This should never happen for a ByteArrayOutputStream
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes the given array of {@code shorts} as a JSON array to the given {@link Writer}.
     *
     * @param array the array to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON text
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonArray(final short[] array, final Writer out) throws IOException {
        if (array == null) {
            out.append("null");
        } else if (array.length == 0) {
            out.append("[]");
        } else {
            out.append('[');
            out.append(String.valueOf(array[0]));

            for (int i = 1; i < array.length; i++) {
                out.append(',');
                out.append(String.valueOf(array[i]));
            }

            out.append(']');
        }
    }

    /**
     * Returns the given array of {@code shorts} as a JSON content stream.
     *
     * @param array the array to return as a JSON content stream
     * @return a JSON content stream representing the given array
     */
    public static InputStream writeJsonTextAsString(final short[] array) {
        UnsynchronizedByteArrayOutputStream baos = new UnsynchronizedByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        try {
            writeJsonArray(array, out);
            out.flush();
            out.close();
             return baos.toByteArrayInputStream();
        } catch (final IOException e) {
            // This should never happen for a ByteArrayOutputStream
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes the given array of {@code ints} as a JSON array to the given {@link Writer}.
     *
     * @param array the array to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON text
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonArray(final int[] array, final Writer out) throws IOException {
        if (array == null) {
            out.append("null");
        } else if (array.length == 0) {
            out.append("[]");
        } else {
            out.append('[');
            out.append(Integer.toString(array[0]));

            for (int i = 1; i < array.length; i++) {
                out.append(',');
                out.append(Integer.toString(array[i]));
            }

            out.append(']');
        }
    }

    /**
     * Returns the given array of {@code ints} as a JSON content stream.
     *
     * @param array the array to return as a JSON content stream
     * @return a JSON content stream representing the given array
     */
    public static InputStream writeJsonTextAsString(final int[] array) {
        UnsynchronizedByteArrayOutputStream baos = new UnsynchronizedByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        try {
            writeJsonArray(array, out);
            out.flush();
            out.close();
             return baos.toByteArrayInputStream();
        } catch (final IOException e) {
            // This should never happen for a ByteArrayOutputStream
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes the given array of {@code longs} as a JSON array to the given {@link Writer}.
     *
     * @param array the array to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON text
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonArray(final long[] array, final Writer out) throws IOException {
        if (array == null) {
            out.append("null");
        } else if (array.length == 0) {
            out.append("[]");
        } else {
            out.append('[');
            out.append(Long.toString(array[0]));

            for (int i = 1; i < array.length; i++) {
                out.append(',');
                out.append(Long.toString(array[i]));
            }

            out.append(']');
        }
    }

    /**
     * Returns the given array of {@code longs} as a JSON content stream.
     *
     * @param array the array to return as a JSON content stream
     * @return a JSON content stream representing the given array
     */
    public static InputStream writeJsonTextAsString(final long[] array) {
        UnsynchronizedByteArrayOutputStream baos = new UnsynchronizedByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        try {
            writeJsonArray(array, out);
            out.flush();
            out.close();
             return baos.toByteArrayInputStream();
        } catch (final IOException e) {
            // This should never happen for a ByteArrayOutputStream
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes the given array of {@code floats} as a JSON array to the given {@link Writer}.
     *
     * @param array the array to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON text
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonArray(final float[] array, final Writer out) throws IOException {
        if (array == null) {
            out.append("null");
        } else if (array.length == 0) {
            out.append("[]");
        } else {
            out.append('[');
            out.append(Float.toString(array[0]));

            for (int i = 1; i < array.length; i++) {
                out.append(',');
                out.append(Float.toString(array[i]));
            }

            out.append(']');
        }
    }

    /**
     * Returns the given array of {@code floats} as a JSON content stream.
     *
     * @param array the array to return as a JSON content stream
     * @return a JSON content stream representing the given array
     */
    public static InputStream writeJsonTextAsString(final float[] array) {
        UnsynchronizedByteArrayOutputStream baos = new UnsynchronizedByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        try {
            writeJsonArray(array, out);
            out.flush();
            out.close();
             return baos.toByteArrayInputStream();
        } catch (final IOException e) {
            // This should never happen for a ByteArrayOutputStream
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes the given array of {@code doubles} as a JSON array to the given {@link Writer}.
     *
     * @param array the array to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON text
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonArray(final double[] array, final Writer out) throws IOException {
        if (array == null) {
            out.append("null");
        } else if (array.length == 0) {
            out.append("[]");
        } else {
            out.append('[');
            out.append(Double.toString(array[0]));

            for (int i = 1; i < array.length; i++) {
                out.append(',');
                out.append(Double.toString(array[i]));
            }

            out.append(']');
        }
    }

    /**
     * Returns the given array of {@code doubles} as a JSON content stream.
     *
     * @param array the array to return as a JSON content stream
     * @return a JSON content stream representing the given array
     */
    public static InputStream writeJsonTextAsString(final double[] array) {
        UnsynchronizedByteArrayOutputStream baos = new UnsynchronizedByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        try {
            writeJsonArray(array, out);
            out.flush();
            out.close();
             return baos.toByteArrayInputStream();
        } catch (final IOException e) {
            // This should never happen for a ByteArrayOutputStream
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes the given array of {@code booleans} as a JSON array to the given {@link Writer}.
     *
     * @param array the array to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON text
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonArray(final boolean[] array, final Writer out) throws IOException {
        if (array == null) {
            out.append("null");
        } else if (array.length == 0) {
            out.append("[]");
        } else {
            out.append('[');
            out.append(array[0] ? "true" : "false");

            for (int i = 1; i < array.length; i++) {
                out.append(',');
                out.append(array[i] ? "true" : "false");
            }

            out.append(']');
        }
    }

    /**
     * Returns the given array of {@code booleans} as a JSON content stream.
     *
     * @param array the array to return as a JSON content stream
     * @return a JSON content stream representing the given array
     */
    public static InputStream writeJsonTextAsString(final boolean[] array) {
        UnsynchronizedByteArrayOutputStream baos = new UnsynchronizedByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        try {
            writeJsonArray(array, out);
            out.flush();
            out.close();
             return baos.toByteArrayInputStream();
        } catch (final IOException e) {
            // This should never happen for a ByteArrayOutputStream
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes the given array of characters as a JSON array to the given {@link Writer}.
     *
     * @param array the array to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON text
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonArray(final char[] array, final Writer out) throws IOException {
        if (array == null) {
            out.append("null");
        } else if (array.length == 0) {
            out.append("[]");
        } else {
            out.append('[');
            writeJsonValue(array[0], out);

            for (int i = 1; i < array.length; i++) {
                out.append(',');
                writeJsonValue(array[i], out);
            }

            out.append(']');
        }
    }

    /**
     * Returns the given array of {@code chars} as a JSON content stream.
     *
     * @param array the array to return as a JSON content stream
     * @return a JSON content stream representing the given array
     */
    public static InputStream writeJsonTextAsString(final char[] array) {
        UnsynchronizedByteArrayOutputStream baos = new UnsynchronizedByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        try {
            writeJsonArray(array, out);
            out.flush();
            out.close();
             return baos.toByteArrayInputStream();
        } catch (final IOException e) {
            // This should never happen for a ByteArrayOutputStream
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes the given array of {@link Object Objects} as a JSON array to the given {@link Writer}.
     *
     * @param array the array to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON text
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonArray(final Object[] array, final Writer out) throws IOException {
        if (array == null) {
            out.append("null");
        } else if (array.length == 0) {
            out.append("[]");
        } else {
            out.append('[');
            writeJsonValue(array[0], out);

            for (int i = 1; i < array.length; i++) {
                out.append(',');
                writeJsonValue(array[i], out);
            }

            out.append(']');
        }
    }

    /**
     * Returns the given array of {@code Objects} as a JSON content stream.
     *
     * @param array the array to return as a JSON content stream
     * @return a JSON content stream representing the given array
     */
    public static InputStream writeJsonTextAsString(final Object[] array) {
        UnsynchronizedByteArrayOutputStream baos = new UnsynchronizedByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        try {
            writeJsonArray(array, out);
            out.flush();
            out.close();
             return baos.toByteArrayInputStream();
        } catch (final IOException e) {
            // This should never happen for a ByteArrayOutputStream
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes the given object as a JSON value.
     *
     * @param o the object to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON value
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonValue(final Object o, final Writer out) throws IOException {
        if (o == null) {
            out.append("null");
        } else if (o instanceof Optional) {
            Optional<?> optional = (Optional<?>) o;
            if (optional.isPresent()) {
                writeJsonValue(optional.get(), out);
            } else {
                writeJsonValue((Object) null, out);
            }
        } else if (o instanceof Map) {
            writeJsonObject((Map<?, ?>) o, out);
        } else if (o instanceof Collection) {
            writeJsonArray((Collection<?>) o, out);
        } else if (o instanceof String) {
            writeJsonValue((String) o, out);
        } else if (o instanceof Number) {
            writeJsonValue((Number) o, out);
        } else if (o instanceof Boolean) {
            writeJsonValue((Boolean) o, out);
        } else if (JSONCoercion.isArray(o)) {
            if (o instanceof byte[]) {
                writeJsonArray((byte[]) o, out);
            } else if (o instanceof short[]) {
                writeJsonArray((short[]) o, out);
            } else if (o instanceof int[]) {
                writeJsonArray((int[]) o, out);
            } else if (o instanceof long[]) {
                writeJsonArray((long[]) o, out);
            } else if (o instanceof float[]) {
                writeJsonArray((float[]) o, out);
            } else if (o instanceof double[]) {
                writeJsonArray((double[]) o, out);
            } else if (o instanceof boolean[]) {
                writeJsonArray((boolean[]) o, out);
            } else if (o instanceof char[]) {
                writeJsonArray((char[]) o, out);
            } else if (o instanceof Object[]) {
                writeJsonArray((Object[]) o, out);
            } else {
                // As last resort
                writeJsonValue(o.toString(), out);
            }
        } else {
            // As last resort
            writeJsonValue(o.toString(), out);
        }
    }

    /**
     * Writes the given {@link String} as a JSON string.
     *
     * @param string the string to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON value
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonValue(final String string, final Writer out) throws IOException {

        if (string == null) {
            out.append("null");
        } else if ("".equals(string)) {
            out.append("\"\"");
        } else {

            out.append('\"');

            int start = 0;

            // The strategy here is to make as few writes as possible by looking for substrings of characters that don't
            // need to be escaped. Whenever we hit a character that needs to be escaped, we write all of the
            // previously-unwritten characters up to that point, then add the escaped version of that character.
            for (int i = 0; i < string.length(); i++) {
                final char c = string.charAt(i);

                if (c == '"' || c == '\\' || c == '/' || c == '\b' || c == '\f' || c == '\n' || c == '\r' || c == '\t' ||
                        Character.isISOControl(c)) {

                    out.append(string, start, i);
                    appendEscapedCharacter(c, out);

                    start = i + 1;
                }
            }

            if (start == 0) {
                // We hit the end of the string without appending anything
                out.append(string);
            } else if (start < string.length()) {
                out.append(string, start, string.length());
            }

            out.append('\"');
        }
    }

    /**
     * Writes the given {@code char} as a JSON string.
     *
     * @param c the character to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON value
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonValue(final char c, final Writer out) throws IOException {
        out.append('"');
        appendEscapedCharacter(c, out);
        out.append('"');
    }

    /**
     * Appends the given {@code char} to the given {@link Writer}, escaping it if necessary.
     *
     * @param c the character to append
     * @param out the {@code Writer} to which to write the given character
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    private static void appendEscapedCharacter(final char c, final Writer out) throws IOException {
        switch (c) {
            case '"':
                out.append("\\\"");
                break;

            case '\\':
                out.append("\\\\");
                break;

            case '/':
                out.append("\\/");
                break;

            case '\b':
                out.append("\\b");
                break;

            case '\f':
                out.append("\\f");
                break;

            case '\n':
                out.append("\\n");
                break;

            case '\r':
                out.append("\\r");
                break;

            case '\t':
                out.append("\\t");
                break;

            default:
                if (Character.isISOControl(c)) {
                    out.append("\\u");
                    out.append(Integer.toHexString(0x10000 | c), 1, 5);
                } else {
                    out.append(c);
                }

                break;
        }
    }

    /**
     * Writes the given {@link Number} as a JSON number.
     *
     * @param number the number to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON value
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonValue(final Number number, final Writer out) throws IOException {
        if (number == null) {
            out.append("null");
        } else if (number instanceof Float) {
            // Floats can have NaN or infinite values; pass them off to a dedicated handler
            writeJsonValue(number.floatValue(), out);
        } else if (number instanceof Double) {
            // Doubles can have NaN or infinite values; pass them off to a dedicated handler
            writeJsonValue(number.doubleValue(), out);
        } else {
            out.append(number.toString());
        }
    }

    /**
     * Writes the given {@code byte} as a JSON number.
     *
     * @param b the byte to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON value
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonValue(final byte b, final Writer out) throws IOException {
        out.append(Byte.toString(b));
    }

    /**
     * Writes the given {@code short} as a JSON number.
     *
     * @param s the short to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON value
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonValue(final short s, final Writer out) throws IOException {
        out.append(Short.toString(s));
    }

    /**
     * Writes the given {@code int} as a JSON number.
     *
     * @param i the integer to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON value
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonValue(final int i, final Writer out) throws IOException {
        out.append(Integer.toString(i));
    }

    /**
     * Writes the given {@code long} as a JSON number.
     *
     * @param l the long to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON value
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonValue(final long l, final Writer out) throws IOException {
        out.append(Long.toString(l));
    }

    /**
     * Writes the given {@code float} as a JSON number. Note that {@code NaN} values and non-finite numbers cannot be
     * written as JSON values.
     *
     * @param f the float to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON value
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     * @throws IllegalArgumentException if the given float was not a number or was not finite
     */
    public static void writeJsonValue(final float f, final Writer out) throws IOException {
        if (Float.isInfinite(f)) {
            throw new IllegalArgumentException("Cannot write non-finite numbers as JSON values.");
        }

        if (Float.isNaN(f)) {
            throw new IllegalArgumentException("Cannot write NaN as a JSON value.");
        }

        out.append(Float.toString(f));
    }

    /**
     * Writes the given {@code double} as a JSON number. Note that {@code NaN} values and non-finite numbers cannot be
     * written as JSON values.
     *
     * @param d the double to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON value
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     * @throws IllegalArgumentException if the given double was not a number or was not finite
     */
    public static void writeJsonValue(final double d, final Writer out) throws IOException {
        if (Double.isInfinite(d)) {
            throw new IllegalArgumentException("Cannot write non-finite numbers as JSON values.");
        }

        if (Double.isNaN(d)) {
            throw new IllegalArgumentException("Cannot write NaN as a JSON value.");
        }

        out.append(Double.toString(d));
    }

    /**
     * Writes the given {@link Boolean} as a JSON value.
     *
     * @param b the boolean to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON value
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonValue(final Boolean b, final Writer out) throws IOException {
        if (b == null) {
            out.append("null");
        } else {
            writeJsonValue(b.booleanValue(), out);
        }
    }

    /**
     * Writes the given {@code boolean} as a JSON value.
     *
     * @param b the boolean to write as a JSON value
     * @param out the {@code Writer} to which to write the JSON value
     * @throws IOException in the event of an I/O error when writing to the given {@code Writer}
     */
    public static void writeJsonValue(final boolean b, final Writer out) throws IOException {
        out.append(b ? "true" : "false");
    }
}

