/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package org.json;

/**
 * {@link NotAJSONValueException} - Thrown to indicate that a given input cannot be parsed as a JSON object nor as a JSON array.
 * <p>
 * Input does neither start with <code>'{'</code> nor <code>'['</code> character.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class NotAJSONValueException extends JSONException {

    private static final long serialVersionUID = -740614846023423019L;

    /**
     * Initializes a new instance of {@link NotAJSONValueException}.
     *
     * @param message The detail message
     */
    public NotAJSONValueException(String message) {
        super(message);
    }

    /**
     * Initializes a new instance of {@link NotAJSONValueException}.
     *
     * @param message The detail message
     * @param t The cause
     */
    public NotAJSONValueException(String message, Throwable t) {
        super(message, t);
    }

    /**
     * Initializes a new instance of {@link NotAJSONValueException}.
     *
     * @param t The cause
     */
    public NotAJSONValueException(Throwable t) {
        super(t);
    }

}
