/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.push.dovecot.requestanalyzer;

import java.io.IOException;
import java.util.Optional;
import javax.ws.rs.Path;
import org.json.JSONException;
import org.json.JSONServices;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.push.dovecot.rest.DovecotPushRESTService;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.BodyData;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.segment.SegmentMarker;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * {@link DovecotPushRESTRequestAnalyzer}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public class DovecotPushRESTRequestAnalyzer implements RequestAnalyzer {

    private final ErrorAwareSupplier<DatabaseService> dbServiceSupplier;
    private final String servletPath;

    /**
     * Initializes a new {@link DovecotPushRESTRequestAnalyzer}.
     *
     * @param services A service lookup reference yielding the {@link DatabaseService}.
     */
    public DovecotPushRESTRequestAnalyzer(ServiceLookup services) {
        super();
        this.dbServiceSupplier = () -> services.getServiceSafe(DatabaseService.class);
        this.servletPath = DovecotPushRESTService.class.getAnnotation(Path.class).value();
    }

    @Override
    public Optional<AnalyzeResult> analyze(RequestData data) throws OXException {
        if (false == "PUT".equals(data.getMethod())) {
            // No dovecot push request
            return Optional.empty();
        }
        Optional<String> path = data.getParsedURL().getPath();
        if (path.isEmpty() || false == path.get().startsWith(servletPath)) {
            // No dovecot push request
            return Optional.empty();
        }
        Optional<BodyData> body = data.optBody();
        if (body.isEmpty()) {
            return Optional.of(AnalyzeResult.MISSING_BODY);
        }
        try {
            int[] userAndContext = DovecotPushRESTService.parseUserAndContext(JSONServices.parseObject(body.get().getDataAsString()));
            if (null == userAndContext || 2 != userAndContext.length) {
                // No context id found
                return Optional.of(AnalyzeResult.UNKNOWN);
            }
            String schema = dbServiceSupplier.get().getSchemaName(userAndContext[1]);
            return Optional.of(new AnalyzeResult(SegmentMarker.of(schema), null));
        } catch (JSONException | IOException | NumberFormatException e) {
            org.slf4j.LoggerFactory.getLogger(DovecotPushRESTRequestAnalyzer.class).debug("Ignoring unparseable push notification body.", e);
            return Optional.of(AnalyzeResult.UNKNOWN);
        }
    }

}
