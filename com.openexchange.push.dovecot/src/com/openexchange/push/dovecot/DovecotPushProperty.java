/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.push.dovecot;

import com.openexchange.config.lean.Property;

/**
 * {@link DovecotPushProperty}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public enum DovecotPushProperty implements Property {

    /**
     * Specifies what system to use to manage a cluster-lock
     * <p>
     * Possible values
     * <ul>
     * <li>"db" for database-based locking
     * <li>"hz" for Hazelcast-based locking (default)
     * <li>"none" for no cluster lock mechanism.<br>
     *      Only applicable if property <code>"com.openexchange.push.dovecot.stateless"</code> is set to <code>"false"</code>
     * </ul>
     */
    CLUSTER_LOCK("clusterLock", "hz"),
    /**
     * Whether to use stateless implementation.
     */
    STATELESS("stateless", Boolean.TRUE),
    /**
     * Whether to prefer Doveadm to issue METADATA commands.
     */
    PREFER_DOVEADM_FOR_METADATA("preferDoveadmForMetadata", Boolean.FALSE),
    /**
     * Enables or disables implicit de-registration of push listeners during user/context delete.
     */
    UNREGISTER_AFTER_DELETE("unregisterAfterDelete", Boolean.TRUE),
    ;

    private final String fqn;
    private final Object defaultValue;

    /**
     * Initializes a new {@link DovecotPushProperty}.
     *
     * @param appendix The appendix for the fully-qualifying name
     * @param defaultValue The default value
     */
    private DovecotPushProperty(String appendix, Object defaultValue) {
        this.fqn = "com.openexchange.push.dovecot." + appendix;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
