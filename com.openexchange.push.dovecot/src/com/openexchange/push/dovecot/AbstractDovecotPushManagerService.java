/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.push.dovecot;

import static com.openexchange.java.Autoboxing.I;
import org.slf4j.Logger;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.config.cascade.ConfigViews;
import com.openexchange.dovecot.doveadm.client.DoveAdmClient;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.log.LogProperties;
import com.openexchange.mail.api.AuthType;
import com.openexchange.mail.api.MailConfig;
import com.openexchange.mailaccount.AccountNature;
import com.openexchange.push.PushExceptionCodes;
import com.openexchange.push.PushListenerService;
import com.openexchange.push.PushManagerExtendedService;
import com.openexchange.push.PushUser;
import com.openexchange.push.PushUtility;
import com.openexchange.push.dovecot.registration.RegistrationContext;
import com.openexchange.push.dovecot.registration.RegistrationContext.DoveAdmClientProvider;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessionMatcher;
import com.openexchange.sessiond.SessiondService;


/**
 * {@link AbstractDovecotPushManagerService}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.6.2
 */
public abstract class AbstractDovecotPushManagerService implements PushManagerExtendedService {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(AbstractDovecotPushManagerService.class);

    /** The timeout in milliseconds to use when unregistering a push user */
    public static final long UNREGISTRATION_TIMEOUT = 3000L;

    private final String name;

    /** The bundle configuration */
    protected final DovecotPushConfiguration config;

    /** The service look-up */
    protected final ServiceLookup services;

    /**
     * Initializes a new {@link AbstractDovecotPushManagerService}.
     *
     * @param config The dovecot push configuration
     * @param services The service look-up
     */
    protected AbstractDovecotPushManagerService(DovecotPushConfiguration config, ServiceLookup services) {
        super();
        name = "Dovecot Push Manager";
        this.config = config;
        this.services = services;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean supportsPermanentListeners() {
        return true;
    }

    @Override
    public boolean listenersRequireResources() {
        return false;
    }

    /**
     * Checks if Dovecot Push is enabled for given user.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> if enabled; otherwise <code>false</code>
     * @throws OXException If check fails
     */
    protected boolean isDovecotPushEnabledFor(int userId, int contextId) throws OXException {
        ConfigViewFactory factory = services.getOptionalService(ConfigViewFactory.class);
        if (factory == null) {
            throw ServiceExceptionCode.absentService(ConfigViewFactory.class);
        }

        ConfigView view = factory.getView(userId, contextId);
        return ConfigViews.getDefinedBoolPropertyFrom("com.openexchange.push.dovecot.enabled", false, view);
    }

    /**
     * Checks if given user has a permanent push registration.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return <code>true</code> if given user has a permanent push registration; otherwise <code>false</code>
     */
    protected boolean hasPermanentPush(int userId, int contextId) {
        try {
            PushListenerService pushListenerService = services.getService(PushListenerService.class);
            return pushListenerService.hasRegistration(new PushUser(userId, contextId));
        } catch (Exception e) {
            LOGGER.warn("Failed to check for push registration for user {} in context {}", I(userId), I(contextId), e);
            return false;
        }
    }

    @Override
    public boolean isUnregisterAfterDelete() {
        return config.unregisterAfterDelete();
    }

    /**
     * Generates a session for specified push user.
     *
     * @param pushUser The push user
     * @return The generated session
     * @throws OXException If session cannot be generated
     */
    protected Session generateSessionFor(PushUser pushUser) throws OXException {
        PushListenerService pushListenerService = services.getService(PushListenerService.class);
        return pushListenerService.generateSessionFor(pushUser);
    }

    /**
     * Looks-up a push-capable session for specified user.
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param optOldSession The optional old session (which expired)
     * @return A push-capable session or <code>null</code>
     */
    public Session lookUpSessionFor(int userId, int contextId, Session optOldSession) {
        // Look-up sessions
        SessiondService sessiondService = services.getService(SessiondService.class);
        if (null != sessiondService) {
            final String oldSessionId = null == optOldSession ? null : optOldSession.getSessionID();

            SessionMatcher matcher = new SessionMatcher() {

                @Override
                public boolean accepts(Session session) {
                    return (oldSessionId == null || !oldSessionId.equals(session.getSessionID())) && PushUtility.allowedClient(session.getClient(), session, true);
                }
            };
            Session anotherActiveSession = sessiondService.findFirstMatchingSessionForUser(userId, contextId, matcher);
            if (anotherActiveSession != null) {
                return anotherActiveSession;
            }
        }

        return null;
    }

    public abstract void unregisterForDeletedUser(PushUser pushUser) throws OXException;

    // -------------------------------------------------------------------------------------------------------------------------------------

    /** Implements a <code>DoveAdmClientProvider</code> based on a service look-up */
    public static class ServiceLookupDoveAdmClientProvider implements DoveAdmClientProvider {

        private final ServiceLookup services;

        /**
         * Initializes a new instance based on given service look-up.
         */
        public ServiceLookupDoveAdmClientProvider(ServiceLookup services) {
            super();
            this.services = services;
        }

        @Override
        public DoveAdmClient getDoveAdmClient() throws OXException {
            DoveAdmClient doveAdmClient = services.getOptionalService(DoveAdmClient.class);
            if (null == doveAdmClient) {
                throw ServiceExceptionCode.absentService(DoveAdmClient.class);
            }
            return doveAdmClient;
        }
    }

    protected RegistrationContext getRegistrationContext(Session session) {
        DoveAdmClient doveAdmClient = config.preferDoveadmForMetadata() ? services.getOptionalService(DoveAdmClient.class): null;
        return null == doveAdmClient ? RegistrationContext.createSessionContext(session) : RegistrationContext.createDoveAdmClientContext(session.getUserId(), session.getContextId(), new ServiceLookupDoveAdmClientProvider(services));
    }

    protected RegistrationContext getRegistrationContext(PushUser pushUser) throws OXException {
        DoveAdmClient doveAdmClient = services.getOptionalService(DoveAdmClient.class);
        if (config.preferDoveadmForMetadata() && doveAdmClient != null) {
            return RegistrationContext.createDoveAdmClientContext(pushUser.getUserId(), pushUser.getContextId(), new ServiceLookupDoveAdmClientProvider(services));
        }

        // TODO: Ensuring OAuth tokens should be implemented as part of com.openexchange.push.PushListenerService.generateSessionFor(PushUser)
        try {
            Session session = null;
            {
                String sessionId = LogProperties.get(LogProperties.Name.SESSION_SESSION_ID);
                if (Strings.isNotEmpty(sessionId)) {
                    session = services.getServiceSafe(SessiondService.class).getSession(sessionId);
                    if (session != null && (session.getContextId() != pushUser.getContextId() || session.getUserId() != pushUser.getUserId())) {
                        session = null;
                    }
                }
            }

            if (session == null) {
                session = generateSessionFor(pushUser);
            }

            if (AuthType.isOAuthType(MailConfig.getConfiguredAuthTypeForMailAccess(AccountNature.PRIMARY, session)) && session.getParameter(Session.PARAM_OAUTH_ACCESS_TOKEN) == null) {
                LOGGER.debug("Falling back to DoveAdm push registration context due to missing OAuth token in session");
                return RegistrationContext.createDoveAdmClientContext(pushUser.getUserId(), pushUser.getContextId(), new ServiceLookupDoveAdmClientProvider(services));
            }

            return RegistrationContext.createSessionContext(session);
        } catch (OXException e) {
            if (doveAdmClient != null && (PushExceptionCodes.MISSING_PASSWORD.equals(e) || PushExceptionCodes.MISSING_MASTER_PASSWORD.equals(e))) {
                LOGGER.debug("Falling back to DoveAdm push registration context due to missing password in session");
                return RegistrationContext.createDoveAdmClientContext(pushUser.getUserId(), pushUser.getContextId(), new ServiceLookupDoveAdmClientProvider(services));
            }

            throw e;
        }

    }

}
