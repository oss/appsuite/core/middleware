/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.segmenter.sitechange.impl.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import com.openexchange.exception.OXException;
import com.openexchange.java.ExceptionCatchingRunnable;
import com.openexchange.rest.services.RESTUtils;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.segmenter.client.SegmenterService;
import com.openexchange.segmenter.client.Site;
import com.openexchange.segmenter.client.SiteImpl;
import com.openexchange.segmenter.sitechange.impl.SiteChangedHandler;
import com.openexchange.server.ServiceLookup;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.threadpool.ThreadPools;
import com.openexchange.threadpool.behavior.CallerRunsBehavior;

/**
 * {@link SegmenterRestAPI} - The REST end-point for "site changed" related HTTP calls.
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
@Path("/segmenter/v1")
@RoleAllowed(Role.BASIC_AUTHENTICATED)
public class SegmenterRestAPI {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(SegmenterRestAPI.class);

    private final SiteChangedHandler siteChangedHandler;
    private final boolean handleSiteChangedAsync;
    private final ServiceLookup services;

    /**
     * Initializes a new {@link SegmenterRestAPI}.
     *
     * @param siteChangedHandler The site change handler
     * @param handleSiteChangedAsync <code>true</code> to asynchronously handle a site change; otherwise <code>false</code>
     * @param services The service look-up
     */
    public SegmenterRestAPI(SiteChangedHandler siteChangedHandler, boolean handleSiteChangedAsync, ServiceLookup services) {
        super();
        this.siteChangedHandler = siteChangedHandler;
        this.services = services;
        this.handleSiteChangedAsync = handleSiteChangedAsync;
    }

    @Path("/changed")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response onSiteChange(JSONObject jSites) {
        if (null == jSites) {
            return RESTUtils.respondBadRequest("Missing request body.");
        }
        try {
            // Parse new- and previous availability of local site
            Site localSite = services.getServiceSafe(SegmenterService.class).getLocalSite();
            Site optPreviousAvailability = optMatchingSite(localSite, jSites.optJSONArray("previousAvailabilities"));
            Site optNewAvailability = optMatchingSite(localSite, jSites.optJSONArray("newAvailabilities"));
            LOG.info("'Site changed' event received (previous: {}, new: {})", optPreviousAvailability, optNewAvailability);

            // Check if local site is now available again
            if (false == isAvailableAgain(optPreviousAvailability, optNewAvailability)) {
                LOG.info("No change detected for local site {}, nothing to do.", optNewAvailability);
                return Response.notModified().build();
            }

            // Handle change of site availability
            ThreadPoolService threadPool = handleSiteChangedAsync ? services.getOptionalService(ThreadPoolService.class) : null;
            if (threadPool == null) {
                LOG.info("Local site {} is available again: handling 'site changed' event.", optNewAvailability);
                if (false == siteChangedHandler.notifySiteChange(true)) {
                    return Response.status(Status.CONFLICT).build();
                }
            } else {
                LOG.info("Local site {} is available again: handling 'site changed' event asynchronously.", optNewAvailability);
                ExceptionCatchingRunnable t = () -> siteChangedHandler.notifySiteChange(true);
                threadPool.submit(ThreadPools.task(t, true), CallerRunsBehavior.getInstance());
            }

            // Return result
            JSONArray result = new JSONArray(1).put(localSite.getId());
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } catch (OXException e) {
            LOG.error("Error handling site change.", e);
            JSONObject message = RESTUtils.generateError(e);
            return RESTUtils.respondServerError(message);
        } catch (JSONException e) {
            LOG.error("Error parsing body.", e);
            JSONObject message = RESTUtils.generateError(OXException.general(e.getMessage(), e));
            return RESTUtils.respondBadRequest(message);
        } catch (Exception e) {
            LOG.error("Error.", e);
            JSONObject message = RESTUtils.generateError(OXException.general(e.getMessage(), e));
            return RESTUtils.respondBadRequest(message);
        }
    }

    /**
     * Checks whether an updated availability indicates a transition from a previously unavailable to a now available state.
     *
     * @param optPreviousAvailability The optionally indicated previous availability of the site
     * @param optNewAvailability The optionally indicated new availability of the site
     * @return <code>true</code> if a site change is indicated, <code>false</code>, otherwise
     */
    private static boolean isAvailableAgain(Site optPreviousAvailability, Site optNewAvailability) {
        // Check that new site is now present/available and was absent/unavailable before
        return isPresentAndAvailable(optNewAvailability) && isAbsentOrUnavailable(optPreviousAvailability);
    }

    /**
     * Checks if specified site is absent (is <code>null</code>) or unavailable (availability &lt; 1.0)
     *
     * @param optSite The optional site to check
     * @return <code>true</code> if absent or unavailable; otherwise <code>false</code> (present and available)
     */
    private static boolean isAbsentOrUnavailable(Site optSite) {
        return (optSite == null || optSite.getAvailability() < 1.0F);
    }

    /**
     * Checks if specified site is present (is <b>not</b> <code>null</code>) and available (availability &gt;= 1.0)
     *
     * @param optSite The optional site to check
     * @return <code>true</code> if present and available; otherwise <code>false</code> (absent or unavailable)
     */
    private static boolean isPresentAndAvailable(Site optSite) {
        return (optSite != null && optSite.getAvailability() >= 1.0F);
    }

    /**
     * Looks up and parses a certain site from the given array of JSON site representations.
     *
     * @param site The site to match
     * @param jSites The JSON site representations
     * @return The matching parsed site, or <code>null</code> if no matching one was parsed
     * @throws JSONException If parsing JSON fails
     */
    private static Site optMatchingSite(Site site, JSONArray jSites) throws JSONException {
        if (null != jSites) {
            for (Object object : jSites) {
                Site parsedSite = parseSite((JSONObject) object);
                if (site.matches(parsedSite)) {
                    return parsedSite;
                }
            }
        }
        return null;
    }

    /**
     * Pares the JSON site representation to an instance of <code>Site</code>.
     *
     * @param jSite The JSON site representation
     * @return The parsed sites
     * @throws JSONException If parsing JSON fails
     */
    private static Site parseSite(JSONObject jSite) throws JSONException {
        String siteId = jSite.optString("siteId", null);
        if (siteId == null) {
            siteId = jSite.getString("id");
        }
        double availability = jSite.optDouble("availability", 1.0F);
        return new SiteImpl(siteId, (float) availability);
    }

}
