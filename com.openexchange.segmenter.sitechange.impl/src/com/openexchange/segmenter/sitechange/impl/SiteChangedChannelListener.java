/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.segmenter.sitechange.impl;

import org.osgi.framework.BundleContext;
import com.openexchange.pubsub.AbstractTrackingChannelListener;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.Message;
import com.openexchange.pubsub.PubSubService;

/**
 * {@link SiteChangedChannelListener} - Receives remotely propagated "site change" messages and calls appropriate {@link SiteChangedListener listeners}.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class SiteChangedChannelListener extends AbstractTrackingChannelListener<Boolean> {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(SiteChangedChannelListener.class);

    private final SiteChangedHandler handler;

    /**
     * Initializes a new {@link SiteChangedChannelListener}.
     *
     * @param handler The handler instance to call
     * @param context The bundle context
     */
    public SiteChangedChannelListener(SiteChangedHandler handler, BundleContext context) {
        super(context);
        this.handler = handler;
    }

    @Override
    public void onMessage(Message<Boolean> message) {
        if (message.isRemote()) {
            try {
                handler.notifySiteChange(false);
            } catch (Exception e) {
                LOG.error("Failed to handle remotely received \"site changed\" event", e);
            }
        }
    }

    @Override
    protected Channel<Boolean> getChannel(PubSubService service) {
        return SiteChangedHandler.getChannel(service);
    }

}
