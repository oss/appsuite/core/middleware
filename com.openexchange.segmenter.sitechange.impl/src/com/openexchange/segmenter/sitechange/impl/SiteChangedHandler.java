/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.segmenter.sitechange.impl;

import static com.openexchange.java.Autoboxing.L;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import com.openexchange.cache.v2.filter.CacheFilter;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;
import com.openexchange.cluster.lock.AbstractUnboundClusterTask;
import com.openexchange.cluster.lock.ClusterLockService;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.RankingAwareNearRegistryServiceTracker;
import com.openexchange.policy.retry.RunOnceRetryPolicy;
import com.openexchange.pubsub.Channel;
import com.openexchange.pubsub.ChannelKey;
import com.openexchange.pubsub.DefaultChannelKey;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.pubsub.StandardCodecs;
import com.openexchange.pubsub.core.CoreChannelName;
import com.openexchange.redis.RedisConnector;
import com.openexchange.redis.RedisConnectorProvider;
import com.openexchange.redis.RedisConnectorService;
import com.openexchange.redis.RedisType;
import com.openexchange.redis.RedisVoidOperation;
import com.openexchange.segmenter.sitechange.SiteChangedListener;
import com.openexchange.server.ServiceLookup;

/**
 * {@link SiteChangedHandler} - Handles stuff to be done when the local site comes back online.
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class SiteChangedHandler extends RankingAwareNearRegistryServiceTracker<SiteChangedListener> {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(SiteChangedHandler.class);

    private final ServiceLookup services;

    /**
     * Initializes a new {@link SiteChangedHandler}.
     *
     * @param services The service look-up
     * @param context The bundle context
     */
    public SiteChangedHandler(ServiceLookup services, BundleContext context) {
        super(context, SiteChangedListener.class);
        this.services = services;
    }

    /**
     * Notifies this handler about a site change.
     *
     * @param distribute <code>true</code> to communicate site change to remote nodes/services; otherwise <code>false</code> for only local processing
     * @return <code>true</code> if the site change was handled, <code>false</code> if not because no exclusive lock could be obtained
     * @throws OXException If propagating the site change fails
     */
    public boolean notifySiteChange(boolean distribute) throws OXException {
        if (!distribute) {
            // Just trigger local "site changed" handling
            notifyListeners();
            return true;
        }

        ClusterLockService lockService = services.getOptionalService(ClusterLockService.class);
        if (lockService == null) {
            // No cluster lock service available. Run it w/o acquiring a lock...
            notifyAndPropagateSiteChange();
            return true;
        }

        // Acquire a cluster lock to prevent from accidental multiple handling of a "site changed" event
        try {
            lockService.runClusterTask(new SiteChangedClusterTask(this), new RunOnceRetryPolicy(), 20_000L, false);
        } catch (OXException e) {
            if ("SRV".equals(e.getPrefix()) && (5 == e.getCode() || 6 == e.getCode())) {
                // ClusterLockExceptionCodes.UNABLE_TO_ACQUIRE_CLUSTER_LOCK or ClusterLockExceptionCodes.UNABLE_TO_ACQUIRE_CLUSTER_LOCK_EXPIRED
                LOG.info("Unable to acquire cluster lock, considering site change as already or just been handled.", e);
                return false;
            }
            throw e;
        }
        return true;
    }

    /**
     * Notifies local "site changed" listeners and propagates "site changed" event to both  - cache and remote nodes.
     */
    private void notifyAndPropagateSiteChange() {
        notifyListeners();
        invalidateCache();
        fireSiteChangedEvent();
    }

    /**
     * Notify all site-chanegd listeners.
     */
    private void notifyListeners() {
        LOG.info("Notifying site-changed listeners about site change");
        long start = System.nanoTime();
        for (SiteChangedListener listener : this) {
            try {
                listener.onSiteChange();
            } catch (Exception e) {
                LOG.error("Failed to propagate site-changed event to listener {}", listener.getClass().getName(), e);
            }
        }
        LOG.info("Notifying site-changed listeners took {} ms", L((System.nanoTime() - start) / 1000000));
    }

    /**
     * Invalidate cache.
     */
    private void invalidateCache() {
        LOG.info("Invalidating cache data for site change");
        try {
            RedisConnectorService redisConnectorService = services.getOptionalService(RedisConnectorService.class);
            if (redisConnectorService == null) {
                LOG.info("Cannot access Redis storage since required service is absent. Falling back to clear cache through \"match all\" filter...");
                invalidateCacheByFilter();
                return;
            }

            if (supportsCacheData(redisConnectorService)) {
                try {
                    // Dedicated Redis instance for cache data advertised
                    RedisConnectorProvider cacheConnectorProvider = redisConnectorService.getCacheConnectorProvider().orElse(null);
                    if (cacheConnectorProvider == null) {
                        // No such dedicated Redis instance for cache data although advertised
                        LOG.info("No Redis instance for cache data available and thus cache data cannot be safely dropped. Falling back to clear cache through \"match all\" filter...");
                        invalidateCacheByFilter();
                    } else {
                        // There is a dedicated Redis instance for cache data. Thus execute FLUSHDB on that instance.
                        RedisConnector cacheConnector = cacheConnectorProvider.getConnector();
                        RedisVoidOperation flushdbOperation = commandsProvider -> commandsProvider.getServerCommands().flushdb();
                        long start = System.nanoTime();
                        cacheConnector.executeVoidOperation(flushdbOperation);
                        LOG.info("Invalidating Redis cache for site change took {}msec", L((System.nanoTime() - start) / 1000000));
                    }
                } catch (Exception e) {
                    LOG.error("Failed to invalidate Redis cache. Falling back to clear cache through \"match all\" filter...", e);
                    invalidateCacheByFilter();
                }
            } else {
                LOG.info("No Redis instance for cache data available and thus cache data cannot be safely dropped. Falling back to clear cache through \"match all\" filter...");
                invalidateCacheByFilter();
            }
        } catch (Exception e) {
            LOG.error("Failed to invalidate cache data on site change", e);
        }
    }

    /**
     * Checks if Redis connector has been configured to have a dedicated instance for cache data.
     *
     * @param redisConnectorService The Redis connector service to check against
     * @return <code>true</code> if there is support for cache data instance; otherwise <code>false</code>
     */
    private static boolean supportsCacheData(RedisConnectorService redisConnectorService) {
        try {
            return redisConnectorService.supports(RedisType.CACHE);
        } catch (Exception e) {
            LOG.error("Failed to check support for a dedicated Redis instance holding cache data. Assuming no such support as fall-back", e);
            return false;
        }
    }

    /**
     * Invalidates cache data through "match all" filter.
     * <p>
     * <b>This is probably a very slow operation!</b>
     */
    private void invalidateCacheByFilter() {
        InvalidationCacheService cacheService = services.getOptionalService(InvalidationCacheService.class);
        if (cacheService == null) {
            LOG.info("Cannot clear cache through \"match all\" filter since required service is absent");
            return;
        }

        try {
            long start = System.nanoTime();
            cacheService.invalidate(CacheFilter.matchAll(), false);
            LOG.info("Clearing cache through \"match all\" filter took {}msec", L((System.nanoTime() - start) / 1000000));
        } catch (Exception e) {
            LOG.error("Failed to clear cache through \"match all\" filter", e);
        }
    }

    /** The channel key for "site changed" messages */
    private static final ChannelKey SITE_CHANGED_CHANNEL_KEY = DefaultChannelKey.builder().withChannelName(CoreChannelName.SITE_CHANGED_EVENTS).build();

    /**
     * Gets the channel to use for local session invalidation messages.
     *
     * @param service The Pub/Sub service
     * @return The channel
     */
    public static Channel<Boolean> getChannel(PubSubService service) {
        return service.getChannel(SITE_CHANGED_CHANNEL_KEY, StandardCodecs.BOOLEAN);
    }

    /**
     * Fires a "site changed" event through pub/sub channel.
     */
    private void fireSiteChangedEvent() {
        PubSubService service = services.getOptionalService(PubSubService.class);
        if (null != service) {
            try {
                getChannel(service).publish(Boolean.TRUE);
                LOG.info("Published \"site changed\" event to remote nodes");
            } catch (Exception e) {
                LOG.warn("Failed publishing \"site changed\" event", e);
            }
        }
    }

    // --------------------------------------------------- Cluster task -----------------------------------------------------------------

    private static class SiteChangedClusterTask extends AbstractUnboundClusterTask<Void> {

        private final SiteChangedHandler handler;

        /**
         * Initializes a new {@link SiteChangedClusterTask}.
         *
         * @param handler The performing handler
         */
        SiteChangedClusterTask(SiteChangedHandler handler) {
            super();
            this.handler = handler;
        }

        @Override
        public Void perform() throws OXException {
            handler.notifyAndPropagateSiteChange();
            return null;
        }

        @Override
        public String getTaskName() {
            return "SiteChangedClusterTask";
        }
    }

}
