/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.segmenter.sitechange.impl.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;
import com.openexchange.cluster.lock.ClusterLockService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.pubsub.PubSubService;
import com.openexchange.redis.RedisConnectorService;
import com.openexchange.segmenter.client.SegmenterService;
import com.openexchange.segmenter.sitechange.impl.SiteChangedChannelListener;
import com.openexchange.segmenter.sitechange.impl.SiteChangedHandler;
import com.openexchange.segmenter.sitechange.impl.rest.SegmenterRestAPI;
import com.openexchange.threadpool.ThreadPoolService;

/**
 * {@link SiteChangedActivator} - The activator for "<code>com.openexchange.segmenter.sitechange.impl</code>" bundle.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class SiteChangedActivator extends HousekeepingActivator {

    private static final Logger LOG = LoggerFactory.getLogger(SiteChangedActivator.class);

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] { SegmenterService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        try {
            LOG.info("Starting bundle {}", context.getBundle().getSymbolicName());

            SiteChangedHandler handler = new SiteChangedHandler(this, context);

            rememberTracker(handler);
            trackService(ThreadPoolService.class);
            trackService(PubSubService.class);
            track(PubSubService.class, new SiteChangedChannelListener(handler, context));
            trackService(ClusterLockService.class);
            trackService(RedisConnectorService.class);
            trackService(InvalidationCacheService.class);
            openTrackers();

            registerService(SegmenterRestAPI.class, new SegmenterRestAPI(handler, false, this));
        } catch (Exception e) {
            LOG.error("Error starting {}", context.getBundle().getSymbolicName(), e);
            throw e;
        }
    }

}
