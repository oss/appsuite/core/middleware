/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.storage.rdb;

import static com.openexchange.database.Databases.closeSQLStuff;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.mail.scheduled.ScheduledMailExceptionCodes;
import com.openexchange.server.ServiceLookup;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

/**
 * {@link RdbExpiringLock} - A database-lock that self expires.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.130.0
 */
public class RdbExpiringLock implements SchemaLock {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(RdbExpiringLock.class);

    /**
     * Represents the unique key for a lock.
     */
    public static final class RdbExpiringLockKey {

        private final int contextId;
        private final int userId;
        private final String name;

        /**
         * Initializes a new {@link RdbExpiringLockKey}.
         *
         * @param name The name of the lock
         * @param userId The optional user identifier or less than/equal to <code>0</code> (zero)
         * @param contextId The optional context identifier or less than/equal to <code>0</code> (zero)
         */
        public RdbExpiringLockKey(String name, int userId, int contextId) {
            super();
            this.contextId = contextId <= 0 ? 0 : contextId;
            this.userId = userId <= 0 ? 0 : userId;
            this.name = name;
        }

        @Override
        public int hashCode() {
            int result = 1;
            result = 31 * result + (contextId);
            result = 31 * result + (userId);
            result = 31 * result + (name == null ? 0 : name.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            RdbExpiringLockKey other = (RdbExpiringLockKey) obj;
            return contextId == other.contextId && userId == other.userId && Objects.equals(name, other.name);
        }

        @Override
        public String toString() {
            return new StringBuilder(name).append('-').append(contextId).append('-').append(userId).toString();
        }
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    private final RdbExpiringLockKey key;
    private final int representativeContextId;
    private final ServiceLookup services;
    private final Duration expireDuration;
    private final Duration refreshInterval;
    private ScheduledTimerTask timerTask;
    private Thread owner;

    /**
     * Initializes a new {@link RdbExpiringLock}.
     *
     * @param name The unique name of the lock
     * @param expireDuration The duration for which this lock lives
     * @param representativeContextId The representative context identifier providing target database schema
     * @param services The service look-up
     */
    public RdbExpiringLock(String name, Duration expireDuration, int representativeContextId, ServiceLookup services) {
        this(new RdbExpiringLockKey(name, 0, 0), expireDuration, representativeContextId, services);
    }

    /**
     * Initializes a new {@link RdbExpiringLock} with default 5 seconds refresh interval.
     *
     * @param key The unique key of the lock
     * @param expireDuration The duration for which this lock lives
     * @param representativeContextId The representative context identifier providing target database schema
     * @param services The service look-up
     */
    public RdbExpiringLock(RdbExpiringLockKey key, Duration expireDuration, int representativeContextId, ServiceLookup services) {
        this(key, expireDuration, Duration.ofMillis(5000L), representativeContextId, services);
    }

    /**
     * Initializes a new {@link RdbExpiringLock}.
     *
     * @param key The unique key of the lock
     * @param expireDuration The duration for which this lock lives
     * @param refreshInterval The refresh interval (must not be greater than expiration duration)
     * @param representativeContextId The representative context identifier providing target database schema
     * @param services The service look-up
     */
    public RdbExpiringLock(RdbExpiringLockKey key, Duration expireDuration, Duration refreshInterval, int representativeContextId, ServiceLookup services) {
        super();
        if (refreshInterval.compareTo(expireDuration) > 0) {
            throw new IllegalArgumentException("Refresh interval must not be greater than expiration duration");
        }
        this.key = key;
        this.expireDuration = expireDuration;
        this.refreshInterval = refreshInterval;
        this.representativeContextId = representativeContextId;
        this.services = services;
        this.timerTask= null;
        this.owner = null;
    }

    /**
     * Tries to acquire this lock.
     *
     * @return <code>true</code> if acquired; otherwise <code>false</code>
     * @throws OXException If lock acquisition fails
     */
    @Override
    public synchronized boolean acquireLock() throws OXException {
        Thread optCurrentThread = null;
        if (owner != null) {
            optCurrentThread = Thread.currentThread();
            if (owner == optCurrentThread) {
                // Already acquired
                return true;
            }
        }

        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        TimerService timerService = services.getServiceSafe(TimerService.class);

        boolean modified = false;
        Connection con = databaseService.getWritable(representativeContextId);
        try {
            boolean acquired = acquireLock(con, databaseService, timerService, optCurrentThread);
            if (acquired) {
                modified = true;
            }
            return acquired;
        } finally {
            if (modified) {
                databaseService.backWritable(representativeContextId, con);
            } else {
                databaseService.backWritableAfterReading(representativeContextId, con);
            }
        }
    }

    private synchronized boolean acquireLock(Connection con, DatabaseService databaseService, TimerService timerService, Thread optCurrentThread) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT `stamp` FROM `scheduledMailLock` WHERE `cid`=? AND `user`=? AND `name`=?");
            stmt.setInt(1, key.contextId);
            stmt.setInt(2, key.userId);
            stmt.setString(3, key.name);
            rs = stmt.executeQuery();
            long stampMillis = rs.next() ? rs.getLong(1) : 0L;
            closeSQLStuff(rs, stmt);
            rs = null;
            stmt = null;

            int rows;
            long currentMillis = System.currentTimeMillis();
            if (stampMillis == 0) {
                // No lock entry
                stmt = con.prepareStatement("INSERT INTO `scheduledMailLock` (`cid`, `user`, `name`, `stamp`) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE `name`=`name`");
                stmt.setInt(1, key.contextId);
                stmt.setInt(2, key.userId);
                stmt.setString(3, key.name);
                stmt.setLong(4, currentMillis);
                rows = stmt.executeUpdate();
                closeSQLStuff(stmt);
                stmt = null;
            } else if ((currentMillis - stampMillis) > expireDuration.toMillis()) {
                // Lock entry expired
                stmt = con.prepareStatement("UPDATE `scheduledMailLock` SET `stamp`=? WHERE `cid`=? AND `user`=? AND `name`=? AND `stamp`=?");
                stmt.setLong(1, currentMillis);
                stmt.setInt(2, key.contextId);
                stmt.setInt(3, key.userId);
                stmt.setString(4, key.name);
                stmt.setLong(5, stampMillis);
                rows = stmt.executeUpdate();
                closeSQLStuff(stmt);
                stmt = null;
            } else {
                // Still locked...
                rows = 0;
            }

            if (rows <= 0) {
                return false;
            }
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            closeSQLStuff(rs, stmt);
        }

        Runnable task = () -> updateLock(key, representativeContextId, databaseService, this);
        long refreshIntervalMillis = refreshInterval.toMillis();
        timerTask = timerService.scheduleWithFixedDelay(task, refreshIntervalMillis, refreshIntervalMillis, TimeUnit.MILLISECONDS);
        owner = optCurrentThread == null ? Thread.currentThread() : optCurrentThread;
        return true;
    }

    private static void updateLock(RdbExpiringLockKey key, int representativeContextId, DatabaseService databaseService, RdbExpiringLock instance) {
        synchronized (instance) { // NOSONARLINT
            try {
                PreparedStatement stmt = null;
                Connection con = databaseService.getWritable(representativeContextId);
                try {
                    stmt = con.prepareStatement("UPDATE `scheduledMailLock` SET `stamp`=? WHERE `cid`=? AND `user`=? AND `name`=?");
                    stmt.setLong(1, System.currentTimeMillis());
                    stmt.setInt(2, key.contextId);
                    stmt.setInt(3, key.userId);
                    stmt.setString(4, key.name);
                    stmt.executeUpdate();
                } finally {
                    closeSQLStuff(stmt);
                    databaseService.backWritable(representativeContextId, con);
                }
            } catch (Exception e) {
                LOG.warn("Failed to update lock {}", key, e);
            }
        }
    }

    /**
     * Unlocks this lock.
     *
     * @param letItExpire Whether to let this lock expire or to drop it allowing immediate subsequent acquisition
     * @throws IllegalStateException If calling thread is not owner of this lock
     */
    @Override
    public synchronized void unlock(boolean letItExpire) {
        Thread currentThread = Thread.currentThread();
        if (owner != currentThread) {
            // Lock not owned by executing thread
            throw new IllegalStateException("Calling thread is not owner of this lock");
        }
        owner = null;

        ScheduledTimerTask task = this.timerTask;
        if (task != null) {
            this.timerTask = null;
            task.cancel();
            TimerService timerService = services.getOptionalService(TimerService.class);
            if (timerService != null) {
                timerService.purge();
            }
        }

        if (letItExpire) {
            return;
        }

        DatabaseService databaseService = services.getOptionalService(DatabaseService.class);
        if (databaseService == null) {
            LOG.warn("Failed to delete lock {} since database service is absent", key);
            return;
        }

        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = databaseService.getWritable(representativeContextId);
            stmt = con.prepareStatement("DELETE FROM `scheduledMailLock` WHERE `cid`=? AND `user`=? AND `name`=?");
            stmt.setInt(1, key.contextId);
            stmt.setInt(2, key.userId);
            stmt.setString(3, key.name);
            stmt.executeUpdate();
        } catch (Exception e) {
            LOG.warn("Failed to delete lock {}", key, e);
        } finally {
            closeSQLStuff(stmt);
            databaseService.backWritable(representativeContextId, con);
        }
    }

    @Override
    public int compareTo(SchemaLock o) {
        return Integer.compare(representativeContextId, ((RdbExpiringLock) o).representativeContextId);
    }

}
