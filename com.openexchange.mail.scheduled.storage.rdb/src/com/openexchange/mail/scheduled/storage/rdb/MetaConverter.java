/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.storage.rdb;

import java.util.Map;
import java.util.Optional;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import com.openexchange.mail.scheduled.Meta;
import com.openexchange.session.ObfuscatorService;

/**
 * {@link MetaConverter} - Utility for converting meta instances.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class MetaConverter {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(MetaConverter.class);

    /**
     * Initializes a new {@link MetaConverter}.
     */
    private MetaConverter() {
        super();
    }

    /**
     * Converts given meta instance to its obfuscated string representation.
     *
     * @param meta The meta instance to convert
     * @param obfuscator The ObfuscatorService to use
     * @return The obfuscated string representation or empty
     */
    public static Optional<String> convertMetaToString(Meta meta, ObfuscatorService obfuscator) {
        if (meta == null) {
            return Optional.empty();
        }

        JSONObject optionalJson = convertMetaToJson(meta);
        if (optionalJson == null) {
            return Optional.empty();
        }
        return Optional.of(obfuscator.obfuscate(optionalJson.toString()));
    }

    /**
     * Converts given meta instance to its JSON representation.
     *
     * @param meta The meta instance to convert
     * @return The JSON representation or empty
     */
    private static JSONObject convertMetaToJson(Meta meta) {
        if (meta == null) {
            return null;
        }

        try {
            JSONObject jMeta = new JSONObject(meta.size());
            for (Map.Entry<String, Object> e : meta.entrySet()) {
                Object value = e.getValue();
                if (value instanceof Meta m) {
                    JSONObject opt = convertMetaToJson(m);
                    if (opt != null) {
                        jMeta.put(e.getKey(), opt);
                    }
                } else {
                    jMeta.put(e.getKey(), value);
                }
            }
            return jMeta;
        } catch (JSONException e) {
            LOG.error("Unable to generate JSON object", e);
        }
        return null;
    }

    /**
     * Parses given obfuscated string representation to its meta instance.
     *
     * @param sObfuscatedMeta The obfuscated string representation to parse
     * @param obfuscator The ObfuscatorService to use
     * @return The meta instance or <code>null</code>
     */
    public static Meta parseStringToMeta(String sObfuscatedMeta, ObfuscatorService obfuscator) {
        if (sObfuscatedMeta == null) {
            return null;
        }

        try {
            return parseJsonToMeta(JSONServices.parseObject(obfuscator.unobfuscate(sObfuscatedMeta)));
        } catch (JSONException e) {
            LOG.error("Unable to parse JSON object", e);
        }
        return null;
    }

    /**
     * Parses given JSON representation to its meta instance.
     *
     * @param jMeta The JSON representation to parse
     * @return The meta instance or <code>null</code>
     */
    private static Meta parseJsonToMeta(JSONObject jMeta) {
        if (jMeta == null) {
            return null;
        }

        Meta.Builder meta = Meta.builder();
        for (Map.Entry<String, Object> e :  jMeta.entrySet()) {
            Object value = e.getValue();
            if (value instanceof JSONObject json) {
                meta.withProperty(e.getKey(), parseJsonToMeta(json));
            } else {
                meta.withProperty(e.getKey(), value);
            }
        }
        return meta.build();
    }

}
