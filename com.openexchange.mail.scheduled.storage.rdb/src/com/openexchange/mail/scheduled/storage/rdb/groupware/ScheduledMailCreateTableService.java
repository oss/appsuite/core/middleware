/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.storage.rdb.groupware;

import com.openexchange.database.AbstractCreateTableImpl;
import com.openexchange.java.Strings;

/**
 * {@link ScheduledMailCreateTableService} - Creates the tables for scheduled mail storage.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class ScheduledMailCreateTableService extends AbstractCreateTableImpl {

    private static final ScheduledMailCreateTableService INSTANCE = new ScheduledMailCreateTableService();

    /**
     * Gets the instance
     *
     * @return The instance
     */
    public static ScheduledMailCreateTableService getInstance() {
        return INSTANCE;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static final String SCHEDULED_MAIL_TABLE = "scheduledMail";

    // @formatter:off
    private static final String CREATE_SCHEDULED_MAIL_TABLE = "CREATE TABLE `" + SCHEDULED_MAIL_TABLE + "` ("
        + "`uuid` BINARY(16) NOT NULL,"
        + "`cid` INT4 unsigned NOT NULL,"
        + "`user` INT4 unsigned NOT NULL,"
        + "`dateToSend` BIGINT(64) unsigned NOT NULL,"
        + "`mailPath` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,"
        + "`processing` BIGINT(64) unsigned NOT NULL DEFAULT 0,"
        + "`meta` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,"
        + "PRIMARY KEY (`uuid`),"
        + "KEY id (`cid`, `user`, `uuid`)"
        + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci";
    // @formatter:on

    private static final String SCHEDULED_MAIL_LOCK_TABLE = "scheduledMailLock";

    // @formatter:off
    private static final String CREATE_SCHEDULED_MAIL_LOCK_TABLE = "CREATE TABLE `" + SCHEDULED_MAIL_LOCK_TABLE + "` ("
        + "`cid` INT4 unsigned NOT NULL DEFAULT 0,"
        + "`user` INT4 unsigned NOT NULL DEFAULT 0,"
        + "`name` VARCHAR(16) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,"
        + "`stamp` BIGINT(64) unsigned NOT NULL,"
        + "PRIMARY KEY (`cid`, `user`, `name`)"
        + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci";
    // @formatter:on

    /**
     * Initializes a new {@link ScheduledMailCreateTableService}.
     */
    private ScheduledMailCreateTableService() {
        super();
    }

    @Override
    public String[] requiredTables() {
        return Strings.getEmptyStrings();
    }

    @Override
    public String[] tablesToCreate() {
        return new String[] { SCHEDULED_MAIL_TABLE, SCHEDULED_MAIL_LOCK_TABLE };
    }

    @Override
    public String[] getCreateStatements() {
        return new String[] { CREATE_SCHEDULED_MAIL_TABLE, CREATE_SCHEDULED_MAIL_LOCK_TABLE };
    }

}
