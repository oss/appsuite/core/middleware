/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.storage.rdb;

import java.util.Optional;
import java.util.UUID;
import com.openexchange.mail.scheduled.LockedScheduledMail;
import com.openexchange.mail.scheduled.Meta;
import com.openexchange.mail.scheduled.ScheduledMail;

/**
 * {@link LockKnowingLockedScheduledMail} - The locked scheduled mail knowing its lock.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class LockKnowingLockedScheduledMail implements LockedScheduledMail {

    private final ScheduledMail scheduledMail;
    private final RdbScheduledMailProcessingLock processingLock;

    /**
     * Initializes a new {@link LockKnowingLockedScheduledMail}.
     *
     * @param scheduledMail The scheduled mail
     * @param processingLock The processing lock
     */
    public LockKnowingLockedScheduledMail(ScheduledMail scheduledMail, RdbScheduledMailProcessingLock processingLock) {
        super();
        this.scheduledMail = scheduledMail;
        this.processingLock = processingLock;
    }

    @Override
    public UUID getId() {
        return scheduledMail.getId();
    }

    @Override
    public int getContextId() {
        return scheduledMail.getContextId();
    }

    @Override
    public int getUserId() {
        return scheduledMail.getUserId();
    }

    @Override
    public String getMailPath() {
        return scheduledMail.getMailPath();
    }

    @Override
    public long getDateToSend() {
        return scheduledMail.getDateToSend();
    }

    @Override
    public Optional<Meta> getMeta() {
        return scheduledMail.getMeta();
    }

    @Override
    public void unlock() {
        processingLock.unlock();
    }

    @Override
    public String toString() {
        return scheduledMail.toString();
    }

}
