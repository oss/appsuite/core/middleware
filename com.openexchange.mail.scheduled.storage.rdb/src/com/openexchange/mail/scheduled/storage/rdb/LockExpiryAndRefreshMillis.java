/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.storage.rdb;


/**
 * {@link LockExpiryAndRefreshMillis} - Simple tuple for lock expiration and refresh milliseconds.
 *
 * @param lockExpiryMillis The lock expiration milliseconds
 * @param lockRefreshMillis The lock refresh milliseconds
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.14.0
 */
public record LockExpiryAndRefreshMillis(long lockExpiryMillis, long lockRefreshMillis) {

    /**
     * Gets the lock expiration milliseconds.
     *
     * @return The lock expiration milliseconds
     */
    public long lockExpiryMillis() { // NOSONARLINT
        return lockExpiryMillis;
    }

    /**
     * Gets the lock refresh milliseconds.
     *
     * @return The lock refresh milliseconds
     */
    public long lockRefreshMillis() { // NOSONARLINT
        return lockRefreshMillis;
    }
}
