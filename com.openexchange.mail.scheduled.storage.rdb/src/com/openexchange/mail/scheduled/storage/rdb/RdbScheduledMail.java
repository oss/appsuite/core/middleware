/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.storage.rdb;

import static com.openexchange.java.Autoboxing.L;
import java.util.Optional;
import java.util.UUID;
import com.openexchange.mail.scheduled.Meta;
import com.openexchange.mail.scheduled.ScheduledMail;

/**
 * {@link RdbScheduledMail} - The scheduled mail loaded from database.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class RdbScheduledMail implements ScheduledMail {

    /**
     * Creates a new builder.
     *
     * @return The newly created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /** The builder for an instance of <code>RdbScheduledMail</code> */
    public static final class Builder {

        private UUID id;
        private int contextId;
        private int userId;
        private String mailPath;
        private Long dateToSend;
        private Meta meta;

        /**
         * Initializes a new {@link Builder}.
         */
        private Builder() {
            super();
            meta = null;
        }

        /**
         * Sets the identifier
         *
         * @param id The identifier to set
         * @return This builder
         */
        public Builder withId(UUID id) {
            this.id = id;
            return this;
        }

        /**
         * Sets the user identifier
         *
         * @param contextId The identifier to set
         * @return This builder
         */
        public Builder withContextId(int contextId) {
            this.contextId = contextId;
            return this;
        }

        /**
         * Sets the context identifier
         *
         * @param userId The identifier to set
         * @return This builder
         */
        public Builder withUserId(int userId) {
            this.userId = userId;
            return this;
        }

        /**
         * Sets the mail path
         *
         * @param mailPath The mail path to set
         * @return This builder
         */
        public Builder withMailPath(String mailPath) {
            this.mailPath = mailPath;
            return this;
        }

        /**
         * Sets the date to send
         *
         * @param dateToSend The date to send to set
         * @return This builder
         */
        public Builder withDateToSend(long dateToSend) {
            this.dateToSend = L(dateToSend);
            return this;
        }

        /**
         * Sets the meta information
         *
         * @param meta The meta information to set
         * @return This builder
         */
        public Builder withMeta(Meta meta) {
            this.meta = meta;
            return this;
        }

        /**
         * Builds the instance of <code>RdbScheduledMail</code> from this builder's arguments.
         * <p>
         * <b>Prerequisites</b> (if violated a <code>java.lang.IllegalArgumentException</code> will be thrown):
         * <ul>
         * <li>UUID must not be <code>null</code></li>
         * <li>Context identifier must not be equal to/less than <code>0</code> (zero)</li>
         * <li>User identifier must not be equal to/less than <code>0</code> (zero)</li>
         * <li>Mail path must not be <code>null</code></li>
         * <li>Date to send is required to be set</li>
         * </ul>
         *
         * @return The instance of <code>RdbScheduledMail</code>
         */
        public RdbScheduledMail build() {
            return new RdbScheduledMail(id, userId, contextId, mailPath, dateToSend, meta);
        }
    }

    // ------------------------------------------------------------------------------------------

    private final UUID id;
    private final String mailPath;
    private final long dateToSend;
    private final int userId;
    private final int contextId;
    private final Optional<Meta> meta;

    /**
     * Initializes a new {@link RdbScheduledMail}.
     *
     * @param id The identifier
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param mailPath The mail path
     * @param dateToSend The date to send
     * @param meta The meta information
     */
    private RdbScheduledMail(UUID id, int userId, int contextId, String mailPath, Long dateToSend, Meta meta) {
        super();
        if (id == null) {
            throw new IllegalArgumentException("UUID must not be null");
        }
        if (contextId <= 0) {
            throw new IllegalArgumentException("Context identifier must not be equal to/less than 0 (zero)");
        }
        if (userId <= 0) {
            throw new IllegalArgumentException("User identifier must not be equal to/less than 0 (zero)");
        }
        if (mailPath == null) {
            throw new IllegalArgumentException("Mail path must not be null");
        }
        if (dateToSend == null) {
            throw new IllegalArgumentException("Date to send must not be null");
        }
        this.id = id;
        this.userId = userId;
        this.contextId = contextId;
        this.mailPath = mailPath;
        this.dateToSend = dateToSend.longValue();
        this.meta = Optional.ofNullable(meta);
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public int getContextId() {
        return contextId;
    }

    @Override
    public int getUserId() {
        return userId;
    }

    @Override
    public String getMailPath() {
        return mailPath;
    }

    @Override
    public long getDateToSend() {
        return dateToSend;
    }

    @Override
    public Optional<Meta> getMeta() {
        return meta;
    }

    @Override
    public String toString() {
        StringBuilder builder2 = new StringBuilder();
        builder2.append("RdbScheduledMail [");
        if (id != null) {
            builder2.append("id=").append(id).append(", ");
        }
        if (mailPath != null) {
            builder2.append("mailPath=").append(mailPath).append(", ");
        }
        builder2.append("dateToSend=").append(dateToSend).append(", userId=").append(userId).append(", contextId=").append(contextId).append(", ");
        if (meta.isPresent()) {
            builder2.append("meta=").append(meta.get());
        }
        builder2.append("]");
        return builder2.toString();
    }

}
