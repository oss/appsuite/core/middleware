/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.storage.rdb;

import static com.openexchange.database.Databases.closeSQLStuff;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.ExponentialBackoffUtils.exponentialBackoffWait;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.context.PoolAndSchema;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.database.SqlUpdateBuilder;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.update.UpdateStatus;
import com.openexchange.groupware.update.Updater;
import com.openexchange.java.BoolReference;
import com.openexchange.java.CollectorUtils;
import com.openexchange.java.Functions;
import com.openexchange.java.Sets;
import com.openexchange.java.util.UUIDs;
import com.openexchange.mail.scheduled.LockedScheduledMail;
import com.openexchange.mail.scheduled.ScheduledMail;
import com.openexchange.mail.scheduled.ScheduledMailDescription;
import com.openexchange.mail.scheduled.ScheduledMailExceptionCodes;
import com.openexchange.mail.scheduled.ScheduledMailProperty;
import com.openexchange.mail.scheduled.ScheduledMailStorageService;
import com.openexchange.mail.scheduled.Schema;
import com.openexchange.mail.scheduled.storage.rdb.RdbExpiringLock.RdbExpiringLockKey;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.ObfuscatorService;
import com.openexchange.session.Session;
import com.openexchange.session.UserAndContext;
import com.openexchange.threadpool.BoundedCompletionService;
import com.openexchange.threadpool.ThreadPoolCompletionService;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.threadpool.ThreadPools.TrackableCallable;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

/**
 * {@link RdbScheduledMailStorageService} - The database-backed storage for scheduled mail.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RdbScheduledMailStorageService implements ScheduledMailStorageService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(RdbScheduledMailStorageService.class);

    private final ServiceLookup services;

    /**
     * Initializes a new {@link RdbScheduledMailStorageService}.
     *
     * @param services The service look-up
     */
    public RdbScheduledMailStorageService(ServiceLookup services) {
        super();
        this.services = services;
    }

    @Override
    public Optional<LockedScheduledMail> acquireProcessingLock(UUID id, Session session) throws OXException {
        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        TimerService timerService = services.getServiceSafe(TimerService.class);
        LockExpiryAndRefreshMillis lockExpiryAndRefreshMillis = getLockExpiryAndRefreshMillis();

        boolean modified = false;
        Connection con = databaseService.getWritable(session.getContextId());
        try {
            Optional<LockedScheduledMail> optLock = acquireProcessingLock(id, lockExpiryAndRefreshMillis, session, con, databaseService, timerService);
            if (optLock.isPresent()) {
                modified = true;
            }
            return optLock;
        } finally {
            if (modified) {
                databaseService.backWritable(session.getContextId(), con);
            } else {
                databaseService.backWritableAfterReading(session.getContextId(), con);
            }
        }
    }

    private Optional<LockedScheduledMail> acquireProcessingLock(UUID id, LockExpiryAndRefreshMillis lockExpiryAndRefreshMillis, Session session, Connection con, DatabaseService databaseService, TimerService timerService) throws OXException {
        int contextId = session.getContextId();
        ScheduledTimerTask timerTask = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT `processing`, `dateToSend`, `mailPath`, `meta` FROM `scheduledMail` WHERE `cid`=? AND `user`=? AND `uuid`=?");
            stmt.setInt(1, contextId);
            stmt.setInt(2, session.getUserId());
            stmt.setBytes(3, UUIDs.toByteArray(id));
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return Optional.empty();
            }

            long stamp = rs.getLong(1);
            long dateToSend = rs.getLong(2);
            String mailPath = rs.getString(3);
            String sObfuscatedMeta = rs.getString(4);
            Databases.closeSQLStuff(rs, stmt);
            rs = null;
            stmt = null;

            long now = System.currentTimeMillis();
            if (stamp != 0 && (now - stamp <= lockExpiryAndRefreshMillis.lockExpiryMillis())) {
                // Already locked
                return Optional.empty();
            }

            // Not locked
            stmt = con.prepareStatement("UPDATE `scheduledMail` SET `processing`=? WHERE `cid`=? AND `user`=? AND `uuid`=? AND `processing`=?");
            stmt.setLong(1, now);
            stmt.setInt(2, contextId);
            stmt.setInt(3, session.getUserId());
            stmt.setBytes(4, UUIDs.toByteArray(id));
            stmt.setLong(5, stamp);
            int rows = stmt.executeUpdate();
            Databases.closeSQLStuff(stmt);
            stmt = null;

            if (rows <= 0) {
                // Another process acquired lock
                return Optional.empty();
            }

            // Get needed service
            ObfuscatorService obfuscator = services.getServiceSafe(ObfuscatorService.class);

            // Start lock-refreshing timer task
            Runnable task = () -> updateLock(id, contextId, databaseService);
            long refreshIntervalMillis = lockExpiryAndRefreshMillis.lockRefreshMillis();
            timerTask = timerService.scheduleWithFixedDelay(task, refreshIntervalMillis, refreshIntervalMillis, TimeUnit.MILLISECONDS);

            ScheduledMail scheduledMail = RdbScheduledMail.builder().withId(id).withUserId(session.getUserId()).withContextId(session.getContextId()).withDateToSend(dateToSend).withMailPath(mailPath).withMeta(MetaConverter.parseStringToMeta(sObfuscatedMeta, obfuscator)).build();
            Optional<LockedScheduledMail> retval = Optional.of(new LockKnowingLockedScheduledMail(scheduledMail, new RdbScheduledMailProcessingLock(id, contextId, timerTask, services)));
            timerTask = null; // Avoid premature cancellation
            return retval;
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
            if (timerTask != null) {
                // Something went wrong
                timerTask.cancel();
            }
        }
    }

    private static void updateLock(UUID id, int contextId, DatabaseService databaseService) {
        Connection c = null;
        PreparedStatement s = null;
        try {
            c = databaseService.getWritable(contextId);
            s = c.prepareStatement("UPDATE `scheduledMail` SET `processing`=? WHERE `uuid`=?");
            s.setLong(1, System.currentTimeMillis());
            s.setBytes(2, UUIDs.toByteArray(id));
            s.executeUpdate();
            LOG.debug("Refreshed lock for scheduled mail {}", UUIDs.getUnformattedStringObjectFor(id));
        } catch (Exception e) {
            LOG.warn("Failed to refresh lock for scheduled mail {}", UUIDs.getUnformattedStringObjectFor(id), e);
        } finally {
            closeSQLStuff(s);
            if (c != null) {
                databaseService.backWritable(contextId, c);
            }
        }
    }

    @Override
    public boolean existsScheduledMail(UUID id, Session session) throws OXException {
        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        Connection con = databaseService.getReadOnly(session.getContextId());
        try {
            return existsScheduledMail(id, session, con);
        } finally {
            databaseService.backReadOnly(session.getContextId(), con);
        }
    }

    private static boolean existsScheduledMail(UUID id, Session session, Connection con) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT 1 FROM `scheduledMail` WHERE `cid`=? AND `user`=? AND `uuid`=?");
            stmt.setInt(1, session.getContextId());
            stmt.setInt(2, session.getUserId());
            stmt.setBytes(3, UUIDs.toByteArray(id));
            rs = stmt.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    @Override
    public Optional<ScheduledMail> optScheduledMail(UUID id, Session session) throws OXException {
        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        Connection con = databaseService.getReadOnly(session.getContextId());
        try {
            return Optional.ofNullable(getScheduledMail(id, session, false, con));
        } finally {
            databaseService.backReadOnly(session.getContextId(), con);
        }
    }

    @Override
    public ScheduledMail getScheduledMail(UUID id, Session session) throws OXException {
        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        Connection con = databaseService.getReadOnly(session.getContextId());
        try {
            return getScheduledMail(id, session, true, con);
        } finally {
            databaseService.backReadOnly(session.getContextId(), con);
        }
    }

    private ScheduledMail getScheduledMail(UUID id, Session session, boolean errorOnAbsence, Connection con) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT `dateToSend`, `mailPath`, `meta` FROM `scheduledMail` WHERE `cid`=? AND `user`=? AND `uuid`=?");
            stmt.setInt(1, session.getContextId());
            stmt.setInt(2, session.getUserId());
            stmt.setBytes(3, UUIDs.toByteArray(id));
            rs = stmt.executeQuery();
            if (!rs.next()) {
                if (errorOnAbsence) {
                    throw ScheduledMailExceptionCodes.NO_SUCH_SCHEDULED_MAIL.create(UUIDs.getUnformattedString(id));
                }
                return null;
            }
            ObfuscatorService obfuscator = services.getServiceSafe(ObfuscatorService.class);
            return RdbScheduledMail.builder()
                .withId(id)
                .withUserId(session.getUserId())
                .withContextId(session.getContextId())
                .withDateToSend(rs.getLong(1))
                .withMailPath(rs.getString(2))
                .withMeta(MetaConverter.parseStringToMeta(rs.getString(3), obfuscator))
                .build();
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    private Map<UUID, ScheduledMail> getScheduledMails(Collection<UUID> ids, Session session,  Connection con) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(Databases.getIN("SELECT `dateToSend`, `mailPath`, `meta`, `uuid` FROM `scheduledMail` WHERE `cid`=? AND `user`=? AND `uuid` IN ", ids.size()));
            int pos = 1;
            stmt.setInt(pos++, session.getContextId());
            stmt.setInt(pos++, session.getUserId());
            for (UUID id : ids) {
                stmt.setBytes(pos++, UUIDs.toByteArray(id));
            }
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return Collections.emptyMap();
            }

            ObfuscatorService obfuscator = services.getServiceSafe(ObfuscatorService.class);
            Map<UUID, ScheduledMail> scheduledMails = HashMap.newHashMap(ids.size());
            do {
                UUID id = UUIDs.toUUID(rs.getBytes(4));
                ScheduledMail sm = RdbScheduledMail.builder()
                .withId(id)
                .withUserId(session.getUserId())
                .withContextId(session.getContextId())
                .withDateToSend(rs.getLong(1))
                .withMailPath(rs.getString(2))
                .withMeta(MetaConverter.parseStringToMeta(rs.getString(3), obfuscator))
                .build();
                scheduledMails.put(id, sm);
            } while (rs.next());
            return scheduledMails;
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    @Override
    public List<ScheduledMail> getScheduledMails(Session session) throws OXException {
        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        Connection con = databaseService.getReadOnly(session.getContextId());
        try {
            return getScheduledMails(session, con);
        } finally {
            databaseService.backReadOnly(session.getContextId(), con);
        }
    }

    private List<ScheduledMail> getScheduledMails(Session session, Connection con) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement("SELECT `uuid`, `dateToSend`, `mailPath`, `meta` FROM `scheduledMail` WHERE `cid`=? AND `user`=?");
            stmt.setInt(1, session.getContextId());
            stmt.setInt(2, session.getUserId());
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return Collections.emptyList();
            }

            ObfuscatorService obfuscator = services.getServiceSafe(ObfuscatorService.class);
            List<ScheduledMail> scheduledMails = new ArrayList<>();
            do {
                scheduledMails.add(RdbScheduledMail.builder()
                    .withId(UUIDs.toUUID(rs.getBytes(1)))
                    .withUserId(session.getUserId())
                    .withContextId(session.getContextId())
                    .withDateToSend(rs.getLong(2))
                    .withMailPath(rs.getString(3))
                    .withMeta(MetaConverter.parseStringToMeta(rs.getString(4), obfuscator))
                    .build()
                );
            } while (rs.next());
            return scheduledMails;
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    @Override
    public Map<Schema, Map<UserAndContext, List<LockedScheduledMail>>> getDueScheduledMails(long dueDate) throws OXException {
        ContextService contextService = services.getServiceSafe(ContextService.class);
        ThreadPoolService threadPool = services.getServiceSafe(ThreadPoolService.class);
        NeededServices neededServices = new NeededServices(services.getServiceSafe(DatabaseService.class), services.getServiceSafe(TimerService.class), contextService, Updater.getInstance(), services);

        List<Integer> contextsIdInDifferentSchemas = contextService.getDistinctContextsPerSchema();
        if (contextsIdInDifferentSchemas.isEmpty()) {
            // No schemas at all
            return Collections.emptyMap();
        }

        LockExpiryAndRefreshMillis lockExpiryAndRefreshMillis = getLockExpiryAndRefreshMillis();

        // Use bounded completion service with up to 10 concurrent tasks in order not to burden the database connection pool too much
        ThreadPoolCompletionService<RepresentativeContextIdAndLockedScheduledMails> completionService = new BoundedCompletionService<RepresentativeContextIdAndLockedScheduledMails>(threadPool, 10).setTrackable(true);
        List<LockedScheduledMailsForSchemaCallable> callables = new ArrayList<>(contextsIdInDifferentSchemas.size());
        try {
            // Submit a callable per schema for concurrent processing (up to 10 concurrent tasks)
            for (Integer representativeContextId : contextsIdInDifferentSchemas) {
                LockedScheduledMailsForSchemaCallable callable = new LockedScheduledMailsForSchemaCallable(representativeContextId, dueDate, lockExpiryAndRefreshMillis, neededServices);
                completionService.submit(callable);
                callables.add(callable); // Remember callable to drop locks on exception path
            }

            Map<Schema, Map<UserAndContext, List<LockedScheduledMail>>> schema2LockedDueMails = null;
            for (int i = callables.size(); i-- > 0;) {
                RepresentativeContextIdAndLockedScheduledMails result = completionService.take().get();
                Map<UserAndContext, List<LockedScheduledMail>> lockedMailsOfSchema = result.lockedMailsOfSchema();
                if (lockedMailsOfSchema != null && !lockedMailsOfSchema.isEmpty()) {
                    // Add schema-associated locked scheduled mails
                    if (schema2LockedDueMails == null) {
                        schema2LockedDueMails = LinkedHashMap.newLinkedHashMap(contextsIdInDifferentSchemas.size());
                    }
                    schema2LockedDueMails.put(new Schema(result.representativeContextId()), lockedMailsOfSchema);
                }
            }

            completionService = null; // Avoid premature unlocking
            callables = null; // Avoid premature unlocking
            return schema2LockedDueMails == null ? Collections.emptyMap() : schema2LockedDueMails;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw ScheduledMailExceptionCodes.UNEXPECTED_ERROR.create(e, "Retrieval of due scheduled mails has been interrupted");
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof OXException) {
                throw (OXException) cause;
            }
            cause = cause == null ? e : cause;
            throw ScheduledMailExceptionCodes.UNEXPECTED_ERROR.create(cause, cause.getMessage());
        } finally {
            if (completionService != null) {
                // Cancel all possibly pending tasks
                completionService.cancel(true);
            }
            if (callables != null) {
                // Something went wrong
                callables.forEach(c -> c.dropLockedMails());
            }
        }
    }

    private static final Object PRESENT = new Object();

    private static Map<UserAndContext, List<LockedScheduledMail>> getLockedScheduledMailsForSchema(Integer representativeContextId, long dueDate, LockExpiryAndRefreshMillis lockExpiryAndRefreshMillis, NeededServices neededServices) throws OXException {
        // Process schema
        PoolAndSchema poolAndSchema = getSchema(representativeContextId, neededServices.contextService);
        if (!checkSchemaStatus(poolAndSchema, neededServices.updater)) {
            // Current schema not ready
            return Collections.emptyMap();
        }

        long lockExceededDate = System.currentTimeMillis() - lockExpiryAndRefreshMillis.lockExpiryMillis();
        Map<UserAndContext, List<ScheduledMail>> dueMailsOfSchema = getDueScheduledMailsForSchema(dueDate, lockExceededDate, representativeContextId.intValue(), neededServices.databaseService, neededServices.services);
        if (dueMailsOfSchema == null || dueMailsOfSchema.isEmpty()) {
            // No due scheduled mail in current schema
            return Collections.emptyMap();
        }

        // Initialize the collection storing the identifiers of the lock entries to refresh
        ConcurrentMap<UUID, Object> markedAsLocked = new ConcurrentHashMap<>();
        ScheduledTimerTask timerTask = null;
        try {
            // Iterate due scheduled mails
            for (List<ScheduledMail> scheduledMailsOfUser : dueMailsOfSchema.values()) {
                for (Iterator<ScheduledMail> it = scheduledMailsOfUser.iterator(); it.hasNext();) {
                    // Check if aborted
                    if (Thread.interrupted()) {
                        throw OXException.general("Interrupted");
                    }

                    ScheduledMail scheduledMail = it.next();
                    // Try to acquire lock
                    if (acquireProcessingLock(scheduledMail, lockExpiryAndRefreshMillis.lockExpiryMillis(), neededServices.databaseService)) {
                        // Lock acquired. Thus put identifier into collection.
                        markedAsLocked.put(scheduledMail.getId(), PRESENT);

                        // Initialize timer task if not already done. Let the timer task run until no scheduled mail is locked anymore.
                        if (timerTask == null) {
                            timerTask = initializeLockRefresher(markedAsLocked, lockExpiryAndRefreshMillis.lockRefreshMillis(), representativeContextId, poolAndSchema, neededServices.timerService, neededServices.services);
                        }
                    } else {
                        // Lock NOT acquired. Thus remove from scheduled mail from user-associated list
                        it.remove();
                    }
                }
            }

            // Check if there is any scheduled mail that has been marked as locked in previous step for current schema
            if (markedAsLocked.isEmpty()) {
                // Nothing could be locked
                return Collections.emptyMap();
            }

            // Yield appropriate locked scheduled mails for locked entries
            ConcurrentMap<UUID, Object> toRemoveFrom = markedAsLocked;
            Map<UserAndContext, List<LockedScheduledMail>> lockedMailsOfSchema = HashMap.newHashMap(dueMailsOfSchema.size());
            for (Map.Entry<UserAndContext, List<ScheduledMail>> e : dueMailsOfSchema.entrySet()) {
                List<ScheduledMail> scheduledMailsOfUser = e.getValue();
                if (!scheduledMailsOfUser.isEmpty()) {
                    lockedMailsOfSchema.put(e.getKey(), scheduledMailsOfUser.stream().map(sm -> new RdbLockedScheduledMail(sm, new OnUnlockRunnable(sm, toRemoveFrom, neededServices.services))).collect(CollectorUtils.toList(scheduledMailsOfUser.size()))); // NOSONARLINT
                }
            }

            // Check if aborted as last step to ensure result is used/needed
            if (Thread.interrupted()) {
                throw OXException.general("Interrupted");
            }

            // All fine. Return schema-associated locked scheduled mails
            timerTask = null;
            markedAsLocked = null;
            return lockedMailsOfSchema;
        } catch (Exception e) {
            // Always have wrapping OXException for detail message
            throw OXException.general("Getting locked scheduled mails for schema " + poolAndSchema.getSchema() + " failed", e);
        } finally {
            if (timerTask != null) {
                timerTask.cancel();
                neededServices.timerService.purge();
            }
            if (markedAsLocked != null) {
                // Something went wrong. Drop all previously acquired locks
                dropLockEntries(markedAsLocked.keySet(), representativeContextId.intValue(), poolAndSchema, neededServices.databaseService);
            }
        }
    }

    private static ScheduledTimerTask initializeLockRefresher(ConcurrentMap<UUID, Object> uuidsToUpdate, long lockRefreshMillis, Integer representativeContextId, PoolAndSchema poolAndSchema, TimerService timerService, ServiceLookup services) {
        AtomicReference<ScheduledTimerTask> timerTaskReference = new AtomicReference<>();
        Runnable task = new LockRefresherRunnable(uuidsToUpdate, representativeContextId.intValue(), timerTaskReference, poolAndSchema, services);

        ScheduledTimerTask timerTask = timerService.scheduleWithFixedDelay(task, lockRefreshMillis, lockRefreshMillis, TimeUnit.MILLISECONDS);
        timerTaskReference.set(timerTask);
        return timerTask;
    }

    private static boolean dropLockEntries(Set<UUID> lockedOnes, int contextId, PoolAndSchema optPoolAndSchema, DatabaseService databaseService) {
        if (lockedOnes.isEmpty()) {
            return false;
        }

        Connection c = null;
        PreparedStatement stmt = null;
        try {
            c = databaseService.getWritable(contextId);

            for (Set<UUID> partition : Sets.partition(lockedOnes, 1000)) {
                if (partition.size() == 1) {
                    stmt = c.prepareStatement("UPDATE `scheduledMail` SET `processing`=0 WHERE `uuid`=?");
                } else {
                    stmt = c.prepareStatement(Databases.getIN("UPDATE `scheduledMail` SET `processing`=0 WHERE `uuid` IN (", partition.size()));
                }
                int pos = 1;
                for (UUID id : partition) {
                    stmt.setBytes(pos++, UUIDs.toByteArray(id));
                }
                stmt.executeUpdate();
                closeSQLStuff(stmt);
                stmt = null;
            }

            LOG.debug("Dropped locks in schema {} for scheduled mails {}", optPoolAndSchema == null ? I(contextId) : optPoolAndSchema.getSchema(), UUIDs.getUnformattedStringObjectFor(lockedOnes));
        } catch (Exception e) {
            LOG.warn("Failed to drop locks in schema {} for scheduled mails {}", optPoolAndSchema == null ? I(contextId) : optPoolAndSchema.getSchema(), UUIDs.getUnformattedStringObjectFor(lockedOnes), e);
        } finally {
            closeSQLStuff(stmt);
            if (c != null) {
                databaseService.backWritable(contextId, c);
            }
        }
        return true;
    }

    private static boolean acquireProcessingLock(ScheduledMail scheduledMail, long lockExpiryMillis, DatabaseService databaseService) throws OXException {
        boolean modified = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = databaseService.getWritable(scheduledMail.getContextId());
            stmt = con.prepareStatement("SELECT `processing` FROM `scheduledMail` WHERE `cid`=? AND `user`=? AND `uuid`=?");
            stmt.setInt(1, scheduledMail.getContextId());
            stmt.setInt(2, scheduledMail.getUserId());
            stmt.setBytes(3, UUIDs.toByteArray(scheduledMail.getId()));
            rs = stmt.executeQuery();
            if (!rs.next()) {
                // No such entry...?
                return false;
            }
            long stamp = rs.getLong(1);
            Databases.closeSQLStuff(rs, stmt);
            rs = null;
            stmt = null;

            long now = System.currentTimeMillis();
            if (stamp != 0 && (now - stamp <= lockExpiryMillis)) {
                // Already locked and not yet expired
                return false;
            }

            // Not locked or lock is expired
            stmt = con.prepareStatement("UPDATE `scheduledMail` SET `processing`=? WHERE `cid`=? AND `user`=? AND `uuid`=? AND `processing`=?");
            stmt.setLong(1, now);
            stmt.setInt(2, scheduledMail.getContextId());
            stmt.setInt(3, scheduledMail.getUserId());
            stmt.setBytes(4, UUIDs.toByteArray(scheduledMail.getId()));
            stmt.setLong(5, stamp);
            modified = stmt.executeUpdate() > 0;
            Databases.closeSQLStuff(stmt);
            stmt = null;

            return modified;
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
            if (con != null) {
                if (modified) {
                    databaseService.backWritable(scheduledMail.getContextId(), con);
                } else {
                    databaseService.backWritableAfterReading(scheduledMail.getContextId(), con);
                }
            }
        }
    }

    private static Map<UserAndContext, List<ScheduledMail>> getDueScheduledMailsForSchema(long dueDate, long lockExceededDate, int representativeContextId, DatabaseService databaseService, ServiceLookup services) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection con = databaseService.getReadOnly(representativeContextId);
        try {
            stmt = con.prepareStatement("SELECT `uuid`, `dateToSend`, `mailPath`, `meta`, `cid`, `user` FROM `scheduledMail` WHERE `dateToSend` <= ? AND (`processing` = 0 OR `processing` < ?)");
            stmt.setLong(1, dueDate);
            stmt.setLong(2, lockExceededDate);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return Collections.emptyMap();
            }

            ObfuscatorService obfuscator = services.getServiceSafe(ObfuscatorService.class);
            Map<UserAndContext, List<ScheduledMail>> scheduledMails = new HashMap<>();
            do {
                int contextId = rs.getInt(5);
                int userId = rs.getInt(6);
                scheduledMails.computeIfAbsent(UserAndContext.newInstance(userId, contextId), Functions.getNewLinkedListFuntion()).add(
                    RdbScheduledMail.builder()
                    .withId(UUIDs.toUUID(rs.getBytes(1)))
                    .withDateToSend(rs.getLong(2))
                    .withMailPath(rs.getString(3))
                    .withMeta(MetaConverter.parseStringToMeta(rs.getString(4), obfuscator))
                    .withContextId(contextId)
                    .withUserId(userId)
                    .build()
                );
            } while (rs.next());
            return scheduledMails;
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
            databaseService.backReadOnly(representativeContextId, con);
        }
    }

    /**
     * Checks the update status of given schema.
     *
     * @param poolAndSchema The pool and schema information
     * @param updater The update instance to use
     * @return <code>true</code> if schema update status is up-to-date, otherwise <code>false</code> if either currently updating or updates are pending
     * @throws OXException If update status cannot be checked
     */
    private static boolean checkSchemaStatus(PoolAndSchema poolAndSchema, Updater updater) throws OXException {
        UpdateStatus status = updater.getStatus(poolAndSchema.getSchema(), poolAndSchema.getPoolId());
        if (status.blockingUpdatesRunning()) {
            // Context-associated schema is currently updated. Abort clean-up for that schema
            LOG.info("Update running: Skipping detection of due scheduled mails for schema {}", poolAndSchema.getSchema());
            return false;
        }
        if ((status.needsBlockingUpdates() || status.needsBackgroundUpdates()) && !status.blockingUpdatesRunning() && !status.backgroundUpdatesRunning()) {
            // Context-associated schema needs an update. Abort clean-up for that schema
            LOG.info("Update needed: Skipping detection of due scheduled mails for schema {} since that schema needs an update", poolAndSchema.getSchema());
            return false;
        }
        return true;
    }

    private static PoolAndSchema getSchema(Integer representativeContextId, ContextService contextService) throws OXException {
        Map<PoolAndSchema, List<Integer>> associations = contextService.getSchemaAssociationsFor(Collections.singletonList(representativeContextId));
        if (associations.isEmpty()) {
            throw OXException.general("No such database pool and schema found for context " + representativeContextId);
        }
        return associations.keySet().iterator().next();
    }

    @Override
    public ScheduledMail createScheduledMail(UUID optionalId, ScheduledMailDescription scheduledMailDesc, int maxNumberOfScheduledMails, int maxNumberOfScheduledMailsPerHour, Session session) throws OXException {
        String sObfuscatedMeta = MetaConverter.convertMetaToString(scheduledMailDesc.getMeta(), services.getServiceSafe(ObfuscatorService.class)).orElse(null);

        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);

        int contextId = session.getContextId();
        if (maxNumberOfScheduledMails <= 0 && maxNumberOfScheduledMailsPerHour <= 0) {
            Connection con = databaseService.getWritable(contextId);
            try {
                return createScheduledMail(optionalId, scheduledMailDesc, sObfuscatedMeta, 0, 0, session, con);
            } finally {
                databaseService.backWritable(contextId, con);
            }
        }

        // Acquire WRITE lock prior to adding to database
        SchemaLock lock = new RdbExpiringLock(new RdbExpiringLockKey("writelock", session.getUserId(), contextId), Duration.ofMinutes(1), contextId, services);
        if (!lock.acquireLock()) {
            int retry = 1;
            do {
                exponentialBackoffWait(retry++, 1000L);
            } while (!lock.acquireLock());
        }
        try {
            boolean modified = false;
            Connection con = databaseService.getWritable(contextId);
            try {
                ScheduledMail createdScheduledMail = createScheduledMail(optionalId, scheduledMailDesc, sObfuscatedMeta, maxNumberOfScheduledMails, maxNumberOfScheduledMailsPerHour, session, con);
                modified = true;
                return createdScheduledMail;
            } finally {
                if (modified) {
                    databaseService.backWritable(contextId, con);
                } else {
                    databaseService.backWritableAfterReading(contextId, con);
                }
            }
        } finally {
            lock.unlock(false);
        }
    }

    private static final long MILLIS_30_MINUTES = 1_800_000L;

    private static ScheduledMail createScheduledMail(UUID optionalId, ScheduledMailDescription scheduledMailDesc, String sObfuscatedMeta, int maxNumberOfScheduledMails, int maxNumberOfScheduledMailsPerHour, Session session, Connection con) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            if (maxNumberOfScheduledMails > 0) {
                stmt = con.prepareStatement("SELECT COUNT(`uuid`) FROM `scheduledMail` WHERE `cid`=? and `user`=?");
                stmt.setInt(1, session.getContextId());
                stmt.setInt(2, session.getUserId());
                rs = stmt.executeQuery();
                int count = rs.next() ? rs.getInt(1) : 0;
                if (count >= maxNumberOfScheduledMails) {
                    throw ScheduledMailExceptionCodes.TOO_MANY_SCHEDULED_MAILS.create(I(maxNumberOfScheduledMails));
                }
                Databases.closeSQLStuff(rs, stmt);
                rs = null;
                stmt = null;
            }
            if (maxNumberOfScheduledMailsPerHour > 0) {
                stmt = con.prepareStatement("SELECT COUNT(`uuid`) FROM `scheduledMail` WHERE `cid`=? and `user`=? AND (`dateToSend` >= ? AND `dateToSend` <= ?)");
                stmt.setInt(1, session.getContextId());
                stmt.setInt(2, session.getUserId());
                stmt.setLong(3, scheduledMailDesc.getDateToSend() - MILLIS_30_MINUTES);
                stmt.setLong(4, scheduledMailDesc.getDateToSend() + MILLIS_30_MINUTES);
                rs = stmt.executeQuery();
                int count = rs.next() ? rs.getInt(1) : 0;
                if (count >= maxNumberOfScheduledMailsPerHour) {
                    throw ScheduledMailExceptionCodes.TOO_MANY_SCHEDULED_MAILS_PER_HOUR.create(I(maxNumberOfScheduledMailsPerHour));
                }
                Databases.closeSQLStuff(rs, stmt);
                rs = null;
                stmt = null;
            }

            UUID id = optionalId == null ?  UUID.randomUUID() : optionalId;
            stmt = con.prepareStatement("INSERT INTO `scheduledMail` (`uuid`, `cid`, `user`, `dateToSend`, `mailPath`, `meta`) VALUES (?, ?, ?, ?, ?, ?)");
            stmt.setBytes(1, UUIDs.toByteArray(id));
            stmt.setInt(2, session.getContextId());
            stmt.setInt(3, session.getUserId());
            stmt.setLong(4, scheduledMailDesc.getDateToSend());
            stmt.setString(5, scheduledMailDesc.getMailPath());
            if (sObfuscatedMeta == null) {
                stmt.setNull(6, Types.VARCHAR);
            } else {
                stmt.setString(6, sObfuscatedMeta);
            }
            stmt.executeUpdate();

            return RdbScheduledMail.builder()
                .withId(id)
                .withUserId(session.getUserId())
                .withContextId(session.getContextId())
                .withDateToSend(scheduledMailDesc.getDateToSend())
                .withMailPath(scheduledMailDesc.getMailPath())
                .withMeta(scheduledMailDesc.getMeta())
                .build();
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    @Override
    public ScheduledMail updateScheduledMail(UUID id, ScheduledMailDescription scheduledMailDesc, int maxNumberOfScheduledMailsPerHour, Session session) throws OXException {
        String sMeta = null;
        if (scheduledMailDesc.containsMeta()) {
            sMeta = MetaConverter.convertMetaToString(scheduledMailDesc.getMeta(), services.getServiceSafe(ObfuscatorService.class)).orElse(null);
        }

        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);

        int contextId = session.getContextId();
        if (maxNumberOfScheduledMailsPerHour <= 0 || !scheduledMailDesc.containsDateToSend()) {
            BoolReference modified = new BoolReference(false);
            Connection con = databaseService.getWritable(contextId);
            try {
                return updateScheduledMail(id, scheduledMailDesc, sMeta, maxNumberOfScheduledMailsPerHour, session, modified, con);
            } finally {
                if (modified.getValue()) {
                    databaseService.backWritable(contextId, con);
                } else {
                    databaseService.backWritableAfterReading(contextId, con);
                }
            }
        }

        // Acquire WRITE lock prior to adding to database
        SchemaLock lock = new RdbExpiringLock(new RdbExpiringLockKey("writelock", session.getUserId(), contextId), Duration.ofMinutes(1), contextId, services);
        if (!lock.acquireLock()) {
            int retry = 1;
            do {
                exponentialBackoffWait(retry++, 1000L);
            } while (!lock.acquireLock());
        }
        try {
            BoolReference modified = new BoolReference(false);
            Connection con = databaseService.getWritable(contextId);
            try {
                return updateScheduledMail(id, scheduledMailDesc, sMeta, maxNumberOfScheduledMailsPerHour, session, modified, con);
            } finally {
                if (modified.getValue()) {
                    databaseService.backWritable(contextId, con);
                } else {
                    databaseService.backWritableAfterReading(contextId, con);
                }
            }
        } finally {
            lock.unlock(false);
        }
    }

    private ScheduledMail updateScheduledMail(UUID id, ScheduledMailDescription scheduledMailDesc, String sMeta, int maxNumberOfScheduledMailsPerHour, Session session, BoolReference modified, Connection con) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            if (maxNumberOfScheduledMailsPerHour > 0 && scheduledMailDesc.containsDateToSend()) {
                stmt = con.prepareStatement("SELECT COUNT(`uuid`) FROM `scheduledMail` WHERE `cid`=? and `user`=? AND (`dateToSend` >= ? AND `dateToSend` <= ?) AND `uuid`<>?");
                stmt.setInt(1, session.getContextId());
                stmt.setInt(2, session.getUserId());
                stmt.setLong(3, scheduledMailDesc.getDateToSend() - MILLIS_30_MINUTES);
                stmt.setLong(4, scheduledMailDesc.getDateToSend() + MILLIS_30_MINUTES);
                stmt.setBytes(5, UUIDs.toByteArray(id));
                rs = stmt.executeQuery();
                int count = rs.next() ? rs.getInt(1) : 0;
                if (count >= maxNumberOfScheduledMailsPerHour) {
                    throw ScheduledMailExceptionCodes.TOO_MANY_SCHEDULED_MAILS_PER_HOUR.create(I(maxNumberOfScheduledMailsPerHour));
                }
                Databases.closeSQLStuff(rs, stmt);
                rs = null;
                stmt = null;
            }

            SqlUpdateBuilder updateBuilder = new SqlUpdateBuilder("scheduledMail");
            if (scheduledMailDesc.containsDateToSend()) {
                updateBuilder.addColumnToSet("dateToSend");
            }
            if (scheduledMailDesc.containsMailPath() && scheduledMailDesc.getMailPath() != null) {
                updateBuilder.addColumnToSet("mailPath");
            }
            if (scheduledMailDesc.containsMeta()) {
                updateBuilder.addColumnToSet("meta");
            }
            String sqlStmt = updateBuilder.finish(" WHERE `cid`=? AND `user`=? AND `uuid`=?").orElse(null);
            updateBuilder = null;
            if (sqlStmt == null) {
                return getScheduledMail(id, session, true, con);
            }

            stmt = con.prepareStatement(sqlStmt);
            int pos = 1;
            if (scheduledMailDesc.containsDateToSend()) {
                stmt.setLong(pos++, scheduledMailDesc.getDateToSend());
            }
            if (scheduledMailDesc.containsMailPath() && scheduledMailDesc.getMailPath() != null) {
                stmt.setString(pos++, scheduledMailDesc.getMailPath());
            }
            if (scheduledMailDesc.containsMeta()) {
                if (sMeta == null) {
                    stmt.setNull(pos++, Types.VARCHAR);
                } else {
                    stmt.setString(pos++, sMeta);
                }
            }
            stmt.setInt(pos++, session.getContextId());
            stmt.setInt(pos++, session.getUserId());
            stmt.setBytes(pos, UUIDs.toByteArray(id));
            int rows = stmt.executeUpdate();
            modified.setValue(rows > 0);

            return getScheduledMail(id, session, true, con);
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    @Override
    public Optional<ScheduledMail> deleteScheduledMail(UUID id, Session session) throws OXException {
        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        Connection con = databaseService.getWritable(session.getContextId());
        try {
            return Optional.ofNullable(deleteScheduledMail(id, session, con));
        } finally {
            databaseService.backWritable(session.getContextId(), con);
        }
    }

    private ScheduledMail deleteScheduledMail(UUID id, Session session, Connection con) throws OXException {
        ScheduledMail scheduledMail = getScheduledMail(id, session, false, con);
        if (scheduledMail == null) {
            return null;
        }

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("DELETE FROM `scheduledMail` WHERE `cid`=? AND `user`=? AND `uuid`=?");
            stmt.setInt(1, session.getContextId());
            stmt.setInt(2, session.getUserId());
            stmt.setBytes(3, UUIDs.toByteArray(id));
            int rows = stmt.executeUpdate();
            return rows <= 0 ? null : scheduledMail;
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(stmt);
        }
    }

    @Override
    public ScheduledMail[] deleteScheduledMails(Collection<UUID> ids, Session session) throws OXException {
        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        Connection con = databaseService.getWritable(session.getContextId());
        try {
            return deleteScheduledMails(ids, session, con);
        } finally {
            databaseService.backWritable(session.getContextId(), con);
        }
    }

    private ScheduledMail[] deleteScheduledMails(Collection<UUID> ids, Session session, Connection con) throws OXException {
        Map<UUID, ScheduledMail> scheduledMails = getScheduledMails(ids, session, con);
        if (scheduledMails.isEmpty()) {
            ScheduledMail[] retval = new ScheduledMail[ids.size()];
            Arrays.fill(retval, null);
            return retval;
        }

        PreparedStatement stmt = null;
        try {
            if (ids.size() == 1) {
                stmt = con.prepareStatement("DELETE FROM `scheduledMail` WHERE `cid`=? AND `user`=? AND `uuid`=?");
            } else {
                stmt = con.prepareStatement(Databases.getIN("DELETE FROM `scheduledMail` WHERE `cid`=? AND `user`=? AND `uuid` IN (", ids.size()));
            }
            int pos = 1;
            stmt.setInt(pos++, session.getContextId());
            stmt.setInt(pos++, session.getUserId());
            for (UUID id : ids) {
                stmt.setBytes(pos++, UUIDs.toByteArray(id));
            }
            stmt.executeUpdate();
            Databases.closeSQLStuff(stmt);
            stmt = null;

            ScheduledMail[] retval = new ScheduledMail[ids.size()];
            int index = 0;
            for (UUID id : ids) {
                retval[index++] = scheduledMails.get(id);
            }
            return retval;
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(stmt);
        }
    }

    @Override
    public boolean deleteScheduledMails(Collection<UUID> ids, int contextId) throws OXException {
        if (ids.isEmpty()) {
            return false;
        }

        boolean modified = false;
        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        Connection con = databaseService.getWritable(contextId);
        try {
            modified = deleteScheduledMails(ids, con);
            return modified;
        } finally {
            if (modified) {
                databaseService.backWritable(contextId, con);
            } else {
                databaseService.backWritableAfterReading(contextId, con);
            }
        }
    }

    private static boolean deleteScheduledMails(Collection<UUID> ids, Connection con) throws OXException {
        PreparedStatement stmt = null;
        try {
            if (ids.size() == 1) {
                stmt = con.prepareStatement("DELETE FROM `scheduledMail` WHERE `uuid`=?");
            } else {
                stmt = con.prepareStatement(Databases.getIN("DELETE FROM `scheduledMail` WHERE `uuid` IN (", ids.size()));
            }
            int pos = 1;
            for (UUID id : ids) {
                stmt.setBytes(pos++, UUIDs.toByteArray(id));
            }
            return stmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(stmt);
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private class LockedScheduledMailsForSchemaCallable implements TrackableCallable<RepresentativeContextIdAndLockedScheduledMails> {

        private final NeededServices neededServices;
        private final LockExpiryAndRefreshMillis lockExpiryAndRefreshMillis;
        private final long dueDate;
        private final Integer representativeContextId;

        private final AtomicReference<Map<UserAndContext, List<LockedScheduledMail>>> lockedMailsOfSchemaReference;

        LockedScheduledMailsForSchemaCallable(Integer representativeContextId, long dueDate, LockExpiryAndRefreshMillis lockExpiryAndRefreshMillis, NeededServices neededServices) {
            super();
            this.neededServices = neededServices;
            this.lockExpiryAndRefreshMillis = lockExpiryAndRefreshMillis;
            this.dueDate = dueDate;
            this.representativeContextId = representativeContextId;
            this.lockedMailsOfSchemaReference = new AtomicReference<>();
        }

        @Override
        public RepresentativeContextIdAndLockedScheduledMails call() throws Exception {
            Map<UserAndContext, List<LockedScheduledMail>> lockedMailsOfSchema = getLockedScheduledMailsForSchema(representativeContextId, dueDate, lockExpiryAndRefreshMillis, neededServices);
            this.lockedMailsOfSchemaReference.set(lockedMailsOfSchema);
            return new RepresentativeContextIdAndLockedScheduledMails(representativeContextId.intValue(), lockedMailsOfSchema);
        }

        /**
         * Drops all locked scheduled mails acquired by this callable.
         */
        public void dropLockedMails() {
            Map<UserAndContext, List<LockedScheduledMail>> lockedMails = lockedMailsOfSchemaReference.get();
            if (lockedMails != null) {
                Set<UUID> uuids = new HashSet<>();
                lockedMails.values().forEach(lockedScheduledMails -> lockedScheduledMails.forEach(lockedScheduledMail -> uuids.add(lockedScheduledMail.getId())));
                dropLockEntries(uuids, representativeContextId.intValue(), null, neededServices.databaseService);
            }
        }
    }

    /**
     * Simple tuple for representative context identifier and locked scheduled mails of that context-associated schema.
     */
    private static record RepresentativeContextIdAndLockedScheduledMails(int representativeContextId, Map<UserAndContext, List<LockedScheduledMail>> lockedMailsOfSchema) {

        /**
         * Gets the representative context identifier.
         *
         * @return The representative context identifier
         */
        public int representativeContextId() { // NOSONARLINT
            return representativeContextId;
        }

        /**
         * Gets the locked scheduled mails of that context-associated schema.
         *
         * @return The locked scheduled mails of that context-associated schema
         */
        public Map<UserAndContext, List<LockedScheduledMail>> lockedMailsOfSchema() { // NOSONARLINT
            return lockedMailsOfSchema;
        }
    }

    private LockExpiryAndRefreshMillis getLockExpiryAndRefreshMillis() throws OXException {
        LeanConfigurationService configurationService = services.getServiceSafe(LeanConfigurationService.class);
        long lockExpiryMillis = Duration.ofMinutes(configurationService.getIntProperty(ScheduledMailProperty.LOCK_EXPIRY_MINUTES)).toMillis();
        long lockRefreshMillis = Duration.ofMinutes(configurationService.getIntProperty(ScheduledMailProperty.LOCK_REFRESH_MINUTES)).toMillis();
        return new LockExpiryAndRefreshMillis(lockExpiryMillis, lockRefreshMillis);
    }

    /**
     * Simple tuple for common needed services.
     */
    private static record NeededServices(DatabaseService databaseService, TimerService timerService, ContextService contextService, Updater updater, ServiceLookup services) {
        // Don't care
    }

}
