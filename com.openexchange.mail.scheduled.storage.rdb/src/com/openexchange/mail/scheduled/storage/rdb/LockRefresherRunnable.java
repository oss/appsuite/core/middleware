/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.storage.rdb;

import static com.openexchange.database.Databases.closeSQLStuff;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.context.PoolAndSchema;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.java.Sets;
import com.openexchange.java.util.UUIDs;
import com.openexchange.server.ServiceLookup;
import com.openexchange.timer.ScheduledTimerTask;

/**
 * Refreshes the acquired lock entries in a database schema according to content of passed collection of identifiers.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.14.0
 */
public class LockRefresherRunnable implements Runnable {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(LockRefresherRunnable.class);

    private final ServiceLookup services;
    private final ConcurrentMap<UUID, Object> uuidsToUpdate;
    private final AtomicReference<ScheduledTimerTask> timerTaskReference;
    private final int contextId;
    private final PoolAndSchema poolAndSchema;

    /**
     * Initializes a new {@link LockRefresherRunnable}.
     *
     * @param uuidsToUpdate The collection of identifiers to update
     * @param contextId The context identifier
     * @param timerTaskReference The reference for the timer task for self-termination
     * @param poolAndSchema The schema information about target database schema
     * @param services The service look-up to acquire database service
     */
    public LockRefresherRunnable(ConcurrentMap<UUID, Object> uuidsToUpdate, int contextId, AtomicReference<ScheduledTimerTask> timerTaskReference, PoolAndSchema poolAndSchema, ServiceLookup services) {
        super();
        this.poolAndSchema = poolAndSchema;
        this.services = services;
        this.uuidsToUpdate = uuidsToUpdate;
        this.timerTaskReference = timerTaskReference;
        this.contextId = contextId;
    }

    @Override
    public void run() {
        boolean somethingDone = updateLockEntries(uuidsToUpdate, contextId, poolAndSchema, services);
        if (!somethingDone) {
            // Self-terminate...
            ScheduledTimerTask stt = timerTaskReference.getAndSet(null);
            if (stt != null) {
                stt.cancel();
                LOG.debug("Terminated lock refresher for schema {}", poolAndSchema.getSchema());
            }
        }
    }

    private static boolean updateLockEntries(ConcurrentMap<UUID, Object> uuidsToUpdate, int contextId, PoolAndSchema poolAndSchema, ServiceLookup services) {
        if (uuidsToUpdate.isEmpty()) {
            return false;
        }

        DatabaseService databaseService = null;
        Connection c = null;
        PreparedStatement stmt = null;
        try {
            databaseService = services.getServiceSafe(DatabaseService.class);
            c = databaseService.getWritable(contextId);

            for (Set<UUID> partition : Sets.partition(uuidsToUpdate.keySet(), 1000)) {
                if (partition.size() == 1) {
                    stmt = c.prepareStatement("UPDATE `scheduledMail` SET `processing`=? WHERE `uuid`=?");
                } else {
                    stmt = c.prepareStatement(Databases.getIN("UPDATE `scheduledMail` SET `processing`=? WHERE `uuid` IN (", partition.size()));
                }
                int pos = 1;
                stmt.setLong(pos++, System.currentTimeMillis());
                for (UUID id : partition) {
                    stmt.setBytes(pos++, UUIDs.toByteArray(id));
                }
                stmt.executeUpdate();
                closeSQLStuff(stmt);
                stmt = null;
            }

            LOG.debug("Refreshed locks in schema {} for scheduled mails {}", poolAndSchema.getSchema(), UUIDs.getUnformattedStringObjectFor(uuidsToUpdate.keySet()));
        } catch (Exception e) {
            LOG.warn("Failed to refresh locks in schema {} for scheduled mails {}", poolAndSchema.getSchema(), UUIDs.getUnformattedStringObjectFor(uuidsToUpdate.keySet()), e);
        } finally {
            closeSQLStuff(stmt);
            if (databaseService != null && c != null) {
                databaseService.backWritable(contextId, c);
            }
        }
        return true;
    }
}