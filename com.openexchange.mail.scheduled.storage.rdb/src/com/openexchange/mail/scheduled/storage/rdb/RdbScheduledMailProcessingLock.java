/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.mail.scheduled.storage.rdb;

import static com.openexchange.database.Databases.closeSQLStuff;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.UUID;
import com.openexchange.database.DatabaseService;
import com.openexchange.java.util.UUIDs;
import com.openexchange.server.ServiceLookup;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;


/**
 * {@link RdbScheduledMailProcessingLock} - The database-backed lock for a scheduled mail.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RdbScheduledMailProcessingLock {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(RdbScheduledMailProcessingLock.class);

    private final UUID scheduledMailId;
    private final int contextId;
    private final ScheduledTimerTask timerTask;
    private final ServiceLookup services;

    /**
     * Initializes a new {@link RdbScheduledMailProcessingLock}.
     *
     * @param scheduledMailId The identifier of the scheduled mail
     * @param contextId The context identifier providing associated database schema
     * @param timerTask The timer task that refreshes lock
     * @param services The service look-up
     */
    public RdbScheduledMailProcessingLock(UUID scheduledMailId, int contextId, ScheduledTimerTask timerTask, ServiceLookup services) {
        super();
        this.scheduledMailId = scheduledMailId;
        this.contextId = contextId;
        this.timerTask = timerTask;
        this.services = services;
    }

    /**
     * Releases this processing lock.
     */
    public void unlock() {
        if (timerTask != null) {
            timerTask.cancel();
            TimerService timerService = services.getOptionalService(TimerService.class);
            if (timerService != null) {
                timerService.purge();
            }
        }

        DatabaseService databaseService = services.getOptionalService(DatabaseService.class);
        if (databaseService == null) {
            LOG.warn("Failed to delete lock for scheduled mail {} since database service is absent", UUIDs.getUnformattedStringObjectFor(scheduledMailId));
            return;
        }

        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = databaseService.getWritable(contextId);
            stmt = con.prepareStatement("UPDATE `scheduledMail` SET `processing` = 0 WHERE `uuid` = ?");
            stmt.setBytes(1, UUIDs.toByteArray(scheduledMailId));
            stmt.executeUpdate();
        } catch (Exception e) {
            LOG.warn("Failed to delete lock for scheduled mail {}", UUIDs.getUnformattedStringObjectFor(scheduledMailId), e);
        } finally {
            closeSQLStuff(stmt);
            databaseService.backWritable(contextId, con);
        }
    }

}
