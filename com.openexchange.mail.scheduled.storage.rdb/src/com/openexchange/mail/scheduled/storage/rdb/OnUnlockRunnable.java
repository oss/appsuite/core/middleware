/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.storage.rdb;

import static com.openexchange.java.Autoboxing.I;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.java.util.UUIDs;
import com.openexchange.mail.scheduled.LockedScheduledMail;
import com.openexchange.mail.scheduled.ScheduledMail;
import com.openexchange.mail.scheduled.ScheduledMailExceptionCodes;
import com.openexchange.server.ServiceLookup;

/**
 * The task to perform when {@link LockedScheduledMail#unlock()} is invoked.
 * <p>
 * <ul>
 * <li>Removes identifier of associated scheduled mail from collection</li>
 * <li>Drops the lock entry from "<i>scheduledMail</i>" table; e.g. resets "<i>processing</i>" column back to <code>0</code> (zero)</li>
 * </ul>
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.14.0
 */
public class OnUnlockRunnable implements Runnable {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(OnUnlockRunnable.class);

    private final ServiceLookup services;
    private final ScheduledMail scheduledMail;
    private final ConcurrentMap<UUID, Object> uuidsToRemoveFrom;

    /**
     * Initializes a new {@link OnUnlockRunnable}.
     *
     * @param scheduledMail The scheduled mail to unlock
     * @param uuidsToRemoveFrom The collection to remove its identifier from
     * @param services The service look-up to acquire database service on execution
     */
    public OnUnlockRunnable(ScheduledMail scheduledMail, ConcurrentMap<UUID, Object> uuidsToRemoveFrom, ServiceLookup services) {
        super();
        this.services = services;
        this.scheduledMail = scheduledMail;
        this.uuidsToRemoveFrom = uuidsToRemoveFrom;
    }

    @Override
    public void run() {
        // Remove identifier from collection...
        uuidsToRemoveFrom.remove(scheduledMail.getId());

        // ... and delete lock
        try {
            dropProcessingLock(scheduledMail, services.getServiceSafe(DatabaseService.class));
        } catch (Exception x) {
            LOG.warn("Failed to delete lock for scheduled mail {} of user {} in context {}", UUIDs.getUnformattedStringObjectFor(scheduledMail.getId()), I(scheduledMail.getUserId()), I(scheduledMail.getContextId()), x);
        }
    }

    /**
     * Drops the lock entry that is to reset "<i>processing</i>" column back to <code>0</code> (zero).
     *
     * @param scheduledMail The scheduled mail to reset for
     * @param databaseService The database service to acquire a connection
     * @throws OXException If operation fails
     */
    private static void dropProcessingLock(ScheduledMail scheduledMail, DatabaseService databaseService) throws OXException {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = databaseService.getWritable(scheduledMail.getContextId());
            stmt = con.prepareStatement("UPDATE `scheduledMail` SET `processing` = 0 WHERE `uuid` = ?");
            stmt.setBytes(1, UUIDs.toByteArray(scheduledMail.getId()));
            stmt.executeUpdate();
            LOG.debug("Deleted lock for scheduled mail {} of user {} in context {}", UUIDs.getUnformattedStringObjectFor(scheduledMail.getId()), I(scheduledMail.getUserId()), I(scheduledMail.getContextId()));
        } catch (SQLException e) {
            throw ScheduledMailExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(stmt);
            if (con != null) {
                databaseService.backWritable(scheduledMail.getContextId(), con);
            }
        }
    }
} // End of OnUnlockRunnable class