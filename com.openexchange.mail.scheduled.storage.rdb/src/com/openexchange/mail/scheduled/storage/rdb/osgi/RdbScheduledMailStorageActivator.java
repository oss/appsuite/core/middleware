/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.scheduled.storage.rdb.osgi;

import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.database.CreateTableService;
import com.openexchange.database.DatabaseService;
import com.openexchange.groupware.delete.DeleteListener;
import com.openexchange.groupware.update.DefaultUpdateTaskProviderService;
import com.openexchange.groupware.update.UpdateTaskProviderService;
import com.openexchange.mail.scheduled.ScheduledMailStorageService;
import com.openexchange.mail.scheduled.storage.rdb.RdbScheduledMailStorageService;
import com.openexchange.mail.scheduled.storage.rdb.groupware.ScheduledMailCreateTableService;
import com.openexchange.mail.scheduled.storage.rdb.groupware.ScheduledMailCreateTableTask;
import com.openexchange.mail.scheduled.storage.rdb.groupware.ScheduledMailDeleteListener;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.session.ObfuscatorService;
import com.openexchange.threadpool.ThreadPoolService;
import com.openexchange.timer.TimerService;

/**
 * {@link RdbScheduledMailStorageActivator} - The activator for <i>"com.openexchange.mail.scheduled.storage.rdb"</i> bundle.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RdbScheduledMailStorageActivator extends HousekeepingActivator {

    /**
     * Initializes a new {@link RdbScheduledMailStorageActivator}.
     */
    public RdbScheduledMailStorageActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { DatabaseService.class, TimerService.class, ContextService.class, ObfuscatorService.class,
            LeanConfigurationService.class, ThreadPoolService.class };
    }

    @Override
    protected synchronized void startBundle() throws Exception {
        // Register Groupware stuff.
        registerService(CreateTableService.class, ScheduledMailCreateTableService.getInstance());
        registerService(UpdateTaskProviderService.class, new DefaultUpdateTaskProviderService(
            new ScheduledMailCreateTableTask()
        ));

        registerService(DeleteListener.class, new ScheduledMailDeleteListener());

        registerService(ScheduledMailStorageService.class, new RdbScheduledMailStorageService(this));
    }

}
