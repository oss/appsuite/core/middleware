/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.request.analyzer.rest.data;

import java.lang.reflect.Type;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.openexchange.servlet.Header;

/**
 * {@link HeaderDeserializer} is a JSON deserializer for {@link Header}s
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class HeaderDeserializer implements JsonDeserializer<Header> {

    @Override
    public Header deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject obj = json.getAsJsonObject();
        if (obj == null) {
            throw new JsonParseException("Missing header fields");
        }

        JsonElement nameElement = obj.get("name");
        if (nameElement == null) {
            throw new JsonParseException("Missing \"name\" field");
        }
        String name = nameElement.getAsString();

        JsonElement valueElement = obj.get("value");
        if (valueElement == null) {
            throw new JsonParseException("Missing \"value\" field");
        }
        String value = valueElement.getAsString();
        return new Header(name, value);
    }

}
