/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.request.analyzer.rest;

import java.io.IOException;
import java.io.Writer;
import java.util.Base64;
import java.util.Locale;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.io.output.StringBuilderWriter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONServices;
import org.json.SimpleJSONSerializer;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.ajax.writer.ResponseWriter;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.net.ClientIPUtil;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.BodyData;
import com.openexchange.request.analyzer.ByteArrayBodyData;
import com.openexchange.request.analyzer.RequestAnalyzerExceptionCodes;
import com.openexchange.request.analyzer.RequestAnalyzerService;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.request.analyzer.UserInfo;
import com.openexchange.rest.services.CustomStatus;
import com.openexchange.rest.services.JAXRSService;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.segment.SegmentMarker;
import com.openexchange.server.ServiceLookup;
import com.openexchange.servlet.Headers;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * {@link RequestAnalyzerServlet} is a servlet which allows to analyze request data.
 *
 * This data is then mapped to a marker if possible.
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
@RoleAllowed(Role.BASIC_AUTHENTICATED)
@Path("/request-analysis/v1/")
public class RequestAnalyzerServlet extends JAXRSService {

    private static final Logger LOG = LoggerFactory.getLogger(RequestAnalyzerServlet.class);

    private static final CustomStatus MISSING_BODY_STATUS_TYPE = new CustomStatus(422, "Unprocessable Entity: Request body required to analyze the request");

    private static final Response NOT_FOUND_RESPONSE = Response.status(Status.NOT_FOUND).build();
    private static final Response BODY_MISSING_RESPONSE = Response.status(MISSING_BODY_STATUS_TYPE).build();
    private static final Response INTERNAL_SERVER_ERROR_RESPONSE = Response.status(Status.INTERNAL_SERVER_ERROR).build();

    private final ErrorAwareSupplier<RequestAnalyzerService> service;
    private final ErrorAwareSupplier<ClientIPUtil> ipUtilService;

    /**
     * Initializes a new {@link RequestAnalyzerServlet}.
     *
     * @param bundleContext The bundle context
     * @param services The {@link ServiceLookup}
     */
    public RequestAnalyzerServlet(BundleContext bundleContext, ServiceLookup services) {
        super(bundleContext);
        this.service = () -> services.getServiceSafe(RequestAnalyzerService.class);
        this.ipUtilService = () -> services.getServiceSafe(ClientIPUtil.class);
    }

    /**
     * Analyzes the request and determines a marker for it if possible
     *
     * @param body The body defining the request
     * @return The response
     */
    @POST
    @Path("/analyze")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response analyze(String body) {
        try {
            checkBody(body);
            RequestData data = convert2RequestData(body);
            AnalyzeResult result = service.get().analyzeRequest(data);
            return switch (result.getType()) {
                case SUCCESS -> Response.ok(convert2jsonText(result)).build();
                case MISSING_BODY -> BODY_MISSING_RESPONSE;
                case UNKNOWN -> NOT_FOUND_RESPONSE;
            };
        } catch (OXException e) {
            if (RequestAnalyzerExceptionCodes.PREFIX.equals(e.getPrefix())) {
                return Response.status(Status.BAD_REQUEST)
                               .entity(e.getDisplayMessage(Locale.US))
                               .build();
            }
            return generateInternalServerError(e);
        } catch (JSONException e) {
            return Response.status(Status.BAD_REQUEST)
                           .entity(e.getMessage())
                           .build();
        }
    }

    // ----------------------- private methods -------------------------

    /**
     * Checks the body
     *
     * @param body The body to check
     * @throws JSONException If body is absent
     */
    private void checkBody(String body) throws JSONException {
        if (Strings.isEmpty(body)) {
            throw new JSONException("Missing body");
        }
    }

    /**
     * Converts the given body to a {@link RequestData} object
     *
     * @param data The JSON data sent by the client
     * @return The {@link RequestData}
     * @throws JSONException In case data is no valid JSON representation for request data
     * @throws OXException In case the body is invalid
     */
    private RequestData convert2RequestData(String data) throws JSONException, OXException {
        JSONObject jData = JSONServices.parseObject(data);

        Headers headers = parseHeadersFrom(jData);
        return RequestData.builder()
            .withHeaders(headers)
            .withMethod(jData.getString("method"))
            .withUrl(jData.getString("url"))
            .withBody(parseBodyDataFrom(jData))
            .withClientIp(getClientIP(jData.getString("remoteIP"), headers))
            .build();
    }

    private static BodyData parseBodyDataFrom(JSONObject jData) {
        String sBody = jData.optString("body", null);
        return Strings.isEmpty(sBody) ? null : new ByteArrayBodyData(Base64.getDecoder().decode(sBody));
    }

    private static Headers parseHeadersFrom(JSONObject jData) throws JSONException {
        JSONArray jHeaders = jData.optJSONArray("headers");
        if (jHeaders == null) {
            throw new JSONException("Missing \"headers\" field");
        }
        Headers.Builder builder = Headers.builderWithExpectedSize(jHeaders.length());
        for (Object o : jHeaders) {
            JSONObject jHeader = (JSONObject) o;
            builder.addHeader(jHeader.getString("name"), jHeader.getString("value"));
        }
        return builder.build();
    }

    /**
     * Gets the client IP address.
     *
     * @param remoteIP The remote IP address
     * @param headers the request headers
     * @return The IP address
     * @throws OXException If the IP service is missing
     */
    private String getClientIP(String remoteIP, Headers headers) throws OXException {
        return ipUtilService.get().getIP(remoteIP, headers);
    }

    /**
     * Converts a {@link AnalyzeResult} to JSON text
     *
     * @param analyzeResult The {@link AnalyzeResult} to convert
     * @return The JSON text
     * @throws JSONException If conversion to JSON fails
     */
    private static String convert2jsonText(AnalyzeResult analyzeResult) throws JSONException {
        try {
            Writer writer;

            UserInfo optUserInfo = analyzeResult.optUserInfo().orElse(null);
            if (optUserInfo == null) {
                writer = new StringBuilderWriter(64); // NOSONARLINT
                writer.append('{');

                SimpleJSONSerializer.writeJsonValue("type", writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(analyzeResult.getType().name(), writer);

                Optional<SegmentMarker> optMarker = analyzeResult.optMarker();
                if (optMarker.isPresent()) {
                    writer.append(',');
                    SimpleJSONSerializer.writeJsonValue("marker", writer);
                    writer.append(':');
                    SimpleJSONSerializer.writeJsonValue(optMarker.get().encode(), writer);
                }
            } else {
                writer = new StringBuilderWriter(128); // NOSONARLINT
                writer.append('{');

                SimpleJSONSerializer.writeJsonValue("type", writer);
                writer.append(':');
                SimpleJSONSerializer.writeJsonValue(analyzeResult.getType().name(), writer);

                writer.append(',');
                SimpleJSONSerializer.writeJsonValue("headers", writer);
                writer.append(':');
                userInfo2JsonText(optUserInfo, writer);

                Optional<SegmentMarker> optMarker = analyzeResult.optMarker();
                if (optMarker.isPresent()) {
                    writer.append(',');
                    SimpleJSONSerializer.writeJsonValue("marker", writer);
                    writer.append(':');
                    SimpleJSONSerializer.writeJsonValue(optMarker.get().encode(), writer);
                }
            }
            writer.append('}');

            return writer.toString();
        } catch (IOException e) {
            // Should not occur for StringBuilderWriter
            throw new JSONException("I/O error occurred", e);
        }
    }

    /**
     * Converts user info to JSON text.
     *
     * @param userInfo The user info to convert
     * @param writer The writer to write to
     * @throws IOException If conversion to JSON fails
     */
    private static void userInfo2JsonText(UserInfo userInfo, Writer writer) throws IOException {
        writer.append('{');

        SimpleJSONSerializer.writeJsonValue("x-ox-context-id", writer);
        writer.append(':');
        SimpleJSONSerializer.writeJsonValue(userInfo.getContextId(), writer);

        Optional<Integer> optUserId = userInfo.getUserId();
        if (optUserId.isPresent()) {
            writer.append(',');
            SimpleJSONSerializer.writeJsonValue("x-ox-user-id", writer);
            writer.append(':');
            SimpleJSONSerializer.writeJsonValue(optUserId.get().intValue(), writer);
        }

        Optional<String> optLogin = userInfo.getLogin();
        if (optLogin.isPresent()) {
            writer.append(',');
            SimpleJSONSerializer.writeJsonValue("x-ox-login", writer);
            writer.append(':');
            SimpleJSONSerializer.writeJsonValue(optLogin.get(), writer);
        }
        writer.append('}');
    }

    /**
     * Generates an internal server error response.
     *
     * @param e The error causing this response
     * @return The response
     */
    private static Response generateInternalServerError(OXException e) {
        try {
            JSONObject entity = new JSONObject();
            ResponseWriter.addException(entity, e);
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(entity.toString()).build();
        } catch (JSONException ex) {
            LOG.error("Error while generating error for client.", ex);
            return INTERNAL_SERVER_ERROR_RESPONSE;
        }
    }

}
