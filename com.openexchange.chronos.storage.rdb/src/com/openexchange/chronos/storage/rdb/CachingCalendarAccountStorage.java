/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.storage.rdb;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.chronos.provider.CalendarAccount;
import com.openexchange.chronos.provider.DefaultCalendarAccount;
import com.openexchange.chronos.storage.CalendarAccountStorage;
import com.openexchange.database.provider.DBTransactionPolicy;
import com.openexchange.exception.OXException;
import com.openexchange.java.Predicates;

/**
 * {@link CachingCalendarAccountStorage}
 *
 * @author <a href="mailto:Jan-Oliver.Huhn@open-xchange.com">Jan-Oliver Huhn</a>
 * @since v7.10
 */
public class CachingCalendarAccountStorage implements CalendarAccountStorage {

    /** Cache options for calendar accounts */
    private static final CacheOptions<Map<String, List<CalendarAccountData>>> OPTIONS_ACCOUNTS = CacheOptions.<Map<String, List<CalendarAccountData>>> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.CALENDAR_ACCOUNT)
        .withCodecAndVersion(new CalendarAccountCodec())
    .build(); // formatter:on

    private final RdbCalendarAccountStorage delegate;
    private final int contextId;
    private final Cache<Map<String, List<CalendarAccountData>>> accountsCache;

    /**
     * Initializes a new {@link CachingCalendarAccountStorage}.
     *
     * @param contextId The context identifier
     * @param delegate The underlying persistent account storage
     * @param cacheSerivce A reference to the cache service
     */
    public CachingCalendarAccountStorage(RdbCalendarAccountStorage delegate, int contextId, CacheService cacheService) {
        super();
        this.delegate = delegate;
        this.contextId = contextId;
        accountsCache = cacheService.getCache(OPTIONS_ACCOUNTS);
    }

    @Override
    public int nextId() throws OXException {
        return delegate.nextId();
    }

    @Override
    public void insertAccount(CalendarAccount account) throws OXException {
        delegate.insertAccount(account);
        invalidateAccounts(account.getUserId());
    }

    @Override
    public void insertAccount(CalendarAccount account, int maxAccounts) throws OXException {
        delegate.insertAccount(account, maxAccounts);
        invalidateAccounts(account.getUserId());
    }

    @Override
    public void updateAccount(CalendarAccount account, long clientTimestamp) throws OXException {
        delegate.updateAccount(account, clientTimestamp);
        invalidateAccounts(account.getUserId());
    }

    @Override
    public void deleteAccount(int userId, int accountId, long clientTimestamp) throws OXException {
        delegate.deleteAccount(userId, accountId, clientTimestamp);
        invalidateAccounts(userId);
    }

    @Override
    public CalendarAccount loadAccount(int userId, int accountId) throws OXException {
        if (bypassCache()) {
            return delegate.loadAccount(userId, accountId);
        }
        return lookupAccount(userId, accountId, getAccountDataPerProviderId(userId));
    }

    @Override
    public CalendarAccount[] loadAccounts(int userId, int[] accountIds) throws OXException {
        if (null == accountIds || bypassCache()) {
            return delegate.loadAccounts(userId, accountIds);
        }
        Map<String, List<CalendarAccountData>> accountDataPerProviderId = getAccountDataPerProviderId(userId);
        CalendarAccount[] accounts = new CalendarAccount[accountIds.length];
        for (int i = 0; i < accountIds.length; i++) {
            accounts[i] = lookupAccount(userId, accountIds[i], accountDataPerProviderId);
        }
        return accounts;
    }

    @Override
    public List<CalendarAccount> loadAccounts(int userId) throws OXException {
        if (bypassCache()) {
            return delegate.loadAccounts(userId);
        }
        return loadAccounts(userId, (String[]) null);
    }

    @Override
    public List<CalendarAccount> loadAccounts(int[] userIds, String providerId) throws OXException {
        if (null == userIds || 0 == userIds.length) {
            return Collections.emptyList();
        }
        if (bypassCache()) {
            return delegate.loadAccounts(userIds, providerId);
        }
        List<CalendarAccount> matchingAccounts = new ArrayList<CalendarAccount>();
        for (int userId : userIds) {
            List<CalendarAccountData> accounts = getAccountDataPerProviderId(userId).get(providerId);
            if (null != accounts) {
                matchingAccounts.addAll(toAccounts(userId, providerId, accounts));
            }
        }
        return matchingAccounts;
    }

    @Override
    public CalendarAccount loadAccount(int userId, String providerId) throws OXException {
        if (bypassCache()) {
            return delegate.loadAccount(userId, providerId);
        }
        List<CalendarAccountData> accounts = getAccountDataPerProviderId(userId).get(providerId);
        return null == accounts || accounts.isEmpty() ? null : toAccount(userId, providerId, accounts.get(0));
    }

    @Override
    public List<CalendarAccount> loadAccounts(int userId, String... providerIds) throws OXException {
        if (bypassCache()) {
            return delegate.loadAccounts(userId, providerIds);
        }
        Map<String, List<CalendarAccountData>> accountsPerProviderId = getAccountDataPerProviderId(userId);
        if (null == providerIds) {
            return accountsPerProviderId.entrySet().stream().map(e -> toAccounts(userId, e.getKey(), e.getValue())).flatMap(List::stream).collect(Collectors.toList());
        }
        return Stream.of(providerIds).map(p -> toAccounts(userId, p, accountsPerProviderId.get(p))).filter(Predicates.isNotNullPredicate()).flatMap(List::stream).collect(Collectors.toList());
    }

    @Override
    public List<CalendarAccount> loadAccounts(String... providerIds) throws OXException {
        return delegate.loadAccounts(providerIds);
    }

    @Override
    public void invalidateAccount(int userId, int accountId) throws OXException {
        invalidateAccounts(userId);
    }

    private void invalidateAccounts(int userId) throws OXException {
        accountsCache.invalidate(getAccountsKey(userId));
    }

    private CacheKey getAccountsKey(int userId) {
        return accountsCache.newKey(asHashPart(contextId), Integer.toString(userId));
    }

    private boolean bypassCache() {
        return DBTransactionPolicy.NO_TRANSACTIONS.equals(delegate.getTransactionPolicy());
    }

    /**
     * Gets cached or loads all calendar account data of a certain user from the storage.
     *
     * @param userId The identifier of the user to get the account data for
     * @return The calendar account data, mapped by provider identifiers
     * @throws OXException If loading data fails
     */
    private Map<String, List<CalendarAccountData>> getAccountDataPerProviderId(int userId) throws OXException {
        return accountsCache.get(getAccountsKey(userId), k -> loadAccountDataPerProviderId(userId));
    }

    /**
     * Loads all calendar account data of a certain user from the storage.
     *
     * @param userId The identifier of the user to load the account data for
     * @return The loaded calendar account data, mapped by provider identifiers
     * @throws OXException If loading data fails
     */
    private Map<String, List<CalendarAccountData>> loadAccountDataPerProviderId(int userId) throws OXException {
        Map<String, List<CalendarAccountData>> accountsPerProviderId = new HashMap<String, List<CalendarAccountData>>();
        for (CalendarAccount account : delegate.loadAccounts(userId)) {
            com.openexchange.tools.arrays.Collections.put(accountsPerProviderId, account.getProviderId(), toAccountData(account));
        }
        return accountsPerProviderId;
    }

    /**
     * Initializes a new calendar account data based on the given calendar account.
     *
     * @param account The account to convert
     * @return The calendar account data
     */
    private static CalendarAccountData toAccountData(CalendarAccount account) {
        long lastModified = null == account.getLastModified() ? -1L : account.getLastModified().getTime();
        return new CalendarAccountData(account.getAccountId(), lastModified, account.getInternalConfiguration(), account.getUserConfiguration());
    }

    /**
     * Converts calendar account data to a calendar account object of a specific provider.
     *
     * @param userId The identifier of the user to apply
     * @param providerId The identifier of the calendar provider to set in the account
     * @param accountData The account data to convert
     * @return The converted calendar account
     */
    private static CalendarAccount toAccount(int userId, String providerId, CalendarAccountData accountData) {
        Date lastModified = 0 < accountData.lastModified() ? new Date(accountData.lastModified()) : null;
        return new DefaultCalendarAccount(providerId, accountData.id(), userId, accountData.internalConfig(), accountData.userConfig(), lastModified);
    }

    /**
     * Converts a list of calendar account data to calendar account objects of a specific provider.
     *
     * @param userId The identifier of the user to apply
     * @param providerId The identifier of the calendar provider to set in the accounts
     * @param accountDataList The account data list to convert
     * @return The converted calendar accounts
     */
    private static List<CalendarAccount> toAccounts(int userId, String providerId, List<CalendarAccountData> accountDataList) {
        if (null == accountDataList) {
            return null;
        }
        if (accountDataList.isEmpty()) {
            return Collections.emptyList();
        }
        return accountDataList.stream().map(d -> toAccount(userId, providerId, d)).toList();
    }

    /**
     * Lookups a specific account by its identifier from the supplied map of accounts per provider.
     *
     * @param userId The identifier of the user
     * @param accountId The identifier of the account to lookup
     * @param accountDataPerProviderId The account data to lookup the account in
     * @return The calendar account, or <code>null</code> if not found
     */
    private static CalendarAccount lookupAccount(int userId, int accountId, Map<String, List<CalendarAccountData>> accountDataPerProviderId) {
        if (null == accountDataPerProviderId || accountDataPerProviderId.isEmpty()) {
            return null;
        }
        for (Entry<String, List<CalendarAccountData>> entry : accountDataPerProviderId.entrySet()) {
            for (CalendarAccountData accountData : entry.getValue()) {
                if (accountId == accountData.id()) {
                    return toAccount(userId, entry.getKey(), accountData);
                }
            }
        }
        return null;
    }

}
