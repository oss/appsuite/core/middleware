/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.storage.rdb;

import java.util.UUID;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.AbstractMultiMapCodec;

/**
 * {@link CalendarAccountCodec} - Cache codec for calendar accounts.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class CalendarAccountCodec extends AbstractMultiMapCodec<CalendarAccountData> {

    private static final UUID CODEC_ID = UUID.fromString("cf0ba193-3b51-4226-00fc-c0e11e000001");

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONObject serializeValue(CalendarAccountData value) throws Exception {
        JSONObject jsonObject = new JSONObject(4);
        jsonObject.put("i", value.id());
        if (0 < value.lastModified()) {
            jsonObject.put("l", value.lastModified());
        }
        jsonObject.putOpt("c", value.internalConfig());
        jsonObject.putOpt("u", value.userConfig());
        return jsonObject;
    }

    @Override
    protected CalendarAccountData deserializeValue(JSONObject jsonObject) throws Exception {
        return new CalendarAccountData(jsonObject.getInt("i"), jsonObject.optLong("l", -1L), jsonObject.optJSONObject("c"), jsonObject.optJSONObject("u"));
    }

}
