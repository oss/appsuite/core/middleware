/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.health;

import com.openexchange.config.lean.Property;

/**
 * {@link MWHealthCheckProperty}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 * @since v7.10.1
 */
public enum MWHealthCheckProperty implements Property {

    /**
     * Username used to access the health-check REST interface. If com.openexchange.health.username
     * and com.openexchange.health.password are empty, no authentication is necessary. This is the default.
     */
    USERNAME("username", ""),
    /**
     * Password used to access the health-check REST interface. If com.openexchange.health.username
     * and com.openexchange.health.password are empty, no authentication is necessary. This is the default.
     */
    PASSWORD("password", ""),
    /**
     * Comma-separated list of health-checks which should not be executed.
     */
    SKIP("skip", ""),
    /**
     * Comma-separated list of health-checks which should be executed, but their status is not considered when
     * evaluating the overall status.
     */
    IGNORE("ignore", ""),
    /**
     * Whether the integrated 'noServicesMissing' check is enabled or not. Prior enabling the check, ensure that all feature/package
     * dependencies are met and verify that `getmissingservices` output is empty. Defaults to `false`.
     */
    NO_SERVICES_MISSING_ENABLED("noServicesMissing.enabled", Boolean.FALSE),
    ;

    private static final String PREFIX = "com.openexchange.health.";

    private final String nameSuffix;
    private final Object defaultValue;

    private MWHealthCheckProperty(String nameSuffix, Object value) {
        this.nameSuffix = nameSuffix;
        this.defaultValue = value;
    }

    @Override
    public String getFQPropertyName() {
        return PREFIX + nameSuffix;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
