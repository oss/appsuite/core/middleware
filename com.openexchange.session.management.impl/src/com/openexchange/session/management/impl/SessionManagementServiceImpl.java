/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.session.management.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import org.slf4j.Logger;
import com.google.common.collect.ImmutableSet;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.LogLevel;
import com.openexchange.exception.OXException;
import com.openexchange.geolocation.GeoInformation;
import com.openexchange.geolocation.GeoLocationService;
import com.openexchange.i18n.tools.StringHelper;
import com.openexchange.java.InetAddresses;
import com.openexchange.java.Strings;
import com.openexchange.log.LogUtility;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.RemovalReason;
import com.openexchange.session.Session;
import com.openexchange.session.management.ManagedSession;
import com.openexchange.session.management.SessionManagementExceptionCodes;
import com.openexchange.session.management.SessionManagementService;
import com.openexchange.session.management.SessionManagementStrings;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.tools.session.ServerSessionAdapter;
import com.openexchange.user.UserService;

/**
 * {@link SessionManagementServiceImpl}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 * @since v7.10.0
 */
public class SessionManagementServiceImpl implements SessionManagementService {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(SessionManagementServiceImpl.class);

    private final AtomicReference<Set<String>> blacklistedClients;
    private final ServiceLookup services;

    /**
     * Initializes a new {@link SessionManagementServiceImpl}.
     *
     * @param services The service look-up
     */
    public SessionManagementServiceImpl(ServiceLookup services) {
        super();
        this.services = services;
        blacklistedClients = new AtomicReference<Set<String>>(doGetBlacklistedClients());
    }

    /**
     * Reinitializes black-list for certain clients.
     */
    public void reinitBlacklistedClients() {
        blacklistedClients.set(doGetBlacklistedClients());
    }

    @Override
    public Collection<ManagedSession> getSessionsForUser(Session session) throws OXException {
        if (null == session) {
            return Collections.emptyList();
        }

        String location = getDefaultLocation(session);
        if (ServerSessionAdapter.valueOf(session).getUser().isAnonymousGuest()) {
            return Collections.singleton(DefaultManagedSession.builder(session).setLocation(optLocationFor(session, location, null)).build());
        }

        SessiondService sessiondService = services.getService(SessiondService.class);
        if (null == sessiondService) {
            throw ServiceExceptionCode.absentService(SessiondService.class);
        }

        Collection<Session> sessions = sessiondService.getSessions(session.getUserId(), session.getContextId(), true);

        Set<String> blackListedClients = getBlacklistedClients();

        int totalSize = sessions.size();
        Map<String, String> ip2locationCache = HashMap.newHashMap(totalSize);
        List<ManagedSession> result = new ArrayList<>(totalSize);
        Set<String> filter = HashSet.newHashSet(totalSize);
        for (Session s : sessions) {
            if (filter.add(s.getSessionID()) && !blackListedClients.contains(s.getClient())) {
                result.add(DefaultManagedSession.builder(s).setLocation(optLocationFor(s, location, ip2locationCache)).build());
            }
        }
        return result;
    }

    @Override
    public void removeSession(Session session, String sessionIdToRemove) throws OXException {
        if (ServerSessionAdapter.valueOf(session).getUser().isAnonymousGuest() && session.getSessionID().equals(sessionIdToRemove) == false) {
            // Only allow to remove the own session for anonymous guests
            throw SessionManagementExceptionCodes.SESSION_NOT_FOUND.create();
        }
        services.getServiceSafe(SessiondService.class).removeSession(sessionIdToRemove, RemovalReason.USER_CLOSED);
    }

    private Set<String> getBlacklistedClients() {
        return blacklistedClients.get();
    }

    private Set<String> doGetBlacklistedClients() {
        LeanConfigurationService configService = services.getService(LeanConfigurationService.class);
        if (null == configService) {
            return Collections.emptySet();
        }

        String value = configService.getProperty(SessionManagementProperty.CLIENT_BLACKLIST);
        if (Strings.isEmpty(value)) {
            return Collections.emptySet();
        }

        String[] clients = Strings.splitByComma(value);
        if (null == clients || clients.length == 0) {
            return Collections.emptySet();
        }

        return ImmutableSet.copyOf(clients);
    }

    private String getDefaultLocation(Session session) {
        // Initialize default value for location
        String location;
        {
            location = SessionManagementStrings.UNKNOWN_LOCATION;
            UserService userService = services.getService(UserService.class);
            if (null != userService) {
                try {
                    location = StringHelper.valueOf(userService.getUser(session.getUserId(), session.getContextId()).getLocale()).getString(SessionManagementStrings.UNKNOWN_LOCATION);
                } catch (OXException e) {
                    LOG.warn("", e);
                }
            }
        }
        return location;
    }

    /**
     * Attempts to determine the location for given session's IP address.
     *
     * @param s The session providing the IP address to determine the location for
     * @param def The default location to return
     * @param optIp2locationCache The optional cache for already determined locations
     * @return The location for given session's IP address or specified default location
     */
    private String optLocationFor(Session s, String def, Map<String, String> optIp2locationCache) {
        String ipAddress = s.getLocalIp();

        // Check "cache" first
        String location = optIp2locationCache == null ? null : optIp2locationCache.get(ipAddress);
        if (null != location) {
            return location;
        }

        try {
            InetAddress address = InetAddresses.forString(ipAddress);
            if (InetAddresses.isInternalAddress(address)) {
                UserService userService = services.getService(UserService.class);
                if (null != userService) {
                    try {
                        return StringHelper.valueOf(userService.getUser(s.getUserId(), s.getContextId()).getLocale()).getString(SessionManagementStrings.INTRANET_LOCATION);
                    } catch (OXException e) {
                        LOG.warn("", e);
                    }
                }
                return SessionManagementStrings.INTRANET_LOCATION;
            }
        } catch (UnknownHostException e) {
            // ignore and try geolocation service
            LOG.debug("Invalid address was specified: {}", ipAddress, e);
        }

        GeoLocationService geoLocationService = services.getOptionalService(GeoLocationService.class);
        if (geoLocationService != null) {
            try {
                GeoInformation geoInformation = geoLocationService.getGeoInformation(s.getContextId(), InetAddresses.forString(ipAddress));
                if (null == geoInformation) {
                    return getDefaultLocation(s);
                }
                StringBuilder sb = null;
                if (geoInformation.hasCity()) {
                    sb = new StringBuilder(geoInformation.getCity());
                }
                if (geoInformation.hasCountry()) {
                    if (null == sb) {
                        sb = new StringBuilder();
                    } else {
                        sb.append(", ");
                    }
                    sb.append(geoInformation.getCountry());
                }
                if (null != sb) {
                    location = sb.toString();
                    if (optIp2locationCache != null) {
                        optIp2locationCache.put(ipAddress, location);
                    }
                    return location;
                }
            } catch (UnknownHostException e) {
                LOG.debug("Invalid address was specified: {}", ipAddress, e);
            } catch (Exception e) {
                LogUtility.logOncePerDay(ipAddress, LOG, LogLevel.WARNING, LogLevel.DEBUG, "Failed to determine location for session with IP address {}", ipAddress, e);
            }
        }

        if (optIp2locationCache != null) {
            optIp2locationCache.put(ipAddress, def);
        }
        return def;
    }
}
