/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tokenlogin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import com.openexchange.java.Strings;

/**
 * {@link DefaultTokenLoginApplication} - The default implementation for {@link TokenLoginApplication}.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class DefaultTokenLoginApplication implements TokenLoginApplication {

    private final String appId;
    private final Map<String, Object> parameters;
    private final boolean hasPasswordAccess;
    private final boolean shallCopyParameters;
    private final boolean shallAnnounceAppId;

    /**
     * Initializes a new {@link DefaultTokenLoginApplication}.
     * 
     * @param appId The application ID
     * @param hasPasswordAccess <code>true</code> to add password to the response
     * @param shallCopyParameters <code>true</code> to copy all session parameters
     * @param shallAnnounceAppId <code>true</code> to announce application ID to clients
     */
    public DefaultTokenLoginApplication(String appId, boolean hasPasswordAccess, boolean shallCopyParameters, boolean shallAnnounceAppId) {
        this(appId, hasPasswordAccess, shallCopyParameters, shallAnnounceAppId, Map.of());
    }

    /**
     * Initializes a new {@link DefaultTokenLoginApplication}.
     * 
     * @param appId The application ID
     * @param hasPasswordAccess <code>true</code> to add password to the response
     * @param shallCopyParameters <code>true</code> to copy all session parameters
     * @param shallAnnounceAppId <code>true</code> to announce application ID to clients
     */
    public DefaultTokenLoginApplication(String appId, boolean hasPasswordAccess, boolean shallCopyParameters, boolean shallAnnounceAppId, String parameters) {
        this(appId, hasPasswordAccess, shallCopyParameters, shallAnnounceAppId, splitParameters(parameters));
    }

    private static Map<String, Object> splitParameters(String parameters) {
        if (Strings.isEmpty(parameters)) {
            return Map.of();
        }
        String[] keyValues = Strings.splitBySemiColon(parameters);
        if (null == keyValues || 0 == keyValues.length) {
            return Map.of();
        }
        Map<String, Object> result = HashMap.newHashMap(keyValues.length);
        for (String pair : keyValues) {
            ArrayList<String> splitBy = Strings.splitBy(pair, '=', true, new ArrayList<String>(2));
            if (2 == splitBy.size()) {
                result.put(splitBy.get(0), splitBy.get(1));
            }
        }
        return result;
    }

    /**
     * Initializes a new {@link DefaultTokenLoginApplication}.
     * 
     * @param appId The application ID
     * @param hasPasswordAccess <code>true</code> to add password to the response
     * @param shallCopyParameters <code>true</code> to copy all session parameters
     * @param shallAnnounceAppId <code>true</code> to announce application ID to clients
     * @param parameters Additional parameters
     */
    public DefaultTokenLoginApplication(String appId, boolean hasPasswordAccess, boolean shallCopyParameters, boolean shallAnnounceAppId, Map<String, Object> parameters) {
        super();
        this.appId = appId;
        this.hasPasswordAccess = hasPasswordAccess;
        this.shallCopyParameters = shallCopyParameters;
        this.shallAnnounceAppId = shallAnnounceAppId;
        this.parameters = parameters;
    }

    @Override
    public String getAppId() {
        return appId;
    }

    @Override
    public boolean hasPasswordAccess() {
        return hasPasswordAccess;
    }

    @Override
    public boolean shallAnnounceAppId() {
        return shallAnnounceAppId;
    }

    @Override
    public boolean shallCopyParameters() {
        return shallCopyParameters;
    }

    @Override
    public Map<String, Object> getParameters() {
        return null == parameters ? Collections.<String, Object> emptyMap() : parameters;
    }

    @Override
    public String toString() {
        return "DefaultTokenLoginApplication [appId=" + appId + ", parameters=" + parameters + ", hasPasswordAccess=" + hasPasswordAccess + ", shallCopyParameters=" + shallCopyParameters + ", shallAnnounceAppId=" + shallAnnounceAppId + "]";
    }

}
