/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tokenlogin;

import com.openexchange.exception.OXException;
import com.openexchange.session.Session;

/**
 * {@link TokenLoginService} - The token-login service.
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public interface TokenLoginService {

    /**
     * Acquires a unique token for specified session.
     * 
     * @param session The associated session
     *
     * @return The token as a string
     * @throws OXException If token cannot be generated for any reason
     */
    default String acquireToken(Session session) throws OXException {
        return acquireToken(session, -1);
    }

    /**
     * Acquires a unique token for specified session.
     * 
     * @param session The associated session
     * @param expiry The expiry of the token to acquire in milliseconds, or <code>-1</code> if undefined
     * @return The token as a string
     * @throws OXException If token cannot be generated for any reason
     */
    String acquireToken(Session session, long expiry) throws OXException;

    /**
     * Redeems given token and generates an appropriate session.
     *
     * @param token The token previously generated
     * @param appId The identifier associated with requesting Web service/application
     * @param optClientIdentifier The optional client identifier
     * @param optAuthId The optional authentication identifier
     * @param optHash The optional hash value that applies to newly generated session
     * @param optClientIp The optional client IP address that applies to newly generated session
     * @param optUserAgent The optional user agent that applies to newly generated session
     * @return The generated session
     * @throws OXException If token cannot be turned into a valid session
     */
    default Session redeemToken(String token, String appId, String optClientIdentifier, String optAuthId, String optHash, String optClientIp, String optUserAgent) throws OXException {
        return redeemToken(token, appId, optClientIdentifier, optAuthId, optHash, optClientIp, optUserAgent, false);
    }

    /**
     * Redeems given token and generates an appropriate session.
     *
     * @param token The token previously generated
     * @param appId The identifier associated with requesting Web service/application
     * @param optClientIdentifier The optional client identifier
     * @param optAuthId The optional authentication identifier
     * @param optHash The optional hash value that applies to newly generated session
     * @param optClientIp The optional client IP address that applies to newly generated session
     * @param optUserAgent The optional user agent that applies to newly generated session
     * @param staySignedIn If set to <code>true</code> cookies will be persisted, default is <code>false</code>
     * @return The generated session
     * @throws OXException If token cannot be turned into a valid session
     */
    Session redeemToken(String token, String appId, String optClientIdentifier, String optAuthId, String optHash, String optClientIp, String optUserAgent, boolean staySignedIn) throws OXException;

    /**
     * Gets the token-login application (and its parameters) for specified identifier.
     * 
     * @param contextId The context of the user
     * @param userId The user identifier
     * @param appId The application identifier
     * @return The associated token-login application or <code>null</code> if there is none associated with given secret identifier
     * @throws OXException In case configuration service is missing
     */
    TokenLoginApplication getTokenLoginApplication(int contextId, int userId, String appId) throws OXException;

}
