/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.tokenlogin;

import java.util.Map;

/**
 * {@link TokenLoginApplication} - A token-login application.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public interface TokenLoginApplication {

    /**
     * Gets the application's identifier.
     *
     * @return The identifier
     */
    String getAppId();

    /**
     * Get a value indicating whether or not the application grants access to the users password
     *
     * @return <code>true</code> if password can be used, <code>false</code> otherwise
     */
    boolean hasPasswordAccess();

    /**
     * Get a value indicating whether or not the application shall copy session parameters when
     * cloning a session
     *
     * @return <code>true</code> if session parameters shall be cloned, <code>false</code> otherwise
     * 
     */
    boolean shallCopyParameters();

    /**
     * Get a value indicating whether or not the application identifier shall be announced to clients or not
     *
     * @return <code>true</code> if the application ID shall be announced, <code>false</code> otherwise
     */
    boolean shallAnnounceAppId();

    /**
     * Get additional the parameters associated with this application.
     *
     * @return The additional parameters
     */
    Map<String, Object> getParameters();

}
