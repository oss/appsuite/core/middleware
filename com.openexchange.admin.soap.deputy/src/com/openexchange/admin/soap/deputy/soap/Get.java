/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.admin.soap.deputy.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.openexchange.admin.soap.deputy.dataobjects.Context;
import com.openexchange.admin.soap.deputy.dataobjects.User;
import com.openexchange.admin.soap.deputy.dataobjects.Credentials;

/**
 * <p>Java-Klasse f\u00fcr anonymous complex type.
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "deputyId",
    "context",
    "user",
    "auth"
})
@XmlRootElement(name = "get")
public class Get {

    @XmlElement(nillable = true)
    protected String deputyId;
    @XmlElement(nillable = true)
    protected Context context;
    @XmlElement(nillable = true)
    protected User user;
    @XmlElement(nillable = true)
    protected Credentials auth;

    /**
     * Gets the deputy identifier
     *
     * @return The deputy identifier
     */
    public String getDeputyId() {
        return deputyId;
    }

    /**
     * Sets the deputy identifier
     *
     * @param deputyId The deputy identifier to set
     */
    public void setDeputyId(String deputyId) {
        this.deputyId = deputyId;
    }

    /**
     * Gets the context
     *
     * @return The context
     */
    public Context getContext() {
        return context;
    }

    /**
     * Sets the context
     *
     * @param context The context to set
     */
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * Gets the user
     *
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets the user
     *
     * @param user The user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Gets the auth
     *
     * @return The auth
     */
    public Credentials getAuth() {
        return auth;
    }

    /**
     * Sets the auth
     *
     * @param auth The auth to set
     */
    public void setAuth(Credentials auth) {
        this.auth = auth;
    }
}
