
package com.openexchange.admin.soap.deputy.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.openexchange.admin.soap.deputy.dataobjects.ActiveDeputyPermission;


/**
 * <p>Java-Klasse f\u00fcr anonymous complex type.
 *
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://dataobjects.soap.admin.openexchange.com/xsd}User" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "getResponse")
public class GetResponse {

    @XmlElement(name = "return", nillable = true)
    protected ActiveDeputyPermission _return;

    /**
     * Gets the value of the return property.
     *
     * @return The return property
     */
    public ActiveDeputyPermission getReturn() {
        return this._return;
    }


    /**
     * Sets the return value
     *
     * @param value The value to set
     */
    public void setReturn(ActiveDeputyPermission value) {
        this._return = value;
    }

}
