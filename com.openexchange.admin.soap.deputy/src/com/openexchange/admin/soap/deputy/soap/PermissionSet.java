/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.soap.deputy.soap;

import java.util.Optional;
import com.openexchange.java.Strings;

/**
 * {@link PermissionSet} - Am enumeration of known permission sets.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public enum PermissionSet {

    /**
     * The permission set for viewer.
     */
    VIEWER("viewer", false, 2, 4, 0, 0),
    /**
     * The permission set for editor.
     */
    EDITOR("editor", false, 2, 4, 4, 0),
    /**
     * The permission set for author.
     */
    AUTHOR("author", false, 8, 4, 4, 4),
    ;

    private final String identifier;
    private final boolean admin;
    private final int folderPermission;
    private final int readPermission;
    private final int writePermission;
    private final int deletePermission;

    private PermissionSet(String identifier, boolean admin, int folderPermission, int readPermission, int writePermission, int deletePermission) {
        this.identifier = identifier;
        this.admin = admin;
        this.folderPermission = folderPermission;
        this.readPermission = readPermission;
        this.writePermission = writePermission;
        this.deletePermission = deletePermission;
    }

    /**
     * Gets the identifier.
     *
     * @return The identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Gets the administrator flag.
     *
     * @return The administrator flag
     */
    public boolean isAdmin() {
        return admin;
    }

    /**
     * Gets the folder permission.
     *
     * @return The folder permission
     */
    public int getFolderPermission() {
        return folderPermission;
    }

    /**
     * Gets the read permission.
     *
     * @return The read permission
     */
    public int getReadPermission() {
        return readPermission;
    }

    /**
     * Gets the write permission.
     *
     * @return The write permission
     */
    public int getWritePermission() {
        return writePermission;
    }

    /**
     * Gets the delete permission.
     *
     * @return The delete permission
     */
    public int getDeletePermission() {
        return deletePermission;
    }

    /**
     * Gets the permission set for specified identifier.
     *
     * @param identifier The identifier to look-up
     * @return The permission set or empty
     */
    public static Optional<PermissionSet> permissionSetFor(String identifier) {
        if (identifier == null) {
            return Optional.empty();
        }

        String id = Strings.asciiLowerCase(identifier);
        for (PermissionSet permissionSet : values()) {
            if (id.equals(permissionSet.getIdentifier())) {
                return Optional.of(permissionSet);
            }
        }
        return Optional.empty();
    }

}
