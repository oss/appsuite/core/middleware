/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.admin.soap.deputy.dataobjects;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java-Klasse f\u00fcr ModulePermission complex type.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModulePermission", propOrder = {
    "moduleId",
    "folderIds",
    "admin",
    "folderPermission",
    "readPermission",
    "writePermission",
    "deletePermission",
})
public class ModulePermission {

    @XmlElement(nillable = true)
    private String moduleId;
    @XmlElement(nillable = true)
    private List<String> folderIds;
    @XmlElement(nillable = true)
    private Boolean admin;
    @XmlElement(nillable = true)
    private Integer folderPermission;
    @XmlElement(nillable = true)
    private Integer readPermission;
    @XmlElement(nillable = true)
    private Integer writePermission;
    @XmlElement(nillable = true)
    private Integer deletePermission;

    /**
     * Gets the moduleId
     *
     * @return The moduleId
     */
    public String getModuleId() {
        return moduleId;
    }

    /**
     * Sets the moduleId
     *
     * @param moduleId The moduleId to set
     */
    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    /**
     * Gets the folderIds
     *
     * @return The folderIds
     */
    public List<String> getFolderIds() {
        return folderIds;
    }

    /**
     * Sets the folderIds
     *
     * @param folderIds The folderIds to set
     */
    public void setFolderIds(List<String> folderIds) {
        this.folderIds = folderIds;
    }

    /**
     * Gets the admin
     *
     * @return The admin
     */
    public Boolean getAdmin() {
        return admin;
    }

    /**
     * Sets the admin
     *
     * @param admin The admin to set
     */
    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    /**
     * Gets the folderPermission
     *
     * @return The folderPermission
     */
    public Integer getFolderPermission() {
        return folderPermission;
    }

    /**
     * Sets the folderPermission
     *
     * @param folderPermission The folderPermission to set
     */
    public void setFolderPermission(Integer folderPermission) {
        this.folderPermission = folderPermission;
    }

    /**
     * Gets the readPermission
     *
     * @return The readPermission
     */
    public Integer getReadPermission() {
        return readPermission;
    }

    /**
     * Sets the readPermission
     *
     * @param readPermission The readPermission to set
     */
    public void setReadPermission(Integer readPermission) {
        this.readPermission = readPermission;
    }

    /**
     * Gets the writePermission
     *
     * @return The writePermission
     */
    public Integer getWritePermission() {
        return writePermission;
    }

    /**
     * Sets the writePermission
     *
     * @param writePermission The writePermission to set
     */
    public void setWritePermission(Integer writePermission) {
        this.writePermission = writePermission;
    }

    /**
     * Gets the deletePermission
     *
     * @return The deletePermission
     */
    public Integer getDeletePermission() {
        return deletePermission;
    }

    /**
     * Sets the deletePermission
     *
     * @param deletePermission The deletePermission to set
     */
    public void setDeletePermission(Integer deletePermission) {
        this.deletePermission = deletePermission;
    }

}
