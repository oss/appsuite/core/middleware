/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.admin.soap.deputy.dataobjects;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java-Klasse f\u00fcr DeputyPermission complex type.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActiveDeputyPermission", propOrder = {
    "userId",
    "sendOnBehalfOf",
    "modulePermissions",
    "deputyId",
    "grantorId",
    "contextId",
})
public class ActiveDeputyPermission {

    @XmlElement(nillable = true)
    private Integer userId;
    @XmlElement(nillable = true)
    private Boolean sendOnBehalfOf;
    @XmlElement(nillable = true)
    private List<ModulePermission> modulePermissions;
    @XmlElement(nillable = true)
    private String deputyId;
    @XmlElement(nillable = true)
    private Integer grantorId;
    @XmlElement(nillable = true)
    private Integer contextId;

    /**
     * Gets the user identifier
     *
     * @return The user identifier
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * Sets the user identifier
     *
     * @param userId The user identifier to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * Gets the send-on-behalf-of flag
     *
     * @return The send-on-behalf-of flag
     */
    public Boolean getSendOnBehalfOf() {
        return sendOnBehalfOf;
    }

    /**
     * Sets the send-on-behalf-of flag
     *
     * @param sendOnBehalfOf The send-on-behalf-of flag to set
     */
    public void setSendOnBehalfOf(Boolean sendOnBehalfOf) {
        this.sendOnBehalfOf = sendOnBehalfOf;
    }

    /**
     * Gets the module permissions
     *
     * @return The module permissions
     */
    public List<ModulePermission> getModulePermissions() {
        return modulePermissions;
    }

    /**
     * Sets the module permissions
     *
     * @param modulePermissions The module permissions to set
     */
    public void setModulePermissions(List<ModulePermission> modulePermissions) {
        this.modulePermissions = modulePermissions;
    }

    /**
     * Gets the deputy identifier
     *
     * @return The deputy identifier
     */
    public String getDeputyId() {
        return deputyId;
    }

    /**
     * Sets the deputy identifier
     *
     * @param deputyId The deputy identifier to set
     */
    public void setDeputyId(String deputyId) {
        this.deputyId = deputyId;
    }

    /**
     * Gets the granter identifier
     *
     * @return The granter identifier
     */
    public Integer getGrantorId() {
        return grantorId;
    }

    /**
     * Sets the granter identifier
     *
     * @param grantorId The granter identifier to set
     */
    public void setGrantorId(Integer grantorId) {
        this.grantorId = grantorId;
    }

    /**
     * Gets the context identifier
     *
     * @return The context identifier
     */
    public Integer getContextId() {
        return contextId;
    }

    /**
     * Sets the context identifier
     *
     * @param contextId The context identifier to set
     */
    public void setContextId(Integer contextId) {
        this.contextId = contextId;
    }

}
