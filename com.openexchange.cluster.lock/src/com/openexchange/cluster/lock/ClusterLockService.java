/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cluster.lock;

import java.util.concurrent.TimeUnit;
import com.openexchange.exception.OXException;
import com.openexchange.osgi.annotation.SingletonService;
import com.openexchange.policy.retry.RetryPolicy;
import com.openexchange.policy.retry.RunOnceRetryPolicy;

/**
 * {@link ClusterLockService}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
@SingletonService
public interface ClusterLockService {

    /** Defines the default time to live of 30 seconds for a cluster lock (in milliseconds) */
    public static final long LOCK_TTL_MILLIS = TimeUnit.SECONDS.toMillis(30);

    /**
     * Acquires a cluster lock for the specified {@link ClusterTask} with {@link #LOCK_TTL_MILLIS default time to live}.
     *
     * @param timeoutMillis The lock's time to live in milliseconds
     * @param clusterTask The {@link ClusterTask} for which to acquire the cluster lock
     * @return The unique lock value if the cluster lock was successfully acquired, <code>null</code> otherwise
     * @throws OXException if an error is occurred during the acquisition of the cluster lock
     */
    default <T> String acquireClusterLock(ClusterTask<T> clusterTask) throws OXException {
        return acquireClusterLock(LOCK_TTL_MILLIS, clusterTask);
    }

    /**
     * Acquires a cluster lock for the specified {@link ClusterTask}
     *
     * @param timeToLiveMillis The lock's time to live in milliseconds;
     *                      if less than or equal to <code>0</code> (zero) is specified, the {@link #LOCK_TTL_MILLIS default time to live} is used
     * @param clusterTask The {@link ClusterTask} for which to acquire the cluster lock
     * @return The unique lock value if the cluster lock was successfully acquired, <code>null</code> otherwise
     * @throws OXException if an error is occurred during the acquisition of the cluster lock
     */
    <T> String acquireClusterLock(long timeToLiveMillis, ClusterTask<T> clusterTask) throws OXException;

    /**
     * (Immediately) Releases the cluster lock that was previously acquired for the specified {@link ClusterTask}.
     *
     * @param lockValue The unique lock value previously obtained
     * @param clusterTask The {@link ClusterTask} for which to release the lock
     * @throws OXException if an error is occurred during the release of the cluster lock
     */
    default <T> void releaseClusterLock(String lockValue, ClusterTask<T> clusterTask) throws OXException {
        releaseClusterLock(lockValue, true, clusterTask);
    }

    /**
     * Releases the cluster lock that was previously acquired for the specified {@link ClusterTask}.
     *
     * @param lockValue The unique lock value previously obtained
     * @param releaseLock Whether to release the lock immediately or to let it expire for next acquisition attempt
     * @param clusterTask The {@link ClusterTask} for which to release the lock
     * @throws OXException if an error is occurred during the release of the cluster lock
     */
    <T> void releaseClusterLock(String lockValue, boolean releaseLock, ClusterTask<T> clusterTask) throws OXException;

    /**
     * Runs the specified cluster task while previously acquiring a lock on the entire cluster
     * for this specific task. The current thread will acquire the lock for a predefined amount
     * of time. This method will either run the cluster task or not depending on whether the
     * cluster lock was acquired. If not an exception will be thrown
     *
     * @param clusterTask The {@link ClusterTask} to perform
     * @return The result {@link T}
     * @throws OXException if an error is occurred during the execution of the task or if the acquisition
     *             of the cluster lock fails
     */
    default <T> T runClusterTask(ClusterTask<T> clusterTask) throws OXException {
        return runClusterTask(clusterTask, new RunOnceRetryPolicy());
    }

    /**
     * Runs the specified cluster task while previously acquiring a lock on the entire cluster
     * for this specific task. The current thread will acquire the lock for a predefined amount
     * of time. The amount of retries to acquire the lock is depended on the specified {@link RetryPolicy}.
     * If the lock is not acquired after the predefined amount of retries an exception will be thrown.
     *
     * @param clusterTask The {@link ClusterTask} to perform
     * @param retryPolicy The {@link RetryPolicy} for acquiring a lock
     * @return The result {@link T}
     * @throws OXException if an error is occurred during the execution or if the acquisition
     *             of the cluster lock fails
     */
    default <T> T runClusterTask(ClusterTask<T> clusterTask, RetryPolicy retryPolicy) throws OXException {
        return runClusterTask(clusterTask, retryPolicy, LOCK_TTL_MILLIS, true);
    }

    /**
     * Runs the specified cluster task while previously acquiring a lock on the entire cluster
     * for this specific task. The current thread will acquire the lock for a predefined amount
     * of time. The amount of retries to acquire the lock is depended on the specified {@link RetryPolicy}.
     * If the lock is not acquired after the predefined amount of retries an exception will be thrown.
     *
     * @param clusterTask The {@link ClusterTask} to perform
     * @param retryPolicy The {@link RetryPolicy} for acquiring a lock
     * @param timeToLiveMillis The lock's time to live in milliseconds;
     *                      if less than or equal to <code>0</code> (zero) is specified, the {@link #LOCK_TTL_MILLIS default time to live} is used
     * @param releaseLock Whether to release the lock immediately or to let it expire for next acquisition attempt
     * @return The result {@link T}
     * @throws OXException if an error is occurred during the execution or if the acquisition
     *             of the cluster lock fails
     */
    <T> T runClusterTask(ClusterTask<T> clusterTask, RetryPolicy retryPolicy, long timeToLiveMillis, boolean releaseLock) throws OXException;
}
