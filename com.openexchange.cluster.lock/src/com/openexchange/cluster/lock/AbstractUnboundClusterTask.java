/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cluster.lock;

/**
 * {@link AbstractUnboundClusterTask} - A <i>global</i> cluster task that is not specific to a certain context/user.
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public abstract class AbstractUnboundClusterTask<T> implements ClusterTask<T> {

    /** Used as context-/user identifier to indicate the unbound task's <i>global</i> nature */
    protected static final int GLOBAL = -1;

    /**
     * Initializes a new {@link AbstractUnboundClusterTask}.
     */
    protected AbstractUnboundClusterTask() {
        super();
    }

    @Override
    public int getUserId() {
        return GLOBAL;
    }

    @Override
    public int getContextId() {
        return GLOBAL;
    }

}
