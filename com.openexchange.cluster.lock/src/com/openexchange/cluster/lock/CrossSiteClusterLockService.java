/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cluster.lock;

import com.openexchange.exception.OXException;
import com.openexchange.osgi.annotation.SingletonService;

/**
 * {@link CrossSiteClusterLockService} - Extends {@link ClusterLockService} with options to acquire/release locks on <i>remote</i> sites
 * as well, which might be necessary in <i>sharded</i> environments with multiple data centers ("Active/Active").
 * 
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
@SingletonService
public interface CrossSiteClusterLockService extends ClusterLockService {

    /**
     * Acquires a cluster lock for the specified {@link ClusterTask}
     *
     * @param timeToLiveMillis The lock's time to live in milliseconds;
     *            if less than or equal to <code>0</code> (zero) is specified, the {@link #LOCK_TTL_MILLIS default time to live} is used
     * @param clusterTask The {@link ClusterTask} for which to acquire the cluster lock
     * @param considerRemoteSites <code>true</code> to also acquire the lock on remote sites (if any), <code>false</code>, otherwise
     * @return The unique lock value if the cluster lock was successfully acquired, <code>null</code> otherwise
     * @throws OXException if an error is occurred during the acquisition of the cluster lock, or if remote sites considered but not supported
     */
    <T> String acquireClusterLock(long timeToLiveMillis, ClusterTask<T> clusterTask, boolean considerRemoteSites) throws OXException;

    /**
     * Releases the cluster lock that was previously acquired for the specified {@link ClusterTask}.
     *
     * @param lockValue The unique lock value previously obtained
     * @param releaseLock Whether to release the lock immediately or to let it expire for next acquisition attempt
     * @param clusterTask The {@link ClusterTask} for which to release the lock
     * @param considerRemoteSites <code>true</code> to also release the lock on remote sites (if any), <code>false</code>, otherwise
     * @throws OXException if an error is occurred during the release of the cluster lock, or if remote sites considered but not supported
     */
    <T> void releaseClusterLock(String lockValue, boolean releaseLock, ClusterTask<T> clusterTask, boolean considerRemoteSites) throws OXException;

}
