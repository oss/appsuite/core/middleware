/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cluster.lock.internal;

import static java.util.Objects.requireNonNull;
import com.openexchange.cluster.lock.ClusterTask;

/**
 * {@link ClusterTaskKey} - The key for a cluster task.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class ClusterTaskKey implements Comparable<ClusterTaskKey> {

    private final int contextId;
    private final int userId;
    private final String taskName;
    private final int hash;

    /**
     * Initializes a new {@link ClusterTaskKey}.
     *
     * @param clusterTask The cluster task to create the key for
     */
    public ClusterTaskKey(ClusterTask<?> clusterTask) {
        this(requireNonNull(clusterTask, "Cluster task must not be null").getContextId(), clusterTask.getUserId(), clusterTask.getTaskName());
    }

    /**
     * Initializes a new {@link ClusterTaskKey}.
     *
     * @param clusterTask The cluster task to create the key for
     */
    public ClusterTaskKey(int contextId, int userId, String taskName) {
        super();
        this.contextId = contextId;
        this.userId = userId;
        this.taskName = taskName;

        int prime = 31;
        int result = 1;
        result = prime * result + contextId;
        result = prime * result + userId;
        result = prime * result + ((taskName == null) ? 0 : taskName.hashCode());
        this.hash = result;
    }

    @Override
    public int compareTo(ClusterTaskKey o) {
        if (contextId != o.contextId) {
            return contextId < o.contextId ? -1 : 1;
        }
        if (userId != o.userId) {
            return userId < o.userId ? -1 : 1;
        }
        return taskName.compareTo(o.taskName);
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ClusterTaskKey other = (ClusterTaskKey) obj;
        if (contextId != other.contextId) {
            return false;
        }
        if (userId != other.userId) {
            return false;
        }
        if (taskName == null) {
            if (other.taskName != null) {
                return false;
            }
        } else if (!taskName.equals(other.taskName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[contextId=").append(contextId).append(", userId=").append(userId).append(", ");
        if (taskName != null) {
            builder.append("taskName=").append(taskName);
        }
        builder.append(']');
        return builder.toString();
    }

}
