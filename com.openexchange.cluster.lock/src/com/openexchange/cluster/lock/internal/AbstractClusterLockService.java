/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cluster.lock.internal;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import com.openexchange.cluster.lock.ClusterLockService;
import com.openexchange.cluster.lock.ClusterTask;
import com.openexchange.server.ServiceLookup;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

/**
 * {@link AbstractClusterLockService} - The abstract cluster lock service.
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public abstract class AbstractClusterLockService implements ClusterLockService {

    /** Defines the threshold for the lock refresh in milliseconds */
    public static final long REFRESH_LOCK_THRESHOLD_MILLIS = TimeUnit.SECONDS.toMillis(10);

    /** The service look-up */
    protected final ServiceLookup services;

    /** The map for established lock refreshers; e.g. touching expiration time stamp */
    protected final ConcurrentMap<ClusterTaskKey, ScheduledTimerTask> refreshers;

    /**
     * Initializes a new {@link AbstractClusterLockService}.
     *
     * @param services The {@link ServiceLookup} instance
     */
    protected AbstractClusterLockService(ServiceLookup services) {
        super();
        this.services = services;
        this.refreshers = new ConcurrentHashMap<>(16, 0.9F, 1);
    }

    /**
     * Registers the refresher performing given task.
     *
     * @param <T> The result type of the cluster task
     * @param task The refresher's task
     * @param timeToLiveMillis The lock's time to live in milliseconds
     * @param clusterTask The cluster task
     * @param logger The logger to use
     * @return <code>true</code> if such a refresher could be successfully registered; otherwise <code>false</code>
     */
    protected <T> boolean registerRefresherFor(Runnable task, long timeToLiveMillis, ClusterTask<T> clusterTask, org.slf4j.Logger logger) {
        return registerRefresherFor(task, timeToLiveMillis, clusterTask, () -> new ClusterTaskKey(clusterTask), logger);
    }

    /**
     * Registers the refresher performing given task.
     *
     * @param <T> The result type of the cluster task
     * @param task The refresher's task
     * @param timeToLiveMillis The lock's time to live in milliseconds
     * @param clusterTask The cluster task
     * @param refresherKeySupplier A {@link Supplier} for the registered refresher key
     * @param logger The logger to use
     * @return <code>true</code> if such a refresher could be successfully registered; otherwise <code>false</code>
     */
    protected <T> boolean registerRefresherFor(Runnable task, long timeToLiveMillis, ClusterTask<T> clusterTask, Supplier<ClusterTaskKey> refresherKeySupplier, org.slf4j.Logger logger) {
        try {
            // Initialize timer task for updating lock's time to live
            TimerService timerService = services.getServiceSafe(TimerService.class);
            long refreshLockThresholdMillis = REFRESH_LOCK_THRESHOLD_MILLIS;
            if (refreshLockThresholdMillis > timeToLiveMillis) {
                refreshLockThresholdMillis = timeToLiveMillis >> 1;
            }
            refreshers.put(refresherKeySupplier.get(), timerService.scheduleWithFixedDelay(task, refreshLockThresholdMillis, refreshLockThresholdMillis));
            logger.debug( "Registered refresher for cluster lock \"{}\"", clusterTask.getTaskName());
            return true;
        } catch (Exception e) {
            logger.error("Failed to initialize refresher for cluster lock \"{}\"", clusterTask.getTaskName(), e);
            return false;
        }
    }

    /**
     * Cancels the timer task for specified task name that cares about refreshing existent lock in Redis storage.
     *
     * @param <T> The result type of the cluster task
     * @param clusterTask The cluster task
     * @param logger The logger to use
     */
    protected <T> void cancelTimerTaskSafely(ClusterTask<T> clusterTask, org.slf4j.Logger logger) {
        cancelTimerTaskSafely(clusterTask, () -> new ClusterTaskKey(clusterTask), logger);
    }

    /**
     * Cancels the timer task for specified task name that cares about refreshing existent lock in Redis storage.
     *
     * @param <T> The result type of the cluster task
     * @param clusterTask The cluster task
     * @param refresherKeySupplier A {@link Supplier} for the registered refresher key
     * @param logger The logger to use
     */
    protected <T> void cancelTimerTaskSafely(ClusterTask<T> clusterTask, Supplier<ClusterTaskKey> refresherKeySupplier, org.slf4j.Logger logger) {
        ScheduledTimerTask timerTask = refreshers.remove(refresherKeySupplier.get());
        if (timerTask != null) {
            try {
                timerTask.cancel(false);
                TimerService timerService = services.getOptionalService(TimerService.class);
                if (timerService != null) {
                    timerService.purge();
                }
            } catch (Exception e) {
                logger.error("Failed to cancel timer task for Redis cluster lock \"{}\"", clusterTask.getTaskName(), e);
            }
        }
    }

}
