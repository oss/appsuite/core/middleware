/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cluster.lock.internal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.OptionalLong;
import com.openexchange.cluster.lock.ClusterTask;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.policy.retry.RetryPolicy;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.sql.DBUtils;

/**
 * {@link DatabaseClusterLockService} - The cluster lock service backed by database.
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class DatabaseClusterLockService extends AbstractClusterLockService {

    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(DatabaseClusterLockService.class);

    private static final String SUCCESSFULLY_LOCKED = "LOCKED";

    /**
     * Initializes a new {@link DatabaseClusterLockService}.
     *
     * @param services The service look-up
     */
    public DatabaseClusterLockService(ServiceLookup services) {
        super(services);
    }

    @Override
    public <T> String acquireClusterLock(long timeToLiveMillis, ClusterTask<T> clusterTask) throws OXException {
        long ttl = timeToLiveMillis <= 0 ? LOCK_TTL_MILLIS : timeToLiveMillis;

        DatabaseService databaseService = services.getService(DatabaseService.class);
        int contextId = clusterTask.getContextId();

        boolean locked;
        do {
            Connection connection = databaseService.getWritable(contextId);
            try {
                OptionalLong expirationTimestamp = getExpirationTimestamp(clusterTask, connection);
                long now = System.currentTimeMillis();
                if (expirationTimestamp.isEmpty()) {
                    // No lock entry
                    locked = acquireClusterLock(clusterTask, now + ttl, connection);
                } else if (expirationTimestamp.getAsLong() < now) {
                    // Lock entry expired
                    locked = updateExpirationTimestamp(clusterTask, now + ttl, expirationTimestamp.getAsLong(), connection);
                } else {
                    // Still locked...
                    return null;
                }
            } catch (SQLException e) {
                throw ClusterLockExceptionCodes.SQL_ERROR.create(e, e.getMessage());
            } finally {
                databaseService.backWritable(contextId, connection);
            }
        } while (!locked);

        // Lock acquired
        Runnable task = new RefreshLockTask<>(ttl, clusterTask);
        if (registerRefresherFor(task, ttl, clusterTask, LOGGER)) {
            return SUCCESSFULLY_LOCKED;
        }

        deleteExpirationTimestamp(clusterTask);
        return null;
    }

    @Override
    public <T> void releaseClusterLock(String lockValue, boolean releaseLock, ClusterTask<T> clusterTask) throws OXException {
        if (!SUCCESSFULLY_LOCKED.equals(lockValue)) {
            return;
        }

        cancelTimerTaskSafely(clusterTask, LOGGER);
        if (releaseLock) {
            deleteExpirationTimestamp(clusterTask);
            LOGGER.debug("Released lock \"{}\"", clusterTask.getTaskName());
        } else {
            LOGGER.debug("Leaving lock \"{}\" to expire", clusterTask.getTaskName());
        }
    }

    @Override
    public <T> T runClusterTask(ClusterTask<T> clusterTask, RetryPolicy retryPolicy, long timeToLiveMillis, boolean releaseLock) throws OXException {
        do {
            // Acquire the lock
            String lockValue = acquireClusterLock(timeToLiveMillis, clusterTask);
            try {
                if (lockValue != null) {
                    LOGGER.debug("Cluster lock for cluster task '{}' acquired with retry policy '{}'", clusterTask.getTaskName(), retryPolicy.getClass().getSimpleName());
                    T t = clusterTask.perform();
                    LOGGER.debug("Cluster task '{}' completed.", clusterTask.getTaskName());
                    return t;
                }
                LOGGER.debug("Another node is performing the cluster task '{}'. Cluster lock was not acquired. Performing retry according to '{}'", clusterTask.getTaskName(), retryPolicy.getClass().getSimpleName());
            } finally {
                LOGGER.debug("Releasing cluster lock held by the cluster task '{}'.", clusterTask.getTaskName());
                if (lockValue != null) {
                    releaseClusterLock(lockValue, releaseLock, clusterTask);
                }
            }
        } while (retryPolicy.isRetryAllowed());

        // Failed to acquire lock permanently
        throw ClusterLockExceptionCodes.UNABLE_TO_ACQUIRE_CLUSTER_LOCK.create(clusterTask.getTaskName());
    }

    //////////////////////////////////// HELPERS ///////////////////////////////////

    /**
     * {@link RefreshLockTask} - Refreshes the lock expiration time stamp for the specified task
     */
    private class RefreshLockTask<T> implements Runnable {

        private final ClusterTask<T> clusterTask;
        private final long timeToLiveMillis;

        /**
         * Initializes a new {@link RefreshLockTask}.
         *
         * @param timeToLiveMillis The time-to-live in milliseconds
         * @param clusterTask The {@link ClusterTask} to refresh
         */
        public RefreshLockTask(long timeToLiveMillis, ClusterTask<T> clusterTask) {
            super();
            this.timeToLiveMillis = timeToLiveMillis;
            this.clusterTask = clusterTask;
        }

        @Override
        public void run() {
            int contextId = clusterTask.getContextId();
            int userId = clusterTask.getUserId();

            DatabaseService databaseService = services.getService(DatabaseService.class);

            Connection connection = null;
            PreparedStatement statement = null;
            try {
                connection = databaseService.getWritable(contextId);
                statement = connection.prepareStatement("UPDATE clusterLock SET timestamp=? WHERE cid=? AND user=? AND name=?");
                statement.setLong(1, System.currentTimeMillis() + timeToLiveMillis);
                statement.setInt(2, contextId);
                statement.setInt(3, userId);
                statement.setString(4, clusterTask.getTaskName());
                statement.executeUpdate();
            } catch (OXException e) {
                LOGGER.error("Cannot get a writable connection for cluster task '{}'. The cluster task lock was not refreshed. {}", clusterTask.getTaskName(), e.getMessage(), e);
            } catch (Exception e) {
                LOGGER.error("The lock for cluster task '{}' was not refreshed. {}", clusterTask.getTaskName(), e.getMessage(), e);
            } finally {
                DBUtils.closeResources(null, statement, connection, false, contextId);
            }
        }
    }

    /**
     * Inserts the specified lock entry using the specified writable {@link Connection}
     *
     * @param clusterTask The {@link ClusterTask}
     * @param expirationStamp The expiration time stamp to insert
     * @param connection The writable {@link Connection}
     * @return <code>true</code> if lock entry was successfully inserted (indicating that the lock was acquired); <code>false</code> otherwise
     * @throws SQLException If an SQL error occurred
     */
    private static <T> boolean acquireClusterLock(ClusterTask<T> clusterTask, long expirationStamp, Connection connection) throws SQLException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("INSERT INTO clusterLock (cid, user, name, timestamp) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE `cid` = `cid` AND `user` = `user` AND `name` = `name`");
            statement.setInt(1, clusterTask.getContextId());
            statement.setInt(2, clusterTask.getUserId());
            statement.setString(3, clusterTask.getTaskName());
            statement.setLong(4, expirationStamp);
            return statement.executeUpdate() > 0;
        } finally {
            Databases.closeSQLStuff(statement);
        }
    }

    /**
     * Gets the existing expiration time stamp from the database (if any).
     *
     * @param clusterTask The {@link ClusterTask}
     * @param connection The {@link Connection}
     * @return The expiration time stamp or <code>0</code> (zero) if no such time stamp exists
     * @throws SQLException If an SQL exception occurred
     */
    private static <T> OptionalLong getExpirationTimestamp(ClusterTask<T> clusterTask, Connection connection) throws SQLException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement("SELECT timestamp FROM clusterLock WHERE cid=? AND user=? AND name=?");
            statement.setInt(1, clusterTask.getContextId());
            statement.setInt(2, clusterTask.getUserId());
            statement.setString(3, clusterTask.getTaskName());
            resultSet = statement.executeQuery();
            return resultSet.next() ? OptionalLong.of(resultSet.getLong(1)) : OptionalLong.empty();
        } finally {
            Databases.closeSQLStuff(statement);
        }
    }

    /**
     * Updates the expiration time stamp for the specified {@link ClusterTask}
     *
     * @param clusterTask The {@link ClusterTask}
     * @param newExpirationTimestamo The time now
     * @param oldExpirationTimestamp The time then
     * @param connection The writable {@link Connection}
     * @return <code>true</code> if the time stamp was successfully updated; <code>false</code> otherwise
     * @throws SQLException if an SQL exception is occurred
     */
    private static <T> boolean updateExpirationTimestamp(ClusterTask<T> clusterTask, long newExpirationTimestamo, long oldExpirationTimestamp, Connection connection) throws SQLException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("UPDATE clusterLock SET timestamp=? WHERE cid=? AND user=? AND name=? AND timestamp=?");
            statement.setLong(1, newExpirationTimestamo);
            statement.setInt(2, clusterTask.getContextId());
            statement.setInt(3, clusterTask.getUserId());
            statement.setString(4, clusterTask.getTaskName());
            statement.setLong(5, oldExpirationTimestamp);
            return statement.executeUpdate() > 0;
        } finally {
            Databases.closeSQLStuff(statement);
        }
    }

    /**
     * Deletes the expiration time stamp for the specified {@link ClusterTask}
     *
     * @param clusterTask The {@link ClusterTask}
     * @throws OXException if an error is occurred
     */
    private <T> void deleteExpirationTimestamp(ClusterTask<T> clusterTask) throws OXException {
        DatabaseService databaseService = services.getService(DatabaseService.class);
        int contextId = clusterTask.getContextId();

        PreparedStatement statement = null;
        Connection connection = databaseService.getWritable(contextId);
        try {
            statement = connection.prepareStatement("DELETE FROM clusterLock WHERE cid=? AND user=? AND name=?");
            statement.setInt(1, contextId);
            statement.setInt(2, clusterTask.getUserId());
            statement.setString(3, clusterTask.getTaskName());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw ClusterLockExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(statement);
        }
    }
}
