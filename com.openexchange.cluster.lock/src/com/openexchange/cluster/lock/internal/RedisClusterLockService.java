/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cluster.lock.internal;

import java.util.List;
import java.util.Stack;
import java.util.function.Supplier;
import com.openexchange.cluster.lock.ClusterTask;
import com.openexchange.cluster.lock.CrossSiteClusterLockService;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.java.util.UUIDs;
import com.openexchange.policy.retry.RetryPolicy;
import com.openexchange.redis.RedisConnector;
import com.openexchange.redis.RedisConnectorProvider;
import com.openexchange.redis.RedisConnectorService;
import com.openexchange.server.ServiceLookup;
import io.lettuce.core.SetArgs;

/**
 * {@link RedisClusterLockService} - The cluster lock service based on Redis.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RedisClusterLockService extends AbstractClusterLockService implements CrossSiteClusterLockService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(RedisClusterLockService.class);

    private static final String REDIS_PREFIX = "ox-lock";
    private static final char DELIMITER = ':';

    private final RedisConnectorService connectorService;

    /**
     * Initializes a new {@link RedisClusterLockService}.
     *
     * @param connectorService The Redis connector service
     * @param services The service look-up
     */
    public RedisClusterLockService(RedisConnectorService connectorService, ServiceLookup services) {
        super(services);
        this.connectorService = connectorService;
    }

    @Override
    public <T> String acquireClusterLock(long timeToLiveMillis, ClusterTask<T> clusterTask) throws OXException {
        return acquireClusterLock(timeToLiveMillis, clusterTask, false);
    }

    @Override
    public <T> String acquireClusterLock(long timeToLiveMillis, ClusterTask<T> clusterTask, boolean considerRemoteSites) throws OXException {
        /*
         * acquire site-local cluster lock
         */
        String lockValue = acquireClusterLock(timeToLiveMillis, clusterTask, getConnectorProvider());
        if (null == lockValue || false == considerRemoteSites) {
            return lockValue;
        }
        /*
         * acquire lock on remote sites if configured
         */
        List<RedisConnectorProvider> remoteConnectorProviders = getRemoteConnectorProviders();
        if (remoteConnectorProviders.isEmpty()) {
            return lockValue;
        }
        Stack<RedisConnectorProvider> visitedConnectorProviders = new Stack<RedisConnectorProvider>();
        visitedConnectorProviders.add(getConnectorProvider());
        boolean success = false;
        try {
            for (RedisConnectorProvider connectorProvider : getRemoteConnectorProviders()) {
                if (false == lockValue.equals(acquireClusterLock(lockValue, timeToLiveMillis, clusterTask, connectorProvider))) {
                    LOG.debug("Unable to acquire cluster lock on remote site via {}.", connectorProvider.getConnector());
                    return null;
                }
                visitedConnectorProviders.add(connectorProvider);
                LOG.debug("Successfully acquired cluster lock on remote site via {}.", connectorProvider.getConnector());
            }
            success = true;
        } finally {
            if (false == success) {
                for (RedisConnectorProvider connectorProvider : visitedConnectorProviders) {
                    try {
                        releaseClusterLock(lockValue, true, clusterTask, connectorProvider);
                    } catch (Exception e) {
                        LOG.warn("Unexpected error releasing cluster lock after failed lock acquisition at {}", connectorProvider.getConnector(), e);
                    }
                }
                lockValue = null;
            }
        }
        return lockValue;
    }

    /**
     * Acquires the cluster lock in Redis storage for specified cluster task.
     *
     * @param <T> The result type of the cluster task
     * @param timeToLiveMillis The lock's time to live in milliseconds
     * @param clusterTask The cluster task for which the lock shall be acquired
     * @param connectorProvider The Redis connector provider
     * @return The unique lock value or <code>null</code>
     * @throws OXException If acquiring lock fails
     */
    private <T> String acquireClusterLock(long timeToLiveMillis, ClusterTask<T> clusterTask, RedisConnectorProvider connectorProvider) throws OXException {
        return acquireClusterLock(UUIDs.getUnformattedStringFromRandom(), timeToLiveMillis, clusterTask, connectorProvider);
    }

    /**
     * Acquires the cluster lock in Redis storage for specified cluster task.
     *
     * @param <T> The result type of the cluster task
     * @param uniqueLockValue The unique value to use for the lock
     * @param timeToLiveMillis The lock's time to live in milliseconds
     * @param clusterTask The cluster task for which the lock shall be acquired
     * @param connectorProvider The Redis connector provider
     * @return The unique lock value or <code>null</code>
     * @throws OXException If acquiring lock fails
     */
    private <T> String acquireClusterLock(String uniqueLockValue, long timeToLiveMillis, ClusterTask<T> clusterTask, RedisConnectorProvider connectorProvider) throws OXException {
        String lockKey = getLockKeyFor(clusterTask);
        RedisConnector connector = connectorProvider.getConnector();
        return connector.executeOperation(commandsProvider -> {
            long ttl = timeToLiveMillis <= 0 ? LOCK_TTL_MILLIS : timeToLiveMillis;
            if (!"OK".equals(commandsProvider.getStringCommands().set(lockKey, uniqueLockValue, new SetArgs().nx().px(ttl)))) {
                // Lock NOT acquired
                return null;
            }

            // Lock acquired...
            Runnable task = new RefreshLockTask(lockKey, ttl, connectorProvider);
            if (registerRefresherFor(task, ttl, clusterTask, getClusterTaskKeySupplier(clusterTask, connector.getInstanceId()), LOG)) {
                return uniqueLockValue;
            }

            // Failed to register refresher
            commandsProvider.getKeyCommands().del(lockKey);
            return null;
        });
    }

    @Override
    public <T> void releaseClusterLock(String lockValue, boolean releaseLock, ClusterTask<T> clusterTask) throws OXException {
        releaseClusterLock(lockValue, releaseLock, clusterTask, false);
    }

    @Override
    public <T> void releaseClusterLock(String lockValue, boolean releaseLock, ClusterTask<T> clusterTask, boolean considerRemoteSites) throws OXException {
        try {
            /*
             * release site-local cluster lock
             */
            releaseClusterLock(lockValue, releaseLock, clusterTask, getConnectorProvider());
        } finally {
            /*
             * release lock on remote sites if configured
             */
            if (considerRemoteSites) {
                for (RedisConnectorProvider connectorProvider : getRemoteConnectorProviders()) {
                    try {
                        releaseClusterLock(lockValue, releaseLock, clusterTask, connectorProvider);
                    } catch (Exception e) {
                        LOG.warn("Unexpected error releasing cluster lock at {}", connectorProvider.getConnector(), e);
                    }
                }
            }
        }
    }

    private static enum ReleaseResult {
        MISMATCH,
        RELEASED,
        UNRELEASED,
        LEFT,
        FAILURE;
    }

    /**
     * Releases the previously obtained cluster lock in Redis storage.
     *
     * @param <T> The result type of the cluster task
     * @param lockValue The unique lock value previously obtained
     * @param releaseLock Whether to release the lock immediately or to let it expire for next acquisition attempt
     * @param clusterTask The cluster task for which the lock has been acquired
     * @param connectorProvider The Redis connector provider
     * @throws OXException If releasing lock fails
     */
    private <T> void releaseClusterLock(String lockValue, boolean releaseLock, ClusterTask<T> clusterTask, RedisConnectorProvider connectorProvider) throws OXException {
        ReleaseResult result = ReleaseResult.FAILURE;
        RedisConnector connector = null;
        try {
            String lockKey = getLockKeyFor(clusterTask);
            connector = connectorProvider.getConnector();
            result = connector.executeOperation(commandsProvider -> {
                try {
                    if (!lockValue.equals(commandsProvider.getStringCommands().get(lockKey))) {
                        // Lock value does NOT match
                        LOG.debug("Unable to release Redis cluster lock \"{}\" since lock value \"{}\" does not match", lockKey, lockValue);
                        return ReleaseResult.MISMATCH;
                    }

                    // Lock value does match; check whether to delete lock or not
                    if (!releaseLock) {
                        LOG.debug("Leaving lock \"{}\" to expire", lockKey);
                        return ReleaseResult.LEFT;
                    }

                    // Drop lock
                    boolean released = commandsProvider.getKeyCommands().del(lockKey).longValue() > 0;
                    LOG.debug(released ? "Released Redis cluster lock \"{}\"" : "Unable to release Redis cluster lock \"{}\"", lockKey);
                    return released ? ReleaseResult.RELEASED : ReleaseResult.UNRELEASED;
                } catch (Exception e) {
                    LOG.error("Failed to release Redis cluster lock \"{}\"", lockKey, e);
                    return ReleaseResult.FAILURE;
                }
            });
        } finally {
            if (result != ReleaseResult.MISMATCH) {
                cancelTimerTaskSafely(clusterTask, getClusterTaskKeySupplier(clusterTask, null != connector ? connector.getInstanceId() : null), LOG);
            }
        }
    }

    @Override
    public <T> T runClusterTask(ClusterTask<T> clusterTask, RetryPolicy retryPolicy, long timeToLiveMillis, boolean releaseLock) throws OXException {
        RedisConnectorProvider connectorProvider = getConnectorProvider();
        do {
            // Acquire the lock
            String lockValue = acquireClusterLock(timeToLiveMillis, clusterTask, connectorProvider);
            try {
                if (lockValue != null) {
                    LOG.debug("Redis cluster lock for cluster task '{}' acquired with retry policy '{}'", clusterTask.getTaskName(), retryPolicy.getClass().getSimpleName());
                    T t = clusterTask.perform();
                    LOG.debug("Cluster task '{}' completed.", clusterTask.getTaskName());
                    return t;
                }
                LOG.debug("Another node is performing the cluster task '{}'. Redis cluster lock was not acquired. Performing retry according to '{}'", clusterTask.getTaskName(), retryPolicy.getClass().getSimpleName());
            } finally {
                if (lockValue != null) {
                    LOG.debug("Releasing cluster lock held by the cluster task '{}'.", clusterTask.getTaskName());
                    releaseClusterLock(lockValue, releaseLock, clusterTask, connectorProvider);
                }
            }
        } while (retryPolicy.isRetryAllowed());

        // Failed to acquire lock permanently
        throw ClusterLockExceptionCodes.UNABLE_TO_ACQUIRE_CLUSTER_LOCK.create(clusterTask.getTaskName());
    }

    private RedisConnectorProvider getConnectorProvider() throws OXException {
        return connectorService.getConnectorProvider();
    }

    private List<RedisConnectorProvider> getRemoteConnectorProviders() throws OXException {
        return connectorService.getRemoteConnectorProviders();
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Simple class that cares about updating expiration of the Redis lock entry.
     */
    private static final class RefreshLockTask implements Runnable {

        private final RedisConnectorProvider connectorProvider;
        private final String lockKey;
        private final long timeToLiveMillis;

        /**
         * Initializes a new {@link RefreshLockTask}.
         *
         * @param lockKey The key of the lock
         * @param timeToLiveMillis The lock's time to live in milliseconds
         * @param connectorProvider The Redis connector provider
         */
        private RefreshLockTask(String lockKey, long timeToLiveMillis, RedisConnectorProvider connectorProvider) {
            super();
            this.connectorProvider = connectorProvider;
            this.lockKey = lockKey;
            this.timeToLiveMillis = timeToLiveMillis;
        }

        @Override
        public void run() {
            try {
                RedisConnector connector = connectorProvider.getConnector();
                updateLock(lockKey, timeToLiveMillis, connector);
                LOG.info("Updated time to live of Redis cluster lock \"{}\" to {}msec at {}", lockKey, Long.valueOf(timeToLiveMillis), connector);
            } catch (Exception e) {
                LOG.error("Failed to update time to live of Redis cluster lock \"{}\"", lockKey, e);
            }
        }

        /**
         * Updates the named lock; reset its time to live in milliseconds.
         *
         * @param lockKey The key of the lock
         * @param timeToLiveMillis The lock's time to live in milliseconds
         * @param connector The Redis connector
         * @throws OXException If lock update fails
         */
        private static void updateLock(String lockKey, long timeToLiveMillis, RedisConnector connector) throws OXException {
            connector.executeVoidOperation(commandsProvider -> commandsProvider.getKeyCommands().pexpire(lockKey, timeToLiveMillis));
        }
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Gets the lock key for given cluster task; e.g. <code>"ox-lock:1337:3:myClusterTask"</code>.
     *
     * @param <T> The return type of the cluster task
     * @param clusterTask The cluster task
     * @return The lock key
     */
    private static <T> String getLockKeyFor(ClusterTask<T> clusterTask) {
        StringBuilder sb = new StringBuilder(REDIS_PREFIX);
        if (clusterTask.getContextId() > 0) {
            sb.append(DELIMITER).append(clusterTask.getContextId());
            if (clusterTask.getUserId() > 0) {
                sb.append(DELIMITER).append(clusterTask.getUserId());
            }
        }
        return sb.append(DELIMITER).append(saneForRedisKey(clusterTask.getTaskName())).toString();
    }

    /**
     * Constructs a {@link Supplier} for a key representing the given cluster task on a specific <i>site</i>.
     *
     * @param clusterTask The cluster task to generate the key supplier for
     * @param siteId The identifier representing the (remote) site the task connects to, or <code>null</code> if local
     * @return The cluster task key supplier
     */
    private static Supplier<ClusterTaskKey> getClusterTaskKeySupplier(ClusterTask<?> clusterTask, String siteId) {
        if (Strings.isEmpty(siteId)) {
            return () -> new ClusterTaskKey(clusterTask);
        }
        return () -> new ClusterTaskKey(clusterTask.getContextId(), clusterTask.getUserId(), clusterTask.getTaskName() + '@' + siteId);
    }

    private static String saneForRedisKey(String taskName) {
        if (Strings.isEmpty(taskName)) {
            return taskName;
        }

        int len = taskName.length();
        StringBuilder sb = null;
        char prev = '\0';
        for (int i = 0; i < len; i++) {
            char c = taskName.charAt(i);
            if (Strings.isWhitespace(c)) {
                if (prev != '_') {
                    prev = '_';
                    if (sb == null) {
                        sb = new StringBuilder(len);
                        if (i > 0) {
                            sb.append(taskName, 0, i);
                        }
                    }
                    sb.append(prev);
                }
            } else if ('/' == c) {
                if (prev != '_') {
                    prev = '_';
                    if (sb == null) {
                        sb = new StringBuilder(len);
                        if (i > 0) {
                            sb.append(taskName, 0, i);
                        }
                    }
                    sb.append(prev);
                }
            } else if ('\\' == c) {
                if (prev != '_') {
                    prev = '_';
                    if (sb == null) {
                        sb = new StringBuilder(len);
                        if (i > 0) {
                            sb.append(taskName, 0, i);
                        }
                    }
                    sb.append(prev);
                }
            } else {
                prev = '\0';
                if (sb != null) {
                    sb.append(c);
                }
            }
        }
        return sb == null ? taskName : sb.toString();
    }

}
