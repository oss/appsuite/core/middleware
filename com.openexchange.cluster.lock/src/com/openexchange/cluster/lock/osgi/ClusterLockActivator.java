/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.cluster.lock.osgi;

import static com.openexchange.osgi.Tools.withRanking;
import org.osgi.framework.ServiceRegistration;
import com.openexchange.cluster.lock.ClusterLockService;
import com.openexchange.cluster.lock.internal.DatabaseClusterLockService;
import com.openexchange.database.DatabaseService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.redis.RedisConnectorService;
import com.openexchange.timer.TimerService;

/**
 * {@link ClusterLockActivator}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class ClusterLockActivator extends HousekeepingActivator {

    private ServiceRegistration<ClusterLockService> databaseClsRegistration; // Guarded by synchronized

    /**
     * Initializes a new {@link ClusterLockActivator}.
     */
    public ClusterLockActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { DatabaseService.class, TimerService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        registerDatabaseClusterLockService();
        track(RedisConnectorService.class, new RedisConnectorTracker(this, context));
        openTrackers();
    }

    @Override
    protected void stopBundle() throws Exception {
        super.stopBundle();
        unregisterDatabaseClusterLockService();
    }

    /**
     * Registers the database-backed cluster lock service (if not already registered).
     */
    synchronized void registerDatabaseClusterLockService() {
        if (databaseClsRegistration == null) {
            databaseClsRegistration = context.registerService(ClusterLockService.class, new DatabaseClusterLockService(this), withRanking(0));
        }
    }

    /**
     * Un-registers the database-backed cluster lock service (if any).
     */
    synchronized void unregisterDatabaseClusterLockService() {
        ServiceRegistration<ClusterLockService> databaseClsRegistration = this.databaseClsRegistration;
        if (databaseClsRegistration != null) {
            this.databaseClsRegistration = null;
            databaseClsRegistration.unregister();
        }
    }

}
