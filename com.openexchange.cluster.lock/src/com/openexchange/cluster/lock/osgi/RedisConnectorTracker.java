/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.cluster.lock.osgi;

import static com.openexchange.osgi.Tools.withRanking;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import com.openexchange.cluster.lock.ClusterLockService;
import com.openexchange.cluster.lock.CrossSiteClusterLockService;
import com.openexchange.cluster.lock.internal.RedisClusterLockService;
import com.openexchange.redis.RedisConnectorService;

/**
 * {@link RedisConnectorTracker} - Tracks Redis connector service and replaces service through a Redis-back cluster lock service.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class RedisConnectorTracker implements ServiceTrackerCustomizer<RedisConnectorService, RedisConnectorService> {

    private final BundleContext context;
    private final ClusterLockActivator activator;
    private ServiceRegistration<ClusterLockService> redisClsRegistration; // Guarded by synchronized
    private ServiceRegistration<CrossSiteClusterLockService> redisCsClsRegistration; // Guarded by synchronized

    /**
     * Initializes a new {@link RedisConnectorTracker}.
     *
     * @param activator The parental activator used to register/unregister database-backed cluster lock service
     * @param context The bundle context to use
     */
    public RedisConnectorTracker(ClusterLockActivator activator, BundleContext context) {
        super();
        this.context = context;
        this.activator = activator;
    }

    @Override
    public synchronized RedisConnectorService addingService(ServiceReference<RedisConnectorService> reference) {
        RedisConnectorService redisConnectorService = context.getService(reference);
        RedisClusterLockService clusterLockService = new RedisClusterLockService(redisConnectorService, activator);
        activator.unregisterDatabaseClusterLockService();
        redisClsRegistration = context.registerService(ClusterLockService.class, clusterLockService, withRanking(10));
        redisCsClsRegistration = context.registerService(CrossSiteClusterLockService.class, clusterLockService, withRanking(10));
        return redisConnectorService;
    }

    @Override
    public synchronized void modifiedService(ServiceReference<RedisConnectorService> reference, RedisConnectorService service) {
        // Nothing
    }

    @Override
    public synchronized void removedService(ServiceReference<RedisConnectorService> reference, RedisConnectorService service) {
        ServiceRegistration<ClusterLockService> redisClsRegistration = this.redisClsRegistration;
        if (redisClsRegistration != null) {
            this.redisClsRegistration = null;
            redisClsRegistration.unregister();
        }
        ServiceRegistration<CrossSiteClusterLockService> redisCsClsRegistration = this.redisCsClsRegistration;
        if (redisCsClsRegistration != null) {
            this.redisCsClsRegistration = null;
            redisCsClsRegistration.unregister();
        }
        activator.registerDatabaseClusterLockService();
        context.ungetService(reference);
    }

}
