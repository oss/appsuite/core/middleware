/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.reseller.impl;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.Caches;
import com.openexchange.cache.v2.codec.IntegerCodec;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.exception.OXException;
import com.openexchange.reseller.ResellerExceptionCodes;
import com.openexchange.reseller.ResellerService;
import com.openexchange.reseller.data.ResellerAdmin;
import com.openexchange.reseller.data.ResellerCapability;
import com.openexchange.reseller.data.ResellerConfigProperty;
import com.openexchange.reseller.data.ResellerTaxonomy;
import com.openexchange.reseller.impl.caching.ResellerAdminCodec;

/**
 * {@link CachingResellerService} - Wraps reseller service implementation with caches for fast look-up.
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @since v7.10.5
 */
public class CachingResellerService implements ResellerService {

    /** Placeholder identifier when there is no reseller associated with a context. */
    private static final Integer NO_RESELLER = I(-1);

    /** Options for the cache holding reseller administrators per reseller identifier */
    private static final CacheOptions<ResellerAdmin> CACHE_OPTIONS_RESELLER = CacheOptions.<ResellerAdmin> builder()
                                                                                       .withCoreModuleName(CoreModuleName.RESELLER_ADMIN)
                                                                                       .withCodecAndVersion(new ResellerAdminCodec())
                                                                                       .build();

    /** Options for the cache holding associating context identifiers with their corresponding reseller identifier */
    private static final CacheOptions<Integer> CACHE_OPTIONS_CONTEXTS = CacheOptions.<Integer> builder()
                                                                                       .withCoreModuleName(CoreModuleName.RESELLER_CONTEXT)
                                                                                       .withCodecAndVersion(IntegerCodec.getInstance())
                                                                                       .build();

    // -------------------------------------------------------------------------------------------------------------------------------------

    private final ResellerServiceImpl delegate;
    private final CacheService cacheService;

    /**
     * Initializes a new {@link CachingResellerService}.
     *
     * @param cacheService The cache service to use
     * @param delegate The {@link ResellerServiceImpl} to delegate to
     */
    public CachingResellerService(CacheService cacheService, ResellerServiceImpl delegate) {
        super();
        this.cacheService = cacheService;
        this.delegate = delegate;
    }

    @Override
    public ResellerAdmin getReseller(int contextId) throws OXException {
        return optReseller(contextId).orElseThrow(() -> ResellerExceptionCodes.NO_RESELLER_FOUND_FOR_CTX.create(I(contextId)));
    }

    @Override
    public ResellerAdmin getResellerById(int resellerId) throws OXException {
        Cache<ResellerAdmin> cache = getResellerCache();
        CacheKey key = createResellerKey(cache, resellerId);
        try {
            return cache.get(key, k -> delegate.getResellerById(resellerId));
        } catch (OXException e) {
            if (!Caches.isCacheException(e)) {
                throw e;
            }
            return delegate.getResellerById(resellerId);
        }
    }

    @Override
    public ResellerAdmin getResellerByName(String resellerName) throws OXException {
        return delegate.getResellerByName(resellerName);
    }

    @Override
    public List<ResellerAdmin> getResellerAdminPath(int contextId) throws OXException {
        return delegate.getResellerAdminPath(contextId);
    }

    @Override
    public List<ResellerAdmin> getSubResellers(int parentId) throws OXException {
        return delegate.getSubResellers(parentId);
    }

    @Override
    public List<ResellerAdmin> getAll() throws OXException {
        return delegate.getAll();
    }

    @Override
    public boolean isEnabled() {
        return delegate.isEnabled();
    }

    @Override
    public Set<ResellerCapability> getCapabilities(int resellerId) throws OXException {
        return getResellerById(resellerId).getCapabilities();
    }

    @Override
    public Set<ResellerCapability> getCapabilitiesByContext(int contextId) throws OXException {
        Optional<ResellerAdmin> optionalReseller = optReseller(contextId);
        if (optionalReseller.isEmpty()) {
            return Collections.emptySet();
        }
        ResellerAdmin reseller = optionalReseller.get();
        Set<ResellerCapability> capabilities = null != reseller.getCapabilities() ?
            new HashSet<>(reseller.getCapabilities()) : new HashSet<>();
        while (null != reseller.getParentId() && 0 < i(reseller.getParentId())) {
            // Traverse the admin path to get all capabilities for the context
            reseller = getResellerById(i(reseller.getParentId()));
            if (null != reseller.getCapabilities()) {
                capabilities.addAll(reseller.getCapabilities());
            }
        }
        return capabilities;
    }

    @Override
    public ResellerConfigProperty getConfigProperty(int resellerId, String key) throws OXException {
        return getAllConfigProperties(resellerId).get(key);
    }

    @Override
    public ResellerConfigProperty getConfigPropertyByContext(int contextId, String key) throws OXException {
        return getAllConfigPropertiesByContext(contextId).get(key);
    }

    @Override
    public Map<String, ResellerConfigProperty> getAllConfigProperties(int resellerId) throws OXException {
        return getResellerById(resellerId).getConfiguration();
    }

    @Override
    public Map<String, ResellerConfigProperty> getAllConfigPropertiesByContext(int contextId) throws OXException {
        Optional<ResellerAdmin> optionalReseller = optReseller(contextId);
        if (optionalReseller.isEmpty()) {
            return Collections.emptyMap();
        }
        ResellerAdmin reseller = optionalReseller.get();
        Map<String, ResellerConfigProperty> properties = null != reseller.getConfiguration() ?
            new HashMap<>(reseller.getConfiguration()) : new HashMap<>();
        while (null != reseller.getParentId() && 0 < i(reseller.getParentId())) {
            // Traverse the admin path to get all properties for the context
            reseller = getResellerById(i(reseller.getParentId()));
            if (null != reseller.getConfiguration()) {
                properties.putAll(reseller.getConfiguration());
            }
        }
        return properties;
    }

    @Override
    public Map<String, ResellerConfigProperty> getConfigProperties(int resellerId, Set<String> keys) throws OXException {
        if (keys == null) {
            return Collections.emptyMap();
        }
        int numberOfKeys = keys.size();
        if (numberOfKeys <= 0) {
            return Collections.emptyMap();
        }
        Map<String, ResellerConfigProperty> properties = getAllConfigProperties(resellerId);
        Map<String, ResellerConfigProperty> ret = null;
        for (String key : keys) {
            ResellerConfigProperty property = properties.get(key);
            if (property != null) {
                if (ret == null) {
                    ret = HashMap.newHashMap(numberOfKeys);
                }
                ret.put(key, property);
            }
        }
        return ret == null ? Collections.emptyMap() : ret;
    }

    @Override
    public Map<String, ResellerConfigProperty> getConfigPropertiesByContext(int contextId, Set<String> keys) throws OXException {
        if (keys == null) {
            return Collections.emptyMap();
        }
        int numberOfKeys = keys.size();
        if (numberOfKeys <= 0) {
            return Collections.emptyMap();
        }
        Map<String, ResellerConfigProperty> configuration = getAllConfigPropertiesByContext(contextId);
        Map<String, ResellerConfigProperty> ret = null;
        for (String key : keys) {
            ResellerConfigProperty property = configuration.get(key);
            if (property != null) {
                if (ret == null) {
                    ret = HashMap.newHashMap(numberOfKeys);
                }
                ret.put(key, property);
            }
        }
        return ret == null ? Collections.emptyMap() : ret;
    }

    @Override
    public Set<ResellerTaxonomy> getTaxonomies(int resellerId) throws OXException {
        return getResellerById(resellerId).getTaxonomies();
    }

    @Override
    public Set<ResellerTaxonomy> getTaxonomiesByContext(int contextId) throws OXException {
        Optional<ResellerAdmin> optionalReseller = optReseller(contextId);
        if (optionalReseller.isEmpty()) {
            return Collections.emptySet();
        }
        ResellerAdmin reseller = optionalReseller.get();
        Set<ResellerTaxonomy> taxonomies = null != reseller.getTaxonomies() ?
            new HashSet<>(reseller.getTaxonomies()) : new HashSet<>();
        while (null != reseller.getParentId() && 0 < i(reseller.getParentId())) {
            // Traverse the admin path to get all taxonomies for the context
            reseller = getResellerById(i(reseller.getParentId()));
            if (null != reseller.getTaxonomies()) {
                taxonomies.addAll(reseller.getTaxonomies());
            }
        }
        return taxonomies;
    }

    ////////////////////////////////// HELPERS ///////////////////////////

    /**
     * Invalidates the reseller cache entry for given context identifier.
     *
     * @param contextId The context identifier
     * @throws OXException If invalidation fails
     */
    public void invalidate(int contextId) throws OXException {
        Cache<Integer> cache = getContextsCache();
        cache.invalidate(createContextKey(cache, contextId));
    }

    /**
     * Optionally retrieves the {@link ResellerAdmin} for the specified context.
     *
     * @param contextId the context identifier
     * @return The reseller administrator, {@link Optional#empty()} if none is defined
     * @throws OXException If an error is occurred
     */
    private Optional<ResellerAdmin> optReseller(int contextId) throws OXException {
        Integer resellerId = getResellerId(contextId);
        if (null == resellerId || NO_RESELLER.equals(resellerId)) {
            return Optional.empty();
        }
        return Optional.of(getResellerById(i(resellerId)));
    }

    /**
     * Gets the identifier of the reseller associated with given context.
     *
     * @param contextId The context identifier
     * @return The reseller identifier
     * @throws OXException If reseller identifier cannot be determined
     */
    private Integer getResellerId(int contextId) throws OXException {
        Cache<Integer> cache = getContextsCache();
        CacheKey key = createContextKey(cache, contextId);
        try {
            return cache.get(key, k -> Optional.ofNullable(delegate.optResellerAdmin(contextId, null)).map(ResellerAdmin::getId).orElse(NO_RESELLER));
        } catch (OXException e) {
            if (!Caches.isCacheException(e)) {
                throw e;
            }
            return Optional.ofNullable(delegate.optResellerAdmin(contextId, null)).map(ResellerAdmin::getId).orElse(NO_RESELLER);
        }
    }

    /**
     * Creates a new cache key for the given reseller identifier.
     *
     * @param cache The reseller administrator cache
     * @param resellerId The reseller identifier
     * @return The key
     */
    private static CacheKey createResellerKey(Cache<ResellerAdmin> cache, int resellerId) {
        return cache.newKey(String.valueOf(resellerId));
    }

    /**
     * Creates a new cache key for the given context identifier.
     *
     * @param cache The contexts cache
     * @param contextId The context identifier
     * @return The key
     */
    private static CacheKey createContextKey(Cache<Integer> cache, int contextId) {
        return cache.newKey(asHashPart(contextId));
    }

    /**
     * Gets the cache holding reseller administrators per reseller identifier.
     *
     * @return The cache holding reseller administrators per reseller identifier
     */
    private Cache<ResellerAdmin> getResellerCache() {
        return cacheService.getCache(CACHE_OPTIONS_RESELLER);
    }

    /**
     * Gets the the cache holding associating context identifiers with their corresponding reseller identifier.
     *
     * @return The cache holding associating context identifiers with their corresponding reseller identifier
     */
    private Cache<Integer> getContextsCache() {
        return cacheService.getCache(CACHE_OPTIONS_CONTEXTS);
    }

}
