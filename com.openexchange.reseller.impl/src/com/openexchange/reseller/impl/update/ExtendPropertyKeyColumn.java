/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.reseller.impl.update;

import java.sql.Connection;
import java.sql.SQLException;
import com.openexchange.database.Databases;
import com.openexchange.tools.update.Column;
import com.openexchange.tools.update.Tools;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.DatabaseConnection;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;

/**
 * {@link ExtendPropertyKeyColumn} extends the propertyKey column to 256 characters from 64
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class ExtendPropertyKeyColumn implements CustomTaskChange {

    /**
     * Initializes a new {@link ExtendPropertyKeyColumn}.
     */
    public ExtendPropertyKeyColumn() {
        super();
    }

    @Override
    public String getConfirmationMessage() {
        return "Column \"propertyKey\" of table \"subadmin_config_properties\" successfully extended to VARCHAR(256)";
    }

    @Override
    public void setUp() throws SetupException {
        // Nothing
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        // Ignore
    }

    @Override
    public ValidationErrors validate(Database database) {
        return new ValidationErrors();
    }

    @Override
    public void execute(Database database) throws CustomChangeException {
        DatabaseConnection databaseConnection = database.getConnection();
        if (!(databaseConnection instanceof JdbcConnection)) {
            throw new CustomChangeException("Cannot get underlying connection because database connection is not of type " + JdbcConnection.class.getName() + ", but of type: " + databaseConnection.getClass().getName());
        }

        org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ExtendPropertyKeyColumn.class);
        Connection configDbCon = ((JdbcConnection) databaseConnection).getUnderlyingConnection();
        int rollback = 0;
        try {
            if (!Tools.columnExists(configDbCon, "subadmin_config_properties", "propertyKey")) {
                return;
            }

            if (Tools.isVARCHAR(configDbCon, "subadmin_config_properties", "propertyKey", 256)) {
                // Already set
                return;
            }

            configDbCon.setAutoCommit(false);
            rollback = 1;

            Tools.modifyColumns(configDbCon, "subadmin_config_properties", new Column("propertyKey", "VARCHAR(256) NOT NULL"));

            configDbCon.commit();
            rollback = 2;
        } catch (SQLException e) {
            logger.error("Failed to change column \"propertyKey\" of table \"subadmin_config_properties\"", e);
            throw new CustomChangeException("SQL error", e);
        } catch (RuntimeException e) {
            logger.error("Failed to change column \"propertyKey\" to table \"subadmin_config_properties\"", e);
            throw new CustomChangeException("Runtime error", e);
        } finally {
            if (rollback > 0) {
                if (rollback == 1) {
                    Databases.rollback(configDbCon);
                }
                Databases.autocommit(configDbCon);
            }
        }
    }

}
