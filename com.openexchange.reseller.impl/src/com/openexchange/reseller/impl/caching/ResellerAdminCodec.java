/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.reseller.impl.caching;

import static com.openexchange.java.Autoboxing.I;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;
import com.openexchange.reseller.data.ResellerAdmin;
import com.openexchange.reseller.data.ResellerAdmin.ResellerAdminBuilder;
import com.openexchange.reseller.data.ResellerCapability;
import com.openexchange.reseller.data.ResellerConfigProperty;
import com.openexchange.reseller.data.ResellerTaxonomy;
import com.openexchange.reseller.data.Restriction;

/**
 * {@link ResellerAdminCodec} is a codec for {@link ResellerAdmin}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 */
public class ResellerAdminCodec extends AbstractJSONObjectCacheValueCodec<ResellerAdmin> {

    private static final UUID CODEC_ID = UUID.fromString("f7fa9d97-0617-43a1-bd1c-176da11a5b5b");

    private static final String DISPLAY_NAME = "d";
    private static final String ID = "i";
    private static final String NAME = "n";
    private static final String PARENT_ID = "p";
    private static final String TAXONOMIES = "t";
    private static final String RESTRICTIONS = "r";
    private static final String CAPABILITIES = "c";
    private static final String CONFIGURATION = "o";
    private static final String SALT = "s";
    private static final String PASSWORD_MECH = "m";
    private static final String PASSWORD = "w";

    private static final String RESTRICTION_ID = "i";
    private static final String RESTRICTION_VALUE = "v";
    private static final String RESTRICTION_NAME = "n";

    private static final String CONFIG_KEY = "k";
    private static final String CONFIG_VALUE = "v";

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    // ------------------- Serializing from ResellerAdmin to json starts here --------------------

    @Override
    protected JSONObject writeJson(ResellerAdmin value) throws Exception {
        JSONObject result = new JSONObject();
        result.putOpt(DISPLAY_NAME, value.getDisplayname());
        result.put(ID, value.getId());
        result.putOpt(NAME, value.getName());
        result.put(PARENT_ID, value.getParentId());
        result.putOpt(PASSWORD, value.getPassword());
        result.putOpt(PASSWORD_MECH, value.getPasswordMech());
        result.putOpt(SALT, value.getSalt());
        result.putOpt(CONFIGURATION, writeConfiguration(value.getConfiguration()));
        result.putOpt(CAPABILITIES, writeCapabilities(value.getCapabilities()));
        result.putOpt(RESTRICTIONS, writeRestrictions(value.getRestrictions()));
        result.putOpt(TAXONOMIES, writeTaxonomies(value.getTaxonomies()));
        return result;
    }

    /**
     * Writes a set of taxonomies to a json array
     *
     * @param taxonomies The taxonomies to write
     * @return The json array
     */
    private static JSONArray writeTaxonomies(Set<ResellerTaxonomy> taxonomies) {
        if (taxonomies == null || taxonomies.isEmpty()) {
            return null;
        }
        JSONArray result = new JSONArray(taxonomies.size());
        for (ResellerTaxonomy taxonomy : taxonomies) {
            result.put(taxonomy.getTaxonomy());
        }
        return result;
    }

    /**
     * Writes the set of restriction to an json array
     *
     * @param restrictions The restrictions to write
     * @return The json array
     * @throws JSONException If serialization to JSON fails
     */
    private static JSONArray writeRestrictions(List<Restriction> restrictions) throws JSONException {
        if (restrictions == null || restrictions.isEmpty()) {
            return null;
        }
        JSONArray result = new JSONArray(restrictions.size());
        for (Restriction restriction : restrictions) {
            result.put(writeRestriction(restriction));
        }
        return result;
    }

    /**
     * Writes a restriction to a json object
     *
     * @param restriction The restriction to write
     * @return The json object
     * @throws JSONException If serialization to JSON fails
     */
    private static JSONObject writeRestriction(Restriction restriction) throws JSONException {
        JSONObject result = new JSONObject(3);
        result.putOpt(RESTRICTION_ID, restriction.getId());
        result.putOpt(RESTRICTION_VALUE, restriction.getValue());
        result.putOpt(RESTRICTION_NAME, restriction.getName());
        return result;
    }

    /**
     * Writes a set of {@link ResellerCapability}s to a json array
     *
     * @param capabilities The capabilities to write
     * @return The json array
     */
    private static JSONArray writeCapabilities(Set<ResellerCapability> capabilities) {
        if (capabilities == null || capabilities.isEmpty()) {
            return null;
        }
        JSONArray result = new JSONArray(capabilities.size());
        for (ResellerCapability cap : capabilities) {
            result.put(cap.getId());
        }
        return result;
    }

    /**
     * Writes a configuration map to a json object
     *
     * @param configuration The configuration to write
     * @return The json object
     * @throws JSONException If serialization to JSON fails
     */
    private static JSONObject writeConfiguration(Map<String, ResellerConfigProperty> configuration) throws JSONException {
        if (configuration == null || configuration.isEmpty()) {
            return null;
        }
        JSONObject result = new JSONObject(configuration.size());
        for (Entry<String, ResellerConfigProperty> entry : configuration.entrySet()) {
            result.put(entry.getKey(), writeProperty(entry.getValue()));
        }
        return result;
    }

    /**
     * Writes a {@link ResellerConfigProperty} to a json object
     *
     * @param property The property to write
     * @return The json object
     * @throws JSONException If serialization to JSON fails
     */
    private static JSONObject writeProperty(ResellerConfigProperty property) throws JSONException {
        JSONObject result = new JSONObject(2);
        result.put(CONFIG_KEY, property.getKey());
        result.put(CONFIG_VALUE, property.getValue());
        return result;
    }

    // ------------------- Parsing from json to ResellerAdmin starts here --------------------

    @Override
    protected ResellerAdmin parseJson(JSONObject json) throws Exception {
        int id = json.getInt(ID);
        ResellerAdminBuilder result = ResellerAdmin.builder()
            .displayname(json.optString(DISPLAY_NAME, null))
                            .id(I(id))
            .name(json.optString(NAME, null));

        int pId = json.optInt(PARENT_ID);
        if (pId > 0) {
            result.parentId(I(pId));
        }
        result.password(json.optString(PASSWORD, null))
              .passwordMech(json.optString(PASSWORD_MECH, null))
              .salt(json.optString(SALT, null))
              .configuration(parseConfiguration(json.optJSONObject(CONFIGURATION), id))
              .capabilities(parseCapabilities(json.optJSONArray(CAPABILITIES), id))
              .restrictions(parseRestrictions(json.optJSONArray(RESTRICTIONS)))
              .taxonomies(parseTaxonomies(json.optJSONArray(TAXONOMIES), id));
        return result.build();
    }

    /**
     * Parses a set of {@link ResellerTaxonomy}s from the given json array
     *
     * @param jsonArray The json array
     * @param resellerId The id of the reseller to parse the taxonomies for
     * @return The set
     * @throws JSONException If parsing fails
     */
    private static Set<ResellerTaxonomy> parseTaxonomies(JSONArray array, int resellerId) throws JSONException {
        if (array == null) {
            return Collections.emptySet();
        }
        int length = array.length();
        HashSet<ResellerTaxonomy> result = HashSet.newHashSet(length);
        for (int x = 0; x < length; x++) {
            result.add(new ResellerTaxonomy(array.getString(x), resellerId));
        }
        return result;
    }

    /**
     * Parses a list of restrictions from the given json array
     *
     * @param jsonArray The json array to parse
     * @return The list of restrictions
     * @throws JSONException If parsing fails
     */
    private static List<Restriction> parseRestrictions(JSONArray jsonArray) throws JSONException {
        if (jsonArray == null) {
            return Collections.emptyList();
        }
        int length = jsonArray.length();
        List<Restriction> result = new ArrayList<>(length);
        for (int x = 0; x < length; x++) {
            result.add(parseRestriction(jsonArray.getJSONObject(x)));
        }
        return result;
    }

    /**
     * Parses a {@link Restriction} from a json object
     *
     * @param json The json object to parse
     * @return The Restriction
     * @throws JSONException If parsing fails
     */
    private static Restriction parseRestriction(JSONObject json) throws JSONException {
        int id = json.getInt(RESTRICTION_ID);
        String name = json.optString(RESTRICTION_NAME, null);
        String value = json.optString(RESTRICTION_VALUE, null);
        return new Restriction(I(id), name, value);
    }

    /**
     * Parses a set of {@link ResellerCapability} from a json array
     *
     * @param jsonArray to parse
     * @param resellerId The id of the reseller to parse the capabilities for
     * @return The set of capabilities
     * @throws JSONException If parsing fails
     */
    private static Set<ResellerCapability> parseCapabilities(JSONArray jsonArray, int resellerId) throws JSONException {
        if (jsonArray == null) {
            return Collections.emptySet();
        }
        int length = jsonArray.length();
        Set<ResellerCapability> result = HashSet.newHashSet(length);
        for (int x = 0; x < length; x++) {
            result.add(new ResellerCapability(jsonArray.getString(x), resellerId));
        }
        return result;
    }


    /**
     * Parses a reseller configuration from the given json object
     *
     * @param jsonObject The json object to parse from
     * @param resellerId The id of the reseller to parse the configuration for
     * @return The configuration
     * @throws JSONException If parsing fails
     */
    private static Map<String, ResellerConfigProperty> parseConfiguration(JSONObject jsonObject, int resellerId) throws JSONException {
        if (jsonObject == null) {
            return Collections.emptyMap();
        }
        HashMap<String, ResellerConfigProperty> result = HashMap.newHashMap(jsonObject.length());
        for (String key : jsonObject.keySet()) {
            JSONObject propertyJson = jsonObject.getJSONObject(key);
            String configKey = propertyJson.optString(CONFIG_KEY, null);
            String configValue = propertyJson.optString(CONFIG_VALUE, null);
            result.put(key, new ResellerConfigProperty(configKey, configValue, resellerId));
        }
        return result;
    }

}
