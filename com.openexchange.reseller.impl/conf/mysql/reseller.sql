#@(#) Reseller tables for the Configuration Database

CREATE TABLE subadmin ( 
    sid INT4 UNSIGNED NOT NULL,
    pid INT4 UNSIGNED NOT NULL, 
    name VARCHAR(128) COLLATE utf8mb4_unicode_ci NOT NULL,
    displayName VARCHAR(128) COLLATE utf8mb4_unicode_ci NOT NULL,
    password VARCHAR(128) COLLATE utf8mb4_unicode_ci NOT NULL,
    passwordMech VARCHAR(32) COLLATE utf8mb4_unicode_ci NOT NULL,
    salt VARBINARY(128) DEFAULT NULL,
    CONSTRAINT name_unique UNIQUE (name),
    PRIMARY KEY (sid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE restrictions (
    rid INT4 UNSIGNED NOT NULL,
    name VARCHAR(128) COLLATE utf8mb4_unicode_ci NOT NULL,
    CONSTRAINT name_unique UNIQUE (name),
    PRIMARY KEY (rid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE subadmin_restrictions (
    sid INT4 UNSIGNED NOT NULL,
    rid INT4 UNSIGNED NOT NULL,
    value VARCHAR(128) COLLATE utf8mb4_unicode_ci NOT NULL,
    CONSTRAINT sid_rid_unique UNIQUE (sid,rid),
    FOREIGN KEY(sid) REFERENCES subadmin(sid),
    FOREIGN KEY(rid) REFERENCES restrictions(rid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE
    context_restrictions (
    cid INT4 UNSIGNED NOT NULL,
    rid INT4 UNSIGNED NOT NULL,
    value VARCHAR(128) COLLATE utf8mb4_unicode_ci NOT NULL,
    CONSTRAINT cid_rid_unique UNIQUE (cid,rid),
    FOREIGN KEY(cid) REFERENCES context(cid),
    FOREIGN KEY(rid) REFERENCES restrictions(rid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE context2subadmin (
    cid INT4 UNSIGNED NOT NULL,
    sid INT4 UNSIGNED NOT NULL,
    PRIMARY KEY (cid,sid),
    INDEX (sid),
    UNIQUE (cid),
    FOREIGN KEY(sid) REFERENCES subadmin(sid),
    FOREIGN KEY(cid) REFERENCES context(cid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE context_customfields (
    cid INT4 UNSIGNED NOT NULL,
    customid VARCHAR(128) COLLATE utf8mb4_unicode_ci,
    createTimestamp INT8 NOT NULL,
    modifyTimestamp INT8 NOT NULL,
    CONSTRAINT cid_unique UNIQUE (cid),
    FOREIGN KEY(cid) REFERENCES context(cid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE subadmin_config_properties (
    sid INT4 UNSIGNED NOT NULL,
    propertyKey VARCHAR(256) CHARACTER SET latin1 NOT NULL DEFAULT '',
    propertyValue TEXT CHARACTER SET latin1 NOT NULL DEFAULT '',
    PRIMARY KEY (sid, propertyKey)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE subadmin_capabilities (
    sid INT4 UNSIGNED NOT NULL,
    capability VARCHAR(64) CHARACTER SET latin1 NOT NULL DEFAULT '',
    PRIMARY KEY (sid, capability)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE subadmin_taxonomies (
    sid INT4 UNSIGNED NOT NULL,
    taxonomy VARCHAR(64) CHARACTER SET latin1 NOT NULL DEFAULT '',
    PRIMARY KEY (sid, taxonomy)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
