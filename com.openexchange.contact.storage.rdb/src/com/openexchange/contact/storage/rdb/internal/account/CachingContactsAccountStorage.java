/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.contact.storage.rdb.internal.account;

import static com.openexchange.cache.v2.CacheKeys.asHashPart;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.contact.common.ContactsAccount;
import com.openexchange.contact.common.DefaultContactsAccount;
import com.openexchange.contact.storage.ContactsAccountStorage;
import com.openexchange.database.provider.DBTransactionPolicy;
import com.openexchange.exception.OXException;

/**
 * {@link CachingContactsAccountStorage}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @since v7.10.5
 */
public class CachingContactsAccountStorage implements ContactsAccountStorage {

    /** Cache options for contacts accounts */
    private static final CacheOptions<Map<String, List<ContactsAccountData>>> OPTIONS_ACCOUNT = CacheOptions.<Map<String, List<ContactsAccountData>>> builder() // @formatter:off
        .withCoreModuleName(CoreModuleName.CONTACTS_ACCOUNT)
        .withCodecAndVersion(new ContactsAccountCodec())
    .build(); // formatter:on

    private final RdbContactsAccountStorage delegate;
    private final int contextId;
    private final Cache<Map<String, List<ContactsAccountData>>> accountsCache;

    /**
     * Initialises a new {@link CachingContactsAccountStorage}.
     *
     * @param delegate The underlying persistent account storage
     * @param contextId The context identifier
     * @param cacheService A reference to the cache service
     */
    public CachingContactsAccountStorage(RdbContactsAccountStorage delegate, int contextId, CacheService cacheService) {
        super();
        this.delegate = delegate;
        this.contextId = contextId;
        accountsCache = cacheService.getCache(OPTIONS_ACCOUNT);
    }

    @Override
    public int nextId() throws OXException {
        return delegate.nextId();
    }

    @Override
    public void insertAccount(ContactsAccount account) throws OXException {
        delegate.insertAccount(account);
        invalidateAccount(account.getUserId(), -1);
    }

    @Override
    public void updateAccount(ContactsAccount account, long clientTimestamp) throws OXException {
        delegate.updateAccount(account, clientTimestamp);
        invalidateAccount(account.getUserId(), account.getAccountId());
    }

    @Override
    public void deleteAccount(int userId, int accountId, long clientTimestamp) throws OXException {
        delegate.deleteAccount(userId, accountId, clientTimestamp);
        invalidateAccount(userId, accountId);
    }

    @Override
    public ContactsAccount loadAccount(int userId, int accountId) throws OXException {
        if (bypassCache()) {
            return delegate.loadAccount(userId, accountId);
        }
        return lookupAccount(userId, accountId, getAccountDataPerProviderId(userId));
    }

    @Override
    public ContactsAccount[] loadAccounts(int userId, int[] accountIds) throws OXException {
        if (null == accountIds || bypassCache()) {
            return delegate.loadAccounts(userId, accountIds);
        }
        Map<String, List<ContactsAccountData>> accountDataPerProviderId = getAccountDataPerProviderId(userId);
        ContactsAccount[] accounts = new ContactsAccount[accountIds.length];
        for (int i = 0; i < accountIds.length; i++) {
            accounts[i] = lookupAccount(userId, accountIds[i], accountDataPerProviderId);
        }
        return accounts;
    }

    @Override
    public List<ContactsAccount> loadAccounts(int userId) throws OXException {
        if (bypassCache()) {
            return delegate.loadAccounts(userId);
        }
        return loadAccounts(userId, (String[]) null);
    }

    @Override
    public List<ContactsAccount> loadAccounts(int userId, String... providerIds) throws OXException {
        if (bypassCache()) {
            return delegate.loadAccounts(userId, providerIds);
        }
        Map<String, List<ContactsAccountData>> accountsPerProviderId = getAccountDataPerProviderId(userId);
        if (null == providerIds) {
            return accountsPerProviderId.entrySet().stream().map(e -> toAccounts(userId, e.getKey(), e.getValue())).flatMap(List::stream).collect(Collectors.toList());
        }
        return Stream.of(providerIds).map(p -> toAccounts(userId, p, accountsPerProviderId.get(p))).filter(Objects::nonNull).flatMap(List::stream).collect(Collectors.toList());
    }

    @Override
    public void invalidateAccount(int userId, int accountId) throws OXException {
        invalidateAccounts(userId);
    }

    private void invalidateAccounts(int userId) throws OXException {
        accountsCache.invalidate(getAccountsKey(userId));
    }

    private CacheKey getAccountsKey(int userId) {
        return accountsCache.newKey(asHashPart(contextId), Integer.toString(userId));
    }

    /**
     * Checks whether the delegates policy is set to transaction
     *
     * @return <code>true</code> if the cache should be bypassed, i.e. if the delegate is not in transaction mode, <code>false</code> otherwise
     */
    private boolean bypassCache() {
        return DBTransactionPolicy.NO_TRANSACTIONS.equals(delegate.getTransactionPolicy());
    }

    /**
     * Gets cached or loads all contacts account data of a certain user from the storage.
     *
     * @param userId The identifier of the user to get the account data for
     * @return The contacts account data, mapped by provider identifiers
     * @throws OXException If loading data fails
     */
    private Map<String, List<ContactsAccountData>> getAccountDataPerProviderId(int userId) throws OXException {
        return accountsCache.get(getAccountsKey(userId), k -> loadAccountDataPerProviderId(userId));
    }

    /**
     * Loads all contacts account data of a certain user from the storage.
     *
     * @param userId The identifier of the user to load the account data for
     * @return The loaded contacts account data, mapped by provider identifiers
     * @throws OXException If loading data fails
     */
    private Map<String, List<ContactsAccountData>> loadAccountDataPerProviderId(int userId) throws OXException {
        Map<String, List<ContactsAccountData>> accountsPerProviderId = new HashMap<String, List<ContactsAccountData>>();
        for (ContactsAccount account : delegate.loadAccounts(userId)) {
            com.openexchange.tools.arrays.Collections.put(accountsPerProviderId, account.getProviderId(), toAccountData(account));
        }
        return accountsPerProviderId;
    }

    /**
     * Initializes a new contacts account data based on the given contacts account.
     *
     * @param account The account to convert
     * @return The contacts account data
     */
    private static ContactsAccountData toAccountData(ContactsAccount account) {
        long lastModified = null == account.getLastModified() ? -1L : account.getLastModified().getTime();
        return new ContactsAccountData(account.getAccountId(), lastModified, account.getInternalConfiguration(), account.getUserConfiguration());
    }

    /**
     * Converts contacts account data to a contacts account object of a specific provider.
     *
     * @param userId The identifier of the user to apply
     * @param providerId The identifier of the contacts provider to set in the account
     * @param accountData The account data to convert
     * @return The converted contacts account
     */
    private static ContactsAccount toAccount(int userId, String providerId, ContactsAccountData accountData) {
        Date lastModified = 0 < accountData.lastModified() ? new Date(accountData.lastModified()) : null;
        return new DefaultContactsAccount(providerId, accountData.id(), userId, accountData.internalConfig(), accountData.userConfig(), lastModified);
    }

    /**
     * Converts a list of contacts account data to contacts account objects of a specific provider.
     *
     * @param userId The identifier of the user to apply
     * @param providerId The identifier of the contacts provider to set in the accounts
     * @param accountDataList The account data list to convert
     * @return The converted contacts accounts
     */
    private static List<ContactsAccount> toAccounts(int userId, String providerId, List<ContactsAccountData> accountDataList) {
        if (null == accountDataList) {
            return null;
        }
        if (accountDataList.isEmpty()) {
            return Collections.emptyList();
        }
        return accountDataList.stream().map(d -> toAccount(userId, providerId, d)).toList();
    }

    /**
     * Lookups a specific account by its identifier from the supplied map of accounts per provider.
     *
     * @param userId The identifier of the user
     * @param accountId The identifier of the account to lookup
     * @param accountDataPerProviderId The account data to lookup the account in
     * @return The contacts account, or <code>null</code> if not found
     */
    private static ContactsAccount lookupAccount(int userId, int accountId, Map<String, List<ContactsAccountData>> accountDataPerProviderId) {
        if (null == accountDataPerProviderId || accountDataPerProviderId.isEmpty()) {
            return null;
        }
        for (Entry<String, List<ContactsAccountData>> entry : accountDataPerProviderId.entrySet()) {
            for (ContactsAccountData accountData : entry.getValue()) {
                if (accountId == accountData.id()) {
                    return toAccount(userId, entry.getKey(), accountData);
                }
            }
        }
        return null;
    }

}
