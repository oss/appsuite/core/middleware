/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.contact.storage.rdb.sql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.openexchange.contact.storage.rdb.mapping.Mappers;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contact.helpers.ContactField;
import com.openexchange.groupware.container.Contact;

/**
 * {@link ContactReader}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class ContactReader {

    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(ContactReader.class);

    /**
     * Initializes a new {@link ContactReader}.
     */
    private ContactReader() {
        super();
    }

    /**
     * Deserializes all contacts found in the result set, using the supplied fields.
     *
     * @param resultSet The result set to read contacts from
     * @param fields The contact fields to read
     * @param withObjectUseCount ResultSet contains additional data for sorting contacts
     * @return The contacts, or an empty list if no there were no results
     * @throws SQLException
     * @throws OXException
     */
    public static List<Contact> readContacts(ResultSet resultSet, ContactField[] fields, boolean withObjectUseCount) throws SQLException, OXException {
        List<Contact> contacts = new ArrayList<Contact>();
        while (resultSet.next()) {
            contacts.add(readNext(resultSet, fields, withObjectUseCount));
        }
        return contacts;
    }

    /**
     * Deserializes the first contact found in the result set, using the supplied fields.
     *
     * @param resultSet The result set to read contacts from
     * @param fields The contact fields to read
     * @param withObjectUseCount ResultSet contains additional data for sorting contacts
     * @return The contact, or <code>null</code> if there was no result
     * @throws SQLException
     * @throws OXException
     */
    public static Contact readContact(ResultSet resultSet, ContactField[] fields, boolean withObjectUseCount) throws SQLException, OXException {
        return resultSet.next() ? readNext(resultSet, fields, withObjectUseCount) : null;
    }

    private static Contact readNext(ResultSet resultSet, ContactField[] fields, boolean withObjectUseCount) throws SQLException, OXException {
        Contact contact = Mappers.CONTACT.fromResultSet(resultSet, fields);
        if (withObjectUseCount) {
            try {
                contact.setUseCount(resultSet.getInt("value"));
            } catch (SQLException e) {
                String query = Databases.getSqlStatement(resultSet.getStatement(), null);
                LOGGER.warn("Failed to determine use-count information from {}", null == query ? "<unknown>" : query, e);
            }
        }
        return contact;
    }

}
