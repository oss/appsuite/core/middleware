/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.contact.storage.rdb.search;

import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import com.openexchange.exception.OXException;
import com.openexchange.java.SimpleTokenizer;

/**
 * {@link MWB1052Test}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @since 7.10.6
 */
public class MWB1052Test {

    private static Stream<Character> characterProvider() {
        return Stream.of('+', '\\', '-', '>', '<', '(', ')', '~', '*', '\"', '@');
    }

    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("characterProvider")
    public void testTokenizeQuery(Character nonWordChar) throws OXException {
        String query = nonWordChar + "x";
        String expectedPattern = "x*";
        List<String> patterns = FulltextAutocompleteAdapter.preparePatterns(SimpleTokenizer.tokenize(query));
        Assertions.assertEquals(1, patterns.size(), "unexpected length of tokenized patterns");
        Assertions.assertEquals(expectedPattern, patterns.get(0), "unexpected tokenized pattern");
    }
}
