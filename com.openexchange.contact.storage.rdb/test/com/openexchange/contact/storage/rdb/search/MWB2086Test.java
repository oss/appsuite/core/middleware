/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.contact.storage.rdb.search;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import com.openexchange.java.SimpleTokenizer;

/**
 * {@link MWB2086Test}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public class MWB2086Test {

    @ParameterizedTest(name = "{index}: {0}")
    @ValueSource(strings = {"xxx'", "x'xx", "'xxx", "xxx''", "'''xxx", "x'x'x''x"})
    public void testEscapeSingleQuotes(String prefix) {
        List<String> patterns = FulltextAutocompleteAdapter.preparePatterns(SimpleTokenizer.tokenize(prefix));
        assertEquals(1, patterns.size(), "unexpected length of tokenized patterns");
        String preparedPattern = patterns.get(0);
        for (int i = 0; i < preparedPattern.length(); i++) {
            char c = preparedPattern.charAt(i);
            if ('\'' == c) {
                assertTrue(0 < i);
                assertEquals('\\', preparedPattern.charAt(i - 1));
            }
        }
    }
}
