/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.database.cleanup.impl.storage.caching;

import java.io.Writer;
import java.util.UUID;
import org.json.JSONObject;
import org.json.SimpleJSONSerializer;
import com.openexchange.cache.v2.codec.json.AbstractSimpleJSONObjectCacheValueCodec;
import com.openexchange.database.cleanup.impl.storage.ExecutionInfo;
import com.openexchange.java.util.UUIDs;

/**
 * {@link ExecutionInfoCodec} - The codec for a clean-up job's execution information.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class ExecutionInfoCodec extends AbstractSimpleJSONObjectCacheValueCodec<ExecutionInfo> {

    private static final UUID ID = UUID.fromString("c6a7b753-2440-467b-aa45-33aff714a5db");

    @Override
    public UUID getCodecId() {
        return ID;
    }

    @Override
    protected ExecutionInfo parseJson(JSONObject jObject) throws Exception {
        long timeStamp = jObject.getLong("t");
        boolean running = jObject.getBoolean("r");
        String sClaim = jObject.optString("c", null);
        return new ExecutionInfo(timeStamp, running, sClaim == null ? null : UUIDs.fromUnformattedString(sClaim));
    }

    @Override
    protected void writeJson(ExecutionInfo value, Writer writer) throws Exception {
        writer.write('{');

        SimpleJSONSerializer.writeJsonValue("t", writer);
        writer.write(':');
        SimpleJSONSerializer.writeJsonValue(value.getTimeStamp(), writer);

        writer.write(',');
        SimpleJSONSerializer.writeJsonValue("r", writer);
        writer.write(':');
        SimpleJSONSerializer.writeJsonValue(value.isRunning(), writer);

        if (value.getClaim() != null) {
            writer.write(',');
            SimpleJSONSerializer.writeJsonValue("c", writer);
            writer.write(':');
            SimpleJSONSerializer.writeJsonValue(UUIDs.getUnformattedString(value.getClaim()), writer);
        }

        writer.write('}');
    }

}
