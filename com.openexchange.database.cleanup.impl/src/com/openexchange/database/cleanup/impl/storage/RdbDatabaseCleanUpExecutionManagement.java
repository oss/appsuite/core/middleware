/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.database.cleanup.impl.storage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheKey;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.codec.Codecs;
import com.openexchange.cache.v2.core.CoreModuleName;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.database.cleanup.CleanUpJob;
import com.openexchange.database.cleanup.CleanUpJobId;
import com.openexchange.database.cleanup.CleanUpJobType;
import com.openexchange.database.cleanup.DatabaseCleanUpExceptionCode;
import com.openexchange.database.cleanup.impl.DatabaseCleanUpServiceImpl;
import com.openexchange.database.cleanup.impl.storage.caching.ExecutionInfoCodec;
import com.openexchange.exception.OXException;
import com.openexchange.java.ExceptionCatchingRunnable;
import com.openexchange.java.Reference;
import com.openexchange.java.util.UUIDs;
import com.openexchange.server.ServiceLookup;


/**
 * {@link RdbDatabaseCleanUpExecutionManagement}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.10.6
 */
public class RdbDatabaseCleanUpExecutionManagement implements DatabaseCleanUpExecutionManagement {

    private static final CacheOptions<ExecutionInfo> OPTIONS = CacheOptions.<ExecutionInfo> builder()
        .withCoreModuleName(CoreModuleName.DB_CLEANUP)
        .withCodecAndVersion(new ExecutionInfoCodec())
        .build();

    private final ServiceLookup services;

    /**
     * Initializes a new {@link RdbDatabaseCleanUpExecutionManagement}.
     *
     * @param services The service look-up
     */
    public RdbDatabaseCleanUpExecutionManagement(ServiceLookup services) {
        super();
        this.services = services;
    }

    private Cache<ExecutionInfo> optCache() {
        CacheService cacheService = services.getOptionalService(CacheService.class);
        return cacheService == null ? null : cacheService.getCache(OPTIONS);
    }

    private static CacheKey newCacheKey(Cache<ExecutionInfo> cache, CleanUpJob job, String schema) {
        return cache.newKey(schema, compressJobId(job.getId()));
    }

    @Override
    public void markExecutionDone(CleanUpJob job, UUID claim, int representativeContextId, String schema) throws OXException {
        if (job.isRunsExclusive() == false && job.getType() != CleanUpJobType.GENERAL) {
            return;
        }

        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        boolean modified = false;
        Connection writeCon = databaseService.getWritable(representativeContextId);
        try {
            modified = doMarkExecutionDone(job, claim,schema, writeCon);
        } finally {
            if (modified) {
                databaseService.backWritable(representativeContextId, writeCon);
            } else {
                databaseService.backWritableAfterReading(representativeContextId, writeCon);
            }
        }
    }

    private boolean doMarkExecutionDone(CleanUpJob job, UUID claim, String schema, Connection writeCon) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = writeCon.prepareStatement("UPDATE `cleanupJobExecution` SET `timestamp` = ?, `running` = 0, `claim` = NULL WHERE `id` = ? AND `claim` IS NOT NULL AND `claim` = ?");
            long timeStamp = System.currentTimeMillis();
            stmt.setLong(1, timeStamp);
            stmt.setString(2, job.getId().getIdentifier());
            stmt.setBytes(3, UUIDs.toByteArray(claim));
            boolean updated = stmt.executeUpdate() > 0;
            if (updated) {
                Cache<ExecutionInfo> cache = optCache();
                if (cache != null) {
                    cache.put(newCacheKey(cache, job, schema), new ExecutionInfo(timeStamp, false, (UUID) null));
                }
            }
            return updated;
        } catch (SQLException e) {
            throw DatabaseCleanUpExceptionCode.SQL_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw DatabaseCleanUpExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    @Override
    public boolean refreshTimeStamp(CleanUpJob job, UUID claim, int representativeContextId) throws OXException {
        if (job.isRunsExclusive() == false && job.getType() != CleanUpJobType.GENERAL) {
            return false;
        }

        DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
        boolean modified = false;
        Connection writeCon = databaseService.getWritable(representativeContextId);
        try {
            modified = doRefreshTimeStamp(job, claim, writeCon);
            return modified;
        } finally {
            if (modified) {
                databaseService.backWritable(representativeContextId, writeCon);
            } else {
                databaseService.backWritableAfterReading(representativeContextId, writeCon);
            }
        }
    }

    private static boolean doRefreshTimeStamp(CleanUpJob job, UUID claim, Connection writeCon) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = writeCon.prepareStatement("UPDATE `cleanupJobExecution` SET `timestamp` = ? WHERE `id` = ? AND `running` = 1 AND `claim` IS NOT NULL AND `claim` = ?");
            stmt.setLong(1, System.currentTimeMillis());
            stmt.setString(2, job.getId().getIdentifier());
            stmt.setBytes(3, UUIDs.toByteArray(claim));
            return stmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw DatabaseCleanUpExceptionCode.SQL_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw DatabaseCleanUpExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    @Override
    public Optional<UUID> checkExecutionPermission(CleanUpJob job, int representativeContextId, String schema) throws OXException {
        if (job.isNotRunsExclusive() && job.getType() != CleanUpJobType.GENERAL) {
            return Optional.of(UUID.randomUUID());
        }

        // Check cache first for not running and not yet due
        Cache<ExecutionInfo> cache = optCache();
        if (cache != null) {
            CacheKey cacheKey = newCacheKey(cache, job, schema);
            ExecutionInfo executionInfo = cache.get(cacheKey);
            if (executionInfo != null && executionInfo.isNotRunning()) {
                long now = System.currentTimeMillis();
                long numberOfMillisSinceLastRun = numberOfMillisSinceLastRun(executionInfo, now);
                if (numberOfMillisSinceLastRun < job.getDelay().toMillis()) {
                    // Not yet due
                    return Optional.empty();
                }

                // Drop from cache and proceed
                cache.invalidate(cacheKey);
            }
        }

        Reference<Runnable> pendingTaskReference = new Reference<>();
        try {
            DatabaseService databaseService = services.getServiceSafe(DatabaseService.class);
            ConnectionProvider connectionProvider = null;
            Connection connection = databaseService.getReadOnly(representativeContextId);
            try {
                connectionProvider = new ConnectionProvider(connection, representativeContextId, databaseService);
                connection = null;
                Optional<UUID> optClaim = doCheckExecutionPermission(job, schema, connectionProvider, cache, pendingTaskReference);
                if (optClaim.isPresent()) {
                    connectionProvider.markModified();
                }
                return optClaim;
            } finally {
                if (connectionProvider != null) {
                    connectionProvider.closeConnection();
                }
                if (connection != null) {
                    databaseService.backReadOnly(representativeContextId, connection);
                }
            }
        } finally {
            Runnable pendingTask = pendingTaskReference.getValue();
            if (pendingTask != null) {
                try {
                    pendingTask.run();
                } catch (Exception e) {
                    // Ignore
                }
            }
        }
    }

    private static Optional<UUID> doCheckExecutionPermission(CleanUpJob job, String schema, ConnectionProvider connectionProvider, Cache<ExecutionInfo> cache, Reference<Runnable> pendingTaskReference) throws OXException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connectionProvider.getConnection().prepareStatement("SELECT `timestamp`, `running`, `claim` FROM `cleanupJobExecution` WHERE `id` = ?");
            stmt.setString(1, job.getId().getIdentifier());
            rs = stmt.executeQuery();
            ExecutionInfo executionInfo = rs.next() ? new ExecutionInfo(rs.getLong(1), rs.getInt(2) > 0, rs.getBytes(3)) : null;
            Databases.closeSQLStuff(rs, stmt);
            rs = null;
            stmt = null;

            UUID newClaim = UUID.randomUUID();
            if (executionInfo == null) {
                // Not present
                connectionProvider.switchToReadWrite();
                stmt = connectionProvider.getConnection().prepareStatement("INSERT INTO `cleanupJobExecution` (`id`, `timestamp`, `running`, `claim`) VALUES (?, ?, 1, ?)");
                stmt.setString(1, job.getId().getIdentifier());
                stmt.setLong(2, System.currentTimeMillis());
                stmt.setBytes(3, UUIDs.toByteArray(newClaim));
                try {
                    stmt.executeUpdate();
                    return Optional.of(newClaim);
                } catch (SQLException e) {
                    if (Databases.isPrimaryKeyConflictInMySQL(e)) {
                        return Optional.empty();
                    }
                    throw e;
                }
            }

            // Is present
            long now = System.currentTimeMillis();
            long numberOfMillisSinceLastRun = numberOfMillisSinceLastRun(executionInfo, now);
            boolean expired = false;
            if (executionInfo.isRunning()) {
                // According to "running" flag another process is still running an execution. Check if considered as expired...
                if (numberOfMillisSinceLastRun <= getExpirationDurationMillis()) {
                    // NOT expired. Apparently another process is running an execution.
                    return Optional.empty();
                }

                // Consider as expired
                expired = true;
            }

            if (expired == false && numberOfMillisSinceLastRun < job.getDelay().toMillis()) {
                // Another process in cluster already performed the clean-up execution and delay has not yet expired
                if (cache != null) {
                    ExceptionCatchingRunnable task = () -> cache.put(newCacheKey(cache, job, schema), executionInfo);
                    pendingTaskReference.setValue(task);
                }
                return Optional.empty();
            }

            // Either not running or expired. Try to update (compare-and-set)
            connectionProvider.switchToReadWrite();
            if (expired) {
                stmt = connectionProvider.getConnection().prepareStatement("UPDATE `cleanupJobExecution` SET `timestamp` = ?, `running` = 1, `claim` = ? WHERE `id` = ? AND `timestamp` = ?");
                stmt.setLong(1, now);
                stmt.setBytes(2, UUIDs.toByteArray(newClaim));
                stmt.setString(3, job.getId().getIdentifier());
                stmt.setLong(4, executionInfo.getTimeStamp());
            } else {
                if (executionInfo.getClaim() == null) {
                    stmt = connectionProvider.getConnection().prepareStatement("UPDATE `cleanupJobExecution` SET `timestamp` = ?, `running` = 1, `claim` = ? WHERE `id` = ? AND `timestamp` = ? AND `running` = 0 AND `claim` IS NULL");
                    stmt.setLong(1, now);
                    stmt.setBytes(2, UUIDs.toByteArray(newClaim));
                    stmt.setString(3, job.getId().getIdentifier());
                    stmt.setLong(4, executionInfo.getTimeStamp());
                } else {
                    stmt = connectionProvider.getConnection().prepareStatement("UPDATE `cleanupJobExecution` SET `timestamp` = ?, `running` = 1, `claim` = ? WHERE `id` = ? AND `timestamp` = ? AND `running` = 0 AND `claim` IS NOT NULL AND `claim` = ?");
                    stmt.setLong(1, now);
                    stmt.setBytes(2, UUIDs.toByteArray(newClaim));
                    stmt.setString(3, job.getId().getIdentifier());
                    stmt.setLong(4, executionInfo.getTimeStamp());
                    stmt.setBytes(5, UUIDs.toByteArray(executionInfo.getClaim()));
                }
            }
            boolean updated = stmt.executeUpdate() > 0;
            Databases.closeSQLStuff(stmt);
            stmt = null;
            if (updated && cache != null) {
                ExceptionCatchingRunnable task = () -> cache.invalidate(newCacheKey(cache, job, schema));
                pendingTaskReference.setValue(task);
            }
            return updated ? Optional.of(newClaim) : Optional.empty();
        } catch (SQLException e) {
            throw DatabaseCleanUpExceptionCode.SQL_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw DatabaseCleanUpExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(rs, stmt);
        }
    }

    private static long getExpirationDurationMillis() {
        return DatabaseCleanUpServiceImpl.EXPIRATION_MILLIS;
    }

    private static long numberOfMillisSinceLastRun(ExecutionInfo executionInfo, long now) {
        return now - executionInfo.getTimeStamp();
    }

    // -------------------------------------------------------------------------------------------------------------------------------------

    private static enum ConnectionUsage {
        /**
         * A read-only connection was used exclusively.
         */
        READ_ONLY,
        /**
         * A read-write connection was used, but nothing modified.
         */
        UNMODIFIED,
        /**
         * A read-write connection was used and data has been modified.
         */
        MODIFIED;
    }

    private static class ConnectionProvider {

        private final DatabaseService databaseService;
        private final int representativeContextId;
        private Connection connection;
        private ConnectionUsage connectionUsage;

        /**
         * Initializes a new {@link ConnectionProvider}.
         *
         * @param connection The read-only connection
         * @param representativeContextId The representative context identifier
         * @param databaseService The database service
         */
        ConnectionProvider(Connection connection, int representativeContextId, DatabaseService databaseService) {
            super();
            this.connection = connection;
            this.representativeContextId = representativeContextId;
            this.databaseService = databaseService;
            this.connectionUsage = ConnectionUsage.READ_ONLY;
        }

        private void checkConnection() {
            if (connection == null) {
                throw new IllegalStateException("Connection already closed");
            }
        }

        Connection getConnection() {
            checkConnection();
            return connection;
        }

        void switchToReadWrite() throws OXException {
            checkConnection();
            if (connectionUsage == ConnectionUsage.READ_ONLY) {
                databaseService.backReadOnly(representativeContextId, connection);
                connection = null;
                connection = databaseService.getWritable(representativeContextId);
                connectionUsage = ConnectionUsage.UNMODIFIED;
            }
        }

        void markModified() {
            checkConnection();
            if (connectionUsage == ConnectionUsage.UNMODIFIED) {
                connectionUsage = ConnectionUsage.MODIFIED;
            }
        }

        void closeConnection() {
            if (connection != null) {
                switch (connectionUsage) {
                    case MODIFIED:
                        databaseService.backWritable(representativeContextId, connection);
                        break;
                    case READ_ONLY:
                        databaseService.backReadOnly(representativeContextId, connection);
                        break;
                    case UNMODIFIED:
                        databaseService.backWritableAfterReading(representativeContextId, connection);
                        break;
                    default:
                        break;
                }
                connection = null;
            }
        }
    }

    // -------------------------------------------------- Compress stuff -------------------------------------------------------------------

    /**
     * A list of replacements
     */
    private static final List<String[]> JOB_ID_REPLACEMENTS = List.of(
        new String[] {"com.openexchange.", "^p"},
        new String[] {"CleanUpTask", "^Z"},
        new String[] {"CleanupTask", "^z"},
        new String[] {"CleanUpExecution", "^E"},
        new String[] {"CleanupExecution", "^e"},
        new String[] {"Task", "^t"},
        new String[] {"CleanUp", "^C"},
        new String[] {"Cleanup", "^c"},
        new String[] {"cleanup", "^u"},
        new String[] {"Cleaner", "^l"},
        new String[] {"Execution", "^e"},
        new String[] {"Worker", "^w"}
    );

    /**
     * Compresses the specified clean-up job identifier.
     *
     * @param id The clean-up job identifier to compress
     * @return The compressed clean-up job identifier
     */
    private static String compressJobId(CleanUpJobId id) {
        return id == null ? null : Codecs.replace(id.getIdentifier(), JOB_ID_REPLACEMENTS);
    }

}
