/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.database.cleanup.impl.storage;

import java.util.UUID;
import com.openexchange.java.util.UUIDs;

/**
 * {@link ExecutionInfo} - A simple helper to carry the basic execution information for a database clean-up job.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.x
 */
public class ExecutionInfo {

    /** The time stamp when last execution was finished/started */
    private final long timeStamp;

    /** Whether execution is still running */
    private final boolean running;

    /** The existent claim */
    private final UUID claim;

    /** The calculated hash code */
    private int hash = 0; // NOSONARLINT

    /**
     * Initializes a new {@link ExecutionInfo}.
     *
     * @param timeStamp The time stamp when last execution was finished/started
     * @param running Whether execution is still running
     * @param claim The existent claim
     */
    public ExecutionInfo(long timeStamp, boolean running, byte[] claim) {
        super();
        this.timeStamp = timeStamp;
        this.running = running;
        this.claim = claim == null ? null : UUIDs.toUUID(claim);
    }

    /**
     * Initializes a new {@link ExecutionInfo}.
     *
     * @param timeStamp The time stamp when last execution was finished/started
     * @param running Whether execution is still running
     * @param claim The existent claim
     */
    public ExecutionInfo(long timeStamp, boolean running, UUID claim) {
        super();
        this.timeStamp = timeStamp;
        this.running = running;
        this.claim = claim;
    }

    /**
     * Gets the time stamp when last execution was finished/started.
     *
     * @return The time stamp when last execution was finished/started
     */
    public long getTimeStamp() {
        return timeStamp;
    }

    /**
     * Gets the flag whether execution is still running.
     *
     * @return <code>true</code> if execution is still running; otherwise <code>false</code>
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Gets the flag whether execution is <b>not</b> running.
     *
     * @return <code>true</code> if execution is <b>not</b> running; otherwise <code>false</code> if running
     */
    public boolean isNotRunning() {
        return !running;
    }

    /**
     * Gets the existent claim.
     *
     * @return The existent claim or <code>null</code>
     */
    public UUID getClaim() {
        return claim;
    }


    @Override
    public int hashCode() {
        int result = hash;
        if (result == 0) {
            int prime = 31;
            result = 1;
            result = prime * result + (running ? 1231 : 1237);
            result = prime * result + (int) (timeStamp ^ (timeStamp >>> 32));
            result = prime * result + ((claim == null) ? 0 : claim.hashCode());
            hash = result;
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ExecutionInfo other = (ExecutionInfo) obj;
        if (running != other.running) {
            return false;
        }
        if (timeStamp != other.timeStamp) {
            return false;
        }
        if (claim == null) {
            if (other.claim != null) {
                return false;
            }
        } else if (!claim.equals(other.claim)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        sb.append("[timeStamp=").append(timeStamp).append(", running=").append(running).append(", ");
        sb.append("claim=").append(claim == null ? "null" : UUIDs.getUnformattedString(claim));
        sb.append(']');
        return sb.toString();
    }

}
