/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.keystore.k8s.osgi;

import com.openexchange.config.ConfigurationService;
import com.openexchange.keystore.KeyStoreService;
import com.openexchange.keystore.k8s.impl.CachingKeyStoreServiceImpl;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link Activator}
 *
 * @author <a href="mailto:kevin.ruthmann@open-xchange.com">Kevin Ruthmann</a>
 * @since v8.0.0
 */
public class Activator extends HousekeepingActivator {

    private CachingKeyStoreServiceImpl keyStoreService;

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { ConfigurationService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        boolean enabled = getService(ConfigurationService.class).getBoolProperty("com.openexchange.keystore.k8s.enabled", true);
        CachingKeyStoreServiceImpl keyStoreService = new CachingKeyStoreServiceImpl(enabled);
        this.keyStoreService = keyStoreService;
        registerService(KeyStoreService.class, keyStoreService);
    }

    @Override
    protected void stopBundle() throws Exception {
        CachingKeyStoreServiceImpl keyStoreService = this.keyStoreService;
        if (keyStoreService != null) {
            this.keyStoreService = null;
            keyStoreService.close();
        }
        super.stopBundle();
    }

}
