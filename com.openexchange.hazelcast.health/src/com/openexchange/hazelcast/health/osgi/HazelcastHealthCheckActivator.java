/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.hazelcast.health.osgi;

import com.hazelcast.core.HazelcastInstance;
import com.openexchange.hazelcast.health.internal.HazelcastCheck;
import com.openexchange.health.MWHealthCheck;
import com.openexchange.health.MWHealthCheckService;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link HazelcastHealthCheckActivator}
 *
 * @author <a href="mailto:philipp.schumacher@open-xchange.com">Philipp Schumacher</a>
 */
public class HazelcastHealthCheckActivator extends HousekeepingActivator {

    /**
     * Initializes a new {@link HazelcastHealthCheckActivator}.
     */
    public HazelcastHealthCheckActivator() {
        super();
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] { MWHealthCheckService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        registerService(MWHealthCheck.class, new HazelcastCheck(this));
        trackService(HazelcastInstance.class);
        openTrackers();
    }

}
