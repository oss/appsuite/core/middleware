/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.json.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.annotation.restricted.RestrictedAction;
import com.openexchange.chronos.CalendarUser;
import com.openexchange.chronos.json.converter.mapper.CalendarUserMapping;
import com.openexchange.chronos.provider.composition.IDBasedCalendarAccess;
import com.openexchange.chronos.scheduling.ScheduleStatus;
import com.openexchange.chronos.service.CalendarParameters;
import com.openexchange.chronos.service.EventID;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.servlet.AjaxExceptionCodes;

/**
 * {@link ForwardAction} - Forwards an event to the given recipients
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 */
@RestrictedAction(module = ChronosAction.MODULE, type = RestrictedAction.Type.READ)
public class ForwardAction extends ChronosAction {

    private static final String PARAMETER_RECIPIENTS = "recipients";
    private static final String PARAM_COMMENT = BODY_PARAM_COMMENT;

    /**
     * Initializes a new {@link ForwardAction}.
     *
     * @param services A service lookup reference
     */
    protected ForwardAction(ServiceLookup services) {
        super(services);
    }

    @Override
    protected Set<String> getOptionalParameters() {
        return Set.of();
    }

    @Override
    protected AJAXRequestResult perform(IDBasedCalendarAccess calendarAccess, AJAXRequestData requestData) throws OXException {
        JSONObject body = extractJsonBody(requestData);
        try {
            /*
             * Parse and build message
             */
            EventID eventId = parseIdParameter(requestData);
            JSONArray jsonArray = body.getJSONArray(PARAMETER_RECIPIENTS);
            List<CalendarUser> recipients = new ArrayList<>(jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                recipients.add(CalendarUserMapping.deserialize(jsonArray.getJSONObject(i), new CalendarUser()));
            }

            String comment = body.optString(PARAM_COMMENT);
            if (Strings.isNotEmpty(comment)) {
                calendarAccess.set(CalendarParameters.PARAMETER_COMMENT, comment);
            }
            /*
             * Forward message
             */
            Map<CalendarUser, ScheduleStatus> statusPerRecipient = calendarAccess.forwardEvent(eventId, recipients);
            /*
             * Sent response
             */
            JSONArray jArray = new JSONArray(statusPerRecipient.size());
            int i = 0;
            for (Entry<CalendarUser, ScheduleStatus> entry : statusPerRecipient.entrySet()) {
                JSONObject json = new JSONObject(2);
                json.put("recipient", CalendarUserMapping.serialize(entry.getKey()));
                json.put("status", entry.getValue().toString());
                jArray.add(i++, json);
            }
            return new AJAXRequestResult(jArray);
        } catch (JSONException e) {
            throw AjaxExceptionCodes.JSON_ERROR.create(e.getMessage(), e);
        }
    }

}
