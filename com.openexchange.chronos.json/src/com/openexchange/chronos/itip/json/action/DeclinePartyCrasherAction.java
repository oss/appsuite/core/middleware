/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
* 
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.chronos.itip.json.action;

import static com.openexchange.chronos.common.CalendarUtils.injectComment;
import static com.openexchange.chronos.common.CalendarUtils.isInternal;
import static com.openexchange.chronos.common.CalendarUtils.optTimeZone;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import org.dmfs.rfc5545.DateTime;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.chronos.CalendarObjectResource;
import com.openexchange.chronos.CalendarUser;
import com.openexchange.chronos.CalendarUserType;
import com.openexchange.chronos.json.converter.mapper.CalendarUserMapping;
import com.openexchange.chronos.provider.composition.IDBasedCalendarAccess;
import com.openexchange.chronos.scheduling.IncomingSchedulingMessage;
import com.openexchange.chronos.scheduling.MessageStatus;
import com.openexchange.chronos.scheduling.MessageStatusService;
import com.openexchange.chronos.scheduling.RecipientSettings;
import com.openexchange.chronos.scheduling.ScheduleStatus;
import com.openexchange.chronos.scheduling.SchedulingBroker;
import com.openexchange.chronos.scheduling.SchedulingMessage;
import com.openexchange.chronos.scheduling.SchedulingMethod;
import com.openexchange.chronos.scheduling.changes.ScheduleChange;
import com.openexchange.chronos.scheduling.changes.SchedulingChangeService;
import com.openexchange.chronos.scheduling.common.FallbackRecipientSettings;
import com.openexchange.chronos.scheduling.common.MessageBuilder;
import com.openexchange.chronos.service.CalendarUtilities;
import com.openexchange.chronos.service.EntityResolver;
import com.openexchange.exception.OXException;
import com.openexchange.java.util.TimeZones;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.tools.arrays.Collections;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link DeclinePartyCrasherAction} - Sends a <code>CANCEL</code> message to unknown calendar user of a <code>REPLY</code> message
 *
 * @author <a href="mailto:daniel.beckerlopen-xchange.com">Daniel Becker</a>
 */
public class DeclinePartyCrasherAction extends AbstractSchedulingAction {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(DeclinePartyCrasherAction.class);
    }

    /**
     * Initializes a new {@link DeclinePartyCrasherAction}.
     * 
     * @param services The service lookup to use
     */
    public DeclinePartyCrasherAction(ServiceLookup services) {
        super(services);
    }

    @Override
    protected IncomingSchedulingMessage getMessage(IDBasedCalendarAccess calendarAccess, int accountId, String folderId, String mailId, String sequenceId) throws OXException {
        /*
         * Use non-patched message to sent back information as-are
         */
        return calendarAccess.getSchedulingAccess().createMessage(accountId, folderId, mailId, sequenceId);
    }

    @Override
    AJAXRequestResult process(AJAXRequestData request, IDBasedCalendarAccess access, IncomingSchedulingMessage message) throws OXException {
        /*
         * Use transmitted data for the CANCEL message. Do not leak any additional data.
         */
        String comment = Utils.getComment(request);
        CalendarObjectResource resource = injectComment(message.getResource(), comment);
        CalendarUser partyCrasher = message.getSchedulingObject().getOriginator();
        CalendarUser originator = getOriginator(message, access.getSession());
        SchedulingMessage schedulingMessage = new MessageBuilder().setMethod(SchedulingMethod.CANCEL)
                                                                  .setOriginator(originator)
                                                                  .setRecipient(partyCrasher)
                                                                  .setResource(resource) // use as-is
                                                                  .setScheduleChange(describeCancel(message, originator, comment))
                                                                  .setRecipientSettings(initRecipientSettings(request.getSession(), partyCrasher, message.getResource()))
                                                                  .build();
        List<ScheduleStatus> status = services.getServiceSafe(SchedulingBroker.class).handleScheduling(access.getSession(), List.of(schedulingMessage));
        /*
         * Mark message as processed and send result
         */
        services.getServiceSafe(MessageStatusService.class).setMessageStatus(access.getSession(), message, MessageStatus.APPLIED);
        return createResult(partyCrasher, status);
    }

    /*
     * ============================== HELPERS ==============================
     */

    /**
     * Get the originator of message to sent
     *
     * @param message The message to get the resource from
     * @param session The user session as fallback originator
     * @return The originator, never <code>null</code>
     */
    private CalendarUser getOriginator(IncomingSchedulingMessage message, Session session) {
        /*
         * Use organizer if available
         */
        if (null != message.getResource().getOrganizer()) {
            return message.getResource().getOrganizer();
        }
        /*
         * Use current session user as fallback
         */
        CalendarUser cu = new CalendarUser();
        cu.setEntity(session.getUserId());
        try {
            services.getServiceSafe(CalendarUtilities.class).getEntityResolver(session.getContextId()).applyEntityData(cu, session.getUserId());
        } catch (OXException e) {
            LoggerHolder.LOGGER.error("Error while loading organizer data: {}", e.getMessage(), e);
        }
        return cu;
    }

    private ScheduleChange describeCancel(IncomingSchedulingMessage message, CalendarUser originator, String comment) throws OXException {
        SchedulingChangeService changeService = services.getServiceSafe(SchedulingChangeService.class);
        return changeService.describeCancel(originator, comment, message.getResource());
    }

    /**
     * Initializes the recipient settings for the generated CANCEL message.
     * 
     * @param session The session
     * @param recipient The recipient of the message
     * @param resource The calendar object resource the message is generated for
     * @return The recipient settings
     */
    private RecipientSettings initRecipientSettings(ServerSession session, CalendarUser recipient, CalendarObjectResource resource) {
        TimeZone eventTimeZone = null;
        if (null != resource && null != resource.getFirstEvent()) {
            DateTime startDate = resource.getFirstEvent().getStartDate();
            if (null != startDate && false == startDate.isFloating()) {
                eventTimeZone = startDate.getTimeZone();
            }
        }
        Locale recipientLocale = null;
        TimeZone recipientTimeZone = null;
        try {
            CalendarUtilities calendarUtilities = services.getServiceSafe(CalendarUtilities.class);
            if (null != eventTimeZone) {
                eventTimeZone = calendarUtilities.selectTimeZone(session, session.getUserId(), eventTimeZone, null);
            }
            EntityResolver entityResolver = calendarUtilities.getEntityResolver(session.getContextId());
            CalendarUser preparedRecipient = entityResolver.prepare(new CalendarUser(recipient), CalendarUserType.INDIVIDUAL);
            if (isInternal(preparedRecipient, CalendarUserType.INDIVIDUAL)) {
                recipientLocale = entityResolver.getLocale(preparedRecipient.getEntity());
                recipientTimeZone = entityResolver.getTimeZone(preparedRecipient.getEntity());
            }
        } catch (OXException e) {
            LoggerHolder.LOGGER.error("Error while loading recipient entity data", e);
        }
        TimeZone timeZone = selectTimeZone(session, eventTimeZone, recipientTimeZone);
        Locale locale = null != recipientLocale ? recipientLocale : session.getUser().getLocale();
        return new FallbackRecipientSettings(recipient, timeZone, locale);
    }

    private TimeZone selectTimeZone(ServerSession session, TimeZone eventTimeZone, TimeZone recipientTimeZone) {
        if (null != eventTimeZone) {
            return eventTimeZone;
        }
        return null != recipientTimeZone ? recipientTimeZone : optTimeZone(session.getUser().getTimeZone(), TimeZones.UTC);
    }

    /**
     * Creates the result for the client.
     *
     * @param recipient The recipient of the message
     * @param status The status of the message, if any
     * @return A result for the client
     * @throws OXException
     */
    private AJAXRequestResult createResult(CalendarUser recipient, List<ScheduleStatus> status) {
        if (Collections.isNotEmpty(status)) {
            JSONObject json = new JSONObject(2);
            try {
                json.put("recipient", CalendarUserMapping.serialize(recipient));
                json.put("status", status.get(0).toString());
                return new AJAXRequestResult(json, new Date());
            } catch (JSONException e) {
                LoggerHolder.LOGGER.error("Unable to sent response: {}", e.getMessage(), e);
            }
        }
        return new AJAXRequestResult(null, new Date());
    }

}
