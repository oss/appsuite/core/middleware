/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.contactcollector;

import com.openexchange.config.lean.Property;

/**
 *
 * {@link ContactCollectProperty}
 *
 * @author <a href="mailto:martin.herfurth@open-xchange.com">Martin Herfurth</a>
 */
public enum ContactCollectProperty implements Property {

    /**
     * The flag for contact collection on mail access.
     */
    CONTACT_COLLECT_ON_MAIL_ACCESS("user.contactCollectOnMailAccess", Boolean.FALSE),
    /**
     * The flag for contact collection on outgoing mails.
     */
    CONTACT_COLLECT_ON_MAIL_TRANSPORT("user.contactCollectOnMailTransport", Boolean.FALSE),
    /**
     * Whether deletion of contact collector folder will be denied.
     */
    CONTACT_COLLECT_FOLDER_DELETE_DENIED("contactcollector.folder.deleteDenied", Boolean.FALSE),
    ;

    private final Object defaultValue;
    private final String fqn;
    private final String appendix;

    private ContactCollectProperty(String appendix, Object defaultValue) {
        this.appendix = appendix;
        this.defaultValue = defaultValue;
        this.fqn = "com.openexchange." + appendix;
    }

    @Override
    public String getFQPropertyName() {
        return fqn;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

    public String getAppendix() {
        return appendix;
    }

}
