/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.reseller.rmi.dataobjects;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import com.openexchange.admin.rmi.dataobjects.EnforceableDataObject;
import com.openexchange.admin.rmi.dataobjects.PasswordMechObject;

/**
 * {@link ResellerAdmin}
 *
 * @author <a href="mailto:carsten.hoeger@open-xchange.com">Carsten Hoeger</a>
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class ResellerAdmin extends EnforceableDataObject implements PasswordMechObject, Cloneable {

    private static final long serialVersionUID = 7212339205350666355L;

    /** The reseller's identifier. */
    private Integer id;

    /** Whether the reseller's identifier was set */
    private boolean idset = false;

    /** The reseller's parent identifier. */
    private Integer parentId;

    /** Whether the reseller's parent identifier was set. */
    private boolean parentIdset = false;

    /** The reseller's name */
    private String name;

    /** Whether the reseller's name was set */
    private boolean nameset = false;

    /** The reseller's password */
    private String password;

    /** Whether the reseller's password was set */
    private boolean passwordset = false;

    /** The reseller's password mechanism */
    private String passwordMech;

    /** Whether the reseller's password mechanism was set */
    private boolean passwordMechset = false;

    /** The reseller's salt */
    private byte[] salt;

    /** Whether the reseller's salt was set */
    private boolean saltSet = false;

    /** The reseller's display name */
    private String displayname;

    /** Whether the reseller's display name was set */
    private boolean displaynameset = false;

    /** The reseller's restrictions */
    private Restriction[] restrictions;

    /** Whether the reseller's restrictions were set */
    private boolean restrictionsset = false;

    /** The reseller's parent name */
    private String parentName;

    /** Whether the reseller's parent name was set */
    private boolean parentNameset = false;

    /** The reseller's capabilities */
    private Set<String> capabilities;

    /** The reseller's capabilities-to-add */
    private Set<String> capabilitiesToAdd;

    /** Whether the reseller's capabilities-to-add were set */
    private boolean capasToAddSet = false;

    /** The reseller's capabilities-to-remove */
    private Set<String> capabilitiesToRemove;

    /** Whether the reseller's capabilities-to-remove were set */
    private boolean capasToRemoveSet = false;

    /** The reseller's capabilities-to-drop */
    private Set<String> capabilitiesToDrop;

    /** Whether the reseller's capabilities-to-drop were set */
    private boolean capasToDropSet = false;

    /** The reseller's taxonomies */
    private Set<String> taxonomies;

    /** The reseller's taxonomies-to-add */
    private Set<String> taxonomiesToAdd;

    /** Whether the reseller's taxonomies-to-add were set */
    private boolean taxonomiesToAddSet = false;

    /** The reseller's taxonomies-to-remove */
    private Set<String> taxonomiesToRemove;

    /** Whether the reseller's taxonomies-to-remove were set */
    private boolean taxonomiesToRemoveSet = false;

    /** The reseller's configuration */
    private Map<String, String> configuration;

    /** The reseller's configuration-to-add */
    private Map<String, String> configurationToAdd;

    /** Whether the reseller's configuration-to-add were set */
    private boolean configurationToAddSet = false;

    /** The reseller's configuration-to-remove */
    private Set<String> configurationToRemove;

    /** Whether the reseller's configuration-to-remove were set */
    private boolean configurationToRemoveSet = false;

    /**
     * Initializes a new {@link ResellerAdmin}.
     */
    public ResellerAdmin() {
        super();
        init();
    }

    /**
     * Initializes a new {@link ResellerAdmin}.
     *
     * @param id The reseller identifier
     */
    public ResellerAdmin(final int id) {
        super();
        init();
        setId(Integer.valueOf(id));
    }

    /**
     * Initializes a new {@link ResellerAdmin}.
     *
     * @param name The reseller name
     */
    public ResellerAdmin(final String name) {
        super();
        init();
        setName(name);
    }

    /**
     * Initializes a new {@link ResellerAdmin}.
     *
     * @param id The reseller identifier
     * @param name The reseller name
     */
    public ResellerAdmin(Integer id, String name) {
        super();
        setId(id);
        setName(name);
    }

    /**
     * Initializes a new {@link ResellerAdmin}.
     *
     * @param name The reseller name
     * @param password The reseller password
     */
    public ResellerAdmin(final String name, final String password) {
        super();
        init();
        setName(name);
        setPassword(password);
    }

    /**
     * Gets the reseller's display name
     *
     * @return The display name
     */
    public String getDisplayname() {
        return displayname;
    }

    /**
     * Gets the reseller's display name
     *
     * @return The reseller identifier
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the reseller's name
     *
     * @return The reseller name
     */
    public String getName() {
        return name;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getPasswordMech() {
        return passwordMech;
    }

    @Override
    public byte[] getSalt() {
        return salt;
    }

    /**
     * Gets the reseller's parent identifier
     *
     * @return The reseller parent identifier
     */
    public Integer getParentId() {
        return parentId;
    }

    private void init() {
        id = null;
        parentId = null;
        name = null;
        password = null;
        displayname = null;
        passwordMech = null;
        salt = null;
        restrictions = null;
    }

    /**
     * Gets whether a reseller display name was set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isDisplaynameset() {
        return displaynameset;
    }

    /**
     * Gets whether a reseller identifier was set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isIdset() {
        return idset;
    }

    /**
     * Gets whether a reseller name was set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isNameset() {
        return nameset;
    }

    /**
     * Gets whether a reseller password mechanism was set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isPasswordMechset() {
        return passwordMechset;
    }

    /**
     * Gets whether a reseller salt was set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isSaltSet() {
        return saltSet;
    }

    /**
     * Gets whether a reseller password was set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isPasswordset() {
        return passwordset;
    }

    /**
     * Gets whether a reseller parent identifier was set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isParentIdset() {
        return parentIdset;
    }

    /**
     * Sets the reseller's display name
     *
     * @param displayname The reseller display name to set
     */
    public void setDisplayname(final String displayname) {
        displaynameset = true;
        this.displayname = displayname;
    }

    /**
     * Sets the reseller's identifier
     *
     * @param id The reseller identifier to set
     */
    public void setId(final Integer id) {
        idset = true;
        this.id = id;
    }

    /**
     * Sets the reseller's name
     *
     * @param name The reseller name to set
     */
    public void setName(final String name) {
        nameset = true;
        this.name = name;
    }

    /**
     * Sets the reseller's password
     *
     * @param password The reseller password to set
     */
    public void setPassword(final String password) {
        passwordset = true;
        this.password = password;
    }

    @Override
    public void setPasswordMech(final String passwordMech) {
        passwordMechset = true;
        this.passwordMech = passwordMech;
    }

    /**
     * Sets the reseller's salt
     *
     * @param salt The reseller salt to set
     */
    public void setSalt(final byte[] salt) {
        saltSet = true;
        this.salt = salt;
    }

    /**
     * Sets the reseller's parent identifier
     *
     * @param pid The reseller parent identifier to set
     */
    public void setParentId(final Integer pid) {
        parentIdset = true;
        parentId = pid;
    }

    /**
     * Gets the reseller's capabilities-to-add
     *
     * @return The reseller's capabilities-to-add
     */
    public Set<String> getCapabilitiesToAdd() {
        return capabilitiesToAdd;
    }

    /**
     * Sets the reseller's capabilities-to-add
     *
     * @param capabilitiesToAdd The reseller's capabilities-to-add to set
     */
    public void setCapabilitiesToAdd(Set<String> capabilitiesToAdd) {
        capasToAddSet = true;
        this.capabilitiesToAdd = capabilitiesToAdd;
    }

    /**
     * Gets the reseller's capabilities-to-remove
     *
     * @return The reseller's capabilities-to-remove
     */
    public Set<String> getCapabilitiesToRemove() {
        return capabilitiesToRemove;
    }

    /**
     * Sets the reseller's capabilities-to-remove
     *
     * @param capabilitiesToRemove The reseller's capabilities-to-remove to set
     */
    public void setCapabilitiesToRemove(Set<String> capabilitiesToRemove) {
        capasToRemoveSet = true;
        this.capabilitiesToRemove = capabilitiesToRemove;
    }

    /**
     * Gets the reseller's capabilities-to-drop
     *
     * @return The reseller's capabilities-to-drop
     */
    public Set<String> getCapabilitiesToDrop() {
        return capabilitiesToDrop;
    }

    /**
     * Sets the reseller's capabilities-to-drop
     *
     * @param capabilitiesToDrop The reseller's capabilities-to-drop to set
     */
    public void setCapabilitiesToDrop(Set<String> capabilitiesToDrop) {
        capasToDropSet = true;
        this.capabilitiesToDrop = capabilitiesToDrop;
    }

    /**
     * Gets whether the capabilities-to-add were set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isCapabilitiesToAddSet() {
        return capasToAddSet;
    }

    /**
     * Gets whether the capabilities-to-remove were set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isCapabilitiesToRemoveSet() {
        return capasToRemoveSet;
    }

    /**
     * Gets whether the capabilities-to-drop were set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isCapabilitiesToDropSet() {
        return capasToDropSet;
    }

    /**
     * Gets the reseller's taxonomies-to-add
     *
     * @return The reseller's taxonomies-to-add
     */
    public Set<String> getTaxonomiesToAdd() {
        return taxonomiesToAdd;
    }

    /**
     * Sets the reseller's taxonomies-to-add
     *
     * @param taxonomies The taxonomies-to-add to set
     */
    public void setTaxonomiesToAdd(Set<String> taxonomies) {
        taxonomiesToAddSet = true;
        taxonomiesToAdd = taxonomies;
    }

    /**
     * Gets whether the capabilities-to-add were set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isTaxonomiesToAddSet() {
        return taxonomiesToAddSet;
    }

    /**
     * Gets the reseller's taxonomies-to-remove
     *
     * @return The reseller's taxonomies-to-remove
     */
    public Set<String> getTaxonomiesToRemove() {
        return taxonomiesToRemove;
    }

    /**
     * Sets the reseller's taxonomies-to-remove
     *
     * @param taxonomiesToRemove The reseller's taxonomies-to-remove to set
     */
    public void setTaxonomiesToRemove(Set<String> taxonomiesToRemove) {
        taxonomiesToRemoveSet = true;
        this.taxonomiesToRemove = taxonomiesToRemove;
    }

    /**
     * Gets whether the taxonomies-to-remove were set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isTaxonomiesToRemoveSet() {
        return taxonomiesToRemoveSet;
    }

    /**
     * Gets the reseller's configuration-to-add
     *
     * @return The reseller's configuration-to-add
     */
    public Map<String, String> getConfigurationToAdd() {
        return configurationToAdd;
    }

    /**
     * Sets the reseller's configuration-to-add
     *
     * @param configurationToAdd The reseller's configuration-to-add set
     */
    public void setConfigurationToAdd(Map<String, String> configurationToAdd) {
        configurationToAddSet = true;
        this.configurationToAdd = configurationToAdd;
    }

    /**
     * Gets the reseller's configuration-to-remove
     *
     * @return The reseller's configuration-to-remove
     */
    public Set<String> getConfigurationToRemove() {
        return configurationToRemove;
    }

    /**
     * Sets the reseller's configuration-to-remove
     *
     * @param configurationToRemove The reseller's configuration-to-remove to set
     */
    public void setConfigurationToRemove(Set<String> configurationToRemove) {
        configurationToRemoveSet = true;
        this.configurationToRemove = configurationToRemove;
    }

    /**
     * Gets whether the configuration-to-add was set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isConfigurationToAddSet() {
        return configurationToAddSet;
    }

    /**
     * Gets whether the configuration-to-remove was set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public boolean isConfigurationToRemoveSet() {
        return configurationToRemoveSet;
    }

    /**
     * Gets the reseller's capabilities
     *
     * @return The reseller's capabilities
     */
    public Set<String> getCapabilities() {
        return capabilities;
    }

    /**
     * Sets the reseller's capabilities
     *
     * @param capabilities The reseller's capabilities to set
     */
    public void setCapabilities(Set<String> capabilities) {
        this.capabilities = capabilities;
    }

    /**
     * Gets the reseller's taxonomies
     *
     * @return The reseller's taxonomies
     */
    public Set<String> getTaxonomies() {
        return taxonomies;
    }

    /**
     * Sets the reseller's taxonomies
     *
     * @param taxonomies The reseller's taxonomies to set
     */
    public void setTaxonomies(Set<String> taxonomies) {
        this.taxonomies = taxonomies;
    }

    /**
     * Gets the reseller's configuration
     *
     * @return The reseller's configuration
     */
    public Map<String, String> getConfiguration() {
        return configuration;
    }

    /**
     * Sets the reseller's configuration
     *
     * @param configuration The reseller's configuration to set
     */
    public void setConfiguration(Map<String, String> configuration) {
        this.configuration = configuration;
    }

    /**
     * Gets the reseller's restrictions
     *
     * @return The reseller's restrictions
     */
    public final Restriction[] getRestrictions() {
        return restrictions;
    }

    /**
     * Sets the reseller's restrictions
     *
     * @param restrictions The reseller's restrictions to set
     */
    public final void setRestrictions(final Restriction[] restrictions) {
        restrictionsset = true;
        this.restrictions = restrictions;
    }

    /**
     * Gets whether the reseller's restrictions were set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public final boolean isRestrictionsset() {
        return restrictionsset;
    }

    /**
     * Gets the reseller's parent name
     *
     * @return The reseller's parent name
     */
    public final String getParentName() {
        return parentName;
    }

    /**
     * Sets the reseller's parent name
     *
     * @param parentName The reseller's parent name to set
     */
    public void setParentName(final String parentName) {
        parentNameset = true;
        this.parentName = parentName;
    }

    /**
     * Gets whether the reseller's parent name was set.
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public final boolean isParentNameset() {
        return parentNameset;
    }

    @Override
    public final String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append("[ \n");
        for (final Field f : this.getClass().getDeclaredFields()) {
            try {
                Object ob = f.get(this);
                String tname = f.getName();
                if (ob != null && !tname.equals("serialVersionUID")) {
                    ret.append("  ");
                    ret.append(tname);
                    ret.append(": ");
                    ret.append(ob);
                    ret.append("\n");
                }
            } catch (IllegalArgumentException e) {
                ret.append("IllegalArgument\n");
            } catch (IllegalAccessException e) {
                ret.append("IllegalAccessException\n");
            }
        }
        ret.append(']');
        return ret.toString();
    }

    @Override
    public String[] getMandatoryMembersChange() {
        return null;
    }

    @Override
    public String[] getMandatoryMembersCreate() {
        return new String[] { "displayname", "name", "password" };
    }

    @Override
    public String[] getMandatoryMembersDelete() {
        return null;
    }

    @Override
    public String[] getMandatoryMembersRegister() {
        return null;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((capabilities == null) ? 0 : capabilities.hashCode());
        result = prime * result + ((capabilitiesToAdd == null) ? 0 : capabilitiesToAdd.hashCode());
        result = prime * result + ((capabilitiesToDrop == null) ? 0 : capabilitiesToDrop.hashCode());
        result = prime * result + ((capabilitiesToRemove == null) ? 0 : capabilitiesToRemove.hashCode());
        result = prime * result + (capasToAddSet ? 1231 : 1237);
        result = prime * result + (capasToDropSet ? 1231 : 1237);
        result = prime * result + (capasToRemoveSet ? 1231 : 1237);
        result = prime * result + ((configuration == null) ? 0 : configuration.hashCode());
        result = prime * result + ((configurationToAdd == null) ? 0 : configurationToAdd.hashCode());
        result = prime * result + (configurationToAddSet ? 1231 : 1237);
        result = prime * result + ((configurationToRemove == null) ? 0 : configurationToRemove.hashCode());
        result = prime * result + (configurationToRemoveSet ? 1231 : 1237);
        result = prime * result + ((displayname == null) ? 0 : displayname.hashCode());
        result = prime * result + (displaynameset ? 1231 : 1237);
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + (idset ? 1231 : 1237);
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + (nameset ? 1231 : 1237);
        result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
        result = prime * result + (parentIdset ? 1231 : 1237);
        result = prime * result + ((parentName == null) ? 0 : parentName.hashCode());
        result = prime * result + (parentNameset ? 1231 : 1237);
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((passwordMech == null) ? 0 : passwordMech.hashCode());
        result = prime * result + (passwordMechset ? 1231 : 1237);
        result = prime * result + (passwordset ? 1231 : 1237);
        result = prime * result + Arrays.hashCode(restrictions);
        result = prime * result + (restrictionsset ? 1231 : 1237);
        result = prime * result + Arrays.hashCode(salt);
        result = prime * result + (saltSet ? 1231 : 1237);
        result = prime * result + ((taxonomies == null) ? 0 : taxonomies.hashCode());
        result = prime * result + ((taxonomiesToAdd == null) ? 0 : taxonomiesToAdd.hashCode());
        result = prime * result + (taxonomiesToAddSet ? 1231 : 1237);
        result = prime * result + ((taxonomiesToRemove == null) ? 0 : taxonomiesToRemove.hashCode());
        result = prime * result + (taxonomiesToRemoveSet ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ResellerAdmin other = (ResellerAdmin) obj;
        if (capabilities == null) {
            if (other.capabilities != null) {
                return false;
            }
        } else if (!capabilities.equals(other.capabilities)) {
            return false;
        }
        if (capabilitiesToAdd == null) {
            if (other.capabilitiesToAdd != null) {
                return false;
            }
        } else if (!capabilitiesToAdd.equals(other.capabilitiesToAdd)) {
            return false;
        }
        if (capabilitiesToDrop == null) {
            if (other.capabilitiesToDrop != null) {
                return false;
            }
        } else if (!capabilitiesToDrop.equals(other.capabilitiesToDrop)) {
            return false;
        }
        if (capabilitiesToRemove == null) {
            if (other.capabilitiesToRemove != null) {
                return false;
            }
        } else if (!capabilitiesToRemove.equals(other.capabilitiesToRemove)) {
            return false;
        }
        if (capasToAddSet != other.capasToAddSet) {
            return false;
        }
        if (capasToDropSet != other.capasToDropSet) {
            return false;
        }
        if (capasToRemoveSet != other.capasToRemoveSet) {
            return false;
        }
        if (configuration == null) {
            if (other.configuration != null) {
                return false;
            }
        } else if (!configuration.equals(other.configuration)) {
            return false;
        }
        if (configurationToAdd == null) {
            if (other.configurationToAdd != null) {
                return false;
            }
        } else if (!configurationToAdd.equals(other.configurationToAdd)) {
            return false;
        }
        if (configurationToAddSet != other.configurationToAddSet) {
            return false;
        }
        if (configurationToRemove == null) {
            if (other.configurationToRemove != null) {
                return false;
            }
        } else if (!configurationToRemove.equals(other.configurationToRemove)) {
            return false;
        }
        if (configurationToRemoveSet != other.configurationToRemoveSet) {
            return false;
        }
        if (displayname == null) {
            if (other.displayname != null) {
                return false;
            }
        } else if (!displayname.equals(other.displayname)) {
            return false;
        }
        if (displaynameset != other.displaynameset) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (idset != other.idset) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (nameset != other.nameset) {
            return false;
        }
        if (parentId == null) {
            if (other.parentId != null) {
                return false;
            }
        } else if (!parentId.equals(other.parentId)) {
            return false;
        }
        if (parentIdset != other.parentIdset) {
            return false;
        }
        if (parentName == null) {
            if (other.parentName != null) {
                return false;
            }
        } else if (!parentName.equals(other.parentName)) {
            return false;
        }
        if (parentNameset != other.parentNameset) {
            return false;
        }
        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }
        if (passwordMech == null) {
            if (other.passwordMech != null) {
                return false;
            }
        } else if (!passwordMech.equals(other.passwordMech)) {
            return false;
        }
        if (passwordMechset != other.passwordMechset) {
            return false;
        }
        if (passwordset != other.passwordset) {
            return false;
        }
        if (!Arrays.equals(restrictions, other.restrictions)) {
            return false;
        }
        if (restrictionsset != other.restrictionsset) {
            return false;
        }
        if (!Arrays.equals(salt, other.salt)) {
            return false;
        }
        if (saltSet != other.saltSet) {
            return false;
        }
        if (taxonomies == null) {
            if (other.taxonomies != null) {
                return false;
            }
        } else if (!taxonomies.equals(other.taxonomies)) {
            return false;
        }
        if (taxonomiesToAdd == null) {
            if (other.taxonomiesToAdd != null) {
                return false;
            }
        } else if (!taxonomiesToAdd.equals(other.taxonomiesToAdd)) {
            return false;
        }
        if (taxonomiesToAddSet != other.taxonomiesToAddSet) {
            return false;
        }
        if (taxonomiesToRemove == null) {
            if (other.taxonomiesToRemove != null) {
                return false;
            }
        } else if (!taxonomiesToRemove.equals(other.taxonomiesToRemove)) {
            return false;
        }
        if (taxonomiesToRemoveSet != other.taxonomiesToRemoveSet) {
            return false;
        }
        return true;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * Prints out the reseller data
     *
     * @return The reseller data as string
     */
    public String printOut() {
        StringBuilder ret = new StringBuilder();
        ret.append("[ \n");
        for (Field f : this.getClass().getDeclaredFields()) {
            try {
                Object ob = f.get(this);
                String tname = f.getName();
                if (tname.equals("restrictions") && restrictions != null) {
                    ret.append("  ");
                    ret.append(tname);
                    ret.append(": ");
                    for (Restriction r : restrictions) {
                        ret.append('{');
                        ret.append(r).append("}, ");
                    }
                    ret.setLength(ret.length() - 2);
                    ret.append("\n");
                } else if (ob != null && (!tname.equals("serialVersionUID") && !tname.equals("password") && !tname.equals("salt"))) {
                    ret.append("  ");
                    ret.append(tname);
                    ret.append(": ");
                    ret.append(ob);
                    ret.append("\n");
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                ret.append(e.getClass().getSimpleName()).append("\n");
            }
        }
        ret.append(']');
        return ret.toString();
    }
}
