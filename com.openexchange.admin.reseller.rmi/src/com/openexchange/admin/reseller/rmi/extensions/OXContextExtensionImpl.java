/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.admin.reseller.rmi.extensions;

import com.openexchange.admin.reseller.rmi.dataobjects.ResellerAdmin;
import com.openexchange.admin.reseller.rmi.dataobjects.Restriction;
import com.openexchange.admin.rmi.extensions.OXCommonExtension;

/**
 * {@link OXContextExtensionImpl} Implementation of OXCommonExtension
 */
public class OXContextExtensionImpl extends OXCommonExtension {

    private static final long serialVersionUID = 8443761921961452860L;

    /**
     * The context error text
     */
    private String errortext;

    /**
     * The context owner
     */
    private ResellerAdmin owner;

    /**
     * The context custom id
     */
    private String customid;

    /**
     * The context restrictions
     */
    private Restriction[] restrictions;

    /**
     * Whether the context restrictions were set or not
     */
    private boolean bRestrictions;

    /**
     * Whether the context owner was set or not
     */
    private boolean bOwner;

    /**
     * The context sid
     */
    private int sid;

    /**
     * Whether the context sid was set or not
     */
    private boolean bSid;

    /**
     * Whether the context custom id was set or not
     */
    private boolean bCustomid;

    /**
     * Initializes a new {@link OXContextExtensionImpl}.
     */
    public OXContextExtensionImpl() {
        super();
    }

    /**
     * Initializes a new {@link OXContextExtensionImpl}.
     *
     * @param sid The sid
     */
    public OXContextExtensionImpl(final int sid) {
        super();
        setSid(sid);
    }

    /**
     * Initializes a new {@link OXContextExtensionImpl}.
     *
     * @param owner The owner
     */
    public OXContextExtensionImpl(final ResellerAdmin owner) {
        super();
        setOwner(owner);
    }

    /**
     * Initializes a new {@link OXContextExtensionImpl}.
     *
     * @param restrictions The restrictions
     */
    public OXContextExtensionImpl(final Restriction[] restrictions) {
        super();
        setRestrictions(restrictions);
    }

    /**
     * Initializes a new {@link OXContextExtensionImpl}.
     *
     * @param owner The owner
     * @param restrictions The restrictions
     */
    public OXContextExtensionImpl(final ResellerAdmin owner, final Restriction[] restrictions) {
        super();
        setOwner(owner);
        setRestrictions(restrictions);
    }

    /**
     * Initializes a new {@link OXContextExtensionImpl}.
     *
     * @param customid The custom id
     */
    public OXContextExtensionImpl(final String customid) {
        super();
        setCustomid(customid);
    }

    /**
     * Returns the owner of this context
     *
     * @return The owner of this context
     */
    public final ResellerAdmin getOwner() {
        return owner;
    }

    /**
     * Gets the sid of this context
     *
     * @return The sid of this context
     */
    public final int getSid() {
        return sid;
    }

    @Override
    public void setExtensionError(final String errortext) {
        this.errortext = errortext;
    }

    @Override
    public String getExtensionError() {
        return this.errortext;
    }

    /**
     * Sets the owner of this context
     *
     * @param owner The owner to set
     */
    public final void setOwner(final ResellerAdmin owner) {
        this.bOwner = true;
        this.owner = owner;
    }

    /**
     * Sets the sid of this context
     *
     * @param sid The sid to set
     */
    public final void setSid(int sid) {
        this.bSid = true;
        this.sid = sid;
    }

    /**
     * Gets whether the owner was set or not
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public final boolean isOwnerset() {
        return bOwner;
    }

    /**
     * Gets whether the sid was set or not
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public final boolean isSidset() {
        return bSid;
    }

    /**
     * Gets the restrictions of this context
     *
     * @return The restrictions
     */
    public final Restriction[] getRestriction() {
        return restrictions;
    }

    /**
     * Sets the restrictions of this context
     *
     * @param restrictions The restrictions to set
     */
    public final void setRestrictions(Restriction[] restrictions) {
        this.bRestrictions = true;
        this.restrictions = restrictions;
    }

    /**
     * Gets whether the restrictions were set or not
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public final boolean isRestrictionset() {
        return bRestrictions;
    }

    /**
     * Gets whether the custom id were set or not
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public final boolean isCustomidset() {
        return bCustomid;
    }

    /**
     * Gets the custom id of this context
     *
     * @return <code>true</code> if so, otherwise <code>false</code>
     */
    public final String getCustomid() {
        return customid;
    }


    /**
     * Sets the custom id of this context
     *
     * @param customid the customid to set
     */
    public final void setCustomid(final String customid) {
        this.bCustomid = true;
        this.customid = customid;
    }

}
